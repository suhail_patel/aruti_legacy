﻿'************************************************************************************************************************************
'Class Name : clszipcode_master.vb
'Purpose    :  All City Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :29/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clszipcode_master
    Private Shared ReadOnly mstrModuleName As String = "clszipcode_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintZipcodeunkid As Integer
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mstrZipcode_Code As String = String.Empty
    Private mstrZipcode_No As String = String.Empty
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set zipcodeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Zipcodeunkid() As Integer
        Get
            Return mintZipcodeunkid
        End Get
        Set(ByVal value As Integer)
            mintZipcodeunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set zipcode_code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Zipcode_Code() As String
        Get
            Return mstrZipcode_Code
        End Get
        Set(ByVal value As String)
            mstrZipcode_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set zipcode_no
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Zipcode_No() As String
        Get
            Return mstrZipcode_No
        End Get
        Set(ByVal value As String)
            mstrZipcode_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  zipcodeunkid " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", zipcode_code " & _
              ", zipcode_no " & _
              ", isactive " & _
             "FROM hrmsConfiguration..cfzipcode_master " & _
             "WHERE zipcodeunkid = @zipcodeunkid "

            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintZipcodeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintZipcodeunkid = CInt(dtRow.Item("zipcodeunkid"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mintCityunkid = CInt(dtRow.Item("cityunkid"))
                mstrZipcode_Code = dtRow.Item("zipcode_code").ToString
                mstrZipcode_No = dtRow.Item("zipcode_no").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblFlag As Boolean = False, Optional ByVal intCityunkid As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblFlag Then
                strQ = "SELECT 0 as zipcodeunkid, 0 countryunkid, '' country, 0 stateunkid, '' state, 0 as cityunkid,'' as city, '' zipcode_code , '' + @zipcode_no as zipcode_no, 1 as active  UNION "
            End If
            strQ &= "SELECT " & _
              "  hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
              ", hrmsConfiguration..cfzipcode_master.countryunkid " & _
              ", hrmsConfiguration..cfcountry_master.country_name as country" & _
              ", hrmsConfiguration..cfzipcode_master.stateunkid " & _
              ", hrmsConfiguration..cfstate_master.name as state " & _
              ", hrmsConfiguration..cfzipcode_master.cityunkid " & _
              ", hrmsConfiguration..cfcity_master.name as city " & _
              ", hrmsConfiguration..cfzipcode_master.zipcode_code " & _
              ", hrmsConfiguration..cfzipcode_master.zipcode_no " & _
              ", hrmsConfiguration..cfzipcode_master.isactive " & _
             "FROM hrmsConfiguration..cfzipcode_master " & _
             " LEFT JOIN hrmsConfiguration..cfcountry_master on hrmsConfiguration..cfcountry_master.countryunkid = hrmsConfiguration..cfzipcode_master.countryunkid " & _
             " LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = hrmsConfiguration..cfzipcode_master.stateunkid " & _
             " LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = hrmsConfiguration..cfzipcode_master.cityunkid "

            If blnOnlyActive Then
                strQ &= " WHERE hrmsConfiguration..cfzipcode_master.isactive = 1 "
            End If

            If intCityunkid > 0 Then
                If blnOnlyActive Then
                    strQ &= "AND hrmsConfiguration..cfzipcode_master.cityunkid=@cityunkid"
                Else
                    strQ &= " WHERE hrmsConfiguration..cfzipcode_master.cityunkid=@cityunkid "
                End If
                objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCityunkid)
            End If

            If mblFlag = True Then
                objDataOperation.AddParameter("@zipcode_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select Zip code"))
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrmsConfiguration..cfzipcode_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintCountryunkid, mstrZipcode_Code) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Zip Code is already defined. Please define new Zip Code.")
            Return False
        ElseIf isExist(mintCountryunkid, "", mstrZipcode_No) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Zip Code No is already defined. Please define new Zip Code no.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@zipcode_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrZipcode_Code.ToString)
            objDataOperation.AddParameter("@zipcode_no", SqlDbType.NVarChar, eZeeDataType.ZIPCODE_SIZE, mstrZipcode_No.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO hrmsConfiguration..cfzipcode_master ( " & _
              "  countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", zipcode_code " & _
              ", zipcode_no " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @countryunkid " & _
              ", @stateunkid " & _
              ", @cityunkid " & _
              ", @zipcode_code " & _
              ", @zipcode_no " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintZipcodeunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmsConfiguration..cfzipcode_master", "zipcodeunkid", mintZipcodeunkid, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrmsConfiguration..cfzipcode_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintCountryunkid, mstrZipcode_Code, "", mintZipcodeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Zip Code is already defined. Please define new Zip Code.")
            Return False
        ElseIf isExist(mintCountryunkid, "", mstrZipcode_No, mintZipcodeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Zip Code No is already defined. Please define new Zip Code no.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintZipcodeunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@zipcode_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrZipcode_Code.ToString)
            objDataOperation.AddParameter("@zipcode_no", SqlDbType.NVarChar, eZeeDataType.ZIPCODE_SIZE, mstrZipcode_No.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE hrmsConfiguration..cfzipcode_master SET " & _
              "  countryunkid = @countryunkid" & _
              ", stateunkid = @stateunkid" & _
              ", cityunkid = @cityunkid" & _
              ", zipcode_code = @zipcode_code" & _
              ", zipcode_no = @zipcode_no" & _
              ", isactive = @isactive " & _
              ", Syncdatetime  = NULL " & _
            "WHERE zipcodeunkid = @zipcodeunkid " 'Sohail (28 Mar 2012) - [Syncdatetime]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsConfigTableDataUpdate("atconfigcommon_log", "hrmsConfiguration..cfzipcode_master", mintZipcodeunkid, "zipcodeunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmsConfiguration..cfzipcode_master", "zipcodeunkid", mintZipcodeunkid, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrmsConfiguration..cfzipcode_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected ZipCode. Reason: This ZipCode is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 


        Try
            'strQ = "DELETE FROM hrmsConfiguration..cfzipcode_master " & _
            '"WHERE zipcodeunkid = @zipcodeunkid "


            strQ = "UPDATE hrmsConfiguration..cfzipcode_master " & _
                  " SET isactive = 0 " & _
                  "WHERE zipcodeunkid = @zipcodeunkid "

            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrmsConfiguration..cfzipcode_master", "zipcodeunkid", intUnkid, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrCurrentDatabase As String = Nothing
        Dim blnIsUsed As Boolean = False
        Dim dsCompanyDatabase As DataSet = Nothing


        objDataOperation = New clsDataOperation

        Try
            mstrCurrentDatabase = FinancialYear._Object._DatabaseName

            strQ = " USE hrmsConfiguration " & _
                    " SELECT " & _
                    "TABLE_NAME AS TableName " & _
                     ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('zipcodeunkid','pincodeunkid','postalunkid') "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)



            For Each dtRow As DataRow In dsList.Tables("List").Rows
                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'If dtRow.Item("TableName") = "hrmsConfiguration..cfzipcode_master" Then Continue For
                If dtRow.Item("TableName").ToString.Trim = "cfzipcode_master" Then Continue For
                'S.SANDEEP [ 12 OCT 2011 ] -- END 
                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & "hrmsConfiguration" & ".." & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @zipcodeunkid "


                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'dsList = objDataOperation.ExecQuery(strQ, "Used")
                Dim dsList1 As DataSet = objDataOperation.ExecQuery(strQ, "Used")
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'If dsList.Tables("Used").Rows.Count > 0 Then
                '    blnIsUsed = True
                '    Exit For
                'End If

                If dsList1.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Next

            If mstrCurrentDatabase <> "" Then

            If blnIsUsed = False Then
                strQ = ""
                strQ = "SELECT " & _
                       "companyunkid " & _
                      ",start_date " & _
                      ",end_date " & _
                      ",code " & _
                      ",database_name " & _
                   "FROM " & _
                       "( SELECT " & _
                           "hrmsConfiguration..cffinancial_year_tran.companyunkid " & _
                          ",hrmsConfiguration..cffinancial_year_tran.start_date " & _
                          ",hrmsConfiguration..cffinancial_year_tran.end_date " & _
                          ",hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                          ",hrmsConfiguration..cffinancial_year_tran.database_name " & _
                          ",row_number() OVER ( PARTITION BY hrmsConfiguration..cffinancial_year_tran.companyunkid ORDER BY hrmsConfiguration..cffinancial_year_tran.yearunkid DESC ) rn " & _
                          ",cfcompany_master.code " & _
                         "FROM hrmsConfiguration..cffinancial_year_tran ,hrmsConfiguration..cfcompany_master " & _
                         "WHERE hrmsConfiguration..cffinancial_year_tran.companyunkid = hrmsConfiguration..cfcompany_master.companyunkid " & _
                           "AND hrmsConfiguration..cfcompany_master.isactive = 1 " & _
                       ") AS A " & _
                   "WHERE rn = 1 "


                dsCompanyDatabase = objDataOperation.ExecQuery(strQ, "CompanyDatabase")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = ""
                strQ = " USE " & mstrCurrentDatabase & " " & _
                        " SELECT " & _
                        "TABLE_NAME AS TableName " & _
                        ",COLUMN_NAME " & _
                        "FROM INFORMATION_SCHEMA.COLUMNS " & _
                        "WHERE COLUMN_NAME IN ('zipcodeunkid','pincodeunkid','present_postcodeunkid','perm_postcodeunkid','domicile_postcodeunkid','present_zipcode','perm_zipcode','postalunkid') "


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                End If


                strQ = ""

                If dsCompanyDatabase.Tables("CompanyDatabase").Rows.Count > 0 Then
                    For Each dtDatabaseRow As DataRow In dsCompanyDatabase.Tables("CompanyDatabase").Rows

                        'S.SANDEEP [ 18 JUL 2014 ] -- START
                        If objDataOperation.RecordCount("SELECT * FROM sys.databases WHERE databases.name = '" & dtDatabaseRow.Item("database_name") & "'") <= 0 Then Continue For
                        'S.SANDEEP [ 18 JUL 2014 ] -- END

                        For Each dtRow As DataRow In dsList.Tables("List").Rows
                            strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtDatabaseRow.Item("database_name") & ".." & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @zipcodeunkid "


                            'S.SANDEEP [ 12 OCT 2011 ] -- START
                            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                            'dsList = objDataOperation.ExecQuery(strQ, "Used")
                            Dim dsList1 As DataSet = objDataOperation.ExecQuery(strQ, "Used")
                            'S.SANDEEP [ 12 OCT 2011 ] -- END 



                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If


                            'S.SANDEEP [ 12 OCT 2011 ] -- START
                            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                            'If dsList.Tables("Used").Rows.Count > 0 Then
                            '    blnIsUsed = True
                            '    Exit For
                            'End If
                            If dsList1.Tables("Used").Rows.Count > 0 Then
                                blnIsUsed = True
                                Exit For
                            End If
                            'S.SANDEEP [ 12 OCT 2011 ] -- END 

                        Next
                        If blnIsUsed = True Then Exit For
                    Next
                End If
            End If

            mstrMessage = ""

            If mstrCurrentDatabase.Length > 0 Then
                eZeeDatabase.change_database(mstrCurrentDatabase)
            Else
                eZeeDatabase.change_database("hrmsConfiguration")
            End If

            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intCountryunkid As Integer, Optional ByVal strZipcode As String = "", Optional ByVal strZipno As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  zipcodeunkid " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", zipcode_code " & _
              ", zipcode_no " & _
              ", isactive " & _
             "FROM hrmsConfiguration..cfzipcode_master " & _
             "WHERE 1=1 and countryunkid = @countryunkid"


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strZipcode.Length > 0 Then
                strQ &= " AND zipcode_code = @zipcode_code "
            End If

            If strZipno.Length > 0 Then
                strQ &= " AND zipcode_no = @zipcode_no "
            End If


            If intUnkid > 0 Then
                strQ &= " AND zipcodeunkid <> @zipcodeunkid"
            End If

            objDataOperation.AddParameter("@zipcode_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strZipcode)
            objDataOperation.AddParameter("@zipcode_no", SqlDbType.NVarChar, eZeeDataType.ZIPCODE_SIZE, strZipno)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCountryunkid)
            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (10-Mar-2011) --Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetZipCodeUnkId(ByVal mstrZipcode As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " zipcodeunkid " & _
                      "  FROM hrmsConfiguration..cfzipcode_master " & _
                      " WHERE zipcode_no = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND hrmsConfiguration..cfzipcode_master.isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrZipcode)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("zipcodeunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetZipCodeUnkId", mstrModuleName)
        End Try
        Return -1

    End Function

    'Pinkal (10-Mar-2011) --End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Zip Code is already defined. Please define new Zip Code.")
			Language.setMessage(mstrModuleName, 2, "Select Zip code")
			Language.setMessage(mstrModuleName, 3, "This Zip Code No is already defined. Please define new Zip Code no.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


''************************************************************************************************************************************
''Class Name : clszipcode_master.vb
''Purpose    :  All City Opration like getList, Insert, Update, Delete, checkDuplicate
''Date       :29/06/2010
''Written By :Pinkal
''Modified   :
''Last Message Index = 3
''************************************************************************************************************************************

'Imports eZeeCommonLib
'''' <summary>
'''' Purpose: 
'''' Developer: Pinkal
'''' </summary>
'Public Class clszipcode_master
'    Private Shared Readonly mstrModuleName As String = "clszipcode_master"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintZipcodeunkid As Integer
'    Private mintCountryunkid As Integer
'    Private mintStateunkid As Integer
'    Private mintCityunkid As Integer
'    Private mstrZipcode_Code As String = String.Empty
'    Private mstrZipcode_No As String = String.Empty
'    Private mblnIsactive As Boolean = True
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set zipcodeunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Zipcodeunkid() As Integer
'        Get
'            Return mintZipcodeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintZipcodeunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set countryunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Countryunkid() As Integer
'        Get
'            Return mintCountryunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintCountryunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set stateunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Stateunkid() As Integer
'        Get
'            Return mintStateunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintStateunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set cityunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Cityunkid() As Integer
'        Get
'            Return mintCityunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintCityunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set zipcode_code
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Zipcode_Code() As String
'        Get
'            Return mstrZipcode_Code
'        End Get
'        Set(ByVal value As String)
'            mstrZipcode_Code = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set zipcode_no
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Zipcode_No() As String
'        Get
'            Return mstrZipcode_No
'        End Get
'        Set(ByVal value As String)
'            mstrZipcode_No = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isactive
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Isactive() As Boolean
'        Get
'            Return mblnIsactive
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsactive = Value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "SELECT " & _
'              "  zipcodeunkid " & _
'              ", countryunkid " & _
'              ", stateunkid " & _
'              ", cityunkid " & _
'              ", zipcode_code " & _
'              ", zipcode_no " & _
'              ", isactive " & _
'             "FROM hrzipcode_master " & _
'             "WHERE zipcodeunkid = @zipcodeunkid "

'            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintZipcodeUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintzipcodeunkid = CInt(dtRow.Item("zipcodeunkid"))
'                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
'                mintstateunkid = CInt(dtRow.Item("stateunkid"))
'                mintcityunkid = CInt(dtRow.Item("cityunkid"))
'                mstrzipcode_code = dtRow.Item("zipcode_code").ToString
'                mstrzipcode_no = dtRow.Item("zipcode_no").ToString
'                mblnisactive = CBool(dtRow.Item("isactive"))
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub


'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblFlag As Boolean = False, Optional ByVal intCityunkid As Integer = 0) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            If mblFlag Then
'                strQ = "SELECT 0 as zipcodeunkid, 0 countryunkid, '' country, 0 stateunkid, '' state, 0 as cityunkid,'' as city, '' zipcode_code , '' + @zipcode_no as zipcode_no, 1 as active  UNION "
'            End If
'            strQ &= "SELECT " & _
'              "  hrzipcode_master.zipcodeunkid " & _
'              ", hrzipcode_master.countryunkid " & _
'              ", cfcountry_master.country_name as country" & _
'              ", hrzipcode_master.stateunkid " & _
'              ", hrstate_master.name as state " & _
'              ", hrzipcode_master.cityunkid " & _
'              ", hrcity_master.name as city " & _
'              ", hrzipcode_master.zipcode_code " & _
'              ", hrzipcode_master.zipcode_no " & _
'              ", hrzipcode_master.isactive " & _
'             "FROM hrzipcode_master " & _
'             " LEFT JOIN cfcountry_master on cfcountry_master.countryunkid = hrzipcode_master.countryunkid " & _
'             " LEFT JOIN hrstate_master on hrstate_master.stateunkid = hrzipcode_master.stateunkid " & _
'             " LEFT JOIN hrcity_master on hrcity_master.cityunkid = hrzipcode_master.cityunkid "

'            If blnOnlyActive Then
'                strQ &= " WHERE hrzipcode_master.isactive = 1 "
'            End If

'            If intCityunkid > 0 Then
'                If blnOnlyActive Then
'                    strQ &= "AND hrzipcode_master.cityunkid=@cityunkid"
'                Else
'                    strQ &= " WHERE hrzipcode_master.cityunkid=@cityunkid "
'                End If
'                objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCityunkid)
'            End If

'            If mblFlag = True Then
'                objDataOperation.AddParameter("@zipcode_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select Zipcode"))
'            End If


'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function


'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrzipcode_master) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mintCountryunkid, mstrZipcode_Code) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This ZipCode is already defined. Please define new ZipCode.")
'            Return False
'        ElseIf isExist(mintCountryunkid, "", mstrZipcode_No) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This ZipCode No is already defined. Please define new ZipCode no.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
'            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
'            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
'            objDataOperation.AddParameter("@zipcode_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrzipcode_code.ToString)
'            objDataOperation.AddParameter("@zipcode_no", SqlDbType.NVarChar, eZeeDataType.ZIPCODE_SIZE, mstrZipcode_No.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

'            StrQ = "INSERT INTO hrzipcode_master ( " & _
'              "  countryunkid " & _
'              ", stateunkid " & _
'              ", cityunkid " & _
'              ", zipcode_code " & _
'              ", zipcode_no " & _
'              ", isactive" & _
'            ") VALUES (" & _
'              "  @countryunkid " & _
'              ", @stateunkid " & _
'              ", @cityunkid " & _
'              ", @zipcode_code " & _
'              ", @zipcode_no " & _
'              ", @isactive" & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintZipcodeUnkId = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hrzipcode_master) </purpose>
'    Public Function Update() As Boolean
'        If isExist(mintCountryunkid, mstrZipcode_Code, "", mintZipcodeunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This ZipCode is already defined. Please define new ZipCode.")
'            Return False
'        ElseIf isExist(mintCountryunkid, "", mstrZipcode_No, mintZipcodeunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This ZipCode No is already defined. Please define new ZipCode no.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintzipcodeunkid.ToString)
'            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
'            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
'            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
'            objDataOperation.AddParameter("@zipcode_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrzipcode_code.ToString)
'            objDataOperation.AddParameter("@zipcode_no", SqlDbType.NVarChar, eZeeDataType.ZIPCODE_SIZE, mstrZipcode_No.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

'            StrQ = "UPDATE hrzipcode_master SET " & _
'              "  countryunkid = @countryunkid" & _
'              ", stateunkid = @stateunkid" & _
'              ", cityunkid = @cityunkid" & _
'              ", zipcode_code = @zipcode_code" & _
'              ", zipcode_no = @zipcode_no" & _
'              ", isactive = @isactive " & _
'            "WHERE zipcodeunkid = @zipcodeunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (hrzipcode_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected ZipCode. Reason: This ZipCode is in use.")
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "DELETE FROM hrzipcode_master " & _
'            "WHERE zipcodeunkid = @zipcodeunkid "

'            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intCountryunkid As Integer, Optional ByVal strZipcode As String = "", Optional ByVal strZipno As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  zipcodeunkid " & _
'              ", countryunkid " & _
'              ", stateunkid " & _
'              ", cityunkid " & _
'              ", zipcode_code " & _
'              ", zipcode_no " & _
'              ", isactive " & _
'             "FROM hrzipcode_master " & _
'             "WHERE 1=1 and countryunkid = @countryunkid"


'            If strZipcode.Length > 0 Then
'                strQ &= " AND zipcode_code = @zipcode_code "
'            End If

'            If strZipno.Length > 0 Then
'                strQ &= " AND zipcode_no = @zipcode_no "
'            End If


'            If intUnkid > 0 Then
'                strQ &= " AND zipcodeunkid <> @zipcodeunkid"
'            End If

'            objDataOperation.AddParameter("@zipcode_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strZipcode)
'            objDataOperation.AddParameter("@zipcode_no", SqlDbType.NVarChar, eZeeDataType.ZIPCODE_SIZE, strZipno)
'            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCountryunkid)
'            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function


'End Class
