﻿Option Strict On

#Region " Import "

Imports eZeeCommonLib
Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

#End Region

Public Class clsMapArutiOrbitParams
    Private Shared ReadOnly mstrModuleName As String = "clsMapArutiOrbitParams"
    Dim mstrMessage As String = ""
    Private mdtTran As DataTable

#Region " Enums "

    Public Enum enArutiParameter
        NONE = 0
        BRANCH = 1
        GENDER = 2
        COUNTRY = 3
        STATE = 4
        CITY = 5
        JOB = 6
        NATIONALITY = 7
        TITLE = 24 'clsCommon_Master.enCommonMaster.TITLE
        IDTYPE = 12 'clsCommon_Master.enCommonMaster.IDENTITY_TYPES        
        MARITALSTATUS = 15 'clsCommon_Master.enCommonMaster.MARRIED_STATUS
    End Enum

    Public Enum enOrbitParameter
        NONE = 0
        BRANCH = 1
        GENDER = 2
        PRODUCT = 3
        RISKCODE = 4
        OPENINGREASON = 5
        NATIONALITY = 6
        CUSTTYPE = 7
        TITLE = 8
        COUNTRY = 9
        IDENTITYTYPE = 10
        CITY = 11
        RSM = 12
        MARITALSTATUS = 13
        PROFESSION = 14
        OCCUPATION = 15
        MARKETING = 16
        STATE = 17
        SRCOFFUNDS = 18
        ADDRESSTYPE = 19
        CONTACTMODE = 20
        PROPERTYTYPE = 21
        SYSUSER = 22
        BUSINESSROLE = 23
        ORGANISATION_POSITION = 24
    End Enum

#End Region

#Region " Property "

    Public Property _Datatable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("List")
            mdtTran.Columns.Add("mappingtranid", GetType(System.Int32)).DefaultValue = 0
            mdtTran.Columns.Add("allocationrefid", GetType(System.Int32)).DefaultValue = 0
            mdtTran.Columns.Add("allocationunkid", GetType(System.Int32)).DefaultValue = 0
            mdtTran.Columns.Add("arutiorbitrefid", GetType(System.Int32)).DefaultValue = 0
            mdtTran.Columns.Add("orbitfldname", GetType(System.String)).DefaultValue = ""
            mdtTran.Columns.Add("orbitfldkey", GetType(System.String)).DefaultValue = ""
            mdtTran.Columns.Add("orbitfldvalue", GetType(System.String)).DefaultValue = ""
            mdtTran.Columns.Add("isactive", GetType(System.Boolean)).DefaultValue = True
            mdtTran.Columns.Add("loginemployeeunkid", GetType(System.Int32)).DefaultValue = 0
            mdtTran.Columns.Add("audittype", GetType(System.Int32)).DefaultValue = 0
            mdtTran.Columns.Add("auditdatetime", GetType(System.DateTime)).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("audituserunkid", GetType(System.Int32)).DefaultValue = 0
            mdtTran.Columns.Add("ip", GetType(System.String)).DefaultValue = ""
            mdtTran.Columns.Add("host", GetType(System.String)).DefaultValue = ""
            mdtTran.Columns.Add("form_name", GetType(System.String)).DefaultValue = ""
            mdtTran.Columns.Add("isweb", GetType(System.Boolean)).DefaultValue = False
            mdtTran.Columns.Add("AUD", GetType(System.String)).DefaultValue = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Public Function FetchDictornaryValues(ByVal eType As enOrbitParameter, ByVal intCompanyId As Integer) As DataTable
        Dim objCParam As New clsConfigOptions
        Dim mDicKeyValues As New Dictionary(Of String, String)
        Dim dtTable As DataTable = Nothing
        Try
            Dim strParamKeys() As String = { _
                                            "IsOrbitIntegrated", _
                                            "_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_FETCH_DICTIONARY), _
                                            "OrbitUsername", _
                                            "OrbitPassword", _
                                            "OrbitAgentUsername" _
                                            }
            mDicKeyValues = objCParam.GetKeyValue(intCompanyId, strParamKeys)
            If mDicKeyValues.Keys.Count > 0 Then
                If CBool(mDicKeyValues("IsOrbitIntegrated")) Then
                    If mDicKeyValues.ContainsKey("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_FETCH_DICTIONARY)) = False Then Exit Try
                    If eType <> enOrbitParameter.NONE Then
                        Dim dc As New clsdataDictionaries
                        Dim random As New Random
                        dc.referenceNo = random.Next(10000, 99999).ToString()
                        dc.credentials.username = mDicKeyValues("OrbitUsername")
                        dc.credentials.password = mDicKeyValues("OrbitPassword")
                        dc.credentials.agentUsername = mDicKeyValues("OrbitAgentUsername")
                        dc.dataDictionaries = eType.ToString()

                        Dim request As HttpWebRequest = CType(WebRequest.Create(mDicKeyValues("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_FETCH_DICTIONARY))), HttpWebRequest)
                        request.Method = "POST"
                        request.ContentType = "application/json; charset=utf-8"
                        request.Accept = "application/json"
                        Using streamWriter = New StreamWriter(request.GetRequestStream())
                            streamWriter.Write(JsonConvert.SerializeObject(dc))
                            streamWriter.Flush()
                        End Using
                        Dim strResponse As String = String.Empty
                        Dim httpResponse = CType(request.GetResponse(), HttpWebResponse)
                        Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                            strResponse = streamReader.ReadToEnd()
                        End Using
                        Dim ds As New DataSet
                        If strResponse.Trim.Length > 0 Then
                            clsOrbit_Request_Tran.ConvertJsonToDataset(strResponse, ds)
                            If ds.Tables.Count > 0 Then
                                If ds.Tables.Contains("dictionary") Then
                                    dtTable = ds.Tables("dictionary")
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch wx As WebException
            mstrMessage = "FetchDictornaryValues : " & wx.Message
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FetchDictornaryValues; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    Public Function SaveMapping() As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Try
            Using objDo As New clsDataOperation
                objDo.BindTransaction()
                For i = 0 To mdtTran.Rows.Count - 1
                    objDo.ClearParameters()
                    With mdtTran.Rows(i)
                        If Not IsDBNull(.Item("AUD")) Then
                            Select Case .Item("AUD").ToString()
                                Case "A"
                                    If IsExists(mdtTran.Rows(i), objDo) = False Then
                                        objDo.ClearParameters()
                                        StrQ = "INSERT INTO hrorbitparam_mapping_tran " & _
                                               "( " & _
                                               "     allocationrefid " & _
                                               "    ,allocationunkid " & _
                                               "    ,arutiorbitrefid " & _
                                               "    ,orbitfldname " & _
                                               "    ,orbitfldkey " & _
                                               "    ,orbitfldvalue " & _
                                               "    ,isactive " & _
                                               ") " & _
                                               "VALUES " & _
                                               "( " & _
                                               "     @allocationrefid " & _
                                               "    ,@allocationunkid " & _
                                               "    ,@arutiorbitrefid " & _
                                               "    ,@orbitfldname " & _
                                               "    ,@orbitfldkey " & _
                                               "    ,@orbitfldvalue " & _
                                               "    ,@isactive " & _
                                               "); SELECT @@IDENTITY "

                                        Dim ds As New DataSet
                                        objDo.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationrefid"))
                                        objDo.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationunkid"))
                                        objDo.AddParameter("@arutiorbitrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("arutiorbitrefid"))
                                        objDo.AddParameter("@orbitfldname", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("orbitfldname"))
                                        objDo.AddParameter("@orbitfldkey", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("orbitfldkey"))
                                        objDo.AddParameter("@orbitfldvalue", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("orbitfldvalue"))
                                        objDo.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive"))

                                        ds = objDo.ExecQuery(StrQ, "List")

                                        If objDo.ErrorMessage <> "" Then
                                            objDo.ReleaseTransaction(False)
                                            exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                                            Throw exForce
                                        End If

                                        If ds.Tables(0).Rows.Count > 0 Then
                                            If InsertAutidTrail(mdtTran.Rows(i), CInt(ds.Tables(0).Rows(0)(0)), objDo) = False Then
                                                objDo.ReleaseTransaction(False)
                                                exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                                                Throw exForce
                                            End If
                                        End If
                                    End If

                                Case "U"

                                    StrQ = "UPDATE hrorbitparam_mapping_tran SET " & _
                                           "     allocationrefid = @allocationrefid " & _
                                           "    ,allocationunkid = @allocationunkid " & _
                                           "    ,arutiorbitrefid = @arutiorbitrefid " & _
                                           "    ,orbitfldname = @orbitfldname " & _
                                           "    ,orbitfldkey = @orbitfldkey " & _
                                           "    ,orbitfldvalue = @orbitfldvalue " & _
                                           "    ,isactive = @isactive " & _
                                           "WHERE mappingtranid = @mappingtranid "

                                    objDo.AddParameter("@mappingtranid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("mappingtranid"))
                                    objDo.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationrefid"))
                                    objDo.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationunkid"))
                                    objDo.AddParameter("@arutiorbitrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("arutiorbitrefid"))
                                    objDo.AddParameter("@orbitfldname", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("orbitfldname"))
                                    objDo.AddParameter("@orbitfldkey", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("orbitfldkey"))
                                    objDo.AddParameter("@orbitfldvalue", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("orbitfldvalue"))
                                    objDo.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive"))

                                    objDo.ExecNonQuery(StrQ)

                                    If objDo.ErrorMessage <> "" Then
                                        objDo.ReleaseTransaction(False)
                                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If InsertAutidTrail(mdtTran.Rows(i), CInt(.Item("mappingtranid")), objDo) = False Then
                                        objDo.ReleaseTransaction(False)
                                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                                        Throw exForce
                                    End If

                                Case "D"

                                    StrQ = "UPDATE hrorbitparam_mapping_tran SET " & _
                                           "    isactive = @isactive " & _
                                           "WHERE mappingtranid = @mappingtranid "

                                    objDo.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive"))
                                    objDo.AddParameter("@mappingtranid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("mappingtranid"))

                                    objDo.ExecNonQuery(StrQ)

                                    If objDo.ErrorMessage <> "" Then
                                        objDo.ReleaseTransaction(False)
                                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If InsertAutidTrail(mdtTran.Rows(i), CInt(.Item("mappingtranid")), objDo) = False Then
                                        objDo.ReleaseTransaction(False)
                                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                                        Throw exForce
                                    End If

                            End Select
                        End If
                    End With
                Next
                objDo.ReleaseTransaction(True)
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveMapping; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function InsertAutidTrail(ByVal dr As DataRow, ByVal intMappingTranId As Integer, ByVal objDo As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Try
            StrQ = "INSERT INTO athrorbitparam_mapping_tran " & _
                   "( " & _
                   "     mappingtranid " & _
                   "    ,allocationrefid " & _
                   "    ,allocationunkid " & _
                   "    ,arutiorbitrefid " & _
                   "    ,orbitfldname " & _
                   "    ,orbitfldkey " & _
                   "    ,orbitfldvalue " & _
                   "    ,audittype " & _
                   "    ,auditdatetime " & _
                   "    ,audituserunkid " & _
                   "    ,ip " & _
                   "    ,host " & _
                   "    ,form_name " & _
                   "    ,isweb " & _
                   "    ,loginemployeeunkid " & _
                   ") " & _
                   "VALUES " & _
                   "( " & _
                   "     @mappingtranid " & _
                   "    ,@allocationrefid " & _
                   "    ,@allocationunkid " & _
                   "    ,@arutiorbitrefid " & _
                   "    ,@orbitfldname " & _
                   "    ,@orbitfldkey " & _
                   "    ,@orbitfldvalue " & _
                   "    ,@audittype " & _
                   "    ,@auditdatetime " & _
                   "    ,@audituserunkid " & _
                   "    ,@ip " & _
                   "    ,@host " & _
                   "    ,@form_name " & _
                   "    ,@isweb " & _
                   "    ,@loginemployeeunkid " & _
                   "); "

            objDo.ClearParameters()
            objDo.AddParameter("@mappingtranid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("mappingtranid"))
            objDo.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("allocationrefid"))
            objDo.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("allocationunkid"))
            objDo.AddParameter("@arutiorbitrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("arutiorbitrefid"))
            objDo.AddParameter("@orbitfldname", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("orbitfldname"))
            objDo.AddParameter("@orbitfldkey", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("orbitfldkey"))
            objDo.AddParameter("@orbitfldvalue", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("orbitfldvalue"))
            objDo.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("audittype"))
            objDo.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dr.Item("auditdatetime"))
            objDo.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("audituserunkid"))
            objDo.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("ip"))
            objDo.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("host"))
            objDo.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("form_name"))
            objDo.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dr.Item("isweb"))
            objDo.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("loginemployeeunkid"))

            objDo.ExecNonQuery(StrQ)

            If objDo.ErrorMessage <> "" Then
                exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAutidTrail; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function VoidMapping(ByVal intMappingId As Integer, ByVal dr As DataRow) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Try
            Using objDo As New clsDataOperation
                objDo.BindTransaction()
                objDo.ClearParameters()

                StrQ = "UPDATE hrorbitparam_mapping_tran SET " & _
                       "    isactive = 0 " & _
                       "WHERE mappingtranid = @mappingtranid "

                objDo.AddParameter("@mappingtranid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMappingId)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If InsertAutidTrail(dr, intMappingId, objDo) = False Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                objDo.ReleaseTransaction(True)
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidMapping; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function VoidAllMapping(ByVal dr As DataRow()) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Try
            Using objDo As New clsDataOperation
                objDo.BindTransaction()
                For i = 0 To dr.Length - 1
                    objDo.ClearParameters()

                    StrQ = "UPDATE hrorbitparam_mapping_tran SET " & _
                           "    isactive = 0 " & _
                           "WHERE mappingtranid = @mappingtranid "

                    objDo.AddParameter("@mappingtranid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr(i)("mappingtranid"))

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        objDo.ReleaseTransaction(False)
                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    If InsertAutidTrail(dr(i), CInt(dr(i)("mappingtranid")), objDo) = False Then
                        objDo.ReleaseTransaction(False)
                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                        Throw exForce
                    End If
                Next
                objDo.ReleaseTransaction(True)
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllMapping; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetList(Optional ByVal strList As String = "List", Optional ByVal strFilterString As String = "") As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Try
            If strList.Trim.Length <= 0 Then strList = "List"

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     GrpName " & _
                       "    ,GrpId " & _
                       "    ,IsGrp " & _
                       "    ,mappingtranid " & _
                       "    ,orbitfldname " & _
                       "    ,orbitfldkey " & _
                       "    ,orbitfldvalue " & _
                       "    ,allocationrefid " & _
                       "    ,arutiorbitrefid " & _
                       "    ,0 AS audittype " & _
                       "    ,CAST(NULL AS datetime) AS auditdatetime " & _
                       "    ,0 AS audituserunkid " & _
                       "    ,'' AS ip " & _
                       "    ,'' AS host " & _
                       "    ,'' AS form_name " & _
                       "    ,CAST(0 AS BIT) AS isweb " & _
                       "    ,0 AS loginemployeeunkid " & _
                       "    ,allocationunkid " & _
                       "    ,isactive " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CASE WHEN OMT.allocationrefid = 1 THEN 'BRANCH' " & _
                       "              WHEN OMT.allocationrefid = 2 THEN 'GENDER' " & _
                       "              WHEN OMT.allocationrefid = 3 THEN 'COUNTRY' " & _
                       "              WHEN OMT.allocationrefid = 4 THEN 'STATE' " & _
                       "              WHEN OMT.allocationrefid = 5 THEN 'CITY' " & _
                       "              WHEN OMT.allocationrefid = 6 THEN 'JOB' " & _
                       "              WHEN OMT.allocationrefid = 7 THEN 'NATIONALITY' " & _
                       "              WHEN OMT.allocationrefid = 24 THEN 'TITLE' " & _
                       "              WHEN OMT.allocationrefid = 12 THEN 'IDTYPE' " & _
                       "              WHEN OMT.allocationrefid = 15 THEN 'MARITALSTATUS' " & _
                       "         END + ' -> ' + OMT.orbitfldname AS GrpName " & _
                       "        ,CAST(OMT.allocationrefid AS NVARCHAR(MAX)) +'|'+ CAST(OMT.arutiorbitrefid AS NVARCHAR(MAX)) AS GrpId " & _
                       "        ,CAST(1 AS BIT) AS IsGrp " & _
                       "        ,0 AS mappingtranid " & _
                       "        ,'' AS orbitfldname " & _
                       "        ,'' AS orbitfldkey " & _
                       "        ,'' AS orbitfldvalue " & _
                       "        ,OMT.allocationrefid " & _
                       "        ,OMT.arutiorbitrefid " & _
                       "        ,0 AS allocationunkid " & _
                       "        ,CAST(1 AS BIT) AS isactive " & _
                       "    FROM hrorbitparam_mapping_tran AS OMT " & _
                       "    WHERE OMT.isactive = 1 " & _
                       "UNION ALL " & _
                       "    SELECT " & _
                       "         SPACE(5) + CASE WHEN OMT.allocationrefid = 1 THEN ST.name " & _
                       "                        WHEN OMT.allocationrefid = 2 THEN CASE WHEN allocationunkid = 1 THEN 'Male' WHEN allocationunkid = 2 THEN 'Female' END " & _
                       "                        WHEN OMT.allocationrefid = 3 THEN CM.country_name " & _
                       "                        WHEN OMT.allocationrefid = 4 THEN ST.name " & _
                       "                        WHEN OMT.allocationrefid = 5 THEN CT.name " & _
                       "                        WHEN OMT.allocationrefid = 6 THEN JB.job_name " & _
                       "                        WHEN OMT.allocationrefid = 7 THEN NT.country_name " & _
                       "                        WHEN OMT.allocationrefid = 24 THEN TL.name " & _
                       "                        WHEN OMT.allocationrefid = 12 THEN ID.name " & _
                       "                        WHEN OMT.allocationrefid = 15 THEN MS.name " & _
                       "         END + ' -> ' + OMT.orbitfldvalue AS GrpName " & _
                       "        ,CAST(OMT.allocationrefid AS NVARCHAR(MAX)) +'|'+ CAST(OMT.arutiorbitrefid AS NVARCHAR(MAX)) AS GrpId " & _
                       "        ,CAST(0 AS BIT) AS IsGrp " & _
                       "        ,OMT.mappingtranid " & _
                       "        ,OMT.orbitfldname " & _
                       "        ,OMT.orbitfldkey " & _
                       "        ,OMT.orbitfldvalue " & _
                       "        ,OMT.allocationrefid " & _
                       "        ,OMT.arutiorbitrefid " & _
                       "        ,OMT.allocationunkid " & _
                       "        ,OMT.isactive " & _
                       "    FROM hrorbitparam_mapping_tran AS OMT " & _
                       "        LEFT JOIN hrstation_master ST ON OMT.allocationunkid = ST.stationunkid AND OMT.allocationrefid = 1 AND ST.isactive = 1 " & _
                       "        LEFT JOIN hrmsConfiguration..cfcountry_master CM ON OMT.allocationunkid = CM.countryunkid AND OMT.allocationrefid = 3 " & _
                       "        LEFT JOIN hrmsConfiguration..cfstate_master SM ON OMT.allocationunkid = SM.stateunkid AND OMT.allocationrefid = 4 AND SM.isactive = 1 " & _
                       "        LEFT JOIN hrmsConfiguration..cfcity_master CT ON OMT.allocationunkid = CT.cityunkid AND OMT.allocationrefid = 5 AND CT.isactive = 1 " & _
                       "        LEFT JOIN hrjob_master AS JB ON OMT.allocationunkid = JB.jobunkid AND OMT.allocationrefid = 6 AND JB.isactive = 1 " & _
                       "        LEFT JOIN hrmsConfiguration..cfcountry_master NT ON OMT.allocationunkid = NT.countryunkid AND OMT.allocationrefid = 7 " & _
                       "        LEFT JOIN cfcommon_master TL ON OMT.allocationunkid = TL.masterunkid AND OMT.allocationrefid = TL.mastertype AND TL.isactive = 1 AND OMT.allocationrefid = 24 " & _
                       "        LEFT JOIN cfcommon_master ID ON OMT.allocationunkid = ID.masterunkid AND OMT.allocationrefid = ID.mastertype AND ID.isactive = 1 AND OMT.allocationrefid = 12 " & _
                       "        LEFT JOIN cfcommon_master MS ON OMT.allocationunkid = MS.masterunkid AND OMT.allocationrefid = MS.mastertype AND MS.isactive = 1 AND OMT.allocationrefid = 15 " & _
                       "    WHERE OMT.isactive = 1 " & _
                       ") AS A WHERE 1 = 1 "
                If strFilterString.Trim.Length > 0 Then
                    StrQ &= " AND LIKE GrpName '%" & strFilterString & "%'"
                End If
                StrQ &= "ORDER BY A.GrpId, A.IsGrp DESC "

                dsList = objDo.ExecQuery(StrQ, strList)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

    Private Function IsExists(ByVal dr As DataRow, ByVal objDo As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = -1
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = False
        Try
            StrQ = "SELECT 1 " & _
                   "FROM hrorbitparam_mapping_tran " & _
                   "WHERE isactive = 1 " & _
                   "    AND allocationrefid = @allocationrefid " & _
                   "    AND allocationunkid = @allocationunkid " & _
                   "    AND arutiorbitrefid = @arutiorbitrefid " & _
                   "    AND orbitfldname = @orbitfldname " & _
                   "    AND orbitfldkey = @orbitfldkey " & _
                   "    AND orbitfldvalue = @orbitfldvalue "

            objDo.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("allocationrefid"))
            objDo.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("allocationunkid"))
            objDo.AddParameter("@arutiorbitrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr.Item("arutiorbitrefid"))
            objDo.AddParameter("@orbitfldname", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("orbitfldname"))
            objDo.AddParameter("@orbitfldkey", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("orbitfldkey"))
            objDo.AddParameter("@orbitfldvalue", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr.Item("orbitfldvalue"))

            iCnt = objDo.RecordCount(StrQ)

            If objDo.ErrorMessage <> "" Then
                exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then blnFlag = True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function

    Public Function GetMappedAllocationIds(ByVal eType As enArutiParameter) As String
        Dim strFilter As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT allocationunkid FROM hrorbitparam_mapping_tran WHERE isactive = 1 AND allocationrefid = @allocationrefid "

                objDo.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, eType)

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    strFilter = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("allocationunkid").ToString()).ToArray())
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMappedAllocationIds; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strFilter
    End Function

#End Region

End Class

'Public Function FetchDictornaryValues(ByVal eType As enOrbitParameter, ByVal strURL As String, ByVal strUname As String, ByVal strPwd As String, ByVal strUserAgent As String) As DataTable
'    Dim dtTable As DataTable = Nothing
'    Try
'        If eType <> enOrbitParameter.NONE Then
'            Dim dc As New clsdataDictionaries
'            Dim random As New Random
'            dc.referenceNo = random.Next(10000, 99999).ToString()
'            dc.credentials.username = strUname
'            dc.credentials.password = strPwd
'            dc.credentials.agentUsername = strUserAgent
'            dc.dataDictionaries = eType.ToString()

'            Dim js As System.Runtime.Serialization.Json.DataContractJsonSerializer = Nothing
'            Dim request As HttpWebRequest = CType(WebRequest.Create(strURL), HttpWebRequest)
'            request.Method = "POST"
'            request.ContentType = "application/json; charset=utf-8"
'            request.Accept = "application/json"
'            Dim msObj As New MemoryStream()
'            js = New System.Runtime.Serialization.Json.DataContractJsonSerializer(dc.GetType())
'            js.WriteObject(msObj, dc)
'            msObj.Position = 0
'            Dim requestStream As Stream = request.GetRequestStream()
'            requestStream.Write(msObj.ToArray(), 0, msObj.Length)
'            requestStream.Close()
'            msObj.Close()
'            request.ContentType = "application/json; charset=utf-8"
'            request.Method = "GET"
'            Using responseReader As New StreamReader(request.GetResponse().GetResponseStream())
'                Dim result As String = responseReader.ReadToEnd()
'                dtTable = JsonStringToDataTable(result)
'            End Using
'        End If
'    Catch ex As WebException
'        Dim iR As String
'        iR = "{""offlineMode"": ""false"",""offlineable"": ""false"",""successFlag"": ""false"",""workflowRequired"": ""false"",""dictionary"": [{""fieldName"": ""BRANCH"",""key"": ""-99"",""value"": ""HEAD OFFICE""},{""fieldName"": ""BRANCH"",""key"": ""-83"",""value"": ""BOMA""},{""fieldName"": ""BRANCH"",""key"": ""-82"",""value"": ""VICTOIRE""},{""fieldName"": ""BRANCH"",""key"": ""-81"",""value"": ""NGABA""},{""fieldName"": ""BRANCH"",""key"": ""-80"",""value"": ""KATUBA""},{""fieldName"": ""BRANCH"",""key"": ""-78"",""value"": ""LIKASI""},{""fieldName"": ""BRANCH"",""key"": ""-77"",""value"": ""KINTAMBO""},{""fieldName"": ""BRANCH"",""key"": ""-76"",""value"": ""N'DJILI""},{""fieldName"": ""BRANCH"",""key"": ""-75"",""value"": ""BUKAVU 1""},{""fieldName"": ""BRANCH"",""key"": ""-74"",""value"": ""KOLWEZI""},{""fieldName"": ""BRANCH"",""key"": ""-73"",""value"": ""BEL AIR""},{""fieldName"": ""BRANCH"",""key"": ""-72"",""value"": ""LIMETE""},{""fieldName"": ""BRANCH"",""key"": ""-71"",""value"": ""MARCHE LIBERTY""},{""fieldName"": ""BRANCH"",""key"": ""-70"",""value"": ""BUKAVU 2""},{""fieldName"": ""BRANCH"",""key"": ""-68"",""value"": ""MATETE""},{""fieldName"": ""BRANCH"",""key"": ""-67"",""value"": ""GOMA""},{""fieldName"": ""BRANCH"",""key"": ""-66"",""value"": ""KISANGANI""},{""fieldName"": ""BRANCH"",""key"": ""-88"",""value"": ""GOMBE""},{""fieldName"": ""BRANCH"",""key"": ""-87"",""value"": ""MASINA""},{""fieldName"": ""BRANCH"",""key"": ""-86"",""value"": ""MATADI""},{""fieldName"": ""BRANCH"",""key"": ""-85"",""value"": ""LUBUMBASHI""},{""fieldName"": ""BRANCH"",""key"": ""-84"",""value"": ""UPN""},{""fieldName"": ""BRANCH"",""key"": ""-55"",""value"": ""MBUJI MAYI""},{""fieldName"": ""BRANCH"",""key"": ""-44"",""value"": ""XXX OLD HEADOFFICE XXXX""},{""fieldName"": ""BRANCH"",""key"": ""-33"",""value"": ""KANANGA""},{""fieldName"": ""BRANCH"",""key"": ""-23"",""value"": ""MOBILE""}]}"
'        dtTable = JsonStringToDataTable(iR)
'        Dim myPerson As New DictionariesValue()
'        Dim ms As New MemoryStream(Encoding.Unicode.GetBytes(iR))
'        Dim serializer As New System.Runtime.Serialization.Json.DataContractJsonSerializer(myPerson.GetType())
'        myPerson = TryCast(serializer.ReadObject(ms), DictionariesValue)
'        ms.Close()

'        'Throw New Exception(ex.Message & "; Procedure Name: FetchDictornaryValues; Module Name: " & mstrModuleName)
'    Catch ex As Exception
'        Throw New Exception(ex.Message & "; Procedure Name: FetchDictornaryValues; Module Name: " & mstrModuleName)
'    Finally
'    End Try
'    Return dtTable
'End Function