﻿'************************************************************************************************************************************
'Class Name : clsemail_queue_Tran.vb
'Purpose    :
'Date       :10/04/2013
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsemail_queue_Tran
    Private Const mstrModuleName = "clsemail_queue_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintNotificatintranunkid As Integer = -1
    Private mintRecipientunkid As Integer = -1
    Private mintEmployeeunkid As Integer = -1
    Private mintDatetypeid As Integer = -1
    Private mintCompanyunkid As Integer = -1
    Private mdtLog_Datetime As Date = Nothing
    Private mdtSend_Datetime As Date = Nothing
    Private mblnStatus_Flag As Boolean = False
    Private mblnIs_User As Boolean = False
    Private mblnIsboth As Boolean = False
    Private mblnIssent As Boolean = False
    Private mstrFail_Reason As String = String.Empty
    Private mintLoginUserunkId As Integer = -1
    Private mintLoginEmployeeunkId As Integer = -1
    Private mstrReceiptunkid As String = ""
    'Sohail (20 Nov 2018) -- Start
    'NMB Enhancement - SMS Integration to send notification 75.1.
    Private mblnIsSendSMS As Boolean = False
    Private mstrSMSFail_Reason As String = String.Empty
    'Sohail (20 Nov 2018) -- End

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set notificatintranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Notificatintranunkid() As Integer
        Get
            Return mintNotificatintranunkid
        End Get
        Set(ByVal value As Integer)
            mintNotificatintranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set recipientunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Recipientunkid() As Integer
        Get
            Return mintRecipientunkid
        End Get
        Set(ByVal value As Integer)
            mintRecipientunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set datetypeid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Datetypeid() As Integer
        Get
            Return mintDatetypeid
        End Get
        Set(ByVal value As Integer)
            mintDatetypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set log_datetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Log_Datetime() As Date
        Get
            Return mdtLog_Datetime
        End Get
        Set(ByVal value As Date)
            mdtLog_Datetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set send_datetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Send_Datetime() As Date
        Get
            Return mdtSend_Datetime
        End Get
        Set(ByVal value As Date)
            mdtSend_Datetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_flag
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Status_Flag() As Boolean
        Get
            Return mblnStatus_Flag
        End Get
        Set(ByVal value As Boolean)
            mblnStatus_Flag = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set is_user
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Is_User() As Boolean
        Get
            Return mblnIs_User
        End Get
        Set(ByVal value As Boolean)
            mblnIs_User = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isboth
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isboth() As Boolean
        Get
            Return mblnIsboth
        End Get
        Set(ByVal value As Boolean)
            mblnIsboth = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issent
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Issent() As Boolean
        Get
            Return mblnIssent
        End Get
        Set(ByVal value As Boolean)
            mblnIssent = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fail_reason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fail_Reason() As String
        Get
            Return mstrFail_Reason
        End Get
        Set(ByVal value As String)
            mstrFail_Reason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginuserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _loginuserunkid() As Integer
        Get
            Return mintLoginUserunkId
        End Get
        Set(ByVal value As Integer)
            mintLoginUserunkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _loginemployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkId
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set strRecipientunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _strRecipientunkid() As String
        Get
            Return mstrReceiptunkid
        End Get
        Set(ByVal value As String)
            mstrReceiptunkid = value
        End Set
    End Property

    'Sohail (20 Nov 2018) -- Start
    'NMB Enhancement - SMS Integration to send notification 75.1.
    ''' <summary>
    ''' Purpose: Get or Set issendsms
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _IsSendSMS() As Boolean
        Get
            Return mblnIsSendSMS
        End Get
        Set(ByVal value As Boolean)
            mblnIsSendSMS = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set smsfail_reason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _SMSFail_Reason() As String
        Get
            Return mstrSMSFail_Reason
        End Get
        Set(ByVal value As String)
            mstrSMSFail_Reason = value
        End Set
    End Property
    'Sohail (20 Nov 2018) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  notificatintranunkid " & _
              ", recipientunkid " & _
              ", employeeunkid " & _
              ", datetypeid " & _
              ", companyunkid " & _
              ", log_datetime " & _
              ", send_datetime " & _
              ", status_flag " & _
              ", is_user " & _
              ", isboth " & _
              ", issent " & _
              ", fail_reason " & _
              ", loginuserunkid " & _
              ", loginemployeeunkid " & _
              ", ISNULL(issendsms, 0) AS issendsms " & _
              ", ISNULL(smsfail_reason, '') AS smsfail_reason " & _
             "FROM cfemail_queue_tran " & _
             "WHERE notificatintranunkid = @notificatintranunkid "
            'Sohail (20 Nov 2018) - [issendsms, smsfail_reason]

            objDataOperation.AddParameter("@notificatintranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintNotificatinTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintnotificatintranunkid = CInt(dtRow.Item("notificatintranunkid"))
                mintrecipientunkid = CInt(dtRow.Item("recipientunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintDatetypeid = CInt(dtRow.Item("datetypeid"))
                mintcompanyunkid = CInt(dtRow.Item("companyunkid"))
                mdtlog_datetime = dtRow.Item("log_datetime")
                mdtsend_datetime = dtRow.Item("send_datetime")
                mblnstatus_flag = CBool(dtRow.Item("status_flag"))
                mblnis_user = CBool(dtRow.Item("is_user"))
                mblnisboth = CBool(dtRow.Item("isboth"))
                mblnissent = CBool(dtRow.Item("issent"))
                mstrFail_Reason = dtRow.Item("fail_reason").ToString
                mintLoginUserunkId = CInt(dtRow.Item("loginuserunkid"))
                mintLoginEmployeeunkId = CInt(dtRow.Item("loginemployeeunkid"))
                'Sohail (20 Nov 2018) -- Start
                'NMB Enhancement - SMS Integration to send notification 75.1.
                mblnIsSendSMS = CBool(dtRow.Item("issendsms"))
                mstrSMSFail_Reason = dtRow.Item("smsfail_reason").ToString
                'Sohail (20 Nov 2018) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  notificatintranunkid " & _
              ", recipientunkid " & _
              ", employeeunkid " & _
              ", datetypeid " & _
              ", companyunkid " & _
              ", log_datetime " & _
              ", send_datetime " & _
              ", status_flag " & _
              ", is_user " & _
              ", isboth " & _
              ", issent " & _
              ", fail_reason " & _
              ", ISNULL(issendsms, 0) AS issendsms " & _
              ", ISNULL(smsfail_reason, '') AS smsfail_reason " & _
             "FROM cfemail_queue_tran "
            'Sohail (20 Nov 2018) - [issendsms, smsfail_reason]

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfemail_queue_tran) </purpose>
    Public Function InsertEmailQueue(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            'Sandeep [ 24 JULY 2013 ] -- START
            'ENHANCEMENT : ISSUE DUPLICATED IN TRA FOR AUTO NOTIFICATION (ARTUI REFLEX) 
            If mstrReceiptunkid.Trim.Length <= 0 Then Return True
            'Sandeep [ 24 JULY 2013 ] -- END

            Dim arUserId() As String = mstrReceiptunkid.Trim.Split(",")

            If arUserId.Length <= 0 Then Return True


            For i As Integer = 0 To arUserId.Length - 1

                If isExist(CInt(arUserId(i)), mintEmployeeunkid, mintDatetypeid, mintCompanyunkid) Then
                    If UpdateEmailQueue(objDataOperation, CInt(arUserId(i))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    Else
                        Continue For
                    End If
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@recipientunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arUserId(i).ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@datetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatetypeid.ToString)
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
                objDataOperation.AddParameter("@log_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLog_Datetime.ToString)
                objDataOperation.AddParameter("@send_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSend_Datetime.ToString)
                objDataOperation.AddParameter("@status_flag", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnStatus_Flag.ToString)
                objDataOperation.AddParameter("@is_user", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_User.ToString)
                objDataOperation.AddParameter("@isboth", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsboth.ToString)
                objDataOperation.AddParameter("@issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssent.ToString)
                objDataOperation.AddParameter("@fail_reason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrFail_Reason.ToString)
                objDataOperation.AddParameter("@loginuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginUserunkId.ToString)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkId.ToString)
                'Sohail (20 Nov 2018) -- Start
                'NMB Enhancement - SMS Integration to send notification 75.1.
                objDataOperation.AddParameter("@issendsms", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSendSMS.ToString)
                objDataOperation.AddParameter("@smsfail_reason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrSMSFail_Reason.ToString)
                'Sohail (20 Nov 2018) -- End

                strQ = "INSERT INTO cfemail_queue_tran ( " & _
                          "  recipientunkid " & _
                          ",  employeeunkid " & _
                          ", datetypeid " & _
                          ", companyunkid " & _
                          ", log_datetime " & _
                          ", send_datetime " & _
                          ", status_flag " & _
                          ", is_user " & _
                          ", isboth " & _
                          ", issent " & _
                          ", fail_reason" & _
                          ", loginuserunkid " & _
                          ", loginemployeeunkid  " & _
                          ", issendsms " & _
                          ", smsfail_reason" & _
                        ") VALUES (" & _
                          "  @recipientunkid " & _
                          ", @employeeunkid " & _
                          ", @datetypeid " & _
                          ", @companyunkid " & _
                          ", @log_datetime " & _
                          ", @send_datetime " & _
                          ", @status_flag " & _
                          ", @is_user " & _
                          ", @isboth " & _
                          ", @issent " & _
                          ", @fail_reason" & _
                          ", @loginuserunkid " & _
                          ", @loginemployeeunkid  " & _
                          ", @issendsms " & _
                          ", @smsfail_reason" & _
                        "); SELECT @@identity"
                'Sohail (20 Nov 2018) - [issendsms, smsfail_reason]

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintNotificatintranunkid = dsList.Tables(0).Rows(0).Item(0)

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertEmailQueue; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfemail_queue_tran) </purpose>
    Public Function UpdateEmailQueue(ByVal objDataOperation As clsDataOperation, ByVal intReceiptId As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@notificatintranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNotificatintranunkid.ToString)
            objDataOperation.AddParameter("@recipientunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReceiptId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@datetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatetypeid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@log_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLog_Datetime.ToString)
            objDataOperation.AddParameter("@send_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSend_Datetime.ToString)
            objDataOperation.AddParameter("@status_flag", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnStatus_Flag.ToString)
            objDataOperation.AddParameter("@is_user", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_User.ToString)
            objDataOperation.AddParameter("@isboth", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsboth.ToString)
            objDataOperation.AddParameter("@issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssent.ToString)
            objDataOperation.AddParameter("@fail_reason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrFail_Reason.ToString)
            objDataOperation.AddParameter("@loginuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginUserunkId.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkId.ToString)
            'Sohail (20 Nov 2018) -- Start
            'NMB Enhancement - SMS Integration to send notification 75.1.
            objDataOperation.AddParameter("@issendsms", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSendSMS.ToString)
            objDataOperation.AddParameter("@smsfail_reason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrSMSFail_Reason.ToString)
            'Sohail (20 Nov 2018) -- End

            strQ = "UPDATE cfemail_queue_tran SET " & _
              "  recipientunkid = @recipientunkid" & _
              ",  employeeunkid = @employeeunkid" & _
              ", datetypeid = @datetypeid" & _
              ", companyunkid = @companyunkid" & _
              ", log_datetime = @log_datetime" & _
              ", send_datetime = @send_datetime" & _
              ", status_flag = @status_flag" & _
              ", is_user = @is_user" & _
              ", isboth = @isboth" & _
              ", issent = @issent" & _
              ", fail_reason = @fail_reason " & _
              ", loginuserunkid  = @loginuserunkid " & _
              ", loginemployeeunkid = @loginemployeeunkid " & _
              ", issendsms = @issendsms" & _
              ", smsfail_reason = @smsfail_reason " & _
            "WHERE notificatintranunkid = @notificatintranunkid "
            'Sohail (20 Nov 2018) - [issendsms, smsfail_reason]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateEmailQueue; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfemail_queue_tran) </purpose>
    Public Function DeleteEmailQueue(ByVal objDataOperation As clsDataOperation, ByVal intRefernceunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal intDateTypeId As Integer, ByVal intCompanyId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            Dim arUserId() As String = mstrReceiptunkid.Trim.Split(",")

            If arUserId.Length <= 0 Then Return True

            For i As Integer = 0 To arUserId.Length - 1


                strQ = "DELETE FROM cfemail_queue_tran WHERE recipientunkid = @recipientunkid AND employeeunkid = @employeeunkid AND  datetypeid = @datetypeid AND companyunkid = @companyunkid"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@recipientunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arUserId(i))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                objDataOperation.AddParameter("@datetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDateTypeId)
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteEmailQueue; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intUnkid As Integer, ByVal intEmployeeID As Integer, ByVal intdateTypeId As Integer, ByVal intCompanyId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                      "  notificatintranunkid " & _
                      ", recipientunkid " & _
                      ", employeeunkid " & _
                      ", datetypeid " & _
                      ", companyunkid " & _
                      ", log_datetime " & _
                      ", send_datetime " & _
                      ", status_flag " & _
                      ", is_user " & _
                      ", isboth " & _
                      ", issent " & _
                      ", fail_reason " & _
                      ",loginuserunkid " & _
                      ",loginemployeeunkid  " & _
                     "FROM cfemail_queue_tran " & _
                     "WHERE recipientunkid = @recipientunkid " & _
                     "AND datetypeid = @datetypeid " & _
                    "AND companyunkid = @companyunkid AND issent = 0"

            If intEmployeeID > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            End If



            objDataOperation.AddParameter("@recipientunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@datetypeid", SqlDbType.Int, eZeeDataType.NAME_SIZE, intdateTypeId)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.NAME_SIZE, intCompanyId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintNotificatintranunkid = CInt(dsList.Tables(0).Rows(0)("notificatintranunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeUsers(ByVal intCompanyID As Integer, ByVal intEmployeeID As Integer) As String
        Dim mstrUserID As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrReferenceID As String = ""
        Dim strAllocationID As String = ""

        Dim objDataOperation As New clsDataOperation
        Try

            '/* START FOR GETTING COMPANY USER ACCESS MODE SETTING

            strQ = "SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = " & intCompanyID & "  AND UPPER(key_name) = 'USERACCESSMODESETTING'"
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mstrReferenceID = dsList.Tables(0).Rows(0)("key_value").ToString()
            End If
            '/*  END FOR GETTING COMPANY USER ACCESS MODE SETTING


            '/* START FOR GETTING EMPLOYEE ALLOCATION (DEPARTMENT,JOB,CLASSGROUP,CLASS)

            strQ = " SELECT " & _
                   " CONVERT(NVARCHAR(7),ISNULL(departmentunkid,0)) + ',' + CONVERT(NVARCHAR(7),ISNULL(jobunkid,0))  + ',' + CONVERT(NVARCHAR(7),ISNULL(classgroupunkid,0)) + ',' +  CONVERT(NVARCHAR(7),ISNULL(classunkid,0)) AS Allocation " & _
                    " FROM hremployee_master " & _
                    " WHERE employeeunkid =   " & intEmployeeID

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strAllocationID = dsList.Tables(0).Rows(0)("Allocation").ToString()
            End If

            '/* END FOR GETTING EMPLOYEE ALLOCATION (DEPARTMENT,JOB,CLASSGROUP,CLASS)


            Dim arAllocation() As String = mstrReferenceID.ToString.Trim.Split(",")
            Dim arAllocationID() As String = strAllocationID.ToString.Trim.Split(",")

            If arAllocation.Length > 0 AndAlso arAllocationID.Length > 0 Then


                'Pinkal (04-Dec-2014) -- Start
                'Enhancement - Bug Solved When Company had User access accept Department it was given Error.

                Dim intCount As Integer = 0

                strQ = "SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CONVERT(NVARCHAR(max), cfuser_master.userunkid)  FROM hrmsConfiguration..cfuser_master " & _
                          " Join ( "

                For i As Integer = 0 To arAllocation.Length - 1

                    For j As Integer = 0 To arAllocationID.Length - 1

                        If CInt(arAllocation(i)) = enAllocation.DEPARTMENT AndAlso CInt(arAllocationID(j)) > 0 Then

                            'strQ = "SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CONVERT(NVARCHAR(max), cfuser_master.userunkid)  FROM hrmsConfiguration..cfuser_master " & _
                            '          " Join ( " & _
                            '          "Select DISTINCT userunkid " & _
                            '          " FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
                            '          " JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                            '          " WHERE companyunkid = " & intCompanyID & "  And allocationunkid =  " & CInt(arAllocationID(i)) & " And referenceunkid = " & enAllocation.DEPARTMENT

                            If intCount > 0 Then
                                strQ &= " UNION "
                            End If

                            strQ &= "Select DISTINCT userunkid " & _
                                      " FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
                                      " JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                      " WHERE companyunkid = " & intCompanyID & "  And allocationunkid =  " & CInt(arAllocationID(i)) & " And referenceunkid = " & enAllocation.DEPARTMENT
                            intCount += 1
                        End If

                        If CInt(arAllocation(i)) = enAllocation.JOBS AndAlso CInt(arAllocationID(j)) > 0 Then

                            If intCount > 0 Then
                                strQ &= " UNION "
                            End If

                            strQ &= " Select DISTINCT userunkid " & _
                                          " FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
                                          " JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                          " WHERE companyunkid = " & intCompanyID & "  And allocationunkid = " & CInt(arAllocationID(i)) & " And referenceunkid = " & enAllocation.JOBS
                            intCount += 1
                        End If

                        If CInt(arAllocation(i)) = enAllocation.CLASS_GROUP AndAlso CInt(arAllocationID(j)) > 0 Then
                            If intCount > 0 Then
                                strQ &= " UNION "
                            End If

                            strQ &= "  Select DISTINCT userunkid " & _
                                         " FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
                                         " JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                         " WHERE companyunkid = " & intCompanyID & "  And allocationunkid = " & CInt(arAllocationID(i)) & "  And referenceunkid = " & enAllocation.CLASS_GROUP
                            intCount += 1
                        End If

                        If CInt(arAllocation(i)) = enAllocation.CLASSES AndAlso CInt(arAllocationID(j)) > 0 Then

                            If intCount > 0 Then
                                strQ &= " UNION "
                            End If

                            strQ &= "   Select DISTINCT userunkid " & _
                                        " FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
                                        " JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                        " WHERE companyunkid = " & intCompanyID & "  And allocationunkid =  " & CInt(arAllocationID(i)) & "  And referenceunkid = " & enAllocation.CLASSES
                            intCount += 1
                       End If
                    Next

                Next
              
                'Pinkal (04-Dec-2014) -- End

                'S.SANDEEP [ 02 DEC 2014 ] -- START
                'strQ &= " ) AS A ON hrmsConfiguration..cfuser_master.userunkid = A.userunkid AND cfuser_master.isactive = 1 " & _
                '        " FOR XML PATH('')),1,1,''),'') AS userunkid "

                strQ &= " ) AS A ON hrmsConfiguration..cfuser_master.userunkid = A.userunkid AND cfuser_master.isactive = 1 " & _
                        " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate " & _
                             " FOR XML PATH('')),1,1,''),'') AS userunkid "

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                'S.SANDEEP [ 02 DEC 2014 ] -- END

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    mstrUserID = dsList.Tables(0).Rows(0)("userunkid")
                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeUsers; Module Name: " & mstrModuleName)
        End Try
        Return mstrUserID
    End Function

End Class