﻿'************************************************************************************************************************************
'Class Name : clsEmployee_Skill_Tran.vb
'Purpose    :
'Date       :15/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsEmployee_Skill_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Skill_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSkillstranunkid As Integer
    Private mintEmp_App_Unkid As Integer
    Private mintSkillcategoryunkid As Integer
    Private mintSkillunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsapplicant As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Private mintLoginemployeeunkid As Integer = -1
    Private mintVoidloginemployeeunkid As Integer = -1
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'S.SANDEEP [ 13 AUG 2012 ] -- END


'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
    Private mstrOther_SkillCategory As String = ""
    Private mstrOther_Skill As String = ""
    Private mintSkillExpertiseunkid As Integer = 0
    'Pinkal (24-Sep-2020) -- End


#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillstranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Skillstranunkid() As Integer
        Get
            Return mintSkillstranunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillstranunkid = value
            Call GetData(objDataOperation) 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_app_unkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emp_App_Unkid() As Integer
        Get
            Return mintEmp_App_Unkid
        End Get
        Set(ByVal value As Integer)
            mintEmp_App_Unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillcategoryunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Skillcategoryunkid() As Integer
        Get
            Return mintSkillcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillcategoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Skillunkid() As Integer
        Get
            Return mintSkillunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isapplicant
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isapplicant() As Boolean
        Get
            Return mblnIsapplicant
        End Get
        Set(ByVal value As Boolean)
            mblnIsapplicant = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
    ''' <summary>
    ''' Purpose: Get or Set Other_SkillCategory
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Other_SkillCategory() As String
        Get
            Return mstrOther_SkillCategory
        End Get
        Set(ByVal value As String)
            mstrOther_SkillCategory = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Ohter_Skill
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Other_Skill() As String
        Get
            Return mstrOther_Skill
        End Get
        Set(ByVal value As String)
            mstrOther_Skill = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SkillExpertiseunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _SkillExpertiseunkid() As Integer
        Get
            Return mintSkillExpertiseunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillExpertiseunkid = value
        End Set
    End Property

    'Pinkal (24-Sep-2020) -- End

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOperation As clsDataOperation = Nothing) 'S.SANDEEP [ 27 APRIL 2012 objDataOperation ] -- START -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Sandeep [ 16 Oct 2010 ] -- Start
        'objDataOperation = New clsDataOperation


        'S.SANDEEP [ 27 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objDataOperation As New clsDataOperation
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        'S.SANDEEP [ 27 APRIL 2012 ] -- END



        'Sandeep [ 16 Oct 2010 ] -- End 

        Try

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'strQ = "SELECT " & _
            '  "  skillstranunkid " & _
            '  ", emp_app_unkid " & _
            '  ", skillcategoryunkid " & _
            '  ", skillunkid " & _
            '  ", description " & _
            '  ", isapplicant " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            ' "FROM hremp_app_skills_tran " & _
            ' "WHERE skillstranunkid = @skillstranunkid "
            strQ = "SELECT " & _
              "  skillstranunkid " & _
              ", emp_app_unkid " & _
              ", skillcategoryunkid " & _
              ", skillunkid " & _
              ", description " & _
              ", isapplicant " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                          ", ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
                          ", ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
              ",ISNULL(other_skillcategory,'') AS other_skillcategory " & _
              ",ISNULL(other_skill,'') AS other_skill " & _
              ",ISNULL(skillexpertiseunkid,0) AS skillexpertiseunkid " & _
              " FROM hremp_app_skills_tran " & _
              " WHERE skillstranunkid = @skillstranunkid "

            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[ISNULL(other_skillcategory,'') AS other_skillcategory,ISNULL(other_skill,'') AS other_skill,ISNULL(skillexpertiseunkid,0) AS skillexpertiseunkid "]

            'S.SANDEEP [ 12 OCT 2011 ] -- END 



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.ClearParameters()
            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillstranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintSkillstranunkid = CInt(dtRow.Item("skillstranunkid"))
                mintEmp_App_Unkid = CInt(dtRow.Item("emp_app_unkid"))
                mintSkillcategoryunkid = CInt(dtRow.Item("skillcategoryunkid"))
                mintSkillunkid = CInt(dtRow.Item("skillunkid"))
                mstrDescription = dtRow.Item("description").ToString
                mblnIsapplicant = CBool(dtRow.Item("isapplicant"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                'S.SANDEEP [ 12 OCT 2011 ] -- END 


                'Pinkal (24-Sep-2020) -- Start
                'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                mstrOther_SkillCategory = dtRow.Item("other_skillcategory").ToString
                mstrOther_Skill = dtRow.Item("other_skill").ToString
                mintSkillExpertiseunkid = CInt(dtRow.Item("skillexpertiseunkid").ToString)
                'Pinkal (24-Sep-2020) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet

        'S.SANDEEP [20-JUN-2018] -- 'Enhancement - Implementing Employee Approver Flow For NMB .[Optional ByVal mblnAddApprovalCondition As Boolean = True]
        'Pinkal (28-Dec-2015) -- Start    'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            'If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition)
            'S.SANDEEP [20-JUN-2018] -- End


            'Pinkal (28-Dec-2015) -- End
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
                       "    ,ISNULL(cfcommon_master.name,'') AS Category " & _
                       "    ,ISNULL(hrskill_master.skillname,'') AS SkillName " & _
                       "    ,hremp_app_skills_tran.description AS Description " & _
                       "    ,hremp_app_skills_tran.skillstranunkid As SkillTranId " & _
                       "    ,cfcommon_master.masterunkid As CatId " & _
                       "    ,hrskill_master.skillunkid As SkillId " & _
                       "    ,hremployee_master.employeeunkid As EmpId " & _
                       "    ,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
                       "    ,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
                       "    ,0 AS operationtypeid " & _
                       "    ,'' AS OperationType " & _
                       "    ,'' AS pCategory " & _
                       "    ,'' AS pSkill " & _
                       ", ISNULL(other_skillcategory, '') AS other_skillcategory " & _
                       ", ISNULL(other_skill, '') AS other_skill " & _
                       "    ,ISNULL(skillexpertiseunkid,0) AS skillexpertiseunkid " & _
                       "    , '' AS AUD " & _
                       "    , '' AS GUID " & _
                       "FROM hremp_app_skills_tran " & _
                       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid AND cfcommon_master.mastertype = " & enCommonMaster.SKILL_CATEGORY & _
                       "    LEFT JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
                       "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid "

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                'Sohail (25 Sep 2020) - [AUD, GUID]
                'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[ ,ISNULL(other_skillcategory,'') AS other_skillcategory, ,ISNULL(other_skill,'') AS other_skill, ,ISNULL(skillexpertiseunkid,0) AS skillexpertiseunkid  ]

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                ' --- ADDED  : "operationtypeid,OperationType,pCategory,pSkill "
                'Gajanan [17-DEC-2018] -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

               

                StrQ &= "WHERE ISNULL(hremp_app_skills_tran.isvoid,0) = 0 AND ISNULL(hremp_app_skills_tran.isapplicant,0) = 0 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If



                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Add Order by.
                StrQ &= " ORDER by NAME "
                'Gajanan [17-DEC-2018] -- End



                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal blnOnlyApplicant As Boolean = True, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
    '    'S.SANDEEP [ 27 APRIL 2012 blnIncludeInactiveEmployee,strEmployeeAsOnDate,strUserAccessLevelFilterString] -- START -- END

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        'S.SANDEEP [ 12 OCT 2011 ] -- START
    '        'strQ = "SELECT " & _
    '        '                     "CASE WHEN ISNULL(hremp_app_skills_tran.isapplicant,0) = 0 THEN " & _
    '        '                          "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
    '        '                     "ELSE '' END AS NAME " & _
    '        '                     ",ISNULL(cfcommon_master.name,'') AS Category " & _
    '        '                     ",ISNULL(hrskill_master.skillname,'') AS SkillName " & _
    '        '                     ",hremp_app_skills_tran.description AS Description " & _
    '        '                     ",hremp_app_skills_tran.skillstranunkid As SkillTranId " & _
    '        '                     ",cfcommon_master.masterunkid As CatId " & _
    '        '                     ",hrskill_master.skillunkid As SkillId " & _
    '        '                     ",hremployee_master.employeeunkid As EmpId " & _
    '        '                "FROM hremp_app_skills_tran " & _
    '        '                    "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid AND cfcommon_master.mastertype = " & enCommonMaster.SKILL_CATEGORY & _
    '        '                    "LEFT JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
    '        '                    "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
    '        '                "WHERE ISNULL(hremp_app_skills_tran.isvoid,0) = 0 "

    '        strQ = "SELECT " & _
    '                 "CASE WHEN ISNULL(hremp_app_skills_tran.isapplicant,0) = 0 THEN " & _
    '                      "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
    '                 "ELSE '' END AS NAME " & _
    '                 ",ISNULL(cfcommon_master.name,'') AS Category " & _
    '                 ",ISNULL(hrskill_master.skillname,'') AS SkillName " & _
    '                 ",hremp_app_skills_tran.description AS Description " & _
    '                 ",hremp_app_skills_tran.skillstranunkid As SkillTranId " & _
    '                 ",cfcommon_master.masterunkid As CatId " & _
    '                 ",hrskill_master.skillunkid As SkillId " & _
    '                 ",hremployee_master.employeeunkid As EmpId "

    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        strQ &= ",ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid " & _
    '                    "	,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
    '                 "	,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid "
    '        'Anjan (21 Nov 2011)-End 


    '        strQ &= "FROM hremp_app_skills_tran " & _
    '                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid AND cfcommon_master.mastertype = " & enCommonMaster.SKILL_CATEGORY & _
    '                "LEFT JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
    '                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
    '            "WHERE ISNULL(hremp_app_skills_tran.isvoid,0) = 0 "
    '        'S.SANDEEP [ 12 OCT 2011 ] -- END 




    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        ''ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- END 

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            'Sohail (06 Jan 2012) -- End
    '        End If

    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END






    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If



    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        '        End If
    '        'End Select

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END



    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Anjan (24 Jun 2011)-End 



    '        If blnOnlyApplicant Then
    '            strQ &= " AND ISNULL(hremp_app_skills_tran.isapplicant,0) = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    'S.SANDEEP [04 JUN 2015] -- END

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremp_app_skills_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Start {xDataOpr} -- End
        If isExist(mintEmp_App_Unkid, mintSkillunkid, mblnIsapplicant) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Selected skill is already assigned to the employee. Please assign new skill.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
            objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End
        


        Try
            objDataOperation.AddParameter("@emp_app_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmp_App_Unkid.ToString)
            objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillcategoryunkid.ToString)
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)




            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            objDataOperation.AddParameter("@other_skillcategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_SkillCategory.ToString)
            objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_Skill.ToString)
            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillExpertiseunkid.ToString)
            'Pinkal (24-Sep-2020) -- End


            'strQ = "INSERT INTO hremp_app_skills_tran ( " & _
            '          "  emp_app_unkid " & _
            '          ", skillcategoryunkid " & _
            '          ", skillunkid " & _
            '          ", description " & _
            '          ", isapplicant " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime" & _
            '          ", voidreason" & _
            '        ") VALUES (" & _
            '          "  @emp_app_unkid " & _
            '          ", @skillcategoryunkid " & _
            '          ", @skillunkid " & _
            '          ", @description " & _
            '          ", @isapplicant " & _
            '          ", @userunkid " & _
            '          ", @isvoid " & _
            '          ", @voiduserunkid " & _
            '          ", @voiddatetime" & _
            '          ", @voidreason" & _
            '        "); SELECT @@identity"

            strQ = "INSERT INTO hremp_app_skills_tran ( " & _
                      "  emp_app_unkid " & _
                      ", skillcategoryunkid " & _
                      ", skillunkid " & _
                      ", description " & _
                      ", isapplicant " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime" & _
                      ", voidreason" & _
                            ", loginemployeeunkid " & _
                            ", voidloginemployeeunkid" & _
                      ", other_skillcategory " & _
                      ", other_skill " & _
                      ", skillexpertiseunkid " & _
                    ") VALUES (" & _
                      "  @emp_app_unkid " & _
                      ", @skillcategoryunkid " & _
                      ", @skillunkid " & _
                      ", @description " & _
                      ", @isapplicant " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime" & _
                      ", @voidreason" & _
                            ", @loginemployeeunkid " & _
                            ", @voidloginemployeeunkid" & _
                      ", @other_skillcategory " & _
                      ", @other_skill " & _
                      ", @skillexpertiseunkid " & _
                    "); SELECT @@identity"
            'S.SANDEEP [ 12 OCT 2011 ] -- END 



            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[@other_skillcategory,@other_skill,@skillexpertiseunkid ]

            'Pinkal (24-Sep-2020) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSkillstranunkid = dsList.Tables(0).Rows(0).Item(0)


            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            'InsertAuditTrailForEmpSkillTran(objDataOperation, 1)
            If InsertAuditTrailForEmpSkillTran(objDataOperation, 1) = False Then
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'objDataOperation.ReleaseTransaction(False)
                If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
                End If
                'Gajanan [17-DEC-2018] -- End
                Return False
            End If
            'Anjan (11 Jun 2011)-End 

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [17-DEC-2018] -- End

            Return True
        Catch ex As Exception
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [22-Feb-2019] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremp_app_skills_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Add Objdata Opration
        If isExist(mintEmp_App_Unkid, mintSkillunkid, mblnIsapplicant, mintSkillstranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Selected skill is already assigned to the employee. Please assign new skill.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.

        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        'Gajanan [17-DEC-2018] -- End

        Try
            objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillstranunkid.ToString)
            objDataOperation.AddParameter("@emp_app_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmp_App_Unkid.ToString)
            objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillcategoryunkid.ToString)
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)


            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            objDataOperation.AddParameter("@other_skillcategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_SkillCategory.ToString)
            objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_Skill.ToString)
            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillExpertiseunkid.ToString)
            'Pinkal (24-Sep-2020) -- End

            'strQ = "UPDATE hremp_app_skills_tran SET " & _
            '          "  emp_app_unkid = @emp_app_unkid" & _
            '          ", skillcategoryunkid = @skillcategoryunkid" & _
            '          ", skillunkid = @skillunkid" & _
            '          ", description = @description" & _
            '          ", isapplicant = @isapplicant" & _
            '          ", userunkid = @userunkid" & _
            '          ", isvoid = @isvoid" & _
            '          ", voiduserunkid = @voiduserunkid" & _
            '          ", voiddatetime = @voiddatetime " & _
            '        "WHERE skillstranunkid = @skillstranunkid "

            strQ = "UPDATE hremp_app_skills_tran SET " & _
                      "  emp_app_unkid = @emp_app_unkid" & _
                      ", skillcategoryunkid = @skillcategoryunkid" & _
                      ", skillunkid = @skillunkid" & _
                      ", description = @description" & _
                      ", isapplicant = @isapplicant" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason" & _
                         ", loginemployeeunkid = @loginemployeeunkid" & _
                         ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      ", other_skillcategory = @other_skillcategory " & _
                      ", other_skill = @other_skill " & _
                      ", skillexpertiseunkid = @skillexpertiseunkid " & _
                      " WHERE skillstranunkid = @skillstranunkid "

            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[ ", other_skillcategory = @other_skillcategory ", other_skill = @other_skill ", skillexpertiseunkid = @skillexpertiseunkid " & _]

            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            'InsertAuditTrailForEmpSkillTran(objDataOperation, 2)
            If InsertAuditTrailForEmpSkillTran(objDataOperation, 2) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End 

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [17-DEC-2018] -- End


            Return True
        Catch ex As Exception
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'Gajanan [17-DEC-2018] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (hremp_app_skills_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer, _
    '                       ByVal blnVoid As Boolean, _
    '                       ByVal intVoidUser As Integer, _
    '                       ByVal dtVoidDate As DateTime, _
    '                       ByVal strReason As String) As Boolean

    '    'If isUsed(intUnkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try

    '        'S.SANDEEP [ 12 OCT 2011 ] -- START
    '        'strQ = "UPDATE hremp_app_skills_tran SET " & _
    '        '               "  isvoid = @isvoid " & _
    '        '               ", voiduserunkid = @voiduserunkid " & _
    '        '               ", voiddatetime = @voiddatetime " & _
    '        '             "WHERE skillstranunkid = @skillstranunkid "

    '        If mintVoidloginemployeeunkid <= -1 Then
    '            strQ = "UPDATE hremp_app_skills_tran SET " & _
    '                      "  isvoid = @isvoid " & _
    '                      ", voiduserunkid = @voiduserunkid " & _
    '                      ", voiddatetime = @voiddatetime " & _
    '                            ", voidreason = @voidreason " & _
    '                    "WHERE skillstranunkid = @skillstranunkid "
    '        Else
    '            strQ = "UPDATE hremp_app_skills_tran SET " & _
    '                        "  isvoid = @isvoid " & _
    '                        ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
    '                        ", voiddatetime = @voiddatetime " & _
    '                        ", voidreason = @voidreason " & _
    '                      "WHERE skillstranunkid = @skillstranunkid "
    '        End If
    '        'S.SANDEEP [ 12 OCT 2011 ] -- END 




    '        objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnVoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUser.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReason.ToString)

    '        If mintVoidloginemployeeunkid > 0 Then
    '            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
    '        End If

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        'Sandeep [ 16 Oct 2010 ] -- Start
    '        'mintSkillstranunkid = intUnkid
    '        _Skillstranunkid = intUnkid
    '        'Sandeep [ 16 Oct 2010 ] -- End 



    '        'Anjan (11 Jun 2011)-Start
    '        'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
    '        'InsertAuditTrailForEmpSkillTran(objDataOperation, 3)

    '        If InsertAuditTrailForEmpSkillTran(objDataOperation, 3) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Anjan (11 Jun 2011)-End 

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    Public Function Delete(ByVal intUnkid As Integer, _
                           ByVal blnVoid As Boolean, _
                           ByVal intVoidUser As Integer, _
                           ByVal dtVoidDate As DateTime, _
                           ByVal strReason As String, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        mstrMessage = ""
        Try
            If mintVoidloginemployeeunkid <= -1 Then
                strQ = "UPDATE hremp_app_skills_tran SET " & _
                          "  isvoid = @isvoid " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voiddatetime = @voiddatetime " & _
                                ", voidreason = @voidreason " & _
                        "WHERE skillstranunkid = @skillstranunkid "
            Else
                strQ = "UPDATE hremp_app_skills_tran SET " & _
                            "  isvoid = @isvoid " & _
                            ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                            ", voiddatetime = @voiddatetime " & _
                            ", voidreason = @voidreason " & _
                          "WHERE skillstranunkid = @skillstranunkid "
            End If

            objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnVoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUser.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReason.ToString)

            If mintVoidloginemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Sandeep [ 16 Oct 2010 ] -- Start
            'mintSkillstranunkid = intUnkid
            _Skillstranunkid = intUnkid
            'Sandeep [ 16 Oct 2010 ] -- End 



            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            'InsertAuditTrailForEmpSkillTran(objDataOperation, 3)

            If InsertAuditTrailForEmpSkillTran(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Anjan (11 Jun 2011)-End 

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [17-DEC-2018] -- End
   

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpAppId As Integer, ByVal intSkillId As Integer, ByVal blnIsApplicant As Boolean, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Start {xDataOpr} -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        
        'objDataOperation = New clsDataOperation
         If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End

        Try
            strQ = "SELECT " & _
                     "  skillstranunkid " & _
                     ", emp_app_unkid " & _
                     ", skillcategoryunkid " & _
                     ", skillunkid " & _
                     ", description " & _
                     ", isapplicant " & _
                     ", userunkid " & _
                     ", isvoid " & _
                     ", voiduserunkid " & _
                     ", voiddatetime " & _
                     ", voidreason " & _
                     ", ISNULL(other_skillcategory,'') AS other_skillcategory " & _
                     ", ISNULL(other_skill,'') AS other_skill " & _
                     ", ISNULL(skillexpertiseunkid,0) AS skillexpertiseunkid " & _
                    "FROM hremp_app_skills_tran "

            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[", ISNULL(other_skillcategory,'') AS other_skillcategory  ", ISNULL(other_skill,'') AS other_skill  ", ISNULL(skillexpertiseunkid,0) AS skillexpertiseunkid " &]

            If blnIsApplicant = True Then
                strQ &= "WHERE ISNULL(isapplicant,0) = 1 "
            Else
                strQ &= "WHERE ISNULL(isapplicant,0) = 0 "
            End If
            strQ &= " AND emp_app_unkid = @EmpAppId " & _
                    " AND skillunkid = @SkillId "

            If intUnkid > 0 Then
                strQ &= " AND skillstranunkid <> @skillstranunkid"
            End If

            'Sandeep [ 14 Aug 2010 ] -- Start
            strQ &= " AND ISNULL(isvoid,0) = 0 "
            'Sandeep [ 14 Aug 2010 ] -- End 

            objDataOperation.AddParameter("@EmpAppId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpAppId)
            objDataOperation.AddParameter("@SkillId", SqlDbType.Int, eZeeDataType.INT_SIZE, intSkillId)
            objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function


    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForEmpSkillTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForEmpSkillTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        'Anjan (11 Jun 2011)-End 


        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            '  strQ = "INSERT INTO athremp_app_skills_tran ( " & _
            '  "  skillstranunkid " & _
            '  ", emp_app_unkid " & _
            '  ", skillcategoryunkid " & _
            '  ", skillunkid " & _
            '  ", description " & _
            '  ", isapplicant " & _
            '  ", audittype " & _
            '  ", audituserunkid " & _
            '  ", auditdatetime " & _
            '  ", ip " & _
            '  ", machine_name " & _
            '") VALUES (" & _
            '  "  @skillstranunkid " & _
            '  ", @emp_app_unkid " & _
            '  ", @skillcategoryunkid " & _
            '  ", @skillunkid " & _
            '  ", @description " & _
            '  ", @isapplicant " & _
            '  ", @audittype " & _
            '  ", @audituserunkid " & _
            '  ", @auditdatetime " & _
            '  ", @ip " & _
            '  ", @machine_name " & _
            '"); "

            strQ = "INSERT INTO athremp_app_skills_tran ( " & _
            "  skillstranunkid " & _
            ", emp_app_unkid " & _
            ", skillcategoryunkid " & _
            ", skillunkid " & _
            ", description " & _
            ", isapplicant " & _
                      ", other_skillcategory " & _
                      ", other_skill " & _
                      ", skillexpertiseunkid " & _
            ", audittype " & _
            ", audituserunkid " & _
            ", auditdatetime " & _
            ", ip " & _
            ", machine_name " & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb " & _
                        ", loginemployeeunkid " & _
          ") VALUES (" & _
            "  @skillstranunkid " & _
            ", @emp_app_unkid " & _
            ", @skillcategoryunkid " & _
            ", @skillunkid " & _
            ", @description " & _
            ", @isapplicant " & _
                      ", @other_skillcategory " & _
                      ", @other_skill " & _
                      ", @skillexpertiseunkid " & _
            ", @audittype " & _
            ", @audituserunkid " & _
            ", @auditdatetime " & _
            ", @ip " & _
            ", @machine_name " & _
                       ", @form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", @isweb " & _
                        ", @loginemployeeunkid " & _
                  "); " 'S.SANDEEP [ 13 AUG 2012 (loginemployeeunkid) ] -- START -- END


            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[@other_skillcategory,@other_skil,@skillexpertiseunkidl ]

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillstranunkid.ToString)
            objDataOperation.AddParameter("@emp_app_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmp_App_Unkid.ToString)
            objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillcategoryunkid.ToString)
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)

'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            objDataOperation.AddParameter("@other_skillcategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_SkillCategory.ToString)
            objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_Skill.ToString)
            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillExpertiseunkid.ToString)
            'Pinkal (24-Sep-2020) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEmpSkillTran", mstrModuleName)
        End Try
    End Function


    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetEmployeeSkillData_Export() As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known
            'Replace Column Name With Getmessage
            StrQ = "SELECT " & _
                      "    ISNULL(hremployee_master.employeecode,'') AS  [" & Language.getMessage(mstrModuleName, 3, "ECODE") & "]" & _
                      "   ,ISNULL(hremployee_master.firstname,'') AS  [" & Language.getMessage(mstrModuleName, 4, "FIRSTNAME") & "]" & _
                      "   ,ISNULL(hremployee_master.surname,'') AS  [" & Language.getMessage(mstrModuleName, 5, "SURNAME") & "]" & _
                      "   ,ISNULL(cfcommon_master.name,'') AS  [" & Language.getMessage(mstrModuleName, 6, "SKILL_CATEGORY") & "]" & _
                      "   ,ISNULL(hrskill_master.skillname,'') AS  [" & Language.getMessage(mstrModuleName, 7, "SKILL_NAME") & "]" & _
                      "   ,ISNULL(hremp_app_skills_tran.description,'') AS  [" & Language.getMessage(mstrModuleName, 8, "DESCRIPTION") & "]" & _
                      "   ,ISNULL(hremp_app_skills_tran.other_skillcategory,'') AS  [" & Language.getMessage(mstrModuleName, 9, "OTHER_SKILLCATEGORY") & "]" & _
                      "   ,ISNULL(hremp_app_skills_tran.other_skill,'') AS  [" & Language.getMessage(mstrModuleName, 10, "OTHER_SKILL") & "]" & _
                      "   ,ISNULL(rcskillexpertise_master.name,'') AS  [" & Language.getMessage(mstrModuleName, 11, "SKILL_EXPERTISE") & "]" & _
                      " FROM hremp_app_skills_tran " & _
                      " JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
                      " JOIN cfcommon_master ON cfcommon_master.masterunkid  = hremp_app_skills_tran.skillcategoryunkid AND mastertype = " & enCommonMaster.SKILL_CATEGORY & " " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid AND isapplicant = 0 " & _
                      " LEFT JOIN rcskillexpertise_master ON rcskillexpertise_master.skillexpertiseunkid = hremp_app_skills_tran.skillexpertiseunkid  " & _
                      " WHERE isvoid = 0 "


            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            '",ISNULL(hremp_app_skills_tran.other_skillcategory,'') AS  [" & Language.getMessage(mstrModuleName, 9, "OTHER_SKILLCATEGORY") & "]" & _
            '",ISNULL(hremp_app_skills_tran.other_skill,'') AS  [" & Language.getMessage(mstrModuleName, 10, "OTHER_SKILL") & "]" & _
            '",ISNULL(rcskillexpertise_master.name,'') AS  [" & Language.getMessage(mstrModuleName, 11, "SKILL_EXPERTISE") & "]" & _
            '" LEFT JOIN rcskillexpertise_master ON rcskillexpertise_master.skillexpertiseunkid = hremp_app_skills_tran.skillexpertiseunkid  " & _
            'Pinkal (24-Sep-2020) -- End


            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            'ISNULL(hrskill_master.skillname,'') AS SKILL -- REMOVED
            'ISNULL(hrskill_master.skillname,'') AS SKILL_NAME -- ADDED
            'S.SANDEEP [14-JUN-2018] -- END
            'Gajanan (24 Nov 2018) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeSkillData_Export; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetSkillTranUnkid(ByVal intEmpId As Integer, ByVal strSkill As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT skillstranunkid " & _
                   "FROM hremp_app_skills_tran " & _
                   "    JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
                   "WHERE emp_app_unkid = @EmpId AND LTRIM(RTRIM(skillname)) = LTRIM(RTRIM(@Skill)) "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND hremp_app_skills_tran.isvoid = 0 AND hrskill_master.isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@Skill", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSkill)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("skillstranunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSkillTranUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Public Shared Function GetCSV_EmpIds(ByVal intSkillId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim StrString As String = String.Empty
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(s.emp_app_unkid AS NVARCHAR(50)) FROM hremp_app_skills_tran s WHERE s.isvoid = 0 AND s.isapplicant = 0 AND s.skillunkid IN (" & intSkillId & ") FOR XML PATH('')),1,1,''), '') AS CSV "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                StrString = dsList.Tables(0).Rows(0).Item("CSV")
            End If
            Return StrString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCSV_EmpIds", mstrModuleName)
            Return ""
        End Try
    End Function
    'Sohail (14 Nov 2019) -- End


    'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.

    Public Function GetRecruitmentEmpSkill(ByVal blnOtherSkill As Boolean, ByVal intEmployeeId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim objDataOperation As New clsDataOperation


            If blnOtherSkill Then

                strQ = " SELECT    " & _
                          " ISNULL(STUFF(( SELECT DISTINCT CASE WHEN other_skillcategory <> '' THEN  ',' + CAST(other_skillcategory AS VARCHAR(max)) ELSE  " & _
                          " CAST(other_skillcategory AS VARCHAR(max)) END " & _
                          " FROM hremp_app_skills_tran " & _
                          " WHERE hremp_app_skills_tran.emp_app_unkid =  @employeeunkid AND hremp_app_skills_tran.isvoid = 0 " & _
                          " FOR  XML PATH('')), 1, 1, ''),'') AS SkillCategory, " & _
                          " ISNULL(STUFF(( SELECT DISTINCT CASE WHEN other_skill <> '' THEN ',' + CAST(other_skill AS VARCHAR(max)) ELSE " & _
                          " CAST(other_skill AS VARCHAR(max)) END   " & _
                          " FROM hremp_app_skills_tran " & _
                          " WHERE hremp_app_skills_tran.emp_app_unkid = @employeeunkid  AND hremp_app_skills_tran.isvoid = 0 " & _
                          " FOR  XML PATH('')), 1, 1, ''),'')  AS Skill "
            Else

                strQ = " SELECT    " & _
                          " ISNULL(STUFF(( SELECT  ',' + CAST(cfcommon_master.name AS VARCHAR(max)) " & _
                          " FROM hremp_app_skills_tran " & _
                          " JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid AND cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & _
                          " WHERE hremp_app_skills_tran.emp_app_unkid =  @employeeunkid  AND hremp_app_skills_tran.isvoid = 0 " & _
                          " ORDER BY cfcommon_master.masterunkid" & _
                          " FOR  XML PATH('')), 1, 1, ''),'') AS SkillCategory, " & _
                          " ISNULL(STUFF(( SELECT  ',' + CAST(hrskill_master.skillname AS VARCHAR(max)) " & _
                          " FROM hremp_app_skills_tran " & _
                          " JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid  " & _
                          " WHERE hremp_app_skills_tran.emp_app_unkid = @employeeunkid  AND hremp_app_skills_tran.isvoid = 0  " & _
                          " ORDER BY hrskill_master.skillunkid " & _
                          " FOR  XML PATH('')), 1, 1, ''),'')  AS Skill "

            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRecruitmentEmpSkill; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (24-Sep-2020) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Selected skill is already assigned to the employee. Please assign new skill.")
            Language.setMessage(mstrModuleName, 2, "WEB")
			Language.setMessage(mstrModuleName, 3, "ECODE")
			Language.setMessage(mstrModuleName, 4, "FIRSTNAME")
			Language.setMessage(mstrModuleName, 5, "SURNAME")
			Language.setMessage(mstrModuleName, 6, "SKILL_CATEGORY")
			Language.setMessage(mstrModuleName, 7, "SKILL_NAME")
			Language.setMessage(mstrModuleName, 8, "DESCRIPTION")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class