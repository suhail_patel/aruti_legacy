﻿
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class clsImportApplicant

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsImportApplicant"
    Dim objDO As clsDataOperation
    Dim objApplicant As clsApplicant_master
    Dim objVacancy As clsVacancy
    Dim objApplicantQualification As clsApplicantQualification_tran
    Dim objApplicantHistory As clsJobhistory
    Dim objApplicantSkill As clsApplicantSkill_tran
    'S.SANDEEP [ 09 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim objApplicantRef As clsrcapp_reference_Tran
    'S.SANDEEP [ 09 NOV 2012 ] -- END

    Dim objEmployee As clsEmployee_Master
    Dim objEmployeeQualification As clsEmp_Qualification_Tran
    Dim objEmployeeHistory As clsJobExperience_tran
    Dim objEmployeeSkill As clsEmployee_Skill_Tran
    'S.SANDEEP [ 09 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim objEmployeeReferee As clsEmployee_Refree_tran
    'S.SANDEEP [ 09 NOV 2012 ] -- END
    Dim mdtTran As DataTable
    Dim mstrMessage As String = String.Empty


    'Pinkal (02-Oct-2012) -- Start
    'Enhancement : TRA Changes
    Dim mstrApprovalString As String = String.Empty
    'Pinkal (02-Oct-2012) -- End

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private mintVacancyId As Integer = 0
    'S.SANDEEP [ 14 May 2013 ] -- END

#End Region

    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property
    'S.SANDEEP [ 23 JAN 2012 ] -- END


    'Pinkal (02-Oct-2012) -- Start
    'Enhancement : TRA Changes

    Public Property _ApprovalString() As String
        Get
            Return mstrApprovalString
        End Get
        Set(ByVal value As String)
            mstrApprovalString = value
        End Set
    End Property

    'Pinkal (02-Oct-2012) -- End

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Public WriteOnly Property _VacancyId() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property
    'S.SANDEEP [ 14 May 2013 ] -- END

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public WriteOnly Property _LoginEmployeeunkid() As Integer
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

'Sohail (02 Mar 2020) -- Start
    'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
    Private mintBaseCountryunkid As Integer = 0
    Public WriteOnly Property _BaseCountryunkid() As Integer
        Set(ByVal value As Integer)
            mintBaseCountryunkid = value
        End Set
    End Property
    'Sohail (02 Mar 2020) -- End
#Region " Private Functions "

    Public Function ImportApplicant(ByVal strDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    ByVal dtTable As DataTable, _
                                    ByVal blnAllowToApproveEarningDeduction As Boolean, _
                                    ByVal dtCurrentDateTime As DateTime, _
                                    ByVal blnIsArutiDemo As Boolean, _
                                    ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                                    ByVal intNoOfEmployees As Integer, _
                                    ByVal intEmployeeCodeNotype As Integer, _
                                    ByVal intDisplayNameSetting As Integer, _
                                    ByVal strEmployeeCodePrifix As String, _
                                    ByVal intRetirementValue As Integer, _
                                    ByVal intFRetirementValue As Integer, _
                                    ByVal blnIsPasswordAutoGenerated As Boolean, _
                                    ByVal intRetirementBy As Integer, _
                                    ByVal intFRetirementBy As Integer, _
                                    ByVal intSalaryAnniversarySetting As Integer, _
                                    ByVal intSalaryAnniversaryMonthBy As Integer, _
                                    ByVal mblnCreateADUserFromEmp As Boolean, _
                                    Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                    Optional ByVal strFilerString As String = "", _
                                    Optional ByVal xHostName As String = "", _
                                    Optional ByVal xIPAddr As String = "", _
                                    Optional ByVal xLoggedUserName As String = "", _
                                    Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP) As Boolean


        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ ByVal mblnCreateADUserFromEmp As Boolean, _]

        'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

        'Nilay (27 Apr 2016) -- Start
        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
        '[intSalaryAnniversaryType, intSalaryAnniversaryMonthBy]
        'Nilay (27 Apr 2016) -- End

        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFilerString]
        objDO = New clsDataOperation
        Dim blnFlag As Boolean = False
        Try
            If dtTable.Rows.Count <= 0 Then Return False
            'STEP 1
            objApplicant = New clsApplicant_master
            objVacancy = New clsVacancy

            objApplicantQualification = New clsApplicantQualification_tran
            objApplicantHistory = New clsJobhistory
            objApplicantSkill = New clsApplicantSkill_tran

            'S.SANDEEP [ 09 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objApplicantRef = New clsrcapp_reference_Tran
            'S.SANDEEP [ 09 NOV 2012 ] -- END



            'Pinkal (02-Oct-2012) -- Start
            'Enhancement : TRA Changes

            Dim StrMessage As New System.Text.StringBuilder
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append("Dear <b>#_UserName</b></span></p>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  This to inform you that following application has been imported as employee by user :  " & User._Object._Firstname & " " & User._Object._Lastname & " </span></p>")
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
            StrMessage.Append(vbCrLf)
                'Gajanan (21 Nov 2018) -- Start
            StrMessage.Append("<TABLE border = '1' WIDTH = '50%' style='margin-left: 25px'>")
                'Gajanan (21 Nov 2018) -- End
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:15%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 2, "Applicant") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 3, "Appointment Date") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 4, "Branch") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 5, "Department") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 6, "Section") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 7, "Job") & "</span></b></TD>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("</TR>")

            'Pinkal (02-Oct-2012) -- End
            'Varsha (10 Nov 2017) -- Start
            'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
            Dim objMasterData As New clsMasterData
            Dim intThemeId As Integer = objMasterData.GetDefaultThemeId(xCompanyUnkid, Nothing)
            'Varsha (10 Nov 2017) -- End

            For Each drRow As DataRow In dtTable.Rows

                objEmployee = New clsEmployee_Master
                objEmployeeQualification = New clsEmp_Qualification_Tran
                objEmployeeHistory = New clsJobExperience_tran
                objEmployeeSkill = New clsEmployee_Skill_Tran
                'S.SANDEEP [ 09 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objEmployeeReferee = New clsEmployee_Refree_tran
                'S.SANDEEP [ 09 NOV 2012 ] -- END


                objApplicant._Applicantunkid = CInt(drRow.Item("applicantunkid"))

                objEmployee._Anniversary_Date = objApplicant._Anniversary_Date
                objEmployee._Birthdate = objApplicant._Birth_Date


                'Pinkal (01-Dec-2012) -- Start
                'Enhancement : TRA Changes
                'objEmployee._Email = objApplicant._Email
                'Pinkal (01-Dec-2012) -- End

                objEmployee._Firstname = objApplicant._Firstname
                objEmployee._Gender = objApplicant._Gender
                objEmployee._Language1unkid = objApplicant._Language1unkid
                objEmployee._Language2unkid = objApplicant._Language2unkid
                objEmployee._Language3unkid = objApplicant._Language3unkid
                objEmployee._Language4unkid = objApplicant._Language4unkid
                objEmployee._Maritalstatusunkid = objApplicant._Marital_Statusunkid
                objEmployee._Nationalityunkid = objApplicant._Nationality
                objEmployee._Othername = objApplicant._Othername
                objEmployee._Domicile_Address1 = objApplicant._Perm_Address1
                objEmployee._Domicile_Address2 = objApplicant._Perm_Address2
                objEmployee._Domicile_Alternateno = objApplicant._Perm_Alternateno
                objEmployee._Domicile_Countryunkid = objApplicant._Perm_Countryunkid
                objEmployee._Domicile_Estate = objApplicant._Perm_Estate
                objEmployee._Domicile_Fax = objApplicant._Perm_Fax
                objEmployee._Domicile_Mobile = objApplicant._Perm_Mobileno
                objEmployee._Domicile_Plotno = objApplicant._Perm_Plotno
                objEmployee._Domicile_Post_Townunkid = objApplicant._Perm_Post_Townunkid
                objEmployee._Domicile_Provicnce = objApplicant._Perm_Province
                objEmployee._Domicile_Road = objApplicant._Perm_Road
                objEmployee._Domicile_Stateunkid = objApplicant._Perm_Stateunkid
                objEmployee._Domicile_Tel_No = objApplicant._Perm_Tel_No
                objEmployee._Domicile_Postcodeunkid = objApplicant._Perm_ZipCode
                objEmployee._Present_Address1 = objApplicant._Present_Address1
                objEmployee._Present_Address2 = objApplicant._Present_Address2
                objEmployee._Present_Alternateno = objApplicant._Present_Alternateno
                objEmployee._Present_Countryunkid = objApplicant._Present_Countryunkid
                objEmployee._Present_Estate = objApplicant._Present_Estate
                objEmployee._Present_Fax = objApplicant._Present_Fax
                objEmployee._Present_Mobile = objApplicant._Present_Mobileno
                objEmployee._Present_Plotno = objApplicant._Present_Plotno
                objEmployee._Present_Post_Townunkid = objApplicant._Present_Post_Townunkid
                objEmployee._Present_Provicnce = objApplicant._Present_Province
                objEmployee._Present_Road = objApplicant._Present_Road
                objEmployee._Present_Stateunkid = objApplicant._Present_Stateunkid
                objEmployee._Present_Tel_No = objApplicant._Present_Tel_No
                objEmployee._Present_Postcodeunkid = objApplicant._Present_ZipCode
                objEmployee._Surname = objApplicant._Surname
                objEmployee._Titalunkid = objApplicant._Titleunkid
                'S.SANDEEP [ 11 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objEmployee._Present_Email = objApplicant._Email
                'S.SANDEEP [ 11 DEC 2012 ] -- END
                'Sohail (03 Sep 2021) -- Start
                'Enhancement :  : Linking applicant with employee.
                objEmployee._Applicantunkid = CInt(drRow.Item("applicantunkid"))
                'Sohail (03 Sep 2021) -- End


                'S.SANDEEP [ 01 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objEmp._Password = dtRow.Item("displayname").ToString

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If blnIsPasswordAutoGenerated = True Then
                    'If ConfigParameter._Object._IsPasswordAutoGenerated = True Then
                    'S.SANDEEP [04 JUN 2015] -- END

                    Dim objPwdOp As New clsPassowdOptions
                    If objPwdOp._IsPasswordLenghtSet Then
                        If objPwdOp._PasswordLength < 6 Then
                            objEmployee._Password = clsRandomPassword.Generate(8, 8)
                        Else
                            objEmployee._Password = clsRandomPassword.Generate(objPwdOp._PasswordLength, objPwdOp._PasswordLength)
                        End If
                    Else
                        objEmployee._Password = clsRandomPassword.Generate(8, 8)
                    End If
                Else
                    objEmployee._Password = clsRandomPassword.Generate(8, 8)
                End If
                'S.SANDEEP [ 01 DEC 2012 ] -- END

                'Pinkal (01-Dec-2012) -- Start
                'Enhancement : TRA Changes
                '


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If intDisplayNameSetting = enDisplayNameSetting.EMPLOYEECODE Then
                    'If ConfigParameter._Object._DisplayNameSetting = enDisplayNameSetting.EMPLOYEECODE Then
                    'S.SANDEEP [04 JUN 2015] -- END

                    If ConfigParameter._Object._EmployeeCodeNotype = 1 Then
                        objEmployee._IsAutomateEmail = True
                        objEmployee._IsAutomateDisplayName = True
                    Else
                        objEmployee._IsAutomateEmail = False
                        objEmployee._IsAutomateDisplayName = False
                        objEmployee._Employeecode = drRow.Item("displayname").ToString().Trim
                        objEmployee._Displayname = drRow.Item("firstname").ToString().Trim & " " & drRow.Item("surname").ToString().Trim
                        objEmployee._Email = drRow.Item("email").ToString().Trim
                    End If
                Else
                    objEmployee._IsAutomateEmail = False
                    objEmployee._IsAutomateDisplayName = False
                    objEmployee._Displayname = drRow.Item("displayname").ToString().Trim
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If ConfigParameter._Object._EmployeeCodeNotype = 0 Then
                    '    objEmployee._Employeecode = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & Now.Millisecond.ToString
                    'End If

                    If intEmployeeCodeNotype = 0 Then
                        objEmployee._Employeecode = eZeeDate.convertDate(dtCurrentDateTime) & Now.Millisecond.ToString
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END
                    objEmployee._Email = drRow.Item("email").ToString().Trim
                End If



                'Pinkal (01-Dec-2012) -- End






                'Pinkal (02-Oct-2012) -- Start
                'Enhancement : TRA Changes

                objEmployee._Isapproved = False

                'If User._Object.Privilege._AllowToApproveEmployee = False Then
                '    objEmployee._Isactive = False
                'End If

                'Pinkal (02-Oct-2012) -- End


                '==== Getting Data Of Vancancy
                objVacancy._Vacancyunkid = objApplicant._Vacancyunkid

                '==== Assigning Data Of Vancancy to Employee

                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objEmployee._Employmenttypeunkid = objVacancy._Employeementtypeunkid
                objEmployee._Employmenttypeunkid = CInt(drRow.Item("employmentunkid"))
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                objEmployee._Paytypeunkid = objVacancy._Paytypeunkid



                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim intShiftUnkid As Integer = 0
                'GetFirstShift(intShiftUnkid, objVacancy._Shifttypeunkid)
                'objEmployee._Shiftunkid = intShiftUnkid


                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes
                'objEmployee._Shiftunkid = CInt(drRow.Item("shiftunkid"))
                'Pinkal (15-Oct-2013) -- End



                'S.SANDEEP [ 28 FEB 2012 ] -- END

                objEmployee._Tranhedunkid = CInt(drRow.Item("tranheadunkid"))
                objEmployee._Gradegroupunkid = CInt(drRow.Item("gradegroupunkid"))
                objEmployee._Gradeunkid = CInt(drRow.Item("gradeunkid"))
                objEmployee._Gradelevelunkid = CInt(drRow.Item("gradelevelunkid"))
                objEmployee._Scale = CDec(drRow.Item("scale"))

                'Pinkal (20-Jan-2012) -- Start
                'Enhancement : TRA Changes
                objEmployee._Stationunkid = CInt(drRow.Item("stationunkid"))

                objEmployee._Deptgroupunkid = CInt(drRow.Item("deptgroupunkid"))
                objEmployee._Departmentunkid = CInt(drRow.Item("departmentunkid"))

                objEmployee._Sectiongroupunkid = CInt(drRow.Item("sectiongrpunkid"))
                objEmployee._Sectionunkid = CInt(drRow.Item("sectionunkid"))

                objEmployee._Unitgroupunkid = CInt(drRow.Item("unitgroupunkid"))
                objEmployee._Unitunkid = CInt(drRow.Item("unitunkid"))
                objEmployee._Teamunkid = CInt(drRow.Item("teamunkid"))

                objEmployee._Classgroupunkid = CInt(drRow.Item("classgroupunkid"))
                objEmployee._Classunkid = CInt(drRow.Item("classunkid"))

                objEmployee._Jobgroupunkid = CInt(drRow.Item("jobgroupunkid"))
                objEmployee._Jobunkid = CInt(drRow.Item("jobunkid"))

                objEmployee._Costcenterunkid = CInt(drRow.Item("costcenterunkid"))
                'Pinkal (20-Jan-2012) -- End

                'Sohail (18 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                objEmployee._AssignDefaultTransactionHeads = CBool(drRow.Item("AssignDefaulTranHeads"))
                'Sohail (18 Feb 2019) -- End



                'S.SANDEEP [ 29 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'TYPE : EMPLOYEMENT CONTRACT PROCESS
                'objEmployee._Appointeddate = Now


                'Pinkal (02-Oct-2012) -- Start
                'Enhancement : TRA Changes

                'objEmployee._Appointeddate = ConfigParameter._Object._CurrentDateAndTime
                'objEmployee._Confirmation_Date = ConfigParameter._Object._CurrentDateAndTime

                objEmployee._Appointeddate = CDate(drRow.Item("appointeddate"))
                objEmployee._Confirmation_Date = CDate(drRow.Item("appointeddate"))
                'Pinkal (02-Oct-2012) -- End


                If objApplicant._Birth_Date <> Nothing Then
                    'S.SANDEEP [ 27 AUG 2014 ] -- START
                    'If ConfigParameter._Object._RetirementBy = 1 Then
                    '    Dim dtRetireDate As Date = Nothing
                    '    dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, objApplicant._Birth_Date)
                    '    objEmployee._Termination_To_Date = dtRetireDate
                    '    'Sohail (10 Apr 2013) -- Start
                    '    'TRA - ENHANCEMENT
                    'Else
                    '    objEmployee._Termination_To_Date = CDate(drRow.Item("retirementdate"))
                    '    'Sohail (10 Apr 2013) -- End
                    'End If
                    If objEmployee._Gender > 0 Then
                        Select Case CInt(objEmployee._Gender)
                            Case 1  'MALE

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                If intRetirementBy = 1 Then
                                    'If ConfigParameter._Object._RetirementBy = 1 Then
                                    'S.SANDEEP [04 JUN 2015] -- END

                                    Dim dtRetireDate As Date = Nothing

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, objApplicant._Birth_Date)
                                    dtRetireDate = DateAdd(DateInterval.Year, intRetirementValue, objApplicant._Birth_Date)
                                    'S.SANDEEP [04 JUN 2015] -- END

                                    objEmployee._Termination_To_Date = dtRetireDate
                                Else
                                    objEmployee._Termination_To_Date = CDate(drRow.Item("retirementdate"))
                                End If
                            Case 2  'FEMALE

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                If intFRetirementBy = 1 Then
                                    'If ConfigParameter._Object._FRetirementBy = 1 Then
                                    'S.SANDEEP [04 JUN 2015] -- END

                                    Dim dtRetireDate As Date = Nothing

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._FRetirementValue, objApplicant._Birth_Date)
                                    dtRetireDate = DateAdd(DateInterval.Year, intFRetirementValue, objApplicant._Birth_Date)
                                    'S.SANDEEP [04 JUN 2015] -- END

                                    objEmployee._Termination_To_Date = dtRetireDate
                                Else
                                    objEmployee._Termination_To_Date = CDate(drRow.Item("retirementdate"))
                                End If
                        End Select
                    Else

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        If intRetirementBy = 1 Then
                            'If ConfigParameter._Object._RetirementBy = 1 Then
                            'S.SANDEEP [04 JUN 2015] -- END

                            Dim dtRetireDate As Date = Nothing

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, objApplicant._Birth_Date)
                            dtRetireDate = DateAdd(DateInterval.Year, intRetirementValue, objApplicant._Birth_Date)
                            'S.SANDEEP [04 JUN 2015] -- END

                            objEmployee._Termination_To_Date = dtRetireDate
                        Else
                            objEmployee._Termination_To_Date = CDate(drRow.Item("retirementdate"))
                        End If
                    End If
                    'S.SANDEEP [ 27 AUG 2014 ] -- END
                Else

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If intRetirementBy = 1 Then
                        'If ConfigParameter._Object._RetirementBy = 1 Then
                        'S.SANDEEP [04 JUN 2015] -- END

                        Dim dtRetireDate As Date = Nothing

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, ConfigParameter._Object._CurrentDateAndTime)
                        dtRetireDate = DateAdd(DateInterval.Year, intRetirementValue, dtCurrentDateTime)
                        'S.SANDEEP [04 JUN 2015] -- END

                        objEmployee._Termination_To_Date = dtRetireDate
                        'Sohail (10 Apr 2013) -- Start
                        'TRA - ENHANCEMENT
                    Else
                        objEmployee._Termination_To_Date = CDate(drRow.Item("retirementdate"))
                        'Sohail (10 Apr 2013) -- End
                    End If
                End If


                'S.SANDEEP [ 29 DEC 2011 ] -- END

                ''EMAIL VALIDATION
                Dim objMData As New clsMasterData
                If objMData.IsEmailPresent("hremployee_master", objEmployee._Email) = True AndAlso objEmployee._Email <> "" Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this email address is already used by some employee. please provide another email address.")
                    Continue For
                End If

                'S.SANDEEP [ 18 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes
                'If objEmployee._Shiftunkid > 0 Then
                If CInt(drRow.Item("shiftunkid")) > 0 Then
                    'Pinkal (15-Oct-2013) -- End

                    Dim objSftTran As New clsEmployee_Shift_Tran
                    objSftTran._EmployeeUnkid = -1
                    Dim mdTable As DataTable
                    mdTable = objSftTran._SDataTable.Copy

                    Dim dtSRow As DataRow
                    dtSRow = mdTable.NewRow
                    dtSRow.Item("shifttranunkid") = -1
                    dtSRow.Item("employeeunkid") = -1
                    dtSRow.Item("shiftunkid") = CInt(drRow.Item("shiftunkid"))
                    dtSRow.Item("isdefault") = True
                    dtSRow.Item("userunkid") = User._Object._Userunkid
                    dtSRow.Item("isvoid") = False
                    dtSRow.Item("voiduserunkid") = -1
                    dtSRow.Item("voiddatetime") = DBNull.Value
                    dtSRow.Item("voidreason") = ""
                    dtSRow.Item("AUD") = "A"

                    'Pinkal (15-Oct-2013) -- Start
                    'Enhancement : TRA Changes
                    dtSRow.Item("effectivedate") = CDate(drRow.Item("appointeddate"))
                    'Pinkal (15-Oct-2013) -- End


                    mdTable.Rows.Add(dtSRow)
                    objEmployee._dtShiftTran = mdTable.Copy
                End If
                'S.SANDEEP [ 18 DEC 2012 ] -- END

                'Varsha (10 Nov 2017) -- Start
                'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
                objEmployee._Theme_Id = intThemeId
                'Varsha (10 Nov 2017) -- End
                'Sohail (02 Mar 2020) -- Start
                'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
                objEmployee._BaseCountryunkid = mintBaseCountryunkid
                'Sohail (02 Mar 2020) -- End

                'Sohail (10 Apr 2013) -- Start
                'License - ENHANCEMENT
                'blnFlag = objEmployee.Insert()
                Dim blnEmployeeExceed As Boolean = False

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objEmployee.Insert(, , , , blnEmployeeExceed)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objEmployee.Insert(ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._IsArutiDemo, Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, , , , , blnEmployeeExceed)

                'Nilay (27 Apr 2016) -- Start
                'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
                'blnFlag = objEmployee.Insert(strDatabaseName, dtCurrentDateTime.Date, blnIsArutiDemo, _
                '                             intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intEmployeeCodeNotype, _
                '                             intDisplayNameSetting, strEmployeeCodePrifix, _
                '                             xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                '                             xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, , , , , blnEmployeeExceed, blnApplyUserAccessFilter, strFilerString)

                'S.SANDEEP [08 DEC 2016] -- START
                'ENHANCEMENT : Issue for Wrong User Name Sent in Email (Edit By User : <UserName>)
                'blnFlag = objEmployee.Insert(strDatabaseName, dtCurrentDateTime.Date, blnIsArutiDemo, _
                '                             intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intEmployeeCodeNotype, _
                '                             intDisplayNameSetting, strEmployeeCodePrifix, _
                '                             xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                '                             xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, _
                '                             intSalaryAnniversarySetting, intSalaryAnniversaryMonthBy, _
                '                             , , , blnEmployeeExceed, blnApplyUserAccessFilter, strFilerString)

                With objEmployee
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._Companyunkid = mintCompanyUnkid
                End With

                'S.SANDEEP [26-SEP-2018] -- START
                objEmployee._IsFlexAccountCreated = False
                'S.SANDEEP [26-SEP-2018] -- END
               
                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'blnFlag = objEmployee.Insert(strDatabaseName, dtCurrentDateTime.Date, blnIsArutiDemo, _
                '                             intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intEmployeeCodeNotype, _
                '                             intDisplayNameSetting, strEmployeeCodePrifix, _
                '                             xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                '                             xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, _
                '                             intSalaryAnniversarySetting, intSalaryAnniversaryMonthBy, _
                '                             , , , , blnEmployeeExceed, blnApplyUserAccessFilter, strFilerString, , xHostName, xIPAddr, xLoggedUserName, xLoginMod)

                blnFlag = objEmployee.Insert(strDatabaseName, dtCurrentDateTime.Date, blnIsArutiDemo, _
                                                         intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intEmployeeCodeNotype, _
                                                         intDisplayNameSetting, strEmployeeCodePrifix, _
                                                         xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                         xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, _
                                                         intSalaryAnniversarySetting, intSalaryAnniversaryMonthBy, mblnCreateADUserFromEmp, _
                                                         , , , , blnEmployeeExceed, blnApplyUserAccessFilter, strFilerString, , xHostName, xIPAddr, xLoggedUserName, xLoginMod)


                'Pinkal (18-Aug-2018) -- End                
                'S.SANDEEP [08 DEC 2016] -- END

                'Nilay (27 Apr 2016) -- End

                'Sohail (21 Aug 2015) -- End
                'S.SANDEEP [04 JUN 2015] -- END


                'Sohail (10 Apr 2013) -- End

                If blnFlag = False AndAlso objEmployee._Message <> "" Then
                    eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
                    Return False
                End If

                Dim intEmpId As Integer = -1
                If blnFlag = True Then
                    intEmpId = objEmployee._RefEmployeeId

                    '================= Inserting Qualification 
                    objApplicantQualification._ApplicantUnkid = objApplicant._Applicantunkid
                    mdtTran = objApplicantQualification._DataList
                    If mdtTran.Rows.Count > 0 Then
                        For Each dtRow As DataRow In mdtTran.Rows
                            'Sandeep [ 09 Oct 2010 ] -- Start
                            'objEmployeeQualification._Employeeunkid = intEmpId
                            'objEmployeeQualification._Award_Name = dtRow.Item("award")
                            'objEmployeeQualification._Contact_No = dtRow.Item("contactno")
                            'objEmployeeQualification._Contact_Person = dtRow.Item("contactperson")
                            'objEmployeeQualification._Institute_Address = dtRow.Item("address")
                            'objEmployeeQualification._Institute_Name = dtRow.Item("institution_name")
                            'objEmployeeQualification._Isvoid = False
                            'If IsDBNull(dtRow.Item("qualificationdate")) = True Then
                            '    objEmployeeQualification._Qualification_Date = Nothing
                            'Else
                            '    objEmployeeQualification._Qualification_Date = dtRow.Item("qualificationdate")
                            'End If
                            'objEmployeeQualification._Qualificationgroupunkid = dtRow.Item("qualificationgroupunkid")
                            'objEmployeeQualification._Qualificationunkid = dtRow.Item("qualificationunkid")
                            'objEmployeeQualification._Reference_No = dtRow.Item("referenceno")
                            'objEmployeeQualification._Remark = dtRow.Item("remark")
                            'objEmployeeQualification._Userunkid = User._Object._Userunkid
                            'objEmployeeQualification._Voiddatetime = Nothing
                            'objEmployeeQualification._Voidreason = ""
                            'objEmployeeQualification._Voiduserunkid = -1
                            'objEmployeeQualification.Insert()

                            If IsDBNull(dtRow.Item("award_end_date")) = False Then
                                objEmployeeQualification._Award_End_Date = dtRow.Item("award_end_date")
                            Else
                                objEmployeeQualification._Award_End_Date = Nothing
                            End If

                            If IsDBNull(dtRow.Item("award_start_date")) = False Then
                                objEmployeeQualification._Award_Start_Date = dtRow.Item("award_start_date")
                            Else
                                objEmployeeQualification._Award_Start_Date = Nothing
                            End If

                            objEmployeeQualification._Employeeunkid = intEmpId
                            objEmployeeQualification._Instituteunkid = dtRow.Item("instituteunkid")
                            objEmployeeQualification._Isvoid = False
                            objEmployeeQualification._Qualificationgroupunkid = dtRow.Item("qualificationgroupunkid")
                            objEmployeeQualification._Qualificationunkid = dtRow.Item("qualificationunkid")
                            objEmployeeQualification._Reference_No = dtRow.Item("reference_no")
                            'S.SANDEEP [ 28 FEB 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'objEmployeeQualification._Remark = dtRow.Item("remark")
                            objEmployeeQualification._Remark = dtRow.Item("remark") & vbCrLf & objApplicant._OtherQualifications.ToString
                            'S.SANDEEP [ 28 FEB 2012 ] -- END
                            objEmployeeQualification._Transaction_Date = dtRow.Item("transaction_date")
                            objEmployeeQualification._Userunkid = User._Object._Userunkid
                            objEmployeeQualification._Voiddatetime = Nothing
                            objEmployeeQualification._Voidreason = ""
                            objEmployeeQualification._Voiduserunkid = -1

                            'S.SANDEEP [ 28 FEB 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            objEmployeeQualification._GPAcode = dtRow.Item("gpacode")
                            objEmployeeQualification._Resultunkid = dtRow.Item("resultunkid")
                            'S.SANDEEP [ 28 FEB 2012 ] -- END


'Pinkal (24-Sep-2020) -- Start
                            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                            objEmployeeQualification._CertificateNo = dtRow.Item("certificateno").ToString()
                            objEmployeeQualification._IsHighestQualification = CBool(dtRow.Item("ishighestqualification").ToString())
                            'Pinkal (24-Sep-2020) -- End

                            With objEmployeeQualification
                                ._FormName = mstrFormName
                                ._Loginemployeeunkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With

                            objEmployeeQualification.Insert()
                            'Sandeep [ 09 Oct 2010 ] -- End 
                        Next
                    End If

                    '================= Inserting Job History
                    objApplicantHistory._Applicantunkid = objApplicant._Applicantunkid
                    mdtTran = objApplicantHistory._DataList
                    If mdtTran.Rows.Count > 0 Then
                        For Each dtRow As DataRow In mdtTran.Rows
                            objEmployeeHistory._Employeeunkid = intEmpId
                            objEmployeeHistory._Address = ""
                            objEmployeeHistory._Company = dtRow.Item("companyname")
                            objEmployeeHistory._Contact_No = dtRow.Item("officephone")
                            objEmployeeHistory._Contact_Person = dtRow.Item("employername")
                            objEmployeeHistory._Iscontact_Previous = True
                            If IsDBNull(dtRow.Item("terminationdate")) = False Then
                                objEmployeeHistory._End_Date = dtRow.Item("terminationdate")
                            Else
                                objEmployeeHistory._End_Date = Nothing
                            End If
                            objEmployeeHistory._Isvoid = False
                            objEmployeeHistory._Jobunkid = 0
                            objEmployeeHistory._Last_Pay = 0
                            objEmployeeHistory._Leave_Reason = dtRow.Item("leavingreason")
                            objEmployeeHistory._Memo = ""
                            objEmployeeHistory._PreviousBenefit = ""
                            objEmployeeHistory._Remark = ""
                            If IsDBNull(dtRow.Item("joiningdate")) = False Then
                                objEmployeeHistory._Start_Date = dtRow.Item("joiningdate")
                            Else
                                objEmployeeHistory._Start_Date = Nothing
                            End If

                            objEmployeeHistory._Supervisor = dtRow.Item("employername")
                            objEmployeeHistory._Userunkid = User._Object._Userunkid
                            objEmployeeHistory._Voidatetime = Nothing
                            objEmployeeHistory._Voidreason = ""
                            objEmployeeHistory._Voiduserunkid = -1

                            With objEmployeeHistory
                                ._FormName = mstrFormName
                                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With

                            objEmployeeHistory.Insert()
                        Next
                    End If

                    '================= Inserting Skills
                    objApplicantSkill._ApplicantUnkid = objApplicant._Applicantunkid
                    mdtTran = objApplicantSkill._DataList

                    With objEmployeeSkill
                        ._FormName = mstrFormName
                        ._Loginemployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With

                    If mdtTran.Rows.Count > 0 Then
                        For Each dtRow As DataRow In mdtTran.Rows
                            objEmployeeSkill._Emp_App_Unkid = intEmpId

                            'Pinkal (24-Sep-2020) -- Start
                            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                            'objEmployeeSkill._Description = dtRow.Item("remark") & vbCrLf & objApplicant._OtherSkills.ToString
                            objEmployeeSkill._Description = dtRow.Item("remark")
                            objEmployeeSkill._Other_SkillCategory = dtRow.Item("other_skillcategory").ToString().Trim()
                            objEmployeeSkill._Other_Skill = dtRow.Item("other_skill").ToString().Trim()
                            objEmployeeSkill._SkillExpertiseunkid = CInt(dtRow.Item("skillexpertiseunkid"))
                            'Pinkal (24-Sep-2020) -- End


                            objEmployeeSkill._Isapplicant = 0
                            objEmployeeSkill._Isvoid = False
                            objEmployeeSkill._Skillcategoryunkid = dtRow.Item("skillcategoryunkid")
                            objEmployeeSkill._Skillunkid = dtRow.Item("skillunkid")
                            objEmployeeSkill._Userunkid = User._Object._Userunkid
                            objEmployeeSkill._Voiddatetime = Nothing
                            objEmployeeSkill._Voidreason = ""
                            objEmployeeSkill._Voiduserunkid = -1
                            objEmployeeSkill.Insert()
                        Next
                    End If
                    'S.SANDEEP [ 09 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    '=================== Inserting References
                    objApplicantRef._Applicantunkid = objApplicant._Applicantunkid
                    mdtTran = objApplicantRef._dtReferences

                    With objEmployeeReferee
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    For Each dtRow As DataRow In mdtTran.Rows
                        objEmployeeReferee._Employeeunkid = intEmpId
                        objEmployeeReferee._Address = dtRow.Item("address")
                        objEmployeeReferee._Countryunkid = dtRow.Item("countryunkid")
                        objEmployeeReferee._Stateunkid = dtRow.Item("stateunkid")
                        objEmployeeReferee._Cityunkid = dtRow.Item("cityunkid")
                        objEmployeeReferee._Company = ""
                        objEmployeeReferee._Email = dtRow.Item("email")
                        objEmployeeReferee._Gender = dtRow.Item("genderunkid")
                        objEmployeeReferee._Mobile_No = dtRow.Item("mobile_no")
                        objEmployeeReferee._Name = dtRow.Item("name")
                        objEmployeeReferee._Ref_Position = dtRow.Item("position")
                        objEmployeeReferee._Relationunkid = dtRow.Item("relationunkid")
                        objEmployeeReferee._Telephone_No = dtRow.Item("telephone_no")
                        objEmployeeReferee._Userunkid = User._Object._Userunkid
                        objEmployeeReferee.Insert()
                    Next

                    'S.SANDEEP [ 03 FEB 2014 ] -- START

                    ''=================== Inserting Shift in Employee Shift Tran
                    'Dim objEmplShift As New clsEmployee_Shift_Tran
                    'objEmplShift._EmployeeUnkid = intEmpId
                    'mdtTran = objEmplShift._SDataTable
                    'Dim dtSRow As DataRow = mdtTran.NewRow

                    'dtSRow.Item("shifttranunkid") = -1
                    'dtSRow.Item("employeeunkid") = intEmpId
                    'dtSRow.Item("shiftunkid") = CInt(drRow.Item("shiftunkid"))
                    'dtSRow.Item("userunkid") = User._Object._Userunkid
                    'dtSRow.Item("isvoid") = False
                    'dtSRow.Item("voiduserunkid") = -1
                    'dtSRow.Item("voiddatetime") = DBNull.Value
                    'dtSRow.Item("voidreason") = ""
                    'dtSRow.Item("AUD") = "A"
                    'dtSRow.Item("GUID") = Guid.NewGuid.ToString
                    'dtSRow.Item("isdefault") = True
                    'mdtTran.Rows.Add(dtSRow)
                    'objEmplShift.InsertUpdateDelete()
                    ''S.SANDEEP [ 09 NOV 2012 ] -- END

                    'S.SANDEEP [ 03 FEB 2014 ] -- END



                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objApplicant._FormName = mstrFormName
                objApplicant._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objApplicant._ClientIP = mstrClientIP
                objApplicant._HostName = mstrHostName
                objApplicant._FromWeb = mblnIsWeb
                objApplicant._AuditUserId = mintAuditUserId
objApplicant._CompanyUnkid = mintCompanyUnkid
                objApplicant._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objApplicant.UpdateImport_Status(CInt(drRow.Item("applicantunkid")), mintVacancyId, User._Object._Userunkid)
                objApplicant.UpdateImport_Status(CInt(drRow.Item("applicantunkid")), mintVacancyId, xUserUnkid, dtCurrentDateTime)
                'S.SANDEEP [04 JUN 2015] -- END




                'Pinkal (02-Oct-2012) -- Start
                'Enhancement : TRA Changes

                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objApplicant._Firstname & " " & objApplicant._Othername & " " & objApplicant._Surname & "</span></TD>")
                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CDate(drRow.Item("appointeddate")).ToShortDateString & "</span></TD>")

                Dim objstation As New clsStation
                objstation._Stationunkid = objEmployee._Stationunkid
                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objstation._Name & "</span></TD>")

                Dim objdept As New clsDepartment
                objdept._Departmentunkid = objEmployee._Departmentunkid
                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objdept._Name & "</span></TD>")

                Dim objSection As New clsSections
                objSection._Sectionunkid = objEmployee._Sectionunkid
                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objSection._Name & "</span></TD>")

                Dim objJob As New clsJobs
                objJob._Jobunkid = objEmployee._Jobunkid
                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objJob._Job_Name & "</span></TD>")

                StrMessage.Append("</TR>" & vbCrLf)

                'Pinkal (02-Oct-2012) -- End



            Next


            'Pinkal (02-Oct-2012) -- Start
            'Enhancement : TRA Changes
            StrMessage.Append("</TABLE>")

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please login to Main Aruti HR system to Approve/Reject these applicant(s) as an employee(s).</span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 8, "Please login to Main Aruti HR system to Approve/Reject these applicant(s) as an employee(s).") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End

            'S.SANDEEP [ 03 FEB 2014 ] -- START
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            'StrMessage.Append("</span></p>")
            'StrMessage.Append("</BODY></HTML>")
            'S.SANDEEP [ 03 FEB 2014 ] -- END

            mstrApprovalString = StrMessage.ToString()

            'Pinkal (02-Oct-2012) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Private Sub GetFirstShift(ByRef intShiftId As Integer, ByVal intShiftTypeId As Integer)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim objDO As New clsDataOperation
            StrQ = "SELECT TOP 1 " & _
                       "  shiftunkid AS Id " & _
                       "FROM tnashift_master " & _
                       "WHERE shifttypeunkid = @ShiftTypeId "

            objDO.AddParameter("@ShiftTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftTypeId)

            dsList = objDO.ExecQuery(StrQ, "List")

            If objDO.ErrorMessage <> "" Then
                exForce = New Exception(objDO.ErrorNumber & ":" & objDO.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intShiftId = dsList.Tables(0).Rows(0)("Id")
            Else
                intShiftId = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFirstShift", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, this email address is already used by some employee. please provide another email address.")
			Language.setMessage(mstrModuleName, 2, "Applicant")
			Language.setMessage(mstrModuleName, 3, "Appointment Date")
			Language.setMessage(mstrModuleName, 4, "Branch")
			Language.setMessage(mstrModuleName, 5, "Department")
			Language.setMessage(mstrModuleName, 6, "Section")
			Language.setMessage(mstrModuleName, 7, "Job")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
