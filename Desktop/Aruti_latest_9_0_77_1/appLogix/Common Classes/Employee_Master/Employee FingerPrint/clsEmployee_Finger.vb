'************************************************************************************************************************************
'Class Name : clsDeskUser_Finger.vb
'Purpose      : All owner level opration like getlist, getComboList, insert, update, delete, checkduplicate.
'Date           : 03-Jan-2011
'Written By  : Pinkal
'Modified     : 
'Last Message Index = 1
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZeeCommonLib.eZeeDatatype
Imports system.Reflection


''' <summary>
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsEmployee_Finger
    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Finger"

#Region " Private variables "

    Private mstrMessage As String = ""
    Private mintFingerUnkId As Integer = -1
    Private mintFinger_No As Integer = 0
    Private mintFinger_Mask As Integer = 0
    Private mintEmployeeUnkId As Integer = -1
    Private mimgFingerprint_Data As Byte() = Nothing
    Private mintData_Size As Integer = 0


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrDeviceCode As String = ""
    'Pinkal (20-Jan-2012) -- End


#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FingerUnkId() As Integer
        Get
            Return mintFingerUnkId
        End Get
        Set(ByVal value As Integer)
            mintFingerUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Finger_No() As Integer
        Get
            Return mintFinger_No
        End Get
        Set(ByVal value As Integer)
            mintFinger_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Finger_Mask() As Integer
        Get
            Return mintFinger_Mask
        End Get
        Set(ByVal value As Integer)
            mintFinger_Mask = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmployeeUnkId() As Integer
        Get
            Return mintEmployeeUnkId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fingerprint_Data() As Byte()
        Get
            Return mimgFingerprint_Data
        End Get
        Set(ByVal value As Byte())
            mimgFingerprint_Data = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Data_Size() As Integer
        Get
            Return mintData_Size
        End Get
        Set(ByVal value As Integer)
            mintData_Size = value
        End Set
    End Property



    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceCode() As String
        Get
            Return mstrDeviceCode
        End Get
        Set(ByVal value As String)
            mstrDeviceCode = value
        End Set
    End Property

    'Pinkal (20-Jan-2012) -- End


#End Region

    ''' <summary>
    ''' For assign database tabel (hremployee_fingerprint) value into property.
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Function getEmployee_FingerData(Optional ByVal intUserUnkId As Integer = -1, Optional ByVal mstrDeviceCode As String = "") As DataTable
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '            "  fingerunkid " & _
            '            ", finger_no " & _
            '            ", finger_mask " & _
            '            ", employeeunkid " & _
            '            ", fingerprint_data " & _
            '            ", data_size " & _
            '        "FROM hremployee_fingerprint "

            strQ = "SELECT " & _
                        "  fingerunkid " & _
                        ", finger_no " & _
                        ", finger_mask " & _
                        ", employeeunkid " & _
                        ", fingerprint_data " & _
                        ", data_size " & _
                        ", ISNULL(devicecode,'') devicecode " & _
                        " FROM hremployee_fingerprint " & _
                        " WHERE 1 = 1"


            If intUserUnkId >= 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            End If


            If mstrDeviceCode.Trim <> "" Then
                strQ &= " AND devicecode = @devicecode "
                objDataOperation.AddParameter("@devicecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceCode)
            End If


            'Pinkal (20-Jan-2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Copy
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getEmployee_FingerData", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' For insert given property value in database table (hremployee_fingerprint).
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> </purpose>
    Public Function Insert() As Boolean
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mintEmployeeUnkId, mintFinger_No, mintFinger_Mask, mstrDeviceCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "The fingerprint of this employee is already enrolled. Please enroll another fingerprint.")
            Return False
        End If

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.BindTransaction()
            objDataOperation.AddParameter("@finger_no", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinger_No.ToString)
            objDataOperation.AddParameter("@finger_mask", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinger_Mask.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId.ToString)
            objDataOperation.AddParameter("@fingerprint_data", SqlDbType.VarBinary, mimgFingerprint_Data.Length, mimgFingerprint_Data)
            objDataOperation.AddParameter("@data_size", SqlDbType.Int, eZeeDataType.INT_SIZE, mintData_Size.ToString)


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            objDataOperation.AddParameter("@devicecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceCode.ToString)



            'strQ = "INSERT INTO hremployee_fingerprint ( " & _
            '            "  finger_no " & _
            '            ", finger_mask " & _
            '            ", employeeunkid " & _
            '            ", fingerprint_data " & _
            '            ", data_size " & _
            '        ") VALUES (" & _
            '            "  @finger_no " & _
            '            ", @finger_mask " & _
            '            ", @employeeunkid " & _
            '            ", @fingerprint_data " & _
            '            ", @data_size " & _
            '        ") ;  SELECT @@identity "

            strQ = "INSERT INTO hremployee_fingerprint ( " & _
                        "  finger_no " & _
                        ", finger_mask " & _
                        ", employeeunkid " & _
                        ", fingerprint_data " & _
                        ", data_size " & _
                        ", devicecode " & _
                    ") VALUES (" & _
                        "  @finger_no " & _
                        ", @finger_mask " & _
                        ", @employeeunkid " & _
                        ", @fingerprint_data " & _
                        ", @data_size " & _
                        ", @devicecode " & _
                    ") ;  SELECT @@identity "

            'Pinkal (20-Jan-2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintFingerUnkId = dsList.Tables(0).Rows(0).Item(0)
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show(-1, ex.Message, "Insert", mstrModuleName)
            Return False
        Finally
            objDataOperation.ReleaseTransaction(False)
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' For delete value from database table (hremployee_fingerprint) on given id.
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function Delete(ByVal intUserunkId As Integer, ByVal intFingureNo As Integer, ByVal strDeviceCode As String) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            objDataOperation.BindTransaction()

            'strQ = "DELETE FROM hremployee_fingerprint " & _
            '        "WHERE employeeunkid = @employeeunkid " & _
            '        "AND finger_no = @finger_no "

            strQ = "DELETE FROM hremployee_fingerprint " & _
                    "WHERE employeeunkid = @employeeunkid " & _
                     " AND finger_no = @finger_no AND devicecode = @devicecode "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@finger_no", SqlDbType.Int, eZeeDataType.INT_SIZE, intFingureNo.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkId.ToString)
            objDataOperation.AddParameter("@devicecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDeviceCode.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show(-1, ex.Message, "Delete", mstrModuleName)
            Return False
        Finally
            objDataOperation.ReleaseTransaction(False)
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function getEnrolledFingersMask(ByVal intUserUnkId As Integer) As Integer
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intEnrolledFingersMask As Integer = 0
        Dim objDataOperation As New clsDataOperation
        Try


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT ISNULL(SUM(finger_mask),0) " & _
            '        "FROM hremployee_fingerprint " & _
            '        "WHERE employeeunkid = @employeeunkid "

            strQ = "SELECT ISNULL(SUM(finger_mask),0) " & _
                    "FROM hremployee_fingerprint " & _
                                "WHERE employeeunkid = @employeeunkid AND devicecode = @devicecode"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId.ToString)
            objDataOperation.AddParameter("@devicecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUserUnkId.ToString)

            'Pinkal (20-Jan-2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                intEnrolledFingersMask = dtRow.Item(0)
                Exit For
            Next

            Return intEnrolledFingersMask

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getData", mstrModuleName)
            Return 0
        Finally
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Function getFingerMask(ByVal intFinger As Integer) As Integer
        Try
            Select Case intFinger
                Case 1
                    Return 32
                Case 2
                    Return 64
                Case 3
                    Return 128
                Case 4
                    Return 256
                Case 5
                    Return 512
                Case 6
                    Return 16
                Case 7
                    Return 8
                Case 8
                    Return 4
                Case 9
                    Return 2
                Case 10
                    Return 1
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getFingerMask", mstrModuleName)
        End Try
    End Function

    Public Function isExist(ByVal intEmployeeId As Integer, ByVal fingerindex As Integer, ByVal finger_mask As Integer, ByVal strDeviceCode As String, Optional ByVal strDisplayName As String = "List") As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '        "  fingerunkid " & _
            '        ", finger_no " & _
            '        ", finger_mask " & _
            '        ", employeeunkid " & _
            '        ", fingerprint_data " & _
            '        ", data_size " & _
            '        " FROM hremployee_fingerprint  " & _
            '        " Where employeeunkid = @employeeunkid and finger_no = @finger_no and finger_mask = @finger_mask"

            strQ = "SELECT " & _
                    "  fingerunkid " & _
                    ", finger_no " & _
                    ", finger_mask " & _
                    ", employeeunkid " & _
                    ", fingerprint_data " & _
                    ", data_size " & _
                 ", devicecode " & _
                    " FROM hremployee_fingerprint  " & _
                 " Where employeeunkid = @employeeunkid and finger_no = @finger_no and finger_mask = @finger_mask AND devicecode = @devicecode "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@finger_no", SqlDbType.Int, eZeeDataType.INT_SIZE, fingerindex)
            objDataOperation.AddParameter("@finger_mask", SqlDbType.Int, eZeeDataType.INT_SIZE, finger_mask)
            objDataOperation.AddParameter("@devicecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDeviceCode)

            'Pinkal (20-Jan-2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, strDisplayName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isExist", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "The fingerprint of this employee is already enrolled. Please enroll another fingerprint.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class