﻿'************************************************************************************************************************************
'Class Name : clsEmployee_Refree_tran.vb
'Purpose    :
'Date       :19/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsEmployee_Refree_tran
    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Refree_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintRefereetranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrName As String = String.Empty
    Private mstrAddress As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mstrEmail As String = String.Empty
    Private mstrGender As Integer = 0
    Private mstrIdentify_No As String = String.Empty
    Private mstrTelephone_No As String = String.Empty
    Private mstrMobile_No As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sandeep [ 17 DEC 2010 ] -- Start
    Private mintRelationunkid As Integer = 0
    'Sandeep [ 17 DEC 2010 ] -- End 


    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    Private mintLoginEmployeeunkid As Integer = -1
    Private mintVoidloginEmployeeunkid As Integer = -1
    'Pinkal (12-Oct-2011) -- End

    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrRef_Position As String = String.Empty
    'S.SANDEEP [ 28 FEB 2012 ] -- END

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'Gajanan [17-DEC-2018] -- End
#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set refereetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Refereetranunkid(Optional ByVal objDataOper As clsDataOperation = Nothing) As Integer 'Gajanan [17-DEC-2018] -- Add Opration Type
        Get
            Return mintRefereetranunkid
        End Get
        Set(ByVal value As Integer)
            mintRefereetranunkid = value
            Call GetData(objDataOper) 'Gajanan [17-DEC-2018] -- Add Oprationtype
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address() As String
        Get
            Return mstrAddress
        End Get
        Set(ByVal value As String)
            mstrAddress = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gender
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Gender() As Integer
        Get
            Return mstrGender
        End Get
        Set(ByVal value As Integer)
            mstrGender = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set identify_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Company() As String
        Get
            Return mstrIdentify_No
        End Get
        Set(ByVal value As String)
            mstrIdentify_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set telephone_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Telephone_No() As String
        Get
            Return mstrTelephone_No
        End Get
        Set(ByVal value As String)
            mstrTelephone_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mobile_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Mobile_No() As String
        Get
            Return mstrMobile_No
        End Get
        Set(ByVal value As String)
            mstrMobile_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


    'Sandeep [ 17 DEC 2010 ] -- Start
    ''' <summary>
    ''' Purpose: Get or Set relationunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Relationunkid() As Integer
        Get
            Return mintRelationunkid
        End Get
        Set(ByVal value As Integer)
            mintRelationunkid = value
        End Set
    End Property
    'Sandeep [ 17 DEC 2010 ] -- End 


    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    ''' <summary>
    ''' Purpose: Get or Set loginEmployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' 
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    ''' <summary>
    ''' Purpose: Get or Set voidloginEmployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' 
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidloginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginEmployeeunkid = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End



    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set ref_position
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Ref_Position() As String
        Get
            Return mstrRef_Position
        End Get
        Set(ByVal value As String)
            mstrRef_Position = value
        End Set
    End Property
    'S.SANDEEP [ 28 FEB 2012 ] -- END


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.



        'objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        'Gajanan [17-DEC-2018] -- End
        Try


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'strQ = "SELECT " & _
            '      "  refereetranunkid " & _
            '      ", employeeunkid " & _
            '      ", name " & _
            '      ", address " & _
            '      ", countryunkid " & _
            '      ", stateunkid " & _
            '      ", cityunkid " & _
            '      ", email " & _
            '      ", gender " & _
            '      ", identify_no " & _
            '      ", telephone_no " & _
            '      ", mobile_no " & _
            '      ", userunkid " & _
            '      ", isvoid " & _
            '      ", voiduserunkid " & _
            '      ", voiddatetime " & _
            '      ", voidreason " & _
            '  ", ISNULL(relationunkid,0) As relationunkid " & _
            '     "FROM hremployee_referee_tran " & _
            '     "WHERE refereetranunkid = @refereetranunkid "

            strQ = "SELECT " & _
              "  refereetranunkid " & _
              ", employeeunkid " & _
              ", name " & _
              ", address " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", email " & _
              ", gender " & _
              ", identify_no " & _
              ", telephone_no " & _
              ", mobile_no " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                    ", ISNULL(relationunkid,0) As relationunkid " & _
              ", ref_position " & _
             ", loginemployeeunkid " & _
             ", voidloginemployeeunkid " & _
             "FROM hremployee_referee_tran " & _
             "WHERE refereetranunkid = @refereetranunkid "

            'Pinkal (12-Oct-2011) -- End


            'S.SANDEEP [ 28 FEB 2012 ref_position ] -- START -- END



            objDataOperation.AddParameter("@refereetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefereetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintrefereetranunkid = CInt(dtRow.Item("refereetranunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrname = dtRow.Item("name").ToString
                mstraddress = dtRow.Item("address").ToString

                'Gajanan [21-June-2019] -- Start      
                'mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                'mintStateunkid = CInt(dtRow.Item("stateunkid"))
                'mintCityunkid = CInt(dtRow.Item("cityunkid"))

                If CInt(dtRow.Item("countryunkid")) = -1 Then
                    mintCountryunkid = 0
                Else
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                End If

                If CInt(dtRow.Item("stateunkid")) = -1 Then
                    mintStateunkid = 0
                Else
                mintstateunkid = CInt(dtRow.Item("stateunkid"))
                End If

                If CInt(dtRow.Item("cityunkid")) = -1 Then
                    mintCityunkid = 0
                Else
                mintcityunkid = CInt(dtRow.Item("cityunkid"))
                End If
                'Gajanan [21-June-2019] -- End

                mstremail = dtRow.Item("email").ToString
                mstrgender = dtRow.Item("gender").ToString
                mstridentify_no = dtRow.Item("identify_no").ToString
                mstrtelephone_no = dtRow.Item("telephone_no").ToString
                mstrmobile_no = dtRow.Item("mobile_no").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sandeep [ 17 DEC 2010 ] -- Start
                mintRelationunkid = CInt(dtRow.Item("relationunkid"))
                'Sandeep [ 17 DEC 2010 ] -- End 


                'Pinkal (12-Oct-2011) -- Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

                If Not IsDBNull(dtRow.Item("loginemployeeunkid")) Then
                    mintLoginEmployeeunkid = dtRow.Item("loginemployeeunkid")
                End If

                If Not IsDBNull(dtRow.Item("voidloginemployeeunkid")) Then
                    mintVoidloginEmployeeunkid = dtRow.Item("voidloginemployeeunkid")
                End If
                'Pinkal (12-Oct-2011) -- End

                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mstrRef_Position = dtRow.Item("ref_position").ToString
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal IsForImport As Boolean = False, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet

        'S.SANDEEP [20-JUN-2018] -- 'Enhancement - Implementing Employee Approver Flow For NMB .[Optional ByVal mblnAddApprovalCondition As Boolean = True]
        'Pinkal (28-Dec-2015) -- Start    'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'Pinkal (28-Dec-2015) -- Start    
            'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            'If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, "", mblnAddApprovalCondition)
            'S.SANDEEP [20-JUN-2018] -- End


            'Pinkal (28-Dec-2015) -- End

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       "    ,ISNULL(hremployee_referee_tran.name,'') AS RefName " & _
                       "    ,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                       "    ,ISNULL(hremployee_referee_tran.identify_no,'') AS Company " & _
                       "    ,ISNULL(hremployee_referee_tran.email,'') AS Email " & _
                       "    ,hremployee_master.employeeunkid AS EmpId " & _
                       "    ,hrmsConfiguration..cfcountry_master.countryunkid AS CountryId " & _
                       "    ,hremployee_referee_tran.refereetranunkid AS RefreeTranId " & _
                       "    ,hremployee_referee_tran.address " & _
                       "    ,hremployee_referee_tran.stateunkid " & _
                       "    ,hremployee_referee_tran.cityunkid " & _
                       "    ,CASE WHEN hremployee_referee_tran.gender = 1 THEN @MALE WHEN hremployee_referee_tran.gender = 2 THEN @FEMALE END AS gender " & _
                       "    ,hremployee_referee_tran.telephone_no " & _
                       "    ,hremployee_referee_tran.mobile_no " & _
                       "    ,hremployee_referee_tran.userunkid " & _
                       "    ,hremployee_referee_tran.isvoid " & _
                       "    ,hremployee_referee_tran.voiduserunkid " & _
                       "    ,hremployee_referee_tran.voiddatetime " & _
                       "    ,hremployee_referee_tran.voidreason " & _
                       "    ,ISNULL(hremployee_referee_tran.relationunkid,0) As relationunkid " & _
                       "    ,ref_position " & _
                       "    ,hremployee_referee_tran.loginemployeeunkid " & _
                       "    ,hremployee_referee_tran.voidloginemployeeunkid " & _
                       "    ,hremployee_referee_tran.gender AS genderunkid " & _
                       "    , '' AS AUD " & _
                       "    , '' AS GUID "
                'Sohail (25 Sep 2020) - [genderunkid, AUD, GUID]

                If IsForImport Then
                    StrQ &= "   ,hrmsConfiguration..cfstate_master.name as state " & _
                            "   ,hrmsConfiguration..cfcity_master.name as city " & _
                            "   ,cfcommon_master.name as relation "
                End If

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                StrQ &= ",0 AS operationtypeid " & _
                        ",'' AS OperationType "
                'Gajanan [17-DEC-2018] -- End


                StrQ &= " FROM hremployee_referee_tran " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                        "   LEFT JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid "


                If IsForImport Then
                    StrQ &= "   LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = hremployee_referee_tran.stateunkid " & _
                            "   LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = hremployee_referee_tran.cityunkid  " & _
                            "   LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid "
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If

                objDo.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDo.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal IsForImport As Boolean = False, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
    '    'Public Function GetList(ByVal strTableName As String, Optional ByVal IsForImport As Boolean = False) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        'Sandeep [ 17 DEC 2010 ] -- Start

    '        'strQ = "SELECT " & _
    '        '       " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '       ",ISNULL(hremployee_referee_tran.name,'') AS RefName " & _
    '        '       ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
    '        '       ",ISNULL(hremployee_referee_tran.identify_no,'') AS IdNo " & _
    '        '       ",ISNULL(hremployee_referee_tran.email,'') AS Email " & _
    '        '       ",hremployee_master.employeeunkid AS EmpId " & _
    '        '       ",hrmsConfiguration..cfcountry_master.countryunkid AS CountryId " & _
    '        '       ",hremployee_referee_tran.refereetranunkid AS RefreeTranId " & _
    '        '       ",hremployee_referee_tran.address " & _
    '        '       ",hremployee_referee_tran.stateunkid " & _
    '        '       ",hremployee_referee_tran.cityunkid " & _
    '        '       ",hremployee_referee_tran.gender " & _
    '        '       ",hremployee_referee_tran.telephone_no " & _
    '        '       ",hremployee_referee_tran.mobile_no " & _
    '        '       ",hremployee_referee_tran.userunkid " & _
    '        '       ",hremployee_referee_tran.isvoid " & _
    '        '       ",hremployee_referee_tran.voiduserunkid " & _
    '        '       ",hremployee_referee_tran.voiddatetime " & _
    '        '       ",hremployee_referee_tran.voidreason " & _
    '        '       ",ISNULL(hremployee_referee_tran.relationunkid,0) As relationunkid " & _
    '        '    "FROM hremployee_referee_tran " & _
    '        '       "LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
    '        '       "LEFT JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '    "WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0 "

    '        'Sandeep [ 17 DEC 2010 ] -- End 

    '        'Pinkal (10-Mar-2011) -- Start

    '        strQ = "SELECT " & _
    '                   " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                   ",ISNULL(hremployee_referee_tran.name,'') AS RefName " & _
    '                   ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
    '                ",ISNULL(hremployee_referee_tran.identify_no,'') AS Company " & _
    '                   ",ISNULL(hremployee_referee_tran.email,'') AS Email " & _
    '                   ",hremployee_master.employeeunkid AS EmpId " & _
    '                   ",hrmsConfiguration..cfcountry_master.countryunkid AS CountryId " & _
    '                   ",hremployee_referee_tran.refereetranunkid AS RefreeTranId " & _
    '               ",hremployee_referee_tran.address " & _
    '               ",hremployee_referee_tran.stateunkid " & _
    '               ",hremployee_referee_tran.cityunkid " & _
    '                   ",CASE WHEN hremployee_referee_tran.gender = 1 THEN @MALE  " & _
    '                   "      WHEN hremployee_referee_tran.gender = 2 THEN @FEMALE END AS gender " & _
    '               ",hremployee_referee_tran.telephone_no " & _
    '               ",hremployee_referee_tran.mobile_no " & _
    '               ",hremployee_referee_tran.userunkid " & _
    '               ",hremployee_referee_tran.isvoid " & _
    '               ",hremployee_referee_tran.voiduserunkid " & _
    '               ",hremployee_referee_tran.voiddatetime " & _
    '               ",hremployee_referee_tran.voidreason " & _
    '                ",ISNULL(hremployee_referee_tran.relationunkid,0) As relationunkid " & _
    '                ",ref_position "
    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        strQ &= ",ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid "
    '        'Anjan (21 Nov 2011)-End 

    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '        strQ &= ", hremployee_referee_tran.loginemployeeunkid " & _
    '                    ", hremployee_referee_tran.voidloginemployeeunkid "
    '        'Pinkal (12-Oct-2011) -- End


    '        If IsForImport Then

    '            strQ &= " , hrmsConfiguration..cfstate_master.name as state " & _
    '                        " , hrmsConfiguration..cfcity_master.name as city " & _
    '                        ", cfcommon_master.name as relation "

    '        End If


    '        strQ &= " FROM hremployee_referee_tran " & _
    '                              "LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
    '                              " LEFT JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid "


    '        If IsForImport Then

    '            strQ &= " LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = hremployee_referee_tran.stateunkid " & _
    '                        " LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = hremployee_referee_tran.cityunkid  " & _
    '                        " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid "
    '        End If

    '        strQ &= " WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0 "

    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        ''ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- END

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If

    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END




    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES

    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        '        End If
    '        'End Select

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END



    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Anjan (24 Jun 2011)-End 

    '        'Pinkal (10-Mar-2011) -- End

    '        objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
    '        objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    'S.SANDEEP [04 JUN 2015] -- END


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_referee_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Start {xDataOpr} -- End
        If isExist(mintEmployeeunkid, mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Referee is already defined. Please define new Referee.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End


        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGender.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentify_No.ToString)
            objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, 500, mstrIdentify_No.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephone_No.ToString)
            objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile_No.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginEmployeeunkid.ToString)

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@ref_position", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRef_Position.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END



            'strQ = "INSERT INTO hremployee_referee_tran ( " & _
            '          "  employeeunkid " & _
            '          ", name " & _
            '          ", address " & _
            '          ", countryunkid " & _
            '          ", stateunkid " & _
            '          ", cityunkid " & _
            '          ", email " & _
            '          ", gender " & _
            '          ", identify_no " & _
            '          ", telephone_no " & _
            '          ", mobile_no " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason" & _
            '            ", relationunkid" & _
            '        ") VALUES (" & _
            '          "  @employeeunkid " & _
            '          ", @name " & _
            '          ", @address " & _
            '          ", @countryunkid " & _
            '          ", @stateunkid " & _
            '          ", @cityunkid " & _
            '          ", @email " & _
            '          ", @gender " & _
            '          ", @identify_no " & _
            '          ", @telephone_no " & _
            '          ", @mobile_no " & _
            '          ", @userunkid " & _
            '          ", @isvoid " & _
            '          ", @voiduserunkid " & _
            '          ", @voiddatetime " & _
            '          ", @voidreason" & _
            '  ", @relationunkid" & _
            '        "); SELECT @@identity"


            strQ = "INSERT INTO hremployee_referee_tran ( " & _
              "  employeeunkid " & _
              ", name " & _
              ", address " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", email " & _
              ", gender " & _
              ", identify_no " & _
              ", telephone_no " & _
              ", mobile_no " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
                        ", relationunkid" & _
             ", loginemployeeunkid " & _
             ", voidloginemployeeunkid " & _
              ", ref_position" & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @name " & _
              ", @address " & _
              ", @countryunkid " & _
              ", @stateunkid " & _
              ", @cityunkid " & _
              ", @email " & _
              ", @gender " & _
              ", @identify_no " & _
              ", @telephone_no " & _
              ", @mobile_no " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
                        ", @relationunkid" & _
             ", @loginemployeeunkid " & _
             ", @voidloginemployeeunkid " & _
              ", @ref_position " & _
            "); SELECT @@identity"

            'Pinkal (12-Oct-2011) -- End



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintRefereetranunkid = dsList.Tables(0).Rows(0).Item(0)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hremployee_referee_tran", "refereetranunkid", mintRefereetranunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hremployee_referee_tran", "refereetranunkid", mintRefereetranunkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ''Pinkal (12-Oct-2011) -- Start
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Pinkal (12-Oct-2011) -- End
            'Gajanan [17-DEC-2018] -- End
            Return True
        Catch ex As Exception
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ''Pinkal (12-Oct-2011) -- Start
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'objDataOperation.ReleaseTransaction(False)
            ''Pinkal (12-Oct-2011) -- End
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'Gajanan [17-DEC-2018] -- End


            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_referee_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Start {xDataOpr} -- End
        If isExist(mintEmployeeunkid, mstrName, mintRefereetranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Referee is already defined. Please define new Referee.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE


        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End

        'Pinkal (12-Oct-2011) -- End



        Try
            objDataOperation.AddParameter("@refereetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefereetranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGender.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentify_No.ToString)
            objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, 500, mstrIdentify_No.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephone_No.ToString)
            objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile_No.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginEmployeeunkid.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@ref_position", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRef_Position.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END



            'strQ = "UPDATE hremployee_referee_tran SET " & _
            '       "  employeeunkid = @employeeunkid" & _
            '       ", name = @name" & _
            '       ", address = @address" & _
            '       ", countryunkid = @countryunkid" & _
            '       ", stateunkid = @stateunkid" & _
            '       ", cityunkid = @cityunkid" & _
            '       ", email = @email" & _
            '       ", gender = @gender" & _
            '       ", identify_no = @identify_no" & _
            '       ", telephone_no = @telephone_no" & _
            '       ", mobile_no = @mobile_no" & _
            '       ", userunkid = @userunkid" & _
            '       ", isvoid = @isvoid" & _
            '       ", voiduserunkid = @voiduserunkid" & _
            '       ", voiddatetime = @voiddatetime" & _
            '       ", voidreason = @voidreason " & _
            '       ", relationunkid = @relationunkid " & _
            '       "WHERE refereetranunkid = @refereetranunkid "

            strQ = "UPDATE hremployee_referee_tran SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", name = @name" & _
              ", address = @address" & _
              ", countryunkid = @countryunkid" & _
              ", stateunkid = @stateunkid" & _
              ", cityunkid = @cityunkid" & _
              ", email = @email" & _
              ", gender = @gender" & _
              ", identify_no = @identify_no" & _
              ", telephone_no = @telephone_no" & _
              ", mobile_no = @mobile_no" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
                   ", relationunkid = @relationunkid " & _
          ", loginemployeeunkid = @loginemployeeunkid " & _
          ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
              ", ref_position = @ref_position " & _
            "WHERE refereetranunkid = @refereetranunkid "

            'Pinkal (12-Oct-2011) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE



            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hremployee_referee_tran", mintRefereetranunkid, "refereetranunkid", 2, objDataOperation) Then


                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hremployee_referee_tran", "refereetranunkid", mintRefereetranunkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End


            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_referee_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Start {xDataOpr} -- End
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()


        Try


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'strQ = "DELETE FROM hremployee_referee_tran " & _
            '"WHERE refereetranunkid = @refereetranunkid "

            strQ = " Update hremployee_referee_tran set isvoid = 1," & _
                      " voiddatetime=@voiddatetime,voidreason=@voidreason "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@refereetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If mintUserunkid > 0 Then
                strQ &= ", voiduserunkid = @voiduserunkid "
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            ElseIf mintVoidloginEmployeeunkid > 0 Then
                strQ &= ", voidloginemployeeunkid = @voidloginemployeeunkid "
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginEmployeeunkid.ToString)

            End If

            strQ &= " where refereetranunkid = @refereetranunkid "

            'Pinkal (12-Oct-2011) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hremployee_referee_tran", "refereetranunkid", intUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hremployee_referee_tran", "refereetranunkid", intUnkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END





            'Pinkal (12-Oct-2011) -- End


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            'Gajanan [17-DEC-2018] -- End

            'Pinkal (12-Oct-2011) -- End
            If xDataOpr IsNot Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation.ReleaseTransaction(False)

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If

            'Gajanan [17-DEC-2018] -- End


            'Pinkal (12-Oct-2011) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@refereetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpId As Integer, ByVal strName As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Start {xDataOpr} -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        'objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End

        Try


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'strQ = "SELECT " & _
            '      "  refereetranunkid " & _
            '      ", employeeunkid " & _
            '      ", name " & _
            '      ", address " & _
            '      ", countryunkid " & _
            '      ", stateunkid " & _
            '      ", cityunkid " & _
            '      ", email " & _
            '      ", gender " & _
            '      ", identify_no " & _
            '      ", telephone_no " & _
            '      ", mobile_no " & _
            '      ", userunkid " & _
            '      ", isvoid " & _
            '      ", voiduserunkid " & _
            '      ", voiddatetime " & _
            '      ", voidreason " & _
            '    "FROM hremployee_referee_tran " & _
            '    "WHERE name = @name " & _
            '    "AND employeeunkid = @EmpId " & _
            '    "AND isvoid = 0 "


            strQ = "SELECT " & _
                      "  refereetranunkid " & _
                      ", employeeunkid " & _
                      ", name " & _
                      ", address " & _
                      ", countryunkid " & _
                      ", stateunkid " & _
                      ", cityunkid " & _
                      ", email " & _
                      ", gender " & _
                      ", identify_no " & _
                      ", telephone_no " & _
                      ", mobile_no " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                    "FROM hremployee_referee_tran " & _
                    "WHERE name = @name " & _
                    "AND employeeunkid = @EmpId " & _
                    "AND isvoid = 0 "

            'Pinkal (12-Oct-2011) -- End

            If intUnkid > 0 Then
                strQ &= " AND refereetranunkid <> @refereetranunkid"
            End If

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@refereetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetRefereeData_Export() As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known
            'Replace Column Name With Getmessage
            StrQ = "SELECT " & _
                      " ISNULL(hremployee_master.employeecode,'') AS  [" & Language.getMessage(mstrModuleName, 2, "ECODE") & "]" & _
                      ",ISNULL(hremployee_master.firstname,'') AS  [" & Language.getMessage(mstrModuleName, 3, "FIRSTNAME") & "]" & _
                      ",ISNULL(hremployee_master.surname,'') AS  [" & Language.getMessage(mstrModuleName, 4, "SURNAME") & "]" & _
                      ",ISNULL(hremployee_referee_tran.name,'') AS  [" & Language.getMessage(mstrModuleName, 5, "REFREE_NAME") & "]" & _
                      ",ISNULL(cfcommon_master.name,'') AS  [" & Language.getMessage(mstrModuleName, 6, "RELATION") & "]" & _
                      ",ISNULL(hremployee_referee_tran.address,'') AS  [" & Language.getMessage(mstrModuleName, 7, "ADDRESS") & "]" & _
                      ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS  [" & Language.getMessage(mstrModuleName, 8, "COUNTRY") & "]" & _
                      ",ISNULL(hrmsConfiguration..cfstate_master.name,'') AS  [" & Language.getMessage(mstrModuleName, 9, "STATE") & "]" & _
                      ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS  [" & Language.getMessage(mstrModuleName, 10, "CITY") & "]" & _
                      ",ISNULL(hremployee_referee_tran.email,'') AS  [" & Language.getMessage(mstrModuleName, 11, "EMAIL") & "]" & _
                      ",CASE WHEN hremployee_referee_tran.gender = 1 THEN @MALE " & _
                      "      WHEN hremployee_referee_tran.gender = 2 THEN @FEMALE " & _
                      " ELSE '' END AS  [" & Language.getMessage(mstrModuleName, 12, "GENDER") & "]" & _
                      ",ISNULL(hremployee_referee_tran.identify_no,'') AS  [" & Language.getMessage(mstrModuleName, 13, "COMPANY") & "]" & _
                      ",ISNULL(hremployee_referee_tran.telephone_no,'') AS  [" & Language.getMessage(mstrModuleName, 14, "TELEPHONE") & "]" & _
                      ",ISNULL(hremployee_referee_tran.mobile_no,'') AS  [" & Language.getMessage(mstrModuleName, 15, "MOBILE_NO") & "]" & _
                      ",ref_position AS  [" & Language.getMessage(mstrModuleName, 16, "POSITION") & "]" & _
                    "FROM hremployee_referee_tran " & _
                      "LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_referee_tran.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
                      "LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_referee_tran.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
                      "LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                      "LEFT JOIN cfcommon_master ON hremployee_referee_tran.relationunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
                      "JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE isvoid = 0 "
            'Gajanan (24 Nov 2018) -- End
            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRefereeData_Export ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetRefereeTranId(ByVal intEmpId As Integer, ByVal StrRefName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   " refereetranunkid " & _
                   "FROM hremployee_referee_tran " & _
                   "WHERE employeeunkid = @EmpId AND LTRIM(RTRIM(NAME)) LIKE LTRIM(RTRIM(@RName)) " & _
                   "AND isvoid = 0 "

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@RName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrRefName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("refereetranunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRefereeTranId ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Referee is already defined. Please define new Referee.")
            Language.setMessage(mstrModuleName, 2, "ECODE")
            Language.setMessage(mstrModuleName, 3, "FIRSTNAME")
            Language.setMessage(mstrModuleName, 4, "SURNAME")
            Language.setMessage(mstrModuleName, 5, "REFREE_NAME")
            Language.setMessage(mstrModuleName, 6, "RELATION")
            Language.setMessage(mstrModuleName, 7, "ADDRESS")
            Language.setMessage(mstrModuleName, 8, "COUNTRY")
            Language.setMessage(mstrModuleName, 9, "STATE")
            Language.setMessage(mstrModuleName, 10, "CITY")
            Language.setMessage(mstrModuleName, 11, "EMAIL")
            Language.setMessage(mstrModuleName, 12, "GENDER")
            Language.setMessage(mstrModuleName, 13, "COMPANY")
            Language.setMessage(mstrModuleName, 14, "TELEPHONE")
            Language.setMessage(mstrModuleName, 15, "MOBILE_NO")
            Language.setMessage(mstrModuleName, 16, "POSITION")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class