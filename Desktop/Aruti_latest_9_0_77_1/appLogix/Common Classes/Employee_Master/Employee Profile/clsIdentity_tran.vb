﻿'************************************************************************************************************************************
'Class Name : clsIdentity_tran.vb
'Purpose    :
'Date       :30/06/2010
'Written By :Sandeep J. Sharma
'Modified   : Sandeep -- > 04 JUN 2015
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsIdentity_tran

#Region " Private Variables "

    Private Const mstrModuleName = "clsIdentity_tran"
    Private mstrMessage As String = ""
    Private mintEmployeeUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mintIdentityTranId As Integer = 0
    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Private xDataOperation As clsDataOperation = Nothing
    'S.SANDEEP [19 OCT 2016] -- END

    'S.SANDEEP |26-APR-2019| -- START
    Dim objDocument As New clsScan_Attach_Documents
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public Property _EmployeeUnkid() As Integer
        Get
            Return mintEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
            Call GetIdentity_tran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Public WriteOnly Property _xDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOperation = value
        End Set
    End Property
    'S.SANDEEP [19 OCT 2016] -- END


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Public ReadOnly Property _IdentityTranId() As Integer
        Get
            Return mintIdentityTranId
        End Get
    End Property

    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property
    'Gajanan [22-Feb-2019] -- End

#End Region

#Region " Contructor "
    Public Sub New()
        mdtTran = New DataTable("IdentityTran")
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("identitytranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("idtypeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("identity_no")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("serial_no")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("issued_place")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dl_class")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.

            dCol = New DataColumn("issue_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("expiry_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("issue_date")
            'dCol.DataType = System.Type.GetType("System.String")
            'mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("expiry_date")
            'dCol.DataType = System.Type.GetType("System.String")
            'mdtTran.Columns.Add(dCol)
            'Pinkal (16-Apr-2016) -- End

            dCol = New DataColumn("isdefault")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES : Changes for WEB
            dCol = New DataColumn("identities")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("country")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

        Catch ex As IndexOutOfRangeException

        Catch ex As OutOfMemoryException

        Catch ex As AccessViolationException

        Catch ex As Exception
            'Sohail (22 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            'DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
            'Sohail (22 Oct 2013) -- End
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub GetIdentity_tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception
        Try

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOperation IsNot Nothing Then
                objDataOperation = xDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'S.SANDEEP [19 OCT 2016] -- END


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            '",hremployee_idinfo_tran.issue_date " & _
            ',hremployee_idinfo_tran.expiry_date  " & _
            'Pinkal (16-Apr-2016) -- End


            strQ = "SELECT hremployee_idinfo_tran.identitytranunkid " & _
                   ",hremployee_idinfo_tran.employeeunkid " & _
                   ",hremployee_idinfo_tran.idtypeunkid " & _
                   ",hremployee_idinfo_tran.identity_no " & _
                   ",hremployee_idinfo_tran.countryunkid " & _
                   ",hremployee_idinfo_tran.serial_no " & _
                   ",hremployee_idinfo_tran.issued_place " & _
                   ",hremployee_idinfo_tran.dl_class " & _
                   ",hremployee_idinfo_tran.issue_date " & _
                   ",hremployee_idinfo_tran.expiry_date " & _
                   ",hremployee_idinfo_tran.isdefault " & _
                    "      ,'' As AUD " & _
                   ",ISNULL(cfcommon_master.name,'') AS identities " & _
                   ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS country " & _
                    "FROM hremployee_idinfo_tran " & _
                   "LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_idinfo_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                   "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid AND mastertype ='" & clsCommon_Master.enCommonMaster.IDENTITY_TYPES & "' " & _
                    "WHERE employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow()

                    dRowID_Tran.Item("identitytranunkid") = .Item("identitytranunkid")
                    dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
                    dRowID_Tran.Item("idtypeunkid") = .Item("idtypeunkid")
                    dRowID_Tran.Item("identity_no") = .Item("identity_no")
                    dRowID_Tran.Item("countryunkid") = .Item("countryunkid")
                    dRowID_Tran.Item("serial_no") = .Item("serial_no")
                    dRowID_Tran.Item("issued_place") = .Item("issued_place")
                    dRowID_Tran.Item("dl_class") = .Item("dl_class")

                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    dRowID_Tran.Item("issue_date") = .Item("issue_date")
                    dRowID_Tran.Item("expiry_date") = .Item("expiry_date")
                    'dRowID_Tran.Item("issue_date") = eZeeDate.convertDate(.Item("issue_date").ToString()).Date
                    'dRowID_Tran.Item("expiry_date") = eZeeDate.convertDate(.Item("expiry_date").ToString()).Date 
                    'Pinkal (16-Apr-2016) -- End

                    
                    dRowID_Tran.Item("isdefault") = .Item("isdefault")
                    dRowID_Tran.Item("AUD") = .Item("AUD")
                    dRowID_Tran.Item("identities") = .Item("identities")
                    dRowID_Tran.Item("country") = .Item("country")

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetIdentity_tran", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function ImportEmployeeIdentity(ByVal intUserUnkId As Integer) As Boolean
        Dim blnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.BindTransaction()

            blnFlag = InsertUpdateDelete_IdentityTran(objDataOperation, intUserUnkId)

            objDataOperation.ReleaseTransaction(True)
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ImportEmployeeIdentity; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return blnFlag
    End Function
    'S.SANDEEP [04 JUN 2015] -- END


    'Pinkal (28-Dec-2015) -- Start
    'Enhancement - Working on Changes in SS for Employee Master.

    'Public Function InsertUpdateDelete_IdentityTran(ByVal objDataOperation As clsDataOperation, Optional ByVal intUserUnkId As Integer = 0) As Boolean
    '    Dim i As Integer
    '    Dim strQ As String = ""
    '    Dim strErrorMessage As String = ""
    '    Dim exForce As Exception
    '    Try
    '        For i = 0 To mdtTran.Rows.Count - 1
    '            With mdtTran.Rows(i)
    '                objDataOperation.ClearParameters()
    '                If Not IsDBNull(.Item("AUD")) Then
    '                    Select Case .Item("AUD")
    '                        Case "A"
    '                            strQ = "INSERT INTO hremployee_idinfo_tran ( " & _
    '                                        "  employeeunkid " & _
    '                                        ", idtypeunkid " & _
    '                                        ", identity_no " & _
    '                                        ", countryunkid " & _
    '                                        ", serial_no " & _
    '                                        ", issued_place " & _
    '                                        ", dl_class " & _
    '                                        ", issue_date " & _
    '                                        ", expiry_date " & _
    '                                        ", isdefault" & _
    '                                    ") VALUES (" & _
    '                                        "  @employeeunkid " & _
    '                                        ", @idtypeunkid " & _
    '                                        ", @identity_no " & _
    '                                        ", @countryunkid " & _
    '                                        ", @serial_no " & _
    '                                        ", @issued_place " & _
    '                                        ", @dl_class " & _
    '                                        ", @issue_date " & _
    '                                        ", @expiry_date " & _
    '                                        ", @isdefault" & _
    '                                      "); SELECT @@identity"

    '                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
    '                            objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("idtypeunkid").ToString)
    '                            objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("identity_no").ToString)
    '                            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
    '                            objDataOperation.AddParameter("@serial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("serial_no").ToString)
    '                            objDataOperation.AddParameter("@issued_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("issued_place").ToString)
    '                            objDataOperation.AddParameter("@dl_class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("dl_class").ToString)
    '                            If .Item("issue_date").ToString = Nothing Then
    '                                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '                            Else
    '                                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date").ToString)
    '                            End If
    '                            If .Item("expiry_date").ToString = Nothing Then
    '                                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '                            Else
    '                                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date").ToString)
    '                            End If

    '                            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdefault").ToString)

    '                            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                            mintIdentityTranId = dsList.Tables(0).Rows(0)(0)

    '                            If .Item("employeeunkid") > 0 Then
    '                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", mintIdentityTranId, 2, 1, , intUserUnkId) = False Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If
    '                            Else
    '                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeUnkid, "hremployee_idinfo_tran", "identitytranunkid", mintIdentityTranId, 1, 1, , intUserUnkId) = False Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If
    '                            End If


    '                        Case "U"
    '                            strQ = "UPDATE hremployee_idinfo_tran SET " & _
    '                                    "  employeeunkid = @employeeunkid" & _
    '                                    ", idtypeunkid = @idtypeunkid" & _
    '                                    ", identity_no = @identity_no" & _
    '                                    ", countryunkid = @countryunkid" & _
    '                                    ", serial_no = @serial_no" & _
    '                                    ", issued_place = @issued_place" & _
    '                                    ", dl_class = @dl_class" & _
    '                                    ", issue_date = @issue_date" & _
    '                                    ", expiry_date = @expiry_date" & _
    '                                    ", isdefault = @isdefault " & _
    '                                  "WHERE identitytranunkid = @identitytranunkid "

    '                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
    '                            objDataOperation.AddParameter("@identitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("identitytranunkid").ToString)
    '                            objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("idtypeunkid").ToString)
    '                            objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("identity_no").ToString)
    '                            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
    '                            objDataOperation.AddParameter("@serial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("serial_no").ToString)
    '                            objDataOperation.AddParameter("@issued_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("issued_place").ToString)
    '                            objDataOperation.AddParameter("@dl_class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("dl_class").ToString)
    '                            If .Item("issue_date").ToString = Nothing Then
    '                                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '                            Else
    '                                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date").ToString)
    '                            End If
    '                            If .Item("expiry_date").ToString = Nothing Then
    '                                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '                            Else
    '                                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date").ToString)
    '                            End If

    '                            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdefault").ToString)

    '                            objDataOperation.ExecNonQuery(strQ)

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 2, , intUserUnkId) = False Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If
    '                        Case "D"
    '                            If .Item("identitytranunkid") > 0 Then
    '                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 3, , intUserUnkId) = False Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If
    '                            End If

    '                            strQ = "DELETE FROM hremployee_idinfo_tran " & _
    '                                        "WHERE identitytranunkid = @identitytranunkid "

    '                            objDataOperation.AddParameter("@identitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("identitytranunkid").ToString)

    '                            Call objDataOperation.ExecNonQuery(strQ)

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If
    '                    End Select
    '                End If
    '            End With
    '        Next
    '        objDataOperation.ReleaseTransaction(True)
    '        Return True

    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_IdentityTran", mstrModuleName)
    '        Return False
    '    End Try
    'End Function


    'S.SANDEEP |26-APR-2019| -- START
    Public Function InsertUpdateDelete_IdentityTran(Optional ByVal objOperation As clsDataOperation = Nothing, Optional ByVal intUserUnkId As Integer = 0, Optional ByVal dtDocument As DataTable = Nothing) As Boolean
        'Public Function InsertUpdateDelete_IdentityTran(Optional ByVal objOperation As clsDataOperation = Nothing, Optional ByVal intUserUnkId As Integer = 0) As Boolean
        'S.SANDEEP |26-APR-2019| -- END
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            If objOperation Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = objOperation
            End If

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)

                    'S.SANDEEP [27-Apr-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002217|#ARUTI-131}
                    Dim blnMakeDeafault As Boolean = True
                    If IsDefaultAssigned(mintEmployeeUnkid, objDataOperation) Then
                        blnMakeDeafault = False
                    End If
                    'S.SANDEEP [27-Apr-2018] -- END

                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hremployee_idinfo_tran ( " & _
                                            "  employeeunkid " & _
                                            ", idtypeunkid " & _
                                            ", identity_no " & _
                                            ", countryunkid " & _
                                            ", serial_no " & _
                                            ", issued_place " & _
                                            ", dl_class " & _
                                            ", issue_date " & _
                                            ", expiry_date " & _
                                            ", isdefault" & _
                                        ") VALUES (" & _
                                            "  @employeeunkid " & _
                                            ", @idtypeunkid " & _
                                            ", @identity_no " & _
                                            ", @countryunkid " & _
                                            ", @serial_no " & _
                                            ", @issued_place " & _
                                            ", @dl_class " & _
                                            ", @issue_date " & _
                                            ", @expiry_date " & _
                                            ", @isdefault" & _
                                          "); SELECT @@identity"

                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
                                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("idtypeunkid").ToString)
                                objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("identity_no").ToString)
                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
                                objDataOperation.AddParameter("@serial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("serial_no").ToString)
                                objDataOperation.AddParameter("@issued_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("issued_place").ToString)
                                objDataOperation.AddParameter("@dl_class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("dl_class").ToString)
                                If .Item("issue_date").ToString = Nothing Then
                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date").ToString)
                                End If
                                If .Item("expiry_date").ToString = Nothing Then
                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date").ToString)
                                End If

                                'S.SANDEEP [27-Apr-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002217|#ARUTI-131}
                                'objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdefault").ToString)
                                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnMakeDeafault)
                                'S.SANDEEP [27-Apr-2018] -- END

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintIdentityTranId = dsList.Tables(0).Rows(0)(0)

                                If .Item("employeeunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", mintIdentityTranId, 2, 1, , intUserUnkId) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeUnkid, "hremployee_idinfo_tran", "identitytranunkid", mintIdentityTranId, 1, 1, , intUserUnkId) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                                'S.SANDEEP |26-APR-2019| -- START
                                If SaveDocument(mdtTran.Rows(i), dtDocument, mintIdentityTranId, objDataOperation) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP |26-APR-2019| -- END

                            Case "U"
                                strQ = "UPDATE hremployee_idinfo_tran SET " & _
                                        "  employeeunkid = @employeeunkid" & _
                                        ", idtypeunkid = @idtypeunkid" & _
                                        ", identity_no = @identity_no" & _
                                        ", countryunkid = @countryunkid" & _
                                        ", serial_no = @serial_no" & _
                                        ", issued_place = @issued_place" & _
                                        ", dl_class = @dl_class" & _
                                        ", issue_date = @issue_date" & _
                                        ", expiry_date = @expiry_date" & _
                                        ", isdefault = @isdefault " & _
                                      "WHERE identitytranunkid = @identitytranunkid "

                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@identitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("identitytranunkid").ToString)
                                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("idtypeunkid").ToString)
                                objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("identity_no").ToString)
                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
                                objDataOperation.AddParameter("@serial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("serial_no").ToString)
                                objDataOperation.AddParameter("@issued_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("issued_place").ToString)
                                objDataOperation.AddParameter("@dl_class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("dl_class").ToString)
                                If .Item("issue_date").ToString = Nothing Then
                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date").ToString)
                                End If
                                If .Item("expiry_date").ToString = Nothing Then
                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date").ToString)
                                End If

                                'S.SANDEEP [27-Apr-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002217|#ARUTI-131}
                                'objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdefault").ToString)
                                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnMakeDeafault)
                                'S.SANDEEP [27-Apr-2018] -- END

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 2, , intUserUnkId) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP |26-APR-2019| -- START
                                If SaveDocument(mdtTran.Rows(i), dtDocument, .Item("identitytranunkid"), objDataOperation) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP |26-APR-2019| -- END

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Case "D"
                                If .Item("identitytranunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 3, , intUserUnkId) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                'S.SANDEEP |26-APR-2019| -- START
                                If SaveDocument(mdtTran.Rows(i), dtDocument, .Item("identitytranunkid"), objDataOperation) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP |26-APR-2019| -- END

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                                strQ = "DELETE FROM hremployee_idinfo_tran " & _
                                            "WHERE identitytranunkid = @identitytranunkid "

                                objDataOperation.AddParameter("@identitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("identitytranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            If objOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            If objOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_IdentityTran; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function


    'Pinkal (28-Dec-2015) -- End

    Public Function GetEmployeeIdentityUnkid(ByVal intEmployeeId As Integer, ByVal intIdentyTypeID As Integer, ByVal mstrIdentityNo As String) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation


            'Gajanan [27-May-2019] -- Start              

            'StrQ = "SELECT " & _
            '           "	hremployee_idinfo_tran.identitytranunkid " & _
            '           "FROM hremployee_idinfo_tran " & _
            '           "WHERE employeeunkid = @EmpId AND idtypeunkid = @idtypeunkid AND identity_no  = @identity_no "

            StrQ = "SELECT " & _
                       "	hremployee_idinfo_tran.identitytranunkid " & _
                       "FROM hremployee_idinfo_tran " & _
                   "WHERE employeeunkid = @EmpId AND idtypeunkid = @idtypeunkid "
            'Gajanan [27-May-2019] -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intIdentyTypeID)
            objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentityNo)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("identitytranunkid")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeIdentityUnkid", mstrModuleName)
        End Try
    End Function

    Public Function GetIdentity_tranList(Optional ByVal strTableName As String = "List", Optional ByVal intEmpUnkId As Integer = 0, Optional ByVal intIDTypeUnkId As Integer = 0, Optional ByVal intIdTranUnkid As Integer = -1) As DataSet
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT identitytranunkid " & _
                    "      ,employeeunkid " & _
                    "      ,idtypeunkid " & _
                    "      ,identity_no " & _
                    "      ,countryunkid " & _
                    "      ,serial_no " & _
                    "      ,issued_place " & _
                    "      ,dl_class " & _
                    "      ,issue_date " & _
                    "      ,expiry_date " & _
                    "      ,isdefault " & _
                    "      ,'' As AUD " & _
                    "FROM hremployee_idinfo_tran " & _
                    "WHERE 1 = 1 "

            If intEmpUnkId > 0 Then
                strQ &= "AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkId.ToString)
            End If

            If intIDTypeUnkId > 0 Then
                strQ &= "AND idtypeunkid = @idtypeunkid "
                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intIDTypeUnkId.ToString)
            End If

            If intIdTranUnkid > 0 Then
                strQ &= "AND identitytranunkid = '" & intIdTranUnkid & "'"
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetIdentity_tranList", mstrModuleName)
        End Try
        Return dsList
    End Function

    'S.SANDEEP [27-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002217|#ARUTI-131}
    Public Function IsDefaultAssigned(ByVal intEmployeeId As Integer, ByVal objDo As clsDataOperation) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Try
            StrQ = "SELECT 1 FROM hremployee_idinfo_tran WHERE employeeunkid = '" & intEmployeeId & "' AND isdefault = 1 "

            If objDo.RecordCount(StrQ) > 0 Then
                blnFlag = True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsDefaultAssigned; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function
    'S.SANDEEP [27-Apr-2018] -- END


    'S.SANDEEP |26-APR-2019| -- START
    Public Function SaveDocument(ByVal dr As DataRow, ByVal dScan As DataTable, ByVal intIdTranId As Integer, ByVal objDataOperation As clsDataOperation)
        Try
            If dScan IsNot Nothing Then
                Dim fData As DataTable = Nothing
                Select Case dr("AUD").ToString
                    Case "A"
                        fData = dScan.Select("pguid = '" & dr("guid").ToString() & "'").CopyToDataTable()
                    Case "U"
                        fData = dScan.Select("transactionunkid = '" & intIdTranId & "'").CopyToDataTable()
                    Case "D"
                        fData = dScan.Select("transactionunkid = '" & intIdTranId & "'").CopyToDataTable()
                        If fData.Columns.Contains("AUD") Then
                            fData.Columns.Remove("AUD")
                            Dim dCol As New DataColumn
                            With dCol
                                .DataType = GetType(System.String)
                                .ColumnName = "AUD"
                                .DefaultValue = "D"
                            End With
                            fData.Columns.Add(dCol)
                        End If
                End Select
                objDocument._Datatable = fData
                objDocument.InsertUpdateDelete_Documents(objDataOperation)
                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
            End If
            
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveDocument; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function
    'S.SANDEEP |26-APR-2019| -- END

#End Region

End Class

'Public Class clsIdentity_tran

'#Region " Private Variables "
'    Private Const mstrModuleName = "clsIdentity_tran"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""
'    Private mintEmployeeUnkid As Integer = -1
'    Private mdtTran As DataTable

'    'S.SANDEEP [ 12 OCT 2011 ] -- START
'    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'    Private mintIdentityTranId As Integer = 0
'    'S.SANDEEP [ 12 OCT 2011 ] -- END 

'#End Region

'#Region " Properties "
'    Public Property _EmployeeUnkid() As Integer
'        Get
'            Return mintEmployeeUnkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeUnkid = value
'            Call GetIdentity_tran()
'        End Set
'    End Property

'    Public Property _DataList() As DataTable
'        Get
'            Return mdtTran
'        End Get
'        Set(ByVal value As DataTable)
'            mdtTran = value
'        End Set
'    End Property
'#End Region

'#Region " Contructor "
'    Public Sub New()
'        mdtTran = New DataTable("IdentityTran")
'        Dim dCol As DataColumn
'        Try

'            dCol = New DataColumn("identitytranunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("employeeunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("idtypeunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("identity_no")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("countryunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("serial_no")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("issued_place")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("dl_class")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("issue_date")
'            dCol.DataType = System.Type.GetType("System.DateTime")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("expiry_date")
'            dCol.DataType = System.Type.GetType("System.DateTime")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("isdefault")
'            dCol.DataType = System.Type.GetType("System.Boolean")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("AUD")
'            dCol.DataType = System.Type.GetType("System.String")
'            dCol.AllowDBNull = True
'            dCol.DefaultValue = DBNull.Value
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("GUID")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES : Changes for WEB
'            dCol = New DataColumn("identities")
'            dCol.DataType = System.Type.GetType("System.String")
'            dCol.DefaultValue = ""
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("country")
'            dCol.DataType = System.Type.GetType("System.String")
'            dCol.DefaultValue = ""
'            mdtTran.Columns.Add(dCol)
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END

'        Catch ex As IndexOutOfRangeException

'        Catch ex As OutOfMemoryException

'        Catch ex As AccessViolationException

'        Catch ex As Exception
'            'Sohail (22 Oct 2013) -- Start
'            'TRA - ENHANCEMENT
'            'DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
'            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
'            'Sohail (22 Oct 2013) -- End
'        End Try
'    End Sub
'#End Region

'#Region " Private Methods "
'    Private Sub GetIdentity_tran()
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim dsList As New DataSet
'        Dim dRowID_Tran As DataRow
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation


'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES : Changes for WEB
'            'strQ = "SELECT identitytranunkid " & _
'            '       "      ,employeeunkid " & _
'            '       "      ,idtypeunkid " & _
'            '       "      ,identity_no " & _
'            '       "      ,countryunkid " & _
'            '       "      ,serial_no " & _
'            '       "      ,issued_place " & _
'            '       "      ,dl_class " & _
'            '       "      ,issue_date " & _
'            '       "      ,expiry_date " & _
'            '       "      ,isdefault " & _
'            '       "      ,'' As AUD " & _
'            '       "FROM hremployee_idinfo_tran " & _
'            '       "WHERE employeeunkid = @employeeunkid "
'            strQ = "SELECT hremployee_idinfo_tran.identitytranunkid " & _
'                   ",hremployee_idinfo_tran.employeeunkid " & _
'                   ",hremployee_idinfo_tran.idtypeunkid " & _
'                   ",hremployee_idinfo_tran.identity_no " & _
'                   ",hremployee_idinfo_tran.countryunkid " & _
'                   ",hremployee_idinfo_tran.serial_no " & _
'                   ",hremployee_idinfo_tran.issued_place " & _
'                   ",hremployee_idinfo_tran.dl_class " & _
'                   ",hremployee_idinfo_tran.issue_date " & _
'                   ",hremployee_idinfo_tran.expiry_date " & _
'                   ",hremployee_idinfo_tran.isdefault " & _
'                    "      ,'' As AUD " & _
'                   ",ISNULL(cfcommon_master.name,'') AS identities " & _
'                   ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS country " & _
'                    "FROM hremployee_idinfo_tran " & _
'                   "LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_idinfo_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
'                   "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid AND mastertype ='" & clsCommon_Master.enCommonMaster.IDENTITY_TYPES & "' " & _
'                    "WHERE employeeunkid = @employeeunkid "
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END




'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mdtTran.Clear()

'            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
'                With dsList.Tables("List").Rows(i)
'                    dRowID_Tran = mdtTran.NewRow()

'                    dRowID_Tran.Item("identitytranunkid") = .Item("identitytranunkid")
'                    dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
'                    dRowID_Tran.Item("idtypeunkid") = .Item("idtypeunkid")
'                    dRowID_Tran.Item("identity_no") = .Item("identity_no")
'                    dRowID_Tran.Item("countryunkid") = .Item("countryunkid")
'                    dRowID_Tran.Item("serial_no") = .Item("serial_no")
'                    dRowID_Tran.Item("issued_place") = .Item("issued_place")
'                    dRowID_Tran.Item("dl_class") = .Item("dl_class")
'                    dRowID_Tran.Item("issue_date") = .Item("issue_date")
'                    dRowID_Tran.Item("expiry_date") = .Item("expiry_date")
'                    dRowID_Tran.Item("isdefault") = .Item("isdefault")
'                    dRowID_Tran.Item("AUD") = .Item("AUD")
'                    'S.SANDEEP [ 05 MARCH 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    dRowID_Tran.Item("identities") = .Item("identities")
'                    dRowID_Tran.Item("country") = .Item("country")
'                    'S.SANDEEP [ 05 MARCH 2012 ] -- END

'                    mdtTran.Rows.Add(dRowID_Tran)
'                End With
'            Next


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetIdentity_tran", mstrModuleName)
'        End Try
'    End Sub

'    Public Function InsertUpdateDelete_IdentityTran(Optional ByVal intUserUnkId As Integer = 0) As Boolean 'Sohail (23 Apr 2012) - [intUserUnkId]
'        Dim i As Integer
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()
'            For i = 0 To mdtTran.Rows.Count - 1
'                With mdtTran.Rows(i)
'                    objDataOperation.ClearParameters()
'                    If Not IsDBNull(.Item("AUD")) Then
'                        Select Case .Item("AUD")
'                            Case "A"
'                                strQ = "INSERT INTO hremployee_idinfo_tran ( " & _
'                                            "  employeeunkid " & _
'                                            ", idtypeunkid " & _
'                                            ", identity_no " & _
'                                            ", countryunkid " & _
'                                            ", serial_no " & _
'                                            ", issued_place " & _
'                                            ", dl_class " & _
'                                            ", issue_date " & _
'                                            ", expiry_date " & _
'                                            ", isdefault" & _
'                                        ") VALUES (" & _
'                                            "  @employeeunkid " & _
'                                            ", @idtypeunkid " & _
'                                            ", @identity_no " & _
'                                            ", @countryunkid " & _
'                                            ", @serial_no " & _
'                                            ", @issued_place " & _
'                                            ", @dl_class " & _
'                                            ", @issue_date " & _
'                                            ", @expiry_date " & _
'                                            ", @isdefault" & _
'                                          "); SELECT @@identity"

'                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
'                                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("idtypeunkid").ToString)
'                                objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("identity_no").ToString)
'                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
'                                objDataOperation.AddParameter("@serial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("serial_no").ToString)
'                                objDataOperation.AddParameter("@issued_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("issued_place").ToString)
'                                objDataOperation.AddParameter("@dl_class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("dl_class").ToString)
'                                If .Item("issue_date").ToString = Nothing Then
'                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                Else
'                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date").ToString)
'                                End If
'                                If .Item("expiry_date").ToString = Nothing Then
'                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                Else
'                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date").ToString)
'                                End If

'                                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdefault").ToString)

'                                'S.SANDEEP [ 12 OCT 2011 ] -- START
'                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'                                'objDataOperation.ExecNonQuery(strQ)
'                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
'                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                'S.SANDEEP [ 12 OCT 2011 ] -- START
'                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'                                mintIdentityTranId = dsList.Tables(0).Rows(0)(0)

'                                If .Item("employeeunkid") > 0 Then
'                                    'Sohail (23 Apr 2012) -- Start
'                                    'TRA - ENHANCEMENT
'                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", mintIdentityTranId, 2, 1, , intUserUnkId) = False Then
'                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", mintIdentityTranId, 2, 1) = False Then
'                                        'Sohail (23 Apr 2012) -- End
'                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                        Throw exForce
'                                    End If
'                                Else
'                                    'Sohail (23 Apr 2012) -- Start
'                                    'TRA - ENHANCEMENT
'                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeUnkid, "hremployee_idinfo_tran", "identitytranunkid", mintIdentityTranId, 1, 1, , intUserUnkId) = False Then
'                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeUnkid, "hremployee_idinfo_tran", "identitytranunkid", mintIdentityTranId, 1, 1) = False Then
'                                        'Sohail (23 Apr 2012) -- End
'                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                        Throw exForce
'                                    End If
'                                End If
'                                'S.SANDEEP [ 12 OCT 2011 ] -- END 


'                            Case "U"
'                                strQ = "UPDATE hremployee_idinfo_tran SET " & _
'                                        "  employeeunkid = @employeeunkid" & _
'                                        ", idtypeunkid = @idtypeunkid" & _
'                                        ", identity_no = @identity_no" & _
'                                        ", countryunkid = @countryunkid" & _
'                                        ", serial_no = @serial_no" & _
'                                        ", issued_place = @issued_place" & _
'                                        ", dl_class = @dl_class" & _
'                                        ", issue_date = @issue_date" & _
'                                        ", expiry_date = @expiry_date" & _
'                                        ", isdefault = @isdefault " & _
'                                      "WHERE identitytranunkid = @identitytranunkid "

'                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
'                                objDataOperation.AddParameter("@identitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("identitytranunkid").ToString)
'                                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("idtypeunkid").ToString)
'                                objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("identity_no").ToString)
'                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
'                                objDataOperation.AddParameter("@serial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("serial_no").ToString)
'                                objDataOperation.AddParameter("@issued_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("issued_place").ToString)
'                                objDataOperation.AddParameter("@dl_class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("dl_class").ToString)
'                                If .Item("issue_date").ToString = Nothing Then
'                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                Else
'                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date").ToString)
'                                End If
'                                If .Item("expiry_date").ToString = Nothing Then
'                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                Else
'                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date").ToString)
'                                End If

'                                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdefault").ToString)

'                                objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                'S.SANDEEP [ 12 OCT 2011 ] -- START
'                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'                                'Sohail (23 Apr 2012) -- Start
'                                'TRA - ENHANCEMENT
'                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 2, , intUserUnkId) = False Then
'                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 2) = False Then
'                                    'Sohail (23 Apr 2012) -- End
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If
'                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

'                            Case "D"

'                                'S.SANDEEP [ 12 OCT 2011 ] -- START
'                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'                                If .Item("identitytranunkid") > 0 Then

'                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "jobskilltranunkid", .Item("identitytranunkid"), 2, 3) = False Then
'                                    'Sohail (23 Apr 2012) -- Start
'                                    'TRA - ENHANCEMENT
'                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 3, , intUserUnkId) = False Then
'                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 3) = False Then
'                                        'Sohail (23 Apr 2012) -- End
'                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                        Throw exForce
'                                    End If
'                                End If
'                                'S.SANDEEP [ 12 OCT 2011 ] -- END

'                                strQ = "DELETE FROM hremployee_idinfo_tran " & _
'                                            "WHERE identitytranunkid = @identitytranunkid "

'                                objDataOperation.AddParameter("@identitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("identitytranunkid").ToString)

'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If
'                        End Select
'                    End If
'                End With
'            Next
'            objDataOperation.ReleaseTransaction(True)
'            Return True

'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_IdentityTran", mstrModuleName)
'            Return False
'        End Try
'    End Function

'    'Pinkal (18-Aug-2013) -- Start
'    'Enhancement : TRA Changes

'    Public Function GetEmployeeIdentityUnkid(ByVal intEmployeeId As Integer, ByVal intIdentyTypeID As Integer, ByVal mstrIdentityNo As String) As Integer
'        Dim StrQ As String = String.Empty
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation

'            StrQ = "SELECT " & _
'                       "	hremployee_idinfo_tran.identitytranunkid " & _
'                       "FROM hremployee_idinfo_tran " & _
'                       "WHERE employeeunkid = @EmpId AND idtypeunkid = @idtypeunkid AND identity_no  = @identity_no "

'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
'            objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intIdentyTypeID)
'            objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentityNo)

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                Return dsList.Tables(0).Rows(0)("identitytranunkid")
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetEmployeeIdentityUnkid", mstrModuleName)
'        End Try
'    End Function

'    'Pinkal (18-Aug-2013) -- End

'#End Region

'    ' Pinkal (22-Dec-2010) -- Start

'    'Sohail (14 Nov 2011) -- Start


'    'S.SANDEEP [ 05 MARCH 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'Public Function GetIdentity_tranList(Optional ByVal strTableName As String = "List", Optional ByVal intEmpUnkId As Integer = 0, Optional ByVal intIDTypeUnkId As Integer = 0) As DataSet
'    Public Function GetIdentity_tranList(Optional ByVal strTableName As String = "List", Optional ByVal intEmpUnkId As Integer = 0, Optional ByVal intIDTypeUnkId As Integer = 0, Optional ByVal intIdTranUnkid As Integer = -1) As DataSet
'        'S.SANDEEP [ 05 MARCH 2012 ] -- END

'        'Public Function GetIdentity_tranList(Optional ByVal strTableName As String = "List") As DataSet
'        'Sohail (14 Nov 2011) -- End
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation

'            strQ = "SELECT identitytranunkid " & _
'                    "      ,employeeunkid " & _
'                    "      ,idtypeunkid " & _
'                    "      ,identity_no " & _
'                    "      ,countryunkid " & _
'                    "      ,serial_no " & _
'                    "      ,issued_place " & _
'                    "      ,dl_class " & _
'                    "      ,issue_date " & _
'                    "      ,expiry_date " & _
'                    "      ,isdefault " & _
'                    "      ,'' As AUD " & _
'                    "FROM hremployee_idinfo_tran " & _
'                    "WHERE 1 = 1 " 'Sohail (14 Nov 2011) - [WHERE 1 = 1]

'            'Sohail (14 Nov 2011) -- Start
'            If intEmpUnkId > 0 Then
'                strQ &= "AND employeeunkid = @employeeunkid "
'                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkId.ToString)
'            End If

'            If intIDTypeUnkId > 0 Then
'                strQ &= "AND idtypeunkid = @idtypeunkid "
'                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intIDTypeUnkId.ToString)
'            End If
'            'Sohail (14 Nov 2011) -- End

'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If intIdTranUnkid > 0 Then
'                strQ &= "AND identitytranunkid = '" & intIdTranUnkid & "'"
'            End If
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetIdentity_tranList", mstrModuleName)
'        End Try
'        Return dsList
'    End Function

'    ' Pinkal (22-Dec-2010) -- End

'End Class
