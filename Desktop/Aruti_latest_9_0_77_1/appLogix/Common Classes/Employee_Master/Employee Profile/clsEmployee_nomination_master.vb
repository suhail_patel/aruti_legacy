﻿'************************************************************************************************************************************
'Class Name : clsEmployee_nomination_master.vb
'Purpose    :
'Date       :30-10-2020
'Written By :Gajanan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clsEmployee_nomination_master
    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_nomination_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintNominationunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintJobunkid As Integer

    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

    Private mblnIsmatch As Boolean = True

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nominationunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Nominationunkid() As Integer
        Get
            Return mintNominationunkid
        End Get
        Set(ByVal value As Integer)
            mintNominationunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public Property _Ismatch() As Boolean
        Get
            Return mblnIsmatch
        End Get
        Set(ByVal value As Boolean)
            mblnIsmatch = value
        End Set
    End Property

#End Region

    ''' <summary>FV
    '''
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  nominationunkid " & _
              ", employeeunkid " & _
              ", jobunkid " & _
              ", isvoid " & _
              ", ismatch " & _
             "FROM hremployee_nomination_master " & _
             "WHERE nominationunkid = @nominationunkid "

            objDataOperation.AddParameter("@nominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNominationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintNominationunkid = CInt(dtRow.Item("nominationunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mblnIsmatch = CBool(dtRow.Item("ismatch"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal intEmployeeunkid As Integer = -1, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal blnIsMatchOnly As Boolean = True, _
                            Optional ByVal intJobid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
             "  hremployee_nomination_master.nominationunkid " & _
             ", hremployee_nomination_master.employeeunkid " & _
             ", hrjob_master.job_name " & _
             ", hremployee_nomination_master.jobunkid " & _
             ", hremployee_nomination_master.isvoid " & _
             ", hremployee_nomination_master.ismatch " & _
             "FROM hremployee_nomination_master " & _
             "LEFT join hrjob_master on hrjob_master.jobunkid = hremployee_nomination_master.jobunkid and hrjob_master.isactive = 1 " & _
             "Where hremployee_nomination_master.isvoid = 0  "

            'Gajanan [26-Feb-2021] -- Start
            If intJobid > 0 Then
                strQ &= " and hremployee_nomination_master.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobid)
            End If
            'Gajanan [26-Feb-2021] -- End


            If intEmployeeunkid > 0 Then
                strQ &= " and hremployee_nomination_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If

            If blnIsMatchOnly Then
                strQ &= " and hremployee_nomination_master.ismatch = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_nomination_master) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mintEmployeeunkid, mintJobunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Employee is already nominated for this job.")
            Return False
        End If

        Dim objPotentialSuccession_tran As New clsPotentialSuccession_tran
        Dim ds As DataSet = objPotentialSuccession_tran.GetList("List", mintJobunkid, "", mintEmployeeunkid)

        If IsNothing(ds) = False AndAlso ds.Tables(0).Rows.Count > 0 Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Employee is already nominated for this job.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@ismatch", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmatch)

            strQ = "INSERT INTO hremployee_nomination_master ( " & _
              "  employeeunkid " & _
              ", jobunkid " & _
              ", isvoid " & _
              ", ismatch " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @jobunkid " & _
              ", @isvoid " & _
              ", @ismatch " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintNominationunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintNominationunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_nomination_master) </purpose>
    'Public Function Update() As Boolean
    '    If isExist(mintEmployeeunkid, mintJobunkid) Then
    '        mstrMessage = Language.getMessage(mstrModuleName, 1, "Employee is already nominated for this job.")
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    If xDataOpr IsNot Nothing Then
    '        objDataOperation = xDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.BindTransaction()
    '    End If

    '    objDataOperation.ClearParameters()

    '    Try
    '        objDataOperation.AddParameter("@nominationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintnominationunkid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
    '        objDataOperation.AddParameter("@jobunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintjobunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        StrQ = "UPDATE hremployee_nomination_master SET " & _
    '          "  employeeunkid = @employeeunkid" & _
    '          ", jobunkid = @jobunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason " & _
    '        "WHERE nominationunkid = @nominationunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_nomination_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objsucscreening_process_master As New clssucscreening_process_master

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hremployee_nomination_master SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE nominationunkid = @nominationunkid "

            objDataOperation.AddParameter("@nominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Remove All It's Sucession Transection Data
            dsList = objsucscreening_process_master.GetList(mintJobunkid, "ScreeningData", False, mintEmployeeunkid, objDataOperation)


            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                objsucscreening_process_master._Isvoid = True
                objsucscreening_process_master._Voidreason = mstrVoidreason
                objsucscreening_process_master._Voiduserunkid = mintVoiduserunkid
                objsucscreening_process_master._Voiddatetime = mdtVoiddatetime
                objsucscreening_process_master._HostName = mstrHostName
                objsucscreening_process_master._ClientIP = mstrClientIP
                objsucscreening_process_master._FormName = mstrFormName
                objsucscreening_process_master._AuditUserId = mintAuditUserId
                objsucscreening_process_master._FromWeb = mblnIsWeb

                For Each drrow As DataRow In dsList.Tables(0).Rows
                    If objsucscreening_process_master.Delete(CInt(drrow("processmstunkid")), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@nominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intemployeeunkid As Integer, ByVal intjobunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  nominationunkid " & _
              ", employeeunkid " & _
              ", jobunkid " & _
             "FROM hremployee_nomination_master " & _
             "WHERE employeeunkid = @employeeunkid " & _
             "AND jobunkid = @jobunkid and isvoid = 0 "

            'If intUnkid > 0 Then
            '    strQ &= " AND nominationunkid <> @nominationunkid"
            'End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intemployeeunkid)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intjobunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function GetEmployeeList(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xEmpid As Integer, _
                                     ByVal xSettingid As clssucsettings_master.enSuccessionConfiguration, _
                                     ByVal mdtAsOnDate As DateTime, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                     Optional ByVal strTempTableSelectQry As String = "", _
                                     Optional ByVal strTempTableJoinQry As String = "", _
                                     Optional ByVal strTempTableDropQry As String = "" _
                                    ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim objsucsetting As New clssucsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim mdicSetting As New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)
        Dim objCommon_Master As New clsCommon_Master

        Dim objsucpipeline_master As New clssucpipeline_master
        Dim intQualificationLevel As Integer = -1


        Try
            mdicSetting = objsucsetting.GetSettingFromPeriod()




            If IsNothing(mdicSetting) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry,Succession Setting Not Available.")
                Return Nothing
            End If


            If clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL = xSettingid Then

                If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION) Then
                    objCommon_Master._Masterunkid = CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL))
                    intQualificationLevel = objCommon_Master._QualificationGrp_Level
                End If
            End If



            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            strQ = strTempTableSelectQry

            strQ += "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ",  ISNULL(suspensiondays, 0) as suspensiondays " & _
                    ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '') AS termination_from_date " & _
                    ", ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '') AS termination_to_date " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '') AS empl_enddate " & _
                    ",  0 as processmstunkid" & _
                    "  FROM " & strDBName & "hremployee_master "

            strQ &= strTempTableJoinQry


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If


            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If


            '********************** Talent Seeting Conditions ************************' --- START

            strQ &= "LEFT JOIN (SELECT " & _
                      "sus.EmpId " & _
                    ",sus.suspensiondays " & _
                 "FROM (SELECT " & _
                           "employeeunkid AS EmpId " & _
                           ",SUM(DATEDIFF(DAY, date1,  " & _
                           " CASE " & _
                           "    WHEN CONVERT(CHAR(8), date2, 112) IS NULL THEN CAST(@Date AS DATETIME) " & _
                           "    ELSE CAST(CONVERT(CHAR(8), date2 + 1, 112)AS DATETIME) " & _
                           " End " & _
                            " )) AS suspensiondays " & _
                      "FROM  " & strDBName & "hremployee_dates_tran " & _
                      "WHERE hremployee_dates_tran.datetypeunkid IN (" & enEmp_Dates_Transaction.DT_SUSPENSION & ") " & _
                      "AND isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), date2, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " " & _
                      "GROUP BY hremployee_dates_tran.employeeunkid) AS sus) AS Emp_suspension " & _
                 "ON Emp_suspension.EmpId = hremployee_master.employeeunkid "
            '"JOIN (SELECT " & _
            '        "emp_transfer.employeeunkid " & _
            '        ",stationunkid " & _
            '        ",deptgroupunkid " & _
            '        ",departmentunkid " & _
            '        ",sectiongroupunkid " & _
            '        ",sectionunkid " & _
            '        ",unitgroupunkid " & _
            '        ",unitunkid " & _
            '        ",teamunkid " & _
            '        ",classgroupunkid " & _
            '        ",classunkid " & _
            '        ",emp_categorization.jobgroupunkid " & _
            '        ",emp_categorization.jobunkid " & _
            '        ",Emp_CCT.costcenterunkid " & _
            '     "FROM (SELECT " & _
            '               "Emp_TT.employeeunkid AS TrfEmpId " & _
            '             ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
            '             ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
            '             ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
            '             ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
            '             ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
            '             ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
            '             ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
            '             ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
            '             ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
            '             ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
            '             ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
            '             ",Emp_TT.employeeunkid " & _
            '             ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
            '          "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
            '          "WHERE isvoid = 0 " & _
            '          "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
            '     "JOIN (SELECT " & _
            '               "Emp_CT.employeeunkid AS CatEmpId " & _
            '             ",Emp_CT.jobgroupunkid " & _
            '             ",Emp_CT.jobunkid " & _
            '             ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
            '             ",Emp_CT.employeeunkid " & _
            '             ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
            '          "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
            '          "WHERE isvoid = 0 " & _
            '          "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
            '          "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
            '     "JOIN (SELECT " & _
            '               "Emp_CCT.employeeunkid AS CCTEmpId " & _
            '             ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
            '             ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
            '             ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
            '          "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
            '          "WHERE Emp_CCT.isvoid = 0 " & _
            '          "AND Emp_CCT.istransactionhead = 0 " & _
            '          "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
            '          "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
            '     "WHERE emp_transfer.Rno = 1 " & _
            '     "AND emp_categorization.Rno = 1 " & _
            '     "AND emp_CCT.Rno = 1"


            'If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.ALLOC_TYPE) Then
            '    Dim allocation As String = mdicSetting(clssucsettings_master.enSuccessionConfiguration.ALLOC_TYPE)
            '    Dim allocationType As String() = allocation.Split("|")

            '    Select Case CInt(allocationType(0))

            '        Case CInt(enAllocation.BRANCH)
            '            strQ &= "AND emp_transfer.stationunkid IN "

            '        Case CInt(enAllocation.DEPARTMENT_GROUP)
            '            strQ &= "AND emp_transfer.deptgroupunkid IN "

            '        Case CInt(enAllocation.DEPARTMENT)
            '            strQ &= "AND emp_transfer.departmentunkid IN "

            '        Case CInt(enAllocation.SECTION_GROUP)
            '            strQ &= "AND emp_transfer.sectiongroupunkid IN "

            '        Case CInt(enAllocation.SECTION)
            '            strQ &= "AND emp_transfer.sectionunkid IN "

            '        Case CInt(enAllocation.UNIT_GROUP)
            '            strQ &= "AND emp_transfer.unitgroupunkid IN "

            '        Case CInt(enAllocation.UNIT)
            '            strQ &= "AND emp_transfer.unitunkid IN "

            '        Case CInt(enAllocation.JOB_GROUP)
            '            strQ &= "AND emp_categorization.jobgroupunkid IN "

            '        Case CInt(enAllocation.JOBS)
            '            strQ &= "AND emp_categorization.jobunkid IN "

            '        Case CInt(enAllocation.CLASS_GROUP)
            '            strQ &= "AND emp_transfer.classgroupunkid IN "

            '        Case CInt(enAllocation.CLASSES)
            '            strQ &= "AND emp_transfer.classunkid IN "

            '        Case CInt(enAllocation.COST_CENTER)
            '            strQ &= "AND emp_CCT.costcenterunkid IN  "

            '    End Select

            '    strQ &= " (" & allocationType(1) & " ) "

            'End If

            'strQ &= " ) AS allocation " & _
            '     "ON allocation.employeeunkid = @EmpId " & _
            '"LEFT JOIN hrjob_master ON allocation.jobunkid = hrjob_master.jobunkid " & _
            '"LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = allocation.departmentunkid "

            If clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL = xSettingid Then

                strQ &= " LEFT JOIN hremp_qualification_tran " & _
                        "   ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN cfcommon_master " & _
                        "   ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid " & _
                        "   AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & " " & _
                        "   AND cfcommon_master.isactive = 1 "
            End If


            strQ &= " WHERE 1=1  "

            strQ &= " and hremployee_master.employeeunkid = @EmpId "

            If clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL = xSettingid Then

                If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION) Then
                    Select Case CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION))
                        Case CInt(enComparison_Operator.LESS_THAN)
                            strQ &= "and cfcommon_master.qlevel < @qlevel "
                        Case CInt(enComparison_Operator.LESS_THAN_EQUAL)
                            strQ &= "and cfcommon_master.qlevel <= @qlevel "
                        Case CInt(enComparison_Operator.GREATER_THAN)
                            strQ &= "and cfcommon_master.qlevel > @qlevel  "
                        Case CInt(enComparison_Operator.GREATER_THAN_EQUAL)
                            strQ &= "and cfcommon_master.qlevel >= @qlevel  "
                        Case Else
                            strQ &= "and cfcommon_master.qlevel = @qlevel  "
                    End Select

                    strQ &= " "
                objDataOperation.AddParameter("@qlevel", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL)))
            End If
            End If

            If clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO = xSettingid Then
                strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS DECIMAL)-CAST(CONVERT(CHAR(8),birthdate,112) AS DECIMAL))/10000 <= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) & " "
            End If


            'If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) Then
            '    strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 <= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) & " "
            'End If

            'If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER) Then
            '    strQ &= " AND hremployee_master.employeeunkid not in (SELECT " & _
            '            " employeeunkid " & _
            '            " FROM sucscreening_stages_tran " & _
            '            " WHERE isvoid = 0 " & _
            '            " GROUP BY employeeunkid " & _
            '            "	HAVING count(employeeunkid) >= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER) & " ) "
            'End If
            '================ Employee ID

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If

            strQ &= strTempTableDropQry

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmpid)


            dsList = objDataOperation.ExecQuery(strQ, strListName)


            If dsList.Tables(0).Rows.Count <= 0 AndAlso clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL = xSettingid Then
                Return False
            End If


            If dsList.Tables(0).Rows.Count <= 0 AndAlso clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO = xSettingid Then
                Return False
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim strEmpIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())

            If strEmpIDs.Length > 0 Then

                Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, xCompanyUnkid, xDatabaseName, strEmpIDs, mdtAsOnDate)
                Dim dtService = New DataView(dsService.Tables(0), "fromdate <> '' ", "", DataViewRowState.CurrentRows).ToTable
                Dim result = (From e In dsList.Tables(0).AsEnumerable() Group Join s In dtService.AsEnumerable() On e.Item("employeeunkid") Equals s.Item("employeeunkid") Into es = Group From s In es.DefaultIfEmpty() Order By e.Field(Of Integer)("employeeunkid") _
                              Select New With { _
                                 Key .employeecode = e.Field(Of String)("employeecode") _
                                , Key .employeename = e.Field(Of String)("employeename") _
                                , Key .employeeunkid = e.Field(Of Integer)("employeeunkid") _
                                , Key .isapproved = e.Field(Of Boolean)("isapproved") _
                                , Key .EmpCodeName = e.Field(Of String)("EmpCodeName") _
                                , Key .age = e.Field(Of Integer)("age") _
                                , Key .exyr = e.Field(Of Integer)("exyr") _
                                , Key .suspensiondays = e.Field(Of Integer)("suspensiondays") _
                                , Key .appointeddate = e.Field(Of String)("appointeddate") _
                                , Key .termination_from_date = e.Field(Of String)("termination_from_date") _
                                , Key .termination_to_date = e.Field(Of String)("termination_to_date") _
                                , Key .empl_enddate = e.Field(Of String)("empl_enddate") _
                                , Key .FromDate = If(s Is Nothing, "", s.Field(Of String)("fromdate")) _
                                , Key .ToDate = If(s Is Nothing, "", s.Field(Of String)("todate")) _
                                , Key .ServiceDays = If(s Is Nothing, 0, s.Field(Of Integer)("ServiceDays")) _
                                })

                Dim intPrevEmp As Integer = 0
                Dim dblTotDays As Double = 0

                If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) Then

                For Each drow In result

                    Dim StartDate As Date = eZeeDate.convertDate(drow.appointeddate)
                    Dim EndDate As Date = mdtAsOnDate


                    Dim intEmpId As Integer = drow.employeeunkid
                    Dim intCount As Integer = dtService.Select("employeeunkid = " & intEmpId & " ").Length

                    If intCount <= 0 Then 'No Dates record found                       
                        If drow.termination_from_date.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.termination_from_date) < EndDate, eZeeDate.convertDate(drow.termination_from_date), EndDate)
                        End If
                        If drow.termination_to_date.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.termination_to_date) < EndDate, eZeeDate.convertDate(drow.termination_to_date), EndDate)
                        End If
                        If drow.empl_enddate.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.empl_enddate) < EndDate, eZeeDate.convertDate(drow.empl_enddate), EndDate)
                        End If
                        dblTotDays = CDbl(DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)))
                    ElseIf intCount = 1 Then
                        dblTotDays = drow.ServiceDays
                    Else
                        dblTotDays = (From p In dtService Where (p.Field(Of Integer)("employeeunkid") = intEmpId) Select (CDbl(p.Item("ServiceDays")))).Sum()
                    End If

                    For Each drRow As DataRow In dsList.Tables(0).Select("employeeunkid = " & intEmpId & " ")
                        drRow.Item("exyr") = (dblTotDays - drow.suspensiondays) / 365
                    Next
                Next

                    dsList.Tables(0).DefaultView.RowFilter = "exyr >= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) & " "
                    If clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO = xSettingid AndAlso dsList.Tables(0).DefaultView.Table.Rows.Count <= 0 Then
                        Return False
                    End If

                End If
            End If


            '=========== Performance Score Settings - Start =====================
            If clssucsettings_master.enSuccessionConfiguration.PERF_SCORE = xSettingid Then
                'S.SANDEEP |17-FEB-2021| -- START
                Dim intPrdNo As Integer = 0
                If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.ANY_PERIOD) Or mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.ALL_PERIOD) Then
                    If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.ANY_PERIOD) Then
                        If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MIN_PERF_NO) Then
                            intPrdNo = mdicSetting(clssucsettings_master.enSuccessionConfiguration.MIN_PERF_NO)
                        Else
                            intPrdNo = 1
                        End If
                    ElseIf mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.ALL_PERIOD) Then
                        intPrdNo = 0
                    End If
                End If
                If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE) Then
                    Dim strPerformanceScoreEmpIds As String = String.Join(",", (From p In objsucpipeline_master.GetPerformanceScoreEmployeeList(xDatabaseName, intPrdNo, mdicSetting(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE), xCompanyUnkid, xYearUnkid) Select (p.Item("employeeunkid").ToString)).ToArray())
                    'S.SANDEEP |17-FEB-2021| -- END
                    If strPerformanceScoreEmpIds.Length > 0 Then
                        dsList.Tables(0).DefaultView.RowFilter = "employeeunkid in ( " & strPerformanceScoreEmpIds & ") and employeeunkid = " & xEmpid & ""
                        Dim dtPerformanceScore As DataTable = dsList.Tables(0).DefaultView.ToTable

                        If xSettingid = clssucsettings_master.enSuccessionConfiguration.PERF_SCORE AndAlso dtPerformanceScore.Rows.Count <= 0 Then
                    Return False
                End If

                        dsList.Tables.Remove(dsList.Tables(0))
                        dsList.Tables.Add(dtPerformanceScore)
                        Return True
                    Else
                        Return False
            End If
            End If
            End If

            '=========== Performance Score Settings - End =====================


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType, ByVal intNominationunkid As String) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..athremployee_nomination_master ( " & _
                       "  tranguid " & _
                       ", nominationunkid " & _
                       ", employeeunkid " & _
                       ", jobunkid " & _
                       ", ismatch " & _
                       ", audittypeid " & _
                       ", audtuserunkid " & _
                       ", auditdatetime " & _
                       ", formname " & _
                       ", ip " & _
                       ", host " & _
                       ", isweb" & _
                   ") VALUES (" & _
                       "  LOWER(NEWID()) " & _
                       ", @nominationunkid " & _
                       ", @employeeunkid " & _
                       ", @jobunkid " & _
                       ", @ismatch " & _
                       ", @audittypeid " & _
                       ", @audtuserunkid " & _
                       ", GETDATE() " & _
                       ", @formname " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @isweb " & _
                    ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@nominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNominationunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@ismatch", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmatch)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    Public Function GetNominatedEmployeeList(ByVal strTableName As String, _
                                    ByVal xDatabaseName As String, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                    Optional ByVal xEmpid As Integer = -1, _
                                    Optional ByVal xNominatedJobid As Integer = -1, _
                                     Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        Try
            strQ &= "Select " & _
                    "hremployee_master.employeecode " & _
                    ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",hremployee_master.employeeunkid " & _
                    ",dept.name as Department FROM hremployee_nomination_master " & _
                    "LEFT JOIN hremployee_master on " & _
                    "    hremployee_master.employeeunkid = hremployee_nomination_master.employeeunkid " & _
                    "JOIN (SELECT " & _
                      "emp_transfer.employeeunkid " & _
                    ",stationunkid " & _
                    ",deptgroupunkid " & _
                    ",departmentunkid " & _
                    ",sectiongroupunkid " & _
                    ",sectionunkid " & _
                    ",unitgroupunkid " & _
                    ",unitunkid " & _
                    ",teamunkid " & _
                    ",classgroupunkid " & _
                    ",classunkid " & _
                    ",emp_categorization.jobgroupunkid " & _
                    ",emp_categorization.jobunkid " & _
                    ",Emp_CCT.costcenterunkid " & _
                 "FROM (SELECT " & _
                           "Emp_TT.employeeunkid AS TrfEmpId " & _
                         ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                         ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                         ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                         ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                         ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                         ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                         ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                         ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                         ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                         ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                         ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                         ",Emp_TT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                      "FROM  hremployee_transfer_tran AS Emp_TT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                 "JOIN (SELECT " & _
                           "Emp_CT.employeeunkid AS CatEmpId " & _
                         ",Emp_CT.jobgroupunkid " & _
                         ",Emp_CT.jobunkid " & _
                         ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                         ",Emp_CT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                      "FROM  hremployee_categorization_tran AS Emp_CT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                      "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                 "JOIN (SELECT " & _
                           "Emp_CCT.employeeunkid AS CCTEmpId " & _
                         ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                         ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                      "FROM  hremployee_cctranhead_tran AS Emp_CCT " & _
                      "WHERE Emp_CCT.isvoid = 0 " & _
                      "AND Emp_CCT.istransactionhead = 0 " & _
                      "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                      "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                 "WHERE emp_transfer.Rno = 1 " & _
                 "AND emp_categorization.Rno = 1 " & _
                 "AND emp_CCT.Rno = 1"

            strQ &= " ) AS allocation " & _
                    "ON allocation.employeeunkid = hremployee_master.employeeunkid "

            strQ &= "LEFT JOIN (SELECT " & _
                      "job_name " & _
                    ",jobunkid " & _
                 "FROM hrjob_master) AS job " & _
                 "ON job.jobunkid = hremployee_master.jobunkid " & _
            "LEFT JOIN (SELECT " & _
                      "name " & _
                    ",departmentunkid " & _
                 "FROM hrdepartment_master) AS dept " & _
                 "ON dept.departmentunkid = hremployee_master.departmentunkid "

            strQ &= "WHERE 	hremployee_nomination_master.isvoid = 0 " & _
                    " and hremployee_nomination_master.ismatch = 1 "

            If xNominatedJobid > 0 Then
                strQ &= " and hremployee_nomination_master.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNominatedJobid)
            End If


            If xEmpid > 0 Then
                strQ &= " and hremployee_nomination_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmpid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            exForce = Nothing
        End Try

    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is already nominated for this job.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
