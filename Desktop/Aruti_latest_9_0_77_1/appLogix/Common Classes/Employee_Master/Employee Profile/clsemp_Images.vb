﻿'************************************************************************************************************************************
'Class Name : clsemp_Images.vb
'Purpose    :
'Date       :28/03/2013
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm


''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsemp_Images
    Private Shared ReadOnly mstrModuleName As String = "clsemp_Images"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmployeeimgunkid As Integer
    Private mintCompanyunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mbytPhoto As Byte()
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mdtAuditdatetime As Date
    Private mstrIp As String = String.Empty
    Private mstrMachine_Name As String = String.Empty
    Private mstrWebForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mintLoginemployeeunkid As Integer
    Private mintvoidLoginemployeeunkid As Integer
    'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
    Private mbytSignature As Byte() = Nothing
    'S.SANDEEP [11-AUG-2017] -- END

#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeimgunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeimgunkid() As Integer
        Get
            Return mintEmployeeimgunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeimgunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set photo
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Photo() As Byte()
        Get
            Return mbytPhoto
        End Get
        Set(ByVal value As Byte())
            mbytPhoto = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set machine_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Machine_Name() As String
        Get
            Return mstrMachine_Name
        End Get
        Set(ByVal value As String)
            mstrMachine_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Webform_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebForm_Name() As String
        Get
            Return mstrWebForm_Name
        End Get
        Set(ByVal value As String)
            mstrWebForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _voidLoginemployeeunkid() As Integer
        Get
            Return mintvoidLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintvoidLoginemployeeunkid = value
        End Set
    End Property

    'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
    Public ReadOnly Property _Signature() As Byte()
        Get
            Return mbytSignature
        End Get
    End Property
    'S.SANDEEP [11-AUG-2017] -- END

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal intCompanyId As Integer, ByVal intEmployeeId As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal objDataOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  employeeimgunkid " & _
                      ", companyunkid " & _
                      ", employeeunkid " & _
                      ", photo " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", symsignature " & _
                     "FROM arutiimages..hremployee_images " & _
                     "WHERE companyunkid = @companyunkid AND employeeunkid = @employeeunkid "
            'S.SANDEEP [11-AUG-2017] -- START {symsignature} -- END
            If blnOnlyActive Then
                strQ &= " AND isvoid = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmployeeimgunkid = CInt(dtRow.Item("employeeimgunkid"))
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))

                If Not IsDBNull(dtRow.Item("photo")) Then
                    mbytPhoto = CType(dtRow.Item("photo"), Byte())
                End If

                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                'S.SANDEEP [11-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                If Not IsDBNull(dtRow.Item("symsignature")) Then
                    mbytSignature = CType(dtRow.Item("symsignature"), Byte())
                End If
                'S.SANDEEP [11-AUG-2017] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  employeeimgunkid " & _
                      ", companyunkid " & _
                      ", employeeunkid " & _
                      ", photo " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", symsignature " & _
                     "FROM arutiimages..hremployee_images "
            'S.SANDEEP [11-AUG-2017] -- START {symsignature} -- END
            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_images) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If isExist(objDataOperation, mintCompanyunkid, mintEmployeeunkid) = True Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                Return True
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@photo", SqlDbType.Image, mbytPhoto.Length, mbytPhoto)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoidLoginemployeeunkid)

            strQ = "INSERT INTO arutiimages..hremployee_images ( " & _
                      "  companyunkid " & _
                      ", employeeunkid " & _
                      ", photo " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime" & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                    ") VALUES (" & _
                      "  @companyunkid " & _
                      ", @employeeunkid " & _
                      ", @photo " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime" & _
                      ", @loginemployeeunkid " & _
                      ", @voidloginemployeeunkid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmployeeimgunkid = dsList.Tables(0).Rows(0).Item(0)


            If InsertAtEmployee_Images(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_images) </purpose>
    Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeimgunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@photo", SqlDbType.Image, mbytPhoto.Length, mbytPhoto)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoidLoginemployeeunkid)

            strQ = "UPDATE arutiimages..hremployee_images SET " & _
                      "  companyunkid = @companyunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", photo = @photo" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime " & _
                      ", loginemployeeunkid = @loginemployeeunkid " & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                    "WHERE employeeimgunkid = @employeeimgunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAtEmployee_Images(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_images) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()

            strQ = "UPDATE arutiimages..hremployee_images  set " & _
                      " isvoid = 1 " & _
                       ", voiddatetime = @voiddatetime "

            If mintLoginemployeeunkid > 0 Then
                strQ &= ", voidloginemployeeunkid = @voidloginemployeeunkid "
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoidLoginemployeeunkid)

            ElseIf mintUserunkid > 0 Then
                strQ &= ", voiduserunkid  = @voiduserunkid "
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            End If

            strQ &= " WHERE employeeimgunkid = @employeeimgunkid "

            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@employeeimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmployeeimgunkid = intunkid
            If InsertAtEmployee_Images(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@employeeimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal objdataoperation As clsDataOperation, ByVal intCompanyId As Integer, ByVal intEmployeeId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            strQ = "SELECT " & _
              "  employeeimgunkid " & _
              ", companyunkid " & _
              ", employeeunkid " & _
              ", photo " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", loginemployeeunkid " & _
              ", voidloginemployeeunkid " & _
             "FROM arutiimages..hremployee_images " & _
             "WHERE companyunkid = @companyunkid " & _
             "AND employeeunkid = @employeeunkid AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND employeeimgunkid <> @employeeimgunkid"
            End If

            objdataoperation.ClearParameters()
            objdataoperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objdataoperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            If intUnkid > 0 Then
                objdataoperation.AddParameter("@employeeimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objdataoperation.ExecQuery(strQ, "List")

            If objdataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objdataoperation.ErrorNumber & ": " & objdataoperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 28 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If dsList.Tables(0).Rows.Count > 0 Then
                mintEmployeeimgunkid = dsList.Tables(0).Rows(0)("employeeimgunkid")
            End If
            'S.SANDEEP [ 28 MAR 2013 ] -- END

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_images) </purpose>
    Public Function InsertAtEmployee_Images(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeimgunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrIp.Trim.Length <= 0, getIP, mstrIp))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrMachine_Name.Trim.Length <= 0, getHostName, mstrMachine_Name))

            If mstrWebForm_Name.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If






            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

            strQ = "INSERT INTO arutiimages..athremployee_images ( " & _
                      "  employeeimgunkid " & _
                      ", companyunkid " & _
                      ", employeeunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", isweb " & _
                      ", loginemployeeunkid" & _
                    ") VALUES (" & _
                      "  @employeeimgunkid " & _
                      ", @companyunkid " & _
                      ", @employeeunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", @isweb " & _
                      ", @loginemployeeunkid" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAtEmployee_Images; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    'S.SANDEEP [ 28 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Public Function Import_Image() As Boolean
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            If Insert(objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "Import_Image", mstrModuleName)
        End Try
    End Function

    Public Function Import_IsExists() As Boolean
        Try
            Dim blnFlag As Boolean = False
            objDataOperation = New clsDataOperation
            blnFlag = isExist(objDataOperation, mintCompanyunkid, mintEmployeeunkid)
                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_IsExists", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 28 MAR 2013 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class