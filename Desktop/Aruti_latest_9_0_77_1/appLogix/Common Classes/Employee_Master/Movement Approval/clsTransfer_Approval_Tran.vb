﻿'************************************************************************************************************************************
'Class Name : clsTransfer_Approval_Tran.vb
'Purpose    :
'Date       :30-Jul-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsTransfer_Approval_Tran
    Private Const mstrModuleName = "clsTransfer_Approval_Tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintTransferunkid As Integer
    Private mdtEffectivedate As Date
    Private mintEmployeeunkid As Integer
    Private mintRehiretranunkid As Integer
    Private mintStationunkid As Integer
    Private mintDeptgroupunkid As Integer
    Private mintDepartmentunkid As Integer
    Private mintSectiongroupunkid As Integer
    Private mintSectionunkid As Integer
    Private mintUnitgroupunkid As Integer
    Private mintUnitunkid As Integer
    Private mintTeamunkid As Integer
    Private mintClassgroupunkid As Integer
    Private mintClassunkid As Integer
    Private mintChangereasonunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsProcessed As Boolean = False
    'S.SANDEEP |17-JAN-2019| -- START
    Private mintOperationTypeId As Integer = 0
    'S.SANDEEP |17-JAN-2019| -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transferunkid() As Integer
        Get
            Return mintTransferunkid
        End Get
        Set(ByVal value As Integer)
            mintTransferunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deptgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Deptgroupunkid() As Integer
        Get
            Return mintDeptgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintDeptgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectiongroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Sectiongroupunkid() As Integer
        Get
            Return mintSectiongroupunkid
        End Get
        Set(ByVal value As Integer)
            mintSectiongroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectionunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Sectionunkid() As Integer
        Get
            Return mintSectionunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Unitgroupunkid() As Integer
        Get
            Return mintUnitgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Unitunkid() As Integer
        Get
            Return mintUnitunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set teamunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Teamunkid() As Integer
        Get
            Return mintTeamunkid
        End Get
        Set(ByVal value As Integer)
            mintTeamunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Classgroupunkid() As Integer
        Get
            Return mintClassgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintClassgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Classunkid() As Integer
        Get
            Return mintClassunkid
        End Get
        Set(ByVal value As Integer)
            mintClassunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mblnIsProcessed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsProcessed() As Boolean
        Get
            Return mblnIsProcessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsProcessed = value
        End Set
    End Property

    'S.SANDEEP |17-JAN-2019| -- START
    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property
    'S.SANDEEP |17-JAN-2019| -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal xEmployeeUnkid As Integer = 0, Optional ByVal blnOnlyPending As Boolean = True) As DataSet 'S.SANDEEP [14-AUG-2018] -- START {Ref#244} [blnOnlyPending] -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            strQ = "SELECT " & _
                   "  hremployee_transfer_approval_tran.tranguid " & _
                   ", hremployee_transfer_approval_tran.transactiondate " & _
                   ", hremployee_transfer_approval_tran.mappingunkid " & _
                   ", hremployee_transfer_approval_tran.remark " & _
                   ", hremployee_transfer_approval_tran.isfinal " & _
                   ", hremployee_transfer_approval_tran.statusunkid " & _
                   ", hremployee_transfer_approval_tran.transferunkid " & _
                   ", hremployee_transfer_approval_tran.effectivedate " & _
                   ", hremployee_transfer_approval_tran.employeeunkid " & _
                   ", hremployee_transfer_approval_tran.rehiretranunkid " & _
                   ", hremployee_transfer_approval_tran.stationunkid " & _
                   ", hremployee_transfer_approval_tran.deptgroupunkid " & _
                   ", hremployee_transfer_approval_tran.departmentunkid " & _
                   ", hremployee_transfer_approval_tran.sectiongroupunkid " & _
                   ", hremployee_transfer_approval_tran.sectionunkid " & _
                   ", hremployee_transfer_approval_tran.unitgroupunkid " & _
                   ", hremployee_transfer_approval_tran.unitunkid " & _
                   ", hremployee_transfer_approval_tran.teamunkid " & _
                   ", hremployee_transfer_approval_tran.classgroupunkid " & _
                   ", hremployee_transfer_approval_tran.classunkid " & _
                   ", hremployee_transfer_approval_tran.changereasonunkid " & _
                   ", hremployee_transfer_approval_tran.isvoid " & _
                   ", hremployee_transfer_approval_tran.voiddatetime " & _
                   ", hremployee_transfer_approval_tran.voidreason " & _
                   ", hremployee_transfer_approval_tran.voiduserunkid " & _
                   ", hremployee_transfer_approval_tran.audittype " & _
                   ", hremployee_transfer_approval_tran.audituserunkid " & _
                   ", hremployee_transfer_approval_tran.ip " & _
                   ", hremployee_transfer_approval_tran.hostname " & _
                   ", hremployee_transfer_approval_tran.form_name " & _
                   ", hremployee_transfer_approval_tran.isweb " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", ISNULL(hrstation_master.name,'') AS branch " & _
                   ", ISNULL(hrdepartment_group_master.name,'') AS deptgroup " & _
                   ", ISNULL(hrdepartment_master.name,'') AS dept " & _
                   ", ISNULL(hrsectiongroup_master.name,'') AS secgroup " & _
                   ", ISNULL(hrsection_master.name,'') AS section " & _
                   ", ISNULL(hrunitgroup_master.name,'') AS unitname " & _
                   ", ISNULL(hrunit_master.name,'') AS unit " & _
                   ", ISNULL(hrteam_master.name,'') AS team " & _
                   ", ISNULL(hrclassgroup_master.name,'') AS classgrp " & _
                   ", ISNULL(hrclasses_master.name,'') AS class " & _
                   ", CONVERT(CHAR(8),hremployee_transfer_approval_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", '' AS EffDate " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name, '') END AS CReason " & _
                   ", ISNULL(hremployee_transfer_approval_tran.operationtypeid,0) AS operationtypeid " & _
                   ", CASE WHEN ISNULL(hremployee_transfer_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                   "       WHEN ISNULL(hremployee_transfer_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                   "       WHEN ISNULL(hremployee_transfer_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                   "  END AS OperationType " & _
                   "FROM hremployee_transfer_approval_tran " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_transfer_approval_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRANSFERS & _
                   "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_transfer_approval_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "    LEFT JOIN hrstation_master on hrstation_master.stationunkid = hremployee_transfer_approval_tran.stationunkid " & _
                   "    LEFT JOIN hrdepartment_group_master on hrdepartment_group_master.deptgroupunkid = hremployee_transfer_approval_tran.deptgroupunkid " & _
                   "    LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_transfer_approval_tran.departmentunkid " & _
                   "    LEFT JOIN hrsectiongroup_master ON hremployee_transfer_approval_tran.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                   "    LEFT JOIN hrsection_master on hrsection_master.sectionunkid = hremployee_transfer_approval_tran.sectionunkid " & _
                   "    LEFT JOIN hrunitgroup_master ON hremployee_transfer_approval_tran.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                   "    LEFT JOIN hrunit_master on hrunit_master.unitunkid = hremployee_transfer_approval_tran.unitunkid " & _
                   "    LEFT JOIN hrteam_master ON hremployee_transfer_approval_tran.teamunkid = hrteam_master.teamunkid " & _
                   "    LEFT JOIN hrclassgroup_master on hrclassgroup_master.classgroupunkid = hremployee_transfer_approval_tran.classgroupunkid " & _
                   "    LEFT JOIN hrclasses_master on hrclasses_master.classesunkid = hremployee_transfer_approval_tran.classunkid " & _
                   "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_transfer_approval_tran.employeeunkid " & _
                   "WHERE hremployee_transfer_approval_tran.isvoid = 0 AND ISNULL(hremployee_transfer_approval_tran.isprocessed,0) = 0 "
            'S.SANDEEP |17-JAN-2019| -- START {operationtypeid} -- END

            If xEmployeeUnkid > 0 Then
                strQ &= " AND hremployee_transfer_approval_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_transfer_approval_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "

            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 62, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 63, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 64, "Information Deleted"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [14-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim strValue As String = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empstat.ToString()).ToArray())
            'S.SANDEEP [09-AUG-2018] -- START {Added : x.empstat | Removed : x.empid} -- END
            If strValue.Trim.Length > 0 Then
                'S.SANDEEP [09-AUG-2018] -- START
                'Dim dtTbl As DataTable = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                Dim dtTbl As DataTable = Nothing
                If strValue = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                    strValue = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empid.ToString()).ToArray())
                    If strValue.Trim.Length > 0 Then
                        dtTbl = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                    End If
                Else
                    dtTbl = New DataView(dsList.Tables(0), "statusunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                End If
                'S.SANDEEP [09-AUG-2018] -- END
                dsList.Tables.Remove(strTableName)
                dsList.Tables.Add(dtTbl)
            End If
            'S.SANDEEP [14-AUG-2018] -- END

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_transfer_approval_tran) </purpose>
    Public Function Insert(ByVal intCompanyId As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, _
                           ByVal mblnCreateADUserFromEmpMst As Boolean, _
                             ByVal xDatabaseName As String, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal blnFromApprovalScreen As Boolean = False, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False) As Boolean


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ ByVal xDatabaseName As String]

        'Pinkal (07-Dec-2019) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        'Pinkal (07-Dec-2019) -- End


        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval}

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START


        'Dim objDataOperation As New clsDataOperation

 '       If xDataOpr IsNot Nothing Then
 '           objDataOperation = xDataOpr
 '           objDataOperation.ClearParameters()
 '       Else
 '           objDataOperation = New clsDataOperation
 '       End If


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
       'If blnFromApproval = False Then
        If blnFromApprovalScreen = False Then
            'Gajanan [23-SEP-2019] -- End
            'S.SANDEEP |17-JAN-2019| -- START
            'If isExist(Nothing, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, mintEmployeeunkid, , objDataOperation, True) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Transfer information is already present in pending state for the selected employee.")
            '    Return False
            'End If

            'If isExist(mdtEffectivedate, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, mintEmployeeunkid, , objDataOperation) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transfer information is already present for the selected effective date.")
            '    Return False
            'End If

            'If isExist(mdtEffectivedate, mintStationunkid, mintDeptgroupunkid, _
            '          mintDepartmentunkid, mintSectiongroupunkid, mintSectionunkid, _
            '          mintUnitgroupunkid, mintUnitunkid, mintTeamunkid, mintClassgroupunkid, mintClassunkid, mintEmployeeunkid, , objDataOperation) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
            '    Return False
            'End If

            'If mintOperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED Then
            '    If isExist(Nothing, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, mintEmployeeunkid, eOprType, , objDataOperation, False, mintTransferunkid) Then
            '        mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Transfer information is already present in pending state for the selected employee.")
            '        Return False
            '    End If
            'End If

            If isExist(Nothing, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, mintEmployeeunkid, eOprType, , objDataOperation, True, mintTransferunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Transfer information is already present in pending state for the selected employee.")
                Return False
            End If

            'If isExist(mdtEffectivedate, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, mintEmployeeunkid, eOprType, , objDataOperation, , mintTransferunkid) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Transfer information is already present in pending state for the selected employee.")
            '    Return False
            'End If

            'If isExist(mdtEffectivedate, mintStationunkid, mintDeptgroupunkid, _
            '          mintDepartmentunkid, mintSectiongroupunkid, mintSectionunkid, _
            '          mintUnitgroupunkid, mintUnitunkid, mintTeamunkid, mintClassgroupunkid, mintClassunkid, mintEmployeeunkid, eOprType, , objDataOperation, True, mintTransferunkid) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
            '    Return False
            'End If
            'S.SANDEEP |17-JAN-2019| -- END
            End If
            'S.SANDEEP |17-JAN-2019| -- END

        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcessed)
            'S.SANDEEP |17-JAN-2019| -- START
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)
            'S.SANDEEP |17-JAN-2019| -- END

            strQ = "INSERT INTO hremployee_transfer_approval_tran ( " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", transferunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectiongroupunkid " & _
              ", sectionunkid " & _
              ", unitgroupunkid " & _
              ", unitunkid " & _
              ", teamunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", changereasonunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb" & _
              ", isprocessed" & _
              ", operationtypeid " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @remark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @transferunkid " & _
              ", @effectivedate " & _
              ", @employeeunkid " & _
              ", @rehiretranunkid " & _
              ", @stationunkid " & _
              ", @deptgroupunkid " & _
              ", @departmentunkid " & _
              ", @sectiongroupunkid " & _
              ", @sectionunkid " & _
              ", @unitgroupunkid " & _
              ", @unitunkid " & _
              ", @teamunkid " & _
              ", @classgroupunkid " & _
              ", @classunkid " & _
              ", @changereasonunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @voiduserunkid " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @hostname " & _
              ", @form_name " & _
              ", @isweb" & _
              ", @isprocessed " & _
              ", @operationtypeid " & _
            "); SELECT @@identity"
            'S.SANDEEP |17-JAN-2019| -- START {operationtypeid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            If blnFromApprovalScreen = False Then


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                'If MigrationInsert(intCompanyId, eOprType, mblnCreateADUserFromEmpMst, objDataOperation, xMdtMigration, xblnFromApproval, blnFromApprovalScreen) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                If MigrationInsert(xDatabaseName, intCompanyId, eOprType, mblnCreateADUserFromEmpMst, objDataOperation, xMdtMigration, xblnFromApproval, blnFromApprovalScreen) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Pinkal (12-Oct-2020) -- End

            End If
            'Gajanan [23-SEP-2019] -- End

            'S.SANDEEP |17-JAN-2019| -- START


            'mstrTranguid = dsList.Tables(0).Rows(0).Item(0)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            
            'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intTranferUnkid As Integer, ByVal dtEffDate As Date, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END
        Try
            'S.SANDEEP [15-NOV-2018] -- Start
            strQ = "UPDATE hremployee_transfer_approval_tran SET " & _
                   " " & strColName & " = '" & intTranferUnkid & "' " & _
                   "WHERE isprocessed= 0 and CONVERT(CHAR(8),effectivedate,112) = '" & eZeeDate.convertDate(dtEffDate).ToString & "' AND employeeunkid = '" & intEmployeeId & "' "
            '----REMOVED (effectivedate)
            '----ADDED (CONVERT(CHAR(8),effectivedate,112))
            'S.SANDEEP [15-NOV-2018] -- End
            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
             
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
'S.SANDEEP |17-JAN-2019| -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_transfer_approval_tran) </purpose>
    Public Function Update(ByVal intCompanyId As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START


' Dim objDataOperation As New clsDataOperation

'        If xDataOpr IsNot Nothing Then
'            objDataOperation = xDataOpr
'            objDataOperation.ClearParameters()
'        Else
'            objDataOperation = New clsDataOperation
'        End If

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END
        If isExist(mdtEffectivedate, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, mintEmployeeunkid, eOprType, mstrTranguid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transfer information is already present for the selected effective date.")
            Return False
        End If

        If isExist(mdtEffectivedate, mintStationunkid, mintDeptgroupunkid, _
                   mintDepartmentunkid, mintSectiongroupunkid, mintSectionunkid, _
                   mintUnitgroupunkid, mintUnitunkid, mintTeamunkid, mintClassgroupunkid, mintClassunkid, mintEmployeeunkid, eOprType, mstrTranguid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
            Return False
        End If

        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            'S.SANDEEP |17-JAN-2019| -- START
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)
            'S.SANDEEP |17-JAN-2019| -- END

            strQ = "UPDATE hremployee_transfer_approval_tran SET " & _
              "  transactiondate = @transactiondate" & _
              ", mappingunkid = @mappingunkid" & _
              ", remark = @remark" & _
              ", isfinal = @isfinal" & _
              ", statusunkid = @statusunkid" & _
              ", transferunkid = @transferunkid" & _
              ", effectivedate = @effectivedate" & _
              ", employeeunkid = @employeeunkid" & _
              ", rehiretranunkid = @rehiretranunkid" & _
              ", stationunkid = @stationunkid" & _
              ", deptgroupunkid = @deptgroupunkid" & _
              ", departmentunkid = @departmentunkid" & _
              ", sectiongroupunkid = @sectiongroupunkid" & _
              ", sectionunkid = @sectionunkid" & _
              ", unitgroupunkid = @unitgroupunkid" & _
              ", unitunkid = @unitunkid" & _
              ", teamunkid = @teamunkid" & _
              ", classgroupunkid = @classgroupunkid" & _
              ", classunkid = @classunkid" & _
              ", changereasonunkid = @changereasonunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", audittype = @audittype" & _
              ", audituserunkid = @audituserunkid" & _
              ", ip = @ip" & _
              ", hostname = @hostname" & _
              ", form_name = @form_name" & _
              ", isweb = @isweb " & _
              ", operationtypeid = @operationtypeid " & _
            "WHERE tranguid = @tranguid "
            'S.SANDEEP |17-JAN-2019| -- START {operationtypeid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If     
       'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_transfer_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, _
                           ByVal intCompanyId As Integer, ByVal mblnCreateADUserFromEmpMst As Boolean, _
                           ByVal xDatabaseName As String, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False, _
                           Optional ByVal xblnFromApprovalScreen As Boolean = False) As Boolean


        'Pinkal (12-Oct-2020) --Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ ByVal xDatabaseName As String]

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval,xblnFromApprovalScreen}


        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        mstrMessage = ""
        Try
            Dim objemployee_transfer_tran As clsemployee_transfer_tran
            objemployee_transfer_tran = New clsemployee_transfer_tran

            objemployee_transfer_tran._Transferunkid(objDataOperation) = intUnkid

            Me._Audittype = enAuditType.ADD
            Me._Effectivedate = objemployee_transfer_tran._Effectivedate
            Me._Employeeunkid = objemployee_transfer_tran._Employeeunkid
            Me._Stationunkid = objemployee_transfer_tran._Stationunkid
            Me._Deptgroupunkid = objemployee_transfer_tran._Deptgroupunkid
            Me._Departmentunkid = objemployee_transfer_tran._Departmentunkid
            Me._Sectiongroupunkid = objemployee_transfer_tran._Sectiongroupunkid
            Me._Sectionunkid = objemployee_transfer_tran._Sectionunkid
            Me._Unitgroupunkid = objemployee_transfer_tran._Unitgroupunkid
            Me._Unitunkid = objemployee_transfer_tran._Unitunkid
            Me._Teamunkid = objemployee_transfer_tran._Teamunkid
            Me._Classgroupunkid = objemployee_transfer_tran._Classgroupunkid
            Me._Classunkid = objemployee_transfer_tran._Classunkid
            Me._Changereasonunkid = objemployee_transfer_tran._Changereasonunkid
            Me._Isvoid = False
            Me._Voiduserunkid = -1
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Remark = ""
            Me._Rehiretranunkid = objemployee_transfer_tran._Rehiretranunkid
            Me._Mappingunkid = 0
            Me._Isfinal = False
            Me._Transferunkid = intUnkid
            Me._IsProcessed = False
            Me._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

            objemployee_transfer_tran._Voidreason = Me._Voidreason

            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            'objemployee_transfer_tran._Voiduserunkid = Me._Audituserunkid
            'Gajanan [9-July-2019] -- End
            objemployee_transfer_tran._Voiddatetime = Now
            objemployee_transfer_tran._Isvoid = True
            objemployee_transfer_tran._ClientIP = Me._Ip
            objemployee_transfer_tran._FormName = Me._Form_Name
            objemployee_transfer_tran._HostName = Me._Hostname

            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            objemployee_transfer_tran._AuditUserId = Me._Audituserunkid
            objemployee_transfer_tran._Voiduserunkid = Me._Voiduserunkid
            'Gajanan [9-July-2019] -- End


            ''S.SANDEEP |12-DEC-2019| -- START
            ''ISSUE/ENHANCEMENT : DELETE MOVEMENTS NOT WORKING DUE TO FALSE CONDITION CHECKPOINT
            'If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then
            '    If objemployee_transfer_tran.Delete(intUnkid, intCompanyId, mblnCreateADUserFromEmpMst, objDataOperation) = False Then
            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            ''S.SANDEEP |12-DEC-2019| -- END
            

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'blnFlag = Insert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, mblnCreateADUserFromEmpMst, objDataOperation, xblnFromApprovalScreen, xMdtMigration, xblnFromApproval)
            blnFlag = Insert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, mblnCreateADUserFromEmpMst, xDatabaseName _
                                 , objDataOperation, xblnFromApprovalScreen, xMdtMigration, xblnFromApproval)
            'Pinkal (12-Oct-2020) -- End



            'Gajanan [23-SEP-2019] -- End

            'S.SANDEEP |12-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : DELETE MOVEMENTS NOT WORKING DUE TO FALSE CONDITION CHECKPOINT
            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then

                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objemployee_transfer_tran.Delete(intUnkid, intCompanyId, mblnCreateADUserFromEmpMst, objDataOperation) = False Then
                If objemployee_transfer_tran.Delete(intUnkid, intCompanyId, mblnCreateADUserFromEmpMst, xDatabaseName, objDataOperation) = False Then
                    'Pinkal (12-Oct-2020) -- End
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                End If
            End If
            'S.SANDEEP |12-DEC-2019| -- END

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return blnFlag
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If ex.Message.ToString().Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
                mstrMessage = ex.Message
            Else
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            End If
            'Pinkal (04-Apr-2020) -- End
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEffDate As DateTime, _
                            ByVal xStationId As Integer, _
                            ByVal xDeptGrpId As Integer, _
                            ByVal xDeptUnkid As Integer, _
                            ByVal xSecGrpId As Integer, _
                            ByVal xSecUnkid As Integer, _
                            ByVal xUnitGrpId As Integer, _
                            ByVal xUnitUnkid As Integer, _
                            ByVal xTeamUnkid As Integer, _
                            ByVal xClassGrpId As Integer, _
                            ByVal xClassUnkid As Integer, _
                            ByVal xEmployeeId As Integer, _
                            ByVal xeOprType As clsEmployeeMovmentApproval.enOperationType, _
                            Optional ByVal strtrangUid As String = "", _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = False, _
                            Optional ByVal xTranferUnkid As Integer = 0 _
) As Boolean 'S.SANDEEP |17-JAN-2019| -- START {xTranferUnkid} -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", transferunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectiongroupunkid " & _
              ", sectionunkid " & _
              ", unitgroupunkid " & _
              ", unitunkid " & _
              ", teamunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", changereasonunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb " & _
             "FROM hremployee_transfer_approval_tran " & _
             "WHERE isvoid = 0 and operationtypeid = @operationtypeid " & _
             "AND statusunkid <> 3 "

            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If

            If blnPreventNewEntry Then
                'S.SANDEEP [09-AUG-2018] -- START
                'strQ &= " AND hremployee_transfer_approval_tran.isfinal = 0 "
                strQ &= " AND hremployee_transfer_approval_tran.isfinal = 0 AND ISNULL(isprocessed,0) = 0 "
                'S.SANDEEP [09-AUG-2018] -- END
            End If

            If strtrangUid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid"
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If xStationId > 0 Then
                strQ &= " AND stationunkid = @stationunkid "
                objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xStationId)
            End If

            If xDeptGrpId > 0 Then
                strQ &= " AND deptgroupunkid = @deptgroupunkid "
                objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDeptGrpId)
            End If

            If xDeptUnkid > 0 Then
                strQ &= " AND departmentunkid = @departmentunkid "
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDeptUnkid)
            End If

            If xSecGrpId > 0 Then
                strQ &= " AND sectiongroupunkid = @sectiongroupunkid "
                objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSecGrpId)
            End If

            If xSecUnkid > 0 Then
                strQ &= " AND sectionunkid = @sectionunkid "
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSecUnkid)
            End If

            If xUnitGrpId > 0 Then
                strQ &= " AND unitgroupunkid = @unitgroupunkid "
                objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnitGrpId)
            End If

            If xUnitUnkid > 0 Then
                strQ &= " AND unitunkid = @unitunkid "
                objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnitUnkid)
            End If

            If xTeamUnkid > 0 Then
                strQ &= " AND teamunkid = @teamunkid "
                objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTeamUnkid)
            End If

            If xClassGrpId > 0 Then
                strQ &= " AND classgroupunkid = @classgroupunkid "
                objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClassGrpId)
            End If

            If xClassUnkid > 0 Then
                strQ &= " AND classunkid = @classunkid "
                objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClassUnkid)
            End If

            'S.SANDEEP |17-JAN-2019| -- START
            If xeOprType = clsEmployeeMovmentApproval.enOperationType.EDITED Or mintOperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED Then
                If xTranferUnkid > 0 Then
                    strQ &= " AND transferunkid = @transferunkid "
                    objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTranferUnkid)
                End If
            End If
            'S.SANDEEP |17-JAN-2019| -- END

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strtrangUid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xeOprType)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function MigrationInsert(ByVal xDatabaseName As String, ByVal intCompanyId As Integer, _
                                    ByVal xOprationType As clsEmployeeMovmentApproval.enOperationType, _
                                    ByVal mblnCreateADUserFromEmpMst As Boolean, _
                                    Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                    Optional ByVal xMdtMigration As DataTable = Nothing, _
                                    Optional ByVal xblnFromApproval As Boolean = False, _
                                    Optional ByVal xblnFromApprovalScreen As Boolean = False, _
                                    Optional ByVal eStatusId As clsEmployee_Master.EmpApprovalStatus = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval) As Boolean


        'Pinkal (12-Oct-2020) --Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens. [ByVal xDatabaseName As String]

        'Pinkal (07-Dec-2019) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()
        Try
            If IsNothing(xMdtMigration) = False AndAlso xMdtMigration.Rows.Count > 0 Then
                Dim objMovementMigration As New clsMovementMigration

                Dim objConfig As New clsConfigOptions
                Dim strParamKeys() As String = { _
                                           "EMPLOYEEASONDATE", _
                                           "CLAIMREQUEST_PAYMENTAPPROVALWITHLEAVEAPPROVAL" _
                                           }
                Dim MigrationDicKeyValues As New Dictionary(Of String, String)
                MigrationDicKeyValues = objConfig.GetKeyValue(intCompanyId, strParamKeys, objDataOperation)

                If IsNothing(MigrationDicKeyValues) = False _
                AndAlso MigrationDicKeyValues.ContainsKey("EmployeeAsOnDate") _
                AndAlso MigrationDicKeyValues.ContainsKey("ClaimRequest_PaymentApprovalwithLeaveApproval") Then
                    Dim Formname As String = String.Empty
                    Formname = mstrForm_Name

                    If IsNothing(xMdtMigration) = True Then
                        objMovementMigration.isMovementInApprovalFlow(objMovementMigration._DataList, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, _
                                                                      enApprovalMigrationScreenType.TRANSFER, 0, objDataOperation)

                        If IsNothing(objMovementMigration._DataList) = False Then
                            xMdtMigration = objMovementMigration._DataList
                        End If

                    End If

                    objMovementMigration._Ip = mstrIp
                    objMovementMigration._Hostname = mstrHostname
                    objMovementMigration._Form_Name = mstrForm_Name
                    objMovementMigration._Isweb = mblnIsweb



                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                    'If objMovementMigration.Insert(xMdtMigration, mintTransferunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.TRANSFER), _
                    '                         xblnFromApproval, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
                    '                        CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintAudituserunkid, Formname, _
                    '                       CInt(xOprationType), mblnCreateADUserFromEmpMst, xblnFromApprovalScreen, objDataOperation) = False Then

                    If objMovementMigration.Insert(xDatabaseName, xMdtMigration, mintTransferunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.TRANSFER), _
                                             xblnFromApproval, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
                                            CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintAudituserunkid, Formname, _
                                           CInt(xOprationType), mblnCreateADUserFromEmpMst, xblnFromApprovalScreen, objDataOperation) = False Then

                        'Pinkal (12-Oct-2020) -- End


                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce

                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If

        End Try
    End Function
    'Gajanan [23-SEP-2019] -- End  

End Class
