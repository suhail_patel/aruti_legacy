﻿Option Strict On
'************************************************************************************************************************************
'Class Name :clsEmployeeMovmentApproval.vb
'Purpose    :
'Date       :31-Jul-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsEmployeeMovmentApproval
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeMovmentApproval"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Enum "

    Public Enum enMovementType
        TRANSFERS = 1
        RECATEGORIZE = 2
        PROBATION = 3
        CONFIRMATION = 4
        SUSPENSION = 5
        TERMINATION = 6
        REHIRE = 7
        RETIREMENTS = 8
        WORKPERMIT = 9
        RESIDENTPERMIT = 10
        COSTCENTER = 11
        'Sohail (21 Oct 2019) -- Start
        'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
        EXEMPTION = 12
        'Sohail (21 Oct 2019) -- End
    End Enum

    'S.SANDEEP |17-JAN-2019| -- START
    Public Enum enOperationType
        ADDED = 1
        EDITED = 2
        DELETED = 3
    End Enum
    'S.SANDEEP |17-JAN-2019| -- END

#End Region

#Region " Private Variables "

    Private mintPrivilegeId As Integer
    Private eMovementType As enMovementType
    Private mintEmpId As Integer
    Private eDateType As enEmp_Dates_Transaction
    'S.SANDEEP |17-JAN-2019| -- START
    Private eOperationType As enOperationType
    'S.SANDEEP |17-JAN-2019| -- END

#End Region

#Region " Properties "

    Public ReadOnly Property _PrivilegeId() As Integer
        Get
            Return mintPrivilegeId
        End Get
    End Property

    Public ReadOnly Property _MovementType() As enMovementType
        Get
            Return eMovementType
        End Get
    End Property

    Public ReadOnly Property _EmpId() As Integer
        Get
            Return mintEmpId
        End Get
    End Property

    Public ReadOnly Property _DateType() As enEmp_Dates_Transaction
        Get
            Return eDateType
        End Get
    End Property

    'S.SANDEEP |17-JAN-2019| -- START
    Public ReadOnly Property _OperationType() As enOperationType
        Get
            Return eOperationType
        End Get
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property
    'S.SANDEEP |17-JAN-2019| -- END

#End Region

#Region " Constructor "

    Public Sub New()

    End Sub

    Public Sub New(ByVal intPrivilegeId As Integer, ByVal eMovement As clsEmployeeMovmentApproval.enMovementType, ByVal iEmpId As Integer, ByVal eDate As enEmp_Dates_Transaction, ByVal eOpernType As enOperationType) 'S.SANDEEP |17-JAN-2019| -- START {enOperationType} -- END
        mintPrivilegeId = intPrivilegeId
        eMovementType = eMovement
        mintEmpId = iEmpId
        eDateType = eDate
        'S.SANDEEP |17-JAN-2019| -- START
        eOperationType = eOpernType
        'S.SANDEEP |17-JAN-2019| -- END
    End Sub

#End Region

#Region " Private/Public Methods "

    Public Function MovementListForNotification() As DataTable
        Dim dsList As New DataSet
        Try
            dsList = getMovementList(1, "List")
            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(Boolean)
                .ColumnName = "iCheck"
                .DefaultValue = False
            End With
            dsList.Tables("List").Columns.Add(dcol)


            dcol = New DataColumn
            With dcol
                .DataType = GetType(String)
                .ColumnName = "userids"
                .DefaultValue = ""
            End With

            dsList.Tables("List").Columns.Add(dcol)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: MovementListForNotication; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables("List")
    End Function

    Public Function getMovementList(ByVal intUserId As Integer, Optional ByVal strList As String = "List", Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = "SELECT DISTINCT " & _
                   "     PM.privilegeunkid AS Id " & _
                   "    ,PM.privilege_name AS Name " & _
                   "FROM hrmsConfiguration..cfuser_privilege AS UP " & _
                   "    JOIN hrmsConfiguration..cfuserprivilege_master AS PM ON PM.privilegeunkid = UP.privilegeunkid " & _
                   "WHERE PM.isactive = 1 AND PM.isdeleted = 'N' AND UP.userunkid = @userunkid AND PM.privilegeunkid IN " & _
                   "( " & _
                    CInt(enUserPriviledge.AllowToApproveRejectEmployeeTransfers) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeRecategorize) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeProbation) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeConfirmation) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeSuspension) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeTermination) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeRetirements) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeWorkPermit) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeResidentPermit) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeCostCenter) & _
                    ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeExemption) & _
                   " ) "
            'Sohail (21 Oct 2019) - [AllowToApproveRejectEmployeeExemption]

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            StrQ = ""
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @select AS Name, 0 AS PrivilegeId, 0 AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In dsList.Tables(0).Rows
                    Select Case CInt(row("Id"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeTransfers)
                            StrQ &= "SELECT " & enMovementType.TRANSFERS & " AS Id, @TRANSFERS AS Name, 1192 AS PrivilegeId, 0 AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@TRANSFERS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Transfer"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeRecategorize)
                            StrQ &= "SELECT " & enMovementType.RECATEGORIZE & " AS Id, @RECATEGORIZE AS Name, 1193 AS PrivilegeId, 0 AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@RECATEGORIZE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Re-Categorization"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeProbation)
                            StrQ &= "SELECT " & enMovementType.PROBATION & " AS Id, @PROBATION AS Name, 1194 AS PrivilegeId, " & enEmp_Dates_Transaction.DT_PROBATION & " AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@PROBATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Probation"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeConfirmation)
                            StrQ &= "SELECT " & enMovementType.CONFIRMATION & " AS Id, @CONFIRMATION AS Name, 1195 AS PrivilegeId, " & enEmp_Dates_Transaction.DT_CONFIRMATION & " AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@CONFIRMATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Confirmation"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeSuspension)
                            StrQ &= "SELECT " & enMovementType.SUSPENSION & " AS Id, @SUSPENSION AS Name, 1196 AS PrivilegeId, " & enEmp_Dates_Transaction.DT_SUSPENSION & " AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@SUSPENSION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Suspension"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeTermination)
                            StrQ &= "SELECT " & enMovementType.TERMINATION & " AS Id, @TERMINATION AS Name, 1197 AS PrivilegeId, " & enEmp_Dates_Transaction.DT_TERMINATION & " AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@TERMINATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Termination"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeRetirements)
                            StrQ &= "SELECT " & enMovementType.RETIREMENTS & " AS Id, @RETIREMENTS AS Name, 1198 AS PrivilegeId, " & enEmp_Dates_Transaction.DT_RETIREMENT & " AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@RETIREMENTS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Retirement"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeWorkPermit)
                            StrQ &= "SELECT " & enMovementType.WORKPERMIT & " AS Id, @WORKPERMIT AS Name, 1199 AS PrivilegeId, 0 AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@WORKPERMIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Work-Permit"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeResidentPermit)
                            StrQ &= "SELECT " & enMovementType.RESIDENTPERMIT & " AS Id, @RESIDENTPERMIT AS Name, 1200 AS PrivilegeId, 0 AS DatesId, CAST(1 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@RESIDENTPERMIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Resident-Permit"))
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeCostCenter)
                            StrQ &= "SELECT " & enMovementType.COSTCENTER & " AS Id, @COSTCENTER AS Name, 1201 AS PrivilegeId, 0 AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@COSTCENTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "CostCenter"))
                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        Case CInt(enUserPriviledge.AllowToApproveRejectEmployeeExemption)
                            StrQ &= "SELECT " & enMovementType.EXEMPTION & " AS Id, @EXEMPTION AS Name, " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeExemption) & " AS PrivilegeId, " & enEmp_Dates_Transaction.DT_EXEMPTION & " AS DatesId, CAST(0 AS BIT) AS IsResident UNION "
                            objDataOperation.AddParameter("@EXEMPTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 65, "Exemption"))
                            'Sohail (21 Oct 2019) -- End
                    End Select
                Next
            End If

            If StrQ.Trim.Length > 0 Then StrQ = StrQ.Substring(0, StrQ.Length - 6)

            objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getMovementList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    'S.SANDEEP |17-JAN-2019| -- START
    Public Function getOperationTypeList(Optional ByVal strList As String = "List", Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            If strList.Trim.Length <= 0 Then strList = "List"
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT '" & CInt(enOperationType.ADDED) & "' AS Id, @ADDED AS NAME " & _
                    "UNION SELECT '" & CInt(enOperationType.EDITED) & "' AS Id, @EDITED AS NAME " & _
                    "UNION SELECT '" & CInt(enOperationType.DELETED) & "' AS Id, @DELETED AS NAME "

            objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 61, "Select"))
            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 62, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 63, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 64, "Information Deleted"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getOperationTypeList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'S.SANDEEP |17-JAN-2019| -- END

    Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, ByVal eMovementType As enMovementType, Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = ""
        Try
            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                            strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as branchid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
                                             "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
                            strJoin &= "AND Fn.branchid = [@emp].branchid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

                    Case enAllocation.DEPARTMENT_GROUP
                            strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as deptgrpid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
                                             "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
                            strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

                    Case enAllocation.DEPARTMENT
                            strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as deptid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
                                             "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
                            strJoin &= "AND Fn.deptid = [@emp].deptid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

                    Case enAllocation.SECTION_GROUP
                            strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as secgrpid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
                                             "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
                            strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

                    Case enAllocation.SECTION
                            strSelect &= ",ISNULL(SC.secid,0) AS secid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as secid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
                                             "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(SC.secid,0) > 0 "
                            strJoin &= "AND Fn.secid = [@emp].secid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

                    Case enAllocation.UNIT_GROUP
                            strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as unitgrpid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
                                             "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
                            strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

                    Case enAllocation.UNIT
                            strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as unitid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
                                             "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
                            strJoin &= "AND Fn.unitid = [@emp].unitid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

                    Case enAllocation.TEAM
                            strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as teamid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
                                             "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
                            strJoin &= "AND Fn.teamid = [@emp].teamid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

                    Case enAllocation.JOB_GROUP
                            strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as jgrpid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
                                             "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
                            strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

                    Case enAllocation.JOBS
                            strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as jobid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
                                             "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
                            strJoin &= "AND Fn.jobid = [@emp].jobid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

                    Case enAllocation.CLASS_GROUP
                            strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as clsgrpid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
                                             "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
                            strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
                            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

                    Case enAllocation.CLASSES
                            strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
                            strAccessJoin &= "    LEFT JOIN " & _
                                             "    ( " & _
                                             "        SELECT " & _
                                             "             UPM.userunkid " & _
                                             "            ,allocationunkid as clsid " & _
                                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
                                             "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
                            strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
                            strJoin &= "AND Fn.clsid = [@emp].clsid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = hrclasses_master.classesunkid "
                End Select

                objDataOperation.ClearParameters()
                StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
                Dim strvalue = ""
                objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                strvalue = CStr(objDataOperation.GetParameterValue("@Value"))

                If strvalue.Trim.Length > 0 Then
                    Select Case CInt(strvalues(index))
                        Case enAllocation.BRANCH
                            strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT_GROUP
                            strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT
                            strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

                        Case enAllocation.SECTION_GROUP
                            strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

                        Case enAllocation.SECTION
                            strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

                        Case enAllocation.UNIT_GROUP
                            strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

                        Case enAllocation.UNIT
                            strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

                        Case enAllocation.TEAM
                            strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

                        Case enAllocation.JOB_GROUP
                            strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

                        Case enAllocation.JOBS
                            strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

                        Case enAllocation.CLASS_GROUP
                            strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

                        Case enAllocation.CLASSES
                            strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

                    End Select
                End If
            Next
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, ByVal eMovementType As enMovementType, Optional ByVal xDataOpr As clsDataOperation = Nothing)
    '    Dim objDataOperation As New clsDataOperation
    '    If xDataOpr IsNot Nothing Then
    '        objDataOperation = xDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Dim StrQ As String = ""
    '    Try
    '        If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '        Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
    '        For index As Integer = 0 To strvalues.Length - 1
    '            Select Case CInt(strvalues(index))
    '                Case enAllocation.BRANCH
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as branchid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
    '                                         "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
    '                        strJoin &= "AND Fn.branchid = [@emp].branchid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "
    '                    End If

    '                Case enAllocation.DEPARTMENT_GROUP
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as deptgrpid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
    '                                         "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
    '                        strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "
    '                    End If

    '                Case enAllocation.DEPARTMENT
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as deptid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
    '                                         "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
    '                        strJoin &= "AND Fn.deptid = [@emp].deptid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "
    '                    End If

    '                Case enAllocation.SECTION_GROUP
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as secgrpid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
    '                                         "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
    '                        strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "
    '                    End If

    '                Case enAllocation.SECTION
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(SC.secid,0) AS secid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as secid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
    '                                         "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(SC.secid,0) > 0 "
    '                        strJoin &= "AND Fn.secid = [@emp].secid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "
    '                    End If

    '                Case enAllocation.UNIT_GROUP
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as unitgrpid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
    '                                         "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
    '                        strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "
    '                    End If

    '                Case enAllocation.UNIT
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as unitid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
    '                                         "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
    '                        strJoin &= "AND Fn.unitid = [@emp].unitid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "
    '                    End If

    '                Case enAllocation.TEAM
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as teamid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
    '                                         "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
    '                        strJoin &= "AND Fn.teamid = [@emp].teamid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "
    '                    End If

    '                Case enAllocation.JOB_GROUP
    '                    If eMovementType <> enMovementType.TRANSFERS Then
    '                        strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as jgrpid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
    '                                         "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
    '                        strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "
    '                    End If

    '                Case enAllocation.JOBS
    '                    If eMovementType <> enMovementType.TRANSFERS Then
    '                        strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as jobid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
    '                                         "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
    '                        strJoin &= "AND Fn.jobid = [@emp].jobid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "
    '                    End If

    '                Case enAllocation.CLASS_GROUP
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as clsgrpid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
    '                                         "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
    '                        strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "
    '                    End If

    '                Case enAllocation.CLASSES
    '                    If eMovementType <> enMovementType.RECATEGORIZE Then
    '                        strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
    '                        strAccessJoin &= "    LEFT JOIN " & _
    '                                         "    ( " & _
    '                                         "        SELECT " & _
    '                                         "             UPM.userunkid " & _
    '                                         "            ,allocationunkid as clsid " & _
    '                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
    '                                         "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
    '                        strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
    '                        strJoin &= "AND Fn.clsid = [@emp].clsid "
    '                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = classesunkid "
    '                    End If

    '            End Select

    '            objDataOperation.ClearParameters()
    '            StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
    '            Dim strvalue = ""
    '            objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

    '            objDataOperation.ExecNonQuery(StrQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            End If

    '            strvalue = CStr(objDataOperation.GetParameterValue("@Value"))

    '            If strvalue.Trim.Length > 0 Then
    '                Select Case CInt(strvalues(index))
    '                    Case enAllocation.BRANCH
    '                        strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

    '                    Case enAllocation.DEPARTMENT_GROUP
    '                        strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.DEPARTMENT
    '                        strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

    '                    Case enAllocation.SECTION_GROUP
    '                        strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.SECTION
    '                        strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

    '                    Case enAllocation.UNIT_GROUP
    '                        strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.UNIT
    '                        strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

    '                    Case enAllocation.TEAM
    '                        strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

    '                    Case enAllocation.JOB_GROUP
    '                        strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.JOBS
    '                        strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

    '                    Case enAllocation.CLASS_GROUP
    '                        strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.CLASSES
    '                        strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

    '                End Select
    '            End If
    '        Next
    '        If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
    '        If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Public Function GetEmployeeApprovalData(ByVal strDatabaseName As String _
                                          , ByVal intCompanyId As Integer _
                                          , ByVal intYearId As Integer _
                                          , ByVal strUserAccessMode As String _
                                          , ByVal intPrivilegeId As Integer _
                                          , ByVal intUserId As Integer _
                                          , ByVal intPriorityId As Integer _
                                          , ByVal blnOnlyMyApprovals As Boolean _
                                          , ByVal eMovement As enMovementType _
                                          , ByVal xEmployeeAsOnDate As String _
                                          , ByVal eDates As enEmp_Dates_Transaction _
                                          , ByVal blnIsResidentPermit As Boolean _
                                          , ByVal eOperationType As enOperationType _
                                          , Optional ByVal objDataOpr As clsDataOperation = Nothing) As DataTable 'S.SANDEEP |17-JAN-2019| -- START {enOperationType} -- END
        Dim StrQ As String = String.Empty
        Dim dList As DataTable = Nothing
        Dim oData As DataTable = Nothing
        Try
            'S.SANDEEP |17-JAN-2019| -- START
            'dList = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eMovement, xEmployeeAsOnDate, intUserId, eDates, blnIsResidentPermit, Nothing)
            dList = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eMovement, xEmployeeAsOnDate, intUserId, eDates, blnIsResidentPermit, eOperationType, Nothing)
            'S.SANDEEP |17-JAN-2019| -- END

            oData = dList.Copy()
            If dList.Rows.Count > 0 Then
                Dim blnFlag As Boolean = False
                Dim iPriority As List(Of Integer) = dList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                Dim intSecondLastPriority As Integer = -1
                If iPriority.Min() = intPriorityId Then
                    intSecondLastPriority = intPriorityId
                    blnFlag = True
                Else
                    'S.SANDEEP [14-AUG-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    'intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
                    Try
                    intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
                    Catch ex As Exception
                        intSecondLastPriority = intPriorityId
                        blnFlag = True
                    End Try
                    'S.SANDEEP [14-AUG-2018] -- END
                End If


                'Dim dr() As DataRow = Nothing
                'dr = dList.Select("priority = '" & intSecondLastPriority & "'", "employeeunkid, priority")
                'If dr.Length > 0 Then
                '    Dim row = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(clsEmployee_Master.EmpApprovalStatus.Approved))
                '    Dim strIds As String
                '    If row.Count() > 0 Then
                '        strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList().Except(row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()).Distinct().ToArray())
                '        If blnFlag Then
                '            strIds = ""
                '        End If
                '        If strIds.Trim.Length > 0 Then
                '            oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                '        Else
                '            oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                '        End If
                '    Else
                '        strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                '        If blnFlag = False Then
                '            strIds = " AND employeeunkid NOT IN(" & strIds & ") "
                '        Else
                '            strIds = ""
                '        End If
                '        oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 " & strIds, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                '    End If
                'Else
                '    oData = New DataView(dList, "userunkid = " & intUserId, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable() '& " AND iStatusId = 1 "
                'End If


                oData = New DataView(dList, "userunkid = " & intUserId & " AND mappingunkid <=0 AND (rno = 1) AND employeeunkid <> uempid ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()

                Dim strValue As String = String.Join(",", oData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = 2).GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")) _
                                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("employeeunkid")) _
                                       .Count()}).Where(Function(y) y.barCount = 1).Select(Function(a) a.barid.ToString).ToArray())

                If strValue.Trim.Length > 0 Then
                    oData = New DataView(oData, "employeeunkid NOT IN (" & strValue & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                End If

                'If strValue.Trim.Length <= 0 Then
                '    strValue = String.Join(",", oData.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")) _
                '                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("employeeunkid")) _
                '                       .Count()}).Where(Function(y) y.barCount = 1).Select(Function(a) a.barid.ToString).ToArray())

                '    If strValue.Trim.Length > 0 Then
                '        oData = New DataView(oData, "employeeunkid NOT IN (" & strValue & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                '    End If
                'End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return oData
    End Function

    Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
                                            , ByVal intCompanyId As Integer _
                                            , ByVal intYearId As Integer _
                                            , ByVal strUserAccessMode As String _
                                            , ByVal intPrivilegeId As Integer _
                                            , ByVal eMovement As enMovementType _
                                            , ByVal xEmployeeAsOnDate As String _
                                            , ByVal intUserId As Integer _
                                            , ByVal eDates As enEmp_Dates_Transaction _
                                            , ByVal blnIsResidentPermit As Boolean _
                                            , ByVal eOperationType As enOperationType _
                                            , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                            , Optional ByVal intCurrentPriorityId As Integer = 0 _
                                            , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                            , Optional ByVal mstrFilterString As String = "" _
                                            ) As DataTable 'S.SANDEEP |17-JAN-2019| -- START {enOperationType} -- END
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try

            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""


            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            'Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, 0, eMovement, objDataOperation)

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                  "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQ &= "SELECT DISTINCT " & _
                        "    B" & index.ToString() & ".userunkid " & _
                        "   ,A.employeeunkid " & _
                        "   ,B" & index.ToString() & ".approvertypeid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT DISTINCT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                        "   JOIN #TABLENAME# AS TAT ON TAT.employeeunkid = AEM.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                        "   AND T.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                        "   AND J.Rno = 1 " & _
                        "   WHERE TAT.isprocessed = 0 and TAT.isvoid = 0  " & _
                        ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "       ,CAM.approvertypeid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN  hremp_appusermapping AS CAM ON CAM.mapuserunkid = UPM.userunkid " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = " & intCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        "   AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
                If index < strvalues.Length - 1 Then
                    StrQ &= " INTERSECT "
                End If
            Next

            StrQ &= ") AS Fl "
            'Gajanan [9-NOV-2019] -- End



            Select Case eMovement
                'Case enMovementType.TRANSFERS
                '    StrQ = "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                '           "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                '           "SELECT " & _
                '           "     TAT.employeeunkid " & _
                '           "    ,TAT.departmentunkid " & _
                '           "    ,0 " & _
                '           "    ,TAT.classgroupunkid " & _
                '           "    ,TAT.classunkid " & _
                '           "    ,TAT.stationunkid " & _
                '           "    ,TAT.deptgroupunkid " & _
                '           "    ,TAT.sectiongroupunkid " & _
                '           "    ,TAT.sectionunkid " & _
                '           "    ,TAT.unitgroupunkid " & _
                '           "    ,TAT.unitunkid " & _
                '           "    ,TAT.teamunkid " & _
                '           "    ,0 " & _
                '           "FROM " & strDatabaseName & "..hremployee_transfer_approval_tran AS TAT " & _
                '           "WHERE TAT.isvoid = 0 AND TAT.transferunkid <= 0 AND TAT.statusunkid = 1 AND ISNULL([TAT].[isprocessed],0) = 0 " & _
                '           "SELECT DISTINCT " & _
                '           "     TA.tranguid " & _
                '           "    ,ISNULL(B.[transactiondate],TA.transactiondate) AS transactiondate " & _
                '           "    ,ISNULL(B.[mappingunkid],TA.mappingunkid) AS mappingunkid " & _
                '           "    ,ISNULL(B.[iStatus],@Pending) AS iStatus " & _
                '           "    ,ISNULL(B.[iStatusId],TA.statusunkid) AS iStatusId " & _
                '           "    ,TA.remark " & _
                '           "    ,TA.isfinal " & _
                '           "    ,TA.transferunkid " & _
                '           "    ,CAST(0 AS BIT) AS icheck " & _
                '           "    ,EM.employeecode as ecode " & _
                '           "    ,EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname AS ename " & _
                '           "    ,ISNULL(AST.name,'') AS branch " & _
                '           "    ,ISNULL(ADG.name,'') AS deptgroup " & _
                '           "    ,ISNULL(ADP.name,'') AS dept " & _
                '           "    ,ISNULL(ASG.name,'') AS secgroup " & _
                '           "    ,ISNULL(ASE.name,'') AS section " & _
                '           "    ,ISNULL(AUG.name,'') AS unitgrp " & _
                '           "    ,ISNULL(AUT.name,'') AS unit " & _
                '           "    ,ISNULL(ATM.name,'') AS team " & _
                '           "    ,ISNULL(ACG.name,'') AS classgrp " & _
                '           "    ,ISNULL(ACL.name,'') AS class " & _
                '           "    ,CONVERT(CHAR(8),TA.effectivedate,112) AS efdate " & _
                '           "    ,CONVERT(CHAR(8),EM.appointeddate,112) AS adate " & _
                '           "    ,'' AS EffDate " & _
                '           "    ,CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name, '') END AS CReason " & _
                '           "    ,ISNULL(ADP.departmentunkid,0) AS deptid " & _
                '           "    ,ISNULL(ACG.classgroupunkid,0) AS clsgrpid " & _
                '           "    ,ISNULL(ACL.classesunkid,0) AS clsid " & _
                '           "    ,ISNULL(AST.stationunkid,0) AS branchid " & _
                '           "    ,ISNULL(ADG.deptgroupunkid,0) AS deptgrpid " & _
                '           "    ,ISNULL(ASG.sectiongroupunkid,0) AS secgrpid " & _
                '           "    ,ISNULL(ASE.sectionunkid,0) AS secid " & _
                '           "    ,ISNULL(AUG.unitgroupunkid,0) AS unitgrpid " & _
                '           "    ,ISNULL(AUT.unitunkid,0) AS unitid " & _
                '           "    ,ISNULL(ATM.teamunkid,0) AS teamid " & _
                '           "    ,Fn.userunkid " & _
                '           "    ,Fn.username " & _
                '           "    ,Fn.email " & _
                '           "    ,Fn.priority " & _
                '           "    ,EM.employeeunkid " & _
                '           "    ,EM.companyunkid " & _
                '           "    ,ISNULL(Fn.uempid,0) AS uempid " & _
                '           "    ,ISNULL(Fn.ecompid,0) AS ecompid " & _
                '           "    ,ISNULL(Fn.LvName,'') AS levelname " & _
                '           "    ,TA.changereasonunkid " & _
                '           "    ,ROW_NUMBER()OVER(PARTITION BY EM.employeeunkid ORDER BY Fn.priority ASC) AS rno " & _
                '           "FROM @emp " & _
                '           "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
                '           "    JOIN " & strDatabaseName & "..hremployee_transfer_approval_tran AS TA ON EM.employeeunkid = TA.employeeunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = TA.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRANSFERS & _
                '           "    LEFT JOIN " & strDatabaseName & "..cfcommon_master AS RH ON RH.masterunkid = TA.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrstation_master AS AST ON AST.stationunkid = TA.stationunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master AS ADG ON ADG.deptgroupunkid = TA.deptgroupunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS ADP ON ADP.departmentunkid = TA.departmentunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master AS ASG ON TA.sectiongroupunkid = ASG.sectiongroupunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrsection_master AS ASE ON ASE.sectionunkid = TA.sectionunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrunitgroup_master AS AUG ON TA.unitgroupunkid = AUG.unitgroupunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrunit_master AS AUT ON AUT.unitunkid = TA.unitunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrteam_master AS ATM ON TA.teamunkid = ATM.teamunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ACG ON ACG.classgroupunkid = TA.classgroupunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ACL ON ACL.classesunkid = TA.classunkid " & _
                '           "    JOIN " & _
                '           "    ( " & _
                '           "        SELECT DISTINCT " & _
                '           "             cfuser_master.userunkid " & _
                '           "            ,cfuser_master.username " & _
                '           "            ,cfuser_master.email " & _
                '                         strSelect & " " & _
                '           "            ,LM.priority " & _
                '           "            ,LM.empapplevelname AS LvName " & _
                '           "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                '           "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                '           "        FROM hrmsConfiguration..cfuser_master " & _
                '           "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                '           "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                '                        strAccessJoin & " " & _
                '           "        JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                '           "        JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                '           "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
                '    If strFilter.Trim.Length > 0 Then
                '        StrQ &= " AND (" & strFilter & " ) "
                '    End If
                '    StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                '           "            AND companyunkid = @C AND yearunkid = @Y AND privilegeunkid = @P  " & _
                '            ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
                '           "LEFT JOIN " & _
                '           "( " & _
                '           "    SELECT " & _
                '           "         [HA].[mapuserunkid] " & _
                '           "        ,[HM].[priority] " & _
                '           "        ,[HTAT].[statusunkid] AS iStatusId " & _
                '           "        ,[HTAT].[transactiondate] " & _
                '           "        ,[HTAT].[mappingunkid] " & _
                '           "        ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                '           "              WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ '+ CM.[username] + ' ]' " & _
                '           "              WHEN [HTAT].[statusunkid] = 3 THEN @Reject+' : [ '+ CM.[username] + ' ]' " & _
                '           "         ELSE @PendingApproval END AS iStatus " & _
                '           "        ,ROW_NUMBER()OVER(PARTITION     BY [HTAT].[employeeunkid] ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                '           "        ,HTAT.[employeeunkid] " & _
                '           "    FROM [dbo].[hremployee_transfer_approval_tran] AS HTAT " & _
                '           "        JOIN [dbo].[hremp_appusermapping] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] " & _
                '           "        JOIN [dbo].[hrempapproverlevel_master] AS HM ON [HA].[empapplevelunkid] = [HM].[empapplevelunkid] " & _
                '           "        LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                '           "    WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND     [HTAT].[isvoid] = 0 AND [HA].[approvertypeid] = " & enEmpApproverType.APPR_MOVEMENT & " AND [HM].[approvertypeid] = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                '            "    AND [HTAT].[transferunkid] <= 0 AND ISNULL([HTAT].[isprocessed],0) = 0 " & _
                '           ") AS B ON B.[employeeunkid] = TA.[employeeunkid] AND [B].[priority] = [Fn].[priority] " & _
                '           strOuterJoin & " " & _
                '           "WHERE 1 = 1 AND TA.isvoid = 0 AND TA.transferunkid <= 0 AND [TA].[statusunkid] = 1 AND ISNULL([TA].[isprocessed],0) = 0  "
                '    'S.SANDEEP [09-AUG-2018] -- START
                '    ''"    AND [HTAT].[transferunkid] <= 0 " ------------ > ADDED
                '    'S.SANDEEP [09-AUG-2018] -- END

                'Case enMovementType.RECATEGORIZE
                '    StrQ = "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                '           "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                '           "SELECT " & _
                '           "     TAT.employeeunkid " & _
                '           "    ,0 " & _
                '           "    ,TAT.jobunkid " & _
                '           "    ,0 " & _
                '           "    ,0 " & _
                '           "    ,0 " & _
                '           "    ,0 " & _
                '           "    ,0 " & _
                '           "    ,0 " & _
                '           "    ,0 " & _
                '           "    ,0 " & _
                '           "    ,0 " & _
                '           "    ,TAT.jobgroupunkid " & _
                '           "FROM " & strDatabaseName & "..hremployee_categorization_approval_tran AS TAT " & _
                '           "WHERE TAT.isvoid = 0 AND TAT.categorizationtranunkid <= 0 AND TAT.statusunkid = 1 AND ISNULL([TAT].[isprocessed],0) = 0  " & _
                '           "SELECT " & _
                '           "     TA.tranguid " & _
                '           "    ,ISNULL(B.[transactiondate],TA.transactiondate) AS transactiondate " & _
                '           "    ,ISNULL(B.[mappingunkid],TA.mappingunkid) AS mappingunkid " & _
                '           "    ,ISNULL(B.[iStatus],@Pending) AS iStatus " & _
                '           "    ,ISNULL(B.[iStatusId],TA.statusunkid) AS iStatusId " & _
                '           "    ,TA.remark " & _
                '           "    ,TA.isfinal " & _
                '           "    ,TA.categorizationtranunkid " & _
                '           "    ,EM.employeecode as ecode " & _
                '           "    ,CAST(0 AS BIT) AS icheck " & _
                '           "    ,EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname AS ename " & _
                '           "    ,ISNULL(AJG.name,'') AS JobGroup " & _
                '           "    ,ISNULL(AJB.job_name,'') AS Job " & _
                '           "    ,ISNULL(AGM.name,'') AS Grade " & _
                '           "    ,ISNULL(AGL.name,'') AS GradeLevel " & _
                '           "    ,CONVERT(CHAR(8),TA.effectivedate,112) AS efdate " & _
                '           "    ,CONVERT(CHAR(8),EM.appointeddate,112) AS adate " & _
                '           "    ,'' AS EffDate " & _
                '           "    ,CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                '           "    ,ISNULL(AJG.jobgroupunkid,0) AS jobgroupunkid " & _
                '           "    ,ISNULL(AJB.jobunkid,0) AS jobunkid " & _
                '           "    ,ISNULL(AGM.gradeunkid,0) AS gradeunkid " & _
                '           "    ,ISNULL(AGL.gradelevelunkid,0) AS gradelevelunkid " & _
                '           "    ,Fn.userunkid " & _
                '           "    ,Fn.username " & _
                '           "    ,Fn.email " & _
                '           "    ,Fn.priority " & _
                '           "    ,EM.employeeunkid " & _
                '           "    ,ISNULL(Fn.uempid,0) AS uempid " & _
                '           "    ,ISNULL(Fn.ecompid,0) AS ecompid " & _
                '           "    ,ISNULL(Fn.LvName,'') AS levelname " & _
                '           "    ,TA.changereasonunkid " & _
                '           "    ,ROW_NUMBER()OVER(PARTITION BY EM.employeeunkid ORDER BY Fn.priority ASC) AS rno " & _
                '           "FROM @emp " & _
                '           "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
                '           "    JOIN " & strDatabaseName & "..hremployee_categorization_approval_tran AS TA ON EM.employeeunkid = TA.employeeunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrjobgroup_master AJG ON AJG.jobgroupunkid = TA.jobgroupunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrjob_master AS AJB ON AJB.jobunkid = TA.jobunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrgrade_master AS AGM ON AGM.gradeunkid = TA.gradeunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..hrgradelevel_master AS AGL ON AGL.gradelevelunkid = TA.gradelevelunkid " & _
                '           "    LEFT JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = TA.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RECATEGORIZE & _
                '           "    LEFT JOIN " & strDatabaseName & "..cfcommon_master AS RH ON RH.masterunkid = TA.changereasonunkid AND RH.mastertype =  " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                '           "    JOIN " & _
                '           "    ( " & _
                '           "        SELECT DISTINCT " & _
                '           "             cfuser_master.userunkid " & _
                '           "            ,cfuser_master.username " & _
                '           "            ,cfuser_master.email " & _
                '                         strSelect & " " & _
                '           "            ,LM.priority " & _
                '           "            ,LM.empapplevelname AS LvName " & _
                '           "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                '           "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                '           "        FROM hrmsConfiguration..cfuser_master " & _
                '           "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                '           "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                '                        strAccessJoin & " " & _
                '           "        JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                '           "        JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                '           "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
                '    If strFilter.Trim.Length > 0 Then
                '        StrQ &= " AND (" & strFilter & " ) "
                '    End If
                '    StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                '           "            AND companyunkid = @C AND yearunkid = @Y AND privilegeunkid = @P  " & _
                '           ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
                '           "LEFT JOIN " & _
                '           "( " & _
                '           "    SELECT " & _
                '           "         [HA].[mapuserunkid] " & _
                '           "        ,[HM].[priority] " & _
                '           "        ,[HTAT].[statusunkid] AS iStatusId " & _
                '           "        ,[HTAT].[transactiondate] " & _
                '           "        ,[HTAT].[mappingunkid] " & _
                '           "        ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                '           "              WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ '+ CM.[username] + ' ]' " & _
                '           "              WHEN [HTAT].[statusunkid] = 3 THEN @Reject+' : [ '+ CM.[username] + ' ]' " & _
                '           "         ELSE @PendingApproval END AS iStatus " & _
                '           "        ,ROW_NUMBER()OVER(PARTITION     BY [HTAT].[employeeunkid] ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                '           "        ,HTAT.[employeeunkid] " & _
                '           "    FROM [dbo].[hremployee_categorization_approval_tran] AS HTAT " & _
                '           "        JOIN [dbo].[hremp_appusermapping] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] " & _
                '           "        JOIN [dbo].[hrempapproverlevel_master] AS HM ON [HA].[empapplevelunkid] = [HM].[empapplevelunkid] " & _
                '           "        LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                '           "    WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND     [HTAT].[isvoid] = 0 AND [HA].[approvertypeid] = " & enEmpApproverType.APPR_MOVEMENT & " AND [HM].[approvertypeid] = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                '           "        AND [HTAT].[categorizationtranunkid] <= 0 AND ISNULL([HTAT].[isprocessed],0) = 0 " & _
                '           ") AS B ON B.[employeeunkid] = TA.[employeeunkid] AND [B].[priority] = [Fn].[priority] " & _
                '           strOuterJoin & " " & _
                '           "WHERE 1 = 1 AND TA.isvoid = 0 AND TA.categorizationtranunkid <= 0 AND TA.statusunkid = 1 AND ISNULL([TA].[isprocessed],0) = 0 "

                '    'S.SANDEEP [09-AUG-2018] -- START
                '    ''"    AND [HTAT].[categorizationtranunkid] <= 0 " ------------ > ADDED
                '    'S.SANDEEP [09-AUG-2018] -- END

                Case Else

                    Dim StrTableName As String = String.Empty
                    Dim StrInnColNames As String = String.Empty
                    Dim StrInnQry As String = String.Empty
                    Dim StrInnJoin As String = String.Empty
                    Dim StrTranUnkidCol As String = String.Empty
                    Dim StrExtraFilter As String = String.Empty
                    Dim StrTypeFilter As String = String.Empty

                    Dim strUACJoin, strUACFilter As String
                    strUACJoin = "" : strUACFilter = ""
                    modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM", True)

                    Select Case eMovement
                        'S.SANDEEP [18-SEP-2018] -- START
                Case enMovementType.TRANSFERS
                            StrInnColNames = "    ,CONVERT(CHAR(8),TA.effectivedate,112) AS efdate " & _
                                             "    ,CONVERT(CHAR(8),EM.appointeddate,112) AS adate " & _
                                             "    ,'' AS EffDate " & _
                                             "    ,CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name, '') END AS CReason " & _
                           "    ,ISNULL(AST.name,'') AS branch " & _
                           "    ,ISNULL(ADG.name,'') AS deptgroup " & _
                           "    ,ISNULL(ADP.name,'') AS dept " & _
                           "    ,ISNULL(ASG.name,'') AS secgroup " & _
                           "    ,ISNULL(ASE.name,'') AS section " & _
                           "    ,ISNULL(AUG.name,'') AS unitgrp " & _
                           "    ,ISNULL(AUT.name,'') AS unit " & _
                           "    ,ISNULL(ATM.name,'') AS team " & _
                           "    ,ISNULL(ACG.name,'') AS classgrp " & _
                           "    ,ISNULL(ACL.name,'') AS class " & _
                           "    ,ISNULL(ADP.departmentunkid,0) AS deptid " & _
                           "    ,ISNULL(ACG.classgroupunkid,0) AS clsgrpid " & _
                           "    ,ISNULL(ACL.classesunkid,0) AS clsid " & _
                           "    ,ISNULL(AST.stationunkid,0) AS branchid " & _
                           "    ,ISNULL(ADG.deptgroupunkid,0) AS deptgrpid " & _
                           "    ,ISNULL(ASG.sectiongroupunkid,0) AS secgrpid " & _
                           "    ,ISNULL(ASE.sectionunkid,0) AS secid " & _
                           "    ,ISNULL(AUG.unitgroupunkid,0) AS unitgrpid " & _
                           "    ,ISNULL(AUT.unitunkid,0) AS unitid " & _
                                             "    ,ISNULL(ATM.teamunkid,0) AS teamid "

                            StrTableName = " " & strDatabaseName & "..hremployee_transfer_approval_tran "

                            StrInnJoin = "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = TA.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRANSFERS & _
                                         "  LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = TA.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & " " & _
                                         "  LEFT JOIN hrstation_master AS AST ON AST.stationunkid = TA.stationunkid " & _
                                         "  LEFT JOIN hrdepartment_group_master AS ADG ON ADG.deptgroupunkid = TA.deptgroupunkid " & _
                                         "  LEFT JOIN hrdepartment_master AS ADP ON ADP.departmentunkid = TA.departmentunkid " & _
                                         "  LEFT JOIN hrsectiongroup_master AS ASG ON TA.sectiongroupunkid = ASG.sectiongroupunkid " & _
                                         "  LEFT JOIN hrsection_master AS ASE ON ASE.sectionunkid = TA.sectionunkid " & _
                                         "  LEFT JOIN hrunitgroup_master AS AUG ON TA.unitgroupunkid = AUG.unitgroupunkid " & _
                                         "  LEFT JOIN hrunit_master AS AUT ON AUT.unitunkid = TA.unitunkid " & _
                                         "  LEFT JOIN hrteam_master AS ATM ON TA.teamunkid = ATM.teamunkid " & _
                                         "  LEFT JOIN hrclassgroup_master AS ACG ON ACG.classgroupunkid = TA.classgroupunkid " & _
                                         "  LEFT JOIN hrclasses_master AS ACL ON ACL.classesunkid = TA.classunkid "

                            StrTranUnkidCol = "transferunkid"

                Case enMovementType.RECATEGORIZE
                            StrInnColNames = "    ,CONVERT(CHAR(8),TA.effectivedate,112) AS efdate " & _
                                             "    ,CONVERT(CHAR(8),EM.appointeddate,112) AS adate " & _
                                             "    ,'' AS EffDate " & _
                                             "    ,CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                           "    ,ISNULL(AJG.name,'') AS JobGroup " & _
                           "    ,ISNULL(AJB.job_name,'') AS Job " & _
                           "    ,ISNULL(AGM.name,'') AS Grade " & _
                           "    ,ISNULL(AGL.name,'') AS GradeLevel " & _
                           "    ,ISNULL(AJG.jobgroupunkid,0) AS jobgroupunkid " & _
                           "    ,ISNULL(AJB.jobunkid,0) AS jobunkid " & _
                           "    ,ISNULL(AGM.gradeunkid,0) AS gradeunkid " & _
                                             "    ,ISNULL(AGL.gradelevelunkid,0) AS gradelevelunkid "

                            StrTableName = " " & strDatabaseName & "..hremployee_categorization_approval_tran "

                            StrInnJoin = "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = TA.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RECATEGORIZE & _
                                         "  LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = TA.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & " " & _
                                         "  LEFT JOIN hrjobgroup_master AJG ON AJG.jobgroupunkid = TA.jobgroupunkid " & _
                                         "  LEFT JOIN hrjob_master AS AJB ON AJB.jobunkid = TA.jobunkid " & _
                                         "  LEFT JOIN hrgrade_master AS AGM ON AGM.gradeunkid = TA.gradeunkid " & _
                                         "  LEFT JOIN hrgradelevel_master AS AGL ON AGL.gradelevelunkid = TA.gradelevelunkid "

                            StrTranUnkidCol = "categorizationtranunkid"

                            'S.SANDEEP [18-SEP-2018] -- END

                        Case enMovementType.CONFIRMATION, enMovementType.PROBATION, enMovementType.RETIREMENTS, enMovementType.SUSPENSION, enMovementType.TERMINATION, enMovementType.EXEMPTION
                            'Sohail (21 Oct 2019) - [EXEMPTION]

                            Dim xMasterType As Integer = 0
                            Select Case eDates
                                Case enEmp_Dates_Transaction.DT_PROBATION
                                    xMasterType = clsCommon_Master.enCommonMaster.PROBATION
                                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                    xMasterType = clsCommon_Master.enCommonMaster.CONFIRMATION
                                Case enEmp_Dates_Transaction.DT_SUSPENSION
                                    xMasterType = clsCommon_Master.enCommonMaster.SUSPENSION
                                Case enEmp_Dates_Transaction.DT_TERMINATION
                                    xMasterType = clsCommon_Master.enCommonMaster.TERMINATION
                                Case enEmp_Dates_Transaction.DT_REHIRE
                                    xMasterType = clsCommon_Master.enCommonMaster.RE_HIRE
                                Case enEmp_Dates_Transaction.DT_RETIREMENT
                                    xMasterType = clsCommon_Master.enCommonMaster.RETIREMENTS
                                    'Sohail (21 Oct 2019) -- Start
                                    'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                                Case enEmp_Dates_Transaction.DT_EXEMPTION
                                    xMasterType = clsCommon_Master.enCommonMaster.EXEMPTION
                                    'Sohail (21 Oct 2019) -- End
                            End Select

                            StrInnColNames = ", CONVERT(CHAR(8),TA.effectivedate,112) AS efdate " & _
                                             ", CONVERT(CHAR(8),TA.date1,112) AS edate1 " & _
                                             ", ISNULL(CONVERT(CHAR(8),TA.date2,112),'') AS edate2 " & _
                                             ", TA.isexclude_payroll " & _
                                             ", '' AS EffDate " & _
                                             ", '' AS dDate1 " & _
                                             ", '' AS dDate2 " & _
                                             ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') WHEN TA.leaveissueunkid > 0 THEN ISNULL(lvleavetype_master.leavename, '') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                                             ", ISNULL(TA.leaveissueunkid, 0) AS leaveissueunkid " & _
                                             ", TA.actualdate " & _
                                             ", '' AS acdate " & _
                                             ", ta.disciplinefileunkid "
                            'Sohail (21 Oct 2019) - [leavename]

                            StrTableName = " " & strDatabaseName & "..hremployee_dates_approval_tran "

                            StrInnJoin = "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = TA.changereasonunkid AND cfcommon_master.mastertype = " & xMasterType & _
                                         "  LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = TA.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE

                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            StrInnJoin &= "  LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = TA.leaveissueunkid " & _
                                            "  LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid "
                            'Sohail (21 Oct 2019) -- End


                            StrTranUnkidCol = "datestranunkid"

                            StrExtraFilter = " AND TA.datetypeunkid = " & eDates
                            StrTypeFilter = " AND TAT.datetypeunkid = " & eDates

                        Case enMovementType.COSTCENTER
                            StrInnColNames = " ,CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                                             " ,CASE WHEN TA.istransactionhead = 1 THEN ISNULL(prtranhead_master.trnheadname,'') " & _
                                             "    ELSE ISNULL(CCT.costcentername,'') END AS DispValue " & _
                                             " ,ISNULL(CCT.costcentername,'') AS costcentername " & _
                                             " ,CONVERT(CHAR(8),TA.effectivedate,112) AS efdate " & _
                                             " ,'' AS EffDate " & _
                                             " ,TA.cctranheadvalueid " & _
                                             " ,TA.istransactionhead " & _
                                             " ,ISNULL(prtranhead_master.trnheadname,'') AS trnheadname "


                            StrTableName = " " & strDatabaseName & "..hremployee_cctranhead_approval_tran "

                            StrInnJoin = "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = TA.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.COST_CENTER & _
                                         "  LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = TA.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                                         "  LEFT JOIN prcostcenter_master AS CCT ON CCT.costcenterunkid = TA.cctranheadvalueid  AND TA.istransactionhead = 0 " & _
                                         "  LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = TA.cctranheadvalueid AND TA.istransactionhead = 1 "

                            StrTranUnkidCol = "cctranheadunkid"

                            StrExtraFilter = " AND TA.istransactionhead = 0  "
                            StrTypeFilter = " AND TAT.istransactionhead = 0  "

                        Case enMovementType.RESIDENTPERMIT, enMovementType.WORKPERMIT

                            StrInnColNames = ", CONVERT(CHAR(8),TA.effectivedate,112) AS efdate " & _
                                             ", ISNULL(CONVERT(CHAR(8),TA.issue_date,112),'') AS isdate " & _
                                             ", ISNULL(CONVERT(CHAR(8),TA.expiry_date,112),'') AS exdate " & _
                                             ", '' AS EffDate " & _
                                             ", '' AS IDate " & _
                                             ", '' AS EDate " & _
                                             ", TA.work_permit_no " & _
                                             ", TA.issue_place " & _
                                             ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE " & _
                                             "        CASE WHEN ISNULL(TA.isresidentpermit,0) <=0 THEN ISNULL(cfcommon_master.name,'') " & _
                                             "        ELSE ISNULL(RP.name,'') END " & _
                                             "  END AS CReason " & _
                                             ", ISNULL(CM.country_name,'') AS Country " & _
                                             ", ISNULL(TA.workcountryunkid,0) AS workcountryunkid " & _
                                             ", ISNULL(TA.isresidentpermit,0) AS isresidentpermit "

                            StrTableName = " " & strDatabaseName & "..hremployee_permit_approval_tran "

                            StrInnJoin = "    LEFT JOIN hrmsConfiguration..cfcountry_master AS CM ON CM.countryunkid = TA.workcountryunkid " & _
                                         "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = TA.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.WORK_PERMIT & _
                                         "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = TA.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                                         "    LEFT JOIN cfcommon_master AS RP ON RP.masterunkid = TA.changereasonunkid AND RP.mastertype = " & clsCommon_Master.enCommonMaster.RESIDENT_PERMIT

                            StrTranUnkidCol = "workpermittranunkid"

                            If blnIsResidentPermit Then
                                StrExtraFilter = "AND ISNULL(TA.isresidentpermit,0) = 1 "
                            Else
                                StrExtraFilter = "AND ISNULL(TA.isresidentpermit,0) = 0 "
                            End If

                            If blnIsResidentPermit Then
                                StrTypeFilter = "AND ISNULL(TAT.isresidentpermit,0) = 1 "
                            Else
                                StrTypeFilter = "AND ISNULL(TAT.isresidentpermit,0) = 0 "
                            End If

                    End Select

                    Dim strApprovalDataJoin As String = String.Empty

                    strApprovalDataJoin = "LEFT JOIN " & _
                                          "( " & _
                                          "    SELECT " & _
                                          "         [HA].[mapuserunkid] " & _
                                          "        ,[HM].[priority] " & _
                                          "        ,[HTAT].[statusunkid] AS iStatusId " & _
                                          "        ,[HTAT].[transactiondate] " & _
                                          "        ,[HTAT].[mappingunkid] "

                    'Gajanan [17-AUG-2019] -- Start      
                    Select Case eMovement
                        Case enMovementType.CONFIRMATION, enMovementType.PROBATION, enMovementType.RETIREMENTS, enMovementType.SUSPENSION, enMovementType.TERMINATION, enMovementType.EXEMPTION
                            'Sohail (21 Oct 2019) - [EXEMPTION]
                            strApprovalDataJoin &= "        ,[HTAT].[datetypeunkid] "
                    End Select
                    'Gajanan [17-AUG-2019] -- End

                    strApprovalDataJoin &= "        ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                                          "              WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ '+ CM.[username] + ' ]' " & _
                                          "              WHEN [HTAT].[statusunkid] = 3 THEN @Reject+' : [ '+ CM.[username] + ' ]' " & _
                                          "         ELSE @PendingApproval END AS iStatus " & _
                                          "        ,ROW_NUMBER()OVER(PARTITION     BY [HTAT].[employeeunkid] ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                                          "        ,HTAT.[employeeunkid] " & _
                                          "    FROM " & StrTableName & " AS HTAT " & _
                                          "        JOIN [dbo].[hremp_appusermapping] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] " & _
                                          "        JOIN [dbo].[hrempapproverlevel_master] AS HM ON [HA].[empapplevelunkid] = [HM].[empapplevelunkid] " & _
                                          "        LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                                          "    WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND     [HTAT].[isvoid] = 0 AND [HA].[approvertypeid] = " & enEmpApproverType.APPR_MOVEMENT & " AND [HM].[approvertypeid] = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                                          "     AND #HTATCND# " & _
                                          ") AS B ON B.[employeeunkid] = TA.[employeeunkid] AND [B].[priority] = [Fn].[priority]  "


                    'Gajanan [17-AUG-2019] -- Start      
                    Select Case eMovement
                        Case enMovementType.CONFIRMATION, enMovementType.PROBATION, enMovementType.RETIREMENTS, enMovementType.SUSPENSION, enMovementType.TERMINATION, enMovementType.EXEMPTION
                            'Sohail (21 Oct 2019) - [EXEMPTION]
                            strApprovalDataJoin &= " AND TA.datetypeunkid = B.datetypeunkid "
                    End Select
                    'Gajanan [17-AUG-2019] -- End

                    'Gajanan [9-July-2019] -- Add  AND [B].[priority] = [Fn].[priority] AND TA.datetypeunkid = B.datetypeunkid      
                   

                    'S.SANDEEP [09-AUG-2018] -- START
                    '"     AND [HTAT].[" & StrTranUnkidCol & "] <= 0 " ------------ > ADDED
                    'S.SANDEEP [09-AUG-2018] -- END

                    'S.SANDEEP |17-JAN-2019| -- START
                    '----- REMOVED : [HTAT].[" & StrTranUnkidCol & "] <= 0 AND ISNULL([HTAT].[isprocessed],0) = 0
                    '----- ADDED : #HTATCND
                    Dim StrCommonFilter As String = String.Empty
                    Select Case eOperationType
                        Case enOperationType.ADDED
                            StrCommonFilter = " TAT." & StrTranUnkidCol & " <= 0 "
                        Case enOperationType.EDITED
                            StrCommonFilter = " TAT." & StrTranUnkidCol & " > 0 "
                        Case enOperationType.DELETED
                            StrCommonFilter = " TAT." & StrTranUnkidCol & " > 0 "
                    End Select
                    StrCommonFilter &= " AND ISNULL(TAT.isprocessed,0) = 0 AND ISNULL(TAT.isvoid,0) = 0 AND TAT.operationtypeid  = '" & CInt(eOperationType) & "' "
                    'S.SANDEEP |17-JAN-2019| -- END

                    StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                           "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                           "SELECT DISTINCT " & _
                           "     TAT.employeeunkid " & _
                           "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                           "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                           "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                           "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                           "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                           "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                           "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                           "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                           "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                           "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                           "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                           "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                           "FROM " & StrTableName & " AS TAT " & _
                           "    JOIN " & strDatabaseName & "..hremployee_master AS AEM ON TAT.employeeunkid = AEM.employeeunkid "
                    If strUACJoin.Trim.Length > 0 Then
                        StrQ &= strUACJoin
                    End If
                    StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        stationunkid " & _
                            "       ,deptgroupunkid " & _
                            "       ,departmentunkid " & _
                            "       ,sectiongroupunkid " & _
                            "       ,sectionunkid " & _
                            "       ,unitgroupunkid " & _
                            "       ,unitunkid " & _
                            "       ,teamunkid " & _
                            "       ,classgroupunkid " & _
                            "       ,classunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                            ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobgroupunkid " & _
                            "       ,jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                            ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 "

                    'S.SANDEEP |17-JAN-2019| -- START
                    'StrQ &= "WHERE TAT.isvoid = 0 AND TAT." & StrTranUnkidCol & " <= 0 " & StrTypeFilter & " AND TAT.statusunkid = 1 AND ISNULL([TAT].[isprocessed],0) = 0 "
                    StrQ &= "WHERE TAT.isvoid = 0 AND " & StrCommonFilter & " " & StrTypeFilter & " AND TAT.statusunkid = 1 "
                    'S.SANDEEP |17-JAN-2019| -- END



                    'Gajanan [9-NOV-2019] -- Start   
                    'Enhancement:Worked On NMB Bio Data Approval Query Optimization   


                    'StrQ &= "SELECT " & _
                    '        "     CAST(0 AS BIT) AS iCheck " & _
                    '        "    ,TA.tranguid " & _
                    '        "    ,ISNULL(B.[transactiondate],TA.transactiondate) AS transactiondate " & _
                    '        "    ,ISNULL(B.[mappingunkid],TA.mappingunkid) AS mappingunkid " & _
                    '        "    ,ISNULL(B.[iStatus],@Pending) AS iStatus " & _
                    '        "    ,ISNULL(B.[iStatusId],TA.statusunkid) AS iStatusId " & _
                    '        "    ,TA.remark " & _
                    '        "    ,TA.isfinal " & _
                    '        "    ,TA.statusunkid " & _
                    '        "    ,TA." & StrTranUnkidCol & " " & _
                    '        "    ,EM.employeecode as ecode " & _
                    '        "    ,EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname AS ename " & _
                    '        "    " & StrInnColNames & " " & _
                    '        "    ,CONVERT(CHAR(8),EM.appointeddate,112) AS adate " & _
                    '        "    ,Fn.userunkid " & _
                    '        "    ,Fn.username " & _
                    '        "    ,Fn.email " & _
                    '        "    ,Fn.priority " & _
                    '        "    ,EM.employeeunkid " & _
                    '        "    ,ISNULL(Fn.uempid,0) AS uempid " & _
                    '       "     ,ISNULL(Fn.ecompid,0) AS ecompid " & _
                    '        "    ,ISNULL(Fn.LvName,'') AS levelname " & _
                    '        "    ,TA.changereasonunkid " & _
                    '        "    ,ROW_NUMBER()OVER(PARTITION BY EM.employeeunkid ORDER BY Fn.priority ASC) AS rno " & _
                    '        "FROM @emp " & _
                    '        "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
                    '        "    JOIN " & StrTableName & " AS TA ON EM.employeeunkid = TA.employeeunkid " & _
                    '        StrInnJoin & _
                    '        "    JOIN " & _
                    '        "    ( " & _
                    '        "        SELECT DISTINCT " & _
                    '        "             cfuser_master.userunkid " & _
                    '        "            ,cfuser_master.username " & _
                    '        "            ,cfuser_master.email " & _
                    '                      strSelect & " " & _
                    '        "            ,LM.priority " & _
                    '        "            ,LM.empapplevelname AS LvName " & _
                    '        "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                    '        "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                    '        "        FROM hrmsConfiguration..cfuser_master " & _
                    '        "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                    '        "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                    '                     strAccessJoin & " " & _
                    '        "        JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                    '        "        JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                    '        "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
                    'If strFilter.Trim.Length > 0 Then
                    '    StrQ &= " AND (" & strFilter & " ) "
                    'End If



                    StrQ &= "SELECT DISTINCT " & _
                            "     CAST(0 AS BIT) AS iCheck " & _
                            "    ,TA.tranguid " & _
                            "    ,ISNULL(B.[transactiondate],TA.transactiondate) AS transactiondate " & _
                            "    ,ISNULL(B.[mappingunkid],TA.mappingunkid) AS mappingunkid " & _
                            "    ,ISNULL(B.[iStatus],@Pending) AS iStatus " & _
                            "    ,ISNULL(B.[iStatusId],TA.statusunkid) AS iStatusId " & _
                            "    ,TA.remark " & _
                            "    ,TA.isfinal " & _
                            "    ,TA.statusunkid " & _
                            "    ,TA." & StrTranUnkidCol & " " & _
                            "    ,EM.employeecode as ecode " & _
                            "    ,EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname AS ename " & _
                            "    " & StrInnColNames & " " & _
                            "    ,CONVERT(CHAR(8),EM.appointeddate,112) AS adate " & _
                            "    ,Fn.userunkid " & _
                            "    ,Fn.username " & _
                            "    ,Fn.email " & _
                            "    ,Fn.priority " & _
                            "    ,EM.employeeunkid " & _
                            "    ,ISNULL(Fn.uempid,0) AS uempid " & _
                           "     ,ISNULL(Fn.ecompid,0) AS ecompid " & _
                            "    ,ISNULL(Fn.LvName,'') AS levelname " & _
                            "    ,TA.changereasonunkid "
                    '"  --,ROW_NUMBER()OVER(PARTITION BY EM.employeeunkid ORDER BY Fn.priority ASC) AS rno " & _

                    StrQ &= ",DENSE_RANK() OVER (PARTITION BY EM.employeeunkid, B.mappingunkid  ORDER BY Fn.priority ASC) AS rno " & _
                            "FROM @emp " & _
                           "    JOIN #USR ON [@emp].empid = #USR.employeeunkid " & _
                            "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
                            "    JOIN " & StrTableName & " AS TA ON EM.employeeunkid = TA.employeeunkid " & _
                            StrInnJoin & _
                            "    JOIN " & _
                            "    ( " & _
                            "        SELECT DISTINCT " & _
                            "             cfuser_master.userunkid " & _
                            "            ,cfuser_master.username " & _
                            "            ,cfuser_master.email " & _
                                          strSelect & " " & _
                            "            ,LM.priority " & _
                            "            ,LM.empapplevelname AS LvName " & _
                            "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                            "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                            "        FROM hrmsConfiguration..cfuser_master " & _
                            "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                            "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                                         strAccessJoin & " " & _
                            "        JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                            "        JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                            "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
                    If strFilter.Trim.Length > 0 Then
                        StrQ &= " AND (" & strFilter & " ) "
                    End If

                    'Gajanan [9-NOV-2019] -- End


                    'S.SANDEEP |17-JAN-2019| -- START
                    'StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                    '        "            /*AND companyunkid = @C*/ AND yearunkid = @Y AND privilegeunkid = @P  " & _
                    '        ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
                    '        strApprovalDataJoin & " " & _
                    '        strOuterJoin & " " & _
                    '        "WHERE 1 = 1 AND TA.isvoid = 0 AND TA." & StrTranUnkidCol & " <= 0 AND TA.statusunkid = 1 AND ISNULL([TA].[isprocessed],0) = 0 "


                    'Gajanan [9-NOV-2019] -- Start   
                    'Enhancement:Worked On NMB Bio Data Approval Query Optimization   



                    'StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                    '        "            /*AND companyunkid = @C*/ AND yearunkid = @Y AND privilegeunkid = @P  " & _
                    '        ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
                    '        strApprovalDataJoin.Replace("#HTATCND#", System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "HTAT")) & " " & _
                    '        strOuterJoin & " " & _
                    '        "WHERE TA.isvoid = 0 AND " & System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "TA") & " AND TA.statusunkid = 1 "

                    StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                            "            /*AND companyunkid = @C*/ AND yearunkid = @Y AND privilegeunkid = @P  " & _
                         ") AS Fn ON #USR.userunkid = Fn.userunkid and #USR.approvertypeid = 2 " & _
                            strApprovalDataJoin.Replace("#HTATCND#", System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "HTAT")) & " " & _
                            strOuterJoin & " " & _
                            "WHERE TA.isvoid = 0 AND " & System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "TA") & " AND TA.statusunkid = 1 "
                    'Gajanan [9-NOV-2019] -- End


                    'S.SANDEEP |17-JAN-2019| -- END




                    StrQ &= StrExtraFilter


                    StrQ = StrQ.Replace("#TABLENAME#", StrTableName)
            End Select

            If mblnIsSubmitForApproaval = False Then
                StrQ &= " AND Fn.priority > " & intCurrentPriorityId
            End If

            'S.SANDEEP [09-AUG-2018] -- START
            If mstrFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilterString
            End If
            'S.SANDEEP [09-AUG-2018] -- END


            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            StrQ &= "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                    "DROP TABLE #USR "
            'Gajanan [9-NOV-2019] -- End


            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            For Each row As DataRow In dsList.Tables("List").Rows
                row.Item("EffDate") = eZeeDate.convertDate(row.Item("efdate").ToString).ToShortDateString()

                If dsList.Tables("List").Columns.Contains("edate1") Then
                    If row.Item("edate1").ToString.Trim.Length > 0 Then
                        row.Item("dDate1") = eZeeDate.convertDate(row.Item("edate1").ToString).ToShortDateString()
                    End If
                End If

                If dsList.Tables("List").Columns.Contains("edate2") Then
                    If row.Item("edate2").ToString.Trim.Length > 0 Then
                        row.Item("dDate2") = eZeeDate.convertDate(row.Item("edate2").ToString).ToShortDateString()
                    End If
                End If

                If dsList.Tables("List").Columns.Contains("isdate") Then
                    If row.Item("isdate").ToString.Trim.Length > 0 Then
                        row.Item("IDate") = eZeeDate.convertDate(row.Item("isdate").ToString).ToShortDateString()
                    End If
                End If

                If dsList.Tables("List").Columns.Contains("exdate") Then
                    If row.Item("exdate").ToString.Trim.Length > 0 Then
                        row.Item("EDate") = eZeeDate.convertDate(row.Item("exdate").ToString).ToShortDateString()
                    End If
                End If
            Next

            dtList = dsList.Tables("List").Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtList
    End Function

    Public Function InsertAll(ByVal eMovType As enMovementType, _
                              ByVal eOprType As enOperationType, _
                               ByVal dtTable As DataTable, _
                               ByVal intUserId As Integer, _
                               ByVal dtCurrentDateTime As DateTime, _
                               ByVal strFormName As String, _
                               ByVal blnIsweb As Boolean, _
                               ByVal eStatusId As clsEmployee_Master.EmpApprovalStatus, _
                               ByVal strRemark As String, _
                               ByVal intUserMappingTranId As Integer, _
                               ByVal intCompanyId As Integer, _
                               ByVal mblnCreateADUserFromEmp As Boolean, _
                               ByVal xDatabaseName As String, _
                               Optional ByVal xHostName As String = "", _
                               Optional ByVal xIPAddr As String = "", _
                               Optional ByVal xLoggedUserName As String = "", _
                               Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP) As Boolean


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens. [  ByVal xDatabaseName As String, _]

        'Pinkal (18-Aug-2018) --  'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[   ByVal mblnCreateADUserFromEmp As Boolean, _]
        'S.SANDEEP |17-JAN-2019| -- 'Enhancement -Add eOprType
        Dim objATransfer As clsTransfer_Approval_Tran = Nothing
        Dim objAPermit As clsPermit_Approval_Tran = Nothing
        Dim objADates As clsDates_Approval_Tran = Nothing
        Dim objACCT As clsCCTranhead_Approval_Tran = Nothing
        Dim objACategorize As clsCategorization_Approval_Tran = Nothing

        Dim objETransfer As clsemployee_transfer_tran = Nothing
        Dim objEPermit As clsemployee_workpermit_tran = Nothing
        Dim objEDates As clsemployee_dates_tran = Nothing
        Dim objECCT As clsemployee_cctranhead_tran = Nothing
        Dim objECategorize As clsemployee_categorization_Tran = Nothing
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Dim exForce As Exception = Nothing


        'Pinkal (16-Jun-2021)-- Start
        'Bug Resolved for NETIS TANZANIA LIMITED for Employee Dates Approval Issue.
        Dim mblnIsFinalDatesApproval As Boolean = False
        'Pinkal (16-Jun-2021) -- End

        Try
            If dtTable.Rows.Count > 0 Then
                Select Case eMovType
                    Case enMovementType.TRANSFERS
                        objATransfer = New clsTransfer_Approval_Tran
                        objETransfer = New clsemployee_transfer_tran
                        For Each xRow As DataRow In dtTable.Rows
                            objATransfer._Audittype = enAuditType.ADD
                            objATransfer._Audituserunkid = intUserId
                            objATransfer._Effectivedate = CDate(eZeeDate.convertDate(xRow("efdate").ToString).Date)
                            objATransfer._Employeeunkid = CInt(xRow("employeeunkid"))
                            objATransfer._Stationunkid = CInt(xRow("branchid"))
                            objATransfer._Deptgroupunkid = CInt(xRow("deptgrpid"))
                            objATransfer._Departmentunkid = CInt(xRow("deptid"))
                            objATransfer._Sectiongroupunkid = CInt(xRow("secgrpid"))
                            objATransfer._Sectionunkid = CInt(xRow("secid"))
                            objATransfer._Unitgroupunkid = CInt(xRow("unitgrpid"))
                            objATransfer._Unitunkid = CInt(xRow("unitid"))
                            objATransfer._Teamunkid = CInt(xRow("teamid"))
                            objATransfer._Classgroupunkid = CInt(xRow("clsgrpid"))
                            objATransfer._Classunkid = CInt(xRow("clsid"))
                            objATransfer._Changereasonunkid = CInt(xRow("changereasonunkid"))
                            objATransfer._Isvoid = False
                            objATransfer._Statusunkid = eStatusId
                            objATransfer._Voiddatetime = Nothing
                            objATransfer._Voidreason = ""
                            objATransfer._Voiduserunkid = -1
                            objATransfer._Tranguid = Guid.NewGuid.ToString()
                            objATransfer._Transactiondate = dtCurrentDateTime
                            objATransfer._Remark = strRemark
                            objATransfer._Rehiretranunkid = 0
                            objATransfer._Mappingunkid = intUserMappingTranId
                            objATransfer._Isweb = blnIsweb
                            objATransfer._Isfinal = CBool(xRow("isfinal"))
                            objATransfer._Ip = xIPAddr
                            objATransfer._Hostname = xHostName
                            objATransfer._Form_Name = strFormName
                            objATransfer._IsProcessed = False
                            objATransfer._OperationTypeId = eOprType

                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objATransfer._Transferunkid = CInt(xRow("transferunkid"))
                            End If


                            'Gajanan [23-SEP-2019] -- Start    
                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                            'If objATransfer.Insert(intCompanyId, eOprType, objDataOperation, True) Then



                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'If objATransfer.Insert(intCompanyId, eOprType, mblnCreateADUserFromEmp, objDataOperation, True, Nothing, True) Then
                            If objATransfer.Insert(intCompanyId, eOprType, mblnCreateADUserFromEmp, xDatabaseName, objDataOperation, True, Nothing, True) Then
                                'Pinkal (12-Oct-2020) -- End

                                'Gajanan [23-SEP-2019] -- End
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(xRow("isfinal")) Then


                                        If eOprType <> enOperationType.DELETED Then
                                            objETransfer._Transferunkid = objATransfer._Transferunkid
                                        objETransfer._Effectivedate = objATransfer._Effectivedate
                                        objETransfer._Employeeunkid = objATransfer._Employeeunkid
                                        objETransfer._Stationunkid = objATransfer._Stationunkid
                                        objETransfer._Deptgroupunkid = objATransfer._Deptgroupunkid
                                        objETransfer._Departmentunkid = objATransfer._Departmentunkid
                                        objETransfer._Sectiongroupunkid = objATransfer._Sectiongroupunkid
                                        objETransfer._Sectionunkid = objATransfer._Sectionunkid
                                        objETransfer._Unitgroupunkid = objATransfer._Unitgroupunkid
                                        objETransfer._Unitunkid = objATransfer._Unitunkid
                                        objETransfer._Teamunkid = objATransfer._Teamunkid
                                        objETransfer._Classgroupunkid = objATransfer._Classgroupunkid
                                        objETransfer._Classunkid = objATransfer._Classunkid
                                        objETransfer._Changereasonunkid = objATransfer._Changereasonunkid
                                        objETransfer._Isvoid = False
                                        objETransfer._Statusunkid = 0
                                        objETransfer._Userunkid = intUserId
                                        objETransfer._Voiddatetime = Nothing
                                        objETransfer._Voidreason = ""
                                        objETransfer._Voiduserunkid = -1

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objETransfer._FromWeb = objATransfer._Isweb
                                            objETransfer._ClientIP = objATransfer._Ip
                                            objETransfer._HostName = objATransfer._Hostname
                                            objETransfer._FormName = objATransfer._Form_Name
                                            objETransfer._AuditUserId = objATransfer._Audituserunkid
                                            'Gajanan [9-July-2019] -- End
                                        End If

                                        'Pinkal (18-Aug-2018) -- Start
                                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                        'If objETransfer.Insert(intCompanyId, objDataOperation) Then
                                        If objATransfer._Transferunkid > 0 Then
                                            Select Case objATransfer._OperationTypeId
                                                Case enOperationType.EDITED
                                                    'Gajanan [23-SEP-2019] -- Start    
                                                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                                    'If objETransfer.Update(mblnCreateADUserFromEmp, intCompanyId, objDataOperation) Then

                                                    'Pinkal (12-Oct-2020) -- Start
                                                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                                    'If objETransfer.Update(mblnCreateADUserFromEmp, intCompanyId, objDataOperation, Nothing, True, True) Then
                                                    If objETransfer.Update(mblnCreateADUserFromEmp, intCompanyId, xDatabaseName, objDataOperation, Nothing, True, True) Then
                                                        'Pinkal (12-Oct-2020) -- End
                                                        'Gajanan [23-SEP-2019] -- End
                                                        If objATransfer.Update(objDataOperation, objETransfer._Employeeunkid, objETransfer._Transferunkid, objETransfer._Effectivedate, "transferunkid") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objATransfer.Update(objDataOperation, objATransfer._Employeeunkid, 1, objATransfer._Effectivedate, "isprocessed") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                        'S.SANDEEP |17-JAN-2019| -- START
                                                    Else
                                                        If objETransfer._Message.Trim.Length > 0 Then mstrMessage = objETransfer._Message
                                                        'S.SANDEEP |17-JAN-2019| -- END

                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.


                                                        'Gajanan [24-OCT-2019] -- Start    
                                                        objDataOperation.ReleaseTransaction(False)
                                                        'Gajanan [24-OCT-2019] -- End

                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If

                                                Case enOperationType.DELETED
                                                    If objATransfer.Update(objDataOperation, objATransfer._Employeeunkid, 1, objATransfer._Effectivedate, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                            End Select
                                        Else

                                            'Gajanan [23-SEP-2019] -- Start    
                                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                            'If objETransfer.Insert(mblnCreateADUserFromEmp, intCompanyId, objDataOperation) Then

                                            'Pinkal (12-Oct-2020) -- Start
                                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                            'If objETransfer.Insert(mblnCreateADUserFromEmp, intCompanyId, objDataOperation, Nothing, True, True) Then
                                            If objETransfer.Insert(xDatabaseName, mblnCreateADUserFromEmp, intCompanyId, objDataOperation, Nothing, True, True) Then
                                                'Pinkal (12-Oct-2020) -- End
                                                'Gajanan [23-SEP-2019] -- End

                                            'Pinkal (18-Aug-2018) -- End
                                            If objATransfer.Update(objDataOperation, objETransfer._Employeeunkid, objETransfer._Transferunkid, objETransfer._Effectivedate, "transferunkid") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If objATransfer.Update(objDataOperation, objATransfer._Employeeunkid, 1, objATransfer._Effectivedate, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objETransfer._Message.Trim.Length > 0 Then mstrMessage = objETransfer._Message
                                                'S.SANDEEP |17-JAN-2019| -- END

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        End If
                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then
                                            objETransfer._Transferunkid = objATransfer._Transferunkid
                                            objETransfer._Isvoid = False
                                            objETransfer._Voidreason = ""
                                            objETransfer._Voiddatetime = Nothing
                                            objETransfer._Voiduserunkid = 0

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objETransfer._FromWeb = objATransfer._Isweb
                                            objETransfer._ClientIP = objATransfer._Ip
                                            objETransfer._HostName = objATransfer._Hostname
                                            objETransfer._FormName = objATransfer._Form_Name
                                            objETransfer._AuditUserId = objATransfer._Audituserunkid
                                            'Gajanan [9-July-2019] -- End

                                            'Gajanan [23-SEP-2019] -- Start    
                                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                            'If objETransfer.Update(mblnCreateADUserFromEmp, intCompanyId, objDataOperation) Then

                                            'Pinkal (12-Oct-2020) -- Start
                                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                            'If objETransfer.Update(mblnCreateADUserFromEmp, intCompanyId, objDataOperation, Nothing, True, True) Then
                                            If objETransfer.Update(mblnCreateADUserFromEmp, intCompanyId, xDatabaseName, objDataOperation, Nothing, True, True) Then
                                                'Pinkal (12-Oct-2020) -- End
                                                'Gajanan [23-SEP-2019] -- End
                                        If objATransfer.Update(objDataOperation, objATransfer._Employeeunkid, 1, objATransfer._Effectivedate, "isprocessed") = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objETransfer._Message.Trim.Length > 0 Then mstrMessage = objETransfer._Message
                                                'S.SANDEEP |17-JAN-2019| -- END

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If

                                            'Gajanan [23-SEP-2019] -- Start    
                                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                            objETransfer._Employeeunkid = objATransfer._Employeeunkid
                                            objETransfer._Userunkid = objATransfer._Audituserunkid


                                            'Pinkal (12-Oct-2020) -- Start
                                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                            'If objETransfer.MigrationInsert(intCompanyId, eOprType, mblnCreateADUserFromEmp, objDataOperation, Nothing, True, True, eStatusId) = False Then
                                            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            '    Throw exForce
                                            'End If
                                            If objETransfer.MigrationInsert(intCompanyId, eOprType, mblnCreateADUserFromEmp, xDatabaseName, objDataOperation, Nothing, True, True, eStatusId) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                            'Pinkal (12-Oct-2020) -- End



                                            'Gajanan [23-SEP-2019] -- End
                                        Else
                                            If objATransfer.Update(objDataOperation, objATransfer._Employeeunkid, 1, objATransfer._Effectivedate, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If


                                            'Gajanan [23-SEP-2019] -- Start    
                                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                            objETransfer._Employeeunkid = objATransfer._Employeeunkid
                                            objETransfer._Userunkid = objATransfer._Audituserunkid


                                            'Pinkal (12-Oct-2020) -- Start
                                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                            'If objETransfer.MigrationInsert(intCompanyId, eOprType, mblnCreateADUserFromEmp, objDataOperation, Nothing, True, True, eStatusId) = False Then
                                            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            '    Throw exForce
                                            'End If

                                            If objETransfer.MigrationInsert(intCompanyId, eOprType, mblnCreateADUserFromEmp, xDatabaseName, objDataOperation, Nothing, True, True, eStatusId) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                            'Pinkal (12-Oct-2020) -- End

                                            'Gajanan [23-SEP-2019] -- End

                                        End If
                                    End If
                                End If

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else
                                If objATransfer._Message.Trim.Length > 0 Then mstrMessage = objATransfer._Message
                                'Gajanan [24-OCT-2019] -- Start    
                                objDataOperation.ReleaseTransaction(False)
                                'Gajanan [24-OCT-2019] -- End
                                Return False
                                'Gajanan [22-Feb-2019] -- End
                            End If
                        Next

                    Case enMovementType.RECATEGORIZE
                        objACategorize = New clsCategorization_Approval_Tran
                        objECategorize = New clsemployee_categorization_Tran
                        For Each xRow As DataRow In dtTable.Rows
                            objACategorize._Audittype = enAuditType.ADD
                            objACategorize._Audituserunkid = intUserId
                            objACategorize._Effectivedate = CDate(eZeeDate.convertDate(xRow("efdate").ToString).Date)
                            objACategorize._Employeeunkid = CInt(xRow("employeeunkid"))
                            objACategorize._Jobgroupunkid = CInt(xRow("jobgroupunkid"))
                            objACategorize._Jobunkid = CInt(xRow("jobunkid"))
                            objACategorize._Gradeunkid = CInt(xRow("gradeunkid"))
                            objACategorize._Gradelevelunkid = CInt(xRow("gradelevelunkid"))
                            objACategorize._Changereasonunkid = CInt(xRow("changereasonunkid"))
                            objACategorize._Isvoid = False
                            objACategorize._Voiddatetime = Nothing
                            objACategorize._Voidreason = ""
                            objACategorize._Voiduserunkid = -1
                            objACategorize._Statusunkid = eStatusId
                            objACategorize._Tranguid = Guid.NewGuid.ToString()
                            objACategorize._Transactiondate = dtCurrentDateTime
                            objACategorize._Remark = strRemark
                            objACategorize._Rehiretranunkid = 0
                            objACategorize._Mappingunkid = intUserMappingTranId
                            objACategorize._Isweb = blnIsweb
                            objACategorize._Isfinal = CBool(xRow("isfinal"))
                            objACategorize._Ip = xIPAddr
                            objACategorize._Hostname = xHostName
                            objACategorize._Form_Name = strFormName
                            objACategorize._IsProcessed = False
                            objACategorize._OperationTypeId = eOprType

                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objACategorize._Categorizationtranunkid = CInt(xRow("categorizationtranunkid"))
                            End If


                            'Gajanan [23-SEP-2019] -- Start    
                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                            'If objACategorize.Insert(intCompanyId, eOprType, objDataOperation, True) Then


                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'If objACategorize.Insert(intCompanyId, eOprType, mblnCreateADUserFromEmp, objDataOperation, True, Nothing, True) Then
                            If objACategorize.Insert(intCompanyId, eOprType, mblnCreateADUserFromEmp, xDatabaseName, objDataOperation, True, Nothing, True) Then
                                'Pinkal (12-Oct-2020) -- End
                                'Pinkal (07-Dec-2019) -- End
                                'Gajanan [23-SEP-2019] -- End
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(xRow("isfinal")) Then
                                        If eOprType <> enOperationType.DELETED Then
                                            objECategorize._Categorizationtranunkid = objACategorize._Categorizationtranunkid
                                        objECategorize._Effectivedate = objACategorize._Effectivedate
                                        objECategorize._Employeeunkid = objACategorize._Employeeunkid
                                        objECategorize._JobGroupunkid = objACategorize._Jobgroupunkid
                                        objECategorize._Jobunkid = objACategorize._Jobunkid
                                        objECategorize._Gradeunkid = objACategorize._Gradeunkid
                                        objECategorize._Gradelevelunkid = objACategorize._Gradelevelunkid
                                        objECategorize._Changereasonunkid = objACategorize._Changereasonunkid
                                        objECategorize._Isvoid = False
                                        objECategorize._Statusunkid = 0
                                        objECategorize._Userunkid = intUserId
                                        objECategorize._Voiddatetime = Nothing
                                        objECategorize._Voidreason = ""
                                        objECategorize._Voiduserunkid = -1



                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objECategorize._FromWeb = objACategorize._Isweb
                                            objECategorize._ClientIP = objACategorize._Ip
                                            objECategorize._HostName = objACategorize._Hostname
                                            objECategorize._FormName = objACategorize._Form_Name
                                            objECategorize._AuditUserId = objACategorize._Audituserunkid
                                            'Gajanan [9-July-2019] -- End
                                        End If

                                        'Pinkal (18-Aug-2018) -- Start
                                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                        'If objECategorize.Insert(intCompanyId, objDataOperation) Then
                                        If objACategorize._Categorizationtranunkid > 0 Then
                                            Select Case objACategorize._OperationTypeId
                                                Case enOperationType.EDITED

                                                    'Gajanan [23-SEP-2019] -- Start    
                                                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                                    'If objECategorize.Update(intCompanyId, mblnCreateADUserFromEmp, objDataOperation) Then

                                                    'Pinkal (12-Oct-2020) -- Start
                                                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                                    'If objECategorize.Update(intCompanyId, mblnCreateADUserFromEmp, objDataOperation, Nothing, True, True) Then
                                                    If objECategorize.Update(intCompanyId, mblnCreateADUserFromEmp, xDatabaseName, objDataOperation, Nothing, True, True) Then
                                                        'Pinkal (12-Oct-2020) -- End
                                                        'Gajanan [23-SEP-2019] -- End
                                                        If objACategorize.Update(objDataOperation, objECategorize._Employeeunkid, objECategorize._Categorizationtranunkid, objECategorize._Effectivedate, "Categorizationtranunkid") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objACategorize.Update(objDataOperation, objACategorize._Employeeunkid, 1, objACategorize._Effectivedate, "isprocessed") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                        'S.SANDEEP |17-JAN-2019| -- START
                                                    Else
                                                        If objECategorize._Message.Trim.Length > 0 Then mstrMessage = objECategorize._Message
                                                        'S.SANDEEP |17-JAN-2019| -- END

                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        'Gajanan [24-OCT-2019] -- Start    
                                                        objDataOperation.ReleaseTransaction(False)
                                                        'Gajanan [24-OCT-2019] -- End
                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If

                                                Case enOperationType.DELETED
                                                    If objACategorize.Update(objDataOperation, objACategorize._Employeeunkid, 1, objACategorize._Effectivedate, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                            End Select
                                        Else

                                            'Gajanan [23-SEP-2019] -- Start    
                                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                            'If objECategorize.Insert(mblnCreateADUserFromEmp, intCompanyId, objDataOperation) Then

                                            'Pinkal (12-Oct-2020) -- Start
                                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                            'If objECategorize.Insert(mblnCreateADUserFromEmp, intCompanyId, objDataOperation, Nothing, True, True) Then
                                            If objECategorize.Insert(mblnCreateADUserFromEmp, intCompanyId, xDatabaseName, objDataOperation, Nothing, True, True) Then
                                                'Pinkal (12-Oct-2020) -- End
                                                'Gajanan [23-SEP-2019] -- End

                                            'Pinkal (18-Aug-2018) -- End
                                            If objACategorize.Update(objDataOperation, objECategorize._Employeeunkid, objECategorize._Categorizationtranunkid, objECategorize._Effectivedate, "categorizationtranunkid") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If objACategorize.Update(objDataOperation, objACategorize._Employeeunkid, 1, objACategorize._Effectivedate, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objECategorize._Message.Trim.Length > 0 Then mstrMessage = objECategorize._Message
                                                'S.SANDEEP |17-JAN-2019| -- END

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        End If
                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then
                                            objECategorize._Categorizationtranunkid = objACategorize._Categorizationtranunkid
                                            objECategorize._Isvoid = False
                                            objECategorize._Voidreason = ""
                                            objECategorize._Voiddatetime = Nothing
                                            objECategorize._Voiduserunkid = 0

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objECategorize._FromWeb = objACategorize._Isweb
                                            objECategorize._ClientIP = objACategorize._Ip
                                            objECategorize._HostName = objACategorize._Hostname
                                            objECategorize._FormName = objACategorize._Form_Name
                                            objECategorize._AuditUserId = objACategorize._Audituserunkid
                                            'Gajanan [9-July-2019] -- End


                                            'Gajanan [23-SEP-2019] -- Start    
                                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                            'If objECategorize.Update(intCompanyId, mblnCreateADUserFromEmp, objDataOperation) Then

                                            'Pinkal (12-Oct-2020) -- Start
                                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                            'If objECategorize.Update(intCompanyId, mblnCreateADUserFromEmp, objDataOperation, Nothing, True, True) Then
                                            If objECategorize.Update(intCompanyId, mblnCreateADUserFromEmp, xDatabaseName, objDataOperation, Nothing, True, True) Then
                                                'Pinkal (12-Oct-2020) -- End
                                                'Gajanan [23-SEP-2019] -- End
                                                If objACategorize.Update(objDataOperation, objACategorize._Employeeunkid, 1, objACategorize._Effectivedate, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objECategorize._Message.Trim.Length > 0 Then mstrMessage = objECategorize._Message
                                                'S.SANDEEP |17-JAN-2019| -- END

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If

                                            'Gajanan [23-SEP-2019] -- Start    
                                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                            objECategorize._Employeeunkid = objACategorize._Employeeunkid
                                            objECategorize._Userunkid = objACategorize._Audituserunkid

                                            'Pinkal (12-Oct-2020) -- Start
                                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                            'If objECategorize.MigrationInsert(intCompanyId, eOprType, mblnCreateADUserFromEmp, objDataOperation, Nothing, True, True, eStatusId) = False Then
                                            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            '    Throw exForce
                                            'End If
                                            If objECategorize.MigrationInsert(xDatabaseName, intCompanyId, eOprType, mblnCreateADUserFromEmp, objDataOperation, Nothing, True, True, eStatusId) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                            'Pinkal (12-Oct-2020) -- End



                                            'Gajanan [23-SEP-2019] -- End
                                        Else
                                        If objACategorize.Update(objDataOperation, objACategorize._Employeeunkid, 1, objACategorize._Effectivedate, "isprocessed") = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                            'Gajanan [23-SEP-2019] -- Start    
                                            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                                            objECategorize._Employeeunkid = objACategorize._Employeeunkid
                                            objECategorize._Userunkid = objACategorize._Audituserunkid


                                            'Pinkal (12-Oct-2020) -- Start
                                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                                            'If objECategorize.MigrationInsert(intCompanyId, eOprType, mblnCreateADUserFromEmp, objDataOperation, Nothing, True, True, eStatusId) = False Then
                                            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            '    Throw exForce
                                            'End If

                                            If objECategorize.MigrationInsert(xDatabaseName, intCompanyId, eOprType, mblnCreateADUserFromEmp, objDataOperation, Nothing, True, True, eStatusId) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                            'Pinkal (12-Oct-2020) -- End


                                            'Gajanan [23-SEP-2019] -- End
                                    End If
                                End If
                            End If

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else
                                If objACategorize._Message.Trim.Length > 0 Then mstrMessage = objATransfer._Message
                                'Gajanan [24-OCT-2019] -- Start    
                                objDataOperation.ReleaseTransaction(False)
                                'Gajanan [24-OCT-2019] -- End
                                Return False
                                'Gajanan [22-Feb-2019] -- End
                            End If
                        Next

                    Case enMovementType.PROBATION, enMovementType.CONFIRMATION, enMovementType.SUSPENSION, enMovementType.TERMINATION, enMovementType.RETIREMENTS, enMovementType.EXEMPTION
                        'Sohail (21 Oct 2019) - [EXEMPTION]
                        objADates = New clsDates_Approval_Tran
                        objEDates = New clsemployee_dates_tran

                        'Gajanan [30-Dec-2019] -- Start   
                        Dim StrLeavingNotificationMessage As New System.Text.StringBuilder

                        Dim objconfig As New clsConfigOptions
                        Dim objCompany As New clsCompany_Master
                        objCompany._Companyunkid = intCompanyId


                        'Pinkal (18-May-2021) -- Start
                        'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                        'Dim StrNotification As String = objconfig.GetKeyValue(intCompanyId, "Notify_Dates")
                        'Dim StrSplit() As String = Nothing
                        'StrSplit = StrNotification.Split(CChar("||"))
                        'Dim blnsendNotification As Boolean = False
                        'If StrSplit(0).Split(CChar(",")).Contains(CInt(enEmployeeDates.EOC_DATE).ToString()) OrElse StrSplit(0).Split(CChar(",")).Contains(CInt(enEmployeeDates.LEAVING_DATE).ToString()) Then
                        '    blnsendNotification = True
                        'End If

                        Dim blnsendNotification As Boolean = False
                        Dim mstrEOCDateUserNotifications As String = ""
                        Dim mstrLeavingDateUserNotifications As String = ""

                        If eMovType = enMovementType.TERMINATION Then
                            mstrEOCDateUserNotifications = objconfig.GetKeyValue(intCompanyId, "EocDateUserNotification")
                            mstrLeavingDateUserNotifications = objconfig.GetKeyValue(intCompanyId, "LeavingDateUserNotification")
                        End If

                        If mstrEOCDateUserNotifications.Trim.Length > 0 OrElse mstrLeavingDateUserNotifications.Trim.Length > 0 Then
                            blnsendNotification = True

                            'If StrSplit(0).Split(CChar(",")).Contains(CInt(enEmployeeDates.EOC_DATE).ToString()) OrElse StrSplit(0).Split(CChar(",")).Contains(CInt(enEmployeeDates.LEAVING_DATE).ToString()) Then

                            'Sohail (07 Jan 2020) -- Start
                            'NMB Issue # : Error : Index was outside the bounds of the array.
                            'If StrSplit(2).Split(CChar(",")).Length > 0 Then
                            'If StrSplit.Length >= 3 AndAlso StrSplit(2).Split(CChar(",")).Length > 0 Then
                                'Sohail (07 Jan 2020) -- End

                                StrLeavingNotificationMessage.Append("<HTML><BODY>")
                                StrLeavingNotificationMessage.Append(vbCrLf)
                                StrLeavingNotificationMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrLeavingNotificationMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & "<b>" & " #user# " & "</b></span></p>")
                                StrLeavingNotificationMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrLeavingNotificationMessage.Append(Language.getMessage(mstrModuleName, 116, "The following employee(s) are separating with") & " " & objCompany._Name & Language.getMessage(mstrModuleName, 117, ", kindly take note of it") & " </span></p>")

                                StrLeavingNotificationMessage.Append("<TABLE border = '1'>")
                                StrLeavingNotificationMessage.Append(vbCrLf)
                                StrLeavingNotificationMessage.Append("<TR bgcolor= 'SteelBlue'>")
                                StrLeavingNotificationMessage.Append(vbCrLf)
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 118, "Employee Code") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 119, "Name") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage("clsMasterData", 428, "Department") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage("clsMasterData", 427, "Section Group") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage("clsMasterData", 426, "Section") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage("clsMasterData", 420, "Class Group") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage("clsMasterData", 419, "Classes") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage("clsMasterData", 421, "Jobs") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 122, "Status") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 120, "Effective Date") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 121, "EOC/Leaving Date") & "</span></b></TD>")
                                StrLeavingNotificationMessage.Append("</TR>")
                            'End If
                            'End If

                            End If
                        'Pinkal (18-May-2021) -- End


                        'Gajanan [30-Dec-2019] -- End


                        For Each xRow As DataRow In dtTable.Rows
                            objADates._Audittype = enAuditType.ADD
                            objADates._Audituserunkid = intUserId
                            objADates._Effectivedate = CDate(eZeeDate.convertDate(xRow("efdate").ToString).Date)
                            objADates._Employeeunkid = CInt(xRow("employeeunkid"))
                            objADates._Date1 = CDate(eZeeDate.convertDate(xRow("edate1").ToString).Date)
                            If xRow("edate2").ToString.Trim.Length > 0 Then
                                objADates._Date2 = CDate(eZeeDate.convertDate(xRow("edate2").ToString).Date)
                            Else
                                objADates._Date2 = Nothing
                            End If

                            If eMovType = enMovementType.CONFIRMATION Then
                                objADates._Isconfirmed = True
                            Else
                                objADates._Isconfirmed = False
                            End If
                            If eMovType = enMovementType.CONFIRMATION Then
                                objADates._Datetypeunkid = enEmp_Dates_Transaction.DT_CONFIRMATION
                            ElseIf eMovType = enMovementType.PROBATION Then
                                objADates._Datetypeunkid = enEmp_Dates_Transaction.DT_PROBATION
                            ElseIf eMovType = enMovementType.RETIREMENTS Then
                                objADates._Datetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT
                            ElseIf eMovType = enMovementType.SUSPENSION Then
                                objADates._Datetypeunkid = enEmp_Dates_Transaction.DT_SUSPENSION
                            ElseIf eMovType = enMovementType.TERMINATION Then
                                objADates._Datetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            ElseIf eMovType = enMovementType.EXEMPTION Then
                                objADates._Datetypeunkid = enEmp_Dates_Transaction.DT_EXEMPTION
                                'Sohail (21 Oct 2019) -- End
                            End If
                            objADates._Changereasonunkid = CInt(xRow("changereasonunkid"))
                            objADates._Isvoid = False
                            objADates._Statusunkid = 0
                            objADates._Voiddatetime = Nothing
                            objADates._Voidreason = ""
                            objADates._Voiduserunkid = -1
                            If eMovType = enMovementType.TERMINATION Then
                                objADates._Actionreasonunkid = CInt(xRow("changereasonunkid"))
                                objADates._Isexclude_Payroll = CBool(xRow("isexclude_payroll"))
                            End If
                            objADates._Isvoid = False
                            objADates._Statusunkid = eStatusId
                            objADates._Voiddatetime = Nothing
                            objADates._Voidreason = ""
                            objADates._Voiduserunkid = -1
                            objADates._Tranguid = Guid.NewGuid.ToString()
                            objADates._Transactiondate = dtCurrentDateTime
                            objADates._Remark = strRemark
                            objADates._Rehiretranunkid = 0
                            objADates._Mappingunkid = intUserMappingTranId
                            objADates._Isweb = blnIsweb
                            objADates._Isfinal = CBool(xRow("isfinal"))
                            objADates._Ip = xIPAddr
                            objADates._Hostname = xHostName
                            objADates._Form_Name = strFormName
                            objADates._IsProcessed = False
                            objADates._OperationTypeId = eOprType
                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            objADates._Leaveissueunkid = CInt(xRow("leaveissueunkid"))
                            'Sohail (21 Oct 2019) -- End


                            'Pinkal (07-Mar-2020) -- Start
                            'Enhancement - Changes Related to Payroll UAT for NMB.
                            If IsDBNull(xRow("actualdate")) = False Then
                                objADates._ActualDate = CDate(xRow("actualdate"))
                            End If

                            'Pinkal (07-Mar-2020) -- End


                            'Gajanan [18-May-2020] -- Start
                            'Enhancement:Discipline Module Enhancement NMB

                            If eMovType = enMovementType.SUSPENSION Then
                                objADates._Disciplinefileunkid = CInt(xRow("disciplinefileunkid"))
                            Else
                                objADates._Disciplinefileunkid = -1
                            End If

                            'Gajanan [18-May-2020] -- End


                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objADates._Datestranunkid = CInt(xRow("datestranunkid"))
                            End If


                            If objADates.Insert(intCompanyId, eOprType, objDataOperation, True) Then
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(xRow("isfinal")) Then
                                        If eOprType <> enOperationType.DELETED Then

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objEDates._Datestranunkid = objADates._Datestranunkid
                                            'Gajanan [9-July-2019] -- End

                                        objEDates._Effectivedate = objADates._Effectivedate
                                        objEDates._Employeeunkid = objADates._Employeeunkid
                                        objEDates._Date1 = objADates._Date1
                                        objEDates._Date2 = objADates._Date2
                                        objEDates._Isconfirmed = objADates._Isconfirmed
                                        objEDates._Datetypeunkid = objADates._Datetypeunkid
                                        objEDates._Changereasonunkid = objADates._Changereasonunkid
                                        objEDates._Isvoid = False
                                        objEDates._Statusunkid = 0
                                        objEDates._Userunkid = intUserId
                                        objEDates._Voiddatetime = Nothing
                                        objEDates._Voidreason = ""
                                        objEDates._Voiduserunkid = -1
                                        objEDates._Actionreasonunkid = objADates._Actionreasonunkid
                                        objEDates._Isexclude_payroll = objADates._Isexclude_Payroll
                                            'Sohail (21 Oct 2019) -- Start
                                            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                                            objEDates._Leaveissueunkid = objADates._Leaveissueunkid
                                            'Sohail (21 Oct 2019) -- End

'Pinkal (07-Mar-2020) -- Start
                                            'Enhancement - Changes Related to Payroll UAT for NMB.
                                            objEDates._ActualDate = objADates._ActualDate
                                            'Pinkal (07-Mar-2020) -- End
'Gajanan [18-May-2020] -- Start
                                            'Enhancement:Discipline Module Enhancement NMB
                                            objEDates._Disciplinefileunkid = objADates._Disciplinefileunkid
                                            'Gajanan [18-May-2020] -- End
                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objEDates._FromWeb = objADates._Isweb
                                            objEDates._ClientIP = objADates._Ip
                                            objEDates._HostName = objADates._Hostname
                                            objEDates._FormName = objADates._Form_Name
                                            objEDates._AuditUserId = objADates._Audituserunkid
                                            'Gajanan [9-July-2019] -- End
                                        End If

                                        'Pinkal (18-Aug-2018) -- Start
                                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                        'If objEDates.Insert(intCompanyId, objDataOperation) Then
                                        'If objEDates.Insert(mblnCreateADUserFromEmp, intCompanyId, objDataOperation) Then
                                            'Pinkal (18-Aug-2018) -- End

                                        If objADates._Datestranunkid > 0 Then
                                            Select Case objADates._OperationTypeId
                                                Case enOperationType.EDITED
                                                    If objEDates.Update(mblnCreateADUserFromEmp, intCompanyId, objDataOperation) Then
                        								'Gajanan [30-Dec-2019] -- Start
                                                        If eMovType = enMovementType.TERMINATION AndAlso blnsendNotification AndAlso CBool(xRow("isfinal")) Then
                                                            'Gajanan [30-Dec-2019] -- Start   
                                                            'Sohail (07 Jan 2020) -- Start
                                                            'NMB Issue # : Error : Index was outside the bounds of the array.
                                                            'If StrSplit(2).Split(CChar(",")).Length > 0 Then


                                                            'Pinkal (18-May-2021) -- Start
                                                            'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                                                            'If StrSplit.Length >= 3 AndAlso StrSplit(2).Split(CChar(",")).Length > 0 Then
                                                            If mstrEOCDateUserNotifications.Trim.Length > 0 OrElse mstrLeavingDateUserNotifications.Trim.Length > 0 Then
                                                                'Pinkal (18-May-2021) -- End
                                                                'Sohail (07 Jan 2020) -- End
                                                                StrLeavingNotificationMessage.Append(SetLeavingdateEmailNotification(objEDates, dtCurrentDateTime))
                                                                'Pinkal (16-Jun-2021)-- Start
                                                                'Bug Resolved for NETIS TANZANIA LIMITED for Employee Dates Approval Issue.
                                                                mblnIsFinalDatesApproval = True
                                                                'Pinkal (16-Jun-2021)-- End
                                                            End If
                                                            'Gajanan [30-Dec-2019] -- End
                                                        End If
                        								'Gajanan [30-Dec-2019] -- End

                                                        If objADates.Update(objDataOperation, objEDates._Employeeunkid, objEDates._Datestranunkid, objEDates._Effectivedate, objEDates._Datetypeunkid, "Datestranunkid") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                                If objADates.Update(objDataOperation, objADates._Employeeunkid, 1, objADates._Effectivedate, objADates._Datetypeunkid, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                        'S.SANDEEP |17-JAN-2019| -- START
                                                    Else
                                                        If objEDates._Message.Trim.Length > 0 Then mstrMessage = objEDates._Message
                                                        'S.SANDEEP |17-JAN-2019| -- END
                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        'Gajanan [24-OCT-2019] -- Start    
                                                        objDataOperation.ReleaseTransaction(False)
                                                        'Gajanan [24-OCT-2019] -- End
                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If
                                                Case enOperationType.DELETED
                                                    If objADates.Update(objDataOperation, objADates._Employeeunkid, 1, objADates._Effectivedate, objADates._Datetypeunkid, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                            End Select
                                        Else
                                            If objEDates.Insert(mblnCreateADUserFromEmp, intCompanyId, objDataOperation) Then

                                                'Gajanan [30-Dec-2019] -- Start   
                                                'Sohail (07 Jan 2020) -- Start
                                                'NMB Issue # : Error : Index was outside the bounds of the array.
                                                'If StrSplit(2).Split(CChar(",")).Length > 0 AndAlso CBool(xRow("isfinal")) Then

                                                'Pinkal (18-May-2021) -- Start
                                                'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                                                'If StrSplit.Length >= 3 AndAlso StrSplit(2).Split(CChar(",")).Length > 0 AndAlso CBool(xRow("isfinal")) Then
                                                If (mstrEOCDateUserNotifications.Trim.Length > 0 OrElse mstrLeavingDateUserNotifications.Trim.Length > 0) AndAlso CBool(xRow("isfinal")) Then
                                                    'Pinkal (18-May-2021) -- End
                                                    'Sohail (07 Jan 2020) -- End
                                                    StrLeavingNotificationMessage.Append(SetLeavingdateEmailNotification(objEDates, dtCurrentDateTime))
                                                    mblnIsFinalDatesApproval = True
                                                End If
                                                'Gajanan [30-Dec-2019] -- End

                                                If objADates.Update(objDataOperation, objEDates._Employeeunkid, objEDates._Datestranunkid, objEDates._Effectivedate, objEDates._Datetypeunkid, "Datestranunkid") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objADates.Update(objDataOperation, objADates._Employeeunkid, 1, objEDates._Effectivedate, objEDates._Datetypeunkid, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objEDates._Message.Trim.Length > 0 Then mstrMessage = objEDates._Message
                                                'S.SANDEEP |17-JAN-2019| -- END
                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If

                                        End If
                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then

                                        If eOprType = enOperationType.DELETED Then
                                            objEDates._Datestranunkid = objADates._Datestranunkid
                                            objEDates._Isvoid = False
                                            objEDates._Voidreason = ""
                                            objEDates._Voiddatetime = Nothing
                                            objEDates._Voiduserunkid = 0

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objEDates._FromWeb = objADates._Isweb
                                            objEDates._ClientIP = objADates._Ip
                                            objEDates._HostName = objADates._Hostname
                                            objEDates._FormName = objADates._Form_Name
                                            objEDates._AuditUserId = objADates._Audituserunkid
                                            'Gajanan [9-July-2019] -- End

                                            If objEDates.Update(mblnCreateADUserFromEmp, intCompanyId, objDataOperation) Then
                                        If objADates.Update(objDataOperation, objADates._Employeeunkid, 1, objADates._Effectivedate, objADates._Datetypeunkid, "isprocessed") = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objEDates._Message.Trim.Length > 0 Then mstrMessage = objEDates._Message
                                                'S.SANDEEP |17-JAN-2019| -- END

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        Else
                                            If objADates.Update(objDataOperation, objADates._Employeeunkid, 1, objADates._Effectivedate, objADates._Datetypeunkid, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        End If
                                    End If
                                End If

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else
                                If objADates._Message.Trim.Length > 0 Then mstrMessage = objATransfer._Message
                                'Gajanan [24-OCT-2019] -- Start    
                                objDataOperation.ReleaseTransaction(False)
                                'Gajanan [24-OCT-2019] -- End
                                Return False
                                'Gajanan [22-Feb-2019] -- End
                            End If
                        Next

                        'Gajanan [30-Dec-2019] -- Start


                        If eMovType = enMovementType.TERMINATION AndAlso blnsendNotification AndAlso (eOprType = enOperationType.ADDED OrElse eOprType = enOperationType.EDITED) Then

                            'Sohail (07 Jan 2020) -- Start
                            'NMB Issue # : Error : Index was outside the bounds of the array.
                            'If StrSplit(2).Split(CChar(",")).Length > 0 Then

                            'Pinkal (18-May-2021) -- Start
                            'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                            'If StrSplit.Length >= 3 AndAlso StrSplit(2).Split(CChar(",")).Length > 0 Then

                            'Pinkal (16-Jun-2021)-- Start
                            'Bug Resolved for NETIS TANZANIA LIMITED for Employee Dates Approval Issue.
                            If mblnIsFinalDatesApproval Then
                                'Pinkal (16-Jun-2021)-- End

                            If mstrEOCDateUserNotifications.Trim.Length > 0 OrElse mstrLeavingDateUserNotifications.Trim.Length > 0 Then
                                'Pinkal (18-May-2021) -- End
                                'Sohail (07 Jan 2020) -- End

                                Dim TempNotification As String = StrLeavingNotificationMessage.ToString()


                                'Pinkal (18-May-2021) -- Start
                                'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                                Dim mstrUsers As String = ""
                                Dim mblnIsUserAccess As Boolean = True
                                Dim mstrOtherEmail As String = ""
                                Dim arDates() As String = Nothing

                                If mstrEOCDateUserNotifications.Trim.Length > 0 Then

                                    arDates = mstrEOCDateUserNotifications.Split(CChar("|"))
                                    If arDates.Length > 0 Then
                                        mstrUsers = arDates(0).ToString()
                                        If arDates.Length > 1 Then
                                            mblnIsUserAccess = CBool(arDates(1))
                                            mstrOtherEmail = arDates(2).ToString()
                                        End If
                                    End If


                                    'For Each sId As String In StrSplit(2).Split(CChar(","))
                                    If mstrUsers.Trim.Length > 0 Then
                                        For Each sId As String In mstrUsers.Split(CChar(","))
                                            Dim objuser As New clsUserAddEdit
                                            objuser._Userunkid = CInt(sId)

                                            TempNotification = TempNotification.Replace("#user#", getTitleCase(If((objuser._Firstname + " " + objuser._Lastname).Trim.Length > 0, objuser._Firstname + " " + objuser._Lastname, objuser._Username)))
                                            TempNotification &= "</TABLE>"
                                            TempNotification &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                                            TempNotification &= "</span></p>"
                                            TempNotification &= "</BODY></HTML>"

                                            Dim objSendMail As New clsSendMail
                                            objSendMail._ToEmail = objuser._Email
                                            objSendMail._Subject = Language.getMessage(mstrModuleName, 115, "Employee Exit")
                                            objSendMail._Message = TempNotification.ToString()
                                            objSendMail._Form_Name = strFormName
                                            objSendMail._LogEmployeeUnkid = -1
                                            objSendMail._OperationModeId = xLoginMod
                                            objSendMail._UserUnkid = CInt(sId)
                                            objSendMail._SenderAddress = xLoggedUserName
                                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                            objSendMail.SendMail(intCompanyId)

                                            TempNotification = StrLeavingNotificationMessage.ToString()

                                        Next

                                    End If    '  If mstrUsers.Trim.Length > 0 Then

                                    If mstrOtherEmail.Trim.Length > 0 Then
                                        For Each sEmailId As String In mstrOtherEmail.Split(CChar(","))

                                            TempNotification = TempNotification.Replace(Language.getMessage("frmEmployeeMaster", 147, "Dear") & "<b>" & " #user# " & "</b></span></p>", "")
                                            TempNotification &= "</TABLE>"
                                            TempNotification &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                                            TempNotification &= "</span></p>"
                                            TempNotification &= "</BODY></HTML>"

                                            Dim objSendMail As New clsSendMail
                                            objSendMail._ToEmail = sEmailId
                                            objSendMail._Subject = Language.getMessage(mstrModuleName, 115, "Employee Exit")
                                            objSendMail._Message = TempNotification.ToString()
                                            objSendMail._Form_Name = strFormName
                                            objSendMail._LogEmployeeUnkid = -1
                                            objSendMail._OperationModeId = xLoginMod
                                            objSendMail._UserUnkid = intUserId
                                            objSendMail._SenderAddress = xLoggedUserName
                                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                            objSendMail.SendMail(intCompanyId)

                                            TempNotification = StrLeavingNotificationMessage.ToString()
                                        Next
                                    End If  'f mstrOtherEmail.Trim.Length > 0 Then

                                End If  'If mstrEOCDateUserNotifications.Trim.Length > 0 Then

                                arDates = Nothing
                                mstrUsers = ""
                                mblnIsUserAccess = True
                                mstrOtherEmail = ""

                                If mstrLeavingDateUserNotifications.Trim.Length > 0 Then

                                    arDates = mstrLeavingDateUserNotifications.Split(CChar("|"))
                                    If arDates.Length > 0 Then
                                        mstrUsers = arDates(0).ToString()
                                        If arDates.Length > 1 Then
                                            mblnIsUserAccess = CBool(arDates(1))
                                            mstrOtherEmail = arDates(2).ToString()
                                        End If
                                    End If

                                    If mstrUsers.Trim.Length > 0 Then
                                        For Each sId As String In mstrUsers.Split(CChar(","))
                                    Dim objuser As New clsUserAddEdit
                                    objuser._Userunkid = CInt(sId)

                                    TempNotification = TempNotification.Replace("#user#", getTitleCase(If((objuser._Firstname + " " + objuser._Lastname).Trim.Length > 0, objuser._Firstname + " " + objuser._Lastname, objuser._Username)))
                                    TempNotification &= "</TABLE>"
                                    TempNotification &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                                    TempNotification &= "</span></p>"
                                    TempNotification &= "</BODY></HTML>"

                                    Dim objSendMail As New clsSendMail
                                    objSendMail._ToEmail = objuser._Email
                                    objSendMail._Subject = Language.getMessage(mstrModuleName, 115, "Employee Exit")
                                    objSendMail._Message = TempNotification.ToString()
                                    objSendMail._Form_Name = strFormName
                                    objSendMail._LogEmployeeUnkid = -1
                                    objSendMail._OperationModeId = xLoginMod
                                    objSendMail._UserUnkid = CInt(sId)
                                    objSendMail._SenderAddress = xLoggedUserName
                                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                    objSendMail.SendMail(intCompanyId)

                                    TempNotification = StrLeavingNotificationMessage.ToString()

                                Next

                                    End If    '  If mstrUsers.Trim.Length > 0 Then

                                    If mstrOtherEmail.Trim.Length > 0 Then
                                        For Each sEmailId As String In mstrOtherEmail.Split(CChar(","))

                                            TempNotification = TempNotification.Replace(Language.getMessage("frmEmployeeMaster", 147, "Dear") & "<b>" & " #user# " & "</b></span></p>", "")
                                            TempNotification &= "</TABLE>"
                                            TempNotification &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                                            TempNotification &= "</span></p>"
                                            TempNotification &= "</BODY></HTML>"

                                            Dim objSendMail As New clsSendMail
                                            objSendMail._ToEmail = sEmailId
                                            objSendMail._Subject = Language.getMessage(mstrModuleName, 115, "Employee Exit")
                                            objSendMail._Message = TempNotification.ToString()
                                            objSendMail._Form_Name = strFormName
                                            objSendMail._LogEmployeeUnkid = -1
                                            objSendMail._OperationModeId = xLoginMod
                                            objSendMail._UserUnkid = intUserId
                                            objSendMail._SenderAddress = xLoggedUserName
                                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                            objSendMail.SendMail(intCompanyId)

                                            TempNotification = StrLeavingNotificationMessage.ToString()
                                        Next
                                    End If  'f mstrOtherEmail.Trim.Length > 0 Then

                                End If ' If mstrLeavingDateUserNotifications.Trim.Length > 0 Then

                                End If  'If mstrEOCDateUserNotifications.Trim.Length > 0 OrElse mstrLeavingDateUserNotifications.Trim.Length > 0 Then

                                'Pinkal (18-May-2021) -- End
                            End If  'mblnIsFinalDatesApproval = True

                        End If
                        'Gajanan [30-Dec-2019] -- End

                    Case enMovementType.WORKPERMIT, enMovementType.RESIDENTPERMIT
                        objAPermit = New clsPermit_Approval_Tran
                        objEPermit = New clsemployee_workpermit_tran
                        For Each xRow As DataRow In dtTable.Rows
                            objAPermit._Audittype = enAuditType.ADD
                            objAPermit._Audituserunkid = intUserId
                            objAPermit._Effectivedate = CDate(eZeeDate.convertDate(xRow("efdate").ToString).Date)
                            objAPermit._Employeeunkid = CInt(xRow("employeeunkid"))
                            If xRow("exdate").ToString().Trim.Length > 0 Then
                                objAPermit._Expiry_Date = CDate(eZeeDate.convertDate(xRow("exdate").ToString).Date)
                            Else
                                objAPermit._Expiry_Date = Nothing
                            End If

                            If xRow("isdate").ToString().Trim.Length > 0 Then
                                objAPermit._Issue_Date = CDate(eZeeDate.convertDate(xRow("isdate").ToString).Date)
                            Else
                                objAPermit._Issue_Date = Nothing
                            End If
                            objAPermit._Issue_Place = xRow("issue_place").ToString()
                            objAPermit._Isvoid = False
                            objAPermit._Voidreason = ""
                            objAPermit._Voiduserunkid = -1
                            objAPermit._Work_Permit_No = xRow("work_permit_no").ToString.Trim
                            objAPermit._Workcountryunkid = CInt(xRow("workcountryunkid"))
                            objAPermit._Changereasonunkid = CInt(xRow("changereasonunkid"))
                            objAPermit._Isresidentpermit = CBool(xRow("isresidentpermit"))
                            objAPermit._Isvoid = False
                            objAPermit._Statusunkid = eStatusId
                            objAPermit._Voidreason = ""
                            objAPermit._Voiduserunkid = -1
                            objAPermit._Tranguid = Guid.NewGuid.ToString()
                            objAPermit._Transactiondate = dtCurrentDateTime
                            'Hemant (23 Apr 2020) -- Start
                            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                            'objAPermit._Remark = ""
                            objAPermit._Remark = strRemark
                            'Hemant (23 Apr 2020) -- End
                            objAPermit._Rehiretranunkid = 0
                            objAPermit._Mappingunkid = intUserMappingTranId
                            objAPermit._Isweb = blnIsweb
                            objAPermit._Isfinal = CBool(xRow("isfinal"))
                            objAPermit._Ip = xIPAddr
                            objAPermit._Hostname = xHostName
                            objAPermit._Form_Name = strFormName
                            objAPermit._IsProcessed = False
                            objAPermit._OperationTypeId = eOprType

                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objAPermit._Workpermittranunkid = CInt(xRow("Workpermittranunkid"))
                            End If


                            If objAPermit.Insert(eOprType, objDataOperation, True) Then
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(xRow("isfinal")) Then

                                        If eOprType <> enOperationType.DELETED Then
                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objEPermit._Workpermittranunkid = objAPermit._Workpermittranunkid
                                            'Gajanan [9-July-2019] -- End
                                        objEPermit._Effectivedate = objAPermit._Effectivedate
                                        objEPermit._Employeeunkid = objAPermit._Employeeunkid
                                        objEPermit._Expiry_Date = objAPermit._Expiry_Date
                                        objEPermit._Issue_Date = objAPermit._Issue_Date
                                        objEPermit._Issue_Place = objAPermit._Issue_Place
                                        objEPermit._Isvoid = False
                                        objEPermit._Userunkid = intUserId
                                        objEPermit._Voiddatetime = Nothing
                                        objEPermit._Voidreason = ""
                                        objEPermit._Voiduserunkid = -1
                                        objEPermit._Work_Permit_No = objAPermit._Work_Permit_No
                                        objEPermit._Workcountryunkid = objAPermit._Workcountryunkid
                                        objEPermit._Changereasonunkid = objAPermit._Changereasonunkid
                                        objEPermit._IsResidentPermit = objAPermit._Isresidentpermit


                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objEPermit._FromWeb = objAPermit._Isweb
                                            objEPermit._ClientIP = objAPermit._Ip
                                            objEPermit._HostName = objAPermit._Hostname
                                            objEPermit._FormName = objAPermit._Form_Name
                                            objEPermit._AuditUserId = objAPermit._Audituserunkid
                                            'Gajanan [9-July-2019] -- End
                                        End If


                                        'If objEPermit.Insert(objDataOperation) Then
                                        If objAPermit._Workpermittranunkid > 0 Then
                                            Select Case objAPermit._OperationTypeId
                                                Case enOperationType.EDITED
                                                    'S.SANDEEP |17-JAN-2019| -- START
                                                    If objEPermit.Update(objDataOperation) Then
                                                    If objAPermit.Update(objDataOperation, objEPermit._Employeeunkid, objEPermit._Workpermittranunkid, objEPermit._Effectivedate, objEPermit._IsResidentPermit, "workpermittranunkid") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    If objAPermit.Update(objDataOperation, objAPermit._Employeeunkid, 1, objAPermit._Effectivedate, objAPermit._Isresidentpermit, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                    Else
                                                        If objEPermit._Message.Trim.Length > 0 Then mstrMessage = objEPermit._Message

                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        'Gajanan [24-OCT-2019] -- Start    
                                                        objDataOperation.ReleaseTransaction(False)
                                                        'Gajanan [24-OCT-2019] -- End
                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If
                                                    'S.SANDEEP |17-JAN-2019| -- END
                                                Case enOperationType.DELETED
                                                    If objAPermit.Update(objDataOperation, objAPermit._Employeeunkid, 1, objAPermit._Effectivedate, objAPermit._Isresidentpermit, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                            End Select
                                        Else
                                        If objEPermit.Insert(objDataOperation) Then
                                                'Pinkal (18-Aug-2018) -- End
                                            If objAPermit.Update(objDataOperation, objEPermit._Employeeunkid, objEPermit._Workpermittranunkid, objEPermit._Effectivedate, objEPermit._IsResidentPermit, "workpermittranunkid") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If objAPermit.Update(objDataOperation, objAPermit._Employeeunkid, 1, objAPermit._Effectivedate, objAPermit._Isresidentpermit, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objEPermit._Message.Trim.Length > 0 Then mstrMessage = objEPermit._Message
                                                'S.SANDEEP |17-JAN-2019| -- END
                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        End If
                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            'objEPermit._Workpermittranunkid = objATransfer._Transferunkid
                                            objEPermit._Workpermittranunkid = objAPermit._Workpermittranunkid
                                            'Gajanan [9-July-2019] -- End
                                            objEPermit._Isvoid = False
                                            objEPermit._Voidreason = ""
                                            objEPermit._Voiddatetime = Nothing
                                            objEPermit._Voiduserunkid = 0

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objEPermit._FromWeb = objADates._Isweb
                                            objEPermit._ClientIP = objADates._Ip
                                            objEPermit._HostName = objADates._Hostname
                                            objEPermit._FormName = objADates._Form_Name
                                            objEPermit._AuditUserId = objADates._Audituserunkid
                                            'Gajanan [9-July-2019] -- End

                                            If objEPermit.Update(objDataOperation) Then
                                                If objAPermit.Update(objDataOperation, objAPermit._Employeeunkid, 1, objAPermit._Effectivedate, objAPermit._Isresidentpermit, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objEPermit._Message.Trim.Length > 0 Then mstrMessage = objEPermit._Message
                                                'S.SANDEEP |17-JAN-2019| -- END
                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If

                                        Else
                                        If objAPermit.Update(objDataOperation, objAPermit._Employeeunkid, 1, objAPermit._Effectivedate, objAPermit._Isresidentpermit, "isprocessed") = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If
                                End If
                            End If
                            End If
                        Next

                    Case enMovementType.COSTCENTER
                        objACCT = New clsCCTranhead_Approval_Tran
                        objECCT = New clsemployee_cctranhead_tran
                        For Each xRow As DataRow In dtTable.Rows
                            objACCT._Audittype = enAuditType.ADD
                            objACCT._Audituserunkid = intUserId
                            objACCT._Cctranheadvalueid = CInt(xRow("cctranheadvalueid"))
                            objACCT._Changereasonunkid = CInt(xRow("changereasonunkid"))
                            objACCT._Effectivedate = CDate(eZeeDate.convertDate(xRow("efdate").ToString).Date)
                            objACCT._Employeeunkid = CInt(xRow("employeeunkid"))
                            objACCT._Istransactionhead = CBool(xRow("istransactionhead"))
                            objACCT._Isvoid = False
                            objACCT._Rehiretranunkid = 0
                            objACCT._Statusunkid = eStatusId
                            objACCT._Voiddatetime = Nothing
                            objACCT._Voidreason = ""
                            objACCT._Voiduserunkid = -1
                            objACCT._Tranguid = Guid.NewGuid.ToString()
                            objACCT._Transactiondate = dtCurrentDateTime
                            'Hemant (23 Apr 2020) -- Start
                            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                            'objACCT._Remark = ""
                            objACCT._Remark = strRemark
                            'Hemant (23 Apr 2020) -- End
                            objACCT._Rehiretranunkid = 0
                            objACCT._Mappingunkid = intUserMappingTranId
                            objACCT._Isweb = blnIsweb
                            objACCT._Isfinal = CBool(xRow("isfinal"))
                            objACCT._Ip = xIPAddr
                            objACCT._Hostname = xHostName
                            objACCT._Form_Name = strFormName
                            objACCT._IsProcessed = False
                            objACCT._OperationTypeId = eOprType
                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objACCT._Cctranheadunkid = CInt(xRow("cctranheadunkid"))
                            End If

                            If objACCT.Insert(eOprType, objDataOperation, True) Then
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(xRow("isfinal")) Then

                                        If eOprType <> enOperationType.DELETED Then
                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objECCT._Cctranheadunkid = objACCT._Cctranheadunkid
                                            'Gajanan [9-July-2019] -- End
                                        objECCT._Cctranheadvalueid = objACCT._Cctranheadvalueid
                                        objECCT._Changereasonunkid = objACCT._Changereasonunkid
                                        objECCT._Effectivedate = objACCT._Effectivedate
                                        objECCT._Employeeunkid = objACCT._Employeeunkid
                                        objECCT._Istransactionhead = objACCT._Istransactionhead
                                        objECCT._Isvoid = False
                                        objECCT._Rehiretranunkid = 0
                                        objECCT._Statusunkid = 0
                                        objECCT._Userunkid = intUserId
                                        objECCT._Voiddatetime = Nothing
                                        objECCT._Voidreason = ""
                                        objECCT._Voiduserunkid = -1

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objECCT._FromWeb = objACCT._Isweb
                                            objECCT._ClientIP = objACCT._Ip
                                            objECCT._HostName = objACCT._Hostname
                                            objECCT._FormName = objACCT._Form_Name
                                            objECCT._AuditUserId = objACCT._Audituserunkid
                                            'Gajanan [9-July-2019] -- End
                                        End If

                                        'If objECCT.Insert(objDataOperation) Then
                                        If objACCT._Cctranheadunkid > 0 Then
                                            Select Case objACCT._OperationTypeId
                                                Case enOperationType.EDITED
                                                    If objECCT.Update(objDataOperation) Then
                                                    If objACCT.Update(objDataOperation, objECCT._Employeeunkid, objECCT._Cctranheadunkid, objECCT._Effectivedate, objECCT._Istransactionhead, "cctranheadunkid") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    If objACCT.Update(objDataOperation, objACCT._Employeeunkid, 1, objACCT._Effectivedate, objACCT._Istransactionhead, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                        'S.SANDEEP |17-JAN-2019| -- START
                                                    Else
                                                        If objECCT._Message.Trim.Length > 0 Then mstrMessage = objECCT._Message
                                                        'S.SANDEEP |17-JAN-2019| -- END

                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        'Gajanan [24-OCT-2019] -- Start    
                                                        objDataOperation.ReleaseTransaction(False)
                                                        'Gajanan [24-OCT-2019] -- End
                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If
                                                Case enOperationType.DELETED
                                                    If objACCT.Update(objDataOperation, objACCT._Employeeunkid, 1, objACCT._Effectivedate, objACCT._Istransactionhead, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                            End Select
                                        Else
                                        If objECCT.Insert(objDataOperation) Then
                                                'Pinkal (18-Aug-2018) -- End
                                                If objACCT.Update(objDataOperation, objECCT._Employeeunkid, objECCT._Cctranheadunkid, objECCT._Effectivedate, objACCT._Istransactionhead, "cctranheadunkid") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If objACCT.Update(objDataOperation, objACCT._Employeeunkid, 1, objACCT._Effectivedate, objACCT._Istransactionhead, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objECCT._Message.Trim.Length > 0 Then mstrMessage = objECCT._Message
                                                'S.SANDEEP |17-JAN-2019| -- END

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If

                                        End If
                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then
                                            objECCT._Cctranheadunkid = objACCT._Cctranheadunkid
                                            objECCT._Isvoid = False
                                            objECCT._Voidreason = ""
                                            objECCT._Voiddatetime = Nothing
                                            objECCT._Voiduserunkid = 0

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objECCT._FromWeb = objACCT._Isweb
                                            objECCT._ClientIP = objACCT._Ip
                                            objECCT._HostName = objACCT._Hostname
                                            objECCT._FormName = objACCT._Form_Name
                                            objECCT._AuditUserId = objACCT._Audituserunkid
                                            'Gajanan [9-July-2019] -- End


                                            If objECCT.Update(objDataOperation) Then
                                                If objACCT.Update(objDataOperation, objACCT._Employeeunkid, 1, objACCT._Effectivedate, objACCT._Istransactionhead, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'S.SANDEEP |17-JAN-2019| -- START
                                            Else
                                                If objECCT._Message.Trim.Length > 0 Then mstrMessage = objECCT._Message
                                                'S.SANDEEP |17-JAN-2019| -- END


                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'Gajanan [24-OCT-2019] -- Start    
                                                objDataOperation.ReleaseTransaction(False)
                                                'Gajanan [24-OCT-2019] -- End
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End

                                            End If
                                        Else
                                        If objACCT.Update(objDataOperation, objACCT._Employeeunkid, 1, objACCT._Effectivedate, objACCT._Istransactionhead, "isprocessed") = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If

                                    End If
                                End If

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else
                                If objACCT._Message.Trim.Length > 0 Then mstrMessage = objATransfer._Message
                                'Gajanan [24-OCT-2019] -- Start    
                                objDataOperation.ReleaseTransaction(False)
                                'Gajanan [24-OCT-2019] -- End
                                Return False
                                'Gajanan [22-Feb-2019] -- End
                            End If
                        Next

                End Select
            End If
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If ex.Message.ToString().Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
                mstrMessage = ex.Message.ToString()
            ElseIf ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                mstrMessage = ex.Message.ToString()
            Else
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            End If
            'Pinkal (04-Apr-2020) -- End

        Finally
            objDataOperation = Nothing
            objATransfer = Nothing : objAPermit = Nothing : objADates = Nothing : objACCT = Nothing : objACategorize = Nothing
            objETransfer = Nothing : objEPermit = Nothing : objEDates = Nothing : objECCT = Nothing : objECategorize = Nothing
        End Try
    End Function
    'Gajanan [30-Dec-2019] -- Start
    Private Function SetLeavingdateEmailNotification(ByVal objEDates As clsemployee_dates_tran, ByVal dtCurrentDateTime As DateTime) As String
        Try

            Dim objEmp As New clsEmployee_Master
            Dim objDept As New clsDepartment
            Dim objsectiongrp As New clsSectionGroup
            Dim objsection As New clsSections
            Dim objClassGroup As New clsClassGroup
            Dim objClass As New clsClass
            Dim objJob As New clsJobs
            Dim objReason As New clsCommon_Master
            Dim StrLeavingNotificationMessage As New System.Text.StringBuilder

            objEmp._Employeeunkid(dtCurrentDateTime, objDataOperation) = objEDates._Employeeunkid
            objDept._Departmentunkid = objEmp._Departmentunkid
            objsectiongrp._Sectiongroupunkid = objEmp._Sectiongroupunkid
            objsection._Sectionunkid = objEmp._Sectionunkid
            objClassGroup._Classgroupunkid = objEmp._Classgroupunkid
            objClass._Classesunkid = objEmp._Classunkid
            objJob._Jobunkid = objEmp._Jobunkid

            objReason._Masterunkid = objEDates._Changereasonunkid

            StrLeavingNotificationMessage.Append("<TR>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objEmp._Employeecode & "</span></TD>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objEmp._Firstname + " " + objEmp._Surname & "</span></TD>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objDept._Name & "</span></TD>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objsectiongrp._Name & "</span></TD>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objsection._Name & "</span></TD>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objClassGroup._Name & "</span></TD>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objClass._Name & "</span></TD>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objJob._Job_Name & "</span></TD>")
            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objReason._Name & "</span></TD>")

            StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objEDates._Effectivedate.Date.ToShortDateString() & "</span></TD>")

            If objEDates._Date2 <> objEDates._Date1 Then
                StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objEDates._Date1 & "</span>  <br/>  <span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objEDates._Date2 & "</span>  </TD>")
            Else
                StrLeavingNotificationMessage.Append("<TD  align = 'LEFT' STYLE='WIDTH:10%;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & objEDates._Date1 & "</span></TD>")
            End If

            StrLeavingNotificationMessage.Append("</TR>")
            Return StrLeavingNotificationMessage.ToString()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetLeavingdateEmailNotification; Module Name: " & mstrModuleName)
        End Try
    End Function
    'Gajanan [30-Dec-2019] -- End

    Private Sub SetEmailBody(ByRef StrMessage As System.Text.StringBuilder, _
                             ByVal eMovement As enMovementType, _
                             ByVal intNotificationType As Integer, _
                             ByVal dr() As DataRow, _
                             ByVal username As String, _
                             ByVal strMainText As String, _
                             ByVal strRejectionRemark As String, _
                             ByVal blnRejection As Boolean)
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & username & "</b></span></p>")
            StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(username) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & strMainText & " </span></p>")
            StrMessage.Append(strMainText & " </span></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'Gajanan (21 Nov 2018) -- Start
            'StrMessage.Append("<TABLE border = '1'>")
            'StrMessage.Append("<TABLE border = '1' style='margin-left: 25px'>")
            StrMessage.Append("<TABLE border = '1' >")
            'Gajanan (21 Nov 2018) -- End
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TR bgcolor= 'SteelBlue'>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 47, "Effective Date") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 48, "Employee") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 29, "Type") & "</span></b></TD>")
            Select Case eMovement
                Case enMovementType.CONFIRMATION
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 49, "Confirmation Date") & "</span></b></TD>")
                Case enMovementType.RETIREMENTS
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 50, "Retirement Date") & "</span></b></TD>")
                Case enMovementType.SUSPENSION
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 51, "Start Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 52, "End Date") & "</span></b></TD>")
                Case enMovementType.TERMINATION
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 53, "EOC Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 54, "Leaving Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 55, "Is Exclude Payroll") & "</span></b></TD>")
                Case enMovementType.PROBATION
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 51, "Start Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 52, "End Date") & "</span></b></TD>")
                Case enMovementType.COSTCENTER
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 43, "Cost Center") & "</span></b></TD>")
                Case enMovementType.RECATEGORIZE
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 12, "Job Group") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 13, "Job") & "</span></b></TD>")
                Case enMovementType.RESIDENTPERMIT
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 14, "Permit No") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 15, "Issue Place") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 16, "Issue Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 17, "Expiry Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 18, "Issue Country") & "</span></b></TD>")
                Case enMovementType.WORKPERMIT
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 14, "Permit No") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 15, "Issue Place") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 16, "Issue Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 17, "Expiry Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 18, "Issue Country") & "</span></b></TD>")
                Case enMovementType.TRANSFERS
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 19, "Branch") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 20, "Department Group") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 21, "Department") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 22, "Section Group") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 23, "Section") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 24, "Unit Grpup") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 25, "Unit") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 26, "Team") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 27, "Class Group") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 28, "Class") & "</span></b></TD>")
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enMovementType.EXEMPTION
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 51, "Start Date") & "</span></b></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 52, "End Date") & "</span></b></TD>")
                    'Sohail (21 Oct 2019) -- End
            End Select
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 38, "Change Reason") & "</span></b></TD>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("</TR>")
            For idx As Integer = 0 To dr.Length - 1
                StrMessage.Append("<TR>" & vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("EffDate").ToString & "</span></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("ecode").ToString & " - " & dr(idx)("ename").ToString & "</span></TD>")
                Select Case eMovement
                    Case enMovementType.CONFIRMATION
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 30, "Confirmation") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate1").ToString & "</span></TD>")
                    Case enMovementType.RETIREMENTS
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 31, "Retirement") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate1").ToString & "</span></TD>")
                    Case enMovementType.SUSPENSION
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 32, "Suspension") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate1").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate2").ToString & "</span></TD>")
                    Case enMovementType.TERMINATION
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 33, "Termination") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate1").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate2").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & IIf(CBool(dr(idx)("isexclude_payroll")) = True, "Yes", "No").ToString & "</span></TD>")
                    Case enMovementType.PROBATION
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 34, "Probation") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate1").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate2").ToString & "</span></TD>")
                    Case enMovementType.COSTCENTER
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 43, "Cost Center") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("DispValue").ToString & "</span></TD>")
                    Case enMovementType.RECATEGORIZE
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 35, "Recategorization") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("JobGroup").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("Job").ToString & "</span></TD>")
                    Case enMovementType.RESIDENTPERMIT
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 36, "Resident Permit") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("work_permit_no").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("issue_place").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("IDate").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("EDate").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("Country").ToString & "</span></TD>")
                    Case enMovementType.WORKPERMIT
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 37, "Work Permit") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("work_permit_no").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("issue_place").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("IDate").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("EDate").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("Country").ToString & "</span></TD>")
                    Case enMovementType.TRANSFERS
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 44, "Transfer") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("branch").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("deptgroup").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dept").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("secgroup").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("section").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("unitgrp").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("unit").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("team").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("classgrp").ToString & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("class").ToString & "</span></TD>")
                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Case enMovementType.EXEMPTION
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 65, "Exemption") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate1").ToString & "</span></TD>")
                        If IsDBNull(dr(idx)("dDate2")) = False Then
                            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dr(idx)("dDate2").ToString & "</span></TD>")
                        Else
                            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>&nbsp;</span></TD>")
                        End If
                        'Sohail (21 Oct 2019) -- End
                End Select
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("CReason").ToString & "</span></TD>")
                StrMessage.Append("</TR>" & vbCrLf)
            Next
            StrMessage.Append("</TABLE>")

            If strRejectionRemark.Trim.Length > 0 Then
                If blnRejection Then
                    StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 41, "Rejection Remark:") & " " & strRejectionRemark & " " & "</span></b>")
                Else
                    StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 42, "Approval Remark:") & " " & strRejectionRemark & " " & "</span></b>")
                End If
                StrMessage.Append("</span></b></p>")
            End If


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'Gajanan (21 Nov 2018) -- Start
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan (21 Nov 2018) -- End
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetEmailBody; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Notification For Movements
    ''' </summary>
    ''' <param name="intNotificationType">1 = Submit for Approval , 2 = Notify Next User for Approve, 3 = Send Rejection to Selected User</param>
    ''' <param name="strDatabaseName"></param>
    ''' <param name="strUserAccessMode"></param>
    ''' <param name="intCompanyId"></param>
    ''' <param name="intYearId"></param>
    ''' <param name="intPrivilegeId"></param>
    ''' <param name="eMovement"></param>
    ''' <param name="xEmployeeAsOnDate"></param>
    ''' <param name="blnIsResidentPermit"></param>
    ''' <param name="eDates"></param>
    ''' <param name="intPriority"></param>
    ''' <param name="intEmployeeIds"></param>
    ''' <param name="strRemark"></param>
    ''' <remarks></remarks>
    Public Sub SendNotification(ByVal intNotificationType As Integer, _
                                ByVal strDatabaseName As String, _
                                ByVal strUserAccessMode As String, _
                                ByVal intCompanyId As Integer, _
                                ByVal intYearId As Integer, _
                                ByVal intPrivilegeId As Integer, _
                                ByVal eMovement As enMovementType, _
                                ByVal xEmployeeAsOnDate As String, _
                                ByVal intUserId As Integer, _
                                ByVal strFormName As String, _
                                ByVal eMode As enLogin_Mode, _
                                ByVal strSenderAddress As String, _
                                ByVal eOperationType As enOperationType, _
                                Optional ByVal blnIsResidentPermit As Boolean = False, _
                                Optional ByVal eDates As enEmp_Dates_Transaction = Nothing, _
                                Optional ByVal intPriority As Integer = 0, _
                                Optional ByVal intEmployeeIds As String = "", _
                                Optional ByVal strRemark As String = "", _
                                Optional ByVal strRejectTransferUserNotification As String = "", _
                                Optional ByVal strRejectReCategorizationUserNotification As String = "", _
                                Optional ByVal strRejectProbationUserNotification As String = "", _
                                Optional ByVal strRejectConfirmationUserNotification As String = "", _
                                Optional ByVal strRejectSuspensionUserNotification As String = "", _
                                Optional ByVal strRejectTerminationUserNotification As String = "", _
                                Optional ByVal strRejectRetirementUserNotification As String = "", _
                                Optional ByVal strRejectWorkPermitUserNotification As String = "", _
                                Optional ByVal strRejectResidentPermitUserNotification As String = "", _
                                Optional ByVal strRejectCostCenterPermitUserNotification As String = "", _
                                Optional ByVal strRejectExemptionUserNotification As String = "" _
                                ) 'S.SANDEEP |17-JAN-2019| -- START {enOperationType} -- END
        'Sohail (21 Oct 2019) - [strRejectExemptionUserNotification]
        Try
            Dim dtAppr As DataTable = Nothing
            Dim StrMessage As New System.Text.StringBuilder
            Select Case intNotificationType
                Case 1, 3
                    dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eMovement, xEmployeeAsOnDate, intUserId, eDates, blnIsResidentPermit, eOperationType, Nothing) 'S.SANDEEP |17-JAN-2019| -- START {eOperationType} -- END
                Case 2
                    dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eMovement, xEmployeeAsOnDate, intUserId, eDates, blnIsResidentPermit, eOperationType, Nothing, intPriority, False) 'S.SANDEEP |17-JAN-2019| -- START {eOperationType} -- END
            End Select
            Dim dr() As DataRow = Nothing


            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            'Dim dtFilterTab As DataTable = dtAppr.DefaultView.ToTable(True, "username", "email", "userunkid")
            Dim dtFilterTab As DataTable = dtAppr.DefaultView.ToTable(True, "username", "email", "userunkid", "priority")
            'Gajanan [9-July-2019] -- End

            Dim strRejectUserIds As String = String.Empty
            Dim strSubject As String = ""
            'If intNotificationType = 3 Then 'Gajanan [21-Oct-2019]
                Select Case eMovement
                    Case enMovementType.CONFIRMATION
                        strRejectUserIds = strRejectConfirmationUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 113, "Notification to Approve/Reject Emplyoee Confirmation")

                    Case enMovementType.COSTCENTER
                        strRejectUserIds = strRejectCostCenterPermitUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 114, "Notification to Approve/Reject Emplyoee CostCenter")

                    Case enMovementType.PROBATION
                        strRejectUserIds = strRejectProbationUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 109, "Notification to Approve/Reject Emplyoee Probation")

                    Case enMovementType.RECATEGORIZE
                        strRejectUserIds = strRejectReCategorizationUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 110, "Notification to Approve/Reject Emplyoee Recategorization")

                    Case enMovementType.RESIDENTPERMIT
                        strRejectUserIds = strRejectResidentPermitUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 112, "Notification to Approve/Reject Emplyoee Resident Permit")

                    Case enMovementType.RETIREMENTS
                        strRejectUserIds = strRejectRetirementUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 104, "Notification to Approve/Reject Emplyoee Retirements")

                    Case enMovementType.SUSPENSION
                        strRejectUserIds = strRejectSuspensionUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 105, "Notification to Approve/Reject Emplyoee Suspension")

                    Case enMovementType.TERMINATION
                        strRejectUserIds = strRejectTerminationUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 106, "Notification to Approve/Reject Emplyoee Termination")

                    Case enMovementType.TRANSFERS
                        strRejectUserIds = strRejectTransferUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 107, "Notification to Approve/Reject Emplyoee Transfer")

                    Case enMovementType.WORKPERMIT
                        strRejectUserIds = strRejectWorkPermitUserNotification
                        strSubject = Language.getMessage(mstrModuleName, 108, "Notification to Approve/Reject Emplyoee Transfer")

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enMovementType.EXEMPTION
                    strRejectUserIds = strRejectExemptionUserNotification
                    strSubject = Language.getMessage(mstrModuleName, 66, "Notification to Approve/Reject Emplyoee Exemption")
                    'Sohail (21 Oct 2019) -- End

                End Select

            If intNotificationType = 3 Then 'Gajanan [21-Oct-2019]
                If strRejectUserIds.Trim.Length > 0 Then
                For Each id As String In strRejectUserIds.Split(CChar(","))
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(id)
                    Dim drow As DataRow = dtFilterTab.NewRow()
                    drow("username") = objUser._Username
                    drow("email") = objUser._Email
                    drow("userunkid") = objUser._Userunkid
                        drow("priority") = intPriority 'Gajanan 18-4-2019 add priority
                    dtFilterTab.Rows.Add(drow)
                    objUser = Nothing
                Next
                End If
            End If

            For Each iRow As DataRow In dtFilterTab.Rows
                Select Case intNotificationType
                    Case 1  'NOTIFICATION FOR SUBMIT FOR APPROVAL
                        dr = dtAppr.Select("rno = 1 AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
                        If dr.Length > 0 Then
                            Call SetEmailBody(StrMessage, eMovement, intNotificationType, dr, iRow("username").ToString, Language.getMessage(mstrModuleName, 45, "This is to inform you that following employee(s) having movement changes, Please login to aruti to approve/reject them."), strRemark, False)

                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = strSubject 'Language.getMessage(mstrModuleName, 46, "Notification to Approve/Reject Emplyoee Movement")
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            objSendMail.SendMail(intCompanyId)

                        End If
                    Case 2  'NOTIFICATION FOR NEXT LEVEL OF APPROVAL
                        dr = dtAppr.Select("rno = 1 AND priority > " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & "  " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
                        If dr.Length > 0 Then
                            Call SetEmailBody(StrMessage, eMovement, intNotificationType, dr, iRow("username").ToString, Language.getMessage(mstrModuleName, 45, "This is to inform you that following employee(s) having movement changes, Please login to aruti to approve/reject them."), strRemark, False)
                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = strSubject 'Language.getMessage(mstrModuleName, 46, "Notification to Approve/Reject Emplyoee Movement")
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            objSendMail.SendMail(intCompanyId)

                        End If
                    Case 3  'NOTIFICATION FOR REJECTION OF EMPLOYEE MOVEMENT
                        dr = dtAppr.Select("priority <= " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
                        If dr.Length > 0 Then
                            Call SetEmailBody(StrMessage, eMovement, intNotificationType, dr, iRow("username").ToString, Language.getMessage(mstrModuleName, 40, "This is to inform you that following employee(s) movement changes has been rejected"), strRemark, True)
                        
                        Else
                            dr = dtAppr.Select(IIf(intEmployeeIds.Trim.Length <= 0, "", " employeeunkid IN (" & intEmployeeIds & ")").ToString())
                            Call SetEmailBody(StrMessage, eMovement, intNotificationType, dr, iRow("username").ToString, Language.getMessage(mstrModuleName, 40, "This is to inform you that following employee(s) movement changes has been rejected"), strRemark, True)
                        End If
                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = strSubject 'Language.getMessage(mstrModuleName, 46, "Notification to Approve/Reject Emplyoee Movement")
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            objSendMail.SendMail(intCompanyId)
                End Select
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendApprovalNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [18-SEP-2018] -- START
    Public Function IsOtherMovmentApproverPresent(ByVal eMovement As enMovementType, _
                                                  ByVal strDatabaseName As String, _
                                                  ByVal strUserAccessMode As String, _
                                                  ByVal intCompanyId As Integer, _
                                                  ByVal intYearId As Integer, _
                                                  ByVal intPrivilegeId As Integer, _
                                                  ByVal intUserId As Integer, _
                                                  ByVal xEmployeeAsOnDate As String, _
                                                  ByVal eDates As enEmp_Dates_Transaction, _
                                                  ByVal intEmployeeUnkid As Integer, _
                                                  Optional ByVal objDataOpr As clsDataOperation = Nothing) As String
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim strMessage As String = ""
        Try

            Dim strUACJoin, strUACFilter As String
            strUACJoin = "" : strUACFilter = ""

            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'StrQ = "SELECT " & _
            '       "     UM.userunkid " & _
            '       "    ,UM.username " & _
            '       "    ,UM.email " & _
            '       "    ,LM.priority " & _
            '       "    ,LM.empapplevelname AS LvName " & _
            '       "FROM " & strDatabaseName & "..hremp_appusermapping AS EM " & _
            '       "    JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = EM.mapuserunkid " & _
            '       "    JOIN hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
            '       "    JOIN hrmsConfiguration..cfcompanyaccess_privilege AS CP ON CP.userunkid = UM.userunkid " & _
            '       "    JOIN hrmsConfiguration..cfuser_privilege AS UP ON UM.userunkid = UP.userunkid " & _
            '       "WHERE isvoid = 0 AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
            '       " AND CP.yearunkid = @Y AND UP.privilegeunkid = @P "

            StrQ = "SELECT " & _
                   "     UM.userunkid " & _
                   "    ,UM.username " & _
                   "    ,UM.email " & _
                   "    ,LM.priority " & _
                   "    ,LM.empapplevelname AS LvName " & _
                   "FROM " & strDatabaseName & "..hremp_appusermapping AS EM " & _
                   "    JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = EM.mapuserunkid " & _
                   "    JOIN hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege AS CP ON CP.userunkid = UM.userunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_privilege AS UP ON UM.userunkid = UP.userunkid " & _
                   "WHERE isvoid = 0 AND EM.isactive = 1 AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
                   " AND CP.yearunkid = @Y AND UP.privilegeunkid = @P "

            'Gajanan [17-April-2019] -- End

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                strMessage = Language.getMessage(mstrModuleName, 60, "Sorry, No approver(s) defined based on selected for employee user access setting.")
                Exit Try
            End If

            Dim blnFlag As Boolean = False

            For Each dr As DataRow In dsList.Tables(0).Rows
                modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, CInt(dr("userunkid")), intCompanyId, intYearId, strUserAccessMode, "AEM", True)
                StrQ = "SELECT " & _
                   "     AEM.employeeunkid " & _
                   "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                   "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                   "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                   "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                   "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                   "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                   "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                   "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                   "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                   "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                   "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                   "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                   "FROM " & strDatabaseName & "..hremployee_master AS AEM "
            If strUACJoin.Trim.Length > 0 Then
                StrQ &= strUACJoin
            End If
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        stationunkid " & _
                    "       ,deptgroupunkid " & _
                    "       ,departmentunkid " & _
                    "       ,sectiongroupunkid " & _
                    "       ,sectionunkid " & _
                    "       ,unitgroupunkid " & _
                    "       ,unitunkid " & _
                    "       ,teamunkid " & _
                    "       ,classgroupunkid " & _
                    "       ,classunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                    ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        jobgroupunkid " & _
                    "       ,jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                        "WHERE AEM.employeeunkid = '" & intEmployeeUnkid & "' "

                Dim dsEmp As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

                If dsEmp.Tables(0).Rows.Count > 0 Then
                    blnFlag = True
                    Exit For
                End If
            Next

            If blnFlag = False Then
                strMessage = Language.getMessage(mstrModuleName, 60, "Sorry, No approver(s) defined based on selected for employee user access setting.")
                Exit Try
            End If



            'StrQ = "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
            '       "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
            '       "SELECT " & _
            '       "     AEM.employeeunkid " & _
            '       "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
            '       "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
            '       "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
            '       "    ,ISNULL(T.classunkid,0) AS classunkid " & _
            '       "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
            '       "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
            '       "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
            '       "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
            '       "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
            '       "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
            '       "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
            '       "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
            '       "FROM " & strDatabaseName & "..hremployee_master AS AEM "
            'If strUACJoin.Trim.Length > 0 Then
            '    StrQ &= strUACJoin
            'End If
            'StrQ &= "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "        stationunkid " & _
            '        "       ,deptgroupunkid " & _
            '        "       ,departmentunkid " & _
            '        "       ,sectiongroupunkid " & _
            '        "       ,sectionunkid " & _
            '        "       ,unitgroupunkid " & _
            '        "       ,unitunkid " & _
            '        "       ,teamunkid " & _
            '        "       ,classgroupunkid " & _
            '        "       ,classunkid " & _
            '        "       ,employeeunkid " & _
            '        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
            '        "   FROM hremployee_transfer_tran " & _
            '        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
            '        ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "        jobgroupunkid " & _
            '        "       ,jobunkid " & _
            '        "       ,employeeunkid " & _
            '        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
            '        "   FROM hremployee_categorization_tran " & _
            '        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
            '        ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 "

            'StrQ &= "SELECT " & _
            '        "    EM.employeeunkid " & _
            '        "FROM @emp " & _
            '        "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
            '        "    JOIN " & _
            '        "    ( " & _
            '        "        SELECT DISTINCT " & _
            '        "             cfuser_master.userunkid " & _
            '        "            ,cfuser_master.username " & _
            '        "            ,cfuser_master.email " & _
            '                      strSelect & " " & _
            '        "            ,LM.priority " & _
            '        "            ,LM.empapplevelname AS LvName " & _
            '        "        FROM hrmsConfiguration..cfuser_master " & _
            '        "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
            '        "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
            '                     strAccessJoin & " " & _
            '        "        JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
            '        "        JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
            '        "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
            'If strFilter.Trim.Length > 0 Then
            '    StrQ &= " AND (" & strFilter & " ) "
            'End If
            'StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
            '        "            AND companyunkid = @C AND yearunkid = @Y AND privilegeunkid = @P  " & _
            '        ") AS Fn ON " & strJoin & " "



            'If dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeUnkid).Count <= 0 Then
            '    strMessage = Language.getMessage(mstrModuleName, 60, "Sorry, No approver(s) defined based on selected for employee user access setting.")
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsOtherMovmentApproverPresent; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMessage
    End Function

    'Public Function IsOtherMovmentApproverPresent(ByVal eMovement As enMovementType, _
    '                                              ByVal strDatabaseName As String, _
    '                                              ByVal strUserAccessMode As String, _
    '                                              ByVal intCompanyId As Integer, _
    '                                              ByVal intYearId As Integer, _
    '                                              ByVal intPrivilegeId As Integer, _
    '                                              ByVal intUserId As Integer, _
    '                                              ByVal xEmployeeAsOnDate As String, _
    '                                              ByVal eDates As enEmp_Dates_Transaction, _
    '                                              ByVal intEmployeeUnkid As Integer, _
    '                                              Optional ByVal objDataOpr As clsDataOperation = Nothing) As String
    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Dim strMessage As String = ""
    '    Try

    '        Dim strUACJoin, strUACFilter As String
    '        strUACJoin = "" : strUACFilter = ""

    '        Dim strFilter As String = String.Empty
    '        Dim strJoin As String = String.Empty
    '        Dim strSelect As String = String.Empty
    '        Dim strAccessJoin As String = String.Empty
    '        Dim strOuterJoin As String = String.Empty
    '        Dim strFinalString As String = ""

    '        modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM", True)
    '        Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, 0, eMovement, objDataOperation)

    '        StrQ = "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
    '               "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
    '               "SELECT " & _
    '               "     AEM.employeeunkid " & _
    '               "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
    '               "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
    '               "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
    '               "    ,ISNULL(T.classunkid,0) AS classunkid " & _
    '               "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
    '               "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
    '               "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '               "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
    '               "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
    '               "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
    '               "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
    '               "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
    '               "FROM " & strDatabaseName & "..hremployee_master AS AEM "
    '        If strUACJoin.Trim.Length > 0 Then
    '            StrQ &= strUACJoin
    '        End If
    '        StrQ &= "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        stationunkid " & _
    '                "       ,deptgroupunkid " & _
    '                "       ,departmentunkid " & _
    '                "       ,sectiongroupunkid " & _
    '                "       ,sectionunkid " & _
    '                "       ,unitgroupunkid " & _
    '                "       ,unitunkid " & _
    '                "       ,teamunkid " & _
    '                "       ,classgroupunkid " & _
    '                "       ,classunkid " & _
    '                "       ,employeeunkid " & _
    '                "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                "   FROM hremployee_transfer_tran " & _
    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '                ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        jobgroupunkid " & _
    '                "       ,jobunkid " & _
    '                "       ,employeeunkid " & _
    '                "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                "   FROM hremployee_categorization_tran " & _
    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '                ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 "

    '        StrQ &= "SELECT " & _
    '                "    EM.employeeunkid " & _
    '                "FROM @emp " & _
    '                "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
    '                "    JOIN " & _
    '                "    ( " & _
    '                "        SELECT DISTINCT " & _
    '                "             cfuser_master.userunkid " & _
    '                "            ,cfuser_master.username " & _
    '                "            ,cfuser_master.email " & _
    '                              strSelect & " " & _
    '                "            ,LM.priority " & _
    '                "            ,LM.empapplevelname AS LvName " & _
    '                "        FROM hrmsConfiguration..cfuser_master " & _
    '                "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '                "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '                             strAccessJoin & " " & _
    '                "        JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '                "        JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
    '                "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
    '        If strFilter.Trim.Length > 0 Then
    '            StrQ &= " AND (" & strFilter & " ) "
    '        End If
    '        StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
    '                "            AND companyunkid = @C AND yearunkid = @Y AND privilegeunkid = @P  " & _
    '                ") AS Fn ON " & strJoin & " "

    '        objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

    '        Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        If dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeUnkid).Count <= 0 Then
    '            strMessage = Language.getMessage(mstrModuleName, 60, "Sorry, No approver(s) defined based on selected for employee user access setting.")
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: IsOtherMovmentApproverPresent; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return strMessage
    'End Function

    'Public Function IsTransferCategorizationApprovers(ByVal eMovement As enMovementType, _
    '                                                   ByVal strDatabaseName As String, _
    '                                                   ByVal strUserAccessMode As String, _
    '                                                   ByVal intCompanyId As Integer, _
    '                                                   ByVal intYearId As Integer, _
    '                                                   ByVal intPrivilegeId As Integer, _
    '                                                   ByVal xStationId As Integer, _
    '                                                   ByVal xDeptGrpId As Integer, _
    '                                                   ByVal xDeptUnkid As Integer, _
    '                                                   ByVal xSecGrpId As Integer, _
    '                                                   ByVal xSecUnkid As Integer, _
    '                                                   ByVal xUnitGrpId As Integer, _
    '                                                   ByVal xUnitUnkid As Integer, _
    '                                                   ByVal xTeamUnkid As Integer, _
    '                                                   ByVal xClassGrpId As Integer, _
    '                                                   ByVal xClassUnkid As Integer, _
    '                                                   ByVal intJobGroupId As Integer, _
    '                                                   ByVal intJobId As Integer, _
    '                                                   Optional ByVal objDataOpr As clsDataOperation = Nothing) As String
    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Dim strMessage As String = ""
    '    Try
    '        Dim strFilter As String = String.Empty
    '        Dim strJoin As String = String.Empty
    '        Dim strSelect As String = String.Empty
    '        Dim strAccessJoin As String = String.Empty
    '        Dim strOuterJoin As String = String.Empty
    '        Dim strFinalString As String = ""
    '        Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, 0, eMovement, objDataOperation)

    '        StrQ = "SELECT DISTINCT " & _
    '             "   cfuser_master.userunkid " & _
    '             "  ,cfuser_master.username " & _
    '             "  ,cfuser_master.email " & _
    '             strSelect & " " & _
    '             "  ,LM.priority " & _
    '             "  ,LM.empapplevelname AS LvName " & _
    '             "FROM hrmsConfiguration..cfuser_master " & _
    '             "  JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '             "  JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '             strAccessJoin & " " & _
    '             "  JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '             "  JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
    '             "WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
    '        If strFilter.Trim.Length > 0 Then
    '            StrQ &= " AND (" & strFilter & " ) "
    '        End If
    '        StrQ &= " AND EM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " AND LM.approvertypeid = " & enEmpApproverType.APPR_MOVEMENT & " " & _
    '             " AND companyunkid = @C AND yearunkid = @Y AND privilegeunkid = @P "

    '        objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)
                        
    '        Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        Dim strSearching As String = String.Empty
    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))

    '            Select Case eMovement
    '                Case enMovementType.TRANSFERS
    '                    For index As Integer = 0 To strvalues.Length - 1
    '                        Select Case CInt(strvalues(index))
    '                            Case enAllocation.BRANCH
    '                                If xStationId > 0 Then
    '                                    strSearching &= "AND branchid = " & xStationId & " "
    '                                End If
    '                            Case enAllocation.DEPARTMENT_GROUP
    '                                If xDeptGrpId > 0 Then
    '                                    strSearching &= "AND deptgrpid = " & xDeptGrpId & " "
    '                                End If
    '                            Case enAllocation.DEPARTMENT
    '                                If xDeptUnkid > 0 Then
    '                                    strSearching &= "AND deptid = " & xDeptUnkid & " "
    '                                End If
    '                            Case enAllocation.SECTION_GROUP
    '                                If xSecGrpId > 0 Then
    '                                    strSearching &= "AND secgrpid = " & xSecGrpId & " "
    '                                End If
    '                            Case enAllocation.SECTION
    '                                If xSecUnkid > 0 Then
    '                                    strSearching &= "AND secid = " & xSecUnkid & " "
    '                                End If
    '                            Case enAllocation.UNIT_GROUP
    '                                If xUnitGrpId > 0 Then
    '                                    strSearching &= "AND unitgrpid = " & xUnitGrpId & " "
    '                                End If
    '                            Case enAllocation.UNIT
    '                                If xUnitUnkid > 0 Then
    '                                    strSearching &= "AND unitid = " & xUnitUnkid & " "
    '                                End If
    '                            Case enAllocation.TEAM
    '                                If xTeamUnkid > 0 Then
    '                                    strSearching &= "AND teamid = " & xTeamUnkid & " "
    '                                End If
    '                            Case enAllocation.CLASS_GROUP
    '                                If xClassGrpId > 0 Then
    '                                    strSearching &= "AND clsgrpid = " & xClassGrpId & " "
    '                                End If
    '                            Case enAllocation.CLASSES
    '                                If xClassUnkid > 0 Then
    '                                    strSearching &= "AND clsid = " & xClassUnkid & " "
    '                                End If
    '                        End Select
    '                    Next

                        'strSearching = strSearching.Substring(3)

    '                    Dim dtmp() As DataRow = dsList.Tables(0).Select(strSearching)
    '                    If dtmp.Length <= 0 Then
    '                        strMessage = Language.getMessage(mstrModuleName, 58, "Sorry, No approver(s) having access to the combination selected for employee transfer.")
    '                    End If

    '                Case enMovementType.RECATEGORIZE

    '                    For index As Integer = 0 To strvalues.Length - 1
    '                        Select Case CInt(strvalues(index))
    '                            Case enAllocation.JOB_GROUP
    '                                If intJobGroupId > 0 Then
    '                                    strSearching &= "AND jgrpid = " & intJobGroupId
    '                                End If
    '                            Case enAllocation.JOBS
    '                                If intJobId > 0 Then
    '                                    strSearching &= "AND jobid = " & intJobId
    '                                End If
    '                        End Select
    '                    Next

    'strSearching = strSearching.Substring(3)

    '                    Dim dtmp() As DataRow = dsList.Tables(0).Select(strSearching)
    '                    If dtmp.Length <= 0 Then
    '                        strMessage = Language.getMessage(mstrModuleName, 59, "Sorry, No approver(s) having access to the combination selected for employee recategorization.")
    '                    End If

    '            End Select
    '        Else
    '            strMessage = Language.getMessage(mstrModuleName, 57, "Sorry, Approver(s) not found in system.")
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetTransferCategorizationApprovers; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return strMessage
    'End Function
    'S.SANDEEP [18-SEP-2018] -- START

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Transfer")
            Language.setMessage(mstrModuleName, 2, "Re-Categorization")
            Language.setMessage(mstrModuleName, 3, "Probation")
            Language.setMessage(mstrModuleName, 4, "Confirmation")
            Language.setMessage(mstrModuleName, 5, "Suspension")
            Language.setMessage(mstrModuleName, 6, "Termination")
            Language.setMessage(mstrModuleName, 7, "Retirement")
            Language.setMessage(mstrModuleName, 8, "Work-Permit")
            Language.setMessage(mstrModuleName, 9, "Resident-Permit")
            Language.setMessage(mstrModuleName, 10, "CostCenter")
            Language.setMessage(mstrModuleName, 11, "Select")
            Language.setMessage(mstrModuleName, 12, "Job Group")
            Language.setMessage(mstrModuleName, 13, "Job")
            Language.setMessage(mstrModuleName, 14, "Permit No")
            Language.setMessage(mstrModuleName, 15, "Issue Place")
            Language.setMessage(mstrModuleName, 16, "Issue Date")
            Language.setMessage(mstrModuleName, 17, "Expiry Date")
            Language.setMessage(mstrModuleName, 18, "Issue Country")
            Language.setMessage(mstrModuleName, 19, "Branch")
            Language.setMessage(mstrModuleName, 20, "Department Group")
            Language.setMessage(mstrModuleName, 21, "Department")
            Language.setMessage(mstrModuleName, 22, "Section Group")
            Language.setMessage(mstrModuleName, 23, "Section")
            Language.setMessage(mstrModuleName, 24, "Unit Grpup")
            Language.setMessage(mstrModuleName, 25, "Unit")
            Language.setMessage(mstrModuleName, 26, "Team")
            Language.setMessage(mstrModuleName, 27, "Class Group")
            Language.setMessage(mstrModuleName, 28, "Class")
            Language.setMessage(mstrModuleName, 29, "Type")
            Language.setMessage(mstrModuleName, 30, "Confirmation")
            Language.setMessage(mstrModuleName, 31, "Retirement")
            Language.setMessage(mstrModuleName, 32, "Suspension")
            Language.setMessage(mstrModuleName, 33, "Termination")
            Language.setMessage(mstrModuleName, 34, "Probation")
            Language.setMessage(mstrModuleName, 35, "Recategorization")
            Language.setMessage(mstrModuleName, 36, "Resident Permit")
            Language.setMessage(mstrModuleName, 37, "Work Permit")
            Language.setMessage(mstrModuleName, 38, "Change Reason")
            Language.setMessage(mstrModuleName, 40, "This is to inform you that following employee(s) movement changes has been rejected")
            Language.setMessage(mstrModuleName, 41, "Rejection Remark:")
            Language.setMessage(mstrModuleName, 42, "Approval Remark:")
            Language.setMessage(mstrModuleName, 43, "Cost Center")
            Language.setMessage(mstrModuleName, 44, "Transfer")
            Language.setMessage(mstrModuleName, 45, "This is to inform you that following employee(s) having movement changes, Please login to aruti to approve/reject them.")
            Language.setMessage(mstrModuleName, 46, "Notification to Approve/Reject Emplyoee Movement")
            Language.setMessage(mstrModuleName, 47, "Effective Date")
            Language.setMessage(mstrModuleName, 48, "Employee")
            Language.setMessage(mstrModuleName, 49, "Confirmation Date")
            Language.setMessage(mstrModuleName, 50, "Retirement Date")
            Language.setMessage(mstrModuleName, 51, "Start Date")
            Language.setMessage(mstrModuleName, 52, "End Date")
            Language.setMessage(mstrModuleName, 53, "EOC Date")
            Language.setMessage(mstrModuleName, 54, "Leaving Date")
            Language.setMessage(mstrModuleName, 55, "Is Exclude Payroll")
            Language.setMessage(mstrModuleName, 60, "Sorry, No approver(s) defined based on selected for employee user access setting.")
            Language.setMessage(mstrModuleName, 61, "Select")
            Language.setMessage(mstrModuleName, 62, "Newly Added Information")
            Language.setMessage(mstrModuleName, 63, "Information Edited")
            Language.setMessage(mstrModuleName, 64, "Information Deleted")
			Language.setMessage(mstrModuleName, 65, "Exemption")
			Language.setMessage(mstrModuleName, 66, "Notification to Approve/Reject Emplyoee Exemption")
            Language.setMessage(mstrModuleName, 100, "Pending")
            Language.setMessage(mstrModuleName, 101, "Approved")
            Language.setMessage(mstrModuleName, 102, "Rejected")
            Language.setMessage(mstrModuleName, 103, "Pending for Approval")
            Language.setMessage(mstrModuleName, 104, "Notification to Approve/Reject Emplyoee Retirements")
            Language.setMessage(mstrModuleName, 105, "Notification to Approve/Reject Emplyoee Suspension")
            Language.setMessage(mstrModuleName, 106, "Notification to Approve/Reject Emplyoee Termination")
            Language.setMessage(mstrModuleName, 107, "Notification to Approve/Reject Emplyoee Transfer")
            Language.setMessage(mstrModuleName, 108, "Notification to Approve/Reject Emplyoee Transfer")
            Language.setMessage(mstrModuleName, 109, "Notification to Approve/Reject Emplyoee Probation")
            Language.setMessage(mstrModuleName, 110, "Notification to Approve/Reject Emplyoee Recategorization")
            Language.setMessage(mstrModuleName, 112, "Notification to Approve/Reject Emplyoee Resident Permit")
            Language.setMessage(mstrModuleName, 113, "Notification to Approve/Reject Emplyoee Confirmation")
            Language.setMessage(mstrModuleName, 114, "Notification to Approve/Reject Emplyoee CostCenter")
			Language.setMessage(mstrModuleName, 115, "Employee Exit")
			Language.setMessage(mstrModuleName, 116, "The following employee(s) are separating with")
			Language.setMessage(mstrModuleName, 117, ", kindly take note of it")
			Language.setMessage(mstrModuleName, 118, "Employee Code")
			Language.setMessage(mstrModuleName, 119, "Name")
			Language.setMessage(mstrModuleName, 120, "Effective Date")
			Language.setMessage(mstrModuleName, 121, "EOC/Leaving Date")
			Language.setMessage(mstrModuleName, 122, "Status")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")
			Language.setMessage("clsMasterData", 419, "Classes")
			Language.setMessage("clsMasterData", 420, "Class Group")
			Language.setMessage("clsMasterData", 421, "Jobs")
			Language.setMessage("clsMasterData", 426, "Section")
			Language.setMessage("clsMasterData", 427, "Section Group")
			Language.setMessage("clsMasterData", 428, "Department")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
