﻿'************************************************************************************************************************************
'Class Name : clsCategorization_Approval_Tran.vb
'Purpose    :
'Date       :30-Jul-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsCategorization_Approval_Tran
    Private Const mstrModuleName = "clsCategorization_Approval_Tran"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintCategorizationtranunkid As Integer
    Private mdtEffectivedate As Date
    Private mintEmployeeunkid As Integer
    Private mintRehiretranunkid As Integer
    Private mintJobgroupunkid As Integer
    Private mintJobunkid As Integer
    Private mintGradeunkid As Integer
    Private mintGradelevelunkid As Integer
    Private mintChangereasonunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsProcessed As Boolean = False
    'S.SANDEEP |17-JAN-2019| -- START
    Private mintOperationTypeId As Integer = 0
    'S.SANDEEP |17-JAN-2019| -- END
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categorizationtranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Categorizationtranunkid() As Integer
        Get
            Return mintCategorizationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintCategorizationtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Jobgroupunkid() As Integer
        Get
            Return mintJobgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradelevelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Gradelevelunkid() As Integer
        Get
            Return mintGradelevelunkid
        End Get
        Set(ByVal value As Integer)
            mintGradelevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mblnIsProcessed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsProcessed() As Boolean
        Get
            Return mblnIsProcessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsProcessed = value
        End Set
    End Property

    'S.SANDEEP |17-JAN-2019| -- START
    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property
    'S.SANDEEP |17-JAN-2019| -- END
#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intEmployeeId As Integer = 0, Optional ByVal blnOnlyPending As Boolean = True) As DataSet 'S.SANDEEP [14-AUG-2018] -- START {Ref#244} [blnOnlyPending] -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
               "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
               "FROM hremployee_master " & _
               "WHERE employeeunkid = '" & intEmployeeId & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")


            strQ = "SELECT " & _
                    "  hremployee_categorization_approval_tran.tranguid " & _
                    ", hremployee_categorization_approval_tran.transactiondate " & _
                    ", hremployee_categorization_approval_tran.mappingunkid " & _
                    ", hremployee_categorization_approval_tran.remark " & _
                    ", hremployee_categorization_approval_tran.isfinal " & _
                    ", hremployee_categorization_approval_tran.statusunkid " & _
                    ", hremployee_categorization_approval_tran.categorizationtranunkid " & _
                    ", CONVERT(CHAR(8),hremployee_categorization_approval_tran.effectivedate,112) AS effectivedate " & _
                    ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                    ", hremployee_categorization_approval_tran.employeeunkid " & _
                    ", hremployee_categorization_approval_tran.jobgroupunkid " & _
                    ", hremployee_categorization_approval_tran.jobunkid " & _
                    ", hremployee_categorization_approval_tran.gradeunkid " & _
                    ", hremployee_categorization_approval_tran.gradelevelunkid " & _
                    ", hremployee_categorization_approval_tran.changereasonunkid " & _
                    ", ISNULL(hrjobgroup_master.name,'') AS JobGroup " & _
                    ", ISNULL(hrjob_master.job_name,'') AS Job " & _
                    ", ISNULL(hrgrade_master.name,'') AS Grade " & _
                    ", ISNULL(hrgradelevel_master.name,'') AS GradeLevel " & _
                    ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS Reason " & _
                    ", ISNULL(hremployee_categorization_approval_tran.rehiretranunkid,0) AS rehiretranunkid " & _
                    ", hremployee_categorization_approval_tran.isvoid " & _
                    ", hremployee_categorization_approval_tran.voiddatetime " & _
                    ", hremployee_categorization_approval_tran.voidreason " & _
                    ", hremployee_categorization_approval_tran.voiduserunkid " & _
                    ", hremployee_categorization_approval_tran.audittype " & _
                    ", hremployee_categorization_approval_tran.audituserunkid " & _
                    ", hremployee_categorization_approval_tran.ip " & _
                    ", hremployee_categorization_approval_tran.hostname " & _
                    ", hremployee_categorization_approval_tran.form_name " & _
                    ", hremployee_categorization_approval_tran.isweb " & _
                    ", ISNULL(hremployee_categorization_approval_tran.operationtypeid,0) AS operationtypeid " & _
                   ", CASE WHEN ISNULL(hremployee_categorization_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                   "       WHEN ISNULL(hremployee_categorization_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                   "       WHEN ISNULL(hremployee_categorization_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                   "  END AS OperationType " & _
                    " FROM hremployee_categorization_approval_tran " & _
                    " JOIN hremployee_master on hremployee_master.employeeunkid = hremployee_categorization_approval_tran.employeeunkid  " & _
                    " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_categorization_approval_tran.jobgroupunkid " & _
                    " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_categorization_approval_tran.jobunkid " & _
                    " LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hremployee_categorization_approval_tran.gradeunkid " & _
                    " LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = hremployee_categorization_approval_tran.gradelevelunkid " & _
                    " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_categorization_approval_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RECATEGORIZE & _
                    " LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_categorization_approval_tran.changereasonunkid AND RH.mastertype =  " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                    "WHERE hremployee_categorization_approval_tran.isvoid = 0 AND hremployee_categorization_approval_tran.categorizationtranunkid >= 0 AND ISNULL(hremployee_categorization_approval_tran.isprocessed,0) = 0 "
            'S.SANDEEP |17-JAN-2019| -- START {operationtypeid} -- END
            If intEmployeeId > 0 Then
                strQ &= " AND hremployee_categorization_approval_tran.employeeunkid = '" & intEmployeeId & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_categorization_approval_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "
            'S.SANDEEP |17-JAN-2019| -- START


            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 62, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 63, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 64, "Information Deleted"))
            'S.SANDEEP |17-JAN-2019| -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [14-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim strValue As String = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empstat.ToString()).ToArray())
            'S.SANDEEP [09-AUG-2018] -- START {Added : x.empstat | Removed : x.empid} -- END
            If strValue.Trim.Length > 0 Then
                'S.SANDEEP [09-AUG-2018] -- START
                'Dim dtTbl As DataTable = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                Dim dtTbl As DataTable = Nothing
                If strValue = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                    strValue = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empid.ToString()).ToArray())
                    If strValue.Trim.Length > 0 Then
                        dtTbl = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                    End If
                Else
                    dtTbl = New DataView(dsList.Tables(0), "statusunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                End If
                'S.SANDEEP [09-AUG-2018] -- END
                dsList.Tables.Remove(strTableName)
                dsList.Tables.Add(dtTbl)
            End If
            'S.SANDEEP [14-AUG-2018] -- END

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("effectivedate") = eZeeDate.convertDate(xRow.Item("effectivedate").ToString).ToShortDateString
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_categorization_approval_tran) </purpose>
    Public Function Insert(ByVal intCompanyId As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, _
                           ByVal mblnCreateADUserFromEmpMst As Boolean, _
                           ByVal xDatabaseName As String, _
                           Optional ByVal objDOperation As clsDataOperation = Nothing, _
                           Optional ByVal blnFromApprovalScreen As Boolean = False, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False) As Boolean

        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ByVal xDatabaseName As String]

        'Pinkal (07-Dec-2019) --Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval}

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP |17-JAN-2019| -- START


        Dim objDataOperation As clsDataOperation

        'If objDOperation Is Nothing Then
        '    objDataOperation = New clsDataOperation
        'Else
        '    objDataOperation = objDOperation
        'End If

        If objDOperation IsNot Nothing Then
            objDataOperation = objDOperation
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP |17-JAN-2019| -- END

        If blnFromApprovalScreen = False Then
            If isExist(Nothing, mintEmployeeunkid, -1, -1, -1, -1, eOprType, "", objDataOperation, True, mintCategorizationtranunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Re-Categorize information is already present in pending state for the selected employee.")
                Return False
            End If
            'S.SANDEEP |17-JAN-2019| -- START

            'If isExist(mdtEffectivedate.Date, mintEmployeeunkid, -1, -1, -1, -1, eOprType, "", objDataOperation) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Re-Categorize information is already present for the selected effective date.")
            '    Return False
            'End If

            'If isExist(mdtEffectivedate.Date, mintEmployeeunkid, mintJobgroupunkid, mintJobunkid, mintGradeunkid, mintGradelevelunkid, eOprType, objDataOperation) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
            '    Return False
            'End If

            'S.SANDEEP |17-JAN-2019| -- END
        End If

        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategorizationtranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcessed)
            'S.SANDEEP |17-JAN-2019| -- START
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)
            'S.SANDEEP |17-JAN-2019| -- END
            strQ = "INSERT INTO hremployee_categorization_approval_tran ( " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", categorizationtranunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", jobgroupunkid " & _
              ", jobunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", changereasonunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb" & _
              ", isprocessed " & _
              ", operationtypeid " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @remark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @categorizationtranunkid " & _
              ", @effectivedate " & _
              ", @employeeunkid " & _
              ", @rehiretranunkid " & _
              ", @jobgroupunkid " & _
              ", @jobunkid " & _
              ", @gradeunkid " & _
              ", @gradelevelunkid " & _
              ", @changereasonunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @voiduserunkid " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @hostname " & _
              ", @form_name " & _
              ", @isweb" & _
              ", @isprocessed " & _
              ", @operationtypeid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            If blnFromApprovalScreen = False Then

                'Pinkal (07-Dec-2019) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                'If MigrationInsert(intCompanyId, eOprType, objDataOperation, xMdtMigration, xblnFromApproval, blnFromApprovalScreen) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                ' If MigrationInsert(intCompanyId, eOprType, mblnCreateADUserFromEmpMst, objDataOperation, xMdtMigration, xblnFromApproval, blnFromApprovalScreen) = False Then
                If MigrationInsert(xDatabaseName, intCompanyId, eOprType, mblnCreateADUserFromEmpMst, objDataOperation, xMdtMigration, xblnFromApproval, blnFromApprovalScreen) = False Then
                    'Pinkal (12-Oct-2020) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Pinkal (07-Dec-2019) -- End

            End If
            'Gajanan [23-SEP-2019] -- End


            'mstrTranguid = dsList.Tables(0).Rows(0).Item(0)
            'S.SANDEEP |17-JAN-2019| -- START

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START


            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP |17-JAN-2019| -- END            
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intTranUnkid As Integer, ByVal dtEffDate As Date, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP |17-JAN-2019| -- START

        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        'S.SANDEEP |17-JAN-2019| -- END
        Try
            'S.SANDEEP [15-NOV-2018] -- START
            strQ = "UPDATE hremployee_categorization_approval_tran SET " & _
                   " " & strColName & " = '" & intTranUnkid & "' " & _
                   "WHERE isprocessed = 0 and CONVERT(CHAR(8),effectivedate,112) = '" & eZeeDate.convertDate(dtEffDate).ToString & "' AND employeeunkid = '" & intEmployeeId & "' "
            '----REMOVED (effectivedate)
            '----ADDED (CONVERT(CHAR(8),effectivedate,112))
            'S.SANDEEP [15-NOV-2018] -- End
            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP |17-JAN-2019| -- END            
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            'S.SANDEEP |17-JAN-2019| -- START


            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP |17-JAN-2019| -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_categorization_approval_tran) </purpose>
    Public Function Update(ByVal intCompanyId As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP |17-JAN-2019| -- add Opration Type
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP |17-JAN-2019| -- START


        Dim objDataOperation As clsDataOperation
        'If xDataOpr IsNot Nothing Then
        '    objDataOperation = xDataOpr
        '    objDataOperation.ClearParameters()
        'Else
        '    objDataOperation = New clsDataOperation
        'End If
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP |17-JAN-2019| -- END

        If isExist(mdtEffectivedate.Date, mintEmployeeunkid, -1, -1, -1, -1, eOprType, mstrTranguid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Re-Categorize information is already present for the selected effective date.")
            Return False
        End If

        If isExist(mdtEffectivedate.Date, mintEmployeeunkid, mintJobgroupunkid, mintJobunkid, mintGradeunkid, mintGradelevelunkid, eOprType, mstrTranguid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
            Return False
        End If

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategorizationtranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            'S.SANDEEP |17-JAN-2019| -- START
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)
            'S.SANDEEP |17-JAN-2019| -- END
            strQ = "UPDATE hremployee_categorization_approval_tran SET " & _
              "  transactiondate = @transactiondate" & _
              ", mappingunkid = @mappingunkid" & _
              ", remark = @remark" & _
              ", isfinal = @isfinal" & _
              ", statusunkid = @statusunkid" & _
              ", categorizationtranunkid = @categorizationtranunkid" & _
              ", effectivedate = @effectivedate" & _
              ", employeeunkid = @employeeunkid" & _
              ", rehiretranunkid = @rehiretranunkid" & _
              ", jobgroupunkid = @jobgroupunkid" & _
              ", jobunkid = @jobunkid" & _
              ", gradeunkid = @gradeunkid" & _
              ", gradelevelunkid = @gradelevelunkid" & _
              ", changereasonunkid = @changereasonunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", audittype = @audittype" & _
              ", audituserunkid = @audituserunkid" & _
              ", ip = @ip" & _
              ", hostname = @hostname" & _
              ", form_name = @form_name" & _
              ", isweb = @isweb " & _
              ", operationtypeid = @operationtypeid " & _
            "WHERE tranguid = @tranguid "
            'S.SANDEEP |17-JAN-2019| -- START {operationtypeid} -- END


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START



            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP |17-JAN-2019| -- END            
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    '' <summary>
    '' Modify By: Sandeep Sharma
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Delete Database Table (hremployee_categorization_approval_tran) </purpose>
   'S.SANDEEP |17-JAN-2019| -- START


    Public Function Delete(ByVal intUnkid As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, _
                           ByVal intCompanyId As Integer, ByVal mblnCreateADUserFromEmpMst As Boolean, _
                           ByVal xDatabaseName As String, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False, _
                           Optional ByVal xblnFromApprovalScreen As Boolean = False) As Boolean


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ByVal xDatabaseName As String]

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval,xblnFromApprovalScreen}

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

 		'Dim objDataOperation As New clsDataOperation

        'S.SANDEEP |17-JAN-2019| -- START

        Dim objDataOperation As clsDataOperation

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP |17-JAN-2019| -- END

        mstrMessage = ""
        Try
            Dim objemployee_categorization_Tran As New clsemployee_categorization_Tran

            objemployee_categorization_Tran._Categorizationtranunkid(objDataOperation) = intUnkid


            Me._Audittype = enAuditType.ADD
            Me._Effectivedate = objemployee_categorization_Tran._Effectivedate
            Me._Employeeunkid = objemployee_categorization_Tran._Employeeunkid
            Me._Jobgroupunkid = objemployee_categorization_Tran._JobGroupunkid
            Me._Jobunkid = objemployee_categorization_Tran._Jobunkid
            Me._Gradeunkid = objemployee_categorization_Tran._Gradeunkid
            Me._Gradelevelunkid = objemployee_categorization_Tran._Gradelevelunkid
            Me._Changereasonunkid = objemployee_categorization_Tran._Changereasonunkid
            Me._Voiddatetime = Nothing
            Me._Voidreason = ""
            Me._Voiduserunkid = -1
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Remark = ""
            Me._Rehiretranunkid = 0
            Me._Mappingunkid = 0
            Me._Isfinal = False
            Me._Categorizationtranunkid = intUnkid
            Me._IsProcessed = False
            Me._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
            Me._Form_Name = mstrForm_Name
            Me._Hostname = mstrHostname
            Me._Ip = mstrIp
            Me._Audituserunkid = mintAudituserunkid
            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            Me._Isvoid = False
            'Gajanan [9-July-2019] -- End

            objemployee_categorization_Tran._Voidreason = Me._Voidreason
            objemployee_categorization_Tran._Voiduserunkid = Me._Audituserunkid
            objemployee_categorization_Tran._Voiddatetime = Now
            objemployee_categorization_Tran._Isvoid = True

            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            objemployee_categorization_Tran._AuditUserId = Me._Audituserunkid
            objemployee_categorization_Tran._ClientIP = Me._Ip
            objemployee_categorization_Tran._FormName = Me._Form_Name
            objemployee_categorization_Tran._HostName = Me._Hostname
            'Gajanan [9-July-2019] -- End

            'S.SANDEEP |12-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : DELETE MOVEMENTS NOT WORKING DUE TO FALSE CONDITION CHECKPOINT
            'If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then
            '    If objemployee_categorization_Tran.Delete(intUnkid, intCompanyId, mblnCreateADUserFromEmpMst, objDataOperation) = False Then
            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            'S.SANDEEP |12-DEC-2019| -- END


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'blnFlag = Insert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, objDataOperation)


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'blnFlag = Insert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, ConfigParameter._Object._CreateADUserFromEmpMst _
            '                      , objDataOperation, xblnFromApprovalScreen, xMdtMigration, xblnFromApproval)

            blnFlag = Insert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, mblnCreateADUserFromEmpMst _
                                  , xDatabaseName, objDataOperation, xblnFromApprovalScreen, xMdtMigration, xblnFromApproval)
            'Pinkal (12-Oct-2020) -- End

            


            'Gajanan [23-SEP-2019] -- End


            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then

                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objemployee_categorization_Tran.Delete(intUnkid, intCompanyId, mblnCreateADUserFromEmpMst, objDataOperation) = False Then
                If objemployee_categorization_Tran.Delete(intUnkid, intCompanyId, mblnCreateADUserFromEmpMst, xDatabaseName, objDataOperation) = False Then
                    'Pinkal (12-Oct-2020) -- End
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                End If
            End If


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return blnFlag
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP |17-JAN-2019| -- END
    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtEffectiveDate As Date, ByVal intEmployeeId As Integer, _
                            ByVal intJobGroupId As Integer, ByVal intJobId As Integer, _
                            ByVal intGradeId As Integer, ByVal intGradeLevelId As Integer, _
                            ByVal xeOprType As clsEmployeeMovmentApproval.enOperationType, _
                            Optional ByVal strtranguid As String = "", Optional ByVal objDOperation As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = False, Optional ByVal xTranferUnkid As Integer = 0) As Boolean 'S.SANDEEP |17-JAN-2019| -- START {xTranferUnkid} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP |17-JAN-2019| -- START


        Dim objDataOperation As clsDataOperation

        'If objDOperation Is Nothing Then
        '    objDataOperation = New clsDataOperation
        'Else
        '    objDataOperation = objDOperation
        'End If
        'objDataOperation.ClearParameters()

        If objDOperation IsNot Nothing Then
            objDataOperation = objDOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP |17-JAN-2019| -- END
        Try
            strQ = "SELECT " & _
                    "  tranguid " & _
                    ", transactiondate " & _
                    ", mappingunkid " & _
                    ", remark " & _
                    ", isfinal " & _
                    ", statusunkid " & _
                    ", categorizationtranunkid " & _
                    ", effectivedate " & _
                    ", employeeunkid " & _
                    ", rehiretranunkid " & _
                    ", jobgroupunkid " & _
                    ", jobunkid " & _
                    ", gradeunkid " & _
                    ", gradelevelunkid " & _
                    ", changereasonunkid " & _
                    ", isvoid " & _
                    ", voiddatetime " & _
                    ", voidreason " & _
                    ", voiduserunkid " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", ip " & _
                    ", hostname " & _
                    ", form_name " & _
                    ", isweb " & _
                    "FROM hremployee_categorization_approval_tran " & _
             "WHERE isvoid = 0 and operationtypeid = @operationtypeid " & _
             "AND statusunkid <> 3 "

            If blnPreventNewEntry Then
                'S.SANDEEP [09-AUG-2018] -- START
                'strQ &= " AND hremployee_categorization_approval_tran.isfinal = 0 "
                strQ &= " AND hremployee_categorization_approval_tran.isfinal = 0 AND ISNULL(isprocessed,0) = 0 "

                'S.SANDEEP [09-AUG-2018] -- END
            End If

            If strtranguid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid"
            End If

            If intEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If intJobGroupId > 0 Then
                strQ &= " AND jobgroupunkid = @jobgroupunkid "
                objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobGroupId)
            End If

            If intJobId > 0 Then
                strQ &= " AND jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)
            End If

            If intGradeId > 0 Then
                strQ &= " AND gradeunkid = @gradeunkid "
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeId)
            End If

            If intGradeLevelId > 0 Then
                strQ &= " AND gradelevelunkid = @gradelevelunkid "
                objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeLevelId)
            End If

            If dtEffectiveDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate"
                objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEffectivedate))
            End If


            'S.SANDEEP |17-JAN-2019| -- START
            If xeOprType = clsEmployeeMovmentApproval.enOperationType.EDITED Or mintOperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED Then
                If xTranferUnkid > 0 Then
                    strQ &= " AND categorizationtranunkid = @categorizationtranunkid "
                    objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTranferUnkid)
                End If
            End If
            'S.SANDEEP |17-JAN-2019| -- END

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            'S.SANDEEP |17-JAN-2019| -- START
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strtranguid)
            'S.SANDEEP |17-JAN-2019| -- END              
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xeOprType)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function MigrationInsert(ByVal xDatabaseName As String, ByVal intCompanyId As Integer, _
                                    ByVal xOprationType As clsEmployeeMovmentApproval.enOperationType, _
                                    ByVal mblnCreateADUserFromEmpMst As Boolean, _
                                    Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                    Optional ByVal xMdtMigration As DataTable = Nothing, _
                                    Optional ByVal xblnFromApproval As Boolean = False, _
                                    Optional ByVal xblnFromApprovalScreen As Boolean = False, _
                                    Optional ByVal eStatusId As clsEmployee_Master.EmpApprovalStatus = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval) As Boolean

        'Pinkal (12-Oct-2020) --Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens. [ByVal xDatabaseName As String]

        'Pinkal (07-Dec-2019) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ ByVal mblnCreateADUserFromEmpMst As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()
        Try
            If IsNothing(xMdtMigration) = False AndAlso xMdtMigration.Rows.Count > 0 Then
                Dim objMovementMigration As New clsMovementMigration

                Dim objConfig As New clsConfigOptions
                Dim strParamKeys() As String = { _
                                           "EMPLOYEEASONDATE", _
                                           "CLAIMREQUEST_PAYMENTAPPROVALWITHLEAVEAPPROVAL" _
                                           }
                Dim MigrationDicKeyValues As New Dictionary(Of String, String)
                MigrationDicKeyValues = objConfig.GetKeyValue(intCompanyId, strParamKeys, objDataOperation)

                If IsNothing(MigrationDicKeyValues) = False _
                AndAlso MigrationDicKeyValues.ContainsKey("EmployeeAsOnDate") _
                AndAlso MigrationDicKeyValues.ContainsKey("ClaimRequest_PaymentApprovalwithLeaveApproval") Then
                    Dim Formname As String = String.Empty
                    Formname = mstrForm_Name

                    If IsNothing(xMdtMigration) = True Then
                        objMovementMigration.isMovementInApprovalFlow(objMovementMigration._DataList, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, _
                                                                      enApprovalMigrationScreenType.RECATEGORIZATION, 0, objDataOperation)

                        If IsNothing(objMovementMigration._DataList) = False Then
                            xMdtMigration = objMovementMigration._DataList
                        End If

                    End If

                    objMovementMigration._Ip = mstrIp
                    objMovementMigration._Hostname = mstrHostname
                    objMovementMigration._Form_Name = mstrForm_Name
                    objMovementMigration._Isweb = mblnIsweb


                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                    'If objMovementMigration.Insert(xMdtMigration, mintCategorizationtranunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.RECATEGORIZATION), _
                    '                         xblnFromApproval, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
                    '                        CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintAudituserunkid, Formname, _
                    '                      CInt(xOprationType), mblnCreateADUserFromEmpMst, xblnFromApprovalScreen, objDataOperation) = False Then

                    If objMovementMigration.Insert(xDatabaseName, xMdtMigration, mintCategorizationtranunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.RECATEGORIZATION), _
                                             xblnFromApproval, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
                                            CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintAudituserunkid, Formname, _
                                          CInt(xOprationType), mblnCreateADUserFromEmpMst, xblnFromApprovalScreen, objDataOperation) = False Then

                        'Pinkal (12-Oct-2020) -- End

                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce

                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If

        End Try
    End Function
    'Gajanan [23-SEP-2019] -- End  

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Re-Categorize information is already present for the selected effective date.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Re-Categorize information is already present in pending state for the selected employee.")
            Language.setMessage("clsEmployeeMovmentApproval", 62, "Newly Added Information")
            Language.setMessage("clsEmployeeMovmentApproval", 63, "Information Edited")
            Language.setMessage("clsEmployeeMovmentApproval", 64, "Information Deleted")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
