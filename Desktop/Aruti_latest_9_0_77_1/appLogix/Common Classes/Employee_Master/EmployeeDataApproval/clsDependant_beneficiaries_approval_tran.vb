﻿Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Imports System.Security.Cryptography

Public Class clsDependant_beneficiaries_approval_tran
    Private Const mstrModuleName = "clsDependant_beneficiaries_approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objImages As New clshr_images_tran
    Dim objDependantImages As New clsdependant_Images
    Dim objDependantMemInfoTran As New clsDependants_Membership_tran
    Dim objDependantBenefitTran As New clsDependants_Benefit_tran


#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrApprovalremark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintDpndtbeneficetranunkid As Integer = -1
    Private mintEmployeeunkid As Integer
    Private mstrFirst_Name As String = String.Empty
    Private mstrLast_Name As String = String.Empty
    Private mstrMiddle_Name As String = String.Empty
    Private mintRelationunkid As Integer
    Private mdtBirthdate As Date
    Private mstrAddress As String = String.Empty
    Private mstrTelephone_No As String = String.Empty
    Private mstrMobile_No As String = String.Empty
    Private mstrPost_Box As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mintZipcodeunkid As Integer
    Private mstrEmail As String = String.Empty
    Private mintNationalityunkid As Integer
    Private mstrIdentify_No As String = String.Empty
    Private mblnIsdependant As Boolean
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtAuditdatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsprocessed As Boolean

    Private mintOperationtypeid As Integer
    Private mstrImagePath As String
    Private mintGender As Integer
    Private mintCompanyID As Integer = -1
    Private mblnImgInDb As Boolean = False
    Private mbytPhoto As Byte() = Nothing
    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mdtEffective_date As Date
    Private mblnIsactive As Boolean = True
    Private mintReasonUnkid As Integer = 0
    Private mblnIsFromStatus As Boolean = False
    Private mstrDeletedpndtbeneficestatustranIDs As String = String.Empty
    'Sohail (18 May 2019) -- End

    Private mstrnewattachdocumnetid As String = String.Empty
    Private mstrdeleteattachdocumnetid As String = String.Empty
    Private mintnewattachimageid As Integer = -1
    Private mintdeleteattachimageid As Integer = -1

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalremark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dpndtbeneficetranunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Dpndtbeneficetranunkid() As Integer
        Get
            Return mintDpndtbeneficetranunkid
        End Get
        Set(ByVal value As Integer)
            mintDpndtbeneficetranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set first_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _First_Name() As String
        Get
            Return mstrFirst_Name
        End Get
        Set(ByVal value As String)
            mstrFirst_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set last_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Last_Name() As String
        Get
            Return mstrLast_Name
        End Get
        Set(ByVal value As String)
            mstrLast_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set middle_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Middle_Name() As String
        Get
            Return mstrMiddle_Name
        End Get
        Set(ByVal value As String)
            mstrMiddle_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set relationunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Relationunkid() As Integer
        Get
            Return mintRelationunkid
        End Get
        Set(ByVal value As Integer)
            mintRelationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthdate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birthdate() As Date
        Get
            Return mdtBirthdate
        End Get
        Set(ByVal value As Date)
            mdtBirthdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Address() As String
        Get
            Return mstrAddress
        End Get
        Set(ByVal value As String)
            mstrAddress = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set telephone_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Telephone_No() As String
        Get
            Return mstrTelephone_No
        End Get
        Set(ByVal value As String)
            mstrTelephone_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mobile_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mobile_No() As String
        Get
            Return mstrMobile_No
        End Get
        Set(ByVal value As String)
            mstrMobile_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set post_box
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Post_Box() As String
        Get
            Return mstrPost_Box
        End Get
        Set(ByVal value As String)
            mstrPost_Box = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set zipcodeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Zipcodeunkid() As Integer
        Get
            Return mintZipcodeunkid
        End Get
        Set(ByVal value As Integer)
            mintZipcodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nationalityunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Nationalityunkid() As Integer
        Get
            Return mintNationalityunkid
        End Get
        Set(ByVal value As Integer)
            mintNationalityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set identify_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Identify_No() As String
        Get
            Return mstrIdentify_No
        End Get
        Set(ByVal value As String)
            mstrIdentify_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdependant
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isdependant() As Boolean
        Get
            Return mblnIsdependant
        End Get
        Set(ByVal value As Boolean)
            mblnIsdependant = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocessed
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    Public Property _Newattacheddocumnetid() As String
        Get
            Return mstrnewattachdocumnetid
        End Get
        Set(ByVal value As String)
            mstrnewattachdocumnetid = value
        End Set
    End Property

    Public Property _Deletedattachdocumnetid() As String
        Get
            Return mstrdeleteattachdocumnetid
        End Get
        Set(ByVal value As String)
            mstrdeleteattachdocumnetid = value
        End Set
    End Property

    Public Property _Newattachedimageid() As Integer
        Get
            Return mintnewattachimageid
        End Get
        Set(ByVal value As Integer)
            mintnewattachimageid = value
        End Set
    End Property

    Public Property _Deletedattachedimageid() As Integer
        Get
            Return mintdeleteattachimageid
        End Get
        Set(ByVal value As Integer)
            mintdeleteattachimageid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set operationtypeid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Operationtypeid() As Integer
        Get
            Return mintOperationtypeid
        End Get
        Set(ByVal value As Integer)
            mintOperationtypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Image Path
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _ImagePath() As String
        Get
            Return mstrImagePath
        End Get
        Set(ByVal value As String)
            mstrImagePath = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gender
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Gender() As Integer
        Get
            Return mintGender
        End Get
        Set(ByVal value As Integer)
            mintGender = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _CompanyId() As Integer
        Get
            Return mintCompanyID
        End Get
        Set(ByVal value As Integer)
            mintCompanyID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set blnImgInDb
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _blnImgInDb() As Boolean
        Get
            Return mblnImgInDb
        End Get
        Set(ByVal value As Boolean)
            mblnImgInDb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Photo
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Photo() As Byte()
        Get
            Return mbytPhoto
        End Get
        Set(ByVal value As Byte())
            mbytPhoto = value
        End Set
    End Property

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Public Property _Effective_date() As Date
        Get
            Return mdtEffective_date
        End Get
        Set(ByVal value As Date)
            mdtEffective_date = value
        End Set
    End Property

    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public Property _ReasonUnkid() As Integer
        Get
            Return mintReasonUnkid
        End Get
        Set(ByVal value As Integer)
            mintReasonUnkid = value
        End Set
    End Property

    Public Property _IsFromStatus() As Boolean
        Get
            Return mblnIsFromStatus
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromStatus = value
        End Set
    End Property

    Public Property _DeletedpndtbeneficestatustranIDs() As String
        Get
            Return mstrDeletedpndtbeneficestatustranIDs
        End Get
        Set(ByVal value As String)
            mstrDeletedpndtbeneficestatustranIDs = value
        End Set
    End Property
    'Sohail (18 May 2019) -- End

#End Region

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal intEmployeeID As Integer = -1, Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet


        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.first_name,'') +  ' ' + ISNULL(hrdependant_beneficiaries_approval_tran.middle_name,'') +' '+ISNULL(hrdependant_beneficiaries_approval_tran.last_name,'') AS DpndtBefName " & _
                       ",ISNULL(cfcommon_master.name,'') AS Relation " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.identify_no,'') AS IdNo " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.first_name,'') AS FName " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.last_name,'') AS LName " & _
                       ",cfcommon_master.masterunkid AS RelationId " & _
                       ",hrdependant_beneficiaries_approval_tran.countryunkid AS CountryId " & _
                       ",hremployee_master.employeeunkid AS EmpId " & _
                       ",hrdependant_beneficiaries_approval_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.isdependant,0) AS IsDependant " & _
                       ",CASE WHEN hrdependant_beneficiaries_approval_tran.gender = 1 THEN @Male " & _
                       "       WHEN hrdependant_beneficiaries_approval_tran.gender = 2 THEN @Female " & _
                       " ELSE '' END AS Gender " & _
                       ",hrdependant_beneficiaries_approval_tran.gender AS GenderId " & _
                       ",ISNULL(CONVERT(CHAR(8),hrdependant_beneficiaries_approval_tran.birthdate,112),'') as birthdate " & _
                       ",hrdependant_beneficiaries_approval_tran.tranguid As tranguid " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.operationtypeid,0) AS operationtypeid " & _
                       ",CASE WHEN ISNULL(hrdependant_beneficiaries_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                       "      WHEN ISNULL(hrdependant_beneficiaries_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                       "      WHEN ISNULL(hrdependant_beneficiaries_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                       "END AS OperationType " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.effective_date, GETDATE()) AS effective_date " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.isactive, 1) AS isactive " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.reasonunkid, 0) AS reasonunkid " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.isfromstatus, 0) AS isfromstatus " & _
                       ",ISNULL(hrdependant_beneficiaries_approval_tran.deletedpndtbeneficestatustranids, '') AS deletedpndtbeneficestatustranids " & _
                       "FROM hrdependant_beneficiaries_approval_tran " & _
                       " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependant_beneficiaries_approval_tran.relationunkid AND cfcommon_master.mastertype = " & enCommonMaster.RELATIONS & _
                       " LEFT JOIN hremployee_master ON hrdependant_beneficiaries_approval_tran.employeeunkid = hremployee_master.employeeunkid "
                'Sohail (18 May 2019) - [effective_date, isactive, reasonunkid, isfromstatus, deletedpndtbeneficestatustranids]

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then

                        'Gajanan [27-May-2019] -- Start              
                        'StrQ &= xUACQry
                        StrQ &= "LEFT " & xUACQry
                        'Gajanan [27-Ma y-2019] -- End

                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " WHERE ISNULL(hrdependant_beneficiaries_approval_tran.isvoid,0) = 0 AND ISNULL(hrdependant_beneficiaries_approval_tran.isprocessed,0) = 0"

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If intEmployeeID > 0 Then
                    StrQ &= " AND hrdependant_beneficiaries_approval_tran.employeeunkid = @employeeunkid  "
                    objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If

                objDo.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDo.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
                objDo.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information"))
                objDo.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 63, "Information Edited"))
                objDo.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 64, "Information Deleted"))

                StrQ &= " ORDER BY ISNULL(CONVERT(CHAR(8),hrdependant_beneficiaries_approval_tran.birthdate,112),'') "
                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function Get_Benefit_Tran(ByVal tranguid As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception

        Try
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                  "hrdependant_approval_benefit_tran.dpndtbenefittranunkid " & _
                  ",hrdependant_approval_benefit_tran.dpndtbeneficetranunkid " & _
                  ",hrdependant_approval_benefit_tran.benefitgroupunkid " & _
                  ",hrdependant_approval_benefit_tran.benefitplanunkid " & _
                  ",hrdependant_approval_benefit_tran.value_id " & _
                   ",hrdependant_approval_benefit_tran.benefit_percent " & _
                  ",hrdependant_approval_benefit_tran.benefit_amount " & _
                  ",CASE " & _
                      "WHEN hrdependant_approval_benefit_tran.audittype = 3 THEN 1 " & _
                      "ELSE 0 " & _
                  "END AS Isvoid " & _
                  ",hrdependant_beneficiaries_approval_tran.audituserunkid as voiduserunkid " & _
                  ",hrdependant_beneficiaries_approval_tran.auditdatetime as voiddatetime " & _
                  ",hrdependant_approval_benefit_tran.voidreason " & _
                  ",hrbenefitplan_master.benefitplanname " & _
                  ",cfcommon_master.name AS benefit_group " & _
                  ",hrbenefitplan_master.benefitplanname AS benefit_type " & _
                  ",CASE hrdependant_approval_benefit_tran.value_id " & _
                      "WHEN 1 THEN @Value " & _
                      "WHEN 2 THEN @Percent " & _
                      "ELSE '' " & _
                  "END AS benefit_value " & _
                  ",CASE " & _
                      "WHEN hrdependant_approval_benefit_tran.audittype = 1 THEN 'A' " & _
                      "WHEN hrdependant_approval_benefit_tran.audittype = 2 THEN 'U' " & _
                      "WHEN hrdependant_approval_benefit_tran.audittype = 3 THEN 'D' " & _
                      "ELSE '' " & _
                  "END AS AUD " & _
                  "FROM hrdependant_approval_benefit_tran " & _
                  "LEFT JOIN hrdependant_beneficiaries_approval_tran " & _
                     "ON hrdependant_beneficiaries_approval_tran.tranguid= hrdependant_approval_benefit_tran.approvaltranguid " & _
                  "LEFT JOIN cfcommon_master " & _
                     "ON cfcommon_master.masterunkid = hrdependant_approval_benefit_tran.benefitgroupunkid " & _
                  "LEFT JOIN hrbenefitplan_master " & _
                     "ON hrbenefitplan_master.benefitplanunkid = hrdependant_approval_benefit_tran.benefitplanunkid " & _
                  "WHERE hrdependant_beneficiaries_approval_tran.isvoid = 0 " & _
                     "AND hrdependant_approval_benefit_tran.approvaltranguid =@approvaltranguid "


            objDataOperation.AddParameter("@approvaltranguid", SqlDbType.VarChar, eZeeDataType.DESC_SIZE, tranguid)
            objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 107, "Value"))
            objDataOperation.AddParameter("@Percent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 108, "Percent"))

            dsList = objDataOperation.ExecQuery(strQ, "BenefitList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Benefit_Tran", mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function Get_Membership_Tran(ByVal tranguid As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                   "hrdependant_approval_membership_tran.dpndtmembershiptranunkid " & _
                   ",hrdependant_approval_membership_tran.dpndtbeneficetranunkid " & _
                   ",hrdependant_approval_membership_tran.membership_categoryunkid " & _
                   ",hrdependant_approval_membership_tran.membershipunkid " & _
                   ",hrdependant_approval_membership_tran.membershipno " & _
                      ",CASE " & _
                          "WHEN hrdependant_approval_membership_tran.audittype = 3 THEN 1 " & _
                          "ELSE 0 " & _
                     "END AS Isvoid " & _
                   ",hrdependant_beneficiaries_approval_tran.audituserunkid AS voiduserunkid " & _
                   ",hrdependant_beneficiaries_approval_tran.auditdatetime AS voiddatetime " & _
                   ",hrdependant_approval_membership_tran.voidreason " & _
                   ",cfcommon_master.name AS Category " & _
                   ",hrmembership_master.membershipname " & _
                   ",CASE " & _
                          "WHEN hrdependant_approval_membership_tran.audittype = 1 THEN 'A' " & _
                          "WHEN hrdependant_approval_membership_tran.audittype = 2 THEN 'U' " & _
                          "WHEN hrdependant_approval_membership_tran.audittype = 3 THEN 'D' " & _
                          "ELSE '' " & _
                     "END AS AUD " & _
                   "FROM hrdependant_approval_membership_tran " & _
                   "LEFT JOIN cfcommon_master " & _
                         "ON cfcommon_master.masterunkid = membership_categoryunkid AND cfcommon_master.mastertype = 16 " & _
                   "LEFT JOIN hrdependant_beneficiaries_approval_tran " & _
                     "ON hrdependant_beneficiaries_approval_tran.tranguid = hrdependant_approval_membership_tran.approvaltranguid " & _
                   "LEFT JOIN hrmembership_master " & _
                     "ON hrmembership_master.membershipunkid = hrdependant_approval_membership_tran.membershipunkid " & _
                   "WHERE hrdependant_approval_membership_tran.approvaltranguid = @approvaltranguid "

            objDataOperation.AddParameter("@approvaltranguid", SqlDbType.VarChar, eZeeDataType.DESC_SIZE, tranguid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "MembershipList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Dependant_Tran", mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal dtMemTran As DataTable = Nothing, Optional ByVal dtBenfTran As DataTable = Nothing, Optional ByVal dtDependantAttachment As DataTable = Nothing, Optional ByVal isVoidAll As Boolean = False, Optional ByVal isfromApproval As Boolean = False, Optional ByVal blnGetLatestStatus As Boolean = True) As Boolean
        'Sohail (18 May 2019) - [blnGetLatestStatus]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            'If mintLinkedMasterId <= 0 Then
            '    Dim iCount As Integer = 1
            '    While iCount > 0
            '        strQ = "SELECT (ISNULL(MAX(linkedmasterid),0) + 1) AS ival FROM hrdependant_beneficiaries_approval_tran "
            '        dsList = objDataOperation.ExecQuery(strQ, "List")
            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '        mintLinkedMasterId = CInt(dsList.Tables("List").Rows(0)("ival"))
            '        strQ = "SELECT 1 FROM hrdependant_beneficiaries_approval_tran WHERE linkedmasterid = '" & mintLinkedMasterId & "' "
            '        iCount = objDataOperation.RecordCount(strQ)
            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '        If iCount <= 0 Then
            '            Exit While
            '        End If
            '    End While
            'End If

            Dim objDocument As New clsScan_Attach_Documents
            If dtDependantAttachment IsNot Nothing Then
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"
                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString

                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

                Dim dr As DataRow
                For Each drow As DataRow In dtDependantAttachment.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("userunkid") = User._Object._Userunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("userunkid") = mintAudituserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    dr("form_name") = drow("form_name")
                    dr("file_data") = drow("file_data")
                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mblnImgInDb = False Then

                If IsNothing(mstrImagePath) = False AndAlso mstrImagePath.Trim <> "" Then
                    objImages._xDataOperation = objDataOperation
                    objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
                    objImages._Employeeunkid = mintEmployeeunkid
                    objImages._Isapplicant = False
                    objImages._Transactionid = mintDpndtbeneficetranunkid

                    objImages.GetData()

                    If objImages._Imagetranunkid Then
                        mintdeleteattachimageid = objImages._Imagetranunkid
                    End If


                    If objImages._Imagename <> mstrImagePath Then
                        objImages._Imagename = mstrImagePath
                        If objImages.Insert(objDataOperation, False) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        mintnewattachimageid = objImages._Imagetranunkid
                    End If
                Else
                    'objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
                    'objImages._Employeeunkid = mintEmployeeunkid
                    'objImages._Imagename = mstrImagePath
                    'objImages._Isapplicant = False
                    'objImages._Transactionid = mintLinkedMasterId

                    'If objImages.Delete(objDataOperation) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                End If
            Else
                If isfromApproval = False Then
                    If SaveEmpDependantImage(objDataOperation, mintCompanyID, mintAudituserunkid, True) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintnewattachimageid = objDependantImages._Dependantimgunkid

                Else
                    objDependantImages.GetData(mintCompanyID, mintEmployeeunkid, 0, True, objDataOperation, mintnewattachimageid)
                End If
            End If


            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If mintDpndtbeneficetranunkid > 0 AndAlso blnGetLatestStatus = True Then
                Dim objDB As New clsDependants_Beneficiary_tran
                objDB._xDataOp = objDataOperation
                objDB.GetData(mintDpndtbeneficetranunkid, mdtEffective_date)
                mblnIsactive = objDB._Isactive
                mintReasonUnkid = objDB._Reasonunkid
            End If
            'Sohail (18 May 2019) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            End If
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@first_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirst_Name.ToString)
            objDataOperation.AddParameter("@last_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLast_Name.ToString)
            objDataOperation.AddParameter("@middle_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMiddle_Name.ToString)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)

            If mdtBirthdate = Nothing Then
                objDataOperation.AddParameter("@birthdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@birthdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBirthdate)
            End If

            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephone_No.ToString)
            objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile_No.ToString)
            objDataOperation.AddParameter("@post_box", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPost_Box.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintZipcodeunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@nationalityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNationalityunkid.ToString)
            objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentify_No.ToString)
            objDataOperation.AddParameter("@isdependant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdependant)
            objDataOperation.AddParameter("@gender", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGender)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationtypeid.ToString)

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            If objDocument._Newattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, objDocument._Newattachdocumentids.Length, objDocument._Newattachdocumentids)
            Else
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, mstrnewattachdocumnetid.Length, mstrnewattachdocumnetid)
            End If

            If objDocument._Deleteattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, objDocument._Deleteattachdocumentids.Length, objDocument._Deleteattachdocumentids)
            Else
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, mstrdeleteattachdocumnetid.Length, mstrdeleteattachdocumnetid)
            End If


            If mblnImgInDb = False Then
                If mstrImagePath.Trim <> "" Then
                    objDataOperation.AddParameter("@newattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintnewattachimageid)
                    objDataOperation.AddParameter("@deleteattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdeleteattachimageid)
                Else
                    objDataOperation.AddParameter("@newattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintnewattachimageid)
                    objDataOperation.AddParameter("@deleteattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdeleteattachimageid)
                End If
            Else

                If mbytPhoto IsNot Nothing Then
                    mintnewattachimageid = objDependantImages._Dependantimgunkid
                    objDataOperation.AddParameter("@newattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintnewattachimageid)
                    objDataOperation.AddParameter("@deleteattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdeleteattachimageid)
                Else
                    If isfromApproval = False Then
                        mintnewattachimageid = objDependantImages._Dependantimgunkid
                    End If
                    objDataOperation.AddParameter("@newattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintnewattachimageid)
                    objDataOperation.AddParameter("@deleteattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdeleteattachimageid)
                End If
            End If

            'Gajanan [22-Feb-2019] -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If mdtEffective_date <> Nothing Then
                objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_date)
            Else
                objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DateAndTime.Now)
            End If
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonUnkid)
            objDataOperation.AddParameter("@isfromstatus", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromStatus)
            objDataOperation.AddParameter("@deletedpndtbeneficestatustranids", SqlDbType.NVarChar, mstrDeletedpndtbeneficestatustranIDs.Length, mstrDeletedpndtbeneficestatustranIDs)
            'Sohail (18 May 2019) -- End

            strQ = "INSERT INTO hrdependant_beneficiaries_approval_tran ( " & _
              "  tranguid" & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", approvalremark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", dpndtbeneficetranunkid " & _
              ", employeeunkid " & _
              ", first_name " & _
              ", last_name " & _
              ", middle_name " & _
              ", relationunkid " & _
              ", birthdate " & _
              ", address " & _
              ", telephone_no " & _
              ", mobile_no " & _
              ", post_box " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", zipcodeunkid " & _
              ", email " & _
              ", nationalityunkid " & _
              ", identify_no " & _
              ", gender " & _
              ", isdependant " & _
              ", loginemployeeunkid " & _
              ", isvoid " & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              ", isprocessed " & _
              ", operationtypeid" & _
              ", newattachdocumentid " & _
              ", deleteattachdocumentid " & _
              ", newattachimageid " & _
              ", deleteattachimageid " & _
              ", effective_date " & _
              ", isactive " & _
              ", reasonunkid " & _
              ", isfromstatus " & _
              ", deletedpndtbeneficestatustranids " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @approvalremark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @dpndtbeneficetranunkid " & _
              ", @employeeunkid " & _
              ", @first_name " & _
              ", @last_name " & _
              ", @middle_name " & _
              ", @relationunkid " & _
              ", @birthdate " & _
              ", @address " & _
              ", @telephone_no " & _
              ", @mobile_no " & _
              ", @post_box " & _
              ", @countryunkid " & _
              ", @stateunkid " & _
              ", @cityunkid " & _
              ", @zipcodeunkid " & _
              ", @email " & _
              ", @nationalityunkid " & _
              ", @identify_no " & _
              ", @gender " & _
              ", @isdependant " & _
              ", @loginemployeeunkid " & _
              ", @isvoid " & _
              ", GetDate() " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @isweb " & _
              ", @isprocessed " & _
              ", @operationtypeid" & _
              ", @newattachdocumentid " & _
              ", @deleteattachdocumentid " & _
              ", @newattachimageid " & _
              ", @deleteattachimageid " & _
              ", @effective_date " & _
              ", @isactive " & _
              ", @reasonunkid " & _
              ", @isfromstatus " & _
              ", @deletedpndtbeneficestatustranids " & _
            "); SELECT @@identity"
            'Sohail (18 May 2019) - [effective_date, isactive, reasonunkid, isfromstatus, deletedpndtbeneficestatustranids]

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If




            If dtMemTran IsNot Nothing Then
                If dtMemTran.Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    blnFlag = InsertUpdateDelete_DependantMembership_Tran(objDataOperation, mintAudituserunkid, dtMemTran, isVoidAll)

                    If blnFlag = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If dtBenfTran IsNot Nothing Then
                If dtBenfTran.Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    blnFlag = InsertUpdateDelete_DependantBenefit_Tran(objDataOperation, mintAudituserunkid, dtBenfTran, isVoidAll)

                    If blnFlag = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If




            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Public Function InsertAll(ByVal dtTable As DataTable, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal dtMemTran As DataTable = Nothing, Optional ByVal dtBenfTran As DataTable = Nothing, Optional ByVal dtDependantAttachment As DataTable = Nothing, Optional ByVal isVoidAll As Boolean = False, Optional ByVal isfromApproval As Boolean = False, Optional ByVal blnGetLatestStatus As Boolean = True) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                mintDpndtbeneficetranunkid = CInt(dtRow.Item("DpndtTranId"))
                mintEmployeeunkid = CInt(dtRow.Item("EmpId"))
                mstrFirst_Name = dtRow.Item("FName").ToString
                mstrLast_Name = dtRow.Item("LName").ToString
                mstrMiddle_Name = dtRow.Item("middle_name").ToString
                mintRelationunkid = CInt(dtRow.Item("RelationId"))
                If dtRow.Item("birthdate").ToString.Trim <> "" Then
                    mdtBirthdate = eZeeDate.convertDate(dtRow.Item("birthdate").ToString)
                Else
                    mdtBirthdate = Nothing
                End If
                mstrAddress = dtRow.Item("address").ToString
                mstrTelephone_No = dtRow.Item("telephone_no").ToString
                mstrMobile_No = dtRow.Item("mobile_no").ToString
                mstrPost_Box = dtRow.Item("post_box").ToString
                mintCountryunkid = CInt(dtRow.Item("CountryId"))
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mintCityunkid = CInt(dtRow.Item("cityunkid"))
                mintZipcodeunkid = CInt(dtRow.Item("zipcodeunkid"))
                mstrEmail = dtRow.Item("email").ToString
                mintNationalityunkid = CInt(dtRow.Item("nationalityunkid"))
                mstrIdentify_No = dtRow.Item("IdNo").ToString
                mblnIsdependant = CBool(dtRow.Item("IsDependant"))
                mintGender = CInt(dtRow.Item("GenderId"))
                'mstrTranguid = Guid.NewGuid.ToString()

                If Insert(objDataOperation, dtMemTran, dtBenfTran, dtDependantAttachment, isVoidAll, isfromApproval, blnGetLatestStatus) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (18 May 2019) -- End

    Public Function InsertUpdateDelete_DependantMembership_Tran(ByVal xDataOpr As clsDataOperation, Optional ByVal intUserUnkid As Integer = 0, Optional ByVal mdtTran As DataTable = Nothing, Optional ByVal isVoidAll As Boolean = False) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            Dim audittype As Integer = 0

            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    audittype = 0
                    'Gajanan [17-April-2019] -- End

                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                audittype = 1
                            Case "U"
                                audittype = 2
                            Case "D"
                                audittype = 3
                        End Select

                        If audittype > 0 Or isVoidAll = True Then
                            strQ = "INSERT INTO hrdependant_approval_membership_tran ( " & _
                                        "  dpndtmembershiptranunkid" & _
                                        ", dpndtbeneficetranunkid " & _
                                        ", membership_categoryunkid " & _
                                        ", membershipunkid " & _
                                        ", membershipno " & _
                                        ", isvoid " & _
                                        ", voidreason" & _
                                        ", audittype" & _
                                        ", approvaltranguid " & _
                                      ") VALUES (" & _
                                        "  @dpndtmembershiptranunkid " & _
                                        ", @dpndtbeneficetranunkid " & _
                                        ", @membership_categoryunkid " & _
                                        ", @membershipunkid " & _
                                        ", @membershipno " & _
                                        ", @isvoid " & _
                                        ", @voidreason" & _
                                        ", @audittype" & _
                                        ", @approvaltranguid " & _
                                      "); SELECT @@identity"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@dpndtmembershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtmembershiptranunkid"))
                            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid)
                            objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membership_categoryunkid").ToString)
                            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershipunkid").ToString)
                            objDataOperation.AddParameter("@membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("membershipno").ToString)
                            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))

                            If isVoidAll = True Then
                                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 3)
                            Else
                                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, audittype)
                            End If

                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                            objDataOperation.AddParameter("@approvaltranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid)

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If
                    End If

                End With
            Next

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True


        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_DependantMembership_Tran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function InsertUpdateDelete_DependantBenefit_Tran(ByVal xDataOpr As clsDataOperation, Optional ByVal intUserUnkid As Integer = 0, Optional ByVal mdtTran As DataTable = Nothing, Optional ByVal isVoidAll As Boolean = False) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            Dim audittype As Integer = 0

            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    audittype = 0
                    'Gajanan [17-April-2019] -- End
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                audittype = 1
                            Case "U"
                                audittype = 2
                            Case "D"
                                audittype = 3
                        End Select

                        If audittype > 0 Or isVoidAll = True Then

                            strQ = "INSERT INTO hrdependant_approval_benefit_tran ( " & _
                                        "  dpndtbenefittranunkid " & _
                                        ", dpndtbeneficetranunkid " & _
                                        ", benefitgroupunkid " & _
                                        ", benefitplanunkid " & _
                                        ", value_id " & _
                                        ", benefit_percent " & _
                                        ", benefit_amount " & _
                                        ", isvoid " & _
                                        ", voidreason" & _
                                        ", audittype" & _
                                        ", approvaltranguid " & _
                                      ") VALUES (" & _
                                        "  @dpndtbenefittranunkid " & _
                                        ", @dpndtbeneficetranunkid " & _
                                        ", @benefitgroupunkid " & _
                                        ", @benefitplanunkid " & _
                                        ", @value_id " & _
                                        ", @benefit_percent " & _
                                        ", @benefit_amount " & _
                                        ", 0 " & _
                                        ", @voidreason" & _
                                        ", @audittype" & _
                                        ", @approvaltranguid " & _
                                      "); SELECT @@identity"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@dpndtbenefittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtbenefittranunkid"))
                            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid)
                            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("benefitgroupunkid").ToString)
                            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("benefitplanunkid").ToString)
                            objDataOperation.AddParameter("@value_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("value_id").ToString)
                            objDataOperation.AddParameter("@benefit_percent", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("benefit_percent").ToString)
                            objDataOperation.AddParameter("@benefit_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("benefit_amount").ToString)
                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                            If isVoidAll = True Then
                                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 3)
                            Else
                                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, audittype)
                            End If
                            objDataOperation.AddParameter("@approvaltranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid)

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If
                    End If
                End With
            Next


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_DependantBenefit_Tran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    'Gajanan [27-May-2019] -- Start              
    'Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intDependantTranUnkid As Integer, ByVal intDependantUnkid As Integer, ByVal strColName As String) As Boolean
    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intDependantTranUnkid As Integer, ByVal intDependantUnkid As Integer, ByVal strColName As String, ByVal Firstname As String, ByVal Lastname As String) As Boolean
        'Gajanan [27-May-2019] -- End

        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hrdependant_beneficiaries_approval_tran SET " & _
                   " " & strColName & " = '" & intDependantUnkid & "' "


            'Gajanan [27-May-2019] -- Start              
            '"WHERE dpndtbeneficetranunkid = '" & intDependantTranUnkid & "' AND employeeunkid = '" & intEmployeeId & "' and first_name='" & Firstname & "' "
            strQ &= "WHERE isprocessed= 0 and dpndtbeneficetranunkid = @dpndtbeneficetranunkid AND employeeunkid = @employeeunkid  and first_name=@first_name and last_name=@last_name "
            'Gajanan [27-May-2019] -- End


            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependantTranUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@first_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Firstname)
            objDataOperation.AddParameter("@last_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Lastname)
            'Gajanan [31-May-2020] -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateDeleteDocument(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intlinkedUnkid As String, _
                                    ByVal strFormname As String, ByVal intDependantTranUnkid As Integer, ByVal strColName As String, ByVal blnisDelete As Boolean) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            If intlinkedUnkid.Length > 0 Then
                strQ = "UPDATE hrdocuments_tran SET " & _
                   " " & strColName & " = '" & intDependantTranUnkid & "'"

                If blnisDelete Then
                    strQ &= ", isactive = 0 "
                Else
                    strQ &= ", isactive = 1 "
                End If

                strQ &= "WHERE employeeunkid = '" & intEmployeeId & "' AND scanattachtranunkid in ( " & intlinkedUnkid & " )"

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: UpdateDeleteDocument; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateDeleteImage(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intTransactionid As Integer, _
                                    ByVal intDependantimgunkid As Integer, ByVal blnisDelete As Boolean) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            If mblnImgInDb = False Then
                objImages._xDataOperation = objDataOperation
                objImages._Transactionid = intDependantimgunkid
                objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
                objImages._Employeeunkid = intEmployeeId

                If intTransactionid > 0 Then
                    objImages._Imagetranunkid = intTransactionid
                End If

                If blnisDelete = False Then
                    objImages.Update()
                Else
                    objImages.Delete(objDataOperation, False)
                End If
            Else
                objDependantImages._Dependantunkid = intDependantimgunkid
                objDependantImages._Employeeunkid = intEmployeeId
                objDependantImages._Dependantimgunkid = intTransactionid

                If blnisDelete = False Then

                    objDependantImages._Isvoid = False

                    objDependantImages.Update(objDataOperation)
                Else
                    objDependantImages._Isvoid = True
                    objDependantImages.Delete(objDataOperation, intTransactionid)
                End If

            End If



            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: UpdateDeleteImage; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Public Function UpdateDeleteDPStatus(ByVal xDataOpr As clsDataOperation, ByVal intlinkedUnkid As String, _
                                    ByVal intDependantTranUnkid As Integer, ByVal strColName As String, ByVal blnisDelete As Boolean) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            If intlinkedUnkid.Length > 0 Then
                strQ = "UPDATE hrdependant_beneficiaries_status_tran SET " & _
                   " " & strColName & " = '" & intDependantTranUnkid & "'"

                If blnisDelete Then
                    strQ &= ", isvoid = 1 " & _
                            ", voiduserunkid = " & mintAudituserunkid & " " & _
                            ", voiddatetime = GETDATE() " & _
                            ", voidreason = 'Wrong Posted' "
                Else
                    strQ &= ", isvoid = 0 " & _
                            ", voiduserunkid = 0 " & _
                            ", voiddatetime = NULL " & _
                            ", voidreason = '' "
                End If

                strQ &= "WHERE dpndtbeneficestatustranunkid in ( " & intlinkedUnkid & " )"

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: UpdateDeleteDPStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (18 May 2019) -- End

    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  dpndtbeneficetranunkid " & _
                      ", employeeunkid " & _
                      ", first_name " & _
                      ", last_name " & _
                      ", middle_name " & _
                      ", relationunkid " & _
                      ", birthdate " & _
                      ", address " & _
                      ", telephone_no " & _
                      ", mobile_no " & _
                      ", post_box " & _
                      ", countryunkid " & _
                      ", stateunkid " & _
                      ", cityunkid " & _
                      ", zipcodeunkid " & _
                      ", email " & _
                      ", nationalityunkid " & _
                      ", identify_no " & _
                      ", isdependant " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", gender " & _
                      ", psoft_syncdatetime " & _
                      ",ISNULL(hrdependant_beneficiaries_approval_tran.effective_date, GETDATE()) AS effective_date " & _
                      ",ISNULL(hrdependant_beneficiaries_approval_tran.isactive, 1) AS isactive " & _
                      ",ISNULL(hrdependant_beneficiaries_approval_tran.reasonunkid, 0) AS reasonunkid " & _
                      ",ISNULL(hrdependant_beneficiaries_approval_tran.isfromstatus, 0) AS isfromstatus " & _
                      ",ISNULL(hrdependant_beneficiaries_approval_tran.deletedpndtbeneficestatustranids, '') AS deletedpndtbeneficestatustranids " & _
                     "FROM hrdependant_beneficiaries_approval_tran " & _
                     "WHERE tranguid = @tranguid "
            'Sohail (18 May 2019) - [effective_date, isactive, reasonunkid, isfromstatus, deletedpndtbeneficestatustranids]

            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrTranguid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
            objImages._Transactionid = mintDpndtbeneficetranunkid
            objImages._Employeeunkid = mintEmployeeunkid
            mstrImagePath = objImages._Imagename

            If mblnImgInDb Then
                Dim objDependantImg As New clsdependant_Images
                objDependantImg.GetData(mintCompanyID, mintEmployeeunkid, mintDpndtbeneficetranunkid, True, objDataOperation)
                mbytPhoto = objDependantImg._Photo
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.



    'Private Function SaveEmpDependantImage(ByVal objDataOperation As clsDataOperation, ByVal mintCompanyId As Integer, ByVal intUserId As Integer) As Boolean
    Private Function SaveEmpDependantImage(ByVal objDataOperation As clsDataOperation, ByVal mintCompanyId As Integer, ByVal intUserId As Integer, Optional ByVal Isfromapprover As Boolean = False) As Boolean
        'Gajanan [22-Feb-2019] -- End
        Dim exForce As Exception
        Try

            objDependantImages._Companyunkid = mintCompanyId
            objDependantImages._Employeeunkid = mintEmployeeunkid
            objDependantImages._Dependantunkid = mintDpndtbeneficetranunkid

            If intUserId <= 0 Then
                intUserId = User._Object._Userunkid
            End If

            objDependantImages.GetData(objDependantImages._Companyunkid, objDependantImages._Employeeunkid, objDependantImages._Dependantunkid)
            objDependantImages._Userunkid = intUserId

            objDependantImages._FormName = mstrForm_Name
            objDependantImages._ClientIP = mstrIp
            objDependantImages._HostName = mstrHost




            If mbytPhoto IsNot Nothing Then
                Dim blnIsInserted As Boolean = False

                If objDependantImages._Photo IsNot Nothing Then

                    If objDependantImages._Photo.Length = mbytPhoto.Length Then

                        Dim Hash1() As Byte = New MD5CryptoServiceProvider().ComputeHash(objDependantImages._Photo)
                        Dim Hash2() As Byte = New MD5CryptoServiceProvider().ComputeHash(mbytPhoto)

                        For i As Int32 = 0 To Hash2.Length - 1
                            If Hash1(i) = Hash2(i) Then
                                blnIsInserted = True
                                Exit For
                            End If
                        Next

                    End If

                End If

                If blnIsInserted = False Then
                    objDependantImages._Photo = mbytPhoto
                    If objDependantImages.Insert(objDataOperation, Isfromapprover) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Else


                If Isfromapprover = False Then


                    If objDependantImages.Delete(objDataOperation, objDependantImages._Dependantimgunkid) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveEmpDependantImage", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function Delete(ByVal intUnkid As Integer, ByVal xVoidReason As String, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            Dim objDependants_Beneficiary_tran As New clsDependants_Beneficiary_tran
            Dim objDependants_Benefit_tran As New clsDependants_Benefit_tran
            Dim objDependants_Membership_tran As New clsDependants_Membership_tran



            objDependants_Beneficiary_tran._Dpndtbeneficetranunkid = intUnkid

            objDependants_Benefit_tran._DependantTranUnkid = intUnkid
            objDependants_Membership_tran._DependantTranUnkid = intUnkid

            Me._Audittype = enAuditType.DELETE
            Me._Audituserunkid = User._Object._Userunkid

            Me._Address = objDependants_Beneficiary_tran._Address

            If ConfigParameter._Object._IsDependant_AgeLimit_Set = True Then
                Me._Birthdate = objDependants_Beneficiary_tran._Birthdate
            Else
                Me._Birthdate = objDependants_Beneficiary_tran._Birthdate
            End If

            Me._Cityunkid = objDependants_Beneficiary_tran._Cityunkid
            Me._Countryunkid = objDependants_Beneficiary_tran._Countryunkid
            Me._Email = objDependants_Beneficiary_tran._Email
            Me._Employeeunkid = objDependants_Beneficiary_tran._Employeeunkid
            Me._First_Name = objDependants_Beneficiary_tran._First_Name
            Me._Identify_No = objDependants_Beneficiary_tran._Identify_No
            Me._Isdependant = objDependants_Beneficiary_tran._Isdependant

            Me._Last_Name = objDependants_Beneficiary_tran._Last_Name
            Me._Middle_Name = objDependants_Beneficiary_tran._Middle_Name
            Me._Mobile_No = objDependants_Beneficiary_tran._Mobile_No
            Me._Nationalityunkid = objDependants_Beneficiary_tran._Nationalityunkid
            Me._Post_Box = objDependants_Beneficiary_tran._Post_Box
            Me._Relationunkid = objDependants_Beneficiary_tran._Relationunkid
            Me._Stateunkid = objDependants_Beneficiary_tran._Stateunkid
            Me._Telephone_No = objDependants_Beneficiary_tran._Telephone_No
            Me._Zipcodeunkid = objDependants_Beneficiary_tran._Zipcodeunkid
            Me._Gender = objDependants_Beneficiary_tran._Gender
            Me._CompanyId = ConfigParameter._Object._Companyunkid
            Me._ImagePath = objDependants_Beneficiary_tran._ImagePath

            Me._Dpndtbeneficetranunkid = intUnkid
            Me._Isprocessed = False
            Me._Isvoid = False
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Approvalremark = ""
            Me._Mappingunkid = 0
            Me._Isfinal = False
            Me._Operationtypeid = clsEmployeeDataApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objDocument As New clsScan_Attach_Documents
            Me._Deletedattachdocumnetid = objDocument.GetCSVAttachmentids(objDependants_Beneficiary_tran._Employeeunkid, CInt(enScanAttactRefId.DEPENDANTS), intUnkid, objDataOperation)

            Dim objDependantMemberTran As New clsDependants_Membership_tran
            Dim objDependantBenefitTran As New clsDependants_Benefit_tran

            objDependantMemberTran._DependantTranUnkid = intUnkid
            objDependantBenefitTran._DependantTranUnkid = intUnkid


            'Gajanan [22-Feb-2019] -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            Me._Effective_date = objDependants_Beneficiary_tran._Effective_date
            Me._Isactive = objDependants_Beneficiary_tran._Isactive
            Me._ReasonUnkid = objDependants_Beneficiary_tran._Reasonunkid
            Me._Form_Name = mstrForm_Name
            Me._Host = mstrHost
            Me._Ip = mstrIp
            Me._Isweb = mblnIsweb
            Me._Loginemployeeunkid = mintLoginemployeeunkid
            Me._Auditdatetime = Now
            'Sohail (18 May 2019) -- End

            blnFlag = Insert(objDataOperation, objDependantMemberTran._DataTable, objDependantBenefitTran._DataTable, Nothing, True)

            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then

                objDependants_Beneficiary_tran._Voidreason = xVoidReason
                objDependants_Beneficiary_tran._Voiduserunkid = Me._Audituserunkid
                objDependants_Beneficiary_tran._Isvoid = True
                objDependants_Beneficiary_tran._Voiddatetime = Now
                objDependants_Beneficiary_tran._ClientIP = Me._Ip
                objDependants_Beneficiary_tran._FormName = Me._Form_Name
                objDependants_Beneficiary_tran._HostName = Me._Host

                'Gajanan [9-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                objDependants_Beneficiary_tran._AuditUserId = Me._Audituserunkid
                'Gajanan [9-July-2019] -- End

                objDependants_Beneficiary_tran._DeletedpndtbeneficestatustranIDs = Me._DeletedpndtbeneficestatustranIDs 'Sohail (18 May 2019)

                If objDependants_Beneficiary_tran.Delete(intUnkid, mintEmployeeunkid, objDataOperation) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Public Function DeleteStatus(ByVal intUnkid As Integer, ByVal xVoidReason As String, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal isfromApproval As Boolean = False, Optional ByVal blnGetLatestStatus As Boolean = True) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            End If
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@first_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirst_Name.ToString)
            objDataOperation.AddParameter("@last_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLast_Name.ToString)
            objDataOperation.AddParameter("@middle_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMiddle_Name.ToString)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)

            If mdtBirthdate = Nothing Then
                objDataOperation.AddParameter("@birthdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@birthdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBirthdate)
            End If

            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephone_No.ToString)
            objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile_No.ToString)
            objDataOperation.AddParameter("@post_box", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPost_Box.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintZipcodeunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@nationalityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNationalityunkid.ToString)
            objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentify_No.ToString)
            objDataOperation.AddParameter("@isdependant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdependant)
            objDataOperation.AddParameter("@gender", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGender)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationtypeid.ToString)

            objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, mstrnewattachdocumnetid.Length, mstrnewattachdocumnetid)
            objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, mstrdeleteattachdocumnetid.Length, mstrdeleteattachdocumnetid)


            If mblnImgInDb = False Then
                If mstrImagePath.Trim <> "" Then
                    objDataOperation.AddParameter("@newattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintnewattachimageid)
                    objDataOperation.AddParameter("@deleteattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdeleteattachimageid)
                Else
                    objDataOperation.AddParameter("@newattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintnewattachimageid)
                    objDataOperation.AddParameter("@deleteattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdeleteattachimageid)
                End If
            Else

                If mbytPhoto IsNot Nothing Then
                    mintnewattachimageid = objDependantImages._Dependantimgunkid
                    objDataOperation.AddParameter("@newattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintnewattachimageid)
                    objDataOperation.AddParameter("@deleteattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdeleteattachimageid)
                Else
                    If isfromApproval = False Then
                        mintnewattachimageid = objDependantImages._Dependantimgunkid
                    End If
                    objDataOperation.AddParameter("@newattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintnewattachimageid)
                    objDataOperation.AddParameter("@deleteattachimageid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdeleteattachimageid)
                End If
            End If

            If mdtEffective_date <> Nothing Then
                objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_date)
            Else
                objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DateAndTime.Now)
            End If
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonUnkid)
            objDataOperation.AddParameter("@isfromstatus", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromStatus)
            objDataOperation.AddParameter("@deletedpndtbeneficestatustranids", SqlDbType.NVarChar, mstrDeletedpndtbeneficestatustranIDs.Length, mstrDeletedpndtbeneficestatustranIDs)

            strQ = "INSERT INTO hrdependant_beneficiaries_approval_tran ( " & _
              "  tranguid" & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", approvalremark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", dpndtbeneficetranunkid " & _
              ", employeeunkid " & _
              ", first_name " & _
              ", last_name " & _
              ", middle_name " & _
              ", relationunkid " & _
              ", birthdate " & _
              ", address " & _
              ", telephone_no " & _
              ", mobile_no " & _
              ", post_box " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", zipcodeunkid " & _
              ", email " & _
              ", nationalityunkid " & _
              ", identify_no " & _
              ", gender " & _
              ", isdependant " & _
              ", loginemployeeunkid " & _
              ", isvoid " & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              ", isprocessed " & _
              ", operationtypeid" & _
              ", newattachdocumentid " & _
              ", deleteattachdocumentid " & _
              ", newattachimageid " & _
              ", deleteattachimageid " & _
              ", effective_date " & _
              ", isactive " & _
              ", reasonunkid " & _
              ", isfromstatus " & _
              ", deletedpndtbeneficestatustranids " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @approvalremark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @dpndtbeneficetranunkid " & _
              ", @employeeunkid " & _
              ", @first_name " & _
              ", @last_name " & _
              ", @middle_name " & _
              ", @relationunkid " & _
              ", @birthdate " & _
              ", @address " & _
              ", @telephone_no " & _
              ", @mobile_no " & _
              ", @post_box " & _
              ", @countryunkid " & _
              ", @stateunkid " & _
              ", @cityunkid " & _
              ", @zipcodeunkid " & _
              ", @email " & _
              ", @nationalityunkid " & _
              ", @identify_no " & _
              ", @gender " & _
              ", @isdependant " & _
              ", @loginemployeeunkid " & _
              ", @isvoid " & _
              ", GetDate() " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @isweb " & _
              ", @isprocessed " & _
              ", @operationtypeid" & _
              ", @newattachdocumentid " & _
              ", @deleteattachdocumentid " & _
              ", @newattachimageid " & _
              ", @deleteattachimageid " & _
              ", @effective_date " & _
              ", @isactive " & _
              ", @reasonunkid " & _
              ", @isfromstatus " & _
              ", @deletedpndtbeneficestatustranids " & _
            "); SELECT @@identity"

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'If dtMemTran IsNot Nothing Then
            '    If dtMemTran.Rows.Count > 0 Then
            '        Dim blnFlag As Boolean = False
            '        blnFlag = InsertUpdateDelete_DependantMembership_Tran(objDataOperation, mintAudituserunkid, dtMemTran, isVoidAll)

            '        If blnFlag = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If

            'If dtBenfTran IsNot Nothing Then
            '    If dtBenfTran.Rows.Count > 0 Then
            '        Dim blnFlag As Boolean = False
            '        blnFlag = InsertUpdateDelete_DependantBenefit_Tran(objDataOperation, mintAudituserunkid, dtBenfTran, isVoidAll)

            '        If blnFlag = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If


            Dim objDPStatus As New clsDependant_Benefice_Status_tran
            objDPStatus._Dpndtbeneficestatustranunkid = intUnkid

            objDPStatus._Voidreason = xVoidReason
            objDPStatus._Voiduserunkid = Me._Audituserunkid
            objDPStatus._Isvoid = True
            objDPStatus._Voiddatetime = Now
            objDPStatus._ClientIP = Me._Ip
            objDPStatus._FormName = Me._Form_Name
            objDPStatus._HostName = Me._Host
            objDPStatus._xDataOp = objDataOperation

            If objDPStatus.Delete() = False Then
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (18 May 2019) -- End

    Public Function isExist(ByVal strName As String, _
                            ByVal strLastName As String, _
                            ByVal strMiddleName As String, _
                            ByVal strIdNo As String, _
                            ByVal intEmployeeId As Integer, _
                            Optional ByVal strtrangUid As String = "", _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = True, _
                            Optional ByRef intOperationTypeId As Integer = 0) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                    " tranguid " & _
                    ",transactiondate " & _
                    ",mappingunkid " & _
                    ",approvalremark " & _
                    ",isfinal " & _
                    ",statusunkid " & _
                    ",dpndtbeneficetranunkid " & _
                    ", employeeunkid " & _
                    ", first_name " & _
                    ", last_name " & _
                    ", middle_name " & _
                    ", relationunkid " & _
                    ", birthdate " & _
                    ", address " & _
                    ", telephone_no " & _
                    ", mobile_no " & _
                    ", post_box " & _
                    ", countryunkid " & _
                    ", stateunkid " & _
                    ", cityunkid " & _
                    ", zipcodeunkid " & _
                    ", email " & _
                    ", nationalityunkid " & _
                    ", identify_no " & _
                    ", isdependant " & _
                    ",operationtypeid " & _
                    "FROM hrdependant_beneficiaries_approval_tran " & _
                    "WHERE first_name = @first_name " & _
                    "AND last_name = @last_name " & _
                    "AND employeeunkid = @EmpId " & _
                    "AND isvoid = 0 "

            If strtrangUid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid"
                objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strtrangUid)
            End If

            If strMiddleName.Trim.Length > 0 Then
                strQ &= "AND middle_name = @middle_name "
                objDataOperation.AddParameter("@middle_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strMiddleName)

            End If

            If strIdNo.Trim.Length > 0 Then
                strQ &= "AND identify_no = @identify_no "
                objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIdNo)
            End If


            'Gajanan [10-June-2019] -- Start      


            'If blnPreventNewEntry Then
            '    'Gajanan [17-April-2019] -- Start
            '    'Enhancement - Implementing Employee Approver Flow On Employee Data.
            '    'strQ &= " AND ISNULL(hremployee_referee_approval_tran.isprocessed,0)= 0 "
            '    strQ &= " AND ISNULL(hrdependant_beneficiaries_approval_tran.isprocessed,0)= 0 "
            '    'Gajanan [17-April-2019] -- End
            'End If
                strQ &= " AND ISNULL(hrdependant_beneficiaries_approval_tran.isprocessed,0)= 0 "
            'Gajanan [10-June-2019] -- End

            objDataOperation.AddParameter("@first_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@last_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLastName)
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intOperationTypeId = CInt(dsList.Tables(0).Rows(0)("operationtypeid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class
