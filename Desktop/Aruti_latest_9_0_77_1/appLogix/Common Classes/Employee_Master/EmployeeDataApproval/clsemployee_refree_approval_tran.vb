﻿Imports eZeeCommonLib

Public Class clsemployee_refree_approval_tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_refree_approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrApprovalremark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintRefereetranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrName As String = String.Empty
    Private mstrAddress As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mstrEmail As String = String.Empty
    Private mstrGender As Integer = 0
    Private mstrIdentify_No As String = String.Empty
    Private mstrTelephone_No As String
    Private mstrMobile_No As String
    Private mstrRef_Position As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mdtAuditdatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer = -1
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsprocess As Boolean
    Private mintRelationunkid As Integer = 0
    Private mintLoginEmployeeUnkid As Integer = 0
    Private mintOperationTypeId As Integer = 0
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalremark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set refereetranunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Refereetranunkid() As Integer
        Get
            Return mintRefereetranunkid
        End Get
        Set(ByVal value As Integer)
            mintRefereetranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Address() As String
        Get
            Return mstrAddress
        End Get
        Set(ByVal value As String)
            mstrAddress = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gender
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Gender() As Integer
        Get
            Return mstrGender
        End Get
        Set(ByVal value As Integer)
            mstrGender = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set identify_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Company() As String
        Get
            Return mstrIdentify_No
        End Get
        Set(ByVal value As String)
            mstrIdentify_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set telephone_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Telephone_No() As String
        Get
            Return mstrTelephone_No
        End Get
        Set(ByVal value As String)
            mstrTelephone_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mobile_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mobile_No() As String
        Get
            Return mstrMobile_No
        End Get
        Set(ByVal value As String)
            mstrMobile_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ref_position
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ref_Position() As String
        Get
            Return mstrRef_Position
        End Get
        Set(ByVal value As String)
            mstrRef_Position = Value
        End Set
    End Property

    Public Property _Relationunkid() As Integer
        Get
            Return mintRelationunkid
        End Get
        Set(ByVal value As Integer)
            mintRelationunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocess
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isprocess() As Boolean
        Get
            Return mblnIsprocess
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocess = Value
        End Set
    End Property


    Public Property _LoginEmployeeUnkid() As Integer
        Get
            Return mintLoginEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeUnkid = value
        End Set
    End Property

    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property

#End Region

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal IsForImport As Boolean = False, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet

    
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, "", mblnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       "    ,ISNULL(hremployee_referee_approval_tran.name,'') AS RefName " & _
                       "    ,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                       "    ,ISNULL(hremployee_referee_approval_tran.identify_no,'') AS Company " & _
                       "    ,ISNULL(hremployee_referee_approval_tran.email,'') AS Email " & _
                       "    ,hremployee_master.employeeunkid AS EmpId " & _
                       "    ,hrmsConfiguration..cfcountry_master.countryunkid AS CountryId " & _
                       "    ,hremployee_referee_approval_tran.refereetranunkid AS RefreeTranId " & _
                       "    ,hremployee_referee_approval_tran.address " & _
                       "    ,hremployee_referee_approval_tran.stateunkid " & _
                       "    ,hremployee_referee_approval_tran.cityunkid " & _
                       "    ,CASE WHEN hremployee_referee_approval_tran.gender = 1 THEN @MALE WHEN hremployee_referee_approval_tran.gender = 2 THEN @FEMALE END AS gender " & _
                       "    ,hremployee_referee_approval_tran.telephone_no " & _
                       "    ,hremployee_referee_approval_tran.mobile_no " & _
                       "    ,-1 as userunkid " & _
                       "    ,-1 as isvoid " & _
                       "    ,-1 as voiduserunkid " & _
                       "    ,'' as voidreason " & _
                       "    ,ISNULL(hremployee_referee_approval_tran.relationunkid,0) As relationunkid " & _
                       "    ,-1 as ref_position " & _
                       "    ,-1 as loginemployeeunkid " & _
                       "    ,-1 as voidloginemployeeunkid " & _
                       "    ,hremployee_referee_approval_tran.tranguid As tranguid " & _
                       "    ,ISNULL(hremployee_referee_approval_tran.operationtypeid,0) AS operationtypeid " & _
                       "    ,CASE WHEN ISNULL(hremployee_referee_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                       "          WHEN ISNULL(hremployee_referee_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                       "          WHEN ISNULL(hremployee_referee_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                       "     END AS OperationType "

                If IsForImport Then
                    StrQ &= "   ,hrmsConfiguration..cfstate_master.name as state " & _
                            "   ,hrmsConfiguration..cfcity_master.name as city " & _
                            "   ,cfcommon_master.name as relation "
                End If

                StrQ &= " FROM hremployee_referee_approval_tran "

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'StrQ &= "   LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_approval_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                '        "   LEFT JOIN hremployee_master ON hremployee_referee_approval_tran.employeeunkid = hremployee_master.employeeunkid "

                StrQ &= "   LEFT JOIN hremployee_referee_tran ON hremployee_referee_approval_tran.refereetranunkid = hremployee_referee_tran.refereetranunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_approval_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                        "   LEFT JOIN hremployee_master ON hremployee_referee_approval_tran.employeeunkid = hremployee_master.employeeunkid "
                'Gajanan [17-April-2019] -- End

                If IsForImport Then
                    StrQ &= "   LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = hremployee_referee_approval_tran.stateunkid " & _
                            "   LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = hremployee_referee_approval_tran.cityunkid  " & _
                            "   LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hremployee_referee_approval_tran.relationunkid "
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then

                        'Gajanan [27-May-2019] -- Start              
                        'StrQ &= xUACQry
                        StrQ &= "LEFT " & xUACQry
                        'Gajanan [27-Ma y-2019] -- End
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE ISNULL(hremployee_referee_approval_tran.isvoid,0) = 0 AND ISNULL(hremployee_referee_approval_tran.isprocessed,0) = 0 "


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                StrQ &= "AND ISNULL(hremployee_referee_approval_tran.statusunkid,0) = 1 "
                'Gajanan [17-April-2019] -- End



                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If

                objDo.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDo.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                objDo.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information"))
                objDo.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 63, "Information Edited"))
                objDo.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 64, "Information Deleted"))

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function Insert(ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnFromApproval As Boolean = False) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        If blnFromApproval = False Then
            If isExist(mintEmployeeunkid, mstrName, mintOperationTypeId, mintRefereetranunkid, True, objDataOperation) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Selected refree is already assigned to the employee. Please assign new refree.")
                Return False
            End If
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            End If
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@refereetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefereetranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGender.ToString)
            objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentify_No.ToString)
            objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.PHONE_SIZE, mstrTelephone_No.ToString)
            objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.PHONE_SIZE, mstrMobile_No.ToString)
            objDataOperation.AddParameter("@ref_position", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRef_Position.ToString)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocess.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeUnkid)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)

            strQ = "INSERT INTO hremployee_referee_approval_tran ( " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", approvalremark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", refereetranunkid " & _
              ", employeeunkid " & _
              ", name " & _
              ", address " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", email " & _
              ", gender " & _
              ", identify_no " & _
              ", telephone_no " & _
              ", mobile_no " & _
              ", relationunkid " & _
              ", ref_position " & _
              ", isvoid " & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              ", isprocessed" & _
              ", loginemployeeunkid " & _
              ", operationtypeid " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @approvalremark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @refereetranunkid " & _
              ", @employeeunkid " & _
              ", @name " & _
              ", @address " & _
              ", @countryunkid " & _
              ", @stateunkid " & _
              ", @cityunkid " & _
              ", @email " & _
              ", @gender " & _
              ", @identify_no " & _
              ", @telephone_no " & _
              ", @mobile_no " & _
              ", @relationunkid " & _
              ", @ref_position " & _
              ", @isvoid " & _
              ", GETDATE() " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @isweb " & _
              ", @isprocessed" & _
              ", @loginemployeeunkid " & _
              ", @operationtypeid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'mstrTranguid = dsList.Tables(0).Rows(0).Item(0)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUnkid As Integer, ByVal xVoidReason As String, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            Dim objEmployee_Refree_tran As clsEmployee_Refree_tran
            objEmployee_Refree_tran = New clsEmployee_Refree_tran

            objEmployee_Refree_tran._Refereetranunkid() = intUnkid
            Me._Audittype = enAuditType.ADD
            Me._Address = objEmployee_Refree_tran._Address
            Me._Cityunkid = objEmployee_Refree_tran._Cityunkid
            Me._Countryunkid = objEmployee_Refree_tran._Countryunkid
            Me._Email = objEmployee_Refree_tran._Email
            Me._Employeeunkid = objEmployee_Refree_tran._Employeeunkid
            Me._Gender = objEmployee_Refree_tran._Gender
            Me._Company = objEmployee_Refree_tran._Company
            Me._Name = objEmployee_Refree_tran._Name
            Me._Stateunkid = objEmployee_Refree_tran._Stateunkid
            Me._Mobile_No = objEmployee_Refree_tran._Mobile_No
            Me._Telephone_No = objEmployee_Refree_tran._Telephone_No
            Me._Relationunkid = objEmployee_Refree_tran._Relationunkid
            Me._Ref_Position = objEmployee_Refree_tran._Ref_Position

            Me._Refereetranunkid = intUnkid
            Me._Isprocess = False
            Me._Isvoid = False
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Approvalremark = ""
            Me._Mappingunkid = 0
            Me._Isfinal = False
            Me._OperationTypeId = clsEmployeeDataApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

            blnFlag = Insert(intCompanyId, objDataOperation)

            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then

                objEmployee_Refree_tran._VoidLoginEmployeeunkid = -1
                objEmployee_Refree_tran._Voidreason = xVoidReason
                objEmployee_Refree_tran._Voiduserunkid = Me._Audituserunkid
                objEmployee_Refree_tran._Voiddatetime = Now
                objEmployee_Refree_tran._Isvoid = True
                objEmployee_Refree_tran._ClientIP = Me._Ip


                'Gajanan [9-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                objEmployee_Refree_tran._FormName = Me._Form_Name
                objEmployee_Refree_tran._HostName = Me._Host
                objEmployee_Refree_tran._AuditUserId = Me._Audituserunkid
                'Gajanan [9-July-2019] -- End

                If objEmployee_Refree_tran.Delete(intUnkid, objDataOperation) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    Public Function isExist(ByVal intEmpId As Integer, ByVal strName As String, ByVal xeOprType As clsEmployeeDataApproval.enOperationType, Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal strtrangUid As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = True, Optional ByRef intOperationTypeId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End

        Try

            strQ = "SELECT " & _
                    " tranguid " & _
                    ", transactiondate " & _
                    ", mappingunkid " & _
                    ", approvalremark " & _
                    ", isfinal " & _
                    ", statusunkid " & _
                    ", refereetranunkid " & _
                    ", employeeunkid " & _
                    ", name " & _
                    ", address " & _
                    ", countryunkid " & _
                    ", stateunkid " & _
                    ", cityunkid " & _
                    ", email " & _
                    ", gender " & _
                    ", identify_no " & _
                    ", telephone_no " & _
                    ", mobile_no " & _
                    ", ref_position " & _
                    ", operationtypeid " & _
                    "FROM hremployee_referee_approval_tran " & _
                    "WHERE name = @name " & _
                    "AND isvoid = 0 "

            If strtrangUid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid"
            End If

            If intEmpId > 0 Then
                strQ &= " AND employeeunkid = @EmpId "
            End If

            'If xeOprType = clsEmployeeDataApproval.enOperationType.EDITED Or mintOperationTypeId = clsEmployeeDataApproval.enOperationType.DELETED Then
            'If intUnkid > 0 Then
            'strQ &= " AND refereetranunkid = @refereetranunkid "
            'End If
            'End If


            'Gajanan [10-June-2019] -- Start      
            If blnPreventNewEntry Then
                strQ &= " AND ISNULL(hremployee_referee_approval_tran.isprocessed,0)= 0 "
            End If
            strQ &= " AND ISNULL(hremployee_referee_approval_tran.isprocessed,0)= 0 "
            'Gajanan [10-June-2019] -- End

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@refereetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strtrangUid)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xeOprType)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intOperationTypeId = CInt(dsList.Tables(0).Rows(0)("operationtypeid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intRefreeUnkid As Integer, ByVal name As String, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hremployee_referee_approval_tran SET " & _
                   " " & strColName & " = '" & intRefreeUnkid & "' " & _
                   "WHERE isprocessed= 0 and name = @name AND employeeunkid = @employeeunkid  "

            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, name)
            'Gajanan [31-May-2020] -- End


            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
End Class
