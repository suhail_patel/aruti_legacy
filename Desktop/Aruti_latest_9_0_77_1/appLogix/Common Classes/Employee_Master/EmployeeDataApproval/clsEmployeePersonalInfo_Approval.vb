﻿Imports eZeeCommonLib
Public Class clsEmployeePersonalInfo_Approval
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeePersonal_Approval"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrApprovalremark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintBirthcountryunkid As Integer = 0
    Private mintBirthstateunkid As Integer = 0
    Private mintBirthcityunkid As Integer = 0
    Private mstrBirth_Ward As String = String.Empty
    Private mstrBirthcertificateno As String = String.Empty
    Private mstrBirth_Village As String = String.Empty
    Private mintBirthtownunkid As Integer = 0
    Private mintBirthchiefdomunkid As Integer = 0
    Private mintBirthvillageunkid As Integer = 0
    Private mintComplexionunkid As Integer = 0
    Private mintBloodgroupunkid As Integer = 0
    Private mintEyecolorunkid As Integer = 0
    Private mintNationalityunkid As Integer = 0
    Private mintEthincityunkid As Integer = 0
    Private mintReligionunkid As Integer = 0
    Private mintHairunkid As Integer = 0
    Private mintMaritalstatusunkid As Integer = 0
    Private mstrExtra_Tel_No As String = String.Empty
    Private mintLanguage1unkid As Integer = 0
    Private mintLanguage2unkid As Integer = 0
    Private mintLanguage3unkid As Integer = 0
    Private mintLanguage4unkid As Integer = 0
    Private mdblHeight As Double = 0
    Private mdblWeight As Double = 0
    Private mdtAnniversary_Date As Date = Nothing
    Private mstrAllergiesunkids As String = String.Empty
    Private mstrDisabilitiesunkids As String = String.Empty
    Private mstrSports_Hobbies As String = String.Empty
    Private mblnIsbirthinfo As Boolean
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtAuditdatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsprocessed As Boolean
    Private mintOperationtypeid As Integer
    Private mstrnewattachdocumentid As String = String.Empty
    Private mstrdeleteattachdocumentid As String = String.Empty
#End Region


#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalremark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Public Property _Employeeunkid() As Integer
    Public Property _Employeeunkid(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        'Gajanan [17-April-2019] -- End
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
            GetData(xDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthcountryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birthcountryunkid() As Integer
        Get
            Return mintBirthcountryunkid
        End Get
        Set(ByVal value As Integer)
            mintBirthcountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthstateunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birthstateunkid() As Integer
        Get
            Return mintBirthstateunkid
        End Get
        Set(ByVal value As Integer)
            mintBirthstateunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthcityunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birthcityunkid() As Integer
        Get
            Return mintBirthcityunkid
        End Get
        Set(ByVal value As Integer)
            mintBirthcityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birth_ward
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birth_Ward() As String
        Get
            Return mstrBirth_Ward
        End Get
        Set(ByVal value As String)
            mstrBirth_Ward = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthcertificateno
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birthcertificateno() As String
        Get
            Return mstrBirthcertificateno
        End Get
        Set(ByVal value As String)
            mstrBirthcertificateno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birth_village
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birth_Village() As String
        Get
            Return mstrBirth_Village
        End Get
        Set(ByVal value As String)
            mstrBirth_Village = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthtownunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birthtownunkid() As Integer
        Get
            Return mintBirthtownunkid
        End Get
        Set(ByVal value As Integer)
            mintBirthtownunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthchiefdomunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birthchiefdomunkid() As Integer
        Get
            Return mintBirthchiefdomunkid
        End Get
        Set(ByVal value As Integer)
            mintBirthchiefdomunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthvillageunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Birthvillageunkid() As Integer
        Get
            Return mintBirthvillageunkid
        End Get
        Set(ByVal value As Integer)
            mintBirthvillageunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set complexionunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Complexionunkid() As Integer
        Get
            Return mintComplexionunkid
        End Get
        Set(ByVal value As Integer)
            mintComplexionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bloodgroupunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Bloodgroupunkid() As Integer
        Get
            Return mintBloodgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintBloodgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set eyecolorunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Eyecolorunkid() As Integer
        Get
            Return mintEyecolorunkid
        End Get
        Set(ByVal value As Integer)
            mintEyecolorunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nationalityunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Nationalityunkid() As Integer
        Get
            Return mintNationalityunkid
        End Get
        Set(ByVal value As Integer)
            mintNationalityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ethnicityunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ethincityunkid() As Integer
        Get
            Return mintEthincityunkid
        End Get
        Set(ByVal value As Integer)
            mintEthincityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set religionunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Religionunkid() As Integer
        Get
            Return mintReligionunkid
        End Get
        Set(ByVal value As Integer)
            mintReligionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hairunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Hairunkid() As Integer
        Get
            Return mintHairunkid
        End Get
        Set(ByVal value As Integer)
            mintHairunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set maritalstatusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Maritalstatusunkid() As Integer
        Get
            Return mintMaritalstatusunkid
        End Get
        Set(ByVal value As Integer)
            mintMaritalstatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set extra_tel_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Extra_Tel_No() As String
        Get
            Return mstrExtra_Tel_No
        End Get
        Set(ByVal value As String)
            mstrExtra_Tel_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set language1unkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Language1unkid() As Integer
        Get
            Return mintLanguage1unkid
        End Get
        Set(ByVal value As Integer)
            mintLanguage1unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set language2unkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Language2unkid() As Integer
        Get
            Return mintLanguage2unkid
        End Get
        Set(ByVal value As Integer)
            mintLanguage2unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set language3unkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Language3unkid() As Integer
        Get
            Return mintLanguage3unkid
        End Get
        Set(ByVal value As Integer)
            mintLanguage3unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set language4unkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Language4unkid() As Integer
        Get
            Return mintLanguage4unkid
        End Get
        Set(ByVal value As Integer)
            mintLanguage4unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set height
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Height() As Double
        Get
            Return mdblHeight
        End Get
        Set(ByVal value As Double)
            mdblHeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Weight() As Double
        Get
            Return mdblWeight
        End Get
        Set(ByVal value As Double)
            mdblWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set anniversary_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Anniversary_Date() As Date
        Get
            Return mdtAnniversary_Date
        End Get
        Set(ByVal value As Date)
            mdtAnniversary_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allergiesunkids
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Allergiesunkids() As String
        Get
            Return mstrAllergiesunkids
        End Get
        Set(ByVal value As String)
            mstrAllergiesunkids = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disabilitiesunkids
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Disabilitiesunkids() As String
        Get
            Return mstrDisabilitiesunkids
        End Get
        Set(ByVal value As String)
            mstrDisabilitiesunkids = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sports_hobbies
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Sports_Hobbies() As String
        Get
            Return mstrSports_Hobbies
        End Get
        Set(ByVal value As String)
            mstrSports_Hobbies = value
        End Set
    End Property

    Public Property _IsBirthInfo() As Boolean
        Get
            Return mblnIsbirthinfo
        End Get
        Set(ByVal value As Boolean)
            mblnIsbirthinfo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocessed
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set operationtypeid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Operationtypeid() As Integer
        Get
            Return mintOperationtypeid
        End Get
        Set(ByVal value As Integer)
            mintOperationtypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set newmrgattachdocumentid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Newattachdocumentid() As String
        Get
            Return mstrnewattachdocumentid
        End Get
        Set(ByVal value As String)
            mstrnewattachdocumentid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deletemrgattachdocumentid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Deleteattachdocumentid() As String
        Get
            Return mstrdeleteattachdocumentid
        End Get
        Set(ByVal value As String)
            mstrdeleteattachdocumentid = value
        End Set
    End Property

#End Region

    Public Sub GetData(Optional ByVal xDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                       "  tranguid " & _
                       ", transactiondate " & _
                       ", mappingunkid " & _
                       ", approvalremark " & _
                       ", isfinal " & _
                       ", statusunkid " & _
                       ", birthcountryunkid " & _
                       ", birthstateunkid " & _
                       ", birthcityunkid " & _
                       ", birth_ward " & _
                       ", birthcertificateno " & _
                       ", birth_village " & _
                       ", birthtownunkid " & _
                       ", birthchiefdomunkid " & _
                       ", birthvillageunkid " & _
                       ", complexionunkid " & _
                       ", bloodgroupunkid " & _
                       ", eyecolorunkid " & _
                       ", nationalityunkid " & _
                       ", ethnicityunkid " & _
                       ", religionunkid " & _
                       ", hairunkid " & _
                       ", maritalstatusunkid " & _
                       ", extra_tel_no " & _
                       ", language1unkid " & _
                       ", language2unkid " & _
                       ", language3unkid " & _
                       ", language4unkid " & _
                       ", height " & _
                       ", weight " & _
                       ", anniversary_date " & _
                       ", allergiesunkids " & _
                       ", disabilitiesunkids " & _
                       ", sports_hobbies " & _
                       ", isbirthinfo " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", auditdatetime " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", ip " & _
                       ", host " & _
                       ", form_name " & _
                       ", isweb " & _
                       ", isprocessed " & _
                       ", operationtypeid " & _
                       ", newattachdocumentid " & _
                       ", deleteattachdocumentid " & _
                   "FROM hremployee_personal_approval_tran " & _
                   "WHERE employeeunkid = @employeeunkid and isfinal=0 and isprocessed = 0 and isbirthinfo=@isbirthinfo  and isvoid = 0"

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@isbirthinfo", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbirthinfo)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            mstrTranguid = ""
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



            mstrnewattachdocumentid = ""
            mstrdeleteattachdocumentid = ""

               'Gajanan [17-April-2019] -- End

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrTranguid = dtRow.Item("tranguid").ToString
                mdtTransactiondate = dtRow.Item("transactiondate")
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mstrApprovalremark = dtRow.Item("approvalremark").ToString
                mblnIsfinal = CBool(dtRow.Item("isfinal"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintBirthcountryunkid = CInt(dtRow.Item("birthcountryunkid"))
                mintBirthstateunkid = CInt(dtRow.Item("birthstateunkid"))
                mintBirthcityunkid = CInt(dtRow.Item("birthcityunkid"))
                mstrBirth_Ward = dtRow.Item("birth_ward").ToString
                mstrBirthcertificateno = dtRow.Item("birthcertificateno").ToString
                mstrBirth_Village = dtRow.Item("birth_village").ToString
                mintBirthtownunkid = CInt(dtRow.Item("birthtownunkid"))
                mintBirthchiefdomunkid = CInt(dtRow.Item("birthchiefdomunkid"))
                mintBirthvillageunkid = CInt(dtRow.Item("birthvillageunkid"))
                mintComplexionunkid = CInt(dtRow.Item("complexionunkid"))
                mintBloodgroupunkid = CInt(dtRow.Item("bloodgroupunkid"))
                mintEyecolorunkid = CInt(dtRow.Item("eyecolorunkid"))
                mintNationalityunkid = CInt(dtRow.Item("nationalityunkid"))
                mintEthincityunkid = CInt(dtRow.Item("ethnicityunkid"))
                mintReligionunkid = CInt(dtRow.Item("religionunkid"))
                mintHairunkid = CInt(dtRow.Item("hairunkid"))
                mintMaritalstatusunkid = CInt(dtRow.Item("maritalstatusunkid"))
                mstrExtra_Tel_No = dtRow.Item("extra_tel_no").ToString
                mintLanguage1unkid = CInt(dtRow.Item("language1unkid"))
                mintLanguage2unkid = CInt(dtRow.Item("language2unkid"))
                mintLanguage3unkid = CInt(dtRow.Item("language3unkid"))
                mintLanguage4unkid = CInt(dtRow.Item("language4unkid"))
                mdblHeight = CDbl(dtRow.Item("height"))
                mdblWeight = CDbl(dtRow.Item("weight"))

                If mdtAnniversary_Date <> Nothing Then
                    mdtAnniversary_Date = dtRow.Item("anniversary_date")
                End If

                mstrAllergiesunkids = dtRow.Item("allergiesunkids").ToString
                mstrDisabilitiesunkids = dtRow.Item("disabilitiesunkids").ToString
                mstrSports_Hobbies = dtRow.Item("sports_hobbies").ToString
                mblnIsbirthinfo = CBool(dtRow.Item("isbirthinfo"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mdtAuditdatetime = dtRow.Item("auditdatetime")
                mintAudittype = CInt(dtRow.Item("audittype"))
                mintAudituserunkid = CInt(dtRow.Item("audituserunkid"))
                mstrIp = dtRow.Item("ip").ToString
                mstrHost = dtRow.Item("host").ToString
                mstrForm_Name = dtRow.Item("form_name").ToString
                mblnIsweb = CBool(dtRow.Item("isweb"))
                mblnIsprocessed = CBool(dtRow.Item("isprocessed"))
                mintOperationtypeid = CInt(dtRow.Item("operationtypeid"))
                mstrnewattachdocumentid = dtRow.Item("newattachdocumentid").ToString
                mstrdeleteattachdocumentid = dtRow.Item("deleteattachdocumentid").ToString
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
If xDataOp Is Nothing Then objDataOperation = Nothing
               'Gajanan [17-April-2019] -- Start
        End Try
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    'Public Function Insert(ByVal xblnIsbirthinfo As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnFromApproval As Boolean = False) As Boolean
    Public Function Insert(ByVal xblnIsbirthinfo As Boolean, _
                           ByVal intCompanyId As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal blnFromApproval As Boolean = False, _
                           Optional ByVal dtDocument As DataTable = Nothing) As Boolean
        'S.SANDEEP |26-APR-2019| -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        If blnFromApproval = False Then
            'S.SANDEEP |26-APR-2019| -- START
            'If isPersonalInfoExist(mintEmployeeunkid, xblnIsbirthinfo, objDataOperation) Then
            '    Return False
            'End If
            Dim blnCheckIsExist As Boolean = True
            If dtDocument IsNot Nothing Then
                If mblnIsbirthinfo Then
                    If dtDocument.AsEnumerable().AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = CInt(enScanAttactRefId.EMPLOYEE_BIRTHINFO) AndAlso x.Field(Of String)("AUD") <> "").Count > 0 Then
                        blnCheckIsExist = False
                    End If
                Else
                    If dtDocument.AsEnumerable().AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = CInt(enScanAttactRefId.EMPLYOEE_OTHERLDETAILS) AndAlso x.Field(Of String)("AUD") <> "").Count > 0 Then
                        blnCheckIsExist = False
                    End If
                End If
            End If
            If blnCheckIsExist Then
            If isPersonalInfoExist(mintEmployeeunkid, xblnIsbirthinfo, objDataOperation) Then
                    'Gajanan [27-May-2019] -- Start      
                    If mblnIsbirthinfo Then
                        mstrMessage = Language.getMessage(mstrModuleName, 50, "Selected birthinfo is already assigned to the employee. Please assign new birthinfo.")
                    End If
                    'Gajanan [27-May-2019] -- End
                Return False
            End If
            End If
            'S.SANDEEP |26-APR-2019| -- END
           
            If isPersonalInfoInApproval(mintEmployeeunkid, xblnIsbirthinfo, objDataOperation) Then
                'Gajanan [27-May-2019] -- Start   
                If mblnIsbirthinfo Then
                    mstrMessage = Language.getMessage(mstrModuleName, 51, "Sorry, you cannot add seleted information, Reason : Same birthinfo is already present in approval process.")
                End If
                'Gajanan [27-May-2019] -- End
                Return False
            End If
        End If

        Try
            'S.SANDEEP |26-APR-2019| -- START
            Dim eAttachType As enScanAttactRefId = Nothing
            If mblnIsbirthinfo Then
                eAttachType = enScanAttactRefId.EMPLOYEE_BIRTHINFO
            Else
                eAttachType = enScanAttactRefId.EMPLYOEE_OTHERLDETAILS
            End If

            Dim objDocument As New clsScan_Attach_Documents
            Dim strLocalPath As String = ""
            Dim dtTran As DataTable = Nothing
            Dim strFolderName As String = ""
            If dtDocument IsNot Nothing Then
                If dtDocument.Rows.Count > 0 Then
                    Dim objConfig As New clsConfigOptions
                    strLocalPath = objConfig.GetKeyValue(intCompanyId, "DocumentPath")
                    If strLocalPath Is Nothing Then strLocalPath = ""
                    objConfig = Nothing
                    strFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", eAttachType).Tables(0).Rows(0)("Name").ToString
                    If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                    dtTran = objDocument._Datatable
                End If
            End If

            If dtDocument IsNot Nothing AndAlso dtDocument.Rows.Count > 0 Then
                Dim ftab As DataTable = New DataView(dtDocument, "scanattachrefid = '" & CInt(eAttachType) & "'", "", DataViewRowState.CurrentRows).ToTable
                For Each drow As DataRow In ftab.Rows
                    Dim dr As DataRow = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("orgfilepath")
                    dr("destfilepath") = strLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("filesize") = drow("filesize")
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")

                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = ftab
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDocument._Datatable.Rows.Count > 0 AndAlso objDocument._Newattachdocumentids.Length <= 0 AndAlso objDocument._Deleteattachdocumentids.Length <= 0 AndAlso _
                  (objDocument._Datatable.Select("AUD = 'D'").Count > 0 Or objDocument._Datatable.Select("scanattachtranunkid = -1").Count > 0) Then
                    Return False
                End If

            End If
            'S.SANDEEP |26-APR-2019| -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            End If
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            objDataOperation.AddParameter("@birthcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthcountryunkid.ToString)
            objDataOperation.AddParameter("@birthstateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthstateunkid.ToString)
            objDataOperation.AddParameter("@birthcityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthcityunkid.ToString)
            objDataOperation.AddParameter("@birth_ward", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBirth_Ward.ToString)
            objDataOperation.AddParameter("@birthcertificateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBirthcertificateno.ToString)
            objDataOperation.AddParameter("@birth_village", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBirth_Village.ToString)
            objDataOperation.AddParameter("@birthtownunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthtownunkid.ToString)
            objDataOperation.AddParameter("@birthchiefdomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthchiefdomunkid.ToString)
            objDataOperation.AddParameter("@birthvillageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthvillageunkid.ToString)
            objDataOperation.AddParameter("@complexionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComplexionunkid.ToString)
            objDataOperation.AddParameter("@bloodgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBloodgroupunkid.ToString)
            objDataOperation.AddParameter("@eyecolorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEyecolorunkid.ToString)
            objDataOperation.AddParameter("@nationalityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNationalityunkid.ToString)
            objDataOperation.AddParameter("@ethnicityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEthincityunkid.ToString)
            objDataOperation.AddParameter("@religionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReligionunkid.ToString)
            objDataOperation.AddParameter("@hairunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHairunkid.ToString)
            objDataOperation.AddParameter("@maritalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaritalstatusunkid.ToString)
            objDataOperation.AddParameter("@extra_tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExtra_Tel_No.ToString)
            objDataOperation.AddParameter("@language1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage1unkid.ToString)
            objDataOperation.AddParameter("@language2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage2unkid.ToString)
            objDataOperation.AddParameter("@language3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage3unkid.ToString)
            objDataOperation.AddParameter("@language4unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage4unkid.ToString)
            objDataOperation.AddParameter("@height", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblHeight.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblWeight.ToString)

            If mdtAnniversary_Date = Nothing Then
                objDataOperation.AddParameter("@anniversary_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@anniversary_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAnniversary_Date)
            End If

            objDataOperation.AddParameter("@allergiesunkids", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrAllergiesunkids.ToString)
            objDataOperation.AddParameter("@disabilitiesunkids", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrDisabilitiesunkids.ToString)
            objDataOperation.AddParameter("@sports_hobbies", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSports_Hobbies.ToString)
            objDataOperation.AddParameter("@isbirthinfo", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbirthinfo)

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationtypeid.ToString)

            'S.SANDEEP |26-APR-2019| -- START
            If objDocument._Newattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, objDocument._Newattachdocumentids.Length, objDocument._Newattachdocumentids)
            Else
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, mstrnewattachdocumentid.Length, mstrnewattachdocumentid.ToString())
            End If

            If objDocument._Deleteattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, objDocument._Deleteattachdocumentids.Length, objDocument._Deleteattachdocumentids)
            Else
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, mstrdeleteattachdocumentid.Length, mstrdeleteattachdocumentid.ToString())
            End If
            'S.SANDEEP |26-APR-2019| -- END

            strQ = "INSERT INTO hremployee_personal_approval_tran ( " & _
                  "  tranguid " & _
                  ", transactiondate " & _
                  ", mappingunkid " & _
                  ", approvalremark " & _
                  ", isfinal " & _
                  ", statusunkid " & _
                  ", employeeunkid " & _
                  ", birthcountryunkid " & _
                  ", birthstateunkid " & _
                  ", birthcityunkid " & _
                  ", birth_ward " & _
                  ", birthcertificateno " & _
                  ", birth_village " & _
                  ", birthtownunkid " & _
                  ", birthchiefdomunkid " & _
                  ", birthvillageunkid " & _
                  ", complexionunkid " & _
                  ", bloodgroupunkid " & _
                  ", eyecolorunkid " & _
                  ", nationalityunkid " & _
                  ", ethnicityunkid " & _
                  ", religionunkid " & _
                  ", hairunkid " & _
                  ", maritalstatusunkid " & _
                  ", extra_tel_no " & _
                  ", language1unkid " & _
                  ", language2unkid " & _
                  ", language3unkid " & _
                  ", language4unkid " & _
                  ", height " & _
                  ", weight " & _
                  ", anniversary_date " & _
                  ", allergiesunkids " & _
                  ", disabilitiesunkids " & _
                  ", sports_hobbies " & _
                  ", isbirthinfo " & _
                  ", newattachdocumentid " & _
                  ", deleteattachdocumentid " & _
                  ", loginemployeeunkid " & _
                  ", isvoid " & _
                  ", auditdatetime " & _
                  ", audittype " & _
                  ", audituserunkid " & _
                  ", ip " & _
                  ", host " & _
                  ", form_name " & _
                  ", isweb " & _
                  ", isprocessed " & _
                  ", operationtypeid " & _
                ") VALUES (" & _
                  "  @tranguid " & _
                  ", @transactiondate " & _
                  ", @mappingunkid " & _
                  ", @approvalremark " & _
                  ", @isfinal " & _
                  ", @statusunkid " & _
                  ", @employeeunkid " & _
                  ", @birthcountryunkid " & _
                  ", @birthstateunkid " & _
                  ", @birthcityunkid " & _
                  ", @birth_ward " & _
                  ", @birthcertificateno " & _
                  ", @birth_village " & _
                  ", @birthtownunkid " & _
                  ", @birthchiefdomunkid " & _
                  ", @birthvillageunkid " & _
                  ", @complexionunkid " & _
                  ", @bloodgroupunkid " & _
                  ", @eyecolorunkid " & _
                  ", @nationalityunkid " & _
                  ", @ethnicityunkid " & _
                  ", @religionunkid " & _
                  ", @hairunkid " & _
                  ", @maritalstatusunkid " & _
                  ", @extra_tel_no " & _
                  ", @language1unkid " & _
                  ", @language2unkid " & _
                  ", @language3unkid " & _
                  ", @language4unkid " & _
                  ", @height " & _
                  ", @weight " & _
                  ", @anniversary_date " & _
                  ", @allergiesunkids " & _
                  ", @disabilitiesunkids " & _
                  ", @sports_hobbies " & _
                  ", @isbirthinfo " & _
                  ", @newattachdocumentid " & _
                  ", @deleteattachdocumentid " & _
                  ", @loginemployeeunkid " & _
                  ", @isvoid " & _
                  ", Getdate() " & _
                  ", @audittype " & _
                  ", @audituserunkid " & _
                  ", @ip " & _
                  ", @host " & _
                  ", @form_name " & _
                  ", @isweb " & _
                  ", @isprocessed " & _
                  ", @operationtypeid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal isBirthinfo As Boolean, ByVal intTranUnkid As Integer, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "UPDATE hremployee_personal_approval_tran SET " & _
                   " " & strColName & " = '" & intTranUnkid & "' " & _
                   "WHERE isprocessed= 0 and employeeunkid = @employeeunkid AND isbirthinfo = @isbirthinfo AND isprocessed = 0"


            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@isbirthinfo", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isBirthinfo)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            'Gajanan [31-May-2020] -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP |26-APR-2019| -- START
    Public Function UpdateDeleteDocument(ByVal xDataOperation As clsDataOperation, _
                                         ByVal intEmployeeId As Integer, _
                                         ByVal intTransactionId As Integer, _
                                         ByVal strScanRefIds As String, _
                                         ByVal strFormName As String, _
                                         ByVal strColName As String, _
                                         ByVal blnIsDelete As Boolean) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            If strScanRefIds.Trim.Length > 0 Then
                strQ = "UPDATE hrdocuments_tran SET " & strColName & " = '" & intTransactionId & "' ,isinapproval = 0 "
                If blnIsDelete Then
                    strQ &= ", isactive = 0 "
                Else
                    strQ &= ", isactive = 1 "
                End If
                strQ &= "WHERE employeeunkid = '" & intEmployeeId & "' AND scanattachtranunkid IN (" & strScanRefIds & ") "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If xDataOperation Is Nothing Then
                    objDataOperation.ReleaseTransaction(True)
                End If
            End If
            Return True
        Catch ex As Exception
            If xDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP |26-APR-2019| -- END

    Public Function isPersonalInfoExist(ByVal intEmpUnkid As Integer, _
                                        ByVal xIsbirthinfo As Boolean, _
                                        Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  hremployee_master.employeeunkid " & _
              " FROM hremployee_master  "


            If xIsbirthinfo = False AndAlso mstrAllergiesunkids.Length > 0 Then
                strQ &= "left join hrallergies_tran on  hremployee_master.employeeunkid = hrallergies_tran.employeeunkid "
            End If

            If xIsbirthinfo = False AndAlso mstrDisabilitiesunkids.Length > 0 Then
                strQ &= "left join hrdisabilities_tran on  hremployee_master.employeeunkid = hrdisabilities_tran.employeeunkid "
            End If

            strQ &= " WHERE "
          
            strQ &= " hremployee_master.employeeunkid = @employeeunkid "

            If xIsbirthinfo Then
                strQ &= " and birthcountryunkid =@birthcountryunkid" & _
                    " and birthstateunkid=@birthstateunkid " & _
                    " and birthcityunkid =@birthcityunkid " & _
                    " and birth_ward =@birth_ward " & _
                    " and birthcertificateno=@birthcertificateno " & _
                    " and birth_village=@birth_village " & _
                    " and birthtownunkid=@birthtownunkid " & _
                    " and birthchiefdomunkid=@birthchiefdomunkid " & _
                    " and birthvillageunkid=@birthvillageunkid "
            Else
                If mstrAllergiesunkids.Length > 0 Then
                    strQ &= "and hrallergies_tran.allergiesunkid in(" & mstrAllergiesunkids & ") "
                End If

                If mstrDisabilitiesunkids.Length > 0 Then
                    strQ &= "and hrdisabilities_tran.disabilitiesunkid in(" & mstrDisabilitiesunkids & ") "
                End If

                strQ &= " and complexionunkid=@complexionunkid " & _
                    " and bloodgroupunkid=@bloodgroupunkid " & _
                    " and eyecolorunkid=@eyecolorunkid " & _
                    " and nationalityunkid=@nationalityunkid " & _
                    " and ethnicityunkid=@ethnicityunkid " & _
                    " and religionunkid=@religionunkid " & _
                    " and hairunkid=@hairunkid " & _
                    " and maritalstatusunkid=@maritalstatusunkid " & _
                    " and extra_tel_no=@extra_tel_no " & _
                    " and language1unkid=@language1unkid " & _
                    " and language2unkid=@language2unkid " & _
                    " and language3unkid=@language3unkid " & _
                    " and language4unkid=@language4unkid " & _
                    " and height=@height " & _
                    " and weight=@weight " & _
                    " and sports_hobbies=@sports_hobbies "

                If mdtAnniversary_Date = Nothing Then
                    strQ &= " and anniversary_date is NULL "
                Else
                    strQ &= " AND CONVERT(CHAR(8),anniversary_date,112) = @anniversary_date "
            End If


            End If
            
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpUnkid)
            If xIsbirthinfo Then

                objDataOperation.AddParameter("@birthcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthcountryunkid)
                objDataOperation.AddParameter("@birthstateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthstateunkid.ToString)
                objDataOperation.AddParameter("@birthcityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthcityunkid.ToString)
                objDataOperation.AddParameter("@birth_ward", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBirth_Ward.ToString)
                objDataOperation.AddParameter("@birthcertificateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBirthcertificateno.ToString)
                objDataOperation.AddParameter("@birth_village", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBirth_Village.ToString)
                objDataOperation.AddParameter("@birthtownunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthtownunkid)
                objDataOperation.AddParameter("@birthchiefdomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthchiefdomunkid)
                objDataOperation.AddParameter("@birthvillageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBirthvillageunkid)

            Else

                objDataOperation.AddParameter("@complexionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComplexionunkid)
                objDataOperation.AddParameter("@bloodgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBloodgroupunkid)
                objDataOperation.AddParameter("@eyecolorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEyecolorunkid)
                objDataOperation.AddParameter("@nationalityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNationalityunkid)
                objDataOperation.AddParameter("@ethnicityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEthincityunkid)
                objDataOperation.AddParameter("@religionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReligionunkid)
                objDataOperation.AddParameter("@hairunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHairunkid)
                objDataOperation.AddParameter("@maritalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaritalstatusunkid)
                objDataOperation.AddParameter("@extra_tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExtra_Tel_No)
                objDataOperation.AddParameter("@language1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage1unkid)
                objDataOperation.AddParameter("@language2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage2unkid)
                objDataOperation.AddParameter("@language3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage3unkid)
                objDataOperation.AddParameter("@language4unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage4unkid)
                objDataOperation.AddParameter("@height", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblHeight)
                objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight)
                If mdtAnniversary_Date = Nothing Then
                    objDataOperation.AddParameter("@anniversary_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@anniversary_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAnniversary_Date))
                End If
                objDataOperation.AddParameter("@sports_hobbies", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrSports_Hobbies)

            End If

            objDataOperation.AddParameter("@isbirthinfo", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xIsbirthinfo)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPersonalInfoExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isPersonalInfoInApproval(ByVal intEmpUnkid As Integer, _
                                             ByVal xIsbirthinfo As Boolean, _
                                             Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  employeeunkid " & _
              " FROM hremployee_personal_approval_tran " & _
              " WHERE employeeunkid = @employeeunkid and isbirthinfo = @isbirthinfo and isprocessed=0 and isfinal=0"

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)
            objDataOperation.AddParameter("@isbirthinfo", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xIsbirthinfo)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPersonalInfoInApproval; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isPersonalInfoNewlyAdded(ByVal intEmpUnkid As Integer, _
                                             ByVal xIsbirthinfo As Boolean, _
                                             Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
              " employeeunkid " & _
              " FROM hremployee_master " & _
              " WHERE " & _
              " employeeunkid = @employeeunkid "

            If xIsbirthinfo Then
                strQ &= " and birthcountryunkid =0  " & _
              " and birthstateunkid=0 " & _
              " and birthcityunkid =0 " & _
              " and birth_ward=0 " & _
              " and birthcertificateno=0 " & _
              " and birth_village=0 " & _
              " and birthtownunkid=0 " & _
              " and birthchiefdomunkid=0 " & _
              " and birthvillageunkid=0 "
            Else
                strQ &= " and complexionunkid=0 " & _
              " and bloodgroupunkid=0 " & _
              " and eyecolorunkid=0 " & _
              " and nationalityunkid=0 " & _
              " and ethnicityunkid=0 " & _
              " and religionunkid=0 " & _
              " and hairunkid=0 " & _
              " and maritalstatusunkid=0 " & _
              " and extra_tel_no='' " & _
              " and language1unkid=0 " & _
              " and language2unkid=0 " & _
              " and language3unkid=0 " & _
              " and language4unkid=0 " & _
              " and height=0 " & _
              " and weight=0 " & _
              " and anniversary_date IS NULL " & _
              " and sports_hobbies='' "
            End If


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPersonalInfoNewlyAdded; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 50, "Selected birthinfo is already assigned to the employee. Please assign new birthinfo.")
			Language.setMessage(mstrModuleName, 51, "Sorry, you cannot add seleted information, Reason : Same birthinfo is already present in approval process.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

