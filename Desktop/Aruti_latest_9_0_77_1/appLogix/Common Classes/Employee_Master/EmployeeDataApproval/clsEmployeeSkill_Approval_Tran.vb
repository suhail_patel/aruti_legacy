﻿Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master

Public Class clsEmployeeSkill_Approval_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeSkill_Approval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrApprovalremark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintSkillstranunkid As Integer
    Private mintEmp_App_Unkid As Integer
    Private mintSkillcategoryunkid As Integer
    Private mintSkillunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsapplicant As Boolean
    Private mblnIsvoid As Boolean
    Private mdtAuditdatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsprocessed As Boolean
    Private mintLoginEmployeeUnkid As Integer = 0
    Private mintOperationTypeId As Integer = 0

    'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
    Private mstrOther_SkillCategory As String = ""
    Private mstrOther_Skill As String = ""
    Private mintSkillExpertiseunkid As Integer = 0
    'Pinkal (24-Sep-2020) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalremark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillstranunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Skillstranunkid() As Integer
        Get
            Return mintSkillstranunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillstranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_app_unkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Emp_App_Unkid() As Integer
        Get
            Return mintEmp_App_Unkid
        End Get
        Set(ByVal value As Integer)
            mintEmp_App_Unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillcategoryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Skillcategoryunkid() As Integer
        Get
            Return mintSkillcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillcategoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Skillunkid() As Integer
        Get
            Return mintSkillunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isapplicant
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isapplicant() As Boolean
        Get
            Return mblnIsapplicant
        End Get
        Set(ByVal value As Boolean)
            mblnIsapplicant = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Object
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Object)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocess
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    Public Property _LoginEmployeeUnkid() As Integer
        Get
            Return mintLoginEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeUnkid = value
        End Set
    End Property

    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property

    'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
    ''' <summary>
    ''' Purpose: Get or Set Other_SkillCategory
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Other_SkillCategory() As String
        Get
            Return mstrOther_SkillCategory
        End Get
        Set(ByVal value As String)
            mstrOther_SkillCategory = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Ohter_Skill
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Other_Skill() As String
        Get
            Return mstrOther_Skill
        End Get
        Set(ByVal value As String)
            mstrOther_Skill = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SkillExpertiseunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _SkillExpertiseunkid() As Integer
        Get
            Return mintSkillExpertiseunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillExpertiseunkid = value
        End Set
    End Property

    'Pinkal (24-Sep-2020) -- End

#End Region

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet


        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
                       "    ,ISNULL(cfcommon_master.name,'') AS Category " & _
                       "    ,ISNULL(hrskill_master.skillname,'') AS SkillName " & _
                       "    ,hremp_app_skills_approval_tran.description AS Description " & _
                       "    ,hremp_app_skills_approval_tran.skillstranunkid As SkillTranId " & _
                       "    ,cfcommon_master.masterunkid As CatId " & _
                       "    ,hrskill_master.skillunkid As SkillId " & _
                       "    ,hremployee_master.employeeunkid As EmpId " & _
                       "    ,-1 AS loginemployeeunkid " & _
                       "    ,-1 AS voidloginemployeeunkid " & _
                       "    ,hremp_app_skills_approval_tran.tranguid As tranguid " & _
                       "    ,ISNULL(hremp_app_skills_approval_tran.operationtypeid,0) AS operationtypeid " & _
                       "    ,CASE WHEN ISNULL(hremp_app_skills_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                       "          WHEN ISNULL(hremp_app_skills_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                       "          WHEN ISNULL(hremp_app_skills_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                       "     END AS OperationType " & _
                       "    ,ISNULL(pInfo.pCategory,'') AS pCategory " & _
                       "    ,ISNULL(pInfo.pSkill,'') AS pSkill " & _
                       "    /*,ISNULL(pInfo.rno,0) AS pRow*/ " & _
                       "    ,hremp_app_skills_approval_tran.other_skillcategory " & _
                       "    ,hremp_app_skills_approval_tran.other_skill " & _
                       "    ,hremp_app_skills_approval_tran.skillexpertiseunkid " & _
                       "FROM hremp_app_skills_approval_tran " & _
                       "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_approval_tran.skillcategoryunkid AND cfcommon_master.mastertype = " & enCommonMaster.SKILL_CATEGORY & _
                       "    LEFT JOIN hrskill_master ON hremp_app_skills_approval_tran.skillunkid = hrskill_master.skillunkid " & _
                       "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_approval_tran.emp_app_unkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             EST.emp_app_unkid " & _
                       "            ,EST.skillstranunkid " & _
                       "            ,SC.name AS pCategory " & _
                       "            ,SK.skillname AS pSkill " & _
                       "            ,ROW_NUMBER()OVER(ORDER BY EST.skillstranunkid) AS rno " & _
                       "        FROM hremp_app_skills_tran AS EST " & _
                       "            JOIN cfcommon_master AS SC ON SC.masterunkid = EST.skillcategoryunkid " & _
                       "            JOIN hrskill_master AS SK ON EST.skillunkid = SK.skillunkid " & _
                       "        WHERE 1 = 1 AND EST.skillstranunkid IN (SELECT skillstranunkid FROM hremp_app_skills_approval_tran WHERE isprocessed = 0 AND operationtypeid IN (2,3)) " & _
                       "    ) AS pInfo ON pInfo.skillstranunkid = hremp_app_skills_approval_tran.skillstranunkid AND pInfo.emp_app_unkid = hremp_app_skills_approval_tran.emp_app_unkid "
                '"    ,hremp_app_skills_approval_tran.transactiondate As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.approvalremark As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.isfinal As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.statusunkid As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.skillstranunkid As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.emp_app_unkid As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.skillcategoryunkid As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.skillunkid As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.description As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.description As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.isapplicant As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.isvoid As EmpId " & _
                '"    ,hremp_app_skills_approval_tran.isprocess As EmpId " & _

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If


                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then

                        'Gajanan [27-May-2019] -- Start              
                        'StrQ &= xUACQry
                        StrQ &= "LEFT " & xUACQry
                        'Gajanan [27-Ma y-2019] -- End
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE ISNULL(hremp_app_skills_approval_tran.isvoid,0) = 0  AND ISNULL(hremp_app_skills_approval_tran.isapplicant,0)=0 AND ISNULL(hremp_app_skills_approval_tran.isprocessed,0) = 0 AND ISNULL(hremp_app_skills_approval_tran.statusunkid,0) = 1 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If

                objDo.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information"))
                objDo.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 63, "Information Edited"))
                objDo.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 64, "Information Deleted"))


                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremp_app_skills_approval_tran) </purpose>
    Public Function Insert(ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnFromApproval As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim objSkillTran As New clsEmployee_Skill_Tran
        'Gajanan [17-April-2019] -- End

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
            objDataOperation.ClearParameters()

        If blnFromApproval = False Then
            If isExist(mintEmp_App_Unkid, mblnIsapplicant, mintOperationTypeId, mintSkillunkid, , objDataOperation) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Selected skill is already assigned to the employee. Please assign new skill.")
                Return False
            End If

        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            End If
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillstranunkid.ToString)
            objDataOperation.AddParameter("@emp_app_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmp_App_Unkid.ToString)
            objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillcategoryunkid.ToString)
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeUnkid)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)

            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            objDataOperation.AddParameter("@other_skillcategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_SkillCategory.ToString)
            objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_Skill.ToString)
            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillExpertiseunkid.ToString)
            'Pinkal (24-Sep-2020) -- End

            strQ = "INSERT INTO hremp_app_skills_approval_tran ( " & _
                      "  tranguid " & _
                      ", transactiondate " & _
                      ", mappingunkid " & _
                      ", approvalremark " & _
                      ", isfinal " & _
                      ", statusunkid " & _
                      ", skillstranunkid " & _
                      ", emp_app_unkid " & _
                      ", skillcategoryunkid " & _
                      ", skillunkid " & _
                      ", description " & _
                      ", isapplicant " & _
                      ", isvoid " & _
                      ", auditdatetime " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", ip " & _
                      ", host " & _
                      ", form_name " & _
                      ", isweb " & _
                      ", isprocessed" & _
                      ", loginemployeeunkid" & _
                      ", operationtypeid " & _
                      ", other_skillcategory " & _
                      ", other_skill " & _
                      ", skillexpertiseunkid " & _
                    ") VALUES (" & _
                      "  @tranguid " & _
                      ", @transactiondate " & _
                      ", @mappingunkid " & _
                      ", @approvalremark " & _
                      ", @isfinal " & _
                      ", @statusunkid " & _
                      ", @skillstranunkid " & _
                      ", @emp_app_unkid " & _
                      ", @skillcategoryunkid " & _
                      ", @skillunkid " & _
                      ", @description " & _
                      ", @isapplicant " & _
                      ", @isvoid " & _
                      ", GETDATE() " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ", @isprocessed" & _
                      ", @loginemployeeunkid" & _
                      ", @operationtypeid " & _
                      ", @other_skillcategory " & _
                      ", @other_skill " & _
                      ", @skillexpertiseunkid " & _
                    ") "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremp_app_skills_approval_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@skillstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillstranunkid.ToString)
            objDataOperation.AddParameter("@emp_app_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmp_App_Unkid.ToString)
            objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillcategoryunkid.ToString)
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.NChar, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditdatetime.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeUnkid)

            strQ = ""

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_app_skills_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal xVoidReason As String, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()


        Try
            Dim objEmployee_Skill_Tran As clsEmployee_Skill_Tran
            objEmployee_Skill_Tran = New clsEmployee_Skill_Tran

            objEmployee_Skill_Tran._Skillstranunkid() = intUnkid
            Me._Audittype = enAuditType.ADD
            Me._Description = objEmployee_Skill_Tran._Description
            Me._Isapplicant = objEmployee_Skill_Tran._Isapplicant
            Me._Emp_App_Unkid = objEmployee_Skill_Tran._Emp_App_Unkid
            Me._Skillcategoryunkid = objEmployee_Skill_Tran._Skillcategoryunkid
            Me._Skillunkid = objEmployee_Skill_Tran._Skillunkid
            Me._Isvoid = False
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Approvalremark = ""
            Me._Mappingunkid = 0
            Me._Isfinal = False
            Me._Skillstranunkid = intUnkid
            Me._Isprocessed = False
            Me._OperationTypeId = clsEmployeeDataApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            objEmployee_Skill_Tran._Voidreason = xVoidReason
            objEmployee_Skill_Tran._Voiddatetime = Now
            objEmployee_Skill_Tran._Isvoid = True
            objEmployee_Skill_Tran._ClientIP = Me._Ip
            objEmployee_Skill_Tran._FormName = Me._Form_Name
            objEmployee_Skill_Tran._HostName = Me._Host
            objEmployee_Skill_Tran._AuditUserId = Me._Audituserunkid
            objEmployee_Skill_Tran._Voiduserunkid = Me._Audituserunkid
            objEmployee_Skill_Tran._Loginemployeeunkid = -1
            'Gajanan [9-July-2019] -- End


            blnFlag = Insert(intCompanyId, objDataOperation)

            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then

                objEmployee_Skill_Tran._Voidloginemployeeunkid = -1
                If objEmployee_Skill_Tran.Delete(intUnkid, intCompanyId, Me._Audituserunkid, Now, xVoidReason, objDataOperation) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpAppId As Integer, _
                            ByVal blnIsApplicant As Boolean, _
                            ByVal xeOprType As clsEmployeeDataApproval.enOperationType, _
                            Optional ByVal intSkillUnkid As Integer = -1, _
                            Optional ByVal strtrangUid As String = "", _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = True, _
                            Optional ByRef intOperationType As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If
        'Gajanan [17-DEC-2018] -- End

        Try
            strQ = "SELECT " & _
                     "  tranguid " & _
                     ", transactiondate " & _
                     ", mappingunkid " & _
                     ", approvalremark " & _
                     ", isfinal " & _
                     ", statusunkid " & _
                     ", skillstranunkid " & _
                     ", emp_app_unkid " & _
                     ", skillcategoryunkid " & _
                     ", description " & _
                     ", isapplicant " & _
                     ", isvoid " & _
                     ", isprocessed " & _
                     ", skillunkid " & _
                     ", operationtypeid " & _
                    "FROM hremp_app_skills_approval_tran WHERE isvoid = 0 " & _
                    " AND statusunkid <> 3 "


            If blnIsApplicant = True Then
                strQ &= "and ISNULL(isapplicant,0) = 1 "
            Else
                strQ &= "and ISNULL(isapplicant,0) = 0 "
            End If

            If strtrangUid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid"
            End If

            If intEmpAppId > 0 Then
                strQ &= " AND emp_app_unkid = @emp_app_unkid "
            End If

            If intSkillUnkid > 0 Then
                strQ &= " AND skillunkid = @skillunkid "
            End If


            'Gajanan [10-June-2019] -- Start      
            'If blnPreventNewEntry Then
            '    strQ &= " AND ISNULL(hremp_app_skills_approval_tran.isprocessed,0)= 0 "
            'End If

                strQ &= " AND ISNULL(hremp_app_skills_approval_tran.isprocessed,0)= 0 "
            'Gajanan [10-June-2019] -- End

            objDataOperation.AddParameter("@emp_app_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpAppId)
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSkillUnkid)
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strtrangUid)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xeOprType)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                intOperationType = CInt(dsList.Tables(0).Rows(0)("operationtypeid"))
            End If



            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intSkillTranUnkid As Integer, ByVal intSkillUnkid As Integer, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "UPDATE hremp_app_skills_approval_tran SET " & _
                   " " & strColName & " = '" & intSkillTranUnkid & "' " & _
                   "WHERE isprocessed= 0 and skillunkid = @skillunkid AND emp_app_unkid = @emp_app_unkid "

            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@emp_app_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSkillUnkid)
            'Gajanan [31-May-2020] -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class
