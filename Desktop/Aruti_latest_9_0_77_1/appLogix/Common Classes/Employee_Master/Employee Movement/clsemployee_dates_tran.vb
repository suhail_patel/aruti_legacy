﻿'************************************************************************************************************************************
'Class Name :clsemployee_dates_tran.vb
'Purpose    :
'Date       :26-Mar-2015
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Xml
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsemployee_dates_tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_dates_tran"
    Private trd As Thread
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintDatestranunkid As Integer = 0
    Private mdtEffectivedate As Date = Nothing
    Private mintEmployeeunkid As Integer = 0
    Private mintRehiretranunkid As Integer = 0
    Private mdtDate1 As Date = Nothing
    Private mdtDate2 As Date = Nothing
    Private mintChangereasonunkid As Integer = 0
    Private mblnIsfromemployee As Boolean = False
    Private mintDatetypeunkid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mintStatusunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty
    Private mintActionreasonunkid As Integer = 0
    Private mblnIsexclude_payroll As Boolean = False
    Private mblnIsConfirmed As Boolean = False
    Private mstrOtherreason As String = String.Empty
    'S.SANDEEP [29 APR 2015] -- START
    Private mblnInsertNullTerminationDate As Boolean = False
    'S.SANDEEP [29 APR 2015] -- END
    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Private mintLeaveissueunkid As Integer = 0
    'Sohail (21 Oct 2019) -- End


 'Pinkal (07-Mar-2020) -- Start
    'Enhancement - Changes Related to Payroll UAT for NMB.
    Private mdtActualDate As DateTime = Nothing
    'Pinkal (07-Mar-2020) -- End


    'Gajanan [18-May-2020] -- Start
    'Enhancement:Discipline Module Enhancement NMB
    Private mintDisciplineunkid As Integer = -1
    'Gajanan [18-May-2020] -- End

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set datestranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Datestranunkid(Optional ByVal objDataOper As clsDataOperation = Nothing) As Integer 'S.SANDEEP [25 OCT 2016] -- START {objDataOperation} -- END
        Get
            Return mintDatestranunkid
        End Get
        Set(ByVal value As Integer)
            mintDatestranunkid = value
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            'Call GetData()
            Call GetData(objDataOper)
            'S.SANDEEP [25 OCT 2016] -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set date1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Date1() As Date
        Get
            Return mdtDate1
        End Get
        Set(ByVal value As Date)
            mdtDate1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set date2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Date2() As Date
        Get
            Return mdtDate2
        End Get
        Set(ByVal value As Date)
            mdtDate2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfromemployee
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isfromemployee() As Boolean
        Get
            Return mblnIsfromemployee
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromemployee = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set datetypeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Datetypeunkid() As Integer
        Get
            Return mintDatetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintDatetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set actionreasonunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Actionreasonunkid() As Integer
        Get
            Return mintActionreasonunkid
        End Get
        Set(ByVal value As Integer)
            mintActionreasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isexclude payroll
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isexclude_payroll() As Boolean
        Get
            Return mblnIsexclude_payroll
        End Get
        Set(ByVal value As Boolean)
            mblnIsexclude_payroll = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isconfirmed
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isconfirmed() As Boolean
        Get
            Return mblnIsconfirmed
        End Get
        Set(ByVal value As Boolean)
            mblnIsconfirmed = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otherreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Otherreason() As String
        Get
            Return mstrOtherreason
        End Get
        Set(ByVal value As String)
            mstrOtherreason = value
        End Set
    End Property

    'S.SANDEEP [29 APR 2015] -- START
    Public WriteOnly Property _InsertNullTerminationDate() As Boolean
        Set(ByVal value As Boolean)
            mblnInsertNullTerminationDate = value
        End Set
    End Property
    'S.SANDEEP [29 APR 2015] -- END

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Public Property _Leaveissueunkid() As Integer
        Get
            Return mintLeaveissueunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveissueunkid = value
        End Set
    End Property
    'Sohail (21 Oct 2019) -- End


    'Pinkal (07-Mar-2020) -- Start
    'Enhancement - Changes Related to Payroll UAT for NMB.

    ''' <summary>
    ''' Purpose: Get or Set ActualDate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ActualDate() As Date
        Get
            Return mdtActualDate
        End Get
        Set(ByVal value As Date)
            mdtActualDate = value
        End Set
    End Property

    'Pinkal (07-Mar-2020) -- End


    'Gajanan [18-May-2020] -- Start
    'Enhancement:Discipline Module Enhancement NMB
    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplineunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineunkid = value
        End Set
    End Property
    'Gajanan [18-May-2020] -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [25 OCT 2016] -- START
        'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
        objDataOperation.ClearParameters()
        'S.SANDEEP [25 OCT 2016] -- END

        Try
            strQ = "SELECT " & _
              "  datestranunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", date1 " & _
              ", date2 " & _
              ", changereasonunkid " & _
              ", isfromemployee " & _
              ", datetypeunkid " & _
              ", userunkid " & _
              ", statusunkid " & _
              ", actionreasonunkid " & _
              ", isexclude_payroll " & _
              ", isconfirmed " & _
              ", otherreason " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(leaveissueunkid, 0) AS leaveissueunkid " & _
                      ", actualdate " & _
              ", disciplinefileunkid " & _
             "FROM hremployee_dates_tran " & _
             "WHERE datestranunkid = @datestranunkid "

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[actualdate]

            'Sohail (21 Oct 2019) - [leaveissueunkid]

            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatestranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDatestranunkid = CInt(dtRow.Item("datestranunkid"))
                mdtEffectivedate = dtRow.Item("effectivedate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
                'S.SANDEEP [29 APR 2015] -- START
                'mdtDate1 = dtRow.Item("date1")
                If IsDBNull(dtRow.Item("date1")) = False Then
                mdtDate1 = dtRow.Item("date1")
                Else
                    mdtDate1 = Nothing
                End If
                'S.SANDEEP [29 APR 2015] -- END
                If IsDBNull(dtRow.Item("date2")) = False Then
                    mdtDate2 = dtRow.Item("date2")
                Else
                    mdtDate2 = Nothing
                End If
                mintChangereasonunkid = CInt(dtRow.Item("changereasonunkid"))
                mblnIsfromemployee = CBool(dtRow.Item("isfromemployee"))
                mintDatetypeunkid = CInt(dtRow.Item("datetypeunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintActionreasonunkid = dtRow.Item("actionreasonunkid")
                mblnIsexclude_payroll = dtRow.Item("isexclude_payroll")
                mblnIsConfirmed = CBool(dtRow.Item("isconfirmed"))
                mstrOtherreason = dtRow.Item("otherreason")
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                mintLeaveissueunkid = CInt(dtRow.Item("leaveissueunkid"))
                'Sohail (21 Oct 2019) -- End


                'Pinkal (07-Mar-2020) -- Start
                'Enhancement - Changes Related to Payroll UAT for NMB.
                If IsDBNull(dtRow.Item("actualdate")) = False Then
                    mdtActualDate = dtRow.Item("actualdate")
                Else
                    mdtActualDate = Nothing
                End If
                'Pinkal (07-Mar-2020) -- End


                'Gajanan [18-May-2020] -- Start
                'Enhancement:Discipline Module Enhancement NMB
                mintDisciplineunkid = CInt(dtRow.Item("disciplinefileunkid"))
                'Gajanan [18-May-2020] -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            ByVal eDateTran As enEmp_Dates_Transaction, _
                            ByVal xFinYearStartDate As Date, _
                            Optional ByVal xEmployeeUnkid As Integer = 0) As DataSet 'S.SANDEEP [16 Jan 2016] -- START -- END

        'Public Function GetList(ByVal strTableName As String, ByVal eDateTran As enEmp_Dates_Transaction, Optional ByVal xEmployeeUnkid As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            Dim xMasterType As Integer = 0
            Select Case eDateTran
                Case enEmp_Dates_Transaction.DT_PROBATION
                    xMasterType = clsCommon_Master.enCommonMaster.PROBATION
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    xMasterType = clsCommon_Master.enCommonMaster.CONFIRMATION
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    xMasterType = clsCommon_Master.enCommonMaster.SUSPENSION
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    xMasterType = clsCommon_Master.enCommonMaster.TERMINATION
                Case enEmp_Dates_Transaction.DT_REHIRE
                    xMasterType = clsCommon_Master.enCommonMaster.RE_HIRE
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    xMasterType = clsCommon_Master.enCommonMaster.RETIREMENTS
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    xMasterType = clsCommon_Master.enCommonMaster.EXEMPTION
                    'Sohail (21 Oct 2019) -- End
            End Select


            strQ = "SELECT " & _
                   "  hremployee_dates_tran.datestranunkid " & _
                   ", hremployee_dates_tran.effectivedate " & _
                   ", hremployee_dates_tran.employeeunkid " & _
                   ", hremployee_dates_tran.rehiretranunkid " & _
                   ", hremployee_dates_tran.date1 " & _
                   ", hremployee_dates_tran.date2 " & _
                   ", hremployee_dates_tran.changereasonunkid " & _
                   ", hremployee_dates_tran.isfromemployee " & _
                   ", hremployee_dates_tran.datetypeunkid " & _
                   ", hremployee_dates_tran.userunkid " & _
                   ", hremployee_dates_tran.statusunkid " & _
                   ", hremployee_dates_tran.actionreasonunkid " & _
                   ", hremployee_dates_tran.isexclude_payroll " & _
                   ", hremployee_dates_tran.isconfirmed " & _
                   ", hremployee_dates_tran.otherreason " & _
                   ", hremployee_dates_tran.isvoid " & _
                   ", hremployee_dates_tran.voiduserunkid " & _
                   ", hremployee_dates_tran.voiddatetime " & _
                   ", hremployee_dates_tran.voidreason " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", CONVERT(CHAR(8),hremployee_dates_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", CONVERT(CHAR(8),hremployee_dates_tran.date1,112) AS edate1 " & _
                   ", CONVERT(CHAR(8),hremployee_dates_tran.date2,112) AS edate2 " & _
                   ", '' AS EffDate " & _
                   ", '' AS dDate1 " & _
                   ", '' AS dDate2 " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'')  WHEN hremployee_dates_tran.leaveissueunkid > 0 THEN ISNULL(lvleavetype_master.leavename, '') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                   ", CASE WHEN CONVERT(CHAR(8),hremployee_dates_tran.date1,112) < '" & eZeeDate.convertDate(xFinYearStartDate).ToString() & "' THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS allowopr " & _
                   ", ISNULL(hremployee_dates_tran.leaveissueunkid, 0) AS leaveissueunkid " & _
                   ", CONVERT(CHAR(8),hremployee_dates_tran.actualdate,112) AS acdate " & _
                   ", hremployee_dates_tran.actualdate " & _
                   "FROM hremployee_dates_tran " & _
                   "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_dates_tran.employeeunkid " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_dates_tran.changereasonunkid AND cfcommon_master.mastertype = " & xMasterType & _
                   "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_dates_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "    LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = hremployee_dates_tran.leaveissueunkid " & _
                   "    LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                   " WHERE hremployee_dates_tran.isvoid = 0 AND hremployee_dates_tran.datetypeunkid = " & eDateTran


            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[" CONVERT(CHAR(8),hremployee_dates_tran.actualdate,112) AS adate , hremployee_dates_tran.actualdate "]


            'Sohail (21 Oct 2019) - [leaveissueunkid, LEFT JOIN lvleaveIssue_master, LEFT JOIN lvleavetype_master]
            'S.SANDEEP [16 Jan 2016] -- START
            '", CASE WHEN CONVERT(CHAR(8),hremployee_dates_tran.date1,112) < '" & eZeeDate.convertDate(xFinYearStartDate).ToString() & "' THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS allowopr " & _
            '------------ > ADDED
            'S.SANDEEP [16 Jan 2016] -- END


            If eDateTran = enEmp_Dates_Transaction.DT_TERMINATION Then
                strQ &= " AND hremployee_dates_tran.date1 IS NOT NULL "
            End If

            'S.SANDEEP [28-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002032}
            'If xEmployeeUnkid Then
            '    strQ &= " AND hremployee_dates_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            'End If

                strQ &= " AND hremployee_dates_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            'S.SANDEEP [28-Feb-2018] -- END

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_dates_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
                'S.SANDEEP [29 APR 2015] -- START
                'xRow("dDate1") = eZeeDate.convertDate(xRow.Item("edate1").ToString).ToShortDateString
                If xRow.Item("edate1").ToString.Trim.Length > 0 Then
                xRow("dDate1") = eZeeDate.convertDate(xRow.Item("edate1").ToString).ToShortDateString
                End If
                'S.SANDEEP [29 APR 2015] -- END
                If xRow.Item("edate2").ToString.Trim.Length > 0 Then
                    xRow("dDate2") = eZeeDate.convertDate(xRow.Item("edate2").ToString).ToShortDateString
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_dates_tran) </purpose>
    Public Function Insert(ByVal mblnCreateADUserFromEmpMst As Boolean, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [10-MAY-2017] -- START {intCompanyId}-- END

        'Pinkal (18-Aug-2018) --  'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]


        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If


        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmpMst Then
            Dim strcQ As String = ""
            strcQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strcQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End


        If isExist(mdtEffectivedate, Nothing, Nothing, mintDatetypeunkid, mintEmployeeunkid, , objDataOperation) Then
            Select Case mintDatetypeunkid
                Case enEmp_Dates_Transaction.DT_PROBATION
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, porbation information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    mstrMessage = Language.getMessage(mstrModuleName, 16, "Sorry, confirmation information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, suspension information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, termination information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_REHIRE
                    mstrMessage = Language.getMessage(mstrModuleName, 17, "Sorry, re-hire information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, retirement information is already present for the selected effective date.")
                    'S.SANDEEP [04 APR 2015] -- START
                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                    mstrMessage = Language.getMessage(mstrModuleName, 19, "Sorry, appointment information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                    mstrMessage = Language.getMessage(mstrModuleName, 20, "Sorry, birthdate information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                    mstrMessage = Language.getMessage(mstrModuleName, 21, "Sorry, first appointment date information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                    mstrMessage = Language.getMessage(mstrModuleName, 22, "Sorry, anniversary information is already present for the selected effective date.")
                    'S.SANDEEP [04 APR 2015] -- END
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    mstrMessage = Language.getMessage(mstrModuleName, 27, "Sorry, exemption information is already present for the selected effective date.")
                    'Sohail (21 Oct 2019) -- End
            End Select
            Return False
        End If

        If isExist(Nothing, mdtDate1, mdtDate2, mintDatetypeunkid, mintEmployeeunkid, , objDataOperation) Then
            Select Case mintDatetypeunkid
                Case enEmp_Dates_Transaction.DT_PROBATION
                    mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, This probation date range is already defined. Please define new date range for this probation.")
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    mstrMessage = Language.getMessage(mstrModuleName, 15, "Sorry, This confirmation information is already present.")
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension.")
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    'S.SANDEEP [29 APR 2015] -- START
                    'mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, This termination information is already present.")
                    If mblnInsertNullTerminationDate = True Then
                        GoTo lbl
                    Else
                    mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, This termination information is already present.")
                    End If
                    'S.SANDEEP [29 APR 2015] -- END
                Case enEmp_Dates_Transaction.DT_REHIRE
                    mstrMessage = Language.getMessage(mstrModuleName, 18, "Sorry, This re-hire information is already present.")
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, This retirement information is already present.")
                    'S.SANDEEP [04 APR 2015] -- START
                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                    mstrMessage = Language.getMessage(mstrModuleName, 23, "Sorry, This appointment date information is already present.")
                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                    mstrMessage = Language.getMessage(mstrModuleName, 24, "Sorry, This birthdate date information is already present.")
                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                    mstrMessage = Language.getMessage(mstrModuleName, 25, "Sorry, This first appointment date information is already present.")
                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                    mstrMessage = Language.getMessage(mstrModuleName, 26, "Sorry, This anniversary information is already present.")
                    'S.SANDEEP [04 APR 2015] -- END
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    mstrMessage = Language.getMessage(mstrModuleName, 28, "Sorry, This exemption information is already present.")
                    'Sohail (21 Oct 2019) -- End
            End Select
            Return False
        End If

        'S.SANDEEP [29 APR 2015] -- START
lbl:
        'S.SANDEEP [29 APR 2015] -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            'S.SANDEEP [29 APR 2015] -- START
            'objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate1)
            If mdtDate1 <> Nothing Then
            objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate1)
            Else
                objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [29 APR 2015] -- END
            If mdtDate2 <> Nothing Then
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate2)
            Else
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@datetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatetypeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionreasonunkid.ToString)
            objDataOperation.AddParameter("@isexclude_payroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexclude_payroll.ToString)
            objDataOperation.AddParameter("@isconfirmed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsConfirmed.ToString)
            objDataOperation.AddParameter("@otherreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrOtherreason)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
            'Sohail (21 Oct 2019) -- End

            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If mdtActualDate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualDate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (07-Mar-2020) -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineunkid)
            'Gajanan [18-May-2020] -- End


            strQ = "INSERT INTO hremployee_dates_tran ( " & _
                       "  effectivedate " & _
                       ", employeeunkid " & _
                       ", rehiretranunkid " & _
                       ", date1 " & _
                       ", date2 " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", datetypeunkid " & _
                       ", userunkid " & _
                       ", statusunkid " & _
                       ", actionreasonunkid " & _
                       ", isexclude_payroll " & _
                       ", isconfirmed " & _
                       ", otherreason " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                       ", leaveissueunkid " & _
                       ", actualdate " & _
                       ", disciplinefileunkid " & _
                   ") VALUES (" & _
                       "  @effectivedate " & _
                       ", @employeeunkid " & _
                       ", @rehiretranunkid " & _
                       ", @date1 " & _
                       ", @date2 " & _
                       ", @changereasonunkid " & _
                       ", @isfromemployee " & _
                       ", @datetypeunkid " & _
                       ", @userunkid " & _
                       ", @statusunkid " & _
                       ", @actionreasonunkid " & _
                       ", @isexclude_payroll " & _
                       ", @isconfirmed " & _
                       ", @otherreason " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                       ", @leaveissueunkid " & _
                       ", @actualdate " & _
                       ", @disciplinefileunkid " & _
                   "); SELECT @@identity"

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[@actualdate]

            'Sohail (21 Oct 2019) - [leaveissueunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDatestranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForDates(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (18-Aug-2018) -- End


            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, mintDatetypeunkid, intCompanyId, mintEmployeeunkid, mdtDate1, mdtDate2) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            'S.SANDEEP |13-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
            Select Case mintDatetypeunkid
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    Call BlockExitStaffInCBS(mintEmployeeunkid, IIf(mdtDate1 < mdtDate2, mdtDate1, mdtDate2), mdtCurrentDate, intCompanyId, "D", objDataOperation, mintUserunkid, IIf(mstrClientIP.Trim.Length > 0, mintEmployeeunkid, 0))
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    Call BlockExitStaffInCBS(mintEmployeeunkid, mdtDate1, mdtCurrentDate, intCompanyId, "D", objDataOperation, mintUserunkid, IIf(mstrClientIP.Trim.Length > 0, mintEmployeeunkid, 0))
            End Select
            'S.SANDEEP |13-DEC-2019| -- END

            'Pinkal (07-Dec-2019) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (07-Dec-2019) -- End

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (18-Aug-2018) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_dates_tran) </purpose>
    Public Function Update(ByVal mblnCreateADUserFromEmpMst As Boolean, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [10-MAY-2017] -- START {intCompanyId}-- END
        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273]. [ByVal mblnCreateADUserFromEmpMst As Boolean] 

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmpMst Then
            Dim strcQ As String = ""
            strcQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strcQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End

        If isExist(mdtEffectivedate, Nothing, Nothing, mintDatetypeunkid, mintEmployeeunkid, mintDatestranunkid, objDataOperation) Then
            Select Case mintDatetypeunkid
                Case enEmp_Dates_Transaction.DT_PROBATION
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, porbation information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    mstrMessage = Language.getMessage(mstrModuleName, 16, "Sorry, confirmation information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, suspension information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, termination information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_REHIRE
                    mstrMessage = Language.getMessage(mstrModuleName, 17, "Sorry, re-hire information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, retirement information is already present for the selected effective date.")
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    mstrMessage = Language.getMessage(mstrModuleName, 27, "Sorry, exemption information is already present for the selected effective date.")
                    'Sohail (21 Oct 2019) -- End
            End Select
            Return False
        End If
        If isExist(Nothing, mdtDate1, mdtDate2, mintDatetypeunkid, mintEmployeeunkid, mintDatestranunkid, objDataOperation) Then
            Select Case mintDatetypeunkid
                Case enEmp_Dates_Transaction.DT_PROBATION
                    mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, This probation date range is already defined. Please define new date range for this probation.")
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    mstrMessage = Language.getMessage(mstrModuleName, 15, "Sorry, This confirmation information is already present.")
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension.")
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, This termination information is already present.")
                Case enEmp_Dates_Transaction.DT_REHIRE
                    mstrMessage = Language.getMessage(mstrModuleName, 18, "Sorry, This re-hire information is already present.")
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, This retirement information is already present.")
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    mstrMessage = Language.getMessage(mstrModuleName, 28, "Sorry, This exemption information is already present.")
                    'Sohail (21 Oct 2019) -- End
            End Select
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatestranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate1)
            If mdtDate2 <> Nothing Then
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate2)
            Else
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@datetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatetypeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionreasonunkid.ToString)
            objDataOperation.AddParameter("@isexclude_payroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexclude_payroll.ToString)
            objDataOperation.AddParameter("@isconfirmed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsConfirmed.ToString)
            objDataOperation.AddParameter("@otherreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrOtherreason)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
            'Sohail (21 Oct 2019) -- End

            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If mdtActualDate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualDate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (07-Mar-2020) -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineunkid)
            'Gajanan [18-May-2020] -- End


            strQ = "UPDATE hremployee_dates_tran SET " & _
                   "  effectivedate = @effectivedate" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", rehiretranunkid = @rehiretranunkid" & _
                   ", date1 = @date1" & _
                   ", date2 = @date2" & _
                   ", changereasonunkid = @changereasonunkid" & _
                   ", isfromemployee = @isfromemployee" & _
                   ", datetypeunkid = @datetypeunkid" & _
                   ", userunkid = @userunkid" & _
                   ", statusunkid = @statusunkid" & _
                   ", actionreasonunkid = @actionreasonunkid " & _
                   ", isexclude_payroll = @isexclude_payroll " & _
                   ", isconfirmed = @isconfirmed " & _
                   ", otherreason = @otherreason " & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   ", leaveissueunkid = @leaveissueunkid " & _
                   ", actualdate = @actualdate " & _
                   ", disciplinefileunkid = @disciplinefileunkid " & _
                   "WHERE datestranunkid = @datestranunkid "

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[  ", actualdate = @actualdate " & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForDates(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (18-Aug-2018) -- End


            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, mintDatetypeunkid, intCompanyId, mintEmployeeunkid, mdtDate1, mdtDate2) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            'S.SANDEEP |13-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
            Select Case mintDatetypeunkid
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    Call BlockExitStaffInCBS(mintEmployeeunkid, IIf(mdtDate1 < mdtDate2, mdtDate1, mdtDate2), mdtCurrentDate, intCompanyId, "D", objDataOperation, mintUserunkid, IIf(mstrClientIP.Trim.Length > 0, mintEmployeeunkid, 0))
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    Call BlockExitStaffInCBS(mintEmployeeunkid, mdtDate1, mdtCurrentDate, intCompanyId, "D", objDataOperation, mintUserunkid, IIf(mstrClientIP.Trim.Length > 0, mintEmployeeunkid, 0))
            End Select
            'S.SANDEEP |13-DEC-2019| -- END

            'Pinkal (07-Dec-2019) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (07-Dec-2019) -- End
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (18-Aug-2018) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_dates_tran) </purpose>
    Public Function Delete(ByVal mblnCreateADUserFromEmpMst As Boolean, ByVal intUnkid As Integer, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [01 JUN 2016] -- START -- END
        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]
        'S.SANDEEP [10-MAY-2017] -- START {intCompanyId} -- END

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()


        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmpMst Then
            strQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End

        Try
            strQ = "UPDATE hremployee_dates_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE datestranunkid = @datestranunkid "

            'S.SANDEEP [04 APR 2015] -- START
            'objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatestranunkid)
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'S.SANDEEP [04 APR 2015] -- END
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintDatestranunkid = intUnkid
            Call GetData(objDataOperation)

            If InsertAuditTrailForDates(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (18-Aug-2018) -- End


            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, mintDatetypeunkid, intCompanyId, mintEmployeeunkid, mdtDate1, mdtDate2) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If


            'Pinkal (07-Dec-2019) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (07-Dec-2019) -- End


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst AndAlso (mintDatetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (18-Aug-2018) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal xEffDate As DateTime, _
                            ByVal xDate1 As DateTime, _
                            ByVal xDate2 As DateTime, _
                            ByVal xDateTran As Integer, _
                            ByVal xEmployeeId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
                   "  datestranunkid " & _
                   ", effectivedate " & _
                   ", employeeunkid " & _
                   ", rehiretranunkid " & _
                   ", date1 " & _
                   ", date2 " & _
                   ", changereasonunkid " & _
                   ", isfromemployee " & _
                   ", datetypeunkid " & _
                   ", userunkid " & _
                   ", statusunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", actualdate " & _
                   "FROM hremployee_dates_tran " & _
                   "WHERE isvoid = 0 AND datetypeunkid = " & xDateTran & " "

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[actualdate]

            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If

            If intUnkid > 0 Then
                strQ &= " AND datestranunkid <> @datestranunkid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            Select Case xDateTran
                Case enEmp_Dates_Transaction.DT_PROBATION
                    'S.SANDEEP [08 APR 2015] -- START
                    'strQ &= "  AND " & _
                    '        "( " & _
                    '        "  (@xDate1 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112) " & _
                    '        "       OR @xDate2 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112)) " & _
                    '        "  OR " & _
                    '        "  (CONVERT(CHAR(8),date1,112) BETWEEN @xDate1 and @xDate2 " & _
                    '        "   AND CONVERT(CHAR(8),date2,112) BETWEEN @xDate1 and @xDate2) " & _
                    '        ") "
                    'objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    'objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))

                    If xDate1 <> Nothing AndAlso xDate2 <> Nothing Then
                        strQ &= "  AND " & _
                                "( " & _
                                "  (@xDate1 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112) " & _
                                "       OR @xDate2 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112)) " & _
                                "  OR " & _
                                "  (CONVERT(CHAR(8),date1,112) BETWEEN @xDate1 and @xDate2 " & _
                                "   AND CONVERT(CHAR(8),date2,112) BETWEEN @xDate1 and @xDate2) " & _
                                ") "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                        objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))
                    End If
                    'S.SANDEEP [08 APR 2015] -- END
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    'S.SANDEEP [08 APR 2015] -- START
                    'strQ &= "  AND " & _
                    '        "( " & _
                    '        "  (@xDate1 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112) " & _
                    '        "       OR @xDate2 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112)) " & _
                    '        "  OR " & _
                    '        "  (CONVERT(CHAR(8),date1,112) BETWEEN @xDate1 and @xDate2 " & _
                    '        "   AND CONVERT(CHAR(8),date2,112) BETWEEN @xDate1 and @xDate2) " & _
                    '        ") "
                    'objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    'objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))

                    If xDate1 <> Nothing AndAlso xDate2 <> Nothing Then
                        strQ &= "  AND " & _
                                "( " & _
                                "  (@xDate1 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112) " & _
                                "       OR @xDate2 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112)) " & _
                                "  OR " & _
                                "  (CONVERT(CHAR(8),date1,112) BETWEEN @xDate1 and @xDate2 " & _
                                "   AND CONVERT(CHAR(8),date2,112) BETWEEN @xDate1 and @xDate2) " & _
                                "   OR date2 IS NULL " & _
                                ") "
                        'S.SANDEEP |02-MAR-2020| -- START
                        'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                        '--------------------- ADDED {OR date2 IS NULL} 
                        'S.SANDEEP |02-MAR-2020| -- END

                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                        objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))


                        'Pinkal (07-Dec-2020) -- Start
                        'Problem in Suspension NMB - Solved Problem when User select only from date and save data for suspension when to date is not decided yet.

                    Else

                        If xDate1 <> Nothing Then
                            strQ &= "  AND " & _
                             "( " & _
                             "  @xDate1 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112) " & _
                             "   OR date2 IS NULL " & _
                             ") "
                            objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))

                        ElseIf xDate2 <> Nothing Then
                            strQ &= "  AND " & _
                             "( " & _
                             "  @xDate2 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112) " & _
                             "   OR date1 IS NULL " & _
                             ") "

                            objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))
                        End If

                        'Pinkal (07-Dec-2020) -- End

                    End If
                    'S.SANDEEP [08 APR 2015] -- END

                Case enEmp_Dates_Transaction.DT_TERMINATION
                    If xDate1 <> Nothing AndAlso xDate2 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1  AND  CONVERT(CHAR(8),date2,112) = @xDate2 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                        objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))
                    End If
                Case enEmp_Dates_Transaction.DT_REHIRE
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                    'S.SANDEEP [04 APR 2015] -- START
                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                    'S.SANDEEP [04 APR 2015] -- END
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8), date1, 112) >= @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                    If xDate2 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8), date1, 112) <= @xDate2 "
                        objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))
                    End If
                    'Sohail (21 Oct 2019) -- End
            End Select

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Select Case xDateTran
                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE, enEmp_Dates_Transaction.DT_BIRTH_DATE, enEmp_Dates_Transaction.DT_FIRST_APP_DATE, enEmp_Dates_Transaction.DT_MARRIGE_DATE
                    dsList.Tables(0).Rows.Clear()
            End Select

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Private Function InsertAuditTrailForDates(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            StrQ = "INSERT INTO athremployee_dates_tran ( " & _
                       "  datestranunkid " & _
                       ", effectivedate " & _
                       ", employeeunkid " & _
                       ", rehiretranunkid " & _
                       ", date1 " & _
                       ", date2 " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", datetypeunkid " & _
                       ", statusunkid " & _
                       ", actionreasonunkid " & _
                       ", isexclude_payroll " & _
                       ", isconfirmed " & _
                       ", otherreason " & _
                       ", leaveissueunkid " & _
                       ", actualdate " & _
                       ", disciplinefileunkid " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb" & _
                   ") VALUES (" & _
                       "  @datestranunkid " & _
                       ", @effectivedate " & _
                       ", @employeeunkid " & _
                       ", @rehiretranunkid " & _
                       ", @date1 " & _
                       ", @date2 " & _
                       ", @changereasonunkid " & _
                       ", @isfromemployee " & _
                       ", @datetypeunkid " & _
                       ", @statusunkid " & _
                       ", @actionreasonunkid " & _
                       ", @isexclude_payroll " & _
                       ", @isconfirmed " & _
                       ", @otherreason " & _
                       ", @leaveissueunkid " & _
                       ", @actualdate " & _
                       ", @disciplinefileunkid " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", @isweb" & _
                   ") "

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[@actualdate ]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatestranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            'S.SANDEEP [29 APR 2015] -- START
            'objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate1)
            If mdtDate1 <> Nothing Then
            objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate1)
            Else
                objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [29 APR 2015] -- END
            If mdtDate2 <> Nothing Then
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate2)
            Else
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@datetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatetypeunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionreasonunkid.ToString)
            objDataOperation.AddParameter("@isexclude_payroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexclude_payroll.ToString)
            objDataOperation.AddParameter("@isconfirmed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsConfirmed.ToString)
            objDataOperation.AddParameter("@otherreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrOtherreason)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
            'Sohail (21 Oct 2019) -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineunkid)

            'Gajanan [18-May-2020] -- End

            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If mdtActualDate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualDate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (07-Mar-2020) -- End


            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailsForDates; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Get_Current_Dates(ByVal xDate As Date, ByVal xDateTypeId As Integer, Optional ByVal xEmployeeId As Integer = 0 _
                                                    , Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal mstrFilter As String = "") As DataSet


        'Pinkal (07-Jun-2019) --  'Enhancement - NMB FINAL LEAVE UAT CHANGES.[Optional ByVal mstrFilter As String = ""]

        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[Optional ByVal objDOperation As clsDataOperation = Nothing]

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mdtAppDate As String = ""

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim objDo As clsDataOperation
        'Pinkal (18-Aug-2018) -- End

        Try

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'Using objDo As New clsDataOperation


            If objDataOperation Is Nothing Then
                objDo = New clsDataOperation
            Else
                objDo = objDataOperation
            End If
            objDo.ClearParameters()

            'Pinkal (18-Aug-2018) -- End



                If xEmployeeId > 0 Then
                    StrQ = "SELECT " & _
                           "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                           "FROM hremployee_master " & _
                           "WHERE employeeunkid = '" & xEmployeeId & "' "

                    objDo.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    End If

                    mdtAppDate = objDo.GetParameterValue("@ADate")
                End If

                StrQ = "SELECT " & _
                       "     effectivedate " & _
                       "    ,employeeunkid " & _
                       "    ,date1 " & _
                       "    ,date2 " & _
                       "    ,ddate1 " & _
                       "    ,ddate2 " & _
                       "    ,actionreasonunkid " & _
                       "    ,isexclude_payroll " & _
                       "    ,isconfirmed " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         effectivedate " & _
                       "        ,employeeunkid " & _
                       "        ,date1 " & _
                       "        ,date2 " & _
                       "        ,CONVERT(CHAR(8),date1,112) AS ddate1 " & _
                       "        ,CONVERT(CHAR(8),date2,112) AS ddate2 " & _
                       "        ,actionreasonunkid " & _
                       "        ,isexclude_payroll " & _
                       "        ,isconfirmed " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                   "        ,actualdate " & _
                       "    FROM hremployee_dates_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effdate " & _
                       "        AND datetypeunkid = '" & xDateTypeId & "' "


            'Pinkal (07-Mar-2020) -- 'Enhancement - Changes Related to Payroll UAT for NMB.[actualdate]

                'S.SANDEEP [29 APR 2015] -- START

                'S.SANDEEP [21-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {#ARUTI-57}
                'If xDateTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                '    StrQ &= " AND date1 IS NOT NULL "
                'End If
                'S.SANDEEP [21-Mar-2018] -- END

                'S.SANDEEP [29 APR 2015] -- END

                If xEmployeeId > 0 Then
                    StrQ &= " AND employeeunkid = '" & xEmployeeId & "' "
                End If
                StrQ &= ") AS CD WHERE 1 = 1 AND CD.xNo = 1 "

                'S.SANDEEP [07 APR 2015] -- START
                If mdtAppDate.Trim.Length > 0 Then
                    StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) >= '" & mdtAppDate & "' "
                End If
                'S.SANDEEP [07 APR 2015] -- END


            'Pinkal (07-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If mstrFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilter
            End If
            'Pinkal (07-Jun-2019) -- End


                objDo.AddParameter("@effdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'End Using
            'Pinkal (18-Aug-2018) -- End
        Catch ex As Exception

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If objDataOperation Is Nothing Then objDo = Nothing
            'Pinkal (18-Aug-2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Get_Current_Dates; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    'Sohail (18 Dec 2019) -- Start
    'Mukuba University # 0004115: Option for EOC to affect number of installments on loans.
    Public Function GetEmployeeEndDates(ByVal xPeriodEnd As Date _
                                      , Optional ByVal strEmployeeIDs As String = "" _
                                      , Optional ByVal strFilter As String = "" _
                                      , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                      ) As DataSet


        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mdtAppDate As String = ""

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT  hremployee_master.employeeunkid " & _
                        ", hremployee_master.employeecode " & _
                        ", hremployee_master.appointeddate " & _
                   "INTO #TableEmp " & _
                   "FROM    hremployee_master " & _
                   "WHERE hremployee_master.isactive = 1 "

            If strEmployeeIDs.Trim <> "" Then
                StrQ &= " AND hremployee_master.employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            StrQ &= "SELECT  #TableEmp.employeeunkid " & _
                            ", #TableEmp.employeecode " & _
                            ", CONVERT(CHAR(8), #TableEmp.appointeddate, 112) AS appointeddate " & _
                            ", ETERM.termination_from_date " & _
                            ", ERET.termination_to_date " & _
                            ", ETERM.empl_enddate " & _
                            ", '' AS finalenddate " & _
                    "FROM   #TableEmp " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "		 TERM.TEEmpId " & _
                            "		,TERM.empl_enddate " & _
                            "		,TERM.termination_from_date " & _
                            "		,TERM.TEfDt " & _
                            "		,TERM.isexclude_payroll " & _
                            "		,TERM.TR_REASON " & _
                            "		,TERM.changereasonunkid " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            TRM.employeeunkid AS TEEmpId " & _
                            "           ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                            "           ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                            "           ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                            "           ,TRM.isexclude_payroll " & _
                            "			,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                            "			,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                            "           ,TRM.changereasonunkid " & _
                            "       FROM hremployee_dates_tran AS TRM " & _
                            "			LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                            "			LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                            "           RIGHT JOIN #TableEmp ON #TableEmp.employeeunkid =  TRM.employeeunkid " & _
                            "		WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            "   ) AS TERM WHERE TERM.Rno = 1 " & _
                            ") AS ETERM ON ETERM.TEEmpId = #TableEmp.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8), #TableEmp.appointeddate,112) " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "		 RET.REmpId " & _
                            "		,RET.termination_to_date " & _
                            "		,RET.REfDt " & _
                            "		,RET.RET_REASON " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            RTD.employeeunkid AS REmpId " & _
                            "           ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                            "           ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                            "			,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                            "       FROM hremployee_dates_tran AS RTD " & _
                            "			LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                            "			LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                            "           RIGHT JOIN #TableEmp ON #TableEmp.employeeunkid =  RTD.employeeunkid " & _
                            "		WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            "   ) AS RET WHERE RET.Rno = 1 " & _
                            ") AS ERET ON ERET.REmpId = #TableEmp.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8), #TableEmp.appointeddate,112) " & _
                            "WHERE   1 = 1 "

            StrQ &= " DROP TABLE #TableEmp "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Dim dtAppointed As Date
            Dim dtTerminationFrom As Date
            Dim dtTerminationTo As Date
            Dim dtEmplEnd As Date
            Dim dtEmployee_enddate As Date

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dtAppointed = Nothing
                dtTerminationFrom = Nothing
                dtTerminationTo = Nothing
                dtEmplEnd = Nothing
                dtEmployee_enddate = Nothing

                If IsDBNull(dsRow.Item("appointeddate")) = False Then
                    dtAppointed = eZeeDate.convertDate(dsRow.Item("appointeddate").ToString)
                End If

                If IsDBNull(dsRow.Item("termination_from_date")) = False Then
                    dtTerminationFrom = eZeeDate.convertDate(dsRow.Item("termination_from_date").ToString)
                End If

                'Sohail (11 Mar 2020) -- Start
                'Independent broadcasting Authority issue # 0004609: User not able to apply for loan. (Error index and length must refer to a location withing string on Loan application add / edit screen.)
                'If IsDBNull(dsRow.Item("termination_from_date")) = False Then
                If IsDBNull(dsRow.Item("termination_to_date")) = False Then
                    'Sohail (11 Mar 2020) -- End
                    dtTerminationTo = eZeeDate.convertDate(dsRow.Item("termination_to_date").ToString)
                End If

                'Sohail (11 Mar 2020) -- Start
                'Independent broadcasting Authority issue # 0004609: User not able to apply for loan. (Error index and length must refer to a location withing string on Loan application add / edit screen.)
                'If IsDBNull(dsRow.Item("termination_from_date")) = False Then
                If IsDBNull(dsRow.Item("empl_enddate")) = False Then
                    'Sohail (11 Mar 2020) -- End
                    dtEmplEnd = eZeeDate.convertDate(dsRow.Item("empl_enddate").ToString)
                End If

                If dtTerminationTo.Date <> Nothing Then
                    dtEmployee_enddate = dtTerminationTo.Date
                Else
                    dtEmployee_enddate = xPeriodEnd
                End If
                If dtTerminationFrom.Date <> Nothing Then
                    dtEmployee_enddate = IIf(dtTerminationFrom.Date < dtEmployee_enddate, dtTerminationFrom.Date, dtEmployee_enddate)
                End If
                If dtEmplEnd.Date <> Nothing Then
                    dtEmployee_enddate = IIf(dtEmplEnd.Date < dtEmployee_enddate, dtEmplEnd.Date, dtEmployee_enddate)
                End If

                dsRow.Item("finalenddate") = eZeeDate.convertDate(dtEmployee_enddate)
            Next
            dsList.Tables(0).AcceptChanges()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeEndDates; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (18 Dec 2019) -- End

    Private Function Set_Notification_Dates(ByVal xDtTypeId As Integer, _
                                            ByVal StrUserName As String, _
                                            ByVal xEmpId As Integer, _
                                            ByVal xEcode As String, _
                                            ByVal xEmployeeName As String, _
                                            ByVal xCurrentDates As Dictionary(Of Integer, String), _
                                            ByVal xNotifDate As String, _
                                            ByVal xEffectiveDate As Date, _
                                            Optional ByVal xHost As String = "", _
                                            Optional ByVal xIP As String = "", Optional ByVal xCurr_User As String = "") As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            If xCurrentDates.Keys.Count <= 0 Then Return ""


            'Pinkal (18-May-2021) -- Start
            'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
            'If xNotifDate.Trim.Length <= 0 Then
            'xNotifDate = ConfigParameter._Object._Notify_Allocation
            'End If
            'Pinkal (18-May-2021) -- End



            Dim dsOldDates As New DataSet
            Dim xOldDate As Date = DateAdd(DateInterval.Day, -1, xEffectiveDate)
            dsOldDates = Get_Current_Dates(xOldDate, xDtTypeId, xEmpId)

            Dim xOldDates As String = ""

            If dsOldDates.Tables(0).Rows.Count > 0 Then
                If dsOldDates.Tables(0).Rows(0).Item("ddate1").ToString.Trim.Length > 0 Then
                    xOldDates = eZeeDate.convertDate(dsOldDates.Tables(0).Rows(0).Item("ddate1").ToString).ToShortDateString
                End If

                If dsOldDates.Tables(0).Rows(0).Item("ddate2").ToString.Trim.Length > 0 Then
                    xOldDates &= " - " & eZeeDate.convertDate(dsOldDates.Tables(0).Rows(0).Item("ddate2").ToString).ToShortDateString
                End If
            End If

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If xNotifDate.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                If StrUserName.Trim.Length > 0 Then
                StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                End If

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & xEmployeeName & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee") & " " & "<b>" & " " & getTitleCase(xEmployeeName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                'S.SANDEEP [08 DEC 2016] -- START
                'ENHANCEMENT : Issue for Wrong User Name Sent in Email (Edit By User : <UserName>)
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & xEcode & "</b>. Following information has been changed by user : <b>" & IIf(xCurr_User.Trim = "", User._Object._Firstname & " " & User._Object._Lastname, xCurr_User) & "</b></span></p>")
                'StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & IIf(xHost.Trim = "", getHostName.ToString, xHost) & "</b> and IPAddress : <b>" & IIf(xIP.Trim = "", getIP.ToString, xIP) & "</b></span></p>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & xEcode & "</b>. Following information has been changed by user : <b>" & xCurr_User & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 143, "with employeecode") & " " & "<b>" & " " & xEcode & "</b>." & " " & Language.getMessage("frmEmployeeMaster", 144, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & xHost & "</b> and IPAddress : <b>" & xIP & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 145, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage("frmEmployeeMaster", 146, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                'S.SANDEEP [08 DEC 2016] -- END
                StrMessage.Append(vbCrLf)
                'Gajanan (21 Nov 2018) -- Start
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                'Gajanan (21 Nov 2018) -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 10, "Effective Date") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & xEffectiveDate.ToShortDateString & "</span></b></TD>")
                StrMessage.Append("</TR>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TABLE>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<BR>")
                StrMessage.Append("<TABLE border = '1' WIDTH = '90%'>")
                StrMessage.Append(vbCrLf)
                'Gajanan (21 Nov 2018) -- Start
                StrMessage.Append("<TR WIDTH = '90%' bgcolor= 'SteelBlue'>")
                'Gajanan (21 Nov 2018) -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 11, "Date") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 12, "Old Date") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 13, "New Date") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")


                'Pinkal (18-May-2021) -- Start
                'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                'For Each sId As String In xNotifDate.Split(CChar("||"))(0).Split(CChar(","))

                If (CInt(xNotifDate) = enEmployeeDates.SUSPENSION_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_SUSPENSION) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 465, "Suspension Date") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xOldDates & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                    'Exit For
                ElseIf (CInt(xNotifDate) = enEmployeeDates.PROBATION_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_PROBATION) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 466, "Probation Date") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xOldDates & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                    'Exit For
                ElseIf (CInt(xNotifDate) = enEmployeeDates.EOC_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_TERMINATION) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 467, "EOC Date") & "</span></TD>")
                        If dsOldDates.Tables(0).Rows.Count > 0 Then
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & eZeeDate.convertDate(dsOldDates.Tables(0).Rows(0).Item("ddate1").ToString).ToShortDateString & "</span></TD>")
                            If dsOldDates.Tables(0).Rows(0).Item("ddate1").ToString.Length > 0 Then
                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & eZeeDate.convertDate(dsOldDates.Tables(0).Rows(0).Item("ddate1").ToString).ToShortDateString & "</span></TD>")
                        Else
                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>&nbsp;</span></TD>")
                        End If
                            'S.SANDEEP |21-AUG-2019| -- END
                        Else
                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>&nbsp;</span></TD>")
                        End If
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                ElseIf (CInt(xNotifDate) = enEmployeeDates.LEAVING_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_TERMINATION) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 468, "Leaving Date") & "</span></TD>")
                        If dsOldDates.Tables(0).Rows.Count > 0 Then
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & eZeeDate.convertDate(dsOldDates.Tables(0).Rows(0).Item("ddate2").ToString).ToShortDateString & "</span></TD>")
                            If dsOldDates.Tables(0).Rows(0).Item("ddate2").ToString.Length > 0 Then
                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & eZeeDate.convertDate(dsOldDates.Tables(0).Rows(0).Item("ddate2").ToString).ToShortDateString & "</span></TD>")
                        Else
                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>&nbsp;</span></TD>")
                        End If
                            'S.SANDEEP |21-AUG-2019| -- END
                        Else
                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>&nbsp;</span></TD>")
                        End If
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                ElseIf (CInt(xNotifDate) = enEmployeeDates.RETIREMENT_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_RETIREMENT) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 469, "Retirement Date") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xOldDates & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                    'Exit For
                ElseIf (CInt(xNotifDate) = enEmployeeDates.CONFIRM_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 463, "Confirmation Date") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xOldDates & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                    'Exit For
                ElseIf (CInt(xNotifDate) = enEmployeeDates.APPOINTED_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_APPOINTED_DATE) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 462, "Appointment Date") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xOldDates & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                    'Exit For
                ElseIf (CInt(xNotifDate) = enEmployeeDates.BIRTH_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_BIRTH_DATE) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 464, "Birthdate") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xOldDates & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                    'Exit For
                ElseIf (CInt(xNotifDate) = enEmployeeDates.MARRIAGE_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_MARRIGE_DATE) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 435, "Marriage Date") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xOldDates & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                    'Exit For
                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                ElseIf (CInt(xNotifDate) = enEmployeeDates.EXEMPTION_DATE) AndAlso (xDtTypeId = enEmp_Dates_Transaction.DT_EXEMPTION) Then
                        StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 29, "Exemption Date") & "</span></TD>")
                        StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xOldDates & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentDates(CInt(xNotifDate)) & "</span></TD>")
                        StrMessage.Append("</TR>" & vbCrLf)
                        blnFlag = True
                    'Exit For
                        'Sohail (21 Oct 2019) -- End
                    End If
                'Next
                'Pinkal (18-May-2021) -- End
            End If
            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_Notification_Dates; Module Name: " & mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    Public Sub SendEmails(ByVal xDateTypeId As Integer, _
                          ByVal xEmployeeId As Integer, _
                          ByVal xCode As String, _
                          ByVal xEName As String, _
                          ByVal xDict_CurrentDate As Dictionary(Of Integer, String), _
                          ByVal xConfigAllocNotif As String, _
                          ByVal xEffDate As Date, _
                          ByVal xHostName As String, _
                          ByVal xIPAddr As String, _
                          ByVal xLoggedUserName As String, _
                          ByVal xUserId As Integer, _
                          ByVal xLoginMod As enLogin_Mode, _
                          ByVal intCompanyUnkId As Integer)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            'Gajanan [4-May-2021] -- Start
            'If xConfigAllocNotif.Trim.Length <= 0 Then xConfigAllocNotif = ConfigParameter._Object._Notify_Allocation
            'Gajanan [4-May-2021] -- End


            If xConfigAllocNotif.Trim.Length > 0 Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty


                'Pinkal (18-May-2021) -- Start
                'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                Dim arDates() As String = xConfigAllocNotif.Split(CChar("|"))
                Dim mstrUsers As String = ""
                Dim mblnIsUserAccess As Boolean = True
                Dim mstrOtherEmail As String = ""
                If arDates.Length > 0 Then
                    mstrUsers = arDates(0).ToString()
                    If arDates.Length > 1 Then
                        mblnIsUserAccess = CBool(arDates(1))
                        mstrOtherEmail = arDates(2).ToString()
                    End If
                End If
                'Pinkal (18-May-2021) -- End

                'Gajanan [4-May-2021] -- Start
                'For Each sId As String In xConfigAllocNotif.Split(CChar("||"))(2).Split(CChar(","))

                'Pinkal (18-May-2021) -- Start
                'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                If mstrUsers.Trim.Length > 0 Then
                    'For Each sId As String In xConfigAllocNotif.Split(CChar(","))
                    For Each sId As String In mstrUsers.Split(CChar(","))
                        'Pinkal (18-May-2021) -- End
                    'Gajanan [4-May-2021] -- End
                        objUsr._Userunkid = CInt(sId)

                        'Pinkal (18-May-2021) -- Start
                        'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                        'StrMessage = Set_Notification_Dates(xDateTypeId, objUsr._Firstname & " " & objUsr._Lastname, xEmployeeId, xCode, xEName, xDict_CurrentDate, xConfigAllocNotif, xEffDate, xHostName, xIPAddr, xLoggedUserName)
                        Dim xNotifyDateId As Integer = 0
                        Select Case xDateTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                xNotifyDateId = enEmployeeDates.PROBATION_DATE

                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                xNotifyDateId = enEmployeeDates.CONFIRM_DATE

                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                xNotifyDateId = enEmployeeDates.SUSPENSION_DATE

                            Case enEmp_Dates_Transaction.DT_TERMINATION

                                If xDict_CurrentDate.ContainsKey(enEmployeeDates.EOC_DATE) Then
                                    xNotifyDateId = enEmployeeDates.EOC_DATE
                                ElseIf xDict_CurrentDate.ContainsKey(enEmployeeDates.LEAVING_DATE) Then
                                    xNotifyDateId = enEmployeeDates.LEAVING_DATE
                                End If

                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                xNotifyDateId = enEmployeeDates.RETIREMENT_DATE

                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                xNotifyDateId = enEmployeeDates.APPOINTED_DATE

                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                xNotifyDateId = enEmployeeDates.BIRTH_DATE

                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                xNotifyDateId = enEmployeeDates.MARRIAGE_DATE

                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                xNotifyDateId = enEmployeeDates.EXEMPTION_DATE
                        End Select
                        StrMessage = Set_Notification_Dates(xDateTypeId, objUsr._Firstname & " " & objUsr._Lastname, xEmployeeId, xCode, xEName, xDict_CurrentDate, xNotifyDateId.ToString(), xEffDate, xHostName, xIPAddr, xLoggedUserName)
                        'Pinkal (18-May-2021) -- End


                    If StrMessage <> "" Then
                        Dim objSendMail As New clsSendMail

                        objSendMail._ToEmail = objUsr._Email
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification of Changes in Employee Master file")
                        objSendMail._Message = StrMessage
                        With objSendMail
                            ._FormName = mstrFormName
                            ._LoginEmployeeunkid = mintLoginEmployeeunkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        objSendMail._OperationModeId = xLoginMod
                        objSendMail._UserUnkid = xUserId
                        objSendMail._SenderAddress = objUsr._Firstname & " " & objUsr._Lastname
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        Try
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(intCompanyUnkId)
                            'Sohail (30 Nov 2017) -- End
                        Catch ex As Exception
                        End Try
                        objSendMail = Nothing
                        'gobjEmailList.Add(New clsEmailCollection(objUsr._Email, _
                        '                                         Language.getMessage(mstrModuleName, 9, "Notification of Changes in Employee Master file"), _
                        '                                         StrMessage, _
                        '                                         "", _
                        '                                         0, _
                        '                                         "", _
                        '                                         "", _
                        '                                         xUserId, _
                        '                                         xLoginMod, _
                        '                                         clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT, _
                        '                                         objUsr._Firstname & " " & objUsr._Lastname))
                    End If
                Next
                End If
                objUsr = Nothing

                If mstrOtherEmail.Trim.Length > 0 Then
                    For Each sEmailId As String In mstrOtherEmail.Split(CChar(","))

                        Dim xNotifyDateId As Integer = 0
                        Select Case xDateTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                xNotifyDateId = enEmployeeDates.PROBATION_DATE

                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                xNotifyDateId = enEmployeeDates.CONFIRM_DATE

                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                xNotifyDateId = enEmployeeDates.SUSPENSION_DATE

                            Case enEmp_Dates_Transaction.DT_TERMINATION

                                If xDict_CurrentDate.ContainsKey(enEmployeeDates.EOC_DATE) Then
                                    xNotifyDateId = enEmployeeDates.EOC_DATE
                                ElseIf xDict_CurrentDate.ContainsKey(enEmployeeDates.LEAVING_DATE) Then
                                    xNotifyDateId = enEmployeeDates.LEAVING_DATE
                                End If

                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                xNotifyDateId = enEmployeeDates.RETIREMENT_DATE

                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                xNotifyDateId = enEmployeeDates.APPOINTED_DATE

                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                xNotifyDateId = enEmployeeDates.BIRTH_DATE

                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                xNotifyDateId = enEmployeeDates.MARRIAGE_DATE

                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                xNotifyDateId = enEmployeeDates.EXEMPTION_DATE
                        End Select

                        StrMessage = Set_Notification_Dates(xDateTypeId, "", xEmployeeId, xCode, xEName, xDict_CurrentDate, xNotifyDateId.ToString(), xEffDate, xHostName, xIPAddr, xLoggedUserName)

                        If StrMessage <> "" Then
                            Dim objSendMail As New clsSendMail

                            objSendMail._ToEmail = sEmailId
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification of Changes in Employee Master file")
                            objSendMail._Message = StrMessage
                            objSendMail._Form_Name = ""
                            objSendMail._LogEmployeeUnkid = 0
                            objSendMail._OperationModeId = xLoginMod
                            objSendMail._UserUnkid = xUserId
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            Try
                                objSendMail.SendMail(intCompanyUnkId)
                            Catch ex As Exception
                            End Try
                            objSendMail = Nothing
                        End If
                    Next
                End If
                'Pinkal (18-May-2021) -- End



                'trd = New Thread(AddressOf Send_Notification)
                'trd.IsBackground = True
                'trd.Start()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendEmails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Send_Notification(ByVal intCompanyUnkId As Integer)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._FormName = obj._FormName
                    objSendMail._LoginEmployeeunkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    objSendMail._ClientIP = mstrClientIP
                    objSendMail._HostName = mstrHostName
                    objSendMail._FromWeb = mblnIsWeb
                    objSendMail._AuditUserId = mintAuditUserId
objSendMail._CompanyUnkid = mintCompanyUnkid
                    objSendMail._AuditDate = mdtAuditDate
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(intCompanyUnkId)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception
                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

    Public Function IsDatesPresent(ByVal xEmployeeId As Integer, ByVal xDtTranTypeId As enEmp_Dates_Transaction, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = 0
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT 1 FROM hremployee_dates_tran WHERE employeeunkid = '" & xEmployeeId & "' AND  datetypeunkid = '" & xDtTranTypeId & "' AND isvoid = 0 "

            iCnt = objDataOperation.RecordCount(StrQ)

            If iCnt > 0 Then
                blnFlag = True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsDatesPresent; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return blnFlag
    End Function

    'S.SANDEEP [10-MAY-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
    Private Function Integrate_Symmetry(ByVal xDataOpr As clsDataOperation, _
                                        ByVal intDatetypeunkid As Integer, _
                                        ByVal intCompanyId As Integer, _
                                        ByVal intEmpId As Integer, _
                                        ByVal dtDate1 As Date, _
                                        ByVal dtDate2 As Date) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsDetails As New DataSet
        Dim exForce As Exception = Nothing
        Dim dtEmpAsOnDate As String = ""
        Dim dtServerDate As Date
        Dim blnIsSymmetryIntegrated As Boolean = False
        Try
            If intCompanyId > 0 Then

                StrQ = "SELECT " & _
                       "  @issymmetryintegrated = cfconfiguration.key_value " & _
                       "FROM hrmsConfiguration..cfconfiguration " & _
                       "WHERE UPPER(cfconfiguration.key_name) = 'ISSYMMETRYINTEGRATED' " & _
                       "AND cfconfiguration.companyunkid = @companyunkid "
                With xDataOpr
                    .ClearParameters()
                    .AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                    .AddParameter("@issymmetryintegrated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsSymmetryIntegrated, ParameterDirection.Output)

                    .ExecNonQuery(StrQ)
                    If .ErrorMessage <> "" Then
                        exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [12-JUN-2017] -- START
                    'ISSUE/ENHANCEMENT : SYMMETRY DBNULL TO BOOLEAN ERROR
                    'blnIsSymmetryIntegrated = .GetParameterValue("@issymmetryintegrated")
                    If IsDBNull(.GetParameterValue("@issymmetryintegrated")) = False Then
                    blnIsSymmetryIntegrated = .GetParameterValue("@issymmetryintegrated")
                    End If
                    'S.SANDEEP [12-JUN-2017] -- END
                End With

                If blnIsSymmetryIntegrated Then
                    Dim mstrEmpCode As String = String.Empty
                    StrQ = "SELECT @employeecode = employeecode,@serverdate = GETDATE() FROM hremployee_master WHERE employeeunkid = @employeeunkid "
                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        .AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpCode, ParameterDirection.InputOutput)
                        .AddParameter("@ServerDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtServerDate, ParameterDirection.Output)
                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        mstrEmpCode = .GetParameterValue("@employeecode")
                        dtServerDate = .GetParameterValue("@serverdate")
                    End With

                    StrQ = "SELECT " & _
                           "  @empasondate = cfconfiguration.key_value " & _
                           "FROM hrmsConfiguration..cfconfiguration " & _
                           "WHERE UPPER(cfconfiguration.key_name) = 'EMPLOYEEASONDATE' " & _
                           "AND cfconfiguration.companyunkid = @companyunkid "
                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                        .AddParameter("@empasondate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dtEmpAsOnDate, ParameterDirection.Output)

                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        dtEmpAsOnDate = .GetParameterValue("@empasondate")
                    End With

                    Dim dtCurDate1, dtCurDate2 As Date : dtCurDate1 = Nothing : dtCurDate2 = Nothing
                    Dim strTermReason As String = String.Empty

                    StrQ = "SELECT TOP 1 " & _
                           "     @date1 = hremployee_dates_tran.date1 " & _
                           "    ,@date2 = hremployee_dates_tran.date2 " & _
                           "    ,@reson = ISNULL(cfcommon_master.name,'') " & _
                           "FROM hremployee_dates_tran " & _
                           " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_dates_tran.changereasonunkid AND cfcommon_master.mastertype = @mastertype " & _
                           "WHERE hremployee_dates_tran.isvoid = 0 " & _
                           " AND hremployee_dates_tran.employeeunkid = @employeeunkid AND hremployee_dates_tran.datetypeunkid = @datetypeunkid " & _
                           " AND CONVERT(NVARCHAR (8),hremployee_dates_tran.effectivedate,112) <= @xDate " & _
                           "ORDER BY CONVERT(NVARCHAR (8),hremployee_dates_tran.effectivedate,112) DESC "

                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        .AddParameter("@datetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDatetypeunkid)
                        .AddParameter("@xDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtEmpAsOnDate)
                        .AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurDate1, ParameterDirection.Output)
                        .AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurDate2, ParameterDirection.Output)
                        .AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(clsCommon_Master.enCommonMaster.TERMINATION))
                        .AddParameter("@reson", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strTermReason, ParameterDirection.InputOutput)

                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        If IsDBNull(.GetParameterValue("@date1")) = False Then
                            dtCurDate1 = .GetParameterValue("@date1")
                        End If
                        If IsDBNull(.GetParameterValue("@date2")) = False Then
                            dtCurDate2 = .GetParameterValue("@date2")
                        End If
                        strTermReason = .GetParameterValue("@reson")
                    End With

                    If mstrEmpCode.Trim.Length > 0 Then
                        Select Case intDatetypeunkid
                            Case enEmp_Dates_Transaction.DT_RETIREMENT, enEmp_Dates_Transaction.DT_SUSPENSION, enEmp_Dates_Transaction.DT_TERMINATION

                                StrQ = "SELECT " & _
                                       "     cfconfiguration.key_name " & _
                                       "    ,cfconfiguration.key_value " & _
                                       "FROM hrmsConfiguration..cfconfiguration " & _
                                       "WHERE cfconfiguration.companyunkid = @companyunkid " & _
                                       "AND UPPER(cfconfiguration.key_name) LIKE '%SYMMETRY%' "

                                xDataOpr.ClearParameters()
                                xDataOpr.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

                                dsDetails = xDataOpr.ExecQuery(StrQ, "List")

                                If xDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                If dsDetails.Tables("List").Rows.Count > 0 Then
                                    Dim strConn As String = String.Empty
                                    Using oSQL As SqlClient.SqlConnection = New SqlClient.SqlConnection
                                        strConn = "Data Source="
                                        Dim tmp As DataRow() = Nothing
                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATASERVERADDRESS'")
                                        If tmp.Length > 0 Then strConn &= tmp(0)("key_value") & ";"

                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASENAME'")
                                        If tmp.Length > 0 Then strConn &= "Initial Catalog=" & tmp(0)("key_value") & ";"

                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYAUTHENTICATIONMODEID'")
                                        If tmp.Length > 0 Then
                                            If CInt(tmp(0)("key_value")) = 0 Then   'WINDOWS
                                                strConn &= "Integrated Security=True "
                                            ElseIf CInt(tmp(0)("key_value")) = 1 Then   'USER
                                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASEUSERNAME'")
                                                If tmp.Length > 0 Then strConn &= "User ID=" & tmp(0)("key_value") & ";"
                                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASEPASSWORD'")
                                                If tmp.Length > 0 Then strConn &= "Password=" & clsSecurity.Decrypt(tmp(0)("key_value"), "ezee") & ";"
                                            End If
                                        End If
                                        oSQL.ConnectionString = strConn
                                        oSQL.Open()
                                        StrQ = "" : Dim intRecCount As Integer = 0
                                        StrQ = "SELECT * FROM DataImportTable WHERE DataImportTable.EmployeeReference = @EmployeeReference "
                                        Using oCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand
                                            oCmd.Connection = oSQL
                                            oCmd.CommandText = StrQ
                                            oCmd.Parameters.Clear()
                                            oCmd.Parameters.AddWithValue("@EmployeeReference", mstrEmpCode)
                                            Dim dsInfo As New DataSet
                                            Dim oDa As New SqlClient.SqlDataAdapter(oCmd)
                                            oDa.Fill(dsInfo)

                                            oCmd.Parameters.Clear()
                                            If dsInfo.Tables(0).Rows.Count > 0 Then 'UPDATE
                                                StrQ = "UPDATE DataImportTable SET "
                                                If intDatetypeunkid > 0 Then
                                                    Select Case intDatetypeunkid
                                                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                                                            StrQ &= " PersonalData10 = @PersonalData10 "
                                                            If dtCurDate1 <> Nothing Then
                                                                If dtServerDate.Date >= dtCurDate1.Date Then
                                                                    StrQ &= ",RecordRequest = @RecordRequest "
                                                                    StrQ &= ",ImportNow = @ImportNow "
                                                                    oCmd.Parameters.AddWithValue("@RecordRequest", 2)
                                                                    oCmd.Parameters.AddWithValue("@ImportNow", 1)
                                                                End If
                                                            End If
                                                            oCmd.Parameters.AddWithValue("@PersonalData10", IIf(dtCurDate1 <> Nothing, dtCurDate1, ""))

                                                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                                                            StrQ &= " PersonalData11 = @PersonalData11 "
                                                            StrQ &= ",PersonalData12 = @PersonalData12 "
                                                            If dtCurDate1 <> Nothing AndAlso dtCurDate2 <> Nothing Then
                                                                If ((dtServerDate.Date >= dtCurDate1.Date) And (dtServerDate.Date <= dtCurDate2.Date)) Then
                                                                    StrQ &= ",RecordRequest = @RecordRequest "
                                                                    StrQ &= ",ImportNow = @ImportNow "
                                                                    oCmd.Parameters.AddWithValue("@RecordRequest", 2)
                                                                    oCmd.Parameters.AddWithValue("@ImportNow", 1)
                                                                End If
                                                            End If
                                                            oCmd.Parameters.AddWithValue("@PersonalData11", IIf(dtCurDate1 <> Nothing, dtCurDate1, ""))
                                                            oCmd.Parameters.AddWithValue("@PersonalData12", IIf(dtCurDate2 <> Nothing, dtCurDate2, ""))

                                                        Case enEmp_Dates_Transaction.DT_TERMINATION
                                                            StrQ &= " PersonalData8 = @PersonalData8 " & _
                                                                    ",PersonalData9 = @PersonalData9 " & _
                                                                    ",PersonalData7 = @PersonalData7 "
                                                            If dtCurDate1 <> Nothing AndAlso dtCurDate2 <> Nothing Then
                                                                If ((dtServerDate.Date >= dtCurDate1.Date) And (dtServerDate.Date <= dtCurDate2.Date)) Then
                                                                    StrQ &= ",RecordRequest = @RecordRequest "
                                                                    StrQ &= ",ImportNow = @ImportNow "
                                                                    oCmd.Parameters.AddWithValue("@RecordRequest", 2)
                                                                    oCmd.Parameters.AddWithValue("@ImportNow", 1)
                                                                End If
                                                            End If
                                                            oCmd.Parameters.AddWithValue("@PersonalData8", IIf(dtCurDate1 <> Nothing, dtCurDate1, ""))
                                                            oCmd.Parameters.AddWithValue("@PersonalData9", IIf(dtCurDate2 <> Nothing, dtCurDate2, ""))
                                                            oCmd.Parameters.AddWithValue("@PersonalData7", strTermReason.ToString.Substring(0, IIf(Len(strTermReason) < 40, Len(strTermReason), 40)))
                                                    End Select
                                                    StrQ &= " WHERE RecordCount = @RecordCount "
                                                    oCmd.Parameters.AddWithValue("@RecordCount", dsInfo.Tables(0).Rows(0).Item("RecordCount"))

                                                    oCmd.CommandText = StrQ
                                                    oCmd.ExecuteNonQuery()

                                                End If
                                            End If
                                        End Using
                                    End Using
                                End If
                        End Select
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Integrate_Symmetry; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [10-MAY-2017] -- END

    'S.SANDEEP [28-Feb-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002032}
    Public Function IsPendingSalaryIncrement(ByVal employeeid As Integer, ByVal edate As DateTime) As Boolean
        Dim mflag As Boolean = False
        Dim StrQ As String = ""
        Try
            Using operation As clsDataOperation = New clsDataOperation()

                StrQ = "SELECT 1 FROM prsalaryincrement_tran " & _
                       "WHERE isvoid = 0 AND isapproved = 0 " & _
                       "AND employeeunkid = @employeeunkid AND CONVERT(NVARCHAR(8),incrementdate,112) >= @effectivedate "

                operation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, employeeid)
                operation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(edate))

                Dim value As Integer = operation.RecordCount(StrQ)

                If operation.ErrorMessage <> "" Then
                    Throw New Exception(operation.ErrorNumber & " : " & operation.ErrorMessage)
                End If

                If value > 0 Then
                    mflag = False
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPendingSalaryIncrement; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [28-Feb-2018] -- END


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    Private Function SetEnableDisableUserInAD(ByVal objDataOperation As clsDataOperation, ByVal xEmployeeID As Integer, ByVal xCurrentDate As Date) As Boolean
        Dim strQ As String = ""
        Dim mdtDate As DateTime = Nothing
        Dim mstrDisplayName As String = ""
        Dim mstrADProperty As String = ""
        Dim exForce As Exception = Nothing
        Try

            Dim mblnEnable As Boolean = True
            Dim lstEmpDates As New List(Of DateTime)
            Dim dtRehireDate As DateTime = Nothing


            Dim objEmpMst As New clsEmployee_Master
            objEmpMst._DataOperation = objDataOperation
            objEmpMst._Employeeunkid(xCurrentDate.Date) = xEmployeeID

            If objEmpMst._Empl_Enddate <> Nothing AndAlso IsDBNull(objEmpMst._Empl_Enddate) = False Then  'EOC DATE
                lstEmpDates.Add(objEmpMst._Empl_Enddate.Date)
            End If

            If objEmpMst._Termination_From_Date <> Nothing AndAlso IsDBNull(objEmpMst._Termination_From_Date) = False Then  'LEAVING DATE
                lstEmpDates.Add(objEmpMst._Termination_From_Date.Date)
            End If

            If objEmpMst._Termination_To_Date <> Nothing AndAlso IsDBNull(objEmpMst._Termination_To_Date) = False Then  'RETIREMENT DATE
                lstEmpDates.Add(objEmpMst._Termination_To_Date.Date)
            End If

            If IsDBNull(objEmpMst._Reinstatementdate.Date) = False Then 'REHIRE DATE
                dtRehireDate = objEmpMst._Reinstatementdate.Date
            End If

            mstrDisplayName = objEmpMst._Displayname

            objEmpMst = Nothing

            Dim dtTerminationDate As DateTime = Nothing
            If lstEmpDates IsNot Nothing AndAlso lstEmpDates.Count > 0 Then
                dtTerminationDate = lstEmpDates.Min()

                If dtTerminationDate <> Nothing AndAlso dtTerminationDate.Date <= xCurrentDate.Date Then
                    mblnEnable = False
                End If

                If dtRehireDate <> Nothing AndAlso dtRehireDate.Date <= xCurrentDate.Date Then
                    mblnEnable = True
                End If

            Else
                If dtRehireDate <> Nothing AndAlso dtRehireDate.Date <= xCurrentDate.Date Then
                    mblnEnable = True
                End If
            End If

            Dim mstrADIPAddress As String = ""
            Dim mstrADDomain As String = ""
            Dim mstrADUserName As String = ""
            Dim mstrADUserPwd As String = ""
            GetADConnection(mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, objDataOperation)
            EnableDisableActiveDirectoryUser(mblnEnable, mstrDisplayName, mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd)

        Catch ex As Exception
            Throw New Exception(mstrModuleName & "SetEnableDisableUserInAD:- " & ex.Message)
        End Try
        Return True
    End Function
    'Pinkal (18-Aug-2018) -- End

    'Sohail (24 Dec 2018) -- Start
    'NMB Enhancement - 76.1 - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
    Public Function GetEmployeeServiceDays(ByVal xDataOp As clsDataOperation _
                                           , ByVal intCompanyId As Integer _
                                           , ByVal strDatabaseName As String _
                                           , ByVal strActiveEmployeeIDs As String _
                                           , ByVal dtAsOnDate As Date _
                                           ) As DataSet

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim i As Integer = -1
        Dim xDBName As String = ""

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT  yearunkid , " & _
                            "financialyear_name , " & _
                            "database_name , " & _
                            "companyunkid , " & _
                            "isclosed , " & _
                            "convert(char(8),start_date,112) AS start_date , " & _
                            "convert(char(8),end_date,112) AS end_date " & _
                            "FROM  " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
                   "WHERE companyunkid = @companyunkid " & _
                   "ORDER BY convert(CHAR(8), start_date, 112) "

            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

            Dim dsYear As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT employeeunkid " & _
                    "INTO #tblEmp " & _
                    "FROM hremployee_master " & _
                    "WHERE employeeunkid IN ( " & strActiveEmployeeIDs & " ) " & _
                    " " & _
                    "SELECT hremployee_master.employeeunkid " & _
                             ", 1 AS datestranunkid " & _
                             ", 0 AS rehiretranunkid " & _
                             ", CONVERT(CHAR(8), appointeddate, 112) AS effectivedate " & _
                             ", CONVERT(CHAR(8), appointeddate, 112) AS date1 " & _
                             ", " & enEmp_Dates_Transaction.DT_APPOINTED_DATE & " AS datetypeunkid " & _
                             ", 0 AS ServiceDays " & _
                        "FROM hremployee_master " & _
                            "JOIN #tblEmp ON #tblEmp.employeeunkid = hremployee_master.employeeunkid " & _
                        "WHERE isapproved = 1 " & _
                              "AND CONVERT(CHAR(8), appointeddate, 112) <= @end_date "

            For Each dsRow As DataRow In dsYear.Tables(0).Rows
                i += 1

                If strDatabaseName.ToUpper = dsRow.Item("database_name").ToString.ToUpper Then
                    xDBName = ""
                Else
                    xDBName = dsRow.Item("database_name").ToString & ".."
                End If

                'If i > 0 Then
                StrQ &= " UNION "
                'End If

                StrQ &= "/*SELECT hremployee_dates_tran.employeeunkid " & _
                             ", datestranunkid " & _
                             ", 0 AS rehiretranunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CONVERT(CHAR(8), date1, 112) AS date1 " & _
                             ", datetypeunkid " & _
                             ", 0 AS ServiceDays " & _
                        "FROM " & xDBName & "hremployee_dates_tran " & _
                            "JOIN #tblEmp ON #tblEmp.employeeunkid = hremployee_dates_tran.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_APPOINTED_DATE & " " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                              "AND CONVERT(CHAR(8), date1, 112) <= @end_date " & _
                        "UNION */ " & _
                        "SELECT hremployee_dates_tran.employeeunkid " & _
                             ", datestranunkid " & _
                             ", 0 AS rehiretranunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CONVERT(CHAR(8), date1, 112) AS date1 " & _
                             ", datetypeunkid " & _
                             ", 0 AS ServiceDays " & _
                        "FROM " & xDBName & "hremployee_dates_tran " & _
                            "JOIN #tblEmp ON #tblEmp.employeeunkid = hremployee_dates_tran.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                              "AND date1 IS NOT NULL " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                              "/*AND CONVERT(CHAR(8), date1, 112) <= @end_date*/ " & _
                        "UNION " & _
                        "SELECT hremployee_dates_tran.employeeunkid " & _
                             ", datestranunkid " & _
                             ", 0 AS rehiretranunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CONVERT(CHAR(8), date2, 112) AS date2 " & _
                             ", datetypeunkid " & _
                             ", 0 AS ServiceDays " & _
                        "FROM " & xDBName & "hremployee_dates_tran " & _
                            "JOIN #tblEmp ON #tblEmp.employeeunkid = hremployee_dates_tran.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                              "AND date2 IS NOT NULL " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                              "/*AND CONVERT(CHAR(8), date2, 112) <= @end_date*/ " & _
                        "UNION " & _
                        "SELECT hremployee_dates_tran.employeeunkid " & _
                             ", datestranunkid " & _
                             ", 0 AS rehiretranunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CONVERT(CHAR(8), date1, 112) AS date1 " & _
                             ", datetypeunkid " & _
                             ", 0 AS ServiceDays " & _
                        "FROM " & xDBName & "hremployee_dates_tran " & _
                            "JOIN #tblEmp ON #tblEmp.employeeunkid = hremployee_dates_tran.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " " & _
                              "AND date1 IS NOT NULL " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                              "/*AND CONVERT(CHAR(8), date1, 112) <= @end_date*/ " & _
                        "UNION " & _
                        "SELECT hremployee_rehire_tran.employeeunkid " & _
                             ", 0 AS datestranunkid " & _
                             ", rehiretranunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CONVERT(CHAR(8), reinstatment_date, 112) AS reinstatment_date " & _
                             ", " & enEmp_Dates_Transaction.DT_REHIRE & " AS datetypeunkid " & _
                             ", 0 AS ServiceDays " & _
                        "FROM " & xDBName & "hremployee_rehire_tran " & _
                            "JOIN #tblEmp ON #tblEmp.employeeunkid = hremployee_rehire_tran.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND reinstatment_date IS NOT NULL " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                              "AND CONVERT(CHAR(8), reinstatment_date, 112) <= @end_date "

            Next

            StrQ &= "ORDER BY employeeunkid " & _
                           ", effectivedate " & _
                           ", date1 " & _
                    " " & _
                    "DROP TABLE #tblEmp "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            dsList.Tables(0).Columns.Add("fromdate", System.Type.GetType("System.String")).DefaultValue = ""
            dsList.Tables(0).Columns.Add("todate", System.Type.GetType("System.String")).DefaultValue = ""

            '*** Removing continues termination from dataset
            Dim strPrevKy As String = ""
            Dim intRowIdx As Integer = -1
            Dim blnCalcDone As Boolean = True
            Dim dtS As Date = Nothing
            Dim dtE As Date = Nothing
            Dim dtL As Date = Nothing 'EOC / Leaving
            Dim dtR As Date = Nothing 'Retirement
            Dim intTotRows As Integer = dsList.Tables(0).Rows.Count - 1
            Dim drNextRow As DataRow = Nothing
            For Each dsRow As DataRow In dsList.Tables(0).Rows
                intRowIdx += 1
                drNextRow = Nothing

                If intRowIdx < intTotRows AndAlso CInt(dsRow.Item("employeeunkid")) = CInt(dsList.Tables(0).Rows(intRowIdx + 1).Item("employeeunkid")) Then 'If Next Row is Exist
                    drNextRow = dsList.Tables(0).Rows(intRowIdx + 1)
                End If

                If strPrevKy <> dsRow.Item("employeeunkid").ToString Then
                    dtS = Nothing
                    dtE = Nothing
                    dtL = Nothing
                    dtR = Nothing
                    blnCalcDone = True
                End If

                If CInt(dsRow.Item("datestranunkid")) > 0 AndAlso CInt(dsRow.Item("datetypeunkid")) <> enEmp_Dates_Transaction.DT_APPOINTED_DATE Then 'EOC / Leaving / Retirement Date

                    If CInt(dsRow.Item("datetypeunkid")) = enEmp_Dates_Transaction.DT_TERMINATION Then
                        dtL = eZeeDate.convertDate(dsRow("date1").ToString())
                    ElseIf CInt(dsRow.Item("datetypeunkid")) = enEmp_Dates_Transaction.DT_RETIREMENT Then
                        dtR = eZeeDate.convertDate(dsRow("date1").ToString())
                    End If

                    If drNextRow IsNot Nothing Then 'If Next Row is Exist
                        If CInt(drNextRow.Item("datestranunkid")) > 0 AndAlso CInt(dsRow.Item("datetypeunkid")) <> enEmp_Dates_Transaction.DT_APPOINTED_DATE Then 'If Next Rows is also EOC / Leaving /Retirement Date then Delete Current Row

                            If CInt(dsRow.Item("datetypeunkid")) <> CInt(drNextRow.Item("datetypeunkid")) Then 'Next row is eother EOC Leave or Retirement and either current or next row is retirement
                                'If eZeeDate.convertDate(dsRow("date1").ToString()) > eZeeDate.convertDate(drNextRow("date1").ToString()) Then
                                strPrevKy = dsRow.Item("employeeunkid").ToString
                                dsRow.Delete()
                                Continue For
                                'End If
                            Else
                                strPrevKy = dsRow.Item("employeeunkid").ToString
                                dsRow.Delete()
                                Continue For
                            End If

                        End If
                    End If

                    If dtL = Nothing Then
                        dtL = dtAsOnDate
                    End If
                    If dtR = Nothing Then
                        dtR = dtAsOnDate
                    End If

                    If dtL <= dtR Then
                        dtE = dtL
                    Else
                        dtE = dtR
                    End If

                    If dtE > dtAsOnDate Then
                        dtE = dtAsOnDate
                    End If

                    If dtS <> Nothing Then
                        dsRow.Item("fromdate") = eZeeDate.convertDate(dtS)
                        dsRow.Item("todate") = eZeeDate.convertDate(dtE)
                        dsRow.Item("ServiceDays") = DateDiff(DateInterval.Day, dtS, dtE.AddDays(1))
                    End If

                    blnCalcDone = True
                    dtS = Nothing

                Else 'Appointment / Rehire Date

                    If drNextRow IsNot Nothing Then 'If Next Row is Exist
                        If CInt(drNextRow.Item("datetypeunkid")) = enEmp_Dates_Transaction.DT_APPOINTED_DATE OrElse CInt(dsList.Tables(0).Rows(intRowIdx + 1).Item("datetypeunkid")) = enEmp_Dates_Transaction.DT_REHIRE Then 'If Next Rows is also Appointment / Rehire Date then Delete Current Row
                            strPrevKy = dsRow.Item("employeeunkid").ToString
                            dsRow.Delete()
                            Continue For
                        End If
                    End If

                    dtS = eZeeDate.convertDate(dsRow("date1").ToString())
                    dtL = Nothing

                    blnCalcDone = False
                End If


                If blnCalcDone = False AndAlso intRowIdx < intTotRows AndAlso dsRow.Item("employeeunkid").ToString <> dsList.Tables(0).Rows(intRowIdx + 1).Item("employeeunkid").ToString Then
                    dtE = dtAsOnDate

                    If dtE > dtR Then
                        dtE = dtR
                    End If

                    If dtS <> Nothing Then
                        dsRow.Item("fromdate") = eZeeDate.convertDate(dtS)
                        dsRow.Item("todate") = eZeeDate.convertDate(dtE)
                        dsRow.Item("ServiceDays") = DateDiff(DateInterval.Day, dtS, dtE.AddDays(1))
                    End If
                End If

                strPrevKy = dsRow.Item("employeeunkid").ToString
            Next
            dsList.Tables(0).AcceptChanges()

            If blnCalcDone = False Then
                dtE = dtAsOnDate

                If dtE > dtR Then
                    dtE = dtR
                End If

                If dtS <> Nothing Then
                    dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1).Item("fromdate") = eZeeDate.convertDate(dtS)
                    dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1).Item("todate") = eZeeDate.convertDate(dtE)
                    dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1).Item("ServiceDays") = DateDiff(DateInterval.Day, dtS, dtE.AddDays(1))
                End If
                dsList.Tables(0).AcceptChanges()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeServiceDays; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (24 Dec 2018) -- End

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Public Function GetUnkIdByLeaveIssueUnkId(ByVal xDataOp As clsDataOperation _
                                            , ByVal intLeaveIssueUnkId As Integer _
                                            , Optional ByVal strFilter As String = "" _
                                            ) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim intID As Integer = 0

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT " & _
                        "hremployee_dates_tran.datestranunkid " & _
                    "FROM hremployee_dates_tran " & _
                    "WHERE hremployee_dates_tran.isvoid = 0 "

            If intLeaveIssueUnkId > 0 Then
                StrQ &= "AND hremployee_dates_tran.leaveissueunkid = @leaveissueunkid "
                objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveIssueUnkId)
            End If

            If strFilter.Trim <> "" Then
                StrQ &= strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intID = CInt(dsList.Tables(0).Rows(0).Item("datestranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUnkIdLeaveIssueUnkId; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intID
    End Function
    'Sohail (21 Oct 2019) -- End

    'S.SANDEEP |13-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
    Public Function BlockExitStaffInCBS(ByVal intEmpId As Integer, _
                                        ByVal iDate As Date, _
                                        ByVal dtCurrentDate As Date, _
                                        ByVal intCompanyId As Integer, _
                                        ByVal strUserState As String, _
                                        ByVal objDataOper As clsDataOperation, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal iLoginEmployeeId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsData As New DataSet
        Dim objCParam As New clsConfigOptions
        Dim strServiceURL As String = ""
        Dim straccno As String = String.Empty
        Dim strcccno As String = String.Empty
        Dim strFailedEmailNotification As String = String.Empty
        Dim strOracleExecption As String = String.Empty
        Try
            Dim objEmp As New clsEmployee_Master
            objDataOper.ClearParameters()
            Dim mDicKeyValues As New Dictionary(Of String, String)
            Dim strParamKeys() As String = { _
                                            "IsHRFlexcubeIntegrated", _
                                            "_FlxSrv_" & CInt(enFlexcubeServiceInfo.FLX_EXBLK_USER), _
                                            "OracleHostName", _
                                            "OraclePortNo", _
                                            "OracleServiceName", _
                                            "OracleUserName", _
                                            "OracleUserPassword" _
                                            }
            mDicKeyValues = objCParam.GetKeyValue(intCompanyId, strParamKeys)
            strFailedEmailNotification = objCParam.GetKeyValue(intCompanyId, "FailedRequestNotificationEmails")

            If mDicKeyValues.Keys.Count > 0 Then
                If CBool(mDicKeyValues("IsHRFlexcubeIntegrated")) Then
                    If dtCurrentDate = Nothing Then dtCurrentDate = Now.Date
                    If iDate.Date > dtCurrentDate.Date Then Return True

                    'S.SANDEEP |31-DEC-2019| -- START
                    'ISSUE/ENHANCEMENT : GIVEN KEY NOT PRESENT
                    'strServiceURL = mDicKeyValues("_FlxSrv_" & CInt(enFlexcubeServiceInfo.FLX_EXBLK_USER))
                    If mDicKeyValues.ContainsKey("_FlxSrv_" & CInt(enFlexcubeServiceInfo.FLX_EXBLK_USER)) Then
                    strServiceURL = mDicKeyValues("_FlxSrv_" & CInt(enFlexcubeServiceInfo.FLX_EXBLK_USER))
                    End If
                    'S.SANDEEP |31-DEC-2019| -- END
                    If strServiceURL IsNot Nothing AndAlso strServiceURL.Trim.Length > 0 Then
                        StrQ = "SELECT 1 FROM hrflexcube_request_tran WHERE reqmst_number = '" & eZeeDate.convertDate(iDate).ToString() & "' AND employeeunkid = '" & intEmpId & "' AND iserror = 0 "
                        Dim intExists As Integer = objDataOper.RecordCount(StrQ)
                        If objDataOper.ErrorMessage <> "" Then
                            Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                        End If
                        If intExists > 0 Then Return True

                        StrQ = "SELECT " & _
                               "    hremployee_master.employeeunkid " & _
                               "   ,EA.customcode " & _
                               "   ,EB.accn " & _
                               "   FROM hremployee_master " & _
                               "   LEFT JOIN " & _
                               "   ( " & _
                               "       SELECT " & _
                               "            A.employeeunkid " & _
                               "           ,A.customcode " & _
                               "       FROM " & _
                               "       ( " & _
                               "           SELECT " & _
                               "                employeeunkid " & _
                               "               ,customcode " & _
                               "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                               "           FROM hremployee_cctranhead_tran " & _
                               "               JOIN prcostcenter_master ON cctranheadvalueid = prcostcenter_master.costcenterunkid " & _
                               "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @end_date " & _
                               "           AND employeeunkid = @employeeunkid AND istransactionhead = 0 " & _
                               "       ) AS A WHERE A.xNo = 1 " & _
                               "   ) AS EA ON EA.employeeunkid = hremployee_master.employeeunkid " & _
                               "   LEFT JOIN " & _
                               "   ( " & _
                               "       SELECT " & _
                               "            B.employeeunkid " & _
                               "           ,'''' + B.accountno + '''' AS accn " & _
                               "       FROM " & _
                               "       ( " & _
                               "           SELECT " & _
                               "                premployee_bank_tran.employeeunkid " & _
                               "               ,premployee_bank_tran.accountno " & _
                               "               ,DENSE_RANK() OVER (PARTITION  BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                               "           FROM premployee_bank_tran " & _
                               "               LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid AND isactive = 1 " & _
                               "           WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                               "       )AS B WHERE B.ROWNO = 1 " & _
                               "   ) AS EB ON EB.employeeunkid = hremployee_master.employeeunkid " & _
                               "WHERE hremployee_master.employeeunkid = @employeeunkid "

                        objDataOper.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        objDataOper.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtCurrentDate))

                        dsData = objDataOper.ExecQuery(StrQ, "List")

                        If objDataOper.ErrorMessage <> "" Then
                            Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                        End If

                        If dsData.Tables(0).Rows.Count > 0 Then
                            straccno = String.Join(",", dsData.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("accn")).ToArray())
                            strcccno = dsData.Tables(0).Rows(0)("customcode").ToString()
                        End If

                        If straccno.Trim.Length > 0 Then
                            dsData = New DataSet
                            If straccno.Trim.Length > 0 Then
                                dsData = New DataSet
                                Using cnnOracle As New OracleClient.OracleConnection
                                    cnnOracle.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & mDicKeyValues("OracleHostName").ToString() & ")(PORT=" & mDicKeyValues("OraclePortNo").ToString() & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & mDicKeyValues("OracleServiceName").ToString() & "))); User Id=" & mDicKeyValues("OracleUserName") & ";Password=" & clsSecurity.Decrypt(mDicKeyValues("OracleUserPassword").ToString(), "ezee") & "; "
                                    Try
                                        cnnOracle.Open()
                                    Catch ex As Exception
                                        strOracleExecption = ex.Message
                                        Throw ex
                                    End Try

                                    StrQ = "SELECT " & _
                                           "     USER_ID " & _
                                           "    ,USER_NAME " & _
                                           "    ,BRANCH " & _
                                           "    ,time_level " & _
                                           "    ,start_date " & _
                                           "    ,USER_STATUS " & _
                                           "FROM fcubs.aruti_user " & _
                                           "WHERE CUST_AC_NO IN (" & straccno & ") " 'ALWAYS TAKE FIRST ROW
                                    Using cmdOracle As New OracleClient.OracleCommand
                                        If cnnOracle.State = ConnectionState.Closed Or cnnOracle.State = ConnectionState.Broken Then
                                            cnnOracle.Open()
                                        End If
                                        cmdOracle.Connection = cnnOracle
                                        cmdOracle.CommandType = CommandType.Text
                                        cmdOracle.CommandText = StrQ
                                        Dim oda As New OracleClient.OracleDataAdapter(cmdOracle)
                                        oda.Fill(dsData)
                                    End Using
                                End Using

                                If dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                                    If dsData.Tables(0).Rows(0)("USER_STATUS").ToString().ToUpper() = "E" Then
                                        Dim strBuilder, strPrefix As String
                                        strPrefix = "UBLK_" : strBuilder = ""
                                        Dim oStrMsgId As String = ""
                                        Dim strFileName As String = ""
                                        Dim blnerror As Boolean = False
                                        Dim oResponseData As String = ""
                                        Dim strErrorDesc As String = ""

                                        ''''''''''''''''''''''''''''' GENERATE UNIQUE MESSAGE ID FOR EACH REQUEST ---- START
                                        Dim iCount As Integer = 1
                                        Dim dsList As New DataSet
                                        While iCount > 0
                                            StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) "
                                            dsList = objDataOper.ExecQuery(StrQ, "List")
                                            If objDataOper.ErrorMessage <> "" Then
                                                Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                                            End If
                                            oStrMsgId = CStr(dsList.Tables(0).Rows(0)(0))
                                            StrQ = "SELECT 1 FROM hrflexcube_request_tran WHERE fmsgeid = '" & oStrMsgId & "' "
                                            iCount = objDataOper.RecordCount(StrQ)
                                            If objDataOper.ErrorMessage <> "" Then
                                                Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                                            End If
                                            If iCount <= 0 Then Exit While
                                        End While
                                        ''''''''''''''''''''''''''''' GENERATE UNIQUE MESSAGE ID FOR EACH REQUEST ---- END

                                        StrQ = "SELECT TOP 1 " & _
                                               "  '<?xml version=""1.0"" encoding=""UTF-8""?>' " & _
                                               ", '<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns=""http://fcubs.ofss.com/service/FCUBSACService"">' " & _
                                               ", '<soapenv:Header/>' " & _
                                               ", '<soapenv:Body>'" & _
                                               ", '<MODIFYUSERMAINT_FSFS_REQ xmlns=""http://fcubs.ofss.com/service/FCUBSSMService"">' " & _
                                               ", '<FCUBS_HEADER>' " & _
                                               ", '<SOURCE>ARUTI</SOURCE>' " & _
                                               ", '<UBSCOMP>FCUBS</UBSCOMP>' " & _
                                               ", '<MSGID>' + '" & oStrMsgId & "' + '</MSGID>' " & _
                                               ", '<CORRELID>' + '" & oStrMsgId & "' + '</CORRELID>' " & _
                                               ", '<USERID>ARUTI</USERID>' " & _
                                               ", '<BRANCH>101</BRANCH>' " & _
                                               ", '<MODULEID>SM</MODULEID>' " & _
                                               ", '<SERVICE>FCUBSSMService</SERVICE>' " & _
                                               ", '<OPERATION>ModifyUserMaint</OPERATION>' " & _
                                               ", '<SOURCE_OPERATION>ModifyUserMaint</SOURCE_OPERATION>' " & _
                                               ", '<SOURCE_USERID/>' " & _
                                               ", '<DESTINATION/>' " & _
                                               ", '<MULTITRIPID/>' " & _
                                               ", '<FUNCTIONID/>' " & _
                                               ", '<ACTION></ACTION>' " & _
                                               ", '</FCUBS_HEADER>' " & _
                                               ", '<FCUBS_BODY>' " & _
                                               ", '<USR-Full>' " & _
                                               ", '<USRID>'+ '" & dsData.Tables(0).Rows(0)("USER_ID") & "' +'</USRID>' " & _
                                               ", '<USRNAME>'+ '" & dsData.Tables(0).Rows(0)("USER_NAME").Replace("'", "''") & "' +'</USRNAME>' " & _
                                               ", '<HOMEBRN>'+'" & dsData.Tables(0).Rows(0)("BRANCH") & "'+'</HOMEBRN>' " & _
                                               ", '<USRSTAT>'+'" & strUserState & "'+'</USRSTAT>' " & _
                                               ", '<USRLANG>ENG</USRLANG>' " & _
                                               ", '<TIMELEVEL>'+'" & dsData.Tables(0).Rows(0)("time_level") & "'+'</TIMELEVEL>' " & _
                                               ", '<STRTDATE>'+ '" & Format(CDate(dsData.Tables(0).Rows(0)("start_date")).Date, "yyyy-MM-dd") & "' +'</STRTDATE>' " & _
                                               ", '</USR-Full>' " & _
                                               ", '</FCUBS_BODY>' " & _
                                               ", '</MODIFYUSERMAINT_FSFS_REQ>' " & _
                                               ", '</soapenv:Body>' " & _
                                               ", '</soapenv:Envelope>' "

                                        dsList = objDataOper.ExecQuery(StrQ, "List")

                                        If objDataOper.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                                        End If

                                        If dsList.Tables("List").Rows.Count > 0 Then
                                            Dim sb As New System.Text.StringBuilder
                                            For Each iRow As DataRow In dsList.Tables("List").Rows
                                                For Each iCol As DataColumn In dsList.Tables("List").Columns
                                                    Dim strData As String = "" : strData = iRow(iCol).ToString()
                                                    SetXMLFormat(strData)
                                                    sb.Append(strData & vbCrLf)
                                                Next
                                            Next
                                            strBuilder = sb.ToString()
                                            If strBuilder.Trim.Length > 0 Then
                                                oResponseData = PostData(strBuilder, strServiceURL, strErrorDesc)
                                                If oResponseData.Trim.Length > 0 Then
                                                    Dim dsErr As New DataSet
                                                    dsErr.ReadXml(New System.IO.StringReader(oResponseData))
                                                    If dsErr.Tables.Contains("FCUBS_HEADER") = True Then
                                                        If dsErr.Tables("FCUBS_HEADER").Rows.Count > 0 Then
                                                            If dsErr.Tables("FCUBS_HEADER").Rows(0).Item("MSGSTAT").ToString.ToUpper <> "SUCCESS" Then
                                                                If dsErr.Tables.Contains("ERROR") = True Then
                                                                    If dsErr.Tables("ERROR").Rows.Count > 0 Then
                                                                        strErrorDesc = String.Join(",", dsErr.Tables("ERROR").AsEnumerable().Select(Function(x) x.Field(Of String)("ECODE").ToString() & " -> " & x.Field(Of String)("EDESC")).ToArray())
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    Else
                                                        strErrorDesc = "Invalid Response Data, [MSGSTAT] Not Found in the given response."
                                                    End If
                                                End If

                                                If strErrorDesc.Trim().Length > 0 Then
                                                    blnerror = True
                                                End If
                                                strFileName = strPrefix & eZeeDate.convertDate(iDate).ToString().ToString() & "_" & Date.Now.ToString("yyyymmdd") & ".xml"
                                                If strErrorDesc.Trim().Length > 0 Then
                                                    If strFailedEmailNotification.Trim.Length > 0 Then
                                                        Dim mDicError As New Dictionary(Of String, String)
                                                        mDicError.Add("USERID", dsData.Tables(0).Rows(0)("USER_ID").ToString)
                                                        mDicError.Add("MSGID/CORRELID", oStrMsgId)
                                                        mDicError.Add("REQUESTDATE", Now)
                                                        mDicError.Add("ERROR", strErrorDesc)
                                                        Call SendCBS_Notification(strFailedEmailNotification, mDicError, "", intCompanyId, "Exit Staff Block Failed Request [" + Now + "]", xUserUnkid, iLoginEmployeeId)
                                                    End If
                                                End If
                                                If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.STAFF, strFileName, strBuilder, "", oStrMsgId, oResponseData, blnerror, strErrorDesc, enRequestForm.EMPLOYEE, eZeeDate.convertDate(iDate).ToString(), dsData.Tables(0).Rows(0)("USER_ID").ToString, IIf(mstrClientIP.Trim.Length <= 0, getIP(), mstrClientIP), objDataOper) = False Then
                                                    Throw New Exception(objDataOper.ErrorNumber & ": " & objDataOper.ErrorMessage)
                                                End If
                                            End If
                                        End If
                                    Else
                                        If dsData.Tables(0).Rows(0)("USER_STATUS").ToString() <> "E" Then
                                            If strFailedEmailNotification.Trim.Length > 0 Then
                                                Dim mDicError As New Dictionary(Of String, String)
                                                mDicError.Add("USERID", dsData.Tables(0).Rows(0)("USER_ID").ToString)
                                                mDicError.Add("REQUESTDATE", Now)
                                                mDicError.Add("ERROR", "User Status : " & dsData.Tables(0).Rows(0)("USER_STATUS").ToString())
                                                Call SendCBS_Notification(strFailedEmailNotification, mDicError, "", intCompanyId, "Exit Staff Block Failed Request [" + Now + "]", xUserUnkid, iLoginEmployeeId)
                                            End If
                                        End If
                                        If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.STAFF, "", "", "", "", "", True, "User Status : " + dsData.Tables(0).Rows(0)("USER_STATUS").ToString(), enRequestForm.EMPLOYEE, eZeeDate.convertDate(iDate).ToString(), dsData.Tables(0).Rows(0)("USER_ID").ToString, IIf(mstrClientIP.Trim.Length <= 0, getIP(), mstrClientIP), objDataOper) = False Then
                                            Throw New Exception(objDataOper.ErrorNumber & ": " & objDataOper.ErrorMessage)
                                        End If
                                    End If
                                Else
                                    If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.STAFF, "", "", "", "", "", True, "Flexcube user not found", enRequestForm.EMPLOYEE, eZeeDate.convertDate(iDate).ToString(), "", IIf(mstrClientIP.Trim.Length <= 0, getIP(), mstrClientIP), objDataOper) = False Then
                                        Throw New Exception(objDataOper.ErrorNumber & ": " & objDataOper.ErrorMessage)
                                    End If
                                End If
                            Else
                                If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.STAFF, "", "", "", "", "", True, "Account Number Not Assigned", enRequestForm.EMPLOYEE, eZeeDate.convertDate(iDate).ToString(), "", IIf(mstrClientIP.Trim.Length <= 0, getIP(), mstrClientIP), objDataOper) = False Then
                                    Throw New Exception(objDataOper.ErrorNumber & ": " & objDataOper.ErrorMessage)
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            If strOracleExecption.Trim.Length > 0 Then
                If strFailedEmailNotification.Trim.Length > 0 Then
                    Call SendCBS_Notification(strFailedEmailNotification, Nothing, ex.Message, intCompanyId, "Block User Oracle Connection Issue [" + Now + "]", xUserUnkid, iLoginEmployeeId)
                End If
            End If
            Throw New Exception(ex.Message & "; Procedure Name: BlockExitStaffInCBS; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SendCBS_Notification(ByVal strFailedEmailNotification As String, _
                                     ByVal mDicError As Dictionary(Of String, String), _
                                     ByVal strExecption As String, _
                                     ByVal intCompanyId As Integer, _
                                     ByVal strSubject As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal iLoginEmployeeId As Integer)
        Try
            Dim arrEmail() As String = Nothing
            If strFailedEmailNotification.Trim.Contains(",") Then
                arrEmail = strFailedEmailNotification.Split(",")
            ElseIf strFailedEmailNotification.Trim.Contains(";") Then
                arrEmail = strFailedEmailNotification.Split(";")
            End If
            If arrEmail.Length > 0 Then
                Dim objComp As New clsCompany_Master
                objComp._Companyunkid = intCompanyId
                Dim strSenderAddress, strSendName As String
                strSenderAddress = "" : strSendName = ""
                strSenderAddress = objComp._Senderaddress
                strSendName = objComp._Sendername

                Dim strMailBody As String = String.Empty
                If strExecption.Trim.Length > 0 Then
                    strMailBody = "Aruti Leave Application Error in Blocking In Flexcube :  <b>" + strExecption.ToString() + "</b>"
                ElseIf mDicError IsNot Nothing AndAlso mDicError.Keys.Count > 0 Then
                    strMailBody = "<table style='width: 100%;' border='1'>" & vbCrLf
                    strMailBody &= "<tr style='width: 100%;'>" & vbCrLf
                    For Each iKey As String In mDicError.Keys
                        strMailBody &= "<td bgcolor = '#5D7B9D' border='1'><b><font color='#fff'>" & iKey & "</font></b></td>" & vbCrLf
                    Next
                    strMailBody &= "</tr>" & vbCrLf
                    strMailBody &= "<tr style='width: 100%;'>" & vbCrLf
                    For Each iKey As String In mDicError.Keys
                        strMailBody &= "<td border='1'>" & mDicError(iKey) & "</td>" & vbCrLf
                    Next
                    strMailBody &= "</tr>" & vbCrLf
                    strMailBody &= "</table>"
                End If
                Dim objMail As New clsSendMail
                If mstrFormName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrFormName
                    objMail._ClientIP = mstrClientIP
                    objMail._HostName = mstrHostName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = enLogin_Mode.DESKTOP
                objMail._UserUnkid = xUserUnkid
                objMail._Subject = strSubject
                objMail._Message = strMailBody
                objMail._SenderAddress = IIf(strSenderAddress.Trim.Length <= 0, strSendName, strSenderAddress)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                For i As Integer = 0 To arrEmail.Length - 1
                    objMail._ToEmail = arrEmail(i)
                    objMail.SendMail(intCompanyId)
                Next
                objMail = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendCBS_Notification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetXMLFormat(ByRef strData As String)
        Try
            strData = strData.Replace("&", "&amp;")
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetXMLFormat; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function PostData(ByVal strxmldata As String, ByVal strSrvURL As String, ByRef strError As String) As String
        Dim strResponseData As String = String.Empty
        Try
            Dim xmlDoc As XmlDocument = New XmlDocument()
            xmlDoc.LoadXml(strxmldata)
            Dim request As HttpWebRequest = CType(WebRequest.Create(strSrvURL), HttpWebRequest)
            Dim bytes As Byte()
            bytes = System.Text.Encoding.ASCII.GetBytes(xmlDoc.InnerXml)
            request.ContentType = "text/xml"
            request.ContentLength = bytes.Length
            request.Method = "POST"
            Dim requestStream As Stream = request.GetRequestStream()
            requestStream.Write(bytes, 0, bytes.Length)
            requestStream.Close()
            Using responseReader As New StreamReader(request.GetResponse().GetResponseStream())
                Dim result As String = responseReader.ReadToEnd()
                Dim ResultXML As XDocument = XDocument.Parse(result)
                strResponseData = ResultXML.ToString()
            End Using
        Catch ex As WebException
            If ex.InnerException IsNot Nothing Then
                strError = ex.InnerException.ToString() & Environment.NewLine & ex.Message
            Else
                strError = ex.Message
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PostData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strResponseData
    End Function
    'S.SANDEEP |13-DEC-2019| -- END


    'Pinkal (21-Dec-2019) -- Start
    'Enhancement HILL PACKAGING COMPANY LTD [0003462] -   Feature to exclude suspended employees from attendance in case they clock in while on suspension.
    'xOperationModeId = 0 'SUSPENSION
    'xOperationModeId = 1 'PROBATION

    'USED ONLY FOR SUSPENSION AND PROBATION.DON'T USE FOR ANY OTHER EMPLOYEE DATES MOVMENTS OPERATION.
    Public Function GetEmpSuspentionProbationDetailDates(ByVal xOperationModeId As Integer, ByVal xFromDate As DateTime, ByVal xToDate As DateTime) As DataTable
        Dim strQ As String = ""
        Dim dtTable As DataTable = Nothing
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            If xOperationModeId > 1 Then Return dtTable

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = " WITH MYCTE AS " & _
                      "( " & _
                      "     SELECT " & _
                      "       CAST(@startdate AS DATETIME) DateValue " & _
                      "     UNION ALL " & _
                      "     SELECT " & _
                      "       DateValue +1 " & _
                      "     FROM MYCTE " & _
                      "     WHERE DateValue + 1 <= @enddate " & _
                      " ) " & _
                      " SELECT " & _
                      "  employeeunkid " & _
                      " , MYCTE.DateValue AS Datevalue " & _
                      " FROM hremployee_dates_tran " & _
                      " LEFT JOIN MYCTE ON CONVERT(CHAR(8), MYCTE.DateValue, 112) BETWEEN CONVERT(CHAR(8), hremployee_dates_tran.date1, 112) AND CONVERT(CHAR(8),hremployee_dates_tran.date2, 112) " & _
                      " WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(CHAR(8), hremployee_dates_tran.effectivedate, 112) <= CONVERT(CHAR(8), MYCTE.DateValue, 112) " & _
                      " AND datetypeunkid = @DateTypeId " & _
                      " OPTION (MAXRECURSION 0) "

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xFromDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xToDate))

            If xOperationModeId = 0 Then  'SUSPENSION
                objDataOperation.AddParameter("@DateTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, enEmp_Dates_Transaction.DT_SUSPENSION)
            ElseIf xOperationModeId = 1 Then  ''PROBATION
                objDataOperation.AddParameter("@DateTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, enEmp_Dates_Transaction.DT_PROBATION)
            End If

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpSuspentionProbationDetailDates; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    'Pinkal (21-Dec-2019) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, porbation information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 2, "Sorry, suspension information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 3, "Sorry, termination information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 4, "Sorry, retirement information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 5, "Sorry, This probation date range is already defined. Please define new date range for this probation.")
			Language.setMessage(mstrModuleName, 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension.")
			Language.setMessage(mstrModuleName, 7, "Sorry, This termination information is already present.")
			Language.setMessage(mstrModuleName, 8, "Sorry, This retirement information is already present.")
			Language.setMessage(mstrModuleName, 9, "Notification of Changes in Employee Master file")
			Language.setMessage(mstrModuleName, 10, "Effective Date")
			Language.setMessage(mstrModuleName, 11, "Date")
			Language.setMessage(mstrModuleName, 12, "Old Date")
			Language.setMessage(mstrModuleName, 13, "New Date")
			Language.setMessage(mstrModuleName, 14, "WEB")
			Language.setMessage(mstrModuleName, 15, "Sorry, This confirmation information is already present.")
			Language.setMessage(mstrModuleName, 16, "Sorry, confirmation information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 17, "Sorry, re-hire information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 18, "Sorry, This re-hire information is already present.")
			Language.setMessage(mstrModuleName, 19, "Sorry, appointment information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 20, "Sorry, birthdate information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 21, "Sorry, first appointment date information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 22, "Sorry, anniversary information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 23, "Sorry, This appointment date information is already present.")
			Language.setMessage(mstrModuleName, 24, "Sorry, This birthdate date information is already present.")
			Language.setMessage(mstrModuleName, 25, "Sorry, This first appointment date information is already present.")
			Language.setMessage(mstrModuleName, 26, "Sorry, This anniversary information is already present.")
			Language.setMessage(mstrModuleName, 27, "Sorry, exemption information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 28, "Sorry, This exemption information is already present.")
			Language.setMessage("clsMasterData", 29, "Exemption Date")
			Language.setMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee")
			Language.setMessage("frmEmployeeMaster", 143, "with employeecode")
			Language.setMessage("frmEmployeeMaster", 144, "Following information has been changed by user")
			Language.setMessage("frmEmployeeMaster", 145, "from Machine")
			Language.setMessage("frmEmployeeMaster", 146, "and IPAddress")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")
			Language.setMessage("clsMasterData", 435, "Marriage Date")
			Language.setMessage("clsMasterData", 462, "Appointment Date")
			Language.setMessage("clsMasterData", 463, "Confirmation Date")
			Language.setMessage("clsMasterData", 464, "Birthdate")
			Language.setMessage("clsMasterData", 465, "Suspension Date")
			Language.setMessage("clsMasterData", 466, "Probation Date")
			Language.setMessage("clsMasterData", 467, "EOC Date")
			Language.setMessage("clsMasterData", 468, "Leaving Date")
			Language.setMessage("clsMasterData", 469, "Retirement Date")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
