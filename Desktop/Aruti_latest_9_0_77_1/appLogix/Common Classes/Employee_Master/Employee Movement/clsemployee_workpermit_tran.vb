﻿'************************************************************************************************************************************
'Class Name : clsemployee_workpermit_tran.vb
'Purpose    :
'Date       :31-Mar-2015
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Threading

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsemployee_workpermit_tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_workpermit_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintWorkpermittranunkid As Integer = 0
    Private mdtEffectivedate As Date = Nothing
    Private mintEmployeeunkid As Integer = 0
    Private mstrWork_Permit_No As String = String.Empty
    Private mintWorkcountryunkid As Integer = 0
    Private mstrIssue_Place As String = String.Empty
    Private mdtIssue_Date As Date = Nothing
    Private mdtExpiry_Date As Date = Nothing
    Private mintChangereasonunkid As Integer
    Private mblnIsfromemployee As Boolean = False
    Private mintUserunkid As Integer = 0
    Private mintStatusunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty
    Private mintRehiretranunkid As Integer = 0
    'S.SANDEEP [14 APR 2015] -- END

    'S.SANDEEP [04-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 120
    Private mblnIsResidentPermit As Boolean = False
    'S.SANDEEP [04-Jan-2018] -- END

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workpermittranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Workpermittranunkid(Optional ByVal objDataOper As clsDataOperation = Nothing) As Integer 'S.SANDEEP [25 OCT 2016] -- START {objDataOperation} -- END
        Get
            Return mintWorkpermittranunkid
        End Get
        Set(ByVal value As Integer)
            mintWorkpermittranunkid = value
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            'Call GetData()
            Call GetData(objDataOper)
            'S.SANDEEP [25 OCT 2016] -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set work_permit_no
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Work_Permit_No() As String
        Get
            Return mstrWork_Permit_No
        End Get
        Set(ByVal value As String)
            mstrWork_Permit_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workcountryunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Workcountryunkid() As Integer
        Get
            Return mintWorkcountryunkid
        End Get
        Set(ByVal value As Integer)
            mintWorkcountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issue_place
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issue_Place() As String
        Get
            Return mstrIssue_Place
        End Get
        Set(ByVal value As String)
            mstrIssue_Place = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issue_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issue_Date() As Date
        Get
            Return mdtIssue_Date
        End Get
        Set(ByVal value As Date)
            mdtIssue_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expiry_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expiry_Date() As Date
        Get
            Return mdtExpiry_Date
        End Get
        Set(ByVal value As Date)
            mdtExpiry_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfromemployee
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfromemployee() As Boolean
        Get
            Return mblnIsfromemployee
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromemployee = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'S.SANDEEP [14 APR 2015] -- START
    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property
    'S.SANDEEP [14 APR 2015] -- END

    'S.SANDEEP [04-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 120
    Public Property _IsResidentPermit() As Boolean
        Get
            Return mblnIsResidentPermit
        End Get
        Set(ByVal value As Boolean)
            mblnIsResidentPermit = value
        End Set
    End Property
    'S.SANDEEP [04-Jan-2018] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
              "  workpermittranunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", work_permit_no " & _
              ", workcountryunkid " & _
              ", issue_place " & _
              ", issue_date " & _
              ", expiry_date " & _
              ", changereasonunkid " & _
              ", isfromemployee " & _
              ", userunkid " & _
              ", statusunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", rehiretranunkid " & _
              ", ISNULL(isresidentpermit,0) AS isresidentpermit " & _
             "FROM hremployee_work_permit_tran " & _
             "WHERE workpermittranunkid = @workpermittranunkid " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END
            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {isresidentpermit} -- END

            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkpermittranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintWorkpermittranunkid = CInt(dtRow.Item("workpermittranunkid"))
                mdtEffectivedate = dtRow.Item("effectivedate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrWork_Permit_No = dtRow.Item("work_permit_no").ToString
                mintWorkcountryunkid = CInt(dtRow.Item("workcountryunkid"))
                mstrIssue_Place = dtRow.Item("issue_place").ToString
                If IsDBNull(dtRow.Item("issue_date")) = False Then
                    mdtIssue_Date = dtRow.Item("issue_date")
                Else
                    mdtIssue_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("expiry_date")) = False Then
                    mdtExpiry_Date = dtRow.Item("expiry_date")
                Else
                    mdtExpiry_Date = Nothing
                End If
                mintChangereasonunkid = CInt(dtRow.Item("changereasonunkid"))
                mblnIsfromemployee = CBool(dtRow.Item("isfromemployee"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'S.SANDEEP [14 APR 2015] -- START
                mintRehiretranunkid = dtRow.Item("rehiretranunkid").ToString
                'S.SANDEEP [14 APR 2015] -- END

                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                mblnIsResidentPermit = CBool(dtRow.Item("isresidentpermit"))
                'S.SANDEEP [04-Jan-2018] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal blnIsResidentPermit As Boolean, Optional ByVal xEmployeeUnkid As Integer = 0) As DataSet 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            strQ = "SELECT " & _
                   "  hremployee_work_permit_tran.workpermittranunkid " & _
                   ", hremployee_work_permit_tran.effectivedate " & _
                   ", hremployee_work_permit_tran.employeeunkid " & _
                   ", hremployee_work_permit_tran.work_permit_no " & _
                   ", hremployee_work_permit_tran.workcountryunkid " & _
                   ", hremployee_work_permit_tran.issue_place " & _
                   ", hremployee_work_permit_tran.issue_date " & _
                   ", hremployee_work_permit_tran.expiry_date " & _
                   ", hremployee_work_permit_tran.changereasonunkid " & _
                   ", hremployee_work_permit_tran.isfromemployee " & _
                   ", hremployee_work_permit_tran.userunkid " & _
                   ", hremployee_work_permit_tran.statusunkid " & _
                   ", hremployee_work_permit_tran.isvoid " & _
                   ", hremployee_work_permit_tran.voiduserunkid " & _
                   ", hremployee_work_permit_tran.voiddatetime " & _
                   ", hremployee_work_permit_tran.voidreason " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", CONVERT(CHAR(8),hremployee_work_permit_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", CONVERT(CHAR(8),hremployee_work_permit_tran.issue_date,112) AS isdate " & _
                   ", CONVERT(CHAR(8),hremployee_work_permit_tran.expiry_date,112) AS exdate " & _
                   ", '' AS EffDate " & _
                   ", '' AS IDate " & _
                   ", '' AS ExDate " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE " & _
                   "        CASE WHEN ISNULL(hremployee_work_permit_tran.isresidentpermit,0) <=0 THEN ISNULL(cfcommon_master.name,'') " & _
                   "        ELSE ISNULL(RP.name,'') END " & _
                   "  END AS CReason " & _
                   ", ISNULL(CM.country_name,'') AS Country " & _
                   ", ISNULL(hremployee_work_permit_tran.rehiretranunkid,0) AS rehiretranunkid " & _
                   ", ISNULL(hremployee_work_permit_tran.isresidentpermit,0) AS isresidentpermit " & _
                   "FROM hremployee_work_permit_tran " & _
                   "    LEFT JOIN hrmsConfiguration..cfcountry_master AS CM ON CM.countryunkid = hremployee_work_permit_tran.workcountryunkid " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_work_permit_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.WORK_PERMIT & _
                   "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_work_permit_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "    LEFT JOIN cfcommon_master AS RP ON RP.masterunkid = hremployee_work_permit_tran.changereasonunkid AND RP.mastertype = " & clsCommon_Master.enCommonMaster.RESIDENT_PERMIT & _
                   "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_work_permit_tran.employeeunkid " & _
                   "WHERE isvoid = 0 AND ISNULL(hremployee_work_permit_tran.isresidentpermit,0) = @isresidentpermit " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END
            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END


            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsResidentPermit)
            'S.SANDEEP [04-Jan-2018] -- END

            If xEmployeeUnkid Then
                strQ &= " AND hremployee_work_permit_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_work_permit_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
                If xRow.Item("isdate").ToString.Length > 0 Then
                    xRow("IDate") = eZeeDate.convertDate(xRow.Item("isdate").ToString).ToShortDateString
                End If
                If xRow.Item("exdate").ToString.Trim.Length > 0 Then
                    xRow("ExDate") = eZeeDate.convertDate(xRow.Item("exdate").ToString).ToShortDateString
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_work_permit_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        If isExist(mblnIsResidentPermit, mdtEffectivedate, , mintEmployeeunkid, mintWorkpermittranunkid, objDataOperation) Then 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, work permit information is already present for the selected effective date.")
            If mblnIsResidentPermit Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, resident permit information is already present for the selected effective date.")
            Else
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, work permit information is already present for the selected effective date.")
            End If
            'S.SANDEEP [04-Jan-2018] -- END
            Return False
        End If


        'Pinkal (09-Apr-2015) -- Start
        'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

        'S.SANDEEP [29 APR 2015] -- START
        'ALLOW SAME WORK PERMIT IF EMPLOYEE IS REHIRED AND USER WANT PUT IN SAME

        'dsList = Get_Current_WorkPermit(mdtEffectivedate.Date, mintEmployeeunkid)
        'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
        '    If dsList.Tables(0).Rows(0)("work_permit_no").ToString().Trim = mstrWork_Permit_No AndAlso CInt(dsList.Tables(0).Rows(0)("workcountryunkid")) = mintWorkcountryunkid Then
        '        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
        '        Return False
        '    End If
        'End If
        'dsList = Nothing

        If mintRehiretranunkid <= 0 Then
            dsList = Get_Current_WorkPermit(mdtEffectivedate.Date, mblnIsResidentPermit, mintEmployeeunkid) 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            If dsList.Tables(0).Rows(0)("work_permit_no").ToString().Trim = mstrWork_Permit_No AndAlso CInt(dsList.Tables(0).Rows(0)("workcountryunkid")) = mintWorkcountryunkid Then
                    'S.SANDEEP [04-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 120
                    'mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
                    If mblnIsResidentPermit Then
                        mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, resident permit no is already present for the selected employee.")
                    Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
                    End If
                    'S.SANDEEP [04-Jan-2018] -- END
                Return False
            End If
        End If
        dsList = Nothing
        End If
        'S.SANDEEP [29 APR 2015] -- END

        'Pinkal (09-Apr-2015) -- End


        If isExist(mblnIsResidentPermit, Nothing, mstrWork_Permit_No, mintEmployeeunkid, mintWorkpermittranunkid, objDataOperation) Then 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
            If mblnIsResidentPermit Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, resident permit no is already present for the selected employee.")
            Else
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
            End If
            'S.SANDEEP [04-Jan-2018] -- END
            Return False
        End If

        If xDataOpr Is Nothing Then
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@work_permit_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWork_Permit_No.ToString)
            objDataOperation.AddParameter("@workcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkcountryunkid.ToString)
            objDataOperation.AddParameter("@issue_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIssue_Place.ToString)
            If mdtIssue_Date <> Nothing Then
                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIssue_Date)
            Else
                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtExpiry_Date <> Nothing Then
                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtExpiry_Date)
            Else
                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [14 APR 2015] -- START
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsResidentPermit)
            'S.SANDEEP [04-Jan-2018] -- END

            strQ = "INSERT INTO hremployee_work_permit_tran ( " & _
                       "  effectivedate " & _
                       ", employeeunkid " & _
                       ", work_permit_no " & _
                       ", workcountryunkid " & _
                       ", issue_place " & _
                       ", issue_date " & _
                       ", expiry_date " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", userunkid " & _
                       ", statusunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                       ", rehiretranunkid" & _
                       ", isresidentpermit" & _
                   ") VALUES (" & _
                       "  @effectivedate " & _
                       ", @employeeunkid " & _
                       ", @work_permit_no " & _
                       ", @workcountryunkid " & _
                       ", @issue_place " & _
                       ", @issue_date " & _
                       ", @expiry_date " & _
                       ", @changereasonunkid " & _
                       ", @isfromemployee " & _
                       ", @userunkid " & _
                       ", @statusunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                       ", @rehiretranunkid " & _
                       ", @isresidentpermit " & _
                   "); SELECT @@identity" 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END
            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintWorkpermittranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForWorkPermit(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_work_permit_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        If isExist(mblnIsResidentPermit, mdtEffectivedate, , mintEmployeeunkid, mintWorkpermittranunkid, objDataOperation) Then 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, work permit information is already present for the selected effective date.")
            If mblnIsResidentPermit Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, resident permit information is already present for the selected effective date.")
            Else
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, work permit information is already present for the selected effective date.")
            End If
            'S.SANDEEP [04-Jan-2018] -- END
            Return False
        End If

        If isExist(mblnIsResidentPermit, Nothing, mstrWork_Permit_No, mintEmployeeunkid, mintWorkpermittranunkid, objDataOperation) Then 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, work permit information is already present for the selected effective date.")
            If mblnIsResidentPermit Then
                mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, resident permit no is already present for the selected employee.")
            Else
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
            End If
            'S.SANDEEP [04-Jan-2018] -- END
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkpermittranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@work_permit_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWork_Permit_No.ToString)
            objDataOperation.AddParameter("@workcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkcountryunkid.ToString)
            objDataOperation.AddParameter("@issue_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIssue_Place.ToString)
            If mdtIssue_Date <> Nothing Then
                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIssue_Date)
            Else
                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtExpiry_Date <> Nothing Then
                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtExpiry_Date)
            Else
                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [14 APR 2015] -- START 
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsResidentPermit)
            'S.SANDEEP [04-Jan-2018] -- END

            strQ = "UPDATE hremployee_work_permit_tran SET " & _
                   "  effectivedate = @effectivedate" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", work_permit_no = @work_permit_no" & _
                   ", workcountryunkid = @workcountryunkid" & _
                   ", issue_place = @issue_place" & _
                   ", issue_date = @issue_date" & _
                   ", expiry_date = @expiry_date" & _
                   ", changereasonunkid = @changereasonunkid" & _
                   ", isfromemployee = @isfromemployee" & _
                   ", userunkid = @userunkid" & _
                   ", statusunkid = @statusunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   ", rehiretranunkid = @rehiretranunkid " & _
                   ", isresidentpermit = @isresidentpermit " & _
                   "WHERE workpermittranunkid = @workpermittranunkid " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END
            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForWorkPermit(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_work_permit_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [01 JUN 2016] -- START -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "UPDATE hremployee_work_permit_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE workpermittranunkid = @workpermittranunkid "

            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintWorkpermittranunkid = intUnkid
            Call GetData(objDataOperation)

            If InsertAuditTrailForWorkPermit(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal blnIsResidentPermit As Boolean, _
                            Optional ByVal xEffDate As DateTime = Nothing, _
                            Optional ByVal xWPermitNo As String = "", _
                            Optional ByVal xEmployeeId As Integer = 0, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
                       "  workpermittranunkid " & _
                       ", effectivedate " & _
                       ", employeeunkid " & _
                       ", work_permit_no " & _
                       ", workcountryunkid " & _
                       ", issue_place " & _
                       ", issue_date " & _
                       ", expiry_date " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", userunkid " & _
                       ", statusunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", isresidentpermit " & _
                   "FROM hremployee_work_permit_tran " & _
                   "WHERE isvoid = 0 AND isresidentpermit = @isresidentpermit "
            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END

            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsResidentPermit) 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END

            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If


            'Pinkal (09-Apr-2015) -- Start
            'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
            If xWPermitNo.Trim.Length > 0 Then
                strQ &= " AND work_permit_no = @work_permit_no "
                objDataOperation.AddParameter("@work_permit_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWork_Permit_No)
            End If
            'Pinkal (09-Apr-2015) -- End

            If intUnkid > 0 Then
                strQ &= " AND workpermittranunkid <> @workpermittranunkid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function Get_Current_WorkPermit(ByVal xDate As Date, ByVal blnIsResidentPermit As Boolean, Optional ByVal xEmployeeId As Integer = 0) As DataSet 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        'S.SANDEEP [07 APR 2015] -- START
        Dim mdtAppDate As String = ""
        'S.SANDEEP [07 APR 2015] -- END
        Try
            Using objDo As New clsDataOperation

                'S.SANDEEP [07 APR 2015] -- START
                If xEmployeeId > 0 Then
                    StrQ = "SELECT " & _
                           "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                           "FROM hremployee_master " & _
                           "WHERE employeeunkid = '" & xEmployeeId & "' "

                    objDo.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    End If

                    mdtAppDate = objDo.GetParameterValue("@ADate")
                End If
                'S.SANDEEP [07 APR 2015] -- END

                StrQ = "SELECT " & _
                       "   effectivedate " & _
                       "  ,employeeunkid " & _
                       "  ,work_permit_no " & _
                       "  ,workcountryunkid " & _
                       "  ,issue_place " & _
                       "  ,issue_date " & _
                       "  ,expiry_date " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         effectivedate " & _
                       "        ,employeeunkid " & _
                       "        ,work_permit_no " & _
                       "        ,workcountryunkid " & _
                       "        ,issue_place " & _
                       "        ,issue_date " & _
                       "        ,expiry_date " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "    FROM hremployee_work_permit_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effdate AND hremployee_work_permit_tran.isresidentpermit = @isresidentpermit " 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
                If xEmployeeId > 0 Then
                    StrQ &= " AND employeeunkid = '" & xEmployeeId & "' "
                End If
                StrQ &= ") AS CA WHERE CA.xNo = 1 "

                'S.SANDEEP [07 APR 2015] -- START
                If mdtAppDate.Trim.Length > 0 Then
                    StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) >= '" & mdtAppDate & "' "
                End If
                'S.SANDEEP [07 APR 2015] -- END

                objDo.AddParameter("@effdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                objDo.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsResidentPermit)
                'S.SANDEEP [04-Jan-2018] -- END

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Current_WorkPermit; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Private Function InsertAuditTrailForWorkPermit(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO athremployee_work_permit_tran ( " & _
                       "  workpermittranunkid " & _
                       ", effectivedate " & _
                       ", employeeunkid " & _
                       ", work_permit_no " & _
                       ", workcountryunkid " & _
                       ", issue_place " & _
                       ", issue_date " & _
                       ", expiry_date " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", statusunkid " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb" & _
                       ", rehiretranunkid" & _
                       ", isresidentpermit " & _
                   ") VALUES (" & _
                       "  @workpermittranunkid " & _
                       ", @effectivedate " & _
                       ", @employeeunkid " & _
                       ", @work_permit_no " & _
                       ", @workcountryunkid " & _
                       ", @issue_place " & _
                       ", @issue_date " & _
                       ", @expiry_date " & _
                       ", @changereasonunkid " & _
                       ", @isfromemployee " & _
                       ", @statusunkid " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", @isweb" & _
                       ", @rehiretranunkid " & _
                       ", @isresidentpermit " & _
                   ") " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END
            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {isresidentpermit} -- END

            objDataOperation.ClearParameters()
            'S.SANDEEP [14 APR 2015] -- START
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
            'S.SANDEEP [14 APR 2015] -- END
            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkpermittranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@work_permit_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWork_Permit_No.ToString)
            objDataOperation.AddParameter("@workcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkcountryunkid.ToString)
            objDataOperation.AddParameter("@issue_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIssue_Place.ToString)
            If mdtIssue_Date <> Nothing Then
                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIssue_Date)
            Else
                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtExpiry_Date <> Nothing Then
                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtExpiry_Date)
            Else
                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsResidentPermit)
            'S.SANDEEP [04-Jan-2018] -- END

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    Public Function IsPermitPresent(ByVal xEmployeeId As Integer, ByVal blnIsResidentPermit As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = 0
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT 1 FROM hremployee_work_permit_tran WHERE employeeunkid = '" & xEmployeeId & "'  AND isvoid = 0 AND isresidentpermit = @isresidentpermit "
            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {isresidentpermit} -- END

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsResidentPermit)
            'S.SANDEEP [04-Jan-2018] -- END

            iCnt = objDataOperation.RecordCount(StrQ)

            If iCnt > 0 Then
                blnFlag = True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPermitPresent; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return blnFlag
    End Function

    'S.SANDEEP [23-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002339|ARUTI-214}
    Public Function GetPermitTranId(ByVal xPermitNumber As String, ByVal xEmployeeId As Integer, ByVal blnIsResidentPermit As Boolean, ByVal xEffectiveDate As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim intTranId As Integer = 0
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT " & _
                   "   workpermittranunkid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         workpermittranunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                   "    FROM hremployee_work_permit_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effdate AND hremployee_work_permit_tran.isresidentpermit = @isresidentpermit "
            If xEmployeeId > 0 Then
                StrQ &= " AND employeeunkid = '" & xEmployeeId & "' "
            End If
            StrQ &= ") AS CA WHERE CA.xNo = 1 "

            objDataOperation.AddParameter("@effdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffectiveDate))
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsResidentPermit)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intTranId = CInt(dsList.Tables(0).Rows(0)("workpermittranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPermitTranId; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return intTranId
    End Function
    'S.SANDEEP [23-JUN-2018] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, work permit information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
			Language.setMessage(mstrModuleName, 3, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
