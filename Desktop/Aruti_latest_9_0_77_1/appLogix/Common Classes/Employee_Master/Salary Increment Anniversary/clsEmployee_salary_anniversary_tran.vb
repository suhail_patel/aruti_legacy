﻿'************************************************************************************************************************************
'Class Name : clsEmployee_salary_anniversary_tran.vb
'Purpose    :
'Date       :21/05/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmployee_salary_anniversary_tran
    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_salary_anniversary_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEmpsalaryanniversaryunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtEffectivedate As Date
    Private mintAnniversarymonth As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""

    'S.SANDEEP [25 OCT 2016] -- START
    'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
    Private mintAnniversaryMonthBy As Integer = 0
    'S.SANDEEP [25 OCT 2016] -- END


#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empsalaryanniversaryunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Empsalaryanniversaryunkid(Optional ByVal objDataOper As clsDataOperation = Nothing) As Integer 'S.SANDEEP [25 OCT 2016] -- START {objDataOperation} -- END
        Get
            Return mintEmpsalaryanniversaryunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpsalaryanniversaryunkid = value
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            'Call GetData()
            Call GetData(objDataOper)
            'S.SANDEEP [25 OCT 2016] -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set anniversarymonth
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Anniversarymonth() As Integer
        Get
            Return mintAnniversarymonth
        End Get
        Set(ByVal value As Integer)
            mintAnniversarymonth = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property


    'S.SANDEEP [25 OCT 2016] -- START
    'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
    Public Property _AnniversaryMonthBy() As Integer
        Get
            Return mintAnniversaryMonthBy
        End Get
        Set(ByVal value As Integer)
            mintAnniversaryMonthBy = value
        End Set
    End Property
    'S.SANDEEP [25 OCT 2016] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
        objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [25 OCT 2016] -- START
        'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
        objDataOperation.ClearParameters()
        'S.SANDEEP [25 OCT 2016] -- END


        Try
            strQ = "SELECT " & _
              "  empsalaryanniversaryunkid " & _
              ", employeeunkid " & _
              ", effectivedate " & _
              ", anniversarymonth " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", anniversarymonthby " & _
             "FROM hremployee_salary_anniversary_tran " & _
             "WHERE empsalaryanniversaryunkid = @empsalaryanniversaryunkid "
            'S.SANDEEP [25 OCT 2016] -- START {anniversarymonthby} -- END



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@empsalaryanniversaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpsalaryanniversaryunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpsalaryanniversaryunkid = CInt(dtRow.Item("empsalaryanniversaryunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtEffectivedate = dtRow.Item("effectivedate")
                mintAnniversarymonth = CInt(dtRow.Item("anniversarymonth"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                'S.SANDEEP [25 OCT 2016] -- START
                'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                'mdtVoiddatetime = dtRow.Item("voiddatetime")
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                'S.SANDEEP [25 OCT 2016] -- END
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'S.SANDEEP [25 OCT 2016] -- START
                'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                mintAnniversaryMonthBy = CInt(dtRow.Item("anniversarymonthby"))
                'S.SANDEEP [25 OCT 2016] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strInnerFiler As String = "", _
                            Optional ByVal strOuterFiler As String = "", _
                            Optional ByVal strOrderBy As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal blnShowAllAsOnDate As Boolean = True) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT  * " & _
                    "FROM    ( SELECT  ISNULL(hremployee_salary_anniversary_tran.empsalaryanniversaryunkid, -1) AS empsalaryanniversaryunkid " & _
                                      ", hremployee_master.employeeunkid " & _
                                      ", hremployee_master.employeecode " & _
                                      ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename  " & _
                                      ", hremployee_master.appointeddate AS appointeddate " & _
                                      ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddatestring " & _
                                      ", hremployee_master.confirmation_date AS confirmation_date " & _
                                      ", CONVERT(CHAR(8), hremployee_master.confirmation_date, 112) AS confirmation_datestring  " & _
                                      ", hremployee_salary_anniversary_tran.effectivedate AS effectivedate " & _
                                      ", ISNULL(CONVERT(CHAR(8), hremployee_salary_anniversary_tran.effectivedate, 112), '') AS effectivedatestring " & _
                                      ", hremployee_salary_anniversary_tran.anniversarymonth " & _
                                      ", hremployee_salary_anniversary_tran.userunkid " & _
                                      ", hremployee_salary_anniversary_tran.isvoid " & _
                                      ", hremployee_salary_anniversary_tran.voiduserunkid " & _
                                      ", hremployee_salary_anniversary_tran.voiddatetime " & _
                                      ", hremployee_salary_anniversary_tran.voidreason " & _
                                      ", CAST(0 AS BIT) AS IsChecked " & _
                                      ", CASE WHEN hremployee_salary_anniversary_tran.effectivedate IS NULL THEN hremployee_master.appointeddate ELSE CAST(NULL AS DATETIME) END AS neweffectivedate " & _
                                      ", NULL AS newanniversarymonth " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY hremployee_master.employeeunkid ORDER BY hremployee_salary_anniversary_tran.effectivedate DESC, hremployee_salary_anniversary_tran.empsalaryanniversaryunkid DESC ) AS ROW_NO " & _
                                      ", hremployee_salary_anniversary_tran.anniversarymonthby " & _
                                "FROM    hremployee_master " & _
                                        "LEFT JOIN hremployee_salary_anniversary_tran ON hremployee_master.employeeunkid = hremployee_salary_anniversary_tran.employeeunkid "
            'S.SANDEEP [25 OCT 2016] -- START {anniversarymonthby} -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            

            strQ &= "           WHERE   hremployee_salary_anniversary_tran.isvoid = 0 " & _
                               "AND CONVERT(CHAR(8), ISNULL(hremployee_salary_anniversary_tran.effectivedate, hremployee_master.appointeddate), 112) <= @enddate "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strInnerFiler.Trim.Length > 0 Then
                strQ &= " AND " & strInnerFiler
            End If

            strQ &= "           ) AS A " & _
                    " WHERE 1=1 "

            If blnShowAllAsOnDate = False Then
                strQ &= " AND A.ROW_NO = 1 "
            End If

            If strOuterFiler.Trim.Length > 0 Then
                strQ &= " AND " & strOuterFiler
            End If

            If strOrderBy.Trim <> "" Then
                strQ &= " ORDER BY " & strOrderBy
            End If

            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (22 Sep 2016) -- Start
    'Issue - 63.1 - Employees were not coming in list on assign screen once annivarsary transaction deleted.
    Public Function GetListForAssignment(ByVal xDatabaseName As String, _
                                         ByVal xUserUnkid As Integer, _
                                         ByVal xYearUnkid As Integer, _
                                         ByVal xCompanyUnkid As Integer, _
                                         ByVal xPeriodStart As DateTime, _
                                         ByVal xPeriodEnd As DateTime, _
                                         ByVal xUserModeSetting As String, _
                                         ByVal xOnlyApproved As Boolean, _
                                         ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                         ByVal strTableName As String, _
                                         Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                         Optional ByVal strInnerFiler As String = "", _
                                         Optional ByVal strOuterFiler As String = "", _
                                         Optional ByVal strOrderBy As String = "", _
                                         Optional ByVal IsUsedAsMSS As Boolean = True, _
                                         Optional ByVal blnShowAllAsOnDate As Boolean = True) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT  * " & _
                    "FROM    ( SELECT  ISNULL(hremployee_salary_anniversary_tran.empsalaryanniversaryunkid, -1) AS empsalaryanniversaryunkid " & _
                                      ", hremployee_master.employeeunkid " & _
                                      ", hremployee_master.employeecode " & _
                                      ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename  " & _
                                      ", hremployee_master.appointeddate AS appointeddate " & _
                                      ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddatestring " & _
                                      ", hremployee_master.confirmation_date AS confirmation_date " & _
                                      ", CONVERT(CHAR(8), hremployee_master.confirmation_date, 112) AS confirmation_datestring  " & _
                                      ", hremployee_salary_anniversary_tran.effectivedate AS effectivedate " & _
                                      ", ISNULL(CONVERT(CHAR(8), hremployee_salary_anniversary_tran.effectivedate, 112), '') AS effectivedatestring " & _
                                      ", hremployee_salary_anniversary_tran.anniversarymonth " & _
                                      ", hremployee_salary_anniversary_tran.userunkid " & _
                                      ", hremployee_salary_anniversary_tran.isvoid " & _
                                      ", hremployee_salary_anniversary_tran.voiduserunkid " & _
                                      ", hremployee_salary_anniversary_tran.voiddatetime " & _
                                      ", hremployee_salary_anniversary_tran.voidreason " & _
                                      ", CAST(0 AS BIT) AS IsChecked " & _
                                      ", CASE WHEN hremployee_salary_anniversary_tran.effectivedate IS NULL THEN hremployee_master.appointeddate ELSE CAST(NULL AS DATETIME) END AS neweffectivedate " & _
                                      ", NULL AS newanniversarymonth " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY hremployee_master.employeeunkid ORDER BY hremployee_salary_anniversary_tran.effectivedate DESC, hremployee_salary_anniversary_tran.empsalaryanniversaryunkid DESC ) AS ROW_NO " & _
                                      ", hremployee_salary_anniversary_tran.anniversarymonthby " & _
                                "FROM    hremployee_master " & _
                                        "LEFT JOIN hremployee_salary_anniversary_tran ON hremployee_master.employeeunkid = hremployee_salary_anniversary_tran.employeeunkid " & _
                                                "AND hremployee_salary_anniversary_tran.isvoid = 0 "
            'S.SANDEEP [25 OCT 2016] -- START {anniversarymonthby} -- END


            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "           WHERE   CONVERT(CHAR(8), ISNULL(hremployee_salary_anniversary_tran.effectivedate, hremployee_master.appointeddate), 112) <= @enddate "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strInnerFiler.Trim.Length > 0 Then
                strQ &= " AND " & strInnerFiler
            End If

            strQ &= "           ) AS A " & _
                    " WHERE 1=1 "

            If blnShowAllAsOnDate = False Then
                strQ &= " AND A.ROW_NO = 1 "
            End If

            If strOuterFiler.Trim.Length > 0 Then
                strQ &= " AND " & strOuterFiler
            End If

            If strOrderBy.Trim <> "" Then
                strQ &= " ORDER BY " & strOrderBy
            End If

            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListForAssignment; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (22 Sep 2016) -- End


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_salary_anniversary_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As DateTime, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEffectivedate <> Nothing, mdtEffectivedate, DBNull.Value))
            objDataOperation.AddParameter("@anniversarymonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnniversarymonth.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            objDataOperation.AddParameter("@anniversarymonthby", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnniversaryMonthBy)
            'S.SANDEEP [25 OCT 2016] -- END

            strQ = "INSERT INTO hremployee_salary_anniversary_tran ( " & _
              "  employeeunkid " & _
              ", effectivedate " & _
              ", anniversarymonth " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", anniversarymonthby " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @effectivedate " & _
              ", @anniversarymonth " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @anniversarymonthby " & _
            "); SELECT @@identity"
            'S.SANDEEP [25 OCT 2016] -- START {anniversarymonthby} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpsalaryanniversaryunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForSalaryAnniversaryTran(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_salary_anniversary_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As DateTime, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintEmpsalaryanniversaryunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        Try
            objDataOperation.AddParameter("@empsalaryanniversaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpsalaryanniversaryunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEffectivedate <> Nothing, mdtEffectivedate, DBNull.Value))
            objDataOperation.AddParameter("@anniversarymonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnniversarymonth.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            objDataOperation.AddParameter("@anniversarymonthby", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnniversaryMonthBy)
            'S.SANDEEP [25 OCT 2016] -- END

            strQ = "UPDATE hremployee_salary_anniversary_tran SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", effectivedate = @effectivedate" & _
              ", anniversarymonth = @anniversarymonth" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", anniversarymonthby = @anniversarymonthby " & _
            "WHERE empsalaryanniversaryunkid = @empsalaryanniversaryunkid "
            'S.SANDEEP [25 OCT 2016] -- START {anniversarymonthby} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForSalaryAnniversaryTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_salary_anniversary_tran) </purpose>
    Public Function Delete(ByVal intEmpSalAnniversaryunkid As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            strQ = "UPDATE hremployee_salary_anniversary_tran SET " & _
                        "  isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
            "WHERE empsalaryanniversaryunkid = @empsalaryanniversaryunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@empsalaryanniversaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpSalAnniversaryunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            mintEmpsalaryanniversaryunkid = intEmpSalAnniversaryunkid
            Call GetData(objDataOperation)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForSalaryAnniversaryTran(objDataOperation, 3, dtCurrentDateAndTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal strEmpSalAnniversaryIds As String) As Boolean
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""

        objDataOperation = New clsDataOperation
        Try
            objDataOperation.BindTransaction()

            strQ = "INSERT INTO athremployee_salary_anniversary_tran " & _
                   "( " & _
                        "  empsalaryanniversaryunkid " & _
                        ", employeeunkid " & _
                        ", effectivedate " & _
                        ", anniversarymonth " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                        ", loginemployeeunkid " & _
                   ") " & _
                   "SELECT " & _
                        "  empsalaryanniversaryunkid " & _
                        ", employeeunkid " & _
                        ", effectivedate " & _
                        ", anniversarymonth " & _
                        ", 3 " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", '" & mstrWebIP & "' " & _
                        ", '" & mstrWebhostName & "' " & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                        ", @loginemployeeunkid " & _
                    "FROM hremployee_salary_anniversary_tran " & _
                    "WHERE isvoid = 0 AND empsalaryanniversaryunkid IN(" & strEmpSalAnniversaryIds & ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If




            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE hremployee_salary_anniversary_tran SET " & _
                        "  isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                   "WHERE empsalaryanniversaryunkid IN(" & strEmpSalAnniversaryIds & ") "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "VoidAll", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertUpdateDelete(ByVal dtTable As DataTable, ByVal dtCurrentDateAndTime As Date) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                objDataOperation.ClearParameters()

                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtEffectivedate = CDate(dtRow.Item("neweffectivedate"))
                mintAnniversarymonth = CInt(dtRow.Item("newanniversarymonth"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'S.SANDEEP [25 OCT 2016] -- START
                'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                mintAnniversaryMonthBy = CInt(dtRow.Item("anniversarymonthby"))
                'S.SANDEEP [25 OCT 2016] -- END

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
                objDataOperation.AddParameter("@anniversarymonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnniversarymonth)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                If mdtVoiddatetime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                End If
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

                'S.SANDEEP [25 OCT 2016] -- START
                'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                objDataOperation.AddParameter("@anniversarymonthby", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnniversaryMonthBy)
                'S.SANDEEP [25 OCT 2016] -- END



                strQ = "INSERT INTO hremployee_salary_anniversary_tran ( " & _
                              "  employeeunkid " & _
                              ", effectivedate " & _
                              ", anniversarymonth " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiduserunkid " & _
                              ", voiddatetime " & _
                              ", voidreason" & _
                              ", anniversarymonthby " & _
                    ") VALUES (" & _
                              "  @employeeunkid " & _
                              ", @effectivedate " & _
                              ", @anniversarymonth " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiduserunkid " & _
                              ", @voiddatetime " & _
                              ", @voidreason" & _
                              ", @anniversarymonthby " & _
                    "); SELECT @@identity"
                'S.SANDEEP [25 OCT 2016] -- START {anniversarymonthby}  -- END



                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintEmpsalaryanniversaryunkid = dsList.Tables(0).Rows(0).Item(0)

                If InsertAuditTrailForSalaryAnniversaryTran(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Next

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@empsalaryanniversaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  empsalaryanniversaryunkid " & _
              ", employeeunkid " & _
              ", effectivedate " & _
              ", anniversarymonth " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hremployee_salary_anniversary_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND empsalaryanniversaryunkid <> @empsalaryanniversaryunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@empsalaryanniversaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrailForSalaryAnniversaryTran(ByVal objDataOperation As clsDataOperation _
                                                            , ByVal AuditType As Integer _
                                                            , ByVal dtCurrentDateAndTime As DateTime _
                                                            ) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            strQ = "INSERT INTO athremployee_salary_anniversary_tran ( " & _
                       "  empsalaryanniversaryunkid " & _
                       ", employeeunkid " & _
                       ", effectivedate " & _
                       ", anniversarymonth " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name" & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb " & _
                       ", loginemployeeunkid " & _
                       ", anniversarymonthby " & _
             ") VALUES (" & _
                       "  @empsalaryanniversaryunkid " & _
                       ", @employeeunkid " & _
                       ", @effectivedate " & _
                       ", @anniversarymonth " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name" & _
                       ", @form_name " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         ", @isweb " & _
                         ", @loginemployeeunkid " & _
                       ", @anniversarymonthby " & _
                 "); SELECT @@identity"
            'S.SANDEEP [25 OCT 2016] -- START {anniversarymonthby} -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@empsalaryanniversaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpsalaryanniversaryunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEffectivedate <> Nothing, mdtEffectivedate, DBNull.Value))
            objDataOperation.AddParameter("@anniversarymonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnniversarymonth)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(dtCurrentDateAndTime <> Nothing, dtCurrentDateAndTime, DBNull.Value))
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            objDataOperation.AddParameter("@anniversarymonthby", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnniversaryMonthBy)
            'S.SANDEEP [25 OCT 2016] -- END

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If




            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForSalaryAnniversaryTran, Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class