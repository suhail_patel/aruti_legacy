﻿'************************************************************************************************************************************
'Class Name : clsBackup.vb
'Purpose    :
'Date       :07/09/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib


Public Class clsBackup
    Private Shared ReadOnly mstrModuleName As String = "clsBackup"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBackupunkid As Integer
    Private mdtBackup_Date As Date
    Private mstrBackup_Path As String = String.Empty
    Private mchrBackup_Type As Char = "M"
    Private mintUserunkid As Integer
    Private mchrIsdeleted As Char = "N"
    Private mblnIsconfiguration As Boolean
    Private mintCompanyunkid As Integer
    Private mintYearunkid As Integer
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set backupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Backupunkid() As Integer
        Get
            Return mintBackupunkid
        End Get
        Set(ByVal value As Integer)
            mintBackupunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set backup_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Backup_Date() As Date
        Get
            Return mdtBackup_Date
        End Get
        Set(ByVal value As Date)
            mdtBackup_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set backup_path
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Backup_Path() As String
        Get
            Return mstrBackup_Path
        End Get
        Set(ByVal value As String)
            mstrBackup_Path = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set backup_type
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Backup_Type() As Char
        Get
            Return mchrBackup_Type
        End Get
        Set(ByVal value As Char)
            mchrBackup_Type = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdeleted
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isdeleted() As Char
        Get
            Return mchrIsdeleted
        End Get
        Set(ByVal value As Char)
            mchrIsdeleted = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isconfiguration
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isconfiguration() As Boolean
        Get
            Return mblnIsconfiguration
        End Get
        Set(ByVal value As Boolean)
            mblnIsconfiguration = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = Value
        End Set
    End Property

#End Region


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  backupunkid " & _
              ", backup_date " & _
              ", backup_path " & _
              ", backup_type " & _
              ", userunkid " & _
              ", isdeleted " & _
              ", isconfiguration " & _
              ", companyunkid " & _
              ", yearunkid " & _
             "FROM hrmsConfiguration..cfbackup " & _
             "WHERE backupunkid = @backupunkid "

            objDataOperation.AddParameter("@backupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBackupUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbackupunkid = CInt(dtRow.Item("backupunkid"))
                mdtbackup_date = dtRow.Item("backup_date")
                mstrbackup_path = dtRow.Item("backup_path").ToString
                mchrbackup_type = dtRow.Item("backup_type")
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mchrisdeleted = dtRow.Item("isdeleted")
                mblnisconfiguration = CBool(dtRow.Item("isconfiguration"))
                mintcompanyunkid = CInt(dtRow.Item("companyunkid"))
                mintyearunkid = CInt(dtRow.Item("yearunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Sandeep [ 10 FEB 2011 ] -- Start
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get Backup History List </purpose>
    ''' Public Function getList(ByVal isConfig As Boolean, ByVal intCompanyId As Integer) As DataSet
    Public Function getList() As DataSet
        'Sandeep [ 10 FEB 2011 ] -- End 
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      " cfbackup.backupunkid " & _
                      ",CONVERT(CHAR(8),cfbackup.backup_date,112) As backup_date " & _
                      ",CONVERT(CHAR(8),cfbackup.backup_date,108) As backup_time " & _
                      ",cfbackup.backup_path " & _
                      ",cfbackup.backup_type " & _
                      ",cfbackup.userunkid " & _
                      ",cfbackup.isdeleted " & _
                      ",cfbackup.isconfiguration " & _
                      ",cfbackup.companyunkid " & _
                      ",cfbackup.yearunkid " & _
                      ",ISNULL(cfuser_master.username,'') AS UserName " & _
                    "FROM hrmsConfiguration..cfbackup " & _
                    "LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfbackup.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                    "WHERE ISNULL(isdeleted,'') = 'N' "


            'Sandeep [ 10 FEB 2011 ] -- Start
            'If isConfig = True Then
            '    strQ &= " And ISNULL(cfbackup.isconfiguration,0) = 1 "
            'Else
            '    strQ &= " And ISNULL(cfbackup.isconfiguration,0) = 0 "
            'End If


            'If intCompanyId > 0 Then
            '    strQ &= " AND cfbackup.companyunkid = @CompanyId "
            '    objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            'End If
            'Sandeep [ 10 FEB 2011 ] -- End 


            
            strQ &= " ORDER BY cfbackup.backup_date DESC "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getList", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfbackup) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            objDataOperation.AddParameter("@backup_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBackup_Date)
            objDataOperation.AddParameter("@backup_path", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBackup_Path.ToString)
            objDataOperation.AddParameter("@backup_type", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrBackup_Type.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isdeleted", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrIsdeleted.ToString)
            objDataOperation.AddParameter("@isconfiguration", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsconfiguration.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)


            strQ = "INSERT INTO hrmsConfiguration..cfbackup ( " & _
                     "  backup_date " & _
                     ", backup_path " & _
                     ", backup_type " & _
                     ", userunkid " & _
                     ", isdeleted " & _
                     ", isconfiguration " & _
                     ", companyunkid " & _
                     ", yearunkid" & _
                   ") VALUES (" & _
                     "  @backup_date " & _
                     ", @backup_path " & _
                     ", @backup_type " & _
                     ", @userunkid " & _
                     ", @isdeleted " & _
                     ", @isconfiguration " & _
                     ", @companyunkid " & _
                     ", @yearunkid" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBackupUnkId = dsList.Tables(0).Rows(0).Item(0)
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Insert", mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            objDataOperation.ReleaseTransaction(False)
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfbackup) </purpose>
    Public Function Update() As Boolean
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.AddParameter("@backupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBackupUnkId.ToString)
            objDataOperation.AddParameter("@backup_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBackup_Date)
            objDataOperation.AddParameter("@backup_path", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBackup_Path.ToString)
            objDataOperation.AddParameter("@backup_type", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrBackup_Type.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isdeleted", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrIsdeleted.ToString)
            objDataOperation.AddParameter("@isconfiguration", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsconfiguration.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)

            strQ = "UPDATE hrmsConfiguration..cfbackup SET " & _
                    "  backup_date = @backup_date" & _
                    ", backup_path = @backup_path" & _
                    ", backup_type = @backup_type" & _
                    ", userunkid = @userunkid" & _
                    ", isdeleted = @isdeleted" & _
                    ", isconfiguration = @isconfiguration" & _
                    ", companyunkid = @companyunkid" & _
                    ", yearunkid = @yearunkid " & _
                  "WHERE backupunkid = @backupunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Update", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    'Sandeep [ 10 FEB 2011 ] -- Start
    Public Function Get_Restore_List(Optional ByVal StrList As String = "List") As DataTable
        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim dsGrpList As New DataSet
        Dim dsTranList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            objDataOperation.AddParameter("@SelectPath", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select Path"))

            StrQ = " SELECT " & _
                   "    TOP 1 " & _
                   "    0 AS CId " & _
                   "   ,'hrmsConfiguration' AS CName " & _
                   "   ,1 AS IsGroup " & _
                   "   ,backup_path " & _
                   "   ,@SelectPath AS Link " & _
                   "   ,CONVERT(CHAR(8),backup_date,112) AS backup_date " & _
                   "   ,CONVERT(CHAR(8),backup_date,108) AS backup_time " & _
                   "   ,ISNULL(cfuser_master.username,'') AS UserName " & _
                   "   ,companyunkid " & _
                   "  FROM cfbackup " & _
                   "    JOIN cfuser_master ON cfbackup.userunkid = cfuser_master.userunkid " & _
                   "  WHERE isconfiguration = 1 AND companyunkid = -1 AND yearunkid = -1 " & _
                   "UNION " & _
                   "  SELECT " & _
                   "	 hrmsConfiguration..cfcompany_master.companyunkid AS CId " & _
                   "	,ISNULL(hrmsConfiguration..cfcompany_master.name, '') AS CName " & _
                   "	,1 AS IsGroup " & _
                   "	,'' AS backup_path " & _
                   "	,'' AS Link " & _
                   "	,CONVERT(CHAR(8),backup_date,112) AS backup_date " & _
                   "	,CONVERT(CHAR(8),backup_date,108) AS backup_time " & _
                   "    ,'' As UserName " & _
                   "	,cfbackup.companyunkid " & _
                   "  FROM hrmsConfiguration..cfcompany_master " & _
                   "	JOIN cfbackup ON cfcompany_master.companyunkid = cfbackup.companyunkid " & _
                   "  WHERE ISNULL(hrmsConfiguration..cfcompany_master.isactive, 0) = 1 AND ISNULL(cfbackup.isdeleted,'') = 'N' " & _
                   "ORDER BY  Cid,CONVERT(CHAR(8),backup_date,108) DESC "

            dsGrpList = objDataOperation.ExecQuery(StrQ, "GrpList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsGrpList.Tables("GrpList").Rows.Count > 0 Then
                dtTable = New DataTable(StrList)
                dtTable.Columns.Add("Database", System.Type.GetType("System.String")).DefaultValue = String.Empty
                dtTable.Columns.Add("Path", System.Type.GetType("System.String")).DefaultValue = String.Empty
                dtTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = 0
                dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = 0
                dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("YearId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("IsTaken", System.Type.GetType("System.Boolean")).DefaultValue = 0
                dtTable.Columns.Add("DateTime", System.Type.GetType("System.String")).DefaultValue = String.Empty
                dtTable.Columns.Add("User", System.Type.GetType("System.String")).DefaultValue = String.Empty
                dtTable.Columns.Add("Link", System.Type.GetType("System.String")).DefaultValue = String.Empty


                StrQ = "SELECT " & _
                       "	 database_name AS dname " & _
                       "	,backup_path " & _
                       "	,@SelectPath AS Link " & _
                       "	,cffinancial_year_tran.companyunkid AS companyunkid " & _
                       "	,hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                       "	,ISNULL(cfuser_master.username,'') AS UserName " & _
                       "	,CONVERT(CHAR(8),backup_date,112) AS backup_date " & _
                       "	,CONVERT(CHAR(8),backup_date,108) AS backup_time " & _
                       "    ,hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                       "FROM hrmsConfiguration..cffinancial_year_tran " & _
                       "	JOIN cfbackup ON cffinancial_year_tran.yearunkid = cfbackup.yearunkid " & _
                       "	JOIN cfuser_master ON cfbackup.userunkid = cfuser_master.userunkid " & _
                       "	JOIN sys.databases ON sys.databases.NAME=hrmsConfiguration ..cffinancial_year_tran.database_name " & _
                       "WHERE ISNULL(cfbackup.isdeleted,'') = 'N' " & _
                       "ORDER BY CONVERT(CHAR(8),backup_date,108) DESC,backup_date DESC "

                dsTranList = objDataOperation.ExecQuery(StrQ, "TranList")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dtFilterTab As DataTable
                Dim dicIs_DatabaseAdded As New Dictionary(Of Integer, String)
                For Each dtRow As DataRow In dsGrpList.Tables("GrpList").Rows
                    If dicIs_DatabaseAdded.ContainsKey(CInt(dtRow.Item("CId"))) Then Continue For
                    dicIs_DatabaseAdded.Add(CInt(dtRow.Item("Cid")), dtRow.Item("CName"))

                    Dim dRow As DataRow = dtTable.NewRow
                    dRow.Item("Database") = dtRow.Item("CName")
                    If dtRow.Item("backup_path").ToString.Trim.Length > 0 Then
                        Dim flInfo As New System.IO.FileInfo(dtRow.Item("backup_path"))
                        If System.IO.File.Exists(dtRow.Item("backup_path")) Then
                            dRow.Item("Path") = dtRow.Item("backup_path")
                            'dRow.Item("Link") = dtRow.Item("Link")
                            dRow.Item("Link") = ""
                        Else
                            dRow.Item("Path") = ""
                            dRow.Item("Link") = dtRow.Item("Link")
                        End If
                    Else
                        dRow.Item("Path") = dtRow.Item("backup_path")
                        dRow.Item("Link") = ""
                    End If
                    dRow.Item("IsGrp") = dtRow.Item("IsGroup")
                    dRow.Item("GrpId") = dtRow.Item("CId")
                    dRow.Item("DateTime") = eZeeDate.convertDate(dtRow.Item("backup_date").ToString).ToShortDateString & " " & dtRow.Item("backup_time").ToString
                    dRow.Item("User") = dtRow.Item("UserName")

                    dtTable.Rows.Add(dRow)

                    dtFilterTab = New DataView(dsTranList.Tables("TranList"), "companyunkid = '" & CInt(dtRow.Item("CId")) & "'", "", DataViewRowState.CurrentRows).ToTable

                    If dtFilterTab.Rows.Count > 0 Then
                        For Each dTrRow As DataRow In dtFilterTab.Rows
                            Dim dRowT As DataRow = dtTable.NewRow

                            dRowT.Item("Database") = Space(5) & dTrRow.Item("dname")
                            dRowT.Item("IsGrp") = False
                            dRowT.Item("GrpId") = dtRow.Item("CId")
                            If dTrRow.Item("backup_path").ToString.Trim.Length > 0 Then
                                Dim flInfo As New System.IO.FileInfo(dTrRow.Item("backup_path"))
                                If System.IO.File.Exists(dTrRow.Item("backup_path")) Then
                                    dRowT.Item("Path") = dTrRow.Item("backup_path")
                                    dRowT.Item("Link") = ""
                                Else
                                    dRowT.Item("Path") = ""
                                    dRowT.Item("Link") = dTrRow.Item("Link")
                                End If
                            Else
                                dRowT.Item("Path") = dTrRow.Item("backup_path")
                            End If
                            dRowT.Item("YearId") = dTrRow.Item("yearunkid")
                            dRowT.Item("DateTime") = eZeeDate.convertDate(dTrRow.Item("backup_date").ToString).ToShortDateString & " " & dTrRow.Item("backup_time").ToString
                            dRowT.Item("User") = dTrRow.Item("UserName")

                            dtTable.Rows.Add(dRowT)
                        Next
                    End If
                Next
            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Restore_List", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Is_DatabasePresent(ByVal strDataBase As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim intCtr As Integer = -1
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT name FROM sys.databases WHERE name= @DName"

            objDataOperation.AddParameter("@DName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDataBase)

            intCtr = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If intCtr > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_DatabasePresent", mstrModuleName)
            Return False
        End Try
    End Function
    'Sandeep [ 10 FEB 2011 ] -- End 

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select Path")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
