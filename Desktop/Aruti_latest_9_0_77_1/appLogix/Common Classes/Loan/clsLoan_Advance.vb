﻿'************************************************************************************************************************************
'Class Name : clsLoan_Advance.vb
'Purpose    :
'Date       :07/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 06 SEP 2011 ] -- START
'ENHANCEMENT : MAKING COMMON FUNCTION
Imports System.Math
'S.SANDEEP [ 06 SEP 2011 ] -- END 

'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsLoan_Advance
    Private Shared ReadOnly mstrModuleName As String = "clsLoan_Advance"
    Dim mstrMessage As String = ""
    Private objStatusTran As New clsLoan_Status_tran

#Region " Private variables "
    Private mintLoanadvancetranunkid As Integer
    Private mstrLoanvoucher_No As String = String.Empty
    Private mdtEffective_Date As Date
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintApproverunkid As Integer
    Private mstrLoan_Purpose As String = String.Empty
    Private mdecBalance_Amount As Decimal 'Sohail (11 May 2011)
    Private mblnIsbrought_Forward As Boolean
    Private mblnIsloan As Boolean
    Private mintLoanschemeunkid As Integer
    Private mdecLoan_Amount As Decimal 'Sohail (11 May 2011)
    Private mdecInterest_Rate As Decimal 'Sohail (11 May 2011)
    Private mintLoan_Duration As Integer
    Private mdecInterest_Amount As Decimal 'Sohail (11 May 2011)
    Private mdecNet_Amount As Decimal 'Sohail (11 May 2011)
    Private mintCalctype_Id As Integer
    Private mintLoanscheduleunkid As Integer
    Private mintEmi_Tenure As Integer
    Private mdecEmi_Amount As Decimal 'Sohail (11 May 2011)
    Private mdtEmi_Start_Date As Date
    Private mdtEmi_End_Date As Date
    Private mdecAdvance_Amount As Decimal 'Sohail (11 May 2011)
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mintLoanStatus As Integer
    Private mintProcesspendingloanunkid As Integer
    Private mstrVoidreason As String = String.Empty

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private mintDeductionperiodunkid As Integer
    'Sandeep [ 21 Aug 2010 ] -- End 

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'S.SANDEEP [ 29 May 2013 ] -- START
    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    Dim objDataOpr As clsDataOperation
    'S.SANDEEP [ 29 May 2013 ] -- END

    'S.SANDEEP [ 18 JAN 2014 ] -- START
    Private mdecLoanBF_Amount As Decimal = 0
    'S.SANDEEP [ 18 JAN 2014 ] -- END

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
Private mintAuditUserId As Integer = 0
Public WriteOnly Property _AuditUserId() As Boolean 
Set(ByVal value As Integer) 
mintAuditUserId = value 
End Set 
End Property 
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanadvancetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanadvancetranunkid() As Integer
        Get
            Return mintLoanadvancetranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanadvancetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanvoucher_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanvoucher_No() As String
        Get
            Return mstrLoanvoucher_No
        End Get
        Set(ByVal value As String)
            mstrLoanvoucher_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effective_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Effective_Date() As Date
        Get
            Return mdtEffective_Date
        End Get
        Set(ByVal value As Date)
            mdtEffective_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_purpose
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loan_Purpose() As String
        Get
            Return mstrLoan_Purpose
        End Get
        Set(ByVal value As String)
            mstrLoan_Purpose = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set balance_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Balance_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecBalance_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecBalance_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isbrought_forward
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isbrought_Forward() As Boolean
        Get
            Return mblnIsbrought_Forward
        End Get
        Set(ByVal value As Boolean)
            mblnIsbrought_Forward = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isloan
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isloan() As Boolean
        Get
            Return mblnIsloan
        End Get
        Set(ByVal value As Boolean)
            mblnIsloan = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loan_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecLoan_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecLoan_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interest_rate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Interest_Rate() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInterest_Rate
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInterest_Rate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_duration
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loan_Duration() As Integer
        Get
            Return mintLoan_Duration
        End Get
        Set(ByVal value As Integer)
            mintLoan_Duration = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interest_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Interest_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInterest_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInterest_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set net_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Net_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecNet_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecNet_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calctype_id
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Calctype_Id() As Integer
        Get
            Return mintCalctype_Id
        End Get
        Set(ByVal value As Integer)
            mintCalctype_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanscheduleunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanscheduleunkid() As Integer
        Get
            Return mintLoanscheduleunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanscheduleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_tenure
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emi_Tenure() As Integer
        Get
            Return mintEmi_Tenure
        End Get
        Set(ByVal value As Integer)
            mintEmi_Tenure = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emi_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecEmi_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecEmi_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_start_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emi_Start_Date() As Date
        Get
            Return mdtEmi_Start_Date
        End Get
        Set(ByVal value As Date)
            mdtEmi_Start_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_end_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emi_End_Date() As Date
        Get
            Return mdtEmi_End_Date
        End Get
        Set(ByVal value As Date)
            mdtEmi_End_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set advance_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Advance_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAdvance_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAdvance_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Loan Status
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LoanStatus() As Integer
        Get
            Return mintLoanStatus
        End Get
        Set(ByVal value As Integer)
            mintLoanStatus = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Sandeep [ 21 Aug 2010 ] -- Start
    ''' <summary>
    ''' Purpose: Get or Set deductionperiodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Deductionperiodunkid() As Integer
        Get
            Return mintDeductionperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintDeductionperiodunkid = Value
        End Set
    End Property
    'Sandeep [ 21 Aug 2010 ] -- End 

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'S.SANDEEP [ 29 May 2013 ] -- START
    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    Public WriteOnly Property _DataOpr() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOpr = value
        End Set
    End Property
    'S.SANDEEP [ 29 May 2013 ] -- END


    'S.SANDEEP [ 18 JAN 2014 ] -- START
    Public Property _LoanBF_Amount() As Decimal
        Get
            Return mdecLoanBF_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecLoanBF_Amount = value
        End Set
    End Property
    'S.SANDEEP [ 18 JAN 2014 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        'S.SANDEEP [ 29 May 2013 ] -- START
        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
        'Dim objDataOperation As New clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If
        'S.SANDEEP [ 29 May 2013 ] -- END

        Try

            'Sandeep [ 21 Aug 2010 ] -- Start
            'strQ = "SELECT " & _
            '          "  loanadvancetranunkid " & _
            '          ", loanvoucher_no " & _
            '          ", effective_date " & _
            '          ", yearunkid " & _
            '          ", periodunkid " & _
            '          ", employeeunkid " & _
            '          ", approverunkid " & _
            '          ", loan_purpose " & _
            '          ", balance_amount " & _
            '          ", isbrought_forward " & _
            '          ", isloan " & _
            '          ", loanschemeunkid " & _
            '          ", loan_amount " & _
            '          ", interest_rate " & _
            '          ", loan_duration " & _
            '          ", interest_amount " & _
            '          ", net_amount " & _
            '          ", calctype_id " & _
            '          ", loanscheduleunkid " & _
            '          ", emi_tenure " & _
            '          ", emi_amount " & _
            '          ", advance_amount " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", loan_statusunkid " & _
            '          ", ISNULL(processpendingloanunkid,-1) As processpendingloanunkid " & _
            '          ", voidreason " & _
            '         "FROM lnloan_advance_tran " & _
            '         "WHERE loanadvancetranunkid = @loanadvancetranunkid "
            strQ = "SELECT " & _
                      "  loanadvancetranunkid " & _
                      ", loanvoucher_no " & _
                      ", effective_date " & _
                      ", yearunkid " & _
                      ", periodunkid " & _
                      ", employeeunkid " & _
                      ", approverunkid " & _
                      ", loan_purpose " & _
                      ", balance_amount " & _
                      ", isbrought_forward " & _
                      ", isloan " & _
                      ", loanschemeunkid " & _
                      ", loan_amount " & _
                      ", interest_rate " & _
                      ", loan_duration " & _
                      ", interest_amount " & _
                      ", net_amount " & _
                      ", calctype_id " & _
                      ", loanscheduleunkid " & _
                      ", emi_tenure " & _
                      ", emi_amount " & _
                      ", advance_amount " & _
                            ", loan_statusunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                            ", processpendingloanunkid " & _
                            ", ISNULL(deductionperiodunkid,0) As deductionperiodunkid " & _
                     "FROM lnloan_advance_tran " & _
                     "WHERE loanadvancetranunkid = @loanadvancetranunkid "
            'Sandeep [ 21 Aug 2010 ] -- End 


            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoanadvancetranunkid = CInt(dtRow.Item("loanadvancetranunkid"))
                mstrLoanvoucher_No = dtRow.Item("loanvoucher_no").ToString
                mdtEffective_Date = dtRow.Item("effective_date")
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                mstrLoan_Purpose = dtRow.Item("loan_purpose").ToString
                mdecBalance_Amount = CDec(dtRow.Item("balance_amount")) 'Sohail (11 May 2011)
                mblnIsbrought_Forward = CBool(dtRow.Item("isbrought_forward"))
                mblnIsloan = CBool(dtRow.Item("isloan"))
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mdecLoan_Amount = CDec(dtRow.Item("loan_amount")) 'Sohail (11 May 2011)
                mdecInterest_Rate = CDec(dtRow.Item("interest_rate")) 'Sohail (11 May 2011)
                mintLoan_Duration = CInt(dtRow.Item("loan_duration"))
                mdecInterest_Amount = CDec(dtRow.Item("interest_amount")) 'Sohail (11 May 2011)
                mdecNet_Amount = CDec(dtRow.Item("net_amount")) 'Sohail (11 May 2011)
                mintCalctype_Id = CInt(dtRow.Item("calctype_id"))
                mintLoanscheduleunkid = CInt(dtRow.Item("loanscheduleunkid"))
                mintEmi_Tenure = CInt(dtRow.Item("emi_tenure"))
                mdecEmi_Amount = CDec(dtRow.Item("emi_amount")) 'Sohail (11 May 2011)
                mdecAdvance_Amount = CDec(dtRow.Item("advance_amount")) 'Sohail (11 May 2011)
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintLoanStatus = dtRow.Item("loan_statusunkid")
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Sandeep [ 21 Aug 2010 ] -- Start
                mintDeductionperiodunkid = CInt(dtRow.Item("deductionperiodunkid"))
                'Sandeep [ 21 Aug 2010 ] -- End 

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'objDataOperation = Nothing
            'S.SANDEEP [ 29 May 2013 ] -- END


        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intLoanAdvanceTranUnkID As Integer = 0, Optional ByVal intEmpUnkID As Integer = 0, Optional ByVal intStatusID As Integer = 0 _
                            , Optional ByVal dtPeriodStart As DateTime = Nothing _
                            , Optional ByVal dtPeriodEnd As DateTime = Nothing, Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet 'Sohail (06 Jan 2012)
        'S.SANDEEP [ 29 May 2013 {strUserAccessLevelFilterString}] -- START -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'Sohail (21 Aug 2010) -- Start
            '***Changes:Set LoanScheme = '@Advance' when isloan = 0
            'strQ = "SELECT " & _
            '          " lnloan_advance_tran.loanvoucher_no AS VocNo " & _
            '          ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '          ",lnloan_scheme_master.name AS LoanScheme " & _
            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_advance_tran.net_amount ELSE lnloan_advance_tran.advance_amount END AS Amount " & _
            '          ",cfcommon_period_tran.period_name AS PeriodName " & _
            '          ",lnloan_advance_tran.loan_statusunkid " & _
            '          ",lnloan_advance_tran.periodunkid " & _
            '          ",lnloan_advance_tran.loanadvancetranunkid " & _
            '          ",hremployee_master.employeeunkid " & _
            '          ",lnloan_scheme_master.loanschemeunkid " & _
            '          ",lnloan_advance_tran.isloan " & _
            '          ",convert(char(8),lnloan_advance_tran.effective_date,112) AS effective_date " & _
            '          ",lnloan_advance_tran.balance_amount " & _
            '          ",lnloan_advance_tran.isloan " & _
            '          ",lnloan_advance_tran.loanschemeunkid " & _
            '          ",lnloan_advance_tran.emi_amount " & _
            '          ",lnloan_advance_tran.advance_amount " & _
            '          ",lnloan_advance_tran.loan_statusunkid " & _
            '          ",lnloan_advance_tran.processpendingloanunkid " & _
            '        "FROM lnloan_advance_tran " & _
            '          "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '          "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
            '          "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
            '        "WHERE ISNULL(lnloan_advance_tran.isvoid,0) = 0 "
            'Sohail (11 Sep 2010) -- Start
            'Cahnges : Deduction period start and end date added
            'strQ = "SELECT " & _
            '          " lnloan_advance_tran.loanvoucher_no AS VocNo " & _
            '          ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_advance_tran.net_amount ELSE lnloan_advance_tran.advance_amount END AS Amount " & _
            '          ",cfcommon_period_tran.period_name AS PeriodName " & _
            '          ",lnloan_advance_tran.loan_statusunkid " & _
            '          ",lnloan_advance_tran.periodunkid " & _
            '          ",lnloan_advance_tran.loanadvancetranunkid " & _
            '          ",hremployee_master.employeeunkid " & _
            '          ",lnloan_scheme_master.loanschemeunkid " & _
            '          ",lnloan_advance_tran.isloan " & _
            '          ",convert(char(8),lnloan_advance_tran.effective_date,112) AS effective_date " & _
            '          ",lnloan_advance_tran.balance_amount " & _
            '          ",lnloan_advance_tran.isloan " & _
            '          ",lnloan_advance_tran.loanschemeunkid " & _
            '          ",lnloan_advance_tran.emi_amount " & _
            '          ",lnloan_advance_tran.advance_amount " & _
            '          ",lnloan_advance_tran.loan_statusunkid " & _
            '          ",lnloan_advance_tran.processpendingloanunkid " & _
            '          ",lnloan_advance_tran.deductionperiodunkid " & _
            '        "FROM lnloan_advance_tran " & _
            '          "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '          "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
            '          "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
            '        "WHERE ISNULL(lnloan_advance_tran.isvoid,0) = 0 "

            'Sohail (04 Mar 2011)-Start
            'Purpose : Get fields of lnloan_advance_tran as well as latest loan status detail from lnloan_status_tran
            'Sandeep ( 18 JAN 2011 ) -- START
            'strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
            '              ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
            '                "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
            '                "+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                     "THEN lnloan_scheme_master.name " & _
            '                     "ELSE @Advance " & _
            '                "END AS LoanScheme " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
            '                     "ELSE @Advance " & _
            '                "END AS Loan_Advance " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
            '                     "ELSE 2 " & _
            '                "END AS Loan_AdvanceUnkid " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                     "THEN lnloan_advance_tran.net_amount " & _
            '                     "ELSE lnloan_advance_tran.advance_amount " & _
            '                "END AS Amount " & _
            '          ",cfcommon_period_tran.period_name AS PeriodName " & _
            '          ",lnloan_advance_tran.loan_statusunkid " & _
            '          ",lnloan_advance_tran.periodunkid " & _
            '          ",lnloan_advance_tran.loanadvancetranunkid " & _
            '          ",hremployee_master.employeeunkid " & _
            '          ",lnloan_scheme_master.loanschemeunkid " & _
            '          ",lnloan_advance_tran.isloan " & _
            '              ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
            '          ",lnloan_advance_tran.balance_amount " & _
            '          ",lnloan_advance_tran.isloan " & _
            '          ",lnloan_advance_tran.loanschemeunkid " & _
            '          ",lnloan_advance_tran.emi_amount " & _
            '          ",lnloan_advance_tran.advance_amount " & _
            '          ",lnloan_advance_tran.loan_statusunkid " & _
            '          ",lnloan_advance_tran.processpendingloanunkid " & _
            '          ",lnloan_advance_tran.deductionperiodunkid " & _
            '              ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
            '              ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
            '        "FROM lnloan_advance_tran " & _
            '          "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '          "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
            '          "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
            '        "WHERE ISNULL(lnloan_advance_tran.isvoid,0) = 0 "

            'strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
            '              ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
            '                "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
            '                "+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                     "THEN lnloan_scheme_master.name " & _
            '                     "ELSE @Advance " & _
            '                "END AS LoanScheme " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
            '                     "ELSE @Advance " & _
            '                "END AS Loan_Advance " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
            '                     "ELSE 2 " & _
            '                "END AS Loan_AdvanceUnkid " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                     "THEN lnloan_advance_tran.net_amount " & _
            '                     "ELSE lnloan_advance_tran.advance_amount " & _
            '                "END AS Amount " & _
            '          ",cfcommon_period_tran.period_name AS PeriodName " & _
            '          ",lnloan_advance_tran.loan_statusunkid " & _
            '          ",lnloan_advance_tran.periodunkid " & _
            '          ",lnloan_advance_tran.loanadvancetranunkid " & _
            '          ",hremployee_master.employeeunkid " & _
            '          ",lnloan_scheme_master.loanschemeunkid " & _
            '          ",lnloan_advance_tran.isloan " & _
            '              ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
            '          ",lnloan_advance_tran.balance_amount " & _
            '          ",lnloan_advance_tran.isloan " & _
            '          ",lnloan_advance_tran.loanschemeunkid " & _
            '          ",lnloan_advance_tran.emi_amount " & _
            '          ",lnloan_advance_tran.advance_amount " & _
            '          ",lnloan_advance_tran.loan_statusunkid " & _
            '          ",lnloan_advance_tran.processpendingloanunkid " & _
            '          ",lnloan_advance_tran.deductionperiodunkid " & _
            '              ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
            '              ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
            '          ",isbrought_forward AS isbrought_forward " & _
            '        "FROM lnloan_advance_tran " & _
            '          "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '          "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
            '          "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
            '        "WHERE ISNULL(lnloan_advance_tran.isvoid,0) = 0 "



            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Rutta's Request
            'strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
            '              ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                     "THEN lnloan_scheme_master.name " & _
            '                     "ELSE @Advance " & _
            '                "END AS LoanScheme " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
            '                     "ELSE @Advance " & _
            '                "END AS Loan_Advance " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
            '                     "ELSE 2 " & _
            '                "END AS Loan_AdvanceUnkid " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                     "THEN lnloan_advance_tran.net_amount " & _
            '                     "ELSE lnloan_advance_tran.advance_amount " & _
            '                "END AS Amount " & _
            '              ", cfcommon_period_tran.period_name AS PeriodName " & _
            '              ", lnloan_advance_tran.loan_statusunkid " & _
            '              ", lnloan_advance_tran.periodunkid " & _
            '              ", lnloan_advance_tran.loanadvancetranunkid " & _
            '              ", hremployee_master.employeeunkid " & _
            '              ", lnloan_scheme_master.loanschemeunkid " & _
            '              ", lnloan_advance_tran.isloan " & _
            '              ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
            '              ", lnloan_advance_tran.balance_amount " & _
            '              ", lnloan_advance_tran.isloan " & _
            '              ", lnloan_advance_tran.loanschemeunkid " & _
            '              ", lnloan_advance_tran.emi_amount " & _
            '              ", lnloan_advance_tran.advance_amount " & _
            '              ", lnloan_advance_tran.loan_statusunkid " & _
            '              ", lnloan_advance_tran.processpendingloanunkid " & _
            '              ", lnloan_advance_tran.deductionperiodunkid " & _
            '              ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
            '              ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
            '              ", isbrought_forward AS isbrought_forward " & _
            '              ", LoanStatus.loanstatustranunkid " & _
            '              ", CONVERT(CHAR(8), LoanStatus.status_date, 112) AS status_date " & _
            '              ", LoanStatus.statusunkid " & _
            '              ", LoanStatus.remark " & _
            '        "FROM    lnloan_advance_tran " & _
            '                "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
            '                                  ", lnloan_status_tran.loanadvancetranunkid " & _
            '                                  ", lnloan_status_tran.status_date " & _
            '                                  ", lnloan_status_tran.statusunkid " & _
            '                                  ", lnloan_status_tran.remark " & _
            '                                  ", lnloan_status_tran.userunkid " & _
            '                                  ", lnloan_status_tran.isvoid " & _
            '                                  ", lnloan_status_tran.voiddatetime " & _
            '                                  ", lnloan_status_tran.voiduserunkid " & _
            '                            "FROM    lnloan_status_tran " & _
            '                                    "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
            '                                                      ", lnloan_status_tran.loanadvancetranunkid " & _
            '                                                 "FROM   lnloan_status_tran " & _
            '                                                 "WHERE  ISNULL(isvoid, 0) = 0 " & _
            '                                                 "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
            '                                               ") AS LoanStatus ON lnloan_status_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
            '                                                                  "AND lnloan_status_tran.status_date = LoanStatus.status_date " & _
            '                            "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
            '                          ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
            '                "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
            '                "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
            '        "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
            'Sohail (29 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
            '              ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                     "THEN lnloan_scheme_master.name " & _
            '                     "ELSE @Advance " & _
            '                "END AS LoanScheme " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
            '                     "ELSE @Advance " & _
            '                "END AS Loan_Advance " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
            '                     "ELSE 2 " & _
            '                "END AS Loan_AdvanceUnkid " & _
            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                     "THEN lnloan_advance_tran.net_amount " & _
            '                     "ELSE lnloan_advance_tran.advance_amount " & _
            '                "END AS Amount " & _
            '              ", cfcommon_period_tran.period_name AS PeriodName " & _
            '              ", lnloan_advance_tran.loan_statusunkid " & _
            '              ", lnloan_advance_tran.periodunkid " & _
            '              ", lnloan_advance_tran.loanadvancetranunkid " & _
            '              ", hremployee_master.employeeunkid " & _
            '              ", lnloan_scheme_master.loanschemeunkid " & _
            '              ", lnloan_advance_tran.isloan " & _
            '              ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
            '              ", lnloan_advance_tran.balance_amount " & _
            '              ", lnloan_advance_tran.isloan " & _
            '              ", lnloan_advance_tran.loanschemeunkid " & _
            '              ", lnloan_advance_tran.emi_amount " & _
            '              ", lnloan_advance_tran.advance_amount " & _
            '              ", lnloan_advance_tran.loan_statusunkid " & _
            '              ", lnloan_advance_tran.processpendingloanunkid " & _
            '              ", lnloan_advance_tran.deductionperiodunkid " & _
            '              ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
            '              ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
            '              ", isbrought_forward AS isbrought_forward " & _
            '              ", LoanStatus.loanstatustranunkid " & _
            '              ", CONVERT(CHAR(8), LoanStatus.status_date, 112) AS status_date " & _
            '              ", LoanStatus.statusunkid " & _
            '              ", LoanStatus.remark " & _
            '              ", hremployee_master.employeecode as empcode " & _
            '              ", lnloan_scheme_master.code as loancode " & _
            '              ", lnloan_advance_tran.emi_tenure AS Intallments " & _
            '        "FROM    lnloan_advance_tran " & _
            '                "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
            '                                  ", lnloan_status_tran.loanadvancetranunkid " & _
            '                                  ", lnloan_status_tran.status_date " & _
            '                                  ", lnloan_status_tran.statusunkid " & _
            '                                  ", lnloan_status_tran.remark " & _
            '                                  ", lnloan_status_tran.userunkid " & _
            '                                  ", lnloan_status_tran.isvoid " & _
            '                                  ", lnloan_status_tran.voiddatetime " & _
            '                                  ", lnloan_status_tran.voiduserunkid " & _
            '                            "FROM    lnloan_status_tran " & _
            '                                    "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
            '                                                      ", lnloan_status_tran.loanadvancetranunkid " & _
            '                                                 "FROM   lnloan_status_tran " & _
            '                                                 "WHERE  ISNULL(isvoid, 0) = 0 " & _
            '                                                 "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
            '                                               ") AS LoanStatus ON lnloan_status_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
            '                                                                  "AND lnloan_status_tran.status_date = LoanStatus.status_date " & _
            '                            "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
            '                          ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
            '                "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            '                "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
            '        "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
            strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                          ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                                 "THEN lnloan_scheme_master.name " & _
                                 "ELSE @Advance " & _
                            "END AS LoanScheme " & _
                          ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
                                 "ELSE @Advance " & _
                            "END AS Loan_Advance " & _
                          ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
                                 "ELSE 2 " & _
                            "END AS Loan_AdvanceUnkid " & _
                          ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                                 "THEN lnloan_advance_tran.net_amount " & _
                                 "ELSE lnloan_advance_tran.advance_amount " & _
                            "END AS Amount " & _
                          ", cfcommon_period_tran.period_name AS PeriodName " & _
                          ", lnloan_advance_tran.loan_statusunkid " & _
                          ", lnloan_advance_tran.periodunkid " & _
                          ", lnloan_advance_tran.loanadvancetranunkid " & _
                          ", hremployee_master.employeeunkid " & _
                          ", lnloan_scheme_master.loanschemeunkid " & _
                          ", lnloan_advance_tran.isloan " & _
                          ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
                          ", lnloan_advance_tran.balance_amount " & _
                          ", lnloan_advance_tran.isloan " & _
                          ", lnloan_advance_tran.loanschemeunkid " & _
                          ", lnloan_advance_tran.emi_amount " & _
                          ", lnloan_advance_tran.advance_amount " & _
                          ", lnloan_advance_tran.loan_statusunkid " & _
                          ", lnloan_advance_tran.processpendingloanunkid " & _
                          ", lnloan_advance_tran.deductionperiodunkid " & _
                          ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
                          ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
                          ", isbrought_forward AS isbrought_forward " & _
                          ", LoanStatus.loanstatustranunkid " & _
                          ", CONVERT(CHAR(8), LoanStatus.status_date, 112) AS status_date " & _
                          ", LoanStatus.statusunkid " & _
                          ", LoanStatus.remark " & _
                          ", hremployee_master.employeecode AS empcode " & _
                          ", lnloan_scheme_master.code AS loancode " & _
                          ", lnloan_advance_tran.emi_tenure AS Intallments " & _
                          ", CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
                          "         WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
                          "         WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
                          "         WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed END AS loan_status " & _
                    "FROM    lnloan_advance_tran " & _
                            "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
                                              ", lnloan_status_tran.loanadvancetranunkid " & _
                                              ", lnloan_status_tran.status_date " & _
                                              ", lnloan_status_tran.statusunkid " & _
                                              ", lnloan_status_tran.remark " & _
                                        "FROM    lnloan_status_tran " & _
                                                "JOIN ( SELECT   MAX(lnloan_status_tran.loanstatustranunkid) AS loanstatustranunkid " & _
                                                              ", lnloan_status_tran.loanadvancetranunkid " & _
                                        "FROM    lnloan_status_tran " & _
                                                "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
                                                                  ", lnloan_status_tran.loanadvancetranunkid " & _
                                                             "FROM   lnloan_status_tran " & _
                                                             "WHERE  ISNULL(isvoid, 0) = 0 " & _
                                                             "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
                                                                           ") AS Loan_Status ON lnloan_status_tran.loanadvancetranunkid = Loan_Status.loanadvancetranunkid " & _
                                                                                  "AND lnloan_status_tran.status_date = Loan_Status.status_date " & _
                                        "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
                                                       "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
                                                     ") AS A ON A.loanstatustranunkid = lnloan_status_tran.loanstatustranunkid " & _
                                      ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
                            "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
                                                              "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                            "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
                                                                                  "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
                    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
            'S.SANDEEP [ 29 May 2013 {loan_status}] -- START -- END

            'Sohail (29 Jun 2012) -- End
            'Anjan (02 Mar 2012)-End 




            'Anjan (09 Aug 2011)-Start
            'Issue : For including setting of acitve and inactive employee.
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If dtPeriodStart <> Nothing AndAlso dtPeriodEnd <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            'Sohail (06 Jan 2012) -- End
            End If
            'Anjan (09 Aug 2011)-End 

            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '        End If
            'End Select

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'strQ &= UserAccessLevel._AccessLevelFilterString
            If strUserAccessLevelFilterString = "" Then
            strQ &= UserAccessLevel._AccessLevelFilterString
            Else
                strQ &= strUserAccessLevelFilterString
            End If
            'S.SANDEEP [ 29 May 2013 ] -- END

            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-End 



            'Sandeep ( 18 JAN 2011 ) -- END 
            'Sohail (04 Mar 2011)-End

            'Sohail (11 Sep 2010) -- End
            'Sohail (21 Aug 2010) -- End

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))
            'S.SANDEEP [ 29 May 2013 ] -- END

            'Sohail (04 Mar 2011)-Start

            If intLoanAdvanceTranUnkID > 0 Then
                strQ &= "AND lnloan_advance_tran.loanadvancetranunkid = @loanadvancetranunkid "
                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceTranUnkID)
            End If

            If intEmpUnkID > 0 Then
                strQ &= "AND lnloan_advance_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
            End If

            If intStatusID > 0 Then
                strQ &= "AND LoanStatus.statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            'Sohail (04 Mar 2011)-End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_advance_tran) </purpose>
    Public Function Insert(Optional ByVal iUserId As Integer = 0, Optional ByVal intCompanyId As Integer = 0) As Boolean 'S.SANDEEP [ 29 May 2013 ] -- START -- END
        'Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        'Anjan (21 Jan 2012)-Start
        'ENHANCEMENT : TRA COMMENTS 
        'Issue : To fix the issue on network of autocode concurrency when more than 1 user press save at same time.


        'S.SANDEEP [ 28 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
        ''Sandeep [ 16 Oct 2010 ] -- Start
        ''Issue : Auto No. Generation
        'Dim intVocNoType As Integer = 0
        'Dim strVocPrifix As String = ""
        'Dim intNextVocNo As Integer = 0

        'intVocNoType = ConfigParameter._Object._LoanVocNoType

        'If intVocNoType = 1 Then
        '    strVocPrifix = ConfigParameter._Object._LoanVocPrefix
        '    intNextVocNo = ConfigParameter._Object._NextLoanVocNo
        '    mstrLoanvoucher_No = strVocPrifix & intNextVocNo
        'End If
        ''Sandeep [ 16 Oct 2010 ] -- End 

        'If isExist(mstrLoanvoucher_No) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
        '    Return False
        'End If
        'Sandeep [ 16 Oct 2010 ] -- Start
        'Issue : Auto No. Generation


        'Pinkal (12-Jun-2013) -- Start
        'Enhancement : TRA Changes
        Dim intVocNoType As Integer = 0
        Dim objConfig As New clsConfigOptions
        objConfig._Companyunkid = IIf(intCompanyId = 0, Company._Object._Companyunkid, intCompanyId)
        intVocNoType = objConfig._LoanVocNoType

        'Dim intVocNoType As Integer = 0
        'intVocNoType = ConfigParameter._Object._LoanVocNoType

        'Pinkal (12-Jun-2013) -- End

        

        If intVocNoType = 0 Then
            If isExist(mstrLoanvoucher_No) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
        ElseIf intVocNoType = 1 Then
            mstrLoanvoucher_No = ""
        End If
        'S.SANDEEP [ 28 MARCH 2012 ] -- END



        'Anjan (21 Jan 2012)-End 

        Try
            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)

            'Anjan (08 Aug 2012)-Start
            'Issue : Loan report was picking principal amount instead of loan balance in new year.

            'S.SANDEEP [ 18 JAN 2014 ] -- START
            'objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString)
            objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoanBF_Amount)
            'S.SANDEEP [ 18 JAN 2014 ] -- END

            'Anjan (08 Aug 2012)-End 


            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            'strQ = "INSERT INTO lnloan_advance_tran ( " & _
            '  "  loanvoucher_no " & _
            '  ", effective_date " & _
            '  ", yearunkid " & _
            '  ", periodunkid " & _
            '  ", employeeunkid " & _
            '  ", approverunkid " & _
            '  ", loan_purpose " & _
            '  ", balance_amount " & _
            '  ", isbrought_forward " & _
            '  ", isloan " & _
            '  ", loanschemeunkid " & _
            '  ", loan_amount " & _
            '  ", interest_rate " & _
            '  ", loan_duration " & _
            '  ", interest_amount " & _
            '  ", net_amount " & _
            '  ", calctype_id " & _
            '  ", loanscheduleunkid " & _
            '  ", emi_tenure " & _
            '  ", emi_amount " & _
            '  ", advance_amount " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime" & _
            '  ", loan_statusunkid " & _
            '  ", voidreason " & _
            '  ", processpendingloanunkid " & _
            '") VALUES (" & _
            '  "  @loanvoucher_no " & _
            '  ", @effective_date " & _
            '  ", @yearunkid " & _
            '  ", @periodunkid " & _
            '  ", @employeeunkid " & _
            '  ", @approverunkid " & _
            '  ", @loan_purpose " & _
            '  ", @balance_amount " & _
            '  ", @isbrought_forward " & _
            '  ", @isloan " & _
            '  ", @loanschemeunkid " & _
            '  ", @loan_amount " & _
            '  ", @interest_rate " & _
            '  ", @loan_duration " & _
            '  ", @interest_amount " & _
            '  ", @net_amount " & _
            '  ", @calctype_id " & _
            '  ", @loanscheduleunkid " & _
            '  ", @emi_tenure " & _
            '  ", @emi_amount " & _
            '  ", @advance_amount " & _
            '  ", @userunkid " & _
            '  ", @isvoid " & _
            '  ", @voiduserunkid " & _
            '  ", @voiddatetime" & _
            '  ", @loan_statusunkid " & _
            '  ", @voidreason " & _
            '  ", @processpendingloanunkid " & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO lnloan_advance_tran ( " & _
              "  loanvoucher_no " & _
              ", effective_date " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", approverunkid " & _
              ", loan_purpose " & _
              ", balance_amount " & _
              ", isbrought_forward " & _
              ", isloan " & _
              ", loanschemeunkid " & _
              ", loan_amount " & _
              ", interest_rate " & _
              ", loan_duration " & _
              ", interest_amount " & _
              ", net_amount " & _
              ", calctype_id " & _
              ", loanscheduleunkid " & _
              ", emi_tenure " & _
              ", emi_amount " & _
              ", advance_amount " & _
                                ", loan_statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime" & _
              ", voidreason " & _
              ", processpendingloanunkid " & _
                                ", deductionperiodunkid" & _
                                ",bf_amount  " & _
            ") VALUES (" & _
              "  @loanvoucher_no " & _
              ", @effective_date " & _
              ", @yearunkid " & _
              ", @periodunkid " & _
              ", @employeeunkid " & _
              ", @approverunkid " & _
              ", @loan_purpose " & _
              ", @balance_amount " & _
              ", @isbrought_forward " & _
              ", @isloan " & _
              ", @loanschemeunkid " & _
              ", @loan_amount " & _
              ", @interest_rate " & _
              ", @loan_duration " & _
              ", @interest_amount " & _
              ", @net_amount " & _
              ", @calctype_id " & _
              ", @loanscheduleunkid " & _
              ", @emi_tenure " & _
              ", @emi_amount " & _
              ", @advance_amount " & _
                                ", @loan_statusunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
              ", @voidreason " & _
              ", @processpendingloanunkid " & _
                                ", @deductionperiodunkid" & _
                                 ",@bf_amount  " & _
            "); SELECT @@identity"
            'Sandeep [ 21 Aug 2010 ] -- End 'Anjan (08 Aug 2012) bf_amount included




            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoanadvancetranunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 28 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
            If intVocNoType = 1 Then


                'Pinkal (12-Jun-2013) -- Start
                'Enhancement : TRA Changes
                'If Set_AutoNumber(objDataOperation, mintLoanadvancetranunkid, "lnloan_advance_tran", "loanvoucher_no", "loanadvancetranunkid", "NextLoanVocNo", ConfigParameter._Object._LoanVocPrefix) = False Then
                If Set_AutoNumber(objDataOperation, mintLoanadvancetranunkid, "lnloan_advance_tran", "loanvoucher_no", "loanadvancetranunkid", "NextLoanVocNo", objConfig._LoanVocPrefix, objConfig._Companyunkid) = False Then
                    'Pinkal (12-Jun-2013) -- End


                    'S.SANDEEP [ 17 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objDataOperation.ErrorMessage <> "" Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'S.SANDEEP [ 17 NOV 2012 ] -- END
                End If

                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If Get_Saved_Number(objDataOperation, mintLoanadvancetranunkid, "lnloan_advance_tran", "loanvoucher_no", "loanadvancetranunkid", mstrLoanvoucher_No) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END
            End If
            'S.SANDEEP [ 28 MARCH 2012 ] -- END



            objStatusTran._Loanadvancetranunkid = mintLoanadvancetranunkid
            objStatusTran._Statusunkid = mintLoanStatus
            objStatusTran._Staus_Date = mdtEffective_Date

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'InsertAuditTrailForLoanAdvanceTran(objDataOperation, 1)
            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 1, iUserId) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End


            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'If objStatusTran.Insert() Then
            If objStatusTran.Insert(objDataOperation) Then
                'S.SANDEEP [ 04 DEC 2013 ] -- END

                'Anjan (04 Apr 2011)-Start
                'Issue : Included new Assigned status for loan/advance
                Dim blnFlag As Boolean = False

                Dim objProcessPendingLoan As New clsProcess_pending_loan

                objProcessPendingLoan._Processpendingloanunkid = mintProcesspendingloanunkid
                objProcessPendingLoan._Loan_Statusunkid = 4

                'S.SANDEEP [ 04 DEC 2013 ] -- START
                'blnFlag = objProcessPendingLoan.Update
                blnFlag = objProcessPendingLoan.Update(objDataOperation)
                'S.SANDEEP [ 04 DEC 2013 ] -- END
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                'Anjan (04 Apr 2011)-End

                'Sandeep [ 16 Oct 2010 ] -- Start
                'Issue : Auto No. Generation


                'S.SANDEEP [ 28 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
                'If intVocNoType = 1 Then
                '    ConfigParameter._Object._NextLoanVocNo = intNextVocNo + 1
                '    ConfigParameter._Object.updateParam()
                '    ConfigParameter._Object.Refresh()
                'End If
                'S.SANDEEP [ 28 MARCH 2012 ] -- END


                'Sandeep [ 16 Oct 2010 ] -- End 
                objDataOperation.ReleaseTransaction(True)
            Else
                objDataOperation.ReleaseTransaction(False)
            End If



            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_advance_tran) </purpose>
    Public Function Update(Optional ByVal iUserId As Integer = 0, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 29 May 2013 ] -- START -- END
        'Public Function Update() As Boolean 
        If isExist(mstrLoanvoucher_No, mintLoanadvancetranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        'S.SANDEEP [ 29 May 2013 ] -- START
        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
        'Dim objDataOperation As New clsDataOperation
        'objDataOperation.BindTransaction()

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END


        
        'S.SANDEEP [ 29 May 2013 ] -- END

        Try
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)

            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)

            'strQ = "UPDATE lnloan_advance_tran SET " & _
            '  "  loanvoucher_no = @loanvoucher_no" & _
            '  ", effective_date = @effective_date" & _
            '  ", yearunkid = @yearunkid" & _
            '  ", periodunkid = @periodunkid" & _
            '  ", employeeunkid = @employeeunkid" & _
            '  ", approverunkid = @approverunkid" & _
            '  ", loan_purpose = @loan_purpose" & _
            '  ", balance_amount = @balance_amount" & _
            '  ", isbrought_forward = @isbrought_forward" & _
            '  ", isloan = @isloan" & _
            '  ", loanschemeunkid = @loanschemeunkid" & _
            '  ", loan_amount = @loan_amount" & _
            '  ", interest_rate = @interest_rate" & _
            '  ", loan_duration = @loan_duration" & _
            '  ", interest_amount = @interest_amount" & _
            '  ", net_amount = @net_amount" & _
            '  ", calctype_id = @calctype_id" & _
            '  ", loanscheduleunkid = @loanscheduleunkid" & _
            '  ", emi_tenure = @emi_tenure" & _
            '  ", emi_amount = @emi_amount" & _
            '  ", advance_amount = @advance_amount" & _
            '  ", userunkid = @userunkid" & _
            '  ", isvoid = @isvoid" & _
            '  ", voiduserunkid = @voiduserunkid" & _
            '  ", voiddatetime = @voiddatetime " & _
            '  ", loan_statusunkid = @loan_statusunkid " & _
            '  ", voidreason = @voidreason " & _
            '  ", processpendingloanunkid = @processpendingloanunkid " & _
            '"WHERE loanadvancetranunkid = @loanadvancetranunkid "

            strQ = "UPDATE lnloan_advance_tran SET " & _
              "  loanvoucher_no = @loanvoucher_no" & _
              ", effective_date = @effective_date" & _
              ", yearunkid = @yearunkid" & _
              ", periodunkid = @periodunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", approverunkid = @approverunkid" & _
              ", loan_purpose = @loan_purpose" & _
              ", balance_amount = @balance_amount" & _
              ", isbrought_forward = @isbrought_forward" & _
              ", isloan = @isloan" & _
              ", loanschemeunkid = @loanschemeunkid" & _
              ", loan_amount = @loan_amount" & _
              ", interest_rate = @interest_rate" & _
              ", loan_duration = @loan_duration" & _
              ", interest_amount = @interest_amount" & _
              ", net_amount = @net_amount" & _
              ", calctype_id = @calctype_id" & _
              ", loanscheduleunkid = @loanscheduleunkid" & _
              ", emi_tenure = @emi_tenure" & _
              ", emi_amount = @emi_amount" & _
              ", advance_amount = @advance_amount" & _
                            ", loan_statusunkid = @loan_statusunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", processpendingloanunkid = @processpendingloanunkid " & _
                            ", deductionperiodunkid = @deductionperiodunkid " & _
            "WHERE loanadvancetranunkid = @loanadvancetranunkid "
            'Sandeep [ 21 Aug 2010 ] -- End 




            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'InsertAuditTrailForLoanAdvanceTran(objDataOperation, 2)
            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 2) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            If objDataOp Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'objDataOperation = Nothing
            'S.SANDEEP [ 29 May 2013 ] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_advance_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'Sandeep [ 09 Oct 2010 ] -- Start
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Loan/Advance. Reason : This Loan/Advance is already linked to some transactions.")
            Return False
        End If
        'Sandeep [ 09 Oct 2010 ] -- End 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE lnloan_advance_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime " & _
                     ", voidreason = @voidreason " & _
            "WHERE loanadvancetranunkid = @loanadvancetranunkid "

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False
            Dim objStatusTran As New clsLoan_Status_tran
            objStatusTran._Isvoid = mblnIsvoid.ToString
            objStatusTran._Voiddatetime = mdtVoiddatetime
            objStatusTran._Voiduserunkid = mintVoiduserunkid
            objStatusTran._Voidreason = mstrVoidreason
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'blnFlag = objStatusTran.Delete(intUnkid)
            blnFlag = objStatusTran.Delete(intUnkid, objDataOperation)
            'S.SANDEEP [ 04 DEC 2013 ] -- END
            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            mintLoanadvancetranunkid = intUnkid

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'InsertAuditTrailForLoanAdvanceTran(objDataOperation, 3)
            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 3) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Anjan (04 Apr 2011)-Start
            'Issue : Rollbacked to status as approved if assigned status transaction is deleted
            Dim blnLoanAdvanceFlag As Boolean = False
            Dim objProcessPendingLoan As New clsProcess_pending_loan
            objProcessPendingLoan._Processpendingloanunkid = mintProcesspendingloanunkid
            objProcessPendingLoan._Loan_Statusunkid = 2

            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'blnLoanAdvanceFlag = objProcessPendingLoan.Update()
            blnLoanAdvanceFlag = objProcessPendingLoan.Update(objDataOperation)
            'S.SANDEEP [ 04 DEC 2013 ] -- END

            If blnLoanAdvanceFlag = False Then objDataOperation.ReleaseTransaction(False)

            'Anjan (04 Apr 2011)-End

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            'Sandeep [ 09 Oct 2010 ] -- Start
            'strQ = "<Query>"
            strQ = "SELECT loanadvancetranunkid FROM prpayrollprocess_tran WHERE loanadvancetranunkid = @loanadvancetranunkid "
            'Sandeep [ 09 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrMessage = ""

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                strQ = "SELECT referencetranunkid FROM prpayment_tran WHERE referenceid IN (1,2) AND referencetranunkid = @loanadvancetranunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'objDataOperation = New clsDataOperation
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  loanadvancetranunkid " & _
                   ", loanvoucher_no " & _
                   ", effective_date " & _
                   ", yearunkid " & _
                   ", periodunkid " & _
                   ", employeeunkid " & _
                   ", approverunkid " & _
                   ", loan_purpose " & _
                   ", balance_amount " & _
                   ", isbrought_forward " & _
                   ", isloan " & _
                   ", loanschemeunkid " & _
                   ", loan_amount " & _
                   ", interest_rate " & _
                   ", interest_amount " & _
                   ", net_amount " & _
                   ", calctype_id " & _
                   ", loanscheduleunkid " & _
                   ", emi_tenure " & _
                   ", emi_amount " & _
                   ", advance_amount " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", processpendingloanunkid " & _
             "FROM lnloan_advance_tran " & _
             "WHERE loanvoucher_no = @loanvoucher_no "

            If intUnkid > 0 Then
                strQ &= " AND loanadvancetranunkid <> @loanadvancetranunkid"
            End If

            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Gets The History </purpose>
    Public Function GetLoanAdvance_History_List(ByVal intUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "    ISNULL(lnScheme, '') AS lnScheme " & _
                    "   ,ISNULL(lnVocNo, '') AS lnVocNo " & _
                    "   ,lnDate AS lnDate " & _
                    "   ,ISNULL(lnAmount, 0) AS lnAmount " & _
                    "   ,ISNULL(lnNetAmount, 0) AS lnNetAmount " & _
                    "   ,ISNULL(lnBalance, 0) AS lnBalance " & _
                    "   ,(ISNULL(lnNetAmount, 0)-ISNULL(lnBalance, 0)) AS lnPaid " & _
                    "   ,ISNULL(lnStatus, '') AS lnStatus " & _
                    "   ,lnUnkid AS lnUnkid " & _
                    "FROM " & _
                    "    ( SELECT " & _
                    "        CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                    "             THEN ISNULL(lnloan_scheme_master.name, '') " & _
                    "             ELSE @Advance " & _
                    "        END AS lnScheme " & _
                    "      ,ISNULL(lnloan_advance_tran.loanvoucher_no, '') AS lnVocNo " & _
                    "       ,CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS lnDate " & _
                    "       ,CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                    "             THEN ISNULL(lnloan_advance_tran.loan_amount, 0) " & _
                    "             ELSE lnloan_advance_tran.advance_amount " & _
                    "        END AS lnAmount " & _
                    "       ,CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                    "             THEN ISNULL(lnloan_advance_tran.net_amount, 0) " & _
                    "             ELSE lnloan_advance_tran.advance_amount " & _
                    "        END AS lnNetAmount " & _
                    "       ,CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
                    "             WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
                    "             WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
                    "             WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed " & _
                    "        END AS lnStatus " & _
                    "       ,lnloan_advance_tran.loanadvancetranunkid AS lnUnkid " & _
                    "       ,lnloan_advance_tran.balance_amount AS lnBalance " & _
                    "      FROM " & _
                    "        lnloan_advance_tran " & _
                    "        LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                    "      WHERE " & _
                    "        ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                    "    ) AS lnHistory " & _
                    "WHERE " & _
                    "    lnUnkid = @Unkid "

            objDataOperation.AddParameter("@Unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoanAdvance_History_List", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Get_Unpaid_LoanAdvance_List(ByVal strTableName As String, Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
        'Sohail (12 Jan 2015) - [strUserAccessLevelFilterString]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'Sohail (20 Aug 2010) -- Start
            'Changes:Set loanscheme ='@Advance' for isloan=0
            'strQ = "SELECT  UnpaidLoan = ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                    "THEN ( loan_amount " & _
            '                           "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
            '                    "ELSE ( advance_amount " & _
            '                           "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
            '               "END ) , " & _
            '            "lnloan_advance_tran.loanvoucher_no AS VocNo , " & _
            '            "ISNULL(hremployee_master.firstname, '') + ' ' " & _
            '            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
            '            "+ ISNULL(hremployee_master.surname, '') AS EmpName , " & _
            '            "lnloan_scheme_master.name AS LoanScheme , " & _
            '            "CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
            '                 "ELSE @Advance " & _
            '            "END AS Loan_Advance , " & _
            '            "CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
            '                 "ELSE 2 " & _
            '            "END AS Loan_AdvanceUnkid , " & _
            '            "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                 "THEN lnloan_advance_tran.net_amount " & _
            '                 "ELSE lnloan_advance_tran.advance_amount " & _
            '            "END AS Amount , " & _
            '            "cfcommon_period_tran.period_name AS PeriodName , " & _
            '            "lnloan_advance_tran.loan_statusunkid , " & _
            '            "lnloan_advance_tran.periodunkid , " & _
            '            "lnloan_advance_tran.loanadvancetranunkid , " & _
            '            "hremployee_master.employeeunkid , " & _
            '            "hremployee_master.employeecode , " & _
            '            "lnloan_scheme_master.loanschemeunkid , " & _
            '            "lnloan_advance_tran.isloan , " & _
            '            "CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date , " & _
            '            "lnloan_advance_tran.balance_amount , " & _
            '            "lnloan_advance_tran.isloan , " & _
            '            "lnloan_advance_tran.loanschemeunkid , " & _
            '            "lnloan_advance_tran.emi_amount , " & _
            '            "lnloan_advance_tran.advance_amount , " & _
            '            "lnloan_advance_tran.loan_statusunkid " & _
            '    "FROM    lnloan_advance_tran " & _
            '            "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
            '            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
            '            "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
            '                                            "AND ISNULL(prpayment_tran.isvoid, " & _
            '                                                       "0) = 0 " & _
            '    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
            '    "GROUP BY lnloan_advance_tran.loanvoucher_no , " & _
            '            "ISNULL(hremployee_master.firstname, '') + ' ' " & _
            '            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
            '            "+ ISNULL(hremployee_master.surname, '') , " & _
            '            "lnloan_advance_tran.isloan , " & _
            '            "lnloan_advance_tran.loan_amount , " & _
            '            "lnloan_advance_tran.advance_amount , " & _
            '            "lnloan_scheme_master.name , " & _
            '            "lnloan_advance_tran.net_amount , " & _
            '            "cfcommon_period_tran.period_name , " & _
            '            "lnloan_advance_tran.loan_statusunkid , " & _
            '            "lnloan_advance_tran.periodunkid , " & _
            '            "lnloan_advance_tran.loanadvancetranunkid , " & _
            '            "hremployee_master.employeeunkid , " & _
            '            "hremployee_master.employeecode , " & _
            '            "lnloan_scheme_master.loanschemeunkid , " & _
            '            "lnloan_advance_tran.effective_date , " & _
            '            "lnloan_advance_tran.balance_amount , " & _
            '            "lnloan_advance_tran.loanschemeunkid , " & _
            '            "lnloan_advance_tran.emi_amount " & _
            '    "HAVING  ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                   "THEN ( loan_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
            '                   "ELSE ( advance_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
            '              "END ) > 0 "


            
            strQ = "SELECT  UnpaidLoan = ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                                            "THEN ( loan_amount " & _
                                                   "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
                                            "ELSE ( advance_amount " & _
                                                   "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
                                       "END ) , " & _
                        "lnloan_advance_tran.loanvoucher_no AS VoucherNo , " & _
                        "ISNULL(hremployee_master.firstname, '') + ' ' " & _
                        "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                        "+ ISNULL(hremployee_master.surname, '') AS EmpName , " & _
                        "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                            "THEN lnloan_scheme_master.name " & _
                            "ELSE @Advance " & _
                        "END AS LoanScheme , " & _
                        "CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
                             "ELSE @Advance " & _
                        "END AS Loan_Advance , " & _
                        "CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
                             "ELSE 2 " & _
                        "END AS Loan_AdvanceUnkid , " & _
                        "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                             "THEN lnloan_advance_tran.net_amount " & _
                             "ELSE lnloan_advance_tran.advance_amount " & _
                        "END AS Amount , " & _
                        "cfcommon_period_tran.period_name AS PeriodName , " & _
                        "lnloan_advance_tran.loan_statusunkid , " & _
                        "lnloan_advance_tran.periodunkid , " & _
                        "lnloan_advance_tran.loanadvancetranunkid , " & _
                        "hremployee_master.employeeunkid , " & _
                        "hremployee_master.employeecode , " & _
                        "lnloan_advance_tran.isloan , " & _
                        "CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date , " & _
                        "lnloan_advance_tran.balance_amount , " & _
                        "ISNULL(lnloan_advance_tran.loanschemeunkid, 0) AS loanschemeunkid , " & _
                        "lnloan_advance_tran.emi_amount , " & _
                        "lnloan_advance_tran.advance_amount , " & _
                        "lnloan_advance_tran.processpendingloanunkid " & _
                "FROM    lnloan_advance_tran " & _
                        "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
                        "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                        "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                                                        "AND ISNULL(prpayment_tran.isvoid, " & _
                                                                   "0) = 0 " & _
                "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "

            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'strQ &= UserAccessLevel._AccessLevelFilterString
            If strUserAccessLevelFilterString = "" Then
            strQ &= UserAccessLevel._AccessLevelFilterString
            Else
                strQ &= strUserAccessLevelFilterString
            End If
            'Sohail (12 Jan 2015) -- End
            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-End 

            strQ &= "GROUP BY lnloan_advance_tran.loanvoucher_no , " & _
                        "ISNULL(hremployee_master.firstname, '') + ' ' " & _
                        "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                        "+ ISNULL(hremployee_master.surname, '') , " & _
                        "lnloan_advance_tran.isloan , " & _
                        "lnloan_advance_tran.loan_amount , " & _
                        "lnloan_advance_tran.advance_amount , " & _
                        "lnloan_scheme_master.name , " & _
                        "lnloan_advance_tran.net_amount , " & _
                        "cfcommon_period_tran.period_name , " & _
                        "lnloan_advance_tran.loan_statusunkid , " & _
                        "lnloan_advance_tran.periodunkid , " & _
                        "lnloan_advance_tran.loanadvancetranunkid , " & _
                        "hremployee_master.employeeunkid , " & _
                        "hremployee_master.employeecode , " & _
                        "lnloan_scheme_master.loanschemeunkid , " & _
                        "lnloan_advance_tran.effective_date , " & _
                        "lnloan_advance_tran.balance_amount , " & _
                        "lnloan_advance_tran.loanschemeunkid , " & _
                        "lnloan_advance_tran.emi_amount , " & _
                        "lnloan_advance_tran.processpendingloanunkid " & _
                "HAVING  ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                               "THEN ( loan_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
                               "ELSE ( advance_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
                          "END ) > 0 "

            'Sohail (20 Aug 2010) -- End

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Advance"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Unpaid_LoanAdvance_List; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForLoanAdvanceTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForLoanAdvanceTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, Optional ByVal iUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 29 May 2013 ] -- START -- END
        'Public Function InsertAuditTrailForLoanAdvanceTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, Optional ByVal iUserUnkid As Integer = 0) As Boolean
        'Anjan (11 Jun 2011)-End
        Dim strQ As String = ""
        Dim exForce As Exception

        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atlnloan_advance_tran ( " & _
            '            "  loanadvancetranunkid " & _
            '            ", loanvoucher_no " & _
            '            ", effective_date " & _
            '            ", yearunkid " & _
            '            ", periodunkid " & _
            '            ", employeeunkid " & _
            '            ", approverunkid " & _
            '            ", loan_purpose " & _
            '            ", balance_amount " & _
            '            ", isbrought_forward " & _
            '            ", isloan " & _
            '            ", loanschemeunkid " & _
            '            ", loan_amount " & _
            '            ", interest_rate " & _
            '            ", loan_duration " & _
            '            ", interest_amount " & _
            '            ", net_amount " & _
            '            ", calctype_id " & _
            '            ", loanscheduleunkid " & _
            '            ", emi_tenure " & _
            '            ", emi_amount " & _
            '            ", advance_amount " & _
            '            ", loan_statusunkid " & _
            '            ", audittype " & _
            '            ", audituserunkid " & _
            '            ", auditdatetime " & _
            '            ", ip " & _
            '            ", machine_name " & _
            '            ", processpendingloanunkid " & _
            '      ") VALUES (" & _
            '            "  @loanadvancetranunkid " & _
            '            ", @loanvoucher_no " & _
            '            ", @effective_date " & _
            '            ", @yearunkid " & _
            '            ", @periodunkid " & _
            '            ", @employeeunkid " & _
            '            ", @approverunkid " & _
            '            ", @loan_purpose " & _
            '            ", @balance_amount " & _
            '            ", @isbrought_forward " & _
            '            ", @isloan " & _
            '            ", @loanschemeunkid " & _
            '            ", @loan_amount " & _
            '            ", @interest_rate " & _
            '            ", @loan_duration " & _
            '            ", @interest_amount " & _
            '            ", @net_amount " & _
            '            ", @calctype_id " & _
            '            ", @loanscheduleunkid " & _
            '            ", @emi_tenure " & _
            '            ", @emi_amount " & _
            '            ", @advance_amount " & _
            '            ", @loan_statusunkid " & _
            '            ", @audittype " & _
            '            ", @audituserunkid " & _
            '            ", @auditdatetime " & _
            '            ", @ip " & _
            '            ", @machine_name " & _
            '            ", @processpendingloanunkid " & _
            '        "); "

            strQ = "INSERT INTO atlnloan_advance_tran ( " & _
                        "  loanadvancetranunkid " & _
                        ", loanvoucher_no " & _
                        ", effective_date " & _
                        ", yearunkid " & _
                        ", periodunkid " & _
                        ", employeeunkid " & _
                        ", approverunkid " & _
                        ", loan_purpose " & _
                        ", balance_amount " & _
                        ", isbrought_forward " & _
                        ", isloan " & _
                        ", loanschemeunkid " & _
                        ", loan_amount " & _
                        ", interest_rate " & _
                        ", loan_duration " & _
                        ", interest_amount " & _
                        ", net_amount " & _
                        ", calctype_id " & _
                        ", loanscheduleunkid " & _
                        ", emi_tenure " & _
                        ", emi_amount " & _
                        ", advance_amount " & _
                        ", loan_statusunkid " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name " & _
                        ", processpendingloanunkid " & _
                        ", form_name " & _
                        ", module_name1 " & _
                        ", module_name2 " & _
                        ", module_name3 " & _
                        ", module_name4 " & _
                        ", module_name5 " & _
                        ", isweb " & _
                        ", bf_amount  " & _
                        ", loginemployeeunkid " & _
                        ", deductionperiodunkid" & _
                  ") VALUES (" & _
                        "  @loanadvancetranunkid " & _
                        ", @loanvoucher_no " & _
                        ", @effective_date " & _
                        ", @yearunkid " & _
                        ", @periodunkid " & _
                        ", @employeeunkid " & _
                        ", @approverunkid " & _
                        ", @loan_purpose " & _
                        ", @balance_amount " & _
                        ", @isbrought_forward " & _
                        ", @isloan " & _
                        ", @loanschemeunkid " & _
                        ", @loan_amount " & _
                        ", @interest_rate " & _
                        ", @loan_duration " & _
                        ", @interest_amount " & _
                        ", @net_amount " & _
                        ", @calctype_id " & _
                        ", @loanscheduleunkid " & _
                        ", @emi_tenure " & _
                        ", @emi_amount " & _
                        ", @advance_amount " & _
                        ", @loan_statusunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @processpendingloanunkid " & _
                        ", @form_name " & _
                        ", @module_name1 " & _
                        ", @module_name2 " & _
                        ", @module_name3 " & _
                        ", @module_name4 " & _
                        ", @module_name5 " & _
                        ", @isweb " & _
                        ", @bf_amount  " & _
                        ", @loginemployeeunkid " & _
                        ", @deductionperiodunkid" & _
                    "); "
            'Sohail (08 Apr 2013) - [deductionperiodunkid]
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
            objDataOperation.AddParameter("@effective_date ", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)

            'Sandeep [ 16 Oct 2010 ] -- Start
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, Now.ToString)

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(iUserUnkid <= 0, User._Object._Userunkid, iUserUnkid))
            'S.SANDEEP [ 29 May 2013 ] -- END

            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            'Sandeep [ 16 Oct 2010 ] -- End 

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            'S.SANDEEP [ 13 AUG 2012 ] -- END

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

            'Anjan (08 Aug 2012)-Start
            'Issue : Loan report was picking principal amount instead of loan balance in new year.
            objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) '
            'Anjan (08 Aug 2012)-End 
            



            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 6, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            'S.SANDEEP [ 13 AUG 2012 ] -- END
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString) 'Sohail (08 Apr 2013)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForLoanAdvanceTran", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End
        End Try
    End Function

    'Sohail (04 Apr 2011) -- Start
    Public Function Is_Payment_Done(Optional ByVal intDeductionPeriodID As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try


            
            strQ = "SELECT  lnloan_advance_tran.loanadvancetranunkid " & _
                          ", lnloan_advance_tran.loanvoucher_no " & _
                          ", lnloan_advance_tran.effective_date " & _
                          ", lnloan_advance_tran.yearunkid " & _
                          ", lnloan_advance_tran.periodunkid " & _
                          ", lnloan_advance_tran.employeeunkid " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                          ", lnloan_scheme_master.name AS LoanSchemeName " & _
                          ", lnloan_advance_tran.approverunkid " & _
                          ", lnloan_advance_tran.loan_purpose " & _
                          ", lnloan_advance_tran.balance_amount " & _
                          ", lnloan_advance_tran.isbrought_forward " & _
                          ", lnloan_advance_tran.isloan " & _
                          ", lnloan_advance_tran.loanschemeunkid " & _
                          ", lnloan_advance_tran.loan_amount " & _
                          ", lnloan_advance_tran.interest_rate " & _
                          ", lnloan_advance_tran.loan_duration " & _
                          ", lnloan_advance_tran.interest_amount " & _
                          ", lnloan_advance_tran.net_amount " & _
                          ", lnloan_advance_tran.calctype_id " & _
                          ", lnloan_advance_tran.loanscheduleunkid " & _
                          ", lnloan_advance_tran.emi_tenure " & _
                          ", lnloan_advance_tran.emi_amount " & _
                          ", lnloan_advance_tran.advance_amount " & _
                          ", lnloan_advance_tran.loan_statusunkid " & _
                          ", lnloan_advance_tran.userunkid " & _
                          ", lnloan_advance_tran.isvoid " & _
                          ", lnloan_advance_tran.voiduserunkid " & _
                          ", lnloan_advance_tran.voiddatetime " & _
                          ", lnloan_advance_tran.voidreason " & _
                          ", lnloan_advance_tran.processpendingloanunkid " & _
                          ", lnloan_advance_tran.deductionperiodunkid " & _
                    "FROM    lnloan_advance_tran " & _
                            "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                                    "AND prpayment_tran.referenceid IN ( 1, 2 ) " & _
                                    "AND prpayment_tran.paytypeid = 1 " & _
                            "LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid=lnloan_scheme_master.loanschemeunkid " & _
                            "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid=hremployee_master.employeeunkid " & _
                            "LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
                    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND lnloan_process_pending_loan.isexternal_entity = 0 " & _
                            "AND prpayment_tran.paymenttranunkid IS NULL "


            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            strQ &= UserAccessLevel._AccessLevelFilterString
            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-End 



            If intDeductionPeriodID > 0 Then
                strQ &= "AND lnloan_advance_tran.deductionperiodunkid = @deductionperiodunkid "
                objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeductionPeriodID)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows.Count <= 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Unpaid_LoanAdvance_List; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (04 Apr 2011) -- End



    'S.SANDEEP [ 06 SEP 2011 ] -- START
    'ENHANCEMENT : MAKING COMMON FUNCTION
    Public Function Calculate_LoanInfo(ByVal enCalc As enLoanCalcId, _
                                                       ByVal decPrincipleAmt As Decimal, _
                                                       ByVal intDuration As Integer, _
                                                       ByVal dblRate As Double, _
                                                       ByRef mdecNetAmount As Decimal, _
                                                       ByRef mdecInterest_Amount As Decimal, _
                                                       Optional ByRef decRedEMI As Decimal = 0) As Decimal
        Try
            Select Case enCalc
                Case enLoanCalcId.Simple_Interest
                    If intDuration > 12 Then
                        mdecInterest_Amount = (decPrincipleAmt * dblRate * (intDuration / 12)) / 100
                    Else
                        mdecInterest_Amount = ((decPrincipleAmt * dblRate * intDuration) / 100) / 12
                    End If
                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt
                Case enLoanCalcId.Compound_Interest
                    If intDuration > 12 Then
                        mdecInterest_Amount = decPrincipleAmt * CDec(Pow((1 + (dblRate / 100)), (intDuration / 12)))
                    Else
                        mdecInterest_Amount = decPrincipleAmt * CDec(Pow((1 + (dblRate / 100)), intDuration))
                    End If
                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt
                Case enLoanCalcId.Reducing_Amount
                    Dim mdecMonthlyRate, mdecTotalRate, mdecNewReduceAmt As Decimal

                    mdecMonthlyRate = 0 : mdecTotalRate = 0 : mdecNewReduceAmt = 0

                    mdecMonthlyRate = ((dblRate / 12) / 100) + 1
                    mdecTotalRate = CDec(Pow(mdecMonthlyRate, intDuration)) - 1

                    If mdecTotalRate > 0 Then
                        mdecTotalRate = (mdecMonthlyRate - 1) / mdecTotalRate
                    Else
                        mdecTotalRate = 0
                    End If

                    mdecTotalRate = mdecTotalRate + (mdecMonthlyRate - 1)
                    decRedEMI = decPrincipleAmt * mdecTotalRate

                    For i As Integer = 1 To CInt(intDuration)
                        If i = 1 Then
                            mdecInterest_Amount += decPrincipleAmt * (mdecMonthlyRate - 1)
                            mdecNewReduceAmt = (decPrincipleAmt - (decRedEMI - (decPrincipleAmt * (mdecMonthlyRate - 1))))
                        Else
                            mdecInterest_Amount += mdecNewReduceAmt * (mdecMonthlyRate - 1)
                            mdecNewReduceAmt = (mdecNewReduceAmt - (decRedEMI - (mdecNewReduceAmt * (mdecMonthlyRate - 1))))
                        End If
                    Next

                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_LoanInfo", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Calculate_LoanEMI(ByVal intEMI_ModeId As Integer, ByVal decNetAmount As Decimal, ByRef decInstallmentAmt As Decimal, ByRef intNoOfEMI As Integer) As Decimal
        Try
            Select Case intEMI_ModeId
                Case 1  'AMOUNT
                    If decInstallmentAmt > 0 Then
                        intNoOfEMI = CInt(IIf(decInstallmentAmt = 0, 0, decNetAmount / decInstallmentAmt))
                    Else
                        intNoOfEMI = 0
                        Exit Try
                    End If

                    If CInt(CDec(IIf(decInstallmentAmt = 0, 0, decNetAmount / decInstallmentAmt))) > intNoOfEMI Then
                        intNoOfEMI += 1
                    End If
                Case 2  'MONTHS
                    If intNoOfEMI > 0 Then
                        decInstallmentAmt = CDec(IIf(intNoOfEMI = 0, 0, decNetAmount / intNoOfEMI))
                    Else
                        decInstallmentAmt = 0
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_LoanEMI", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 06 SEP 2011 ] -- END 

    'Sohail (08 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetDataForProcessPayroll(ByVal strTableName As String, ByVal intEmpUnkID As Integer, ByVal dtPeriodStart As DateTime) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            strQ = "SELECT   loanadvancetranunkid " & _
                          ", lnloan_advance_tran.balance_amount " & _
                          ", lnloan_advance_tran.isloan " & _
                          ", lnloan_advance_tran.emi_amount " & _
                          ", lnloan_advance_tran.advance_amount " & _
                          ", lnloan_advance_tran.balance_amount " & _
                    "FROM    lnloan_advance_tran " & _
                    "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
                    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                    "AND     balance_amount > 0 " & _
                    "AND     loan_statusunkid = 1 " & _
                    "AND     TableDeductionPeriod.start_date <= @startdate " & _
                    "AND     employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)

            dtTable = objDataOperation.ExecQuery(strQ, strTableName).Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataForProcessPayroll; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dtTable IsNot Nothing Then dtTable.Dispose()
            objDataOperation = Nothing
        End Try
        Return dtTable
    End Function
    'Sohail (08 Jun 2012) -- End

    'Sohail (29 Jun 2015) -- Start
    'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
    Public Function GetLoanBalance(ByVal strTableName As String _
                                   , Optional ByVal strPeriodIDs As String = "" _
                                   , Optional ByVal strEmployeeIDs As String = "" _
                                   , Optional ByVal strLoanSchemeIDs As String = "" _
                                   , Optional ByVal strLoanAdvanceUnkIDs As String = "" _
                                   , Optional ByVal intBranchId As Integer = 0 _
                                   , Optional ByVal blnFirstNamethenSurname As Boolean = True _
                                   , Optional ByVal dtActiveEmpStartDate As Date = Nothing _
                                   , Optional ByVal dtActiveEmpEndDate As Date = Nothing _
                                   , Optional ByVal dtDatabaseStartDate As Date = Nothing _
                                   , Optional ByVal dtDatabaseEndDate As Date = Nothing _
                                   , Optional ByVal str_AccessLevelFilterString As String = "" _
                                   , Optional ByVal strFilter As String = "" _
                                   ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, 1, mintYearunkid)
            If str_AccessLevelFilterString.Trim = "" Then str_AccessLevelFilterString = UserAccessLevel._AccessLevelFilterString

            strQ = "CREATE TABLE #tbl " & _
                    "( " & _
                      "unkid INT IDENTITY(1, 1) " & _
                    ", loanschemeunkid INT " & _
                    ", periodunkid INT " & _
                    ", loanadvancetranunkid INT " & _
                    ", employeeunkid INT " & _
                    ", payrollprocesstranunkid INT " & _
                    ", paymenttranunkid INT " & _
                    ", loanbf_amount DECIMAL(36, 6) " & _
                    ", amount DECIMAL(36, 6) " & _
                    ", enddate DATETIME " & _
                    ") " & _
                " " & _
                "INSERT  INTO #tbl " & _
                        "( loanschemeunkid " & _
                        ", periodunkid " & _
                        ", loanadvancetranunkid " & _
                        ", employeeunkid " & _
                        ", payrollprocesstranunkid " & _
                        ", paymenttranunkid " & _
                        ", loanbf_amount " & _
                        ", amount " & _
                        ", enddate " & _
                        ") " & _
                        "SELECT  lnloan_scheme_master.loanschemeunkid " & _
                              ", TableLoan.payperiodunkid " & _
                              ", TableLoan.loanadvancetranunkid " & _
                              ", TableLoan.employeeunkid " & _
                              ", TableLoan.payrollprocesstranunkid " & _
                              ", TableLoan.paymenttranunkid " & _
                              ", ISNULL(TableBF.cf_amount, lnloan_advance_tran.bf_amount) AS bf_amount " & _
                              ", TableLoan.amount " & _
                              ", TableLoan.enddate " & _
                        "FROM    ( SELECT    prtnaleave_tran.payperiodunkid " & _
                                          ", prpayrollprocess_tran.employeeunkid " & _
                                          ", prpayrollprocess_tran.loanadvancetranunkid AS loanadvancetranunkid " & _
                                          ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                                          ", 0 AS paymenttranunkid " & _
                                          ", prpayrollprocess_tran.amount " & _
                                          ", cfcommon_period_tran.end_date AS enddate " & _
                                  "FROM      prpayrollprocess_tran " & _
                                            "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                            "JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                                            "INNER JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                  "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                            "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                            "AND lnloan_advance_tran.isloan = 1 " & _
                                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                            "AND cfcommon_period_tran.periodunkid = @FirstOpenPeriodId " & _
                                            "AND lnloan_advance_tran.bf_amount IS NOT NULL "

            If strEmployeeIDs.Trim <> "" Then
                strQ &= "                   AND prpayrollprocess_tran.employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            If strLoanSchemeIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_advance_tran.loanschemeunkid IN (" & strLoanSchemeIDs & ") "
            End If

            If strLoanAdvanceUnkIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_advance_tran.loanadvancetranunkid IN (" & strLoanAdvanceUnkIDs & ") "
            End If

            strQ &= "UNION ALL " & _
                                  "SELECT    Payment.periodunkid " & _
                                          ", employeeunkid " & _
                                          ", loanadvancetranunkid " & _
                                          ", 0 AS payrollprocesstranunkid " & _
                                          ", Payment.paymenttranunkid " & _
                                          ", amount " & _
                                          ", cfcommon_period_tran.end_date AS enddate " & _
                                  "FROM      ( SELECT    ( SELECT    C.periodunkid " & _
                                                          "FROM      cfcommon_period_tran " & _
                                                                    "AS C " & _
                                                          "WHERE     CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) BETWEEN C.start_date AND C.end_date " & _
                                                                    "AND c.modulerefid = " & enModuleReference.Payroll & " " & _
                                                                    "AND c.isactive = 1 " & _
                                                        ") AS periodunkid " & _
                                                      ", prpayment_tran.paymenttranunkid " & _
                                      ", prpayment_tran.employeeunkid " & _
                                      ", prpayment_tran.referencetranunkid AS loanadvancetranunkid " & _
                                                      ", prpayment_tran.amount " & _
                                              "FROM      prpayment_tran " & _
                                                        "JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                              "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                        "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.LOAN & " " & _
                                                        "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.RECEIVED & " " & _
                                                        "AND prpayment_tran.paymentdate BETWEEN @db_start_date AND @db_end_date "

            If strEmployeeIDs.Trim <> "" Then
                strQ &= "                   AND prpayment_tran.employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            If strLoanSchemeIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_advance_tran.loanschemeunkid IN (" & strLoanSchemeIDs & ") "
            End If

            If strLoanAdvanceUnkIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_advance_tran.loanadvancetranunkid IN (" & strLoanAdvanceUnkIDs & ") "
            End If

            strQ &= "                       ) AS Payment " & _
                                            "JOIN cfcommon_period_tran ON Payment.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                                              "AND cfcommon_period_tran.periodunkid = @FirstOpenPeriodId " & _
                                ") AS TableLoan " & _
                            "INNER JOIN lnloan_advance_tran ON TableLoan.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                            "INNER JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                                "LEFT JOIN ( SELECT  loanadvancetranunkid " & _
                                                  ", MIN(cf_amount) AS cf_amount " & _
                                            "FROM    lnloan_balance_tran " & _
                                            "WHERE   ISNULL(isvoid, 0) = 0  "

            If strEmployeeIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_balance_tran.employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            If strLoanSchemeIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_balance_tran.loanschemeunkid IN (" & strLoanSchemeIDs & ") "
            End If

            If strLoanAdvanceUnkIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_balance_tran.loanadvancetranunkid IN (" & strLoanAdvanceUnkIDs & ") "
            End If

            strQ &= "GROUP BY loanadvancetranunkid " & _
                                          ") AS TableBF ON TableBF.loanadvancetranunkid = TableLoan.loanadvancetranunkid " & _
                        "ORDER BY TableLoan.loanadvancetranunkid " & _
                               ", TableLoan.enddate " & _
                    " " & _
                    "SELECT  a.loanschemeunkid AS LoanSchemeId " & _
                          ", lnloan_scheme_master.code AS LoanCode " & _
                          ", lnloan_scheme_master.name AS LoanSchemeName " & _
                          ", a.periodunkid AS PeriodId " & _
                             ", cfcommon_period_tran.period_name AS PeriodName " & _
                          ", CONVERT(CHAR(8), a.end_date, 112) AS PeriodEndDate " & _
                          ", a.loanadvancetranunkid AS LoanAdvanceTranUnkId " & _
                          ", a.employeeunkid AS EmpId " & _
                               ", hremployee_master.employeecode AS EmpCode "

            If blnFirstNamethenSurname = False Then
                strQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
            Else
                strQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
            End If


            strQ &= ", a.bf_amount AS LoanAmountBF " & _
                      ", a.amount AS DeductionAmount " & _
                      ", a.cf_amount AS LoanAmountCF " & _
                          ", lnloan_advance_tran.interest_amount AS InterestAmount " & _
                      ", ( a.amount * lnloan_advance_tran.interest_amount ) / lnloan_advance_tran.net_amount AS InterestDeduction " & _
                      ", CASE ISNULL(receipt.voucherno, " & _
                                    "ISNULL(prglobalvoc_master.globalvocno, lnloan_advance_tran.loanvoucher_no)) " & _
                          "WHEN '' THEN prpayment_tran.voucherno ELSE ISNULL(receipt.voucherno, ISNULL(prglobalvoc_master.globalvocno, lnloan_advance_tran.loanvoucher_no)) " & _
                        "END AS loanvoucher_no " & _
                      ", CASE WHEN receipt.voucherno IS NULL THEN 0 ELSE 1 END AS IsReceipt " & _
                "FROM    ( SELECT    lnloan_balance_tran.loanschemeunkid " & _
                                  ", lnloan_balance_tran.periodunkid " & _
                                  ", lnloan_balance_tran.end_date " & _
                                  ", lnloan_balance_tran.loanadvancetranunkid " & _
                                  ", lnloan_balance_tran.employeeunkid " & _
                                  ", lnloan_balance_tran.payrollprocesstranunkid " & _
                                  ", lnloan_balance_tran.paymenttranunkid " & _
                                  ", lnloan_balance_tran.bf_amount " & _
                                  ", lnloan_balance_tran.amount " & _
                                  ", lnloan_balance_tran.cf_amount " & _
                          "FROM      lnloan_balance_tran " & _
                          "WHERE   ISNULL(lnloan_balance_tran.isvoid, 0) = 0 "

            If strPeriodIDs.Trim <> "" Then
                strQ &= " AND lnloan_balance_tran.periodunkid IN (" & strPeriodIDs & ") "
            End If

            If strEmployeeIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_balance_tran.employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            If strLoanSchemeIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_balance_tran.loanschemeunkid IN (" & strLoanSchemeIDs & ") "
            End If

            If strLoanAdvanceUnkIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_balance_tran.loanadvancetranunkid IN (" & strLoanAdvanceUnkIDs & ") "
            End If

            strQ &= "      UNION ALL " & _
                          "SELECT    a.loanschemeunkid " & _
                                  ", a.periodunkid " & _
                                  ", a.enddate " & _
                                  ", a.loanadvancetranunkid " & _
                                  ", a.employeeunkid " & _
                                  ", a.payrollprocesstranunkid " & _
                                  ", a.paymenttranunkid " & _
                                  ", a.loanbf_amount - SUM(b.amount) + a.amount AS BFAmount " & _
                                  ", a.amount AS Amount " & _
                                  ", a.loanbf_amount - SUM(b.amount) AS CFAmount " & _
                          "FROM      #tbl a " & _
                                    "JOIN #tbl AS b ON a.employeeunkid = b.employeeunkid " & _
                                                      "AND a.loanadvancetranunkid = b.loanadvancetranunkid " & _
                                                      "AND b.unkid <= a.unkid " & _
                          "GROUP BY  a.loanadvancetranunkid " & _
                                  ", a.loanschemeunkid " & _
                                  ", a.employeeunkid " & _
                                  ", a.loanbf_amount " & _
                                  ", a.enddate " & _
                                  ", a.periodunkid " & _
                                  ", a.payrollprocesstranunkid " & _
                                  ", a.paymenttranunkid " & _
                                  ", a.amount " & _
                        ") AS a " & _
                        "JOIN cfcommon_period_tran ON a.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "JOIN hremployee_master ON hremployee_master.employeeunkid = a.employeeunkid " & _
                        "LEFT JOIN lnloan_scheme_master ON a.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                        "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = a.loanadvancetranunkid " & _
                        "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.payrollprocesstranunkid = a.payrollprocesstranunkid " & _
                        "LEFT JOIN prpayment_tran ON prpayment_tran.referencetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND a.payrollprocesstranunkid > 0 " & _
                                                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                        "AND prpayment_tran.referenceid IN ( " & clsPayment_tran.enPaymentRefId.PAYSLIP & " ) " & _
                        "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
                        "LEFT JOIN prpayment_tran AS receipt ON receipt.paymenttranunkid = a.paymenttranunkid " & _
                                                                   "AND a.paymenttranunkid > 0 " & _
                                                                   "AND ISNULL(receipt.isvoid, 0) = 0 " & _
                                                                   "AND receipt.referenceid IN ( " & clsPayment_tran.enPaymentRefId.LOAN & " ) " & _
                                                                   "AND receipt.isreceipt = 1 " & _
                        "WHERE  cfcommon_period_tran.isactive = 1 " & _
                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                        "AND lnloan_scheme_master.isactive = 1 " & _
                        "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                        "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "

            If strPeriodIDs.Trim <> "" Then
                strQ &= " AND a.periodunkid IN (" & strPeriodIDs & ") "
            End If

            If strEmployeeIDs.Trim <> "" Then
                strQ &= "                   AND a.employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            If strLoanSchemeIDs.Trim <> "" Then
                strQ &= "                   AND a.loanschemeunkid IN (" & strLoanSchemeIDs & ") "
            End If

            If strLoanAdvanceUnkIDs.Trim <> "" Then
                strQ &= "                   AND lnloan_balance_tran.loanadvancetranunkid IN (" & strLoanAdvanceUnkIDs & ") "
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If


            If dtActiveEmpStartDate <> Nothing AndAlso dtActiveEmpEndDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate" & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtActiveEmpStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtActiveEmpEndDate))
            End If

            If intBranchId > 0 Then
                strQ &= " AND hremployee_master.stationunkid = @BranchId "
            End If

            If str_AccessLevelFilterString.Length > 0 Then
                strQ &= str_AccessLevelFilterString
            End If

            'If strPeriodIDs.Trim <> "" AndAlso mblnShowInterestInformation = False Then
            '    strQ &= "AND a.periodunkid = @PeriodId "
            'End If

            strQ &= "ORDER BY a.loanadvancetranunkid " & _
                       ", CONVERT(CHAR(8), a.end_date, 112) " & _
                       ", a.BF_Amount DESC "

            strQ &= "DROP TABLE #tbl "


            objDataOperation.AddParameter("@FirstOpenPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intFirstOpenPeriodID)
            objDataOperation.AddParameter("@db_start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(IIf(dtDatabaseStartDate <> Nothing, dtDatabaseStartDate, FinancialYear._Object._Database_Start_Date)))
            objDataOperation.AddParameter("@db_end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(IIf(dtDatabaseEndDate <> Nothing, dtDatabaseEndDate, FinancialYear._Object._Database_End_Date)))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataForProcessPayroll; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (29 Jun 2015) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
			Language.setMessage(mstrModuleName, 2, "Loan")
			Language.setMessage(mstrModuleName, 3, "Advance")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Loan/Advance. Reason : This Loan/Advance is already linked to some transactions.")
			Language.setMessage(mstrModuleName, 5, "Advance")
			Language.setMessage(mstrModuleName, 6, "WEB")
			Language.setMessage("clsMasterData", 96, "In Progress")
			Language.setMessage("clsMasterData", 97, "On Hold")
			Language.setMessage("clsMasterData", 98, "Written Off")
			Language.setMessage("clsMasterData", 100, "Completed")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class