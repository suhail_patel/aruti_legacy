﻿'************************************************************************************************************************************
'Class Name : clsloanotherop_approval_tran.vb
'Purpose    : Approval Process in Loan Other Operation Parameters
'Date       : 23-Nov-2015
'Written By : Nilay Mistry
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm


Public Class clsloanotherop_approval_tran

    Private Shared ReadOnly mstrModuleName As String = "clsloanotherop_approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region "Private Variables"

    Private objInterestTran As New clslnloan_interest_tran
    Private objEmiTenureTran As New clslnloan_emitenure_tran
    Private objTopupTran As New clslnloan_topup_tran
    Private mintlnOtherOptranunkid As Integer = -1
    Private mintLoanAdvancetranunkid As Integer = -1
    Private mintPeriodunkid As Integer = -1
    Private mintLoanOpTypeId As Integer = -1
    Private mintEmployeeunkid As Integer = -1
    Private mintApprovertranunkid As Integer = -1
    Private mintApproverEmpunkid As Integer = -1
    Private mintPriority As Integer = -1
    Private mintvisibleid As Integer = -1
    Private mintStatusunkid As Integer = 1
    Private mdtApprovalDate As Date = Nothing
    Private mdtEffectiveDate As Date = Nothing
    Private mdbInterestRate As Double = 0
    Private mintEmiTenure As Integer = 0
    Private mdecEmiAmount As Decimal = 0
    Private mintLoanDuration As Integer = 0
    Private mdtEndDate As Date = Nothing
    Private mdecTopupAmount As Decimal = 0
    Private mdecPrincipalAmount As Decimal = 0
    Private mdecInterestAmount As Decimal = 0
    Private mdecExchangeRate As Decimal = 0
    Private mdecBaseCurrencyAmount As Decimal = 0
    Private mblnFinalApproved As Boolean = False
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer = -1
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = -1
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mintFinalStatus As Integer
    Private mstrIdentifyGuid As String = String.Empty
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty
    Private mintMinApprovedPriority As Integer = -1
    'Nilay (27-Dec-2016) -- End

    'Varsha Rana (12-Sept-2017) -- Start
    'Enhancement - Loan Topup in ESS
    Private mblnIsfromess As Boolean = False
    ' Varsha Rana (12-Sept-2017) -- End



#End Region

#Region "Properties"

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _LoanOtherOptranunkid() As Integer
        Get
            Return mintlnOtherOptranunkid
        End Get
        Set(ByVal value As Integer)
            mintlnOtherOptranunkid = value
            Call getData()
        End Set
    End Property

    Public Property _LoanAdvancetranunkid() As Integer
        Get
            Return mintLoanAdvancetranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanAdvancetranunkid = value
        End Set
    End Property

    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Public Property _LoanOperationTypeId() As Integer
        Get
            Return mintLoanOpTypeId
        End Get
        Set(ByVal value As Integer)
            mintLoanOpTypeId = value
        End Set
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = value
        End Set
    End Property

    Public Property _ApproverEmpunkid() As Integer
        Get
            Return mintApproverEmpunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverEmpunkid = value
        End Set
    End Property

    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    Public Property _Visibleid() As Integer
        Get
            Return mintvisibleid
        End Get
        Set(ByVal value As Integer)
            mintvisibleid = value
        End Set
    End Property

    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    Public Property _ApprovalDate() As Date
        Get
            Return mdtApprovalDate
        End Get
        Set(ByVal value As Date)
            mdtApprovalDate = value
        End Set
    End Property

    Public Property _EffectiveDate() As Date
        Get
            Return mdtEffectiveDate
        End Get
        Set(ByVal value As Date)
            mdtEffectiveDate = value
        End Set
    End Property

    Public Property _InterestRate() As Double
        Get
            Return mdbInterestRate
        End Get
        Set(ByVal value As Double)
            mdbInterestRate = value
        End Set
    End Property

    Public Property _EmiTenure() As Integer
        Get
            Return mintEmiTenure
        End Get
        Set(ByVal value As Integer)
            mintEmiTenure = value
        End Set
    End Property

    Public Property _EmiAmount() As Decimal
        Get
            Return mdecEmiAmount
        End Get
        Set(ByVal value As Decimal)
            mdecEmiAmount = value
        End Set
    End Property

    Public Property _LoanDuration() As Integer
        Get
            Return mintLoanDuration
        End Get
        Set(ByVal value As Integer)
            mintLoanDuration = value
        End Set
    End Property

    Public Property _EndDate() As Date
        Get
            Return mdtEndDate
        End Get
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public Property _TopupAmount() As Decimal
        Get
            Return mdecTopupAmount
        End Get
        Set(ByVal value As Decimal)
            mdecTopupAmount = value
        End Set
    End Property

    Public Property _PrincipalAmount() As Decimal
        Get
            Return mdecPrincipalAmount
        End Get
        Set(ByVal value As Decimal)
            mdecPrincipalAmount = value
        End Set
    End Property

    Public Property _InterestAmount() As Decimal
        Get
            Return mdecInterestAmount
        End Get
        Set(ByVal value As Decimal)
            mdecInterestAmount = value
        End Set
    End Property

    Public Property _ExchangeRate() As Decimal
        Get
            Return mdecExchangeRate
        End Get
        Set(ByVal value As Decimal)
            mdecExchangeRate = value
        End Set
    End Property

    Public Property _BaseCurrencyAmount() As Decimal
        Get
            Return mdecBaseCurrencyAmount
        End Get
        Set(ByVal value As Decimal)
            mdecBaseCurrencyAmount = value
        End Set
    End Property

    Public Property _FinalApproved() As Boolean
        Get
            Return mblnFinalApproved
        End Get
        Set(ByVal value As Boolean)
            mblnFinalApproved = value
        End Set
    End Property

    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _VoidDateTime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _VoidReason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _FinalStatus() As Integer
        Get
            Return mintFinalStatus
        End Get
        Set(ByVal value As Integer)
            mintFinalStatus = value
        End Set
    End Property

    Public Property _Identify_Guid() As String
        Get
            Return mstrIdentifyGuid
        End Get
        Set(ByVal value As String)
            mstrIdentifyGuid = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'Nilay (27-Dec-2016) -- Start
    Public Property _MinApprovedPriority() As Integer
        Get
            Return mintMinApprovedPriority
        End Get
        Set(ByVal value As Integer)
            mintMinApprovedPriority = value
        End Set
    End Property
    'Nilay (27-Dec-2016) -- End


    'Varsha Rana (12-Sept-2017) -- Start
    'Enhancement - Loan Topup in ESS
    Public Property _Isfromess() As Boolean
        Get
            Return mblnIsfromess
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromess = value
        End Set
    End Property
    ' Varsha Rana (12-Sept-2017) -- End



#End Region

    'Nilay (02-Jul-2016) -- Start
#Region " Enum "
    Public Enum enOtherOpStatus
        Pending = 1
        Approved = 2
        NoOtherOp = 100
    End Enum
#End Region
    'Nilay (02-Jul-2016) -- End


    Public Sub getData(Optional ByVal objDataOp As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If objDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDataOp
            End If

            strQ = "SELECT " & _
                        "  lnotheroptranunkid " & _
                        ", loanadvancetranunkid " & _
                        ", periodunkid " & _
                        ", lnoperationtypeid " & _
                        ", employeeunkid " & _
                        ", approvertranunkid " & _
                        ", approverempunkid " & _
                        ", statusunkid " & _
                        ", priority " & _
                        ", visibleid " & _
                        ", approvaldate " & _
                        ", effectivedate " & _
                        ", interest_rate " & _
                        ", emi_tenure " & _
                        ", emi_amount " & _
                        ", loan_duration " & _
                        ", end_date " & _
                        ", principal_amount " & _
                        ", interest_amount " & _
                        ", topup_amount " & _
                        ", exchange_rate " & _
                        ", basecurrency_amount " & _
                        ", final_approved " & _
                        ", remark " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                        ", final_status " & _
                        ", identify_guid " & _
                        ", isfromess " & _
                   "FROM lnloanotherop_approval_tran " & _
                   "WHERE lnotheroptranunkid=@lnotheroptranunkid "

            'Varsha Rana (12-Sept-2017) -- [isfromess]



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lnotheroptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnOtherOptranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows

                mintlnOtherOptranunkid = CInt(dRow.Item("lnotheroptranunkid"))
                mintLoanAdvancetranunkid = CInt(dRow.Item("loanadvancetranunkid"))
                mintPeriodunkid = CInt(dRow.Item("periodunkid"))
                mintLoanOpTypeId = CInt(dRow.Item("lnoperationtypeid"))
                mintEmployeeunkid = CInt(dRow.Item("employeeunkid"))
                mintApprovertranunkid = CInt(dRow.Item("approvertranunkid"))
                mintApproverEmpunkid = CInt(dRow.Item("approverempunkid"))
                mintStatusunkid = CInt(dRow.Item("statusunkid"))
                mintPriority = CInt(dRow.Item("priority"))
                mintvisibleid = CInt(dRow.Item("visibleid"))
                If IsDBNull(dRow.Item("approvaldate")) Then
                    mdtApprovalDate = Nothing
                Else
                    mdtApprovalDate = dRow.Item("approvaldate")
                End If
                If IsDBNull(dRow.Item("effectivedate")) Then
                    mdtEffectiveDate = Nothing
                Else
                    mdtEffectiveDate = dRow.Item("effectivedate")
                End If
                mdbInterestRate = CDbl(dRow.Item("interest_rate"))
                mintEmiTenure = CInt(dRow.Item("emi_tenure"))
                mdecEmiAmount = CDec(dRow.Item("emi_amount"))
                mintLoanDuration = CInt(dRow.Item("loan_duration"))
                If IsDBNull(dRow.Item("end_date")) Then
                    mdtEndDate = Nothing
                Else
                    mdtEndDate = dRow.Item("end_date")
                End If
                mdecPrincipalAmount = CDec(dRow.Item("principal_amount"))
                mdecInterestAmount = CDec(dRow.Item("interest_amount"))
                mdecTopupAmount = CDec(dRow.Item("topup_amount"))
                mdecExchangeRate = CDec(dRow.Item("exchange_rate"))
                mdecBaseCurrencyAmount = CDec(dRow.Item("basecurrency_amount"))
                mblnFinalApproved = CBool(dRow.Item("final_approved"))
                mstrRemark = dRow.Item("remark").ToString
                mintUserunkid = CInt(dRow.Item("userunkid"))
                mblnIsvoid = CBool(dRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dRow.Item("voiduserunkid"))
                If IsDBNull(dRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dRow.Item("voiddatetime")
                End If
                mstrVoidreason = dRow.Item("voidreason").ToString
                mintFinalStatus = CInt(dRow("final_status"))
                mstrIdentifyGuid = CStr(dRow("identify_guid"))
                'Nilay (27-Dec-2016) -- Start
                mintMinApprovedPriority = -1
                'Nilay (27-Dec-2016) -- End


                'Varsha Rana (12-Sept-2017) -- Start
                'Enhancement - Loan Topup in ESS
                mblnIsfromess = CBool(dRow.Item("isfromess"))
                ' Varsha Rana (12-Sept-2017) -- End


                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: getData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    'Nilay (02-Jul-2016) -- Start
    Public Function getComboOtherOpStatus(ByVal strList As String, ByVal blnAddSelect As Boolean) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If blnAddSelect = True Then
                strQ = "SELECT 0 AS id, @Select AS name UNION "
            End If

            strQ &= "SELECT '" & enOtherOpStatus.Pending & "' AS id, @Pending AS name UNION " & _
                    "SELECT '" & enOtherOpStatus.Approved & "' AS id, @Approved AS name UNION " & _
                    "SELECT '" & enOtherOpStatus.NoOtherOp & "' AS id, @NoOtherOp AS name "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 2, "Approved"))
            objDataOperation.AddParameter("@NoOtherOp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 7, "No Other Operation"))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboOtherOpStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function
    'Nilay (02-Jul-2016) -- End

    Public Function getPendingOpList(ByVal intLoanAdvancetranunkid As Integer, _
                                     ByVal strTableName As String, _
                                     Optional ByVal strFilter As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            Dim strQCondition As String = String.Empty
            Dim strQFinal As String = String.Empty

            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                        "  -1 AS lnotheroptranunkid " & _
                        ", lnloanotherop_approval_tran.loanadvancetranunkid " & _
                        ", lnloanotherop_approval_tran.periodunkid " & _
                        ", cfcommon_period_tran.period_name AS PeriodName " & _
                        ", CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) AS PeriodEndDate" & _
                        ", NULL AS effectivedate " & _
                        ", lnloanotherop_approval_tran.employeeunkid " & _
                        ", -1 AS approvertranunkid " & _
                        ", -1 AS approverempunkid " & _
                        ", NULL AS approvaldate " & _
                        ", '' AS ApproverName " & _
                        ", '' AS Approver " & _
                        ", -1 AS statusunkid " & _
                        ", '' AS [Status] " & _
                        ", -1 AS priority " & _
                        ", 0 AS visibleid " & _
                        ", -1 AS MappedUserId " & _
                        ", 0 AS interest_rate " & _
                        ", 0 AS emi_tenure " & _
                        ", 0 AS emi_amount " & _
                        ", 0 AS loan_duration " & _
                        ", NULL as end_date " & _
                        ", 0 AS principal_amount " & _
                        ", 0 AS interest_amount " & _
                        ", 0 AS topup_amount " & _
                        ", 0 AS exchange_rate " & _
                        ", 0 AS basecurrency_amount " & _
                        ", -1 AS lnoperationtypeid " & _
                        ", '' AS Operation " & _
                        ", CAST(0 AS BIT) AS final_approved " & _
                        ", -1 AS final_status" & _
                        ", '' AS remark " & _
                        ", -1 AS userunkid " & _
                        ", 0 AS isvoid " & _
                        ", -1 AS voiduserunkid " & _
                        ", NULL AS voiddatetime " & _
                        ", '' AS voidreason " & _
                        ", 1 AS isgrp " & _
                        ", '' AS identify_guid " & _
                        ", 0 AS isexternalapprover " & _
                        ", isfromess " & _
                    " FROM lnloanotherop_approval_tran " & _
                        " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanotherop_approval_tran.periodunkid " & _
                    " WHERE     lnloanotherop_approval_tran.loanadvancetranunkid='" & intLoanAdvancetranunkid & "' " & _
                    "       AND lnloanotherop_approval_tran.isvoid=0 AND lnloanotherop_approval_tran.final_approved=0 AND lnloanotherop_approval_tran.final_status=1 "

            'Varsha Rana (12-Sept-2017) -- [isfromess], [AND lnloanotherop_approval_tran.final_status=1] 


            '" LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = lnloanotherop_approval_tran.approverempunkid " & _
            '    " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanotherop_approval_tran.approvertranunkid " & _
            '    " LEFT JOIN lnapproverlevel_master ON	lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
            '    " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _

            strQ &= " UNION " & _
                        " SELECT " & _
                                "  lnloanotherop_approval_tran.lnotheroptranunkid " & _
                                ", lnloanotherop_approval_tran.loanadvancetranunkid " & _
                                ", lnloanotherop_approval_tran.periodunkid " & _
                                ", '' AS PeriodName " & _
                                ", CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) AS PeriodEndDate" & _
                                ", lnloanotherop_approval_tran.effectivedate AS effectivedate " & _
                                ", lnloanotherop_approval_tran.employeeunkid " & _
                                ", lnloanotherop_approval_tran.approvertranunkid " & _
                                ", lnloanotherop_approval_tran.approverempunkid " & _
                                ", lnloanotherop_approval_tran.approvaldate AS approvaldate " & _
                                ", #APPROVER_NAME# AS ApproverName " & _
                                ", #APPROVER_NAME# + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname, '') AS Approver " & _
                                ", lnloanotherop_approval_tran.statusunkid " & _
                                ", CASE WHEN lnloanotherop_approval_tran.statusunkid = " & enLoanApplicationStatus.PENDING & " THEN @Pending " & _
                                "       WHEN lnloanotherop_approval_tran.statusunkid = " & enLoanApplicationStatus.APPROVED & " THEN @Approved " & _
                                "       WHEN lnloanotherop_approval_tran.statusunkid = " & enLoanApplicationStatus.REJECTED & " THEN @Rejected " & _
                                "  END AS [Status] " & _
                                ", lnloanotherop_approval_tran.priority " & _
                                ", lnloanotherop_approval_tran.visibleid " & _
                                ", lnloan_approver_mapping.userunkid AS MappedUserId " & _
                                ", ISNULL(lnloanotherop_approval_tran.interest_rate,0) AS interest_rate " & _
                                ", ISNULL(lnloanotherop_approval_tran.emi_tenure,0) AS emi_tenure " & _
                                ", ISNULL(lnloanotherop_approval_tran.emi_amount,0) AS emi_amount " & _
                                ", ISNULL(lnloanotherop_approval_tran.loan_duration,0) AS loan_duration " & _
                                ", ISNULL(CONVERT(CHAR(8),lnloanotherop_approval_tran.end_date,112),'') AS end_date " & _
                                ", ISNULL(lnloanotherop_approval_tran.principal_amount,0) AS principal_amount " & _
                                ", ISNULL(lnloanotherop_approval_tran.interest_amount,0) AS interest_amount " & _
                                ", ISNULL(lnloanotherop_approval_tran.topup_amount,0) AS topup_amount " & _
                                ", ISNULL(lnloanotherop_approval_tran.exchange_rate,0) AS exchange_rate " & _
                                ", ISNULL(lnloanotherop_approval_tran.basecurrency_amount,0) AS basecurrency_amount " & _
                                ", lnloanotherop_approval_tran.lnoperationtypeid " & _
                                ", CASE WHEN lnloanotherop_approval_tran.lnoperationtypeid = 1 THEN @RChanged " & _
                                " 	    WHEN lnloanotherop_approval_tran.lnoperationtypeid = 2 THEN @IChanged " & _
                                "       WHEN lnloanotherop_approval_tran.lnoperationtypeid = 3 THEN @TChanged " & _
                                "  END AS Operation " & _
                                ", lnloanotherop_approval_tran.final_approved " & _
                                ", lnloanotherop_approval_tran.final_status " & _
                                ", lnloanotherop_approval_tran.remark " & _
                                ", lnloanotherop_approval_tran.userunkid " & _
                                ", lnloanotherop_approval_tran.isvoid " & _
                                ", lnloanotherop_approval_tran.voiduserunkid " & _
                                ", ISNULL(lnloanotherop_approval_tran.voiddatetime,'') AS voiddatetime " & _
                                ", lnloanotherop_approval_tran.voidreason " & _
                                ", 0 AS isgrp " & _
                                ", ISNULL(lnloanotherop_approval_tran.identify_guid,'') AS identify_guid " & _
                                ", lnloanapprover_master.isexternalapprover AS isexternalapprover " & _
                                ", lnloanotherop_approval_tran.isfromess " & _
                        " FROM lnloanotherop_approval_tran " & _
                                " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanotherop_approval_tran.periodunkid " & _
                                " #APPROVER_JOIN# " & _
                                " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanotherop_approval_tran.approvertranunkid " & _
                                " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                                " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                        " WHERE   lnloanotherop_approval_tran.loanadvancetranunkid='" & intLoanAdvancetranunkid & "' " & _
                        "     AND lnloanotherop_approval_tran.isvoid=0 " & _
                        "     AND lnloanotherop_approval_tran.visibleid <> -1 "

            'Varsha Rana (12-Sept-2017) -- [isfromess] 
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'ADD Enum in [Status]
            'Nilay (20-Sept-2016) -- End

            strQFinal = strQ

            strQCondition = " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If strFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & strFilter
            End If

            strQ &= strQCondition

            '" ORDER BY lnloanotherop_approval_tran.loanadvancetranunkid "

            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(Emp.firstname,'')+ ' ' + ISNULL(Emp.othername,'')+ ' ' + ISNULL(Emp.surname,'') ")
            strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = lnloanotherop_approval_tran.approverempunkid   ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 3, "Rejected"))
            objDataOperation.AddParameter("@RChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 4, "Rate Changed"))
            objDataOperation.AddParameter("@IChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 5, "Installment Changed"))
            objDataOperation.AddParameter("@TChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 6, "Topup Added"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows

                strQ = strQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanotherop_approval_tran.approverempunkid ")
                Else
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(Emp.firstname,'')+ ' ' + ISNULL(Emp.othername,'')+ ' ' + ISNULL(Emp.surname,'') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(Emp.firstname,'')+ ' ' + ISNULL(Emp.othername,'')+ ' ' + ISNULL(Emp.surname,'') END ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanotherop_approval_tran.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master AS Emp ON Emp.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")
                strQ = strQ.Replace("#EXT_APPROVER#", "1")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 1, "Pending"))
                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 2, "Approved"))
                objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 3, "Rejected"))
                objDataOperation.AddParameter("@RChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 4, "Rate Changed"))
                objDataOperation.AddParameter("@IChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 5, "Installment Changed"))
                objDataOperation.AddParameter("@TChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 6, "Topup Added"))

                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    Dim dtExtList As DataTable = New DataView(dsExtList.Tables("List"), "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
                    dsExtList.Tables.RemoveAt(0)
                    dsExtList.Tables.Add(dtExtList.Copy)
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)
                End If
            Next

            Dim dtTable As DataTable   'lnoperationtypeid, lnotheroptranunkid
            dtTable = New DataView(dsList.Tables("List"), "", "priority, PeriodEndDate DESC, effectivedate, loanadvancetranunkid ", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: getPendingOpList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function getOtherOpApprovaltranList(ByVal strTableName As String, _
                                               ByVal xEmployeeId As Integer, _
                                               ByVal xLoanOpTypeId As Integer, _
                                               ByVal xIdentifyGuid As String, _
                                               Optional ByVal mstrFilter As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strQFinal As String = String.Empty
        Dim strQCondition As String = String.Empty
        Try
            objDataOperation = New clsDataOperation

            strQ = " SELECT " & _
                                "  lnloanotherop_approval_tran.lnotheroptranunkid " & _
                                ", lnloanotherop_approval_tran.loanadvancetranunkid " & _
                                ", lnloanotherop_approval_tran.periodunkid " & _
                                ", cfcommon_period_tran.period_name AS PeriodName " & _
                                ", lnloanotherop_approval_tran.effectivedate " & _
                                ", lnloanotherop_approval_tran.employeeunkid " & _
                                ", lnloanotherop_approval_tran.approvertranunkid " & _
                                ", lnloanotherop_approval_tran.approverempunkid " & _
                                ", '' AS approvaldate " & _
                                ", #APPROVER_NAME# AS ApproverName " & _
                                ", #APPROVER_NAME# + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname, '') AS Approver " & _
                                ", lnloanotherop_approval_tran.statusunkid " & _
                                ", CASE WHEN lnloanotherop_approval_tran.statusunkid = " & enLoanApplicationStatus.PENDING & " THEN @Pending " & _
                                "       WHEN lnloanotherop_approval_tran.statusunkid = " & enLoanApplicationStatus.APPROVED & " THEN @Approved " & _
                                "       WHEN lnloanotherop_approval_tran.statusunkid = " & enLoanApplicationStatus.REJECTED & " THEN @Rejected " & _
                                "  END AS [Status] " & _
                                ", lnapproverlevel_master.lnlevelunkid " & _
                                ", lnloanotherop_approval_tran.priority " & _
                                ", lnloan_approver_mapping.userunkid AS MappedUserId " & _
                                ", lnloanotherop_approval_tran.lnoperationtypeid " & _
                                ", lnloanotherop_approval_tran.final_approved " & _
                                ", ISNULL(lnloanotherop_approval_tran.interest_rate,0) AS interest_rate " & _
                                ", ISNULL(lnloanotherop_approval_tran.emi_tenure,0) AS emi_tenure " & _
                                ", ISNULL(lnloanotherop_approval_tran.emi_amount,0) AS emi_amount " & _
                                ", ISNULL(lnloanotherop_approval_tran.loan_duration,0) AS loan_duration " & _
                                ", lnloanotherop_approval_tran.end_date AS end_date " & _
                                ", ISNULL(lnloanotherop_approval_tran.principal_amount,0) AS principal_amount " & _
                                ", ISNULL(lnloanotherop_approval_tran.interest_amount,0) AS interest_amount " & _
                                ", ISNULL(lnloanotherop_approval_tran.topup_amount,0) AS topup_amount " & _
                                ", ISNULL(lnloanotherop_approval_tran.exchange_rate,0) AS exchange_rate " & _
                                ", ISNULL(lnloanotherop_approval_tran.basecurrency_amount,0) AS basecurrency_amount " & _
                                ", lnloanotherop_approval_tran.lnoperationtypeid " & _
                                ", CASE WHEN lnloanotherop_approval_tran.lnoperationtypeid = 1 THEN @RChanged " & _
                                " 	    WHEN lnloanotherop_approval_tran.lnoperationtypeid = 2 THEN @IChanged " & _
                                "       WHEN lnloanotherop_approval_tran.lnoperationtypeid = 3 THEN @TChanged " & _
                                "  END AS Operation " & _
                                ", lnloanotherop_approval_tran.userunkid " & _
                                ", ISNULL(lnloanotherop_approval_tran.identify_guid,'') AS identify_guid " & _
                                ", lnloanotherop_approval_tran.isfromess " & _
                        " FROM lnloanotherop_approval_tran " & _
                                " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanotherop_approval_tran.periodunkid " & _
                                " #APPROVER_JOIN# " & _
                                " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanotherop_approval_tran.approvertranunkid " & _
                                " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                                " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                        " WHERE     lnloanotherop_approval_tran.isvoid=0 " & _
                        "       AND lnloanotherop_approval_tran.employeeunkid = '" & xEmployeeId & "' " & _
                        "       AND lnloanotherop_approval_tran.lnoperationtypeid = '" & xLoanOpTypeId & "' " & _
                        "       AND lnloanotherop_approval_tran.identify_guid = '" & xIdentifyGuid & "' " & _
                        "       AND lnloanotherop_approval_tran.final_approved = 0 "
            'Varsha Rana (12-Sept-2017) -- [isfromess] 
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'ADD Enum in [Status]
            'Nilay (20-Sept-2016) -- End

            strQFinal = strQ

            strQCondition = " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter
            End If

            strQ &= strQCondition

            'strQ &= " ORDER BY lnloanotherop_approval_tran.lnotheroptranunkid,lnloanotherop_approval_tran.loanadvancetranunkid, lnloanotherop_approval_tran.effectivedate DESC "

            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(Emp.firstname,'')+ ' ' + ISNULL(Emp.othername,'')+ ' ' + ISNULL(Emp.surname,'') ")
            strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = lnloanotherop_approval_tran.approverempunkid ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 3, "Rejected"))
            objDataOperation.AddParameter("@RChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 4, "Rate Changed"))
            objDataOperation.AddParameter("@IChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 5, "Installment Changed"))
            objDataOperation.AddParameter("@TChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 6, "Topup Added"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = strQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanotherop_approval_tran.approverempunkid ")
                Else
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(Emp.firstname,'')+ ' ' + ISNULL(Emp.othername,'')+ ' ' + ISNULL(Emp.surname,'') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(Emp.firstname,'')+ ' ' + ISNULL(Emp.othername,'')+ ' ' + ISNULL(Emp.surname,'') END ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanotherop_approval_tran.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master AS Emp ON Emp.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")
                strQ = strQ.Replace("#EXT_APPROVER#", "1")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 1, "Pending"))
                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 2, "Approved"))
                objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 3, "Rejected"))
                objDataOperation.AddParameter("@RChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 4, "Rate Changed"))
                objDataOperation.AddParameter("@IChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 5, "Installment Changed"))
                objDataOperation.AddParameter("@TChanged", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 6, "Topup Added"))

                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)
                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "lnotheroptranunkid, loanadvancetranunkid, effectivedate DESC ", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: getOtherOpApprovaltranList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Function insert(ByVal objDataOp As clsDataOperation) As Boolean

        Dim dsList As New DataSet
        Dim strQ As String = String.Empty
        Dim exForce As Exception

        Try
            If objDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = objDataOp
            End If

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanAdvancetranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanOpTypeId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvisibleid.ToString)
            If mdtApprovalDate = Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovalDate.ToString)
            End If
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DateTime.Parse(mdtEffectiveDate & " " & Format(Now, "hh:mm:ss tt")))
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdbInterestRate.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmiTenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmiAmount.ToString)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanDuration.ToString)
            If mdtEndDate = Nothing Then
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
            End If
            objDataOperation.AddParameter("@principal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrincipalAmount.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestAmount.ToString)
            objDataOperation.AddParameter("@topup_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTopupAmount.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestAmount.ToString)
            objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseCurrencyAmount.ToString)
            objDataOperation.AddParameter("@final_approved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFinalApproved.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@final_status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalStatus.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, mstrIdentifyGuid.ToString)

            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            objDataOperation.AddParameter("@isfromess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromess.ToString)
            'Varsha Rana (12-Sept-2017) -- End


            strQ = "INSERT INTO lnloanotherop_approval_tran (" & _
                        "  loanadvancetranunkid " & _
                        ", periodunkid " & _
                        ", lnoperationtypeid " & _
                        ", employeeunkid " & _
                        ", approvertranunkid " & _
                        ", approverempunkid " & _
                        ", statusunkid " & _
                        ", priority " & _
                        ", visibleid " & _
                        ", approvaldate " & _
                        ", effectivedate " & _
                        ", interest_rate " & _
                        ", emi_tenure " & _
                        ", emi_amount " & _
                        ", loan_duration " & _
                        ", end_date " & _
                        ", principal_amount " & _
                        ", interest_amount " & _
                        ", topup_amount " & _
                        ", exchange_rate " & _
                        ", basecurrency_amount " & _
                        ", final_approved " & _
                        ", remark " & _
                        ", final_status " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                        ", identify_guid " & _
                        ", Isfromess " & _
                    ") VALUES (" & _
                        "  @loanadvancetranunkid " & _
                        ", @periodunkid " & _
                        ", @lnoperationtypeid " & _
                        ", @employeeunkid " & _
                        ", @approvertranunkid    " & _
                        ", @approverempunkid " & _
                        ", @statusunkid " & _
                        ", @priority " & _
                        ", @visibleid " & _
                        ", @approvaldate " & _
                        ", @effectivedate " & _
                        ", @interest_rate " & _
                        ", @emi_tenure " & _
                        ", @emi_amount " & _
                        ", @loan_duration " & _
                        ", @end_date " & _
                        ", @principal_amount " & _
                        ", @interest_amount " & _
                        ", @topup_amount " & _
                        ", @exchange_rate " & _
                        ", @basecurrency_amount " & _
                        ", @final_approved " & _
                        ", @remark " & _
                        ", @final_status " & _
                        ", @userunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", @voidreason " & _
                        ", @identify_guid " & _
                        ", @isfromess " & _
                    "); SELECT @@identity "
            'Varsha Rana (12-Sept-2017) -- [isfromess]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintlnOtherOptranunkid = dsList.Tables("List").Rows(0).Item(0)

            If InsertAuditTrailForOtherOpApproval(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "Procedure Name: insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp IsNot Nothing Then objDataOp = Nothing
        End Try
    End Function

    Public Function InsertAuditTrailForOtherOpApproval(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@lnotheroptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnOtherOptranunkid.ToString)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanAdvancetranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanOpTypeId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvisibleid.ToString)
            If mdtApprovalDate = Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovalDate.ToString)
            End If
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DateTime.Parse(mdtEffectiveDate & " " & Format(Now, "hh:mm:ss tt")))
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdbInterestRate.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmiTenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmiAmount.ToString)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanDuration.ToString)
            If mdtEndDate = Nothing Then
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
            End If
            objDataOperation.AddParameter("@principal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrincipalAmount.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestAmount.ToString)
            objDataOperation.AddParameter("@topup_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTopupAmount.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestAmount.ToString)
            objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseCurrencyAmount.ToString)
            objDataOperation.AddParameter("@final_approved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFinalApproved.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@final_status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalStatus.ToString)
            objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, mstrIdentifyGuid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@isfromess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromess.ToString)

            strQ = "INSERT INTO atlnloanotherop_approval_tran (" & _
                        "  lnotheroptranunkid " & _
                        ", loanadvancetranunkid " & _
                        ", periodunkid " & _
                        ", lnoperationtypeid " & _
                        ", employeeunkid " & _
                        ", approvertranunkid " & _
                        ", approverempunkid " & _
                        ", statusunkid " & _
                        ", priority " & _
                        ", visibleid " & _
                        ", approvaldate " & _
                        ", effectivedate " & _
                        ", interest_rate " & _
                        ", emi_tenure " & _
                        ", emi_amount " & _
                        ", loan_duration " & _
                        ", end_date " & _
                        ", principal_amount " & _
                        ", interest_amount " & _
                        ", topup_amount " & _
                        ", exchange_rate " & _
                        ", basecurrency_amount " & _
                        ", final_approved " & _
                        ", remark " & _
                        ", final_status " & _
                        ", identify_guid " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name " & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                        ", isfromess " & _
                    ") VALUES (" & _
                        "  @lnotheroptranunkid " & _
                        ", @loanadvancetranunkid " & _
                        ", @periodunkid " & _
                        ", @lnoperationtypeid " & _
                        ", @employeeunkid " & _
                        ", @approvertranunkid    " & _
                        ", @approverempunkid " & _
                        ", @statusunkid " & _
                        ", @priority " & _
                        ", @visibleid " & _
                        ", @approvaldate " & _
                        ", @effectivedate " & _
                        ", @interest_rate " & _
                        ", @emi_tenure " & _
                        ", @emi_amount " & _
                        ", @loan_duration " & _
                        ", @end_date " & _
                        ", @principal_amount " & _
                        ", @interest_amount " & _
                        ", @topup_amount " & _
                        ", @exchange_rate " & _
                        ", @basecurrency_amount " & _
                        ", @final_approved " & _
                        ", @remark " & _
                        ", @final_status " & _
                        ", @identify_guid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                        ", @isfromess " & _
                    "); SELECT @@identity "
            'Varsha Rana (12-Sept-2017) -- [isfromess]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForOtherOpApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsAllowedAddParameters(ByVal xintLoanAdvtranunkid As Integer, _
                                           ByVal xintParameterId As Integer, _
                                           ByVal xintEmployeeId As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = String.Empty
        Dim exForce As Exception

        Try
            'Nilay (08-Dec-2016) -- Start
            'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
            'objDataOperation = New clsDataOperation
            If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            End If
            'Nilay (08-Dec-2016) -- End

            strQ = "SELECT " & _
                        "  lnotheroptranunkid " & _
                        " ,final_approved " & _
                   "FROM lnloanotherop_approval_tran " & _
                   "WHERE   final_approved = 0 AND (final_status = 2 OR final_status = 1) " & _
                   "    AND isvoid = 0 " & _
                   "    AND loanadvancetranunkid = " & xintLoanAdvtranunkid & _
                   "    AND lnoperationtypeid = " & xintParameterId & _
                   "    AND employeeunkid = " & xintEmployeeId

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: IsAllowedAddParameters; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsAllowEditPendingListParameters(ByVal xintLoanAdvtranunkid As Integer, _
                                                     ByVal xintlnOpTypeId As Integer, _
                                                     ByVal xintEmployeeId As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            'Nilay (08-Dec-2016) -- Start
            'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
            'objDataOperation = New clsDataOperation
            If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            End If
            'Nilay (08-Dec-2016) -- End

            strQ = "SELECT " & _
                        "  lnotheroptranunkid " & _
                        ", loanadvancetranunkid " & _
                        ", lnoperationtypeid " & _
                        ", employeeunkid " & _
                        ", statusunkid " & _
                        ", CASE WHEN statusunkid = " & enLoanApplicationStatus.APPROVED & " THEN @Approved " & _
                        "       WHEN statusunkid = " & enLoanApplicationStatus.REJECTED & " THEN @Rejected " & _
                        "  END AS status " & _
                        ", final_approved " & _
                   "FROM lnloanotherop_approval_tran " & _
                   "WHERE   statusunkid <> 1 AND final_approved = 0 " & _
                   "    AND isvoid = 0 " & _
                   "    AND loanadvancetranunkid = " & xintLoanAdvtranunkid & _
                   "    AND lnoperationtypeid = " & xintlnOpTypeId & _
                   "    AND employeeunkid = " & xintEmployeeId & _
                   "ORDER BY lnotheroptranunkid DESC "
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'ADD Enum in [status]
            'Nilay (20-Sept-2016) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 3, "Rejected"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 11, "Sorry, you cannot perform edit operation. Reason: Selected entry is already")
                mstrMessage &= " " & dsList.Tables("List").Rows(0).Item("status") & " "
                mstrMessage &= Language.getMessage(mstrModuleName, 12, "by approver.")
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: IsAllowEditPendingListParameters; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsLoanParameterApprovalPending(ByVal intPeriodId As Integer, _
                                                   Optional ByVal strEmployeeIds As String = "", _
                                                   Optional ByVal dtEffectiveDate As Date = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            'Nilay (08-Dec-2016) -- Start
            'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
            'objDataOperation = New clsDataOperation
            If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            End If
            'Nilay (08-Dec-2016) -- End

            strQ = " SELECT " & _
                   "    lnotheroptranunkid " & _
                   " FROM lnloanotherop_approval_tran " & _
                   " WHERE  final_approved = 0 " & _
                   "    AND final_status = 1 " & _
                   "    AND isvoid = 0 " & _
                   "    AND periodunkid = @PeriodId "

            If strEmployeeIds.Trim.Length > 0 Then
                strQ &= " AND employeeunkid IN(" & strEmployeeIds & ") "
            End If

            If dtEffectiveDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), lnloanotherop_approval_tran.effectivedate,112) <= @effectivedate "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffectiveDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: IsLoanParameterApprovalPending; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsAllowedLnAdvChangeStatusOrDelete(ByVal intLoanAdvanceTranunkid As Integer, _
                                                       Optional ByVal intEmployeeunkid As Integer = 0) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            'Nilay (08-Dec-2016) -- Start
            'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
            'objDataOperation = New clsDataOperation
            If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            End If
            'Nilay (08-Dec-2016) -- End

            strQ = " SELECT " & _
                   "    lnotheroptranunkid " & _
                   " FROM lnloanotherop_approval_tran " & _
                   " WHERE  final_approved = 0 " & _
                   "    AND final_status = 1 " & _
                   "    AND isvoid = 0 " & _
                   "    AND loanadvancetranunkid = @loanadvancetranunkid "

            If intEmployeeunkid > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceTranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: IsLoanParameterApprovalPending; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsAllowedPendingOpChangeStatus(ByVal strIdentifyGuid As String, _
                                                   ByVal dtEffectiveDate As Date, _
                                                   ByVal intLoanAdvanceTranunkid As Integer, _
                                                   ByVal intEmployeeId As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = String.Empty
        Dim exForce As Exception

        Try
            'Nilay (08-Dec-2016) -- Start
            'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
            'objDataOperation = New clsDataOperation
            If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            End If
            'Nilay (08-Dec-2016) -- End

            strQ = " SELECT " & _
                   "    lnotheroptranunkid " & _
                   " FROM lnloanotherop_approval_tran " & _
                   " WHERE  final_approved = 0 " & _
                   "    AND final_status = 1 " & _
                   "    AND isvoid = 0 " & _
                   "    AND identify_guid <> '" & strIdentifyGuid & "' " & _
                   "    AND CONVERT(CHAR(8), lnloanotherop_approval_tran.effectivedate,112) < @effectivedate " & _
                   "    AND loanadvancetranunkid = " & intLoanAdvanceTranunkid & " " & _
                   "    AND employeeunkid = " & intEmployeeId & " "
            'Nilay (04-Nov-2016) -- Start
            'Enhancements: Global Change Status feature requested by {Rutta}
            '<= @effectivedate RELACED BY < @effectivedate
            'Nilay (04-Nov-2016) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffectiveDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: IsAllowedPendingOpChangeStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function InsertApprovalTran(ByVal xDatabaseName As String, _
                                       ByVal xUserUnkid As Integer, _
                                       ByVal xYearUnkid As Integer, _
                                       ByVal xCompanyUnkid As Integer, _
                                       ByVal xPeriodStart As DateTime, _
                                       ByVal xPeriodEnd As DateTime, _
                                       ByVal xUserModeSetting As String, _
                                       ByVal xOnlyApproved As Boolean, _
                                       ByVal xEmployeeunkid As Integer, _
                                       ByVal xLoanSchemeunkid As Integer, _
                                       ByVal mblnIsLoanApprover_ForLoanScheme As Boolean, _
                                       ByVal dtCurrentDateTime As DateTime) As Boolean
        'Nilay (08-Dec-2016) -- [dtCurrentDateTime]

        If IsAllowedAddParameters(mintLoanAdvancetranunkid, mintLoanOpTypeId, xEmployeeunkid) = False Then
            mstrMessage = Language.getMessage(mstrModuleName, 10, "Sorry, you cannot Add Operation. Reason: Previous same operation is not final approved yet.")
            Return False
        End If

        Select Case mintLoanOpTypeId
            Case enParameterMode.LN_RATE
                If objInterestTran.isExist(mdtEffectiveDate, mintLoanAdvancetranunkid) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, Interest rate is already defined for the selected date.")
                    Return False
                End If
            Case enParameterMode.LN_EMI
                If objEmiTenureTran.isExist(mdtEffectiveDate, mintLoanAdvancetranunkid) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, Installment amount is already defined for the selected date.")
                    Return False
                End If
            Case enParameterMode.LN_TOPUP
                If objTopupTran.isExist(mdtEffectiveDate, mintLoanAdvancetranunkid) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, Topup amount is already defined for the selected date.")
                    Return False
                End If
        End Select

        Dim dtApprover As New DataTable
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        'Nilay (08-Dec-2016) -- Start
        'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
        Dim blnFlag As Boolean
        'Nilay (08-Dec-2016) -- End
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            Dim objLoanApprover As New clsLoanApprover_master

            dtApprover = objLoanApprover.GetEmployeeApprover(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xEmployeeunkid, _
                                                             mblnIsLoanApprover_ForLoanScheme, xLoanSchemeunkid)

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
            Dim blnEnableVisibility As Boolean = False
            Dim intMinPriority As Integer = -1
            Dim intApproverID As Integer = -1
            'Nilay (08-Dec-2016) -- End
            'Nilay (27-Dec-2016) -- Start
            Dim intMaxPriority As Integer
            'Nilay (27-Dec-2016) -- End

            'Nilay (23 Mar 2017) -- Start
            'NOTES: To add Rate, Emi, Topup when final approved and to avoid multiple entries in main tables of (Rate, Emi, Topup) when same level has multiple approver and its going to be final approve
            Dim blnAddRateEmiTopup As Boolean = True
            'Nilay (23 Mar 2017) -- End

            If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                For Each dtRow As DataRow In dtApprover.Rows
                    mintEmployeeunkid = CInt(dtRow("employeeunkid"))
                    mintApprovertranunkid = CInt(dtRow("lnapproverunkid"))
                    mintApproverEmpunkid = CInt(dtRow("approverempunkid"))
                    mintPriority = CInt(dtRow("priority"))

                    intMinPriority = CInt(dtApprover.Compute("MIN(priority)", "1=1")) 'Nilay (08-Dec-2016) -- Declare outside foreach loop
                    If intMinPriority = CInt(dtRow("priority")) Then
                        'Nilay (08-Dec-2016) -- Start
                        'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
                        'mintvisibleid = mintStatusunkid
                        If mintUserunkid = CInt(dtRow.Item("MappedUserID")) Then
                            blnFlag = IsAllowedPendingOpChangeStatus(mstrIdentifyGuid, mdtEffectiveDate, mintLoanAdvancetranunkid, mintEmployeeunkid)
                            If blnFlag = True Then
                                blnEnableVisibility = True
                                mintStatusunkid = enLoanApplicationStatus.APPROVED
                                mdtApprovalDate = dtCurrentDateTime
                                'Nilay (27-Dec-2016) -- Start
                                mintMinApprovedPriority = CInt(dtRow("priority"))
                                'Nilay (27-Dec-2016) -- End
                            Else
                                mintStatusunkid = enLoanApplicationStatus.PENDING
                            End If
                        Else
                            mintStatusunkid = enLoanApplicationStatus.PENDING
                        End If

                        If blnEnableVisibility = True Then
                            mintvisibleid = enLoanApplicationStatus.APPROVED
                        Else
                            'Nilay (27-Dec-2016) -- Start
                            'mintvisibleid = mintStatusunkid
                            Dim dRow As DataRow() = dtApprover.Select("priority=" & intMinPriority & " AND MappedUserID=" & mintUserunkid & "")
                            If dRow.Length > 0 Then
                                mintvisibleid = enLoanApplicationStatus.APPROVED
                                intMaxPriority = CInt(dtApprover.Compute("MAX(priority)", "1=1"))
                                If intMinPriority = intMaxPriority Then
                                    mblnFinalApproved = True
                                    mintFinalStatus = enLoanApplicationStatus.APPROVED
                                End If
                            Else
                        mintvisibleid = mintStatusunkid
                        End If
                            'Nilay (27-Dec-2016) -- End
                        End If
                        'Nilay (08-Dec-2016) -- End
                    Else
                        'Nilay (08-Dec-2016) -- Start
                        'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
                        'mintvisibleid = -1
                        If blnEnableVisibility = True Then
                            Dim intNextMinPriority As Integer = CInt(dtApprover.Compute("MIN(priority)", "priority > " & intMinPriority))
                            If intNextMinPriority = CInt(dtRow("priority")) Then
                                mintvisibleid = enLoanApplicationStatus.PENDING
                                mintStatusunkid = enLoanApplicationStatus.PENDING
                    Else
                        mintvisibleid = -1
                    End If
                        Else
                            mintvisibleid = -1
                        End If
                        'Nilay (08-Dec-2016) -- End
                    End If

                    'Nilay (08-Dec-2016) -- Start
                    'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
                    intMaxPriority = CInt(dtApprover.Compute("MAX(priority)", "1=1"))
                    If blnEnableVisibility = True Then
                        If intMinPriority = intMaxPriority Then
                            mintStatusunkid = enLoanApplicationStatus.APPROVED
                            mblnFinalApproved = True
                            mintFinalStatus = enLoanApplicationStatus.APPROVED
                            mdtApprovalDate = dtCurrentDateTime
                        End If
                    End If
                    'Nilay (08-Dec-2016) -- End

                    If insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Nilay (08-Dec-2016) -- Start
                    'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
                    mdtApprovalDate = Nothing

                    If blnEnableVisibility = True Then
                        If intMinPriority = intMaxPriority Then

                            'Nilay (23 Mar 2017) -- Start
                            If blnAddRateEmiTopup = True Then
                            blnFlag = InsertInterestEmiTopup(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, _
                                                             xOnlyApproved, mintLoanOpTypeId, objDataOperation)
                            If blnFlag = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                                Else
                                    blnAddRateEmiTopup = False
                            End If
                        End If
                            'Nilay (23 Mar 2017) -- End

                        End If
                    End If
                    'Nilay (08-Dec-2016) -- End
                Next

                dtApprover.Rows.Clear()
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtApprover = Nothing
            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "Procedure Name: InsertApprovalTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dtApprover IsNot Nothing Then dtApprover.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdatePendingOpChangeStatus(ByVal xDatabaseName As String, _
                                                ByVal xUserUnkid As Integer, _
                                                ByVal xYearUnkid As Integer, _
                                                ByVal xCompanyUnkid As Integer, _
                                                ByVal xPeriodStart As DateTime, _
                                                ByVal xPeriodEnd As DateTime, _
                                                ByVal xUserModeSetting As String, _
                                                ByVal xOnlyApproved As Boolean, _
                                                ByVal xLoanSchemeunkid As Integer, _
                                                ByVal mblnIsLoanApprover_ForLoanScheme As Boolean, _
                                                Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean

        Dim dsList As New DataSet
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Dim blnFlag As Boolean

        Select Case mintLoanOpTypeId
            Case enParameterMode.LN_RATE
                If objInterestTran.isExist(mdtEffectiveDate, mintLoanAdvancetranunkid) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, Interest rate is already defined for the selected date.")
                    Return False
                End If
            Case enParameterMode.LN_EMI
                If objEmiTenureTran.isExist(mdtEffectiveDate, mintLoanAdvancetranunkid) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, Installment amount is already defined for the selected date.")
                    Return False
                End If
            Case enParameterMode.LN_TOPUP
                If objTopupTran.isExist(mdtEffectiveDate, mintLoanAdvancetranunkid) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, Topup amount is already defined for the selected date.")
                    Return False
                End If
        End Select

        Try
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'If mintStatusunkid = 1 Then Return True
            If mintStatusunkid = enLoanApplicationStatus.PENDING Then Return True
            'Nilay (20-Sept-2016) -- End
            If objDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = objDataOp
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lnotheroptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnOtherOptranunkid.ToString)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanAdvancetranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanOpTypeId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvisibleid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovalDate <> Nothing, mdtApprovalDate.ToString, DBNull.Value))
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovalDate <> Nothing, mdtEffectiveDate.ToString, DBNull.Value))
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdbInterestRate.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmiTenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmiAmount.ToString)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanDuration.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEndDate <> Nothing, mdtEndDate.ToString, DBNull.Value))
            objDataOperation.AddParameter("@principal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrincipalAmount.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestAmount.ToString)
            objDataOperation.AddParameter("@topup_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTopupAmount.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestAmount.ToString)
            objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseCurrencyAmount.ToString)
            objDataOperation.AddParameter("@final_approved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFinalApproved.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@final_status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalStatus.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentifyGuid.ToString)

            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            objDataOperation.AddParameter("@isfromess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromess.ToString)
            ' Varsha Rana (12-Sept-2017) -- End

            strQ = "UPDATE lnloanotherop_approval_tran SET " & _
                        "  loanadvancetranunkid = @loanadvancetranunkid " & _
                        ", periodunkid = @periodunkid " & _
                        ", lnoperationtypeid = @lnoperationtypeid " & _
                        ", employeeunkid = @employeeunkid " & _
                        ", approvertranunkid = @approvertranunkid " & _
                        ", approverempunkid = @approverempunkid " & _
                        ", priority = @priority " & _
                        ", visibleid = @visibleid " & _
                        ", statusunkid = @statusunkid " & _
                        ", approvaldate = @approvaldate " & _
                        ", effectivedate = @effectivedate " & _
                        ", interest_rate = @interest_rate " & _
                        ", emi_tenure = @emi_tenure " & _
                        ", emi_amount = @emi_amount " & _
                        ", loan_duration = @loan_duration " & _
                        ", end_date = @end_date " & _
                        ", principal_amount = @principal_amount " & _
                        ", interest_amount = @interest_amount " & _
                        ", topup_amount = @topup_amount " & _
                        ", exchange_rate = @exchange_rate " & _
                        ", basecurrency_amount = @basecurrency_amount " & _
                        ", final_approved = @final_approved " & _
                        ", remark = @remark " & _
                        ", final_status = @final_status " & _
                        ", userunkid = @userunkid " & _
                        ", isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                        ", identify_guid = @identify_guid " & _
                        ", isfromess = @isfromess " & _
                   "WHERE lnotheroptranunkid = @lnotheroptranunkid "

            'Varsha Rana (12-Sept-2017) -- [isfromess]


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If InsertAuditTrailForOtherOpApproval(objDataOperation, 2) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            strQ = "SELECT " & _
                        "  lnotheroptranunkid " & _
                        ", loanadvancetranunkid " & _
                        ", lnoperationtypeid " & _
                        ", employeeunkid " & _
                        ", approvertranunkid " & _
                        ", approverempunkid " & _
                        ", priority " & _
                        ", visibleid " & _
                        ", identify_guid " & _
                   "FROM lnloanotherop_approval_tran " & _
                   "WHERE isvoid = 0 " & _
                        " AND loanadvancetranunkid = @loanadvancetranunkid " & _
                        " AND lnoperationtypeid = @lnoperationtypeid " & _
                        " AND employeeunkid = @employeeunkid " & _
                        " AND identify_guid = @identify_guid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanAdvancetranunkid.ToString)
            objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanOpTypeId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, mstrIdentifyGuid.ToString)

            Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsApprover IsNot Nothing AndAlso dsApprover.Tables("List").Rows.Count > 0 Then

                strQ = "UPDATE lnloanotherop_approval_tran set " & _
                            "  visibleid = '" & mintStatusunkid & "' " & _
                       "WHERE isvoid = 0 AND final_approved = 0 " & _
                            " AND loanadvancetranunkid = @loanadvancetranunkid " & _
                            " AND lnoperationtypeid = @lnoperationtypeid " & _
                            " AND employeeunkid = @employeeunkid " & _
                            " AND approvertranunkid = @approvertranunkid " & _
                            " AND approverempunkid = @approverempunkid " & _
                            " AND identify_guid = @identify_guid "

                Dim dtVisibility As DataTable = New DataView(dsApprover.Tables("List"), "priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

                For i As Integer = 0 To dtVisibility.Rows.Count - 1
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("loanadvancetranunkid").ToString)
                    objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("lnoperationtypeid").ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("employeeunkid").ToString)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                    objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approverempunkid").ToString)
                    objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, dtVisibility.Rows(i)("identify_guid").ToString)

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables("List").Compute("Min(priority)", "priority > " & mintPriority)), _
                                                    -1, dsApprover.Tables("List").Compute("Min(priority)", "priority > " & mintPriority))

                dtVisibility = New DataView(dsApprover.Tables("List"), "priority = " & intMinPriority & " AND priority <> -1 ", "", DataViewRowState.CurrentRows).ToTable

                If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'If mintStatusunkid = 2 Then 'Approved
                    If mintStatusunkid = enLoanApplicationStatus.APPROVED Then
                        'Nilay (20-Sept-2016) -- End
                        strQ = "UPDATE lnloanotherop_approval_tran set " & _
                                    "  visibleid = 1 " & _
                               "WHERE isvoid = 0 AND final_approved = 0 " & _
                                    " AND loanadvancetranunkid = @loanadvancetranunkid " & _
                                    " AND lnoperationtypeid = @lnoperationtypeid " & _
                                    " AND employeeunkid = @employeeunkid " & _
                                    " AND approvertranunkid = @approvertranunkid " & _
                                    " AND approverempunkid = @approverempunkid " & _
                                    " AND identify_guid = @identify_guid "

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'ElseIf mintStatusunkid = 3 Then 'Rejected
                    ElseIf mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                        'Nilay (20-Sept-2016) -- End
                        strQ = "UPDATE lnloanotherop_approval_tran set " & _
                                   "  visibleid = -1 " & _
                              "WHERE isvoid = 0 AND final_approved = 0 " & _
                                   " AND loanadvancetranunkid = @loanadvancetranunkid " & _
                                   " AND lnoperationtypeid = @lnoperationtypeid " & _
                                   " AND employeeunkid = @employeeunkid " & _
                                   " AND approvertranunkid = @approvertranunkid " & _
                                   " AND approverempunkid = @approverempunkid " & _
                                   " AND identify_guid = @identify_guid "
                    End If

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("loanadvancetranunkid").ToString)
                        objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("lnoperationtypeid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("employeeunkid").ToString)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                        objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approverempunkid").ToString)
                        objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, dtVisibility.Rows(i)("identify_guid").ToString)

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next

                End If

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If mintStatusunkid <> 3 Then
                'If mintStatusunkid = 2 Then 'Approved
                If mintStatusunkid <> enLoanApplicationStatus.REJECTED Then
                    If mintStatusunkid = enLoanApplicationStatus.APPROVED Then
                        'Nilay (20-Sept-2016) -- End
                        If mintLoanOpTypeId = enParameterMode.LN_RATE Then
                            'Nilay (13-Sept-2016) -- Start
                            'Enhancement : Enable Default Parameter Edit & other fixes
                            objInterestTran._DataOperation = objDataOperation
                            'Nilay (13-Sept-2016) -- End
                            Dim strmsg As String = objInterestTran.IsValidInterestRate(mintLoanAdvancetranunkid, mdtEffectiveDate, CDec(mdbInterestRate))
                            If strmsg <> "" Then
                                mstrMessage = strmsg
                                'Nilay (13-Sept-2016) -- Start
                                'Enhancement : Enable Default Parameter Edit & other fixes
                                objDataOperation.ReleaseTransaction(False)
                                'Nilay (13-Sept-2016) -- End
                                Return False
                            End If
                        End If

                        strQ = "UPDATE lnloanotherop_approval_tran SET " & _
                                    "  periodunkid = @periodunkid " & _
                                    ", effectivedate = @effectivedate " & _
                                    ", interest_rate = @interest_rate " & _
                                    ", emi_tenure = @emi_tenure " & _
                                    ", emi_amount = @emi_amount " & _
                                    ", loan_duration = @loan_duration " & _
                                    ", end_date = @end_date " & _
                                    ", principal_amount = @principal_amount " & _
                                    ", interest_amount = @interest_amount " & _
                                    ", topup_amount = @topup_amount " & _
                                    ", exchange_rate = @exchange_rate " & _
                                    ", basecurrency_amount = @basecurrency_amount " & _
                                "WHERE isvoid = 0 AND final_approved = 0 " & _
                                "   AND loanadvancetranunkid = @loanadvancetranunkid " & _
                                "   AND lnoperationtypeid = @lnoperationtypeid " & _
                                "   AND employeeunkid = @employeeunkid " & _
                                "   AND priority >= @priority " & _
                                "   AND identify_guid = @identify_guid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                        objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectiveDate.ToString)
                        objDataOperation.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdbInterestRate.ToString)
                        objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmiTenure.ToString)
                        objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmiAmount.ToString)
                        objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanDuration.ToString)
                        If mdtEndDate = Nothing Then
                            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
                        End If
                        objDataOperation.AddParameter("@principal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrincipalAmount.ToString)
                        objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestAmount.ToString)
                        objDataOperation.AddParameter("@topup_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTopupAmount.ToString)
                        objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestAmount.ToString)
                        objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseCurrencyAmount.ToString)
                        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanAdvancetranunkid.ToString)
                        objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanOpTypeId.ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                        objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
                        objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, mstrIdentifyGuid.ToString)

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                    Dim dtFinalApprover As DataTable = New DataView(dsApprover.Tables("List"), "priority > " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

                    If dtFinalApprover IsNot Nothing AndAlso dtFinalApprover.Rows.Count <= 0 Then

                        strQ = "UPDATE lnloanotherop_approval_tran set " & _
                                    "  final_approved = 1 " & _
                                    " ,final_status = " & mintStatusunkid & _
                                "WHERE    loanadvancetranunkid = @loanadvancetranunkid " & _
                                    " AND lnoperationtypeid = @lnoperationtypeid " & _
                                    " AND employeeunkid = @employeeunkid " & _
                                    " AND identify_guid = @identify_guid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanAdvancetranunkid.ToString)
                        objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanOpTypeId.ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                        objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, mstrIdentifyGuid.ToString)

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'If InsertAuditTrailForOtherOpApproval(objDataOperation, 2) = False Then
                        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '    Throw exForce
                        'End If

                        dtFinalApprover = Nothing

                        'Nilay (08-Dec-2016) -- Start
                        'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
                        'Select Case mintLoanOpTypeId
                        '    Case enParameterMode.LN_RATE
                        '        objInterestTran._Loanadvancetranunkid = mintLoanAdvancetranunkid
                        '        objInterestTran._Periodunkid = mintPeriodunkid
                        '        objInterestTran._Effectivedate = mdtEffectiveDate
                        '        objInterestTran._Interest_Rate = mdbInterestRate
                        '        objInterestTran._Userunkid = mintUserunkid
                        '        objInterestTran._IsDefault = False
                        '        objInterestTran._Approverunkid = mintApprovertranunkid
                        '        'Nilay (20-Sept-2016) -- Start
                        '        'Enhancement : Cancel feature for approved but not assigned loan application
                        '        objInterestTran._Identify_Guid = mstrIdentifyGuid
                        '        'Nilay (20-Sept-2016) -- End
                        '        objInterestTran._DataOperation = objDataOperation
                        '        'Nilay (05-May-2016) -- Start
                        '        objInterestTran._WebIP = mstrWebClientIP
                        '        objInterestTran._WebFormName = mstrWebFormName
                        '        objInterestTran._WebHost = mstrWebHostName
                        '        'Nilay (05-May-2016) -- End

                        '        blnFlag = objInterestTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, _
                        '                                         xOnlyApproved, True)
                        '        If blnFlag = False Then
                        '            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        '            exForce = New Exception(objInterestTran._Message)
                        '            Throw exForce
                        '        End If

                        '    Case enParameterMode.LN_EMI

                        '        objEmiTenureTran._Loanadvancetranunkid = mintLoanAdvancetranunkid
                        '        objEmiTenureTran._Periodunkid = mintPeriodunkid
                        '        objEmiTenureTran._Effectivedate = mdtEffectiveDate
                        '        objEmiTenureTran._Emi_Tenure = mintEmiTenure
                        '        objEmiTenureTran._Emi_Amount = mdecEmiAmount
                        '        objEmiTenureTran._Loan_Duration = mintLoanDuration
                        '        objEmiTenureTran._Userunkid = mintUserunkid
                        '        objEmiTenureTran._End_Date = mdtEndDate
                        '        objEmiTenureTran._Exchange_rate = mdecExchangeRate
                        '        objEmiTenureTran._Basecurrency_amount = mdecBaseCurrencyAmount
                        '        objEmiTenureTran._Principal_amount = mdecPrincipalAmount
                        '        objEmiTenureTran._Interest_amount = mdecInterestAmount
                        '        objEmiTenureTran._IsDefault = False
                        '        objEmiTenureTran._Approverunkid = mintApprovertranunkid
                        '        'Nilay (20-Sept-2016) -- Start
                        '        'Enhancement : Cancel feature for approved but not assigned loan application
                        '        objEmiTenureTran._Identify_Guid = mstrIdentifyGuid
                        '        'Nilay (20-Sept-2016) -- End
                        '        objEmiTenureTran._DataOperation = objDataOperation
                        '        'Nilay (05-May-2016) -- Start
                        '        objEmiTenureTran._WebIP = mstrWebClientIP
                        '        objEmiTenureTran._WebFormName = mstrWebFormName
                        '        objEmiTenureTran._WebHost = mstrWebHostName
                        '        'Nilay (05-May-2016) -- End

                        '        blnFlag = objEmiTenureTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, _
                        '                                          xOnlyApproved, True)
                        '        If blnFlag = False Then
                        '            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        '            exForce = New Exception(objEmiTenureTran._Message)
                        '            Throw exForce
                        '        End If

                        '    Case enParameterMode.LN_TOPUP
                        '        objTopupTran._Loanadvancetranunkid = mintLoanAdvancetranunkid
                        '        objTopupTran._Periodunkid = mintPeriodunkid
                        '        objTopupTran._Effectivedate = mdtEffectiveDate
                        '        objTopupTran._Topup_Amount = mdecTopupAmount
                        '        objTopupTran._Userunkid = mintUserunkid
                        '        objTopupTran._Exchange_rate = mdecExchangeRate
                        '        objTopupTran._Basecurrency_amount = mdecBaseCurrencyAmount
                        '        objTopupTran._IsDefault = False
                        '        objTopupTran._Approverunkid = mintApprovertranunkid
                        '        'Nilay (20-Sept-2016) -- Start
                        '        'Enhancement : Cancel feature for approved but not assigned loan application
                        '        objTopupTran._Identify_Guid = mstrIdentifyGuid
                        '        'Nilay (20-Sept-2016) -- End
                        '        objTopupTran._DataOperation = objDataOperation
                        '        'Nilay (05-May-2016) -- Start
                        '        objTopupTran._WebIP = mstrWebClientIP
                        '        objTopupTran._WebFormName = mstrWebFormName
                        '        objTopupTran._WebHost = mstrWebHostName
                        '        'Nilay (05-May-2016) -- End

                        '        blnFlag = objTopupTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, _
                        '                                      xOnlyApproved, True)
                        '        If blnFlag = False Then
                        '            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        '            exForce = New Exception(objTopupTran._Message)
                        '            Throw exForce
                        '        End If
                        'End Select
                        'Sohail (01 Nov 2017) -- Start
                        'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                        'Transaction was not getting inserted in loan_balance table as xCompanyUnkid was passed in xUserUnkid parameter.
                        'blnFlag = InsertInterestEmiTopup(xDatabaseName, xCompanyUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
                        '                                 xUserModeSetting, xOnlyApproved, mintLoanOpTypeId, objDataOperation)
                        blnFlag = InsertInterestEmiTopup(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
                                                         xUserModeSetting, xOnlyApproved, mintLoanOpTypeId, objDataOperation)
                        'Sohail (01 Nov 2017) -- End
                                If blnFlag = False Then
                                    If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        'Nilay (08-Dec-2016) -- End
                    Else
                        'strQ = "UPDATE lnloanotherop_approval_tran set " & _
                        '            " final_status = " & mintStatusunkid & _
                        '        "WHERE    final_approved = 0 " & _
                        '            " AND loanadvancetranunkid = @loanadvancetranunkid " & _
                        '            " AND lnoperationtypeid = @lnoperationtypeid " & _
                        '            " AND employeeunkid = @employeeunkid " & _
                        '            " AND identify_guid = @identify_guid "

                        'objDataOperation.ClearParameters()
                        'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanAdvancetranunkid.ToString)
                        'objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanOpTypeId.ToString)
                        'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                        'objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, mstrIdentifyGuid.ToString)

                        'objDataOperation.ExecNonQuery(strQ)

                        'If objDataOperation.ErrorMessage <> "" Then
                        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '    Throw exForce
                        'End If
                    End If

                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'ElseIf mintStatusunkid = 3 Then
                ElseIf mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                    'Nilay (20-Sept-2016) -- End
                    strQ = "UPDATE lnloanotherop_approval_tran set " & _
                                    " final_status = " & mintStatusunkid & _
                                "WHERE    final_approved = 0 " & _
                                    " AND loanadvancetranunkid = @loanadvancetranunkid " & _
                                    " AND lnoperationtypeid = @lnoperationtypeid " & _
                                    " AND employeeunkid = @employeeunkid " & _
                                    " AND identify_guid = @identify_guid "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanAdvancetranunkid.ToString)
                    objDataOperation.AddParameter("@lnoperationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanOpTypeId.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, mstrIdentifyGuid.Length, mstrIdentifyGuid.ToString)

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If InsertAuditTrailForOtherOpApproval(objDataOperation, 2) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            End If

            strQ = " SELECT " & _
                   "   lnotheroptranunkid, final_approved, final_status " & _
                   " FROM lnloanotherop_approval_tran " & _
                   " WHERE lnotheroptranunkid = @lnotheroptranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lnotheroptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnOtherOptranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow In dsList.Tables("List").Rows
                mintlnOtherOptranunkid = CInt(dRow("lnotheroptranunkid"))
                Call getData(objDataOperation)
                If InsertAuditTrailForOtherOpApproval(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePendingOpChangeStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp IsNot Nothing Then objDataOp = Nothing
        End Try
    End Function

    'Nilay (08-Dec-2016) -- Start
    'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
    Public Function InsertInterestEmiTopup(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal intOperationTypeID As Integer, _
                                           ByVal objDataOp As clsDataOperation) As Boolean
        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If

        Dim exForce As Exception
        Dim blnFlag As Boolean

        Try
            Select Case intOperationTypeID
                Case enParameterMode.LN_RATE
                    objInterestTran._Loanadvancetranunkid = mintLoanAdvancetranunkid
                    objInterestTran._Periodunkid = mintPeriodunkid
                    objInterestTran._Effectivedate = mdtEffectiveDate
                    objInterestTran._Interest_Rate = mdbInterestRate
                    objInterestTran._Userunkid = mintUserunkid
                    objInterestTran._IsDefault = False
                    objInterestTran._Approverunkid = mintApprovertranunkid
                    objInterestTran._Identify_Guid = mstrIdentifyGuid
                    objInterestTran._DataOperation = objDataOperation
                    'objInterestTran._WebIP = mstrWebClientIP
                    'objInterestTran._WebFormName = mstrWebFormName
                    'objInterestTran._WebHost = mstrWebHostName

                    objInterestTran._ClientIP = mstrClientIP
                    objInterestTran._FormName = mstrFormName
                    objInterestTran._HostName = mstrHostName
                    'Hemant (19 Mar 2019) -- Start
                    'ENHANCEMENT : Add Missing Column fields of AT Tables & Show its on "Audit & Trails" Module.
                    'objInterestTran._AuditDate = mstrFormName
                    objInterestTran._AuditDate = mdtAuditDate
                    'Hemant (19 Mar 2019) -- End
                    objInterestTran._AuditUserId = mintAuditUserId
objInterestTran._CompanyUnkid = mintCompanyUnkid
                    objInterestTran._FromWeb = mblnIsWeb
                    objInterestTran._LoginEmployeeunkid = mintLoginEmployeeunkid

                    blnFlag = objInterestTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, _
                                                     xOnlyApproved, True)
                    If blnFlag = False Then
                        If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objInterestTran._Message)
                        Throw exForce
                    End If

                Case enParameterMode.LN_EMI

                    objEmiTenureTran._Loanadvancetranunkid = mintLoanAdvancetranunkid
                    objEmiTenureTran._Periodunkid = mintPeriodunkid
                    objEmiTenureTran._Effectivedate = mdtEffectiveDate
                    objEmiTenureTran._Emi_Tenure = mintEmiTenure
                    objEmiTenureTran._Emi_Amount = mdecEmiAmount
                    objEmiTenureTran._Loan_Duration = mintLoanDuration
                    objEmiTenureTran._Userunkid = mintUserunkid
                    objEmiTenureTran._End_Date = mdtEndDate
                    objEmiTenureTran._Exchange_rate = mdecExchangeRate
                    objEmiTenureTran._Basecurrency_amount = mdecBaseCurrencyAmount
                    objEmiTenureTran._Principal_amount = mdecPrincipalAmount
                    objEmiTenureTran._Interest_amount = mdecInterestAmount
                    objEmiTenureTran._IsDefault = False
                    objEmiTenureTran._Approverunkid = mintApprovertranunkid
                    objEmiTenureTran._Identify_Guid = mstrIdentifyGuid
                    objEmiTenureTran._DataOperation = objDataOperation
                    'objEmiTenureTran._WebIP = mstrWebClientIP
                    'objEmiTenureTran._WebFormName = mstrWebFormName
                    'objEmiTenureTran._WebHost = mstrWebHostName

                    objEmiTenureTran._ClientIP = mstrClientIP
                    objEmiTenureTran._FormName = mstrFormName
                    objEmiTenureTran._HostName = mstrHostName
                    'Hemant (19 Mar 2019) -- Start
                    'ENHANCEMENT : Add Missing Column fields of AT Tables & Show its on "Audit & Trails" Module.
                    'objEmiTenureTran._AuditDate = mstrFormName
                    objEmiTenureTran._AuditDate = mdtAuditDate
                    'Hemant (19 Mar 2019) -- End
                    objEmiTenureTran._AuditUserId = mintAuditUserId
objEmiTenureTran._CompanyUnkid = mintCompanyUnkid
                    objEmiTenureTran._FromWeb = mblnIsWeb
                    objEmiTenureTran._LoginEmployeeunkid = mintLoginEmployeeunkid

                    blnFlag = objEmiTenureTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, _
                                                      xOnlyApproved, True)
                    If blnFlag = False Then
                        If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objEmiTenureTran._Message)
                        Throw exForce
                    End If

                Case enParameterMode.LN_TOPUP

                    objTopupTran._Loanadvancetranunkid = mintLoanAdvancetranunkid
                    objTopupTran._Periodunkid = mintPeriodunkid
                    objTopupTran._Effectivedate = mdtEffectiveDate
                    objTopupTran._Topup_Amount = mdecTopupAmount
                    objTopupTran._Userunkid = mintUserunkid
                    objTopupTran._Exchange_rate = mdecExchangeRate
                    objTopupTran._Basecurrency_amount = mdecBaseCurrencyAmount
                    objTopupTran._IsDefault = False
                    objTopupTran._Approverunkid = mintApprovertranunkid
                    objTopupTran._Identify_Guid = mstrIdentifyGuid
                    objTopupTran._DataOperation = objDataOperation
                    'objTopupTran._WebIP = mstrWebClientIP
                    'objTopupTran._WebFormName = mstrWebFormName
                    'objTopupTran._WebHost = mstrWebHostName

                    objTopupTran._ClientIP = mstrClientIP
                    objTopupTran._FormName = mstrFormName
                    objTopupTran._HostName = mstrHostName
                    'Hemant (19 Mar 2019) -- Start
                    'ENHANCEMENT : Add Missing Column fields of AT Tables & Show its on "Audit & Trails" Module.
                    'objTopupTran._AuditDate = mstrFormName
                    objTopupTran._AuditDate = mdtAuditDate
                    'Hemant (19 Mar 2019) -- End
                    objTopupTran._AuditUserId = mintAuditUserId
objTopupTran._CompanyUnkid = mintCompanyUnkid
                    objTopupTran._FromWeb = mblnIsWeb
                    objTopupTran._Loginemployeeunkid = mintLoginEmployeeunkid


                    'If mblnIsfromess = True Then
                    '    objTopupTran._Loginemployeeunkid = mintEmployeeunkid
                    'Else
                    '    objTopupTran._Loginemployeeunkid = -1
                    'End If






                    blnFlag = objTopupTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, _
                                                  xOnlyApproved, True)
                    If blnFlag = False Then
                        If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objTopupTran._Message)
                        Throw exForce
                    End If

            End Select

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertInterestEmiTopup; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If objDataOp IsNot Nothing Then objDataOp = Nothing
        End Try
        Return True
    End Function
    'Nilay (08-Dec-2016) -- End

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Public Function Delete(ByVal strIdentifyGuid As String, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean

        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If objDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = objDataOp
            End If

            strQ = "UPDATE lnloanotherop_approval_tran SET " & _
                        "  isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voidreason = @voidreason " & _
                   "WHERE identify_guid = @identify_guid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIdentifyGuid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If objDataOp IsNot Nothing Then objDataOp = Nothing
        End Try
    End Function
    'Nilay (13-Sept-2016) -- End

    'Pinkal (26-Sep-2017) -- Start
    'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetApproverPendingOtherLoanOperation(ByVal intApproverID As Integer, ByVal mstrEmpID As String, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrFormID As String = String.Empty
        Try
            Dim objDataOperation As clsDataOperation = Nothing

            If mobjDataOperation IsNot Nothing Then
                objDataOperation = mobjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()

            strQ = " SELECT STUFF( " & _
                      " (SELECT  ',' +  CAST(lnloanotherop_approval_tran.loanadvancetranunkid AS NVARCHAR(max)) " & _
                      "   FROM lnloanotherop_approval_tran " & _
                      " WHERE lnloanotherop_approval_tran.final_approved = 0 AND lnloanotherop_approval_tran.employeeunkid IN (" & mstrEmpID & ")  " & _
                      " AND lnloanotherop_approval_tran.isvoid = 0  AND lnloanotherop_approval_tran.isvoid = 0 AND lnloanotherop_approval_tran.approvertranunkid  = " & intApproverID & _
                      " ORDER BY lnloanotherop_approval_tran.loanadvancetranunkid  FOR XML PATH('')),1,1,'') AS CSV"

            dsList = objDataOperation.ExecQuery(strQ, "FormList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrFormID = dsList.Tables(0).Rows(0)("CSV").ToString()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverPendingOtherLoanOperation; Module Name: " & mstrModuleName)
        End Try
        Return mstrFormID
    End Function

    'Pinkal (26-Sep-2017) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsloanotherop_approval_tran", 1, "Pending")
            Language.setMessage("clsloanotherop_approval_tran", 2, "Approved")
            Language.setMessage("clsloanotherop_approval_tran", 3, "Rejected")
            Language.setMessage("clsloanotherop_approval_tran", 4, "Rate Changed")
            Language.setMessage("clsloanotherop_approval_tran", 5, "Installment Changed")
            Language.setMessage("clsloanotherop_approval_tran", 6, "Topup Added")
            Language.setMessage("clsloanotherop_approval_tran", 7, "No Other Operation")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 7, "Sorry, Interest rate is already defined for the selected date.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Installment amount is already defined for the selected date.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Topup amount is already defined for the selected date.")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot Add Operation. Reason: Previous same operation is not final approved yet.")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot perform edit operation. Reason: Selected entry is already")
            Language.setMessage(mstrModuleName, 12, "by approver.")
            Language.setMessage(mstrModuleName, 13, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
