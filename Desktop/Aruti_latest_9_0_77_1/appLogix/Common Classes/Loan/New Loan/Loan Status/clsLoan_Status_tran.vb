﻿'************************************************************************************************************************************
'Class Name : clsLoan_Status_tran.vb
'Purpose    :
'Date       :09/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm 'Nilay (01-Apr-2016)

'' <summary>
'' Purpose: 
'' Developer: Sandeep J. Sharma
'' </summary>
'Public Class clsLoan_Status_tran1
'    Private Const mstrModuleName = "clsLoan_Status_tran1"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintLoanstatustranunkid As Integer
'    Private mintLoanadvancetranunkid As Integer
'    Private mdtStaus_Date As Date
'    Private mintStatusunkid As Integer

'    'Anjan (11 May 2011)-Start
'    'Private mdblSettle_Amount As Double
'    Private mdecSettle_Amount As Decimal
'    'Anjan (11 May 2011)-End 


'    Private mstrRemark As String = String.Empty
'    Private mblnIsvoid As Boolean
'    Private mdtVoiddatetime As Date
'    Private mintVoiduserunkid As Integer
'    Private mstrVoidreason As String = String.Empty
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loanstatustranunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loanstatustranunkid() As Integer
'        Get
'            Return mintLoanstatustranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanstatustranunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loanadvancetranunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loanadvancetranunkid() As Integer
'        Get
'            Return mintLoanadvancetranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanadvancetranunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set status_date
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Staus_Date() As Date
'        Get
'            Return mdtStaus_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtStaus_Date = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set statusunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Statusunkid() As Integer
'        Get
'            Return mintStatusunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintStatusunkid = value
'        End Set
'    End Property

'    '''' <summary>
'    '''' Purpose: Get or Set settle_amount
'    '''' Modify By: Sandeep J. Sharma
'    '''' </summary>


'    'Anjan (11 May 2011)-Start
'    'Public Property _Settle_Amount() As Double
'    Public Property _Settle_Amount() As Decimal
'        'Anjan (11 May 2011)-End 
'        Get
'            Return mdecSettle_Amount
'        End Get

'        'Anjan (11 May 2011)-Start
'        'Set(ByVal value As Double)
'        Set(ByVal value As Decimal)
'            'Anjan (11 May 2011)-End 
'            mdecSettle_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set remark
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Remark() As String
'        Get
'            Return mstrRemark
'        End Get
'        Set(ByVal value As String)
'            mstrRemark = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = value
'        End Set
'    End Property


'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                          "  loanstatustranunkid " & _
'                          ", loanadvancetranunkid " & _
'                          ", status_date " & _
'                          ", statusunkid " & _
'                          ", settle_amount " & _
'                          ", remark " & _
'                          ", isvoid " & _
'                          ", voiddatetime " & _
'                          ", voiduserunkid " & _
'                          ", voidreason " & _
'                      "FROM lnloan_status_tran " & _
'                      "WHERE loanstatustranunkid = @loanstatustranunkid "

'            objDataOperation.AddParameter("@loanstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanstatustranunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintLoanstatustranunkid = CInt(dtRow.Item("loanstatustranunkid"))
'                mintLoanadvancetranunkid = CInt(dtRow.Item("loanadvancetranunkid"))
'                mdtStaus_Date = dtRow.Item("status_date")
'                mintStatusunkid = CInt(dtRow.Item("statusunkid"))

'                'Anjan (11 May 2011)-Start
'                'mdecSettle_Amount = CDbl(dtRow.Item("settle_amount"))
'                mdecSettle_Amount = CDec(dtRow.Item("settle_amount"))
'                'Anjan (11 May 2011)-End 


'                mstrRemark = dtRow.Item("remark").ToString
'                mblnIsvoid = CBool(dtRow.Item("isvoid"))
'                If IsDBNull(dtRow.Item("voiddatetime")) Then
'                    mdtVoiddatetime = Nothing
'                Else
'                    mdtVoiddatetime = dtRow.Item("voiddatetime")
'                End If
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                mstrVoidreason = dtRow.Item("voidreason").ToString
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  loanstatustranunkid " & _
'              ", loanadvancetranunkid " & _
'              ", status_date " & _
'              ", statusunkid " & _
'              ", settle_amount " & _
'              ", remark " & _
'              ", isvoid " & _
'              ", voiddatetime " & _
'              ", voiduserunkid " & _
'             "FROM lnloan_status_tran "

'            If blnOnlyActive Then
'                strQ &= " WHERE isvoid = 0 "
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (lnloan_status_tran) </purpose>
'    Public Function Insert(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
'        'Public Function Insert() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'S.SANDEEP [ 04 DEC 2013 ] -- START
'        'objDataOperation = New clsDataOperation
'        If objDataOpr Is Nothing Then
'        objDataOperation = New clsDataOperation
'        Else
'            objDataOperation = objDataOpr
'            objDataOperation.ClearParameters()
'        End If
'        'S.SANDEEP [ 04 DEC 2013 ] -- END

'        Try
'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
'            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStaus_Date)
'            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)


'            'Anjan (11 May 2011)-Start
'            'objDataOperation.AddParameter("@settle_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecSettle_Amount.ToString)
'            objDataOperation.AddParameter("@settle_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSettle_Amount)
'            'Anjan (11 May 2011)-End 


'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            strQ = "INSERT INTO lnloan_status_tran ( " & _
'                        "  loanadvancetranunkid " & _
'                        ", status_date " & _
'                        ", statusunkid " & _
'                        ", settle_amount " & _
'                        ", remark " & _
'                        ", isvoid " & _
'                        ", voiddatetime " & _
'                        ", voiduserunkid" & _
'                        ", voidreason " & _
'                    ") VALUES (" & _
'                        "  @loanadvancetranunkid " & _
'                        ", @status_date " & _
'                        ", @statusunkid " & _
'                        ", @settle_amount " & _
'                        ", @remark " & _
'                        ", @isvoid " & _
'                        ", @voiddatetime " & _
'                        ", @voiduserunkid" & _
'                        ", @voidreason " & _
'                    "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintLoanstatustranunkid = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'S.SANDEEP [ 04 DEC 2013 ] -- START
'            'objDataOperation = Nothing
'            'S.SANDEEP [ 04 DEC 2013 ] -- END
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (lnloan_status_tran) </purpose>
'    Public Function Update() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@loanstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanstatustranunkid.ToString)
'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
'            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStaus_Date)
'            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)

'            'Anjan (11 May 2011)-Start
'            'objDataOperation.AddParameter("@settle_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecSettle_Amount.ToString)
'            objDataOperation.AddParameter("@settle_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSettle_Amount)
'            'Anjan (11 May 2011)-End 


'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            strQ = "UPDATE lnloan_status_tran SET " & _
'                        "  loanadvancetranunkid = @loanadvancetranunkid" & _
'                        ", status_date = @status_date" & _
'                        ", statusunkid = @statusunkid" & _
'                        ", settle_amount = @settle_amount" & _
'                        ", remark = @remark" & _
'                        ", isvoid = @isvoid" & _
'                        ", voiddatetime = @voiddatetime" & _
'                        ", voiduserunkid = @voiduserunkid " & _
'                        ", voidreason = @voidreason " & _
'                    "WHERE loanstatustranunkid = @loanstatustranunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (lnloan_status_tran) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
'        'Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'S.SANDEEP [ 04 DEC 2013 ] -- START
'        'objDataOperation = New clsDataOperation
'        If objDataOpr Is Nothing Then
'        objDataOperation = New clsDataOperation
'        Else
'            objDataOperation = objDataOpr
'            objDataOperation.ClearParameters()
'        End If
'        'S.SANDEEP [ 04 DEC 2013 ] -- END

'        Try
'            strQ = "UPDATE lnloan_status_tran SET " & _
'                    "  isvoid = @isvoid" & _
'                    ", voiddatetime = @voiddatetime" & _
'                    ", voiduserunkid = @voiduserunkid " & _
'                    ", voidreason = @voidreason " & _
'                  "WHERE loanadvancetranunkid = @loanadvancetranunkid "

'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'S.SANDEEP [ 04 DEC 2013 ] -- START
'            'objDataOperation = Nothing
'            'S.SANDEEP [ 04 DEC 2013 ] -- END
'        End Try
'    End Function
'End Class


Public Class clsLoan_Status_tran
    Private Const mstrModuleName = "clsLoan_Status_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintLoanstatustranunkid As Integer
    Private mintLoanadvancetranunkid As Integer
    Private mdtStaus_Date As DateTime 'Shani (21-Jul-2016) -- [Date]
    Private mintStatusunkid As Integer
    Private mdecSettle_Amount As Decimal
    Private mstrRemark As String = String.Empty
    Private mintPeriodunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As DateTime 'Shani (21-Jul-2016) -- [Date]
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    'SHANI (07 JUL 2015) -- Start
    Private mblnIsCalcInterest As Boolean
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty


#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanstatustranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanstatustranunkid() As Integer
        Get
            Return mintLoanstatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanstatustranunkid = Value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanadvancetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanadvancetranunkid() As Integer
        Get
            Return mintLoanadvancetranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanadvancetranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Staus_Date() As DateTime 'Shani (21-Jul-2016) -- [Date]

        Get
            Return mdtStaus_Date
        End Get
        Set(ByVal value As DateTime)
            mdtStaus_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set settle_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Settle_Amount() As Decimal
        'Anjan (11 May 2011)-End 
        Get
            Return mdecSettle_Amount
        End Get

        'Anjan (11 May 2011)-Start
        'Set(ByVal value As Double)
        Set(ByVal value As Decimal)
            'Anjan (11 May 2011)-End 
            mdecSettle_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As DateTime 'Shani (21-Jul-2016) -- [Date]
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscalculateinterest
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _IsCalculateInterest() As Boolean
        Get
            Return mblnIsCalcInterest
        End Get
        Set(ByVal value As Boolean)
            mblnIsCalcInterest = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

#End Region

    '' <summary>
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Nilay (05-May-2016) -- Start
    'Public Sub GetData()
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        'Nilay (05-May-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (05-May-2016) -- Start
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If
        'objDataOperation = New clsDataOperation
        'Nilay (05-May-2016) -- End

        Try
            strQ = "SELECT " & _
                          "  loanstatustranunkid " & _
                          ", loanadvancetranunkid " & _
                          ", status_date " & _
                          ", statusunkid " & _
                          ", periodunkid " & _
                          ", settle_amount " & _
                          ", remark " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiddatetime " & _
                          ", voiduserunkid " & _
                          ", voidreason " & _
                          ", iscalculateinterest " & _
                      "FROM lnloan_status_tran " & _
                      "WHERE loanstatustranunkid = @loanstatustranunkid "
            'Shani(07 JUL 2015)--(iscalculateinterest)

            objDataOperation.ClearParameters() 'Nilay (05-May-2016)
            objDataOperation.AddParameter("@loanstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanstatustranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoanstatustranunkid = CInt(dtRow.Item("loanstatustranunkid"))
                mintLoanadvancetranunkid = CInt(dtRow.Item("loanadvancetranunkid"))
                mdtStaus_Date = dtRow.Item("status_date")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mdecSettle_Amount = CDec(dtRow.Item("settle_amount"))
                mstrRemark = dtRow.Item("remark").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Pinkal (04-Mar-2015) -- Start
                'Enhancement - REDESIGN LOAN MODULE.
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                'Pinkal (04-Mar-2015) -- End

                'SHANI (07 JUL 2015) -- Start
                mblnIsCalcInterest = CBool(dtRow.Item("iscalculateinterest"))
                'SHANI (07 JUL 2015) -- End 

                'Nilay (05-May-2016) -- Start
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                'Nilay (05-May-2016) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Nilay (05-May-2016) -- Start
            'objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Nilay (05-May-2016) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intPeriodUnkId As Integer = 0, Optional ByVal intEmployeeUnkId As Integer = 0) As DataSet
        'Sohail (04 Jun 2018) - [intPeriodUnkId, intEmployeeUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (03 May 2018) -- Start
            'CCK Issue - Support Issue Id # 0002297 - Mismatch of amount deducted from loan between payroll report and loan report. Tried running the Written off loan tool but experienced an error on it. (Completed loans were coming in process payroll) in 72.1.
            'strQ = "SELECT " & _
            '  "  loanstatustranunkid " & _
            '  ", loanadvancetranunkid " & _
            '  ", status_date " & _
            '  ", statusunkid " & _
            '  ", settle_amount " & _
            '  ", remark " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiddatetime " & _
            '  ", voiduserunkid " & _
            '          ", periodunkid " & _
            '  ", iscalculateinterest " & _
            ' "FROM lnloan_status_tran " 'Shani[07 Jul 2015]--(iscalculateinterest)
            strQ = "SELECT " & _
                        "  lnloan_status_tran.loanstatustranunkid " & _
                        ", lnloan_status_tran.loanadvancetranunkid " & _
                        ", lnloan_status_tran.status_date " & _
                        ", lnloan_status_tran.statusunkid " & _
                        ", lnloan_status_tran.settle_amount " & _
                        ", lnloan_status_tran.remark " & _
                        ", lnloan_status_tran.userunkid " & _
                        ", lnloan_status_tran.isvoid " & _
                        ", lnloan_status_tran.voiddatetime " & _
                        ", lnloan_status_tran.voiduserunkid " & _
                        ", lnloan_status_tran.periodunkid " & _
                        ", lnloan_status_tran.iscalculateinterest " & _
                    "FROM lnloan_status_tran "
            'Sohail (03 May 2018) -- End

            'Sohail (04 Jun 2018) -- Start
            'CCK Issue - Support Issue Id # 0002297 - Mismatch of amount deducted from loan between payroll report and loan report. Tried running the Written off loan tool but experienced an error on it. (Completed loans were coming in process payroll) in 72.1.
            If intEmployeeUnkId > 0 Then
                strQ &= " LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_status_tran.loanadvancetranunkid "
            End If
            'Sohail (04 Jun 2018) -- End

            If blnOnlyActive Then
                'Sohail (04 Jun 2018) -- Start
                'CCK Issue - Support Issue Id # 0002297 - Mismatch of amount deducted from loan between payroll report and loan report. Tried running the Written off loan tool but experienced an error on it. (Completed loans were coming in process payroll) in 72.1.
                'strQ &= " WHERE isvoid = 0 "
                strQ &= " WHERE lnloan_status_tran.isvoid = 0 "
            Else
                strQ &= " WHERE 1 = 1 "
                'Sohail (04 Jun 2018) -- End

            End If

            'Sohail (04 Jun 2018) -- Start
            'CCK Issue - Support Issue Id # 0002297 - Mismatch of amount deducted from loan between payroll report and loan report. Tried running the Written off loan tool but experienced an error on it. (Completed loans were coming in process payroll) in 72.1.
            If intPeriodUnkId > 0 Then
                strQ &= " AND lnloan_status_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND lnloan_advance_tran.isvoid = 0 AND lnloan_advance_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If
            'Sohail (04 Jun 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ' <summary>
    ' Modify By: Sandeep J. Sharma
    ' </summary>
    ' <returns>Boolean</returns>
    ' <purpose> INSERT INTO Database Table (lnloan_status_tran) </purpose>

    'Nilay (25-Mar-2016) -- Start
    'Public Function Insert(ByVal xDatabaseName As String, ByVal intYearUnkid As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing, _
    '                       Optional ByVal blnIsAddBalanceTran As Boolean = False) As Boolean

    Public Function Insert(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xPeriodStart As Date, _
                           ByVal xPeriodEnd As Date, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           Optional ByVal objDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal blnIsAddBalanceTran As Boolean = False, _
                           Optional ByVal intEmployeeUnkId As Integer = 0 _
                           , Optional ByVal strFmtCurrency As String = "" _
                           ) As Boolean
        'Sohail (26 Oct 2020) - [strFmtCurrency]
        'Sohail (30 Apr 2019) - [intEmployeeUnkId]
        'Nilay (25-Mar-2016) -- End

        'Sohail (15 Dec 2015) -- [xDatabaseName,intYearUnkid,blnIsAddBalanceTran]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction() 'Sohail (07 May 2015)
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStaus_Date)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@settle_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSettle_Amount)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
            'Nilay (01-Apr-2016) -- End
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'SHANI (07 JUL 2015) -- Start
            objDataOperation.AddParameter("@iscalculateinterest", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCalcInterest.ToString)
            'SHANI (07 JUL 2015) -- End 


            strQ = "INSERT INTO lnloan_status_tran ( " & _
                        "  loanadvancetranunkid " & _
                        ", status_date " & _
                        ", periodunkid " & _
                        ", statusunkid " & _
                        ", settle_amount " & _
                        ", remark " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiddatetime " & _
                        ", voiduserunkid" & _
                        ", voidreason " & _
                        ", iscalculateinterest " & _
                    ") VALUES (" & _
                        "  @loanadvancetranunkid " & _
                        ", @status_date " & _
                        ", @periodunkid " & _
                        ", @statusunkid " & _
                        ", @settle_amount " & _
                        ", @remark " & _
                        ", @userunkid " & _
                        ", @isvoid " & _
                        ", @voiddatetime " & _
                        ", @voiduserunkid" & _
                        ", @voidreason " & _
                        ", @iscalculateinterest " & _
                    "); SELECT @@identity" 'Shani(07 JUL 2015)--(iscalculateinterest)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                If objDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                'Sohail (07 May 2015) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoanstatustranunkid = dsList.Tables(0).Rows(0).Item(0)
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            If InertAuditTrailsForLoanStatus(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Nilay (01-Apr-2016) -- End

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            If mintStatusunkid = enLoanStatus.ON_HOLD Then

                strQ = "UPDATE  lnloan_balance_tran " & _
                        "SET     isonhold = 1 " & _
                        "WHERE   lnloan_balance_tran.isvoid = 0 " & _
                                "AND lnloan_balance_tran.transaction_periodunkid = " & mintPeriodunkid & " " & _
                                "AND lnloan_balance_tran.loanadvancetranunkid = " & mintLoanadvancetranunkid & " " & _
                                "AND lnloan_balance_tran.isonhold = 0 "

            Else
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                If blnIsAddBalanceTran = True Then

                    Dim objPeriod As New clscommom_period_Tran
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                    objPeriod._xDataOperation = objDataOperation
                    'Hemant (30 Aug 2019) -- End
                    objPeriod._Periodunkid(xDatabaseName) = mintPeriodunkid
                    'Hemant (08 Nov 2019) -- Start
                    'ISSUE/ENHANCEMENT#4271(TUJIJENGE- TZ) - Not able to close Payroll period due to Loan deduction mismatch.
                    'Dim mdtEffectivedate As Date = objPeriod._Start_Date
                    Dim mdtEffectivedate As Date
                    If mintStatusunkid = enLoanStatus.COMPLETED OrElse mintStatusunkid = enLoanStatus.WRITTEN_OFF Then
                        mdtEffectivedate = objPeriod._End_Date
                    Else
                        mdtEffectivedate = objPeriod._Start_Date
                    End If
                    'Hemant (08 Nov 2019) -- End
                    Dim objLoanAdvance As New clsLoan_Advance
                    Dim dsBalance As New DataSet
                    'Nilay (15-Dec-2015) -- Start
                    'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo("List", mdtEffectivedate, "", mintLoanadvancetranunkid, , , , , True)   

                    'Nilay (25-Mar-2016) -- Start
                    'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo("List", mdtEffectivedate, "", mintLoanadvancetranunkid, , True, , , True, , , , True)       
                    'Nilay (28 Jan 2017) -- Start
                    'Issue - 64.1 - Loan EMI deducted when changing status from ON HOLD to IN PROGRESS.
                    'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                    '                                                     xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                    '                                                     "List", mdtEffectivedate, "", mintLoanadvancetranunkid, , True, , , True, , , , True)

                    'Sohail (30 Apr 2019) -- Start
                    'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
                    'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                    '                                                     xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                    '                                                     "List", mdtEffectivedate, "", mintLoanadvancetranunkid, , , , , True, , , , True)
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                    'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                    '                                                    xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                    '                                                    "List", mdtEffectivedate, intEmployeeUnkId.ToString, mintLoanadvancetranunkid, , , , , True, , , , True)
                    dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                         xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                                        "List", mdtEffectivedate, intEmployeeUnkId.ToString, mintLoanadvancetranunkid, , , , , True, , , , True, xDataOp:=objDataOperation, strFmtCurrency:=strFmtCurrency)
                    'Sohail (26 Oct 2020) - [strFmtCurrency]
                    'Hemant (30 Aug 2019) -- End                   
                    'Sohail (30 Apr 2019) -- End
                    'Nilay (28 Jan 2017) -- End

                    'Nilay (25-Mar-2016) -- End

                    'Nilay (15-Dec-2015) -- End
                    

                    If dsBalance.Tables(0).Rows.Count > 0 Then
                        Dim intPeriodID As Integer = 0

                        'Hemant (30 Aug 2019) -- Start
                        'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                        'intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtEffectivedate.AddDays(-1), xYearUnkid, 0) 'Nilay (25-Mar-2016) -- [xYearUnkid]
                        intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtEffectivedate.AddDays(-1), xYearUnkid, 0, xDataOperation:=objDataOperation)
                        'Hemant (30 Aug 2019) -- End

                        If intPeriodID <= 0 Then intPeriodID = CInt(dsBalance.Tables(0).Rows(0).Item("periodunkid"))

                        '*** Do not change bf_amount (keep amount same as previous bf_amount amount) as it will be used in Interest Amount column for Simple interesr loan calculation
                        strQ = "INSERT  INTO lnloan_balance_tran " & _
                                "( loanschemeunkid  " & _
                                ", transaction_periodunkid " & _
                                ", transactiondate " & _
                                ", periodunkid " & _
                                ", end_date " & _
                                ", loanadvancetranunkid " & _
                                ", employeeunkid " & _
                                ", payrollprocesstranunkid " & _
                                ", paymenttranunkid " & _
                                ", bf_amount " & _
                                ", bf_amountpaidcurrency " & _
                                ", amount " & _
                                ", amountpaidcurrency " & _
                                ", cf_amount " & _
                                ", cf_amountpaidcurrency " & _
                                ", userunkid " & _
                                ", isvoid " & _
                                ", voiduserunkid " & _
                                ", voiddatetime " & _
                                ", voidreason " & _
                                ", lninteresttranunkid " & _
                                ", lnemitranunkid " & _
                                ", lntopuptranunkid " & _
                                ", days " & _
                                ", bf_balance " & _
                                ", bf_balancepaidcurrency " & _
                                ", principal_amount " & _
                                ", principal_amountpaidcurrency " & _
                                ", topup_amount " & _
                                ", topup_amountpaidcurrency " & _
                                ", repayment_amount " & _
                                ", repayment_amountpaidcurrency " & _
                                ", interest_rate " & _
                                ", interest_amount " & _
                                ", interest_amountpaidcurrency " & _
                                ", cf_balance " & _
                                ", cf_balancepaidcurrency " & _
                                ", isonhold " & _
                                ", isreceipt  " & _
                                ", nexteffective_days " & _
                                ", nexteffective_months " & _
                                ", exchange_rate " & _
                                ", isbrought_forward " & _
                                ", loanstatustranunkid " & _
                                ") " & _
                        "VALUES  ( " & dsBalance.Tables(0).Rows(0).Item("loanschemeunkid") & "  " & _
                                ", " & mintPeriodunkid & " " & _
                                ", '" & Format(mdtEffectivedate, "yyyyMMdd HH:mm:ss") & "' " & _
                                ", " & intPeriodID & " " & _
                                ", '" & Format(mdtEffectivedate.AddDays(-1), "yyyyMMdd HH:mm:ss") & "' " & _
                                ", " & mintLoanadvancetranunkid & " " & _
                                ", " & dsBalance.Tables(0).Rows(0).Item("employeeunkid") & " " & _
                                ", 0 " & _
                                ", 0 " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BF_Amount")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BF_AmountPaidCurrency")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPMTAmount")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPMTAmountPaidCurrency")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalance")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalancePaidCurrency")) & " " & _
                                ", " & mintUserunkid & " " & _
                                ", 0 " & _
                                ", -1 " & _
                                ", NULL " & _
                                ", '' " & _
                                ", -1 " & _
                                ", -1 " & _
                                ", -1 " & _
                                ", " & CInt(dsBalance.Tables(0).Rows(0).Item("DaysDiff")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BalanceAmount")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BalanceAmountPaidCurrency")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmount")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency")) & " " & _
                                ", 0 " & _
                                ", 0 " & _
                                ", 0 " & _
                                ", 0 " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("interest_rate")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotInterestAmount")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotInterestAmountPaidCurrency")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalance")) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalancePaidCurrency")) & " " & _
                                ", " & CInt(Int(dsBalance.Tables(0).Rows(0).Item("isonhold"))) & " " & _
                                ", 0 " & _
                                ", " & DateDiff(DateInterval.Day, mdtEffectivedate, eZeeDate.convertDate(dsBalance.Tables(0).Rows(0).Item("LoanEndDate").ToString).AddDays(1)) & " " & _
                                ", " & DateDiff(DateInterval.Month, mdtEffectivedate, eZeeDate.convertDate(dsBalance.Tables(0).Rows(0).Item("LoanEndDate").ToString).AddDays(1)) & " " & _
                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("exchange_rate")) & " " & _
                                ", 0 " & _
                                ", " & mintLoanstatustranunkid & " " & _
                                ") "

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Hemant (30 Aug 2019) -- Start
                        'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                        objLoanAdvance._DataOpr = objDataOperation
                        'Hemant (30 Aug 2019) -- End
                        objLoanAdvance._Loanadvancetranunkid = mintLoanadvancetranunkid
                        'Nilay (15-Dec-2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'objLoanAdvance._Balance_Amount = objLoanAdvance._Balance_Amount - CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                        'objLoanAdvance._Balance_AmountPaidCurrency = objLoanAdvance._Balance_AmountPaidCurrency - CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency"))

                        'If objLoanAdvance.Update(, objDataOperation) = False Then
                        '    exForce = New Exception(objLoanAdvance._Message)
                        '    Throw exForce
                        'End If

                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        'If mintStatusunkid = enLoanStatus.IN_PROGRESS Then
                        '    objLoanAdvance._Balance_Amount = objLoanAdvance._Balance_Amount - CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                        '    objLoanAdvance._Balance_AmountPaidCurrency = objLoanAdvance._Balance_AmountPaidCurrency - CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency"))
                        '    'Nilay (05-May-2016) -- Start
                        '    objLoanAdvance._WebClientIP = mstrWebClientIP
                        '    objLoanAdvance._WebFormName = mstrWebFormName
                        '    objLoanAdvance._WebHostName = mstrWebHostName
                        '    'Nilay (05-May-2016) -- End

                        '    'Nilay (05-May-2016) -- Start
                        '    'If objLoanAdvance.Update(, objDataOperation) = False Then
                        '    If objLoanAdvance.Update(objDataOperation) = False Then
                        '        'Nilay (05-May-2016) -- End
                        '        exForce = New Exception(objLoanAdvance._Message)
                        '        Throw exForce
                        '    End If
                        'End If
                        'Nilay (04-Nov-2016) -- End

                        'Nilay (15-Dec-2015) -- End
                    End If
                    objLoanAdvance = Nothing
                End If
                'Sohail (15 Dec 2015) -- End

                strQ = "UPDATE  lnloan_balance_tran " & _
                        "SET     isonhold = 0 " & _
                        "WHERE   lnloan_balance_tran.isvoid = 0 " & _
                                "AND lnloan_balance_tran.transaction_periodunkid = " & mintPeriodunkid & " " & _
                                "AND lnloan_balance_tran.loanadvancetranunkid = " & mintLoanadvancetranunkid & " " & _
                                "AND lnloan_balance_tran.isonhold = 1 "
            End If

            Dim intRowAff As Integer = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                If objDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Sohail (07 May 2015) -- End

            Return True
        Catch ex As Exception
            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Public Function InertAuditTrailsForLoanStatus(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@loanstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanstatustranunkid.ToString)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStaus_Date)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@settle_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSettle_Amount)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@iscalculateinterest", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCalcInterest.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atlnloan_status_tran ( " & _
                        "  loanstatustranunkid " & _
                        ", loanadvancetranunkid " & _
                        ", status_date " & _
                        ", statusunkid " & _
                        ", settle_amount " & _
                        ", remark " & _
                        ", iscalculateinterest " & _
                        ", periodunkid " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name " & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                    ") VALUES (" & _
                        "  @loanstatustranunkid " & _
                        ", @loanadvancetranunkid " & _
                        ", @status_date " & _
                        ", @statusunkid " & _
                        ", @settle_amount " & _
                        ", @remark " & _
                        ", @iscalculateinterest " & _
                        ", @periodunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InertAuditTrailsForLoanStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    'Nilay (01-Apr-2016) -- End


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_status_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanstatustranunkid.ToString)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStaus_Date)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@settle_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSettle_Amount)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'SHANI (07 JUL 2015) -- Start
            objDataOperation.AddParameter("@iscalculateinterest", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCalcInterest.ToString)
            'SHANI (07 JUL 2015) -- End

            strQ = "UPDATE lnloan_status_tran SET " & _
                        "  loanadvancetranunkid = @loanadvancetranunkid" & _
                        ", status_date = @status_date" & _
                        ", periodunkid = @periodunkid" & _
                        ", statusunkid = @statusunkid" & _
                        ", settle_amount = @settle_amount" & _
                        ", remark = @remark" & _
                        ", userunkid = @userunkid" & _
                        ", isvoid = @isvoid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voidreason = @voidreason " & _
                        ", iscalculateinterest = @iscalculateinterest " & _
                    "WHERE loanstatustranunkid = @loanstatustranunkid " 'Shani[07 JUL 2015]--(iscalculateinterest)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'Hemant (19 Mar 2019) -- Start
            'ENHANCEMENT : Add Missing Column fields of AT Tables & Show its on "Audit & Trails" Module.
            'If InertAuditTrailsForLoanStatus(objDataOperation, 2) = False Then
            If InertAuditTrailsForLoanStatus(objDataOperation, 3) = False Then
                'Hemant (19 Mar 2019) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Nilay (01-Apr-2016) -- Start

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_status_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "UPDATE lnloan_status_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voiduserunkid = @voiduserunkid " & _
                    ", voidreason = @voidreason " & _
                  "WHERE loanadvancetranunkid = @loanadvancetranunkid "

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Dim objCommonATLog As New clsCommonATLog
            dsList = objCommonATLog.GetChildList(objDataOperation, "lnloan_status_tran", "loanadvancetranunkid", intUnkid)
            objCommonATLog = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    mintLoanstatustranunkid = dRow.Item("loanstatustranunkid")
                    Call GetData(objDataOperation)
            If InertAuditTrailsForLoanStatus(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                Next
            End If
            'Shani (21-Jul-2016) -- End
            'Nilay (01-Apr-2016) -- Start

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


    'SHANI (07 JUL 2015) -- Start
    ''' <summary>
    ''' Modify By: Shani
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_status_tran) </purpose>
    Public Function DeleteByLoanStatusUnkId(ByVal intStatusTranUnkId As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction() 'Sohail (15 Dec 2015)
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "UPDATE lnloan_status_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voiduserunkid = @voiduserunkid " & _
                    ", voidreason = @voidreason " & _
                  "WHERE loanstatustranunkid = @loanstatustranunkid "

            objDataOperation.AddParameter("@loanstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusTranUnkId)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                If objDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (05-May-2016) -- Start
            mintLoanstatustranunkid = intStatusTranUnkId
            Call GetData(objDataOperation)
            'Nilay (05-May-2016) -- End

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            If InertAuditTrailsForLoanStatus(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Nilay (01-Apr-2016) -- Start

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            strQ = "UPDATE  lnloan_balance_tran " & _
                    "SET      isvoid = 1 " & _
                    "       , voiduserunkid = " & mintVoiduserunkid & _
                    "       , voiddatetime = GETDATE() " & _
                    "       , voidreason = '" & mstrVoidreason & "' " & _
                    "WHERE   lnloan_balance_tran.isvoid = 0 " & _
                            "AND lnloan_balance_tran.loanstatustranunkid = " & intStatusTranUnkId & " "

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                If objDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Sohail (15 Dec 2015) -- End

            Return True
        Catch ex As Exception
            'Sohail (15 Dec 2015) -- Start
            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'Sohail (15 Dec 2015) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    'SHANI (07 JUL 2015) -- End 

    ''' <summary>
    ''' Modify By:Pinkal Jariwala.
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_status_tran) </purpose>
    Public Function GetPeriodWiseLoanStatusHistory(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  loanstatustranunkid " & _
                      ", loanadvancetranunkid " & _
                      ", status_date " & _
                      ", statusunkid " & _
                      ", CASE WHEN statusunkid = " & enLoanStatus.IN_PROGRESS & " THEN @InProgress " & _
                      "       WHEN statusunkid = " & enLoanStatus.ON_HOLD & " THEN @OnHold " & _
                      "       WHEN statusunkid = " & enLoanStatus.WRITTEN_OFF & " THEN @WrittenOff " & _
                      "       WHEN statusunkid = " & enLoanStatus.COMPLETED & " THEN @Completed " & _
                      "  END As status " & _
                      ", remark " & _
                      ", lnloan_status_tran.userunkid " & _
                      ", SPACE(10) + hrmsconfiguration..cfuser_master.username AS username " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", ISNULL(lnloan_status_tran.periodunkid,0) AS periodunkid " & _
                      ", ISNULL(cfcommon_period_tran.period_name,'') AS period " & _
                      ",cfcommon_period_tran.start_date " & _
                      ",cfcommon_period_tran.end_date " & _
                      ", 0 as grp	 " & _
                      ", lnloan_status_tran.iscalculateinterest " & _
                      ", cfcommon_period_tran.statusid " & _
                      " FROM lnloan_status_tran " & _
                      " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_status_tran.periodunkid " & _
                      " LEFT JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = lnloan_status_tran.userunkid "
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'ADD enum in [status]
            'Nilay (20-Sept-2016) -- End

            'Shani [07 JUL 2015]--(lnloan_status_tran.iscalculateinterest)
            If blnOnlyActive Then
                strQ &= " WHERE lnloan_status_tran.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " UNION " & _
                          " SELECT " & _
                          "  -1 AS loanstatustranunkid " & _
                          ", -1 AS loanadvancetranunkid " & _
                          ", NULL AS status_date " & _
                          ", -1 AS statusunkid " & _
                          ", '' As status " & _
                          ", '' AS remark " & _
                          ", -1 AS userunkid " & _
                          ", ISNULL(cfcommon_period_tran.period_name,'') AS username " & _
                          ", 0 AS isvoid " & _
                          ", NULL AS voiddatetime" & _
                          ", -1 AS voiduserunkid " & _
                          ", ISNULL(lnloan_status_tran.periodunkid,0) AS periodunkid " & _
                          ", ISNULL(cfcommon_period_tran.period_name,'') AS period " & _
                          ",cfcommon_period_tran.start_date " & _
                          ",cfcommon_period_tran.end_date " & _
                          ", 1 As grp " & _
                          ",'' AS iscalculateinterest " & _
                          ",cfcommon_period_tran.statusid " & _
                          " FROM lnloan_status_tran " & _
                          " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_status_tran.periodunkid "

            If blnOnlyActive Then
                strQ &= " WHERE lnloan_status_tran.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " Order by cfcommon_period_tran.end_date DESC,grp DESC,status_date DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPeriodWiseLoanStatusHistory; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'S.SANDEEP [25 MAY 2015] -- START
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Check For Entry In Database Table (lnloan_status_tran) </purpose>
    Public Function IsStatusExists(ByVal xPeriodId As Integer, ByVal xStatusId As Integer, ByVal xLoanAdvanceTranId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim blnFlag As Boolean = False
        Try
            Using objDataOpr As New clsDataOperation

                StrQ = "SELECT 1 FROM lnloan_status_tran " & _
                       "WHERE isvoid = 0 AND loanadvancetranunkid = '" & xLoanAdvanceTranId & "' " & _
                       "AND periodunkid = '" & xPeriodId & "' AND statusunkid = '" & xStatusId & "' "

                Dim dsList As New DataSet

                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    blnFlag = True
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsStatusExists; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function
    'S.SANDEEP [25 MAY 2015] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 96, "In Progress")
            Language.setMessage("clsMasterData", 97, "On Hold")
            Language.setMessage("clsMasterData", 98, "Written Off")
            Language.setMessage("clsMasterData", 100, "Completed")
            Language.setMessage(mstrModuleName, 1, "WEB")
    
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class