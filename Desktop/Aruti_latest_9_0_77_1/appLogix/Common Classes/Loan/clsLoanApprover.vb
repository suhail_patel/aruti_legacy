﻿
'************************************************************************************************************************************
'Class Name : clsLoan_Approver.vb
'Purpose    :
'Date       :06/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsLoan_Approver
    Private Shared ReadOnly mstrModuleName As String = "clsLoan_Approver"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintApprovertranunkid As Integer
    Private mintApproverunkid As Integer
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
Private mintAuditUserId As Integer = 0
Public WriteOnly Property _AuditUserId() As Boolean 
Set(ByVal value As Integer) 
mintAuditUserId = value 
End Set 
End Property 
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  approvertranunkid " & _
              ", approverunkid " & _
              ", isactive " & _
             "FROM lnloan_approver_tran " & _
             "WHERE approvertranunkid = @approvertranunkid "

            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintApproverTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintapprovertranunkid = CInt(dtRow.Item("approvertranunkid"))
                mintapproverunkid = CInt(dtRow.Item("approverunkid"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Pinkal (15-Jun-2011) -- Start

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ' <summary>
    ' Modify By: Anjan
    ' </summary>
    ' <purpose> Assign all Property variable </purpose>

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal mblnFlag As Boolean = False, _
                            Optional ByVal isFromPendingProcess As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xUACQry, xUACFiltrQry As String
            xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            If mblnFlag = True Then
                strQ = "SELECT 0 as approvertranunkid,0 As approverunkid , @Select As  approvername,'' AS Code  UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            End If

            strQ &= "SELECT  " & _
                        " lnloan_approver_tran.approvertranunkid " & _
                        ",approverunkid " & _
                        ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'') AS approvername	 " & _
                       ",hremployee_master.employeecode AS Code " & _
                    "FROM lnloan_approver_tran " & _
                        " JOIN hremployee_master ON hremployee_master.employeeunkid=lnloan_approver_tran.approverunkid "

            If isFromPendingProcess Then
                strQ &= "  JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloan_approver_tran.approvertranunkid " & _
                            "  AND lnloan_approver_mapping.userunkid = @userunkid "

                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

            End If

            'S.SANDEEP [15 NOV 2016] -- START
            If isFromPendingProcess = False Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            strQ &= " WHERE 1=1 "

            If blnOnlyActive = True Then
                strQ &= " AND lnloan_approver_tran.isactive = 1 "
            End If

            If isFromPendingProcess = False Then
                'If strUserAccessLevelFilterString = "" Then
                '    strQ &= UserAccessLevel._AccessLevelFilterString
                'Else
                '    strQ &= strUserAccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                        End If

            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblnFlag As Boolean = False, Optional ByVal isFromPendingProcess As Boolean = False _
    '                        , Optional ByVal intUserUnkId As Integer = 0 _
    '                        , Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet 'Sohail (23 Apr 2012) - [intUserUnkId,strUserAccessLevelFilterString]
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        'S.SANDEEP [ 08 AUG 2011 ] -- START
    '        'ISSUE : GENERAL CHANGES
    '        'If mblnFlag = True Then
    '        '    strQ = "SELECT 0 as approvertranunkid,0 As approverunkid , @Select As  approvername UNION "
    '        '    objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
    '        'End If

    '        If mblnFlag = True Then
    '            strQ = "SELECT 0 as approvertranunkid,0 As approverunkid , @Select As  approvername,'' AS Code  UNION "
    '            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
    '        End If
    '        'S.SANDEEP [ 08 AUG 2011 ] -- END 

    '        'S.SANDEEP [ 08 AUG 2011 ] -- START
    '        'ISSUE : GENERAL CHANGES
    '        'strQ &= "SELECT  " & _
    '        '            " lnloan_approver_tran.approvertranunkid " & _
    '        '            ",approverunkid " & _
    '        '            ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'') AS approvername	 " & _
    '        '        "FROM lnloan_approver_tran " & _
    '        '            " JOIN hremployee_master ON hremployee_master.employeeunkid=lnloan_approver_tran.approverunkid "

    '        strQ &= "SELECT  " & _
    '                    " lnloan_approver_tran.approvertranunkid " & _
    '                    ",approverunkid " & _
    '                    ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'') AS approvername	 " & _
    '                   ",hremployee_master.employeecode AS Code " & _
    '                "FROM lnloan_approver_tran " & _
    '                    " JOIN hremployee_master ON hremployee_master.employeeunkid=lnloan_approver_tran.approverunkid "
    '        'S.SANDEEP [ 08 AUG 2011 ] -- END 



    '        If isFromPendingProcess Then
    '            strQ &= "  JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloan_approver_tran.approvertranunkid " & _
    '                        "  AND lnloan_approver_mapping.userunkid = @userunkid "
    '            'Sohail (23 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId))
    '            'Sohail (23 Apr 2012) -- End
    '        End If

    '        strQ &= " WHERE 1=1 "

    '        If blnOnlyActive = True Then
    '            strQ &= " AND lnloan_approver_tran.isactive = 1 AND hremployee_master.isactive=1"
    '        End If


    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.
    '        If isFromPendingProcess = False Then
    '            'S.SANDEEP [ 04 FEB 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '            'End If
    '            'Sohail (23 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'Select Case ConfigParameter._Object._UserAccessModeSetting
    '            '    Case enAllocation.BRANCH
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.DEPARTMENT_GROUP
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.DEPARTMENT
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.SECTION_GROUP
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.SECTION
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.UNIT_GROUP
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.UNIT
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.TEAM
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.JOB_GROUP
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.JOBS
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '            '        End If
    '            'End Select
    '            If strUserAccessLevelFilterString = "" Then
    '                strQ &= UserAccessLevel._AccessLevelFilterString
    '            Else
    '                strQ &= strUserAccessLevelFilterString
    '            End If
    '            'Sohail (23 Apr 2012) -- End
    '            'S.SANDEEP [ 04 FEB 2012 ] -- END
    '        End If
    '        'Anjan (24 Jun 2011)-End 


    '        'If blnOnlyActive Then
    '        '    strQ &= " WHERE isactive = 1 "
    '        'End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- End




    'Pinkal (15-Jun-2011) -- End


    'S.SANDEEP [ 08 AUG 2011 ] -- START
    'ISSUE : GENERAL CHANGES
    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (lnloan_approver_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
    '        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


    '        strQ = "INSERT INTO lnloan_approver_tran ( " & _
    '          "  approverunkid " & _
    '          ", isactive" & _
    '        ") VALUES (" & _
    '          "  @approverunkid " & _
    '          ", @isactive" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintApprovertranunkid = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '' <summary>
    '' Modify By: Sandeep
    '' </summary>
    '' <returns>Integer</returns>
    '' <purpose> INSERT INTO Database Table (lnloan_approver_tran) </purpose>
    'Public Function Insert() As Integer
    '        'If isExist(mstrName) Then
    '        '    mstrMessage = "<Message>"
    '        '    Return False
    '        'End If

    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation
    '        Try
    '            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
    '            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

    '            strQ = "INSERT INTO lnloan_approver_tran ( " & _
    '              "  approverunkid " & _
    '              ", isactive" & _
    '            ") VALUES (" & _
    '              "  @approverunkid " & _
    '              ", @isactive" & _
    '            "); SELECT @@identity"

    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            mintApprovertranunkid = dsList.Tables(0).Rows(0).Item(0)

    '            Return mintApprovertranunkid

    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '            Return -1
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '    End Function
    'S.SANDEEP [ 08 AUG 2011 ] -- END 

    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE


    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Integer</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_approver_tran) </purpose>
    Public Function Insert(ByVal mdicValue As Dictionary(Of String, Integer), ByVal dctApprover As Dictionary(Of Integer, String)) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If mdicValue.Keys.Count > 0 Then

                For Each strkeys As String In mdicValue.Keys
                    strQ = "Select  ISNULL(approvertranunkid,0) approvertranunkid from lnloan_approver_tran where approverunkid IN (" & mdicValue(strkeys) & ")"
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                        For Each dr As DataRow In dsList.Tables(0).Rows

                            Delete(objDataOperation, CInt(dr("approvertranunkid")))

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        Next

                    End If

                Next
               

            End If


            strQ = "INSERT INTO lnloan_approver_tran ( " & _
                       "  approverunkid " & _
                       ", isactive" & _
                     ") VALUES (" & _
                       "  @approverunkid " & _
                       ", @isactive" & _
                     "); SELECT @@identity"



            For i As Integer = 0 To dctApprover.Keys.Count - 1

                If CInt(dctApprover.Keys(i)) > 0 Then Continue For

                Dim strApprover() As String = dctApprover(dctApprover.Keys(i)).ToString().Split(",")

                For j As Integer = 0 To strApprover.Length - 1

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strApprover(j).ToString())
                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintApprovertranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_approver_tran", "approvertranunkid", mintApprovertranunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END



                Next

            Next

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (12-Oct-2011) -- End

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_approver_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintApprovertranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE lnloan_approver_tran SET " & _
              "  approverunkid = @approverunkid" & _
              ", isactive = @isactive " & _
            "WHERE approvertranunkid = @approvertranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '' <summary>
    '' Modify By: Anjan
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Delete Database Table (lnloan_approver_tran) </purpose>
    'Public Function Delete(Optional ByVal intUnkid As Integer = -1) As Boolean
    'If isUsed(intUnkid) Then
    '    mstrMessage = "<Message>"
    '    Return False
    'End If

    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation

    '        Try
    '            strQ = "DELETE FROM lnloan_approver_tran "


    'S.SANDEEP [ 08 AUG 2011 ] -- START
    'ISSUE : GENERAL CHANGES
    ' objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '            If intUnkid > 0 Then
    '                strQ &= " WHERE approvertranunkid = @approvertranunkid "
    '                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '            End If
    'S.SANDEEP [ 08 AUG 2011 ] -- END 


    '            Call objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            Return True
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '            Return False
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            ' objDataOperation = Nothing
    '        End Try
    '    End Function

    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_approver_tran) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intunkID As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_tran", "approvertranunkid", intunkID) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END


            strQ = "DELETE FROM lnloan_approver_tran WHERE approvertranunkid = @approvertranunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkID)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'START FOR DELETE LOAN APPROVER MAPPING

            DeleteMapping(objDataOperation, intunkID)

            'END FOR DELETE LOAN APPROVER MAPPING

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            ' objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (12-Oct-2011) -- End
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  approvertranunkid " & _
              ", approverunkid " & _
              ", isactive " & _
             "FROM lnloan_approver_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If strCode.Length > 0 Then
                strQ &= " AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Length > 0 Then
                strQ &= "AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND approvertranunkid <> @approvertranunkid "
                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateMapping(ByVal intOldId As Integer, ByVal intNewId As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "UPDATE lnloan_approver_mapping SET approvertranunkid = @NewId WHERE approvertranunkid = @OldId "

            objDataOperation.AddParameter("@OldId", SqlDbType.Int, eZeeDataType.INT_SIZE, intOldId)
            objDataOperation.AddParameter("@NewId", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewId)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateMapping", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    '' <summary>
    '' Modify By: Sandeep
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    'Public Function DeleteMapping(ByVal intUnkid As Integer) As Boolean
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "DELETE FROM lnloan_approver_mapping "

    '        If intUnkid > 0 Then
    '            strQ &= " WHERE approvertranunkid = @approvertranunkid "
    '            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        End If

    '            Call objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '
    '            Return True
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '            Return False
    '        Finally
    '            exForce = Nothing
    '        End Try
    '    End Function

    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function DeleteMapping(ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = "SELECT ISNULL(approvermappingunkid,0) approvermappingunkid FROM lnloan_approver_mapping WHERE approvertranunkid = @approvertranunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsList.Tables(0).Rows

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_mapping", "approvermappingunkid", CInt(dr("approvermappingunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END


                Next

            End If

            strQ = "DELETE FROM lnloan_approver_mapping WHERE approvertranunkid = @approvertranunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    'Pinkal (12-Oct-2011) -- End
    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function CanDeleteApprover(ByVal intUnkid As Integer) As Boolean
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim blnFlag As Boolean = False
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT processpendingloanunkid FROM lnloan_process_pending_loan WHERE approverunkid = @Id AND isvoid = 0 "

            objDataOperation.AddParameter("@Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If objDataOperation.RecordCount(StrQ) > 0 Then
                blnFlag = True
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CanDeleteApprover", mstrModuleName)
        Finally
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class