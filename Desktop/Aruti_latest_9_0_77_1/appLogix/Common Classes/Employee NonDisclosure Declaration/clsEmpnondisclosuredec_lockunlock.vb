﻿'************************************************************************************************************************************
'Class Name : clsEmpnondisclosuredec_lockunlock.vb
'Purpose    :
'Date       :10-Feb-2020
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmpnondisclosuredec_lockunlock
    Private Const mstrModuleName = "clsEmpnondisclosuredec_lockunlock"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private xDataOp As clsDataOperation

#Region " Private variables "
    Private mintNdlockunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtLockunlockdatetime As Date
    Private mblnIslock As Boolean
    Private mintLockuserunkid As Integer
    Private mintUnlockuserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private minAuditUserunkid As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private minClientIp As String = ""
    Private mstrHostName As String = ""
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False
    Private mdtNextlockdatetime As Date
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ndlockunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Ndlockunkid() As Integer
        Get
            Return mintNdlockunkid
        End Get
        Set(ByVal value As Integer)
            mintNdlockunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lockunlockdatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Lockunlockdatetime() As Date
        Get
            Return mdtLockunlockdatetime
        End Get
        Set(ByVal value As Date)
            mdtLockunlockdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lockuserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Lockuserunkid() As Integer
        Get
            Return mintLockuserunkid
        End Get
        Set(ByVal value As Integer)
            mintLockuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unlockuserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Unlockuserunkid() As Integer
        Get
            Return mintUnlockuserunkid
        End Get
        Set(ByVal value As Integer)
            mintUnlockuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set islock
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Islock() As Boolean
        Get
            Return mblnIslock
        End Get
        Set(ByVal value As Boolean)
            mblnIslock = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Nextlockdatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Nextlockdatetime() As Date
        Get
            Return mdtNextlockdatetime
        End Get
        Set(ByVal value As Date)
            mdtNextlockdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _AuditUserunkid() As Integer
        Get
            Return minAuditUserunkid
        End Get
        Set(ByVal value As Integer)
            minAuditUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDateTime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _AuditDateTime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIp
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  ndlockunkid " & _
              ", employeeunkid " & _
              ", lockunlockdatetime " & _
              ", lockuserunkid " & _
              ", unlockuserunkid " & _
              ", islock " & _
              ", nextlockdatetime " & _
              ", loginemployeeunkid " & _
             "FROM hrempnondisclosuredec_lockunlock " & _
             "WHERE ndlockunkid = @ndlockunkid "

            objDataOperation.AddParameter("@ndlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNdlockunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintNdlockunkid = CInt(dtRow.Item("ndlockunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtLockunlockdatetime = dtRow.Item("lockunlockdatetime")
                mintLockuserunkid = CInt(dtRow.Item("lockuserunkid"))
                mintUnlockuserunkid = CInt(dtRow.Item("unlockuserunkid"))
                mblnIslock = CBool(dtRow.Item("islock"))
                If IsDBNull(dtRow.Item("nextlockdatetime")) = True Then
                    mdtNextlockdatetime = Nothing
                Else
                    mdtNextlockdatetime = CDate(dtRow.Item("nextlockdatetime"))
                End If
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal strTableName As String _
                            , Optional ByVal strFilter As String = "" _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT " & _
                          "  ndlockunkid " & _
                          ", hremployee_master.employeeunkid " & _
                          ", hremployee_master.employeecode " & _
                          ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _
                          ", lockunlockdatetime " & _
                          ", lockuserunkid " & _
                          ", unlockuserunkid " & _
                          ", islock " & _
                          ", nextlockdatetime " & _
                          ", loginemployeeunkid " & _
                         " FROM hrempnondisclosuredec_lockunlock " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrempnondisclosuredec_lockunlock.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE 1 = 1 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrempnondisclosuredec_lockunlock) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
            objDataOperation.AddParameter("@lockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockuserunkid.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            If mdtNextlockdatetime = Nothing Then
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNextlockdatetime.ToString)
            End If
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

            strQ = "INSERT INTO hrempnondisclosuredec_lockunlock ( " & _
                          "  employeeunkid " & _
                          ", lockunlockdatetime " & _
                          ", lockuserunkid " & _
                          ", unlockuserunkid " & _
                          ", islock " & _
                          ", nextlockdatetime " & _
                          ", loginemployeeunkid" & _
                        ") VALUES (" & _
                          "  @employeeunkid " & _
                          ", @lockunlockdatetime " & _
                          ", @lockuserunkid " & _
                          ", @unlockuserunkid " & _
                          ", @islock " & _
                          ", @nextlockdatetime " & _
                          ", @loginemployeeunkid" & _
                        "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintNdlockunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATEmployee_LockUnlock(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrempnondisclosuredec_lockunlock) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ndlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNdlockunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
            objDataOperation.AddParameter("@lockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockuserunkid.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            If mdtNextlockdatetime = Nothing Then
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNextlockdatetime.ToString)
            End If
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

            strQ = " UPDATE hrempnondisclosuredec_lockunlock SET " & _
                         "  employeeunkid = @employeeunkid" & _
                         ", lockunlockdatetime = @lockunlockdatetime" & _
                         ", lockuserunkid = @lockuserunkid" & _
                         ", unlockuserunkid = @unlockuserunkid" & _
                         ", islock = @islock" & _
                         ", nextlockdatetime = @nextlockdatetime" & _
                         ", loginemployeeunkid = @loginemployeeunkid " & _
                         " WHERE ndlockunkid = @ndlockunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATEmployee_LockUnlock(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrempnondisclosuredec_lockunlock) </purpose>
    Public Function GlobalUnlockEmployee(ByVal dtUnlockEmployee As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If dtUnlockEmployee Is Nothing Then Return True

            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()

            strQ = " UPDATE hrempnondisclosuredec_lockunlock SET " & _
                         "  lockunlockdatetime = @lockunlockdatetime" & _
                         ", unlockuserunkid = @unlockuserunkid" & _
                         ", islock = @islock" & _
                         ", nextlockdatetime = @nextlockdatetime" & _
                         " WHERE ndlockunkid = @ndlockunkid "


            For Each dr As DataRow In dtUnlockEmployee.Rows
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@ndlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("ndlockunkid").ToString)
                objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
                objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
                objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False.ToString)
                If mdtNextlockdatetime = Nothing Then
                    objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNextlockdatetime.ToString)
                End If
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintNdlockunkid = CInt(dr("ndlockunkid"))
                mintEmployeeunkid = CInt(dr("employeeunkid"))

                If InsertATEmployee_LockUnlock(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: GlobalUnlockEmployee; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@ndlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEmployeeId As Integer, ByRef blnisLock As Boolean, ByRef xAdLockId As Integer, ByRef dtNextLockdate As Date) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        blnisLock = False
        xAdLockId = 0
        dtNextLockdate = Nothing

        Try
            strQ = "SELECT " & _
                          "  ndlockunkid " & _
                          ", employeeunkid " & _
                          ", lockunlockdatetime " & _
                          ", lockuserunkid " & _
                          ", unlockuserunkid " & _
                          ", islock " & _
                          ", nextlockdatetime " & _
                          ", loginemployeeunkid " & _
                          "FROM hrempnondisclosuredec_lockunlock " & _
                          "WHERE employeeunkid = @employeeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                xAdLockId = CInt(dsList.Tables(0).Rows(0)("ndlockunkid"))
                blnisLock = CBool(dsList.Tables(0).Rows(0)("islock"))
                If IsDBNull(dsList.Tables(0).Rows(0)("nextlockdatetime")) = True Then
                    dtNextLockdate = Nothing
                Else
                    dtNextLockdate = CDate(dsList.Tables(0).Rows(0)("nextlockdatetime"))
                End If
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertATEmployee_LockUnlock(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ndlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNdlockunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            If mdtNextlockdatetime = Nothing Then
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNextlockdatetime.ToString)
            End If
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            strQ = "INSERT INTO athrempnondisclosuredec_lockunlock ( " & _
                          "  ndlockunkid " & _
                          ", employeeunkid " & _
                          ", islock " & _
                          ", nextlockdatetime " & _
                          ", auditdatetime " & _
                          ", audittype " & _
                          ", audituserunkid " & _
                          ", loginemployeeunkid " & _
                          ", ip " & _
                          ", host " & _
                          ", form_name " & _
                          ", isweb " & _
                        ") VALUES (" & _
                          "  @ndlockunkid " & _
                          ", @employeeunkid " & _
                          ", @islock " & _
                           ",@nextlockdatetime " & _
                          ", @auditdatetime " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @loginemployeeunkid " & _
                          ", @ip " & _
                          ", @host " & _
                          ", @form_name " & _
                          ", @isweb " & _
                        "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATEmployee_LockUnlock; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


End Class
