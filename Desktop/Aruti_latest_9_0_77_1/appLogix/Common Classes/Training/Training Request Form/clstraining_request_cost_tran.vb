﻿'************************************************************************************************************************************
'Class Name : clstraining_request_master.vb
'Purpose    :
'Date       :17-Feb-2021
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>

Public Class clstraining_request_cost_tran
    Private Shared ReadOnly mstrModuleName As String = "clstraining_request_cost_tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintTrainingRequestCostTranunkid As Integer
    Private mintTrainingRequestunkid As Integer
    Private mintTrainingCostItemunkid As Integer
    Private mdecAmount As Decimal
    Private mintUserunkid As Integer
    Private mintLoginEmployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidLoginEmployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mdtTran As DataTable

#End Region

#Region " Public variables "
    Public pintTrainingRequestCostTranunkid As Integer
    Public pintTrainingRequestunkid As Integer
    Public pintTrainingCostItemunkid As Integer
    Public pdecAmount As Decimal
    Public pintUserunkid As Integer
    Public pintLoginEmployeeunkid As Integer
    Public pblnIsvoid As Boolean
    Public pintVoiduserunkid As Integer
    Public pintVoidLoginEmployeeunkid As Integer
    Public pdtVoiddatetime As Date
    Public pstrVoidreason As String = String.Empty
    Public pstrHostName As String = ""
    Public pstrClientIP As String = ""
    Public pintAuditUserId As Integer = 0
    Public pblnIsWeb As Boolean = False
    Public pstrFormName As String = ""
    Public pdtTran As DataTable

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Private mDataOp As clsDataOperation = Nothing
    Public Property _DataOp() As clsDataOperation
        Get
            Return mDataOp
        End Get
        Set(ByVal value As clsDataOperation)
            mDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingrequestcosttranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingRequestCostTranunkid() As Integer
        Get
            Return mintTrainingRequestCostTranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingRequestCostTranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingrequestunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingRequestunkid() As Integer
        Get
            Return mintTrainingRequestunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingRequestunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingcostitemunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingCostItemunkid() As Integer
        Get
            Return mintTrainingCostItemunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingCostItemunkid = value
        End Set
    End Property

    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TranDataTable
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TranDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("ischeck")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingrequestcosttranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingrequestunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingcostitemunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("loginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidloginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  trainingrequestcosttranunkid " & _
                       ", trainingrequestunkid " & _
                       ", trainingcostitemunkid " & _
                       ", amount " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
             "FROM trtraining_request_cost_tran " & _
             "WHERE trainingrequestcosttranunkid = @trainingrequestcosttranunkid "

            objDataOperation.AddParameter("@trainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestCostTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrainingRequestCostTranunkid = CInt(dtRow.Item("trainingrequestcosttranunkid"))
                mintTrainingRequestunkid = CInt(dtRow.Item("trainingrequestunkid"))
                mintTrainingCostItemunkid = CInt(dtRow.Item("trainingcostitemunkid"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginEmployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidLoginEmployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>    ''' 
    Public Function GetList(ByVal strTableName As String, ByVal xItemTypeId As Integer, ByVal intTrainingRequestId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mDataOp IsNot Nothing Then
            objDataOperation = mDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ &= " SELECT " & _
                       "  infounkid " & _
                       ", infotypeid " & _
                       ", info_code " & _
                       ", info_name " & _
                       ", description " & _
                       ", defaultitemtypeid " & _
                       ", isactive " & _
                       ", trtraining_request_cost_tran.amount " & _
                       ", trtraining_request_cost_tran.trainingrequestcosttranunkid " & _
                    " FROM trtrainingitemsinfo_master " & _
                    " LEFT JOIN trtraining_request_cost_tran ON trtraining_request_cost_tran.trainingcostitemunkid = trtrainingitemsinfo_master.infounkid " & _
                                " AND trtraining_request_cost_tran.isvoid = 0  " & _
                    " WHERE isactive = 1 " & _
                    " AND infotypeid = @infotypeid " & _
                    " AND trtraining_request_cost_tran.trainingrequestunkid = @trainingrequestunkid " & _
                    " UNION ALL " & _
                    " SELECT " & _
                       "  infounkid " & _
                       ", infotypeid " & _
                       ", info_code " & _
                       ", info_name " & _
                       ", description " & _
                       ", defaultitemtypeid " & _
                       ", isactive " & _
                       ", 0 AS amount " & _
                       ", -1 as trainingrequestcosttranunkid " & _
                    " FROM trtrainingitemsinfo_master " & _
                    " WHERE isactive = 1 " & _
                    " AND infotypeid = @infotypeid " & _
                    " AND infounkid NOT IN " & _
                            " ( " & _
                                " SELECT " & _
                                    " trainingcostitemunkid " & _
                                "FROM trtraining_request_cost_tran " & _
                                "WHERE  trtraining_request_cost_tran.isvoid = 0 AND trainingrequestunkid = @trainingrequestunkid " & _
                            " ) "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingRequestId.ToString)
            objDataOperation.AddParameter("@infotypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemTypeId)

            'strQ = "SELECT " & _
            ' "  trtraining_request_cost_tran.trainingrequestcosttranunkid " & _
            ' ", trtraining_request_cost_tran.trainingrequestunkid " & _
            ' ", trtraining_request_cost_tran.trainingcostitemunkid " & _
            ' ", ISNULL(trtrainingitemsinfo_master.info_code, '') AS info_code " & _
            ' ", ISNULL(trtrainingitemsinfo_master.info_name, '') AS info_name " & _
            ' ", trtraining_request_cost_tran.amount " & _
            ' ", trtraining_request_cost_tran.userunkid " & _
            ' ", trtraining_request_cost_tran.loginemployeeunkid " & _
            ' ", trtraining_request_cost_tran.isweb " & _
            ' ", trtraining_request_cost_tran.isvoid " & _
            ' ", trtraining_request_cost_tran.voiduserunkid " & _
            ' ", trtraining_request_cost_tran.voidloginemployeeunkid " & _
            ' ", trtraining_request_cost_tran.voiddatetime " & _
            ' ", trtraining_request_cost_tran.voidreason " & _
            '"FROM trtraining_request_cost_tran " & _
            '   "LEFT JOIN trtrainingitemsinfo_master ON trtrainingitemsinfo_master.infounkid = trtraining_request_cost_tran.trainingcostitemunkid " & _
            '" WHERE trtraining_request_cost_tran.isvoid = 0 "

            'If intTrainingRequestId > 0 Then
            '    strQ &= " AND trtraining_request_cost_tran.trainingrequestunkid = @trainingrequestunkid "
            '    objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingRequestId)
            'ElseIf intTrainingRequestId = 0 Then
            '    strQ &= " AND 1 = 2 "
            'End If

            'If strFilter.Trim <> "" Then
            '    strQ &= " " & strFilter & " "
            'End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtraining_request_cost_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintTrainingRequestunkid, mintTrainingCostItemunkid, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Training is already defined. Please define new Training.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingCostItemunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "INSERT INTO trtraining_request_cost_tran ( " & _
                       "  trainingrequestunkid " & _
                       ", trainingcostitemunkid " & _
                       ", amount " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                    ") VALUES (" & _
                       "  @trainingrequestunkid " & _
                       ", @trainingcostitemunkid " & _
                       ", @amount " & _
                       ", @userunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidloginemployeeunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingRequestCostTranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtraining_request_cost_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintTrainingRequestunkid, mintTrainingCostItemunkid, mintTrainingRequestCostTranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Training is already defined. Please define new Training.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestCostTranunkid.ToString)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingCostItemunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "UPDATE trtraining_request_cost_tran SET " & _
                        "trainingrequestunkid = @trainingrequestunkid " & _
                        "trainingcostitemunkid = @trainingcostitemunkid " & _
                        "amount = @amount " & _
                        "userunkid = @userunkid " & _
                        "loginemployeeunkid = @loginemployeeunkid " & _
                        "isvoid = @isvoid " & _
                        "voiduserunkid = @voiduserunkid " & _
                        "voidloginemployeeunkid = @voidloginemployeeunkid " & _
                        "voiddatetime = @voiddatetime " & _
                        "voidreason = @voidreason " & _
                    "WHERE trainingrequestcosttranunkid = @trainingrequestcosttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SaveAll(ByVal lstTrainigCost As List(Of clstraining_request_cost_tran) _
                            , Optional ByVal clsTrainingRequestMaster As clstraining_request_master = Nothing _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            If mintTrainingRequestunkid <= 0 AndAlso clsTrainingRequestMaster IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If clsTrainingRequestMaster.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintTrainingRequestunkid = intNewUnkId
                End If
            ElseIf mintTrainingRequestunkid > 0 AndAlso clsTrainingRequestMaster IsNot Nothing Then
                If clsTrainingRequestMaster.Update(objDataOperation) = False Then
                    Return False
                End If
            End If

            For Each clsTrainingCost As clstraining_request_cost_tran In lstTrainigCost

                With clsTrainingCost
                    mintTrainingRequestCostTranunkid = .pintTrainingRequestCostTranunkid
                    mintTrainingRequestunkid = mintTrainingRequestunkid
                    mintTrainingCostItemunkid = .pintTrainingCostItemunkid
                    mdecAmount = .pdecAmount

                    mintUserunkid = .pintUserunkid
                    mintLoginEmployeeunkid = .pintLoginemployeeunkid
                    mblnIsWeb = .pblnIsweb
                    mblnIsvoid = .pblnIsvoid
                    mintVoiduserunkid = .pintVoiduserunkid
                    mintVoidLoginEmployeeunkid = .pintVoidloginemployeeunkid
                    mdtVoiddatetime = .pdtVoiddatetime
                    mstrVoidreason = .pstrVoidreason

                    mintAuditUserId = .pintAuditUserId
                    mstrClientIP = .pstrClientIP
                    mstrHostName = .pstrHostName
                    mstrFormName = .pstrFormName

                    mintTrainingRequestCostTranunkid = isExist(mintTrainingRequestunkid, mintTrainingCostItemunkid, , objDataOperation)
                    If mintTrainingRequestCostTranunkid <= 0 Then
                        If Insert(objDataOperation) = False Then
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    Else
                        If Update(False, objDataOperation) = False Then
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    End If

                End With

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtraining_request_cost_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _TrainingRequestCostTranunkid = intUnkid

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE trtraining_request_cost_tran SET " & _
                    " isvoid = @isvoid" & _
                    ",voiduserunkid = @voiduserunkid" & _
                    ",voiddatetime = @voiddatetime" & _
                    ",voidreason = @voidreason " & _
                   "WHERE trainingrequestcosttranunkid = @trainingrequestcosttranunkid "

            objDataOperation.AddParameter("@trainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal lstTrainingCostItem As List(Of clstraining_request_cost_tran) _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            For Each clsTrainingCostItem As clstraining_request_cost_tran In lstTrainingCostItem

                With clsTrainingCostItem
                    mDataOp = objDataOperation
                    _TrainingRequestCostTranunkid = .pintTrainingRequestCostTranunkid

                    mblnIsvoid = .pblnIsvoid
                    mintVoiduserunkid = .pintVoiduserunkid
                    mintVoidLoginEmployeeunkid = .pintVoidLoginEmployeeunkid
                    mdtVoiddatetime = .pdtVoiddatetime
                    mstrVoidreason = .pstrVoidreason

                    mblnIsWeb = .pblnIsWeb
                    mintAuditUserId = .pintAuditUserId
                    mstrClientIP = .pstrClientIP
                    mstrHostName = .pstrHostName
                    mstrFormName = .pstrFormName

                    If Delete(mintTrainingRequestCostTranunkid, objDataOperation) = False Then
                        If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        Exit For
                    End If

                End With

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Insert_Update_Delete(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim i As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exFoece As Exception

        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = " INSERT INTO trtraining_request_cost_tran (" & _
                                            "  trainingrequestunkid " & _
                                            " ,trainingcostitemunkid " & _
                                            " ,amount " & _
                                            " ,userunkid " & _
                                            " ,loginemployeeunkid " & _
                                            " ,isvoid " & _
                                            " ,voiddatetime " & _
                                            " ,voiduserunkid " & _
                                            " ,voidloginemployeeunkid " & _
                                            " ,voidreason " & _
                                        ") VALUES (" & _
                                            "  @trainingrequestunkid " & _
                                            " ,@trainingcostitemunkid " & _
                                            " ,@amount " & _
                                            " ,@userunkid " & _
                                            " ,@loginemployeeunkid " & _
                                            " ,@isvoid " & _
                                            " ,@voiddatetime " & _
                                            " ,@voiduserunkid " & _
                                            " ,@voidloginemployeeunkid " & _
                                            " ,@voidreason " & _
                                        "); SELECT @@identity "

                                objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid)
                                objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingcostitemunkid"))
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                _TrainingRequestCostTranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertAuditTrails(objDataOperation, 1) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If


                            Case "U"
                                strQ = "UPDATE trtraining_request_cost_tran SET " & _
                                            "  trainingrequestunkid = @trainingrequestunkid " & _
                                            " ,trainingcostitemunkid = @trainingcostitemunkid " & _
                                            " ,amount = @amount " & _
                                            " ,userunkid = @userunkid " & _
                                            " ,loginemployeeunkid = @loginemployeeunkid  " & _
                                            " ,isvoid = @isvoid " & _
                                            " ,voiddatetime = @voiddatetime " & _
                                            " ,voiduserunkid = @voiduserunkid " & _
                                            " ,voidloginemployeeunkid = @voidloginemployeeunkid " & _
                                            " ,voidreason = @voidreason " & _
                                        "WHERE trainingrequestcosttranunkid = @trainingrequestcosttranunkid "

                                objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid)
                                objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingcostitemunkid"))
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@trainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingrequestcosttranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                _TrainingRequestCostTranunkid = CInt(.Item("trainingrequestcosttranunkid"))

                                If InsertAuditTrails(objDataOperation, 2) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                            Case "D"
                                strQ = "UPDATE trtraining_request_cost_tran SET " & _
                                            " isvoid = @isvoid " & _
                                            " ,voiddatetime = @voiddatetime " & _
                                            " ,voiduserunkid = @voiduserunkid " & _
                                            " ,voidloginemployeeunkid = @voidloginemployeeunkid " & _
                                            " ,voidreason = @voidreason " & _
                                        "WHERE trainingrequestcosttranunkid = @trainingrequestcosttranunkid "

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@trainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingrequestcosttranunkid"))

                                _TrainingRequestCostTranunkid = .Item("trainingrequestcosttranunkid")

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                If InsertAuditTrails(objDataOperation, 3) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                        End Select
                    End If

                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_Delete , Module Name: " & mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@trainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iTrainingRequestId As Integer, ByVal iTrainingCostItemId As String, Optional ByVal intUnkid As Integer = -1 _
                             , Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                        "trainingrequestcosttranunkid " & _
                       ",trainingrequestunkid " & _
                       ",trainingcostitemunkid " & _
                       ",amount " & _
                       ",userunkid " & _
                       ",loginemployeeunkid " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voidloginemployeeunkid " & _
                       ",voiddatetime " & _
                       ", voidreason " & _
                    "FROM trtraining_request_cost_tran " & _
                    "WHERE isvoid = 0 " & _
                    " AND trainingcostitemunkid = @trainingcostitemunkid " & _
                    " AND trainingrequestunkid  = @trainingrequestunkid "

            If intUnkid > 0 Then
                strQ &= " AND trainingrequestcosttranunkid <> @trainingrequestcosttranunkid"
            End If

            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iTrainingRequestId)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iTrainingCostItemId)
            objDataOperation.AddParameter("@trainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO attrtraining_request_cost_tran ( " & _
                       "  tranguid " & _
                       ", trainingrequestcosttranunkid " & _
                       ", trainingrequestunkid " & _
                       ", trainingcostitemunkid " & _
                       ", amount " & _
                       ", audituserunkid " & _
                       ", loginemployeeunkid " & _
                       ", audittypeid " & _
                       ", auditdatetime " & _
                       ", isweb " & _
                       ", ip " & _
                       ", host " & _
                       ", form_name " & _
                  ") VALUES (" & _
                       "  LOWER(NEWID()) " & _
                       ", @trainingrequestcosttranunkid " & _
                       ", @trainingrequestunkid " & _
                       ", @trainingcostitemunkid " & _
                       ", @amount " & _
                       ", @audituserunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @audittypeid " & _
                       ", GETDATE() " & _
                       ", @isweb " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @form_name " & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestCostTranunkid.ToString)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingCostItemunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function DeleteByTrainingRequestUnkid(ByVal intTrainingRequestid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "INSERT INTO attrtraining_request_cost_tran ( " & _
                       "  tranguid " & _
                       ", trainingrequestcosttranunkid " & _
                       ", trainingrequestunkid " & _
                       ", trainingcostitemunkid " & _
                       ", amount " & _
                       ", audittypeid " & _
                       ", audituserunkid " & _
                       ", loginemployeeunkid " & _
                       ", auditdatetime " & _
                       ", form_name " & _
                       ", ip " & _
                       ", host " & _
                       ", isweb ) " & _
                  " SELECT " & _
                       "  LOWER(NEWID()) " & _
                       ", trainingrequestcosttranunkid " & _
                       ", trainingrequestunkid " & _
                       ", trainingcostitemunkid " & _
                       ", amount " & _
                       ", 3 " & _
                       ", @audituserunkid " & _
                       ", @loginemployeeunkid " & _
                       ", GETDATE() " & _
                       ", @form_name " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @isweb" & _
                   " From trtraining_request_cost_tran " & _
                    "WHERE isvoid = 0 " & _
               "AND trainingrequestunkid = @trainingrequestunkid "


            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingRequestid.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE trtraining_request_cost_tran SET " & _
                       "  isvoid = @isvoid" & _
                       " ,voiduserunkid = @voiduserunkid" & _
                       " ,voidloginemployeeunkid = @voidloginemployeeunkid" & _
                       " ,voiddatetime = @voiddatetime" & _
                       " ,voidreason = @voidreason " & _
                   "WHERE isvoid = 0 AND trainingrequestunkid = @trainingrequestunkid "

            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingRequestid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteByCalendarUnkid; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
End Class
