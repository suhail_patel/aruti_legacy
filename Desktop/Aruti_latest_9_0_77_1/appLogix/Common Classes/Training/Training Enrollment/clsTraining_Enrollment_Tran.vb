﻿'************************************************************************************************************************************
'Class Name : clsTraining_Enrollment_Tran.vb
'Purpose    :
'Date       :03/08/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsTraining_Enrollment_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Enrollment_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTrainingenrolltranunkid As Integer
    Private mintTrainingschedulingunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtEnroll_Date As Date
    Private mintStatus_Id As Integer
    Private mstrEnroll_Remark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mblnIscancel As Boolean
    Private mintCancellationuserunkid As Integer
    Private mstrCancellationreason As String = String.Empty
    Private mdtCancellationdate As Date
    'Sohail (24 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintOperationmodeid As Integer
    Private mintJobunkid As Integer
    Private mintPeriodunkid As Integer
    Private mblnIsqualificaionupdated As Boolean
    'Sohail (24 Feb 2012) -- End
    Private mintSalaryincrementtranUnkid As Integer 'Sohail (29 Dec 2012)
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingenrolltranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Trainingenrolltranunkid() As Integer
        Get
            Return mintTrainingenrolltranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingenrolltranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingschedulingunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Trainingschedulingunkid() As Integer
        Get
            Return mintTrainingschedulingunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingschedulingunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enroll_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Enroll_Date() As Date
        Get
            Return mdtEnroll_Date
        End Get
        Set(ByVal value As Date)
            mdtEnroll_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_id
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Status_Id() As Integer
        Get
            Return mintStatus_Id
        End Get
        Set(ByVal value As Integer)
            mintStatus_Id = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enroll_remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Enroll_Remark() As String
        Get
            Return mstrEnroll_Remark
        End Get
        Set(ByVal value As String)
            mstrEnroll_Remark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscancel() As Boolean
        Get
            Return mblnIscancel
        End Get
        Set(ByVal value As Boolean)
            mblnIscancel = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancellationuserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cancellationuserunkid() As Integer
        Get
            Return mintCancellationuserunkid
        End Get
        Set(ByVal value As Integer)
            mintCancellationuserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancellationreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cancellationreason() As String
        Get
            Return mstrCancellationreason
        End Get
        Set(ByVal value As String)
            mstrCancellationreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancellationdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cancellationdate() As Date
        Get
            Return mdtCancellationdate
        End Get
        Set(ByVal value As Date)
            mdtCancellationdate = Value
        End Set
    End Property


    'Sohail (24 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set operationmodeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Operationmodeid() As Integer
        Get
            Return mintOperationmodeid
        End Get
        Set(ByVal value As Integer)
            mintOperationmodeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isqualificaionupdated
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isqualificaionupdated() As Boolean
        Get
            Return mblnIsqualificaionupdated
        End Get
        Set(ByVal value As Boolean)
            mblnIsqualificaionupdated = Value
        End Set
    End Property

    'Sohail (24 Feb 2012) -- End

    'Sohail (29 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set SalaryincrementtranUnkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _SalaryincrementtranUnkid() As Integer
        Get
            Return mintSalaryincrementtranUnkid
        End Get
        Set(ByVal value As Integer)
            mintSalaryincrementtranUnkid = value
        End Set
    End Property
    'Sohail (29 Dec 2012) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  trainingenrolltranunkid " & _
              ", trainingschedulingunkid " & _
              ", employeeunkid " & _
              ", enroll_date " & _
              ", status_id " & _
              ", enroll_remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", iscancel " & _
              ", cancellationuserunkid " & _
              ", cancellationreason " & _
              ", cancellationdate " & _
              ", ISNULL(operationmodeid, 0) AS operationmodeid " & _
              ", ISNULL(jobunkid, 0) AS jobunkid " & _
              ", ISNULL(periodunkid, 0) AS periodunkid " & _
              ", ISNULL(isqualificaionupdated, 0) AS isqualificaionupdated " & _
              ", ISNULL(salaryincrementtranunkid, 0) AS salaryincrementtranunkid " & _
             "FROM hrtraining_enrollment_tran " & _
             "WHERE trainingenrolltranunkid = @trainingenrolltranunkid " 'Sohail (24 Feb 2012) - [operationmodeid,jobunkid,periodunkid,isqualificaionupdated]
            'Sohail (29 Dec 2012) - [salaryincrementtranunkid]

            objDataOperation.AddParameter("@trainingenrolltranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintTrainingenrollTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrainingenrolltranunkid = CInt(dtRow.Item("trainingenrolltranunkid"))
                mintTrainingschedulingunkid = CInt(dtRow.Item("trainingschedulingunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtEnroll_Date = dtRow.Item("enroll_date")
                mintStatus_Id = CInt(dtRow.Item("status_id"))
                mstrEnroll_Remark = dtRow.Item("enroll_remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrvoidreason = dtRow.Item("voidreason").ToString
                mblniscancel = CBool(dtRow.Item("iscancel"))
                mintcancellationuserunkid = CInt(dtRow.Item("cancellationuserunkid"))
                mstrCancellationreason = dtRow.Item("cancellationreason").ToString
                If IsDBNull(dtRow.Item("cancellationdate")) Then
                    mdtCancellationdate = Nothing
                Else
                    mdtCancellationdate = dtRow.Item("cancellationdate")
                End If
                'Sohail (24 Feb 2012) -- Start
                'TRA - ENHANCEMENT
                mintOperationmodeid = CInt(dtRow.Item("operationmodeid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mblnIsqualificaionupdated = CBool(dtRow.Item("isqualificaionupdated"))
                mintSalaryincrementtranUnkid = CInt(dtRow.Item("salaryincrementtranunkid"))
                'Sohail (24 Feb 2012) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            ByVal blnConcateDate As Boolean, _
                            Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     hrtraining_enrollment_tran.trainingenrolltranunkid " & _
                       "    ,hrtraining_enrollment_tran.trainingschedulingunkid " & _
                       "    ,hrtraining_enrollment_tran.employeeunkid " & _
                       "    ,CONVERT(CHAR(8),hrtraining_enrollment_tran.enroll_date,112) AS EnrollDate " & _
                       "    ,hrtraining_enrollment_tran.status_id " & _
                       "    ,hrtraining_enrollment_tran.enroll_remark " & _
                       "    ,hrtraining_enrollment_tran.userunkid " & _
                       "    ,hrtraining_enrollment_tran.isvoid " & _
                       "    ,hrtraining_enrollment_tran.voiddatetime " & _
                       "    ,hrtraining_enrollment_tran.voiduserunkid " & _
                       "    ,hrtraining_enrollment_tran.voidreason " & _
                       "    ,hrtraining_enrollment_tran.iscancel " & _
                       "    ,hrtraining_enrollment_tran.cancellationuserunkid " & _
                       "    ,hrtraining_enrollment_tran.cancellationreason " & _
                       "    ,CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,112)AS CancelDate " & _
                       "    ,CASE WHEN hrtraining_enrollment_tran.status_id = 1 THEN @Progress " & _
                       "    	  WHEN hrtraining_enrollment_tran.status_id = 2 THEN @Postponed " & _
                       "    	  WHEN hrtraining_enrollment_tran.status_id = 3 THEN @Completed END AS STATUS " & _
                       "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       "    ,ISNULL(cfcommon_master.name,'') AS Title " & _
                       "    ,cfcommon_master.masterunkid as TitleId " & _
                       "    ,hremployee_master.stationunkid AS BranchId " & _
                       "    ,ISNULL(hrtraining_enrollment_tran.operationmodeid, 0) AS operationmodeid " & _
                       "    ,ISNULL(hrtraining_enrollment_tran.jobunkid, 0) AS jobunkid " & _
                       "    ,ISNULL(hrtraining_enrollment_tran.periodunkid, 0) AS periodunkid " & _
                       "    ,ISNULL(hrtraining_enrollment_tran.isqualificaionupdated, 0) AS isqualificaionupdated " & _
                       "    ,ISNULL(hrtraining_enrollment_tran.salaryincrementtranunkid, 0) AS salaryincrementtranunkid " & _
                       "    ,ISNULL(hrinstitute_master.institute_name,'') AS Provider " & _
                       "    ,ISNULL(hrtraining_scheduling.training_venue,'') AS Venue "
                If blnConcateDate = True Then
                    StrQ &= "   ,CONVERT(CHAR(8),hrtraining_scheduling.start_date,112)AS start_date " & _
                            "   ,CONVERT(CHAR(8),hrtraining_scheduling.end_date,112)AS end_date "
                End If
                StrQ &= "FROM hrtraining_enrollment_tran " & _
                        "	LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                        "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title AND mastertype = '" & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "' " & _
                        "	LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hrinstitute_master ON dbo.hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                'Anjan (28 Aug 2017) --Start
                'StrQ &= "WHERE 1 = 1 AND hrtraining_enrollment_tran.isvoid = 0 "
                StrQ &= " WHERE 1 = 1 "
                'Anjan (28 Aug 2017) --End

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If strFilter.Trim.Length > 0 Then
                    StrQ &= "AND " & strFilter
                End If

                objDo.AddParameter("@Progress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 321, "Enrolled"))
                objDo.AddParameter("@Postponed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 118, "Postponed"))
                objDo.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 119, "Completed"))

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, ByVal blnConcateDate As Boolean _
    '                        , Optional ByVal strIncludeInactiveEmployee As String = "" _
    '                        , Optional ByVal strEmployeeAsOnDate As String = "" _
    '                        , Optional ByVal strUserAccessLevelFilterString As String = "" _
    '                        , Optional ByVal strFilter As String = "") As DataSet 'Sohail (23 Apr 2012) - [strIncludeInactiveEmployee,strEmployeeAsOnDate,strUserAccessLevelFilterString]
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        'strQ = "SELECT " & _
    '        '                        " hrtraining_enrollment_tran.trainingenrolltranunkid " & _
    '        '                        ",hrtraining_enrollment_tran.trainingschedulingunkid " & _
    '        '                        ",hrtraining_enrollment_tran.employeeunkid " & _
    '        '                        ",CONVERT(CHAR(8),hrtraining_enrollment_tran.enroll_date,112)AS EnrollDate " & _
    '        '                        ",hrtraining_enrollment_tran.status_id " & _
    '        '                        ",hrtraining_enrollment_tran.enroll_remark " & _
    '        '                        ",hrtraining_enrollment_tran.userunkid " & _
    '        '                        ",hrtraining_enrollment_tran.isvoid " & _
    '        '                        ",hrtraining_enrollment_tran.voiddatetime " & _
    '        '                        ",hrtraining_enrollment_tran.voiduserunkid " & _
    '        '                        ",hrtraining_enrollment_tran.voidreason " & _
    '        '                        ",hrtraining_enrollment_tran.iscancel " & _
    '        '                        ",hrtraining_enrollment_tran.cancellationuserunkid " & _
    '        '                        ",hrtraining_enrollment_tran.cancellationreason " & _
    '        '                        ",CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,112)AS CancelDate " & _
    '        '                        ",CASE WHEN hrtraining_enrollment_tran.status_id = 1 THEN @Progress " & _
    '        '                        "		  WHEN hrtraining_enrollment_tran.status_id = 2 THEN @Postponed " & _
    '        '                        "		  WHEN hrtraining_enrollment_tran.status_id = 3 THEN @Completed " & _
    '        '                        " END AS STATUS " & _
    '        '                        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '                        ",ISNULL(hrtraining_scheduling.course_title,'') AS Title " & _
    '        '                    "FROM hrtraining_enrollment_tran " & _
    '        '                    "	LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '        '                    "	LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                    "WHERE 1 = 1 AND hrtraining_enrollment_tran.isvoid = 0 "



    '        'S.SANDEEP [ 18 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'strQ = "SELECT " & _
    '        '            " hrtraining_enrollment_tran.trainingenrolltranunkid " & _
    '        '            ",hrtraining_enrollment_tran.trainingschedulingunkid " & _
    '        '            ",hrtraining_enrollment_tran.employeeunkid " & _
    '        '            ",CONVERT(CHAR(8),hrtraining_enrollment_tran.enroll_date,112)AS EnrollDate " & _
    '        '            ",hrtraining_enrollment_tran.status_id " & _
    '        '            ",hrtraining_enrollment_tran.enroll_remark " & _
    '        '            ",hrtraining_enrollment_tran.userunkid " & _
    '        '            ",hrtraining_enrollment_tran.isvoid " & _
    '        '            ",hrtraining_enrollment_tran.voiddatetime " & _
    '        '            ",hrtraining_enrollment_tran.voiduserunkid " & _
    '        '            ",hrtraining_enrollment_tran.voidreason " & _
    '        '            ",hrtraining_enrollment_tran.iscancel " & _
    '        '            ",hrtraining_enrollment_tran.cancellationuserunkid " & _
    '        '            ",hrtraining_enrollment_tran.cancellationreason " & _
    '        '            ",CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,112)AS CancelDate " & _
    '        '            ",CASE WHEN hrtraining_enrollment_tran.status_id = 1 THEN @Progress " & _
    '        '            "		  WHEN hrtraining_enrollment_tran.status_id = 2 THEN @Postponed " & _
    '        '            "		  WHEN hrtraining_enrollment_tran.status_id = 3 THEN @Completed " & _
    '        '            " END AS STATUS " & _
    '        '            ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '            ",ISNULL(hrtraining_scheduling.course_title,'') AS Title " & _
    '        '                        ",hremployee_master.stationunkid AS BranchId " & _
    '        '        "FROM hrtraining_enrollment_tran " & _
    '        '        "	LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '        '        "	LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE 1 = 1 AND hrtraining_enrollment_tran.isvoid = 0 "


    '        strQ = "SELECT " & _
    '                    " hrtraining_enrollment_tran.trainingenrolltranunkid " & _
    '                    ",hrtraining_enrollment_tran.trainingschedulingunkid " & _
    '                    ",hrtraining_enrollment_tran.employeeunkid " & _
    '                    ",CONVERT(CHAR(8),hrtraining_enrollment_tran.enroll_date,112)AS EnrollDate " & _
    '                    ",hrtraining_enrollment_tran.status_id " & _
    '                    ",hrtraining_enrollment_tran.enroll_remark " & _
    '                    ",hrtraining_enrollment_tran.userunkid " & _
    '                    ",hrtraining_enrollment_tran.isvoid " & _
    '                    ",hrtraining_enrollment_tran.voiddatetime " & _
    '                    ",hrtraining_enrollment_tran.voiduserunkid " & _
    '                    ",hrtraining_enrollment_tran.voidreason " & _
    '                    ",hrtraining_enrollment_tran.iscancel " & _
    '                    ",hrtraining_enrollment_tran.cancellationuserunkid " & _
    '                    ",hrtraining_enrollment_tran.cancellationreason " & _
    '                    ",CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,112)AS CancelDate " & _
    '                    ",CASE WHEN hrtraining_enrollment_tran.status_id = 1 THEN @Progress " & _
    '                    "		  WHEN hrtraining_enrollment_tran.status_id = 2 THEN @Postponed " & _
    '                    "		  WHEN hrtraining_enrollment_tran.status_id = 3 THEN @Completed " & _
    '                    " END AS STATUS " & _
    '                    ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                    ",ISNULL(cfcommon_master.name,'') AS Title " & _
    '                    ",cfcommon_master.masterunkid as TitleId " & _
    '                    ",hremployee_master.stationunkid AS BranchId " & _
    '                     ", ISNULL(hrtraining_enrollment_tran.operationmodeid, 0) AS operationmodeid " & _
    '                    ", ISNULL(hrtraining_enrollment_tran.jobunkid, 0) AS jobunkid " & _
    '                    ", ISNULL(hrtraining_enrollment_tran.periodunkid, 0) AS periodunkid " & _
    '                    ", ISNULL(hrtraining_enrollment_tran.isqualificaionupdated, 0) AS isqualificaionupdated " & _
    '                    ", ISNULL(hrtraining_enrollment_tran.salaryincrementtranunkid, 0) AS salaryincrementtranunkid "
    '        'Sohail (29 Dec 2012) - [salaryincrementtranunkid]

    '        If blnConcateDate = True Then
    '            strQ &= ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112)AS start_date " & _
    '                    ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112)AS end_date "
    '        End If

    '        'SANDEEP (04 Apr 2012) - Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '        strQ &= ",ISNULL(hrinstitute_master.institute_name,'') AS Provider " & _
    '                ",ISNULL(dbo.hrtraining_scheduling.training_venue,'') AS Venue "
    '        'SANDEEP (04 Apr 2012) - END

    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        strQ &= ",hremployee_master.stationunkid " & _
    '                ",hremployee_master.deptgroupunkid " & _
    '                ",hremployee_master.departmentunkid " & _
    '                ",hremployee_master.sectiongroupunkid " & _
    '                ",hremployee_master.sectionunkid " & _
    '                ",hremployee_master.unitgroupunkid " & _
    '                ",hremployee_master.unitunkid " & _
    '                ",hremployee_master.teamunkid " & _
    '                ",hremployee_master.jobgroupunkid " & _
    '                ",hremployee_master.jobunkid " & _
    '                ",hremployee_master.classgroupunkid " & _
    '                ",hremployee_master.classunkid " & _
    '                ",hremployee_master.gradegroupunkid " & _
    '                ",hremployee_master.gradelevelunkid " & _
    '                ",hremployee_master.gradeunkid "

    '        'Anjan (21 Nov 2011)-End


    '        strQ &= "FROM hrtraining_enrollment_tran " & _
    '                "	LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '                "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title AND mastertype = '" & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "' " & _
    '                "	LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "   LEFT JOIN hrinstitute_master ON dbo.hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid " & _
    '                "WHERE 1 = 1 AND hrtraining_enrollment_tran.isvoid = 0 " 'Sohail (24 Feb 2012) - [operationmodeid,jobunkid,periodunkid,isqualificaionupdated]

    '        'S.SANDEEP [ 18 FEB 2012 ] -- END

    '        'SANDEEP (04 Apr 2012 {LEFT JOIN hrinstitute_master}) - Start - END

    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 



    '        'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'Sohail (23 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If strIncludeInactiveEmployee = "" Then strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            'Sohail (23 Apr 2012) -- End
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            'Sohail (23 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            'Sohail (23 Apr 2012) -- End
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'S.SANDEEP [ 29 JUNE 2011 ] -- END 


    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If
    '        'Sohail (23 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        '        End If
    '        'End Select
    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'Sohail (23 Apr 2012) -- End
    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Anjan (24 Jun 2011)-End 

    '        If strFilter.Trim <> "" Then
    '            strQ &= " AND " & strFilter
    '        End If
    '        'S.SANDEEP [ 24 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES [<TRAINING>]
    '        'objDataOperation.AddParameter("@Progress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 117, "In Progress"))
    '        objDataOperation.AddParameter("@Progress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 321, "Enrolled"))
    '        'S.SANDEEP [ 24 FEB 2012 ] -- END 
    '        objDataOperation.AddParameter("@Postponed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 118, "Postponed"))
    '        objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 119, "Completed"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'S.SANDEEP [ 24 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES [<TRAINING>]
    '        If blnConcateDate = True Then
    '            For Each dRow As DataRow In dsList.Tables(strTableName).Rows
    '                dRow.Item("Title") = dRow.Item("Title").ToString & " ( " & eZeeDate.convertDate(dRow.Item("start_date").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("end_date").ToString).ToShortDateString & " ) "
    '            Next
    '        End If
    '        'S.SANDEEP [ 24 FEB 2012 ] -- END 


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_enrollment_tran) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintTrainingschedulingunkid, mintEmployeeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular employee is already enrolled for this course. Please select other employee to enroll.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingschedulingunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@enroll_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnroll_Date)
            objDataOperation.AddParameter("@status_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus_Id.ToString)
            objDataOperation.AddParameter("@enroll_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEnroll_Remark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancellationuserunkid.ToString)
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancellationreason.ToString)
            If mdtCancellationdate = Nothing Then
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancellationdate)
            End If
            'Sohail (24 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationmodeid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@isqualificaionupdated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualificaionupdated.ToString)
            'Sohail (24 Feb 2012) -- End
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranUnkid.ToString) 'Sohail (29 Dec 2012)

            strQ = "INSERT INTO hrtraining_enrollment_tran ( " & _
                      "  trainingschedulingunkid " & _
                      ", employeeunkid " & _
                      ", enroll_date " & _
                      ", status_id " & _
                      ", enroll_remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", iscancel " & _
                      ", cancellationuserunkid " & _
                      ", cancellationreason " & _
                      ", cancellationdate" & _
                      ", operationmodeid " & _
                      ", jobunkid " & _
                      ", periodunkid " & _
                      ", isqualificaionupdated" & _
                      ", salaryincrementtranunkid" & _
                   ") VALUES (" & _
                      "  @trainingschedulingunkid " & _
                      ", @employeeunkid " & _
                      ", @enroll_date " & _
                      ", @status_id " & _
                      ", @enroll_remark " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason " & _
                      ", @iscancel " & _
                      ", @cancellationuserunkid " & _
                      ", @cancellationreason " & _
                      ", @cancellationdate" & _
                      ", @operationmodeid " & _
                      ", @jobunkid " & _
                      ", @periodunkid " & _
                      ", @isqualificaionupdated" & _
                      ", @salaryincrementtranunkid" & _
                    "); SELECT @@identity" 'Sohail (24 Feb 2012) - [operationmodeid,jobunkid,periodunkid,isqualificaionupdated]
            '       'Sohail (29 Dec 2012) - [salaryincrementtranunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingenrolltranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrtraining_enrollment_tran", "trainingenrolltranunkid", mintTrainingenrolltranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_enrollment_tran) </purpose>
    Public Function Update() As Boolean
        If isExist(mintTrainingschedulingunkid, mintEmployeeunkid, mintTrainingenrolltranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular employee is already enrolled for this course. Please select other employee to enroll.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@trainingenrolltranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttrainingenrolltranunkid.ToString)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttrainingschedulingunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
            objDataOperation.AddParameter("@enroll_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnroll_Date)
            objDataOperation.AddParameter("@status_id", SqlDbType.int, eZeeDataType.INT_SIZE, mintstatus_id.ToString)
            objDataOperation.AddParameter("@enroll_remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrenroll_remark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancellationuserunkid.ToString)
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancellationreason.ToString)
            If mdtCancellationdate = Nothing Then
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancellationdate)
            End If
            'Sohail (24 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationmodeid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@isqualificaionupdated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualificaionupdated.ToString)
            'Sohail (24 Feb 2012) -- End
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranUnkid.ToString) 'Sohail (29 Dec 2012)

            strQ = "UPDATE hrtraining_enrollment_tran SET " & _
              "  trainingschedulingunkid = @trainingschedulingunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", enroll_date = @enroll_date" & _
              ", status_id = @status_id" & _
              ", enroll_remark = @enroll_remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidreason = @voidreason" & _
              ", iscancel = @iscancel" & _
              ", cancellationuserunkid = @cancellationuserunkid" & _
              ", cancellationreason = @cancellationreason" & _
              ", cancellationdate = @cancellationdate " & _
              ", operationmodeid = @operationmodeid" & _
              ", jobunkid = @jobunkid" & _
              ", periodunkid = @periodunkid" & _
              ", isqualificaionupdated = @isqualificaionupdated " & _
              ", salaryincrementtranunkid = @salaryincrementtranunkid " & _
            "WHERE trainingenrolltranunkid = @trainingenrolltranunkid " 'Sohail (24 Feb 2012) - [operationmodeid,jobunkid,periodunkid,isqualificaionupdated]
            '       'Sohail (29 Dec 2012) - [salaryincrementtranunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrtraining_enrollment_tran", "trainingenrolltranunkid", mintTrainingenrolltranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_enrollment_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrtraining_enrollment_tran SET " & _
                    "  isvoid = @isvoid " & _
                    ", voiddatetime = @voiddatetime " & _
                    ", voiduserunkid = @voiduserunkid " & _
                    ", voidreason = @voidreason " & _
                   "WHERE trainingenrolltranunkid = @trainingenrolltranunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@trainingenrolltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrtraining_enrollment_tran", "trainingenrolltranunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



            Dim objAnalysisMaster As New clsTraining_Analysis_Master
            Dim blnFlag As Boolean = False
            objAnalysisMaster._Isvoid = mblnIsvoid
            objAnalysisMaster._Voidatetime = mdtVoiddatetime
            objAnalysisMaster._Voidreason = mstrVoidreason
            objAnalysisMaster._Voiduserunkid = mintVoiduserunkid
            With objAnalysisMaster
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            blnFlag = objAnalysisMaster.Delete(intUnkid)

            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@trainingenrolltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intCourseId As Integer, ByVal intEmpId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  trainingenrolltranunkid " & _
              ", trainingschedulingunkid " & _
              ", employeeunkid " & _
              ", enroll_date " & _
              ", status_id " & _
              ", enroll_remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", iscancel " & _
              ", cancellationuserunkid " & _
              ", cancellationreason " & _
              ", cancellationdate " & _
              ", ISNULL(operationmodeid, 0) AS operationmodeid " & _
              ", ISNULL(jobunkid, 0) AS jobunkid " & _
              ", ISNULL(periodunkid, 0) AS periodunkid " & _
              ", ISNULL(isqualificaionupdated, 0) AS isqualificaionupdated " & _
             "FROM hrtraining_enrollment_tran " & _
                        "WHERE trainingschedulingunkid = @CourseId " & _
                         "AND employeeunkid = @EmpId AND hrtraining_enrollment_tran.isvoid = 0 " 'Sohail (24 Feb 2012) - [operationmodeid,jobunkid,periodunkid,isqualificaionupdated]

            If intUnkid > 0 Then
                strQ &= " AND trainingenrolltranunkid <> @trainingenrolltranunkid"
            End If

            objDataOperation.AddParameter("@CourseId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseId)
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@trainingenrolltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose>Get The  Name Of Last Qualification Gained By Employee</purpose>
    Public Function Get_Last_Qualification(ByVal intEmpId As Integer) As String
        Dim strLastQualification As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            StrQ = "SELECT TOP 1 " & _
                        "    ISNULL(hrqualification_master.qualificationname, '') AS Qualification " & _
                        "FROM hremp_qualification_tran " & _
                        "    JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                        "WHERE hremp_qualification_tran.employeeunkid = @EmpId " & _
                        "    AND ISNULL(hremp_qualification_tran.isvoid, 0) = 0 " & _
                        "ORDER BY award_start_date DESC "

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            dsList = objDataOperation.ExecQuery(StrQ, "LIST")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strLastQualification = dsList.Tables(0).Rows(0)("Qualification")
            Else
                strLastQualification = ""
            End If

            Return strLastQualification

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Last_Qualification", mstrModuleName)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose>Get Total Position Filled</purpose>
    Public Function Get_Filled_Position(ByVal intCourseId As Integer, ByRef intFilled As Integer) As Integer
        Dim strLastQualification As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT " & _
                    "	COUNT(trainingschedulingunkid) AS Filled " & _
                    "FROM hrtraining_enrollment_tran " & _
                    "WHERE ISNULL(isvoid,0) = 0 AND ISNULL(iscancel,0) = 0 " & _
                    "AND trainingschedulingunkid = @CourseId "

            objDataOperation.AddParameter("@CourseId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseId)

            dsList = objDataOperation.ExecQuery(StrQ, "LIST")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intFilled = dsList.Tables(0).Rows(0)("Filled")
            Else
                intFilled = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Filled_Position", mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose>Return the Boolean If Analysis is done</purpose>
    Public Function Is_Analysis_Done(ByVal intEnrollId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
                    "	COUNT(trainingenrolltranunkid) As Cnt " & _
                   "FROM hrtraining_analysis_master " & _
                   "WHERE trainingenrolltranunkid = @EnrollId " & _
                   "AND ISNULL(isvoid,0) = 0 "
            objDataOperation.AddParameter("@EnrollId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEnrollId)

            dsList = objDataOperation.ExecQuery(StrQ, "LIST")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows(0)("Cnt") > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_EnrolledEmp(ByVal intTrainingScheduleId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objDataoperation As New clsDataOperation
        Try
            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'StrQ &= "SELECT " & _
            '      "    hremployee_master.employeeunkid AS employeeunkid " & _
            '      "   ,ISNULL(hremployee_master.employeecode,'')  AS employeecode " & _
            '      "   ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
            '      "FROM hrtraining_enrollment_tran " & _
            '      "   JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '      "WHERE trainingschedulingunkid = '" & intTrainingScheduleId & "' AND ISNULL(isvoid,0) = 0 "
            StrQ &= "SELECT " & _
                   "    hremployee_master.employeeunkid AS employeeunkid " & _
                   "   ,ISNULL(hremployee_master.employeecode,'')  AS employeecode " & _
                   "   ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                  "   ,ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                   "FROM hrtraining_enrollment_tran " & _
                   "   JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "WHERE trainingschedulingunkid = '" & intTrainingScheduleId & "' AND ISNULL(isvoid,0) = 0 "
            'Nilay (09-Aug-2016) -- End

            dsList = objDataoperation.ExecQuery(StrQ, "List")

            If objDataoperation.ErrorMessage <> "" Then
                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_EnrolledEmp; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 18 FEB 2012 ] -- END

    'S.SANDEEP [ 04 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Shared Function GetEmployee_TrainingList(ByVal intEmpId As Integer, Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation

            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT " & _
                        " hrtraining_enrollment_tran.trainingschedulingunkid AS Id " & _
                        ",ISNULL(hrtraining_scheduling.course_title,'') AS Name " & _
                    "FROM hrtraining_enrollment_tran " & _
                    "	LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                    "WHERE hrtraining_enrollment_tran.employeeunkid = @EmpId AND hrtraining_enrollment_tran.isvoid = 0 AND hrtraining_enrollment_tran.iscancel = 0 "

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select Course"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Function
    'S.SANDEEP [ 04 FEB 2012 ] -- END

    'S.SANDEEP [06-MAR-2017] -- START
    'ISSUE/ENHANCEMENT : Training Module Notification
    Public Sub SendNotification(ByVal intTrainingSchedulingId As Integer, _
                                ByVal intEmployeeId As Integer, _
                                ByVal mdtEmpAsOnDate As DateTime, _
                                ByVal strEnrollRemark As String, ByVal xLoginMod As enLogin_Mode, ByVal xUserId As Integer, ByVal xSenderName As String, ByVal intCompanyUnkId As Integer)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Dim strMessage As New System.Text.StringBuilder
        Dim objEmp As New clsEmployee_Master : Dim objTrSchedule As New clsTraining_Scheduling : Dim objCMaster As New clsCommon_Master
        Try
            objEmp._Employeeunkid(mdtEmpAsOnDate) = intEmployeeId
            objTrSchedule._Trainingschedulingunkid = intTrainingSchedulingId
            objCMaster._Masterunkid = objTrSchedule._Course_Title

            strMessage.Append("<HTML> <BODY>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>, <BR><BR>")
            strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>, <BR><BR>")
            'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 101, "This is to inform you that, you have been enrolled for the training. Please find the details below :") & "<BR><BR>")
            strMessage.Append(Language.getMessage(mstrModuleName, 101, "This is to inform you that, you have been enrolled for the training. Please find the details below :") & "<BR><BR>")
            'Gajanan [27-Mar-2019] -- End
            strMessage.Append("<TABLE border = '1'>")
            strMessage.Append("<TR bgcolor= 'SteelBlue'>")
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 102, "Course Title") & "</span></b></TD>")
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 103, "Duration") & "</span></b></TD>")
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 104, "Enrollment Remark") & "</span></b></TD>")
            'S.SANDEEP [19-MAY-2017] -- START
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 108, "Contact Person") & "</span></b></TD>")
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 109, "Venue") & "</span></b></TD>")
            'S.SANDEEP [19-MAY-2017] -- END
            strMessage.Append("</TR>")
            strMessage.Append("<TR>" & vbCrLf)
            strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & objCMaster._Name & "</span></TD>")
            'S.SANDEEP [10-MAY-2017] -- START
            'strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage(mstrModuleName, 106, "Date :") & " " & objTrSchedule._Start_Date.ToShortDateString & " - " & objTrSchedule._End_Date.ToShortDateString & "<BR> " & Language.getMessage(mstrModuleName, 107, "Time :") & " " & " (" & Format(objTrSchedule._Start_Time, "HH:MM") & " - " & Format(objTrSchedule._End_Time, "HH:MM") & ") </span></TD>")
            strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage(mstrModuleName, 106, "Date :") & " " & objTrSchedule._Start_Date.ToShortDateString & " - " & objTrSchedule._End_Date.ToShortDateString & "<BR> " & Language.getMessage(mstrModuleName, 107, "Time :") & " " & " (" & Format(objTrSchedule._Start_Time, "HH:mm") & " - " & Format(objTrSchedule._End_Time, "HH:mm") & ") </span></TD>")
            'S.SANDEEP [10-MAY-2017] -- END
            strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & strEnrollRemark & "</span></TD>")
            'S.SANDEEP [19-MAY-2017] -- START
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & objTrSchedule._Contact_Person & " - " & objTrSchedule._Contact_No & "</span></TD>")
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & objTrSchedule._Traning_Venue & "</span></TD>")
            'S.SANDEEP [19-MAY-2017] -- END
            strMessage.Append("</TR>" & vbCrLf)
            strMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End
            strMessage.Append("</span></p>")
            strMessage.Append("</BODY></HTML>")

            If strMessage.ToString.Trim.Length > 0 Then
                Dim objSendMail As New clsSendMail
                objSendMail._ToEmail = objEmp._Email
                objSendMail._Subject = Language.getMessage(mstrModuleName, 105, "Notification of Training Enrollment")
                objSendMail._Message = strMessage.ToString
                'objSendMail._FormName = ""
                'objSendMail._LoginEmployeeunkid = 0
                With objSendMail
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                objSendMail._OperationModeId = xLoginMod
                objSendMail._UserUnkid = xUserId
                objSendMail._SenderAddress = xSenderName
                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                Try
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objSendMail.SendMail()
                    objSendMail.SendMail(intCompanyUnkId)
                    'Sohail (30 Nov 2017) -- End
                Catch ex As Exception
                End Try
                objSendMail = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
        Finally
            objEmp = Nothing : objTrSchedule = Nothing : objCMaster = Nothing
        End Try
    End Sub
    'S.SANDEEP [06-MAR-2017] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Particular employee is already enrolled for this course. Please select other employee to enroll.")
            Language.setMessage(mstrModuleName, 2, "Select Course")
            Language.setMessage(mstrModuleName, 100, "Dear")
            Language.setMessage(mstrModuleName, 101, "This is to inform you that, you have been enrolled for the training. Please find the details below :")
            Language.setMessage(mstrModuleName, 102, "Course Title")
            Language.setMessage(mstrModuleName, 103, "Duration")
            Language.setMessage(mstrModuleName, 104, "Enrollment Remark")
            Language.setMessage(mstrModuleName, 105, "Notification of Training Enrollment")
            Language.setMessage(mstrModuleName, 106, "Date :")
            Language.setMessage(mstrModuleName, 107, "Time :")
			Language.setMessage(mstrModuleName, 108, "Contact Person")
			Language.setMessage(mstrModuleName, 109, "Venue")
			Language.setMessage("clsMasterData", 118, "Postponed")
			Language.setMessage("clsMasterData", 119, "Completed")
			Language.setMessage("clsMasterData", 321, "Enrolled")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
