﻿'************************************************************************************************************************************
'Class Name : clshrtnatraining_impact_feedback.vb
'Purpose    :
'Date       :28/02/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clshrtnatraining_impact_feedback
    Private Const mstrModuleName = "clshrtnatraining_impact_feedback"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFeedbackunkid As Integer
    Private mintImpactunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private dsFeedbackList As DataSet
#End Region

#Region "Constructor"

    Public Sub New()
        dsFeedbackList = New DataSet
        dsFeedbackList.Tables.Add("FeedBack")
        dsFeedbackList.Tables(0).Columns.Add("feedbackunkid", Type.GetType("System.Int32"))
        dsFeedbackList.Tables(0).Columns.Add("Impactunkid", Type.GetType("System.Int32"))
        dsFeedbackList.Tables(0).Columns.Add("questionunkid", Type.GetType("System.Int32"))
        dsFeedbackList.Tables(0).Columns.Add("question", Type.GetType("System.String"))
        dsFeedbackList.Tables(0).Columns.Add("answerunkid", Type.GetType("System.Int32"))
        dsFeedbackList.Tables(0).Columns.Add("answer", Type.GetType("System.String"))
        dsFeedbackList.Tables(0).Columns.Add("manager_remark", Type.GetType("System.String"))
        dsFeedbackList.Tables(0).Columns.Add("isvoid", Type.GetType("System.Boolean"))
        dsFeedbackList.Tables(0).Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
        dsFeedbackList.Tables(0).Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
        dsFeedbackList.Tables(0).Columns.Add("voidreason", Type.GetType("System.String"))
        dsFeedbackList.Tables(0).Columns.Add("AUD", Type.GetType("System.String"))
        dsFeedbackList.Tables(0).Columns.Add("GUID", Type.GetType("System.String"))

    End Sub

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set feedbackunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Feedbackunkid() As Integer
        Get
            Return mintFeedbackunkid
        End Get
        Set(ByVal value As Integer)
            mintFeedbackunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set impactunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Impactunkid() As Integer
        Get
            Return mintImpactunkid
        End Get
        Set(ByVal value As Integer)
            mintImpactunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dsFeedbackList
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _FeedBackList() As DataSet
        Get
            Return dsFeedbackList
        End Get
        Set(ByVal value As DataSet)
            dsFeedbackList = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intfeedbackid As Integer = -1, Optional ByVal intimpactid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  feedbackunkid " & _
              ", impactunkid " & _
              ", questionunkid " & _
              ", hrtnafdbk_item_master.name as question " & _
              ", answerunkid " & _
              ", hrresult_master.resultname as answer " & _
              ", manager_remark " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AUD " & _
              ", '' GUID " & _
              " FROM hrtnatraining_impact_feedback " & _
              " LEFT JOIN hrtnafdbk_item_master on hrtnafdbk_item_master.fdbkitemunkid = hrtnatraining_impact_feedback.questionunkid AND hrtnafdbk_item_master.isactive = 1 " & _
              " LEFT JOIN hrresult_master on hrresult_master.resultunkid = hrtnatraining_impact_feedback.answerunkid AND hrresult_master.isactive = 1 " & _
              " WHERE 1 =1 "


            If blnOnlyActive Then
                strQ &= " AND isvoid = 0 "
            End If

            If intfeedbackid > 0 Then
                strQ &= " AND feedbackunkid = @feedbackunkid "
                objDataOperation.AddParameter("@feedbackunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intfeedbackid)
            End If

            If intimpactid > 0 Then
                strQ &= " AND impactunkid = @impactunkid "
                objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intimpactid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnatraining_impact_Development) </purpose>
    Public Function InsertUpdateDelete_FeedBack(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If dsFeedbackList Is Nothing Then Return True

            For i = 0 To dsFeedbackList.Tables(0).Rows.Count - 1
                With dsFeedbackList.Tables(0).Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

Insert:
                                objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImpactunkid.ToString)
                                objDataOperation.AddParameter("@questionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("questionunkid").ToString)
                                objDataOperation.AddParameter("@answerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("answerunkid").ToString)
                                objDataOperation.AddParameter("@manager_remark", SqlDbType.NVarChar, 4000, .Item("manager_remark").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                strQ = "INSERT INTO hrtnatraining_impact_feedback ( " & _
                                            "  impactunkid " & _
                                            ", questionunkid " & _
                                            ", answerunkid " & _
                                            ", manager_remark " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                          ") VALUES (" & _
                                            "  @impactunkid " & _
                                            ", @questionunkid " & _
                                            ", @answerunkid " & _
                                            ", @manager_remark " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                          "); SELECT @@identity"

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintFeedbackunkid = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("feedbackunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_feedback", "feedbackunkid", .Item("feedbackunkid"), 2, 1, , mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", mintImpactunkid, "hrtnatraining_impact_feedback", "feedbackunkid", mintFeedbackunkid, 1, 1, , mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "U"

                                If .Item("GUID").ToString() <> "" Then
                                    GoTo Insert
                                End If
                                strQ = "UPDATE hrtnatraining_impact_feedback SET " & _
                                          "  impactunkid = @impactunkid" & _
                                          ", questionunkid = @questionunkid" & _
                                          ", answerunkid = @answerunkid" & _
                                          ", manager_remark = @manager_remark" & _
                                          ", isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = @voiddatetime" & _
                                          ", voidreason = @voidreason " & _
                                        "WHERE feedbackunkid = @feedbackunkid AND impactunkid = @impactunkid"

                                objDataOperation.AddParameter("@feedbackunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("feedbackunkid").ToString)
                                objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("impactunkid").ToString)
                                objDataOperation.AddParameter("@questionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("questionunkid").ToString)
                                objDataOperation.AddParameter("@answerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("answerunkid").ToString)
                                objDataOperation.AddParameter("@manager_remark", SqlDbType.NVarChar, 4000, .Item("manager_remark").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_feedback", "feedbackunkid", .Item("feedbackunkid"), 2, 2, , mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                            Case "D"

                                strQ = "UPDATE hrtnatraining_impact_feedback SET " & _
                                            " isvoid = @isvoid" & _
                                            ", voiduserunkid = @voiduserunkid" & _
                                            ", voiddatetime = @voiddatetime" & _
                                            ", voidreason = @voidreason " & _
                                          "WHERE feedbackunkid = @feedbackunkid AND impactunkid = @impactunkid"


                                objDataOperation.AddParameter("@feedbackunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("feedbackunkid").ToString)
                                objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("impactunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IIf(.Item("isvoid") Is DBNull.Value, False, .Item("isvoid").ToString()))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(.Item("voiduserunkid") Is DBNull.Value, -1, .Item("voiduserunkid").ToString))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("voiddatetime") Is Nothing, DBNull.Value, .Item("voiddatetime")))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(.Item("voidreason") Is DBNull.Value, "", .Item("voidreason").ToString))
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_feedback", "feedbackunkid", .Item("feedbackunkid"), 2, 3, , mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                        End Select

                    End If

                End With

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_FeedBack; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

End Class