﻿'************************************************************************************************************************************
'Class Name : clshrtnacoursepriority_tran.vb
'Purpose    :
'Date       :14/02/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clshrtnacoursepriority_tran
    Private Shared ReadOnly mstrModuleName As String = "clshrtnacoursepriority_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintTnAPriorityUnkid As Integer = 0
    Private mdTran As DataTable
    Private mintUserunkid As Integer = -1
    Private mblnIsVoid As Boolean = False
    Private mintVoidUserUnkid As Integer = -1
    Private mdtVoidDate As DateTime = Nothing
    Private mstrVoidReason As String = String.Empty
    Private mstrRefrenceNo As String = String.Empty
    Private mintPeriodUnkid As Integer = -1
    Private mintcoursepriotranunkid As Integer = -1

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public Property _TnAPriorityUnkid() As Integer
        Get
            Return mintTnAPriorityUnkid
        End Get
        Set(ByVal value As Integer)
            mintTnAPriorityUnkid = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdTran
        End Get
        Set(ByVal value As DataTable)
            mdTran = value
        End Set
    End Property

    Public Property _UserUnkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _IsVoid() As Boolean
        Get
            Return mblnIsVoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsVoid = value
        End Set
    End Property

    Public Property _VoidUserUnkid() As Integer
        Get
            Return mintVoidUserUnkid
        End Get
        Set(ByVal value As Integer)
            mintVoidUserUnkid = value
        End Set
    End Property

    Public Property _VoidDate() As DateTime
        Get
            Return mdtVoidDate
        End Get
        Set(ByVal value As DateTime)
            mdtVoidDate = value
        End Set
    End Property

    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    Public Property _RefrenceNo() As String
        Get
            Return mstrRefrenceNo
        End Get
        Set(ByVal value As String)
            mstrRefrenceNo = value
            Call GetTraingNeedAnalysis()
        End Set
    End Property

    Public Property _PeriodUnkid() As Integer
        Get
            Return mintPeriodUnkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodUnkid = value
            Call GetTraingNeedAnalysis()
        End Set
    End Property

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdTran = New DataTable("TnA")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "IsCheck"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "coursepriotranunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "prioritymasterunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)


            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "courseemptranunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "courseunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "gappriority"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "bussinesspriority"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "providerunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "goalunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 1, "KEY RESULT AREAS (KRA)")
            dCol.ColumnName = "kra"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 2, "TRAINING NEEDS")
            dCol.ColumnName = "training_need"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 3, "SKILL GAP")
            dCol.ColumnName = "skill_gap"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "IMPACT ON BUSINESS")
            dCol.ColumnName = "business_impact"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "PRIORITY RANKING")
            dCol.ColumnName = "priority_ranking"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "TARGET GROUP")
            dCol.ColumnName = "target_group"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "TRAINING SOLUTIONS")
            dCol.ColumnName = "training_sol"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "TRAINING OBJECTIVE")
            dCol.ColumnName = "training_objective"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "NUMBER OF PEOPLE")
            dCol.ColumnName = "total_emp"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 10, "PREFERRED PROVIDER")
            dCol.ColumnName = "pref_provider"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 11, "TRAINING VENUE")
            dCol.ColumnName = "training_venue"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 12, "STRATEGIC GOAL")
            dCol.ColumnName = "strategic_goal"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "IsGrp"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "GrpId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "employeeunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            '============= FOR ALLOCATION===================

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "BranchId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "DepartmentId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "SectionGrpId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "SectionId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "UnitGrpId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "UnitId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "JobId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "CostcenterId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)


            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "TeamId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            '============= FOR ALLOCATION===================


            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "AUD"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "GUID"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "isvoid"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "voiduserunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "voiddatetime"
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "voidreason"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdTran.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private & Public Functions "

    Private Sub GetTraingNeedAnalysis()
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim exForce As Exception
        Try
            If mstrRefrenceNo.Trim.Length <= 0 Then
                Dim dsGrpList As New DataSet : Dim dsGrpTran As New DataSet : mdTran.Rows.Clear()
                '''''''''''''''''''''''''''''''''''''''' [ GROUP QUERY ] '''''''''''''''''''''''''''''''''''''''''''''''''
                StrQ = "SELECT " & _
                       "     masterunkid AS courseunkid " & _
                       "    ,name AS training_need " & _
                       "    ,COUNT(A.selfemployeeunkid) AS total_emp " & _
                       "FROM cfcommon_master " & _
                       "JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         hrassess_analysis_master.selfemployeeunkid " & _
                       "        ,coursemasterunkid " & _
                       "    FROM hrassess_analysis_master " & _
                       "        JOIN hrassess_remarks_tran ON hrassess_analysis_master.analysisunkid = hrassess_remarks_tran.analysisunkid " & _
                       "    WHERE periodunkid ='" & mintPeriodUnkid & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 AND hrassess_analysis_master.isvoid = 0 " & _
                       "UNION " & _
                       "    SELECT DISTINCT " & _
                       "         hrassess_analysis_master.assessedemployeeunkid " & _
                       "        ,coursemasterunkid " & _
                       "    FROM hrassess_analysis_master " & _
                       "        JOIN hrassess_remarks_tran ON hrassess_analysis_master.analysisunkid = hrassess_remarks_tran.analysisunkid " & _
                       "    WHERE periodunkid = '" & mintPeriodUnkid & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 AND hrassess_analysis_master.isvoid = 0 " & _
                       "UNION " & _
                       "    SELECT DISTINCT " & _
                       "         hrassess_analysis_master.assessedemployeeunkid " & _
                       "        ,coursemasterunkid " & _
                       "    FROM hrassess_analysis_master " & _
                       "        JOIN hrassess_remarks_tran ON hrassess_analysis_master.analysisunkid = hrassess_remarks_tran.analysisunkid " & _
                       "    WHERE periodunkid = '" & mintPeriodUnkid & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 AND hrassess_analysis_master.isvoid = 0 " & _
                       ") AS A ON A.coursemasterunkid = cfcommon_master.masterunkid " & _
                       "WHERE mastertype = '" & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "' AND isactive = 1 " & _
                       "GROUP BY masterunkid,name ORDER BY COUNT(A.selfemployeeunkid) DESC "

                dsGrpList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                '''''''''''''''''''''''''''''''''''''''' [ GROUP QUERY ] '''''''''''''''''''''''''''''''''''''''''''''''''

                '''''''''''''''''''''''''''''''''''''''' [ TRANS QUERY ] '''''''''''''''''''''''''''''''''''''''''''''''''
                StrQ = "SELECT " & _
                        "	 coursemasterunkid " & _
                        "	,EName " & _
                        "   ,employeeunkid " & _
                        "   ,ISNULL(name,'') AS JBName " & _
                        "FROM " & _
                        "( " & _
                        "	SELECT DISTINCT " & _
                        "		 coursemasterunkid " & _
                        "		,ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                        "       ,employeeunkid " & _
                        "       ,hrjobgroup_master.name " & _
                        "	FROM hrassess_analysis_master " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_analysis_master.selfemployeeunkid " & _
                        "       LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
                        "		JOIN hrassess_remarks_tran ON hrassess_analysis_master.analysisunkid = hrassess_remarks_tran.analysisunkid " & _
                        "	WHERE periodunkid = '" & mintPeriodUnkid & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 AND hrassess_analysis_master.isvoid = 0  "
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'S.SANDEEP [ 01 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA DISCIPLINE CHANGES
                'Select Case ConfigParameter._Object._UserAccessModeSetting
                '    Case enAllocation.BRANCH
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.TEAM
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOB_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOBS
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
                '        End If
                'End Select
                StrQ &= UserAccessLevel._AccessLevelFilterString
                'S.SANDEEP [ 01 JUNE 2012 ] -- END


                'S.SANDEEP [ 18 FEB 2012 ] -- END
                StrQ &= "UNION " & _
                        "	SELECT DISTINCT " & _
                        "		 coursemasterunkid " & _
                        "		,ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                        "       ,employeeunkid " & _
                        "       ,hrjobgroup_master.name " & _
                        "	FROM hrassess_analysis_master " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_analysis_master.assessedemployeeunkid " & _
                        "       LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
                        "		JOIN hrassess_remarks_tran ON hrassess_analysis_master.analysisunkid = hrassess_remarks_tran.analysisunkid " & _
                        "	WHERE periodunkid = '" & mintPeriodUnkid & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 AND hrassess_analysis_master.isvoid = 0  "
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'S.SANDEEP [ 01 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA DISCIPLINE CHANGES
                'Select Case ConfigParameter._Object._UserAccessModeSetting
                '    Case enAllocation.BRANCH
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.TEAM
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOB_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOBS
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
                '        End If
                'End Select
                StrQ &= UserAccessLevel._AccessLevelFilterString
                'S.SANDEEP [ 01 JUNE 2012 ] -- END

                
                'S.SANDEEP [ 18 FEB 2012 ] -- END
                StrQ &= "UNION " & _
                        "	SELECT DISTINCT " & _
                        "		 coursemasterunkid " & _
                        "		,ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                        "       ,employeeunkid " & _
                        "       ,hrjobgroup_master.name " & _
                        "	FROM hrassess_analysis_master " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_analysis_master.assessedemployeeunkid " & _
                        "       LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
                        "		JOIN hrassess_remarks_tran ON hrassess_analysis_master.analysisunkid = hrassess_remarks_tran.analysisunkid " & _
                        "	WHERE periodunkid = '" & mintPeriodUnkid & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 AND hrassess_analysis_master.isvoid = 0  "
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'S.SANDEEP [ 01 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA DISCIPLINE CHANGES
                'Select Case ConfigParameter._Object._UserAccessModeSetting
                '    Case enAllocation.BRANCH
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.TEAM
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOB_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOBS
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
                '        End If
                'End Select
                StrQ &= UserAccessLevel._AccessLevelFilterString
                'S.SANDEEP [ 01 JUNE 2012 ] -- END


                'S.SANDEEP [ 18 FEB 2012 ] -- END
                StrQ &= ") AS A "

                dsGrpTran = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                '''''''''''''''''''''''''''''''''''''''' [ TRANS QUERY ] '''''''''''''''''''''''''''''''''''''''''''''''''
                Dim dtRow As DataRow = Nothing
                For Each dRow As DataRow In dsGrpList.Tables("List").Rows
                    dtRow = mdTran.NewRow
                    dtRow.Item("courseunkid") = dRow.Item("courseunkid")
                    dtRow.Item("training_need") = dRow.Item("training_need")
                    dtRow.Item("total_emp") = dRow.Item("total_emp")
                    dtRow.Item("IsGrp") = True
                    dtRow.Item("GrpId") = dRow.Item("courseunkid")
                    mdTran.Rows.Add(dtRow)

                    Dim dFilter As DataTable = New DataView(dsGrpTran.Tables("List"), "coursemasterunkid = '" & dRow.Item("courseunkid") & "'", "", DataViewRowState.CurrentRows).ToTable
                    Dim StrTargetGrp As String = ""
                    If dFilter IsNot Nothing Then
                        For Each dFRow As DataRow In dFilter.Rows
                            dtRow = mdTran.NewRow

                            dtRow.Item("courseunkid") = dRow.Item("courseunkid")
                            dtRow.Item("training_need") = Space(5) & dFRow.Item("EName")
                            dtRow.Item("total_emp") = ""
                            dtRow.Item("IsGrp") = False
                            dtRow.Item("GrpId") = dRow.Item("courseunkid")
                            dtRow.Item("employeeunkid") = dFRow.Item("employeeunkid")
                            If StrTargetGrp.Contains(dFRow.Item("JBName").ToString) = False Then
                                StrTargetGrp &= "," & dFRow.Item("JBName").ToString
                            End If
                            mdTran.Rows.Add(dtRow)
                        Next
                    End If
                    If StrTargetGrp.Length > 0 Then StrTargetGrp = Mid(StrTargetGrp, 2)
                    Dim dGrp() As DataRow = mdTran.Select("courseunkid='" & dRow.Item("courseunkid") & "' AND IsGrp=true")
                    If dGrp.Length > 0 Then
                        dGrp(0)("target_group") = StrTargetGrp.ToString
                    End If
                Next

            Else

                StrQ = " SELECT " & _
                           " hrtnaprirority_master.prioritymasterunkid " & _
                           ", hrtnacoursepriority_tran.coursepriotranunkid " & _
                           ", hrtnaprirority_master.periodunkid " & _
                           ", ISNULL(cfcommon_period_tran.period_name,'') AS period_name " & _
                           ", referenceno " & _
                           ", ISNULL(hrtnaprirority_master.remark,'') remark " & _
                           ", ISNULL(isfinal,0) isfinal " & _
                           ", hrtnacoursepriority_tran.courseunkid " & _
                           ", course.name AS course " & _
                           ", hrtnacoursepriority_tran.gappriority AS gappriorityId " & _
                           ", CASE WHEN hrtnacoursepriority_tran.gappriority = 1 THEN @Low " & _
                                    "WHEN hrtnacoursepriority_tran.gappriority = 2 THEN @Medium " & _
                                    "WHEN hrtnacoursepriority_tran.gappriority = 3 THEN @High " & _
                           "END gappriority " & _
                           ", hrtnacoursepriority_tran.bussinesspriority AS bussinesspriorityId " & _
                           ", CASE WHEN hrtnacoursepriority_tran.bussinesspriority = 1 THEN @Low " & _
                                    "WHEN hrtnacoursepriority_tran.bussinesspriority = 2 THEN @Medium " & _
                                    "WHEN hrtnacoursepriority_tran.bussinesspriority = 3 THEN @High " & _
                            "END bussinesspriority " & _
                            ", ISNULL(kra,'') kra " & _
                            ", priority_ranking " & _
                            ", hrtnacoursepriority_tran.providerunkid " & _
                            ", hrinstitute_master.institute_name " & _
                            ", ISNULL(training_venue,'') AS training_venue " & _
                            ", ISNULL(training_solution,'') AS training_solution " & _
                            ", ISNULL(training_objective,'') AS training_objective " & _
                            ", hrtnacoursepriority_tran.goalunkid " & _
                            ", goal.name AS strategicgoal " & _
                            ",  (SELECT COUNT(*) FROM hrtnaemp_course_tran " & _
                            "    WHERE hrtnaemp_course_tran.coursepriotranunkid=hrtnacoursepriority_tran.coursepriotranunkid AND hrtnaemp_course_tran.isvoid=0) as total_emp " & _
                            "FROM    hrtnaprirority_master " & _
                                    " JOIN hrtnacoursepriority_tran ON hrtnacoursepriority_tran.prioritymasterunkid = hrtnaprirority_master.prioritymasterunkid AND hrtnacoursepriority_tran.isvoid = 0 " & _
                                    " JOIN cfcommon_master course ON course.masterunkid = hrtnacoursepriority_tran.courseunkid AND course.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & _
                                    " JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = hrtnacoursepriority_tran.providerunkid AND hrinstitute_master.ishospital = 0 " & _
                                    " JOIN cfcommon_master goal ON goal.masterunkid = hrtnacoursepriority_tran.goalunkid AND goal.mastertype = " & clsCommon_Master.enCommonMaster.STRATEGIC_GOAL & _
                                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrtnaprirority_master.periodunkid " & _
                          " WHERE  referenceno = '" & mstrRefrenceNo & "' AND hrtnaprirority_master.isvoid = 0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Low", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 331, "Low"))
                objDataOperation.AddParameter("@Medium", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 332, "Medium"))
                objDataOperation.AddParameter("@High", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 333, "High"))

                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If



                StrQ = " SELECT  hrtnaprirority_master.prioritymasterunkid " & _
                           ", hrtnaemp_course_tran.courseemptranunkid " & _
                           ", hrtnaemp_course_tran.courseunkid " & _
                           ", hrtnaemp_course_tran.employeeunkid " & _
                           ", ISNULL(hremployee_master.firstname,'' ) + ' ' + ISNULL(hremployee_master.surname,'' ) EName " & _
                           ", ISNULL(hremployee_master.jobgroupunkid,0) as jobgroupunkid " & _
                           ", ISNULL(hrjobgroup_master.name,'') as JobGroup" & _
                           ", ISNULL(hremployee_master.stationunkid,0) branchId " & _
                           ", ISNULL(hremployee_master.departmentunkid,0) departmentId " & _
                           ", ISNULL(hremployee_master.sectiongroupunkid,0) sectiongroupId " & _
                           ", ISNULL(hremployee_master.sectionunkid,0) sectionId " & _
                           ", ISNULL(hremployee_master.unitgroupunkid,0) unitgroupId " & _
                           ", ISNULL(hremployee_master.unitunkid,0) unitId " & _
                           ", ISNULL(hremployee_master.jobunkid,0) jobId " & _
                           ", ISNULL(hremployee_master.costcenterunkid,0) costcenterId " & _
                           ", ISNULL(hremployee_master.teamunkid,0) teamId " & _
                           " FROM hrtnaprirority_master " & _
                           " JOIN hrtnacoursepriority_tran ON hrtnacoursepriority_tran.prioritymasterunkid = hrtnaprirority_master.prioritymasterunkid " & _
                           " JOIN hrtnaemp_course_tran ON hrtnacoursepriority_tran.coursepriotranunkid = hrtnaemp_course_tran.coursepriotranunkid " & _
                           "    AND hrtnacoursepriority_tran.courseunkid = hrtnaemp_course_tran.courseunkid AND hrtnaemp_course_tran.isvoid = 0 " & _
                           " JOIN hremployee_master ON hrtnaemp_course_tran.employeeunkid = hremployee_master.employeeunkid  " & _
                           " LEFT JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                           " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hremployee_master.stationunkid " & _
                           " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                           " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hremployee_master.sectiongroupunkid " & _
                           " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hremployee_master.sectionunkid " & _
                           " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hremployee_master.unitgroupunkid " & _
                           " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hremployee_master.unitunkid " & _
                           " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
                           " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hremployee_master.teamunkid " & _
                          " WHERE  referenceno = '" & mstrRefrenceNo & "' AND hrtnaprirority_master.isvoid = 0 "


                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'S.SANDEEP [ 01 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA DISCIPLINE CHANGES
                'Select Case ConfigParameter._Object._UserAccessModeSetting
                '    Case enAllocation.BRANCH
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.TEAM
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOB_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOBS
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
                '        End If
                'End Select
                StrQ &= UserAccessLevel._AccessLevelFilterString
                'S.SANDEEP [ 01 JUNE 2012 ] -- END

                
                'S.SANDEEP [ 18 FEB 2012 ] -- END

                Dim dsEmployee As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dtRow As DataRow = Nothing
                For Each dRow As DataRow In dsList.Tables("List").Rows
                    dtRow = mdTran.NewRow
                    dtRow.Item("prioritymasterunkid") = dRow.Item("prioritymasterunkid")
                    dtRow.Item("coursepriotranunkid") = dRow.Item("coursepriotranunkid")
                    dtRow.Item("courseunkid") = dRow.Item("courseunkid")
                    dtRow.Item("training_need") = dRow.Item("course")
                    dtRow.Item("total_emp") = dRow.Item("total_emp")
                    dtRow.Item("IsGrp") = True
                    dtRow.Item("GrpId") = dRow.Item("courseunkid")
                    dtRow.Item("gappriority") = dRow.Item("gappriorityId")
                    dtRow.Item("skill_gap") = dRow.Item("gappriority")
                    dtRow.Item("bussinesspriority") = dRow.Item("bussinesspriorityId")
                    dtRow.Item("business_impact") = dRow.Item("bussinesspriority")
                    dtRow.Item("kra") = dRow.Item("kra")
                    dtRow.Item("priority_ranking") = dRow.Item("priority_ranking")
                    dtRow.Item("providerunkid") = dRow.Item("providerunkid")
                    dtRow.Item("pref_provider") = dRow.Item("institute_name")
                    dtRow.Item("training_venue") = dRow.Item("training_venue")
                    dtRow.Item("training_sol") = dRow.Item("training_solution")
                    dtRow.Item("training_objective") = dRow.Item("training_objective")
                    dtRow.Item("goalunkid") = dRow.Item("goalunkid")
                    dtRow.Item("strategic_goal") = dRow.Item("strategicgoal")
                    mdTran.Rows.Add(dtRow)

                    Dim dFilter As DataTable = New DataView(dsEmployee.Tables("List"), "courseunkid = '" & dRow.Item("courseunkid") & "'", "", DataViewRowState.CurrentRows).ToTable
                    Dim strTargetGrp As String = ""
                    If dFilter IsNot Nothing Then
                        For Each dFRow As DataRow In dFilter.Rows
                            dtRow = mdTran.NewRow
                            dtRow.Item("prioritymasterunkid") = dFRow.Item("prioritymasterunkid")
                            dtRow.Item("courseemptranunkid") = dFRow.Item("courseemptranunkid")
                            dtRow.Item("coursepriotranunkid") = dRow.Item("coursepriotranunkid")
                            dtRow.Item("courseunkid") = dRow.Item("courseunkid")
                            dtRow.Item("training_need") = Space(5) & dFRow.Item("EName")
                            dtRow.Item("total_emp") = ""
                            dtRow.Item("IsGrp") = False
                            dtRow.Item("GrpId") = dRow.Item("courseunkid")
                            dtRow.Item("employeeunkid") = dFRow.Item("employeeunkid")
                            dtRow.Item("BranchId") = dFRow.Item("branchId")
                            dtRow.Item("DepartmentId") = dFRow.Item("departmentId")
                            dtRow.Item("SectionGrpId") = dFRow.Item("sectiongroupId")
                            dtRow.Item("SectionId") = dFRow.Item("sectionId")
                            dtRow.Item("UnitGrpId") = dFRow.Item("unitgroupId")
                            dtRow.Item("UnitId") = dFRow.Item("unitId")
                            dtRow.Item("JobId") = dFRow.Item("jobId")
                            dtRow.Item("CostcenterId") = dFRow.Item("costcenterId")
                            dtRow.Item("TeamId") = dFRow.Item("teamId")

                            If strTargetGrp.Contains(dFRow.Item("JobGroup").ToString()) = False Then
                                strTargetGrp &= "," & dFRow.Item("JobGroup").ToString()
                            End If

                            mdTran.Rows.Add(dtRow)
                        Next
                    End If

                    If strTargetGrp.Length > 0 Then strTargetGrp = Mid(strTargetGrp, 2)
                    Dim drRow() As DataRow = mdTran.Select("courseunkid = '" & dRow.Item("courseunkid") & "' AND IsGrp = true")
                    If drRow.Length > 0 Then
                        drRow(0)("target_group") = strTargetGrp.ToString()
                    End If

                Next

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure: GetTraingNeedAnalysis; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnaemp_course_tran) </purpose>
    Public Function InsertUpdate_Delete(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            If mdTran Is Nothing Then Return True

            For i = 0 To mdTran.Rows.Count - 1
                With mdTran.Rows(i)
                    If CBool(.Item("IsGrp")) = True Then
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("courseunkid").ToString())
                        objDataOperation.AddParameter("@gappriority", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("gappriority").ToString())
                        objDataOperation.AddParameter("@bussinesspriority", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("bussinesspriority").ToString())
                        objDataOperation.AddParameter("@kra", SqlDbType.NVarChar, 4000, .Item("kra").ToString())
                        If .Item("priority_ranking").ToString().Trim.Length > 0 Then
                            objDataOperation.AddParameter("@priority_ranking", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("priority_ranking").ToString())
                        Else
                            objDataOperation.AddParameter("@priority_ranking", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                        End If
                        objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("providerunkid").ToString())
                        objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("training_venue").ToString())
                        objDataOperation.AddParameter("@training_objective", SqlDbType.NVarChar, 4000, .Item("training_objective").ToString())
                        objDataOperation.AddParameter("@training_solution", SqlDbType.NVarChar, 4000, .Item("training_sol").ToString())
                        objDataOperation.AddParameter("@goalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("goalunkid").ToString())
                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVoid.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserUnkid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoidDate <> Nothing, mdtVoidDate, DBNull.Value))
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)

                        If IsExist(Convert.ToInt32(.Item("courseunkid")), Convert.ToInt32(.Item("prioritymasterunkid")), objDataOperation) = False Then
                            objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnAPriorityUnkid.ToString)

                            strQ = " INSERT INTO hrtnacoursepriority_tran ( " & _
                                              " prioritymasterunkid " & _
                                              ", courseunkid  " & _
                                              ", gappriority  " & _
                                              ", bussinesspriority " & _
                                              ", kra " & _
                                              ", priority_ranking " & _
                                              ", providerunkid " & _
                                              ", training_venue " & _
                                              ", training_objective " & _
                                              ", training_solution " & _
                                              ", goalunkid " & _
                                              ",userunkid  " & _
                                              ",isvoid " & _
                                              ",voiduserunkid " & _
                                              ",voiddatetime  " & _
                                              ", voidreason " & _
                                      ") VALUES (" & _
                                              "  @prioritymasterunkid " & _
                                              ", @courseunkid  " & _
                                              ", @gappriority  " & _
                                              ", @bussinesspriority " & _
                                              ", @kra " & _
                                              ", @priority_ranking " & _
                                              ", @providerunkid " & _
                                              ", @training_venue " & _
                                              ", @training_objective " & _
                                              ", @training_solution " & _
                                              ", @goalunkid " & _
                                              ", @userunkid  " & _
                                              ", @isvoid " & _
                                              ", @voiduserunkid " & _
                                              ", @voiddatetime  " & _
                                              ", @voidreason " & _
                                    "); SELECT @@identity"

                            dsList = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mintcoursepriotranunkid = dsList.Tables(0).Rows(0).Item(0)

                            If .Item("coursepriotranunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", .Item("prioritymasterunkid"), "hrtnacoursepriority_tran", "coursepriotranunkid", mintcoursepriotranunkid, 2, 1) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Else
                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", mintTnAPriorityUnkid, "hrtnacoursepriority_tran", "coursepriotranunkid", mintcoursepriotranunkid, 1, 1) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            End If
                        Else

                            objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("prioritymasterunkid")))
                            objDataOperation.AddParameter("@coursepriotranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("coursepriotranunkid")))


                            If .Item("AUD").ToString() = "D" Then

                                strQ = "UPDATE hrtnacoursepriority_tran SET " & _
                                                " isvoid = @isvoid" & _
                                                ", voiduserunkid = @voiduserunkid" & _
                                                ", voiddatetime = @voiddatetime" & _
                                                ", voidreason = @voidreason " & _
                                              "WHERE coursepriotranunkid = @coursepriotranunkid AND prioritymasterunkid = @prioritymasterunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("prioritymasterunkid")))
                                objDataOperation.AddParameter("@coursepriotranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("coursepriotranunkid")))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                                If .Item("voidreason").ToString.Trim.Length > 0 Then
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                Else
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)
                                End If

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", .Item("prioritymasterunkid"), "hrtnacoursepriority_tran", "coursepriotranunkid", .Item("coursepriotranunkid"), 2, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                            Else

                                strQ = "UPDATE hrtnacoursepriority_tran SET " & _
                                                "  prioritymasterunkid = @prioritymasterunkid" & _
                                                ", courseunkid = @courseunkid" & _
                                                ", gappriority = @gappriority" & _
                                                ", bussinesspriority = @bussinesspriority" & _
                                                ", kra = @kra" & _
                                                ", priority_ranking = @priority_ranking" & _
                                                ", providerunkid = @providerunkid" & _
                                                ", training_venue = @training_venue" & _
                                                ", training_objective = @training_objective" & _
                                                ", training_solution = @training_solution" & _
                                                ", goalunkid = @goalunkid" & _
                                                ", userunkid = @userunkid" & _
                                                ", isvoid = @isvoid" & _
                                                ", voiduserunkid = @voiduserunkid" & _
                                                ", voiddatetime = @voiddatetime" & _
                                                ", voidreason = @voidreason " & _
                                              "WHERE coursepriotranunkid = @coursepriotranunkid "

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", .Item("prioritymasterunkid"), "hrtnacoursepriority_tran", "coursepriotranunkid", .Item("coursepriotranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                            End If

                        End If

                        Dim dTemp() As DataRow = mdTran.Select("IsGrp=false AND GrpId = '" & .Item("courseunkid") & "'")

                        If dTemp.Length > 0 Then
                            Dim objEmpCourse As New clshrtnaemp_course_Tran
                            objEmpCourse._Prioritymasterunkid = mintTnAPriorityUnkid
                            If CInt(.Item("coursepriotranunkid")) <= 0 Then
                                objEmpCourse._Coursepriotranunkid = mintcoursepriotranunkid
                            Else
                                objEmpCourse._Coursepriotranunkid = CInt(.Item("coursepriotranunkid"))
                            End If
                            objEmpCourse._Userunkid = User._Object._Userunkid
                            With objEmpCourse
                                ._FormName = mstrFormName
                                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With
                            If objEmpCourse.InsertUpdate_Delete(objDataOperation, dTemp) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If

                    End If
                End With
            Next

            'For i = 0 To mdTran.Rows.Count - 1

            '    With mdTran.Rows(i)
            '        objDataOperation.ClearParameters()
            '        If Not IsDBNull(.Item("AUD")) Then

            '            Select Case .Item("AUD")

            '                Case "A"

            '                    objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("courseunkid").ToString())
            '                    objDataOperation.AddParameter("@gappriority", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("gappriority").ToString())
            '                    objDataOperation.AddParameter("@bussinesspriority", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("bussinesspriority").ToString())
            '                    objDataOperation.AddParameter("@kra", SqlDbType.NVarChar, 4000, .Item("kra").ToString())
            '                    objDataOperation.AddParameter("@priority_ranking", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("priority_ranking").ToString())
            '                    objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("providerunkid").ToString())
            '                    objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("training_venue").ToString())
            '                    objDataOperation.AddParameter("@training_objective", SqlDbType.NVarChar, 4000, .Item("training_objective").ToString())
            '                    objDataOperation.AddParameter("@training_solution", SqlDbType.NVarChar, 4000, .Item("training_solution").ToString())
            '                    objDataOperation.AddParameter("@goalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("goalunkid").ToString())
            '                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            '                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVoid.ToString)
            '                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserUnkid.ToString)
            '                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoidDate <> Nothing, mdtVoidDate, DBNull.Value))
            '                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)

            '                    If IsExist(CInt(.Item("courseunkid").ToString()), CInt(.Item("prioritymasterunkid").ToString()), objDataOperation) = False Then

            '                        objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnAPriorityUnkid.ToString)

            '                        strQ = " INSERT INTO hrtnacoursepriority_tran ( " & _
            '                                  " prioritymasterunkid " & _
            '                                  ", courseunkid  " & _
            '                                  ", gappriority  " & _
            '                                  ", bussinesspriority " & _
            '                                  ", kra " & _
            '                                  ", priority_ranking " & _
            '                                  ", providerunkid " & _
            '                                  ", training_venue " & _
            '                                  ", training_objective " & _
            '                                  ", training_solution " & _
            '                                  ", goalunkid " & _
            '                                  ",userunkid  " & _
            '                                  ",isvoid " & _
            '                                  ",voiduserunkid " & _
            '                                  ",voiddatetime  " & _
            '                                  ", voidreason " & _
            '                          ") VALUES (" & _
            '                                  "  @prioritymasterunkid " & _
            '                                  ", @courseunkid  " & _
            '                                  ", @gappriority  " & _
            '                                  ", @bussinesspriority " & _
            '                                  ", @kra " & _
            '                                  ", @priority_ranking " & _
            '                                  ", @providerunkid " & _
            '                                  ", @training_venue " & _
            '                                  ", @training_objective " & _
            '                                  ", @training_solution " & _
            '                                  ", @goalunkid " & _
            '                                  ", @userunkid  " & _
            '                                  ", @isvoid " & _
            '                                  ", @voiduserunkid " & _
            '                                  ", @voiddatetime  " & _
            '                                  ", @voidreason " & _
            '                        "); SELECT @@identity"

            '                        dsList = objDataOperation.ExecQuery(strQ, "List")

            '                        If objDataOperation.ErrorMessage <> "" Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If

            '                        Dim mintcoursepriotranunkid = dsList.Tables(0).Rows(0).Item(0)

            '                        If .Item("coursepriotranunkid") > 0 Then
            '                            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", .Item("prioritymasterunkid"), "hrtnacoursepriority_tran", "coursepriotranunkid", mintcoursepriotranunkid, 2, 1) = False Then
            '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                                Throw exForce
            '                            End If
            '                        Else
            '                            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", mintTnAPriorityUnkid, "hrtnacoursepriority_tran", "coursepriotranunkid", mintcoursepriotranunkid, 1, 1) = False Then
            '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                                Throw exForce
            '                            End If
            '                        End If

            '                        Dim objEmpCourse As New clshrtnaemp_course_Tran
            '                        If objEmpCourse.InsertUpdate_Delete(objDataOperation, mdTran) = False Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If
            '                    Else
            '                        objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("prioritymasterunkid")))

            '                        strQ = "UPDATE hrtnacoursepriority_tran SET " & _
            '                                "  prioritymasterunkid = @prioritymasterunkid" & _
            '                                ", courseunkid = @courseunkid" & _
            '                                ", gappriority = @gappriority" & _
            '                                ", bussinesspriority = @bussinesspriority" & _
            '                                ", kra = @kra" & _
            '                                ", priority_ranking = @priority_ranking" & _
            '                                ", providerunkid = @providerunkid" & _
            '                                ", training_venue = @training_venue" & _
            '                                ", training_objective = @training_objective" & _
            '                                ", training_solution = @training_solution" & _
            '                                ", goalunkid = @goalunkid" & _
            '                                ", userunkid = @userunkid" & _
            '                                ", isvoid = @isvoid" & _
            '                                ", voiduserunkid = @voiduserunkid" & _
            '                                ", voiddatetime = @voiddatetime" & _
            '                                ", voidreason = @voidreason " & _
            '                              "WHERE coursepriotranunkid = @coursepriotranunkid "

            '                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", .Item("prioritymasterunkid"), "hrtnacoursepriority_tran", "coursepriotranunkid", .Item("coursepriotranunkid"), 2, 2) = False Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If


            '                    End If

            '                Case "D"

            '                    objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnAPriorityUnkid)
            '                    objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("courseunkid").ToString())
            '                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString())
            '                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVoid.ToString)
            '                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserUnkid.ToString)
            '                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoidDate <> Nothing, mdtVoidDate, DBNull.Value))
            '                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)

            '                    strQ = " UPDATE hrtnacoursepriority_tran SET " & _
            '                              "  isvoid  = @isvoid" & _
            '                              ", voiduserunkid = @voiduserunkid" & _
            '                              ", voiddatetime = @voiddatetime " & _
            '                              ", voidreason = @voidreason " & _
            '                             " WHERE courseunkid = @courseunkid AND prioritymasterunkid = @prioritymasterunkid "

            '                    dsList = objDataOperation.ExecQuery(strQ, "List")

            '                    If objDataOperation.ErrorMessage <> "" Then
            '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                        Throw exForce
            '                    End If

            '                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", mintTnAPriorityUnkid, "hrtnacoursepriority_tran", "coursepriotranunkid", .Item("coursepriotranunkid").ToString, 2, 3) = False Then
            '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                        Throw exForce
            '                    End If

            '            End Select

            '        End If

            '    End With

            'Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsExist(ByVal intCourseUnkid As Integer, ByVal intPriorityUnkid As Integer, ByVal objDataoperation As clsDataOperation) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = ""
        Dim iCnt As Integer = -1
        Try
            StrQ = "SELECT * FROM hrtnacoursepriority_tran WHERE courseunkid = " & intCourseUnkid & " AND prioritymasterunkid = " & intPriorityUnkid & " "

            'iCnt = objDataoperation.RecordCo(StrQ)

            'If objDataoperation.ErrorMessage <> "" Then
            '    Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
            'End If

            'If iCnt > 0 Then
            '    blnFlag = True
            'Else
            '    blnFlag = False
            'End If

            Dim dsList As New DataSet

            dsList = objDataoperation.ExecQuery(StrQ, "List")


            If objDataoperation.ErrorMessage <> "" Then
                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure IsExist: Insert; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "KEY RESULT AREAS (KRA)")
			Language.setMessage(mstrModuleName, 2, "TRAINING NEEDS")
			Language.setMessage(mstrModuleName, 3, "SKILL GAP")
			Language.setMessage(mstrModuleName, 4, "IMPACT ON BUSINESS")
			Language.setMessage(mstrModuleName, 5, "PRIORITY RANKING")
			Language.setMessage(mstrModuleName, 6, "TARGET GROUP")
			Language.setMessage(mstrModuleName, 7, "TRAINING SOLUTIONS")
			Language.setMessage(mstrModuleName, 8, "TRAINING OBJECTIVE")
			Language.setMessage(mstrModuleName, 9, "NUMBER OF PEOPLE")
			Language.setMessage(mstrModuleName, 10, "PREFERRED PROVIDER")
			Language.setMessage(mstrModuleName, 11, "TRAINING VENUE")
			Language.setMessage(mstrModuleName, 12, "STRATEGIC GOAL")
			Language.setMessage("clsMasterData", 331, "Low")
			Language.setMessage("clsMasterData", 332, "Medium")
			Language.setMessage("clsMasterData", 333, "High")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
