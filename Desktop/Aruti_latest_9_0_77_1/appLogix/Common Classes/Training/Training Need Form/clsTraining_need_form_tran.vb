﻿'************************************************************************************************************************************
'Class Name : clsTraining_need_form_tran.vb
'Purpose    :
'Date       :22-11-2019
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsTraining_need_form_tran
    Private Const mstrModuleName = "clsTraining_need_form_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private xDataOp As clsDataOperation
    Private mintTrainingneedformtranunkid As Integer
    Private mintTrainingneedformunkid As Integer
    Private mintPeriodunkid As Integer
    Private mdtStart_Date As Date
    Private mdtEnd_Date As Date
    Private mintTrainingcourseunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mintPriority As Integer
    Private mblnIsperformance_Training As Boolean
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingneedformtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trainingneedformtranunkid() As Integer
        Get
            Return mintTrainingneedformtranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingneedformtranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingneedformunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trainingneedformunkid() As Integer
        Get
            Return mintTrainingneedformunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingneedformunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingcourseunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trainingcourseunkid() As Integer
        Get
            Return mintTrainingcourseunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingcourseunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isperformance_training
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isperformance_Training() As Boolean
        Get
            Return mblnIsperformance_Training
        End Get
        Set(ByVal value As Boolean)
            mblnIsperformance_Training = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property




    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "SELECT " & _
              "  trainingneedformtranunkid " & _
              ", trainingneedformunkid " & _
              ", periodunkid " & _
              ", start_date " & _
              ", end_date " & _
              ", trainingcourseunkid " & _
              ", description " & _
              ", priority " & _
              ", isperformance_training " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtraining_need_form_tran " & _
             "WHERE trainingneedformtranunkid = @trainingneedformtranunkid "

            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintTrainingneedformTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                minttrainingneedformtranunkid = CInt(dtRow.Item("trainingneedformtranunkid"))
                minttrainingneedformunkid = CInt(dtRow.Item("trainingneedformunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mdtstart_date = dtRow.Item("start_date")
                mdtend_date = dtRow.Item("end_date")
                minttrainingcourseunkid = CInt(dtRow.Item("trainingcourseunkid"))
                mstrdescription = dtRow.Item("description").ToString
                mintpriority = CInt(dtRow.Item("priority"))
                mblnisperformance_training = CBool(dtRow.Item("isperformance_training"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal intTrainingneedformunkid As Integer = 0 _
                            , Optional ByVal strFilter As String = "" _
                            , Optional ByVal blnAddGrouping As Boolean = False _
                            ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim ds As DataSet = (New clsMasterData).GetPriority(True)

            strQ = "SELECT    hrtraining_need_form_emp_tran.trainingneedformtranunkid " & _
                      ", COUNT(hrtraining_need_form_emp_tran.trainingneedformtranunkid) AS TotalEmployee  " & _
                    "INTO #TotEmp " & _
                    "FROM hrtraining_need_form_emp_tran " & _
                    "LEFT JOIN hrtraining_need_form_tran ON hrtraining_need_form_tran.trainingneedformtranunkid = hrtraining_need_form_emp_tran.trainingneedformtranunkid " & _
                    "WHERE hrtraining_need_form_emp_tran.isvoid = 0 "

            If intTrainingneedformunkid > 0 Then
                strQ &= " AND hrtraining_need_form_tran.trainingneedformunkid = @trainingneedformunkid "
                objDataOperation.AddParameter("@trainingneedformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedformunkid)
            End If

            strQ &= "GROUP BY hrtraining_need_form_emp_tran.trainingneedformtranunkid "

            strQ &= "SELECT " & _
                        "  hrtraining_need_form_tran.trainingneedformtranunkid " & _
                        ", hrtraining_need_form_tran.trainingneedformunkid " & _
                        ", ISNULL(hrtraining_need_form_master.formno, '') AS formno " & _
                        ", hrtraining_need_form_tran.periodunkid " & _
                        ", cfcommon_period_tran.period_code " & _
                        ", cfcommon_period_tran.period_name " & _
                        ", hrtraining_need_form_tran.start_date " & _
                        ", hrtraining_need_form_tran.end_date " & _
                        ", hrtraining_need_form_tran.trainingcourseunkid " & _
                        ", ISNULL(cfcommon_master.name, '') AS trainingcoursename " & _
                        ", hrtraining_need_form_tran.description " & _
                        ", hrtraining_need_form_tran.priority " & _
                        ", hrtraining_need_form_tran.isperformance_training " & _
                        ", hrtraining_need_form_tran.userunkid " & _
                        ", hrtraining_need_form_tran.loginemployeeunkid " & _
                        ", hrtraining_need_form_tran.isweb " & _
                        ", hrtraining_need_form_tran.isvoid " & _
                        ", hrtraining_need_form_tran.voiduserunkid " & _
                        ", hrtraining_need_form_tran.voidloginemployeeunkid " & _
                        ", hrtraining_need_form_tran.voiddatetime " & _
                        ", hrtraining_need_form_tran.voidreason " & _
                        ", ISNULL(#TotEmp.TotalEmployee, 0) AS TotalEmployee " & _
                        ", CAST(0 AS BIT) AS IsGrp "

            strQ &= ", CASE hrtraining_need_form_tran.priority "
            For Each row As DataRow In ds.Tables(0).Rows
                strQ &= " WHEN " & CInt(row.Item("Id")) & "  THEN '" & row.Item("Name").ToString & "' "
            Next
            strQ &= " END AS priorityname "

            strQ &= "FROM hrtraining_need_form_tran " & _
                         " LEFT JOIN hrtraining_need_form_master ON hrtraining_need_form_master.trainingneedformunkid = hrtraining_need_form_tran.trainingneedformunkid  " & _
                         " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrtraining_need_form_tran.periodunkid " & _
                         " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_need_form_tran.trainingcourseunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                         " LEFT JOIN #TotEmp ON #TotEmp.trainingneedformtranunkid = hrtraining_need_form_tran.trainingneedformtranunkid " & _
                     "WHERE hrtraining_need_form_tran.isvoid = 0 " & _
                         " AND hrtraining_need_form_master.isvoid = 0 " & _
                         " AND cfcommon_period_tran.isactive = 1 " & _
                         " AND modulerefid = " & enModuleReference.Assessment & " "

            If intTrainingneedformunkid > 0 Then
                strQ &= " AND hrtraining_need_form_tran.trainingneedformunkid = @trainingneedformunkid "
                objDataOperation.AddParameter("@trainingneedformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedformunkid)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            strQ &= " DROP TABLE #TotEmp "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If blnAddGrouping = True Then

                Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "formno", "trainingneedformunkid")
                Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dtCol = New DataColumn("IsGrp", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = True
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dt.Merge(dsList.Tables(0), False)
                dt.DefaultView.Sort = "formno, trainingneedformunkid, trainingcoursename, IsGrp DESC"

                dsList.Tables.Clear()
                dsList.Tables.Add(dt.DefaultView.ToTable)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_need_form_tran) </purpose>
    Public Function Insert(ByVal intTrainingNeedFormNoType As Integer, ByVal strTrainingNeedFormNoPrefix As String, Optional ByVal objTNFMaster As clsTraining_need_form_master = Nothing, Optional ByVal mdtEmployee As DataTable = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            If mintTrainingneedformunkid <= 0 AndAlso objTNFMaster IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                objTNFMaster._xDataOp = objDataOperation
                objTNFMaster._CompanyUnkid = mintCompanyUnkid
                If objTNFMaster.Insert(intTrainingNeedFormNoType, strTrainingNeedFormNoPrefix, intNewUnkId) = False Then
                    Return False
                Else
                    mintTrainingneedformunkid = intNewUnkId
                End If
            ElseIf mintTrainingneedformunkid > 0 AndAlso objTNFMaster IsNot Nothing Then
                objTNFMaster._xDataOp = objDataOperation
                If objTNFMaster.Update() = False Then
                    Return False
                End If
            End If

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@trainingneedformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@isperformance_training", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsperformance_Training.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hrtraining_need_form_tran ( " & _
              "  trainingneedformunkid " & _
              ", periodunkid " & _
              ", start_date " & _
              ", end_date " & _
              ", trainingcourseunkid " & _
              ", description " & _
              ", priority " & _
              ", isperformance_training " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @trainingneedformunkid " & _
              ", @periodunkid " & _
              ", @start_date " & _
              ", @end_date " & _
              ", @trainingcourseunkid " & _
              ", @description " & _
              ", @priority " & _
              ", @isperformance_training " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingneedformtranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If mdtEmployee IsNot Nothing Then
                Dim objTNFEmp As New clsTraining_need_form_emp_tran
                objTNFEmp._Trainingneedformtranunkid = mintTrainingneedformtranunkid
                objTNFEmp._Userunkid = mintUserunkid
                objTNFEmp._Isvoid = mblnIsvoid
                objTNFEmp._Voiduserunkid = mintVoiduserunkid
                objTNFEmp._Voiddatetime = mdtVoiddatetime
                objTNFEmp._Voidreason = mstrVoidreason
                objTNFEmp._Loginemployeeunkid = mintLoginemployeeunkid
                objTNFEmp._Isweb = mblnIsweb

                objTNFEmp._AuditDate = mdtAuditDate
                objTNFEmp._FormName = mstrFormName
                objTNFEmp._HostName = mstrHostName
                objTNFEmp._ClientIP = mstrClientIP
                objTNFEmp._CompanyUnkid = mintCompanyUnkid

                objTNFEmp._xDataOp = objDataOperation
                If objTNFEmp.InsertAll(mdtEmployee) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_need_form_tran) </purpose>
    Public Function Update(Optional ByVal objTNFMaster As clsTraining_need_form_master = Nothing, Optional ByVal mdtEmployee As DataTable = Nothing) As Boolean
        'If isExist(mstrName, mintTrainingneedformtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid.ToString)
            objDataOperation.AddParameter("@trainingneedformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@isperformance_training", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsperformance_Training.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hrtraining_need_form_tran SET " & _
              "  trainingneedformunkid = @trainingneedformunkid" & _
              ", periodunkid = @periodunkid" & _
              ", start_date = @start_date" & _
              ", end_date = @end_date" & _
              ", trainingcourseunkid = @trainingcourseunkid" & _
              ", description = @description" & _
              ", priority = @priority" & _
              ", isperformance_training = @isperformance_training" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE trainingneedformtranunkid = @trainingneedformtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If mdtEmployee IsNot Nothing Then
                Dim objTNFEmp As New clsTraining_need_form_emp_tran
                objTNFEmp._Trainingneedformtranunkid = mintTrainingneedformtranunkid
                objTNFEmp._Userunkid = mintUserunkid
                objTNFEmp._Isvoid = mblnIsvoid
                objTNFEmp._Voiduserunkid = mintVoiduserunkid
                objTNFEmp._Voiddatetime = mdtVoiddatetime
                objTNFEmp._Voidreason = mstrVoidreason
                objTNFEmp._Loginemployeeunkid = mintLoginemployeeunkid
                objTNFEmp._Isweb = mblnIsweb

                objTNFEmp._AuditDate = mdtAuditDate
                objTNFEmp._FormName = mstrFormName
                objTNFEmp._HostName = mstrHostName
                objTNFEmp._ClientIP = mstrClientIP
                objTNFEmp._CompanyUnkid = mintCompanyUnkid

                objTNFEmp._xDataOp = objDataOperation
                If objTNFEmp.InsertAll(mdtEmployee) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_need_form_tran) </purpose>
    Public Function Delete() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnxDataOp As Boolean = False

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
            blnxDataOp = True
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hrtraining_need_form_tran SET " & _
                     " isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid " & _
                     ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
                   "WHERE trainingneedformtranunkid = @trainingneedformtranunkid "

            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._Trainingneedformtranunkid = mintTrainingneedformtranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Dim objTNFEmp As New clsTraining_need_form_emp_tran
            objTNFEmp._xDataOp = objDataOperation
            objTNFEmp._Trainingneedformtranunkid = mintTrainingneedformtranunkid
            objTNFEmp._Isvoid = True
            objTNFEmp._Voiduserunkid = mintVoiduserunkid
            objTNFEmp._Voidloginemployeeunkid = mintVoidloginemployeeunkid
            objTNFEmp._Voiddatetime = mdtVoiddatetime
            objTNFEmp._Voidreason = mstrVoidreason
            objTNFEmp._Isweb = mblnIsweb

            objTNFEmp._HostName = mstrHostName
            objTNFEmp._ClientIP = mstrClientIP
            objTNFEmp._AuditDate = mdtAuditDate
            objTNFEmp._AuditUserId = mintUserunkid
            objTNFEmp._FormName = mstrFormName

            objTNFEmp._xDataOp = objDataOperation
            If objTNFEmp.DeleteByTrainingNeedFormTranUnkId() = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Dim objTNFCost As New clsTraining_need_form_cost_tran
            objTNFCost._xDataOp = objDataOperation
            objTNFCost._Trainingneedformtranunkid = mintTrainingneedformtranunkid
            objTNFCost._Isvoid = True
            objTNFCost._Voiduserunkid = mintVoiduserunkid
            objTNFCost._Voidloginemployeeunkid = mintVoidloginemployeeunkid
            objTNFCost._Voiddatetime = mdtVoiddatetime
            objTNFCost._Voidreason = mstrVoidreason
            objTNFCost._Isweb = mblnIsweb

            objTNFCost._HostName = mstrHostName
            objTNFCost._ClientIP = mstrClientIP
            objTNFCost._AuditDate = mdtAuditDate
            objTNFCost._AuditUserId = mintUserunkid
            objTNFCost._FormName = mstrFormName

            objTNFCost._xDataOp = objDataOperation
            If objTNFCost.DeleteByTrainingNeedFormTranUnkId() = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Dim objDocument As New clsScan_Attach_Documents
            If objDocument.DeleteTransacation(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingneedformtranunkid, objDataOperation, mstrFormName) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If blnxDataOp = False Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If blnxDataOp = False Then objDataOperation = Nothing
        End Try
    End Function

    Public Function DeleteByTrainingNeedFormUnkId() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnxDataOp As Boolean = False

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
            blnxDataOp = True
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                 " hrtraining_need_form_tran.trainingneedformtranunkid " & _
            "FROM hrtraining_need_form_tran " & _
            "WHERE hrtraining_need_form_tran.isvoid = 0 " & _
            "AND hrtraining_need_form_tran.trainingneedformunkid = @trainingneedformunkid "

            objDataOperation.AddParameter("@trainingneedformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dsRow As DataRow In dsList.Tables(0).Rows

                mintTrainingneedformtranunkid = CInt(dsRow.Item("trainingneedformtranunkid"))

                Me._xDataOp = objDataOperation
                If Delete() = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Next

            If blnxDataOp = False Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteByTrainingNeedFormUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If blnxDataOp = False Then objDataOperation = Nothing
        End Try
    End Function
    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  trainingneedformtranunkid " & _
              ", trainingneedformunkid " & _
              ", periodunkid " & _
              ", start_date " & _
              ", end_date " & _
              ", trainingcourseunkid " & _
              ", description " & _
              ", priority " & _
              ", isperformance_training " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtraining_need_form_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND trainingneedformtranunkid <> @trainingneedformtranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid.ToString)
            objDataOperation.AddParameter("@trainingneedformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@isperformance_training", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsperformance_Training.ToString)

            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO athrtraining_need_form_tran ( " & _
                            "  trainingneedformtranunkid " & _
                            ", trainingneedformunkid " & _
                            ", periodunkid " & _
                            ", start_date " & _
                            ", end_date " & _
                            ", trainingcourseunkid " & _
                            ", description " & _
                            ", priority " & _
                            ", isperformance_training " & _
                            ", audituserunkid " & _
                            ", loginemployeeunkid " & _
                            ", audittype " & _
                            ", auditdatetime " & _
                            ", isweb " & _
                            ", ip " & _
                            ", host " & _
                            ", form_name " & _
                  ") VALUES (" & _
                            "  @trainingneedformtranunkid " & _
                            ", @trainingneedformunkid " & _
                            ", @periodunkid " & _
                            ", @start_date " & _
                            ", @end_date " & _
                            ", @trainingcourseunkid " & _
                            ", @description " & _
                            ", @priority " & _
                            ", @isperformance_training " & _
                            ", @audituserunkid " & _
                            ", @loginemployeeunkid " & _
                            ", @audittype " & _
                            ", @auditdatetime " & _
                            ", @isweb " & _
                            ", @ip " & _
                            ", @host " & _
                            ", @form_name " & _
                  "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class