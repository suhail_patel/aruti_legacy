﻿Imports eZeeCommonLib

Public Class clsTraining_Attendance_Tran

#Region "Private Variable"
    Private objDataOperation As clsDataOperation
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Attendance_Tran"
    Private mstrMessage As String = ""
    Private mintAttendanceUnkid As Integer = -1
    Private objTrainingAttendanceMaster As clsTraining_Attendance_Master
    Private mdtTran As DataTable
    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    Private strOldValue As String = String.Empty
    'S.SANDEEP [ 12 OCT 2011 ] -- END 
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("AttendanceTran")
        Dim dCol As New DataColumn
        Try
            dCol = New DataColumn("attendancetranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("attendanceunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ispresent")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isabsent")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("GUID")
            'dCol.DataType = System.Type.GetType("System.String")
            'mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("AUD")
            'dCol.DataType = System.Type.GetType("System.String")
            'dCol.AllowDBNull = True
            'dCol.DefaultValue = DBNull.Value
            'mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public Property _AttendanceUnkid() As Integer
        Get
            Return mintAttendanceUnkid
        End Get
        Set(ByVal value As Integer)
            mintAttendanceUnkid = value
            Call getAttendanceTran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

    Private Sub getAttendanceTran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim dtRow As DataRow
        '  Dim drawTranHeadInfo As DataRow

        Try

            objDataOperation = New clsDataOperation

            strQ = "SELECT attendancetranunkid " & _
                    ",attendanceunkid " & _
                    ",employeeunkid " & _
                    ",ispresent " & _
                    ",isabsent " & _
                    ",remark " & _
                    ",isvoid " & _
                    ",voiduserunkid " & _
                    ",voiddatetime " & _
                    "FROM hrtraining_attendance_tran " & _
                    "WHERE hrtraining_attendance_tran.attendanceunkid = @attendanceunkid " & _
                    "AND ISNULL(isvoid,0) = 0 "

            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttendanceUnkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dtRow = mdtTran.NewRow
                With dtRow
                    .Item("attendancetranunkid") = CInt(dsRow.Item("attendancetranunkid").ToString)
                    .Item("attendanceunkid") = CInt(dsRow.Item("attendanceunkid").ToString)
                    .Item("employeeunkid") = CInt(dsRow.Item("employeeunkid").ToString)
                    .Item("ispresent") = CBool(dsRow.Item("ispresent").ToString)
                    .Item("isabsent") = CBool(dsRow.Item("isabsent").ToString)
                    .Item("remark") = dsRow.Item("remark").ToString
                    .Item("isvoid") = CBool(dsRow.Item("isvoid").ToString)
                    .Item("voiduserunkid") = dsRow.Item("voiduserunkid")

                    If IsDBNull(.Item("voiddatetime")) Then
                        dsRow.Item("voiddatetime") = DBNull.Value
                    Else
                        .Item("voiddatetime") = dsRow.Item("voiddatetime")
                    End If


                    mdtTran.Rows.Add(dtRow)
                End With
            Next

            'For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
            '    With dsList.Tables("List").Rows(i)
            '        dtRow = mdtTran.NewRow()

            '        dtRow.Item("attendancetranunkid") = .Item("attendancetranunkid")
            '        dtRow.Item("attendanceunkid") = .Item("attendanceunkid")
            '        dtRow.Item("employeeunkid") = .Item("employeeunkid")
            '        dtRow.Item("ispresent") = .Item("ispresent")
            '        dtRow.Item("isabsent") = .Item("isabsent")
            '        dtRow.Item("remark") = .Item("remark")
            '        dtRow.Item("isvoid") = .Item("isvoid")
            '        dtRow.Item("voiduserunkid") = .Item("voiduserunkid")
            '        If IsDBNull(.Item("voiddatetime")) Then
            '            dtRow.Item("voiddatetime") = DBNull.Value
            '        Else
            '            dtRow.Item("voiddatetime") = .Item("voiddatetime")
            '        End If

            '        mdtTran.Rows.Add(dtRow)

            '    End With
            'Next


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            'exForce = Nothing
            'If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Sub

    Public Function InserAttendanceTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()

                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                    If CBool(.Item("ispresent")) = False AndAlso CBool(.Item("isabsent")) = False Then Continue For
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 

                    strQ = "INSERT INTO hrtraining_attendance_tran ( " & _
                        "  attendanceunkid " & _
                        ", employeeunkid " & _
                        ", ispresent " & _
                        ", isabsent " & _
                        ", remark " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                      ") VALUES (" & _
                        "  @attendanceunkid " & _
                        ", @employeeunkid " & _
                        ", @ispresent " & _
                        ", @isabsent " & _
                        ", @remark " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                      "); SELECT @@identity"

                    objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttendanceUnkid.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                    objDataOperation.AddParameter("@ispresent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("ispresent").ToString)
                    objDataOperation.AddParameter("@isabsent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isabsent").ToString)
                    objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                    If IsDBNull(.Item("voiddatetime")) Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                    End If


                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                    'objDataOperation.ExecNonQuery(strQ)
                    Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_attendance_master", "attendanceunkid", mintAttendanceUnkid, "hrtraining_attendance_tran", "attendancetranunkid", dList.Tables(0).Rows(0)(0), 1, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                End With
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InserAttendanceTran", mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            'exForce = Nothing
            'objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateAttendanceTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing

        Try
            objDataOperation = New clsDataOperation

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)

                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                    If CBool(.Item("ispresent")) = False AndAlso CBool(.Item("isabsent")) = False Then Continue For
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 

                    If Row_Insert_On_Edit(mintAttendanceUnkid, mdtTran.Rows(i).Item("employeeunkid")) Then
                        objDataOperation.ClearParameters()

                        strQ = "UPDATE hrtraining_attendance_tran SET " & _
                            "  attendanceunkid = @attendanceunkid" & _
                            ", employeeunkid = @employeeunkid" & _
                            ", ispresent = @ispresent" & _
                            ", isabsent = @isabsent" & _
                            ", remark = @remark" & _
                            ", isvoid = @isvoid" & _
                            ", voiduserunkid = @voiduserunkid" & _
                            ", voiddatetime = @voiddatetime" & _
                            " WHERE attendancetranunkid = @attendancetranunkid "

                        objDataOperation.AddParameter("@attendancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attendancetranunkid").ToString)
                        objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attendanceunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                        objDataOperation.AddParameter("@ispresent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("ispresent").ToString)
                        objDataOperation.AddParameter("@isabsent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isabsent").ToString)
                        objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                        If IsDBNull(.Item("voiddatetime")) Then
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                        End If


                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If strOldValue = "PRESENT" AndAlso CBool(.Item("isabsent")) = True Then
                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            Dim objCommonATLog As New clsCommonATLog
                            objCommonATLog._FormName = mstrFormName
                            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_attendance_master", "attendanceunkid", .Item("attendanceunkid"), "hrtraining_attendance_tran", "attendancetranunkid", .Item("attendancetranunkid"), 2, 2) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END

                        ElseIf strOldValue = "ABSENT" AndAlso CBool(.Item("ispresent")) = True Then
                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            Dim objCommonATLog As New clsCommonATLog
                            objCommonATLog._FormName = mstrFormName
                            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_attendance_master", "attendanceunkid", .Item("attendanceunkid"), "hrtraining_attendance_tran", "attendancetranunkid", .Item("attendancetranunkid"), 2, 2) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END

                        End If


                    Else
                        objDataOperation = New clsDataOperation

                        Try
                            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttendanceUnkid.ToString)
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                            objDataOperation.AddParameter("@ispresent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("ispresent").ToString)
                            objDataOperation.AddParameter("@isabsent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isabsent").ToString)
                            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                            If IsDBNull(.Item("voiddatetime")) Then
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            Else
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                            End If
                            strQ = "INSERT INTO hrtraining_attendance_tran ( " & _
                            "  attendanceunkid " & _
                            ", employeeunkid " & _
                            ", ispresent " & _
                            ", isabsent " & _
                            ", remark " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                          ") VALUES (" & _
                            "  @attendanceunkid " & _
                            ", @employeeunkid " & _
                            ", @ispresent " & _
                            ", @isabsent " & _
                            ", @remark " & _
                            ", @isvoid " & _
                            ", @voiduserunkid " & _
                            ", @voiddatetime " & _
                          "); SELECT @@identity"

                            dsList = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            Dim objCommonATLog As New clsCommonATLog
                            objCommonATLog._FormName = mstrFormName
                            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_attendance_master", "attendanceunkid", mintAttendanceUnkid, "hrtraining_attendance_tran", "attendancetranunkid", dsList.Tables(0).Rows(0)(0), 2, 1) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END


                        Catch ex As Exception
                            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
                            Return False
                        Finally
                        End Try
                    End If

                End With

            Next


           
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateAttendanceTran", mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
        End Try

    End Function

    Public Function Void_All(ByVal isVoid As Boolean, _
                            ByVal dtVoidDate As DateTime, _
                            ByVal intVoidUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_attendance_tran SET " & _
                     "  isvoid = @isvoid " & _
                     ", voiduserunkid = @voiduserunkid " & _
                     ", voiddatetime = @voiddatetime " & _
                     "WHERE attendanceunkid = @attendanceunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttendanceUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
        End Try
    End Function


    Public Function Row_Insert_On_Edit(ByVal intAttendanceId As Integer, ByVal intEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'strQ = "SELECT hrtraining_attendance_master.attendanceunkid, " & _
            '       "hrtraining_attendance_master.trainingschedulingunkid, " & _
            '       "hrtraining_attendance_tran.employeeunkid " & _
            '       "FROM hrtraining_attendance_master " & _
            '       "JOIN hrtraining_attendance_tran ON hrtraining_attendance_master.attendanceunkid = hrtraining_attendance_tran.attendanceunkid " & _
            '       "WHERE hrtraining_attendance_master.attendanceunkid = @attendanceunkid " & _
            '       "AND hrtraining_attendance_tran.employeeunkid = @employeeunkid "

            strQ = "SELECT " & _
                   "    hrtraining_attendance_master.attendanceunkid, " & _
                   "hrtraining_attendance_master.trainingschedulingunkid, " & _
                   "    hrtraining_attendance_tran.employeeunkid, " & _
                   "    hrtraining_attendance_tran.ispresent, " & _
                   "    hrtraining_attendance_tran.isabsent " & _
                   "FROM hrtraining_attendance_master " & _
                   "JOIN hrtraining_attendance_tran ON hrtraining_attendance_master.attendanceunkid = hrtraining_attendance_tran.attendanceunkid " & _
                   "WHERE hrtraining_attendance_master.attendanceunkid = @attendanceunkid " & _
                   "AND hrtraining_attendance_tran.employeeunkid = @employeeunkid "
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAttendanceId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0)("ispresent") = True Then
                    strOldValue = "PRESENT"
                ElseIf dsList.Tables(0).Rows(0)("isabsent") = True Then
                    strOldValue = "ABSENT"
                End If
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Row_Insert_On_Edit; Module Name: " & mstrModuleName)
        Finally
            'exForce = Nothing
            'If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function



End Class
