﻿'************************************************************************************************************************************
'Class Name : clsDepttrainingneed_allocation_Tran.vb
'Purpose    :
'Date       :06-03-2021
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsDepttrainingneed_allocation_Tran
    Private Const mstrModuleName = "clsDepttrainingneed_allocation_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDepttrainingneedallocationtranunkid As Integer
    Private mintDepartmentaltrainingneedunkid As Integer
    Private mintAllocationtranunkid As Integer
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Public Variables "
    Public pintDepttrainingneedallocationtranunkid As Integer
    Public pintDepartmentaltrainingneedunkid As Integer
    Public pintAllocationtranunkid As Integer
    Public pintUserunkid As Integer
    Public pintLoginemployeeunkid As Integer
    Public pblnIsweb As Boolean
    Public pblnIsvoid As Boolean
    Public pintVoiduserunkid As Integer
    Public pintVoidloginemployeeunkid As Integer
    Public pdtVoiddatetime As Date
    Public pstrVoidreason As String = String.Empty

    Public pintAuditUserId As Integer = 0
    Public pdtAuditDate As DateTime
    Public pstrClientIp As String = ""
    Public pstrHostName As String = ""
    Public pstrFormName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Private mDataOp As clsDataOperation = Nothing
    Public Property _DataOp() As clsDataOperation
        Get
            Return mDataOp
        End Get
        Set(ByVal value As clsDataOperation)
            mDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set depttrainingneedallocationtranunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Depttrainingneedallocationtranunkid() As Integer
        Get
            Return mintDepttrainingneedallocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintDepttrainingneedallocationtranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentaltrainingneedunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Departmentaltrainingneedunkid() As Integer
        Get
            Return mintDepartmentaltrainingneedunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentaltrainingneedunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationtranunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Allocationtranunkid() As Integer
        Get
            Return mintAllocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationtranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property


    Private mintAuditUserId As Integer = 0
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime
    Public Property _AuditDate() As DateTime
        Get
            Return mdtAuditDate
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mstrClientIp As String = ""
    Public Property _ClientIP() As String
        Get
            Return mstrClientIp
        End Get
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mstrFormName As String = ""
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mDataOp IsNot Nothing Then
            objDataOperation = mDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "SELECT " & _
              "  depttrainingneedallocationtranunkid " & _
              ", departmentaltrainingneedunkid " & _
              ", allocationtranunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM trdepttrainingneed_allocation_tran " & _
             "WHERE depttrainingneedallocationtranunkid = @depttrainingneedallocationtranunkid "

            objDataOperation.AddParameter("@depttrainingneedallocationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintDepttrainingneedallocationTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintdepttrainingneedallocationtranunkid = CInt(dtRow.Item("depttrainingneedallocationtranunkid"))
                mintdepartmentaltrainingneedunkid = CInt(dtRow.Item("departmentaltrainingneedunkid"))
                mintallocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintvoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intDepartmentalTrainingNeedUnkid As Integer = -1, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mDataOp IsNot Nothing Then
            objDataOperation = mDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim dsAllocation As DataSet = (New clsMasterData).GetTrainingTargetedGroup("List", , , False)

            strQ = "SELECT " & _
              "  trdepttrainingneed_allocation_tran.depttrainingneedallocationtranunkid " & _
              ", trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid " & _
              ", trdepttrainingneed_allocation_tran.allocationtranunkid " & _
              ", trdepttrainingneed_allocation_tran.userunkid " & _
              ", trdepttrainingneed_allocation_tran.loginemployeeunkid " & _
              ", trdepttrainingneed_allocation_tran.isweb " & _
              ", trdepttrainingneed_allocation_tran.isvoid " & _
              ", trdepttrainingneed_allocation_tran.voiduserunkid " & _
              ", trdepttrainingneed_allocation_tran.voidloginemployeeunkid " & _
              ", trdepttrainingneed_allocation_tran.voiddatetime " & _
              ", trdepttrainingneed_allocation_tran.voidreason " & _
              ", CASE ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) "

            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                If CInt(CInt(dsRow.Item("Id"))) <= 0 Then
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("AllocationTranCode").ToString & "' "
                Else
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN " & dsRow.Item("AllocationTranCode").ToString & " "
                End If

            Next
            strQ &= " END AS AllocationCode " & _
            ", CASE ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) "

            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                If CInt(CInt(dsRow.Item("Id"))) <= 0 Then
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("AllocationTranName").ToString & "' "
                Else
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN " & dsRow.Item("AllocationTranName").ToString & " "
                End If

            Next
            strQ &= " END AS AllocationName "

            strQ &= "FROM trdepttrainingneed_allocation_tran " & _
                  "LEFT JOIN trdepartmentaltrainingneed_master ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid " & _
                  "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.DEPARTMENT) & " " & _
                  "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.JOBS) & " " & _
                  "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.BRANCH) & " " & _
                  "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.DEPARTMENT_GROUP) & " " & _
                  "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.SECTION) & " " & _
                  "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.UNIT) & " " & _
                  "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.JOB_GROUP) & " " & _
                  "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.CLASS_GROUP) & " " & _
                  "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.CLASSES) & " " & _
                  " LEFT JOIN prcostcenter_master ON trdepttrainingneed_allocation_tran.allocationtranunkid = prcostcenter_master.costcenterunkid AND prcostcenter_master.isactive = 1 AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.COST_CENTER) & " " & _
                  "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = trdepttrainingneed_allocation_tran.allocationtranunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.TEAM) & " " & _
                  "LEFT JOIN hrunitgroup_master ON trdepttrainingneed_allocation_tran.allocationtranunkid = hrunitgroup_master.unitgroupunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.UNIT_GROUP) & " " & _
                  "LEFT JOIN hrsectiongroup_master ON trdepttrainingneed_allocation_tran.allocationtranunkid = hrsectiongroup_master.sectiongroupunkid AND ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) = " & CInt(enAllocation.SECTION_GROUP) & " " & _
             "WHERE trdepttrainingneed_allocation_tran.isvoid = 0 " & _
                "AND trdepartmentaltrainingneed_master.isvoid = 0 "

            If intDepartmentalTrainingNeedUnkid > 0 Then
                strQ &= " AND trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid = @departmentaltrainingneedunkid "
                objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentalTrainingNeedUnkid)
            ElseIf intDepartmentalTrainingNeedUnkid = 0 Then
                strQ &= " AND 1 = 2 "
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trdepttrainingneed_allocation_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO trdepttrainingneed_allocation_tran ( " & _
              "  departmentaltrainingneedunkid " & _
              ", allocationtranunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @departmentaltrainingneedunkid " & _
              ", @allocationtranunkid " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDepttrainingneedallocationtranunkid = dsList.Tables(0).Rows(0).Item(0)

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trdepttrainingneed_allocation_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintDepttrainingneedallocationtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@depttrainingneedallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepttrainingneedallocationtranunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE trdepttrainingneed_allocation_tran SET " & _
              "  departmentaltrainingneedunkid = @departmentaltrainingneedunkid" & _
              ", allocationtranunkid = @allocationtranunkid" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE depttrainingneedallocationtranunkid = @depttrainingneedallocationtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintDepttrainingneedallocationtranunkid, objDataOperation) = False Then

                If Insert_AtTranLog(objDataOperation, 2) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                blnChildTableChanged = True
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SaveAll(ByVal lstDeptTAlloc As List(Of clsDepttrainingneed_allocation_Tran) _
                            , Optional ByVal clsDeptTNeedMaster As clsDepartmentaltrainingneed_master = Nothing _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            If mintDepartmentaltrainingneedunkid <= 0 AndAlso clsDeptTNeedMaster IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If clsDeptTNeedMaster.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintDepartmentaltrainingneedunkid = intNewUnkId
                End If
            ElseIf mintDepartmentaltrainingneedunkid > 0 AndAlso clsDeptTNeedMaster IsNot Nothing Then
                If clsDeptTNeedMaster.Update(objDataOperation) = False Then
                    Return False
                End If
            End If

            For Each clsDTAlloc As clsDepttrainingneed_allocation_Tran In lstDeptTAlloc

                With clsDTAlloc
                    mintDepttrainingneedallocationtranunkid = .pintDepttrainingneedallocationtranunkid
                    mintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                    mintAllocationtranunkid = .pintAllocationtranunkid

                    mintUserunkid = .pintUserunkid
                    mintLoginemployeeunkid = .pintLoginemployeeunkid
                    mblnIsweb = .pblnIsweb
                    mblnIsvoid = .pblnIsvoid
                    mintVoiduserunkid = .pintVoiduserunkid
                    mintVoidloginemployeeunkid = .pintVoidloginemployeeunkid
                    mdtVoiddatetime = .pdtVoiddatetime
                    mstrVoidreason = .pstrVoidreason

                    mintAuditUserId = .pintAuditUserId
                    mdtAuditDate = .pdtAuditDate
                    mstrClientIp = .pstrClientIp
                    mstrHostName = .pstrHostName
                    mstrFormName = .pstrFormName

                    mintDepttrainingneedallocationtranunkid = isExist(mintDepartmentaltrainingneedunkid, mintAllocationtranunkid, , objDataOperation)
                    If mintDepttrainingneedallocationtranunkid <= 0 Then
                        If Insert(objDataOperation) = False Then
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    Else
                        If Update(False, objDataOperation) = False Then
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    End If

                End With

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trdepttrainingneed_allocation_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE trdepttrainingneed_allocation_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
            "WHERE depttrainingneedallocationtranunkid = @depttrainingneedallocationtranunkid "

            objDataOperation.AddParameter("@depttrainingneedallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal lstDeptTAlloc As List(Of clsDepttrainingneed_allocation_Tran) _
                            , Optional ByVal lstDeptTEmp As List(Of clsDepttrainingneed_employee_Tran) = Nothing _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            For Each clsDTAlloc As clsDepttrainingneed_allocation_Tran In lstDeptTAlloc

                With clsDTAlloc
                    mDataOp = objDataOperation
                    _Depttrainingneedallocationtranunkid = .pintDepttrainingneedallocationtranunkid

                    mblnIsvoid = .pblnIsvoid
                    mintVoiduserunkid = .pintVoiduserunkid
                    mintVoidloginemployeeunkid = .pintVoidloginemployeeunkid
                    mdtVoiddatetime = .pdtVoiddatetime
                    mstrVoidreason = .pstrVoidreason

                    mblnIsweb = .pblnIsweb
                    mintAuditUserId = .pintAuditUserId
                    mdtAuditDate = .pdtAuditDate
                    mstrClientIp = .pstrClientIp
                    mstrHostName = .pstrHostName
                    mstrFormName = .pstrFormName

                    If Void(mintDepttrainingneedallocationtranunkid, objDataOperation) = False Then
                        If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        Exit For
                    End If

                End With

            Next

            If lstDeptTEmp IsNot Nothing Then
                Dim clsDTEmp As New clsDepttrainingneed_employee_Tran

                If clsDTEmp.VoidAll(lstDeptTEmp, objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidByMasterUnkID(ByVal intMasterUnkID As Integer, _
                                      ByVal intParentAuditType As Integer, _
                                      Optional ByVal xDataOp As clsDataOperation = Nothing _
                                      ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "INSERT INTO attrdepttrainingneed_allocation_tran ( " & _
                      "  depttrainingneedallocationtranunkid " & _
                      ", departmentaltrainingneedunkid " & _
                      ", allocationtranunkid " & _
                      ", audituserunkid " & _
                      ", loginemployeeunkid " & _
                      ", audittypeid " & _
                      ", auditdatetime " & _
                      ", isweb " & _
                      ", ip " & _
                      ", host " & _
                      ", form_name " & _
                ") SELECT " & _
                      "  depttrainingneedallocationtranunkid " & _
                      ", departmentaltrainingneedunkid " & _
                      ", allocationtranunkid " & _
                      ", @audituserunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @audittypeid " & _
                      ", @auditdatetime " & _
                      ", @isweb " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @form_name " & _
                "FROM trdepttrainingneed_allocation_tran " & _
                         "WHERE isvoid = 0 " & _
                         "AND departmentaltrainingneedunkid = @departmentaltrainingneedunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterUnkID)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParentAuditType)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ClearParameters()

            strQ = "UPDATE trdepttrainingneed_allocation_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE departmentaltrainingneedunkid = @departmentaltrainingneedunkid "

            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterUnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByMasterUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@depttrainingneedallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intDepartmentaltrainingneedunkid As Integer _
                            , ByVal intAllocationtranunkid As Integer _
                            , Optional ByVal intUnkid As Integer = -1 _
                            , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                            ) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intRetUnkId As Integer = 0
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  depttrainingneedallocationtranunkid " & _
             "FROM trdepttrainingneed_allocation_tran " & _
             "WHERE isvoid = 0 "

            If intDepartmentaltrainingneedunkid > 0 Then
                strQ &= " AND trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid = @departmentaltrainingneedunkid "
                objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentaltrainingneedunkid)
            End If

            If intAllocationtranunkid > 0 Then
                strQ &= " AND trdepttrainingneed_allocation_tran.allocationtranunkid = @allocationtranunkid "
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationtranunkid)
            End If

            If intUnkid > 0 Then
                strQ &= " AND depttrainingneedallocationtranunkid <> @depttrainingneedallocationtranunkid "
                objDataOperation.AddParameter("@depttrainingneedallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRetUnkId = CInt(dsList.Tables(0).Rows(0).Item("depttrainingneedallocationtranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRetUnkId
    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO attrdepttrainingneed_allocation_tran ( " & _
                      "  depttrainingneedallocationtranunkid " & _
                      ", departmentaltrainingneedunkid " & _
                      ", allocationtranunkid " & _
                      ", audituserunkid " & _
                      ", loginemployeeunkid " & _
                      ", audittypeid " & _
                      ", auditdatetime " & _
                      ", isweb " & _
                      ", ip " & _
                      ", host " & _
                      ", form_name " & _
            ") VALUES (" & _
                      "  @depttrainingneedallocationtranunkid " & _
                      ", @departmentaltrainingneedunkid " & _
                      ", @allocationtranunkid " & _
                      ", @audituserunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @audittypeid " & _
                      ", @auditdatetime " & _
                      ", @isweb " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @form_name " & _
            ")"

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@depttrainingneedallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepttrainingneedallocationtranunkid.ToString)
            objDoOps.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDoOps.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)

            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDoOps.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True

    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM attrdepttrainingneed_allocation_tran WHERE depttrainingneedallocationtranunkid = @depttrainingneedallocationtranunkid AND departmentaltrainingneedunkid = @departmentaltrainingneedunkid AND audittypeid <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@depttrainingneedallocationtranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("allocationtranunkid").ToString() = mintAllocationtranunkid Then

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class