﻿'************************************************************************************************************************************
'Class Name : clseval_question_master.vb
'Purpose    :
'Date       :08-07-2021
'Written By :Gajanan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clseval_question_master
    Private Shared ReadOnly mstrModuleName As String = "clseval_question_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintQuestionnaireid As Integer
    Private mintCategoryid As Integer
    Private mstrCode As String = String.Empty
    Private mstrQuestion As String = String.Empty
    Private mintAnswertype As Integer
    Private mblnIsaskforjustification As Boolean
    Private mstrDescription As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Private mblnIsForLineManagerFeedback As Boolean
    'Hemant (20 Aug 2021) -- End

    Enum enAnswerType
        FREETEXT = 1
        RATING = 2
        SELECTION = 3
        PICKDATE = 4
        NUMERIC = 5
    End Enum
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set questionnaireId
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Questionnaireid() As Integer
        Get
            Return mintQuestionnaireid
        End Get
        Set(ByVal value As Integer)
            mintQuestionnaireid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Categoryid() As Integer
        Get
            Return mintCategoryid
        End Get
        Set(ByVal value As Integer)
            mintCategoryid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set question
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Question() As String
        Get
            Return mstrQuestion
        End Get
        Set(ByVal value As String)
            mstrQuestion = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set answertype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Answertype() As Integer
        Get
            Return mintAnswertype
        End Get
        Set(ByVal value As Integer)
            mintAnswertype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isAskforJustification
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isaskforjustification() As Boolean
        Get
            Return mblnIsaskforjustification
        End Get
        Set(ByVal value As Boolean)
            mblnIsaskforjustification = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property


    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Public Property _IsForLineManagerFeedback() As Boolean
        Get
            Return mblnIsForLineManagerFeedback
        End Get
        Set(ByVal value As Boolean)
            mblnIsForLineManagerFeedback = value
        End Set
    End Property
    'Hemant (20 Aug 2021) -- End

#End Region

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  questionnaireId " & _
              ", categoryid " & _
              ", code " & _
              ", question " & _
              ", answertype " & _
              ", isAskforJustification " & _
              ", description " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(isforlinemanager,0) AS isforlinemanager " & _
             "FROM treval_question_master " & _
             "WHERE questionnaireId = @questionnaireId "

            'Hemant (20 Aug 2021) -- [isforlinemanager]

            objDataOperation.AddParameter("@questionnaireId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintQuestionnaireid = CInt(dtRow.Item("questionnaireId"))
                mintCategoryid = CInt(dtRow.Item("categoryid"))
                mstrCode = dtRow.Item("code").ToString
                mstrQuestion = dtRow.Item("question").ToString
                mintAnswertype = CInt(dtRow.Item("answertype"))
                mblnIsaskforjustification = CBool(dtRow.Item("isAskforJustification"))
                mstrDescription = dtRow.Item("description").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                mblnIsForLineManagerFeedback = CBool(dtRow.Item("isforlinemanager"))
                'Hemant (20 Aug 2021) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intCategoryid As Integer = -1 _
                            , Optional ByVal intFeedbackModeid As Integer = -1 _
                            , Optional ByVal blnExcludeLineManagerFeedbackQuestions As Boolean = False _
                            , Optional ByVal blnForLineManagerFeedback As Boolean = False _
                            ) As DataSet
        'Hemant (20 Aug 2021) -- [blnExcludeLineManagerFeedbackQuestions,blnForLineManagerFeedback]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "questionnaireId " & _
                   ",treval_question_master.categoryid " & _
                   ",code " & _
                   ",question " & _
                   ",answertype " & _
                   ",isAskforJustification " & _
                   ",description " & _
                   ",treval_question_master.isvoid " & _
                   ",treval_group_master.categoryname AS categoryname " & _
                   ",CASE " & _
                          "WHEN answertype = " & enAnswerType.FREETEXT & " THEN @FREETEXT " & _
                          "WHEN answertype = " & enAnswerType.NUMERIC & " THEN @NUMERIC " & _
                          "WHEN answertype = " & enAnswerType.PICKDATE & " THEN @PICKDATE " & _
                          "WHEN answertype = " & enAnswerType.RATING & " THEN @RATING " & _
                          "WHEN answertype = " & enAnswerType.SELECTION & " THEN @SELECTION " & _
                     "END AS answertypeval " & _
                     ", treval_group_master.feedbackmode as feedbackmodeid " & _
                     ", ISNULL(isforlinemanager,0) AS isforlinemanager " & _
                "FROM treval_question_master " & _
                "LEFT JOIN treval_group_master " & _
                     "ON treval_question_master.categoryid = treval_group_master.categoryid WHERE 1=1 "

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            If blnOnlyActive Then
                strQ &= " and treval_question_master.isvoid = 0 "
            End If

            If intCategoryid > 0 Then
                strQ &= " and treval_question_master.categoryid = @categoryid "
            End If

            If intFeedbackModeid > 0 Then
                strQ &= " and treval_group_master.feedbackmode = @feedbackmodeid "
                objDataOperation.AddParameter("@feedbackmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFeedbackModeid)
            End If

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            If blnExcludeLineManagerFeedbackQuestions = True Then
                strQ &= " and treval_question_master.isforlinemanager <> 1 "
            End If

            If blnForLineManagerFeedback = True Then
                strQ &= " and isforlinemanager = 1 "
            End If
            'Hemant (20 Aug 2021) -- End

            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryid)
            objDataOperation.AddParameter("@FREETEXT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Free Text"))
            objDataOperation.AddParameter("@NUMERIC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Numeric"))
            objDataOperation.AddParameter("@PICKDATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Date"))
            objDataOperation.AddParameter("@RATING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Rating"))
            objDataOperation.AddParameter("@SELECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Selection"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetQuestionComboList(ByVal strTableName As String, Optional ByVal blnSelect As Boolean = True, Optional ByVal intCategoryid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnSelect Then
                strQ = "SELECT 0 AS Id, @SE AS Name UNION ALL "
            End If

            strQ &= " SELECT " & _
              "  questionnaireId as Id " & _
              ", question AS Name " & _
             "FROM treval_question_master WHERE isvoid = 0 "

            If intCategoryid > 0 Then
                strQ &= " and categoryid = @categoryid "
            End If

            objDataOperation.AddParameter("@SE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetQuestionComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (treval_question_master) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByRef intNewUnkId As Integer = 0) As Boolean
        If isExist(mstrQuestion, -1, xDataOpr) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This question is already defined. Please define new question.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@question", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrQuestion.ToString)
            objDataOperation.AddParameter("@answertype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswertype.ToString)
            objDataOperation.AddParameter("@isAskforJustification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsaskforjustification.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isforlinemanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForLineManagerFeedback.ToString)
            'Hemant (20 Aug 2021) -- End


            strQ = "INSERT INTO treval_question_master ( " & _
              "  categoryid " & _
              ", code " & _
              ", question " & _
              ", answertype " & _
              ", isAskforJustification " & _
              ", description " & _
              ", isvoid" & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", isforlinemanager " & _
            ") VALUES (" & _
              "  @categoryid " & _
              ", @code " & _
              ", @question " & _
              ", @answertype " & _
              ", @isAskforJustification " & _
              ", @description " & _
              ", @isvoid" & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @isforlinemanager " & _
            "); SELECT @@identity"

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintQuestionnaireid = dsList.Tables(0).Rows(0).Item(0)

            intNewUnkId = mintQuestionnaireid

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintQuestionnaireid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (treval_question_master) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mstrQuestion, mintQuestionnaireid, xDataOpr) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This question is already defined. Please define new question.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Try
            objDataOperation.AddParameter("@questionnaireId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireid.ToString)
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@question", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrQuestion.ToString)
            objDataOperation.AddParameter("@answertype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswertype.ToString)
            objDataOperation.AddParameter("@isAskforJustification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsaskforjustification.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isforlinemanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForLineManagerFeedback.ToString)
            'Hemant (20 Aug 2021) -- End

            strQ = "UPDATE treval_question_master SET " & _
              "  categoryid = @categoryid " & _
              ", code = @code " & _
              ", question = @question " & _
              ", answertype = @answertype " & _
              ", isAskforJustification = @isAskforJustification " & _
              ", description = @description " & _
              ", isforlinemanager = @isforlinemanager " & _
              " WHERE questionnaireId = @questionnaireId "

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintQuestionnaireid, objDataOperation) = False Then
                If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintQuestionnaireid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (treval_question_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objEval As New clseval_answer_master
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            With objEval
                ._Isvoid = mblnIsvoid
                ._Voiduserunkid = mintVoiduserunkid
                ._Voiddatetime = mdtVoiddatetime
                ._Voidreason = mstrVoidreason
                ._IsWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._ClientIP = mstrClientIP
                ._FormName = mstrFormName
                ._HostName = mstrHostName
                .VoidAllByMasterUnkID(intUnkid, 3, objDataOperation, -1)
            End With

            strQ = "UPDATE treval_question_master SET " & _
              " isvoid = @isvoid " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE questionnaireId = @questionnaireId "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@questionnaireId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"treval_subquestion_master", "trtraining_evaluation_tran"}
            objDataOperation.AddParameter("@questionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each value As String In Tables
                Select Case value
                    Case "treval_subquestion_master"
                        strQ = "SELECT questionid FROM " & value & " WHERE isvoid = 0 AND questionid = @questionid  "
                    Case "trtraining_evaluation_tran"
                        strQ = "SELECT questionnaireid FROM " & value & " WHERE isvoid = 0 AND questionnaireid = @questionid  "

                    Case Else
                End Select

                If strQ.Trim.Length > 0 Then

                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("Used").Rows.Count > 0 Then
                        Return True
                        Exit For
                    End If
                End If
            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  questionnaireId " & _
              ", categoryid " & _
              ", code " & _
              ", question " & _
             "FROM treval_question_master " & _
             "WHERE  isvoid = 0 AND question = @question "
            If intUnkid > 0 Then
                strQ &= " AND questionnaireId <> @questionnaireId"
            End If

            objDataOperation.AddParameter("@question", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@questionnaireId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType, ByVal intUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Try

            StrQ = "INSERT INTO attreval_question_master ( " & _
                    "  tranguid " & _
                    ", questionnaireId " & _
                    ", categoryid " & _
                    ", code " & _
                    ", question " & _
                    ", answertype " & _
                    ", isAskforJustification " & _
                    ", description " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", form_name " & _
                    ", ip " & _
                    ", hostname " & _
                    ", isweb" & _
                    ", isforlinemanager " & _
                  ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", questionnaireId " & _
                    ", categoryid " & _
                    ", code " & _
                    ", question " & _
                    ", answertype " & _
                    ", isAskforJustification " & _
                    ", description " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @form_name " & _
                    ", @ip " & _
                    ", @hostname " & _
                    ", @isweb" & _
                    ", isforlinemanager " & _
                    "  From treval_question_master WHERE 1=1 AND treval_question_master.questionnaireId = @questionnaireId "

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@questionnaireId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM attreval_question_master WHERE questionnaireId = @questionnaireId AND audittype <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@questionnaireId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("questionnaireId").ToString() = mintQuestionnaireid AndAlso dr("categoryid").ToString() = mintCategoryid _
                    AndAlso dr("code").ToString() = mstrCode AndAlso dr("question").ToString() = mstrQuestion _
                    AndAlso dr("answertype").ToString() = mintAnswertype AndAlso CBool(dr("isAskforJustification")) = mblnIsaskforjustification _
                    AndAlso dr("description").ToString() = mstrDescription AndAlso CBool(dr("isforlinemanager")) = mblnIsForLineManagerFeedback _
                    Then
                    'Hemant (20 Aug 2021) -- [isforlinemanager]

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Sorry, This question is already defined. Please define new question.")
			Language.setMessage(mstrModuleName, 3, "Free Text")
			Language.setMessage(mstrModuleName, 4, "Numeric")
			Language.setMessage(mstrModuleName, 5, "Date")
			Language.setMessage(mstrModuleName, 6, "Rating")
			Language.setMessage(mstrModuleName, 7, "Selection")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class