﻿'************************************************************************************************************************************
'Class Name : clseval_answer_master.vb
'Purpose    :
'Date       : 30-Jul-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clseval_answer_master
    Private Shared ReadOnly mstrModuleName As String = "clseval_answer_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintTrainingEvaluationAnswerunkid As Integer
    Private mintQuestionnaireId As Integer
    Private mintAnswerTypeid As Integer
    Private mintSubQuestionId As Integer
    Private mintOptionId As Integer
    Private mstrOptionValue As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingevaluationanswerunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingEvaluationAnswerunkid() As Integer
        Get
            Return mintTrainingEvaluationAnswerunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingEvaluationAnswerunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set questionnaireid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _QuestionnaireId() As Integer
        Get
            Return mintQuestionnaireId
        End Get
        Set(ByVal value As Integer)
            mintQuestionnaireId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set answertypeid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _AnswerTypeid() As Integer
        Get
            Return mintAnswerTypeid
        End Get
        Set(ByVal value As Integer)
            mintAnswerTypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set subquestionid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _SubQuestionId() As Integer
        Get
            Return mintSubQuestionId
        End Get
        Set(ByVal value As Integer)
            mintSubQuestionId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set optionid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _OptionId() As Integer
        Get
            Return mintOptionId
        End Get
        Set(ByVal value As Integer)
            mintOptionId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set optionvalue
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _OptionValue() As String
        Get
            Return mstrOptionValue
        End Get
        Set(ByVal value As String)
            mstrOptionValue = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  trainingevaluationanswerunkid " & _
                       ", questionnaireid " & _
                       ", answertypeid " & _
                       ", subquestionid " & _
                       ", optionid " & _
                       ", optionvalue " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
             "FROM treval_answer_master " & _
             "WHERE trainingevaluationanswerunkid = @trainingevaluationanswerunkid "

            objDataOperation.AddParameter("@trainingevaluationanswerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingEvaluationAnswerunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrainingEvaluationAnswerunkid = CInt(dtRow.Item("trainingevaluationanswerunkid"))
                mintQuestionnaireId = CInt(dtRow.Item("questionnaireid"))
                mintAnswerTypeid = CInt(dtRow.Item("answertypeid"))
                mintSubQuestionId = CInt(dtRow.Item("subquestionid"))
                mintOptionId = CInt(dtRow.Item("optionid"))
                mstrOptionValue = dtRow.Item("optionvalue").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>    ''' 
    Public Function GetList(ByVal strTableName As String, Optional ByVal intQuestionaireId As Integer = -1, Optional ByVal intSubQuestionId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  trainingevaluationanswerunkid " & _
                       ", questionnaireid " & _
                       ", answertypeid " & _
                       ", subquestionid " & _
                       ", optionid " & _
                       ", optionvalue " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
             " FROM treval_answer_master " & _
             " WHERE treval_answer_master.isvoid = 0 "

            objDataOperation.ClearParameters()

            If intQuestionaireId > -1 Then
                strQ &= " AND questionnaireid = @questionnaireid "
                objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionaireId)
            End If

            If intSubQuestionId > -1 Then
                strQ &= " AND subquestionid = @subquestionid "
                objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubQuestionId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (treval_answer_master) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mstrOptionValue, mintQuestionnaireId, mintSubQuestionId, -1, xDataOp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Option Value is already defined. Please define new Option Value.")
            Return False
        End If

        Dim intOptionId As Integer = -1
        GetNextOptionId(intOptionId, mintQuestionnaireId, mintSubQuestionId, xDataOp)
        mintOptionId = intOptionId


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireId.ToString)
            objDataOperation.AddParameter("@answertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswerTypeid.ToString)
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubQuestionId.ToString)
            objDataOperation.AddParameter("@optionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptionId.ToString)
            objDataOperation.AddParameter("@optionvalue", SqlDbType.NVarChar, mstrOptionValue.Trim.Length, mstrOptionValue.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "INSERT INTO treval_answer_master ( " & _
                       "  questionnaireid " & _
                       ", answertypeid " & _
                       ", subquestionid " & _
                       ", optionid " & _
                       ", optionvalue " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                    ") VALUES (" & _
                       "  @questionnaireid " & _
                       ", @answertypeid " & _
                       ", @subquestionid " & _
                       ", @optionid " & _
                       ", @optionvalue " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingEvaluationAnswerunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (treval_answer_master) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mstrOptionValue, mintQuestionnaireId, mintSubQuestionId, mintTrainingEvaluationAnswerunkid, xDataOp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Option Value is already defined. Please define new Option Value.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingevaluationanswerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingEvaluationAnswerunkid.ToString)
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireId.ToString)
            objDataOperation.AddParameter("@answertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswerTypeid.ToString)
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubQuestionId.ToString)
            objDataOperation.AddParameter("@optionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptionId.ToString)
            objDataOperation.AddParameter("@optionvalue", SqlDbType.NVarChar, mstrOptionValue.Trim.Length, mstrOptionValue.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "UPDATE treval_answer_master SET " & _
                       "  questionnaireid = @questionnaireid " & _
                       ", answertypeid = @answertypeid " & _
                       ", subquestionid = @subquestionid " & _
                       ", optionid = @optionid " & _
                       ", optionvalue = @optionvalue " & _
                       ", isvoid = @isvoid " & _
                       ", voiduserunkid = @voiduserunkid " & _
                       ", voiddatetime = @voiddatetime " & _
                       ", voidreason = @voidreason " & _
                    "WHERE trainingevaluationanswerunkid = @trainingevaluationanswerunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SaveAllQuestionAnswer(Optional ByVal clsEvalQuestionMaster As clseval_question_master = Nothing _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            If mintQuestionnaireId <= 0 AndAlso clsEvalQuestionMaster IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If clsEvalQuestionMaster.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintQuestionnaireId = intNewUnkId
                End If
            ElseIf mintQuestionnaireId > 0 AndAlso clsEvalQuestionMaster IsNot Nothing Then
                If clsEvalQuestionMaster.Update(objDataOperation) = False Then
                    Return False
                End If
            End If


            'mintTrainingEvaluationAnswerunkid = isExist(mstrOptionValue, mintQuestionnaireId, mintSubQuestionId, , objDataOperation)
            If mintTrainingEvaluationAnswerunkid <= 0 Then
                If Insert(objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Exit Function
                End If
            Else
                If Update(objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Exit Function
                End If
            End If


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveAllQuestionAnswer; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SaveAllSubQuestionAnswer(Optional ByVal clsEvalSubQuestionMaster As clseval_subquestion_master = Nothing _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            If mintSubQuestionId <= 0 AndAlso clsEvalSubQuestionMaster IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If clsEvalSubQuestionMaster.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintSubQuestionId = intNewUnkId
                End If
            ElseIf mintSubQuestionId > 0 AndAlso clsEvalSubQuestionMaster IsNot Nothing Then
                If clsEvalSubQuestionMaster.Update(objDataOperation) = False Then
                    Return False
                End If
            End If


            'mintTrainingEvaluationAnswerunkid = isExist(mstrOptionValue, mintQuestionnaireId, mintSubQuestionId, , objDataOperation)
            If mintTrainingEvaluationAnswerunkid <= 0 Then
                If Insert(objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Exit Function
                End If
            Else
                If Update(objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Exit Function
                End If
            End If


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveAllSubQuestionAnswer; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (treval_answer_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE treval_answer_master SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                   "WHERE trainingevaluationanswerunkid = @trainingevaluationanswerunkid "

            objDataOperation.AddParameter("@trainingevaluationanswerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            _TrainingEvaluationAnswerunkid = intUnkid

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAllByMasterUnkID(ByVal intQuestionnaireId As Integer, _
                                         ByVal intParentAuditType As Integer, _
                                         Optional ByVal xDataOp As clsDataOperation = Nothing, _
                                         Optional ByVal intSubquestionId As Integer = -1 _
                                         ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "INSERT INTO attreval_answer_master ( " & _
                     "  tranguid " & _
                     ", trainingevaluationanswerunkid " & _
                     ", questionnaireid " & _
                     ", answertypeid " & _
                     ", subquestionid " & _
                     ", optionid " & _
                     ", optionvalue " & _
                     ", audituserunkid " & _
                     ", audittype " & _
                     ", auditdatetime " & _
                     ", isweb " & _
                     ", ip " & _
                     ", hostname " & _
                     ", form_name " & _
               ") SELECT " & _
                     "  LOWER(NEWID()) " & _
                     ", trainingevaluationanswerunkid " & _
                     ", questionnaireid " & _
                     ", answertypeid " & _
                     ", subquestionid " & _
                     ", optionid " & _
                     ", optionvalue " & _
                     ", @audituserunkid " & _
                     ", @audittype " & _
                     ", GETDATE() " & _
                     ", @isweb " & _
                     ", @ip " & _
                     ", @hostname " & _
                     ", @form_name " & _
               "FROM treval_answer_master " & _
                        "WHERE isvoid = 0 " & _
                        "AND questionnaireid = @questionnaireid "

            objDataOperation.ClearParameters()
            If intSubquestionId > 0 Then
                strQ &= "AND subquestionid = @subquestionid "
                objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubquestionId)
            Else
                strQ &= "AND subquestionid <= 0 "
            End If
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionnaireId)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intParentAuditType)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ClearParameters()

            strQ = "UPDATE treval_answer_master SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE isvoid = 0 AND questionnaireid = @questionnaireid "

            If intSubquestionId > 0 Then
                strQ &= "AND subquestionid = @subquestionid "
                objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubquestionId)
            Else
                strQ &= "AND subquestionid <= 0 "
            End If
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionnaireId)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByMasterUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            Dim Tables() As String = {"trtraining_evaluation_tran"}
            objDataOperation.AddParameter("@trainingevaluationanswerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each value As String In Tables
                Select Case value
                    Case "trtraining_evaluation_tran"
                        _TrainingEvaluationAnswerunkid = intUnkid
                        strQ = "SELECT answer FROM " & value & " WHERE isvoid = 0 AND questionnaireid = " & mintQuestionnaireId

                        If mintSubQuestionId > 0 Then
                            strQ &= " AND subquestionid = " & mintSubQuestionId & " "
                        Else
                            strQ &= " AND subquestionid <= 0 "
                        End If

                        strQ &= " AND answer = '" & mintOptionId & "' "

                    Case Else
                End Select

                If strQ.Trim.Length > 0 Then

                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("Used").Rows.Count > 0 Then
                        Return True
                        Exit For
                    End If
                End If
            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strOptionValue As String, ByVal intQuestionnaireId As Integer, Optional ByVal intSubQuestionId As Integer = -1, _
                            Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                       "  trainingevaluationanswerunkid " & _
                       ", questionnaireid " & _
                       ", answertypeid " & _
                       ", subquestionid " & _
                       ", optionid " & _
                       ", optionvalue " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                    "FROM treval_answer_master " & _
                    "WHERE isvoid = 0 " & _
                    " AND optionvalue = @optionvalue " & _
                    " AND questionnaireid = @questionnaireid " & _
                    " AND subquestionid  = @subquestionid "

            If intUnkid > 0 Then
                strQ &= " AND trainingevaluationanswerunkid <> @trainingevaluationanswerunkid"
            End If

            objDataOperation.AddParameter("@optionvalue", SqlDbType.NVarChar, strOptionValue.Trim.Length, strOptionValue)
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubQuestionId)
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionnaireId)
            objDataOperation.AddParameter("@trainingevaluationanswerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO attreval_answer_master ( " & _
                       "  tranguid " & _
                       ", trainingevaluationanswerunkid " & _
                       ", questionnaireid " & _
                       ", answertypeid " & _
                       ", subquestionid " & _
                       ", optionid " & _
                       ", optionvalue " & _
                       ", audituserunkid " & _
                       ", audittype " & _
                       ", auditdatetime " & _
                       ", isweb " & _
                       ", ip " & _
                       ", hostname " & _
                       ", form_name " & _
                  ") VALUES (" & _
                       "  LOWER(NEWID()) " & _
                       ", @trainingevaluationanswerunkid " & _
                       ", @questionnaireid " & _
                       ", @answertypeid " & _
                       ", @subquestionid " & _
                       ", @optionid " & _
                       ", @optionvalue " & _
                       ", @audituserunkid " & _
                       ", @audittype " & _
                       ", GETDATE() " & _
                       ", @isweb " & _
                       ", @ip " & _
                       ", @hostname " & _
                       ", @form_name " & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingevaluationanswerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingEvaluationAnswerunkid.ToString)
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireId.ToString)
            objDataOperation.AddParameter("@answertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswerTypeid.ToString)
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubQuestionId.ToString)
            objDataOperation.AddParameter("@optionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptionId.ToString)
            objDataOperation.AddParameter("@optionvalue", SqlDbType.NVarChar, mstrOptionValue.Trim.Length, mstrOptionValue.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Sub GetNextOptionId(ByRef intNextOptionId As Integer, ByVal intQuestionid As Integer, Optional ByVal intSubQuestionid As Integer = -1, _
                               Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception : Dim dsList As DataSet = Nothing
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try

            StrQ = "SELECT " & _
                     " ISNULL(max(optionid),0) + 1 as NextOptionId " & _
                     "   FROM treval_answer_master " & _
                     "   WHERE isvoid = 0 " & _
                     "         AND questionnaireid = @questionnaireid "


            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionid)

            If intSubQuestionid > 0 Then
                StrQ &= " AND subquestionid = @subquestionid"
                objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubQuestionid)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intNextOptionId = Convert.ToInt32(dsList.Tables(0).Rows(0)("NextOptionId"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextOptionId; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Sub

    Public Function GetComboList(ByVal strTableName As String, _
                                 Optional ByVal blnSelect As Boolean = True, _
                                 Optional ByVal intQuestionid As Integer = -1, _
                                 Optional ByVal intSubQuestionid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnSelect Then
                strQ = "SELECT 0 AS Id, @SE AS Name UNION ALL "
            End If

            strQ &= " SELECT " & _
              "  optionid as Id " & _
              ", optionvalue AS Name " & _
             "FROM treval_answer_master WHERE isvoid = 0 "

            If intQuestionid > 0 Then
                strQ &= " and questionnaireid = @questionnaireid "
            End If

            If intSubQuestionid > 0 Then
                strQ &= " and subquestionid = @subquestionid "
                objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubQuestionid)
            Else
                strQ &= " and subquestionid <= 0 "
            End If

            objDataOperation.AddParameter("@SE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
End Class
