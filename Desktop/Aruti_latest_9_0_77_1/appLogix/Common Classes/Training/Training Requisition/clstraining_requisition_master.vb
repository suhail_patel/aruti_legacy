﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clstraining_requisition_master.vb
'Purpose    :
'Date       :15-Oct-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clstraining_requisition_master
    Private Shared ReadOnly mstrModuleName As String = "clstraining_requisition_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private objRequisitionTran As New clstraining_requisition_tran

#Region " Private variables "

    Private mintRequisitionmasterunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintCoursemasterunkid As Integer
    Private mintInstituteunkid As Integer
    Private mintTrainingmodeunkid As Integer
    Private mstrTraining_Venue As String = String.Empty
    Private mdtTraining_Startdate As Date
    Private mdtTraining_Enddate As Date
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrAdditonal_Comments As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private mintLoginEmployeeId As Integer = 0
    Private mblnIsCancel As Boolean = False
    Private mintCancelFromModuleId As Integer = 0
    Private mintCancelUserId As Integer = 0
    Private mstrCancelRemark As String = String.Empty
    Private mdtCancelDateTime As DateTime = Nothing
    Private mintClaimRequestMasterId As Integer = 0
    Private mdtApplyDate As DateTime = Nothing
    'S.SANDEEP [07-NOV-2018] -- START
    Private mintTrainingTypeid As Integer = 0
    'S.SANDEEP [07-NOV-2018] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set requisitionmasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Requisitionmasterunkid(Optional ByVal objOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintRequisitionmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintRequisitionmasterunkid = value
            Call GetData(objOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coursemasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Coursemasterunkid() As Integer
        Get
            Return mintCoursemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCoursemasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set instituteunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Instituteunkid() As Integer
        Get
            Return mintInstituteunkid
        End Get
        Set(ByVal value As Integer)
            mintInstituteunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingmodeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Trainingmodeunkid() As Integer
        Get
            Return mintTrainingmodeunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingmodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set training_venue
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Training_Venue() As String
        Get
            Return mstrTraining_Venue
        End Get
        Set(ByVal value As String)
            mstrTraining_Venue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set training_startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Training_Startdate() As Date
        Get
            Return mdtTraining_Startdate
        End Get
        Set(ByVal value As Date)
            mdtTraining_Startdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set training_enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Training_Enddate() As Date
        Get
            Return mdtTraining_Enddate
        End Get
        Set(ByVal value As Date)
            mdtTraining_Enddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set additonal_comments
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Additonal_Comments() As String
        Get
            Return mstrAdditonal_Comments
        End Get
        Set(ByVal value As String)
            mstrAdditonal_Comments = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _LoginEmployeeId() As Integer
        Set(ByVal value As Integer)
            mintLoginEmployeeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsCancel() As Boolean
        Get
            Return mblnIsCancel
        End Get
        Set(ByVal value As Boolean)
            mblnIsCancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelmoduleid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelFromModuleId() As Integer
        Get
            Return mintCancelFromModuleId
        End Get
        Set(ByVal value As Integer)
            mintCancelFromModuleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceluserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelUserId() As Integer
        Get
            Return mintCancelUserId
        End Get
        Set(ByVal value As Integer)
            mintCancelUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelremark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelRemark() As String
        Get
            Return mstrCancelRemark
        End Get
        Set(ByVal value As String)
            mstrCancelRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceldatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelDateTime() As DateTime
        Get
            Return mdtCancelDateTime
        End Get
        Set(ByVal value As DateTime)
            mdtCancelDateTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get crmasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ClaimRequestMasterId() As Integer
        Get
            Return mintClaimRequestMasterId
        End Get
        Set(ByVal value As Integer)
            mintClaimRequestMasterId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get applydate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ApplyDate() As DateTime
        Get
            Return mdtApplyDate
        End Get
        Set(ByVal value As DateTime)
            mdtApplyDate = value
        End Set
    End Property

    'S.SANDEEP [07-NOV-2018] -- START
    ''' <summary>
    ''' Purpose: Get applydate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _TrainingTypeid() As Integer
        Get
            Return mintTrainingTypeid
        End Get
        Set(ByVal value As Integer)
            mintTrainingTypeid = value
        End Set
    End Property
    'S.SANDEEP [07-NOV-2018] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                   "  requisitionmasterunkid " & _
                   ", employeeunkid " & _
                   ", coursemasterunkid " & _
                   ", instituteunkid " & _
                   ", trainingmodeunkid " & _
                   ", training_venue " & _
                   ", training_startdate " & _
                   ", training_enddate " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voiduserunkid " & _
                   ", voidreason " & _
                   ", additonal_comments " & _
                   ", cancelfrommoduleid " & _
                   ", iscancel " & _
                   ", canceluserunkid " & _
                   ", cancel_remark " & _
                   ", cancel_datetime " & _
                   ", crmasterunkid " & _
                   ", applydate " & _
                   ", trainingtypeid " & _
                   "FROM hrtraining_requisition_master " & _
                   "WHERE requisitionmasterunkid = @requisitionmasterunkid " & _
                   " AND iscancel = 0 AND isvoid = 0 "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequisitionmasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintRequisitionmasterunkid = CInt(dtRow.Item("requisitionmasterunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintCoursemasterunkid = CInt(dtRow.Item("coursemasterunkid"))
                mintInstituteunkid = CInt(dtRow.Item("instituteunkid"))
                mintTrainingmodeunkid = CInt(dtRow.Item("trainingmodeunkid"))
                mstrTraining_Venue = dtRow.Item("training_venue").ToString
                mdtTraining_Startdate = CDate(dtRow.Item("training_startdate"))
                mdtTraining_Enddate = CDate(dtRow.Item("training_enddate"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                Else
                    mdtVoiddatetime = Nothing
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mstrAdditonal_Comments = dtRow.Item("additonal_comments").ToString

                mblnIsCancel = CBool(dtRow.Item("iscancel"))
                mintCancelFromModuleId = CInt(dtRow.Item("cancelfrommoduleid"))
                mintCancelUserId = CInt(dtRow.Item("canceluserunkid"))
                mstrCancelRemark = CStr(dtRow.Item("cancel_remark"))
                If IsDBNull(dtRow.Item("cancel_datetime")) = False Then
                    mdtCancelDateTime = CDate(dtRow.Item("cancel_datetime"))
                Else
                    mdtCancelDateTime = Nothing
                End If
                mintClaimRequestMasterId = CInt(dtRow.Item("crmasterunkid"))
                mdtApplyDate = CDate(dtRow.Item("applydate"))
                mintTrainingTypeid = CInt(dtRow.Item("trainingtypeid")) 'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal mstrFilter As String = "", Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            If blnAddSelect Then
                strQ = "SELECT " & _
                       "     0 AS requisitionmasterunkid " & _
                       "    ,0 AS employeeunkid " & _
                       "    ,0 AS coursemasterunkid " & _
                       "    ,0 AS instituteunkid " & _
                       "    ,0 AS trainingmodeunkid " & _
                       "    ,'' AS training_venue " & _
                       "    ,'' AS training_startdate " & _
                       "    ,'' AS training_enddate " & _
                       "    ,0 AS userunkid " & _
                       "    ,CAST(0 AS BIT) AS isvoid " & _
                       "    ,NULL AS voiddatetime " & _
                       "    ,0 AS voiduserunkid " & _
                       "    ,'' AS voidreason " & _
                       "    ,'' AS additonal_comments " & _
                       "    ,'' AS TrainingMode " & _
                       "    ,'' AS CourseMaster " & _
                       "    ,@Select AS Employee " & _
                       "    ,'' AS institute_name " & _
                       "    ,0 AS Duration " & _
                       "    ,'' AS iStatus " & _
                       "    ,0 AS iStatusId " & _
                       "    ,'' AS masterguid " & _
                       "    ,0 AS cancelfrommoduleid " & _
                       "    ,CAST(0 AS BIT) AS iscancel " & _
                       "    ,0 AS canceluserunkid " & _
                       "    ,'' AS cancel_remark " & _
                       "    ,NULL cancel_datetime " & _
                       "    ,0 AS linkedmasterid " & _
                       "    ,0 AS crmasterunkid " & _
                       "    ,NULL AS applydate " & _
                       "    ,0 AS trainingtypeid " & _
                       "    ,'' AS trainingtype "
            End If
            strQ &= "SELECT " & _
                    "     TRM.requisitionmasterunkid " & _
                    "    ,TRM.employeeunkid " & _
                    "    ,TRM.coursemasterunkid " & _
                    "    ,TRM.instituteunkid " & _
                    "    ,TRM.trainingmodeunkid " & _
                    "    ,TRM.training_venue " & _
                    "    ,ISNULL(CONVERT(NVARCHAR(8),TRM.training_startdate,112),'') AS training_startdate " & _
                    "    ,ISNULL(CONVERT(NVARCHAR(8),TRM.training_enddate,112),'') AS training_enddate " & _
                    "    ,TRM.userunkid " & _
                    "    ,TRM.isvoid " & _
                    "    ,TRM.voiddatetime " & _
                    "    ,TRM.voiduserunkid " & _
                    "    ,TRM.voidreason " & _
                    "    ,TRM.additonal_comments " & _
                    "    ,ISNULL(TM.name,'') AS TrainingMode " & _
                    "    ,ISNULL(CM.name,'') AS CourseMaster " & _
                    "    ,EM.employeecode + '-' + EM.firstname+' '+EM.surname AS Employee " & _
                    "    ,ISNULL(IM.institute_name,'') AS institute_name " & _
                    "    ,(DATEDIFF(DAY,training_startdate,training_enddate) + 1) AS Duration " & _
                    "    ,@Status AS iStatus " & _
                    "    ,2 AS iStatusId " & _
                    "    ,ISNULL(A.masterguid,'') AS masterguid " & _
                    "    ,TRM.cancelfrommoduleid " & _
                    "    ,TRM.iscancel " & _
                    "    ,TRM.canceluserunkid " & _
                    "    ,TRM.cancel_remark " & _
                    "    ,TRM.cancel_datetime " & _
                    "    ,ISNULL(A.linkedmasterid,0) AS linkedmasterid " & _
                    "    ,ISNULL(A.crmasterunkid,0) AS crmasterunkid " & _
                    "    ,TRM.applydate " & _
                    "    ,TRM.trainingtypeid " & _
                    "    ,CASE WHEN TRM.trainingtypeid = 0 THEN @S " & _
                    "          WHEN TRM.trainingtypeid = 1 THEN @E " & _
                    "          WHEN TRM.trainingtypeid = 2 THEN @I END AS trainingtype " & _
                    "FROM hrtraining_requisition_master AS TRM " & _
                    "   LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = TRM.coursemasterunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            requisitionmasterunkid " & _
                    "           ,employeeunkid " & _
                    "           ,masterguid " & _
                    "           ,linkedmasterid " & _
                    "           ,crmasterunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY requisitionmasterunkid,employeeunkid ORDER BY transactiondate DESC) AS rno " & _
                    "       FROM hrtraining_requisition_approval_master " & _
                    "       WHERE isvoid = 0 " & _
                    "   ) AS A ON A.requisitionmasterunkid = TRM.requisitionmasterunkid " & _
                    "   AND A.employeeunkid = TRM.employeeunkid AND A.rno = 1 " & _
                    "   LEFT JOIN cfcommon_master AS TM ON TM.masterunkid = TRM.trainingmodeunkid " & _
                    "   JOIN hremployee_master AS EM ON EM.employeeunkid = TRM.employeeunkid " & _
                    "   JOIN hrinstitute_master AS IM ON IM.instituteunkid = TRM.instituteunkid AND IM.ishospital = 0 " & _
                    "WHERE TRM.isvoid = 0 "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid,trainingtype} -- END

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@Status", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 101, "Approved"))
            'S.SANDEEP [07-NOV-2018] -- START
            objDataOperation.AddParameter("@S", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "NA"))
            objDataOperation.AddParameter("@E", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 862, "External"))
            objDataOperation.AddParameter("@I", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 863, "Internal"))
            'S.SANDEEP [07-NOV-2018] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_requisition_master) </purpose>
    Public Function Insert(ByVal dtRequisition As DataTable, ByVal objDataOperation As clsDataOperation) As Boolean
        If isExist(mintEmployeeunkid, mintCoursemasterunkid, mdtTraining_Startdate, mdtTraining_Enddate, mintInstituteunkid, , objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Training requisition for selected course, startdate, enddate and employee already present. Please add new training requisition.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoursemasterunkid.ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingmodeunkid.ToString)
            objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraining_Venue.ToString)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Startdate)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Enddate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@additonal_comments", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrAdditonal_Comments.ToString)
            objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelUserId)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancelRemark.ToString)
            If mdtCancelDateTime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancelDateTime)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRequestMasterId)
            objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplyDate)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeid) 'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            strQ = "INSERT INTO hrtraining_requisition_master ( " & _
                   "  employeeunkid " & _
                   ", coursemasterunkid " & _
                   ", instituteunkid " & _
                   ", trainingmodeunkid " & _
                   ", training_venue " & _
                   ", training_startdate " & _
                   ", training_enddate " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voiduserunkid " & _
                   ", voidreason " & _
                   ", additonal_comments" & _
                   ", cancelfrommoduleid " & _
                   ", iscancel " & _
                   ", canceluserunkid " & _
                   ", cancel_remark " & _
                   ", cancel_datetime " & _
                   ", crmasterunkid " & _
                   ", applydate " & _
                   ", trainingtypeid " & _
                   ") VALUES (" & _
                   "  @employeeunkid " & _
                   ", @coursemasterunkid " & _
                   ", @instituteunkid " & _
                   ", @trainingmodeunkid " & _
                   ", @training_venue " & _
                   ", @training_startdate " & _
                   ", @training_enddate " & _
                   ", @userunkid " & _
                   ", @isvoid " & _
                   ", @voiddatetime " & _
                   ", @voiduserunkid " & _
                   ", @voidreason " & _
                   ", @additonal_comments" & _
                   ", @cancelfrommoduleid " & _
                   ", @iscancel " & _
                   ", @canceluserunkid " & _
                   ", @cancel_remark " & _
                   ", @cancel_datetime " & _
                   ", @crmasterunkid " & _
                   ", @applydate " & _
                   ", @trainingtypeid " & _
                   "); SELECT @@identity"
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintRequisitionmasterunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

            If dtRequisition IsNot Nothing Then
                objRequisitionTran._TrainingRequisitionMasterId(objDataOperation) = mintRequisitionmasterunkid
                objRequisitionTran._DataTable = dtRequisition
                objRequisitionTran._AuditDatetime = mdtAuditDatetime
                objRequisitionTran._AuditUserId = mintAuditUserId
                objRequisitionTran._ClientIP = mstrClientIP
                objRequisitionTran._FormName = mstrFormName
                objRequisitionTran._HostName = mstrHostName
                objRequisitionTran._IsFromWeb = mblnIsFromWeb
                objRequisitionTran._LoginEmployeeId = mintLoginEmployeeId
                If objRequisitionTran.InsertUpdateDelete_TrainingRequisition(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If InsertAuditTrailTrainingRequisition(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_requisition_master) </purpose>
    Public Function Update(ByVal dtRequisition As DataTable) As Boolean
        If isExist(mintEmployeeunkid, mintCoursemasterunkid, mdtTraining_Startdate, mdtTraining_Enddate, mintInstituteunkid, mintRequisitionmasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Training requisition for selected course, startdate, enddate and employee already present. Please add new training requisition.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequisitionmasterunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoursemasterunkid.ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingmodeunkid.ToString)
            objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraining_Venue.ToString)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Startdate)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Enddate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@additonal_comments", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrAdditonal_Comments.ToString)
            objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelUserId)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancelRemark.ToString)
            If mdtCancelDateTime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancelDateTime)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRequestMasterId)
            objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplyDate)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeid) 'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            strQ = "UPDATE hrtraining_requisition_master SET " & _
                   "  employeeunkid = @employeeunkid" & _
                   ", coursemasterunkid = @coursemasterunkid" & _
                   ", instituteunkid = @instituteunkid" & _
                   ", trainingmodeunkid = @trainingmodeunkid" & _
                   ", training_venue = @training_venue" & _
                   ", training_startdate = @training_startdate" & _
                   ", training_enddate = @training_enddate" & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", additonal_comments = @additonal_comments " & _
                   ", cancelfrommoduleid = @cancelfrommoduleid " & _
                   ", iscancel = @iscancel " & _
                   ", canceluserunkid  = @canceluserunkid " & _
                   ", cancel_remark = @cancel_remark " & _
                   ", cancel_datetime = @cancel_datetime " & _
                   ", crmasterunkid = @crmasterunkid " & _
                   ", applydate = @applydate " & _
                   ", trainingtypeid = @trainingtypeid " & _
                   "WHERE requisitionmasterunkid = @requisitionmasterunkid "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dtRequisition IsNot Nothing Then
                objRequisitionTran._TrainingRequisitionMasterId(objDataOperation) = mintRequisitionmasterunkid
                objRequisitionTran._DataTable = dtRequisition
                objRequisitionTran._AuditDatetime = mdtAuditDatetime
                objRequisitionTran._AuditUserId = mintAuditUserId
                objRequisitionTran._ClientIP = mstrClientIP
                objRequisitionTran._FormName = mstrFormName
                objRequisitionTran._HostName = mstrHostName
                objRequisitionTran._IsFromWeb = mblnIsFromWeb
                objRequisitionTran._LoginEmployeeId = mintLoginEmployeeId
                If objRequisitionTran.InsertUpdateDelete_TrainingRequisition(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If InsertAuditTrailTrainingRequisition(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpid As Integer, ByVal intCourseMasterId As Integer, ByVal StDate As DateTime, ByVal EdDate As DateTime, ByVal intInstituteId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOper As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOper Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOper
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "     requisitionmasterunkid " & _
                   "    ,employeeunkid " & _
                   "    ,coursemasterunkid " & _
                   "    ,instituteunkid " & _
                   "    ,trainingmodeunkid " & _
                   "    ,training_venue " & _
                   "    ,training_startdate " & _
                   "    ,training_enddate " & _
                   "    ,userunkid " & _
                   "    ,isvoid " & _
                   "    ,voiddatetime " & _
                   "    ,voiduserunkid " & _
                   "    ,voidreason " & _
                   "    ,additonal_comments " & _
                   "    ,iscancel " & _
                   "    ,canceluserunkid " & _
                   "    ,cancel_remark " & _
                   "    ,cancel_datetime " & _
                   "    ,crmasterunkid " & _
                   "    ,applydate " & _
                   "    ,trainingtypeid " & _
                   "FROM hrtraining_requisition_master " & _
                   "WHERE isvoid = 0 AND iscancel = 0 " & _
                   " AND employeeunkid = @employeeunkid " & _
                   " AND coursemasterunkid = @coursemasterunkid " & _
                   " AND CONVERT(NVARCHAR(8),training_startdate,112) = @training_startdate " & _
                   " AND CONVERT(NVARCHAR(8),training_enddate,112) = @training_enddate " & _
                   " AND instituteunkid = @instituteunkid "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            If intUnkid > 0 Then
                strQ &= " AND requisitionmasterunkid <> @requisitionmasterunkid "
            End If

            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseMasterId)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(StDate).ToString)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(EdDate).ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInstituteId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_appusermapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtraining_requisition_master SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   "WHERE requisitionmasterunkid = @requisitionmasterunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As New DataTable
            objRequisitionTran._TrainingRequisitionMasterId(objDataOperation) = intUnkid
            dtTable = objRequisitionTran._DataTable.Copy()
            If dtTable IsNot Nothing Then

                Dim dRows As DataRow() = dtTable.Select("")
                dRows.ToList.ForEach(Function(x) UpdateRow(x, "D"))
                dtTable.AcceptChanges()

                objRequisitionTran._TrainingRequisitionMasterId(objDataOperation) = mintRequisitionmasterunkid
                objRequisitionTran._DataTable = dtTable
                objRequisitionTran._AuditDatetime = mdtAuditDatetime
                objRequisitionTran._AuditUserId = mintAuditUserId
                objRequisitionTran._ClientIP = mstrClientIP
                objRequisitionTran._FormName = mstrFormName
                objRequisitionTran._HostName = mstrHostName
                objRequisitionTran._IsFromWeb = mblnIsFromWeb
                objRequisitionTran._LoginEmployeeId = mintLoginEmployeeId
                If objRequisitionTran.InsertUpdateDelete_TrainingRequisition(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            _Requisitionmasterunkid(objDataOperation) = intUnkid

            If InsertAuditTrailTrainingRequisition(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailTrainingRequisition(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO athrtraining_requisition_master ( " & _
                   "  tranguid " & _
                   ", requisitionmasterunkid " & _
                   ", employeeunkid " & _
                   ", coursemasterunkid " & _
                   ", instituteunkid " & _
                   ", trainingmodeunkid " & _
                   ", training_venue " & _
                   ", training_startdate " & _
                   ", training_enddate " & _
                   ", additonal_comments" & _
                   ", loginemoployeeunkid " & _
                   ", cancelfrommoduleid " & _
                   ", iscancel " & _
                   ", canceluserunkid " & _
                   ", cancel_remark " & _
                   ", cancel_datetime " & _
                   ", crmasterunkid " & _
                   ", applydate " & _
                   ", trainingtypeid " & _
                   ") VALUES ( " & _
                   "  @tranguid " & _
                   ", @requisitionmasterunkid " & _
                   ", @employeeunkid " & _
                   ", @coursemasterunkid " & _
                   ", @instituteunkid " & _
                   ", @trainingmodeunkid " & _
                   ", @training_venue " & _
                   ", @training_startdate " & _
                   ", @training_enddate " & _
                   ", @additonal_comments" & _
                   ", @loginemoployeeunkid " & _
                   ", @cancelfrommoduleid " & _
                   ", @iscancel " & _
                   ", @canceluserunkid " & _
                   ", @cancel_remark " & _
                   ", @cancel_datetime " & _
                   ", @crmasterunkid " & _
                   ", @applydate " & _
                   ", @trainingtypeid " & _
                   ") "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequisitionmasterunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoursemasterunkid.ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingmodeunkid.ToString)
            objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraining_Venue.ToString)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Startdate)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Enddate)
            objDataOperation.AddParameter("@additonal_comments", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrAdditonal_Comments.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@loginemoployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeId)
            objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelUserId)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancelRemark.ToString)
            If mdtCancelDateTime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancelDateTime)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRequestMasterId)
            objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplyDate)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeid) 'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailTrainingRequisition; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Private Function UpdateRow(ByVal dr As DataRow, ByVal strValue As String) As Boolean
        Try
            dr("AUD") = strValue
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 862, "External")
            Language.setMessage("clsMasterData", 863, "Internal")
            Language.setMessage("clstraining_requisition_approval_master", 101, "Approved")
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Sorry, Training requisition for selected course, startdate, enddate and employee already present. Please add new training requisition.")
            Language.setMessage(mstrModuleName, 3, "NA")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
