﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clstraining_approver_master.vb
'Purpose    :
'Date       :10-Oct-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clstraining_approver_master
    Private Shared ReadOnly mstrModuleName As String = "clstraining_approver_master"
    Dim objTrainingApproverTrans As New clsTraining_Approver_Tran
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintMappingunkid As Integer
    Private mintCalendarunkid As Integer
    Private mintLevelunkid As Integer
    Private mintMapuserunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private mintRoleunkid As Integer
    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    Private mintAllocationId As Integer
    Private mintAllocationunkid As Integer
    'Hemant (09 Feb 2022) -- End

#End Region

#Region " Public variables "

    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    Public pintMappingunkid As Integer
    Public pintCalendarunkid As Integer
    Public pintLevelunkid As Integer
    Public pintMapuserunkid As Integer
    Public pblnIsactive As Boolean = True
    Public pblnIsvoid As Boolean
    Public pintVoiduserunkid As Integer
    Public pdtVoiddatetime As Date
    Public pstrVoidreason As String = String.Empty
    Public pstrFormName As String = ""
    Public pstrClientIP As String = ""
    Public pstrHostName As String = ""
    Public pblnIsFromWeb As Boolean = False
    Public pintAuditUserId As Integer = 0
    Public pdtAuditDatetime As DateTime = Nothing
    Public pintRoleunkid As Integer
    Public pintAllocationId As Integer
    Public pintAllocationunkid As Integer
    'Hemant (09 Feb 2022) -- End

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid(Optional ByVal objOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
            Call GetData(objOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calendarunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Calendarunkid() As Integer
        Get
            Return mintCalendarunkid
        End Get
        Set(ByVal value As Integer)
            mintCalendarunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roleunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Roleunkid() As Integer
        Get
            Return mintRoleunkid
        End Get
        Set(ByVal value As Integer)
            mintRoleunkid = value
        End Set
    End Property

    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    ''' <summary>
    ''' Purpose: Get or Set allocationid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _AllocationId() As Integer
        Get
            Return mintAllocationId
        End Get
        Set(ByVal value As Integer)
            mintAllocationId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Allocationunkid() As Integer
        Get
            Return mintAllocationunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationunkid = value
        End Set
    End Property

    Private mlstTrainingApproverNew As List(Of clstraining_approver_master)
    Public WriteOnly Property _lstTrainingApproverNew() As List(Of clstraining_approver_master)
        Set(ByVal value As List(Of clstraining_approver_master))
            mlstTrainingApproverNew = value
        End Set
    End Property

    Private mlstTrainingApproverVoid As List(Of clstraining_approver_master)
    Public WriteOnly Property _lstTrainingApproverVoid() As List(Of clstraining_approver_master)
        Set(ByVal value As List(Of clstraining_approver_master))
            mlstTrainingApproverVoid = value
        End Set
    End Property

    Private mDataOp As clsDataOperation = Nothing
    Public Property _DataOp() As clsDataOperation
        Get
            Return mDataOp
        End Get
        Set(ByVal value As clsDataOperation)
            mDataOp = value
        End Set
    End Property
    'Hemant (09 Feb 2022) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  mappingunkid " & _
              ", calendarunkid " & _
              ", levelunkid " & _
              ", mapuserunkid " & _
              ", isactive " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", roleunkid " & _
              ", ISNULL(allocationid, 0) AS allocationid " & _
              ", ISNULL(allocationunkid, 0) AS allocationunkid " & _
             "FROM hrtraining_approver_master " & _
             "WHERE mappingunkid = @mappingunkid "

            'Hemant (09 Feb 2022) -- [allocationid,allocationunkid]
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintCalendarunkid = CInt(dtRow.Item("calendarunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
                'Hemant (09 Feb 2022) -- Start            
                'OLD-549(NMB) : Give new screen for training approver allocation mapping
                mintAllocationId = CInt(dtRow.Item("allocationid"))
                mintAllocationunkid = CInt(dtRow.Item("allocationunkid"))
                'Hemant (09 Feb 2022) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                          , Optional ByVal blnOnlyActive As Boolean = True _
                          , Optional ByVal mstrFilter As String = "" _
                          , Optional ByVal intApproverUserUnkId As Integer = 0 _
                            , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                            , Optional ByVal blnAddSelect As Boolean = False _
                            , Optional ByVal intRoleUnkId As Integer = 0 ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim dsAllocation As DataSet = (New clsMasterData).GetEAllocation_Notification("AList", "", False, False)

            If blnAddSelect Then
                strQ = "SELECT " & _
                        "  0 AS mappingunkid " & _
                        " ,0 AS calendarunkid " & _
                        " ,0 AS levelunkid " & _
                        " ,'' AS LevelCode " & _
                        " ,'' AS  Level " & _
                        " ,0 AS mapuserunkid " & _
                        " ,@Select AS approver " & _
                        " ,0 AS isactive " & _
                        " ,'' As Status " & _
                        " ,0 AS isvoid " & _
                        " ,0 AS voiduserunkid " & _
                        " ,NULL AS voiddatetime " & _
                        " ,'' AS voidreason " & _
                        " ,0 AS priority " & _
                        " ,'' as Calendar " & _
                        " ,0 AS roleunkid " & _
                        " ,'' AS role " & _
                        " ,0 AS allocationid " & _
                        " ,'' AS allocationname " & _
                        " ,0 AS allocationunkid " & _
                        " ,'' AS Branch " & _
                        " ,'' AS DepartmentGroup " & _
                        " ,'' AS Department " & _
                        " ,'' AS SectionGroup " & _
                        " ,'' AS Section " & _
                        " ,'' AS UnitGroup " & _
                        " ,'' AS Unit " & _
                        " ,'' AS Team " & _
                        " ,'' AS JobGroup " & _
                        " ,'' AS Job " & _
                        " ,'' AS ClassGroup " & _
                        " ,'' AS Class " & _
                        " ,'' AS allocationitem " & _
                        "UNION ALL "
            End If
            strQ &= "SELECT " & _
                    "     TAM.mappingunkid " & _
                    "    ,TAM.calendarunkid " & _
                    "    ,TAM.levelunkid " & _
                    "    ,ISNULL(TLM.levelcode,'') as LevelCode " & _
                    "    ,ISNULL(TLM.levelname,'') as Level " & _
                    "    ,TAM.mapuserunkid " & _
                    "    ,ISNULL(UM.username,'') AS approver " & _
                    "    ,TAM.isactive " & _
                    "    ,CASE WHEN TAM.isactive = 0 THEN @InActive ELSE @Active END As Status " & _
                    "    ,TAM.isvoid " & _
                    "    ,TAM.voiduserunkid " & _
                    "    ,TAM.voiddatetime " & _
                    "    ,TAM.voidreason " & _
                    "    ,ISNULL(TLM.priority,0) AS priority " & _
                    "    ,ISNULL(TCM.calendar_name , '') AS calendar " & _
                    "    ,TAM.roleunkid " & _
                    "    ,ISNULL(RM.name,'') as role " & _
                    "    ,TAM.allocationid " & _
                    "    ,CASE ISNULL(TAM.allocationid, 0) "

            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
            Next

            strQ &= " END AS allocationname " & _
                    "    ,TAM.allocationunkid " & _
                    "    ,ISNULL(hrstation_master.name, '') AS Branch " & _
                    "    ,ISNULL(hrdepartment_group_master.name, '') AS DepartmentGroup " & _
                    "    ,ISNULL(hrdepartment_master.name, '') AS Department " & _
                    "    ,ISNULL(hrsectiongroup_master.name, '') AS SectionGroup " & _
                    "    ,ISNULL(hrsection_master.name, '') AS Section " & _
                    "    ,ISNULL(hrunitgroup_master.name, '') AS UnitGroup " & _
                    "    ,ISNULL(hrunit_master.name, '') AS Unit " & _
                    "    ,ISNULL(hrteam_master.name, '') AS Team " & _
                    "    ,ISNULL(hrjobgroup_master.name, '') AS JobGroup " & _
                    "    ,ISNULL(hrjob_master.job_name, '') AS Job " & _
                    "    ,ISNULL(hrclassgroup_master.name, '') AS ClassGroup " & _
                    "    ,ISNULL(hrclasses_master.name, '') AS Class " & _
                    "   ,'' AS allocationitem " & _
                    "FROM hrtraining_approver_master AS TAM " & _
                    "    JOIN hrtraining_approverlevel_master AS TLM ON TAM.levelunkid = TLM.levelunkid " & _
                    "    LEFT JOIN hrmsconfiguration..cfuser_master AS UM ON UM.userunkid = TAM.mapuserunkid " & _
                    "    LEFT JOIN trtraining_calendar_master as TCM ON TCM.calendarunkid = TAM.calendarunkid " & _
                    "    LEFT JOIN hrmsConfiguration..cfrole_master AS RM ON RM.roleunkid = TAM.roleunkid " & _
                    "    LEFT JOIN hrstation_master ON hrstation_master.stationunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrunit_master ON hrunit_master.unitunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrteam_master ON hrteam_master.teamunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrjob_master ON hrjob_master.jobunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = TAM.allocationunkid " & _
                    "    LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = TAM.allocationunkid " & _
                    "WHERE TAM.isvoid = 0 AND TLM.isactive = 1 " & _
                    " AND TAM.roleunkid > 0 "
            'Hemant (09 Feb 2022) -- [allocationid,allocation,allocationunkid,allocationitem]

            If intApproverUserUnkId > 0 Then
                strQ &= " AND TAM.mapuserunkid = @mapuserunkid "
            End If

            If intRoleUnkId > 0 Then
                strQ &= " AND TAM.roleunkid = @roleunkid "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOperation.AddParameter("@InActive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "In Active"))
            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Active"))
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRoleUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(strTableName).Rows
                Select Case CInt(dtRow.Item("allocationid"))
                    Case enAllocation.BRANCH
                        dtRow.Item("allocationitem") = dtRow.Item("Branch")
                    Case enAllocation.DEPARTMENT_GROUP
                        dtRow.Item("allocationitem") = dtRow.Item("DepartmentGroup")
                    Case enAllocation.DEPARTMENT
                        dtRow.Item("allocationitem") = dtRow.Item("Department")
                    Case enAllocation.SECTION_GROUP
                        dtRow.Item("allocationitem") = dtRow.Item("SectionGroup")
                    Case enAllocation.SECTION
                        dtRow.Item("allocationitem") = dtRow.Item("Section")
                    Case enAllocation.UNIT_GROUP
                        dtRow.Item("allocationitem") = dtRow.Item("UnitGroup")
                    Case enAllocation.UNIT
                        dtRow.Item("allocationitem") = dtRow.Item("Unit")
                    Case enAllocation.TEAM
                        dtRow.Item("allocationitem") = dtRow.Item("Team")
                    Case enAllocation.JOB_GROUP
                        dtRow.Item("allocationitem") = dtRow.Item("JobGroup")
                    Case enAllocation.JOBS
                        dtRow.Item("allocationitem") = dtRow.Item("Job")
                    Case enAllocation.CLASS_GROUP
                        dtRow.Item("allocationitem") = dtRow.Item("ClassGroup")
                    Case enAllocation.CLASSES
                        dtRow.Item("allocationitem") = dtRow.Item("Class")
                End Select
            Next

            dsList.Tables(strTableName).AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_approver_master) </purpose>
    Public Function Insert(Optional ByVal mdtran As DataTable = Nothing, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (09 Feb 2022) -- [xDataOp]

        If isExist(mintCalendarunkid, mintMapuserunkid, mintLevelunkid, mintRoleunkid, mintAllocationId, mintAllocationunkid, -1, xDataOp) > 0 Then
            'Hemant (09 Feb 2022) -- [mintAllocationId,mintAllocationunkid,xDataOp]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Training Approver already exists for selected level. Please define new Training Approver.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Hemant (09 Feb 2022) -- Start            
        'OLD-549(NMB) : Give new screen for training approver allocation mapping
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            'Hemant (09 Feb 2022) -- End
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If 'Hemant (09 Feb 2022) 

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationunkid.ToString)
            'Hemant (09 Feb 2022) -- End

            strQ = "INSERT INTO hrtraining_approver_master ( " & _
                   "  calendarunkid " & _
                   ", levelunkid " & _
                   ", mapuserunkid " & _
                   ", isactive " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason" & _
                   ", roleunkid " & _
                   ", allocationid " & _
                   ", allocationunkid " & _
                   ") VALUES (" & _
                   "  @calendarunkid " & _
                   ", @levelunkid " & _
                   ", @mapuserunkid " & _
                   ", @isactive " & _
                   ", @isvoid " & _
                   ", @voiduserunkid " & _
                   ", @voiddatetime " & _
                   ", @voidreason" & _
                   ", @roleunkid " & _
                   ", @allocationid " & _
                   ", @allocationunkid " & _
                   "); SELECT @@identity"
            'Hemant (09 Feb 2022) -- [allocationid,allocationunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMappingunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

            If InsertAuditTrailApproverMapping(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtran IsNot Nothing Then
            objTrainingApproverTrans._TrainingApproverunkid = mintMappingunkid
            objTrainingApproverTrans._TranDataTable = mdtran
            objTrainingApproverTrans._WebFormName = mstrFormName
            If objTrainingApproverTrans._TranDataTable IsNot Nothing AndAlso objTrainingApproverTrans._TranDataTable.Rows.Count > 0 Then

                If objTrainingApproverTrans.Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            End If

            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (09 Feb 2022) -- End

            Return True
        Catch ex As Exception
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objDataOperation.ReleaseTransaction(False)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (09 Feb 2022) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (09 Feb 2022) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_appusermapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "INSERT INTO attrtraining_approver_tran ( " & _
                      "  tranguid" & _
                      ", trapprovertranunkid " & _
                      ", trapproverunkid " & _
                      ", employeeunkid " & _
                      ", audittypeid " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", form_name " & _
                      ", ip " & _
                      ", host " & _
                      ", isweb " & _
                     " )" & _
                     "SELECT " & _
                      "  LOWER(NEWID()) " & _
                      ", trapprovertranunkid " & _
                      ", trapproverunkid " & _
                      ", employeeunkid " & _
                      ", 3 " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @form_name " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @isweb " & _
                 "FROM trtraining_approver_tran " & _
                      "WHERE isvoid = 0 " & _
                       "AND trapproverunkid = @trapproverunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE hrtraining_approver_master SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   "WHERE mappingunkid = @mappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = intUnkid

            If InsertAuditTrailApproverMapping(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xCalendarId As Integer, ByVal xMapUserId As Integer, ByVal xLevelId As Integer, _
                            ByVal xRoleId As Integer, ByVal xAllocationId As Integer, ByVal xAllocationUnkId As Integer, _
                            Optional ByVal intunkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing _
                            ) As Integer
        'Hemant (09 Feb 2022) -- [xAllocationId,xAllocationUnkId,xDataOpr]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (09 Feb 2022) -- Start            
        'OLD-549(NMB) : Give new screen for training approver allocation mapping
        Dim intRetUnkId As Integer = 0
        If xDataOpr Is Nothing Then
            'Hemant (09 Feb 2022) -- End
        objDataOperation = New clsDataOperation
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
        Else
            objDataOperation = xDataOpr
        End If
        'Hemant (09 Feb 2022) -- End

        Try
            strQ = "SELECT " & _
                   "  mappingunkid " & _
                   " ,calendarunkid " & _
                   " ,levelunkid " & _
                   " ,mapuserunkid " & _
                   " ,isactive " & _
                   " ,isvoid " & _
                   " ,voiduserunkid " & _
                   " ,voiddatetime " & _
                   " ,voidreason " & _
                   " ,roleunkid " & _
                   " ,allocationid " & _
                   " ,allocationunkid " & _
                   "FROM hrtraining_approver_master " & _
                   "WHERE isvoid = 0  " & _
                   " AND calendarunkid = @calendarunkid " & _
                   " AND mapuserunkid = @mapuserunkid " & _
                   " AND levelunkid = @levelunkid " & _
                   " AND roleunkid = @roleunkid " & _
                   " AND allocationid = @allocationid " & _
                   " AND allocationunkid = @allocationunkid "
            'Hemant (09 Feb 2022) -- [allocationid,allocationunkid]

            objDataOperation.ClearParameters()

            If intunkid > 0 Then
                strQ &= " AND mappingunkid <> @mappingunkid"
                objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If

            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCalendarId)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMapUserId)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLevelId)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRoleId)
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationUnkId)
            'Hemant (09 Feb 2022) -- End
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRetUnkId = CInt(dsList.Tables(0).Rows(0).Item("mappingunkid"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Hemant (09 Feb 2022) -- End
        End Try
        Return intRetUnkId
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InActiveApprover(ByVal xMappingId As Integer, ByVal intCompanyID As Integer, ByVal blnIsActive As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If blnIsActive Then
                strQ = " UPDATE hrtraining_approver_master set isactive = 1 where mappingunkid = @mappingunkid AND isvoid = 0  "
            Else
                strQ = " UPDATE hrtraining_approver_master set isactive = 0 where mappingunkid = @mappingunkid AND isvoid = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMappingId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = xMappingId

            If InsertAuditTrailApproverMapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InActiveApprover; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function GetEmployeeListByApprover(ByVal xDatabaseName As String, _
                                              ByVal xUserUnkid As Integer, _
                                              ByVal xYearUnkid As Integer, _
                                              ByVal xCompanyUnkid As Integer, _
                                              ByVal xEmpAsOnDate As DateTime, _
                                              ByVal xUserModeSetting As String, _
                                              ByVal xOnlyApproved As Boolean, _
                                              ByVal strListName As String, _
                                              ByVal mblnAddSelect As Boolean, _
                                              Optional ByVal strFilterQuery As String = "", _
                                              Optional ByVal blnReinstatementDate As Boolean = False, _
                                              Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                              Optional ByVal blnAddApprovalCondition As Boolean = True) As DataSet
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsList = objEmp.GetEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xEmpAsOnDate, xEmpAsOnDate, xUserModeSetting, xOnlyApproved, False, strListName, mblnAddSelect, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, strFilterQuery, blnReinstatementDate, blnIncludeAccessFilterQry, blnAddApprovalCondition)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeListByApprover; Module Name: " & mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
        Return dsList
    End Function

    Public Function InsertAuditTrailApproverMapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO athrtraining_approver_master ( " & _
                   "  tranguid " & _
                   ", mappingunkid " & _
                   ", calendarunkid " & _
                   ", levelunkid " & _
                   ", mapuserunkid " & _
                   ", isactive " & _
                   ", audittype " & _
                   ", audituserunkid " & _
                   ", auditdatetime " & _
                   ", ip" & _
                   ", hostname" & _
                   ", form_name " & _
                   ", isweb " & _
                   ", roleunkid " & _
                   ", allocationid " & _
                   ", allocationunkid " & _
                   ") VALUES (" & _
                   "  @tranguid " & _
                   ", @mappingunkid " & _
                   ", @calendarunkid " & _
                   ", @levelunkid " & _
                   ", @mapuserunkid " & _
                   ", @isactive " & _
                   ", @audittype " & _
                   ", @audituserunkid " & _
                   ", @auditdatetime " & _
                   ", @ip" & _
                   ", @hostname" & _
                   ", @form_name " & _
                   ", @isweb " & _
                   ", @roleunkid " & _
                   ", @allocationid " & _
                   ", @allocationunkid " & _
                   ") "
            'Hemant (09 Feb 2022) -- [allocationid,allocationunkid]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtAuditDatetime <> Nothing, mdtAuditDatetime, DBNull.Value))
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationunkid.ToString)
            'Hemant (09 Feb 2022) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailApproverMapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Public Function GetApproverEmployeeId(ByVal CalendarID As Integer, ByVal ApproveID As Integer) As String
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
                               "+ CAST(trtraining_approver_tran.employeeunkid AS VARCHAR(MAX)) " & _
                      "FROM     hrtraining_approver_master " & _
                                "JOIN trtraining_approver_tran ON hrtraining_approver_master.mappingunkid = trtraining_approver_tran.trapproverunkid AND trtraining_approver_tran.isvoid = 0" & _
                      "WHERE    hrtraining_approver_master.calendarunkid = " & CalendarID & " " & _
                                " AND hrtraining_approver_master.mapuserunkid = " & ApproveID & " " & _
                                " AND hrtraining_approver_master.isvoid = 0 " & _
                                " AND hrtraining_approver_master.isactive = 1 " & _
                      "ORDER BY trtraining_approver_tran.employeeunkid " & _
                    "FOR " & _
                      "XML PATH('') " & _
                    "), 1, 1, ''), '') EmployeeIds "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrModuleName)
        End Try
        Return mstrEmployeeIds
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Update Database Table hrtraining_approver_master </purpose>
    Public Function Update(Optional ByVal mdtran As DataTable = Nothing, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (09 Feb 2022) -- [xDataOp]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mintCalendarunkid, mintMapuserunkid, mintLevelunkid, mintRoleunkid, mintAllocationId, mintAllocationunkid, mintMappingunkid, xDataOp) > 0 Then
            'Hemant (09 Feb 2022) -- [mintAllocatioid,mintAllocationunkid,xDataOp]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Training Approver already exists. Please define new Training Approver.")
            Return False
        End If

        'Hemant (09 Feb 2022) -- Start            
        'OLD-549(NMB) : Give new screen for training approver allocation mapping
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            'Hemant (09 Feb 2022) -- End
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If 'Hemant (09 Feb 2022)

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationunkid.ToString)
            'Hemant (09 Feb 2022) -- End

            strQ = "UPDATE hrtraining_approver_master  SET " & _
                    " calendarunkid = @calendarunkid " & _
                    " ,levelunkid = @levelunkid " & _
                    " ,mapuserunkid = @mapuserunkid " & _
                    " ,isactive = @isactive " & _
                    " ,isvoid = @isvoid " & _
                    " ,voiddatetime = @voiddatetime " & _
                    " ,voiduserunkid = @voiduserunkid " & _
                    " ,voidreason = @voidreason " & _
                    " ,roleunkid = @roleunkid " & _
                    " ,allocationid = @allocationid " & _
                    " ,allocationunkid = @allocationunkid " & _
                " WHERE mappingunkid = @mappingunkid"
            'Hemant (09 Feb 2022) -- [allocationid,allocationunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailApproverMapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtran IsNot Nothing Then
            objTrainingApproverTrans._TrainingApproverunkid = mintMappingunkid
            objTrainingApproverTrans._TranDataTable = mdtran
            objTrainingApproverTrans._UserId = mintAuditUserId
            objTrainingApproverTrans._WebClientIP = mstrClientIP
            objTrainingApproverTrans._WebFormName = mstrFormName
            objTrainingApproverTrans._WebHostName = mstrHostName

            If objTrainingApproverTrans.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If

            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (09 Feb 2022) -- End

            Return True
        Catch ex As Exception
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objDataOperation.ReleaseTransaction(False)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (09 Feb 2022) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (09 Feb 2022) -- End
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    Public Function GetEmployeeApprover(ByVal xDatabaseName As String, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal xYearUnkid As Integer, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal intEmployeeID As Integer, _
                                        ByVal intMaxPriority As Integer, _
                                        Optional ByVal mstrFilter As String = "" _
                                        ) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            objDataOperation.ClearParameters()

            strQ &= " SELECT " & _
                      "  hrtraining_approver_master.mapuserunkid " & _
                      ", trtraining_approver_tran.trapproverunkid " & _
                      ", trtraining_approver_tran.employeeunkid " & _
                      ", hrtraining_approverlevel_master.levelunkid " & _
                      ", hrtraining_approverlevel_master.levelname " & _
                      ", hrtraining_approverlevel_master.priority " & _
                      ", hrtraining_approver_master.roleunkid " & _
                      ", hrtraining_approver_master.allocationid " & _
                      ", hrtraining_approver_master.allocationunkid " & _
                      " FROM trtraining_approver_tran " & _
                      " JOIN hrtraining_approver_master ON trtraining_approver_tran.trapproverunkid = hrtraining_approver_master.mappingunkid " & _
                      "     AND hrtraining_approver_master.isvoid = 0 AND hrtraining_approver_master.isactive = 1 " & _
                      " JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = hrtraining_approver_master.levelunkid "
            'Hemant (09 Feb 2022) -- [allocationid,allocationunkid]

            strQFinal = strQ

            strQCondition = " WHERE trtraining_approver_tran.isvoid = 0  AND trtraining_approver_tran.employeeunkid = " & intEmployeeID


            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter.Trim
            End If

            strQ &= strQCondition

            objDataOperation.ClearParameters()

            Dim blnTrainingRequireForForeignTravelling As Boolean = False
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = xCompanyUnkid
            blnTrainingRequireForForeignTravelling = objConfig._TrainingRequireForForeignTravelling
            objConfig = Nothing

            If blnTrainingRequireForForeignTravelling = True Then
                If intMaxPriority > 0 Then
                    strQ &= "  AND hrtraining_approverlevel_master.priority = @priority "
                    objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intMaxPriority.ToString)
                End If
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            'For Each dRow As DataRow In dsCompany.Tables("Company").Rows
            '    strQ = strQFinal

            '    strQ &= strQCondition
            '    strQ &= " AND cfuser_master.companyunkid = " & CInt(dRow.Item("companyunkid")) & ""

            '    dsExtList = objDataOperation.ExecQuery(strQ, "List")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    If dsList.Tables.Count <= 0 Then
            '        dsList.Tables.Add(dsExtList.Tables("List").Copy)
            '    Else
            '        dsList.Tables("List").Merge(dsExtList.Tables("List"), True)
            '    End If
            'Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeApprover", mstrMessage)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
            Return dsList.Tables("List")
        Else
            Return Nothing
        End If
    End Function

    Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
                                          , ByVal intCompanyId As Integer _
                                          , ByVal intYearId As Integer _
                                          , ByVal strUserAccessMode As String _
                                          , ByVal intPrivilegeId As Integer _
                                          , ByVal xEmployeeAsOnDate As String _
                                          , ByVal intUserId As Integer _
                                          , ByVal intEmployeeID As Integer _
                                          , ByVal decCostAmount As Decimal _
                                              , ByVal intCalendarId As Decimal _
                                              , ByVal intTrainingApproverAllocationID As Decimal _
                                          , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                          , Optional ByVal mstrFilterString As String = "" _
                                          ) As DataTable
        'Hemant (09 Feb 2022) -- [intTrainingApproverAllocationID]
        'Hemant (03 Dec 2021) -- [intCalendarId]
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""

            'Hemant (16 Nov 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-520(Finca Uganda) - Training Notifications are being sent Even to Inactive.
            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(xEmployeeAsOnDate), eZeeDate.convertDate(xEmployeeAsOnDate), , , strDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(xEmployeeAsOnDate), strDatabaseName)
            'Hemant (16 Nov 2021) -- End

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                 "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQ &= "SELECT DISTINCT " & _
                        "    B" & index.ToString() & ".userunkid AS mapuserunkid " & _
                        "   ,A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                        "   AND T.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                        "   AND J.Rno = 1 " & _
                        ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = " & intCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "

                'Hemant (09 Feb 2022) -- Start            
                'OLD-549(NMB) : Give new screen for training approver allocation mapping
                StrQ &= " AND A.employeeunkid = " & intEmployeeID & " "
                'Hemant (09 Feb 2022) -- End
                If index < strvalues.Length - 1 Then
                    StrQ &= " INTERSECT "
                End If
            Next

            StrQ &= ") AS Fl "

            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            StrQ &= "SELECT " & _
                       "    mapuserunkid " & _
                       ",   employeeunkid " & _
                       ",   roleunkid " & _
                       ",   levelunkid " & _
                       ",   priority " & _
                       ",   trapproverunkid " & _
                       ",   costamountfrom " & _
                       ",   costamountto " & _
                    "  FROM ( "
            'Hemant (09 Feb 2022) -- End

            StrQ &= "SELECT " & _
                        "#USR.mapuserunkid " & _
                       ",#USR.employeeunkid " & _
                       ",UM.roleunkid " & _
                       ",TAM.levelunkid " & _
                       ",TLM.priority " & _
                       ",TAM.mappingunkid trapproverunkid " & _
                       ",TAMX.costamountfrom " & _
                       ",TAMX.costamountto " & _
                       ",DENSE_RANK() OVER (PARTITION BY #USR.mapuserunkid ORDER BY TAM.roleunkid ASC, TLM.priority ASC, TAM.levelunkid ASC, TAM.mappingunkid ASC) AS rowno "
            StrQ &= "FROM #USR " & _
                    "JOIN hrmsConfiguration..cfuser_master UM    ON UM.userunkid = #USR.mapuserunkid " & _
                    "JOIN hrtraining_approver_master TAM    ON TAM.roleunkid = UM.roleunkid " & _
                            "AND TAM.isvoid = 0 AND TAM.isactive = 1 "
            'Hemant (09 Feb 2022) -- [rowno]
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            StrQ &= " AND TAM.calendarunkid = @calendarunkid "
            'Hemant (03 Dec 2021) -- End

            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            StrQ &= " AND TAM.allocationid = " & intTrainingApproverAllocationID & " "
            StrQ &= " JOIN (  " & _
                    "        SELECT DISTINCT " & _
                    "               userunkid " & _
                    "               ,referenceunkid " & _
                    "               ,allocationunkid " & _
                    "       FROM hrmsConfiguration..cfuseraccess_privilege_master UPM " & _
                    "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT	ON UPT.useraccessprivilegeunkid = UPM.useraccessprivilegeunkid " & _
                    "       ) X  ON X.userunkid = #USR.mapuserunkid " & _
                    "       AND X.referenceunkid = TAM.allocationid " & _
                    "       AND X.allocationunkid = TAM.allocationunkid "
            'Hemant (09 Feb 2022) -- End

            StrQ &= "JOIN hrtraining_approverlevel_master TLM ON TLM.levelunkid = TAM.levelunkid AND TLM.isactive = 1 "
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            StrQ &= " AND TLM.calendarunkid = @calendarunkid "
            'Hemant (03 Dec 2021) -- End
            StrQ &= "JOIN trtraining_approval_matrix TAMX ON TAMX.levelunkid = TLM.levelunkid AND TAMX.isvoid = 0 "
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            StrQ &= " AND TAMX.calendarunkid = @calendarunkid "
            'Hemant (03 Dec 2021) -- End

            StrQ &= "    JOIN " & _
                   "    ( " & _
                   "        SELECT DISTINCT " & _
                   "             cfuser_master.userunkid " & _
                   "            ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
                   "            ,cfuser_master.email " & _
                   "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                   "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                   "        FROM hrmsConfiguration..cfuser_master " & _
                   "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                   "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                   "        WHERE cfuser_master.isactive = 1 " & _
                    "            AND yearunkid = @Y AND privilegeunkid = @P  " & _
                    ") AS Fn ON #USR.mapuserunkid = Fn.userunkid  "

            'Hemant (16 Nov 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-520(Finca Uganda) - Training Notifications are being sent Even to Inactive.
            StrQ &= " LEFT JOIN hrmsConfiguration..cfuser_master on cfuser_master.userunkid = #USR.mapuserunkid " & _
                    " LEFT JOIN hremployee_master UserEmp ON UserEmp.employeeunkid = hrmsConfiguration..cfuser_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry.Replace("hremployee_master", "UserEmp")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry.Replace("hremployee_master", "UserEmp")
            End If
            'Hemant (16 Nov 2021) -- End

            StrQ &= "WHERE #USR.employeeunkid = " & intEmployeeID & " " & _
                    "AND UM.roleunkid > 0 "

            If decCostAmount >= 0 Then
                StrQ &= " AND TAMX.costamountfrom <= @costamount "
                objDataOperation.AddParameter("@costamount", SqlDbType.Int, eZeeDataType.DECIMAL_SIZE, decCostAmount)
            End If

            'Hemant (16 Nov 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-520(Finca Uganda) - Training Notifications are being sent Even to Inactive.
            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry.Replace("hremployee_master", "UserEmp")
            End If
            'Hemant (16 Nov 2021) -- End

            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            StrQ &= " ) AS Y " & _
                    " WHERE Y.rowno = 1 "
            'Hemant (09 Feb 2022) -- End

            StrQ &= " DROP TABLE #USR "


            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalendarId)
            'Hemant (03 Dec 2021) -- End
            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)


            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            dtList = dsList.Tables("List").Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dtList
    End Function

    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    Public Function SaveAll(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            For Each clsTrainingApprover As clstraining_approver_master In mlstTrainingApproverNew
                With clsTrainingApprover
                    mintMappingunkid = .pintMappingunkid
                    mintCalendarunkid = .pintCalendarunkid
                    mintLevelunkid = .pintLevelunkid
                    mintMapuserunkid = .pintMapuserunkid
                    mblnIsactive = .pblnIsactive
                    mblnIsvoid = .pblnIsvoid
                    mintVoiduserunkid = .pintVoiduserunkid
                    mdtVoiddatetime = .pdtVoiddatetime
                    mstrVoidreason = .pstrVoidreason
                    mstrFormName = .pstrFormName
                    mstrClientIP = .pstrClientIP
                    mstrHostName = .pstrHostName
                    mblnIsFromWeb = .pblnIsFromWeb
                    mintAuditUserId = .pintAuditUserId
                    mdtAuditDatetime = .pdtAuditDatetime
                    mintRoleunkid = .pintRoleunkid
                    mintAllocationId = .pintAllocationId
                    mintAllocationunkid = .pintAllocationunkid

                    mintMappingunkid = isExist(mintCalendarunkid, mintMapuserunkid, mintLevelunkid, mintRoleunkid, mintAllocationId, mintAllocationunkid, , objDataOperation)
                    If mintMappingunkid <= 0 Then
                        If Insert(, objDataOperation) = False Then
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    Else
                        If Update(, objDataOperation) = False Then
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    End If

                End With
            Next
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            For Each clsTrainingApprover As clstraining_approver_master In mlstTrainingApproverVoid

                With clsTrainingApprover

                    _Mappingunkid(objDataOperation) = .pintMappingunkid

                    mblnIsvoid = .pblnIsvoid
                    mintVoiduserunkid = .pintVoiduserunkid
                    mdtVoiddatetime = .pdtVoiddatetime
                    mstrVoidreason = .pstrVoidreason

                    mblnIsFromWeb = .pblnIsFromWeb
                    mintAuditUserId = .pintAuditUserId
                    mdtAuditDatetime = .pdtAuditDatetime
                    mstrClientIP = .pstrClientIP
                    mstrHostName = .pstrHostName
                    mstrFormName = .pstrFormName

                    If Void(mintMappingunkid, objDataOperation) = False Then
                        If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        Exit For
                    End If

                End With

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Void(ByVal intUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hrtraining_approver_master SET " & _
              "  isvoid = @isvoid " & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
            "WHERE mappingunkid = @mappingunkid "

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = intUnkid

            If InsertAuditTrailApproverMapping(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Save(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            If mlstTrainingApproverVoid IsNot Nothing AndAlso mlstTrainingApproverVoid.Count > 0 Then
                If VoidAll(objDataOperation) = False Then
                    Return False
                End If
            End If

            If mlstTrainingApproverNew IsNot Nothing AndAlso mlstTrainingApproverNew.Count > 0 Then
                If SaveAll(objDataOperation) = False Then
                    Return False
                End If
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Hemant (09 Feb 2022) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This user is already map with another level or same level.please select new user to map with this level.")
			Language.setMessage(mstrModuleName, 2, "In Active")
			Language.setMessage(mstrModuleName, 3, "Active")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
