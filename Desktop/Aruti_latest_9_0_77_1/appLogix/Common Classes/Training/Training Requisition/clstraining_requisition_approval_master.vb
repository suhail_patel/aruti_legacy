﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clstraining_requisition_approval_master.vb
'Purpose    :
'Date       :15-Oct-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clstraining_requisition_approval_master
    Private Shared ReadOnly mstrModuleName As String = "clstraining_requisition_approval_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private objRequisitionApprovalTran As New clstraining_requisition_approval_tran
    Private objRequisitionMaster As New clstraining_requisition_master
    Private objRequisitionTran As New clstraining_requisition_tran
    Private objDocument As New clsScan_Attach_Documents
    Private objClaimRequestMaster As New clsclaim_request_master
    Private objClaimRequestTran As New clsclaim_request_tran


#Region " Private variables "

    Private mstrMasterguid As String = String.Empty
    Private mdtTransactiondate As Date = Nothing
    Private mintMappingunkid As Integer = 0
    Private mintLevelunkid As Integer = 0
    Private mintMapuserunkid As Integer = 0
    Private mintStatusunkid As Integer = 0
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean = False
    Private mintRequisitionmasterunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mintCoursemasterunkid As Integer = 0
    Private mintInstituteunkid As Integer = 0
    Private mintTrainingmodeunkid As Integer = 0
    Private mstrTraining_Venue As String = String.Empty
    Private mdtTraining_Startdate As Date = Nothing
    Private mdtTraining_Enddate As Date = Nothing
    Private mstrAdditonal_Comments As String = String.Empty
    Private mintPeriodunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mdtVoiddatetime As Date = Nothing
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mintAudittype As Integer = 0
    Private mintAudituserunkid As Integer = 0
    Private mdtAuditdatetime As Date = Nothing
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean = False
    Private mintLoginemoployeeunkid As Integer = 0
    Private mblnIsprocessed As Boolean = False
    Private mblnIsCancel As Boolean = False
    Private mintCancelFromModuleId As Integer = 0
    Private mintCancelUserId As Integer = 0
    Private mstrCancelRemark As String = String.Empty
    Private mdtCancelDateTime As DateTime = Nothing
    Private mintLinkedMasterId As Integer = 0
    Private mintClaimRequestMasterId As Integer = 0
    Private mobjClaimRequestMaster As New clsclaim_request_master
    Private mdtApplyDate As DateTime = Nothing
    'S.SANDEEP [07-NOV-2018] -- START
    Private mintTrainingTypeid As Integer = 0
    'S.SANDEEP [07-NOV-2018] -- END

#End Region

#Region " ENUM "

    Public Enum enApprovalStatus
        SubmitForApproval = 1
        Approved = 2
        Rejected = 3
    End Enum

    Public Enum enCompletionApprovalStatus
        Pending = 1
        Completed = 2
        Rejected = 3
    End Enum

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set masterguid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Masterguid(Optional ByVal objOperation As clsDataOperation = Nothing) As String
        Get
            Return mstrMasterguid
        End Get
        Set(ByVal value As String)
            mstrMasterguid = value
            Call GetData(objOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set requisitionmasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Requisitionmasterunkid() As Integer
        Get
            Return mintRequisitionmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintRequisitionmasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coursemasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Coursemasterunkid() As Integer
        Get
            Return mintCoursemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCoursemasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set instituteunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Instituteunkid() As Integer
        Get
            Return mintInstituteunkid
        End Get
        Set(ByVal value As Integer)
            mintInstituteunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingmodeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Trainingmodeunkid() As Integer
        Get
            Return mintTrainingmodeunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingmodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set training_venue
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Training_Venue() As String
        Get
            Return mstrTraining_Venue
        End Get
        Set(ByVal value As String)
            mstrTraining_Venue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set training_startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Training_Startdate() As Date
        Get
            Return mdtTraining_Startdate
        End Get
        Set(ByVal value As Date)
            mdtTraining_Startdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set training_enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Training_Enddate() As Date
        Get
            Return mdtTraining_Enddate
        End Get
        Set(ByVal value As Date)
            mdtTraining_Enddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set additonal_comments
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Additonal_Comments() As String
        Get
            Return mstrAdditonal_Comments
        End Get
        Set(ByVal value As String)
            mstrAdditonal_Comments = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemoployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Loginemoployeeunkid() As Integer
        Get
            Return mintLoginemoployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemoployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocessed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsCancel() As Boolean
        Get
            Return mblnIsCancel
        End Get
        Set(ByVal value As Boolean)
            mblnIsCancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelmoduleid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelFromModuleId() As Integer
        Get
            Return mintCancelFromModuleId
        End Get
        Set(ByVal value As Integer)
            mintCancelFromModuleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceluserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelUserId() As Integer
        Get
            Return mintCancelUserId
        End Get
        Set(ByVal value As Integer)
            mintCancelUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelremark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelRemark() As String
        Get
            Return mstrCancelRemark
        End Get
        Set(ByVal value As String)
            mstrCancelRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceldatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelDateTime() As DateTime
        Get
            Return mdtCancelDateTime
        End Get
        Set(ByVal value As DateTime)
            mdtCancelDateTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get linkedmasterid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _LinkedMasterId() As Integer
        Get
            Return mintLinkedMasterId
        End Get
        Set(ByVal value As Integer)
            mintLinkedMasterId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get crmasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ClaimRequestMasterId() As Integer
        Get
            Return mintClaimRequestMasterId
        End Get
        Set(ByVal value As Integer)
            mintClaimRequestMasterId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get mobjClaimRequestMaster
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ObjClaimRequestMaster() As clsclaim_request_master
        Get
            Return mobjClaimRequestMaster
        End Get
        Set(ByVal value As clsclaim_request_master)
            mobjClaimRequestMaster = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get applydate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ApplyDate() As DateTime
        Get
            Return mdtApplyDate
        End Get
        Set(ByVal value As DateTime)
            mdtApplyDate = value
        End Set
    End Property

    'S.SANDEEP [07-NOV-2018] -- START
    ''' <summary>
    ''' Purpose: Get applydate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _TrainingTypeid() As Integer
        Get
            Return mintTrainingTypeid
        End Get
        Set(ByVal value As Integer)
            mintTrainingTypeid = value
        End Set
    End Property
    'S.SANDEEP [07-NOV-2018] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  masterguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", levelunkid " & _
              ", mapuserunkid " & _
              ", statusunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", requisitionmasterunkid " & _
              ", employeeunkid " & _
              ", coursemasterunkid " & _
              ", instituteunkid " & _
              ", trainingmodeunkid " & _
              ", training_venue " & _
              ", training_startdate " & _
              ", training_enddate " & _
              ", additonal_comments " & _
              ", periodunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", auditdatetime " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb " & _
              ", loginemoployeeunkid " & _
              ", isprocessed " & _
                   ", cancelfrommoduleid " & _
                   ", iscancel " & _
                   ", canceluserunkid " & _
                   ", cancel_remark " & _
                   ", cancel_datetime " & _
                   ", linkedmasterid " & _
                   ", crmasterunkid " & _
                   ", applydate " & _
                   ", trainingtypeid " & _
             "FROM hrtraining_requisition_approval_master " & _
             "WHERE masterguid = @masterguid " & _
                   " AND isvoid = 0 AND iscancel = 0 "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            objDataOperation.AddParameter("@masterguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMasterguid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrMasterguid = dtRow.Item("masterguid").ToString
                mdtTransactiondate = CDate(dtRow.Item("transactiondate"))
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mblnIsfinal = CBool(dtRow.Item("isfinal"))
                mintRequisitionmasterunkid = CInt(dtRow.Item("requisitionmasterunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintCoursemasterunkid = CInt(dtRow.Item("coursemasterunkid"))
                mintInstituteunkid = CInt(dtRow.Item("instituteunkid"))
                mintTrainingmodeunkid = CInt(dtRow.Item("trainingmodeunkid"))
                mstrTraining_Venue = dtRow.Item("training_venue").ToString
                mdtTraining_Startdate = CDate(dtRow.Item("training_startdate"))
                mdtTraining_Enddate = CDate(dtRow.Item("training_enddate"))
                mstrAdditonal_Comments = dtRow.Item("additonal_comments").ToString
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintAudittype = CInt(dtRow.Item("audittype"))
                mintAudituserunkid = CInt(dtRow.Item("audituserunkid"))
                If IsDBNull(dtRow.Item("auditdatetime")) = False Then
                    mdtAuditdatetime = CDate(dtRow.Item("auditdatetime"))
                End If
                mstrIp = dtRow.Item("ip").ToString
                mstrHostname = dtRow.Item("hostname").ToString
                mstrForm_Name = dtRow.Item("form_name").ToString
                mblnIsweb = CBool(dtRow.Item("isweb"))
                mintLoginemoployeeunkid = CInt(dtRow.Item("loginemoployeeunkid"))
                mblnIsprocessed = CBool(dtRow.Item("isprocessed"))

                mblnIsCancel = CBool(dtRow.Item("iscancel"))
                mintCancelFromModuleId = CInt(dtRow.Item("cancelfrommoduleid"))
                mintCancelUserId = CInt(dtRow.Item("canceluserunkid"))
                mstrCancelRemark = CStr(dtRow.Item("cancel_remark"))
                If IsDBNull(dtRow.Item("cancel_datetime")) = False Then
                    mdtCancelDateTime = CDate(dtRow.Item("cancel_datetime"))
                Else
                    mdtCancelDateTime = Nothing
                End If
                mintLinkedMasterId = CInt(dtRow.Item("linkedmasterid"))
                mintClaimRequestMasterId = CInt(dtRow.Item("crmasterunkid"))
                mdtApplyDate = CDate(dtRow.Item("applydate"))
                mintTrainingTypeid = CInt(dtRow.Item("trainingtypeid")) 'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal mstrFilter As String = "", Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            If blnAddSelect Then
                strQ = "SELECT " & _
                       "  '' AS masterguid " & _
                       ", NULL AS transactiondate " & _
                       ", 0 AS mappingunkid " & _
                       ", 0 AS levelunkid " & _
                       ", 0 AS mapuserunkid " & _
                       ", 0 AS statusunkid " & _
                       ", '' AS remark " & _
                       ", CAST(0 AS BIT) AS isfinal " & _
                       ", 0 AS requisitionmasterunkid " & _
                       ", 0 AS employeeunkid " & _
                       ", 0 AS coursemasterunkid " & _
                       ", 0 AS instituteunkid " & _
                       ", 0 AS trainingmodeunkid " & _
                       ", '' AS training_venue " & _
                       ", '' AS training_startdate " & _
                       ", '' AS training_enddate " & _
                       ", '' AS additonal_comments " & _
                       ", 0 AS periodunkid " & _
                       ", CAST(0 AS BIT) AS isvoid " & _
                       ", NULL AS voiddatetime " & _
                       ", 0 AS voiduserunkid " & _
                       ", '' AS voidreason " & _
                       ", 0 AS audittype " & _
                       ", 0 AS audituserunkid " & _
                       ", NULL AS auditdatetime " & _
                       ", '' AS ip " & _
                       ", '' AS hostname " & _
                       ", '' AS form_name " & _
                       ", CAST(0 AS BIT) AS isweb " & _
                       ", 0 AS loginemoployeeunkid " & _
                       ", CAST(0 AS BIT) AS isprocessed " & _
                       ", '' AS TrainingMode " & _
                       ", '' AS CourseMaster " & _
                       ", '' AS Employee " & _
                       ", '' AS institute_name " & _
                       ", 0 AS Duration " & _
                       ", '' AS iStatus " & _
                       ", 0 AS iStatusId " & _
                       ", 0 AS userunkid " & _
                       ", 1 AS rno " & _
                       ", 0 AS cancelfrommoduleid " & _
                       ", CAST(0 AS BIT) AS iscancel " & _
                       ", 0 AS canceluserunkid " & _
                       ", '' AS cancel_remark " & _
                       ", NULL cancel_datetime " & _
                       ", 0 AS linkedmasterid " & _
                       ", 0 AS crmasterunkid " & _
                       ", NULL AS applydate " & _
                       ", 0 AS trainingtypeid " & _
                       ", '' AS trainingtype "
            End If
            strQ &= "SELECT " & _
                    "  TRAM.masterguid " & _
                    ", TRAM.transactiondate " & _
                    ", TRAM.mappingunkid " & _
                    ", TRAM.levelunkid " & _
                    ", TRAM.mapuserunkid " & _
                    ", TRAM.statusunkid " & _
                    ", TRAM.remark " & _
                    ", TRAM.isfinal " & _
                    ", TRAM.requisitionmasterunkid " & _
                    ", TRAM.employeeunkid " & _
                    ", TRAM.coursemasterunkid " & _
                    ", TRAM.instituteunkid " & _
                    ", TRAM.trainingmodeunkid " & _
                    ", TRAM.training_venue " & _
                    ", ISNULL(CONVERT(NVARCHAR(8),TRAM.training_startdate,112),'') AS training_startdate " & _
                    ", ISNULL(CONVERT(NVARCHAR(8),TRAM.training_enddate,112),'') AS training_enddate " & _
                    ", TRAM.additonal_comments " & _
                    ", TRAM.periodunkid " & _
                    ", TRAM.isvoid " & _
                    ", TRAM.voiddatetime " & _
                    ", TRAM.voiduserunkid " & _
                    ", TRAM.voidreason " & _
                    ", TRAM.audittype " & _
                    ", TRAM.audituserunkid " & _
                    ", TRAM.auditdatetime " & _
                    ", TRAM.ip " & _
                    ", TRAM.hostname " & _
                    ", TRAM.form_name " & _
                    ", TRAM.isweb " & _
                    ", TRAM.loginemoployeeunkid " & _
                    ", TRAM.isprocessed " & _
                    ", ISNULL(TM.name,'') AS TrainingMode " & _
                    ", ISNULL(CM.name,'') AS CourseMaster " & _
                    ", EM.employeecode + '-' + EM.firstname+' '+EM.surname AS Employee " & _
                    ", ISNULL(IM.institute_name,'') AS institute_name " & _
                    ", (DATEDIFF(DAY,training_startdate,training_enddate) + 1) AS Duration " & _
                    ", ISNULL(B.iStatus,'') AS iStatus " & _
                    ", ISNULL(B.stId,1) AS iStatusId " & _
                    ", TRAM.audituserunkid AS userunkid " & _
                    ", ROW_NUMBER()OVER(PARTITION BY TRAM.employeeunkid,TRAM.coursemasterunkid,TRAM.training_startdate,TRAM.training_enddate,TRAM.instituteunkid ORDER BY TRAM.transactiondate DESC) AS rno " & _
                    ", TRAM.cancelfrommoduleid " & _
                    ", TRAM.iscancel " & _
                    ", TRAM.canceluserunkid " & _
                    ", TRAM.cancel_remark " & _
                    ", TRAM.cancel_datetime " & _
                    ", TRAM.linkedmasterid " & _
                    ", TRAM.crmasterunkid " & _
                    ", TRAM.applydate " & _
                    ", TRAM.trainingtypeid " & _
                    ", CASE WHEN TRAM.trainingtypeid = 0 THEN @S " & _
                    "       WHEN TRAM.trainingtypeid = 1 THEN @E " & _
                    "       WHEN TRAM.trainingtypeid = 2 THEN @I END AS trainingtype " & _
                    "FROM hrtraining_requisition_approval_master AS TRAM " & _
                    "   LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = TRAM.coursemasterunkid " & _
                    "   LEFT JOIN cfcommon_master AS TM ON TM.masterunkid = TRAM.trainingmodeunkid " & _
                    "   JOIN hremployee_master AS EM ON EM.employeeunkid = TRAM.employeeunkid " & _
                    "   JOIN hrinstitute_master AS IM ON IM.instituteunkid = TRAM.instituteunkid AND IM.ishospital = 0 " & _
                    "   LEFT JOIN " & _
                    "   (" & _
                    "       SELECT " & _
                    "            A.employeeunkid " & _
                    "           ,A.coursemasterunkid " & _
                    "           ,CASE WHEN A.requisitionmasterunkid <= 0 AND A.stId = 3 THEN @Reject " & _
                    "                 WHEN A.requisitionmasterunkid <= 0 AND A.stId = 2 THEN @Pending " & _
                    "            ELSE @Pending END AS iStatus " & _
                    "           ,sDate " & _
                    "           ,eDate " & _
                    "           ,iVendorId " & _
                    "           ,stId " & _
                    "       FROM " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                employeeunkid " & _
                    "               ,statusunkid AS stId " & _
                    "               ,requisitionmasterunkid " & _
                    "               ,coursemasterunkid " & _
                    "               ,ISNULL(CONVERT(NVARCHAR(8),training_startdate,112),'') AS sDate " & _
                    "               ,ISNULL(CONVERT(NVARCHAR(8),training_enddate,112),'') AS eDate " & _
                    "               ,instituteunkid AS iVendorId " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid,coursemasterunkid,training_startdate,training_enddate,instituteunkid ORDER BY transactiondate DESC) AS rno " & _
                    "           FROM hrtraining_requisition_approval_master " & _
                    "           WHERE isvoid = 0 AND isprocessed = 0 " & _
                    "       ) AS A WHERE A.rno = 1 " & _
                    "   ) AS B ON B.employeeunkid = TRAM.employeeunkid AND B.coursemasterunkid = TRAM.coursemasterunkid AND B.sDate = ISNULL(CONVERT(NVARCHAR(8),TRAM.training_startdate,112),'') AND B.eDate = ISNULL(CONVERT(NVARCHAR(8),TRAM.training_enddate,112),'') AND B.iVendorId = TRAM.instituteunkid " & _
                    "WHERE TRAM.isvoid = 0 AND TRAM.isprocessed = 0 AND TRAM.requisitionmasterunkid <= 0 "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid,trainingtype} -- END

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

            'S.SANDEEP [07-NOV-2018] -- START
            objDataOperation.AddParameter("@S", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_master", 3, "NA"))
            objDataOperation.AddParameter("@E", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 862, "External"))
            objDataOperation.AddParameter("@I", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 863, "Internal"))
            'S.SANDEEP [07-NOV-2018] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_requisition_master) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal mdtEmployeeAsonDate As Date, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal dtCurrentDateAndTime As DateTime, _
                           ByVal dtRequisition As DataTable, _
                           ByVal blnRequisitionApproval As Boolean, _
                           ByVal dtDocuments As DataTable, _
                           ByVal dtClaimRequest As DataTable, _
                           ByVal dtClaimAttachment As DataTable) As Boolean
        If blnRequisitionApproval = False Then
            If isExist(mintEmployeeunkid, mintCoursemasterunkid, mdtTraining_Startdate, mdtTraining_Enddate, mintInstituteunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Training requisition for selected course, startdate, enddate and employee already present. Please add new training requisition.")
                Return False
            End If
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            If mintLinkedMasterId <= 0 Then
                Dim iCount As Integer = 1
                While iCount > 0
                    strQ = "SELECT (ISNULL(MAX(linkedmasterid),0) + 1) AS ival FROM hrtraining_requisition_approval_master "
                    dsList = objDataOperation.ExecQuery(strQ, "List")
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    mintLinkedMasterId = CInt(dsList.Tables("List").Rows(0)("ival"))
                    strQ = "SELECT 1 FROM hrtraining_requisition_approval_master WHERE linkedmasterid = '" & mintLinkedMasterId & "' "
                    iCount = objDataOperation.RecordCount(strQ)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    If iCount <= 0 Then
                        Exit While
                    End If
                End While
            End If


            If dtClaimRequest IsNot Nothing Then
                If mintClaimRequestMasterId <= 0 Then
                    objClaimRequestMaster._Expensetypeid = mobjClaimRequestMaster._Expensetypeid
                    objClaimRequestMaster._Claimrequestno = mobjClaimRequestMaster._Claimrequestno
                    objClaimRequestMaster._Expensetypeid = mobjClaimRequestMaster._Expensetypeid
                    objClaimRequestMaster._Employeeunkid = mobjClaimRequestMaster._Employeeunkid
                    objClaimRequestMaster._Transactiondate = mobjClaimRequestMaster._Transactiondate
                    objClaimRequestMaster._Claim_Remark = mobjClaimRequestMaster._Claim_Remark
                    objClaimRequestMaster._Userunkid = mobjClaimRequestMaster._Userunkid
                    objClaimRequestMaster._Isvoid = mobjClaimRequestMaster._Isvoid
                    objClaimRequestMaster._Voiduserunkid = mobjClaimRequestMaster._Voiduserunkid
                    objClaimRequestMaster._Voiddatetime = mobjClaimRequestMaster._Voiddatetime
                    objClaimRequestMaster._Voidreason = mobjClaimRequestMaster._Voidreason
                    objClaimRequestMaster._Loginemployeeunkid = mobjClaimRequestMaster._Loginemployeeunkid
                    objClaimRequestMaster._Modulerefunkid = mobjClaimRequestMaster._Modulerefunkid
                    objClaimRequestMaster._Referenceunkid = mobjClaimRequestMaster._Referenceunkid
                    objClaimRequestMaster._FromModuleId = mobjClaimRequestMaster._FromModuleId
                    objClaimRequestMaster._TrainingRequisitionMasterId = mintLinkedMasterId
                    objClaimRequestMaster._Statusunkid = mobjClaimRequestMaster._Statusunkid

                    Dim row As DataRow() = dtClaimAttachment.Select("form_name = '" & mstrForm_Name & "'")
                    If row.Length > 0 Then
                        row.ToList.ForEach(Function(x) UpdateRow(x, "", mintLinkedMasterId.ToString()))
                    End If

                    If objClaimRequestMaster.Insert(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, xIncludeIn_ActiveEmployee, dtClaimRequest, dtCurrentDateAndTime, dtClaimAttachment, objDataOperation, False) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    Else
                        mintClaimRequestMasterId = objClaimRequestMaster._Crmasterunkid
                    End If
                End If
            End If
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@masterguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMasterguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequisitionmasterunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoursemasterunkid.ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingmodeunkid.ToString)
            objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraining_Venue.ToString)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Startdate.ToString)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Enddate.ToString)
            objDataOperation.AddParameter("@additonal_comments", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrAdditonal_Comments.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditdatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@loginemoployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemoployeeunkid.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelUserId)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancelRemark.ToString)
            If mdtCancelDateTime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancelDateTime)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@linkedmasterid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLinkedMasterId)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRequestMasterId)
            objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplyDate)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeid) 'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            strQ = "INSERT INTO hrtraining_requisition_approval_master ( " & _
                    "  masterguid " & _
                    ", transactiondate " & _
                    ", mappingunkid " & _
                    ", levelunkid " & _
                    ", mapuserunkid " & _
                    ", statusunkid " & _
                    ", remark " & _
                    ", isfinal " & _
                    ", requisitionmasterunkid " & _
                    ", employeeunkid " & _
                    ", coursemasterunkid " & _
                    ", instituteunkid " & _
                    ", trainingmodeunkid " & _
                    ", training_venue " & _
                    ", training_startdate " & _
                    ", training_enddate " & _
                    ", additonal_comments " & _
                    ", periodunkid " & _
                    ", isvoid " & _
                    ", voiddatetime " & _
                    ", voiduserunkid " & _
                    ", voidreason " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", ip " & _
                    ", hostname " & _
                    ", form_name " & _
                    ", isweb " & _
                    ", loginemoployeeunkid " & _
                    ", isprocessed" & _
                    ", cancelfrommoduleid " & _
                    ", iscancel " & _
                    ", canceluserunkid " & _
                    ", cancel_remark " & _
                    ", cancel_datetime " & _
                    ", linkedmasterid " & _
                    ", crmasterunkid " & _
                    ", applydate " & _
                    ", trainingtypeid " & _
                  ") VALUES (" & _
                    "  @masterguid " & _
                    ", @transactiondate " & _
                    ", @mappingunkid " & _
                    ", @levelunkid " & _
                    ", @mapuserunkid " & _
                    ", @statusunkid " & _
                    ", @remark " & _
                    ", @isfinal " & _
                    ", @requisitionmasterunkid " & _
                    ", @employeeunkid " & _
                    ", @coursemasterunkid " & _
                    ", @instituteunkid " & _
                    ", @trainingmodeunkid " & _
                    ", @training_venue " & _
                    ", @training_startdate " & _
                    ", @training_enddate " & _
                    ", @additonal_comments " & _
                    ", @periodunkid " & _
                    ", @isvoid " & _
                    ", @voiddatetime " & _
                    ", @voiduserunkid " & _
                    ", @voidreason " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", @auditdatetime " & _
                    ", @ip " & _
                    ", @hostname " & _
                    ", @form_name " & _
                    ", @isweb " & _
                    ", @loginemoployeeunkid " & _
                    ", @isprocessed" & _
                    ", @cancelfrommoduleid " & _
                    ", @iscancel " & _
                    ", @canceluserunkid " & _
                    ", @cancel_remark " & _
                    ", @cancel_datetime " & _
                    ", @linkedmasterid " & _
                    ", @crmasterunkid " & _
                    ", @applydate " & _
                    ", @trainingtypeid " & _
                  "); "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtRequisition IsNot Nothing Then
                objRequisitionApprovalTran._RequisitionMasterguid(objDataOperation) = mstrMasterguid
                objRequisitionApprovalTran._DataTable = dtRequisition
                If objRequisitionApprovalTran.InsertUpdateDelete_TrainingRequisition(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If dtDocuments IsNot Nothing Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = dtDocuments.Select("form_name = '" & mstrForm_Name & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRow(x, "transactionunkid", mintLinkedMasterId.ToString()))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If mblnIsfinal Then
                If mintStatusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved Then
                    Dim intRequisitionMasterUnkid As Integer = 0
                    With objRequisitionMaster
                        ._Additonal_Comments = mstrAdditonal_Comments
                        ._AuditDatetime = mdtAuditdatetime
                        ._AuditUserId = mintAudituserunkid
                        ._ClientIP = mstrIp
                        ._Coursemasterunkid = mintCoursemasterunkid
                        ._Employeeunkid = mintEmployeeunkid
                        ._FormName = mstrForm_Name
                        ._HostName = mstrHostname
                        ._Instituteunkid = mintInstituteunkid
                        ._IsFromWeb = mblnIsweb
                        ._Isvoid = mblnIsvoid
                        ._LoginEmployeeId = mintLoginemoployeeunkid
                        ._Training_Enddate = mdtTraining_Enddate
                        ._Training_Startdate = mdtTraining_Startdate
                        ._Training_Venue = mstrTraining_Venue
                        ._Trainingmodeunkid = mintTrainingmodeunkid
                        ._Userunkid = mintAudituserunkid
                        ._Voiddatetime = mdtVoiddatetime
                        ._Voidreason = mstrVoidreason
                        ._Voiduserunkid = mintVoiduserunkid
                        ._IsCancel = mblnIsCancel
                        ._CancelDateTime = mdtCancelDateTime
                        ._CancelFromModuleId = mintCancelFromModuleId
                        ._CancelRemark = mstrCancelRemark
                        ._CancelUserId = mintCancelUserId
                        ._ApplyDate = mdtApplyDate
                        ._TrainingTypeid = mintTrainingTypeid
                        ._ClaimRequestMasterId = mintClaimRequestMasterId
                        If .Insert(dtRequisition, objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        intRequisitionMasterUnkid = ._Requisitionmasterunkid
                    End With
                    Dim list As DataTable = GetGuidDictionary(mintEmployeeunkid, mintCoursemasterunkid, mdtTraining_Startdate, mdtTraining_Enddate, mintInstituteunkid, objDataOperation)
                    For Each row As DataRow In dtRequisition.Rows
                        Dim iRow() As DataRow = Nothing
                        If CInt(row("trainingmodeunkid")) > 0 Then
                            iRow = list.Select("trainingmodeunkid = '" & row("trainingmodeunkid").ToString() & "'")
                            If iRow.Length > 0 Then
                                strQ = "UPDATE hrtraining_requisition_approval_tran SET requisitionmasterunkid = '" & intRequisitionMasterUnkid & "', requisitiontranunkid = '" & CInt(row("requisitiontranunkid")) & "'  WHERE requisitiontranunkid <= 0 AND trainingmodeunkid = '" & row("trainingmodeunkid").ToString() & "'; "
                                Call objDataOperation.ExecNonQuery(strQ)
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                        End If

                        If CInt(row("resourcemasterunkid")) > 0 Then
                            iRow = list.Select("trainingmodeunkid = '" & row("resourcemasterunkid").ToString() & "'")
                            If iRow.Length > 0 Then
                                strQ = "UPDATE hrtraining_requisition_approval_tran SET requisitionmasterunkid = '" & intRequisitionMasterUnkid & "', requisitiontranunkid = '" & CInt(row("requisitiontranunkid")) & "'  WHERE requisitiontranunkid <= 0 AND resourcemasterunkid = '" & row("resourcemasterunkid").ToString() & "'; "
                                Call objDataOperation.ExecNonQuery(strQ)
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                        End If
                    Next

                    Dim strGuids As String = ""
                    strGuids = String.Join("','", list.AsEnumerable().Select(Function(x) x.Field(Of String)("masterguid")).Distinct().ToArray())
                    If strGuids.Trim().Length > 0 Then
                        strGuids = "'" & strGuids & "'"

                    strQ = "UPDATE hrtraining_requisition_approval_master SET requisitionmasterunkid = '" & intRequisitionMasterUnkid & "',isprocessed = 1 WHERE masterguid IN (" & strGuids & ") AND requisitionmasterunkid <= 0 "

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

                    If objClaimRequestMaster.UpdateColumnValue(enExpenseType.EXP_TRAINING, mintClaimRequestMasterId, "referenceunkid", intRequisitionMasterUnkid, mintAudituserunkid, objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
            End If

                End If
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_requisition_master) </purpose>
    Public Function Update(ByVal xDatabaseName As String, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal mdtEmployeeAsonDate As Date, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal dtCurrentDateAndTime As DateTime, _
                           ByVal dtRequisition As DataTable, _
                           ByVal blnRequisitionApproval As Boolean, _
                           ByVal dtDocuments As DataTable, _
                           ByVal dtClaimRequest As DataTable, _
                           ByVal dtClaimAttachment As DataTable) As Boolean
        If blnRequisitionApproval = False Then
            If isExist(mintEmployeeunkid, mintCoursemasterunkid, mdtTraining_Startdate, mdtTraining_Enddate, mintInstituteunkid, mstrMasterguid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Training requisition for selected course, startdate, enddate and employee already present. Please add new training requisition.")
                Return False
            End If
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            If dtClaimRequest IsNot Nothing Then
                If mintClaimRequestMasterId > 0 Then
                    objClaimRequestMaster._Expensetypeid = mobjClaimRequestMaster._Expensetypeid
                    objClaimRequestMaster._Crmasterunkid = mobjClaimRequestMaster._Crmasterunkid
                    objClaimRequestMaster._Claimrequestno = mobjClaimRequestMaster._Claimrequestno
                    objClaimRequestMaster._Expensetypeid = mobjClaimRequestMaster._Expensetypeid
                    objClaimRequestMaster._Employeeunkid = mobjClaimRequestMaster._Employeeunkid
                    objClaimRequestMaster._Transactiondate = mobjClaimRequestMaster._Transactiondate
                    objClaimRequestMaster._Claim_Remark = mobjClaimRequestMaster._Claim_Remark
                    objClaimRequestMaster._Userunkid = mobjClaimRequestMaster._Userunkid
                    objClaimRequestMaster._Isvoid = mobjClaimRequestMaster._Isvoid
                    objClaimRequestMaster._Voiduserunkid = mobjClaimRequestMaster._Voiduserunkid
                    objClaimRequestMaster._Voiddatetime = mobjClaimRequestMaster._Voiddatetime
                    objClaimRequestMaster._Voidreason = mobjClaimRequestMaster._Voidreason
                    objClaimRequestMaster._Loginemployeeunkid = mobjClaimRequestMaster._Loginemployeeunkid
                    objClaimRequestMaster._Modulerefunkid = mobjClaimRequestMaster._Modulerefunkid
                    objClaimRequestMaster._Referenceunkid = mobjClaimRequestMaster._Referenceunkid
                    objClaimRequestMaster._FromModuleId = mobjClaimRequestMaster._FromModuleId
                    objClaimRequestMaster._TrainingRequisitionMasterId = mintLinkedMasterId
                    objClaimRequestMaster._Statusunkid = mobjClaimRequestMaster._Statusunkid
                    If objClaimRequestMaster.Update(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, xIncludeIn_ActiveEmployee, dtClaimRequest, dtCurrentDateAndTime, dtClaimAttachment, objDataOperation, False) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@masterguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMasterguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequisitionmasterunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoursemasterunkid.ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingmodeunkid.ToString)
            objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraining_Venue.ToString)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Startdate.ToString)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTraining_Enddate.ToString)
            objDataOperation.AddParameter("@additonal_comments", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrAdditonal_Comments.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditdatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@loginemoployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemoployeeunkid.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelUserId)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancelRemark.ToString)
            If mdtCancelDateTime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancelDateTime)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRequestMasterId)
            objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplyDate)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeid) 'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            strQ = "UPDATE hrtraining_requisition_approval_master SET " & _
                   "  transactiondate = @transactiondate" & _
                   ", mappingunkid = @mappingunkid" & _
                   ", levelunkid = @levelunkid" & _
                   ", mapuserunkid = @mapuserunkid" & _
                   ", statusunkid = @statusunkid" & _
                   ", remark = @remark" & _
                   ", isfinal = @isfinal" & _
                   ", requisitionmasterunkid = @requisitionmasterunkid" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", coursemasterunkid = @coursemasterunkid" & _
                   ", instituteunkid = @instituteunkid" & _
                   ", trainingmodeunkid = @trainingmodeunkid" & _
                   ", training_venue = @training_venue" & _
                   ", training_startdate = @training_startdate" & _
                   ", training_enddate = @training_enddate" & _
                   ", additonal_comments = @additonal_comments" & _
                   ", periodunkid = @periodunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", audittype = @audittype" & _
                   ", audituserunkid = @audituserunkid" & _
                   ", auditdatetime = @auditdatetime" & _
                   ", ip = @ip" & _
                   ", hostname = @hostname" & _
                   ", form_name = @form_name" & _
                   ", isweb = @isweb" & _
                   ", loginemoployeeunkid = @loginemoployeeunkid" & _
                   ", isprocessed = @isprocessed " & _
                   ", cancelfrommoduleid = @cancelfrommoduleid " & _
                   ", iscancel = @iscancel " & _
                   ", canceluserunkid  = @canceluserunkid " & _
                   ", cancel_remark = @cancel_remark " & _
                   ", cancel_datetime = @cancel_datetime " & _
                   ", crmasterunkid = @crmasterunkid " & _
                   ", applydate = @applydate " & _
                   ", trainingtypeid = @trainingtypeid " & _
                   "WHERE masterguid = @masterguid "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtRequisition IsNot Nothing Then
                objRequisitionApprovalTran._RequisitionMasterguid(objDataOperation) = mstrMasterguid
                objRequisitionApprovalTran._DataTable = dtRequisition
                If objRequisitionApprovalTran.InsertUpdateDelete_TrainingRequisition(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If dtDocuments IsNot Nothing Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = dtDocuments.Select("form_name = '" & mstrForm_Name & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRow(x, "transactionunkid", mintLinkedMasterId.ToString()))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpid As Integer, ByVal intCourseMasterId As Integer, ByVal StDate As DateTime, ByVal EdDate As DateTime, ByVal intInstituteId As Integer, Optional ByVal strGuid As String = "") As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "  masterguid " & _
                   ", transactiondate " & _
                   ", mappingunkid " & _
                   ", levelunkid " & _
                   ", mapuserunkid " & _
                   ", statusunkid " & _
                   ", remark " & _
                   ", isfinal " & _
                   ", requisitionmasterunkid " & _
                   ", employeeunkid " & _
                   ", coursemasterunkid " & _
                   ", instituteunkid " & _
                   ", trainingmodeunkid " & _
                   ", training_venue " & _
                   ", training_startdate " & _
                   ", training_enddate " & _
                   ", additonal_comments " & _
                   ", periodunkid " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voiduserunkid " & _
                   ", voidreason " & _
                   ", audittype " & _
                   ", audituserunkid " & _
                   ", auditdatetime " & _
                   ", ip " & _
                   ", hostname " & _
                   ", form_name " & _
                   ", isweb " & _
                   ", loginemoployeeunkid " & _
                   ", isprocessed " & _
                   ", iscancel " & _
                   ", canceluserunkid " & _
                   ", cancel_remark " & _
                   ", cancel_datetime " & _
                   ", crmasterunkid " & _
                   ", applydate " & _
                   ", trainingtypeid " & _
                   "FROM hrtraining_requisition_approval_master " & _
                   "WHERE isvoid = 0 AND isprocessed = 0 AND requisitionmasterunkid <=0 AND iscancel = 0 " & _
                   " AND employeeunkid = @employeeunkid " & _
                   " AND coursemasterunkid = @coursemasterunkid " & _
                   " AND CONVERT(NVARCHAR(8),training_startdate,112) = @training_startdate " & _
                   " AND CONVERT(NVARCHAR(8),training_enddate,112) = @training_enddate " & _
                   " AND instituteunkid = @instituteunkid "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            If strGuid.Trim.Length > 0 Then
                strQ &= " AND masterguid <> @masterguid "
            End If

            objDataOperation.AddParameter("@masterguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strGuid)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseMasterId)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(StDate).ToString)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(EdDate).ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInstituteId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsResponseGivenOnSamePriority(ByVal intEmpid As Integer, ByVal intCourseMasterId As Integer, ByVal StDate As DateTime, ByVal EdDate As DateTime, ByVal intInstituteId As Integer, ByVal intPriority As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Dim strMsg As String = String.Empty
        Try
            strQ = "SELECT priority " & _
                   "FROM hrtraining_requisition_approval_master " & _
                   "    JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = hrtraining_requisition_approval_master.levelunkid " & _
                   "WHERE isvoid = 0 AND isprocessed = 0 AND requisitionmasterunkid <=0 AND iscancel  = 0 " & _
                   " AND employeeunkid = @employeeunkid " & _
                   " AND coursemasterunkid = @coursemasterunkid " & _
                   " AND CONVERT(NVARCHAR(8),training_startdate,112) = @training_startdate " & _
                   " AND CONVERT(NVARCHAR(8),training_enddate,112) = @training_enddate " & _
                   " AND instituteunkid = @instituteunkid AND statusunkid <> 1 "
            strQ &= "UNION " & _
                    "SELECT priority " & _
                    "FROM hrtraining_requisition_approval_master " & _
                    "    JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = hrtraining_requisition_approval_master.levelunkid " & _
                    "WHERE isvoid = 0 AND isprocessed = 1 AND requisitionmasterunkid > 0 AND iscancel = 0 " & _
                    " AND employeeunkid = @employeeunkid " & _
                    " AND coursemasterunkid = @coursemasterunkid " & _
                    " AND CONVERT(NVARCHAR(8),training_startdate,112) = @training_startdate " & _
                    " AND CONVERT(NVARCHAR(8),training_enddate,112) = @training_enddate " & _
                    " AND instituteunkid = @instituteunkid AND statusunkid <> 1 "


            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseMasterId)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(StDate).ToString)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(EdDate).ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInstituteId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim iPriority As List(Of Integer) = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                If iPriority.IndexOf(intPriority) <> -1 Then
                    strMsg = Language.getMessage(mstrModuleName, 14, "Sorry, you cannot do approve/reject operation. Reason another user with same level/priority has approved or rejected the training requisition.")
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsResponseGivenOnSamePriority; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMsg
    End Function

    Public Function IsResponseGiven(ByVal intEmpid As Integer, ByVal intCourseMasterId As Integer, ByVal StDate As DateTime, ByVal EdDate As DateTime, ByVal intInstituteId As Integer, ByVal intMappingId As Integer, ByVal intLevelId As Integer, ByVal intUserId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT 1 " & _
                   "FROM hrtraining_requisition_approval_master " & _
                   "WHERE isvoid = 0 AND isprocessed = 0 AND requisitionmasterunkid <=0 AND iscancel = 0 " & _
                   " AND employeeunkid = @employeeunkid " & _
                   " AND coursemasterunkid = @coursemasterunkid " & _
                   " AND CONVERT(NVARCHAR(8),training_startdate,112) = @training_startdate " & _
                   " AND CONVERT(NVARCHAR(8),training_enddate,112) = @training_enddate " & _
                   " AND instituteunkid = @instituteunkid " & _
                   " AND mappingunkid = @mappingunkid " & _
                   " AND levelunkid = @levelunkid " & _
                   " AND mapuserunkid = @mapuserunkid "
            strQ &= "UNION " & _
                    "SELECT 1 " & _
                    "FROM hrtraining_requisition_approval_master " & _
                    "WHERE isvoid = 0 AND isprocessed = 1 AND requisitionmasterunkid > 0 AND iscancel = 0 " & _
                    " AND employeeunkid = @employeeunkid " & _
                    " AND coursemasterunkid = @coursemasterunkid " & _
                    " AND CONVERT(NVARCHAR(8),training_startdate,112) = @training_startdate " & _
                    " AND CONVERT(NVARCHAR(8),training_enddate,112) = @training_enddate " & _
                    " AND instituteunkid = @instituteunkid " & _
                    " AND mappingunkid = @mappingunkid " & _
                    " AND levelunkid = @levelunkid " & _
                    " AND mapuserunkid = @mapuserunkid "

            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseMasterId)
            objDataOperation.AddParameter("@training_startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(StDate).ToString)
            objDataOperation.AddParameter("@training_enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(EdDate).ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInstituteId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpid)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMappingId)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelId)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsResponseGiven; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_approver_master) </purpose>
    Public Function Delete(ByVal strGuid As String, _
                           ByVal iUserId As Integer, _
                           ByVal xDatabaseName As String, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal strEmployeeAsOnDate As String, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal dtTrainingAttachment As DataTable, _
                           ByVal dtClaimAttachment As DataTable, _
                           ByVal intClaimMasterId As Integer, _
                           Optional ByVal blnApplyUserAccessFilter As Boolean = True) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtraining_requisition_approval_master SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   " ,loginemoployeeunkid = @loginemoployeeunkid " & _
                   "WHERE masterguid = @masterguid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@masterguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strGuid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@loginemoployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemoployeeunkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As New DataTable
            objRequisitionApprovalTran._RequisitionMasterguid(objDataOperation) = mstrMasterguid
            dtTable = objRequisitionApprovalTran._DataTable.Copy()
            If dtTable IsNot Nothing Then
                Dim dRows As DataRow() = dtTable.Select("")
                dRows.ToList.ForEach(Function(x) UpdateRow(x, "D"))
                dtTable.AcceptChanges()
                objRequisitionApprovalTran._RequisitionMasterguid(objDataOperation) = mstrMasterguid
                objRequisitionApprovalTran._DataTable = dtTable
                If objRequisitionApprovalTran.InsertUpdateDelete_TrainingRequisition(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If dtTrainingAttachment IsNot Nothing Then
                Dim dRows As DataRow() = dtTrainingAttachment.Select("")
                dRows.ToList.ForEach(Function(x) UpdateRow(x, "D"))
                objDocument._Datatable = dtTrainingAttachment
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
            End If

            _Masterguid(objDataOperation) = strGuid

            objClaimRequestMaster._Isvoid = mobjClaimRequestMaster._Isvoid
            objClaimRequestMaster._Voiduserunkid = mobjClaimRequestMaster._Voiduserunkid
            objClaimRequestMaster._Voiddatetime = mobjClaimRequestMaster._Voiddatetime
            objClaimRequestMaster._Voidreason = mobjClaimRequestMaster._Voidreason
            objClaimRequestMaster._Loginemployeeunkid = mobjClaimRequestMaster._Loginemployeeunkid
            objClaimRequestMaster._VoidLoginEmployeeID = mobjClaimRequestMaster._Loginemployeeunkid
            objClaimRequestMaster._Modulerefunkid = mobjClaimRequestMaster._Modulerefunkid
            objClaimRequestMaster._Referenceunkid = mobjClaimRequestMaster._Referenceunkid
            objClaimRequestMaster._FromModuleId = mobjClaimRequestMaster._FromModuleId
            objClaimRequestMaster._TrainingRequisitionMasterId = mintLinkedMasterId

            If objClaimRequestMaster.Delete(intClaimMasterId, iUserId, xDatabaseName, xYearUnkid, xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, dtClaimAttachment, True, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getStatusComboList(ByVal strList As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT '" & enApprovalStatus.SubmitForApproval & "' AS Id, @Pending AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.Approved & "' AS Id, @Approved AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.Rejected & "' AS Id, @Reject AS Name "

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 104, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getStatusComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function IsApproverPresent(ByVal strDatabaseName As String, _
                                      ByVal strUserAccessMode As String, _
                                      ByVal intCompanyId As Integer, _
                                      ByVal intYearId As Integer, _
                                      ByVal intPrivilegeId As Integer, _
                                      ByVal intUserId As Integer, _
                                      ByVal xEmployeeAsOnDate As String, _
                                      ByVal intEmployeeUnkid As Integer, _
                                      Optional ByVal objDataOpr As clsDataOperation = Nothing) As String
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim strMsg As String = ""
        Try
            Dim strUACJoin, strUACFilter As String
            strUACJoin = "" : strUACFilter = ""


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'StrQ = "SELECT " & _
            '       "     UM.userunkid " & _
            '       "    ,UM.username " & _
            '       "    ,UM.email " & _
            '       "    ,TAL.priority " & _
            '       "    ,TAL.levelname " & _
            '       "FROM " & strDatabaseName & "..hrtraining_approver_master AS TAM " & _
            '       "    JOIN hrmsConfiguration..cfuser_master AS UM ON TAM.mapuserunkid = UM.userunkid " & _
            '       "    JOIN hrtraining_approverlevel_master AS TAL ON TAL.levelunkid = TAM.levelunkid " & _
            '       "    JOIN hrmsConfiguration..cfcompanyaccess_privilege AS CP ON CP.userunkid = UM.userunkid " & _
            '       "    JOIN hrmsConfiguration..cfuser_privilege AS UP ON UM.userunkid = UP.userunkid " & _
            '       "WHERE TAM.isvoid = 0 AND CP.yearunkid = @Y AND UP.privilegeunkid = @P " & _
            '       "ORDER BY TAL.priority "

            StrQ = "SELECT " & _
                   "     UM.userunkid " & _
                   "    ,UM.username " & _
                   "    ,UM.email " & _
                   "    ,TAL.priority " & _
                   "    ,TAL.levelname " & _
                   "FROM " & strDatabaseName & "..hrtraining_approver_master AS TAM " & _
                   "    JOIN hrmsConfiguration..cfuser_master AS UM ON TAM.mapuserunkid = UM.userunkid " & _
                   "    JOIN hrtraining_approverlevel_master AS TAL ON TAL.levelunkid = TAM.levelunkid " & _
                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege AS CP ON CP.userunkid = UM.userunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_privilege AS UP ON UM.userunkid = UP.userunkid " & _
                   "WHERE TAM.isvoid = 0 AND TAM.isactive=1  AND CP.yearunkid = @Y AND UP.privilegeunkid = @P " & _
                   "ORDER BY TAL.priority "
            'Gajanan [17-April-2019] -- End

            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 3, "Sorry, No approver(s) defined based on selected for employee user access setting.")
                Exit Try
            End If

            Dim blnFlag As Boolean = False

            For Each dr As DataRow In dsList.Tables(0).Rows
                modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, CInt(dr("userunkid")), intCompanyId, intYearId, strUserAccessMode, "AEM", True)
                StrQ = "SELECT " & _
                   "     AEM.employeeunkid " & _
                   "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                   "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                   "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                   "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                   "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                   "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                   "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                   "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                   "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                   "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                   "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                   "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                   "FROM " & strDatabaseName & "..hremployee_master AS AEM "
                If strUACJoin.Trim.Length > 0 Then
                    StrQ &= strUACJoin
                End If
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        stationunkid " & _
                        "       ,deptgroupunkid " & _
                        "       ,departmentunkid " & _
                        "       ,sectiongroupunkid " & _
                        "       ,sectionunkid " & _
                        "       ,unitgroupunkid " & _
                        "       ,unitunkid " & _
                        "       ,teamunkid " & _
                        "       ,classgroupunkid " & _
                        "       ,classunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "   FROM hremployee_transfer_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "   FROM hremployee_categorization_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                        "WHERE AEM.employeeunkid = '" & intEmployeeUnkid & "' "

                Dim dsEmp As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                If dsEmp.Tables(0).Rows.Count > 0 Then
                    blnFlag = True
                    Exit For
                End If
            Next

            If blnFlag = False Then
                strMsg = Language.getMessage(mstrModuleName, 3, "Sorry, No approver(s) defined based on selected for employee user access setting.")
                Exit Try
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsApproverPresent; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMsg
    End Function

    Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
                                            , ByVal intCompanyId As Integer _
                                            , ByVal intYearId As Integer _
                                            , ByVal strUserAccessMode As String _
                                            , ByVal intPrivilegeId As Integer _
                                            , ByVal xEmployeeAsOnDate As String _
                                            , ByVal intUserId As Integer _
                                            , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                            , Optional ByVal intCurrentPriorityId As Integer = -1 _
                                            , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                            , Optional ByVal mstrFilterString As String = "" _
                                            , Optional ByVal blnAddUserAccess As Boolean = True _
                                            , Optional ByVal strEmployeeIds As String = "" _
                                            ) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""
            Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, 0, objDataOperation)


            Dim strUACJoin, strUACFilter As String
            strUACJoin = "" : strUACFilter = ""
            modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM", blnAddUserAccess)

            Dim strApprovalDataJoin As String = String.Empty

            strApprovalDataJoin = "LEFT JOIN " & _
                                  "( " & _
                                  "    SELECT " & _
                                  "         [HA].[mapuserunkid] " & _
                                  "        ,[HM].[priority] " & _
                                  "        ,[HTAT].[statusunkid] AS iStatusId " & _
                                  "        ,[HTAT].[transactiondate] " & _
                                  "        ,[HTAT].[mappingunkid] " & _
                                  "        ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                                  "              WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ '+ CM.[username] + ' ]' " & _
                                  "              WHEN [HTAT].[statusunkid] = 3 THEN @Reject+' : [ '+ CM.[username] + ' ]' " & _
                                  "         ELSE @PendingApproval END AS iStatus " & _
                                  "        ,ROW_NUMBER()OVER(PARTITION     BY [HTAT].[employeeunkid] ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                                  "        ,HTAT.[employeeunkid] " & _
                                  "        ,HTAT.[masterguid] AS mguid " & _
                                  "        ,HTAT.[coursemasterunkid] AS cmstid " & _
                                  "        ,HTAT.[instituteunkid] AS imstid " & _
                                  "        ,ISNULL(CONVERT(NVARCHAR(8), HTAT.training_startdate, 112), '') as stdate" & _
                                  "        ,ISNULL(CONVERT(NVARCHAR(8), HTAT.training_enddate, 112), '') as eddate " & _
                                  "        ,[HTAT].[linkedmasterid] AS linkedmasterid " & _
                                  "        ,[HTAT].[crmasterunkid] AS crmasterunkid " & _
                                  "        ,[HTAT].[applydate] AS applydate " & _
                                  "        ,[HTAT].[trainingtypeid] AS trainingtypeid " & _
                                  "    FROM hrtraining_requisition_approval_master AS HTAT " & _
                                  "        JOIN [hrtraining_approver_master] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] " & _
                                  "        JOIN [hrtraining_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                                  "        LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                                  "    WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 AND [HTAT].[iscancel] = 0 " & _
                                  "     AND [HTAT].[requisitionmasterunkid] <= 0 AND ISNULL([HTAT].[isprocessed],0) = 0 " & _
                                  ") AS B ON B.[employeeunkid] = TA.[employeeunkid] AND [B].[priority] = [Fn].[priority] AND TA.instituteunkid = B.imstid AND TA.coursemasterunkid = B.cmstid " & _
                                  " AND ISNULL(CONVERT(NVARCHAR(8), TA.training_startdate, 112), '') = B.stdate AND ISNULL(CONVERT(NVARCHAR(8), TA.training_enddate, 112), '')= B.eddate "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END

            StrQ = "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL " & _
                           "DROP TABLE #EMP "

            StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                           "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                           "SELECT " & _
                    "     AEM.employeeunkid " & _
                           "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                           "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                           "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                           "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                           "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                           "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                           "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                           "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                           "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                           "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                           "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                           "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                    "FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                    "    JOIN hrtraining_requisition_approval_master AS TAT ON TAT.employeeunkid = AEM.employeeunkid "
            If strUACJoin.Trim.Length > 0 AndAlso intUserId > 0 Then
                StrQ &= strUACJoin
            End If
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        stationunkid " & _
                    "       ,deptgroupunkid " & _
                    "       ,departmentunkid " & _
                    "       ,sectiongroupunkid " & _
                    "       ,sectionunkid " & _
                    "       ,unitgroupunkid " & _
                    "       ,unitunkid " & _
                    "       ,teamunkid " & _
                    "       ,classgroupunkid " & _
                    "       ,classunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
            If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "
            StrQ &= ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        jobgroupunkid " & _
                    "       ,jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
            If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "
            StrQ &= ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 "
            StrQ &= "WHERE 1 = 1 AND TAT.isvoid = 0 AND TAT.requisitionmasterunkid <= 0 AND TAT.statusunkid = 1 AND ISNULL([TAT].[isprocessed],0) = 0 "
            If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND AEM.employeeunkid IN (" & strEmployeeIds & ") "
            StrQ &= " SELECT * INTO #EMP FROM @emp "
            StrQ &= "SELECT DISTINCT " & _
                    "     CAST(0 AS BIT) AS iCheck " & _
                    "    ,TA.masterguid " & _
                    "    ,ISNULL(B.[transactiondate],TA.transactiondate) AS transactiondate " & _
                    "    ,ISNULL(B.[mappingunkid],TA.mappingunkid) AS mappingunkid " & _
                    "    ,ISNULL(B.[iStatus],@Pending) AS iStatus " & _
                    "    ,ISNULL(B.[iStatusId],TA.statusunkid) AS iStatusId " & _
                    "    ,TA.remark " & _
                    "    ,TA.isfinal " & _
                    "    ,TA.statusunkid " & _
                    "    ,TA.requisitionmasterunkid " & _
                    "    ,EM.employeecode as ecode " & _
                    "    ,EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname AS ename " & _
                    "    ,CONVERT(CHAR(8),EM.appointeddate,112) AS adate " & _
                    "    ,Fn.userunkid " & _
                    "    ,Fn.username " & _
                    "    ,Fn.email " & _
                    "    ,Fn.priority " & _
                    "    ,EM.employeeunkid " & _
                    "    ,ISNULL(Fn.uempid,0) AS uempid " & _
                    "    ,ISNULL(Fn.ecompid,0) AS ecompid " & _
                    "    ,ISNULL(Fn.LvName,'') AS levelname " & _
                    "    ,ISNULL(TM.name,'') AS TrainingMode " & _
                    "    ,ISNULL(CM.name,'') AS CourseMaster " & _
                    "    ,ISNULL(IM.institute_name,'') AS institute_name " & _
                    "    ,(DATEDIFF(DAY,training_startdate,training_enddate) + 1) AS Duration " & _
                    "    ,TA.training_venue " & _
                    "    ,ISNULL(CONVERT(NVARCHAR(8),TA.training_startdate,112),'') AS training_startdate " & _
                    "    ,ISNULL(CONVERT(NVARCHAR(8),TA.training_enddate,112),'') AS training_enddate " & _
                    "    ,TA.additonal_comments " & _
                    "    ,Fn.mapuserunkid " & _
                    "    ,Fn.oMappingId " & _
                    "    ,TA.linkedmasterid " & _
                    "    ,TA.crmasterunkid " & _
                    "    ,TA.applydate " & _
                    "    ,TA.trainingtypeid " & _
                    "    ,CASE WHEN TA.trainingtypeid = 0 THEN @S " & _
                    "          WHEN TA.trainingtypeid = 1 THEN @E " & _
                    "          WHEN TA.trainingtypeid = 2 THEN @I END AS trainingtype " & _
                    "FROM @emp " & _
                    "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
                    "    JOIN hrtraining_requisition_approval_master AS TA ON EM.employeeunkid = TA.employeeunkid " & _
                    "    LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = TA.coursemasterunkid " & _
                    "    LEFT JOIN cfcommon_master AS TM ON TM.masterunkid = TA.trainingmodeunkid " & _
                    "    JOIN hrinstitute_master AS IM ON IM.instituteunkid = TA.instituteunkid AND IM.ishospital = 0 " & _
                    "    JOIN " & _
                    "    ( " & _
                    "        SELECT DISTINCT " & _
                    "             cfuser_master.userunkid " & _
                    "            ,cfuser_master.username " & _
                    "            ,cfuser_master.email " & _
                                  strSelect & " " & _
                    "            ,LM.priority " & _
                    "            ,LM.levelname AS LvName " & _
                    "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                    "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                    "            ,EM.mapuserunkid " & _
                    "            ,EM.mappingunkid AS oMappingId " & _
                    "        FROM hrmsConfiguration..cfuser_master " & _
                    "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                    "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                                 strAccessJoin & " " & _
                    "        JOIN " & strDatabaseName & "..hrtraining_approver_master EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                    "        JOIN " & strDatabaseName & "..hrtraining_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
                    "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
            'S.SANDEEP [07-NOV-2018] -- START {trainingtypeid} -- END
            If strFilter.Trim.Length > 0 Then
                StrQ &= " AND (" & strFilter & " ) "
            End If

            StrQ &= "            AND yearunkid = @Y AND privilegeunkid = @P  " & _
                    ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
                    strApprovalDataJoin & " " & _
                    strOuterJoin & " " & _
                    "WHERE 1 = 1 AND TA.isvoid = 0 AND TA.iscancel  = 0 AND TA.requisitionmasterunkid <= 0 AND TA.statusunkid = 1 AND ISNULL([TA].[isprocessed],0) = 0 "

            If mblnIsSubmitForApproaval = False Then
                StrQ &= " AND Fn.priority > " & intCurrentPriorityId
            End If

            If mstrFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilterString
            End If

            StrQ &= "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL " & _
                    "DROP TABLE #EMP "

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

            'S.SANDEEP [07-NOV-2018] -- START
            objDataOperation.AddParameter("@S", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_master", 3, "NA"))
            objDataOperation.AddParameter("@E", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 862, "External"))
            objDataOperation.AddParameter("@I", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 863, "Internal"))
            'S.SANDEEP [07-NOV-2018] -- END

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            dtList = dsList.Tables("List").Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtList
    End Function

    Public Function GetEmployeeApprovalData(ByVal strDatabaseName As String _
                                          , ByVal intCompanyId As Integer _
                                          , ByVal intYearId As Integer _
                                          , ByVal strUserAccessMode As String _
                                          , ByVal intPrivilegeId As Integer _
                                          , ByVal intUserId As Integer _
                                          , ByVal intPriorityId As Integer _
                                          , ByVal blnOnlyMyApprovals As Boolean _
                                          , ByVal xEmployeeAsOnDate As String _
                                          , ByVal blnAddUserAccess As Boolean _
                                          , Optional ByVal objDataOpr As clsDataOperation = Nothing) As DataTable
        Dim StrQ As String = String.Empty
        Dim dList As DataTable = Nothing
        Dim oData As DataTable = Nothing
        Try
            dList = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, Nothing, , , , blnAddUserAccess)
            oData = New DataView(dList, "", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable().Copy
            If blnOnlyMyApprovals = True Then
            If dList.Rows.Count > 0 Then
                Dim blnFlag As Boolean = False
                Dim iPriority As List(Of Integer) = dList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                Dim intSecondLastPriority As Integer = -1
                If iPriority.Min() = intPriorityId Then
                    intSecondLastPriority = intPriorityId
                    blnFlag = True
                        If blnOnlyMyApprovals = True Then
                            oData = New DataView(dList, "userunkid = " & intUserId, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                        End If
                Else
                    Try
                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
                    Catch ex As Exception
                        intSecondLastPriority = intPriorityId
                        blnFlag = True
                    End Try
                    Dim dr() As DataRow = Nothing
                    dr = dList.Select("priority = '" & intSecondLastPriority & "'", "employeeunkid, priority")
                    If dr.Length > 0 Then
                        Dim row = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(clsEmployee_Master.EmpApprovalStatus.Approved))
                        Dim strIds As String
                        If row.Count() > 0 Then
                            strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList().Except(row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()).Distinct().ToArray())
                            If blnFlag Then
                                strIds = ""
                            End If
                            If strIds.Trim.Length > 0 Then
                                oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                            Else
                                oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                            End If
                        Else
                            strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                            If blnFlag = False Then
                                strIds = " AND employeeunkid NOT IN(" & strIds & ") "
                            Else
                                strIds = ""
                            End If
                            oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 " & strIds, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                        End If
                    Else
                        oData = New DataView(dList, "userunkid = " & intUserId, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable() '& " AND iStatusId = 1 "
                    End If
                End If
            End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return oData
    End Function

    Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = ""
        Try
            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as branchid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
                                         "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
                        strJoin &= "AND Fn.branchid = [@emp].branchid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

                    Case enAllocation.DEPARTMENT_GROUP
                        strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.deptgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
                                         "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
                        strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

                    Case enAllocation.DEPARTMENT
                        strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.deptid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
                                         "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
                        strJoin &= "AND Fn.deptid = [@emp].deptid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

                    Case enAllocation.SECTION_GROUP
                        strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.secgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
                                         "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
                        strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

                    Case enAllocation.SECTION
                        strSelect &= ",ISNULL(SC.secid,0) AS secid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.secid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
                                         "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SC.secid,0) > 0 "
                        strJoin &= "AND Fn.secid = [@emp].secid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

                    Case enAllocation.UNIT_GROUP
                        strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.unitgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
                                         "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
                        strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

                    Case enAllocation.UNIT
                        strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.unitid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
                                         "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
                        strJoin &= "AND Fn.unitid = [@emp].unitid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

                    Case enAllocation.TEAM
                        strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as teamid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.teamid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
                                         "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
                        strJoin &= "AND Fn.teamid = [@emp].teamid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

                    Case enAllocation.JOB_GROUP
                        strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.jgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
                                         "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
                        strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

                    Case enAllocation.JOBS
                        strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jobid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.jobid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
                                         "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
                        strJoin &= "AND Fn.jobid = [@emp].jobid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

                    Case enAllocation.CLASS_GROUP
                        strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.clsgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
                                         "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
                        strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

                    Case enAllocation.CLASSES
                        strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.clsid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
                                         "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
                        strJoin &= "AND Fn.clsid = [@emp].clsid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = hrclasses_master.classesunkid "
                End Select

                objDataOperation.ClearParameters()
                StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
                Dim strvalue = ""
                objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                strvalue = CStr(objDataOperation.GetParameterValue("@Value"))

                If strvalue.Trim.Length > 0 Then
                    Select Case CInt(strvalues(index))
                        Case enAllocation.BRANCH
                            strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT_GROUP
                            strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT
                            strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

                        Case enAllocation.SECTION_GROUP
                            strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

                        Case enAllocation.SECTION
                            strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

                        Case enAllocation.UNIT_GROUP
                            strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

                        Case enAllocation.UNIT
                            strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

                        Case enAllocation.TEAM
                            strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

                        Case enAllocation.JOB_GROUP
                            strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

                        Case enAllocation.JOBS
                            strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

                        Case enAllocation.CLASS_GROUP
                            strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

                        Case enAllocation.CLASSES
                            strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

                    End Select
                End If
            Next
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function UpdateRow(ByVal dr As DataRow, ByVal strValue As String) As Boolean
        Try
            dr("AUD") = strValue
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Function UpdateRow(ByVal dr As DataRow, ByVal strColName As String, ByVal strValue As String) As Boolean
        Try
            If strColName.Trim.Length > 0 Then
                dr(strColName) = strValue
            End If
            dr("filesize") = dr("filesize_kb")
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Public Function GetAssessmentTrainingIds(ByVal intEmpId As Integer, ByVal intPeriodId As Integer) As String
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Dim strIds As String = ""
        Try
            StrQ = "SELECT DISTINCT " & _
                   "    custom_value " & _
                   "FROM hrassess_custom_items AS CI " & _
                   "    JOIN hrcompetency_customitem_tran AS CT ON CI.customitemunkid = CT.customitemunkid " & _
                   "    JOIN hrevaluation_analysis_master AS EV ON EV.analysisunkid = CT.analysisunkid " & _
                   "WHERE CI.periodunkid = @p AND CI.isactive = 1 AND CI.itemtypeid = 2 AND CI.selectionmodeid = 1 " & _
                   "    AND (selfemployeeunkid = @e OR assessedemployeeunkid = @e) " & _
                   "    AND CT.isvoid = 0 AND EV.isvoid = 0 AND EV.iscommitted = 1 AND EV.periodunkid = @p "

            objDataOperation.AddParameter("@e", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@p", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strIds = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("custom_value")).ToArray())
            End If
            If strIds.Trim.Length > 0 Then
                strIds = "0," & strIds
            Else
                strIds = "0"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssessmentTrainingIds; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strIds
    End Function

    ''' <summary>
    ''' Notification For Training Requisition
    ''' </summary>
    ''' <param name="intNotificationType">1 = Submit for Approval , 2 = Notify Next User for Approve, 3 = Send Rejection to Selected User</param>
    ''' <param name="strDatabaseName"></param>
    ''' <param name="strUserAccessMode"></param>
    ''' <param name="intCompanyId"></param>
    ''' <param name="intYearId"></param>
    ''' <param name="intPrivilegeId"></param>
    ''' <param name="xEmployeeAsOnDate"></param>
    ''' <param name="intUserId"></param>
    ''' <param name="strFormName"></param>
    ''' <param name="eMode"></param>
    ''' <param name="strSenderAddress"></param>
    ''' <param name="intPriority"></param>
    ''' <param name="intEmployeeIds"></param>
    ''' <param name="strRemark"></param>
    ''' <remarks></remarks>
    Public Sub SendNotification(ByVal intNotificationType As Integer, _
                                ByVal strDatabaseName As String, _
                                ByVal strUserAccessMode As String, _
                                ByVal intCompanyId As Integer, _
                                ByVal intYearId As Integer, _
                                ByVal intPrivilegeId As Integer, _
                                ByVal xEmployeeAsOnDate As String, _
                                ByVal intUserId As Integer, _
                                ByVal strFormName As String, _
                                ByVal eMode As enLogin_Mode, _
                                ByVal strSenderAddress As String, _
                                ByVal strArutiSelfServiceURL As String, _
                                Optional ByVal intPriority As Integer = -1, _
                                Optional ByVal intEmployeeIds As String = "", _
                                Optional ByVal strRemark As String = "", _
                                Optional ByVal blnAddUserAccess As Boolean = True, _
                                Optional ByVal strMasterGuid As String = "")
        Try
            Dim dtAppr As DataTable = Nothing
            Dim StrMessage As New System.Text.StringBuilder
            Select Case intNotificationType
                Case 1
                    dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, Nothing, , , , blnAddUserAccess)
                    If intPriority <> -1 Then
                        dtAppr = New DataView(dtAppr, "priority > '" & intPriority & "'", "priority", DataViewRowState.CurrentRows).ToTable()
                        intPriority = -1
                    End If
                Case 2
                    dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, Nothing, intPriority, False, , blnAddUserAccess)
                Case 3
                    dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, Nothing, , , , blnAddUserAccess)
            End Select
            If strMasterGuid.Trim.Length > 0 Then
                dtAppr = New DataView(dtAppr, "masterguid = '" & strMasterGuid & "'", "priority", DataViewRowState.CurrentRows).ToTable()
            End If
            Dim dr() As DataRow = Nothing
            Dim iPriority As List(Of Integer) = dtAppr.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
            Dim intNextPriority As Integer = -1
            If intPriority = -1 Then intNextPriority = iPriority.Min()
            If intNextPriority = -1 Then
                Try
                    intNextPriority = iPriority.Item(iPriority.IndexOf(intPriority) + 1)
                Catch ex As Exception
                    intNextPriority = intPriority
                End Try
            End If

            'dr = dtAppr.Select("uempid = '" & CInt(intEmployeeIds) & "' AND ecompid = " & CInt(intCompanyId) & " ")
            'If dr.Length > 0 Then
            '    dtAppr.Rows.Remove(dr(0))
            '    dtAppr.AcceptChanges()
            '    intPriority = CInt(dr(0)("priority"))
            '    Try
            '        intNextPriority = iPriority.Item(iPriority.IndexOf(intPriority) + 1)
            '    Catch ex As Exception
            '        intNextPriority = intPriority
            '    End Try
            '    If intNextPriority = iPriority.Max() Then
            '        If dtAppr.Select("uempid <> '" & CInt(intEmployeeIds) & "' AND priority = '" & intNextPriority & "'").Length <= 0 Then
            '            Exit Sub
            '        End If
            '    End If
            'End If
            'dr = Nothing

            Dim dtFilterTab As DataTable = dtAppr.DefaultView.ToTable(True, "username", "email", "userunkid")

            For Each iRow As DataRow In dtFilterTab.Rows
                StrMessage = New System.Text.StringBuilder
                Select Case intNotificationType
                    Case 1 'NOTIFICATION FOR SUBMIT FOR APPROVAL                        
                        dr = dtAppr.Select("priority = '" & intNextPriority & "' AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
                        If dr.Length > 0 Then
                            Call SetEmailBody(StrMessage, intNotificationType, dr, iRow("username").ToString, Language.getMessage(mstrModuleName, 10, "This is to inform you that following employee(s) has applied for training requisition, Please login to aruti to approve/reject them."), strRemark, False, strArutiSelfServiceURL, intCompanyId)
                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 11, "Notification to Approve/Reject Training Requisition")
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                            objSendMail.SendMail(intCompanyId)
                        End If
                    Case 2 'NOTIFICATION FOR NEXT LEVEL OF APPROVAL
                        dr = dtAppr.Select("priority = '" & intNextPriority & "' AND userunkid = " & CInt(iRow("userunkid")) & "  " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
                        If dr.Length > 0 Then
                            Call SetEmailBody(StrMessage, intNotificationType, dr, iRow("username").ToString, Language.getMessage(mstrModuleName, 10, "This is to inform you that following employee(s) has applied for training requisition, Please login to aruti to approve/reject them."), strRemark, False, strArutiSelfServiceURL, intCompanyId)
                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 11, "Notification to Approve/Reject Training Requisition")
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                            objSendMail.SendMail(intCompanyId)
                        End If
                    Case 3 'NOTIFICATION FOR REJECTION OF EMPLOYEE MOVEMENT
                        dr = dtAppr.Select("priority <= " & intNextPriority & " AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
                        If dr.Length > 0 Then
                            Call SetEmailBody(StrMessage, intNotificationType, dr, iRow("username").ToString, Language.getMessage(mstrModuleName, 12, "This is to inform you that following employee(s) applied training requisition has been rejected"), strRemark, True, strArutiSelfServiceURL, intCompanyId)
                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 13, "Rejection Notification for Training Requisition")
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            objSendMail.SendMail(intCompanyId)
                        End If
                End Select
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetEmailBody(ByRef StrMessage As System.Text.StringBuilder, _
                             ByVal intNotificationType As Integer, _
                             ByVal dr() As DataRow, _
                             ByVal username As String, _
                             ByVal strMainText As String, _
                             ByVal strRejectionRemark As String, _
                             ByVal blnRejection As Boolean, _
                             ByVal strSelfServiceURL As String, _
                             ByVal iCompanyId As Integer)
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & username & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 16, "Dear") & " <b>" & getTitleCase(username) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & strMainText & " </span></p>")
            StrMessage.Append(strMainText & "</span></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append("<TABLE border = '1'>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TR bgcolor= 'SteelBlue'>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 15, "Employee") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 4, "Training Name") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 5, "Training Mode") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 6, "Vendor") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 7, "Venue") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 8, "Training Dates") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:7%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 9, "Duration") & "</span></b></TD>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("</TR>")
            For idx As Integer = 0 To dr.Length - 1
                StrMessage.Append("<TR>" & vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("ename").ToString & "</span></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("CourseMaster").ToString & "</span></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("TrainingMode").ToString & "</span></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("institute_name").ToString & "</span></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("training_venue").ToString & "</span></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:13%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & eZeeDate.convertDate(dr(idx)("training_startdate").ToString).Date.ToShortDateString & " - " & eZeeDate.convertDate(dr(idx)("training_enddate").ToString).Date.ToShortDateString & "</span></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:7%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("Duration").ToString & "</span></TD>")
                StrMessage.Append("</TR>" & vbCrLf)
            Next
            StrMessage.Append("</TABLE>")

            Select Case intNotificationType
                Case 1, 2
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    Dim strLink As String = ""
                    strLink = strSelfServiceURL & "/Training_Requisition/wPgApplyTrainingRequisition.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dr(0)("mapuserunkid").ToString() & "|" & dr(0)("masterguid").ToString() & "|" & dr(0)("priority").ToString() & "|" & dr(0)("oMappingId").ToString() & "|" & dr(0)("linkedmasterid").ToString() & "|" & dr(0)("crmasterunkid").ToString()))
                    StrMessage.Append(strLink)
                    StrMessage.Append("</span></p>")
            End Select


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetEmailBody; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function GetGuidDictionary(ByVal intEmpid As Integer, _
                                       ByVal intCourseMasterId As Integer, _
                                       ByVal StDate As DateTime, _
                                       ByVal EdDate As DateTime, _
                                       ByVal intInstituteId As Integer, _
                                       ByVal xDataOpr As clsDataOperation) As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim dt As DataTable = Nothing
        Try
            StrQ = "SELECT " & _
                   "     RAM.masterguid,RAT.tranguid " & _
                   "    ,RAT.trainingmodeunkid,RAT.resourcemasterunkid " & _
                   "FROM hrtraining_requisition_approval_master AS RAM " & _
                   "    JOIN hrtraining_requisition_approval_tran AS RAT ON RAT.masterguid = RAM.masterguid " & _
                   "WHERE RAM.isvoid = 0 AND RAT.isvoid = 0 " & _
                   " AND RAM.employeeunkid = '" & intEmpid & "' AND coursemasterunkid = '" & intCourseMasterId & "' " & _
                   " AND CONVERT(NVARCHAR(8),RAM.training_startdate,112) = '" & eZeeDate.convertDate(StDate).ToString & "' " & _
                   " AND CONVERT(NVARCHAR(8),RAM.training_enddate,112) = '" & eZeeDate.convertDate(EdDate).ToString & "' " & _
                   " AND RAM.instituteunkid = '" & intInstituteId & "' AND (RAT.trainingmodeunkid > 0 OR RAT.resourcemasterunkid > 0) "

            dsList = xDataOpr.ExecQuery(StrQ, "List")

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                StrQ = "SELECT " & _
                       "     RAM.masterguid,'' AS tranguid " & _
                       "    ,0 AS trainingmodeunkid,0 AS resourcemasterunkid " & _
                       "FROM hrtraining_requisition_approval_master AS RAM " & _
                       "WHERE RAM.isvoid = 0 " & _
                       " AND RAM.employeeunkid = '" & intEmpid & "' AND coursemasterunkid = '" & intCourseMasterId & "' " & _
                       " AND CONVERT(NVARCHAR(8),RAM.training_startdate,112) = '" & eZeeDate.convertDate(StDate).ToString & "' " & _
                       " AND CONVERT(NVARCHAR(8),RAM.training_enddate,112) = '" & eZeeDate.convertDate(EdDate).ToString & "' " & _
                       " AND RAM.instituteunkid = '" & intInstituteId & "' "

                dsList = xDataOpr.ExecQuery(StrQ, "List")

                If xDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                    Throw exForce
                End If

            End If

            'mdicGuid = dsList.Tables(0).AsEnumerable().ToDictionary(Function(x) x.Field(Of String)("tranguid"), Function(y) y.Field(Of String)("masterguid"))
            'mdicGuidValue = dsList.Tables(0).AsEnumerable().ToDictionary(Function(x) x.Field(Of String)("tranguid"), Function(y) y.Field(Of String)("trainingmodeunkid"))

            dt = dsList.Tables(0).Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetGuidDictionary; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dt
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getCompletionStatusComboList(ByVal strList As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT '" & enCompletionApprovalStatus.Pending & "' AS Id, @Pending AS Name UNION " & _
                    "SELECT '" & enCompletionApprovalStatus.Completed & "' AS Id, @Completed AS Name UNION " & _
                    "SELECT '" & enCompletionApprovalStatus.Rejected & "' AS Id, @Rejected AS Name "

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 105, "Pending"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 106, "Completed"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 107, "Rejected"))
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 108, "Select"))
    
            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getStatusComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Sorry, Training requisition for selected course, startdate, enddate and employee already present. Please add new training requisition.")
			Language.setMessage("clstraining_requisition_master", 3, "NA")
            Language.setMessage(mstrModuleName, 4, "Training Name")
            Language.setMessage(mstrModuleName, 5, "Training Mode")
            Language.setMessage(mstrModuleName, 6, "Vendor")
            Language.setMessage(mstrModuleName, 7, "Venue")
            Language.setMessage(mstrModuleName, 8, "Training Dates")
            Language.setMessage(mstrModuleName, 9, "Duration")
            Language.setMessage(mstrModuleName, 10, "This is to inform you that following employee(s) has applied for training requisition, Please login to aruti to approve/reject them.")
            Language.setMessage(mstrModuleName, 11, "Notification to Approve/Reject Training Requisition")
            Language.setMessage(mstrModuleName, 12, "This is to inform you that following employee(s) applied training requisition has been rejected")
            Language.setMessage(mstrModuleName, 13, "Rejection Notification for Training Requisition")
            Language.setMessage(mstrModuleName, 14, "Sorry, you cannot do approve/reject operation. Reason another user with same level/priority has approved or rejected the training requisition.")
            Language.setMessage(mstrModuleName, 15, "Employee")
			Language.setMessage(mstrModuleName, 16, "Dear")
            Language.setMessage(mstrModuleName, 100, "Pending")
            Language.setMessage(mstrModuleName, 101, "Approved")
            Language.setMessage(mstrModuleName, 102, "Rejected")
            Language.setMessage(mstrModuleName, 103, "Pending for Approval")
            Language.setMessage(mstrModuleName, 104, "Select")
			Language.setMessage("clsMasterData", 862, "External")
			Language.setMessage("clsMasterData", 863, "Internal")
			Language.setMessage(mstrModuleName, 3, "Sorry, No approver(s) defined based on selected for employee user access setting.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
