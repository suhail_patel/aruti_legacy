﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clstraining_requisition_approval_tran.vb
'Purpose    :
'Date       :15-Oct-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clstraining_requisition_approval_tran
    Private Const mstrModuleName As String = "clstraining_requisition_approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mstrMasterguid As String = ""
    Private mdtTran As DataTable
    Private mstrTranguid As String = ""

#Region " Properties "

    Public Property _RequisitionMasterguid(Optional ByVal objOperation As clsDataOperation = Nothing) As String
        Get
            Return mstrMasterguid
        End Get
        Set(ByVal value As String)
            mstrMasterguid = value
            Get_RequisitionApproval_Tran(objOperation)
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Contructor "

    Public Sub New()
        mdtTran = New DataTable("RequisitionTran")
        Dim dCol As DataColumn = Nothing
        Try
            dCol = New DataColumn("tranguid")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("masterguid")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("requisitiontranunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("requisitionmasterunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingmodeunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("resourcemasterunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("item_description")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = GetType(System.DateTime)
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingmode")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("resourcetype")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Method(s) "

    Private Sub Get_RequisitionApproval_Tran(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try

            strQ = "SELECT " & _
                   "     TRAT.tranguid " & _
                   "    ,TRAT.masterguid " & _
                   "    ,TRAT.requisitiontranunkid " & _
                   "    ,TRAT.requisitionmasterunkid " & _
                   "    ,TRAT.trainingmodeunkid " & _
                   "    ,TRAT.resourcemasterunkid " & _
                   "    ,TRAT.item_description " & _
                   "    ,TRAT.isvoid " & _
                   "    ,TRAT.voiddatetime " & _
                   "    ,TRAT.voiduserunkid " & _
                   "    ,TRAT.voidreason " & _
                   "    ,'' AS AUD " & _
                   "    ,ISNULL(TM.name,'') AS trainingmode " & _
                   "    ,ISNULL(RT.name,'') AS resourcetype " & _
                   "FROM hrtraining_requisition_approval_tran AS TRAT " & _
                   "    LEFT JOIN cfcommon_master AS TM ON TM.masterunkid = TRAT.trainingmodeunkid AND TM.isactive = 1 " & _
                   "    LEFT JOIN cfcommon_master AS RT ON RT.masterunkid = TRAT.resourcemasterunkid AND RT.isactive = 1 " & _
                   "WHERE TRAT.isvoid = 0 AND TRAT.masterguid = @masterguid "

            objDataOperation.AddParameter("@masterguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMasterguid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For Each row As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(row)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_RequisitionApproval_Tran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_TrainingRequisition(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    If CInt(.Item("trainingmodeunkid")) <= 0 AndAlso CInt(.Item("resourcemasterunkid")) <= 0 Then
                        Continue For
                    End If
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD").ToString.ToUpper()
                            Case "A"
                                strQ = "INSERT INTO hrtraining_requisition_approval_tran ( " & _
                                       "  tranguid " & _
                                       ", masterguid " & _
                                       ", requisitiontranunkid " & _
                                       ", requisitionmasterunkid " & _
                                       ", trainingmodeunkid " & _
                                       ", resourcemasterunkid " & _
                                       ", item_description " & _
                                       ", isvoid " & _
                                       ", voiddatetime " & _
                                       ", voiduserunkid " & _
                                       ", voidreason" & _
                                       ") VALUES (" & _
                                       "  @tranguid " & _
                                       ", @masterguid " & _
                                       ", @requisitiontranunkid " & _
                                       ", @requisitionmasterunkid " & _
                                       ", @trainingmodeunkid " & _
                                       ", @resourcemasterunkid " & _
                                       ", @item_description " & _
                                       ", @isvoid " & _
                                       ", @voiddatetime " & _
                                       ", @voiduserunkid " & _
                                       ", @voidreason" & _
                                       ") "

                                objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("tranguid").ToString())
                                objDataOperation.AddParameter("@masterguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMasterguid.ToString)
                                objDataOperation.AddParameter("@requisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("requisitiontranunkid"))
                                objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("requisitionmasterunkid").ToString)
                                objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingmodeunkid").ToString)
                                objDataOperation.AddParameter("@resourcemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resourcemasterunkid").ToString)
                                objDataOperation.AddParameter("@item_description", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("item_description").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "U"

                                strQ = "UPDATE hrtraining_requisition_approval_tran SET " & _
                                       "  masterguid = @masterguid" & _
                                       ", requisitiontranunkid = @requisitiontranunkid" & _
                                       ", requisitionmasterunkid = @requisitionmasterunkid" & _
                                       ", trainingmodeunkid = @trainingmodeunkid" & _
                                       ", resourcemasterunkid = @resourcemasterunkid" & _
                                       ", item_description = @item_description" & _
                                       ", isvoid = @isvoid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voidreason = @voidreason " & _
                                       "WHERE tranguid = @tranguid "

                                objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("tranguid").ToString)
                                objDataOperation.AddParameter("@masterguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("masterguid").ToString)
                                objDataOperation.AddParameter("@requisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("requisitiontranunkid"))
                                objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("requisitionmasterunkid").ToString)
                                objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingmodeunkid").ToString)
                                objDataOperation.AddParameter("@resourcemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resourcemasterunkid").ToString)
                                objDataOperation.AddParameter("@item_description", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("item_description").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                strQ = "UPDATE hrtraining_requisition_approval_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voidreason = @voidreason " & _
                                       "WHERE tranguid = @tranguid "

                                objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("tranguid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class
