﻿'************************************************************************************************************************************
'Class Name : clsSaving_Tran.vb
'Purpose    :
'Date       :12/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsSaving_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsSaving_Tran"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSavingtranunkid As Integer
    Private mstrVoucherno As String = String.Empty
    Private mdtEffectivedate As Date
    Private mintPayperiodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintSavingschemeunkid As Integer
    'Private mintSavingperiods As Integer
    Private mdecContribution As Decimal 'Sohail (11 May 2011)
    'Private mdtMaturitydate As Date
    'Private mdblMaturity_Amount As Double
    Private mintSavingstatus As Integer
    Private mdecTotalContribution As Decimal 'Sohail (11 May 2011)
    Private mdecInterest_Rate As Decimal 'Sohail (11 May 2011)
    Private mdecInterest_Amount As Decimal 'Sohail (11 May 2011)
    Private mdecBalance_Amount As Decimal 'Sohail (11 May 2011)
    Private mblnBroughtforward As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidReason As String = String.Empty
    'Anjan (26 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mdtStopDate As Date
    'Anjan (26 Jan 2012)-End 

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END


    'Pinkal (03-Jun-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrWebIP As String = ""
    Private mstrWebHost As String = ""
    'Pinkal (03-Jun-2013) -- End


    'SHANI [12 JAN 2015]-START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    Private mdecBf_Amount As Decimal = 0
    Private mdecBf_Balance As Decimal = 0
    'SHANI [12 JAN 2015]--END 

    'SHANI [09 MAR 2015]-START
    'Enhancement - Add Currency Field.
    Private mintCountryunkid As Integer = 0
    'SHANI [09 MAR 2015]--END 


#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set savingtranunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Savingtranunkid() As Integer
        Get
            Return mintSavingtranunkid
        End Get
        Set(ByVal value As Integer)
            mintSavingtranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voucherno
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voucherno() As String
        Get
            Return mstrVoucherno
        End Get
        Set(ByVal value As String)
            mstrVoucherno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set payperiodunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Payperiodunkid() As Integer
        Get
            Return mintPayperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintPayperiodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set savingschemeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Savingschemeunkid() As Integer
        Get
            Return mintSavingschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintSavingschemeunkid = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set savingperiods
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Savingperiods() As Integer
    '    Get
    '        Return mintSavingperiods
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintSavingperiods = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set contribution
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Contribution() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecContribution
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecContribution = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set maturitydate
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Maturitydate() As Date
    '    Get
    '        Return mdtMaturitydate
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtMaturitydate = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set maturity_amount
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Maturity_Amount() As Double
    '    Get
    '        Return mdblMaturity_Amount
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblMaturity_Amount = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set savingstatus
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Savingstatus() As Integer
        Get
            Return mintSavingstatus
        End Get
        Set(ByVal value As Integer)
            mintSavingstatus = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set balance_amount
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Total_Contribution() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecTotalContribution
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecTotalContribution = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interest_rate
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Interest_Rate() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInterest_Rate
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInterest_Rate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interest_amount
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Interest_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInterest_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInterest_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set balance_amount
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Balance_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecBalance_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecBalance_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set broughtforward
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Broughtforward() As Boolean
        Get
            Return mblnBroughtforward
        End Get
        Set(ByVal value As Boolean)
            mblnBroughtforward = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidReason
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property


    'Anjan (26 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Stopdate() As Date
        Get
            Return mdtStopDate
        End Get
        Set(ByVal value As Date)
            mdtStopDate = value
        End Set
    End Property

    'Anjan (26 Jan 2012)-End 

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END


    'Pinkal (03-Jun-2013) -- Start
    'Enhancement : TRA Changes

    'Public Property _WebIP() As String
    '    Get
    '        Return mstrWebIP
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public Property _WebHost() As String
    '    Get
    '        Return mstrWebHost
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHost = value
    '    End Set
    'End Property


    'SHANI [12 JAN 2015]-START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.

    ''' <summary>
    ''' Purpose: Get or Set Bf_Amount
    ''' Modify By: Shani
    ''' </summary>
    Public Property _Bf_Amount() As Decimal
        Get
            Return mdecBf_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecBf_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Bf_Balance
    ''' Modify By: Shani
    ''' </summary>
    Public Property _Bf_Balance() As Decimal
        Get
            Return mdecBf_Balance
        End Get
        Set(ByVal value As Decimal)
            mdecBf_Balance = value
        End Set
    End Property
    'SHANI [12 JAN 2015]--END 

    'SHANI [09 MAR 2015]-START
    'Enhancement - Add Currency Field.

    ''' <summary>
    ''' Purpose: Get or Set Countryunkid
    ''' Modify By: Shani
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    'SHANI [09 MAR 2015]--END 

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'Anjan (26 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            'strQ = "SELECT " & _
            '  "  savingtranunkid " & _
            '  ", voucherno " & _
            '  ", effectivedate " & _
            '  ", payperiodunkid " & _
            '  ", employeeunkid " & _
            '  ", savingschemeunkid " & _
            '  ", contribution " & _
            '  ", savingstatus " & _
            '  ", ISNULL(total_contribution,0) AS total_contribution " & _
            '  ", ISNULL(interest_rate,0) AS interest_rate " & _
            '  ", ISNULL(interest_amount,0) AS interest_amount " & _
            '  ", ISNULL(balance_amount,0) AS balance_amount " & _
            '  ", broughtforward " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            '  ", ISNULL(convert(char(8),stopdate,112),'') as stopdate" 
            ' "FROM svsaving_tran " & _
            ' "WHERE savingtranunkid = @savingtranunkid "

            strQ = "SELECT " & _
              "  savingtranunkid " & _
              ", voucherno " & _
              ", effectivedate " & _
              ", payperiodunkid " & _
              ", employeeunkid " & _
              ", savingschemeunkid " & _
              ", contribution " & _
              ", savingstatus " & _
              ", ISNULL(total_contribution,0) AS total_contribution " & _
              ", ISNULL(interest_rate,0) AS interest_rate " & _
              ", ISNULL(interest_amount,0) AS interest_amount " & _
              ", ISNULL(balance_amount,0) AS balance_amount " & _
              ", broughtforward " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", CONVERT(CHAR(8),stopdate,112) as stopdate " & _
              ", ISNULL(bf_balance, 0) AS bf_balance " & _
              ", ISNULL(bf_amount, 0) AS bf_amount " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
             "FROM svsaving_tran " & _
             "WHERE savingtranunkid = @savingtranunkid "
            'Anjan (26 Jan 2012)-End 
            'SHANI [12 JAN 2015]--[bf_balance,bf_amount]
            'SHANI [09 Mar 2015]--[Countryunkid]

            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingtranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintSavingtranunkid = CInt(dtRow.Item("savingtranunkid"))
                mstrVoucherno = dtRow.Item("voucherno").ToString
                If IsDBNull(dtRow.Item("effectivedate")) Then
                    mdtEffectivedate = Nothing
                Else
                    mdtEffectivedate = dtRow.Item("effectivedate")
                End If
                mintPayperiodunkid = CInt(dtRow.Item("payperiodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintSavingschemeunkid = CInt(dtRow.Item("savingschemeunkid"))
                'mintsavingperiods = CInt(dtRow.Item("savingperiods"))
                mdecContribution = CDec(dtRow.Item("contribution")) 'Sohail (11 May 2011)
                'If IsDBNull(dtRow.Item("maturitydate")) Then
                '    mdtMaturitydate = Nothing
                'Else
                '    mdtMaturitydate = dtRow.Item("maturitydate")
                'End If

                'mdblmaturity_amount = cdec(dtRow.Item("maturity_amount"))
                mintSavingstatus = CInt(dtRow.Item("savingstatus"))
                mdecTotalContribution = CDec(dtRow.Item("total_contribution")) 'Sohail (11 May 2011)
                mdecInterest_Rate = CDec(dtRow.Item("interest_rate")) 'Sohail (11 May 2011)
                mdecInterest_Amount = CDec(dtRow.Item("interest_amount")) 'Sohail (11 May 2011)
                mdecBalance_Amount = CDec(dtRow.Item("balance_amount")) 'Sohail (11 May 2011)
                mblnBroughtforward = CBool(dtRow.Item("broughtforward"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))


                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                'Anjan (26 Jan 2012)-Start
                'ENHANCEMENT : TRA COMMENTS
                If IsDBNull(dtRow.Item("stopdate")) Then
                    mdtStopDate = Nothing
                Else
                    mdtStopDate = eZeeDate.convertDate(dtRow.Item("stopdate"))
                End If
                'Anjan (26 Jan 2012)-End 

                mstrVoidReason = dtRow.Item("voidreason")

                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                mdecBf_Amount = CInt(dtRow.Item("bf_amount"))
                mdecBf_Balance = CInt(dtRow.Item("bf_balance"))
                'SHANI [12 JAN 2015]--END 

                'SHANI [09 MAR 2015]-START
                'Enhancement - Add Currency Field.
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                'SHANI [09 MAR 2015]--END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    '' <summary>
    '' Modify By: Anjan
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    ''Shani(24-Aug-2015) -- Start
    ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '' 

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Change Saving GetList method for get active Employee saving
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intSavingTranUnkID As Integer = 0, Optional ByVal intEmpUnkID As Integer = 0, Optional ByVal intStatusID As Integer = 0 _
    '                       , Optional ByVal dtPeriodStart As DateTime = Nothing _
    '                       , Optional ByVal dtPeriodEnd As DateTime = Nothing _
    '                       , Optional ByVal mstrUserAccessFilter As String = "" _
    '                       , Optional ByVal mdtAsonDate As DateTime = Nothing _
    '                       , Optional ByVal mstrEmployeeIDs As String = "") As DataSet 'Sohail (06 Jan 2012)    '[mstrUserAccessFilter] Pinkal (05-Jun-2013)

    '    'Pinkal (12 Jan 2015) -- Enhancement - CHANGE IN SAVINGS MODULE  [Optional ByVal mdtAsonDate As DateTime = Nothing, Optional ByVal mstrEmployeeIDs As String = ""] .

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try

    '        'Sohail (04 Mar 2011) -- Start
    '        'strQ = "SELECT svsaving_tran.savingtranunkid " & _
    '        '               ", svsaving_tran.voucherno " & _
    '        '               ", convert(char(8),svsaving_tran.effectivedate,112) AS effectivedate " & _
    '        '               ", svsaving_tran.contribution " & _
    '        '               ", svsaving_tran.savingstatus " & _
    '        '               ", ISNULL(svsaving_tran.total_contribution,0) AS total_contribution " & _
    '        '               ", ISNULL(svsaving_tran.interest_rate,0) AS interest_rate  " & _
    '        '               ", ISNULL(svsaving_tran.interest_amount,0) AS interest_amount  " & _
    '        '               ", ISNULL(svsaving_tran.balance_amount,0) AS balance_amount " & _
    '        '               ", svsaving_tran.broughtforward " & _
    '        '               ", svsaving_tran.userunkid " & _
    '        '               ", svsaving_tran.isvoid " & _
    '        '               ", svsaving_tran.voiduserunkid " & _
    '        '               ", svsaving_tran.voiddatetime " & _
    '        '               ", svsaving_tran.voidreason " & _
    '        '               ", svsavingscheme_master.savingschemename AS savingscheme " & _
    '        '               ", remarkid.remark AS remark " & _
    '        '               ", cfcommon_period_tran.period_name AS period " & _
    '        '               ", hremployee_master.firstname+' '+hremployee_master.surname AS employeename " & _
    '        '               ", svsaving_tran.employeeunkid " & _
    '        '               ", svsaving_tran.savingschemeunkid " & _
    '        '               ", svsaving_tran.payperiodunkid " & _
    '        '         "FROM svsaving_tran " & _
    '        '         "LEFT JOIN svsavingscheme_master ON svsavingscheme_master.savingschemeunkid=svsaving_tran.savingschemeunkid " & _
    '        '         "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid=svsaving_tran.payperiodunkid " & _
    '        '         " LEFT JOIN ( SELECT t1.savingstatustranunkid,t1.savingtranunkid,t1.remark " & _
    '        '                        " FROM  svsaving_status_tran AS t1 " & _
    '        '                                "JOIN (SELECT MAX(svsaving_status_tran.savingstatustranunkid) AS id " & _
    '        '                                                ",svsaving_status_tran.savingtranunkid " & _
    '        '                                        "FROM svsaving_status_tran " & _
    '        '                                        "WHERE isvoid = 0 " & _
    '        '                                        "GROUP BY  svsaving_status_tran.savingtranunkid )  AS savingtran " & _
    '        '                                " ON t1.savingstatustranunkid=savingtran.id) AS remarkid " & _
    '        '            "ON remarkid.savingtranunkid=svsaving_tran.savingtranunkid  " & _
    '        '         "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=svsaving_tran.employeeunkid " & _
    '        '         "WHERE svsaving_tran.isvoid=0 "


    '        'Anjan (26 Jan 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        'strQ = "SELECT  svsaving_tran.savingtranunkid " & _
    '        '                          ", svsaving_tran.voucherno " & _
    '        '                          ", CONVERT(CHAR(8),svsaving_tran.effectivedate,112) AS effectivedate " & _
    '        '                          ", svsaving_tran.payperiodunkid " & _
    '        '                          ", cfcommon_period_tran.period_name AS period " & _
    '        '                          ", svsaving_tran.employeeunkid " & _
    '        '                          ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename " & _
    '        '                          ", svsaving_tran.savingschemeunkid " & _
    '        '                          ", svsavingscheme_master.savingschemename as savingscheme " & _
    '        '                          ", svsaving_tran.contribution " & _
    '        '                          ", svsaving_tran.savingstatus " & _
    '        '                          ", svsaving_tran.total_contribution " & _
    '        '                          ", svsaving_tran.interest_rate " & _
    '        '                          ", svsaving_tran.interest_amount " & _
    '        '                          ", svsaving_tran.balance_amount " & _
    '        '                          ", svsaving_tran.broughtforward " & _
    '        '                          ", svsaving_tran.userunkid " & _
    '        '                          ", svsaving_tran.isvoid " & _
    '        '                          ", svsaving_tran.voiduserunkid " & _
    '        '                          ", svsaving_tran.voiddatetime " & _
    '        '                          ", svsaving_tran.voidreason " & _
    '        '                          ", Saving_Status.savingstatustranunkid " & _
    '        '                          ", CONVERT(CHAR(8),Saving_Status.status_date,112) AS status_date " & _
    '        '                          ", Saving_Status.statusunkid " & _
    '        '                          ", Saving_Status.remark " & _
    '        '                    "FROM    svsaving_tran " & _
    '        '                            "LEFT JOIN ( SELECT  svsaving_status_tran.savingstatustranunkid " & _
    '        '                                              ", svsaving_status_tran.savingtranunkid " & _
    '        '                                              ", svsaving_status_tran.status_date " & _
    '        '                                              ", svsaving_status_tran.statusunkid " & _
    '        '                                              ", svsaving_status_tran.remark " & _
    '        '                                              ", svsaving_status_tran.userunkid " & _
    '        '                                              ", svsaving_status_tran.isvoid " & _
    '        '                                              ", svsaving_status_tran.voiddatetime " & _
    '        '                                              ", svsaving_status_tran.voiduserunkid " & _
    '        '                                        "FROM    svsaving_status_tran " & _
    '        '                                                "INNER JOIN ( SELECT MAX(svsaving_status_tran.status_date) AS status_date " & _
    '        '                                                                  ", svsaving_status_tran.savingtranunkid " & _
    '        '                                                             "FROM   svsaving_status_tran " & _
    '        '                                                             "WHERE  ISNULL(isvoid, 0) = 0 " & _
    '        '                                                             "GROUP BY svsaving_status_tran.savingtranunkid " & _
    '        '                                                           ") AS SavingStatus ON svsaving_status_tran.savingtranunkid = SavingStatus.savingtranunkid " & _
    '        '                                                                                "AND svsaving_status_tran.status_date = SavingStatus.status_date " & _
    '        '                                        "WHERE   ISNULL(svsaving_status_tran.isvoid, 0) = 0 " & _
    '        '                                      ") AS Saving_Status ON svsaving_tran.savingtranunkid = Saving_Status.savingtranunkid " & _
    '        '                            "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
    '        '                            "LEFT JOIN hremployee_master ON svsaving_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                            "LEFT JOIN cfcommon_period_tran ON svsaving_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '                    "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 "
    '        'Sohail (04 Mar 2011) -- End

    '        'Anjan (02 Mar 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '        'strQ = "SELECT  svsaving_tran.savingtranunkid " & _
    '        '                          ", svsaving_tran.voucherno " & _
    '        '                          ", CONVERT(CHAR(8),svsaving_tran.effectivedate,112) AS effectivedate " & _
    '        '                          ", svsaving_tran.payperiodunkid " & _
    '        '                          ", cfcommon_period_tran.period_name AS period " & _
    '        '                          ", svsaving_tran.employeeunkid " & _
    '        '                          ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename " & _
    '        '                          ", svsaving_tran.savingschemeunkid " & _
    '        '                          ", svsavingscheme_master.savingschemename as savingscheme " & _
    '        '                          ", svsaving_tran.contribution " & _
    '        '                          ", svsaving_tran.savingstatus " & _
    '        '                          ", svsaving_tran.total_contribution " & _
    '        '                          ", svsaving_tran.interest_rate " & _
    '        '                          ", svsaving_tran.interest_amount " & _
    '        '                          ", svsaving_tran.balance_amount " & _
    '        '                          ", svsaving_tran.broughtforward " & _
    '        '                          ", svsaving_tran.userunkid " & _
    '        '                          ", svsaving_tran.isvoid " & _
    '        '                          ", svsaving_tran.voiduserunkid " & _
    '        '                          ", svsaving_tran.voiddatetime " & _
    '        '                          ", svsaving_tran.voidreason " & _
    '        '                          ", Saving_Status.savingstatustranunkid " & _
    '        '                          ", CONVERT(CHAR(8),Saving_Status.status_date,112) AS status_date " & _
    '        '                          ", Saving_Status.statusunkid " & _
    '        '                          ", Saving_Status.remark " & _
    '        '                          ", ISNULL(CONVERT(CHAR(8),svsaving_tran.stopdate,112),'') as stopdate " & _
    '        '                    "FROM  svsaving_tran " & _
    '        '                            "LEFT JOIN ( SELECT  svsaving_status_tran.savingstatustranunkid " & _
    '        '                                              ", svsaving_status_tran.savingtranunkid " & _
    '        '                                              ", svsaving_status_tran.status_date " & _
    '        '                                              ", svsaving_status_tran.statusunkid " & _
    '        '                                              ", svsaving_status_tran.remark " & _
    '        '                                              ", svsaving_status_tran.userunkid " & _
    '        '                                              ", svsaving_status_tran.isvoid " & _
    '        '                                              ", svsaving_status_tran.voiddatetime " & _
    '        '                                              ", svsaving_status_tran.voiduserunkid " & _
    '        '                                        "FROM    svsaving_status_tran " & _
    '        '                                                "INNER JOIN ( SELECT MAX(svsaving_status_tran.status_date) AS status_date " & _
    '        '                                                                  ", svsaving_status_tran.savingtranunkid " & _
    '        '                                                             "FROM   svsaving_status_tran " & _
    '        '                                                             "WHERE  ISNULL(isvoid, 0) = 0 " & _
    '        '                                                             "GROUP BY svsaving_status_tran.savingtranunkid " & _
    '        '                                                           ") AS SavingStatus ON svsaving_status_tran.savingtranunkid = SavingStatus.savingtranunkid " & _
    '        '                                                                                "AND svsaving_status_tran.status_date = SavingStatus.status_date " & _
    '        '                                        "WHERE   ISNULL(svsaving_status_tran.isvoid, 0) = 0 " & _
    '        '                                      ") AS Saving_Status ON svsaving_tran.savingtranunkid = Saving_Status.savingtranunkid " & _
    '        '                            "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
    '        '                            "LEFT JOIN hremployee_master ON svsaving_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                            "LEFT JOIN cfcommon_period_tran ON svsaving_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '                    "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 "
    '        'Sohail (29 Jun 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'strQ = "SELECT  svsaving_tran.savingtranunkid " & _
    '        '                          ", svsaving_tran.voucherno " & _
    '        '                          ", CONVERT(CHAR(8),svsaving_tran.effectivedate,112) AS effectivedate " & _
    '        '                          ", svsaving_tran.payperiodunkid " & _
    '        '                          ", cfcommon_period_tran.period_name AS period " & _
    '        '                          ", svsaving_tran.employeeunkid " & _
    '        '                          ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename " & _
    '        '                          ", svsaving_tran.savingschemeunkid " & _
    '        '                          ", svsavingscheme_master.savingschemename as savingscheme " & _
    '        '                          ", svsaving_tran.contribution " & _
    '        '                          ", svsaving_tran.savingstatus " & _
    '        '                          ", svsaving_tran.total_contribution " & _
    '        '                          ", svsaving_tran.interest_rate " & _
    '        '                          ", svsaving_tran.interest_amount " & _
    '        '                          ", svsaving_tran.balance_amount " & _
    '        '                          ", svsaving_tran.broughtforward " & _
    '        '                          ", svsaving_tran.userunkid " & _
    '        '                          ", svsaving_tran.isvoid " & _
    '        '                          ", svsaving_tran.voiduserunkid " & _
    '        '                          ", svsaving_tran.voiddatetime " & _
    '        '                          ", svsaving_tran.voidreason " & _
    '        '                          ", Saving_Status.savingstatustranunkid " & _
    '        '                          ", CONVERT(CHAR(8),Saving_Status.status_date,112) AS status_date " & _
    '        '                          ", Saving_Status.statusunkid " & _
    '        '                          ", Saving_Status.remark " & _
    '        '                          ", ISNULL(CONVERT(CHAR(8),svsaving_tran.stopdate,112),'') as stopdate " & _
    '        '                          ", hremployee_master.employeecode as empcode " & _
    '        '                          ", svsavingscheme_master.savingschemecode as savingscode " & _
    '        '                    "FROM  svsaving_tran " & _
    '        '                            "LEFT JOIN ( SELECT  svsaving_status_tran.savingstatustranunkid " & _
    '        '                                              ", svsaving_status_tran.savingtranunkid " & _
    '        '                                              ", svsaving_status_tran.status_date " & _
    '        '                                              ", svsaving_status_tran.statusunkid " & _
    '        '                                              ", svsaving_status_tran.remark " & _
    '        '                                              ", svsaving_status_tran.userunkid " & _
    '        '                                              ", svsaving_status_tran.isvoid " & _
    '        '                                              ", svsaving_status_tran.voiddatetime " & _
    '        '                                              ", svsaving_status_tran.voiduserunkid " & _
    '        '                                        "FROM    svsaving_status_tran " & _
    '        '                                                "INNER JOIN ( SELECT MAX(svsaving_status_tran.status_date) AS status_date " & _
    '        '                                                                  ", svsaving_status_tran.savingtranunkid " & _
    '        '                                                             "FROM   svsaving_status_tran " & _
    '        '                                                             "WHERE  ISNULL(isvoid, 0) = 0 " & _
    '        '                                                             "GROUP BY svsaving_status_tran.savingtranunkid " & _
    '        '                                                           ") AS SavingStatus ON svsaving_status_tran.savingtranunkid = SavingStatus.savingtranunkid " & _
    '        '                                                                                "AND svsaving_status_tran.status_date = SavingStatus.status_date " & _
    '        '                                        "WHERE   ISNULL(svsaving_status_tran.isvoid, 0) = 0 " & _
    '        '                                      ") AS Saving_Status ON svsaving_tran.savingtranunkid = Saving_Status.savingtranunkid " & _
    '        '                            "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
    '        '                            "LEFT JOIN hremployee_master ON svsaving_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                            "LEFT JOIN cfcommon_period_tran ON svsaving_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '                    "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 "



    '        'Pinkal (12 Jan 2015) -- Start
    '        'Enhancement - CHANGE IN SAVINGS MODULE  [QUERY GIVEN BY SOHAIL ON 20-JAN-2015] .

    '        'strQ = "SELECT  svsaving_tran.savingtranunkid " & _
    '        '                          ", svsaving_tran.voucherno " & _
    '        '                          ", CONVERT(CHAR(8),svsaving_tran.effectivedate,112) AS effectivedate " & _
    '        '                          ", svsaving_tran.payperiodunkid " & _
    '        '                          ", cfcommon_period_tran.period_name AS period " & _
    '        '                          ", svsaving_tran.employeeunkid " & _
    '        '                          ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename " & _
    '        '                          ", svsaving_tran.savingschemeunkid " & _
    '        '              ", svsavingscheme_master.savingschemename AS savingscheme " & _
    '        '                          ", svsaving_tran.contribution " & _
    '        '                          ", svsaving_tran.savingstatus " & _
    '        '                          ", svsaving_tran.total_contribution " & _
    '        '                          ", svsaving_tran.interest_rate " & _
    '        '                          ", svsaving_tran.interest_amount " & _
    '        '                          ", svsaving_tran.balance_amount " & _
    '        '                          ", svsaving_tran.broughtforward " & _
    '        '                          ", svsaving_tran.userunkid " & _
    '        '                          ", svsaving_tran.isvoid " & _
    '        '                          ", svsaving_tran.voiduserunkid " & _
    '        '                          ", svsaving_tran.voiddatetime " & _
    '        '                          ", svsaving_tran.voidreason " & _
    '        '                          ", Saving_Status.savingstatustranunkid " & _
    '        '                          ", CONVERT(CHAR(8),Saving_Status.status_date,112) AS status_date " & _
    '        '                          ", Saving_Status.statusunkid " & _
    '        '                          ", Saving_Status.remark " & _
    '        '              ", ISNULL(CONVERT(CHAR(8), svsaving_tran.stopdate, 112), '') AS stopdate " & _
    '        '              ", hremployee_master.employeecode AS empcode " & _
    '        '              ", svsavingscheme_master.savingschemecode AS savingscode " & _
    '        '                    "FROM  svsaving_tran " & _
    '        '                            "LEFT JOIN ( SELECT  svsaving_status_tran.savingstatustranunkid " & _
    '        '                                              ", svsaving_status_tran.savingtranunkid " & _
    '        '                                              ", svsaving_status_tran.status_date " & _
    '        '                                              ", svsaving_status_tran.statusunkid " & _
    '        '                                              ", svsaving_status_tran.remark " & _
    '        '                                        "FROM    svsaving_status_tran " & _
    '        '                                    "JOIN ( SELECT   MAX(svsaving_status_tran.savingstatustranunkid) AS savingstatustranunkid " & _
    '        '                                                  ", svsaving_status_tran.savingtranunkid " & _
    '        '                                           "FROM     svsaving_status_tran " & _
    '        '                                                    "INNER JOIN ( SELECT " & _
    '        '                                                                      "MAX(svsaving_status_tran.status_date) AS status_date " & _
    '        '                                                                  ", svsaving_status_tran.savingtranunkid " & _
    '        '                                                             "FROM   svsaving_status_tran " & _
    '        '                                                             "WHERE  ISNULL(isvoid, 0) = 0 " & _
    '        '                                                             "GROUP BY svsaving_status_tran.savingtranunkid " & _
    '        '                                                           ") AS SavingStatus ON svsaving_status_tran.savingtranunkid = SavingStatus.savingtranunkid " & _
    '        '                                                                                "AND svsaving_status_tran.status_date = SavingStatus.status_date " & _
    '        '                                        "WHERE   ISNULL(svsaving_status_tran.isvoid, 0) = 0 " & _
    '        '                                           "GROUP BY svsaving_status_tran.savingtranunkid " & _
    '        '                                         ") AS A ON A.savingstatustranunkid = svsaving_status_tran.savingstatustranunkid " & _
    '        '                                      ") AS Saving_Status ON svsaving_tran.savingtranunkid = Saving_Status.savingtranunkid " & _
    '        '                            "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
    '        '                            "LEFT JOIN hremployee_master ON svsaving_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                            "LEFT JOIN cfcommon_period_tran ON svsaving_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '                                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '        '                    "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 "
    '        strQ = "SELECT  a.savingtranunkid " & _
    '                  ", a.voucherno " & _
    '                  ", a.effectivedate " & _
    '                  ", a.payperiodunkid " & _
    '                  ", a.period " & _
    '                  ", a.employeeunkid " & _
    '                  ", a.employeename " & _
    '                  ", a.savingschemeunkid " & _
    '                  ", a.savingscheme " & _
    '                  ", b.contribution " & _
    '                  ", a.total_contribution " & _
    '                  ", c.interest_rate " & _
    '                  ", a.interest_amount " & _
    '                  ", a.balance_amount " & _
    '                  ", a.broughtforward " & _
    '                  ", a.userunkid " & _
    '                  ", a.isvoid " & _
    '                  ", a.voiduserunkid " & _
    '                  ", a.voiddatetime " & _
    '                  ", a.voidreason " & _
    '                  ", a.savingstatustranunkid " & _
    '                  ", a.status_date " & _
    '                  ", a.statusunkid as savingstatus " & _
    '                  ", a.remark " & _
    '                  ", a.stopdate " & _
    '                  ", a.empcode " & _
    '                  ", a.savingscode " & _
    '                  ", a.bf_balance " & _
    '                  ", a.bf_amount " & _
    '                  ", a.countryunkid " & _
    '            "FROM    ( SELECT    svsaving_tran.savingtranunkid " & _
    '                          ", svsaving_tran.voucherno " & _
    '                          ", CONVERT(CHAR(8),svsaving_tran.effectivedate,112) AS effectivedate " & _
    '                          ", svsaving_tran.payperiodunkid " & _
    '                          ", cfcommon_period_tran.period_name AS period " & _
    '                          ", svsaving_tran.employeeunkid " & _
    '                          ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename " & _
    '                          ", svsaving_tran.savingschemeunkid " & _
    '              ", svsavingscheme_master.savingschemename AS savingscheme " & _
    '                          ", svsaving_tran.total_contribution " & _
    '                          ", svsaving_tran.interest_amount " & _
    '                          ", svsaving_tran.balance_amount " & _
    '                          ", svsaving_tran.broughtforward " & _
    '                          ", svsaving_tran.userunkid " & _
    '                          ", svsaving_tran.isvoid " & _
    '                          ", svsaving_tran.voiduserunkid " & _
    '                          ", svsaving_tran.voiddatetime " & _
    '                          ", svsaving_tran.voidreason " & _
    '                              ", svsaving_status_tran.savingstatustranunkid " & _
    '                              ", CONVERT(CHAR(8), svsaving_status_tran.status_date, 112) AS status_date " & _
    '                              ", svsaving_status_tran.statusunkid " & _
    '                              ", svsaving_status_tran.remark " & _
    '              ", ISNULL(CONVERT(CHAR(8), svsaving_tran.stopdate, 112), '') AS stopdate " & _
    '              ", hremployee_master.employeecode AS empcode " & _
    '              ", svsavingscheme_master.savingschemecode AS savingscode " & _
    '                              ", ISNULL(svsaving_tran.bf_balance, 0) AS bf_balance " & _
    '                              ", ISNULL(svsaving_tran.bf_amount, 0) AS bf_amount " & _
    '                                  ", ISNULL(svsaving_tran.countryunkid, 0) AS countryunkid " & _
    '                              ", DENSE_RANK() OVER ( PARTITION BY svsaving_status_tran.savingtranunkid ORDER BY statusperiod.end_date DESC, svsaving_status_tran.status_date DESC, svsaving_status_tran.savingstatustranunkid DESC ) AS ROWNO " & _
    '                    "FROM  svsaving_tran " & _
    '                                "LEFT JOIN svsaving_status_tran ON svsaving_status_tran.savingtranunkid = svsaving_tran.savingtranunkid " & _
    '                            "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
    '                            "LEFT JOIN hremployee_master ON svsaving_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                            "LEFT JOIN cfcommon_period_tran ON svsaving_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                                        "AND cfcommon_period_tran.isactive = 1 " & _
    '                                                                        "AND cfcommon_period_tran.modulerefid = 1 " & _
    '                                "LEFT JOIN cfcommon_period_tran AS statusperiod ON statusperiod.periodunkid = svsaving_status_tran.periodunkid " & _
    '                                                                        "AND statusperiod.isactive = 1 " & _
    '                                                                        "AND statusperiod.modulerefid = " & enModuleReference.Payroll & " " & _
    '                      "WHERE    ISNULL(svsaving_tran.isvoid, 0) = 0  AND    ISNULL(svsaving_status_tran.isvoid,0) = 0 "
    '        'Sohail (09 Mar 2015) - [countryunkid]
    '        'SHANI [12 JAN 2015]--[bf_balance,bf_amount]

    '        If mstrEmployeeIDs.Trim.Length > 0 Then
    '            strQ &= "AND svsaving_tran.employeeunkid IN (" & mstrEmployeeIDs & ")"
    '        End If

    '        If mdtAsonDate <> Nothing Then
    '            strQ &= " AND Convert(char(8),statusperiod.start_date,112) <= @asondate "
    '        End If

    '       If dtPeriodStart <> Nothing AndAlso dtPeriodEnd <> Nothing Then
    '           strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '           objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
    '           objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
    '       End If

    '       If mstrUserAccessFilter.ToString().Trim.Length > 0 Then
    '           strQ &= mstrUserAccessFilter
    '       Else
    '           strQ &= UserAccessLevel._AccessLevelFilterString
    '       End If

    '        strQ &= ") AS A " & _
    '                    " JOIN ( SELECT   DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY end_date DESC ) AS ROWNO " & _
    '                    "          , svsaving_tran.savingtranunkid " & _
    '                    "          , svsaving_contribution_tran.contribution " & _
    '                    " FROM svsaving_tran " & _
    '                    " LEFT JOIN svsaving_contribution_tran ON svsaving_tran.savingtranunkid = svsaving_contribution_tran.savingtranunkid " & _
    '                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_contribution_tran.periodunkid " & _
    '                            "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 AND ISNULL(svsaving_contribution_tran.isvoid, 0) = 0"

    '        If mdtAsonDate <> Nothing Then
    '            strQ &= " AND Convert(char(8),cfcommon_period_tran.start_date,112) <= @asondate "
    '        End If

    '        If mstrEmployeeIDs.Trim.Length > 0 Then
    '            strQ &= "AND svsaving_tran.employeeunkid IN (" & mstrEmployeeIDs & ")"
    '        End If

    '        strQ &= "            ) AS b ON a.savingtranunkid = b.savingtranunkid " & _
    '                    "  JOIN ( SELECT   DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY end_date DESC,svsaving_interest_rate_tran.effectivedate DESC ) AS ROWNO " & _
    '                    " , svsaving_tran.savingtranunkid " & _
    '                    ", svsaving_interest_rate_tran.interest_rate " & _
    '                    " FROM svsaving_tran " & _
    '                    " LEFT JOIN svsaving_interest_rate_tran ON svsaving_tran.savingtranunkid = svsaving_interest_rate_tran.savingtranunkid " & _
    '                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_interest_rate_tran.periodunkid " & _
    '                            "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 AND ISNULL(svsaving_interest_rate_tran.isvoid, 0) = 0 "


    '        If mdtAsonDate <> Nothing Then
    '            strQ &= " AND Convert(char(8),svsaving_interest_rate_tran.effectivedate,112) <= @asondate "
    '            objDataOperation.AddParameter("@asondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsonDate))
    '        End If

    '        If mstrEmployeeIDs.Trim.Length > 0 Then
    '            strQ &= "AND svsaving_tran.employeeunkid IN (" & mstrEmployeeIDs & ")"
    '        End If

    '        strQ &= " ) AS c ON a.savingtranunkid = c.savingtranunkid " & _
    '                    " WHERE   A.ROWNO = 1 and b.ROWNO =1 and c.ROWNO =1"

    '        'Pinkal (12 Jan 2015) -- End

    '        'Sohail (29 Jun 2012) -- End
    '        'Anjan (02 Mar 2012)-End 



    '        'Anjan (26 Jan 2012)-End 

    '        'If blnOnlyActive Then
    '        '    strQ &= " WHERE isactive = 1 "
    '        'End If

    '        'Pinkal (12 Jan 2015) -- Start
    '        'Enhancement - CHANGE IN SAVINGS MODULE.
    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        'If dtPeriodStart <> Nothing AndAlso dtPeriodEnd <> Nothing Then
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 " 
    '        '    'Sohail (06 Jan 2012) -- Start 
    '        '    'TRA - ENHANCEMENT
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        'Anjan (09 Aug 2011)-End 
    '        'Pinkal (12 Jan 2015) -- End

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select


    '        'Pinkal (12 Jan 2015) -- Start
    '        'Enhancement - CHANGE IN SAVINGS MODULE.

    '        'If mstrUserAccessFilter.ToString().Trim.Length > 0 Then
    '        '    strQ &= mstrUserAccessFilter
    '        'Else
    '        '    strQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If

    '        'Pinkal (12 Jan 2015) -- End

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Sohail (24 Jun 2011) -- End

    '        'Sohail (04 Mar 2011) -- Start


    '        'Pinkal (12 Jan 2015) -- Start
    '        'Enhancement - CHANGE IN SAVINGS MODULE.

    '        'If intSavingTranUnkID > 0 Then
    '        '    strQ &= "AND svsaving_tran.savingtranunkid = @savingtranunkid "
    '        '    objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSavingTranUnkID)
    '        'End If

    '        'If intEmpUnkID > 0 Then
    '        '    strQ &= "AND svsaving_tran.employeeunkid = @employeeunkid "
    '        '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
    '        'End If

    '        'If intStatusID > 0 Then
    '        '    strQ &= "AND Saving_Status.statusunkid = @statusunkid "
    '        '    objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
    '        'End If

    '        If intSavingTranUnkID > 0 Then
    '            strQ &= "AND  a.savingtranunkid  = @savingtranunkid "
    '            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSavingTranUnkID)
    '        End If

    '        If intEmpUnkID > 0 Then
    '            strQ &= "AND a.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
    '        End If

    '        If intStatusID > 0 Then
    '            strQ &= "AND savingstatus = @statusunkid "
    '            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
    '        End If

    '        'Pinkal (12 Jan 2015) -- End

    '        'Sohail (04 Mar 2011) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal dtPeriodStart As DateTime, _
                            ByVal dtPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal mstrFlter As String = "", _
                            Optional ByVal blnReinstatementdate As Boolean = False, _
                            Optional ByVal intSavingTranUnkID As Integer = 0, _
                            Optional ByVal intEmpUnkID As Integer = 0, _
                            Optional ByVal intStatusID As Integer = 0, _
                            Optional ByVal mdtAsonDate As DateTime = Nothing, _
                            Optional ByVal mstrEmployeeIDs As String = "", _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal mstrAdvanceFilter As String = "", _
                            Optional ByVal strOuterFilter As String = "" _
                            ) As DataSet
        'Sohail (25 Aug 2021) - [mstrAdvanceFilter, strOuterFilter]
        'Sohail (06 Jan 2016) - [blnApplyUserAccessFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            'Sohail (25 Aug 2021) -- Start
            'TRA Issue :  : Slowness on edit employee on employee master screen.
            Dim dsStatus As DataSet = (New clsMasterData).GetLoan_Saving_Status("List", True)
            'Sohail (25 Aug 2021) -- End
            'Sohail (25 Aug 2021) -- End

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, blnReinstatementdate, , xDatabaseName)
            'Sohail (06 Jan 2016) -- Start
            'Enhancement - Show Close Year Process Logs on Close Year Wizard.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, , xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, , xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If
            'Sohail (06 Jan 2016) -- End
            If mstrAdvanceFilter.Trim.Length > 0 Then 'Sohail (25 Aug 2021)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, xDatabaseName)
            End If 'Sohail (25 Aug 2021)

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            strQ = "SELECT   hremployee_master.employeeunkid " & _
                          ", hremployee_master.employeecode AS empcode  " & _
                          ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename "

            strQ &= "INTO #TableEmp " & _
                     "FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If intEmpUnkID > 0 Then
                strQ &= " AND hremployee_master.employeeunkid = " & intEmpUnkID & " "
            End If

            If mstrEmployeeIDs.Trim.Length > 0 Then
                strQ &= "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIDs & ") "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvanceFilter & " "
            End If
            'Sohail (25 Aug 2021) -- End

            strQ &= "SELECT  a.savingtranunkid " & _
                      ", a.voucherno " & _
                      ", a.effectivedate " & _
                      ", a.payperiodunkid " & _
                      ", a.period " & _
                      ", a.employeeunkid " & _
                      ", a.employeename " & _
                      ", a.savingschemeunkid " & _
                      ", a.savingscheme " & _
                      ", b.contribution " & _
                      ", a.total_contribution " & _
                      ", c.interest_rate " & _
                      ", a.interest_amount " & _
                      ", a.balance_amount " & _
                      ", a.broughtforward " & _
                      ", a.userunkid " & _
                      ", a.isvoid " & _
                      ", a.voiduserunkid " & _
                      ", a.voiddatetime " & _
                      ", a.voidreason " & _
                      ", a.savingstatustranunkid " & _
                      ", a.status_date " & _
                      ", a.statusunkid as savingstatus " & _
                      ", a.remark " & _
                      ", a.stopdate " & _
                      ", a.empcode " & _
                      ", a.savingscode " & _
                      ", a.bf_balance " & _
                      ", a.bf_amount " & _
                      ", a.countryunkid "

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            strQ &= ", CASE a.statusunkid "
            For Each row As DataRow In dsStatus.Tables(0).Rows
                strQ &= "       WHEN " & CInt(row.Item("Id")) & " THEN '" & row.Item("NAME").ToString & "' "
            Next
            strQ &= " END AS statusname "
            'Sohail (25 Aug 2021) -- End

            strQ &= "FROM    ( SELECT    svsaving_tran.savingtranunkid " & _
                                      ", svsaving_tran.voucherno " & _
                                      ", CONVERT(CHAR(8),svsaving_tran.effectivedate,112) AS effectivedate " & _
                                      ", svsaving_tran.payperiodunkid " & _
                                      ", cfcommon_period_tran.period_name AS period " & _
                                      ", svsaving_tran.employeeunkid " & _
                                      ", #TableEmp.employeename " & _
                                      ", svsaving_tran.savingschemeunkid " & _
                          ", svsavingscheme_master.savingschemename AS savingscheme " & _
                                      ", svsaving_tran.total_contribution " & _
                                      ", svsaving_tran.interest_amount " & _
                                      ", svsaving_tran.balance_amount " & _
                                      ", svsaving_tran.broughtforward " & _
                                      ", svsaving_tran.userunkid " & _
                                      ", svsaving_tran.isvoid " & _
                                      ", svsaving_tran.voiduserunkid " & _
                                      ", svsaving_tran.voiddatetime " & _
                                      ", svsaving_tran.voidreason " & _
                                  ", svsaving_status_tran.savingstatustranunkid " & _
                                  ", CONVERT(CHAR(8), svsaving_status_tran.status_date, 112) AS status_date " & _
                                  ", svsaving_status_tran.statusunkid " & _
                                  ", svsaving_status_tran.remark " & _
                          ", ISNULL(CONVERT(CHAR(8), svsaving_tran.stopdate, 112), '') AS stopdate " & _
                          ", #TableEmp.empcode " & _
                          ", svsavingscheme_master.savingschemecode AS savingscode " & _
                                  ", ISNULL(svsaving_tran.bf_balance, 0) AS bf_balance " & _
                                  ", ISNULL(svsaving_tran.bf_amount, 0) AS bf_amount " & _
                                      ", ISNULL(svsaving_tran.countryunkid, 0) AS countryunkid " & _
                                  ", DENSE_RANK() OVER ( PARTITION BY svsaving_status_tran.savingtranunkid ORDER BY statusperiod.end_date DESC, svsaving_status_tran.status_date DESC, svsaving_status_tran.savingstatustranunkid DESC ) AS ROWNO " & _
                                "FROM  svsaving_tran " & _
                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = svsaving_tran.employeeunkid " & _
                                    "LEFT JOIN svsaving_status_tran ON svsaving_status_tran.savingtranunkid = svsaving_tran.savingtranunkid " & _
                                        "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON svsaving_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                      "AND cfcommon_period_tran.isactive = 1 " & _
                                                                      "AND cfcommon_period_tran.modulerefid = 1 " & _
                                    "LEFT JOIN cfcommon_period_tran AS statusperiod ON statusperiod.periodunkid = svsaving_status_tran.periodunkid " & _
                                                                              "AND statusperiod.isactive = 1 " & _
                                                                            "AND statusperiod.modulerefid = " & enModuleReference.Payroll & " "
            'Sohail (25 Aug 2021) - [LEFT JOIN hremployee_master]=[JOIN #TableEmp]

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            'Sohail (25 Aug 2021) -- End

            strQ &= "WHERE    ISNULL(svsaving_tran.isvoid, 0) = 0  AND    ISNULL(svsaving_status_tran.isvoid,0) = 0 "

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    strQ &= " AND " & xUACFiltrQry
            'End If

            'If mstrEmployeeIDs.Trim.Length > 0 Then
            '    strQ &= "AND svsaving_tran.employeeunkid IN (" & mstrEmployeeIDs & ")"
            'End If
            'Sohail (25 Aug 2021) -- End

            If mdtAsonDate <> Nothing Then
                strQ &= " AND Convert(char(8),statusperiod.start_date,112) <= @asondate "
            End If

            If mstrFlter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFlter
            End If

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If
            'Sohail (25 Aug 2021) -- End

            strQ &= ") AS A " & _
                        " JOIN ( SELECT   DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY end_date DESC ) AS ROWNO " & _
                        "          , svsaving_tran.savingtranunkid " & _
                        "          , svsaving_contribution_tran.contribution " & _
                        " FROM svsaving_tran " & _
                        "JOIN #TableEmp ON #TableEmp.employeeunkid = svsaving_tran.employeeunkid " & _
                        " LEFT JOIN svsaving_contribution_tran ON svsaving_tran.savingtranunkid = svsaving_contribution_tran.savingtranunkid " & _
                        " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_contribution_tran.periodunkid " & _
                                "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 AND ISNULL(svsaving_contribution_tran.isvoid, 0) = 0"
            'Sohail (25 Aug 2021) - [JOIN #TableEmp]

            If mdtAsonDate <> Nothing Then
                strQ &= " AND Convert(char(8),cfcommon_period_tran.start_date,112) <= @asondate "
            End If

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            'If mstrEmployeeIDs.Trim.Length > 0 Then
            '    strQ &= "AND svsaving_tran.employeeunkid IN (" & mstrEmployeeIDs & ")"
            'End If
            'Sohail (25 Aug 2021) -- End

            strQ &= "            ) AS b ON a.savingtranunkid = b.savingtranunkid " & _
                        "  JOIN ( SELECT   DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY end_date DESC,svsaving_interest_rate_tran.effectivedate DESC ) AS ROWNO " & _
                        " , svsaving_tran.savingtranunkid " & _
                        ", svsaving_interest_rate_tran.interest_rate " & _
                        " FROM svsaving_tran " & _
                        "JOIN #TableEmp ON #TableEmp.employeeunkid = svsaving_tran.employeeunkid " & _
                        " LEFT JOIN svsaving_interest_rate_tran ON svsaving_tran.savingtranunkid = svsaving_interest_rate_tran.savingtranunkid " & _
                        " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_interest_rate_tran.periodunkid " & _
                                "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 AND ISNULL(svsaving_interest_rate_tran.isvoid, 0) = 0 "
            'Sohail (25 Aug 2021) - [JOIN #TableEmp]

            If mdtAsonDate <> Nothing Then
                strQ &= " AND Convert(char(8),svsaving_interest_rate_tran.effectivedate,112) <= @asondate "
                objDataOperation.AddParameter("@asondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsonDate))
            End If

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            'If mstrEmployeeIDs.Trim.Length > 0 Then
            '    strQ &= "AND svsaving_tran.employeeunkid IN (" & mstrEmployeeIDs & ")"
            'End If
            'Sohail (25 Aug 2021) -- End

            strQ &= " ) AS c ON a.savingtranunkid = c.savingtranunkid " & _
                        " WHERE   A.ROWNO = 1 and b.ROWNO =1 and c.ROWNO =1"

            If intSavingTranUnkID > 0 Then
                strQ &= "AND  a.savingtranunkid  = @savingtranunkid "
                objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSavingTranUnkID)
            End If

            If intEmpUnkID > 0 Then
                strQ &= "AND a.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
            End If

            If intStatusID > 0 Then
                strQ &= "AND savingstatus = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            'Sohail (25 Aug 2021) -- Start
            'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
            If strOuterFilter.Trim.Length > 0 Then
                strQ &= " " & strOuterFilter & " "
            End If

            strQ &= " DROP TABLE #TableEmp "
            'Sohail (25 Aug 2021) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Shani(24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (svsaving_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Insert(Optional ByVal intCompanyID As Integer = 0, Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal mdtContribution As DataTable = Nothing, Optional ByVal mdtInterestRate As DataTable = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
    Public Function Insert(ByVal xCurrentDateAndTime As DateTime, Optional ByVal intCompanyID As Integer = 0, Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal mdtContribution As DataTable = Nothing, Optional ByVal mdtInterestRate As DataTable = Nothing) As Boolean
        'Shani(24-Aug-2015) -- End

        'Public Function Insert(Optional ByVal intCompanyID As Integer = 0, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'Shani [ 12 JAN 2015 ] -- START -- END
        'Public Function Insert(Optional ByVal intCompanyID As Integer = 0) As Boolean

        'Sohail (12 Dec 2015) -- Start
        'Enhancement - Provide Deposit feaure in Employee Saving.
        Dim strVocNo As String = ""
        If isExistSameSaving(mintPayperiodunkid, mdtEffectivedate, mintEmployeeunkid, mintSavingschemeunkid, mdecContribution, mdecInterest_Rate, strVocNo) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, This Employee Saving already exists.")
            Return False
        End If
        'Sohail (12 Dec 2015) -- End

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFalg As Boolean = False
        Dim objStatus As New clsSaving_Status_Tran


        'S.SANDEEP [ 04 DEC 2013 ] -- START
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END


        'Anjan (21 Jan 2012)-Start
        'ENHANCEMENT : TRA COMMENTS 
        'Issue : To fix the issue on network of autocode concurrency when more than 1 user press save at same time.
        'Sandeep [ 16 Oct 2010 ] -- Start
        'Issue : Auto No. Generation



        'S.SANDEEP [ 28 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
        'Dim intSavVocType As Integer = 0
        'Dim strSavVocPrifix As String = ""
        'Dim intNextSavVocNo As Integer = 0

        'intSavVocType = ConfigParameter._Object._SavingsVocNoType

        'If intSavVocType = 1 Then
        '    strSavVocPrifix = ConfigParameter._Object._SavingsVocPrefix
        '    intNextSavVocNo = ConfigParameter._Object._NextSavingsVocNo
        '    mstrVoucherno = strSavVocPrifix & intNextSavVocNo
        'End If
        ''Sandeep [ 16 Oct 2010 ] -- End 


        'If isExist(mstrVoucherno) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
        '    Return False
        'End If


        'Pinkal (03-Jun-2013) -- Start
        'Enhancement : TRA Changes

        Dim intSavVocType As Integer = 0
        Dim objConfig As New clsConfigOptions
        objConfig._Companyunkid = IIf(intCompanyID = 0, Company._Object._Companyunkid, intCompanyID)
        intSavVocType = objConfig._SavingsVocNoType

        'intSavVocType = ConfigParameter._Object._SavingsVocNoType

        'Pinkal (03-Jun-2013) -- End


        If intSavVocType = 0 Then
            If isExist(mstrVoucherno) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
        Else
            mstrVoucherno = ""
        End If
        'S.SANDEEP [ 28 MARCH 2012 ] -- END



        'Anjan (21 Jan 2012)-End 

        Try
            objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno.ToString)
            If mdtEffectivedate = Nothing Then
                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            End If

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingschemeunkid.ToString)
            'objDataOperation.AddParameter("@savingperiods", SqlDbType.int, eZeeDataType.INT_SIZE, mintsavingperiods.ToString)
            objDataOperation.AddParameter("@contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecContribution.ToString) 'Sohail (11 May 2011)
            'If mdtMaturitydate = Nothing Then
            '    objDataOperation.AddParameter("@maturitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@maturitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtMaturitydate)
            'End If

            'objDataOperation.AddParameter("@maturity_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblMaturity_Amount.ToString)
            objDataOperation.AddParameter("@totalcontribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalContribution.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@savingstatus", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingstatus.ToString)
            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@broughtforward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnBroughtforward.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)

            'Anjan (26 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            If mdtStopDate = Nothing Then
                objDataOperation.AddParameter("@stopDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@stopDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStopDate)
            End If
            'Anjan (26 Jan 2012)-End 


            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBf_Amount)
            objDataOperation.AddParameter("@bf_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBf_Balance)
            'SHANI [12 JAN 2015]--END 

            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
            'SHANI [09 MAR 2015]--END

            'Anjan (26 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            'strQ = "INSERT INTO svsaving_tran ( " & _
            '  "  voucherno " & _
            '  ", effectivedate " & _
            '  ", payperiodunkid " & _
            '  ", employeeunkid " & _
            '  ", savingschemeunkid " & _
            '  ", contribution " & _
            '  ", savingstatus " & _
            '  ", total_contribution " & _
            '  ", interest_rate " & _
            '  ", interest_amount " & _
            '  ", balance_amount " & _
            '  ", broughtforward " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime" & _
            '  ", voidreason " & _
            '") VALUES (" & _
            '  "  @voucherno " & _
            '  ", @effectivedate " & _
            '  ", @payperiodunkid " & _
            '  ", @employeeunkid " & _
            '  ", @savingschemeunkid " & _
            '  ", @contribution " & _
            '  ", @savingstatus " & _
            '  ", @totalcontribution " & _
            '  ", @interest_rate " & _
            '  ", @interest_amount " & _
            '  ", @balance_amount " & _
            '  ", @broughtforward " & _
            '  ", @userunkid " & _
            '  ", @isvoid " & _
            '  ", @voiduserunkid " & _
            '  ", @voiddatetime" & _
            '  ", @voidreason " & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO svsaving_tran ( " & _
              "  voucherno " & _
              ", effectivedate " & _
              ", payperiodunkid " & _
              ", employeeunkid " & _
              ", savingschemeunkid " & _
              ", contribution " & _
              ", savingstatus " & _
              ", total_contribution " & _
              ", interest_rate " & _
              ", interest_amount " & _
              ", balance_amount " & _
              ", broughtforward " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime" & _
              ", voidreason " & _
              ", stopdate " & _
              ", bf_amount " & _
              ", bf_balance " & _
              ", countryunkid " & _
            ") VALUES (" & _
              "  @voucherno " & _
              ", @effectivedate " & _
              ", @payperiodunkid " & _
              ", @employeeunkid " & _
              ", @savingschemeunkid " & _
              ", @contribution " & _
              ", @savingstatus " & _
              ", @totalcontribution " & _
              ", @interest_rate " & _
              ", @interest_amount " & _
              ", @balance_amount " & _
              ", @broughtforward " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
              ", @voidreason " & _
              ", @stopdate " & _
              ", @bf_amount " & _
              ", @bf_balance " & _
              ", @countryunkid " & _
            "); SELECT @@identity"
            'SHANI [12 JAN 2015]-- [bf_amount, bf_balance]
            'SHANI [09 MAR 2015]-Includeing countryunkid Field. _

            'Anjan (26 Jan 2012)-End 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSavingtranunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 28 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
            If intSavVocType = 1 Then


                'Pinkal (03-Jun-2013) -- Start
                'Enhancement : TRA Changes

                'If Set_AutoNumber(objDataOperation, mintSavingtranunkid, "svsaving_tran", "voucherno", "savingtranunkid", "NextSavingsVocNo", ConfigParameter._Object._SavingsVocPrefix) = False Then
                If Set_AutoNumber(objDataOperation, mintSavingtranunkid, "svsaving_tran", "voucherno", "savingtranunkid", "NextSavingsVocNo", objConfig._SavingsVocPrefix, objConfig._Companyunkid) = False Then
                    'Pinkal (03-Jun-2013) -- End


                    'S.SANDEEP [ 17 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objDataOperation.ErrorMessage <> "" Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'S.SANDEEP [ 17 NOV 2012 ] -- END
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If Get_Saved_Number(objDataOperation, mintSavingtranunkid, "svsaving_tran", "voucherno", "savingtranunkid", mstrVoucherno) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END
            End If
            'S.SANDEEP [ 28 MARCH 2012 ] -- END


            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            If mdtContribution IsNot Nothing Then
                Dim objSavingContribution As New clsSaving_contribution_tran

                With objSavingContribution
                    ._Savingtranunkid = mintSavingtranunkid
                    ._DataTable = mdtContribution
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objSavingContribution.Insert_Update_Delete(objDataOperation) = False Then
                If objSavingContribution.Insert_Update_Delete(objDataOperation, xCurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            End If

            If mdtInterestRate IsNot Nothing Then
                Dim objSavingInterestRate As New clsSaving_interest_rate_tran
                With objSavingInterestRate
                    ._Savingtranunkid = mintSavingtranunkid
                    ._DataTable = mdtInterestRate
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objSavingInterestRate.Insert_Update_Delete(objDataOperation) = False Then
                If objSavingInterestRate.Insert_Update_Delete(objDataOperation, xCurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            End If
            'Shani [ 12 JAN 2015 ] -- END



            'Sohail (26 Jul 2010) -- Start
            With objStatus
                ._Savingtranunkid = mintSavingtranunkid
                ._Status_Date = mdtEffectivedate
                ._Statusunkid = mintSavingstatus
                ._Remark = ""

                'Pinkal (03-Jun-2013) -- Start
                'Enhancement : TRA Changes

                '._Userunkid = User._Object._Userunkid
                ._Userunkid = mintUserunkid

                'Pinkal (03-Jun-2013) -- End


                ._Isvoid = False
                ._Voiddatetime = Nothing
                'Anjan (04 Apr 2011)-Start
                '._Voiduserunkid = 0
                ._Voiduserunkid = -1
                'Anjan (04 Apr 2011)-End

                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                ._PeriodID = mintPayperiodunkid
                'Sohail (12 Jan 2015) -- End

                ._FormName = mstrFormName
                ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
                blnFalg = .Insert(xCurrentDateAndTime, True, objDataOperation)
                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 04 DEC 2013 ] -- END
            End With


            'Anjan (04 Apr 2011)-Start

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'InsertAuditTrailForSavingTran(objDataOperation, 1)
            InsertAuditTrailForSavingTran(objDataOperation, 1, xCurrentDateAndTime)
            'Shani(24-Aug-2015) -- End

            'Anjan (04 Apr 2011)-End

            If blnFalg = True Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'Issue : Auto No. Generation


                'S.SANDEEP [ 28 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
                'If intSavVocType = 1 Then
                '    ConfigParameter._Object._NextSavingsVocNo = intNextSavVocNo + 1
                '    ConfigParameter._Object.updateParam()
                '    ConfigParameter._Object.Refresh()
                'End If
                'S.SANDEEP [ 28 MARCH 2012 ] -- END



                'Sandeep [ 16 Oct 2010 ] -- End 

                objDataOperation.ReleaseTransaction(True)
            Else
                objDataOperation.ReleaseTransaction(False)
            End If
            'Sohail (26 Jul 2010) -- End



            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            objStatus = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (svsaving_tran) </purpose>
    ''' 
    '''Public Function Update(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'Shani [ 12 JAN 2015 ] -- START -- END
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Update(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal mdtContribution As DataTable = Nothing, Optional ByVal mdtInterestRate As DataTable = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
    Public Function Update(ByVal xCurrentDateAndTime As DateTime, Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal mdtContribution As DataTable = Nothing, Optional ByVal mdtInterestRate As DataTable = Nothing) As Boolean
        'Shani(24-Aug-2015) -- End

        'Public Function Update() As Boolean
        If isExist(mstrVoucherno, , mintSavingtranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        'Dim objDataOperation As New clsDataOperation
        ''Anjan (04 Apr 2011)-Start
        'objDataOperation.BindTransaction()
        ''Anjan (04 Apr 2011)-End
        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END

        Try
            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingtranunkid.ToString)
            objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno.ToString)

            If mdtEffectivedate = Nothing Then
                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            End If


            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingschemeunkid.ToString)
            'objDataOperation.AddParameter("@savingperiods", SqlDbType.int, eZeeDataType.INT_SIZE, mintsavingperiods.ToString)
            objDataOperation.AddParameter("@contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecContribution.ToString) 'Sohail (11 May 2011)
            'If mdtMaturitydate = Nothing Then
            '    objDataOperation.AddParameter("@maturitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@maturitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtMaturitydate)
            'End If

            'objDataOperation.AddParameter("@maturity_amount", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblmaturity_amount.ToString)
            objDataOperation.AddParameter("@savingstatus", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingstatus.ToString)
            objDataOperation.AddParameter("@totalcontribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalContribution.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@broughtforward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnBroughtforward.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)


            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)

            'Anjan (26 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            If mdtStopDate = Nothing Then
                objDataOperation.AddParameter("@stopDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@stopDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStopDate)
            End If
            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
            'SHANI [09 MAR 2015]--END

            'strQ = "UPDATE svsaving_tran SET " & _
            '              "  voucherno = @voucherno" & _
            '              ", effectivedate = @effectivedate" & _
            '              ", payperiodunkid = @payperiodunkid" & _
            '              ", employeeunkid = @employeeunkid" & _
            '              ", savingschemeunkid = @savingschemeunkid" & _
            '              ", contribution = @contribution" & _
            '              ", savingstatus = @savingstatus" & _
            '              ", interest_rate = @interest_rate " & _
            '              ", interest_amount = @interest_amount " & _
            '              ", balance_amount = @balance_amount " & _
            '              ", broughtforward = @broughtforward " & _
            '              ", userunkid = @userunkid" & _
            '              ", isvoid = @isvoid" & _
            '              ", voiduserunkid = @voiduserunkid" & _
            '              ", voiddatetime = @voiddatetime " & _
            '              ", voidreason = @voidreason " & _
            '            "WHERE savingtranunkid = @savingtranunkid "

            strQ = "UPDATE svsaving_tran SET " & _
                          "  voucherno = @voucherno" & _
                          ", effectivedate = @effectivedate" & _
                          ", payperiodunkid = @payperiodunkid" & _
                          ", employeeunkid = @employeeunkid" & _
                          ", savingschemeunkid = @savingschemeunkid" & _
                          ", contribution = @contribution" & _
                          ", savingstatus = @savingstatus" & _
                          ", interest_rate = @interest_rate " & _
                          ", interest_amount = @interest_amount " & _
                          ", balance_amount = @balance_amount " & _
                          ", broughtforward = @broughtforward " & _
                          ", userunkid = @userunkid" & _
                          ", isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voidreason = @voidreason " & _
                          ", stopdate = @stopdate " & _
                          ", countryunkid = @countryunkid " & _
                        "WHERE savingtranunkid = @savingtranunkid "


            'Anjan (26 Jan 2012)-End 
            'SHANI [09 MAR 2015]-Includeing countryunkid

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            If mdtContribution IsNot Nothing Then
                Dim objSavingContribution As New clsSaving_contribution_tran
                With objSavingContribution
                    ._Savingtranunkid = mintSavingtranunkid
                    ._DataTable = mdtContribution
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objSavingContribution.Insert_Update_Delete(objDataOperation) = False Then
                If objSavingContribution.Insert_Update_Delete(objDataOperation, xCurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            End If

            If mdtInterestRate IsNot Nothing Then
                Dim objSavingInterestRate As New clsSaving_interest_rate_tran
                With objSavingInterestRate
                    ._Savingtranunkid = mintSavingtranunkid
                    ._DataTable = mdtInterestRate
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objSavingInterestRate.Insert_Update_Delete(objDataOperation) = False Then
                If objSavingInterestRate.Insert_Update_Delete(objDataOperation, xCurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            End If
            'Shani [ 12 JAN 2015 ] -- END

            'Anjan (04 Apr 2011)-Start

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'InsertAuditTrailForSavingTran(objDataOperation, 2)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If InsertAuditTrailForSavingTran(objDataOperation, 2) = False Then
            If InsertAuditTrailForSavingTran(objDataOperation, 2, xCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End

                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation.ReleaseTransaction(True)
            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [ 04 DEC 2013 ] -- END




            'Anjan (04 Apr 2011)-End

            Return True
        Catch ex As Exception
            'Anjan (04 Apr 2011)-Start
            objDataOperation.ReleaseTransaction(False)
            'Anjan (04 Apr 2011)-End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation = Nothing
            'S.SANDEEP [ 04 DEC 2013 ] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (svsaving_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Delete(ByVal intUnkid As Integer) As Boolean
    Public Function Delete(ByVal intUnkid As Integer, ByVal xCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        'Anjan (04 Apr 2011)-Start
        objDataOperation.BindTransaction()
        'Anjan (04 Apr 2011)-End

        Try

            Dim objSavingContribution As New clsSaving_contribution_tran
            With objSavingContribution
                ._Savingtranunkid = mintSavingtranunkid
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLogEmployeeUnkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            Dim dtContribution As DataTable = objSavingContribution._DataTable
            For Each Item As DataRow In dtContribution.Rows
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'Item("voiddatetime") = mdtVoiddatetime
                Item("voiddatetime") = xCurrentDateAndTime
                'Sohail (12 Dec 2015) -- End
                Item("voidreason") = mstrVoidReason
                Item("voiduserunkid") = mintVoiduserunkid
                Item.AcceptChanges()

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objSavingContribution.Delete(Item, objDataOperation) = False Then
                If objSavingContribution.Delete(Item, objDataOperation, xCurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            objSavingContribution = Nothing

            Dim objSavingInterest As New clsSaving_interest_rate_tran
            With objSavingInterest
                ._Savingtranunkid = mintSavingtranunkid
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLogEmployeeUnkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            Dim dtInterestRate As DataTable = objSavingInterest._DataTable
            For Each Item As DataRow In dtInterestRate.Rows
                Item("voiddatetime") = mdtVoiddatetime
                Item("voidreason") = mstrVoidReason
                Item("voiduserunkid") = mintVoiduserunkid

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objSavingInterest.Delete(Item, objDataOperation) = False Then
                If objSavingInterest.Delete(Item, objDataOperation, xCurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End

                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            objSavingInterest = Nothing

            objDataOperation.ClearParameters()
            strQ = "UPDATE svsaving_tran SET " & _
                       " isvoid = 1 " & _
                       ", voiduserunkid = @voiduserunkid " & _
                       ", voiddatetime = @voiddatetime " & _
                       ", voidreason = @voidreason " & _
                   "WHERE savingtranunkid = @savingtranunkid "

            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (04 Apr 2011)-Start

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'InsertAuditTrailForSavingTran(objDataOperation, 3)
            InsertAuditTrailForSavingTran(objDataOperation, 3, xCurrentDateAndTime)
            'Shani(24-Aug-2015) -- End


            objDataOperation.ReleaseTransaction(True)
            'Anjan (04 Apr 2011)-End


            Return True
        Catch ex As Exception
            'Anjan (04 Apr 2011)-Start
            objDataOperation.ReleaseTransaction(False)
            'Anjan (04 Apr 2011)-End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strVoucherno As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  savingtranunkid " & _
              ", voucherno " & _
              ", effectivedate " & _
              ", payperiodunkid " & _
              ", employeeunkid " & _
              ", savingschemeunkid " & _
              ", contribution " & _
              ", savingstatus " & _
              " ,total_contribution " & _
              ", interest_rate " & _
              ", interest_amount " & _
              ", balance_amount " & _
              ", broughtforward " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM svsaving_tran " & _
             "WHERE 1 = 1 "



            If strVoucherno.Length > 0 Then
                strQ &= " AND voucherno = @voucherno "
                objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno)
            End If

            If intUnkid > 0 Then
                strQ &= " AND savingtranunkid <> @savingtranunkid"
                objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Public Function isExistSameSaving(ByVal intPeriodID As Integer _
                                      , ByVal dtEffectiveDate As Date _
                                      , ByVal intEmployeeID As Integer _
                                      , ByVal intSavingSchemeID As Integer _
                                      , ByVal decContribution As Decimal _
                                      , ByVal decInterestRate As Decimal _
                                      , Optional ByVal refstrVoucherNo As String = "" _
                                      ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            refstrVoucherNo = ""

            strQ = "SELECT   svsaving_tran.voucherno " & _
                    "FROM    svsaving_tran " & _
                    "WHERE   svsaving_tran.isvoid = 0 " & _
                            "AND svsaving_tran.payperiodunkid = @payperiodunkid " & _
                            "AND CONVERT(CHAR(8), svsaving_tran.effectivedate, 112) = effectivedate " & _
                            "AND svsaving_tran.employeeunkid = @employeeunkid " & _
                            "AND svsaving_tran.savingschemeunkid = @savingschemeunkid " & _
                            "AND CAST(svsaving_tran.contribution AS DECIMAL (36, 2)) = @contribution " & _
                            "AND CAST(svsaving_tran.interest_rate AS DECIMAL (36, 2)) = @interest_rate "

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffectiveDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSavingSchemeID)
            objDataOperation.AddParameter("@contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Format(decContribution, "#.00"))
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Format(decInterestRate, "#.00"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                refstrVoucherNo = dsList.Tables(0).Rows(0).Item("voucherno").ToString
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (12 Dec 2015) -- End

    'Sohail (18 Dec 2010) -- Start
    Public Function Get_Saving_Contribution(ByVal strTableName As String, Optional ByVal intEmpID As Integer = 0, Optional ByVal intSavingTranID As Integer = 0, Optional ByVal intPeriodID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'Anjan (26 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS - stop date added
            '    strQ = "SELECT  effectivedate " & _
            '                              ", PeriodId " & _
            '                              ", PeriodName " & _
            '                              ", PeriodStartDate " & _
            '                              ", PeriodEndDate " & _
            '                              ", TotalPeriodDays " & _
            '                              ", EmpId " & _
            '                              ", EmpCode " & _
            '                              ", EmpName " & _
            '                              ", Savingtranunkid " & _
            '                              ", SavingSchemeId " & _
            '                              ", SavingSchemeName " & _
            '                              ", DeductionAmount " & _
            '                              ", TotalContribution " & _
            '                              ", InterestRate " & _
            '                              ", InterestAmount " & _
            '                              ", BFId " & _
            '                        "FROM    ( SELECT    effectivedate " & _
            '                                          ", PaySlipDeduct.periodunkid AS PeriodId " & _
            '                                          ", PaySlipDeduct.period_name AS PeriodName " & _
            '                                          ", PaySlipDeduct.start_date AS PeriodStartDate " & _
            '                                          ", PaySlipDeduct.end_date AS PeriodEndDate " & _
            '                                          ", PaySlipDeduct.total_days AS TotalPeriodDays " & _
            '                                          ", PaySlipDeduct.employeeunkid AS EmpId " & _
            '                                          ", employeecode AS EmpCode " & _
            '                                          ", employeename AS EmpName " & _
            '                                          ", vwPayroll.tranheadunkid AS Savingtranunkid " & _
            '                                          ", svsavingscheme_master.savingschemeunkid AS SavingSchemeId " & _
            '                                          ", svsavingscheme_master.savingschemename AS SavingSchemeName " & _
            '                                          ", vwPayroll.amount AS DeductionAmount " & _
            '                                          ", svsaving_tran.total_contribution AS TotalContribution " & _
            '                                          ", svsaving_tran.interest_rate AS InterestRate " & _
            '                                          ", svsaving_tran.interest_amount AS InterestAmount " & _
            '                                          ", 0 AS BFId " & _
            '                      "FROM      ( SELECT    prpayment_tran.periodunkid " & _
            '                              ", cfcommon_period_tran.period_name " & _
            '                              ", cfcommon_period_tran.start_date " & _
            '                              ", cfcommon_period_tran.end_date " & _
            '                              ", cfcommon_period_tran.total_days " & _
            '                              ", prpayment_tran.employeeunkid " & _
            '                              ", prpayment_tran.referencetranunkid AS tnaleaveunkid " & _
            '                              ", prpayment_tran.amount " & _
            '                      "FROM      prpayment_tran " & _
            '                                "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                      "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                                "AND prpayment_tran.referenceid = 3 " & _
            '                                "AND cfcommon_period_tran.isactive = 1 " & _
            '                    ") AS PaySlipDeduct " & _
            '                    "JOIN vwPayroll ON PaySlipDeduct.employeeunkid = vwPayroll.employeeunkid " & _
            '                                      "AND PaySlipDeduct.tnaleaveunkid = vwPayroll.tnaleavetranunkid " & _
            '                                      "AND PaySlipDeduct.periodunkid = vwPayroll.payperiodunkid " & _
            '                                      "AND vwPayroll.GroupID = 7 " & _
            '                    "JOIN svsaving_tran ON vwPayroll.tranheadunkid = svsaving_tran.savingtranunkid " & _
            '                    "JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
            '          "UNION ALL " & _
            '          "SELECT    effectivedate " & _
            '                  ", payperiodunkid AS PeriodId " & _
            '                  ", period_name AS PeriodName " & _
            '                  ", start_date AS PeriodStartDate " & _
            '                  ", end_date AS PeriodEndDate " & _
            '                  ", total_days AS TotalPeriodDays " & _
            '                  ", svsaving_tran.employeeunkid AS EmpId " & _
            '                  ", employeecode AS EmpCode " & _
            '                  ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
            '                  ", savingtranunkid AS Savingtranunkid " & _
            '                  ", svsaving_tran.savingschemeunkid AS SavingSchemeId " & _
            '                  ", savingschemename AS SavingSchemeName " & _
            '                  ", 0 AS DeductionAmount " & _
            '                  ", total_contribution AS TotalContribution " & _
            '                  ", interest_rate AS InterestRate " & _
            '                  ", interest_amount AS InterestAmount " & _
            '                  ", broughtforward AS BFId " & _
            '          "FROM      svsaving_tran " & _
            '                    "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_tran.payperiodunkid " & _
            '                    "JOIN hremployee_master ON hremployee_master.employeeunkid = svsaving_tran.employeeunkid " & _
            '                    "JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
            '          "WHERE     broughtforward = 1 " & _
            '        ") AS TableSaving " & _
            '"WHERE   1 = 1 " 'Sohail (04 Mar 2011)

            strQ = "SELECT  effectivedate " & _
                          ", PeriodId " & _
                          ", PeriodName " & _
                          ", PeriodStartDate " & _
                          ", PeriodEndDate " & _
                          ", TotalPeriodDays " & _
                          ", EmpId " & _
                          ", EmpCode " & _
                          ", EmpName " & _
                          ", Savingtranunkid " & _
                          ", SavingSchemeId " & _
                          ", SavingSchemeName " & _
                          ", DeductionAmount " & _
                          ", TotalContribution " & _
                          ", InterestRate " & _
                          ", InterestAmount " & _
                          ", BFId " & _
                          ", savingstopdate " & _
                    "FROM    ( SELECT    effectivedate " & _
                                      ", PaySlipDeduct.periodunkid AS PeriodId " & _
                                      ", PaySlipDeduct.period_name AS PeriodName " & _
                                      ", PaySlipDeduct.start_date AS PeriodStartDate " & _
                                      ", PaySlipDeduct.end_date AS PeriodEndDate " & _
                                      ", PaySlipDeduct.total_days AS TotalPeriodDays " & _
                                      ", PaySlipDeduct.employeeunkid AS EmpId " & _
                                      ", employeecode AS EmpCode " & _
                                      ", employeename AS EmpName " & _
                                      ", vwPayroll.tranheadunkid AS Savingtranunkid " & _
                                      ", svsavingscheme_master.savingschemeunkid AS SavingSchemeId " & _
                                      ", svsavingscheme_master.savingschemename AS SavingSchemeName " & _
                                      ", vwPayroll.amount AS DeductionAmount " & _
                                      ", svsaving_tran.total_contribution AS TotalContribution " & _
                                      ", svsaving_tran.interest_rate AS InterestRate " & _
                                      ", svsaving_tran.interest_amount AS InterestAmount " & _
                                      ", 0 AS BFId " & _
                                      ", svsaving_tran.stopdate AS savingstopdate " & _
            "FROM      ( SELECT    prpayment_tran.periodunkid " & _
                                      ", cfcommon_period_tran.period_name " & _
                                      ", cfcommon_period_tran.start_date " & _
                                      ", cfcommon_period_tran.end_date " & _
                                      ", cfcommon_period_tran.total_days " & _
                                      ", prpayment_tran.employeeunkid " & _
                                      ", prpayment_tran.referencetranunkid AS tnaleaveunkid " & _
                                      ", prpayment_tran.amount " & _
                              "FROM      prpayment_tran " & _
                                        "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                              "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                        "AND prpayment_tran.referenceid = 3 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                            ") AS PaySlipDeduct " & _
                            "JOIN vwPayroll ON PaySlipDeduct.employeeunkid = vwPayroll.employeeunkid " & _
                                              "AND PaySlipDeduct.tnaleaveunkid = vwPayroll.tnaleavetranunkid " & _
                                              "AND PaySlipDeduct.periodunkid = vwPayroll.payperiodunkid " & _
                                              "AND vwPayroll.GroupID = 7 " & _
                            "JOIN svsaving_tran ON vwPayroll.tranheadunkid = svsaving_tran.savingtranunkid " & _
                            "JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                  "UNION ALL " & _
                  "SELECT    effectivedate " & _
                          ", payperiodunkid AS PeriodId " & _
                          ", period_name AS PeriodName " & _
                          ", start_date AS PeriodStartDate " & _
                          ", end_date AS PeriodEndDate " & _
                          ", total_days AS TotalPeriodDays " & _
                          ", svsaving_tran.employeeunkid AS EmpId " & _
                          ", employeecode AS EmpCode " & _
                          ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                          ", savingtranunkid AS Savingtranunkid " & _
                          ", svsaving_tran.savingschemeunkid AS SavingSchemeId " & _
                          ", savingschemename AS SavingSchemeName " & _
                          ", 0 AS DeductionAmount " & _
                          ", total_contribution AS TotalContribution " & _
                          ", interest_rate AS InterestRate " & _
                          ", interest_amount AS InterestAmount " & _
                          ", broughtforward AS BFId " & _
                          ", svsaving_tran.stopdate AS savingstopdate  " & _
                  "FROM      svsaving_tran " & _
                            "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_tran.payperiodunkid " & _
                            "JOIN hremployee_master ON hremployee_master.employeeunkid = svsaving_tran.employeeunkid " & _
                            "JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                  "WHERE     broughtforward = 1 " & _
                ") AS TableSaving " & _
        "WHERE   1 = 1 " 'Sohail (04 Mar 2011)
            'Anjan (26 Jan 2012)-End 

            If intEmpID > 0 Then
                strQ &= "AND EmpId = @EmpId "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
            End If
            If intSavingTranID > 0 Then
                strQ &= "AND Savingtranunkid = @Savingtranunkid "
                objDataOperation.AddParameter("@Savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSavingTranID)
            End If
            If intPeriodID > 0 Then
                strQ &= "AND PeriodId = @PeriodId "
                objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            End If

            strQ &= " ORDER BY BFId DESC " & _
                          ", EmpId " & _
                          ", Savingtranunkid " & _
                          ", PeriodEndDate "


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (18 Dec 2010) -- End

    'Anjan (04 Apr 2011)-Start

    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForSavingTran(ByVal objDataoperation As clsDataOperation, ByVal AuditType As Integer)

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function InsertAuditTrailForSavingTran(ByVal objDataoperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
    Public Function InsertAuditTrailForSavingTran(ByVal objDataoperation As clsDataOperation, ByVal AuditType As Integer, ByVal xCurrerntDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        'Anjan (11 Jun 2011)-End
        Dim strQ As String
        Dim exForce As Exception
        Try
            'Anjan (26 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            'strQ = " INSERT INTO atsvsaving_tran ( " & _
            '                "  savingtranunkid  " & _
            '                ", voucherno " & _
            '                ", effectivedate " & _
            '                ", payperiodunkid " & _
            '                ", employeeunkid " & _
            '                ", savingschemeunkid " & _
            '                ", contribution " & _
            '                ", savingstatus " & _
            '                ", total_contribution " & _
            '                ", interest_rate " & _
            '                ", interest_amount " & _
            '                ", balance_amount " & _
            '                ", broughtforward " & _
            '                ", userunkid " & _
            '                ", auditType " & _
            '                ", audituserunkid " & _
            '                ", auditdatetime " & _
            '                ", ip " & _
            '                ", machine_name " & _
            '  ") VALUES (" & _
            '            "  @savingtranunkid " & _
            '            ", @voucherno " & _
            '            ", @effectivedate " & _
            '            ", @payperiodunkid " & _
            '            ", @employeeunkid " & _
            '            ", @savingschemeunkid " & _
            '            ", @contribution " & _
            '            ", @savingstatus " & _
            '            ", @total_contribution " & _
            '            ", @interest_rate " & _
            '            ", @interest_amount " & _
            '            ", @balance_amount " & _
            '            ", @broughtforward " & _
            '            ", @userunkid " & _
            '            ", @audittype " & _
            '            ", @audituserunkid " & _
            '            ", @auditdatetime " & _
            '            ", @ip " & _
            '            ", @machine_name " & _
            '   "); "


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = " INSERT INTO atsvsaving_tran ( " & _
            '                "  savingtranunkid  " & _
            '                ", voucherno " & _
            '                ", effectivedate " & _
            '                ", payperiodunkid " & _
            '                ", employeeunkid " & _
            '                ", savingschemeunkid " & _
            '                ", contribution " & _
            '                ", savingstatus " & _
            '                ", total_contribution " & _
            '                ", interest_rate " & _
            '                ", interest_amount " & _
            '                ", balance_amount " & _
            '                ", broughtforward " & _
            '                ", userunkid " & _
            '                ", auditType " & _
            '                ", audituserunkid " & _
            '                ", auditdatetime " & _
            '                ", ip " & _
            '                ", machine_name " & _
            '                ", stopdate " & _
            '  ") VALUES (" & _
            '            "  @savingtranunkid " & _
            '            ", @voucherno " & _
            '            ", @effectivedate " & _
            '            ", @payperiodunkid " & _
            '            ", @employeeunkid " & _
            '            ", @savingschemeunkid " & _
            '            ", @contribution " & _
            '            ", @savingstatus " & _
            '            ", @total_contribution " & _
            '            ", @interest_rate " & _
            '            ", @interest_amount " & _
            '            ", @balance_amount " & _
            '            ", @broughtforward " & _
            '            ", @userunkid " & _
            '            ", @audittype " & _
            '            ", @audituserunkid " & _
            '            ", @auditdatetime " & _
            '            ", @ip " & _
            '            ", @machine_name " & _
            '            ", @stopdate " & _
            '   "); "


            strQ = " INSERT INTO atsvsaving_tran ( " & _
                            "  savingtranunkid  " & _
                            ", voucherno " & _
                            ", effectivedate " & _
                            ", payperiodunkid " & _
                            ", employeeunkid " & _
                            ", savingschemeunkid " & _
                            ", contribution " & _
                            ", savingstatus " & _
                            ", total_contribution " & _
                            ", interest_rate " & _
                            ", interest_amount " & _
                            ", balance_amount " & _
                            ", broughtforward " & _
                            ", userunkid " & _
                            ", auditType " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name " & _
                            ", stopdate " & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb " & _
                       ", bf_amount " & _
                       ", bf_balance " & _
                       ", countryunkid " & _
              ") VALUES (" & _
                        "  @savingtranunkid " & _
                        ", @voucherno " & _
                        ", @effectivedate " & _
                        ", @payperiodunkid " & _
                        ", @employeeunkid " & _
                        ", @savingschemeunkid " & _
                        ", @contribution " & _
                        ", @savingstatus " & _
                        ", @total_contribution " & _
                        ", @interest_rate " & _
                        ", @interest_amount " & _
                        ", @balance_amount " & _
                        ", @broughtforward " & _
                        ", @userunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @stopdate " & _
                   ", @form_name " & _
                   " " & _
                   " " & _
                   " " & _
                   " " & _
                   " " & _
                   ", @isweb " & _
                   ", @bf_amount " & _
                   ", @bf_balance " & _
                   ", @countryunkid " & _
               "); "

            'S.SANDEEP [ 19 JULY 2012 ] -- END
            'Shani [ 9 Mar 2015 ] -- END

            'Anjan (26 Jan 2012)-End 

            objDataoperation.ClearParameters()
            objDataoperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingtranunkid.ToString)
            objDataoperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno.ToString)
            If mdtEffectivedate = Nothing Then
                objDataoperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataoperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            End If

            objDataoperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid.ToString)
            objDataoperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataoperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingschemeunkid.ToString)
            objDataoperation.AddParameter("@contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecContribution.ToString) 'Sohail (11 May 2011)
            objDataoperation.AddParameter("@total_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalContribution.ToString) 'Sohail (11 May 2011)
            objDataoperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataoperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataoperation.AddParameter("@savingstatus", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingstatus.ToString)
            objDataoperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataoperation.AddParameter("@broughtforward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnBroughtforward.ToString)
            objDataoperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataoperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)


            If mintUserunkid <= 0 Then
                mintUserunkid = User._Object._Userunkid
            End If

            objDataoperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataoperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataoperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataoperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            If mdtStopDate = Nothing Then
                objDataoperation.AddParameter("@stopDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataoperation.AddParameter("@stopDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStopDate)
            End If
            objDataoperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataoperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataoperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBf_Amount)
            objDataoperation.AddParameter("@bf_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBf_Balance)
            objDataoperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)

            objDataoperation.ExecNonQuery(strQ)

            If objDataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForSavingTran", mstrModuleName)
        End Try

    End Function

    'Sohail (08 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetDataForProcessPayroll(ByVal strTableName As String, ByVal intEmpUnkID As Integer, ByVal dtPeriodEnd As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataTable
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'strQ = "SELECT  svsaving_tran.savingtranunkid " & _
            '                          ", svsaving_tran.contribution " & _
            '                    "FROM  svsaving_tran " & _
            '                    "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
            '                    "AND     savingstatus = 1 " & _
            '                    "AND     effectivedate <= @enddate " & _
            '                    "AND     employeeunkid = @employeeunkid "
            'Sohail (09 Mar 2015) -- Start
            'Enhancement - Provide Multi Currency on Saving scheme contribution.
            'strQ = "SELECT  a.savingtranunkid  " & _
            '              ", a.contribution " & _
            '        "FROM    ( SELECT    DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY end_date DESC ) AS ROWNO  " & _
            '                          ", svsaving_tran.savingtranunkid " & _
            '                          ", svsaving_contribution_tran.contribution " & _
            '                  "FROM      svsaving_tran " & _
            '                            "LEFT JOIN svsaving_contribution_tran ON svsaving_tran.savingtranunkid = svsaving_contribution_tran.savingtranunkid " & _
            '                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_contribution_tran.periodunkid " & _
            '                  "WHERE     ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
            '                            "AND CONVERT(CHAR(8), end_date, 112) <= @enddate " & _
            '                            "AND employeeunkid = @employeeunkid " & _
            '                ") AS a " & _
            '                "JOIN    ( SELECT    DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY end_date DESC, status_date DESC ) AS ROWNO  " & _
            '                                  ", svsaving_tran.savingtranunkid " & _
            '                                  ", svsaving_status_tran.statusunkid " & _
            '                          "FROM      svsaving_tran " & _
            '                                    "LEFT JOIN svsaving_status_tran ON svsaving_tran.savingtranunkid = svsaving_status_tran.savingtranunkid " & _
            '                                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_status_tran.periodunkid " & _
            '                          "WHERE     ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
            '                                    "AND CONVERT(CHAR(8), end_date, 112) <= @enddate " & _
            '                                    "AND employeeunkid = @employeeunkid " & _
            '                ") AS b ON a.savingtranunkid = b.savingtranunkid " & _
            '        "WHERE   a.ROWNO = 1 " & _
            '           "AND  b.ROWNO = 1 " & _
            '           "AND  b.statusunkid = " & enSavingStatus.IN_PROGRESS & " "
            strQ = "SELECT  a.savingtranunkid  " & _
                          ", a.savingschemeunkid " & _
                          ", a.contribution / c.exchange_rate AS contribution " & _
                          ", c.exchangerateunkid AS currencyunkid " & _
                    "FROM    ( SELECT    DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY end_date DESC ) AS ROWNO  " & _
                                      ", svsaving_tran.savingtranunkid " & _
                                      ", svsaving_tran.savingschemeunkid " & _
                                      ", svsaving_contribution_tran.contribution " & _
                                "FROM  svsaving_tran " & _
                                        "LEFT JOIN svsaving_contribution_tran ON svsaving_tran.savingtranunkid = svsaving_contribution_tran.savingtranunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_contribution_tran.periodunkid " & _
                                "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 AND  ISNULL(svsaving_contribution_tran.isvoid,0) = 0 " & _
                                        "AND CONVERT(CHAR(8), end_date, 112) <= @enddate " & _
                                        "AND employeeunkid = @employeeunkid " & _
                            ") AS a " & _
                            "JOIN    ( SELECT    DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY end_date DESC, status_date DESC, svsaving_status_tran.savingstatustranunkid DESC ) AS ROWNO  " & _
                                              ", svsaving_tran.savingtranunkid " & _
                                              ", svsaving_status_tran.statusunkid " & _
                                      "FROM      svsaving_tran " & _
                                                "LEFT JOIN svsaving_status_tran ON svsaving_tran.savingtranunkid = svsaving_status_tran.savingtranunkid " & _
                                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_status_tran.periodunkid " & _
                                      "WHERE     ISNULL(svsaving_tran.isvoid, 0) = 0 AND  ISNULL(svsaving_status_tran.isvoid,0)= 0 " & _
                                                "AND CONVERT(CHAR(8), end_date, 112) <= @enddate " & _
                                                "AND employeeunkid = @employeeunkid " & _
                            ") AS b ON a.savingtranunkid = b.savingtranunkid " & _
                            "JOIN    ( SELECT    DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY exchange_date DESC, exchangerateunkid DESC ) AS ROWNO  " & _
                                              ", svsaving_tran.savingtranunkid " & _
                                              ", cfexchange_rate.exchange_rate " & _
                                              ", ISNULL(cfexchange_rate.exchangerateunkid, 1) AS exchangerateunkid " & _
                                      "FROM      svsaving_tran " & _
                                                "LEFT JOIN cfexchange_rate ON cfexchange_rate.countryunkid = svsaving_tran.countryunkid " & _
                                      "WHERE     ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
                                                "AND isactive = 1 " & _
                                                "AND CONVERT(CHAR(8), exchange_date, 112) <= @enddate " & _
                                                "AND employeeunkid = @employeeunkid " & _
                            ") AS c ON a.savingtranunkid = c.savingtranunkid " & _
                    "WHERE   a.ROWNO = 1 " & _
                       "AND  b.ROWNO = 1 " & _
                       "AND  c.ROWNO = 1 " & _
                       "AND  b.statusunkid = " & enSavingStatus.IN_PROGRESS & " "
            'Sohail (14 Mar 2019) - [savingschemeunkid]
            'Sohail (09 Mar 2015) -- End
            'Sohail (12 Jan 2015) -- End

            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)

            dsTable = objDataOperation.ExecQuery(strQ, strTableName).Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataForProcessPayroll; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsTable IsNot Nothing Then dsTable.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsTable
    End Function
    'Sohail (08 Jun 2012) -- End


    ''Sohail (04 Mar 2011) -- Start
    '''' <summary>
    '''' Modify By: Sohail
    '''' Get fields of svsaving_tran as well as latest saving status detail from svsaving_status_tran
    '''' </summary>
    '''' <param name="strTableName">Table Name</param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Function Get_List(ByVal strTableName As String, Optional ByVal intSavingTranUnkID As Integer = 0, Optional ByVal intEmpUnkID As Integer = 0, Optional ByVal intStatusID As Integer = 0) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT  svsaving_tran.savingtranunkid " & _
    '                      ", svsaving_tran.voucherno " & _
    '                      ", CONVERT(CHAR(8),svsaving_tran.effectivedate,112) AS effectivedate " & _
    '                      ", svsaving_tran.payperiodunkid " & _
    '                      ", cfcommon_period_tran.period_name " & _
    '                      ", svsaving_tran.employeeunkid " & _
    '                      ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename " & _
    '                      ", svsaving_tran.savingschemeunkid " & _
    '                      ", svsavingscheme_master.savingschemename " & _
    '                      ", svsaving_tran.contribution " & _
    '                      ", svsaving_tran.savingstatus " & _
    '                      ", svsaving_tran.total_contribution " & _
    '                      ", svsaving_tran.interest_rate " & _
    '                      ", svsaving_tran.interest_amount " & _
    '                      ", svsaving_tran.balance_amount " & _
    '                      ", svsaving_tran.broughtforward " & _
    '                      ", svsaving_tran.userunkid " & _
    '                      ", svsaving_tran.isvoid " & _
    '                      ", svsaving_tran.voiduserunkid " & _
    '                      ", svsaving_tran.voiddatetime " & _
    '                      ", svsaving_tran.voidreason " & _
    '                      ", Saving_Status.savingstatustranunkid " & _
    '                      ", CONVERT(CHAR(8),Saving_Status.status_date,112) AS status_date " & _
    '                      ", Saving_Status.statusunkid " & _
    '                      ", Saving_Status.remark " & _
    '                "FROM    svsaving_tran " & _
    '                        "LEFT JOIN ( SELECT  svsaving_status_tran.savingstatustranunkid " & _
    '                                          ", svsaving_status_tran.savingtranunkid " & _
    '                                          ", svsaving_status_tran.status_date " & _
    '                                          ", svsaving_status_tran.statusunkid " & _
    '                                          ", svsaving_status_tran.remark " & _
    '                                          ", svsaving_status_tran.userunkid " & _
    '                                          ", svsaving_status_tran.isvoid " & _
    '                                          ", svsaving_status_tran.voiddatetime " & _
    '                                          ", svsaving_status_tran.voiduserunkid " & _
    '                                    "FROM    svsaving_status_tran " & _
    '                                            "INNER JOIN ( SELECT MAX(svsaving_status_tran.status_date) AS status_date " & _
    '                                                              ", svsaving_status_tran.savingtranunkid " & _
    '                                                         "FROM   svsaving_status_tran " & _
    '                                                         "WHERE  ISNULL(isvoid, 0) = 0 " & _
    '                                                         "GROUP BY svsaving_status_tran.savingtranunkid " & _
    '                                                       ") AS SavingStatus ON svsaving_status_tran.savingtranunkid = SavingStatus.savingtranunkid " & _
    '                                                                            "AND svsaving_status_tran.status_date = SavingStatus.status_date " & _
    '                                    "WHERE   ISNULL(svsaving_status_tran.isvoid, 0) = 0 " & _
    '                                  ") AS Saving_Status ON svsaving_tran.savingtranunkid = Saving_Status.savingtranunkid " & _
    '                        "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
    '                        "LEFT JOIN hremployee_master ON svsaving_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "LEFT JOIN cfcommon_period_tran ON svsaving_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                "WHERE   ISNULL(svsaving_tran.isvoid, 0) = 0 "

    '        If intSavingTranUnkID > 0 Then
    '            strQ &= "AND svsaving_tran.savingtranunkid = @savingtranunkid "
    '            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSavingTranUnkID)
    '        End If

    '        If intEmpUnkID > 0 Then
    '            strQ &= "AND svsaving_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
    '        End If

    '        If intStatusID > 0 Then
    '            strQ &= "AND Saving_Status.statusunkid = @statusunkid "
    '            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_List; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    ''Sohail (04 Mar 2011) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
            Language.setMessage(mstrModuleName, 2, "WEB")
            Language.setMessage(mstrModuleName, 3, "Sorry, This Employee Saving already exists.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class