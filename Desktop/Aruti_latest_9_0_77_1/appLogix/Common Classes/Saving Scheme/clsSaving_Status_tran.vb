﻿'************************************************************************************************************************************
'Class Name : clsSaving_Status_Tran.vb
'Purpose    :
'Date       : 20/07/2010
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsSaving_Status_Tran
    Private Const mstrModuleName = "clsSaving_Status_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSavingstatustranunkid As Integer
    Private mintSavingtranunkid As Integer
    Private mdtStatus_Date As Date
    Private mintStatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer



    Private mintPeriodId As Integer = -1
    'Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    'Private mstrWebIP As String = ""
    'Private mstrWebHost As String = ""


#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set savingstatustranunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Savingstatustranunkid() As Integer
        Get
            Return mintSavingstatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintSavingstatustranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set savingtranunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Savingtranunkid() As Integer
        Get
            Return mintSavingtranunkid
        End Get
        Set(ByVal value As Integer)
            mintSavingtranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_date
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Status_Date() As Date
        Get
            Return mdtStatus_Date
        End Get
        Set(ByVal value As Date)
            mdtStatus_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE.

    ''' <summary>
    ''' Purpose: Get or Set Periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _PeriodID() As Integer
        Get
            Return mintPeriodId
        End Get
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    'Public Property _WebIP() As String
    '    Get
    '        Return mstrWebIP
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public Property _WebHost() As String
    '    Get
    '        Return mstrWebHost
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHost = value
    '    End Set
    'End Property


#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  savingstatustranunkid " & _
              ", savingtranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", ISNULL(periodunkid,0) AS periodunkid " & _
             "FROM svsaving_status_tran " & _
             "WHERE savingstatustranunkid = @savingstatustranunkid "

            'Pinkal (12 Jan 2015) -- Enhancement - CHANGE IN SAVINGS MODULE [", ISNULL(periodunkid,0) AS periodunkid " & _]


            objDataOperation.AddParameter("@savingstatustranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintSavingstatusTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintsavingstatustranunkid = CInt(dtRow.Item("savingstatustranunkid"))
                mintsavingtranunkid = CInt(dtRow.Item("savingtranunkid"))
                mdtStatus_Date = dtRow.Item("status_date")
                mintstatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrremark = dtRow.Item("remark").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                'Pinkal (12 Jan 2015) -- End

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE.

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  savingstatustranunkid " & _
              ", savingtranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", CASE WHEN statusunkid = 1 THEN @InProgress " & _
              "            WHEN statusunkid = 2 THEN @OnHold " & _
              "            WHEN statusunkid = 3 THEN @WrittenOff " & _
              "            WHEN statusunkid = 4 THEN @Completed " & _
              "  END As status " & _
              ", remark " & _
              ", svsaving_status_tran.userunkid " & _
              ", hrmsconfiguration..cfuser_master.username " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", ISNULL(svsaving_status_tran.periodunkid,0) AS periodunkid " & _
              ", ISNULL(cfcommon_period_tran.period_name,'') AS period " & _
              " FROM svsaving_status_tran " & _
              " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_status_tran.periodunkid " & _
              " LEFT JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = svsaving_status_tran.userunkid "

            'Pinkal (12 Jan 2015) -- Enhancement - CHANGE IN SAVINGS MODULE [", ISNULL(periodunkid,0) AS periodunkid " & _]


            If blnOnlyActive Then
                strQ &= " WHERE svsaving_status_tran.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 99, "Redemption"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (12 Jan 2015) -- End


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (svsaving_status_tran) </purpose>
    '''Anjan (04 Apr 2011)-Start
    '''Public Function Insert() As Boolean
    '''Shani(24-Aug-2015) -- Start
    '''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '''Public Function Insert(Optional ByVal blnFromSavingTranInsert As Boolean = False, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
    Public Function Insert(ByVal xCurrentDateAndTime As DateTime, Optional ByVal blnFromSavingTranInsert As Boolean = False, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        'Shani(24-Aug-2015) -- End

        'Public Function Insert(Optional ByVal blnFromSavingTranInsert As Boolean = False) As Boolean
        'Anjan (04 Apr 2011)-End
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim objEmpSaving As New clsSaving_Tran

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END

        Try

            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingtranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatus_Date)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId.ToString)
            'Pinkal (12 Jan 2015) -- End

            strQ = "INSERT INTO svsaving_status_tran ( " & _
              "  savingtranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid" & _
                   ", periodunkid " & _
            ") VALUES (" & _
              "  @savingtranunkid " & _
              ", @status_date " & _
              ", @statusunkid " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid" & _
                   ", @periodunkid " & _
            "); SELECT @@identity"
            'Pinkal (12 Jan 2015) -- [periodunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSavingstatustranunkid = dsList.Tables(0).Rows(0).Item(0)

            'Anjan (04 Apr 2011)-Start
            If blnFromSavingTranInsert = False Then

                'Sohail (26 Jul 2010) -- Start
                objEmpSaving._Savingtranunkid = mintSavingtranunkid
                objEmpSaving._Savingstatus = mintStatusunkid

                If mintStatusunkid = 4 Then 'Complete

                    'Sohail (12 Jan 2015) -- Start
                    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                    'Dim objInterest As New clsInterestRate_tran
                    ''Sohail (18 Dec 2010) -- Start
                    ''Dim objStatus As New clsSaving_Status_Tran 
                    ''Dim objCompany As New clsCompany_Master
                    ''Sohail (18 Dec 2010) -- End
                    'Dim dtpComplete As Date
                    ''Dim dtpStart As Date 'Sohail (18 Dec 2010)
                    'Dim dblRate As Double = 0
                    'Dim intDays As Integer = 0
                    ''Sohail (18 Dec 2010) -- Start
                    'Dim dblTotInterest As Double = 0
                    ''Sohail (18 Dec 2010) -- End
                    'dsList = GetLastStatus("Status", mintSavingtranunkid) 'Sohail (18 Dec 2010)
                    'If dsList.Tables("Status").Rows.Count > 0 Then
                    '    With dsList.Tables("Status").Rows(0)
                    '        dtpComplete = CDate(.Item("status_date").ToString)
                    '    End With

                    '    Call objInterest.GetInterestRate(objEmpSaving._Savingschemeunkid, DateDiff(DateInterval.Day, objEmpSaving._Effectivedate, dtpComplete))
                    '    dblRate = objInterest._Rate

                    '    'Sohail (18 Dec 2010) -- Start
                    '    dsList = objEmpSaving.Get_Saving_Contribution("Contrib", objEmpSaving._Employeeunkid, objEmpSaving._Savingtranunkid, 0)
                    '    For Each dsRow As DataRow In dsList.Tables("Contrib").Rows
                    '        With dsRow
                    '            If CBool(.Item("BFID")) = True Then 'Broghht Forward (Opening Balance)
                    '                'Anjan - Sohail (09 Mar 2011)-Start
                    '                'Issue : Interest was getting calculated again due to below line as interest amount gets stored in interest amount field so no need to calculate last year interest amount.
                    '                'dblTotInterest += cdec(.Item("InterestAmount").ToString)
                    '                'Anjan -Sohail (09 Mar 2011) -End

                        'Else
                    '                If CDate(.Item("PeriodStartDate").ToString).Date > CDate(.Item("effectivedate").ToString).Date Then
                    '                    intDays = DateDiff(DateInterval.Day, CDate(.Item("PeriodStartDate").ToString).Date, dtpComplete.AddDays(1))
                    '                Else
                    '                    intDays = DateDiff(DateInterval.Day, CDate(.Item("effectivedate").ToString).Date, dtpComplete.AddDays(1))
                    '                End If
                    '                dblTotInterest += (CDec(.Item("DeductionAmount").ToString) * intDays * dblRate) / (DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date.AddDays(1)) * 100)
                    '            End If
                    '        End With
                    '    Next
                    '    'Sohail (18 Dec 2010) -- End

                    '    'Sandeep [ 29 NOV 2010 ] -- Start
                    '    'objCompany.GetFinancialDate(1)
                    '    'objCompany.GetFinancialDate(Company._Object._Companyunkid) 'Sohail (18 Dec 2010)
                    '    'Sandeep [ 29 NOV 2010 ] -- End 
                    '    'Sohail (15 Dec 2010) -- Start
                    '    'If objEmpSaving._Effectivedate >= objCompany._Financial_Start_Date Then
                    '    '    dtpStart = objEmpSaving._Effectivedate
                    '    'Else
                    '    '    dtpStart = objCompany._Financial_Start_Date
                    '    'End If
                    '    'dtpStart = objEmpSaving._Effectivedate 'Sohail (18 Dec 2010)
                    '    'Sohail (15 Dec 2010) -- End

                    '    'Sohail (18 Dec 2010) -- Start
                    '    'intDays = DateDiff(DateInterval.Day, dtpStart, dtpComplete)
                    '    'objEmpSaving._Interest_Amount += objInterest.GetInterestAmount(objEmpSaving._Total_Contribution, dblRate, intDays)
                    '    objEmpSaving._Interest_Amount += dblTotInterest
                    '    'Sohail (18 Dec 2010) -- End

                        'End If

                    'objEmpSaving._Interest_Rate = dblRate
                    'objEmpSaving._Balance_Amount = objEmpSaving._Total_Contribution + objEmpSaving._Interest_Amount
                    'Sohail (12 Jan 2015) -- End

                    End If


                'objEmpSaving._WebFormName = mstrWebFormName
                'objEmpSaving._WebHost = mstrWebHost
                'objEmpSaving._WebIP = mstrWebIP

                With objEmpSaving
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objEmpSaving.Update(objDataOperation)
                blnFlag = objEmpSaving.Update(xCurrentDateAndTime, objDataOperation)
                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 04 DEC 2013 ] -- END
            Else
                blnFlag = True
            End If
            'Anjan (04 Apr 2011)-End
            If blnFlag = True Then
                objDataOperation.ReleaseTransaction(True)
            Else
                objDataOperation.ReleaseTransaction(False)
            End If
            'Sohail (26 Jul 2010) -- End
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation = Nothing
            'S.SANDEEP [ 04 DEC 2013 ] -- END
            objEmpSaving = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (svsaving_status_tran) </purpose>
    Public Function Update() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@savingstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingstatustranunkid.ToString)
            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingtranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatus_Date)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            strQ = "UPDATE svsaving_status_tran SET " & _
              "  savingtranunkid = @savingtranunkid" & _
              ", status_date = @status_date" & _
              ", statusunkid = @statusunkid" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid " & _
            "WHERE savingstatustranunkid = @savingstatustranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation = Nothing
            'S.SANDEEP [ 04 DEC 2013 ] -- END
        End Try
    End Function


    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE. DON'T REMOVE THIS METHOD.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (svsaving_status_tran) </purpose>
    'Public Function Delete() As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim blnFlag As Boolean = False
    '    Dim objEmpSaving As New clsSaving_Tran

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try
    '        objDataOperation.AddParameter("@savingstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingstatustranunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

    '        strQ = "UPDATE svsaving_status_tran SET " & _
    '                  " isvoid = @isvoid" & _
    '                  ", voiddatetime = @voiddatetime" & _
    '                  ", voiduserunkid = @voiduserunkid " & _
    '        "WHERE savingstatustranunkid = @savingstatustranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            objDataOperation.ReleaseTransaction(False)
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        objEmpSaving._Savingtranunkid = mintSavingtranunkid
    '        objEmpSaving._Savingstatus = mintStatusunkid
    '        objEmpSaving._WebFormName = mstrWebFormName
    '        objEmpSaving._WebHost = mstrWebHost
    '        objEmpSaving._WebIP = mstrWebIP

    '        blnFlag = objEmpSaving.Update(objDataOperation)

    '        If blnFlag Then
    '            objDataOperation.ReleaseTransaction(True)
    '        Else
    '            objDataOperation.ReleaseTransaction(False)
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Pinkal (12 Jan 2015) -- End

    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@savingstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  savingstatustranunkid " & _
    '          ", savingtranunkid " & _
    '          ", status_date " & _
    '          ", statusunkid " & _
    '          ", remark " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiddatetime " & _
    '          ", voiduserunkid " & _
    '         "FROM svsaving_status_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND savingstatustranunkid <> @savingstatustranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@savingstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Sohail (26 Jul 2010) -- Start
    ''' <summary>
    ''' Get Last Status of given Employee Saving
    ''' Modify By : Sohail
    ''' </summary>
    ''' <param name="strTableName"></param>
    ''' <param name="intSavingTranID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLastStatus(ByVal strTableName As String, ByVal intSavingTranID As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  savingstatustranunkid " & _
              ", savingtranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
                      ", ISNULL(periodunkid,0) AS periodunkid " & _
             "FROM svsaving_status_tran " & _
             "WHERE savingtranunkid = @savingtranunkid " & _
             "AND isvoid = 0 " & _
             "ORDER BY status_date DESC "

            'Pinkal (12 Jan 2015) -- Enhancement - CHANGE IN SAVINGS MODULE [", ISNULL(periodunkid,0) AS periodunkid " & _]

            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSavingTranID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (26 Jul 2010) -- End


    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetPeriodWiseSavingStatusHistory(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            strQ = "SELECT " & _
              "  savingstatustranunkid " & _
              ", savingtranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", CASE WHEN statusunkid = 1 THEN @InProgress " & _
              "            WHEN statusunkid = 2 THEN @OnHold " & _
              "            WHEN statusunkid = 3 THEN @WrittenOff " & _
              "            WHEN statusunkid = 4 THEN @Completed " & _
              "  END As status " & _
              ", remark " & _
              ", svsaving_status_tran.userunkid " & _
              ", SPACE(10) + hrmsconfiguration..cfuser_master.username AS username " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", ISNULL(svsaving_status_tran.periodunkid,0) AS periodunkid " & _
              ", ISNULL(cfcommon_period_tran.period_name,'') AS period " & _
              ",cfcommon_period_tran.start_date " & _
              ",cfcommon_period_tran.end_date " & _
              ", 0 as grp	 " & _
              " FROM svsaving_status_tran " & _
              " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_status_tran.periodunkid " & _
              " LEFT JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = svsaving_status_tran.userunkid "

            If blnOnlyActive Then
                strQ &= " WHERE svsaving_status_tran.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " UNION " & _
                          " SELECT " & _
                          "  -1 AS savingstatustranunkid " & _
                          ", -1 AS savingtranunkid " & _
                          ", NULL AS status_date " & _
                          ", -1 AS statusunkid " & _
                          ", '' As status " & _
                          ", '' AS remark " & _
                          ", -1 AS userunkid " & _
                          ", ISNULL(cfcommon_period_tran.period_name,'') AS username " & _
                          ", 0 AS isvoid " & _
                          ", NULL AS voiddatetime" & _
                          ", -1 AS voiduserunkid " & _
                          ", ISNULL(svsaving_status_tran.periodunkid,0) AS periodunkid " & _
                          ", ISNULL(cfcommon_period_tran.period_name,'') AS period " & _
                          ",cfcommon_period_tran.start_date " & _
                          ",cfcommon_period_tran.end_date " & _
                          ", 1 As grp " & _
                          " FROM svsaving_status_tran " & _
                          " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = svsaving_status_tran.periodunkid "

            If blnOnlyActive Then
                strQ &= " WHERE svsaving_status_tran.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " Order by cfcommon_period_tran.end_date DESC,grp DESC,status_date DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 99, "Redemption"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPeriodWiseSavingStatusHistory; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (12 Jan 2015) -- End


End Class
