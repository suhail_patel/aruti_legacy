﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region


Public Class frmReportPrivilegeAssignment

#Region "Private Variables"

    Private mstrModuleName As String = "frmReportPrivilegeAssignment"
    Private objUserAddEdit As clsUserAddEdit
    Private objRoleMaster As clsUserRole_Master
    Private objReport As clsArutiReportClass
    Private dtUserList As DataTable
    Private dtRoleList As DataTable
    Private dtReportList As DataTable
    Private dvUser As DataView
    Private dvRole As DataView
    Private dvReport As DataView
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintUserUnkid As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintUserUnkid = intUnkId
            menAction = eAction

            'Pinkal (12-Apr-2016) -- Start
            'Enhancement - WORKED ON Edit Report Priviledge For User.
            RemoveHandler radByUser.CheckedChanged, AddressOf radByUser_CheckedChanged
            radByUser.Checked = True
            AddHandler radByUser.CheckedChanged, AddressOf radByUser_CheckedChanged
            'Pinkal (12-Apr-2016) -- End
            radByRole.Enabled = False : radByUser.Enabled = False

            Me.ShowDialog()

            intUnkId = mintUserUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region "Form's Events"
    Private Sub frmReportPrivilegeAssignment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetGridColor()
            Call FillUserRoleList()
            Call FillReportList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReportPrivilegeAssignment_Load:- ", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region "Private Functions"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objCompany As New clsCompany_Master
            dsList = objCompany.GetList("List", True)
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillUserRoleList()
        Try
            pnlUserRoleList.Enabled = False

            Dim dsRole As New DataSet

            objRoleMaster = New clsUserRole_Master
            objUserAddEdit = New clsUserAddEdit

            dgUserRoleList.AutoGenerateColumns = False

            If radByRole.Checked = True Then

                dsRole = objRoleMaster.GetList("Role", True)

                If dsRole.Tables("Role") Is Nothing Then Exit Sub

                dsRole.Tables("Role").Columns.Add("IsUcheck", Type.GetType("System.Boolean"))
                For Each dRow In dsRole.Tables("Role").Rows
                    dRow("IsUcheck") = False
                Next

                dtRoleList = dsRole.Tables("Role")
                dtRoleList = New DataView(dtRoleList, "roleunkid > 1", "", DataViewRowState.CurrentRows).ToTable
                dvRole = dtRoleList.DefaultView

                objdgcolhUserRole.DataPropertyName = "name"
                objdgcolhUCheck.DataPropertyName = "IsUcheck"
                objdgcolhUnkid.DataPropertyName = "roleunkid"

                dgUserRoleList.DataSource = dvRole

            ElseIf radByUser.Checked = True Then

                Dim strFilterValue As String = String.Empty

                If mintUserUnkid > 0 Then
                    strFilterValue = "hrmsConfiguration..cfuser_master.userunkid = " & mintUserUnkid
                End If

                dtUserList = objUserAddEdit.GetGroupedUserList("User", clsUserAddEdit.enViewUserType.SHOW_ACTIVE_USER, Now, Now, strFilterValue)

                If dtUserList Is Nothing Then Exit Sub

                dtUserList.Columns.Add("IsUcheck", Type.GetType("System.Boolean"))
                For Each dRow In dtUserList.Rows
                    dRow("IsUcheck") = False
                    dRow("Username") = Trim(dRow("Username"))
                Next

                dvUser = New DataView(dtUserList, "IsGrp=false AND UserUnkid > 1", "", DataViewRowState.CurrentRows)
                dtUserList = dvUser.ToTable
                dvUser = dtUserList.DefaultView

                objdgcolhUserRole.DataPropertyName = "Username"
                'objdgcolhIsGrp.DataPropertyName = "IsGrp"
                'objdgcolhGrpId.DataPropertyName = "GrpId"
                objdgcolhUCheck.DataPropertyName = "IsUcheck"
                objdgcolhUnkid.DataPropertyName = "UserUnkid"
                dgUserRoleList.DataSource = dvUser

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillUserRoleList:- ", mstrModuleName)
        Finally
            pnlUserRoleList.Enabled = True
        End Try
    End Sub

    Private Sub FillReportList()
        Try
            Dim dsList As New DataSet
            objReport = New clsArutiReportClass

            dsList = objReport.getReportListGroupedByCategory("hrmsConfiguration", User._Object._Userunkid, Company._Object._Companyunkid, False, mintUserUnkid)

            dtReportList = dsList.Tables(0)

            If dtReportList.Rows.Count <= 0 Then Exit Sub

            dvReport = dtReportList.DefaultView

            dgReportList.AutoGenerateColumns = False

            dgcolhReports.DataPropertyName = "DisplayName"
            objdgcolhCategory.DataPropertyName = "CategoryName"
            objdgcolhCategoryId.DataPropertyName = "reportcategoryunkid"
            objdgcolhreportunkid.DataPropertyName = "reportunkid"
            objdgcolhRCheck.DataPropertyName = "IsRcheck"
            objdgcolhAUD.DataPropertyName = "AUD"
            objdgcolhreportprivilegeunkid.DataPropertyName = "reportprivilegeunkid"

            dgReportList.DataSource = dvReport

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillReportList:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.SelectionForeColor = Color.Black
            dgvcsChild.SelectionBackColor = Color.White
            dgvcsChild.BackColor = Color.White

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor:- ", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            Dim iList As List(Of Integer) = Nothing

            iList = CType(dgUserRoleList.DataSource, DataView).ToTable.AsEnumerable().Where(Function(r) r.Field(Of Boolean)(objdgcolhUCheck.DataPropertyName) = True).[Select](Function(s) s.Field(Of Integer)(objdgcolhUnkid.DataPropertyName)).ToList

            If iList.Count <= 0 Then
                If radByRole.Checked = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Please select atleast one role in order to assign report privilege."), enMsgBoxStyle.Information)
                    Return False
                ElseIf radByUser.Checked = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Please select atleast one user in order to assign report privilege."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            iList = CType(dgReportList.DataSource, DataView).ToTable.AsEnumerable().Where(Function(r) r.Field(Of Boolean)(objdgcolhRCheck.DataPropertyName) = True And r.Field(Of Integer)(objdgcolhreportunkid.DataPropertyName) > 0).[Select](Function(s) s.Field(Of Integer)(objdgcolhreportunkid.DataPropertyName)).ToList

            If iList.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Please select atleast one report in order to assign report privilege."), enMsgBoxStyle.Information)
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

#End Region

#Region "DataGrid Events"

    Private Sub dgUserRoleList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgUserRoleList.CellContentClick, dgUserRoleList.CellContentDoubleClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgUserRoleList.IsCurrentCellDirty Then
                Me.dgUserRoleList.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            RemoveHandler objchkURSelectAll.CheckedChanged, AddressOf objchkURSelectAll_CheckedChanged

            If radByRole.Checked = True Then
                Dim drRow As DataRow() = dvRole.ToTable.Select("IsUcheck = true", "")

                dvRole.Table.AcceptChanges()

                If drRow.Length > 0 Then
                    If dvRole.Table.Rows.Count = drRow.Length Then
                        objchkURSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkURSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkURSelectAll.CheckState = CheckState.Unchecked
                End If

            ElseIf radByUser.Checked = True Then

                Dim drRow As DataRow() = dvUser.ToTable.Select("IsUcheck = true", "")

                dvUser.Table.AcceptChanges()

                If drRow.Length > 0 Then
                    If dvUser.Table.Rows.Count = drRow.Length Then
                        objchkURSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkURSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkURSelectAll.CheckState = CheckState.Unchecked
                End If

            End If

            AddHandler objchkURSelectAll.CheckedChanged, AddressOf objchkURSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub dgReportList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgReportList.CellContentClick, dgReportList.CellContentDoubleClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgReportList.IsCurrentCellDirty Then
                Me.dgReportList.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CInt(dgReportList.Rows(e.RowIndex).Cells(objdgcolhreportunkid.Index).Value) = "-1" Then

                Select Case CInt(e.ColumnIndex)

                    Case objdgcolhCollapse.Index

                        If dgReportList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is "-" Then
                            dgReportList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "+"
                        Else
                            dgReportList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "-"
                        End If

                        For i As Integer = e.RowIndex + 1 To dgReportList.Rows.Count - 1
                            If CInt(dgReportList.Rows(e.RowIndex).Cells(objdgcolhCategoryId.Index).Value) = CInt(dgReportList.Rows(i).Cells(objdgcolhCategoryId.Index).Value) Then
                                If dgReportList.Rows(i).Visible = True Then
                                    dgReportList.Rows(i).Visible = False
                                Else
                                    dgReportList.Rows(i).Visible = True
                                End If
                            Else
                                Exit For
                            End If
                        Next

                    Case objdgcolhRCheck.Index

                        For i As Integer = e.RowIndex + 1 To dgReportList.Rows.Count - 1
                            If CInt(dgReportList.Rows(e.RowIndex).Cells(objdgcolhCategoryId.Index).Value) = CInt(dgReportList.Rows(i).Cells(objdgcolhCategoryId.Index).Value) Then
                                dgReportList.Rows(i).Cells(e.ColumnIndex).Value = dgReportList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                                If CBool(dgReportList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                                    If CInt(dgReportList.Rows(i).Cells(objdgcolhreportprivilegeunkid.Index).Value) > 0 Then
                                        dgReportList.Rows(i).Cells(objdgcolhAUD.Index).Value = "D"
                                    Else
                                        dgReportList.Rows(i).Cells(objdgcolhAUD.Index).Value = ""
                                    End If
                                ElseIf CBool(dgReportList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
                                    If CInt(dgReportList.Rows(i).Cells(objdgcolhreportprivilegeunkid.Index).Value) > 0 Then
                                        dgReportList.Rows(i).Cells(objdgcolhAUD.Index).Value = "U"
                                    Else
                                        dgReportList.Rows(i).Cells(objdgcolhAUD.Index).Value = "A"
                                    End If
                                End If
                            End If
                        Next

                        'RemoveHandler chkRptSelectAll.CheckedChanged, AddressOf chkRptSelectAll_CheckedChanged

                        'Dim dRow() As DataRow = dvReport.ToTable.Select("IsRcheck=true")

                        'dvReport.Table.AcceptChanges()

                        'If dRow.Length <= 0 Then
                        '    chkRptSelectAll.CheckState = CheckState.Unchecked
                        'ElseIf dRow.Length < dvReport.Table.Rows.Count Then
                        '    chkRptSelectAll.CheckState = CheckState.Indeterminate
                        'ElseIf dRow.Length = dvReport.Table.Rows.Count Then
                        '    chkRptSelectAll.CheckState = CheckState.Checked
                        'End If

                        'AddHandler chkRptSelectAll.CheckedChanged, AddressOf chkRptSelectAll_CheckedChanged
                End Select
            Else
                Select Case CInt(e.ColumnIndex)
                    Case objdgcolhRCheck.Index
                        If CBool(dgReportList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                            If CInt(dgReportList.Rows(e.RowIndex).Cells(objdgcolhreportprivilegeunkid.Index).Value) > 0 Then
                                dgReportList.Rows(e.RowIndex).Cells(objdgcolhAUD.Index).Value = "D"
                            Else
                                dgReportList.Rows(e.RowIndex).Cells(objdgcolhAUD.Index).Value = ""
                            End If
                        ElseIf CBool(dgReportList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
                            If CInt(dgReportList.Rows(e.RowIndex).Cells(objdgcolhreportprivilegeunkid.Index).Value) > 0 Then
                                dgReportList.Rows(e.RowIndex).Cells(objdgcolhAUD.Index).Value = "U"
                            Else
                                dgReportList.Rows(e.RowIndex).Cells(objdgcolhAUD.Index).Value = "A"
                            End If
                        End If
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgReportList_CellContentClick:- ", mstrModuleName)
        End Try
    End Sub

    'Private Sub dgReportList_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgReportList.CellClick
    '    Try
    '        If e.RowIndex = -1 Then Exit Sub

    '        RemoveHandler dgReportList.CellContentClick, AddressOf dgReportList_CellContentClick



    '        AddHandler dgReportList.CellContentClick, AddressOf dgReportList_CellContentClick

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgReportList_CellClick:- ", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub dgReportList_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgReportList.DataBindingComplete
        Try
            For Each dgRow As DataGridViewRow In dgReportList.Rows
                If (dgRow.Cells(objdgcolhreportunkid.Index).Value) = "-1" Then
                    dgRow.DefaultCellStyle.ForeColor = Color.White
                    dgRow.DefaultCellStyle.BackColor = Color.Gray

                    If dgRow.Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgRow.Cells(objdgcolhCollapse.Index).Value = "-"
                    End If


                    dgRow.DefaultCellStyle.SelectionForeColor = Color.White
                    dgRow.DefaultCellStyle.SelectionBackColor = Color.Gray
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgReportList_DataBindingComplete:- ", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            Me.Enabled = False
            If IsValidData() = False Then Exit Sub

            Dim iUsrRole As List(Of Integer) = Nothing
            Dim iReportDictionary As Dictionary(Of Integer, String)

            'Dim iReports As List(Of Integer) = Nothing
            'iReports = CType(dgReportList.DataSource, DataView).ToTable.AsEnumerable().Where(Function(r) r.Field(Of Boolean)(objdgcolhRCheck.DataPropertyName) = True And r.Field(Of Integer)(objdgcolhreportunkid.DataPropertyName) > 0).[Select](Function(s) s.Field(Of Integer)(objdgcolhreportunkid.DataPropertyName)).ToList

            iUsrRole = CType(dgUserRoleList.DataSource, DataView).ToTable.AsEnumerable().Where(Function(r) r.Field(Of Boolean)(objdgcolhUCheck.DataPropertyName) = True).[Select](Function(s) s.Field(Of Integer)(objdgcolhUnkid.DataPropertyName)).ToList
            iReportDictionary = CType(dgReportList.DataSource, DataView).ToTable.AsEnumerable().Where(Function(r) r.Field(Of String)(objdgcolhAUD.DataPropertyName) <> "" And r.Field(Of Integer)(objdgcolhreportunkid.DataPropertyName) > 0).ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)(objdgcolhreportunkid.DataPropertyName), Function(row) row.Field(Of String)(objdgcolhAUD.DataPropertyName))

            Dim blnAssignByRole As Boolean = False

            If radByRole.Checked = True Then
                blnAssignByRole = True
            ElseIf radByUser.Checked = True Then
                blnAssignByRole = False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objReport._FormName = mstrModuleName
            objReport._LoginEmployeeunkid = 0
            objReport._ClientIP = getIP()
            objReport._HostName = getHostName()
            objReport._FromWeb = False
            objReport._AuditUserId = User._Object._Userunkid
            objReport._CompanyUnkid = CInt(cboCompany.SelectedValue)
            objReport._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objReport.InsertReportPrivilegeByRole_User("hrmsConfiguration", iUsrRole, iReportDictionary, CInt(cboCompany.SelectedValue), blnAssignByRole, User._Object._Userunkid, objlblProcessValue)

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Report privilege assignment operation successfully completed."), enMsgBoxStyle.Information)
                Call btnClose_Click(sender, e)
            ElseIf blnFlag = False AndAlso objReport._Message <> "" Then
                eZeeMsgBox.Show(objReport._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Enabled = True
        End Try
    End Sub

#End Region

#Region "ComboBox Events"

    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            Company._Object._Companyunkid = CInt(cboCompany.SelectedValue)
            Call FillReportList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompany_SelectedIndexChanged:- ", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox's Events"

    Private Sub objchkURSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkURSelectAll.CheckedChanged
        Try
            If radByRole.Checked = True Then
                RemoveHandler dgUserRoleList.CellContentClick, AddressOf dgUserRoleList_CellContentClick
                For Each dRow As DataRowView In dvRole
                    dRow.Item("IsUcheck") = CBool(objchkURSelectAll.CheckState)
                Next
                dvRole.Table.AcceptChanges()
                dgUserRoleList.Refresh()

                'For Each dRow As DataRow In dtRoleList.Rows
                '    dRow.Item("IsUcheck") = CBool(objchkURSelectAll.CheckState)
                'Next
                'dtRoleList.AcceptChanges()
                'dvRole = dtRoleList.DefaultView
                'dgUserRoleList.DataSource = dvRole
                AddHandler dgUserRoleList.CellContentClick, AddressOf dgUserRoleList_CellContentClick

            ElseIf radByUser.Checked = True Then
                RemoveHandler dgUserRoleList.CellContentClick, AddressOf dgUserRoleList_CellContentClick

                For Each dRow As DataRowView In dvUser
                    dRow.Item("IsUcheck") = CBool(objchkURSelectAll.CheckState)
                Next
                dvUser.Table.AcceptChanges()
                dgUserRoleList.Refresh()

                'For Each dRow As DataRow In dtUserList.Rows
                '    dRow.Item("IsUcheck") = CBool(objchkURSelectAll.CheckState)
                'Next
                'dtUserList.AcceptChanges()
                'dvUser = dtUserList.DefaultView
                'dgUserRoleList.DataSource = dvUser

                AddHandler dgUserRoleList.CellContentClick, AddressOf dgUserRoleList_CellContentClick
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkURSelectAll_CheckedChanged:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkRptSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkRptSelectAll.CheckedChanged
        Try
            RemoveHandler dgReportList.CellContentClick, AddressOf dgReportList_CellContentClick

            For Each dRow As DataRowView In dvReport
                dRow.Item("IsRcheck") = CBool(objchkRptSelectAll.CheckState)
                If objchkRptSelectAll.CheckState = CheckState.Checked Then
                    dRow.Item("AUD") = "A"
                ElseIf objchkRptSelectAll.CheckState = CheckState.Unchecked Then
                    dRow.Item("AUD") = "D"
                End If
            Next

            'For Each dRow As DataRow In dtReportList.Rows
            '    dRow.Item("IsRcheck") = CBool(objchkRptSelectAll.CheckState)
            'Next
            'dtReportList.AcceptChanges()
            'dvReport = dtReportList.DefaultView
            'dgReportList.DataSource = dvReport

            AddHandler dgReportList.CellContentClick, AddressOf dgReportList_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkRptSelectAll_CheckedChanged:- ", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox's Events"

    Private Sub txtSearchUserRole_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchUserRole.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgUserRoleList.Rows.Count > 0 Then
                        If dgUserRoleList.SelectedRows(0).Index = dgUserRoleList.Rows(dgUserRoleList.RowCount - 1).Index Then Exit Sub
                        dgUserRoleList.Rows(dgUserRoleList.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgUserRoleList.Rows.Count > 1 Then
                        If dgUserRoleList.SelectedRows(0).Index Then Exit Sub
                        dgUserRoleList.Rows(dgUserRoleList.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchUserRole_KeyDown:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchUserRole_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchUserRole.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchUserRole.Text.Trim.Length > 0 Then
                If radByRole.Checked = True Then
                    strSearch = objdgcolhUserRole.DataPropertyName & " LIKE '%" & txtSearchUserRole.Text & "%' "
                ElseIf radByUser.Checked = True Then
                    strSearch = objdgcolhUserRole.DataPropertyName & " LIKE '%" & txtSearchUserRole.Text & "%' "
                End If
            End If

            If radByRole.Checked = True Then
                dvRole.RowFilter = strSearch
            Else
                dvUser.RowFilter = strSearch
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchUserRole_TextChanged:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchReport.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgReportList.Rows.Count > 0 Then
                        If dgReportList.SelectedRows(0).Index = dgReportList.Rows(dgReportList.RowCount - 1).Index Then Exit Sub
                        dgReportList.Rows(dgReportList.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgReportList.Rows.Count > 1 Then
                        If dgReportList.SelectedRows(0).Index Then Exit Sub
                        dgReportList.Rows(dgReportList.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchReport_KeyDown:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchReport_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchReport.TextChanged
        Try
            Dim strSearch As String = "" : Dim strFinalFilter As String = ""
            If txtSearchReport.Text.Trim.Length > 0 Then
                strSearch = "(" & dgcolhReports.DataPropertyName & " LIKE '%" & txtSearchReport.Text & "%' OR reportunkid = -1 )"
                strFinalFilter = strSearch
            End If

            If strSearch.Trim.Length > 0 Then
                dvReport.RowFilter = strSearch
                Dim xOtherFilterValue As String = String.Empty

                Dim iCateGoryLst As List(Of Integer) = dvReport.ToTable.AsEnumerable().AsEnumerable().[Select](Function(row) row.Field(Of Integer)("reportcategoryunkid")).ToList()
                Dim duplicates = iCateGoryLst.GroupBy(Function(i) i).Where(Function(g) g.Count() = 1).[Select](Function(g) g.Key).ToArray()

                If duplicates.Length > 0 Then
                    xOtherFilterValue = String.Join(",", duplicates.[Select](Function(cv) cv.ToString()).ToArray())
                End If

                If xOtherFilterValue.Length > 0 Then
                    strSearch = " AND reportcategoryunkid NOT IN (" & xOtherFilterValue & ")"
                    strFinalFilter &= strSearch
                End If

            End If

            dvReport.RowFilter = strFinalFilter

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchReport_TextChanged:- ", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "RadioButton's Events"

    Private Sub radByUser_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radByUser.CheckedChanged, radByRole.CheckedChanged
        Try
            RemoveHandler objchkURSelectAll.CheckedChanged, AddressOf objchkURSelectAll_CheckedChanged
            objchkURSelectAll.Checked = False
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case radByRole.Name.ToUpper
                    objchkURSelectAll.Checked = False
                    objdgcolhUserRole.HeaderText = Language.getMessage(mstrModuleName, 1, "User Role")
                Case radByUser.Name.ToUpper
                    objchkURSelectAll.Checked = False
                    objdgcolhUserRole.HeaderText = Language.getMessage(mstrModuleName, 2, "User(s)")
            End Select
            Call FillUserRoleList()
            AddHandler objchkURSelectAll.CheckedChanged, AddressOf objchkURSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radByUser_CheckedChanged:- ", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbRoleUser.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRoleUser.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbReports.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbReports.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbRoleUser.Text = Language._Object.getCaption(Me.gbRoleUser.Name, Me.gbRoleUser.Text)
            Me.gbReports.Text = Language._Object.getCaption(Me.gbReports.Name, Me.gbReports.Text)
            Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
            Me.lblReportPrivilege.Text = Language._Object.getCaption(Me.lblReportPrivilege.Name, Me.lblReportPrivilege.Text)
            Me.radByUser.Text = Language._Object.getCaption(Me.radByUser.Name, Me.radByUser.Text)
            Me.radByRole.Text = Language._Object.getCaption(Me.radByRole.Name, Me.radByRole.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.dgcolhReports.HeaderText = Language._Object.getCaption(Me.dgcolhReports.Name, Me.dgcolhReports.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "User Role")
            Language.setMessage(mstrModuleName, 2, "User(s)")
            Language.setMessage(mstrModuleName, 3, "Sorry, Please select atleast one role in order to assign report privilege.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Please select atleast one user in order to assign report privilege.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Please select atleast one report in order to assign report privilege.")
            Language.setMessage(mstrModuleName, 6, "Report privilege assignment operation successfully completed.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
