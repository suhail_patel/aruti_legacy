﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmReportAbilityLevel

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmReportAbilityLevel"
    Private intIsReportCatColOrdinal As Integer = 0
    Private intrepotcateunkidOrdinal As Integer = 0
    Private intreportunkidOrdinal As Integer = 0
    Private mdtReportAbilityLevel As DataTable
    Private objReportRolemapping As clsReport_role_mapping_tran

#End Region

#Region "Form's Events"

    Private Sub frmReportAbilityLevel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objReportRolemapping = New clsReport_role_mapping_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            If User._Object.Privilege._AllowtoViewReportAbilityLevel = True Then
                Call FillReportAbilityList()
                Call SetVisibility()
            Else
                btnExport.Enabled = False
                btnSave.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReportAbilityLevel_Load:- ", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objCompany As New clsCompany_Master
            dsList = objCompany.GetList("Company", True)
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Company")
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub FillReportAbilityList()
        Try
            mdtReportAbilityLevel = objReportRolemapping.GetReportAbilityLevelList(CInt(cboCompany.SelectedValue), User._Object._Userunkid, "hrmsConfiguration")

            If mdtReportAbilityLevel IsNot Nothing Then
                dgvReportAbilityLevel.DataSource = mdtReportAbilityLevel
                intIsReportCatColOrdinal = mdtReportAbilityLevel.Columns("objisReportCategory").Ordinal + 1
                intreportunkidOrdinal = mdtReportAbilityLevel.Columns("objreportunkid").Ordinal + 1
                intrepotcateunkidOrdinal = mdtReportAbilityLevel.Columns("objreportcategoryunkid").Ordinal + 1
            End If

            Dim intAdminRolColOrdinal As Integer = 0
            For Each dCol As DataColumn In mdtReportAbilityLevel.Columns
                If dCol.ExtendedProperties.Count > 0 Then
                    If dCol.ExtendedProperties(dCol.ColumnName) = 1 Then
                        intAdminRolColOrdinal = dCol.Ordinal + 1
                        Exit For
                    End If
                End If
            Next

            dgvReportAbilityLevel.Columns(intAdminRolColOrdinal).ReadOnly = True

            Call SetColorTemplate()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillReportAbilityList:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColorTemplate()
        Dim intRow As Integer = 0
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.SelectionBackColor = Color.LightGray
            dgvcsChild.BackColor = Color.LightGray

            Dim dgvcsRowHeader As New DataGridViewCellStyle
            dgvcsRowHeader.BackColor = Color.Gray

            For intRow = 0 To dgvReportAbilityLevel.Rows.Count - 1
                If CBool(dgvReportAbilityLevel.Rows(intRow).Cells(intIsReportCatColOrdinal).Value) = True Then
                    dgvReportAbilityLevel.Rows(intRow).DefaultCellStyle = dgvcsHeader
                Else
                    dgvReportAbilityLevel.Rows(intRow).DefaultCellStyle = dgvcsChild
                    dgvReportAbilityLevel.Rows(intRow).Cells(0).Style.BackColor = Color.DarkGray
                    dgvReportAbilityLevel.Rows(intRow).Cells(1).Style.BackColor = Color.DarkGray
                    dgvReportAbilityLevel.Rows(intRow).Cells(1).Style.SelectionBackColor = Color.DarkGray
                End If
            Next

            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvReportAbilityLevel.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsCollapseChild As New DataGridViewCellStyle
            objdgvsCollapseChild.Font = New Font(dgvReportAbilityLevel.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseChild.ForeColor = Color.White
            objdgvsCollapseChild.BackColor = Color.DarkGray
            objdgvsCollapseChild.SelectionBackColor = Color.DarkGray
            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i = 0 To dgvReportAbilityLevel.Rows.Count - 1
                If CBool(dgvReportAbilityLevel.Rows(i).Cells(intIsReportCatColOrdinal).Value) = True Then
                    dgvReportAbilityLevel.Rows(i).Cells(0).Value = "+"
                    dgvReportAbilityLevel.Rows(i).Cells(0).Style = objdgvsCollapseHeader
                Else
                    dgvReportAbilityLevel.Rows(i).Visible = False
                End If
            Next

            For i As Integer = 1 To dgvReportAbilityLevel.Columns.Count - 1
                If i > 1 Then
                    If dgvReportAbilityLevel.Columns(i).HeaderText.StartsWith("obj") Then
                        dgvReportAbilityLevel.Columns(i).Visible = False
                    Else
                        dgvReportAbilityLevel.Columns(i).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        dgvReportAbilityLevel.Columns(i).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    End If
                Else
                    dgvReportAbilityLevel.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColorTemplate:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnExport.Enabled = User._Object.Privilege._AllowtoExportReportAbilityLevel
            btnSave.Enabled = User._Object.Privilege._AllowtoSaveReportAbilityLevel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "DataGrid's Events"

    Private Sub dgvReportAbilityLevel_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReportAbilityLevel.CellContentClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvReportAbilityLevel.IsCurrentCellDirty Then
                Me.dgvReportAbilityLevel.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvReportAbilityLevel.Rows(e.RowIndex).Cells(intIsReportCatColOrdinal).Value) = True Then
                Select Case (e.ColumnIndex)
                    Case 0
                        If dgvReportAbilityLevel.Rows(e.RowIndex).Cells(0).Value.ToString = "-" Then
                            dgvReportAbilityLevel.Rows(e.RowIndex).Cells(0).Value = "+"
                        Else
                            dgvReportAbilityLevel.Rows(e.RowIndex).Cells(0).Value = "-"
                        End If

                        For i As Integer = e.RowIndex + 1 To dgvReportAbilityLevel.Rows.Count - 1
                            If CInt(dgvReportAbilityLevel.Rows(e.RowIndex).Cells(intrepotcateunkidOrdinal).Value) = CInt(dgvReportAbilityLevel.Rows(i).Cells(intrepotcateunkidOrdinal).Value) Then
                                If dgvReportAbilityLevel.Rows(i).Visible = False Then
                                    dgvReportAbilityLevel.Rows(i).Visible = True
                                Else
                                    dgvReportAbilityLevel.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case Is >= 2
                        For i As Integer = e.RowIndex + 1 To dgvReportAbilityLevel.Rows.Count - 1
                            If CInt(dgvReportAbilityLevel.Rows(e.RowIndex).Cells(intrepotcateunkidOrdinal).Value) = CInt(dgvReportAbilityLevel.Rows(i).Cells(intrepotcateunkidOrdinal).Value) Then
                                If CBool(dgvReportAbilityLevel.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                                    dgvReportAbilityLevel.Rows(i).Cells(e.ColumnIndex).Value = False
                                    dgvReportAbilityLevel.Rows(i).Cells(e.ColumnIndex + 1).Value = "D"
                                Else
                                    dgvReportAbilityLevel.Rows(i).Cells(e.ColumnIndex).Value = True
                                    dgvReportAbilityLevel.Rows(i).Cells(e.ColumnIndex + 1).Value = "A"
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            Else
                Select Case e.ColumnIndex
                    Case Is >= 2
                        If CBool(dgvReportAbilityLevel.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                            dgvReportAbilityLevel.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value = "D"
                        Else
                            dgvReportAbilityLevel.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value = "A"
                        End If
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvReportAbilityLevel_CellContentClick:- ", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ComboBox's Events"
    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            If User._Object.Privilege._AllowtoViewReportAbilityLevel = True Then
                Call FillReportAbilityList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompany_SelectedValueChanged:- ", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Button's events"

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            'S.SANDEEP [21-SEP-2018] -- START
            'Dim strBuilder As New System.Text.StringBuilder
            'Dim objReportrolemapping As New clsReport_role_mapping_tran

            'strBuilder = objReportrolemapping.ExportReportAbilityLevel(CType(dgvReportAbilityLevel.DataSource, DataTable))

            'Dim svDialog As New SaveFileDialog
            'svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
            'If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            '    Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
            '    Dim strWriter As New IO.StreamWriter(fsFile)
            '    With strWriter
            '        .BaseStream.Seek(0, IO.SeekOrigin.End)
            '        .WriteLine(strBuilder)
            '        .Close()
            '    End With
            '    Diagnostics.Process.Start(svDialog.FileName)
            'End If
            Dim dt As DataTable = CType(dgvReportAbilityLevel.DataSource, DataTable).Select("objreportunkid > 0").CopyToDataTable()
            Dim objUList As New ArutiReports.clsUserDetailsLogReport
            objUList.ExportPrivilegeList(dt, Me.Text, 2, cboCompany.Text, True)
            'S.SANDEEP [21-SEP-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click:- ", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If mdtReportAbilityLevel IsNot Nothing Then
                Dim blnFlag As Boolean = True
                Dim strColName As String = ""
                Dim xColCheckedIdCol As String = ""
                For Each dCol As DataColumn In mdtReportAbilityLevel.Columns
                    strColName = "" : xColCheckedIdCol = ""
                    If dCol.ExtendedProperties.Count > 0 AndAlso dCol.ExtendedProperties(dCol.ColumnName) <> 1 Then
                        Dim mDictionary As New Dictionary(Of Integer, String)
                        strColName = dCol.ColumnName
                        If strColName.StartsWith("objAUD") Then
                            xColCheckedIdCol = strColName.Replace("objAUD", "")

                            mDictionary = (From oPrvilege In mdtReportAbilityLevel.AsEnumerable() Where oPrvilege(strColName) <> "" And oPrvilege("objisReportCategory") = False Select New With {Key .ID = CInt(oPrvilege.Item("objreportunkid")), Key .NAME = oPrvilege.Item(strColName).ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)


                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objReportRolemapping._FormName = mstrModuleName
                            objReportRolemapping._LoginEmployeeunkid = 0
                            objReportRolemapping._ClientIP = getIP()
                            objReportRolemapping._HostName = getHostName()
                            objReportRolemapping._FromWeb = False
                            objReportRolemapping._AuditUserId = User._Object._Userunkid
                            objReportRolemapping._CompanyUnkid = CInt(cboCompany.SelectedValue)
                            objReportRolemapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            If Not objReportRolemapping.SaveReportAbilityLevel("hrmsConfiguration", dCol.ExtendedProperties(dCol.ColumnName), mDictionary, CInt(cboCompany.SelectedValue)) Then
                                blnFlag = False
                            End If

                        End If
                    End If
                Next

                If blnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Information Saved Successfully.")) '?1
                    Me.Close()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Information Saved Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class