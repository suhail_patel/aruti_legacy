﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportAbilityLevel
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportAbilityLevel))
        Me.objHeading = New eZee.Common.eZeeHeading
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.pnl_ReportAbilityLevel = New System.Windows.Forms.Panel
        Me.dgvReportAbilityLevel = New System.Windows.Forms.DataGridView
        Me.objdgcolCollapse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objHeading.SuspendLayout()
        Me.pnl_ReportAbilityLevel.SuspendLayout()
        CType(Me.dgvReportAbilityLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'objHeading
        '
        Me.objHeading.BorderColor = System.Drawing.Color.Black
        Me.objHeading.Controls.Add(Me.cboCompany)
        Me.objHeading.Controls.Add(Me.lblCompany)
        Me.objHeading.Dock = System.Windows.Forms.DockStyle.Top
        Me.objHeading.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objHeading.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objHeading.Location = New System.Drawing.Point(0, 0)
        Me.objHeading.Name = "objHeading"
        Me.objHeading.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.objHeading.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objHeading.ShowDefaultBorderColor = True
        Me.objHeading.Size = New System.Drawing.Size(884, 25)
        Me.objHeading.TabIndex = 3
        Me.objHeading.Text = "Report Ability Level"
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(624, 2)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(259, 21)
        Me.cboCompany.TabIndex = 22
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.BackColor = System.Drawing.Color.Transparent
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(506, 6)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(84, 13)
        Me.lblCompany.TabIndex = 21
        Me.lblCompany.Text = "Select Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnl_ReportAbilityLevel
        '
        Me.pnl_ReportAbilityLevel.Controls.Add(Me.dgvReportAbilityLevel)
        Me.pnl_ReportAbilityLevel.Location = New System.Drawing.Point(0, 25)
        Me.pnl_ReportAbilityLevel.Name = "pnl_ReportAbilityLevel"
        Me.pnl_ReportAbilityLevel.Size = New System.Drawing.Size(884, 378)
        Me.pnl_ReportAbilityLevel.TabIndex = 4
        '
        'dgvReportAbilityLevel
        '
        Me.dgvReportAbilityLevel.AllowUserToAddRows = False
        Me.dgvReportAbilityLevel.AllowUserToDeleteRows = False
        Me.dgvReportAbilityLevel.AllowUserToOrderColumns = True
        Me.dgvReportAbilityLevel.AllowUserToResizeColumns = False
        Me.dgvReportAbilityLevel.AllowUserToResizeRows = False
        Me.dgvReportAbilityLevel.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvReportAbilityLevel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReportAbilityLevel.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvReportAbilityLevel.ColumnHeadersHeight = 22
        Me.dgvReportAbilityLevel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvReportAbilityLevel.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolCollapse})
        Me.dgvReportAbilityLevel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvReportAbilityLevel.EnableHeadersVisualStyles = False
        Me.dgvReportAbilityLevel.GridColor = System.Drawing.Color.Black
        Me.dgvReportAbilityLevel.Location = New System.Drawing.Point(0, 0)
        Me.dgvReportAbilityLevel.Name = "dgvReportAbilityLevel"
        Me.dgvReportAbilityLevel.RowHeadersVisible = False
        Me.dgvReportAbilityLevel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvReportAbilityLevel.Size = New System.Drawing.Size(884, 378)
        Me.dgvReportAbilityLevel.TabIndex = 0
        '
        'objdgcolCollapse
        '
        Me.objdgcolCollapse.Frozen = True
        Me.objdgcolCollapse.HeaderText = ""
        Me.objdgcolCollapse.Name = "objdgcolCollapse"
        Me.objdgcolCollapse.ReadOnly = True
        Me.objdgcolCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolCollapse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolCollapse.Width = 24
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnExport)
        Me.objefFormFooter.Controls.Add(Me.btnSave)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 407)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(884, 55)
        Me.objefFormFooter.TabIndex = 4
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(12, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(105, 30)
        Me.btnExport.TabIndex = 3
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(689, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(785, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'frmReportAbilityLevel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(884, 462)
        Me.Controls.Add(Me.objefFormFooter)
        Me.Controls.Add(Me.pnl_ReportAbilityLevel)
        Me.Controls.Add(Me.objHeading)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportAbilityLevel"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Report Ability Level"
        Me.objHeading.ResumeLayout(False)
        Me.objHeading.PerformLayout()
        Me.pnl_ReportAbilityLevel.ResumeLayout(False)
        CType(Me.dgvReportAbilityLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objHeading As eZee.Common.eZeeHeading
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents pnl_ReportAbilityLevel As System.Windows.Forms.Panel
    Friend WithEvents dgvReportAbilityLevel As System.Windows.Forms.DataGridView
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objdgcolCollapse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
End Class
