﻿
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

#End Region

Public Class frmAbilityLevel_Privilege

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAbilityLevel_Privilege"
    Private mblnFormload As Boolean = False
    Private objUser As clsUserAddEdit
    Private mdtPrivilegeAbilityTran As DataTable
    Private intIsPrivilegeColOrdinal As Integer = 0
    Private intPrivilegGroupOrdinal As Integer = 0
    Private intPrivilegUnkidOrdinal As Integer = 0

#End Region

#Region " Private Methods "

    Private Sub SetColorTemplate()
        Dim intRow As Integer = 0
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.SelectionBackColor = Color.LightGray
            dgvcsChild.BackColor = Color.LightGray

            Dim dgvcsRowHeader As New DataGridViewCellStyle
            dgvcsRowHeader.BackColor = Color.Gray

            For intRow = 0 To dgAbilityGroupPrivilege.RowCount - 1
                If CBool(dgAbilityGroupPrivilege.Rows(intRow).Cells(intIsPrivilegeColOrdinal).Value) = True Then
                    dgAbilityGroupPrivilege.Rows(intRow).DefaultCellStyle = dgvcsHeader
                Else
                    dgAbilityGroupPrivilege.Rows(intRow).DefaultCellStyle = dgvcsChild
                    dgAbilityGroupPrivilege.Rows(intRow).Cells(0).Style.BackColor = Color.DarkGray
                    dgAbilityGroupPrivilege.Rows(intRow).Cells(1).Style.BackColor = Color.DarkGray
                    dgAbilityGroupPrivilege.Rows(intRow).Cells(1).Style.SelectionBackColor = Color.DarkGray
                End If
            Next

            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgAbilityGroupPrivilege.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsCollapseChild As New DataGridViewCellStyle
            objdgvsCollapseChild.Font = New Font(dgAbilityGroupPrivilege.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseChild.ForeColor = Color.White
            objdgvsCollapseChild.BackColor = Color.DarkGray
            objdgvsCollapseChild.SelectionBackColor = Color.DarkGray
            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i = 0 To dgAbilityGroupPrivilege.RowCount - 1
                If CBool(dgAbilityGroupPrivilege.Rows(i).Cells(intIsPrivilegeColOrdinal).Value) = True Then
                    dgAbilityGroupPrivilege.Rows(i).Cells(0).Value = "+"
                    dgAbilityGroupPrivilege.Rows(i).Cells(0).Style = objdgvsCollapseHeader
                Else
                    dgAbilityGroupPrivilege.Rows(i).Visible = False
                End If
            Next

            For i As Integer = 1 To dgAbilityGroupPrivilege.Columns.Count - 1
                'Anjan (28 Aug 2017) -Start
                'Issue: Due to sorting grid data was getting scattered.
                dgAbilityGroupPrivilege.Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                'Anjan (28 Aug 2017) -End
                If i > 1 Then
                    If dgAbilityGroupPrivilege.Columns(i).HeaderText.StartsWith("obj") Then
                        dgAbilityGroupPrivilege.Columns(i).Visible = False
                    Else
                        dgAbilityGroupPrivilege.Columns(i).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        dgAbilityGroupPrivilege.Columns(i).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    End If
                Else
                    dgAbilityGroupPrivilege.Columns(i).Frozen = True
                    dgAbilityGroupPrivilege.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColorTemplate", mstrModuleName)
        End Try
    End Sub

    Private Sub FillDataGridView()
        Try
            mdtPrivilegeAbilityTran = objUser.GetPrivilegeAbilityList("hrmsConfiguration")
            dgAbilityGroupPrivilege.DataSource = mdtPrivilegeAbilityTran

            intIsPrivilegeColOrdinal = mdtPrivilegeAbilityTran.Columns("objisprivilegegroup").Ordinal + 1
            intPrivilegGroupOrdinal = mdtPrivilegeAbilityTran.Columns("objprivilegeGroupunkidunkid").Ordinal + 1
            intPrivilegUnkidOrdinal = mdtPrivilegeAbilityTran.Columns("objprivilegeunkid").Ordinal + 1

            Dim intAdminRolColOrdinal As Integer = 0
            For Each dCol As DataColumn In mdtPrivilegeAbilityTran.Columns
                If dCol.ExtendedProperties.Count > 0 Then
                    If dCol.ExtendedProperties(dCol.ColumnName) = 1 Then
                        intAdminRolColOrdinal = dCol.Ordinal + 1
                        Exit For
                    End If
                End If
            Next
            'Anjan/Sandeep (28 Aug 2017) -- Start
            'Issue : User were able to change name of privileges in grid.
            dgAbilityGroupPrivilege.Columns(0).ReadOnly = True
            dgAbilityGroupPrivilege.Columns(1).ReadOnly = True
            'Anjan/Sandeep (28 Aug 2017) -- End

            dgAbilityGroupPrivilege.Columns(intAdminRolColOrdinal).ReadOnly = True


            Call SetColorTemplate()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDataGridView", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnExport.Enabled = User._Object.Privilege._AllowtoExportAbilityLevel
            btnSave.Enabled = User._Object.Privilege._AllowtoSaveAbilityLevel
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAbilityLevel_Previlege_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmAbilityLevel_Previlege_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUser = New clsUserAddEdit
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            If User._Object.Privilege._AllowtoViewAbilityLevel = True Then
                Call FillDataGridView()
                Call SetVisibility()
            Else
                btnExport.Enabled = False
                btnSave.Enabled = False
            End If

            mblnFormload = True

            If dgAbilityGroupPrivilege.RowCount > 0 Then
                AddHandler dgAbilityGroupPrivilege.CellContentClick, AddressOf dgAbilityGroupPrivilege_CellContentClick
                AddHandler dgAbilityGroupPrivilege.CellContentDoubleClick, AddressOf dgAbilityGroupPrivilege_CellContentClick
            End If

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event(s) "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If mdtPrivilegeAbilityTran IsNot Nothing Then
                Dim strPrivilegeUnkid As String = ""
                Dim blnFlag As Boolean = True
                Dim strColName As String = ""

                Dim xColCheckedIdCol As String = ""
                For Each dCol As DataColumn In mdtPrivilegeAbilityTran.Columns
                    strColName = "" : strPrivilegeUnkid = "" : xColCheckedIdCol = ""
                    If dCol.ExtendedProperties.Count > 0 AndAlso dCol.ExtendedProperties(dCol.ColumnName) <> 1 Then
                        Dim mDictionary As New Dictionary(Of Integer, String)
                        strColName = dCol.ColumnName
                        If strColName.StartsWith("objAUD") Then
                            xColCheckedIdCol = strColName.Replace("objAUD", "")

                            Dim strPrivilegs As Integer() = mdtPrivilegeAbilityTran.AsEnumerable().Where(Function(r) r.Field(Of Boolean)(xColCheckedIdCol) = True And r.Field(Of Boolean)("objisprivilegegroup") = False And r.Field(Of String)(strColName) <> "D").[Select](Function(s) s.Field(Of Integer)("objprivilegeunkid")).ToArray()
                            strPrivilegeUnkid = String.Join(",", strPrivilegs.[Select](Function(cv) cv.ToString()).ToArray())

                            mDictionary = (From oPrvilege In mdtPrivilegeAbilityTran.AsEnumerable() Where oPrvilege(strColName) <> "" And oPrvilege("objisprivilegegroup") = False Select New With {Key .ID = CInt(oPrvilege.Item("objprivilegeunkid")), Key .NAME = oPrvilege.Item(strColName).ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)


                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objUser._FormName = mstrModuleName
                            objUser._LoginEmployeeunkid = 0
                            objUser._ClientIP = getIP()
                            objUser._HostName = getHostName()
                            objUser._FromWeb = False
                            objUser._AuditUserId = User._Object._Userunkid
                            objUser._CompanyUnkid = Company._Object._Companyunkid
                            objUser._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            If Not objUser.SaveAbilityLevelPrivilege(dCol.ExtendedProperties(dCol.ColumnName), mDictionary, strPrivilegeUnkid) Then
                                blnFlag = False
                            End If

                        End If
                    End If
                Next
                If blnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Information Saved Successfully.")) '?1
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            'S.SANDEEP [21-SEP-2018] -- START
            'Dim strBuilder As New System.Text.StringBuilder
            'strBuilder.Append(" <HTML> " & vbCrLf)
            'strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            'strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR = BLACK WIDTH='120%'> " & vbCrLf)
            'strBuilder.Append(" <TR ALIGN = LEFT VALIGN = TOP WIDTH = '120%'> " & vbCrLf)
            'For i As Integer = 1 To dgAbilityGroupPrivilege.ColumnCount - 1
            '    If dgAbilityGroupPrivilege.Columns(i).HeaderText.StartsWith("obj") Then Continue For
            '    If i = 1 Then
            '        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%' BGCOLOR = 'LightBlue'><FONT SIZE=2><B>" & dgAbilityGroupPrivilege.Columns(i).HeaderText & "</B></FONT></TD>" & vbCrLf)
            '    Else
            '        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' WIDTH = '20%' BGCOLOR = 'LightBlue'><FONT SIZE=2><B>" & dgAbilityGroupPrivilege.Columns(i).HeaderText & "</B></FONT></TD>" & vbCrLf)
            '    End If
            'Next
            'strBuilder.Append(" </TR> " & vbCrLf)

            'Dim intVisibileColCountDataGridView = dgAbilityGroupPrivilege.Columns.Cast(Of DataGridViewColumn)().Where(Function(visibleCol) visibleCol.Visible = True).Count()

            'For i As Integer = 0 To dgAbilityGroupPrivilege.Rows.Count - 1
            '    strBuilder.Append(" <TR WIDTH='120%'> " & vbCrLf)
            '    For k As Integer = 1 To dgAbilityGroupPrivilege.ColumnCount - 1
            '        If dgAbilityGroupPrivilege.Columns(k).HeaderText.StartsWith("obj") Then Continue For
            '        If k = 1 Then
            '            If CBool(dgAbilityGroupPrivilege.Rows(i).Cells(intIsPrivilegeColOrdinal).Value) = True Then
            '                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' BGCOLOR = 'TEAL' COLSPAN='" & intVisibileColCountDataGridView - 1 & "'><FONT SIZE=2 COLOR = 'WHITE'><B>" & CStr(dgAbilityGroupPrivilege.Rows(i).Cells(k).Value) & "</B></FONT></TD>" & vbCrLf)
            '            Else
            '                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '100%'><FONT SIZE=2> &nbsp;&nbsp;&nbsp;&nbsp; " & CStr(dgAbilityGroupPrivilege.Rows(i).Cells(k).Value) & "</FONT></TD>" & vbCrLf)
            '            End If
            '        Else
            '            If CBool(dgAbilityGroupPrivilege.Rows(i).Cells(intIsPrivilegeColOrdinal).Value) = False Then
            '                If CBool(dgAbilityGroupPrivilege.Rows(i).Cells(k).Value) = True Then
            '                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN = 'CENTER' WIDTH = '20%'><FONT SIZE=2>" & "YES" & "</FONT></TD>" & vbCrLf)
            '                Else
            '                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN = 'CENTER' WIDTH = '20%'><FONT SIZE=2>" & "NO" & "</FONT></TD>" & vbCrLf)
            '                End If
            '            End If
            '        End If
            '    Next
            '    strBuilder.Append(" </TR> " & vbCrLf)
            'Next
            'strBuilder.Append(" </TABLE> " & vbCrLf)
            'strBuilder.Append(" </BODY> " & vbCrLf)
            'strBuilder.Append(" </HTML> " & vbCrLf)

            'Dim svDialog As New SaveFileDialog
            'svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
            'If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            '    Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
            '    Dim strWriter As New IO.StreamWriter(fsFile)
            '    With strWriter
            '        .BaseStream.Seek(0, IO.SeekOrigin.End)
            '        .WriteLine(strBuilder)
            '        .Close()
            '    End With
            '    Diagnostics.Process.Start(svDialog.FileName)
            'End If
            Dim dt As DataTable = CType(dgAbilityGroupPrivilege.DataSource, DataTable).Select("objprivilegeunkid > 0").CopyToDataTable()
            Dim objUList As New ArutiReports.clsUserDetailsLogReport
            objUList.ExportPrivilegeList(dt, Me.Text, 1)
            'S.SANDEEP [21-SEP-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGridView Event(s) "

    Private Sub dgAbilityGroupPrivilege_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Dim i As Integer
        If mblnFormload = False Then Exit Sub

        If e.RowIndex = -1 Then Exit Sub

        Try
            If Me.dgAbilityGroupPrivilege.IsCurrentCellDirty Then
                Me.dgAbilityGroupPrivilege.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(intIsPrivilegeColOrdinal).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(0).Value.ToString = "-" Then

                            dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(0).Value = "+"
                        Else
                            dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(0).Value = "-"
                        End If
                        dgAbilityGroupPrivilege.Visible = True
                        For i = e.RowIndex + 1 To dgAbilityGroupPrivilege.RowCount - 1
                            If CInt(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(intPrivilegGroupOrdinal).Value) = CInt(dgAbilityGroupPrivilege.Rows(i).Cells(intPrivilegGroupOrdinal).Value) Then
                                If dgAbilityGroupPrivilege.Rows(i).Visible = False Then
                                    dgAbilityGroupPrivilege.Rows(i).Visible = True
                                Else
                                    dgAbilityGroupPrivilege.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case Is >= 2
                        For i = e.RowIndex + 1 To dgAbilityGroupPrivilege.RowCount - 1
                            If CInt(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(intPrivilegGroupOrdinal).Value) = CInt(dgAbilityGroupPrivilege.Rows(i).Cells(intPrivilegGroupOrdinal).Value) Then
                                If CBool(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                                    dgAbilityGroupPrivilege.Rows(i).Cells(e.ColumnIndex).Value = False
                                    dgAbilityGroupPrivilege.Rows(i).Cells(e.ColumnIndex + 1).Value = "D"
                                Else
                                    dgAbilityGroupPrivilege.Rows(i).Cells(e.ColumnIndex).Value = True
                                    dgAbilityGroupPrivilege.Rows(i).Cells(e.ColumnIndex + 1).Value = "A"
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            Else
                Select Case e.ColumnIndex
                    Case Is >= 2
                        If CBool(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
                            dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value = "A"
                        Else
                            dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value = "D"
                        End If
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "dgAbilityGroupPrivilege_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

'Public Class frmAbilityLevel_Privilege1
'    Inherits eZee.Common.eZeeForm

'#Region " Private Function "
'    Dim mblnFormload As Boolean = False
'    Private ReadOnly mstrModuleName As String = "frmAbilityLevel_Privilege"
'#End Region

'#Region " Private Methods "

'    Private Sub SetColorTemplate()
'        Dim intRow As Integer
'        Try
'            Dim dgvcsHeader As New DataGridViewCellStyle
'            dgvcsHeader.ForeColor = Color.White
'            dgvcsHeader.SelectionBackColor = Color.Gray
'            dgvcsHeader.BackColor = Color.Gray

'            Dim dgvcsChild As New DataGridViewCellStyle
'            dgvcsChild.SelectionBackColor = Color.LightGray
'            dgvcsChild.BackColor = Color.LightGray

'            Dim dgvcsRowHeader As New DataGridViewCellStyle
'            dgvcsRowHeader.BackColor = Color.Gray

'            For intRow = 0 To dgAbilityGroupPrivilege.RowCount - 1
'                If CBool(dgAbilityGroupPrivilege.Rows(intRow).Cells(dgAbilityGroupPrivilege.Columns.Count - 3).Value) = True Then
'                    dgAbilityGroupPrivilege.Rows(intRow).DefaultCellStyle = dgvcsHeader
'                Else
'                    dgAbilityGroupPrivilege.Rows(intRow).DefaultCellStyle = dgvcsChild
'                    dgAbilityGroupPrivilege.Rows(intRow).Cells(0).Style.BackColor = Color.DarkGray
'                    dgAbilityGroupPrivilege.Rows(intRow).Cells(1).Style.BackColor = Color.DarkGray
'                    dgAbilityGroupPrivilege.Rows(intRow).Cells(1).Style.SelectionBackColor = Color.DarkGray
'                End If
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColorTemplate", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillDataGridView()
'        Dim dsList As New DataSet
'        Dim dsAbilityList As New DataSet
'        Dim objUser As New clsUserAddEdit
'        Try
'            objUser._Userunkid = User._Object._Userunkid
'            Dim i As Integer
'            Dim objdgcol As DataGridViewCheckBoxColumn
'            dsAbilityList = objUser.GetAbilityGroupList()

'            For i = 0 To dsAbilityList.Tables(0).Rows.Count - 1
'                objdgcol = New DataGridViewCheckBoxColumn
'                objdgcol.Name = "obj" & i
'                objdgcol.HeaderText = dsAbilityList.Tables(0).Rows(i).Item("name").ToString
'                objdgcol.DefaultCellStyle.WrapMode = DataGridViewTriState.False
'                objdgcol.DataPropertyName = dsAbilityList.Tables(0).Rows(i).Item("roleunkid").ToString

'                'Sandeep [ 29 NOV 2010 ] -- Start

'                'S.SANDEEP [ 23 NOV 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If dsAbilityList.Tables(0).Rows(i).Item("roleunkid") = 1 Then objdgcol.ReadOnly = True
'                ''Sandeep [ 07 FEB 2011 ] -- START
'                'If User._Object._Userunkid <> 1 Then objdgcol.ReadOnly = True
'                ''Sandeep [ 07 FEB 2011 ] -- END 

'                If dsAbilityList.Tables(0).Rows(i).Item("roleunkid") = 1 Then
'                    objdgcol.ReadOnly = True
'                Else
'                    objdgcol.ReadOnly = Not User._Object.Privilege._AllowtoSaveAbilityLevel
'                End If
'                'S.SANDEEP [ 23 NOV 2012 ] -- END

'                'S.SANDEEP [10 AUG 2015] -- START
'                'ENHANCEMENT : Aruti SaaS Changes
'                objdgcol.Tag = dsAbilityList.Tables(0).Rows(i).Item("roleunkid").ToString
'                'S.SANDEEP [10 AUG 2015] -- END


'                'Sandeep [ 29 NOV 2010 ] -- End 
'                dgAbilityGroupPrivilege.Columns.Add(objdgcol)
'            Next

'            dsList = objUser.GetPrivilegeAbilityList
'            objdgcolIsPrivilegeGroup.DataPropertyName = "isprivilegegroup"
'            objdgcolPrivilegeUnkid.DataPropertyName = "privilegeunkid"
'            objdgcolPrivilegeGroupUnkid.DataPropertyName = "privilegeGroupunkidunkid"
'            dgcolPrivilege.DataPropertyName = "privilege"
'            dgAbilityGroupPrivilege.DataSource = dsList.Tables(0)

'            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
'            objdgvsCollapseHeader.Font = New Font(dgAbilityGroupPrivilege.Font.FontFamily, 12, FontStyle.Bold)
'            objdgvsCollapseHeader.ForeColor = Color.White
'            objdgvsCollapseHeader.BackColor = Color.Gray
'            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
'            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

'            Dim objdgvsCollapseChild As New DataGridViewCellStyle
'            objdgvsCollapseChild.Font = New Font(dgAbilityGroupPrivilege.Font.FontFamily, 12, FontStyle.Bold)
'            objdgvsCollapseChild.ForeColor = Color.White
'            objdgvsCollapseChild.BackColor = Color.DarkGray
'            objdgvsCollapseChild.SelectionBackColor = Color.DarkGray
'            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter

'            For i = 0 To dgAbilityGroupPrivilege.RowCount - 1
'                If CBool(dgAbilityGroupPrivilege.Rows(i).Cells(dgAbilityGroupPrivilege.ColumnCount - 3).Value) = True Then
'                    'S.SANDEEP [ 19 MAY 2012 ] -- START
'                    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'                    'dgAbilityGroupPrivilege.Rows(i).Cells(0).Value = "-"
'                    dgAbilityGroupPrivilege.Rows(i).Cells(0).Value = "+"
'                    'S.SANDEEP [ 19 MAY 2012 ] -- END
'                    dgAbilityGroupPrivilege.Rows(i).Cells(0).Style = objdgvsCollapseHeader
'                Else
'                    'S.SANDEEP [ 19 MAY 2012 ] -- START
'                    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'                    'dgAbilityGroupPrivilege.Rows(i).Cells(0).Value = ""
'                    'dgAbilityGroupPrivilege.Rows(i).Cells(0).Style = objdgvsCollapseChild
'                    dgAbilityGroupPrivilege.Rows(i).Visible = False
'                    'S.SANDEEP [ 19 MAY 2012 ] -- END
'                End If
'            Next

'            Call SetColorTemplate()
'            mblnFormload = True

'            AddHandler dgAbilityGroupPrivilege.CellContentClick, AddressOf dgAbilityGroupPrivilege_CellContentClick
'            AddHandler dgAbilityGroupPrivilege.CellContentDoubleClick, AddressOf dgAbilityGroupPrivilege_CellContentClick


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillDataGridView", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 29 OCT 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub SetVisibility()
'        Try
'            btnExport.Enabled = User._Object.Privilege._AllowtoExportAbilityLevel
'            btnSave.Enabled = User._Object.Privilege._AllowtoSaveAbilityLevel
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 29 OCT 2012 ] -- END

'#End Region

'#Region " Form's Events "
'    Private Sub frmAbilityLevel_Previlege_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        If e.Control = True And e.KeyCode = Keys.S Then
'            Call btnSave.PerformClick()
'        End If
'    End Sub

'    Private Sub frmAbilityLevel_Previlege_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'Anjan (02 Sep 2011)-End 
'            'Call Language.setLanguage(Me.Name)

'            'S.SANDEEP [ 23 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'Call FillDataGridView()
'            If User._Object.Privilege._AllowtoViewAbilityLevel = True Then
'                Call FillDataGridView()
'                Call SetVisibility()
'            Else
'                btnExport.Enabled = False
'                btnSave.Enabled = False
'            End If
'            ''S.SANDEEP [ 29 OCT 2012 ] -- START
'            ''ENHANCEMENT : TRA CHANGES
'            'Call SetVisibility()
'            ''S.SANDEEP [ 29 OCT 2012 ] -- END

'            'S.SANDEEP [ 23 NOV 2012 ] -- END

'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-Start
'    'Issue : Including Language Settings.
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-End 

'#End Region

'#Region " Button's Events "
'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim intRow As Integer = 0
'        Dim intCol As Integer = 0
'        Dim strPrivilegeUnkid As String = ""
'        Dim objUser As New clsUserAddEdit
'        Dim blnFlag As Boolean = True
'        Try
'            objUser._Userunkid = User._Object._Userunkid
'            For intCol = 2 To dgAbilityGroupPrivilege.ColumnCount - 4
'                strPrivilegeUnkid = ""

'                'S.SANDEEP [10 AUG 2015] -- START
'                'ENHANCEMENT : Aruti SaaS Changes
'                'For intRow = 0 To dgAbilityGroupPrivilege.RowCount - 1
'                '    If CBool(dgAbilityGroupPrivilege.Rows(intRow).Cells(dgAbilityGroupPrivilege.ColumnCount - 3).Value) = False Then
'                '        If CBool(dgAbilityGroupPrivilege.Rows(intRow).Cells(intCol).Value) = True Then
'                '            strPrivilegeUnkid += "," & dgAbilityGroupPrivilege.Rows(intRow).Cells(dgAbilityGroupPrivilege.ColumnCount - 2).Value.ToString
'                '        End If
'                '    End If
'                'Next
'                'strPrivilegeUnkid = Mid(strPrivilegeUnkid, 2)
'                'If Not objUser.SaveAbilityLevelPrivilege(CInt(dgAbilityGroupPrivilege.Columns(intCol).DataPropertyName), strPrivilegeUnkid) Then
'                '    blnFlag = False
'                'End If

'                Dim strSummCities As Integer() = CType(dgAbilityGroupPrivilege.DataSource, DataTable).AsEnumerable().Where(Function(r) r.Field(Of Boolean)(intCol) = True And r.Field(Of Boolean)("isprivilegegroup") = False).[Select](Function(s) s.Field(Of Integer)("privilegeunkid")).ToArray()
'                strPrivilegeUnkid = String.Join(",", strSummCities.[Select](Function(cv) cv.ToString()).ToArray())

'                If strPrivilegeUnkid.Trim.Length > 0 Then
'                    If Not objUser.SaveAbilityLevelPrivilege(CInt(dgAbilityGroupPrivilege.Columns(intCol + 1).DataPropertyName), strPrivilegeUnkid) Then
'                        blnFlag = False
'                    End If
'                End If

'                'S.SANDEEP [10 AUG 2015] -- END
'            Next
'            If blnFlag Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Information Saved Successfully.")) '?1
'                Me.Close()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        Finally
'            objUser = Nothing
'        End Try
'    End Sub

'    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
'        Me.Close()
'    End Sub
'#End Region

'#Region " Controls "

'    Private Sub dgAbilityGroupPrivilege_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
'        Dim i As Integer
'        If mblnFormload = False Then Exit Sub

'        If e.RowIndex = -1 Then Exit Sub

'        Try
'            If Me.dgAbilityGroupPrivilege.IsCurrentCellDirty Then
'                Me.dgAbilityGroupPrivilege.CommitEdit(DataGridViewDataErrorContexts.Commit)
'            End If

'            If CBool(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(dgAbilityGroupPrivilege.Columns.Count - 3).Value) = True Then
'                Select Case CInt(e.ColumnIndex)
'                    Case 0
'                        If dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(0).Value.ToString = "-" Then

'                            dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(0).Value = "+"
'                        Else
'                            dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(0).Value = "-"
'                        End If
'                        dgAbilityGroupPrivilege.Visible = True
'                        For i = e.RowIndex + 1 To dgAbilityGroupPrivilege.RowCount - 1
'                            If CInt(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(dgAbilityGroupPrivilege.ColumnCount - 1).Value) = CInt(dgAbilityGroupPrivilege.Rows(i).Cells(dgAbilityGroupPrivilege.ColumnCount - 1).Value) Then
'                                If dgAbilityGroupPrivilege.Rows(i).Visible = False Then
'                                    dgAbilityGroupPrivilege.Rows(i).Visible = True
'                                Else
'                                    dgAbilityGroupPrivilege.Rows(i).Visible = False
'                                End If
'                            Else
'                                Exit For
'                            End If
'                        Next
'                    Case Is >= 2
'                        For i = e.RowIndex + 1 To dgAbilityGroupPrivilege.RowCount - 1
'                            If CInt(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(dgAbilityGroupPrivilege.ColumnCount - 1).Value) = CInt(dgAbilityGroupPrivilege.Rows(i).Cells(dgAbilityGroupPrivilege.ColumnCount - 1).Value) Then
'                                If CBool(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
'                                    dgAbilityGroupPrivilege.Rows(i).Cells(e.ColumnIndex).Value = False
'                                Else
'                                    dgAbilityGroupPrivilege.Rows(i).Cells(e.ColumnIndex).Value = True
'                                End If
'                            Else
'                                Exit For
'                            End If
'                        Next
'                End Select
'            Else
'                Select Case e.ColumnIndex
'                    Case Is >= 2
'                        For i = 0 To dgAbilityGroupPrivilege.RowCount - 1
'                            If CInt(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(dgAbilityGroupPrivilege.ColumnCount - 1).Value) = CInt(dgAbilityGroupPrivilege.Rows(i).Cells(dgAbilityGroupPrivilege.ColumnCount - 1).Value) Then
'                                If CBool(dgAbilityGroupPrivilege.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
'                                    dgAbilityGroupPrivilege.Rows(i).Cells(e.ColumnIndex).Value = True
'                                End If
'                                Exit For
'                            End If
'                        Next
'                End Select
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "dgAbilityGroupPrivilege_CellContentClick", mstrModuleName)
'        End Try
'    End Sub




'#End Region

'    'S.SANDEEP [ 24 MAY 2012 ] -- START
'    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
'        Dim strBuilder As New System.Text.StringBuilder
'        strBuilder.Append(" <HTML> " & vbCrLf)
'        strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
'        strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR = BLACK WIDTH='120%'> " & vbCrLf)
'        strBuilder.Append(" <TR ALIGN = LEFT VALIGN = TOP WIDTH = '120%'> " & vbCrLf)
'        For i As Integer = 1 To dgAbilityGroupPrivilege.ColumnCount - 4
'            If i = 1 Then
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '100%' BGCOLOR = 'LightBlue'><FONT SIZE=2><B>" & dgAbilityGroupPrivilege.Columns(i).HeaderText & "</B></FONT></TD>" & vbCrLf)
'            Else
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' WIDTH = '100%' BGCOLOR = 'LightBlue'><FONT SIZE=2><B>" & dgAbilityGroupPrivilege.Columns(i).HeaderText & "</B></FONT></TD>" & vbCrLf)
'            End If
'        Next
'        strBuilder.Append(" </TR> " & vbCrLf)

'        For i As Integer = 0 To dgAbilityGroupPrivilege.Rows.Count - 1
'            strBuilder.Append(" <TR WIDTH='120%'> " & vbCrLf)
'            For k As Integer = 1 To dgAbilityGroupPrivilege.ColumnCount - 4
'                If k = 1 Then
'                    If CBool(dgAbilityGroupPrivilege.Rows(i).Cells(objdgcolIsPrivilegeGroup.Name).Value) = True Then
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' BGCOLOR = 'BLACK' COLSPAN='" & dgAbilityGroupPrivilege.ColumnCount - 4 & "'><FONT SIZE=2 COLOR = 'WHITE'><B>" & CStr(dgAbilityGroupPrivilege.Rows(i).Cells(k).Value) & "</B></FONT></TD>" & vbCrLf)
'                    Else
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '100%'><FONT SIZE=2> &nbsp;&nbsp;&nbsp;&nbsp; " & CStr(dgAbilityGroupPrivilege.Rows(i).Cells(k).Value) & "</FONT></TD>" & vbCrLf)
'                    End If
'                Else
'                    If CBool(dgAbilityGroupPrivilege.Rows(i).Cells(objdgcolIsPrivilegeGroup.Name).Value) = False Then
'                        If CBool(dgAbilityGroupPrivilege.Rows(i).Cells(k).Value) = True Then
'                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN = 'CENTER' WIDTH = '20%'><FONT SIZE=2>" & "YES" & "</FONT></TD>" & vbCrLf)
'                        Else
'                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN = 'CENTER' WIDTH = '20%'><FONT SIZE=2>" & "NO" & "</FONT></TD>" & vbCrLf)
'                        End If
'                    End If
'                End If
'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)
'        Next
'        strBuilder.Append(" </TABLE> " & vbCrLf)
'        strBuilder.Append(" </BODY> " & vbCrLf)
'        strBuilder.Append(" </HTML> " & vbCrLf)

'        Dim svDialog As New SaveFileDialog
'        svDialog.Filter = "Excel files (*.xlsx)|*.xlsx""
'        If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
'            Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
'            Dim strWriter As New IO.StreamWriter(fsFile)
'            With strWriter
'                .BaseStream.Seek(0, IO.SeekOrigin.End)
'                .WriteLine(strBuilder)
'                .Close()
'            End With
'            Diagnostics.Process.Start(svDialog.FileName)
'        End If

'    End Sub
'    'S.SANDEEP [ 24 MAY 2012 ] -- END


'    '<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'    Private Sub OtherSettings()
'        Try
'            Me.SuspendLayout()

'            Call SetLanguage()

'            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
'            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
'            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
'            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
'            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


'            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
'            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

'            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
'            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

'            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
'            Me.btnExport.GradientForeColor = GUI._ButttonFontColor


'            Me.ResumeLayout()
'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetLanguage()
'        Try
'            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
'            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
'            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
'            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
'            Me.dgcolPrivilege.HeaderText = Language._Object.getCaption(Me.dgcolPrivilege.Name, Me.dgcolPrivilege.HeaderText)
'            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetMessages()
'        Try
'            Language.setMessage(mstrModuleName, 1, "Information Saved Successfully.")

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
'        End Try
'    End Sub
'#End Region 'Language & UI Settings
'    '</Language>

'End Class