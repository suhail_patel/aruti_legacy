﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPasswordOptions
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPasswordOptions))
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.tbOptions = New System.Windows.Forms.TabControl
        Me.tbpOtherOptions = New System.Windows.Forms.TabPage
        Me.objSpcGOption = New System.Windows.Forms.SplitContainer
        Me.gbPasswordHistorySettings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.nudDaysPassword = New System.Windows.Forms.NumericUpDown
        Me.chkDonotAllowReuseOfPasswordUsedInTheLast = New System.Windows.Forms.CheckBox
        Me.lblDays = New System.Windows.Forms.Label
        Me.nudNumberPassword = New System.Windows.Forms.NumericUpDown
        Me.chkDonotAllowToReuseAnyOfTheLast = New System.Windows.Forms.CheckBox
        Me.lblPasswordsUsed = New System.Windows.Forms.Label
        Me.gbPassowordExpiration = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.nudMaxDays = New System.Windows.Forms.NumericUpDown
        Me.lblMaximumDays = New System.Windows.Forms.Label
        Me.nudMinDays = New System.Windows.Forms.NumericUpDown
        Me.lblMinimumDays = New System.Windows.Forms.Label
        Me.gbPasswordPolicy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblPasswordComplexity = New System.Windows.Forms.Label
        Me.chkPasswordComplexity = New System.Windows.Forms.CheckBox
        Me.chkSpecialCharsMandatory = New System.Windows.Forms.CheckBox
        Me.chkNumericMandatory = New System.Windows.Forms.CheckBox
        Me.chkLowerCaseMandatory = New System.Windows.Forms.CheckBox
        Me.chkUpperCaseMandatory = New System.Windows.Forms.CheckBox
        Me.gbPasswordLength = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.nudMinLength = New System.Windows.Forms.NumericUpDown
        Me.lblMinimumLength = New System.Windows.Forms.Label
        Me.gbUserLoginOptions = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkAllowLoginOnActualEOCRetireDate = New System.Windows.Forms.CheckBox
        Me.EZeeLine2 = New eZee.Common.eZeeLine
        Me.chkUserChangePwdnextLogon = New System.Windows.Forms.CheckBox
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.chkADUserFromEmpMst = New System.Windows.Forms.CheckBox
        Me.LblDomain = New System.Windows.Forms.Label
        Me.txtDomainName = New eZee.TextBox.AlphanumericTextBox
        Me.LblDomainPassword = New System.Windows.Forms.Label
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.txtDomainPassword = New eZee.TextBox.AlphanumericTextBox
        Me.chkEnableAllUser = New System.Windows.Forms.CheckBox
        Me.LblDomainUser = New System.Windows.Forms.Label
        Me.chkProhibitPasswordChange = New System.Windows.Forms.CheckBox
        Me.txtDomainUser = New eZee.TextBox.AlphanumericTextBox
        Me.chkImportEmployee = New System.Windows.Forms.CheckBox
        Me.txtADServerIP = New eZee.TextBox.AlphanumericTextBox
        Me.lblADSeverIP = New System.Windows.Forms.Label
        Me.radSSOAuthentication = New System.Windows.Forms.RadioButton
        Me.radADAuthentication = New System.Windows.Forms.RadioButton
        Me.radBasicAuthentication = New System.Windows.Forms.RadioButton
        Me.gbUnameLength = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.nudUMinLength = New System.Windows.Forms.NumericUpDown
        Me.lblUMinLength = New System.Windows.Forms.Label
        Me.gbAccountLock = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblNote = New System.Windows.Forms.Label
        Me.radAdminUnlock = New System.Windows.Forms.RadioButton
        Me.lblMinutes = New System.Windows.Forms.Label
        Me.txtRetrydurattion = New eZee.TextBox.IntegerTextBox
        Me.radLoginRetry = New System.Windows.Forms.RadioButton
        Me.lblAttempts = New System.Windows.Forms.Label
        Me.nudAttempts = New System.Windows.Forms.NumericUpDown
        Me.lblLockAttempts = New System.Windows.Forms.Label
        Me.gbApplicationIdleSetting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblIdleMinutes = New System.Windows.Forms.Label
        Me.txtIdleMinutes = New eZee.TextBox.IntegerTextBox
        Me.lblAutoLogout = New System.Windows.Forms.Label
        Me.objlinef1 = New eZee.Common.eZeeLine
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.tbpAnnouncements = New System.Windows.Forms.TabPage
        Me.objspcAmmounce = New System.Windows.Forms.SplitContainer
        Me.dgvAnnouncements = New System.Windows.Forms.DataGridView
        Me.tlsOperations = New System.Windows.Forms.ToolStrip
        Me.objtlbbtnCut = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnCopy = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnPaste = New System.Windows.Forms.ToolStripButton
        Me.objtoolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.objcboFont = New System.Windows.Forms.ToolStripComboBox
        Me.objcboFontSize = New System.Windows.Forms.ToolStripComboBox
        Me.objToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnColor = New System.Windows.Forms.ToolStripDropDownButton
        Me.objToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnBold = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnItalic = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnUnderline = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnLeftAlign = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnCenterAlign = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRightAlign = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnUndo = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRedo = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnBulletSimple = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnLeftIndent = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRightIndent = New System.Windows.Forms.ToolStripButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtAnnouncement = New System.Windows.Forms.RichTextBox
        Me.btnAttachment = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkRequireAcknowledgement = New System.Windows.Forms.CheckBox
        Me.btnMarkAsClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.LblAnnouncement = New System.Windows.Forms.Label
        Me.txtTitle = New eZee.TextBox.AlphanumericTextBox
        Me.LblTitle = New System.Windows.Forms.Label
        Me.imgAnnouncement = New eZee.Common.eZeeImageControl
        Me.btnAnnoucementSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlColor = New System.Windows.Forms.Panel
        Me.objbtn48 = New System.Windows.Forms.Button
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.btnMoreColor = New System.Windows.Forms.Button
        Me.objbtn47 = New System.Windows.Forms.Button
        Me.objbtn34 = New System.Windows.Forms.Button
        Me.objbtn18 = New System.Windows.Forms.Button
        Me.objbtn46 = New System.Windows.Forms.Button
        Me.objbtn32 = New System.Windows.Forms.Button
        Me.objbtn33 = New System.Windows.Forms.Button
        Me.objbtn45 = New System.Windows.Forms.Button
        Me.objbtn19 = New System.Windows.Forms.Button
        Me.objbtn2 = New System.Windows.Forms.Button
        Me.objbtn44 = New System.Windows.Forms.Button
        Me.objbtn31 = New System.Windows.Forms.Button
        Me.objbtn42 = New System.Windows.Forms.Button
        Me.objbtn20 = New System.Windows.Forms.Button
        Me.objbtn43 = New System.Windows.Forms.Button
        Me.objbtn3 = New System.Windows.Forms.Button
        Me.objbtn15 = New System.Windows.Forms.Button
        Me.objbtn26 = New System.Windows.Forms.Button
        Me.objbtn16 = New System.Windows.Forms.Button
        Me.objbtn35 = New System.Windows.Forms.Button
        Me.objbtn41 = New System.Windows.Forms.Button
        Me.objbtn21 = New System.Windows.Forms.Button
        Me.objbtn8 = New System.Windows.Forms.Button
        Me.objbtn4 = New System.Windows.Forms.Button
        Me.objbtn23 = New System.Windows.Forms.Button
        Me.objbtn27 = New System.Windows.Forms.Button
        Me.objbtn1 = New System.Windows.Forms.Button
        Me.objbtn36 = New System.Windows.Forms.Button
        Me.objbtn7 = New System.Windows.Forms.Button
        Me.objbtn22 = New System.Windows.Forms.Button
        Me.objbtn24 = New System.Windows.Forms.Button
        Me.objbtn5 = New System.Windows.Forms.Button
        Me.objbtn12 = New System.Windows.Forms.Button
        Me.objbtn28 = New System.Windows.Forms.Button
        Me.objbtn17 = New System.Windows.Forms.Button
        Me.objbtn37 = New System.Windows.Forms.Button
        Me.objbtn40 = New System.Windows.Forms.Button
        Me.objbtn9 = New System.Windows.Forms.Button
        Me.objbtn25 = New System.Windows.Forms.Button
        Me.objbtn6 = New System.Windows.Forms.Button
        Me.objbtn13 = New System.Windows.Forms.Button
        Me.objbtn29 = New System.Windows.Forms.Button
        Me.objbtn11 = New System.Windows.Forms.Button
        Me.objbtn38 = New System.Windows.Forms.Button
        Me.objbtn39 = New System.Windows.Forms.Button
        Me.objbtn10 = New System.Windows.Forms.Button
        Me.objbtn30 = New System.Windows.Forms.Button
        Me.objbtn14 = New System.Windows.Forms.Button
        Me.objlinef2 = New eZee.Common.eZeeLine
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMarkAsClose = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAnnouncementId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.tbOptions.SuspendLayout()
        Me.tbpOtherOptions.SuspendLayout()
        Me.objSpcGOption.Panel1.SuspendLayout()
        Me.objSpcGOption.Panel2.SuspendLayout()
        Me.objSpcGOption.SuspendLayout()
        Me.gbPasswordHistorySettings.SuspendLayout()
        CType(Me.nudDaysPassword, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudNumberPassword, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbPassowordExpiration.SuspendLayout()
        CType(Me.nudMaxDays, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMinDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbPasswordPolicy.SuspendLayout()
        Me.gbPasswordLength.SuspendLayout()
        CType(Me.nudMinLength, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbUserLoginOptions.SuspendLayout()
        Me.gbUnameLength.SuspendLayout()
        CType(Me.nudUMinLength, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbAccountLock.SuspendLayout()
        CType(Me.nudAttempts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbApplicationIdleSetting.SuspendLayout()
        Me.tbpAnnouncements.SuspendLayout()
        Me.objspcAmmounce.Panel1.SuspendLayout()
        Me.objspcAmmounce.Panel2.SuspendLayout()
        Me.objspcAmmounce.SuspendLayout()
        CType(Me.dgvAnnouncements, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlsOperations.SuspendLayout()
        Me.pnlColor.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.tbOptions)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(880, 567)
        Me.pnlMainInfo.TabIndex = 3
        '
        'tbOptions
        '
        Me.tbOptions.Controls.Add(Me.tbpOtherOptions)
        Me.tbOptions.Controls.Add(Me.tbpAnnouncements)
        Me.tbOptions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbOptions.Location = New System.Drawing.Point(0, 0)
        Me.tbOptions.Name = "tbOptions"
        Me.tbOptions.SelectedIndex = 0
        Me.tbOptions.Size = New System.Drawing.Size(880, 567)
        Me.tbOptions.TabIndex = 41
        '
        'tbpOtherOptions
        '
        Me.tbpOtherOptions.BackColor = System.Drawing.Color.Transparent
        Me.tbpOtherOptions.Controls.Add(Me.objSpcGOption)
        Me.tbpOtherOptions.Location = New System.Drawing.Point(4, 22)
        Me.tbpOtherOptions.Name = "tbpOtherOptions"
        Me.tbpOtherOptions.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpOtherOptions.Size = New System.Drawing.Size(872, 541)
        Me.tbpOtherOptions.TabIndex = 0
        Me.tbpOtherOptions.Text = "General Options"
        Me.tbpOtherOptions.UseVisualStyleBackColor = True
        '
        'objSpcGOption
        '
        Me.objSpcGOption.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objSpcGOption.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.objSpcGOption.IsSplitterFixed = True
        Me.objSpcGOption.Location = New System.Drawing.Point(3, 3)
        Me.objSpcGOption.Name = "objSpcGOption"
        Me.objSpcGOption.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSpcGOption.Panel1
        '
        Me.objSpcGOption.Panel1.Controls.Add(Me.gbPasswordHistorySettings)
        Me.objSpcGOption.Panel1.Controls.Add(Me.gbPassowordExpiration)
        Me.objSpcGOption.Panel1.Controls.Add(Me.gbPasswordPolicy)
        Me.objSpcGOption.Panel1.Controls.Add(Me.gbPasswordLength)
        Me.objSpcGOption.Panel1.Controls.Add(Me.gbUserLoginOptions)
        Me.objSpcGOption.Panel1.Controls.Add(Me.gbUnameLength)
        Me.objSpcGOption.Panel1.Controls.Add(Me.gbAccountLock)
        Me.objSpcGOption.Panel1.Controls.Add(Me.gbApplicationIdleSetting)
        '
        'objSpcGOption.Panel2
        '
        Me.objSpcGOption.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.objSpcGOption.Panel2.Controls.Add(Me.objlinef1)
        Me.objSpcGOption.Panel2.Controls.Add(Me.btnSave)
        Me.objSpcGOption.Panel2MinSize = 50
        Me.objSpcGOption.Size = New System.Drawing.Size(866, 535)
        Me.objSpcGOption.SplitterDistance = 484
        Me.objSpcGOption.SplitterWidth = 1
        Me.objSpcGOption.TabIndex = 46
        '
        'gbPasswordHistorySettings
        '
        Me.gbPasswordHistorySettings.BorderColor = System.Drawing.Color.Black
        Me.gbPasswordHistorySettings.Checked = False
        Me.gbPasswordHistorySettings.CollapseAllExceptThis = False
        Me.gbPasswordHistorySettings.CollapsedHoverImage = Nothing
        Me.gbPasswordHistorySettings.CollapsedNormalImage = Nothing
        Me.gbPasswordHistorySettings.CollapsedPressedImage = Nothing
        Me.gbPasswordHistorySettings.CollapseOnLoad = False
        Me.gbPasswordHistorySettings.Controls.Add(Me.nudDaysPassword)
        Me.gbPasswordHistorySettings.Controls.Add(Me.chkDonotAllowReuseOfPasswordUsedInTheLast)
        Me.gbPasswordHistorySettings.Controls.Add(Me.lblDays)
        Me.gbPasswordHistorySettings.Controls.Add(Me.nudNumberPassword)
        Me.gbPasswordHistorySettings.Controls.Add(Me.chkDonotAllowToReuseAnyOfTheLast)
        Me.gbPasswordHistorySettings.Controls.Add(Me.lblPasswordsUsed)
        Me.gbPasswordHistorySettings.ExpandedHoverImage = Nothing
        Me.gbPasswordHistorySettings.ExpandedNormalImage = Nothing
        Me.gbPasswordHistorySettings.ExpandedPressedImage = Nothing
        Me.gbPasswordHistorySettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPasswordHistorySettings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPasswordHistorySettings.HeaderHeight = 25
        Me.gbPasswordHistorySettings.HeaderMessage = ""
        Me.gbPasswordHistorySettings.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPasswordHistorySettings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPasswordHistorySettings.HeightOnCollapse = 0
        Me.gbPasswordHistorySettings.LeftTextSpace = 0
        Me.gbPasswordHistorySettings.Location = New System.Drawing.Point(394, 396)
        Me.gbPasswordHistorySettings.Name = "gbPasswordHistorySettings"
        Me.gbPasswordHistorySettings.OpenHeight = 300
        Me.gbPasswordHistorySettings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPasswordHistorySettings.ShowBorder = True
        Me.gbPasswordHistorySettings.ShowCheckBox = True
        Me.gbPasswordHistorySettings.ShowCollapseButton = False
        Me.gbPasswordHistorySettings.ShowDefaultBorderColor = True
        Me.gbPasswordHistorySettings.ShowDownButton = False
        Me.gbPasswordHistorySettings.ShowHeader = True
        Me.gbPasswordHistorySettings.Size = New System.Drawing.Size(468, 84)
        Me.gbPasswordHistorySettings.TabIndex = 27
        Me.gbPasswordHistorySettings.Temp = 0
        Me.gbPasswordHistorySettings.Text = "Password History Settings"
        Me.gbPasswordHistorySettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDaysPassword
        '
        Me.nudDaysPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDaysPassword.Location = New System.Drawing.Point(293, 56)
        Me.nudDaysPassword.Name = "nudDaysPassword"
        Me.nudDaysPassword.Size = New System.Drawing.Size(51, 21)
        Me.nudDaysPassword.TabIndex = 28
        Me.nudDaysPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudDaysPassword.Value = New Decimal(New Integer() {90, 0, 0, 0})
        '
        'chkDonotAllowReuseOfPasswordUsedInTheLast
        '
        Me.chkDonotAllowReuseOfPasswordUsedInTheLast.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDonotAllowReuseOfPasswordUsedInTheLast.Location = New System.Drawing.Point(22, 58)
        Me.chkDonotAllowReuseOfPasswordUsedInTheLast.Name = "chkDonotAllowReuseOfPasswordUsedInTheLast"
        Me.chkDonotAllowReuseOfPasswordUsedInTheLast.Size = New System.Drawing.Size(277, 17)
        Me.chkDonotAllowReuseOfPasswordUsedInTheLast.TabIndex = 27
        Me.chkDonotAllowReuseOfPasswordUsedInTheLast.Text = "Do not Allow Re-use Of Password Used In The Last"
        Me.chkDonotAllowReuseOfPasswordUsedInTheLast.UseVisualStyleBackColor = True
        '
        'lblDays
        '
        Me.lblDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDays.Location = New System.Drawing.Point(349, 58)
        Me.lblDays.Name = "lblDays"
        Me.lblDays.Size = New System.Drawing.Size(100, 15)
        Me.lblDays.TabIndex = 26
        Me.lblDays.Text = "Days"
        '
        'nudNumberPassword
        '
        Me.nudNumberPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudNumberPassword.Location = New System.Drawing.Point(248, 32)
        Me.nudNumberPassword.Name = "nudNumberPassword"
        Me.nudNumberPassword.Size = New System.Drawing.Size(51, 21)
        Me.nudNumberPassword.TabIndex = 25
        Me.nudNumberPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudNumberPassword.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'chkDonotAllowToReuseAnyOfTheLast
        '
        Me.chkDonotAllowToReuseAnyOfTheLast.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDonotAllowToReuseAnyOfTheLast.Location = New System.Drawing.Point(22, 33)
        Me.chkDonotAllowToReuseAnyOfTheLast.Name = "chkDonotAllowToReuseAnyOfTheLast"
        Me.chkDonotAllowToReuseAnyOfTheLast.Size = New System.Drawing.Size(229, 17)
        Me.chkDonotAllowToReuseAnyOfTheLast.TabIndex = 24
        Me.chkDonotAllowToReuseAnyOfTheLast.Text = "Do not Allow To Re-use Any Of The Last"
        Me.chkDonotAllowToReuseAnyOfTheLast.UseVisualStyleBackColor = True
        '
        'lblPasswordsUsed
        '
        Me.lblPasswordsUsed.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPasswordsUsed.Location = New System.Drawing.Point(309, 34)
        Me.lblPasswordsUsed.Name = "lblPasswordsUsed"
        Me.lblPasswordsUsed.Size = New System.Drawing.Size(127, 15)
        Me.lblPasswordsUsed.TabIndex = 13
        Me.lblPasswordsUsed.Text = "Passwords Used"
        '
        'gbPassowordExpiration
        '
        Me.gbPassowordExpiration.BorderColor = System.Drawing.Color.Black
        Me.gbPassowordExpiration.Checked = False
        Me.gbPassowordExpiration.CollapseAllExceptThis = False
        Me.gbPassowordExpiration.CollapsedHoverImage = Nothing
        Me.gbPassowordExpiration.CollapsedNormalImage = Nothing
        Me.gbPassowordExpiration.CollapsedPressedImage = Nothing
        Me.gbPassowordExpiration.CollapseOnLoad = False
        Me.gbPassowordExpiration.Controls.Add(Me.nudMaxDays)
        Me.gbPassowordExpiration.Controls.Add(Me.lblMaximumDays)
        Me.gbPassowordExpiration.Controls.Add(Me.nudMinDays)
        Me.gbPassowordExpiration.Controls.Add(Me.lblMinimumDays)
        Me.gbPassowordExpiration.ExpandedHoverImage = Nothing
        Me.gbPassowordExpiration.ExpandedNormalImage = Nothing
        Me.gbPassowordExpiration.ExpandedPressedImage = Nothing
        Me.gbPassowordExpiration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPassowordExpiration.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPassowordExpiration.HeaderHeight = 25
        Me.gbPassowordExpiration.HeaderMessage = ""
        Me.gbPassowordExpiration.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPassowordExpiration.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPassowordExpiration.HeightOnCollapse = 0
        Me.gbPassowordExpiration.LeftTextSpace = 0
        Me.gbPassowordExpiration.Location = New System.Drawing.Point(4, 4)
        Me.gbPassowordExpiration.Name = "gbPassowordExpiration"
        Me.gbPassowordExpiration.OpenHeight = 300
        Me.gbPassowordExpiration.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPassowordExpiration.ShowBorder = True
        Me.gbPassowordExpiration.ShowCheckBox = True
        Me.gbPassowordExpiration.ShowCollapseButton = False
        Me.gbPassowordExpiration.ShowDefaultBorderColor = True
        Me.gbPassowordExpiration.ShowDownButton = False
        Me.gbPassowordExpiration.ShowHeader = True
        Me.gbPassowordExpiration.Size = New System.Drawing.Size(381, 57)
        Me.gbPassowordExpiration.TabIndex = 21
        Me.gbPassowordExpiration.Temp = 0
        Me.gbPassowordExpiration.Text = "Password Expiration"
        Me.gbPassowordExpiration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudMaxDays
        '
        Me.nudMaxDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaxDays.Location = New System.Drawing.Point(293, 30)
        Me.nudMaxDays.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.nudMaxDays.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMaxDays.Name = "nudMaxDays"
        Me.nudMaxDays.Size = New System.Drawing.Size(58, 21)
        Me.nudMaxDays.TabIndex = 4
        Me.nudMaxDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMaxDays.Value = New Decimal(New Integer() {60, 0, 0, 0})
        '
        'lblMaximumDays
        '
        Me.lblMaximumDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaximumDays.Location = New System.Drawing.Point(195, 33)
        Me.lblMaximumDays.Name = "lblMaximumDays"
        Me.lblMaximumDays.Size = New System.Drawing.Size(92, 15)
        Me.lblMaximumDays.TabIndex = 3
        Me.lblMaximumDays.Text = "Maximum Days"
        '
        'nudMinDays
        '
        Me.nudMinDays.Enabled = False
        Me.nudMinDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinDays.Location = New System.Drawing.Point(99, 30)
        Me.nudMinDays.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMinDays.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMinDays.Name = "nudMinDays"
        Me.nudMinDays.Size = New System.Drawing.Size(58, 21)
        Me.nudMinDays.TabIndex = 2
        Me.nudMinDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMinDays.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblMinimumDays
        '
        Me.lblMinimumDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinimumDays.Location = New System.Drawing.Point(8, 33)
        Me.lblMinimumDays.Name = "lblMinimumDays"
        Me.lblMinimumDays.Size = New System.Drawing.Size(85, 15)
        Me.lblMinimumDays.TabIndex = 1
        Me.lblMinimumDays.Text = "Minimum Days"
        '
        'gbPasswordPolicy
        '
        Me.gbPasswordPolicy.BorderColor = System.Drawing.Color.Black
        Me.gbPasswordPolicy.Checked = False
        Me.gbPasswordPolicy.CollapseAllExceptThis = False
        Me.gbPasswordPolicy.CollapsedHoverImage = Nothing
        Me.gbPasswordPolicy.CollapsedNormalImage = Nothing
        Me.gbPasswordPolicy.CollapsedPressedImage = Nothing
        Me.gbPasswordPolicy.CollapseOnLoad = False
        Me.gbPasswordPolicy.Controls.Add(Me.LblPasswordComplexity)
        Me.gbPasswordPolicy.Controls.Add(Me.chkPasswordComplexity)
        Me.gbPasswordPolicy.Controls.Add(Me.chkSpecialCharsMandatory)
        Me.gbPasswordPolicy.Controls.Add(Me.chkNumericMandatory)
        Me.gbPasswordPolicy.Controls.Add(Me.chkLowerCaseMandatory)
        Me.gbPasswordPolicy.Controls.Add(Me.chkUpperCaseMandatory)
        Me.gbPasswordPolicy.ExpandedHoverImage = Nothing
        Me.gbPasswordPolicy.ExpandedNormalImage = Nothing
        Me.gbPasswordPolicy.ExpandedPressedImage = Nothing
        Me.gbPasswordPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPasswordPolicy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPasswordPolicy.HeaderHeight = 25
        Me.gbPasswordPolicy.HeaderMessage = ""
        Me.gbPasswordPolicy.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPasswordPolicy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPasswordPolicy.HeightOnCollapse = 0
        Me.gbPasswordPolicy.LeftTextSpace = 0
        Me.gbPasswordPolicy.Location = New System.Drawing.Point(4, 327)
        Me.gbPasswordPolicy.Name = "gbPasswordPolicy"
        Me.gbPasswordPolicy.OpenHeight = 300
        Me.gbPasswordPolicy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPasswordPolicy.ShowBorder = True
        Me.gbPasswordPolicy.ShowCheckBox = True
        Me.gbPasswordPolicy.ShowCollapseButton = False
        Me.gbPasswordPolicy.ShowDefaultBorderColor = True
        Me.gbPasswordPolicy.ShowDownButton = False
        Me.gbPasswordPolicy.ShowHeader = True
        Me.gbPasswordPolicy.Size = New System.Drawing.Size(381, 153)
        Me.gbPasswordPolicy.TabIndex = 20
        Me.gbPasswordPolicy.Temp = 0
        Me.gbPasswordPolicy.Text = "Password Policy"
        Me.gbPasswordPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblPasswordComplexity
        '
        Me.LblPasswordComplexity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPasswordComplexity.ForeColor = System.Drawing.Color.Red
        Me.LblPasswordComplexity.Location = New System.Drawing.Point(4, 99)
        Me.LblPasswordComplexity.Name = "LblPasswordComplexity"
        Me.LblPasswordComplexity.Size = New System.Drawing.Size(372, 30)
        Me.LblPasswordComplexity.TabIndex = 8
        Me.LblPasswordComplexity.Text = "The password will not contain the user/employee's account name or full name."
        '
        'chkPasswordComplexity
        '
        Me.chkPasswordComplexity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPasswordComplexity.Location = New System.Drawing.Point(6, 79)
        Me.chkPasswordComplexity.Name = "chkPasswordComplexity"
        Me.chkPasswordComplexity.Size = New System.Drawing.Size(198, 17)
        Me.chkPasswordComplexity.TabIndex = 7
        Me.chkPasswordComplexity.Text = "Password Complexity"
        Me.chkPasswordComplexity.UseVisualStyleBackColor = True
        '
        'chkSpecialCharsMandatory
        '
        Me.chkSpecialCharsMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSpecialCharsMandatory.Location = New System.Drawing.Point(6, 56)
        Me.chkSpecialCharsMandatory.Name = "chkSpecialCharsMandatory"
        Me.chkSpecialCharsMandatory.Size = New System.Drawing.Size(198, 17)
        Me.chkSpecialCharsMandatory.TabIndex = 4
        Me.chkSpecialCharsMandatory.Text = "Make Special Characters Mandatory"
        Me.chkSpecialCharsMandatory.UseVisualStyleBackColor = True
        '
        'chkNumericMandatory
        '
        Me.chkNumericMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNumericMandatory.Location = New System.Drawing.Point(208, 56)
        Me.chkNumericMandatory.Name = "chkNumericMandatory"
        Me.chkNumericMandatory.Size = New System.Drawing.Size(170, 17)
        Me.chkNumericMandatory.TabIndex = 3
        Me.chkNumericMandatory.Text = "Make Numeric Mandatory"
        Me.chkNumericMandatory.UseVisualStyleBackColor = True
        '
        'chkLowerCaseMandatory
        '
        Me.chkLowerCaseMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLowerCaseMandatory.Location = New System.Drawing.Point(208, 33)
        Me.chkLowerCaseMandatory.Name = "chkLowerCaseMandatory"
        Me.chkLowerCaseMandatory.Size = New System.Drawing.Size(169, 17)
        Me.chkLowerCaseMandatory.TabIndex = 2
        Me.chkLowerCaseMandatory.Text = "Make Lower Case Mandatory"
        Me.chkLowerCaseMandatory.UseVisualStyleBackColor = True
        '
        'chkUpperCaseMandatory
        '
        Me.chkUpperCaseMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUpperCaseMandatory.Location = New System.Drawing.Point(6, 33)
        Me.chkUpperCaseMandatory.Name = "chkUpperCaseMandatory"
        Me.chkUpperCaseMandatory.Size = New System.Drawing.Size(198, 17)
        Me.chkUpperCaseMandatory.TabIndex = 1
        Me.chkUpperCaseMandatory.Text = "Make Upper Case Mandatory"
        Me.chkUpperCaseMandatory.UseVisualStyleBackColor = True
        '
        'gbPasswordLength
        '
        Me.gbPasswordLength.BorderColor = System.Drawing.Color.Black
        Me.gbPasswordLength.Checked = False
        Me.gbPasswordLength.CollapseAllExceptThis = False
        Me.gbPasswordLength.CollapsedHoverImage = Nothing
        Me.gbPasswordLength.CollapsedNormalImage = Nothing
        Me.gbPasswordLength.CollapsedPressedImage = Nothing
        Me.gbPasswordLength.CollapseOnLoad = False
        Me.gbPasswordLength.Controls.Add(Me.nudMinLength)
        Me.gbPasswordLength.Controls.Add(Me.lblMinimumLength)
        Me.gbPasswordLength.ExpandedHoverImage = Nothing
        Me.gbPasswordLength.ExpandedNormalImage = Nothing
        Me.gbPasswordLength.ExpandedPressedImage = Nothing
        Me.gbPasswordLength.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPasswordLength.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPasswordLength.HeaderHeight = 25
        Me.gbPasswordLength.HeaderMessage = ""
        Me.gbPasswordLength.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPasswordLength.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPasswordLength.HeightOnCollapse = 0
        Me.gbPasswordLength.LeftTextSpace = 0
        Me.gbPasswordLength.Location = New System.Drawing.Point(4, 67)
        Me.gbPasswordLength.Name = "gbPasswordLength"
        Me.gbPasswordLength.OpenHeight = 300
        Me.gbPasswordLength.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPasswordLength.ShowBorder = True
        Me.gbPasswordLength.ShowCheckBox = True
        Me.gbPasswordLength.ShowCollapseButton = False
        Me.gbPasswordLength.ShowDefaultBorderColor = True
        Me.gbPasswordLength.ShowDownButton = False
        Me.gbPasswordLength.ShowHeader = True
        Me.gbPasswordLength.Size = New System.Drawing.Size(188, 56)
        Me.gbPasswordLength.TabIndex = 22
        Me.gbPasswordLength.Temp = 0
        Me.gbPasswordLength.Text = "Password Length"
        Me.gbPasswordLength.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudMinLength
        '
        Me.nudMinLength.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinLength.Location = New System.Drawing.Point(99, 30)
        Me.nudMinLength.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nudMinLength.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMinLength.Name = "nudMinLength"
        Me.nudMinLength.Size = New System.Drawing.Size(58, 21)
        Me.nudMinLength.TabIndex = 6
        Me.nudMinLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMinLength.Value = New Decimal(New Integer() {8, 0, 0, 0})
        '
        'lblMinimumLength
        '
        Me.lblMinimumLength.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinimumLength.Location = New System.Drawing.Point(8, 33)
        Me.lblMinimumLength.Name = "lblMinimumLength"
        Me.lblMinimumLength.Size = New System.Drawing.Size(85, 15)
        Me.lblMinimumLength.TabIndex = 5
        Me.lblMinimumLength.Text = "Minimum Length"
        '
        'gbUserLoginOptions
        '
        Me.gbUserLoginOptions.BorderColor = System.Drawing.Color.Black
        Me.gbUserLoginOptions.Checked = False
        Me.gbUserLoginOptions.CollapseAllExceptThis = False
        Me.gbUserLoginOptions.CollapsedHoverImage = Nothing
        Me.gbUserLoginOptions.CollapsedNormalImage = Nothing
        Me.gbUserLoginOptions.CollapsedPressedImage = Nothing
        Me.gbUserLoginOptions.CollapseOnLoad = False
        Me.gbUserLoginOptions.Controls.Add(Me.chkAllowLoginOnActualEOCRetireDate)
        Me.gbUserLoginOptions.Controls.Add(Me.EZeeLine2)
        Me.gbUserLoginOptions.Controls.Add(Me.chkUserChangePwdnextLogon)
        Me.gbUserLoginOptions.Controls.Add(Me.EZeeLine1)
        Me.gbUserLoginOptions.Controls.Add(Me.chkADUserFromEmpMst)
        Me.gbUserLoginOptions.Controls.Add(Me.LblDomain)
        Me.gbUserLoginOptions.Controls.Add(Me.txtDomainName)
        Me.gbUserLoginOptions.Controls.Add(Me.LblDomainPassword)
        Me.gbUserLoginOptions.Controls.Add(Me.CheckBox1)
        Me.gbUserLoginOptions.Controls.Add(Me.txtDomainPassword)
        Me.gbUserLoginOptions.Controls.Add(Me.chkEnableAllUser)
        Me.gbUserLoginOptions.Controls.Add(Me.LblDomainUser)
        Me.gbUserLoginOptions.Controls.Add(Me.chkProhibitPasswordChange)
        Me.gbUserLoginOptions.Controls.Add(Me.txtDomainUser)
        Me.gbUserLoginOptions.Controls.Add(Me.chkImportEmployee)
        Me.gbUserLoginOptions.Controls.Add(Me.txtADServerIP)
        Me.gbUserLoginOptions.Controls.Add(Me.lblADSeverIP)
        Me.gbUserLoginOptions.Controls.Add(Me.radSSOAuthentication)
        Me.gbUserLoginOptions.Controls.Add(Me.radADAuthentication)
        Me.gbUserLoginOptions.Controls.Add(Me.radBasicAuthentication)
        Me.gbUserLoginOptions.ExpandedHoverImage = Nothing
        Me.gbUserLoginOptions.ExpandedNormalImage = Nothing
        Me.gbUserLoginOptions.ExpandedPressedImage = Nothing
        Me.gbUserLoginOptions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUserLoginOptions.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUserLoginOptions.HeaderHeight = 25
        Me.gbUserLoginOptions.HeaderMessage = ""
        Me.gbUserLoginOptions.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbUserLoginOptions.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUserLoginOptions.HeightOnCollapse = 0
        Me.gbUserLoginOptions.LeftTextSpace = 0
        Me.gbUserLoginOptions.Location = New System.Drawing.Point(394, 4)
        Me.gbUserLoginOptions.Name = "gbUserLoginOptions"
        Me.gbUserLoginOptions.OpenHeight = 300
        Me.gbUserLoginOptions.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUserLoginOptions.ShowBorder = True
        Me.gbUserLoginOptions.ShowCheckBox = False
        Me.gbUserLoginOptions.ShowCollapseButton = False
        Me.gbUserLoginOptions.ShowDefaultBorderColor = True
        Me.gbUserLoginOptions.ShowDownButton = False
        Me.gbUserLoginOptions.ShowHeader = True
        Me.gbUserLoginOptions.Size = New System.Drawing.Size(468, 386)
        Me.gbUserLoginOptions.TabIndex = 26
        Me.gbUserLoginOptions.Temp = 0
        Me.gbUserLoginOptions.Text = "User Login Options"
        Me.gbUserLoginOptions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAllowLoginOnActualEOCRetireDate
        '
        Me.chkAllowLoginOnActualEOCRetireDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAllowLoginOnActualEOCRetireDate.Location = New System.Drawing.Point(9, 363)
        Me.chkAllowLoginOnActualEOCRetireDate.Name = "chkAllowLoginOnActualEOCRetireDate"
        Me.chkAllowLoginOnActualEOCRetireDate.Size = New System.Drawing.Size(441, 17)
        Me.chkAllowLoginOnActualEOCRetireDate.TabIndex = 39
        Me.chkAllowLoginOnActualEOCRetireDate.Text = "Allow Employee to Access System on Acutal Date of EOC/Retriement Date" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.chkAllowLoginOnActualEOCRetireDate.UseVisualStyleBackColor = True
        '
        'EZeeLine2
        '
        Me.EZeeLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine2.Location = New System.Drawing.Point(6, 50)
        Me.EZeeLine2.Name = "EZeeLine2"
        Me.EZeeLine2.Size = New System.Drawing.Size(444, 21)
        Me.EZeeLine2.TabIndex = 37
        Me.EZeeLine2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkUserChangePwdnextLogon
        '
        Me.chkUserChangePwdnextLogon.Enabled = False
        Me.chkUserChangePwdnextLogon.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUserChangePwdnextLogon.Location = New System.Drawing.Point(9, 33)
        Me.chkUserChangePwdnextLogon.Name = "chkUserChangePwdnextLogon"
        Me.chkUserChangePwdnextLogon.Size = New System.Drawing.Size(338, 17)
        Me.chkUserChangePwdnextLogon.TabIndex = 36
        Me.chkUserChangePwdnextLogon.Text = "User must change password at next logon"
        Me.chkUserChangePwdnextLogon.UseVisualStyleBackColor = True
        '
        'EZeeLine1
        '
        Me.EZeeLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(10, 143)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(440, 11)
        Me.EZeeLine1.TabIndex = 34
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkADUserFromEmpMst
        '
        Me.chkADUserFromEmpMst.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkADUserFromEmpMst.Location = New System.Drawing.Point(9, 339)
        Me.chkADUserFromEmpMst.Name = "chkADUserFromEmpMst"
        Me.chkADUserFromEmpMst.Size = New System.Drawing.Size(441, 17)
        Me.chkADUserFromEmpMst.TabIndex = 32
        Me.chkADUserFromEmpMst.Text = "Create Active Directory User From Employee Master"
        Me.chkADUserFromEmpMst.UseVisualStyleBackColor = True
        '
        'LblDomain
        '
        Me.LblDomain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDomain.Location = New System.Drawing.Point(25, 256)
        Me.LblDomain.Name = "LblDomain"
        Me.LblDomain.Size = New System.Drawing.Size(96, 16)
        Me.LblDomain.TabIndex = 9
        Me.LblDomain.Text = "Domain Name"
        Me.LblDomain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomainName
        '
        Me.txtDomainName.Enabled = False
        Me.txtDomainName.Flags = 0
        Me.txtDomainName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomainName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtDomainName.Location = New System.Drawing.Point(127, 254)
        Me.txtDomainName.Name = "txtDomainName"
        Me.txtDomainName.Size = New System.Drawing.Size(323, 21)
        Me.txtDomainName.TabIndex = 10
        '
        'LblDomainPassword
        '
        Me.LblDomainPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDomainPassword.Location = New System.Drawing.Point(25, 309)
        Me.LblDomainPassword.Name = "LblDomainPassword"
        Me.LblDomainPassword.Size = New System.Drawing.Size(96, 16)
        Me.LblDomainPassword.TabIndex = 13
        Me.LblDomainPassword.Text = "Domain Password"
        Me.LblDomainPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(163, 123)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(204, 17)
        Me.CheckBox1.TabIndex = 30
        Me.CheckBox1.Text = "Allow to login from multiple machines."
        Me.CheckBox1.UseVisualStyleBackColor = True
        Me.CheckBox1.Visible = False
        '
        'txtDomainPassword
        '
        Me.txtDomainPassword.Enabled = False
        Me.txtDomainPassword.Flags = 0
        Me.txtDomainPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomainPassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomainPassword.Location = New System.Drawing.Point(127, 307)
        Me.txtDomainPassword.Name = "txtDomainPassword"
        Me.txtDomainPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtDomainPassword.Size = New System.Drawing.Size(323, 21)
        Me.txtDomainPassword.TabIndex = 14
        '
        'chkEnableAllUser
        '
        Me.chkEnableAllUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEnableAllUser.Location = New System.Drawing.Point(29, 123)
        Me.chkEnableAllUser.Name = "chkEnableAllUser"
        Me.chkEnableAllUser.Size = New System.Drawing.Size(338, 17)
        Me.chkEnableAllUser.TabIndex = 3
        Me.chkEnableAllUser.Text = "Enable All User(s)."
        Me.chkEnableAllUser.UseVisualStyleBackColor = True
        '
        'LblDomainUser
        '
        Me.LblDomainUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDomainUser.Location = New System.Drawing.Point(25, 282)
        Me.LblDomainUser.Name = "LblDomainUser"
        Me.LblDomainUser.Size = New System.Drawing.Size(96, 16)
        Me.LblDomainUser.TabIndex = 11
        Me.LblDomainUser.Text = "Domain User "
        Me.LblDomainUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkProhibitPasswordChange
        '
        Me.chkProhibitPasswordChange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkProhibitPasswordChange.Location = New System.Drawing.Point(9, 74)
        Me.chkProhibitPasswordChange.Name = "chkProhibitPasswordChange"
        Me.chkProhibitPasswordChange.Size = New System.Drawing.Size(342, 17)
        Me.chkProhibitPasswordChange.TabIndex = 1
        Me.chkProhibitPasswordChange.Text = "Prohibit Employee Password Change by User"
        Me.chkProhibitPasswordChange.UseVisualStyleBackColor = True
        '
        'txtDomainUser
        '
        Me.txtDomainUser.Enabled = False
        Me.txtDomainUser.Flags = 0
        Me.txtDomainUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomainUser.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(47)}
        Me.txtDomainUser.Location = New System.Drawing.Point(127, 280)
        Me.txtDomainUser.Name = "txtDomainUser"
        Me.txtDomainUser.Size = New System.Drawing.Size(323, 21)
        Me.txtDomainUser.TabIndex = 12
        '
        'chkImportEmployee
        '
        Me.chkImportEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkImportEmployee.Location = New System.Drawing.Point(9, 97)
        Me.chkImportEmployee.Name = "chkImportEmployee"
        Me.chkImportEmployee.Size = New System.Drawing.Size(342, 17)
        Me.chkImportEmployee.TabIndex = 2
        Me.chkImportEmployee.Text = "Import users from employee list."
        Me.chkImportEmployee.UseVisualStyleBackColor = True
        '
        'txtADServerIP
        '
        Me.txtADServerIP.Enabled = False
        Me.txtADServerIP.Flags = 0
        Me.txtADServerIP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtADServerIP.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtADServerIP.Location = New System.Drawing.Point(127, 228)
        Me.txtADServerIP.Name = "txtADServerIP"
        Me.txtADServerIP.Size = New System.Drawing.Size(323, 21)
        Me.txtADServerIP.TabIndex = 8
        '
        'lblADSeverIP
        '
        Me.lblADSeverIP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblADSeverIP.Location = New System.Drawing.Point(25, 230)
        Me.lblADSeverIP.Name = "lblADSeverIP"
        Me.lblADSeverIP.Size = New System.Drawing.Size(96, 16)
        Me.lblADSeverIP.TabIndex = 7
        Me.lblADSeverIP.Text = "A.D Server IP "
        '
        'radSSOAuthentication
        '
        Me.radSSOAuthentication.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSSOAuthentication.Location = New System.Drawing.Point(13, 205)
        Me.radSSOAuthentication.Name = "radSSOAuthentication"
        Me.radSSOAuthentication.Size = New System.Drawing.Size(437, 17)
        Me.radSSOAuthentication.TabIndex = 6
        Me.radSSOAuthentication.TabStop = True
        Me.radSSOAuthentication.Text = "Integrate Active Directory User(s). With  SSO (Single Sign On) Mode."
        Me.radSSOAuthentication.UseVisualStyleBackColor = True
        '
        'radADAuthentication
        '
        Me.radADAuthentication.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radADAuthentication.Location = New System.Drawing.Point(13, 182)
        Me.radADAuthentication.Name = "radADAuthentication"
        Me.radADAuthentication.Size = New System.Drawing.Size(437, 17)
        Me.radADAuthentication.TabIndex = 5
        Me.radADAuthentication.TabStop = True
        Me.radADAuthentication.Text = "Integrate Active Directory User(s). With Basic Authentication Mode."
        Me.radADAuthentication.UseVisualStyleBackColor = True
        '
        'radBasicAuthentication
        '
        Me.radBasicAuthentication.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radBasicAuthentication.Location = New System.Drawing.Point(13, 159)
        Me.radBasicAuthentication.Name = "radBasicAuthentication"
        Me.radBasicAuthentication.Size = New System.Drawing.Size(437, 17)
        Me.radBasicAuthentication.TabIndex = 4
        Me.radBasicAuthentication.TabStop = True
        Me.radBasicAuthentication.Text = "Use Basic Authentication Mode."
        Me.radBasicAuthentication.UseVisualStyleBackColor = True
        '
        'gbUnameLength
        '
        Me.gbUnameLength.BorderColor = System.Drawing.Color.Black
        Me.gbUnameLength.Checked = False
        Me.gbUnameLength.CollapseAllExceptThis = False
        Me.gbUnameLength.CollapsedHoverImage = Nothing
        Me.gbUnameLength.CollapsedNormalImage = Nothing
        Me.gbUnameLength.CollapsedPressedImage = Nothing
        Me.gbUnameLength.CollapseOnLoad = False
        Me.gbUnameLength.Controls.Add(Me.nudUMinLength)
        Me.gbUnameLength.Controls.Add(Me.lblUMinLength)
        Me.gbUnameLength.ExpandedHoverImage = Nothing
        Me.gbUnameLength.ExpandedNormalImage = Nothing
        Me.gbUnameLength.ExpandedPressedImage = Nothing
        Me.gbUnameLength.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUnameLength.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUnameLength.HeaderHeight = 25
        Me.gbUnameLength.HeaderMessage = ""
        Me.gbUnameLength.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbUnameLength.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUnameLength.HeightOnCollapse = 0
        Me.gbUnameLength.LeftTextSpace = 0
        Me.gbUnameLength.Location = New System.Drawing.Point(196, 67)
        Me.gbUnameLength.Name = "gbUnameLength"
        Me.gbUnameLength.OpenHeight = 300
        Me.gbUnameLength.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUnameLength.ShowBorder = True
        Me.gbUnameLength.ShowCheckBox = True
        Me.gbUnameLength.ShowCollapseButton = False
        Me.gbUnameLength.ShowDefaultBorderColor = True
        Me.gbUnameLength.ShowDownButton = False
        Me.gbUnameLength.ShowHeader = True
        Me.gbUnameLength.Size = New System.Drawing.Size(189, 56)
        Me.gbUnameLength.TabIndex = 23
        Me.gbUnameLength.Temp = 0
        Me.gbUnameLength.Text = "User Name Length"
        Me.gbUnameLength.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudUMinLength
        '
        Me.nudUMinLength.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudUMinLength.Location = New System.Drawing.Point(99, 30)
        Me.nudUMinLength.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nudUMinLength.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudUMinLength.Name = "nudUMinLength"
        Me.nudUMinLength.Size = New System.Drawing.Size(58, 21)
        Me.nudUMinLength.TabIndex = 6
        Me.nudUMinLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudUMinLength.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'lblUMinLength
        '
        Me.lblUMinLength.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUMinLength.Location = New System.Drawing.Point(8, 33)
        Me.lblUMinLength.Name = "lblUMinLength"
        Me.lblUMinLength.Size = New System.Drawing.Size(85, 15)
        Me.lblUMinLength.TabIndex = 5
        Me.lblUMinLength.Text = "Minimum Length"
        '
        'gbAccountLock
        '
        Me.gbAccountLock.BorderColor = System.Drawing.Color.Black
        Me.gbAccountLock.Checked = False
        Me.gbAccountLock.CollapseAllExceptThis = False
        Me.gbAccountLock.CollapsedHoverImage = Nothing
        Me.gbAccountLock.CollapsedNormalImage = Nothing
        Me.gbAccountLock.CollapsedPressedImage = Nothing
        Me.gbAccountLock.CollapseOnLoad = False
        Me.gbAccountLock.Controls.Add(Me.lblNote)
        Me.gbAccountLock.Controls.Add(Me.radAdminUnlock)
        Me.gbAccountLock.Controls.Add(Me.lblMinutes)
        Me.gbAccountLock.Controls.Add(Me.txtRetrydurattion)
        Me.gbAccountLock.Controls.Add(Me.radLoginRetry)
        Me.gbAccountLock.Controls.Add(Me.lblAttempts)
        Me.gbAccountLock.Controls.Add(Me.nudAttempts)
        Me.gbAccountLock.Controls.Add(Me.lblLockAttempts)
        Me.gbAccountLock.ExpandedHoverImage = Nothing
        Me.gbAccountLock.ExpandedNormalImage = Nothing
        Me.gbAccountLock.ExpandedPressedImage = Nothing
        Me.gbAccountLock.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAccountLock.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAccountLock.HeaderHeight = 25
        Me.gbAccountLock.HeaderMessage = ""
        Me.gbAccountLock.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAccountLock.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAccountLock.HeightOnCollapse = 0
        Me.gbAccountLock.LeftTextSpace = 0
        Me.gbAccountLock.Location = New System.Drawing.Point(4, 192)
        Me.gbAccountLock.Name = "gbAccountLock"
        Me.gbAccountLock.OpenHeight = 300
        Me.gbAccountLock.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAccountLock.ShowBorder = True
        Me.gbAccountLock.ShowCheckBox = True
        Me.gbAccountLock.ShowCollapseButton = False
        Me.gbAccountLock.ShowDefaultBorderColor = True
        Me.gbAccountLock.ShowDownButton = False
        Me.gbAccountLock.ShowHeader = True
        Me.gbAccountLock.Size = New System.Drawing.Size(381, 129)
        Me.gbAccountLock.TabIndex = 24
        Me.gbAccountLock.Temp = 0
        Me.gbAccountLock.Text = "Locking Out Users"
        Me.gbAccountLock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNote
        '
        Me.lblNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote.ForeColor = System.Drawing.Color.Maroon
        Me.lblNote.Location = New System.Drawing.Point(8, 109)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(362, 15)
        Me.lblNote.TabIndex = 13
        Me.lblNote.Text = "*Administrator(s) will be locked out for 5 minutes only."
        '
        'radAdminUnlock
        '
        Me.radAdminUnlock.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAdminUnlock.Location = New System.Drawing.Point(22, 82)
        Me.radAdminUnlock.Name = "radAdminUnlock"
        Me.radAdminUnlock.Size = New System.Drawing.Size(289, 17)
        Me.radAdminUnlock.TabIndex = 12
        Me.radAdminUnlock.TabStop = True
        Me.radAdminUnlock.Text = "User must be unlocked by Administrator."
        Me.radAdminUnlock.UseVisualStyleBackColor = True
        '
        'lblMinutes
        '
        Me.lblMinutes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinutes.Location = New System.Drawing.Point(216, 58)
        Me.lblMinutes.Name = "lblMinutes"
        Me.lblMinutes.Size = New System.Drawing.Size(127, 15)
        Me.lblMinutes.TabIndex = 11
        Me.lblMinutes.Text = "Minute(s)."
        '
        'txtRetrydurattion
        '
        Me.txtRetrydurattion.AllowNegative = False
        Me.txtRetrydurattion.Decimal = New Decimal(New Integer() {20, 0, 0, 0})
        Me.txtRetrydurattion.DigitsInGroup = 0
        Me.txtRetrydurattion.Flags = 65536
        Me.txtRetrydurattion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRetrydurattion.Location = New System.Drawing.Point(160, 55)
        Me.txtRetrydurattion.MaxDecimalPlaces = 0
        Me.txtRetrydurattion.MaxWholeDigits = 9
        Me.txtRetrydurattion.Name = "txtRetrydurattion"
        Me.txtRetrydurattion.Prefix = ""
        Me.txtRetrydurattion.RangeMax = 2147483647
        Me.txtRetrydurattion.RangeMin = -2147483647
        Me.txtRetrydurattion.Size = New System.Drawing.Size(50, 21)
        Me.txtRetrydurattion.TabIndex = 10
        Me.txtRetrydurattion.Text = "20"
        Me.txtRetrydurattion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'radLoginRetry
        '
        Me.radLoginRetry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLoginRetry.Location = New System.Drawing.Point(22, 57)
        Me.radLoginRetry.Name = "radLoginRetry"
        Me.radLoginRetry.Size = New System.Drawing.Size(132, 17)
        Me.radLoginRetry.TabIndex = 9
        Me.radLoginRetry.TabStop = True
        Me.radLoginRetry.Text = "Allow login retry after"
        Me.radLoginRetry.UseVisualStyleBackColor = True
        '
        'lblAttempts
        '
        Me.lblAttempts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttempts.Location = New System.Drawing.Point(216, 31)
        Me.lblAttempts.Name = "lblAttempts"
        Me.lblAttempts.Size = New System.Drawing.Size(127, 15)
        Me.lblAttempts.TabIndex = 8
        Me.lblAttempts.Text = "Unsuccessful Attempts"
        '
        'nudAttempts
        '
        Me.nudAttempts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAttempts.Location = New System.Drawing.Point(160, 28)
        Me.nudAttempts.Maximum = New Decimal(New Integer() {8, 0, 0, 0})
        Me.nudAttempts.Minimum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudAttempts.Name = "nudAttempts"
        Me.nudAttempts.Size = New System.Drawing.Size(50, 21)
        Me.nudAttempts.TabIndex = 7
        Me.nudAttempts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudAttempts.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'lblLockAttempts
        '
        Me.lblLockAttempts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLockAttempts.Location = New System.Drawing.Point(19, 31)
        Me.lblLockAttempts.Name = "lblLockAttempts"
        Me.lblLockAttempts.Size = New System.Drawing.Size(135, 15)
        Me.lblLockAttempts.TabIndex = 6
        Me.lblLockAttempts.Text = "Lock out users after"
        '
        'gbApplicationIdleSetting
        '
        Me.gbApplicationIdleSetting.BorderColor = System.Drawing.Color.Black
        Me.gbApplicationIdleSetting.Checked = False
        Me.gbApplicationIdleSetting.CollapseAllExceptThis = False
        Me.gbApplicationIdleSetting.CollapsedHoverImage = Nothing
        Me.gbApplicationIdleSetting.CollapsedNormalImage = Nothing
        Me.gbApplicationIdleSetting.CollapsedPressedImage = Nothing
        Me.gbApplicationIdleSetting.CollapseOnLoad = False
        Me.gbApplicationIdleSetting.Controls.Add(Me.lblIdleMinutes)
        Me.gbApplicationIdleSetting.Controls.Add(Me.txtIdleMinutes)
        Me.gbApplicationIdleSetting.Controls.Add(Me.lblAutoLogout)
        Me.gbApplicationIdleSetting.ExpandedHoverImage = Nothing
        Me.gbApplicationIdleSetting.ExpandedNormalImage = Nothing
        Me.gbApplicationIdleSetting.ExpandedPressedImage = Nothing
        Me.gbApplicationIdleSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApplicationIdleSetting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApplicationIdleSetting.HeaderHeight = 25
        Me.gbApplicationIdleSetting.HeaderMessage = ""
        Me.gbApplicationIdleSetting.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbApplicationIdleSetting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApplicationIdleSetting.HeightOnCollapse = 0
        Me.gbApplicationIdleSetting.LeftTextSpace = 0
        Me.gbApplicationIdleSetting.Location = New System.Drawing.Point(4, 129)
        Me.gbApplicationIdleSetting.Name = "gbApplicationIdleSetting"
        Me.gbApplicationIdleSetting.OpenHeight = 300
        Me.gbApplicationIdleSetting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApplicationIdleSetting.ShowBorder = True
        Me.gbApplicationIdleSetting.ShowCheckBox = True
        Me.gbApplicationIdleSetting.ShowCollapseButton = False
        Me.gbApplicationIdleSetting.ShowDefaultBorderColor = True
        Me.gbApplicationIdleSetting.ShowDownButton = False
        Me.gbApplicationIdleSetting.ShowHeader = True
        Me.gbApplicationIdleSetting.Size = New System.Drawing.Size(381, 57)
        Me.gbApplicationIdleSetting.TabIndex = 25
        Me.gbApplicationIdleSetting.Temp = 0
        Me.gbApplicationIdleSetting.Text = "Application Idle Setting"
        Me.gbApplicationIdleSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIdleMinutes
        '
        Me.lblIdleMinutes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdleMinutes.Location = New System.Drawing.Point(216, 33)
        Me.lblIdleMinutes.Name = "lblIdleMinutes"
        Me.lblIdleMinutes.Size = New System.Drawing.Size(127, 15)
        Me.lblIdleMinutes.TabIndex = 13
        Me.lblIdleMinutes.Text = "Minute(s)."
        '
        'txtIdleMinutes
        '
        Me.txtIdleMinutes.AllowNegative = False
        Me.txtIdleMinutes.Decimal = New Decimal(New Integer() {5, 0, 0, 0})
        Me.txtIdleMinutes.DigitsInGroup = 0
        Me.txtIdleMinutes.Flags = 65536
        Me.txtIdleMinutes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdleMinutes.Location = New System.Drawing.Point(160, 30)
        Me.txtIdleMinutes.MaxDecimalPlaces = 0
        Me.txtIdleMinutes.MaxWholeDigits = 9
        Me.txtIdleMinutes.Name = "txtIdleMinutes"
        Me.txtIdleMinutes.Prefix = ""
        Me.txtIdleMinutes.RangeMax = 2147483647
        Me.txtIdleMinutes.RangeMin = -2147483647
        Me.txtIdleMinutes.Size = New System.Drawing.Size(50, 21)
        Me.txtIdleMinutes.TabIndex = 12
        Me.txtIdleMinutes.Text = "5"
        Me.txtIdleMinutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAutoLogout
        '
        Me.lblAutoLogout.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAutoLogout.Location = New System.Drawing.Point(19, 33)
        Me.lblAutoLogout.Name = "lblAutoLogout"
        Me.lblAutoLogout.Size = New System.Drawing.Size(135, 15)
        Me.lblAutoLogout.TabIndex = 5
        Me.lblAutoLogout.Text = "Auto logout user after"
        '
        'objlinef1
        '
        Me.objlinef1.Dock = System.Windows.Forms.DockStyle.Top
        Me.objlinef1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlinef1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objlinef1.Location = New System.Drawing.Point(0, 0)
        Me.objlinef1.Name = "objlinef1"
        Me.objlinef1.Size = New System.Drawing.Size(866, 1)
        Me.objlinef1.TabIndex = 131
        Me.objlinef1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(767, 15)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(93, 30)
        Me.btnSave.TabIndex = 130
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'tbpAnnouncements
        '
        Me.tbpAnnouncements.BackColor = System.Drawing.Color.Transparent
        Me.tbpAnnouncements.Controls.Add(Me.objspcAmmounce)
        Me.tbpAnnouncements.Location = New System.Drawing.Point(4, 22)
        Me.tbpAnnouncements.Name = "tbpAnnouncements"
        Me.tbpAnnouncements.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpAnnouncements.Size = New System.Drawing.Size(872, 541)
        Me.tbpAnnouncements.TabIndex = 1
        Me.tbpAnnouncements.Text = "Announcements"
        Me.tbpAnnouncements.UseVisualStyleBackColor = True
        '
        'objspcAmmounce
        '
        Me.objspcAmmounce.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspcAmmounce.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.objspcAmmounce.Location = New System.Drawing.Point(3, 3)
        Me.objspcAmmounce.Name = "objspcAmmounce"
        Me.objspcAmmounce.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspcAmmounce.Panel1
        '
        Me.objspcAmmounce.Panel1.Controls.Add(Me.dgvAnnouncements)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.tlsOperations)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.Panel1)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.txtAnnouncement)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.btnAttachment)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.chkRequireAcknowledgement)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.btnMarkAsClose)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.LblAnnouncement)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.txtTitle)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.LblTitle)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.imgAnnouncement)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.btnAnnoucementSave)
        Me.objspcAmmounce.Panel1.Controls.Add(Me.pnlColor)
        '
        'objspcAmmounce.Panel2
        '
        Me.objspcAmmounce.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.objspcAmmounce.Panel2.Controls.Add(Me.objlinef2)
        Me.objspcAmmounce.Panel2.Controls.Add(Me.btnClose)
        Me.objspcAmmounce.Panel2MinSize = 50
        Me.objspcAmmounce.Size = New System.Drawing.Size(866, 535)
        Me.objspcAmmounce.SplitterDistance = 484
        Me.objspcAmmounce.SplitterWidth = 1
        Me.objspcAmmounce.TabIndex = 37
        '
        'dgvAnnouncements
        '
        Me.dgvAnnouncements.AllowUserToAddRows = False
        Me.dgvAnnouncements.AllowUserToDeleteRows = False
        Me.dgvAnnouncements.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvAnnouncements.BackgroundColor = System.Drawing.Color.White
        Me.dgvAnnouncements.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAnnouncements.ColumnHeadersHeight = 25
        Me.dgvAnnouncements.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhTitle, Me.objdgcolhMarkAsClose, Me.objdgcolhAnnouncementId})
        Me.dgvAnnouncements.Location = New System.Drawing.Point(11, 220)
        Me.dgvAnnouncements.Name = "dgvAnnouncements"
        Me.dgvAnnouncements.ReadOnly = True
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.dgvAnnouncements.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvAnnouncements.RowHeadersVisible = False
        Me.dgvAnnouncements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAnnouncements.Size = New System.Drawing.Size(849, 259)
        Me.dgvAnnouncements.TabIndex = 175
        '
        'tlsOperations
        '
        Me.tlsOperations.BackColor = System.Drawing.Color.Transparent
        Me.tlsOperations.Dock = System.Windows.Forms.DockStyle.None
        Me.tlsOperations.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objtlbbtnCut, Me.objtlbbtnCopy, Me.objtlbbtnPaste, Me.objtoolStripSeparator1, Me.objcboFont, Me.objcboFontSize, Me.objToolStripSeparator2, Me.objtlbbtnColor, Me.objToolStripSeparator7, Me.objtlbbtnBold, Me.objtlbbtnItalic, Me.objtlbbtnUnderline, Me.objToolStripSeparator4, Me.objtlbbtnLeftAlign, Me.objtlbbtnCenterAlign, Me.objtlbbtnRightAlign, Me.objToolStripSeparator5, Me.objtlbbtnUndo, Me.objtlbbtnRedo, Me.objToolStripSeparator3, Me.objtlbbtnBulletSimple, Me.objToolStripSeparator8, Me.objtlbbtnLeftIndent, Me.objtlbbtnRightIndent})
        Me.tlsOperations.Location = New System.Drawing.Point(108, 34)
        Me.tlsOperations.Name = "tlsOperations"
        Me.tlsOperations.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsOperations.Size = New System.Drawing.Size(565, 25)
        Me.tlsOperations.TabIndex = 178
        '
        'objtlbbtnCut
        '
        Me.objtlbbtnCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCut.Image = CType(resources.GetObject("objtlbbtnCut.Image"), System.Drawing.Image)
        Me.objtlbbtnCut.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCut.Name = "objtlbbtnCut"
        Me.objtlbbtnCut.Size = New System.Drawing.Size(23, 22)
        '
        'objtlbbtnCopy
        '
        Me.objtlbbtnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCopy.Image = CType(resources.GetObject("objtlbbtnCopy.Image"), System.Drawing.Image)
        Me.objtlbbtnCopy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCopy.Name = "objtlbbtnCopy"
        Me.objtlbbtnCopy.Size = New System.Drawing.Size(23, 22)
        '
        'objtlbbtnPaste
        '
        Me.objtlbbtnPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnPaste.Image = CType(resources.GetObject("objtlbbtnPaste.Image"), System.Drawing.Image)
        Me.objtlbbtnPaste.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnPaste.Name = "objtlbbtnPaste"
        Me.objtlbbtnPaste.Size = New System.Drawing.Size(23, 22)
        '
        'objtoolStripSeparator1
        '
        Me.objtoolStripSeparator1.Name = "objtoolStripSeparator1"
        Me.objtoolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'objcboFont
        '
        Me.objcboFont.DropDownHeight = 150
        Me.objcboFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objcboFont.DropDownWidth = 250
        Me.objcboFont.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objcboFont.IntegralHeight = False
        Me.objcboFont.Name = "objcboFont"
        Me.objcboFont.Size = New System.Drawing.Size(125, 25)
        Me.objcboFont.ToolTipText = "Select Font"
        '
        'objcboFontSize
        '
        Me.objcboFontSize.AutoSize = False
        Me.objcboFontSize.DropDownWidth = 75
        Me.objcboFontSize.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objcboFontSize.Name = "objcboFontSize"
        Me.objcboFontSize.Size = New System.Drawing.Size(40, 23)
        Me.objcboFontSize.ToolTipText = "Font Size"
        '
        'objToolStripSeparator2
        '
        Me.objToolStripSeparator2.Name = "objToolStripSeparator2"
        Me.objToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnColor
        '
        Me.objtlbbtnColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnColor.Image = CType(resources.GetObject("objtlbbtnColor.Image"), System.Drawing.Image)
        Me.objtlbbtnColor.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnColor.Name = "objtlbbtnColor"
        Me.objtlbbtnColor.Size = New System.Drawing.Size(29, 22)
        Me.objtlbbtnColor.ToolTipText = "Color"
        '
        'objToolStripSeparator7
        '
        Me.objToolStripSeparator7.Name = "objToolStripSeparator7"
        Me.objToolStripSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnBold
        '
        Me.objtlbbtnBold.CheckOnClick = True
        Me.objtlbbtnBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnBold.Image = CType(resources.GetObject("objtlbbtnBold.Image"), System.Drawing.Image)
        Me.objtlbbtnBold.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnBold.Name = "objtlbbtnBold"
        Me.objtlbbtnBold.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnBold.ToolTipText = "Bold"
        '
        'objtlbbtnItalic
        '
        Me.objtlbbtnItalic.CheckOnClick = True
        Me.objtlbbtnItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnItalic.Image = CType(resources.GetObject("objtlbbtnItalic.Image"), System.Drawing.Image)
        Me.objtlbbtnItalic.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnItalic.Name = "objtlbbtnItalic"
        Me.objtlbbtnItalic.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnItalic.ToolTipText = "Italic"
        '
        'objtlbbtnUnderline
        '
        Me.objtlbbtnUnderline.CheckOnClick = True
        Me.objtlbbtnUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnUnderline.Image = CType(resources.GetObject("objtlbbtnUnderline.Image"), System.Drawing.Image)
        Me.objtlbbtnUnderline.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnUnderline.Name = "objtlbbtnUnderline"
        Me.objtlbbtnUnderline.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnUnderline.ToolTipText = "Underline"
        '
        'objToolStripSeparator4
        '
        Me.objToolStripSeparator4.Name = "objToolStripSeparator4"
        Me.objToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnLeftAlign
        '
        Me.objtlbbtnLeftAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnLeftAlign.Image = CType(resources.GetObject("objtlbbtnLeftAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnLeftAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnLeftAlign.Name = "objtlbbtnLeftAlign"
        Me.objtlbbtnLeftAlign.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnLeftAlign.ToolTipText = "Align Left"
        '
        'objtlbbtnCenterAlign
        '
        Me.objtlbbtnCenterAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCenterAlign.Image = CType(resources.GetObject("objtlbbtnCenterAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnCenterAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCenterAlign.Name = "objtlbbtnCenterAlign"
        Me.objtlbbtnCenterAlign.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnCenterAlign.ToolTipText = "Center"
        '
        'objtlbbtnRightAlign
        '
        Me.objtlbbtnRightAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRightAlign.Image = CType(resources.GetObject("objtlbbtnRightAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnRightAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRightAlign.Name = "objtlbbtnRightAlign"
        Me.objtlbbtnRightAlign.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnRightAlign.ToolTipText = "Align Right"
        '
        'objToolStripSeparator5
        '
        Me.objToolStripSeparator5.Name = "objToolStripSeparator5"
        Me.objToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnUndo
        '
        Me.objtlbbtnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnUndo.Image = CType(resources.GetObject("objtlbbtnUndo.Image"), System.Drawing.Image)
        Me.objtlbbtnUndo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnUndo.Name = "objtlbbtnUndo"
        Me.objtlbbtnUndo.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnUndo.ToolTipText = "Undo"
        '
        'objtlbbtnRedo
        '
        Me.objtlbbtnRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRedo.Image = CType(resources.GetObject("objtlbbtnRedo.Image"), System.Drawing.Image)
        Me.objtlbbtnRedo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRedo.Name = "objtlbbtnRedo"
        Me.objtlbbtnRedo.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnRedo.ToolTipText = "Redo"
        '
        'objToolStripSeparator3
        '
        Me.objToolStripSeparator3.Name = "objToolStripSeparator3"
        Me.objToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnBulletSimple
        '
        Me.objtlbbtnBulletSimple.CheckOnClick = True
        Me.objtlbbtnBulletSimple.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnBulletSimple.Image = CType(resources.GetObject("objtlbbtnBulletSimple.Image"), System.Drawing.Image)
        Me.objtlbbtnBulletSimple.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnBulletSimple.Name = "objtlbbtnBulletSimple"
        Me.objtlbbtnBulletSimple.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnBulletSimple.ToolTipText = "Insert Bullet"
        '
        'objToolStripSeparator8
        '
        Me.objToolStripSeparator8.Name = "objToolStripSeparator8"
        Me.objToolStripSeparator8.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnLeftIndent
        '
        Me.objtlbbtnLeftIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnLeftIndent.Image = CType(resources.GetObject("objtlbbtnLeftIndent.Image"), System.Drawing.Image)
        Me.objtlbbtnLeftIndent.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnLeftIndent.Name = "objtlbbtnLeftIndent"
        Me.objtlbbtnLeftIndent.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnLeftIndent.ToolTipText = "Left Indent"
        '
        'objtlbbtnRightIndent
        '
        Me.objtlbbtnRightIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRightIndent.Image = CType(resources.GetObject("objtlbbtnRightIndent.Image"), System.Drawing.Image)
        Me.objtlbbtnRightIndent.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRightIndent.Name = "objtlbbtnRightIndent"
        Me.objtlbbtnRightIndent.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnRightIndent.ToolTipText = "Right Indent"
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(195, 375)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(233, 45)
        Me.Panel1.TabIndex = 181
        '
        'txtAnnouncement
        '
        Me.txtAnnouncement.Location = New System.Drawing.Point(115, 62)
        Me.txtAnnouncement.Name = "txtAnnouncement"
        Me.txtAnnouncement.Size = New System.Drawing.Size(562, 112)
        Me.txtAnnouncement.TabIndex = 179
        Me.txtAnnouncement.Text = ""
        '
        'btnAttachment
        '
        Me.btnAttachment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAttachment.BackColor = System.Drawing.Color.White
        Me.btnAttachment.BackgroundImage = CType(resources.GetObject("btnAttachment.BackgroundImage"), System.Drawing.Image)
        Me.btnAttachment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAttachment.BorderColor = System.Drawing.Color.Empty
        Me.btnAttachment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAttachment.FlatAppearance.BorderSize = 0
        Me.btnAttachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAttachment.ForeColor = System.Drawing.Color.Black
        Me.btnAttachment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAttachment.GradientForeColor = System.Drawing.Color.Black
        Me.btnAttachment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAttachment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAttachment.Location = New System.Drawing.Point(544, 182)
        Me.btnAttachment.Name = "btnAttachment"
        Me.btnAttachment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAttachment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAttachment.Size = New System.Drawing.Size(105, 30)
        Me.btnAttachment.TabIndex = 177
        Me.btnAttachment.Text = "&Attachment"
        Me.btnAttachment.UseVisualStyleBackColor = True
        '
        'chkRequireAcknowledgement
        '
        Me.chkRequireAcknowledgement.Location = New System.Drawing.Point(115, 189)
        Me.chkRequireAcknowledgement.Name = "chkRequireAcknowledgement"
        Me.chkRequireAcknowledgement.Size = New System.Drawing.Size(187, 17)
        Me.chkRequireAcknowledgement.TabIndex = 176
        Me.chkRequireAcknowledgement.Text = "Require User Acknowledgement"
        Me.chkRequireAcknowledgement.UseVisualStyleBackColor = True
        '
        'btnMarkAsClose
        '
        Me.btnMarkAsClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMarkAsClose.BackColor = System.Drawing.Color.White
        Me.btnMarkAsClose.BackgroundImage = CType(resources.GetObject("btnMarkAsClose.BackgroundImage"), System.Drawing.Image)
        Me.btnMarkAsClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMarkAsClose.BorderColor = System.Drawing.Color.Empty
        Me.btnMarkAsClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMarkAsClose.FlatAppearance.BorderSize = 0
        Me.btnMarkAsClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMarkAsClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMarkAsClose.ForeColor = System.Drawing.Color.Black
        Me.btnMarkAsClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMarkAsClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMarkAsClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsClose.Location = New System.Drawing.Point(752, 182)
        Me.btnMarkAsClose.Name = "btnMarkAsClose"
        Me.btnMarkAsClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMarkAsClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsClose.Size = New System.Drawing.Size(108, 30)
        Me.btnMarkAsClose.TabIndex = 174
        Me.btnMarkAsClose.Text = "&Mark As Close"
        Me.btnMarkAsClose.UseVisualStyleBackColor = True
        '
        'LblAnnouncement
        '
        Me.LblAnnouncement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAnnouncement.Location = New System.Drawing.Point(8, 37)
        Me.LblAnnouncement.Name = "LblAnnouncement"
        Me.LblAnnouncement.Size = New System.Drawing.Size(92, 15)
        Me.LblAnnouncement.TabIndex = 172
        Me.LblAnnouncement.Text = "Announcement"
        '
        'txtTitle
        '
        Me.txtTitle.Flags = 0
        Me.txtTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTitle.Location = New System.Drawing.Point(115, 8)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(562, 21)
        Me.txtTitle.TabIndex = 171
        '
        'LblTitle
        '
        Me.LblTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTitle.Location = New System.Drawing.Point(8, 11)
        Me.LblTitle.Name = "LblTitle"
        Me.LblTitle.Size = New System.Drawing.Size(92, 15)
        Me.LblTitle.TabIndex = 170
        Me.LblTitle.Text = "Title"
        '
        'imgAnnouncement
        '
        Me.imgAnnouncement._FileMode = False
        Me.imgAnnouncement._Image = Nothing
        Me.imgAnnouncement._ShowAddButton = True
        Me.imgAnnouncement._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.imgAnnouncement._ShowDeleteButton = True
        Me.imgAnnouncement.BackColor = System.Drawing.Color.Transparent
        Me.imgAnnouncement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imgAnnouncement.Location = New System.Drawing.Point(683, 8)
        Me.imgAnnouncement.Name = "imgAnnouncement"
        Me.imgAnnouncement.Size = New System.Drawing.Size(177, 165)
        Me.imgAnnouncement.TabIndex = 169
        '
        'btnAnnoucementSave
        '
        Me.btnAnnoucementSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAnnoucementSave.BackColor = System.Drawing.Color.White
        Me.btnAnnoucementSave.BackgroundImage = CType(resources.GetObject("btnAnnoucementSave.BackgroundImage"), System.Drawing.Image)
        Me.btnAnnoucementSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAnnoucementSave.BorderColor = System.Drawing.Color.Empty
        Me.btnAnnoucementSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAnnoucementSave.FlatAppearance.BorderSize = 0
        Me.btnAnnoucementSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnnoucementSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnnoucementSave.ForeColor = System.Drawing.Color.Black
        Me.btnAnnoucementSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAnnoucementSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnAnnoucementSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAnnoucementSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAnnoucementSave.Location = New System.Drawing.Point(655, 182)
        Me.btnAnnoucementSave.Name = "btnAnnoucementSave"
        Me.btnAnnoucementSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAnnoucementSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAnnoucementSave.Size = New System.Drawing.Size(92, 30)
        Me.btnAnnoucementSave.TabIndex = 173
        Me.btnAnnoucementSave.Text = "&Save"
        Me.btnAnnoucementSave.UseVisualStyleBackColor = True
        '
        'pnlColor
        '
        Me.pnlColor.BackColor = System.Drawing.Color.White
        Me.pnlColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlColor.Controls.Add(Me.objbtn48)
        Me.pnlColor.Controls.Add(Me.EZeeStraightLine1)
        Me.pnlColor.Controls.Add(Me.btnMoreColor)
        Me.pnlColor.Controls.Add(Me.objbtn47)
        Me.pnlColor.Controls.Add(Me.objbtn34)
        Me.pnlColor.Controls.Add(Me.objbtn18)
        Me.pnlColor.Controls.Add(Me.objbtn46)
        Me.pnlColor.Controls.Add(Me.objbtn32)
        Me.pnlColor.Controls.Add(Me.objbtn33)
        Me.pnlColor.Controls.Add(Me.objbtn45)
        Me.pnlColor.Controls.Add(Me.objbtn19)
        Me.pnlColor.Controls.Add(Me.objbtn2)
        Me.pnlColor.Controls.Add(Me.objbtn44)
        Me.pnlColor.Controls.Add(Me.objbtn31)
        Me.pnlColor.Controls.Add(Me.objbtn42)
        Me.pnlColor.Controls.Add(Me.objbtn20)
        Me.pnlColor.Controls.Add(Me.objbtn43)
        Me.pnlColor.Controls.Add(Me.objbtn3)
        Me.pnlColor.Controls.Add(Me.objbtn15)
        Me.pnlColor.Controls.Add(Me.objbtn26)
        Me.pnlColor.Controls.Add(Me.objbtn16)
        Me.pnlColor.Controls.Add(Me.objbtn35)
        Me.pnlColor.Controls.Add(Me.objbtn41)
        Me.pnlColor.Controls.Add(Me.objbtn21)
        Me.pnlColor.Controls.Add(Me.objbtn8)
        Me.pnlColor.Controls.Add(Me.objbtn4)
        Me.pnlColor.Controls.Add(Me.objbtn23)
        Me.pnlColor.Controls.Add(Me.objbtn27)
        Me.pnlColor.Controls.Add(Me.objbtn1)
        Me.pnlColor.Controls.Add(Me.objbtn36)
        Me.pnlColor.Controls.Add(Me.objbtn7)
        Me.pnlColor.Controls.Add(Me.objbtn22)
        Me.pnlColor.Controls.Add(Me.objbtn24)
        Me.pnlColor.Controls.Add(Me.objbtn5)
        Me.pnlColor.Controls.Add(Me.objbtn12)
        Me.pnlColor.Controls.Add(Me.objbtn28)
        Me.pnlColor.Controls.Add(Me.objbtn17)
        Me.pnlColor.Controls.Add(Me.objbtn37)
        Me.pnlColor.Controls.Add(Me.objbtn40)
        Me.pnlColor.Controls.Add(Me.objbtn9)
        Me.pnlColor.Controls.Add(Me.objbtn25)
        Me.pnlColor.Controls.Add(Me.objbtn6)
        Me.pnlColor.Controls.Add(Me.objbtn13)
        Me.pnlColor.Controls.Add(Me.objbtn29)
        Me.pnlColor.Controls.Add(Me.objbtn11)
        Me.pnlColor.Controls.Add(Me.objbtn38)
        Me.pnlColor.Controls.Add(Me.objbtn39)
        Me.pnlColor.Controls.Add(Me.objbtn10)
        Me.pnlColor.Controls.Add(Me.objbtn30)
        Me.pnlColor.Controls.Add(Me.objbtn14)
        Me.pnlColor.Location = New System.Drawing.Point(324, 62)
        Me.pnlColor.Name = "pnlColor"
        Me.pnlColor.Size = New System.Drawing.Size(214, 206)
        Me.pnlColor.TabIndex = 180
        Me.pnlColor.Visible = False
        '
        'objbtn48
        '
        Me.objbtn48.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn48.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn48.Location = New System.Drawing.Point(187, 135)
        Me.objbtn48.Name = "objbtn48"
        Me.objbtn48.Size = New System.Drawing.Size(20, 20)
        Me.objbtn48.TabIndex = 76
        Me.objbtn48.UseVisualStyleBackColor = False
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(5, 161)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(202, 3)
        Me.EZeeStraightLine1.TabIndex = 3
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'btnMoreColor
        '
        Me.btnMoreColor.BackColor = System.Drawing.Color.White
        Me.btnMoreColor.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnMoreColor.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnMoreColor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnMoreColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMoreColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoreColor.Location = New System.Drawing.Point(5, 170)
        Me.btnMoreColor.Name = "btnMoreColor"
        Me.btnMoreColor.Size = New System.Drawing.Size(202, 27)
        Me.btnMoreColor.TabIndex = 68
        Me.btnMoreColor.Text = "More Color"
        Me.btnMoreColor.UseVisualStyleBackColor = False
        '
        'objbtn47
        '
        Me.objbtn47.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn47.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn47.Location = New System.Drawing.Point(161, 135)
        Me.objbtn47.Name = "objbtn47"
        Me.objbtn47.Size = New System.Drawing.Size(20, 20)
        Me.objbtn47.TabIndex = 75
        Me.objbtn47.UseVisualStyleBackColor = False
        '
        'objbtn34
        '
        Me.objbtn34.BackColor = System.Drawing.Color.Maroon
        Me.objbtn34.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn34.Location = New System.Drawing.Point(31, 109)
        Me.objbtn34.Name = "objbtn34"
        Me.objbtn34.Size = New System.Drawing.Size(20, 20)
        Me.objbtn34.TabIndex = 61
        Me.objbtn34.UseVisualStyleBackColor = False
        '
        'objbtn18
        '
        Me.objbtn18.BackColor = System.Drawing.Color.Red
        Me.objbtn18.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn18.Location = New System.Drawing.Point(31, 57)
        Me.objbtn18.Name = "objbtn18"
        Me.objbtn18.Size = New System.Drawing.Size(20, 20)
        Me.objbtn18.TabIndex = 37
        Me.objbtn18.UseVisualStyleBackColor = False
        '
        'objbtn46
        '
        Me.objbtn46.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn46.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn46.Location = New System.Drawing.Point(135, 135)
        Me.objbtn46.Name = "objbtn46"
        Me.objbtn46.Size = New System.Drawing.Size(20, 20)
        Me.objbtn46.TabIndex = 74
        Me.objbtn46.UseVisualStyleBackColor = False
        '
        'objbtn32
        '
        Me.objbtn32.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn32.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn32.Location = New System.Drawing.Point(187, 83)
        Me.objbtn32.Name = "objbtn32"
        Me.objbtn32.Size = New System.Drawing.Size(20, 20)
        Me.objbtn32.TabIndex = 59
        Me.objbtn32.UseVisualStyleBackColor = False
        '
        'objbtn33
        '
        Me.objbtn33.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn33.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn33.Location = New System.Drawing.Point(5, 109)
        Me.objbtn33.Name = "objbtn33"
        Me.objbtn33.Size = New System.Drawing.Size(20, 20)
        Me.objbtn33.TabIndex = 60
        Me.objbtn33.UseVisualStyleBackColor = False
        '
        'objbtn45
        '
        Me.objbtn45.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn45.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn45.Location = New System.Drawing.Point(109, 135)
        Me.objbtn45.Name = "objbtn45"
        Me.objbtn45.Size = New System.Drawing.Size(20, 20)
        Me.objbtn45.TabIndex = 73
        Me.objbtn45.UseVisualStyleBackColor = False
        '
        'objbtn19
        '
        Me.objbtn19.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn19.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn19.Location = New System.Drawing.Point(57, 57)
        Me.objbtn19.Name = "objbtn19"
        Me.objbtn19.Size = New System.Drawing.Size(20, 20)
        Me.objbtn19.TabIndex = 38
        Me.objbtn19.UseVisualStyleBackColor = False
        '
        'objbtn2
        '
        Me.objbtn2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn2.Location = New System.Drawing.Point(31, 5)
        Me.objbtn2.Name = "objbtn2"
        Me.objbtn2.Size = New System.Drawing.Size(20, 20)
        Me.objbtn2.TabIndex = 36
        Me.objbtn2.UseVisualStyleBackColor = False
        '
        'objbtn44
        '
        Me.objbtn44.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn44.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn44.Location = New System.Drawing.Point(83, 135)
        Me.objbtn44.Name = "objbtn44"
        Me.objbtn44.Size = New System.Drawing.Size(20, 20)
        Me.objbtn44.TabIndex = 72
        Me.objbtn44.UseVisualStyleBackColor = False
        '
        'objbtn31
        '
        Me.objbtn31.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn31.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn31.Location = New System.Drawing.Point(161, 83)
        Me.objbtn31.Name = "objbtn31"
        Me.objbtn31.Size = New System.Drawing.Size(20, 20)
        Me.objbtn31.TabIndex = 58
        Me.objbtn31.UseVisualStyleBackColor = False
        '
        'objbtn42
        '
        Me.objbtn42.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn42.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn42.Location = New System.Drawing.Point(31, 135)
        Me.objbtn42.Name = "objbtn42"
        Me.objbtn42.Size = New System.Drawing.Size(20, 20)
        Me.objbtn42.TabIndex = 70
        Me.objbtn42.UseVisualStyleBackColor = False
        '
        'objbtn20
        '
        Me.objbtn20.BackColor = System.Drawing.Color.Yellow
        Me.objbtn20.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn20.Location = New System.Drawing.Point(83, 57)
        Me.objbtn20.Name = "objbtn20"
        Me.objbtn20.Size = New System.Drawing.Size(20, 20)
        Me.objbtn20.TabIndex = 39
        Me.objbtn20.UseVisualStyleBackColor = False
        '
        'objbtn43
        '
        Me.objbtn43.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn43.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn43.Location = New System.Drawing.Point(57, 135)
        Me.objbtn43.Name = "objbtn43"
        Me.objbtn43.Size = New System.Drawing.Size(20, 20)
        Me.objbtn43.TabIndex = 71
        Me.objbtn43.UseVisualStyleBackColor = False
        '
        'objbtn3
        '
        Me.objbtn3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn3.Location = New System.Drawing.Point(57, 5)
        Me.objbtn3.Name = "objbtn3"
        Me.objbtn3.Size = New System.Drawing.Size(20, 20)
        Me.objbtn3.TabIndex = 35
        Me.objbtn3.UseVisualStyleBackColor = False
        '
        'objbtn15
        '
        Me.objbtn15.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn15.Location = New System.Drawing.Point(161, 31)
        Me.objbtn15.Name = "objbtn15"
        Me.objbtn15.Size = New System.Drawing.Size(20, 20)
        Me.objbtn15.TabIndex = 48
        Me.objbtn15.UseVisualStyleBackColor = False
        '
        'objbtn26
        '
        Me.objbtn26.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn26.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn26.Location = New System.Drawing.Point(31, 83)
        Me.objbtn26.Name = "objbtn26"
        Me.objbtn26.Size = New System.Drawing.Size(20, 20)
        Me.objbtn26.TabIndex = 57
        Me.objbtn26.UseVisualStyleBackColor = False
        '
        'objbtn16
        '
        Me.objbtn16.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn16.Location = New System.Drawing.Point(187, 31)
        Me.objbtn16.Name = "objbtn16"
        Me.objbtn16.Size = New System.Drawing.Size(20, 20)
        Me.objbtn16.TabIndex = 49
        Me.objbtn16.UseVisualStyleBackColor = False
        '
        'objbtn35
        '
        Me.objbtn35.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn35.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn35.Location = New System.Drawing.Point(57, 109)
        Me.objbtn35.Name = "objbtn35"
        Me.objbtn35.Size = New System.Drawing.Size(20, 20)
        Me.objbtn35.TabIndex = 62
        Me.objbtn35.UseVisualStyleBackColor = False
        '
        'objbtn41
        '
        Me.objbtn41.BackColor = System.Drawing.Color.Black
        Me.objbtn41.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn41.Location = New System.Drawing.Point(5, 135)
        Me.objbtn41.Name = "objbtn41"
        Me.objbtn41.Size = New System.Drawing.Size(20, 20)
        Me.objbtn41.TabIndex = 69
        Me.objbtn41.UseVisualStyleBackColor = False
        '
        'objbtn21
        '
        Me.objbtn21.BackColor = System.Drawing.Color.Lime
        Me.objbtn21.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn21.Location = New System.Drawing.Point(109, 57)
        Me.objbtn21.Name = "objbtn21"
        Me.objbtn21.Size = New System.Drawing.Size(20, 20)
        Me.objbtn21.TabIndex = 40
        Me.objbtn21.UseVisualStyleBackColor = False
        '
        'objbtn8
        '
        Me.objbtn8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn8.Location = New System.Drawing.Point(187, 5)
        Me.objbtn8.Name = "objbtn8"
        Me.objbtn8.Size = New System.Drawing.Size(20, 20)
        Me.objbtn8.TabIndex = 47
        Me.objbtn8.UseVisualStyleBackColor = False
        '
        'objbtn4
        '
        Me.objbtn4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn4.Location = New System.Drawing.Point(83, 5)
        Me.objbtn4.Name = "objbtn4"
        Me.objbtn4.Size = New System.Drawing.Size(20, 20)
        Me.objbtn4.TabIndex = 34
        Me.objbtn4.UseVisualStyleBackColor = False
        '
        'objbtn23
        '
        Me.objbtn23.BackColor = System.Drawing.Color.Blue
        Me.objbtn23.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn23.Location = New System.Drawing.Point(161, 57)
        Me.objbtn23.Name = "objbtn23"
        Me.objbtn23.Size = New System.Drawing.Size(20, 20)
        Me.objbtn23.TabIndex = 50
        Me.objbtn23.UseVisualStyleBackColor = False
        '
        'objbtn27
        '
        Me.objbtn27.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn27.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn27.Location = New System.Drawing.Point(57, 83)
        Me.objbtn27.Name = "objbtn27"
        Me.objbtn27.Size = New System.Drawing.Size(20, 20)
        Me.objbtn27.TabIndex = 56
        Me.objbtn27.UseVisualStyleBackColor = False
        '
        'objbtn1
        '
        Me.objbtn1.BackColor = System.Drawing.Color.White
        Me.objbtn1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn1.Location = New System.Drawing.Point(5, 5)
        Me.objbtn1.Name = "objbtn1"
        Me.objbtn1.Size = New System.Drawing.Size(20, 20)
        Me.objbtn1.TabIndex = 28
        Me.objbtn1.UseVisualStyleBackColor = False
        '
        'objbtn36
        '
        Me.objbtn36.BackColor = System.Drawing.Color.Olive
        Me.objbtn36.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn36.Location = New System.Drawing.Point(83, 109)
        Me.objbtn36.Name = "objbtn36"
        Me.objbtn36.Size = New System.Drawing.Size(20, 20)
        Me.objbtn36.TabIndex = 63
        Me.objbtn36.UseVisualStyleBackColor = False
        '
        'objbtn7
        '
        Me.objbtn7.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn7.Location = New System.Drawing.Point(161, 5)
        Me.objbtn7.Name = "objbtn7"
        Me.objbtn7.Size = New System.Drawing.Size(20, 20)
        Me.objbtn7.TabIndex = 46
        Me.objbtn7.UseVisualStyleBackColor = False
        '
        'objbtn22
        '
        Me.objbtn22.BackColor = System.Drawing.Color.Cyan
        Me.objbtn22.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn22.Location = New System.Drawing.Point(135, 57)
        Me.objbtn22.Name = "objbtn22"
        Me.objbtn22.Size = New System.Drawing.Size(20, 20)
        Me.objbtn22.TabIndex = 41
        Me.objbtn22.UseVisualStyleBackColor = False
        '
        'objbtn24
        '
        Me.objbtn24.BackColor = System.Drawing.Color.Fuchsia
        Me.objbtn24.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn24.Location = New System.Drawing.Point(187, 57)
        Me.objbtn24.Name = "objbtn24"
        Me.objbtn24.Size = New System.Drawing.Size(20, 20)
        Me.objbtn24.TabIndex = 51
        Me.objbtn24.UseVisualStyleBackColor = False
        '
        'objbtn5
        '
        Me.objbtn5.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn5.Location = New System.Drawing.Point(109, 5)
        Me.objbtn5.Name = "objbtn5"
        Me.objbtn5.Size = New System.Drawing.Size(20, 20)
        Me.objbtn5.TabIndex = 33
        Me.objbtn5.UseVisualStyleBackColor = False
        '
        'objbtn12
        '
        Me.objbtn12.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn12.Location = New System.Drawing.Point(83, 31)
        Me.objbtn12.Name = "objbtn12"
        Me.objbtn12.Size = New System.Drawing.Size(20, 20)
        Me.objbtn12.TabIndex = 29
        Me.objbtn12.UseVisualStyleBackColor = False
        '
        'objbtn28
        '
        Me.objbtn28.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn28.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn28.Location = New System.Drawing.Point(83, 83)
        Me.objbtn28.Name = "objbtn28"
        Me.objbtn28.Size = New System.Drawing.Size(20, 20)
        Me.objbtn28.TabIndex = 55
        Me.objbtn28.UseVisualStyleBackColor = False
        '
        'objbtn17
        '
        Me.objbtn17.BackColor = System.Drawing.Color.Silver
        Me.objbtn17.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn17.Location = New System.Drawing.Point(5, 57)
        Me.objbtn17.Name = "objbtn17"
        Me.objbtn17.Size = New System.Drawing.Size(20, 20)
        Me.objbtn17.TabIndex = 45
        Me.objbtn17.UseVisualStyleBackColor = False
        '
        'objbtn37
        '
        Me.objbtn37.BackColor = System.Drawing.Color.Green
        Me.objbtn37.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn37.Location = New System.Drawing.Point(109, 109)
        Me.objbtn37.Name = "objbtn37"
        Me.objbtn37.Size = New System.Drawing.Size(20, 20)
        Me.objbtn37.TabIndex = 64
        Me.objbtn37.UseVisualStyleBackColor = False
        '
        'objbtn40
        '
        Me.objbtn40.BackColor = System.Drawing.Color.Purple
        Me.objbtn40.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn40.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn40.Location = New System.Drawing.Point(187, 109)
        Me.objbtn40.Name = "objbtn40"
        Me.objbtn40.Size = New System.Drawing.Size(20, 20)
        Me.objbtn40.TabIndex = 67
        Me.objbtn40.UseVisualStyleBackColor = False
        '
        'objbtn9
        '
        Me.objbtn9.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.objbtn9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn9.Location = New System.Drawing.Point(5, 31)
        Me.objbtn9.Name = "objbtn9"
        Me.objbtn9.Size = New System.Drawing.Size(20, 20)
        Me.objbtn9.TabIndex = 42
        Me.objbtn9.UseVisualStyleBackColor = False
        '
        'objbtn25
        '
        Me.objbtn25.BackColor = System.Drawing.Color.Gray
        Me.objbtn25.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn25.Location = New System.Drawing.Point(5, 83)
        Me.objbtn25.Name = "objbtn25"
        Me.objbtn25.Size = New System.Drawing.Size(20, 20)
        Me.objbtn25.TabIndex = 52
        Me.objbtn25.UseVisualStyleBackColor = False
        '
        'objbtn6
        '
        Me.objbtn6.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn6.Location = New System.Drawing.Point(135, 5)
        Me.objbtn6.Name = "objbtn6"
        Me.objbtn6.Size = New System.Drawing.Size(20, 20)
        Me.objbtn6.TabIndex = 32
        Me.objbtn6.UseVisualStyleBackColor = False
        '
        'objbtn13
        '
        Me.objbtn13.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn13.Location = New System.Drawing.Point(109, 31)
        Me.objbtn13.Name = "objbtn13"
        Me.objbtn13.Size = New System.Drawing.Size(20, 20)
        Me.objbtn13.TabIndex = 30
        Me.objbtn13.UseVisualStyleBackColor = False
        '
        'objbtn29
        '
        Me.objbtn29.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn29.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn29.Location = New System.Drawing.Point(109, 83)
        Me.objbtn29.Name = "objbtn29"
        Me.objbtn29.Size = New System.Drawing.Size(20, 20)
        Me.objbtn29.TabIndex = 54
        Me.objbtn29.UseVisualStyleBackColor = False
        '
        'objbtn11
        '
        Me.objbtn11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn11.Location = New System.Drawing.Point(57, 31)
        Me.objbtn11.Name = "objbtn11"
        Me.objbtn11.Size = New System.Drawing.Size(20, 20)
        Me.objbtn11.TabIndex = 44
        Me.objbtn11.UseVisualStyleBackColor = False
        '
        'objbtn38
        '
        Me.objbtn38.BackColor = System.Drawing.Color.Teal
        Me.objbtn38.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn38.Location = New System.Drawing.Point(135, 109)
        Me.objbtn38.Name = "objbtn38"
        Me.objbtn38.Size = New System.Drawing.Size(20, 20)
        Me.objbtn38.TabIndex = 65
        Me.objbtn38.UseVisualStyleBackColor = False
        '
        'objbtn39
        '
        Me.objbtn39.BackColor = System.Drawing.Color.Navy
        Me.objbtn39.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn39.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn39.Location = New System.Drawing.Point(161, 109)
        Me.objbtn39.Name = "objbtn39"
        Me.objbtn39.Size = New System.Drawing.Size(20, 20)
        Me.objbtn39.TabIndex = 66
        Me.objbtn39.UseVisualStyleBackColor = False
        '
        'objbtn10
        '
        Me.objbtn10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn10.Location = New System.Drawing.Point(31, 31)
        Me.objbtn10.Name = "objbtn10"
        Me.objbtn10.Size = New System.Drawing.Size(20, 20)
        Me.objbtn10.TabIndex = 43
        Me.objbtn10.UseVisualStyleBackColor = False
        '
        'objbtn30
        '
        Me.objbtn30.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn30.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn30.Location = New System.Drawing.Point(135, 83)
        Me.objbtn30.Name = "objbtn30"
        Me.objbtn30.Size = New System.Drawing.Size(20, 20)
        Me.objbtn30.TabIndex = 53
        Me.objbtn30.UseVisualStyleBackColor = False
        '
        'objbtn14
        '
        Me.objbtn14.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn14.Location = New System.Drawing.Point(135, 31)
        Me.objbtn14.Name = "objbtn14"
        Me.objbtn14.Size = New System.Drawing.Size(20, 20)
        Me.objbtn14.TabIndex = 31
        Me.objbtn14.UseVisualStyleBackColor = False
        '
        'objlinef2
        '
        Me.objlinef2.Dock = System.Windows.Forms.DockStyle.Top
        Me.objlinef2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlinef2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objlinef2.Location = New System.Drawing.Point(0, 0)
        Me.objlinef2.Name = "objlinef2"
        Me.objlinef2.Size = New System.Drawing.Size(866, 1)
        Me.objlinef2.TabIndex = 132
        Me.objlinef2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(767, 15)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 37
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn1.HeaderText = "Title"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 300
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn2.HeaderText = "Announcement"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "MarkAsClose"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "AnnouncementId"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'objdgcolhEdit
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle1.NullValue = Nothing
        Me.objdgcolhEdit.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Width = 30
        '
        'objdgcolhDelete
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle2.NullValue = Nothing
        Me.objdgcolhDelete.DefaultCellStyle = DataGridViewCellStyle2
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Width = 30
        '
        'dgcolhTitle
        '
        Me.dgcolhTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhTitle.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhTitle.HeaderText = "Title"
        Me.dgcolhTitle.Name = "dgcolhTitle"
        Me.dgcolhTitle.ReadOnly = True
        Me.dgcolhTitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhMarkAsClose
        '
        Me.objdgcolhMarkAsClose.HeaderText = "MarkAsClose"
        Me.objdgcolhMarkAsClose.Name = "objdgcolhMarkAsClose"
        Me.objdgcolhMarkAsClose.ReadOnly = True
        Me.objdgcolhMarkAsClose.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhMarkAsClose.Visible = False
        '
        'objdgcolhAnnouncementId
        '
        Me.objdgcolhAnnouncementId.HeaderText = "AnnouncementId"
        Me.objdgcolhAnnouncementId.Name = "objdgcolhAnnouncementId"
        Me.objdgcolhAnnouncementId.ReadOnly = True
        Me.objdgcolhAnnouncementId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhAnnouncementId.Visible = False
        '
        'frmPasswordOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(880, 567)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPasswordOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Options"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tbOptions.ResumeLayout(False)
        Me.tbpOtherOptions.ResumeLayout(False)
        Me.objSpcGOption.Panel1.ResumeLayout(False)
        Me.objSpcGOption.Panel2.ResumeLayout(False)
        Me.objSpcGOption.ResumeLayout(False)
        Me.gbPasswordHistorySettings.ResumeLayout(False)
        CType(Me.nudDaysPassword, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudNumberPassword, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbPassowordExpiration.ResumeLayout(False)
        CType(Me.nudMaxDays, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMinDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbPasswordPolicy.ResumeLayout(False)
        Me.gbPasswordLength.ResumeLayout(False)
        CType(Me.nudMinLength, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbUserLoginOptions.ResumeLayout(False)
        Me.gbUserLoginOptions.PerformLayout()
        Me.gbUnameLength.ResumeLayout(False)
        CType(Me.nudUMinLength, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbAccountLock.ResumeLayout(False)
        Me.gbAccountLock.PerformLayout()
        CType(Me.nudAttempts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbApplicationIdleSetting.ResumeLayout(False)
        Me.gbApplicationIdleSetting.PerformLayout()
        Me.tbpAnnouncements.ResumeLayout(False)
        Me.objspcAmmounce.Panel1.ResumeLayout(False)
        Me.objspcAmmounce.Panel1.PerformLayout()
        Me.objspcAmmounce.Panel2.ResumeLayout(False)
        Me.objspcAmmounce.ResumeLayout(False)
        CType(Me.dgvAnnouncements, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlsOperations.ResumeLayout(False)
        Me.tlsOperations.PerformLayout()
        Me.pnlColor.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents tbOptions As System.Windows.Forms.TabControl
    Friend WithEvents tbpOtherOptions As System.Windows.Forms.TabPage
    Friend WithEvents tbpAnnouncements As System.Windows.Forms.TabPage
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents objSpcGOption As System.Windows.Forms.SplitContainer
    Friend WithEvents gbPasswordHistorySettings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents nudDaysPassword As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkDonotAllowReuseOfPasswordUsedInTheLast As System.Windows.Forms.CheckBox
    Friend WithEvents lblDays As System.Windows.Forms.Label
    Friend WithEvents nudNumberPassword As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkDonotAllowToReuseAnyOfTheLast As System.Windows.Forms.CheckBox
    Friend WithEvents lblPasswordsUsed As System.Windows.Forms.Label
    Friend WithEvents gbPassowordExpiration As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents nudMaxDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMaximumDays As System.Windows.Forms.Label
    Friend WithEvents nudMinDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMinimumDays As System.Windows.Forms.Label
    Friend WithEvents gbPasswordPolicy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents LblPasswordComplexity As System.Windows.Forms.Label
    Friend WithEvents chkPasswordComplexity As System.Windows.Forms.CheckBox
    Friend WithEvents chkSpecialCharsMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents chkNumericMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents chkLowerCaseMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents chkUpperCaseMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents gbPasswordLength As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents nudMinLength As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMinimumLength As System.Windows.Forms.Label
    Friend WithEvents gbUserLoginOptions As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkAllowLoginOnActualEOCRetireDate As System.Windows.Forms.CheckBox
    Friend WithEvents EZeeLine2 As eZee.Common.eZeeLine
    Friend WithEvents chkUserChangePwdnextLogon As System.Windows.Forms.CheckBox
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents chkADUserFromEmpMst As System.Windows.Forms.CheckBox
    Friend WithEvents LblDomain As System.Windows.Forms.Label
    Friend WithEvents txtDomainName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents LblDomainPassword As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents txtDomainPassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkEnableAllUser As System.Windows.Forms.CheckBox
    Friend WithEvents LblDomainUser As System.Windows.Forms.Label
    Friend WithEvents chkProhibitPasswordChange As System.Windows.Forms.CheckBox
    Friend WithEvents txtDomainUser As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkImportEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents txtADServerIP As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblADSeverIP As System.Windows.Forms.Label
    Friend WithEvents radSSOAuthentication As System.Windows.Forms.RadioButton
    Friend WithEvents radADAuthentication As System.Windows.Forms.RadioButton
    Friend WithEvents radBasicAuthentication As System.Windows.Forms.RadioButton
    Friend WithEvents gbUnameLength As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents nudUMinLength As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblUMinLength As System.Windows.Forms.Label
    Friend WithEvents gbAccountLock As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents radAdminUnlock As System.Windows.Forms.RadioButton
    Friend WithEvents lblMinutes As System.Windows.Forms.Label
    Friend WithEvents txtRetrydurattion As eZee.TextBox.IntegerTextBox
    Friend WithEvents radLoginRetry As System.Windows.Forms.RadioButton
    Friend WithEvents lblAttempts As System.Windows.Forms.Label
    Friend WithEvents nudAttempts As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLockAttempts As System.Windows.Forms.Label
    Friend WithEvents gbApplicationIdleSetting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblIdleMinutes As System.Windows.Forms.Label
    Friend WithEvents txtIdleMinutes As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblAutoLogout As System.Windows.Forms.Label
    Friend WithEvents objspcAmmounce As System.Windows.Forms.SplitContainer
    Friend WithEvents dgvAnnouncements As System.Windows.Forms.DataGridView
    Friend WithEvents tlsOperations As System.Windows.Forms.ToolStrip
    Friend WithEvents objtlbbtnCut As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnCopy As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnPaste As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtoolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objcboFont As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents objcboFontSize As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents objToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnColor As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents objToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnBold As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnItalic As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnUnderline As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnLeftAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnCenterAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRightAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnUndo As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRedo As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnBulletSimple As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnLeftIndent As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRightIndent As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtAnnouncement As System.Windows.Forms.RichTextBox
    Friend WithEvents btnAttachment As eZee.Common.eZeeLightButton
    Friend WithEvents chkRequireAcknowledgement As System.Windows.Forms.CheckBox
    Friend WithEvents btnMarkAsClose As eZee.Common.eZeeLightButton
    Friend WithEvents LblAnnouncement As System.Windows.Forms.Label
    Friend WithEvents txtTitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents LblTitle As System.Windows.Forms.Label
    Friend WithEvents imgAnnouncement As eZee.Common.eZeeImageControl
    Friend WithEvents btnAnnoucementSave As eZee.Common.eZeeLightButton
    Friend WithEvents pnlColor As System.Windows.Forms.Panel
    Friend WithEvents objbtn48 As System.Windows.Forms.Button
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents btnMoreColor As System.Windows.Forms.Button
    Friend WithEvents objbtn47 As System.Windows.Forms.Button
    Friend WithEvents objbtn34 As System.Windows.Forms.Button
    Friend WithEvents objbtn18 As System.Windows.Forms.Button
    Friend WithEvents objbtn46 As System.Windows.Forms.Button
    Friend WithEvents objbtn32 As System.Windows.Forms.Button
    Friend WithEvents objbtn33 As System.Windows.Forms.Button
    Friend WithEvents objbtn45 As System.Windows.Forms.Button
    Friend WithEvents objbtn19 As System.Windows.Forms.Button
    Friend WithEvents objbtn2 As System.Windows.Forms.Button
    Friend WithEvents objbtn44 As System.Windows.Forms.Button
    Friend WithEvents objbtn31 As System.Windows.Forms.Button
    Friend WithEvents objbtn42 As System.Windows.Forms.Button
    Friend WithEvents objbtn20 As System.Windows.Forms.Button
    Friend WithEvents objbtn43 As System.Windows.Forms.Button
    Friend WithEvents objbtn3 As System.Windows.Forms.Button
    Friend WithEvents objbtn15 As System.Windows.Forms.Button
    Friend WithEvents objbtn26 As System.Windows.Forms.Button
    Friend WithEvents objbtn16 As System.Windows.Forms.Button
    Friend WithEvents objbtn35 As System.Windows.Forms.Button
    Friend WithEvents objbtn41 As System.Windows.Forms.Button
    Friend WithEvents objbtn21 As System.Windows.Forms.Button
    Friend WithEvents objbtn8 As System.Windows.Forms.Button
    Friend WithEvents objbtn4 As System.Windows.Forms.Button
    Friend WithEvents objbtn23 As System.Windows.Forms.Button
    Friend WithEvents objbtn27 As System.Windows.Forms.Button
    Friend WithEvents objbtn1 As System.Windows.Forms.Button
    Friend WithEvents objbtn36 As System.Windows.Forms.Button
    Friend WithEvents objbtn7 As System.Windows.Forms.Button
    Friend WithEvents objbtn22 As System.Windows.Forms.Button
    Friend WithEvents objbtn24 As System.Windows.Forms.Button
    Friend WithEvents objbtn5 As System.Windows.Forms.Button
    Friend WithEvents objbtn12 As System.Windows.Forms.Button
    Friend WithEvents objbtn28 As System.Windows.Forms.Button
    Friend WithEvents objbtn17 As System.Windows.Forms.Button
    Friend WithEvents objbtn37 As System.Windows.Forms.Button
    Friend WithEvents objbtn40 As System.Windows.Forms.Button
    Friend WithEvents objbtn9 As System.Windows.Forms.Button
    Friend WithEvents objbtn25 As System.Windows.Forms.Button
    Friend WithEvents objbtn6 As System.Windows.Forms.Button
    Friend WithEvents objbtn13 As System.Windows.Forms.Button
    Friend WithEvents objbtn29 As System.Windows.Forms.Button
    Friend WithEvents objbtn11 As System.Windows.Forms.Button
    Friend WithEvents objbtn38 As System.Windows.Forms.Button
    Friend WithEvents objbtn39 As System.Windows.Forms.Button
    Friend WithEvents objbtn10 As System.Windows.Forms.Button
    Friend WithEvents objbtn30 As System.Windows.Forms.Button
    Friend WithEvents objbtn14 As System.Windows.Forms.Button
    Friend WithEvents objlinef1 As eZee.Common.eZeeLine
    Friend WithEvents objlinef2 As eZee.Common.eZeeLine
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMarkAsClose As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAnnouncementId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
