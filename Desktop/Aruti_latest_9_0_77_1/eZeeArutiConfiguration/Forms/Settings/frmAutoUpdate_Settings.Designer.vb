﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAutoUpdate_Settings
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutoUpdate_Settings))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbSettings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radAutoUpdate = New System.Windows.Forms.RadioButton
        Me.radManually = New System.Windows.Forms.RadioButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbSettings.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbSettings)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(326, 158)
        Me.pnlMain.TabIndex = 0
        '
        'gbSettings
        '
        Me.gbSettings.BorderColor = System.Drawing.Color.Black
        Me.gbSettings.Checked = False
        Me.gbSettings.CollapseAllExceptThis = False
        Me.gbSettings.CollapsedHoverImage = Nothing
        Me.gbSettings.CollapsedNormalImage = Nothing
        Me.gbSettings.CollapsedPressedImage = Nothing
        Me.gbSettings.CollapseOnLoad = False
        Me.gbSettings.Controls.Add(Me.radAutoUpdate)
        Me.gbSettings.Controls.Add(Me.radManually)
        Me.gbSettings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbSettings.ExpandedHoverImage = Nothing
        Me.gbSettings.ExpandedNormalImage = Nothing
        Me.gbSettings.ExpandedPressedImage = Nothing
        Me.gbSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSettings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSettings.HeaderHeight = 25
        Me.gbSettings.HeaderMessage = ""
        Me.gbSettings.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbSettings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSettings.HeightOnCollapse = 0
        Me.gbSettings.LeftTextSpace = 0
        Me.gbSettings.Location = New System.Drawing.Point(0, 0)
        Me.gbSettings.Name = "gbSettings"
        Me.gbSettings.OpenHeight = 300
        Me.gbSettings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSettings.ShowBorder = True
        Me.gbSettings.ShowCheckBox = False
        Me.gbSettings.ShowCollapseButton = False
        Me.gbSettings.ShowDefaultBorderColor = True
        Me.gbSettings.ShowDownButton = False
        Me.gbSettings.ShowHeader = True
        Me.gbSettings.Size = New System.Drawing.Size(326, 103)
        Me.gbSettings.TabIndex = 12
        Me.gbSettings.Temp = 0
        Me.gbSettings.Text = "Settings"
        Me.gbSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radAutoUpdate
        '
        Me.radAutoUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAutoUpdate.Location = New System.Drawing.Point(12, 38)
        Me.radAutoUpdate.Name = "radAutoUpdate"
        Me.radAutoUpdate.Size = New System.Drawing.Size(303, 17)
        Me.radAutoUpdate.TabIndex = 1
        Me.radAutoUpdate.TabStop = True
        Me.radAutoUpdate.Text = "Server to Get update from Internet"
        Me.radAutoUpdate.UseVisualStyleBackColor = True
        '
        'radManually
        '
        Me.radManually.Checked = True
        Me.radManually.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radManually.Location = New System.Drawing.Point(12, 64)
        Me.radManually.Name = "radManually"
        Me.radManually.Size = New System.Drawing.Size(303, 17)
        Me.radManually.TabIndex = 1
        Me.radManually.TabStop = True
        Me.radManually.Text = "Server to be Updated Manually"
        Me.radManually.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 103)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(326, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(114, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(217, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmAutoUpdate_Settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 158)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAutoUpdate_Settings"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Auto-Update Settings"
        Me.pnlMain.ResumeLayout(False)
        Me.gbSettings.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbSettings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radAutoUpdate As System.Windows.Forms.RadioButton
    Friend WithEvents radManually As System.Windows.Forms.RadioButton
End Class
