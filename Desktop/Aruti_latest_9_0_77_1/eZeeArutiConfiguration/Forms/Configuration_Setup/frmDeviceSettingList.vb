﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System

Public Class frmDeviceSettingList

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmDeviceSettingList"
    Dim dsSetting As DataSet = Nothing
    Dim objAppSetting As clsApplicationSettings

#End Region

#Region "Form's Event"

    Private Sub frmDeviceSettingList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End


            objAppSetting = New clsApplicationSettings
            fillList()

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 29 OCT 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDeviceSettingList_Load", mstrModuleName)
        End Try
    End Sub


    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- End

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Try

            If IO.File.Exists(objAppSetting._ApplicationPath & "DeviceSetting.xml") Then
                dsSetting = New DataSet
                dsSetting.ReadXml(objAppSetting._ApplicationPath & "DeviceSetting.xml")
            End If

            If dsSetting IsNot Nothing AndAlso dsSetting.Tables.Count = 0 Then
                lvDeviceSetting.Items.Clear()
                Exit Sub
            End If

            If dsSetting IsNot Nothing AndAlso dsSetting.Tables(0).Rows.Count > 0 Then


                'Pinkal (15-Nov-2013) -- Start
                'Enhancement : Oman Changes
                Dim dtTable As DataTable = New DataView(dsSetting.Tables(0), "", "companyunkid,devicecode asc", DataViewRowState.CurrentRows).ToTable
                'Pinkal (15-Nov-2013) -- End

                Dim lvItem As ListViewItem

                lvDeviceSetting.Items.Clear()
                Dim objCompany As New clsCompany_Master


                'Pinkal (04-Oct-2017) -- Start
                'Enhancement - Working SAGEM Database Device Integration.
                Dim objConfig As New clsConfigOptions
                'Pinkal (04-Oct-2017) -- End


                'Pinkal (15-Nov-2013) -- Start
                'Enhancement : Oman Changes
                'For Each drRow As DataRow In dsSetting.Tables(0).Rows
                For Each drRow As DataRow In dtTable.Rows
                    'Pinkal (15-Nov-2013) -- End

                    lvItem = New ListViewItem
                    lvItem.Tag = drRow("deviceunkid")

                    'Pinkal (04-Oct-2017) -- Start
                    'Enhancement - Working SAGEM Database Device Integration.
                    objConfig._Companyunkid = CInt(drRow("companyunkid").ToString())
                    'Pinkal (04-Oct-2017) -- End

                    objCompany._Companyunkid = CInt(drRow("companyunkid").ToString)
                    lvItem.Text = objCompany._Name
                    lvItem.SubItems.Add(drRow("devicecode").ToString().Trim.Remove(drRow("devicecode").ToString().IndexOf("||"), drRow("devicecode").ToString().Trim.Length - drRow("devicecode").ToString().IndexOf("||")))
                    lvItem.SubItems.Add(drRow("commType").ToString)
                    lvItem.SubItems.Add(drRow("commDevice").ToString)
                    lvItem.SubItems.Add(drRow("connectiontype").ToString)

                    'Pinkal (04-Oct-2017) -- Start
                    'Enhancement - Working SAGEM Database Device Integration.
                    If IsDBNull(drRow("ip")) OrElse drRow("ip").ToString() <> "" Then
                    lvItem.SubItems.Add(drRow("ip").ToString)
                    ElseIf CInt(drRow("commdeviceid").ToString()) = enFingerPrintDevice.SAGEM Then
                        lvItem.SubItems.Add(ConfigParameter._Object._SagemDataServerAddress)
                    End If
                    If IsDBNull(drRow("port")) OrElse drRow("port").ToString() <> "" Then
                    lvItem.SubItems.Add(drRow("port").ToString)
                    ElseIf CInt(drRow("commdeviceid").ToString()) = enFingerPrintDevice.SAGEM Then
                        lvItem.SubItems.Add("")
                    End If

                    'Pinkal (04-Oct-2017) -- End

                    lvItem.SubItems.Add(drRow("baudrate").ToString)
                    lvItem.SubItems.Add(drRow("machinesrno").ToString)
                    lvItem.SubItems.Add(objCompany._Companyunkid.ToString())
                    lvDeviceSetting.Items.Add(lvItem)
                Next

            End If
            lvDeviceSetting.GridLines = False
            lvDeviceSetting.GroupingColumn = colhCompany
            lvDeviceSetting.DisplayGroups(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        End Try
    End Sub


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub DeleteDevice(ByVal deviceunkid As Integer, ByVal companyunkid As Integer)
        Try
            Dim drRow As DataRow() = dsSetting.Tables(0).Select("deviceunkid= " & deviceunkid & " AND companyunkid = " & companyunkid)
            If drRow.Length > 0 Then
                If IO.File.Exists(objAppSetting._ApplicationPath & "DeviceSetting.xml") Then
                    Dim fl As New IO.FileInfo(objAppSetting._ApplicationPath & "DeviceSetting.xml")
                    fl.Attributes = IO.FileAttributes.Archive
                    drRow(0).Delete()
                    dsSetting.AcceptChanges()
                    dsSetting.WriteXml(objAppSetting._ApplicationPath & "DeviceSetting.xml")

                    If IO.File.Exists(objAppSetting._ApplicationPath & "DeviceSetting.xml") Then
                        fl = New IO.FileInfo(objAppSetting._ApplicationPath & "DeviceSetting.xml")
                        fl.Attributes = IO.FileAttributes.ReadOnly
                        fl.Attributes = IO.FileAttributes.Hidden
                    End If

                    fillList()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DeleteDevice", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 29 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddDeviceSettings
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditDeviceSettings
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteDeviceSettings
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 29 OCT 2012 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmDeviceSettings
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvDeviceSetting.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select device from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvDeviceSetting.Select()
            Exit Sub
        End If
        Dim objfrmDevicesetting As New frmDeviceSettings
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvDeviceSetting.SelectedItems(0).Index
            If objfrmDevicesetting.displayDialog(CInt(lvDeviceSetting.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            objfrmDevicesetting = Nothing

            lvDeviceSetting.Items(intSelectedIndex).Selected = True
            lvDeviceSetting.EnsureVisible(intSelectedIndex)
            lvDeviceSetting.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvDeviceSetting.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select device from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvDeviceSetting.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvDeviceSetting.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this device?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then


                'Pinkal (04-Oct-2017) -- Start
                'Enhancement - Working SAGEM Database Device Integration.
                Dim objConfig As New clsConfigOptions

                'Pinkal (06-May-2016) -- Start
                'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                If lvDeviceSetting.SelectedItems(0).SubItems(colhCommDevice.Index).Text = enFingerPrintDevice.Handpunch.ToString() Then
                    If objConfig.UpdateHandpunchDeviceStatus(lvDeviceSetting.SelectedItems(0).SubItems(colhDeviceCode.Index).Text.Trim) = False Then
                        Exit Sub
                    End If
                ElseIf lvDeviceSetting.SelectedItems(0).SubItems(colhCommDevice.Index).Text.ToUpper() = enFingerPrintDevice.SAGEM.ToString() Then
                    objConfig._Companyunkid = CInt(lvDeviceSetting.SelectedItems(0).SubItems(objColhCompanyunkid.Index).Text)
                    objConfig._SagemDataServerAddress = ""
                    objConfig._SagemDatabaseName = ""
                    objConfig._SagemDatabaseUserName = ""
                    objConfig._SagemDatabasePassword = ""
                    objConfig.updateParam()
                End If
                'Pinkal (06-May-2016) -- End
                objConfig = Nothing
                'Pinkal (04-Oct-2017) -- End


                DeleteDevice(CInt(lvDeviceSetting.SelectedItems(0).Tag), CInt(lvDeviceSetting.SelectedItems(0).SubItems(objColhCompanyunkid.Index).Text))

                If lvDeviceSetting.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvDeviceSetting.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvDeviceSetting.Items.Count - 1
                    lvDeviceSetting.Items(intSelectedIndex).Selected = True
                    lvDeviceSetting.EnsureVisible(intSelectedIndex)
                ElseIf lvDeviceSetting.Items.Count <> 0 Then
                    lvDeviceSetting.Items(intSelectedIndex).Selected = True
                    lvDeviceSetting.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvDeviceSetting.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
            Me.colhCommType.Text = Language._Object.getCaption(CStr(Me.colhCommType.Tag), Me.colhCommType.Text)
            Me.colhConnectionType.Text = Language._Object.getCaption(CStr(Me.colhConnectionType.Tag), Me.colhConnectionType.Text)
            Me.colhCommDevice.Text = Language._Object.getCaption(CStr(Me.colhCommDevice.Tag), Me.colhCommDevice.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhIp.Text = Language._Object.getCaption(CStr(Me.colhIp.Tag), Me.colhIp.Text)
            Me.colhPort.Text = Language._Object.getCaption(CStr(Me.colhPort.Tag), Me.colhPort.Text)
            Me.colhBaudrate.Text = Language._Object.getCaption(CStr(Me.colhBaudrate.Tag), Me.colhBaudrate.Text)
            Me.colhMachineSrNo.Text = Language._Object.getCaption(CStr(Me.colhMachineSrNo.Tag), Me.colhMachineSrNo.Text)
            Me.colhDeviceCode.Text = Language._Object.getCaption(CStr(Me.colhDeviceCode.Tag), Me.colhDeviceCode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select device from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this device?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class