﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeviceSettings
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDeviceSettings))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnTestConnection = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.lblSelectCompany = New System.Windows.Forms.Label
        Me.objbtnSearchCompany = New eZee.Common.eZeeGradientButton
        Me.lblComProduct = New System.Windows.Forms.Label
        Me.cboComDevice = New System.Windows.Forms.ComboBox
        Me.lblConnectionType = New System.Windows.Forms.Label
        Me.cboConnectionType = New System.Windows.Forms.ComboBox
        Me.lblCommType = New System.Windows.Forms.Label
        Me.cboCommunicationType = New System.Windows.Forms.ComboBox
        Me.lblIP = New System.Windows.Forms.Label
        Me.txtPort = New System.Windows.Forms.TextBox
        Me.lblPort = New System.Windows.Forms.Label
        Me.txtIp = New System.Windows.Forms.TextBox
        Me.cboPort = New System.Windows.Forms.ComboBox
        Me.lblBaudrate = New System.Windows.Forms.Label
        Me.cboBaudRate = New System.Windows.Forms.ComboBox
        Me.lblRsMachineSN = New System.Windows.Forms.Label
        Me.txtMachineSN = New System.Windows.Forms.TextBox
        Me.LblUserID = New System.Windows.Forms.Label
        Me.lblDeviceCode = New System.Windows.Forms.Label
        Me.txtDeviceCode = New System.Windows.Forms.TextBox
        Me.LblPassword = New System.Windows.Forms.Label
        Me.lblCommunicationKey = New System.Windows.Forms.Label
        Me.txtUserID = New eZee.TextBox.AlphanumericTextBox
        Me.txtCommunicationKey = New eZee.TextBox.NumericTextBox
        Me.txtPassword = New eZee.TextBox.AlphanumericTextBox
        Me.gbCompanyInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtDeviceModel = New System.Windows.Forms.TextBox
        Me.lblDeviceModel = New System.Windows.Forms.Label
        Me.lnkConfigureSagem = New System.Windows.Forms.LinkLabel
        Me.lnkHandpunchConfig = New System.Windows.Forms.LinkLabel
        Me.EZeeFooter1.SuspendLayout()
        Me.gbCompanyInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnTestConnection)
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 316)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(508, 55)
        Me.EZeeFooter1.TabIndex = 17
        '
        'btnTestConnection
        '
        Me.btnTestConnection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTestConnection.BackColor = System.Drawing.Color.White
        Me.btnTestConnection.BackgroundImage = CType(resources.GetObject("btnTestConnection.BackgroundImage"), System.Drawing.Image)
        Me.btnTestConnection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTestConnection.BorderColor = System.Drawing.Color.Empty
        Me.btnTestConnection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnTestConnection.FlatAppearance.BorderSize = 0
        Me.btnTestConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTestConnection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTestConnection.ForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTestConnection.GradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestConnection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.Location = New System.Drawing.Point(7, 13)
        Me.btnTestConnection.Name = "btnTestConnection"
        Me.btnTestConnection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestConnection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.Size = New System.Drawing.Size(113, 30)
        Me.btnTestConnection.TabIndex = 0
        Me.btnTestConnection.Text = "&Test Connection"
        Me.btnTestConnection.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(410, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(314, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(152, 32)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(307, 21)
        Me.cboCompany.TabIndex = 1
        '
        'lblSelectCompany
        '
        Me.lblSelectCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectCompany.Location = New System.Drawing.Point(8, 34)
        Me.lblSelectCompany.Name = "lblSelectCompany"
        Me.lblSelectCompany.Size = New System.Drawing.Size(130, 17)
        Me.lblSelectCompany.TabIndex = 0
        Me.lblSelectCompany.Text = "Company"
        Me.lblSelectCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCompany
        '
        Me.objbtnSearchCompany.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCompany.BorderSelected = False
        Me.objbtnSearchCompany.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCompany.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCompany.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCompany.Location = New System.Drawing.Point(465, 34)
        Me.objbtnSearchCompany.Name = "objbtnSearchCompany"
        Me.objbtnSearchCompany.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCompany.TabIndex = 2
        '
        'lblComProduct
        '
        Me.lblComProduct.BackColor = System.Drawing.Color.Transparent
        Me.lblComProduct.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComProduct.ForeColor = System.Drawing.Color.Black
        Me.lblComProduct.Location = New System.Drawing.Point(8, 115)
        Me.lblComProduct.Name = "lblComProduct"
        Me.lblComProduct.Size = New System.Drawing.Size(130, 17)
        Me.lblComProduct.TabIndex = 7
        Me.lblComProduct.Text = "Communication Device"
        '
        'cboComDevice
        '
        Me.cboComDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboComDevice.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboComDevice.FormattingEnabled = True
        Me.cboComDevice.Location = New System.Drawing.Point(152, 113)
        Me.cboComDevice.Name = "cboComDevice"
        Me.cboComDevice.Size = New System.Drawing.Size(158, 21)
        Me.cboComDevice.TabIndex = 8
        '
        'lblConnectionType
        '
        Me.lblConnectionType.BackColor = System.Drawing.Color.Transparent
        Me.lblConnectionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConnectionType.ForeColor = System.Drawing.Color.Black
        Me.lblConnectionType.Location = New System.Drawing.Point(8, 142)
        Me.lblConnectionType.Name = "lblConnectionType"
        Me.lblConnectionType.Size = New System.Drawing.Size(130, 17)
        Me.lblConnectionType.TabIndex = 9
        Me.lblConnectionType.Text = "Connection Type"
        '
        'cboConnectionType
        '
        Me.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboConnectionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboConnectionType.FormattingEnabled = True
        Me.cboConnectionType.Location = New System.Drawing.Point(152, 140)
        Me.cboConnectionType.Name = "cboConnectionType"
        Me.cboConnectionType.Size = New System.Drawing.Size(158, 21)
        Me.cboConnectionType.TabIndex = 10
        '
        'lblCommType
        '
        Me.lblCommType.BackColor = System.Drawing.Color.Transparent
        Me.lblCommType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommType.ForeColor = System.Drawing.Color.Black
        Me.lblCommType.Location = New System.Drawing.Point(8, 87)
        Me.lblCommType.Name = "lblCommType"
        Me.lblCommType.Size = New System.Drawing.Size(130, 17)
        Me.lblCommType.TabIndex = 5
        Me.lblCommType.Text = "Communication Type"
        '
        'cboCommunicationType
        '
        Me.cboCommunicationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommunicationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCommunicationType.FormattingEnabled = True
        Me.cboCommunicationType.Location = New System.Drawing.Point(152, 85)
        Me.cboCommunicationType.Name = "cboCommunicationType"
        Me.cboCommunicationType.Size = New System.Drawing.Size(158, 21)
        Me.cboCommunicationType.TabIndex = 6
        '
        'lblIP
        '
        Me.lblIP.BackColor = System.Drawing.Color.Transparent
        Me.lblIP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIP.ForeColor = System.Drawing.Color.Black
        Me.lblIP.Location = New System.Drawing.Point(8, 169)
        Me.lblIP.Name = "lblIP"
        Me.lblIP.Size = New System.Drawing.Size(130, 17)
        Me.lblIP.TabIndex = 11
        Me.lblIP.Text = "IP"
        '
        'txtPort
        '
        Me.txtPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPort.Location = New System.Drawing.Point(152, 194)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(158, 21)
        Me.txtPort.TabIndex = 15
        Me.txtPort.Text = "4370"
        '
        'lblPort
        '
        Me.lblPort.BackColor = System.Drawing.Color.Transparent
        Me.lblPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPort.ForeColor = System.Drawing.Color.Black
        Me.lblPort.Location = New System.Drawing.Point(8, 196)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(130, 17)
        Me.lblPort.TabIndex = 14
        Me.lblPort.Text = "Port"
        '
        'txtIp
        '
        Me.txtIp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIp.Location = New System.Drawing.Point(152, 167)
        Me.txtIp.Name = "txtIp"
        Me.txtIp.Size = New System.Drawing.Size(158, 21)
        Me.txtIp.TabIndex = 12
        Me.txtIp.Text = "192.168.1.201"
        '
        'cboPort
        '
        Me.cboPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPort.FormattingEnabled = True
        Me.cboPort.Items.AddRange(New Object() {"COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9"})
        Me.cboPort.Location = New System.Drawing.Point(152, 167)
        Me.cboPort.Name = "cboPort"
        Me.cboPort.Size = New System.Drawing.Size(158, 21)
        Me.cboPort.TabIndex = 12
        '
        'lblBaudrate
        '
        Me.lblBaudrate.BackColor = System.Drawing.Color.Transparent
        Me.lblBaudrate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBaudrate.ForeColor = System.Drawing.Color.Black
        Me.lblBaudrate.Location = New System.Drawing.Point(8, 223)
        Me.lblBaudrate.Name = "lblBaudrate"
        Me.lblBaudrate.Size = New System.Drawing.Size(130, 17)
        Me.lblBaudrate.TabIndex = 18
        Me.lblBaudrate.Text = "BaudRate"
        '
        'cboBaudRate
        '
        Me.cboBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBaudRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBaudRate.FormattingEnabled = True
        Me.cboBaudRate.Items.AddRange(New Object() {"1200", "2400", "4800", "9600", "19200", "38400", "115200"})
        Me.cboBaudRate.Location = New System.Drawing.Point(152, 221)
        Me.cboBaudRate.Name = "cboBaudRate"
        Me.cboBaudRate.Size = New System.Drawing.Size(158, 21)
        Me.cboBaudRate.TabIndex = 19
        '
        'lblRsMachineSN
        '
        Me.lblRsMachineSN.BackColor = System.Drawing.Color.Transparent
        Me.lblRsMachineSN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRsMachineSN.ForeColor = System.Drawing.Color.Black
        Me.lblRsMachineSN.Location = New System.Drawing.Point(8, 250)
        Me.lblRsMachineSN.Name = "lblRsMachineSN"
        Me.lblRsMachineSN.Size = New System.Drawing.Size(130, 17)
        Me.lblRsMachineSN.TabIndex = 22
        Me.lblRsMachineSN.Text = "Device No"
        '
        'txtMachineSN
        '
        Me.txtMachineSN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMachineSN.Location = New System.Drawing.Point(152, 248)
        Me.txtMachineSN.Name = "txtMachineSN"
        Me.txtMachineSN.Size = New System.Drawing.Size(73, 21)
        Me.txtMachineSN.TabIndex = 23
        Me.txtMachineSN.Text = "1"
        '
        'LblUserID
        '
        Me.LblUserID.BackColor = System.Drawing.Color.Transparent
        Me.LblUserID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUserID.ForeColor = System.Drawing.Color.Black
        Me.LblUserID.Location = New System.Drawing.Point(313, 196)
        Me.LblUserID.Name = "LblUserID"
        Me.LblUserID.Size = New System.Drawing.Size(64, 17)
        Me.LblUserID.TabIndex = 16
        Me.LblUserID.Text = "User ID"
        '
        'lblDeviceCode
        '
        Me.lblDeviceCode.BackColor = System.Drawing.Color.Transparent
        Me.lblDeviceCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeviceCode.ForeColor = System.Drawing.Color.Black
        Me.lblDeviceCode.Location = New System.Drawing.Point(8, 61)
        Me.lblDeviceCode.Name = "lblDeviceCode"
        Me.lblDeviceCode.Size = New System.Drawing.Size(130, 17)
        Me.lblDeviceCode.TabIndex = 3
        Me.lblDeviceCode.Text = "Device Code"
        '
        'txtDeviceCode
        '
        Me.txtDeviceCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDeviceCode.Location = New System.Drawing.Point(152, 59)
        Me.txtDeviceCode.MaxLength = 7
        Me.txtDeviceCode.Name = "txtDeviceCode"
        Me.txtDeviceCode.Size = New System.Drawing.Size(73, 21)
        Me.txtDeviceCode.TabIndex = 4
        '
        'LblPassword
        '
        Me.LblPassword.BackColor = System.Drawing.Color.Transparent
        Me.LblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPassword.ForeColor = System.Drawing.Color.Black
        Me.LblPassword.Location = New System.Drawing.Point(313, 223)
        Me.LblPassword.Name = "LblPassword"
        Me.LblPassword.Size = New System.Drawing.Size(64, 17)
        Me.LblPassword.TabIndex = 20
        Me.LblPassword.Text = "Password"
        '
        'lblCommunicationKey
        '
        Me.lblCommunicationKey.BackColor = System.Drawing.Color.Transparent
        Me.lblCommunicationKey.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommunicationKey.ForeColor = System.Drawing.Color.Black
        Me.lblCommunicationKey.Location = New System.Drawing.Point(231, 250)
        Me.lblCommunicationKey.Name = "lblCommunicationKey"
        Me.lblCommunicationKey.Size = New System.Drawing.Size(110, 17)
        Me.lblCommunicationKey.TabIndex = 24
        Me.lblCommunicationKey.Text = "Communication Key"
        '
        'txtUserID
        '
        Me.txtUserID.Flags = 0
        Me.txtUserID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserID.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(35), Global.Microsoft.VisualBasic.ChrW(47), Global.Microsoft.VisualBasic.ChrW(46), Global.Microsoft.VisualBasic.ChrW(61), Global.Microsoft.VisualBasic.ChrW(96), Global.Microsoft.VisualBasic.ChrW(126)}
        Me.txtUserID.Location = New System.Drawing.Point(383, 194)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(94, 21)
        Me.txtUserID.TabIndex = 17
        '
        'txtCommunicationKey
        '
        Me.txtCommunicationKey.AllowNegative = False
        Me.txtCommunicationKey.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCommunicationKey.DigitsInGroup = 0
        Me.txtCommunicationKey.Flags = 65536
        Me.txtCommunicationKey.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCommunicationKey.Location = New System.Drawing.Point(347, 248)
        Me.txtCommunicationKey.MaxDecimalPlaces = 0
        Me.txtCommunicationKey.MaxWholeDigits = 6
        Me.txtCommunicationKey.Name = "txtCommunicationKey"
        Me.txtCommunicationKey.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtCommunicationKey.Prefix = ""
        Me.txtCommunicationKey.RangeMax = 1.7976931348623157E+308
        Me.txtCommunicationKey.RangeMin = -1.7976931348623157E+308
        Me.txtCommunicationKey.Size = New System.Drawing.Size(130, 21)
        Me.txtCommunicationKey.TabIndex = 25
        Me.txtCommunicationKey.Text = "0"
        '
        'txtPassword
        '
        Me.txtPassword.Flags = 0
        Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(35), Global.Microsoft.VisualBasic.ChrW(47), Global.Microsoft.VisualBasic.ChrW(46), Global.Microsoft.VisualBasic.ChrW(61), Global.Microsoft.VisualBasic.ChrW(96), Global.Microsoft.VisualBasic.ChrW(126)}
        Me.txtPassword.Location = New System.Drawing.Point(383, 221)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(94, 21)
        Me.txtPassword.TabIndex = 21
        Me.txtPassword.UseSystemPasswordChar = True
        '
        'gbCompanyInformation
        '
        Me.gbCompanyInformation.BorderColor = System.Drawing.Color.Black
        Me.gbCompanyInformation.Checked = False
        Me.gbCompanyInformation.CollapseAllExceptThis = False
        Me.gbCompanyInformation.CollapsedHoverImage = Nothing
        Me.gbCompanyInformation.CollapsedNormalImage = Nothing
        Me.gbCompanyInformation.CollapsedPressedImage = Nothing
        Me.gbCompanyInformation.CollapseOnLoad = False
        Me.gbCompanyInformation.Controls.Add(Me.txtDeviceModel)
        Me.gbCompanyInformation.Controls.Add(Me.lblDeviceModel)
        Me.gbCompanyInformation.Controls.Add(Me.lnkConfigureSagem)
        Me.gbCompanyInformation.Controls.Add(Me.lnkHandpunchConfig)
        Me.gbCompanyInformation.Controls.Add(Me.txtPassword)
        Me.gbCompanyInformation.Controls.Add(Me.txtCommunicationKey)
        Me.gbCompanyInformation.Controls.Add(Me.txtUserID)
        Me.gbCompanyInformation.Controls.Add(Me.lblCommunicationKey)
        Me.gbCompanyInformation.Controls.Add(Me.LblPassword)
        Me.gbCompanyInformation.Controls.Add(Me.txtDeviceCode)
        Me.gbCompanyInformation.Controls.Add(Me.lblDeviceCode)
        Me.gbCompanyInformation.Controls.Add(Me.LblUserID)
        Me.gbCompanyInformation.Controls.Add(Me.txtMachineSN)
        Me.gbCompanyInformation.Controls.Add(Me.lblRsMachineSN)
        Me.gbCompanyInformation.Controls.Add(Me.cboBaudRate)
        Me.gbCompanyInformation.Controls.Add(Me.lblBaudrate)
        Me.gbCompanyInformation.Controls.Add(Me.cboPort)
        Me.gbCompanyInformation.Controls.Add(Me.txtIp)
        Me.gbCompanyInformation.Controls.Add(Me.lblPort)
        Me.gbCompanyInformation.Controls.Add(Me.txtPort)
        Me.gbCompanyInformation.Controls.Add(Me.lblIP)
        Me.gbCompanyInformation.Controls.Add(Me.cboCommunicationType)
        Me.gbCompanyInformation.Controls.Add(Me.lblCommType)
        Me.gbCompanyInformation.Controls.Add(Me.cboConnectionType)
        Me.gbCompanyInformation.Controls.Add(Me.lblConnectionType)
        Me.gbCompanyInformation.Controls.Add(Me.cboComDevice)
        Me.gbCompanyInformation.Controls.Add(Me.lblComProduct)
        Me.gbCompanyInformation.Controls.Add(Me.objbtnSearchCompany)
        Me.gbCompanyInformation.Controls.Add(Me.lblSelectCompany)
        Me.gbCompanyInformation.Controls.Add(Me.cboCompany)
        Me.gbCompanyInformation.ExpandedHoverImage = Nothing
        Me.gbCompanyInformation.ExpandedNormalImage = Nothing
        Me.gbCompanyInformation.ExpandedPressedImage = Nothing
        Me.gbCompanyInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCompanyInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCompanyInformation.HeaderHeight = 25
        Me.gbCompanyInformation.HeaderMessage = ""
        Me.gbCompanyInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCompanyInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCompanyInformation.HeightOnCollapse = 0
        Me.gbCompanyInformation.LeftTextSpace = 0
        Me.gbCompanyInformation.Location = New System.Drawing.Point(7, 6)
        Me.gbCompanyInformation.Name = "gbCompanyInformation"
        Me.gbCompanyInformation.OpenHeight = 300
        Me.gbCompanyInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCompanyInformation.ShowBorder = True
        Me.gbCompanyInformation.ShowCheckBox = False
        Me.gbCompanyInformation.ShowCollapseButton = False
        Me.gbCompanyInformation.ShowDefaultBorderColor = True
        Me.gbCompanyInformation.ShowDownButton = False
        Me.gbCompanyInformation.ShowHeader = True
        Me.gbCompanyInformation.Size = New System.Drawing.Size(493, 304)
        Me.gbCompanyInformation.TabIndex = 1
        Me.gbCompanyInformation.Temp = 0
        Me.gbCompanyInformation.Text = "Company Information"
        Me.gbCompanyInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDeviceModel
        '
        Me.txtDeviceModel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDeviceModel.Location = New System.Drawing.Point(152, 275)
        Me.txtDeviceModel.Name = "txtDeviceModel"
        Me.txtDeviceModel.Size = New System.Drawing.Size(158, 21)
        Me.txtDeviceModel.TabIndex = 32
        '
        'lblDeviceModel
        '
        Me.lblDeviceModel.BackColor = System.Drawing.Color.Transparent
        Me.lblDeviceModel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeviceModel.ForeColor = System.Drawing.Color.Black
        Me.lblDeviceModel.Location = New System.Drawing.Point(8, 277)
        Me.lblDeviceModel.Name = "lblDeviceModel"
        Me.lblDeviceModel.Size = New System.Drawing.Size(130, 17)
        Me.lblDeviceModel.TabIndex = 31
        Me.lblDeviceModel.Text = "Device Model"
        '
        'lnkConfigureSagem
        '
        Me.lnkConfigureSagem.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkConfigureSagem.Location = New System.Drawing.Point(316, 137)
        Me.lnkConfigureSagem.Name = "lnkConfigureSagem"
        Me.lnkConfigureSagem.Size = New System.Drawing.Size(171, 16)
        Me.lnkConfigureSagem.TabIndex = 29
        Me.lnkConfigureSagem.TabStop = True
        Me.lnkConfigureSagem.Text = "Configure Sagem"
        '
        'lnkHandpunchConfig
        '
        Me.lnkHandpunchConfig.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkHandpunchConfig.Location = New System.Drawing.Point(316, 115)
        Me.lnkHandpunchConfig.Name = "lnkHandpunchConfig"
        Me.lnkHandpunchConfig.Size = New System.Drawing.Size(171, 16)
        Me.lnkHandpunchConfig.TabIndex = 27
        Me.lnkHandpunchConfig.TabStop = True
        Me.lnkHandpunchConfig.Text = "Configure Handpunch "
        '
        'frmDeviceSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(508, 371)
        Me.Controls.Add(Me.gbCompanyInformation)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDeviceSettings"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Device Settings"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbCompanyInformation.ResumeLayout(False)
        Me.gbCompanyInformation.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnTestConnection As eZee.Common.eZeeLightButton
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblSelectCompany As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCompany As eZee.Common.eZeeGradientButton
    Private WithEvents lblComProduct As System.Windows.Forms.Label
    Friend WithEvents cboComDevice As System.Windows.Forms.ComboBox
    Private WithEvents lblConnectionType As System.Windows.Forms.Label
    Friend WithEvents cboConnectionType As System.Windows.Forms.ComboBox
    Private WithEvents lblCommType As System.Windows.Forms.Label
    Friend WithEvents cboCommunicationType As System.Windows.Forms.ComboBox
    Private WithEvents lblIP As System.Windows.Forms.Label
    Private WithEvents txtPort As System.Windows.Forms.TextBox
    Private WithEvents lblPort As System.Windows.Forms.Label
    Private WithEvents txtIp As System.Windows.Forms.TextBox
    Private WithEvents cboPort As System.Windows.Forms.ComboBox
    Private WithEvents lblBaudrate As System.Windows.Forms.Label
    Private WithEvents cboBaudRate As System.Windows.Forms.ComboBox
    Private WithEvents lblRsMachineSN As System.Windows.Forms.Label
    Private WithEvents txtMachineSN As System.Windows.Forms.TextBox
    Private WithEvents LblUserID As System.Windows.Forms.Label
    Private WithEvents lblDeviceCode As System.Windows.Forms.Label
    Private WithEvents txtDeviceCode As System.Windows.Forms.TextBox
    Private WithEvents LblPassword As System.Windows.Forms.Label
    Private WithEvents lblCommunicationKey As System.Windows.Forms.Label
    Friend WithEvents txtUserID As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCommunicationKey As eZee.TextBox.NumericTextBox
    Friend WithEvents txtPassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents gbCompanyInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkHandpunchConfig As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkConfigureSagem As System.Windows.Forms.LinkLabel
    Private WithEvents txtDeviceModel As System.Windows.Forms.TextBox
    Private WithEvents lblDeviceModel As System.Windows.Forms.Label
End Class
