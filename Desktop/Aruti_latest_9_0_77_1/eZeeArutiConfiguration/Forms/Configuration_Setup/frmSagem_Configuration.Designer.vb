﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSagem_Configuration
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSagem_Configuration))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.txtSagemDbServer = New System.Windows.Forms.TextBox
        Me.lblSagemDBUserPwd = New System.Windows.Forms.Label
        Me.lblSagemDBUserName = New System.Windows.Forms.Label
        Me.lblSagemDBName = New System.Windows.Forms.Label
        Me.lblSagemDbServer = New System.Windows.Forms.Label
        Me.txtSagemDBUserPwd = New System.Windows.Forms.TextBox
        Me.txtSagemDBUserName = New System.Windows.Forms.TextBox
        Me.txtSagemDBName = New System.Windows.Forms.TextBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnTestConnection = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.txtSagemDbServer)
        Me.pnlMain.Controls.Add(Me.lblSagemDBUserPwd)
        Me.pnlMain.Controls.Add(Me.lblSagemDBUserName)
        Me.pnlMain.Controls.Add(Me.lblSagemDBName)
        Me.pnlMain.Controls.Add(Me.lblSagemDbServer)
        Me.pnlMain.Controls.Add(Me.txtSagemDBUserPwd)
        Me.pnlMain.Controls.Add(Me.txtSagemDBUserName)
        Me.pnlMain.Controls.Add(Me.txtSagemDBName)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(355, 178)
        Me.pnlMain.TabIndex = 0
        '
        'txtSagemDbServer
        '
        Me.txtSagemDbServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSagemDbServer.Location = New System.Drawing.Point(148, 12)
        Me.txtSagemDbServer.Name = "txtSagemDbServer"
        Me.txtSagemDbServer.Size = New System.Drawing.Size(200, 21)
        Me.txtSagemDbServer.TabIndex = 1
        '
        'lblSagemDBUserPwd
        '
        Me.lblSagemDBUserPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSagemDBUserPwd.Location = New System.Drawing.Point(6, 96)
        Me.lblSagemDBUserPwd.Name = "lblSagemDBUserPwd"
        Me.lblSagemDBUserPwd.Size = New System.Drawing.Size(136, 16)
        Me.lblSagemDBUserPwd.TabIndex = 6
        Me.lblSagemDBUserPwd.Text = "Database User Password"
        Me.lblSagemDBUserPwd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSagemDBUserName
        '
        Me.lblSagemDBUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSagemDBUserName.Location = New System.Drawing.Point(6, 69)
        Me.lblSagemDBUserName.Name = "lblSagemDBUserName"
        Me.lblSagemDBUserName.Size = New System.Drawing.Size(136, 16)
        Me.lblSagemDBUserName.TabIndex = 4
        Me.lblSagemDBUserName.Text = "Database User Name"
        Me.lblSagemDBUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSagemDBName
        '
        Me.lblSagemDBName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSagemDBName.Location = New System.Drawing.Point(6, 41)
        Me.lblSagemDBName.Name = "lblSagemDBName"
        Me.lblSagemDBName.Size = New System.Drawing.Size(136, 16)
        Me.lblSagemDBName.TabIndex = 2
        Me.lblSagemDBName.Text = "Database Name"
        Me.lblSagemDBName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSagemDbServer
        '
        Me.lblSagemDbServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSagemDbServer.Location = New System.Drawing.Point(6, 14)
        Me.lblSagemDbServer.Name = "lblSagemDbServer"
        Me.lblSagemDbServer.Size = New System.Drawing.Size(136, 16)
        Me.lblSagemDbServer.TabIndex = 0
        Me.lblSagemDbServer.Text = "Database Server"
        Me.lblSagemDbServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSagemDBUserPwd
        '
        Me.txtSagemDBUserPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSagemDBUserPwd.Location = New System.Drawing.Point(148, 94)
        Me.txtSagemDBUserPwd.Name = "txtSagemDBUserPwd"
        Me.txtSagemDBUserPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSagemDBUserPwd.Size = New System.Drawing.Size(200, 21)
        Me.txtSagemDBUserPwd.TabIndex = 7
        Me.txtSagemDBUserPwd.UseSystemPasswordChar = True
        '
        'txtSagemDBUserName
        '
        Me.txtSagemDBUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSagemDBUserName.Location = New System.Drawing.Point(148, 67)
        Me.txtSagemDBUserName.Name = "txtSagemDBUserName"
        Me.txtSagemDBUserName.Size = New System.Drawing.Size(200, 21)
        Me.txtSagemDBUserName.TabIndex = 5
        '
        'txtSagemDBName
        '
        Me.txtSagemDBName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSagemDBName.Location = New System.Drawing.Point(148, 39)
        Me.txtSagemDBName.Name = "txtSagemDBName"
        Me.txtSagemDBName.Size = New System.Drawing.Size(200, 21)
        Me.txtSagemDBName.TabIndex = 3
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnTestConnection)
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 123)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(355, 55)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnTestConnection
        '
        Me.btnTestConnection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTestConnection.BackColor = System.Drawing.Color.White
        Me.btnTestConnection.BackgroundImage = CType(resources.GetObject("btnTestConnection.BackgroundImage"), System.Drawing.Image)
        Me.btnTestConnection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTestConnection.BorderColor = System.Drawing.Color.Empty
        Me.btnTestConnection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnTestConnection.FlatAppearance.BorderSize = 0
        Me.btnTestConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTestConnection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTestConnection.ForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTestConnection.GradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestConnection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.Location = New System.Drawing.Point(9, 13)
        Me.btnTestConnection.Name = "btnTestConnection"
        Me.btnTestConnection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestConnection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.Size = New System.Drawing.Size(113, 30)
        Me.btnTestConnection.TabIndex = 2
        Me.btnTestConnection.Text = "&Test Connection"
        Me.btnTestConnection.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(258, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(162, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmSagem_Configuration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(355, 178)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSagem_Configuration"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Sagem Configuration"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents txtSagemDbServer As System.Windows.Forms.TextBox
    Friend WithEvents lblSagemDBUserPwd As System.Windows.Forms.Label
    Friend WithEvents lblSagemDBUserName As System.Windows.Forms.Label
    Friend WithEvents lblSagemDBName As System.Windows.Forms.Label
    Friend WithEvents lblSagemDbServer As System.Windows.Forms.Label
    Friend WithEvents txtSagemDBUserPwd As System.Windows.Forms.TextBox
    Friend WithEvents txtSagemDBUserName As System.Windows.Forms.TextBox
    Friend WithEvents txtSagemDBName As System.Windows.Forms.TextBox
    Friend WithEvents btnTestConnection As eZee.Common.eZeeLightButton
End Class
