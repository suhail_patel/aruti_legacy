﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGroup_Master

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGroup_Master"
    Private mblnCancel As Boolean = True
    Private objGMaster As clsGroup_Master
    Private menAction As enAction = enAction.EDIT_ONE
    Private mintGroupUnkid As Integer = 1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : displayDialog ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Medthos "

    Private Sub SetColor()
        Try
            txtAddress1.BackColor = GUI.ColorOptional
            txtAddress2.BackColor = GUI.ColorOptional
            txtCity.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional
            txtFax.BackColor = GUI.ColorOptional
            txtGroupName.BackColor = GUI.ColorComp
            txtId.BackColor = GUI.ColorOptional
            txtPhone.BackColor = GUI.ColorOptional
            txtState.BackColor = GUI.ColorOptional
            txtWebsite.BackColor = GUI.ColorOptional
            txtZipcode.BackColor = GUI.ColorOptional
            cboPropCountry.BackColor = GUI.ColorOptional
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetColor ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCMaster As New clsMasterData
        Try
            dsList = objCMaster.getCountryList("List", True)
            With cboPropCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsList.Tables("List")
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillCombo ; Module Name : " & mstrModuleName)
        Finally
            dsList.Dispose()
            objCMaster = Nothing
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objGMaster._Groupunkid = CInt(txtId.Text)
            objGMaster._Address1 = txtAddress1.Text
            objGMaster._Address2 = txtAddress2.Text
            objGMaster._City = txtCity.Text
            objGMaster._Country = cboPropCountry.Text
            objGMaster._Email = txtEmail.Text
            objGMaster._Fax = txtFax.Text
            objGMaster._Groupname = txtGroupName.Text.Trim
            objGMaster._Phone = txtPhone.Text
            objGMaster._State = txtState.Text
            objGMaster._Website = txtWebsite.Text
            objGMaster._Zipcode = txtZipcode.Text
            If menAction = enAction.ADD_ONE Then
                objGMaster._Startdate = Today.Date
                objGMaster._Freezdate = Today.Date.AddDays(-1)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetValue ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAddress1.Text = objGMaster._Address1
            txtAddress2.Text = objGMaster._Address2
            txtCity.Text = objGMaster._City
            cboPropCountry.SelectedIndex = cboPropCountry.FindStringExact(objGMaster._Country)
            txtEmail.Text = objGMaster._Email
            txtFax.Text = objGMaster._Fax
            txtGroupName.Text = objGMaster._Groupname
            txtId.Text = CStr(objGMaster._Groupunkid)
            txtPhone.Text = objGMaster._Phone
            txtState.Text = objGMaster._State
            txtWebsite.Text = objGMaster._Website
            txtZipcode.Text = objGMaster._Zipcode
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetValue ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If txtGroupName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Group Name cannot be blank. Group Name is mandatory information."), enMsgBoxStyle.Information)
                txtGroupName.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : IsValid ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmGroup_Master_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objGMaster = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmGroup_Master_FormClosed ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmGroup_Master_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmGroup_Master_KeyDown ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmGroup_Master_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmGroup_Master_KeyPress ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmGroup_Master_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objGMaster = New clsGroup_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()
            Dim objMasterData As New clsMasterData
            If objMasterData.IsCompanyCreated = True And objMasterData.IsGroupCreated = False Then
                menAction = enAction.ADD_ONE
            End If

            If menAction = enAction.EDIT_ONE Then
                objGMaster._Groupunkid = mintGroupUnkid
            End If
            Call GetValue()
            objMasterData = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmGroup_Master_Load ; Module Name : " & mstrModuleName)
        Finally

        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsGroup_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsGroup_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnClose_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objGMaster._FormName = mstrModuleName
            objGMaster._LoginEmployeeunkid = 0
            objGMaster._ClientIP = getIP()
            objGMaster._HostName = getHostName()
            objGMaster._FromWeb = False
            objGMaster._AuditUserId = User._Object._Userunkid
objGMaster._CompanyUnkid = Company._Object._Companyunkid
            objGMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.ADD_ONE Then
                blnFlag = objGMaster.Insert
            Else
                blnFlag = objGMaster.Update
            End If

            If blnFlag Then
                Me.Close()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnSave_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbGroupInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGroupInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbGroupInfo.Text = Language._Object.getCaption(Me.gbGroupInfo.Name, Me.gbGroupInfo.Text)
			Me.lblWebsite.Text = Language._Object.getCaption(Me.lblWebsite.Name, Me.lblWebsite.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblAddress2.Text = Language._Object.getCaption(Me.lblAddress2.Name, Me.lblAddress2.Text)
			Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
			Me.lblPhone.Text = Language._Object.getCaption(Me.lblPhone.Name, Me.lblPhone.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblZipCode.Text = Language._Object.getCaption(Me.lblZipCode.Name, Me.lblZipCode.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Group Name cannot be blank. Group Name is mandatory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class