﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGroup_Master
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGroup_Master))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbGroupInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtGroupName = New eZee.TextBox.AlphanumericTextBox
        Me.cboPropCountry = New System.Windows.Forms.ComboBox
        Me.lblWebsite = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.txtWebsite = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtFax = New eZee.TextBox.AlphanumericTextBox
        Me.lblPhone = New System.Windows.Forms.Label
        Me.txtPhone = New eZee.TextBox.AlphanumericTextBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.lblZipCode = New System.Windows.Forms.Label
        Me.txtZipcode = New eZee.TextBox.AlphanumericTextBox
        Me.lblState = New System.Windows.Forms.Label
        Me.txtState = New eZee.TextBox.AlphanumericTextBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.txtCity = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtId = New System.Windows.Forms.TextBox
        Me.Panel1.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbGroupInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.EZeeFooter1)
        Me.Panel1.Controls.Add(Me.gbGroupInfo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(370, 344)
        Me.Panel1.TabIndex = 3
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 289)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(370, 55)
        Me.EZeeFooter1.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(160, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 130
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(263, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 129
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbGroupInfo
        '
        Me.gbGroupInfo.BorderColor = System.Drawing.Color.Black
        Me.gbGroupInfo.Checked = False
        Me.gbGroupInfo.CollapseAllExceptThis = False
        Me.gbGroupInfo.CollapsedHoverImage = Nothing
        Me.gbGroupInfo.CollapsedNormalImage = Nothing
        Me.gbGroupInfo.CollapsedPressedImage = Nothing
        Me.gbGroupInfo.CollapseOnLoad = False
        Me.gbGroupInfo.Controls.Add(Me.txtGroupName)
        Me.gbGroupInfo.Controls.Add(Me.cboPropCountry)
        Me.gbGroupInfo.Controls.Add(Me.lblWebsite)
        Me.gbGroupInfo.Controls.Add(Me.lblEmail)
        Me.gbGroupInfo.Controls.Add(Me.txtWebsite)
        Me.gbGroupInfo.Controls.Add(Me.txtAddress2)
        Me.gbGroupInfo.Controls.Add(Me.lblAddress2)
        Me.gbGroupInfo.Controls.Add(Me.txtEmail)
        Me.gbGroupInfo.Controls.Add(Me.lblFax)
        Me.gbGroupInfo.Controls.Add(Me.txtFax)
        Me.gbGroupInfo.Controls.Add(Me.lblPhone)
        Me.gbGroupInfo.Controls.Add(Me.txtPhone)
        Me.gbGroupInfo.Controls.Add(Me.lblCountry)
        Me.gbGroupInfo.Controls.Add(Me.lblZipCode)
        Me.gbGroupInfo.Controls.Add(Me.txtZipcode)
        Me.gbGroupInfo.Controls.Add(Me.lblState)
        Me.gbGroupInfo.Controls.Add(Me.txtState)
        Me.gbGroupInfo.Controls.Add(Me.lblCity)
        Me.gbGroupInfo.Controls.Add(Me.txtCity)
        Me.gbGroupInfo.Controls.Add(Me.txtAddress1)
        Me.gbGroupInfo.Controls.Add(Me.lblAddress)
        Me.gbGroupInfo.Controls.Add(Me.lblName)
        Me.gbGroupInfo.Controls.Add(Me.txtId)
        Me.gbGroupInfo.ExpandedHoverImage = Nothing
        Me.gbGroupInfo.ExpandedNormalImage = Nothing
        Me.gbGroupInfo.ExpandedPressedImage = Nothing
        Me.gbGroupInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGroupInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGroupInfo.HeaderHeight = 25
        Me.gbGroupInfo.HeaderMessage = ""
        Me.gbGroupInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbGroupInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGroupInfo.HeightOnCollapse = 0
        Me.gbGroupInfo.LeftTextSpace = 0
        Me.gbGroupInfo.Location = New System.Drawing.Point(10, 8)
        Me.gbGroupInfo.Name = "gbGroupInfo"
        Me.gbGroupInfo.OpenHeight = 345
        Me.gbGroupInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.gbGroupInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGroupInfo.ShowBorder = True
        Me.gbGroupInfo.ShowCheckBox = False
        Me.gbGroupInfo.ShowCollapseButton = False
        Me.gbGroupInfo.ShowDefaultBorderColor = True
        Me.gbGroupInfo.ShowDownButton = False
        Me.gbGroupInfo.ShowHeader = True
        Me.gbGroupInfo.Size = New System.Drawing.Size(351, 277)
        Me.gbGroupInfo.TabIndex = 3
        Me.gbGroupInfo.Temp = 0
        Me.gbGroupInfo.Text = "Group Details"
        Me.gbGroupInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGroupName
        '
        Me.txtGroupName.Flags = 0
        Me.txtGroupName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGroupName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGroupName.Location = New System.Drawing.Point(98, 32)
        Me.txtGroupName.Name = "txtGroupName"
        Me.txtGroupName.Size = New System.Drawing.Size(239, 21)
        Me.txtGroupName.TabIndex = 202
        Me.txtGroupName.Tag = "group_name"
        '
        'cboPropCountry
        '
        Me.cboPropCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPropCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPropCountry.FormattingEnabled = True
        Me.cboPropCountry.Location = New System.Drawing.Point(98, 167)
        Me.cboPropCountry.Name = "cboPropCountry"
        Me.cboPropCountry.Size = New System.Drawing.Size(239, 21)
        Me.cboPropCountry.TabIndex = 214
        Me.cboPropCountry.Tag = "country"
        '
        'lblWebsite
        '
        Me.lblWebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWebsite.Location = New System.Drawing.Point(7, 252)
        Me.lblWebsite.Name = "lblWebsite"
        Me.lblWebsite.Size = New System.Drawing.Size(84, 13)
        Me.lblWebsite.TabIndex = 223
        Me.lblWebsite.Text = "Website"
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(7, 225)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(84, 13)
        Me.lblEmail.TabIndex = 221
        Me.lblEmail.Text = "Email"
        '
        'txtWebsite
        '
        Me.txtWebsite.Flags = 0
        Me.txtWebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWebsite.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWebsite.Location = New System.Drawing.Point(98, 248)
        Me.txtWebsite.Name = "txtWebsite"
        Me.txtWebsite.Size = New System.Drawing.Size(239, 21)
        Me.txtWebsite.TabIndex = 224
        Me.txtWebsite.Tag = "website"
        '
        'txtAddress2
        '
        Me.txtAddress2.Flags = 0
        Me.txtAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress2.Location = New System.Drawing.Point(98, 86)
        Me.txtAddress2.Name = "txtAddress2"
        Me.txtAddress2.Size = New System.Drawing.Size(239, 21)
        Me.txtAddress2.TabIndex = 206
        Me.txtAddress2.Tag = "address2"
        '
        'lblAddress2
        '
        Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress2.Location = New System.Drawing.Point(7, 90)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.Size = New System.Drawing.Size(84, 13)
        Me.lblAddress2.TabIndex = 205
        Me.lblAddress2.Text = "Address2"
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(98, 221)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(239, 21)
        Me.txtEmail.TabIndex = 222
        Me.txtEmail.Tag = "email"
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(190, 198)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(60, 13)
        Me.lblFax.TabIndex = 217
        Me.lblFax.Text = "Fax"
        '
        'txtFax
        '
        Me.txtFax.Flags = 0
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFax.Location = New System.Drawing.Point(256, 194)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(81, 21)
        Me.txtFax.TabIndex = 218
        Me.txtFax.Tag = "fax"
        '
        'lblPhone
        '
        Me.lblPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(7, 198)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(84, 13)
        Me.lblPhone.TabIndex = 215
        Me.lblPhone.Text = "Phone"
        '
        'txtPhone
        '
        Me.txtPhone.Flags = 0
        Me.txtPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone.Location = New System.Drawing.Point(98, 194)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(81, 21)
        Me.txtPhone.TabIndex = 216
        Me.txtPhone.Tag = "phone"
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(7, 171)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(84, 13)
        Me.lblCountry.TabIndex = 213
        Me.lblCountry.Text = "Country"
        '
        'lblZipCode
        '
        Me.lblZipCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZipCode.Location = New System.Drawing.Point(190, 117)
        Me.lblZipCode.Name = "lblZipCode"
        Me.lblZipCode.Size = New System.Drawing.Size(60, 13)
        Me.lblZipCode.TabIndex = 209
        Me.lblZipCode.Text = "Zip"
        '
        'txtZipcode
        '
        Me.txtZipcode.Flags = 0
        Me.txtZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZipcode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtZipcode.Location = New System.Drawing.Point(256, 113)
        Me.txtZipcode.Name = "txtZipcode"
        Me.txtZipcode.Size = New System.Drawing.Size(81, 21)
        Me.txtZipcode.TabIndex = 210
        Me.txtZipcode.Tag = "zipcode"
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(7, 144)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(84, 13)
        Me.lblState.TabIndex = 211
        Me.lblState.Text = "State"
        '
        'txtState
        '
        Me.txtState.Flags = 0
        Me.txtState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtState.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtState.Location = New System.Drawing.Point(98, 140)
        Me.txtState.Name = "txtState"
        Me.txtState.Size = New System.Drawing.Size(239, 21)
        Me.txtState.TabIndex = 212
        Me.txtState.Tag = "state"
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(7, 117)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(84, 13)
        Me.lblCity.TabIndex = 207
        Me.lblCity.Text = "City"
        '
        'txtCity
        '
        Me.txtCity.Flags = 0
        Me.txtCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCity.Location = New System.Drawing.Point(98, 113)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Size = New System.Drawing.Size(81, 21)
        Me.txtCity.TabIndex = 208
        Me.txtCity.Tag = "city"
        '
        'txtAddress1
        '
        Me.txtAddress1.Flags = 0
        Me.txtAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress1.Location = New System.Drawing.Point(98, 59)
        Me.txtAddress1.Name = "txtAddress1"
        Me.txtAddress1.Size = New System.Drawing.Size(239, 21)
        Me.txtAddress1.TabIndex = 204
        Me.txtAddress1.Tag = "Address1"
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(7, 63)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblAddress.TabIndex = 203
        Me.lblAddress.Text = "Address1"
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 36)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(84, 13)
        Me.lblName.TabIndex = 201
        Me.lblName.Text = "Group Name"
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtId.Location = New System.Drawing.Point(283, 32)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(54, 21)
        Me.txtId.TabIndex = 225
        Me.txtId.Tag = "ownerunkid"
        Me.txtId.Visible = False
        '
        'frmGroup_Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 344)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGroup_Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Group Information"
        Me.Panel1.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbGroupInfo.ResumeLayout(False)
        Me.gbGroupInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbGroupInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtGroupName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboPropCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblWebsite As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtWebsite As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents txtPhone As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents lblZipCode As System.Windows.Forms.Label
    Friend WithEvents txtZipcode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents txtState As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents txtCity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
End Class
