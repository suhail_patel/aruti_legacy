﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.eZeeConfigWiz = New eZee.Common.eZeeWizard
        Me.ewpGeneralInfo = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbGeneralInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStarrtDate = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.objlblGenInfo = New System.Windows.Forms.Label
        Me.ewpFinish = New eZee.Common.eZeeWizardPage(Me.components)
        Me.objlblFinishCaption = New System.Windows.Forms.Label
        Me.ewpGroupInfo = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbGroupInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtGroupName = New eZee.TextBox.AlphanumericTextBox
        Me.cboPropCountry = New System.Windows.Forms.ComboBox
        Me.lblWebsite = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.txtWebsite = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtFax = New eZee.TextBox.AlphanumericTextBox
        Me.lblPhone = New System.Windows.Forms.Label
        Me.txtPhone = New eZee.TextBox.AlphanumericTextBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.lblZipCode = New System.Windows.Forms.Label
        Me.txtZipcode = New eZee.TextBox.AlphanumericTextBox
        Me.lblState = New System.Windows.Forms.Label
        Me.txtState = New eZee.TextBox.AlphanumericTextBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.txtCity = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtId = New System.Windows.Forms.TextBox
        Me.objeZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMain.SuspendLayout()
        Me.eZeeConfigWiz.SuspendLayout()
        Me.ewpGeneralInfo.SuspendLayout()
        Me.gbGeneralInfo.SuspendLayout()
        Me.ewpFinish.SuspendLayout()
        Me.ewpGroupInfo.SuspendLayout()
        Me.gbGroupInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.eZeeConfigWiz)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(498, 392)
        Me.pnlMain.TabIndex = 0
        '
        'eZeeConfigWiz
        '
        Me.eZeeConfigWiz.Controls.Add(Me.ewpGeneralInfo)
        Me.eZeeConfigWiz.Controls.Add(Me.ewpFinish)
        Me.eZeeConfigWiz.Controls.Add(Me.ewpGroupInfo)
        Me.eZeeConfigWiz.Dock = System.Windows.Forms.DockStyle.None
        Me.eZeeConfigWiz.HeaderImage = Global.eZeeArutiConfiguration.My.Resources.Resources.Aruti_Wiz
        Me.eZeeConfigWiz.Location = New System.Drawing.Point(3, 60)
        Me.eZeeConfigWiz.Name = "eZeeConfigWiz"
        Me.eZeeConfigWiz.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.ewpGeneralInfo, Me.ewpGroupInfo, Me.ewpFinish})
        Me.eZeeConfigWiz.Size = New System.Drawing.Size(493, 330)
        Me.eZeeConfigWiz.TabIndex = 1
        Me.eZeeConfigWiz.WelcomeImage = Nothing
        '
        'ewpGeneralInfo
        '
        Me.ewpGeneralInfo.Controls.Add(Me.gbGeneralInfo)
        Me.ewpGeneralInfo.Location = New System.Drawing.Point(0, 0)
        Me.ewpGeneralInfo.Name = "ewpGeneralInfo"
        Me.ewpGeneralInfo.Size = New System.Drawing.Size(493, 282)
        Me.ewpGeneralInfo.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpGeneralInfo.TabIndex = 7
        '
        'gbGeneralInfo
        '
        Me.gbGeneralInfo.BorderColor = System.Drawing.Color.Black
        Me.gbGeneralInfo.Checked = False
        Me.gbGeneralInfo.CollapseAllExceptThis = False
        Me.gbGeneralInfo.CollapsedHoverImage = Nothing
        Me.gbGeneralInfo.CollapsedNormalImage = Nothing
        Me.gbGeneralInfo.CollapsedPressedImage = Nothing
        Me.gbGeneralInfo.CollapseOnLoad = False
        Me.gbGeneralInfo.Controls.Add(Me.lblStarrtDate)
        Me.gbGeneralInfo.Controls.Add(Me.dtpStartDate)
        Me.gbGeneralInfo.Controls.Add(Me.objlblGenInfo)
        Me.gbGeneralInfo.ExpandedHoverImage = Nothing
        Me.gbGeneralInfo.ExpandedNormalImage = Nothing
        Me.gbGeneralInfo.ExpandedPressedImage = Nothing
        Me.gbGeneralInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGeneralInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGeneralInfo.HeaderHeight = 25
        Me.gbGeneralInfo.HeaderMessage = ""
        Me.gbGeneralInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbGeneralInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGeneralInfo.HeightOnCollapse = 0
        Me.gbGeneralInfo.LeftTextSpace = 0
        Me.gbGeneralInfo.Location = New System.Drawing.Point(167, 0)
        Me.gbGeneralInfo.Name = "gbGeneralInfo"
        Me.gbGeneralInfo.OpenHeight = 300
        Me.gbGeneralInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGeneralInfo.ShowBorder = True
        Me.gbGeneralInfo.ShowCheckBox = False
        Me.gbGeneralInfo.ShowCollapseButton = False
        Me.gbGeneralInfo.ShowDefaultBorderColor = True
        Me.gbGeneralInfo.ShowDownButton = False
        Me.gbGeneralInfo.ShowHeader = True
        Me.gbGeneralInfo.Size = New System.Drawing.Size(325, 280)
        Me.gbGeneralInfo.TabIndex = 0
        Me.gbGeneralInfo.Temp = 0
        Me.gbGeneralInfo.Text = "General Information"
        Me.gbGeneralInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStarrtDate
        '
        Me.lblStarrtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStarrtDate.Location = New System.Drawing.Point(8, 257)
        Me.lblStarrtDate.Name = "lblStarrtDate"
        Me.lblStarrtDate.Size = New System.Drawing.Size(69, 13)
        Me.lblStarrtDate.TabIndex = 21
        Me.lblStarrtDate.Text = "Start Date"
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(82, 253)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(93, 21)
        Me.dtpStartDate.TabIndex = 22
        '
        'objlblGenInfo
        '
        Me.objlblGenInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblGenInfo.Location = New System.Drawing.Point(8, 28)
        Me.objlblGenInfo.Name = "objlblGenInfo"
        Me.objlblGenInfo.Size = New System.Drawing.Size(310, 219)
        Me.objlblGenInfo.TabIndex = 19
        Me.objlblGenInfo.Text = "lblGenInfo"
        '
        'ewpFinish
        '
        Me.ewpFinish.Controls.Add(Me.objlblFinishCaption)
        Me.ewpFinish.Location = New System.Drawing.Point(0, 0)
        Me.ewpFinish.Name = "ewpFinish"
        Me.ewpFinish.Size = New System.Drawing.Size(493, 282)
        Me.ewpFinish.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpFinish.TabIndex = 10
        '
        'objlblFinishCaption
        '
        Me.objlblFinishCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblFinishCaption.Location = New System.Drawing.Point(169, 2)
        Me.objlblFinishCaption.Name = "objlblFinishCaption"
        Me.objlblFinishCaption.Size = New System.Drawing.Size(320, 276)
        Me.objlblFinishCaption.TabIndex = 0
        Me.objlblFinishCaption.Text = "objlblCatiopn"
        Me.objlblFinishCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ewpGroupInfo
        '
        Me.ewpGroupInfo.Controls.Add(Me.gbGroupInfo)
        Me.ewpGroupInfo.Location = New System.Drawing.Point(0, 0)
        Me.ewpGroupInfo.Name = "ewpGroupInfo"
        Me.ewpGroupInfo.Size = New System.Drawing.Size(428, 208)
        Me.ewpGroupInfo.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpGroupInfo.TabIndex = 9
        '
        'gbGroupInfo
        '
        Me.gbGroupInfo.BorderColor = System.Drawing.Color.Black
        Me.gbGroupInfo.Checked = False
        Me.gbGroupInfo.CollapseAllExceptThis = False
        Me.gbGroupInfo.CollapsedHoverImage = Nothing
        Me.gbGroupInfo.CollapsedNormalImage = Nothing
        Me.gbGroupInfo.CollapsedPressedImage = Nothing
        Me.gbGroupInfo.CollapseOnLoad = False
        Me.gbGroupInfo.Controls.Add(Me.txtGroupName)
        Me.gbGroupInfo.Controls.Add(Me.cboPropCountry)
        Me.gbGroupInfo.Controls.Add(Me.lblWebsite)
        Me.gbGroupInfo.Controls.Add(Me.lblEmail)
        Me.gbGroupInfo.Controls.Add(Me.txtWebsite)
        Me.gbGroupInfo.Controls.Add(Me.txtAddress2)
        Me.gbGroupInfo.Controls.Add(Me.lblAddress2)
        Me.gbGroupInfo.Controls.Add(Me.txtEmail)
        Me.gbGroupInfo.Controls.Add(Me.lblFax)
        Me.gbGroupInfo.Controls.Add(Me.txtFax)
        Me.gbGroupInfo.Controls.Add(Me.lblPhone)
        Me.gbGroupInfo.Controls.Add(Me.txtPhone)
        Me.gbGroupInfo.Controls.Add(Me.lblCountry)
        Me.gbGroupInfo.Controls.Add(Me.lblZipCode)
        Me.gbGroupInfo.Controls.Add(Me.txtZipcode)
        Me.gbGroupInfo.Controls.Add(Me.lblState)
        Me.gbGroupInfo.Controls.Add(Me.txtState)
        Me.gbGroupInfo.Controls.Add(Me.lblCity)
        Me.gbGroupInfo.Controls.Add(Me.txtCity)
        Me.gbGroupInfo.Controls.Add(Me.txtAddress1)
        Me.gbGroupInfo.Controls.Add(Me.lblAddress)
        Me.gbGroupInfo.Controls.Add(Me.lblName)
        Me.gbGroupInfo.Controls.Add(Me.txtId)
        Me.gbGroupInfo.ExpandedHoverImage = Nothing
        Me.gbGroupInfo.ExpandedNormalImage = Nothing
        Me.gbGroupInfo.ExpandedPressedImage = Nothing
        Me.gbGroupInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGroupInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGroupInfo.HeaderHeight = 25
        Me.gbGroupInfo.HeaderMessage = ""
        Me.gbGroupInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbGroupInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGroupInfo.HeightOnCollapse = 0
        Me.gbGroupInfo.LeftTextSpace = 0
        Me.gbGroupInfo.Location = New System.Drawing.Point(167, 0)
        Me.gbGroupInfo.Name = "gbGroupInfo"
        Me.gbGroupInfo.OpenHeight = 345
        Me.gbGroupInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.gbGroupInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGroupInfo.ShowBorder = True
        Me.gbGroupInfo.ShowCheckBox = False
        Me.gbGroupInfo.ShowCollapseButton = False
        Me.gbGroupInfo.ShowDefaultBorderColor = True
        Me.gbGroupInfo.ShowDownButton = False
        Me.gbGroupInfo.ShowHeader = True
        Me.gbGroupInfo.Size = New System.Drawing.Size(325, 280)
        Me.gbGroupInfo.TabIndex = 1
        Me.gbGroupInfo.Temp = 0
        Me.gbGroupInfo.Text = "Group Details"
        Me.gbGroupInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGroupName
        '
        Me.txtGroupName.Flags = 0
        Me.txtGroupName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGroupName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGroupName.Location = New System.Drawing.Point(98, 32)
        Me.txtGroupName.Name = "txtGroupName"
        Me.txtGroupName.Size = New System.Drawing.Size(200, 21)
        Me.txtGroupName.TabIndex = 202
        Me.txtGroupName.Tag = "group_name"
        '
        'cboPropCountry
        '
        Me.cboPropCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPropCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPropCountry.FormattingEnabled = True
        Me.cboPropCountry.Location = New System.Drawing.Point(98, 167)
        Me.cboPropCountry.Name = "cboPropCountry"
        Me.cboPropCountry.Size = New System.Drawing.Size(200, 21)
        Me.cboPropCountry.TabIndex = 214
        Me.cboPropCountry.Tag = "country"
        '
        'lblWebsite
        '
        Me.lblWebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWebsite.Location = New System.Drawing.Point(7, 252)
        Me.lblWebsite.Name = "lblWebsite"
        Me.lblWebsite.Size = New System.Drawing.Size(84, 13)
        Me.lblWebsite.TabIndex = 223
        Me.lblWebsite.Text = "Website"
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(7, 225)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(84, 13)
        Me.lblEmail.TabIndex = 221
        Me.lblEmail.Text = "Email"
        '
        'txtWebsite
        '
        Me.txtWebsite.Flags = 0
        Me.txtWebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWebsite.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWebsite.Location = New System.Drawing.Point(98, 248)
        Me.txtWebsite.Name = "txtWebsite"
        Me.txtWebsite.Size = New System.Drawing.Size(200, 21)
        Me.txtWebsite.TabIndex = 224
        Me.txtWebsite.Tag = "website"
        '
        'txtAddress2
        '
        Me.txtAddress2.Flags = 0
        Me.txtAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress2.Location = New System.Drawing.Point(98, 86)
        Me.txtAddress2.Name = "txtAddress2"
        Me.txtAddress2.Size = New System.Drawing.Size(200, 21)
        Me.txtAddress2.TabIndex = 206
        Me.txtAddress2.Tag = "address2"
        '
        'lblAddress2
        '
        Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress2.Location = New System.Drawing.Point(7, 90)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.Size = New System.Drawing.Size(84, 13)
        Me.lblAddress2.TabIndex = 205
        Me.lblAddress2.Text = "Address2"
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(98, 221)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(200, 21)
        Me.txtEmail.TabIndex = 222
        Me.txtEmail.Tag = "email"
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(169, 198)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(60, 13)
        Me.lblFax.TabIndex = 217
        Me.lblFax.Text = "Fax"
        '
        'txtFax
        '
        Me.txtFax.Flags = 0
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFax.Location = New System.Drawing.Point(231, 194)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(67, 21)
        Me.txtFax.TabIndex = 218
        Me.txtFax.Tag = "fax"
        '
        'lblPhone
        '
        Me.lblPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(7, 198)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(84, 13)
        Me.lblPhone.TabIndex = 215
        Me.lblPhone.Text = "Phone"
        '
        'txtPhone
        '
        Me.txtPhone.Flags = 0
        Me.txtPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone.Location = New System.Drawing.Point(98, 194)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(65, 21)
        Me.txtPhone.TabIndex = 216
        Me.txtPhone.Tag = "phone"
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(7, 171)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(84, 13)
        Me.lblCountry.TabIndex = 213
        Me.lblCountry.Text = "Country"
        '
        'lblZipCode
        '
        Me.lblZipCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZipCode.Location = New System.Drawing.Point(169, 117)
        Me.lblZipCode.Name = "lblZipCode"
        Me.lblZipCode.Size = New System.Drawing.Size(60, 13)
        Me.lblZipCode.TabIndex = 209
        Me.lblZipCode.Text = "Zip"
        '
        'txtZipcode
        '
        Me.txtZipcode.Flags = 0
        Me.txtZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZipcode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtZipcode.Location = New System.Drawing.Point(231, 113)
        Me.txtZipcode.Name = "txtZipcode"
        Me.txtZipcode.Size = New System.Drawing.Size(67, 21)
        Me.txtZipcode.TabIndex = 210
        Me.txtZipcode.Tag = "zipcode"
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(7, 144)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(84, 13)
        Me.lblState.TabIndex = 211
        Me.lblState.Text = "State"
        '
        'txtState
        '
        Me.txtState.Flags = 0
        Me.txtState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtState.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtState.Location = New System.Drawing.Point(98, 140)
        Me.txtState.Name = "txtState"
        Me.txtState.Size = New System.Drawing.Size(200, 21)
        Me.txtState.TabIndex = 212
        Me.txtState.Tag = "state"
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(7, 117)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(84, 13)
        Me.lblCity.TabIndex = 207
        Me.lblCity.Text = "City"
        '
        'txtCity
        '
        Me.txtCity.Flags = 0
        Me.txtCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCity.Location = New System.Drawing.Point(98, 113)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Size = New System.Drawing.Size(65, 21)
        Me.txtCity.TabIndex = 208
        Me.txtCity.Tag = "city"
        '
        'txtAddress1
        '
        Me.txtAddress1.Flags = 0
        Me.txtAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress1.Location = New System.Drawing.Point(98, 59)
        Me.txtAddress1.Name = "txtAddress1"
        Me.txtAddress1.Size = New System.Drawing.Size(200, 21)
        Me.txtAddress1.TabIndex = 204
        Me.txtAddress1.Tag = "Address1"
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(7, 63)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblAddress.TabIndex = 203
        Me.lblAddress.Text = "Address1"
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 36)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(84, 13)
        Me.lblName.TabIndex = 201
        Me.lblName.Text = "Group Name"
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtId.Location = New System.Drawing.Point(244, 32)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(54, 21)
        Me.txtId.TabIndex = 225
        Me.txtId.Tag = "ownerunkid"
        Me.txtId.Visible = False
        '
        'objeZeeHeader
        '
        Me.objeZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.objeZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.objeZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.objeZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.objeZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objeZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.objeZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.objeZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objeZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.objeZeeHeader.Icon = Nothing
        Me.objeZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.objeZeeHeader.Message = ""
        Me.objeZeeHeader.Name = "objeZeeHeader"
        Me.objeZeeHeader.Size = New System.Drawing.Size(498, 58)
        Me.objeZeeHeader.TabIndex = 2
        Me.objeZeeHeader.Title = ""
        '
        'frmWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(498, 392)
        Me.Controls.Add(Me.objeZeeHeader)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Configuration Wizard - Information"
        Me.pnlMain.ResumeLayout(False)
        Me.eZeeConfigWiz.ResumeLayout(False)
        Me.ewpGeneralInfo.ResumeLayout(False)
        Me.gbGeneralInfo.ResumeLayout(False)
        Me.ewpFinish.ResumeLayout(False)
        Me.ewpGroupInfo.ResumeLayout(False)
        Me.gbGroupInfo.ResumeLayout(False)
        Me.gbGroupInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents eZeeConfigWiz As eZee.Common.eZeeWizard
    Friend WithEvents ewpGeneralInfo As eZee.Common.eZeeWizardPage
    Friend WithEvents ewpGroupInfo As eZee.Common.eZeeWizardPage
    Friend WithEvents gbGeneralInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblStarrtDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblGenInfo As System.Windows.Forms.Label
    Friend WithEvents gbGroupInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtGroupName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboPropCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblWebsite As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtWebsite As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents txtPhone As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents lblZipCode As System.Windows.Forms.Label
    Friend WithEvents txtZipcode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents txtState As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents txtCity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents objeZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents ewpFinish As eZee.Common.eZeeWizardPage
    Friend WithEvents objlblFinishCaption As System.Windows.Forms.Label
End Class
