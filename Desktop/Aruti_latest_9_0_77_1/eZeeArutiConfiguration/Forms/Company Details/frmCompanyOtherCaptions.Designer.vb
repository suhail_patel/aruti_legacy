﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompanyOtherCaptions
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompanyOtherCaptions))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbOtherRegCaptions = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRegCaption3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtRegCaption2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtRegCaption1 = New eZee.TextBox.AlphanumericTextBox
        Me.ChkCaption3 = New System.Windows.Forms.CheckBox
        Me.ChkCaption2 = New System.Windows.Forms.CheckBox
        Me.ChkCaption1 = New System.Windows.Forms.CheckBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbOtherRegCaptions.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbOtherRegCaptions)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(343, 193)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbOtherRegCaptions
        '
        Me.gbOtherRegCaptions.BorderColor = System.Drawing.Color.Black
        Me.gbOtherRegCaptions.Checked = False
        Me.gbOtherRegCaptions.CollapseAllExceptThis = False
        Me.gbOtherRegCaptions.CollapsedHoverImage = Nothing
        Me.gbOtherRegCaptions.CollapsedNormalImage = Nothing
        Me.gbOtherRegCaptions.CollapsedPressedImage = Nothing
        Me.gbOtherRegCaptions.CollapseOnLoad = False
        Me.gbOtherRegCaptions.Controls.Add(Me.txtRegCaption3)
        Me.gbOtherRegCaptions.Controls.Add(Me.txtRegCaption2)
        Me.gbOtherRegCaptions.Controls.Add(Me.txtRegCaption1)
        Me.gbOtherRegCaptions.Controls.Add(Me.ChkCaption3)
        Me.gbOtherRegCaptions.Controls.Add(Me.ChkCaption2)
        Me.gbOtherRegCaptions.Controls.Add(Me.ChkCaption1)
        Me.gbOtherRegCaptions.ExpandedHoverImage = Nothing
        Me.gbOtherRegCaptions.ExpandedNormalImage = Nothing
        Me.gbOtherRegCaptions.ExpandedPressedImage = Nothing
        Me.gbOtherRegCaptions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtherRegCaptions.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOtherRegCaptions.HeaderHeight = 25
        Me.gbOtherRegCaptions.HeightOnCollapse = 0
        Me.gbOtherRegCaptions.LeftTextSpace = 0
        Me.gbOtherRegCaptions.Location = New System.Drawing.Point(13, 13)
        Me.gbOtherRegCaptions.Name = "gbOtherRegCaptions"
        Me.gbOtherRegCaptions.OpenHeight = 300
        Me.gbOtherRegCaptions.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOtherRegCaptions.ShowBorder = True
        Me.gbOtherRegCaptions.ShowCheckBox = False
        Me.gbOtherRegCaptions.ShowCollapseButton = False
        Me.gbOtherRegCaptions.ShowDefaultBorderColor = True
        Me.gbOtherRegCaptions.ShowDownButton = False
        Me.gbOtherRegCaptions.ShowHeader = True
        Me.gbOtherRegCaptions.Size = New System.Drawing.Size(318, 117)
        Me.gbOtherRegCaptions.TabIndex = 0
        Me.gbOtherRegCaptions.Temp = 0
        Me.gbOtherRegCaptions.Text = "Other Registration Caption"
        Me.gbOtherRegCaptions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRegCaption3
        '
        Me.txtRegCaption3.Flags = 0
        Me.txtRegCaption3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegCaption3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRegCaption3.Location = New System.Drawing.Point(120, 88)
        Me.txtRegCaption3.Name = "txtRegCaption3"
        Me.txtRegCaption3.Size = New System.Drawing.Size(185, 21)
        Me.txtRegCaption3.TabIndex = 5
        '
        'txtRegCaption2
        '
        Me.txtRegCaption2.Flags = 0
        Me.txtRegCaption2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegCaption2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRegCaption2.Location = New System.Drawing.Point(120, 61)
        Me.txtRegCaption2.Name = "txtRegCaption2"
        Me.txtRegCaption2.Size = New System.Drawing.Size(185, 21)
        Me.txtRegCaption2.TabIndex = 3
        '
        'txtRegCaption1
        '
        Me.txtRegCaption1.Flags = 0
        Me.txtRegCaption1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegCaption1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRegCaption1.Location = New System.Drawing.Point(120, 34)
        Me.txtRegCaption1.Name = "txtRegCaption1"
        Me.txtRegCaption1.Size = New System.Drawing.Size(185, 21)
        Me.txtRegCaption1.TabIndex = 1
        '
        'ChkCaption3
        '
        Me.ChkCaption3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCaption3.Location = New System.Drawing.Point(8, 90)
        Me.ChkCaption3.Name = "ChkCaption3"
        Me.ChkCaption3.Size = New System.Drawing.Size(106, 17)
        Me.ChkCaption3.TabIndex = 4
        Me.ChkCaption3.Text = "Reg. Caption3"
        Me.ChkCaption3.UseVisualStyleBackColor = True
        '
        'ChkCaption2
        '
        Me.ChkCaption2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCaption2.Location = New System.Drawing.Point(8, 63)
        Me.ChkCaption2.Name = "ChkCaption2"
        Me.ChkCaption2.Size = New System.Drawing.Size(106, 17)
        Me.ChkCaption2.TabIndex = 2
        Me.ChkCaption2.Text = "Reg. Caption2"
        Me.ChkCaption2.UseVisualStyleBackColor = True
        '
        'ChkCaption1
        '
        Me.ChkCaption1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCaption1.Location = New System.Drawing.Point(8, 36)
        Me.ChkCaption1.Name = "ChkCaption1"
        Me.ChkCaption1.Size = New System.Drawing.Size(106, 17)
        Me.ChkCaption1.TabIndex = 0
        Me.ChkCaption1.Text = "Reg. Caption1"
        Me.ChkCaption1.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOk)
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 138)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(343, 55)
        Me.objFooter.TabIndex = 7
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(131, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 128
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(234, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 127
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmCompanyOtherCaptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(343, 193)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCompanyOtherCaptions"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Company Other Captions"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbOtherRegCaptions.ResumeLayout(False)
        Me.gbOtherRegCaptions.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents gbOtherRegCaptions As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents ChkCaption3 As System.Windows.Forms.CheckBox
    Friend WithEvents ChkCaption2 As System.Windows.Forms.CheckBox
    Friend WithEvents ChkCaption1 As System.Windows.Forms.CheckBox
    Friend WithEvents txtRegCaption3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRegCaption2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRegCaption1 As eZee.TextBox.AlphanumericTextBox
End Class
