﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports Microsoft.Exchange.WebServices.Data 'Sohail (30 Nov 2017)

Public Class frmCompany_AddEdit

#Region " Private Function "
    Private ReadOnly mstrModuleName As String = "frmCompany_AddEdit"
    Dim mblnFrmLoad As Boolean = False
    Private mblnCancel As Boolean = True
    Private objCompany As clsCompany_Master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintCompanyId As Integer = -1
    'Sandeep [ 01 MARCH 2011 ] -- Start
    Private mdtTran As DataTable
    Private objCBankTran As clsCompany_Bank_tran
    Private mintBankTranIdx As Integer = -1
    'Sandeep [ 01 MARCH 2011 ] -- End 

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtOldTable As DataTable
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 03 OCT 2012 ] -- END

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCompanyId = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintCompanyId

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Medthos "
    Private Sub SetColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtAccountNo.BackColor = GUI.ColorOptional
            txtCompanyName.BackColor = GUI.ColorComp
            txtAddressLine1.BackColor = GUI.ColorOptional
            txtAddressLine2.BackColor = GUI.ColorOptional
            txtBankNo.BackColor = GUI.ColorOptional
            txtCompamyRegNo.BackColor = GUI.ColorOptional
            txtCompanyEmail.BackColor = GUI.ColorOptional
            txtDistrictNo.BackColor = GUI.ColorOptional
            txtFax.BackColor = GUI.ColorOptional
            txtMailSenderEmail.BackColor = GUI.ColorOptional
            txtMailSenderName.BackColor = GUI.ColorOptional
            txtMailServer.BackColor = GUI.ColorOptional
            txtMailServerPort.BackColor = GUI.ColorOptional
            txtNSSFNo.BackColor = GUI.ColorOptional
            txtPassword.BackColor = GUI.ColorOptional
            txtPayrollNo.BackColor = GUI.ColorOptional
            txtPhone1.BackColor = GUI.ColorOptional
            txtPhone2.BackColor = GUI.ColorOptional
            txtPhone3.BackColor = GUI.ColorOptional
            txtPPFNo.BackColor = GUI.ColorOptional
            txtReference.BackColor = GUI.ColorOptional
            txtRegCaption1Value.BackColor = GUI.ColorOptional
            txtRegCaption2Value.BackColor = GUI.ColorOptional
            txtRegCaption3Value.BackColor = GUI.ColorOptional
            txtRegdNo.BackColor = GUI.ColorOptional
            txtTinNo.BackColor = GUI.ColorOptional
            txtUserName.BackColor = GUI.ColorOptional
            txtVRNNo.BackColor = GUI.ColorOptional
            txtWebsite.BackColor = GUI.ColorOptional
            cboBank.BackColor = GUI.ColorOptional
            cboBranch.BackColor = GUI.ColorOptional
            cboCity.BackColor = GUI.ColorOptional
            cboCountry.BackColor = GUI.ColorOptional
            cboFromMonth.BackColor = GUI.ColorOptional
            cboFromYear.BackColor = GUI.ColorOptional
            cboZipcode.BackColor = GUI.ColorOptional
            cboState.BackColor = GUI.ColorOptional
            cboToMonth.BackColor = GUI.ColorOptional
            cboToYear.BackColor = GUI.ColorOptional
            nudDay.BackColor = GUI.ColorOptional
            nudToDay.BackColor = GUI.ColorOptional
            txtBody.BackColor = GUI.ColorOptional
            'Sandeep [ 01 MARCH 2011 ] -- Start
            cboAccountType.BackColor = GUI.ColorOptional
            'Sandeep [ 01 MARCH 2011 ] -- End 
            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            cboAuthenticationProtocol.BackColor = GUI.ColorOptional
            'Hemant (01 Feb 2022) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try

            objCompany._Code = txtCode.Text
            objCompany._Name = txtCompanyName.Text
            objCompany._Address1 = txtAddressLine1.Text
            objCompany._Address2 = txtAddressLine2.Text
            objCompany._Countryunkid = CInt(cboCountry.SelectedValue)
            objCompany._Stateunkid = CInt(cboState.SelectedValue)
            objCompany._Cityunkid = CInt(cboCity.SelectedValue)
            objCompany._Postalunkid = CInt(cboZipcode.SelectedValue)
            objCompany._Phone1 = txtPhone1.Text
            objCompany._Phone2 = txtPhone2.Text
            objCompany._Phone3 = txtPhone3.Text
            objCompany._Fax = txtFax.Text
            objCompany._Email = txtCompanyEmail.Text
            objCompany._Website = txtWebsite.Text
            objCompany._Registerdno = txtRegdNo.Text
            objCompany._Tinno = txtTinNo.Text
            objCompany._Vatno = txtVRNNo.Text
            objCompany._Nssfno = txtNSSFNo.Text
            objCompany._Ppfno = txtPPFNo.Text
            objCompany._Payrollno = txtPayrollNo.Text
            objCompany._District = txtDistrictNo.Text
            objCompany._Bank_No = txtBankNo.Text
            objCompany._Accountno = txtAccountNo.Text
            objCompany._Bankgroupunkid = CInt(cboBank.SelectedValue)
            objCompany._Branchunkid = CInt(cboBranch.SelectedValue)
            objCompany._Sendername = txtMailSenderName.Text
            objCompany._Senderaddress = txtMailSenderEmail.Text
            objCompany._Reference = txtReference.Text
            objCompany._Mailserverip = txtMailServer.Text
            objCompany._Mailserverport = CInt(txtMailServerPort.Text)
            objCompany._Username = txtUserName.Text
            objCompany._Password = txtPassword.Text
            objCompany._Isloginssl = chkSSL.Checked
            objCompany._Company_Reg_No = txtCompamyRegNo.Text
            objCompany._Fin_Start_Day = CInt(nudDay.Value)
            objCompany._Fin_Start_Month = CInt(cboFromMonth.SelectedValue)
            objCompany._Fin_Start_Year = CInt(cboFromYear.Text)
            objCompany._Fin_End_Day = CInt(nudToDay.Value)
            objCompany._Fin_End_Month = CInt(cboToMonth.SelectedValue)
            objCompany._Fin_End_Year = CInt(cboToYear.Text)
            objCompany._Isreg1captionused = CBool(IIf(objlblCaption1.Text.Trim = "", 0, 1))
            objCompany._Reg1_Caption = objlblCaption1.Text
            objCompany._Reg1_Value = txtRegCaption1Value.Text
            objCompany._Isreg2captionused = CBool(IIf(objlblCaption2.Text.Trim = "", 0, 1))
            objCompany._Reg2_Caption = objlblCaption2.Text
            objCompany._Reg2_Value = txtRegCaption2Value.Text
            objCompany._Isreg3captionused = CBool(IIf(objlblCaption3.Text.Trim = "", 0, 1))
            objCompany._Reg3_Caption = objlblCaption3.Text
            objCompany._Reg3_Value = txtRegCaption3Value.Text
            objCompany._Mail_Body = txtBody.Text
            objCompany._Image = ImgCompanyLogo._Image
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            objCompany._Localization_Country = CInt(cboLocalCountry.SelectedValue)
            objCompany._Localization_Currency = txtDefaultCurrency.Text
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            objCompany._Stamp = ImgCompanyStamp._Image 'Sohail (27 Mar 2014)
            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
            objCompany._IsCertificateAuthorization = chkCertificateAuthentication.Checked
            'S.SANDEEP [11-AUG-2017] -- END
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            If radSMTP.Checked = True Then
                objCompany._Email_Type = 0 '0 = SMTP, 1 = EWS
            ElseIf radExchangeWebService.Checked = True Then
                objCompany._Email_Type = 1 '0 = SMTP, 1 = EWS
            Else
                objCompany._Email_Type = 0 '0 = SMTP, 1 = EWS
            End If
            objCompany._EWS_URL = txtEWSUrl.Text
            objCompany._EWS_Domain = txtEWSDomain.Text
            'Sohail (30 Nov 2017) -- End

            'S.SANDEEP [21-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002552}
            objCompany._Isbypassproxy = chkBypassProxyServer.Checked
            'S.SANDEEP [21-SEP-2018] -- END

            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            objCompany._Protocolunkid = CInt(cboAuthenticationProtocol.SelectedValue)
            'Hemant (01 Feb 2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objCompany._Code
            txtCompanyName.Text = objCompany._Name
            txtAddressLine1.Text = objCompany._Address1
            txtAddressLine2.Text = objCompany._Address2
            cboCountry.SelectedValue = objCompany._Countryunkid
            cboState.SelectedValue = objCompany._Stateunkid
            cboCity.SelectedValue = objCompany._Cityunkid
            cboZipcode.SelectedValue = objCompany._Postalunkid
            txtPhone1.Text = objCompany._Phone1
            txtPhone2.Text = objCompany._Phone2
            txtPhone3.Text = objCompany._Phone3
            txtFax.Text = objCompany._Fax
            txtCompanyEmail.Text = objCompany._Email
            txtWebsite.Text = objCompany._Website
            txtRegdNo.Text = objCompany._Registerdno
            txtTinNo.Text = objCompany._Tinno
            txtVRNNo.Text = objCompany._Vatno
            txtNSSFNo.Text = objCompany._Nssfno
            txtPPFNo.Text = objCompany._Ppfno
            txtPayrollNo.Text = objCompany._Payrollno
            txtDistrictNo.Text = objCompany._District
            txtBankNo.Text = objCompany._Bank_No
            txtAccountNo.Text = objCompany._Accountno
            cboBank.SelectedValue = objCompany._Bankgroupunkid
            cboBranch.SelectedValue = objCompany._Branchunkid
            txtMailSenderName.Text = objCompany._Sendername
            txtMailSenderEmail.Text = objCompany._Senderaddress
            txtReference.Text = objCompany._Reference
            txtMailServer.Text = objCompany._Mailserverip
            txtMailServerPort.Text = CStr(IIf(CInt(objCompany._Mailserverport) = 0, 25, CStr(objCompany._Mailserverport)))
            txtUserName.Text = objCompany._Username
            txtPassword.Text = objCompany._Password
            chkSSL.Checked = objCompany._Isloginssl
            txtCompamyRegNo.Text = objCompany._Company_Reg_No
            nudDay.Value = objCompany._Fin_Start_Day
            cboFromMonth.SelectedValue = IIf(objCompany._Fin_Start_Month = 0, 1, objCompany._Fin_Start_Month)
            cboFromYear.Text = CStr(IIf(CInt(objCompany._Fin_Start_Year) = 0, Now.Date.Year, CStr(objCompany._Fin_Start_Year)))
            nudToDay.Value = objCompany._Fin_End_Day
            cboToMonth.SelectedValue = objCompany._Fin_End_Month
            cboToYear.Text = CStr(IIf(CInt(objCompany._Fin_End_Year) = 0, Now.Date.Year, CStr(objCompany._Fin_End_Year)))
            objlblCaption1.Text = objCompany._Reg1_Caption
            txtRegCaption1Value.Text = objCompany._Reg1_Value
            objlblCaption2.Text = objCompany._Reg2_Caption
            txtRegCaption2Value.Text = objCompany._Reg2_Value
            objlblCaption3.Text = objCompany._Reg3_Caption
            txtRegCaption3Value.Text = objCompany._Reg3_Value
            txtBody.Text = objCompany._Mail_Body
            ImgCompanyLogo._Image = objCompany._Image
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            cboLocalCountry.SelectedValue = objCompany._Localization_Country
            txtDefaultCurrency.Text = objCompany._Localization_Currency
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            ImgCompanyStamp._Image = objCompany._Stamp 'Sohail (27 Mar 2014)

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
            chkCertificateAuthentication.Checked = objCompany._IsCertificateAuthorization
            'S.SANDEEP [11-AUG-2017] -- END
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            If objCompany._Email_Type = 0 Then '0 = SMTP, 1 = EWS
                radSMTP.Checked = True
            ElseIf objCompany._Email_Type = 1 Then '0 = SMTP, 1 = EWS
                radExchangeWebService.Checked = True
            Else
                radSMTP.Checked = True
            End If
            txtEWSUrl.Text = objCompany._EWS_URL
            txtEWSDomain.Text = objCompany._EWS_Domain
            'Sohail (30 Nov 2017) -- End

            'S.SANDEEP [21-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002552}
            chkBypassProxyServer.Checked = objCompany._Isbypassproxy
            'S.SANDEEP [21-SEP-2018] -- END

            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            cboAuthenticationProtocol.SelectedValue = CInt(objCompany._Protocolunkid)
            'Hemant (01 Feb 2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim ObjMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Dim objBankGrp As New clspayrollgroup_master
        'Sandeep [ 01 MARCH 2011 ] -- Start
        'Dim objBankBranch As New clsbankbranch_master
        Dim objAccType As New clsBankAccType
        'Sandeep [ 01 MARCH 2011 ] -- End 
        Try
            dsCombos = ObjMasterData.getMonthList("FromMonth")
            With cboFromMonth
                .ValueMember = "MonthId"
                .DisplayMember = "MonthName"
                .DataSource = dsCombos.Tables("FromMonth")
            End With
            cboFromMonth.SelectedIndex = 0

            dsCombos = ObjMasterData.getMonthList("ToMonth")
            With cboToMonth
                .ValueMember = "MonthId"
                .DisplayMember = "MonthName"
                .DataSource = dsCombos.Tables("ToMonth")
            End With
            cboToMonth.SelectedIndex = 0

            dsCombos = ObjMasterData.getYearList("FromYear")
            With cboFromYear
                .DisplayMember = dsCombos.Tables("FromYear").Columns(0).ColumnName
                .DataSource = dsCombos.Tables("FromYear")
            End With
            cboFromYear.SelectedIndex = cboFromYear.FindStringExact(CStr(Now.Date.Year))

            dsCombos = ObjMasterData.getYearList("ToYear")
            With cboToYear
                .DisplayMember = dsCombos.Tables("ToYear").Columns(0).ColumnName
                .DataSource = dsCombos.Tables("ToYear")
            End With
            cboFromYear.SelectedIndex = cboToYear.FindStringExact(CStr(Now.Date.Year))

            dsCombos = ObjMasterData.getCountryList("PresentCountry", True)
            With cboCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("PresentCountry").Copy
            End With

            With cboLocalCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("PresentCountry").Copy
            End With

            dsCombos = objBankGrp.getListForCombo(PayrollGroupType.Bank, "BankGrp", True)
            With cboBank
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("BankGrp")
            End With

            'Sandeep [ 01 MARCH 2011 ] -- Start
            'dsCombos = objBankBranch.getListForCombo("Branch", True)
            'With cboBranch
            '    .ValueMember = "branchunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Branch")
            'End With

            dsCombos = objAccType.getComboList(True, "AccType")
            With cboAccountType
                .ValueMember = "accounttypeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AccType")
                .SelectedValue = 0
            End With
            'Sandeep [ 01 MARCH 2011 ] -- End 


            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            dsCombos = ObjMasterData.getComboListForEmailProtocol("List")
            With cboAuthenticationProtocol
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List").Copy
            End With
            'Hemant (01 Feb 2022) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetAccessibility()
        If objlblCaption1.Text.Trim = "" Then
            txtRegCaption1Value.Enabled = False
            txtRegCaption1Value.Text = ""
        Else
            txtRegCaption1Value.Enabled = True
        End If
        If objlblCaption2.Text.Trim = "" Then
            txtRegCaption2Value.Enabled = False
            txtRegCaption2Value.Text = ""
        Else
            txtRegCaption2Value.Enabled = True
        End If

        If objlblCaption3.Text.Trim = "" Then
            txtRegCaption3Value.Enabled = False
            txtRegCaption3Value.Text = ""
        Else
            txtRegCaption3Value.Enabled = True
        End If
    End Sub

    'Sandeep [ 01 MARCH 2011 ] -- Start
    Private Sub FillBankTranList()
        Try
            dgvBankInfo.AutoGenerateColumns = False

            Dim dtView As New DataView(mdtTran, "AUD <> 'D'", "", DataViewRowState.CurrentRows)

            dgcolhAccType.DataPropertyName = "AccName"
            dgcolhAccTypeNo.DataPropertyName = "account_no"
            dgcolhBankGroup.DataPropertyName = "BankName"
            dgcolhBranch.DataPropertyName = "BranchName"
            objdgcolhAccTypeId.DataPropertyName = "accounttypeunkid"
            objdgcolhBankId.DataPropertyName = "groupmasterunkid"
            objdgcolhBankNo.DataPropertyName = "bank_no"
            objdgcolhBranchId.DataPropertyName = "branchunkid"
            objdgcolhCompanyId.DataPropertyName = "companyunkid"
            objdgcolhGuid.DataPropertyName = "GUID"
            objdgcolhTranId.DataPropertyName = "companybanktranunkid"
            objdgcolhAUD.DataPropertyName = "AUD"
            objdgcolhIsBeingEdit.DataPropertyName = "isBeingEdit"

            If dgvBankInfo.RowCount > 0 Then
                If dgvBankInfo.SelectedRows.Count > 0 Then
                    dgvBankInfo.SelectedRows(0).Selected = False
                End If
            End If

            dgvBankInfo.DataSource = dtView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBankTranList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetBankTranList()
        Try
            cboAccountType.SelectedValue = 0
            cboBank.SelectedValue = 0
            cboBranch.SelectedValue = 0
            txtAccountNo.Text = ""
            txtBankNo.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetBankTranList", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 01 MARCH 2011 ] -- End 

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If ConfigParameter._Object._Notify_Bank_Users.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 37, "Dear") & " <b>" & getTitleCase(StrUserName) & "</b></span></p>")

                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for company : <b>" & txtCompanyName.Text & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 32, "This is to inform you that changes have been made for company") & " " & "<b>" & getTitleCase(txtCompanyName.Text) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with code : <b>" & txtCode.Text & "</b>. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage(mstrModuleName, 33, "with code") & " " & "<b>" & txtCode.Text & " " & "</b>." & " " & Language.getMessage(mstrModuleName, 34, "Following information has been changed by user") & " " & "<b>" & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage(mstrModuleName, 35, "from Machine") & " " & "<b>" & " " & getHostName.ToString & "</b>" & " " & Language.getMessage(mstrModuleName, 36, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                For Each sId As String In ConfigParameter._Object._Notify_Bank_Users.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enNotificationBank.COMPANY_BANK
                            '///////////// NEWLY ADDED ROWS -- START
                            Dim dTemp() As DataRow = mdtTran.Select("AUD = 'A'")
                            If dTemp.Length > 0 Then
                                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TR WIDTH = '50%' bgcolor= '#58ACFA'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '5'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 25, "NEWLY ADDED INFORMATION") & "</span></b></TD>")
                                StrMessage.Append("</TR>")
                                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhBankGroup.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhBranch.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhAccType.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhAccTypeNo.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 26, "Bank No.") & "</span></b></TD>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TR>")
                                For i As Integer = 0 To dTemp.Length - 1
                                    StrMessage.Append("<TR WIDTH = '50%'>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BankName").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BranchName").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("AccName").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("account_no").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("bank_no").ToString & "</span></TD>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("</TR>")
                                Next
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TABLE>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<BR>")
                            End If
                            '///////////// NEWLY ADDED ROWS -- END

                            '///////////// EDITED ROWS -- START
                            dTemp = mdtTran.Select("AUD = 'U' AND companybanktranunkid > 0")
                            Dim iRowIdx As Integer = -1
                            If dTemp.Length > 0 Then
                                StrMessage.Append("<TABLE border = '1' WIDTH = '60%'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TR WIDTH = '60%' bgcolor= '#58ACFA'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '6'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 17, "EDITED INFORMATION") & "</span></b></TD>")
                                StrMessage.Append("</TR>")
                                StrMessage.Append("<TR WIDTH = '60%' bgcolor= 'SteelBlue'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 18, "Value Type") & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhBankGroup.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhBranch.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhAccType.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhAccTypeNo.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 26, "Bank No.") & "</span></b></TD>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TR>")
                                For i As Integer = 0 To dTemp.Length - 1
                                    iRowIdx = -1
                                    iRowIdx = mdtTran.Rows.IndexOf(dTemp(i))
                                    If iRowIdx >= 0 Then
                                        StrMessage.Append("<TR WIDTH = '60%'>")
                                        StrMessage.Append(vbCrLf)
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 19, "Old") & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("BankName").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("BranchName").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("AccName").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("account_no").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("bank_no").ToString & "</span></TD>")
                                        StrMessage.Append(vbCrLf)
                                        StrMessage.Append("</TR>")

                                        StrMessage.Append("<TR WIDTH = '60%'>")
                                        StrMessage.Append(vbCrLf)
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 20, "New") & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BankName").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BranchName").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("AccName").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("account_no").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("bank_no").ToString & "</span></TD>")
                                        StrMessage.Append(vbCrLf)
                                        StrMessage.Append("</TR>")

                                        If dTemp.Length > 1 Then
                                            StrMessage.Append("<TR WIDTH = '60%'>")
                                            StrMessage.Append(vbCrLf)
                                            StrMessage.Append("<TD COLSPAN='6'> &nbsp; </TD>")
                                            StrMessage.Append(vbCrLf)
                                            StrMessage.Append("</TR>")
                                        End If
                                    End If
                                Next
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TABLE>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<BR>")
                            End If
                            '///////////// EDITED ROWS -- END

                            '///////////// DELETED ROWS -- START
                            dTemp = mdtTran.Select("AUD = 'D' AND companybanktranunkid > 0")
                            If dTemp.Length > 0 Then
                                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TR WIDTH = '50%' bgcolor= '#58ACFA'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '5'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 21, "DELETED INFORMATION") & "</span></b></TD>")
                                StrMessage.Append("</TR>")
                                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhBankGroup.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhBranch.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhAccType.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dgcolhAccTypeNo.HeaderText & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 26, "Bank No.") & "</span></b></TD>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TR>")
                                For i As Integer = 0 To dTemp.Length - 1
                                    StrMessage.Append("<TR WIDTH = '50%'>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BankName").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BranchName").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("AccName").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("account_no").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("bank_no").ToString & "</span></TD>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("</TR>")
                                Next
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TABLE>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<BR>")
                            End If
                            '///////////// DELETED ROWS -- END
                            blnFlag = True
                    End Select
                Next
                StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                If blnFlag = False Then
                    StrMessage = StrMessage.Remove(0, StrMessage.Length)
                End If
            End If
            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification(ByVal intCompanyUnkId As Object)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            If dicNotification.Keys.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each sKey As String In dicNotification.Keys
                    If dicNotification(sKey).Trim.Length > 0 Then
                        objSendMail._ToEmail = sKey
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 22, "Notification of Changes in Company Bank(s).")
                        objSendMail._Message = dicNotification(sKey)
With objSendMail
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
                            ._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        'Sohail (13 Dec 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail(CInt(intCompanyUnkId))
                        If TypeOf intCompanyUnkId Is Integer Then
                        objSendMail.SendMail(CInt(intCompanyUnkId))
                        Else
                            Dim arr() As Object = CType(intCompanyUnkId, Object())
                            objSendMail.SendMail(CInt(arr(0)))
                        End If
                        'Sohail (13 Dec 2017) -- End
                        'Sohail (30 Nov 2017) -- End
                    End If
                Next
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 03 OCT 2012 ] -- END

    'Sohail (30 Nov 2017) -- Start
    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    Private Function IsValidateEWSMailSetting() As Boolean
        Try
            If txtMailSenderEmail.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter valid Email address of the sender."), enMsgBoxStyle.Information)
                txtMailSenderEmail.Focus()
                Return False

            ElseIf txtEWSUrl.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Please enter EWS URL."), enMsgBoxStyle.Information)
                txtEWSUrl.Focus()
                Return False

            ElseIf txtUserName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Please enter User Name."), enMsgBoxStyle.Information)
                txtUserName.Focus()
                Return False

            ElseIf txtPassword.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Please enter Password."), enMsgBoxStyle.Information)
                txtPassword.Focus()
                Return False

            ElseIf txtEWSDomain.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Please enter EWS Domain."), enMsgBoxStyle.Information)
                txtEWSDomain.Focus()
                Return False

            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidateEWSMailSetting", mstrModuleName)
        End Try
    End Function
    'Sohail (30 Nov 2017) -- End

#End Region

#Region " Form's Events "
    Private Sub frmCompany_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objCompany = Nothing
    End Sub

    Private Sub frmCompany_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompany_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompany_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompany_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompany_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCompany = New clsCompany_Master

        'Sandeep [ 01 MARCH 2011 ] -- Start
        objCBankTran = New clsCompany_Bank_tran
        'Sandeep [ 01 MARCH 2011 ] -- End 


        'Call Language.setLanguage(Me.Name)
        'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Call OtherSettings()
            Call SetColor()
            Call FillCombo()

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            pnlExchangeWebService.Location = New System.Drawing.Point(lblServerInformation.Location.X - 3, lblServerInformation.Location.Y + 17)
            pnlExchangeWebService.Size = New System.Drawing.Size(357, 57)
            'Sohail (30 Nov 2017) -- End

            If menAction = enAction.EDIT_ONE Then
                txtCode.Enabled = False
                objCompany._Companyunkid = mintCompanyId
                'S.SANDEEP [ 12 OCT 2011 ] -- START
                gbLocalization.Enabled = False
                'S.SANDEEP [ 12 OCT 2011 ] -- END
            End If

            Call GetValue()

            mblnFrmLoad = True

            'nudToDay.Value = 1
            nudToDay.Minimum = 1

            'S.SANDEEP [21-DEC-2017] -- START
            'ISSUE/ENHANCEMENT : STOP AGAIN SETTING VALUE TO 1 WHEN COMES IN EDIT MODE
            'nudDay.Value = 1
            If menAction <> enAction.EDIT_ONE Then
            nudDay.Value = 1
            End If
            'S.SANDEEP [21-DEC-2017] -- END

            nudDay.Minimum = 1

            Call SetAccessibility()

            'Sandeep [ 07 FEB 2011 ] -- START
            'ImgCompanyLogo._FilePath = My.Computer.FileSystem.SpecialDirectories.MyPictures
            Dim objAppSettings As New clsApplicationSettings
            ImgCompanyLogo._FilePath = objAppSettings._ApplicationPath & "Data\Images"
            'Sandeep [ 07 FEB 2011 ] -- END 

            txtCode.Focus()

            'Sandeep [ 01 MARCH 2011 ] -- Start
            objCBankTran._CompanyId = mintCompanyId
            mdtTran = objCBankTran._SetDataTable
            Call FillBankTranList()
            'Sandeep [ 01 MARCH 2011 ] -- End 

            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtOldTable = mdtTran.Copy
            'S.SANDEEP [ 03 OCT 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompany_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCompany_Master.SetMessages()
            clsCompany_Bank_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsCompany_Master,clsCompany_Bank_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " Button's Events "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtCompanyName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Company Name cannot be blank. Company Name is required information."), enMsgBoxStyle.Information)
                txtCompanyName.Focus()
                Exit Sub
            End If


            'Anjan [ 14 August 2013 ] -- Start
            'ENHANCEMENT : Recruitment TRA changes requested by Andrew
            If CInt(cboCountry.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Country is mandatory information. Please select Country to continue."), enMsgBoxStyle.Information)
                cboCountry.Focus()
                Exit Sub
            End If
            'Anjan [ 14 August 2013 ] -- End

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            If radExchangeWebService.Checked = True Then
                If IsValidateEWSMailSetting() = False Then Exit Sub
            End If
            'Sohail (30 Nov 2017) -- End




            'S.SANDEEP [ 12 OCT 2011 ] -- START
            If menAction <> enAction.EDIT_ONE Then
                If CInt(cboLocalCountry.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Localization country is mandatory information. Please select Localization country to continue."), enMsgBoxStyle.Information)
                    tabcOtherInfo.SelectedTab = tabpLocalization
                    cboLocalCountry.Focus()
                    Exit Sub
                End If

                'Sohail (10 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                If ConfigParameter._Object._IsArutiDemo = False AndAlso (Company._Object._Total_Active_Company + 1) > ConfigParameter._Object._NoOfCompany Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, You have Registered for ") & ConfigParameter._Object._NoOfCompany & Language.getMessage(mstrModuleName, 27, " companies and it is exceeding the limit of Maximum No. of Active Companies. Please contact Aruti team to register for more companies."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Sohail (10 Apr 2013) -- End
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES


            'Pinkal (03-Sep-2013) -- Start
            'Enhancement : TRA Changes

            Dim dtView As DataTable = CType(dgvBankInfo.DataSource, DataView).Table
            Dim drRow() As DataRow = dtView.Select("AUD <> ''")
            If drRow.Length > 0 Then

            ConfigParameter._Object._Companyunkid = mintCompanyId
            Company._Object._Companyunkid = mintCompanyId
            If ConfigParameter._Object._Notify_Bank_Users.Trim.Length > 0 Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                For Each sId As String In ConfigParameter._Object._Notify_Bank_Users.Split(CChar("||"))(2).Split(CChar(","))
                    objUsr._Userunkid = CInt(sId)
                    StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                    If dicNotification.ContainsKey(objUsr._Email) = False Then
                        dicNotification.Add(objUsr._Email, StrMessage)
                    End If
                Next
                objUsr = Nothing
            End If

            End If
            'S.SANDEEP [ 03 OCT 2012 ] -- END

            'Pinkal (03-Sep-2013) -- End

            Call SetValue()


            'Sandeep [ 01 MARCH 2011 ] -- Start
            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objCompany.Update()
            'Else
            '    blnFlag = objCompany.Insert()
            'End If


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            Me.Cursor = Cursors.WaitCursor
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            frmMDI.tmrReminder.Enabled = False

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCompany._FormName = mstrModuleName
            objCompany._LoginEmployeeunkid = 0
            objCompany._ClientIP = getIP()
            objCompany._HostName = getHostName()
            objCompany._FromWeb = False
            objCompany._AuditUserId = User._Object._Userunkid            
            objCompany._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objCompany.Update(mdtTran)
            Else
                blnFlag = objCompany.Insert(mdtTran)
            End If
            'Sandeep [ 01 MARCH 2011 ] -- End 



            If blnFlag = False And objCompany._Message <> "" Then
                eZeeMsgBox.Show(objCompany._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                txtCode.Tag = CInt(objCompany._Companyunkid)
                'Sohail (30 Nov 2017) -- End
                If menAction = enAction.ADD_CONTINUE Then
                    objCompany = Nothing
                    objCompany = New clsCompany_Master
                    Call GetValue()
                    txtCompanyName.Focus()


                Else
                    mintCompanyId = objCompany._Companyunkid
                    Me.Close()
                End If
            End If

            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'trd.Start()
                Dim arr(1) As Object
                arr(0) = CInt(txtCode.Tag)
                trd.Start(arr)
                'Sohail (30 Nov 2017) -- End
            End If
            'S.SANDEEP [ 03 OCT 2012 ] -- END

        Catch ex As Exception

            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
            'Sandeep [ 01 MARCH 2011 ] -- Start
        Finally
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            frmMDI.tmrReminder.Enabled = True
            Me.Cursor = Cursors.Default
            'Sandeep [ 01 MARCH 2011 ] -- End 
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnTestMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestMail.Click
        'Hemant (01 Feb 2022) -- Start
        'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.  
        Dim _Tls12 As System.Security.Authentication.SslProtocols = CType(&HC00, System.Security.Authentication.SslProtocols)
        Dim Tls12 As System.Net.SecurityProtocolType = CType(_Tls12, System.Net.SecurityProtocolType)
        'Hemant (01 Feb 2022) -- End
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            If radSMTP.Checked = True Then
                'Sohail (30 Nov 2017) -- End

            If txtMailSenderEmail.Text = "" Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter valid Email address of the sender."), enMsgBoxStyle.Information)
                Exit Sub
            End If

                'Sohail (18 Jul 2019) -- Start
                'Internal Enhancement - Ref #  - 76.1 - Show Sender Name instead of sender email in mail box.
                'Dim objMail As New System.Net.Mail.MailMessage(txtMailSenderEmail.Text, txtMailSenderEmail.Text)
                Dim strSenderDispName As String = txtMailSenderName.Text
                If strSenderDispName.Trim.Length <= 0 Then
                    strSenderDispName = txtMailSenderEmail.Text
                End If
                Dim objMail As New System.Net.Mail.MailMessage(New System.Net.Mail.MailAddress(txtMailSenderEmail.Text, strSenderDispName), New System.Net.Mail.MailAddress(txtMailSenderEmail.Text))
                'Sohail (18 Jul 2019) -- End
            objMail.Body = txtBody.Text
            objMail.Subject = txtReference.Text
            objMail.IsBodyHtml = True
            Dim SmtpMail As New System.Net.Mail.SmtpClient()
            SmtpMail.Host = txtMailServer.Text
            SmtpMail.Port = CInt(Val(txtMailServerPort.Text))
            If txtUserName.Text <> "" Then
                SmtpMail.Credentials = New System.Net.NetworkCredential(txtUserName.Text, txtPassword.Text)
            End If
            SmtpMail.EnableSsl = chkSSL.Checked

                'S.SANDEEP [21-SEP-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002552}
                If chkBypassProxyServer.Checked Then
                    System.Net.ServicePointManager.Expect100Continue = False
                End If
                'S.SANDEEP [21-SEP-2018] -- END

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
            If chkCertificateAuthentication.Checked Then
                System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
            End If
            'SmtpMail.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
            'S.SANDEEP [11-AUG-2017] -- END

                'Hemant (01 Feb 2022) -- Start
                'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                If SmtpMail.Port = 587 Then
                    If CInt(cboAuthenticationProtocol.SelectedValue) = enAuthenticationProtocol.TLS Then
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls
                    ElseIf CInt(cboAuthenticationProtocol.SelectedValue) = enAuthenticationProtocol.SSL3 Then
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3
                    ElseIf CInt(cboAuthenticationProtocol.SelectedValue) = enAuthenticationProtocol.TLS12 Then
                        System.Net.ServicePointManager.SecurityProtocol = Tls12
                    Else
                    End If
                End If
                'Hemant (01 Feb 2022) -- End
            SmtpMail.Send(objMail)

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            ElseIf radExchangeWebService.Checked = True Then

                If IsValidateEWSMailSetting() = False Then Exit Try

                'Creating the ExchangeService Object 
                Dim objService As New ExchangeService()

                'Seting Credentials to be used
                objService.Credentials = New WebCredentials(txtUserName.Text, txtPassword.Text, txtEWSDomain.Text)

                'Setting the EWS Uri
                objService.Url = New Uri(txtEWSUrl.Text)

                'Creating an Email Service and passing in the service
                Dim objMessage As New EmailMessage(objService)

                'Setting Email message properties
                objMessage.Subject = txtReference.Text
                objMessage.Body = txtBody.Text
                objMessage.ToRecipients.Add(txtMailSenderEmail.Text)

                'Sending the email and saving a copy.
                'objMessage.Send()
                objMessage.SendAndSaveCopy()

            End If
            'Sohail (30 Nov 2017) -- End

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Mail sent Successfully..."))

        Catch ex As Exception
            If ex.Message.Contains("Fail") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 11, "Please check your Internet settings."), enMsgBoxStyle.Information)
            ElseIf ex.Message.Contains("secure") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 12, "Please Uncheck Login using SSL setting."), enMsgBoxStyle.Information)
            Else
                DisplayError.Show("-1", ex.Message, "btnTestMail_Click", mstrModuleName)
            End If

        End Try
    End Sub
#End Region

    'Sohail (30 Nov 2017) -- Start
    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
#Region " Radio Buttons Events "

    Private Sub radSMTP_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSMTP.CheckedChanged
        Try
            pnlExchangeWebService.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radSMTP_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radExchangeWebService_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExchangeWebService.CheckedChanged
        Try
            pnlExchangeWebService.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExchangeWebService_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (30 Nov 2017) -- End

#Region " Other Events "
    Private Sub nudDay_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudDay.ValueChanged, cboFromMonth.SelectedIndexChanged, cboFromYear.SelectedIndexChanged
        Try
            If mblnFrmLoad = False Then Exit Sub
            Dim dtTemp As Date
            Dim dtTemp1 As Date
            dtTemp = New Date(CInt(cboFromYear.Text), CInt(cboFromMonth.SelectedValue), CInt(nudDay.Value))
            dtTemp1 = DateAdd(DateInterval.Day, -1, dtTemp)
            nudToDay.Value = dtTemp1.Day
            cboToMonth.SelectedValue = dtTemp1.Month
            If CInt(cboFromMonth.SelectedValue) > 1 Then
                cboToYear.Text = CStr(CInt(cboFromYear.Text) + 1)
                If CInt(cboToYear.FindStringExact(CStr(CInt(cboFromYear.Text) + 1))) > 0 Then
                    cboToYear.Text = CStr(CInt(cboFromYear.Text) + 1)
                Else
                    Dim dsSource As New DataTable
                    dsSource = CType(cboToYear.DataSource, DataTable)
                    Dim dtRow As DataRow = dsSource.NewRow
                    dtRow.Item("years") = CStr(CInt(cboFromYear.Text) + 1)
                    dsSource.Rows.Add(dtRow)
                    cboToYear.DataSource = dsSource
                    cboToYear.Text = CStr(CInt(cboFromYear.Text) + 1)
                End If
            ElseIf CInt(cboFromMonth.SelectedValue) = 1 And nudDay.Value = 1 Then
                cboToYear.Text = cboFromYear.Text
            ElseIf nudDay.Value > 1 Then
                cboToYear.Text = CStr(CInt(cboFromYear.Text) + 1)
            End If
        Catch ex As Exception
            nudDay.Value = nudDay.Value - 1
        End Try
    End Sub

    Private Sub lnkEditCaption_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEditCaption.LinkClicked
        Dim objFrm As New frmCompanyOtherCaptions
        Try
            objFrm.displayDialog(objCompany._Reg1_Caption, objCompany._Reg2_Caption, objCompany._Reg3_Caption)
            objlblCaption1.Text = objCompany._Reg1_Caption
            objlblCaption2.Text = objCompany._Reg2_Caption
            objlblCaption3.Text = objCompany._Reg3_Caption
            Call SetAccessibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEditCaption_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub cboCity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCity.SelectedIndexChanged
        Try
            If CInt(cboCity.SelectedValue) > 0 Then
                Dim objZipCode As New clszipcode_master
                Dim dsCombos As New DataSet
                dsCombos = objZipCode.GetList("Zipcode", , True, CInt(cboCity.SelectedValue))
                With cboZipcode
                    .ValueMember = "zipcodeunkid"
                    .DisplayMember = "zipcode_no"
                    .DataSource = dsCombos.Tables("Zipcode").Copy
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCity_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            If CInt(cboState.SelectedValue) > 0 Then
                Dim objCity As New clscity_master
                Dim dsCombos As New DataSet
                dsCombos = objCity.GetList("City", , True, CInt(cboState.SelectedValue))
                With cboCity
                    .ValueMember = "cityunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("City").Copy
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            If CInt(cboCountry.SelectedValue) > 0 Then
                Dim objState As New clsstate_master
                Dim dsCombos As New DataSet
                dsCombos = objState.GetList("State", , True, CInt(cboCountry.SelectedValue))
                With cboState
                    .ValueMember = "stateunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("State").Copy
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 


    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Private Sub cboLocalCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLocalCountry.SelectedIndexChanged
        Try
            If CInt(cboLocalCountry.SelectedValue) > 0 Then
                Dim objCurrency As New clsMasterData
                Dim dsList As New DataSet
                dsList = objCurrency.getCountryList(, , CInt(cboLocalCountry.SelectedValue))
                txtDefaultCurrency.Text = dsList.Tables(0).Rows(0)("currency").ToString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLocalCountry_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    Private Sub txtMailServerPort_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMailServerPort.LostFocus
        Try
            If txtMailServerPort.Text.Trim.Length <= 0 Then
                txtMailServerPort.Text = CStr(25)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtMailServerPort_LostFocus", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sandeep [ 01 MARCH 2011 ] -- Start
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If CInt(cboBank.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Bank is required information. Please select Bank to continue."), enMsgBoxStyle.Information)
                cboBank.Focus()
                Exit Sub
            End If

            If CInt(cboBranch.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Branch is required information. Please select Branch to continue."), enMsgBoxStyle.Information)
                cboBank.Focus()
                Exit Sub
            End If

            If CInt(cboAccountType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Account Type is required information. Please select Account Type to continue."), enMsgBoxStyle.Information)
                cboBank.Focus()
                Exit Sub
            End If

            If txtAccountNo.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Account no cannot be blank. Account no. is required information."), enMsgBoxStyle.Information)
                txtAccountNo.Focus()
                Exit Sub
            End If

            Dim dtTemp() As DataRow = Nothing

            dtTemp = mdtTran.Select("groupmasterunkid = '" & CInt(cboBank.SelectedValue) & _
                                                    "' AND branchunkid = '" & CInt(cboBranch.SelectedValue) & _
                                                    "' AND accounttypeunkid = '" & CInt(cboAccountType.SelectedValue) & _
                                                    "' AND account_no = '" & txtAccountNo.Text & "'")

            If dtTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "This information is already added to list. Please add another entry."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow

            dtRow = mdtTran.NewRow

            dtRow.Item("companybanktranunkid") = -1
            dtRow.Item("companyunkid") = mintCompanyId
            dtRow.Item("groupmasterunkid") = CInt(cboBank.SelectedValue)
            dtRow.Item("branchunkid") = CInt(cboBranch.SelectedValue)
            dtRow.Item("accounttypeunkid") = CInt(cboAccountType.SelectedValue)
            dtRow.Item("account_no") = txtAccountNo.Text.Trim
            dtRow.Item("bank_no") = txtBankNo.Text.Trim
            dtRow.Item("userunkid") = User._Object._Userunkid
            dtRow.Item("AUD") = "A"
            dtRow.Item("GUID") = Guid.NewGuid.ToString
            dtRow.Item("AccName") = CStr(cboAccountType.Text)
            dtRow.Item("BranchName") = CStr(cboBranch.Text)
            dtRow.Item("BankName") = CStr(cboBank.Text)

            mdtTran.Rows.Add(dtRow)

            Call FillBankTranList()

            Call ResetBankTranList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try

            If mintBankTranIdx = -1 Then Exit Sub

            'S.SANDEEP [ 10 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objCBankTran.Can_Change_AccNo(CStr(dgvBankInfo.Rows(mintBankTranIdx).Cells(dgcolhAccTypeNo.Index).Value)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You cannot change the account number." & vbCrLf & "Reason : This account number is already used in some of the transactions."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 10 NOV 2012 ] -- END

            Dim drTemp() As DataRow = Nothing

            If CInt(dgvBankInfo.Rows(mintBankTranIdx).Cells(objdgcolhTranId.Index).Value) = -1 Then
                drTemp = mdtTran.Select("GUID = '" & CStr(dgvBankInfo.Rows(mintBankTranIdx).Cells(objdgcolhGuid.Index).Value) & "'")
            Else
                drTemp = mdtTran.Select("companybanktranunkid = '" & CStr(dgvBankInfo.Rows(mintBankTranIdx).Cells(objdgcolhTranId.Index).Value) & "'")
            End If

            Dim dtTemp() As DataRow = Nothing

            dtTemp = mdtTran.Select("groupmasterunkid = '" & CInt(cboBank.SelectedValue) & _
                                            "' AND branchunkid = '" & CInt(cboBranch.SelectedValue) & _
                                            "' AND accounttypeunkid = '" & CInt(cboAccountType.SelectedValue) & _
                                            "' AND account_no = '" & txtAccountNo.Text & _
                                            "' AND isBeingEdit = " & False)

            If dtTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "This information is already added to list. Please add another entry."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If drTemp.Length > 0 Then
                With drTemp(0)

                    'Pinkal (03-Sep-2013) -- Start
                    'Enhancement : TRA Changes

                    Dim blnIsChange As Boolean = False
                    If CInt(.Item("companybanktranunkid")) <> CInt(dgvBankInfo.Rows(mintBankTranIdx).Cells(objdgcolhTranId.Index).Value) OrElse CInt(.Item("companyunkid")) <> mintCompanyId _
                       OrElse CInt(.Item("groupmasterunkid")) <> CInt(cboBank.SelectedValue) OrElse CInt(.Item("branchunkid")) <> CInt(cboBranch.SelectedValue) _
                       OrElse CInt(.Item("accounttypeunkid")) <> CInt(cboAccountType.SelectedValue) OrElse CStr(.Item("account_no")) <> txtAccountNo.Text.Trim _
                       OrElse CStr(.Item("bank_no")) <> txtBankNo.Text.Trim Then
                        blnIsChange = True
                    End If


                    .Item("companybanktranunkid") = CInt(dgvBankInfo.Rows(mintBankTranIdx).Cells(objdgcolhTranId.Index).Value)
                    .Item("companyunkid") = mintCompanyId
                    .Item("groupmasterunkid") = CInt(cboBank.SelectedValue)
                    .Item("branchunkid") = CInt(cboBranch.SelectedValue)
                    .Item("accounttypeunkid") = CInt(cboAccountType.SelectedValue)
                    .Item("account_no") = txtAccountNo.Text.Trim
                    .Item("bank_no") = txtBankNo.Text.Trim
                    .Item("userunkid") = User._Object._Userunkid
                    .Item("isBeingEdit") = False

                    'If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                    '    .Item("AUD") = "U"
                    'End If
                    If blnIsChange Then
                        .Item("AUD") = "U"
                    End If

                    .Item("GUID") = Guid.NewGuid.ToString
                    .AcceptChanges()

                    'Pinkal (03-Sep-2013) -- End

                End With
                Call FillBankTranList()
            End If
            Call ResetBankTranList()
            cboAccountType.Enabled = True
            cboBank.Enabled = True
            cboBranch.Enabled = True

            mintBankTranIdx = -1

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If mintBankTranIdx = -1 Then Exit Sub

            'S.SANDEEP [ 10 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objCBankTran.Can_Change_AccNo(CStr(dgvBankInfo.Rows(mintBankTranIdx).Cells(dgcolhAccTypeNo.Index).Value)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, You cannot delete this account." & vbCrLf & "Reason : This account is already used in some of the transactions."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 10 NOV 2012 ] -- END

            Dim drTemp() As DataRow = Nothing

            If CInt(dgvBankInfo.Rows(mintBankTranIdx).Cells(objdgcolhTranId.Index).Value) = -1 Then
                drTemp = mdtTran.Select("GUID = '" & CStr(dgvBankInfo.Rows(mintBankTranIdx).Cells(objdgcolhGuid.Index).Value) & "'")
            Else
                drTemp = mdtTran.Select("companybanktranunkid = '" & CStr(dgvBankInfo.Rows(mintBankTranIdx).Cells(objdgcolhTranId.Index).Value) & "'")
            End If

            If drTemp.Length > 0 Then
                Dim objConfig As New clsConfigOptions
                With drTemp(0)
                    .Item("voidreason") = Language.getMessage(mstrModuleName, 10, "You have added incorrect Bank information. Please insert correct Bank Information.")
                    .Item("isvoid") = True
                    .Item("voiduserunkid") = User._Object._Userunkid
                    .Item("voiddatetime") = objConfig._CurrentDateAndTime
                    .Item("AUD") = "D"
                End With
            End If
            mdtTran.AcceptChanges()
            Call FillBankTranList()
            Call ResetBankTranList()

            cboAccountType.Enabled = True
            cboBank.Enabled = True
            cboBranch.Enabled = True

            mintBankTranIdx = -1

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboBank_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBank.SelectedIndexChanged
        Try
            If CInt(cboBank.SelectedValue) > 0 Then
                Dim objBankBranch As New clsbankbranch_master
                Dim dsCombos As New DataSet
                dsCombos = objBankBranch.getListForCombo("Branch", True, CInt(cboBank.SelectedValue))
                With cboBranch
                    .ValueMember = "branchunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Branch")
                End With
            Else
                cboBranch.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddAccType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddAccType.Click
        Dim frm As New frmBankAccountType_AddEdit
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim objAccType As New clsBankAccType
                Dim dsCombos As New DataSet
                dsCombos = objAccType.getComboList(True, "AccType")
                With cboAccountType
                    .ValueMember = "accounttypeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("AccType")
                    .SelectedValue = intRefId
                End With
                objAccType = Nothing
                dsCombos.Dispose()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddAccType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBranch.Click
        Dim frm As New frmBankBranch_AddEdit
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim objBankBranch As New clsbankbranch_master
                Dim dsCombos As New DataSet
                dsCombos = objBankBranch.getListForCombo("Branch", True)
                With cboBranch
                    .ValueMember = "branchunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Branch")
                    .SelectedValue = intRefId
                End With
                objBankBranch = Nothing
                dsCombos.Dispose()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBranch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddBankName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBankName.Click
        Dim frm As New frmPayrollGroups_AddEdit
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE, enPayrollGroupType.Bank)
            If intRefId > 0 Then
                Dim objBankGrp As New clspayrollgroup_master
                Dim dsCombos As New DataSet
                dsCombos = objBankGrp.getListForCombo(PayrollGroupType.Bank, "BankGrp", True)
                With cboBank
                    .ValueMember = "groupmasterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("BankGrp")
                    .SelectedValue = intRefId
                End With
                objBankGrp = Nothing
                dsCombos.Dispose()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBankName_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub dgvBankInfo_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBankInfo.CellClick
        Try
            If e.RowIndex = -1 Then Exit Sub
            If dgvBankInfo.SelectedRows.Count > 0 Then
                cboBank.SelectedValue = 0
                cboAccountType.SelectedValue = 0
                cboBranch.SelectedValue = 0

                mintBankTranIdx = e.RowIndex

                dgvBankInfo.Rows(e.RowIndex).Cells(objdgcolhIsBeingEdit.Index).Value = True

                cboBank.SelectedValue = CInt(dgvBankInfo.Rows(e.RowIndex).Cells(objdgcolhBankId.Index).Value)
                cboAccountType.SelectedValue = CInt(dgvBankInfo.Rows(e.RowIndex).Cells(objdgcolhAccTypeId.Index).Value)
                cboBranch.SelectedValue = CInt(dgvBankInfo.Rows(e.RowIndex).Cells(objdgcolhBranchId.Index).Value)

                txtAccountNo.Text = CStr(dgvBankInfo.Rows(e.RowIndex).Cells(dgcolhAccTypeNo.Index).Value)
                txtBankNo.Text = CStr(dgvBankInfo.Rows(e.RowIndex).Cells(objdgcolhBankNo.Index).Value)
            End If
            cboAccountType.Enabled = False
            cboBank.Enabled = False
            cboBranch.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBankInfo_CellClick", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 01 MARCH 2011 ] -- End 


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.gbCompanyInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCompanyInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBankingInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBankingInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmailSetup.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmailSetup.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbLocalization.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLocalization.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnTestMail.GradientBackColor = GUI._ButttonBackColor 
			Me.btnTestMail.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.tabpCompanyInfo.Text = Language._Object.getCaption(Me.tabpCompanyInfo.Name, Me.tabpCompanyInfo.Text)
			Me.gbCompanyInformation.Text = Language._Object.getCaption(Me.gbCompanyInformation.Name, Me.gbCompanyInformation.Text)
			Me.tabpBankingInfo.Text = Language._Object.getCaption(Me.tabpBankingInfo.Name, Me.tabpBankingInfo.Text)
			Me.gbBankingInfo.Text = Language._Object.getCaption(Me.gbBankingInfo.Name, Me.gbBankingInfo.Text)
			Me.lblCompanyName.Text = Language._Object.getCaption(Me.lblCompanyName.Name, Me.lblCompanyName.Text)
			Me.elAddressInfo.Text = Language._Object.getCaption(Me.elAddressInfo.Name, Me.elAddressInfo.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblPostal.Text = Language._Object.getCaption(Me.lblPostal.Name, Me.lblPostal.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.elContactInfo.Text = Language._Object.getCaption(Me.elContactInfo.Name, Me.elContactInfo.Text)
			Me.lblPhone3.Text = Language._Object.getCaption(Me.lblPhone3.Name, Me.lblPhone3.Text)
			Me.lblPhone2.Text = Language._Object.getCaption(Me.lblPhone2.Name, Me.lblPhone2.Text)
			Me.lblPhone1.Text = Language._Object.getCaption(Me.lblPhone1.Name, Me.lblPhone1.Text)
			Me.lblWebsite.Text = Language._Object.getCaption(Me.lblWebsite.Name, Me.lblWebsite.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
			Me.elRegistrationInfo.Text = Language._Object.getCaption(Me.elRegistrationInfo.Name, Me.elRegistrationInfo.Text)
			Me.lblVATNo.Text = Language._Object.getCaption(Me.lblVATNo.Name, Me.lblVATNo.Text)
			Me.lblTinNo.Text = Language._Object.getCaption(Me.lblTinNo.Name, Me.lblTinNo.Text)
			Me.lblRegNo.Text = Language._Object.getCaption(Me.lblRegNo.Name, Me.lblRegNo.Text)
			Me.lblDistrict.Text = Language._Object.getCaption(Me.lblDistrict.Name, Me.lblDistrict.Text)
			Me.lblNSSFNo.Text = Language._Object.getCaption(Me.lblNSSFNo.Name, Me.lblNSSFNo.Text)
			Me.lblPPFNo.Text = Language._Object.getCaption(Me.lblPPFNo.Name, Me.lblPPFNo.Text)
			Me.lblPayWorkCheck.Text = Language._Object.getCaption(Me.lblPayWorkCheck.Name, Me.lblPayWorkCheck.Text)
			Me.gbEmailSetup.Text = Language._Object.getCaption(Me.gbEmailSetup.Name, Me.gbEmailSetup.Text)
			Me.lblBankName.Text = Language._Object.getCaption(Me.lblBankName.Name, Me.lblBankName.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.lblBankNo.Text = Language._Object.getCaption(Me.lblBankNo.Name, Me.lblBankNo.Text)
			Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.Name, Me.lblAccountNo.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.elOtherRegistrationNo.Text = Language._Object.getCaption(Me.elOtherRegistrationNo.Name, Me.elOtherRegistrationNo.Text)
			Me.lblCompanyRegNo.Text = Language._Object.getCaption(Me.lblCompanyRegNo.Name, Me.lblCompanyRegNo.Text)
			Me.tabpLogo.Text = Language._Object.getCaption(Me.tabpLogo.Name, Me.tabpLogo.Text)
			Me.tabpCompamyPeriod.Text = Language._Object.getCaption(Me.tabpCompamyPeriod.Name, Me.tabpCompamyPeriod.Text)
			Me.lblYearTo.Text = Language._Object.getCaption(Me.lblYearTo.Name, Me.lblYearTo.Text)
			Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
			Me.lblToMonth.Text = Language._Object.getCaption(Me.lblToMonth.Name, Me.lblToMonth.Text)
			Me.lblToDay.Text = Language._Object.getCaption(Me.lblToDay.Name, Me.lblToDay.Text)
			Me.lblFromMonth.Text = Language._Object.getCaption(Me.lblFromMonth.Name, Me.lblFromMonth.Text)
			Me.lblFromDay.Text = Language._Object.getCaption(Me.lblFromDay.Name, Me.lblFromDay.Text)
			Me.tabpOtherInfo.Text = Language._Object.getCaption(Me.tabpOtherInfo.Name, Me.tabpOtherInfo.Text)
			Me.ImgCompanyLogo.Text = Language._Object.getCaption(Me.ImgCompanyLogo.Name, Me.ImgCompanyLogo.Text)
			Me.lnkEditCaption.Text = Language._Object.getCaption(Me.lnkEditCaption.Name, Me.lnkEditCaption.Text)
			Me.lblReference.Text = Language._Object.getCaption(Me.lblReference.Name, Me.lblReference.Text)
			Me.lblSenderInformation.Text = Language._Object.getCaption(Me.lblSenderInformation.Name, Me.lblSenderInformation.Text)
			Me.lblSenderName.Text = Language._Object.getCaption(Me.lblSenderName.Name, Me.lblSenderName.Text)
			Me.lblEmailAddress.Text = Language._Object.getCaption(Me.lblEmailAddress.Name, Me.lblEmailAddress.Text)
			Me.lblMailServerPort.Text = Language._Object.getCaption(Me.lblMailServerPort.Name, Me.lblMailServerPort.Text)
			Me.lblMailServer.Text = Language._Object.getCaption(Me.lblMailServer.Name, Me.lblMailServer.Text)
			Me.lblServerInformation.Text = Language._Object.getCaption(Me.lblServerInformation.Name, Me.lblServerInformation.Text)
			Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
			Me.lblPassword.Text = Language._Object.getCaption(Me.lblPassword.Name, Me.lblPassword.Text)
			Me.chkSSL.Text = Language._Object.getCaption(Me.chkSSL.Name, Me.chkSSL.Text)
			Me.lblUserInformation.Text = Language._Object.getCaption(Me.lblUserInformation.Name, Me.lblUserInformation.Text)
			Me.btnTestMail.Text = Language._Object.getCaption(Me.btnTestMail.Name, Me.btnTestMail.Text)
			Me.lblTestingMessage.Text = Language._Object.getCaption(Me.lblTestingMessage.Name, Me.lblTestingMessage.Text)
			Me.lblTestingTitle.Text = Language._Object.getCaption(Me.lblTestingTitle.Name, Me.lblTestingTitle.Text)
			Me.lblBody.Text = Language._Object.getCaption(Me.lblBody.Name, Me.lblBody.Text)
			Me.lblAccountType.Text = Language._Object.getCaption(Me.lblAccountType.Name, Me.lblAccountType.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.dgcolhBankGroup.HeaderText = Language._Object.getCaption(Me.dgcolhBankGroup.Name, Me.dgcolhBankGroup.HeaderText)
			Me.dgcolhBranch.HeaderText = Language._Object.getCaption(Me.dgcolhBranch.Name, Me.dgcolhBranch.HeaderText)
			Me.dgcolhAccType.HeaderText = Language._Object.getCaption(Me.dgcolhAccType.Name, Me.dgcolhAccType.HeaderText)
			Me.dgcolhAccTypeNo.HeaderText = Language._Object.getCaption(Me.dgcolhAccTypeNo.Name, Me.dgcolhAccTypeNo.HeaderText)
			Me.tabpLocalization.Text = Language._Object.getCaption(Me.tabpLocalization.Name, Me.tabpLocalization.Text)
			Me.gbLocalization.Text = Language._Object.getCaption(Me.gbLocalization.Name, Me.gbLocalization.Text)
			Me.lblDefaultCurrency.Text = Language._Object.getCaption(Me.lblDefaultCurrency.Name, Me.lblDefaultCurrency.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.tabpStamp.Text = Language._Object.getCaption(Me.tabpStamp.Name, Me.tabpStamp.Text)
			Me.ImgCompanyStamp.Text = Language._Object.getCaption(Me.ImgCompanyStamp.Name, Me.ImgCompanyStamp.Text)
			Me.chkCertificateAuthentication.Text = Language._Object.getCaption(Me.chkCertificateAuthentication.Name, Me.chkCertificateAuthentication.Text)
			Me.radExchangeWebService.Text = Language._Object.getCaption(Me.radExchangeWebService.Name, Me.radExchangeWebService.Text)
			Me.radSMTP.Text = Language._Object.getCaption(Me.radSMTP.Name, Me.radSMTP.Text)
			Me.lblEWSDomain.Text = Language._Object.getCaption(Me.lblEWSDomain.Name, Me.lblEWSDomain.Text)
			Me.lblEWSUrl.Text = Language._Object.getCaption(Me.lblEWSUrl.Name, Me.lblEWSUrl.Text)
			Me.chkBypassProxyServer.Text = Language._Object.getCaption(Me.chkBypassProxyServer.Name, Me.chkBypassProxyServer.Text)
            Me.lblAuthenticationProtocol.Text = Language._Object.getCaption(Me.lblAuthenticationProtocol.Name, Me.lblAuthenticationProtocol.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Company Name cannot be blank. Company Name is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Please enter valid Email address of the sender.")
			Language.setMessage(mstrModuleName, 4, "Mail sent Successfully...")
			Language.setMessage(mstrModuleName, 5, "Bank is required information. Please select Bank to continue.")
			Language.setMessage(mstrModuleName, 6, "Branch is required information. Please select Branch to continue.")
			Language.setMessage(mstrModuleName, 7, "Account Type is required information. Please select Account Type to continue.")
			Language.setMessage(mstrModuleName, 8, "Account no cannot be blank. Account no. is required information.")
			Language.setMessage(mstrModuleName, 9, "This information is already added to list. Please add another entry.")
			Language.setMessage(mstrModuleName, 10, "You have added incorrect Bank information. Please insert correct Bank Information.")
			Language.setMessage(mstrModuleName, 11, "Please check your Internet settings.")
			Language.setMessage(mstrModuleName, 12, "Please Uncheck Login using SSL setting.")
			Language.setMessage(mstrModuleName, 14, "Localization country is mandatory information. Please select Localization country to continue.")
			Language.setMessage(mstrModuleName, 15, "Sorry, You cannot change the account number." & vbCrLf & "Reason : This account number is already used in some of the transactions.")
			Language.setMessage(mstrModuleName, 16, "Sorry, You cannot delete this account." & vbCrLf & "Reason : This account is already used in some of the transactions.")
			Language.setMessage(mstrModuleName, 17, "EDITED INFORMATION")
			Language.setMessage(mstrModuleName, 18, "Value Type")
			Language.setMessage(mstrModuleName, 19, "Old")
			Language.setMessage(mstrModuleName, 20, "New")
			Language.setMessage(mstrModuleName, 21, "DELETED INFORMATION")
			Language.setMessage(mstrModuleName, 22, "Notification of Changes in Company Bank(s).")
			Language.setMessage(mstrModuleName, 23, "Sorry, You have Registered for")
			Language.setMessage(mstrModuleName, 24, "Country is mandatory information. Please select Country to continue.")
			Language.setMessage(mstrModuleName, 25, "NEWLY ADDED INFORMATION")
			Language.setMessage(mstrModuleName, 26, "Bank No.")
			Language.setMessage(mstrModuleName, 27, " companies and it is exceeding the limit of Maximum No. of Active Companies. Please contact Aruti team to register for more companies.")
			Language.setMessage(mstrModuleName, 28, "Please enter EWS URL.")
			Language.setMessage(mstrModuleName, 29, "Please enter User Name.")
			Language.setMessage(mstrModuleName, 30, "Please enter Password.")
			Language.setMessage(mstrModuleName, 31, "Please enter EWS Domain.")
            Language.setMessage(mstrModuleName, 32, "This is to inform you that changes have been made for company")
            Language.setMessage(mstrModuleName, 33, "with code")
            Language.setMessage(mstrModuleName, 34, "Following information has been changed by user")
			Language.setMessage(mstrModuleName, 35, "from Machine")
			Language.setMessage(mstrModuleName, 36, "and IPAddress")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class