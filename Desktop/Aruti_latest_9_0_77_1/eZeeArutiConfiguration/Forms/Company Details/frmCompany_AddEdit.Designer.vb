﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompany_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompany_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.tabcCompanyProfile = New System.Windows.Forms.TabControl
        Me.tabpCompanyInfo = New System.Windows.Forms.TabPage
        Me.gbCompanyInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboZipcode = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.tabcOtherInfo = New System.Windows.Forms.TabControl
        Me.tabpCompamyPeriod = New System.Windows.Forms.TabPage
        Me.cboToYear = New System.Windows.Forms.ComboBox
        Me.cboFromYear = New System.Windows.Forms.ComboBox
        Me.lblYearTo = New System.Windows.Forms.Label
        Me.lblFromYear = New System.Windows.Forms.Label
        Me.cboToMonth = New System.Windows.Forms.ComboBox
        Me.lblToMonth = New System.Windows.Forms.Label
        Me.nudToDay = New System.Windows.Forms.NumericUpDown
        Me.lblToDay = New System.Windows.Forms.Label
        Me.cboFromMonth = New System.Windows.Forms.ComboBox
        Me.lblFromMonth = New System.Windows.Forms.Label
        Me.nudDay = New System.Windows.Forms.NumericUpDown
        Me.lblFromDay = New System.Windows.Forms.Label
        Me.tabpLocalization = New System.Windows.Forms.TabPage
        Me.gbLocalization = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtDefaultCurrency = New eZee.TextBox.AlphanumericTextBox
        Me.lblDefaultCurrency = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboLocalCountry = New System.Windows.Forms.ComboBox
        Me.tabpLogo = New System.Windows.Forms.TabPage
        Me.ImgCompanyLogo = New eZee.Common.eZeeImageControl
        Me.tabpOtherInfo = New System.Windows.Forms.TabPage
        Me.lnkEditCaption = New System.Windows.Forms.LinkLabel
        Me.txtRegCaption2Value = New eZee.TextBox.AlphanumericTextBox
        Me.objlblCaption2 = New System.Windows.Forms.Label
        Me.objlblCaption3 = New System.Windows.Forms.Label
        Me.txtRegCaption3Value = New eZee.TextBox.AlphanumericTextBox
        Me.txtRegCaption1Value = New eZee.TextBox.AlphanumericTextBox
        Me.objlblCaption1 = New System.Windows.Forms.Label
        Me.tabpStamp = New System.Windows.Forms.TabPage
        Me.ImgCompanyStamp = New eZee.Common.eZeeImageControl
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.lblCompanyRegNo = New System.Windows.Forms.Label
        Me.lblCity = New System.Windows.Forms.Label
        Me.txtCompamyRegNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblPostal = New System.Windows.Forms.Label
        Me.elOtherRegistrationNo = New eZee.Common.eZeeLine
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.txtPayrollNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblPayWorkCheck = New System.Windows.Forms.Label
        Me.lblPPFNo = New System.Windows.Forms.Label
        Me.txtPPFNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblDistrict = New System.Windows.Forms.Label
        Me.txtDistrictNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblNSSFNo = New System.Windows.Forms.Label
        Me.txtNSSFNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblVATNo = New System.Windows.Forms.Label
        Me.lblTinNo = New System.Windows.Forms.Label
        Me.txtVRNNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtTinNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtRegdNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblRegNo = New System.Windows.Forms.Label
        Me.elRegistrationInfo = New eZee.Common.eZeeLine
        Me.lblWebsite = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtWebsite = New eZee.TextBox.AlphanumericTextBox
        Me.txtFax = New eZee.TextBox.AlphanumericTextBox
        Me.txtCompanyEmail = New eZee.TextBox.AlphanumericTextBox
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.lblPhone3 = New System.Windows.Forms.Label
        Me.lblPhone2 = New System.Windows.Forms.Label
        Me.lblPhone1 = New System.Windows.Forms.Label
        Me.txtPhone3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtPhone2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtPhone1 = New eZee.TextBox.AlphanumericTextBox
        Me.elContactInfo = New eZee.Common.eZeeLine
        Me.lblCountry = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblState = New System.Windows.Forms.Label
        Me.txtAddressLine2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddressLine1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.elAddressInfo = New eZee.Common.eZeeLine
        Me.txtCompanyName = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompanyName = New System.Windows.Forms.Label
        Me.tabpBankingInfo = New System.Windows.Forms.TabPage
        Me.gbEmailSetup = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radExchangeWebService = New System.Windows.Forms.RadioButton
        Me.radSMTP = New System.Windows.Forms.RadioButton
        Me.pnlEmail = New System.Windows.Forms.Panel
        Me.chkBypassProxyServer = New System.Windows.Forms.CheckBox
        Me.pnlExchangeWebService = New System.Windows.Forms.Panel
        Me.txtEWSDomain = New eZee.TextBox.AlphanumericTextBox
        Me.txtEWSUrl = New eZee.TextBox.AlphanumericTextBox
        Me.lblEWSDomain = New System.Windows.Forms.Label
        Me.lblEWSUrl = New System.Windows.Forms.Label
        Me.chkCertificateAuthentication = New System.Windows.Forms.CheckBox
        Me.btnTestMail = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblTestingMessage = New System.Windows.Forms.Label
        Me.lblTestingTitle = New System.Windows.Forms.Label
        Me.txtBody = New eZee.TextBox.AlphanumericTextBox
        Me.lblBody = New System.Windows.Forms.Label
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.txtPassword = New eZee.TextBox.AlphanumericTextBox
        Me.txtUserName = New eZee.TextBox.AlphanumericTextBox
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblPassword = New System.Windows.Forms.Label
        Me.chkSSL = New System.Windows.Forms.CheckBox
        Me.lblUserInformation = New System.Windows.Forms.Label
        Me.txtMailServerPort = New eZee.TextBox.AlphanumericTextBox
        Me.lblMailServerPort = New System.Windows.Forms.Label
        Me.lblMailServer = New System.Windows.Forms.Label
        Me.txtMailServer = New eZee.TextBox.AlphanumericTextBox
        Me.lblServerInformation = New System.Windows.Forms.Label
        Me.lblReference = New System.Windows.Forms.Label
        Me.txtReference = New eZee.TextBox.AlphanumericTextBox
        Me.lblSenderInformation = New System.Windows.Forms.Label
        Me.lblSenderName = New System.Windows.Forms.Label
        Me.lblEmailAddress = New System.Windows.Forms.Label
        Me.txtMailSenderEmail = New eZee.TextBox.AlphanumericTextBox
        Me.txtMailSenderName = New eZee.TextBox.AlphanumericTextBox
        Me.gbBankingInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlBank = New System.Windows.Forms.Panel
        Me.dgvBankInfo = New System.Windows.Forms.DataGridView
        Me.dgcolhBankGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAccType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAccTypeNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBankNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBankId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBranchId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAccTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCompanyId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGuid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAUD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsBeingEdit = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objbtnAddAccType = New eZee.Common.eZeeGradientButton
        Me.cboAccountType = New System.Windows.Forms.ComboBox
        Me.lblAccountType = New System.Windows.Forms.Label
        Me.objbtnAddBranch = New eZee.Common.eZeeGradientButton
        Me.objbtnAddBankName = New eZee.Common.eZeeGradientButton
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.cboBank = New System.Windows.Forms.ComboBox
        Me.lblAccountNo = New System.Windows.Forms.Label
        Me.txtAccountNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.lblBankNo = New System.Windows.Forms.Label
        Me.txtBankNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblBankName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblAuthenticationProtocol = New System.Windows.Forms.Label
        Me.cboAuthenticationProtocol = New System.Windows.Forms.ComboBox
        Me.pnlMainInfo.SuspendLayout()
        Me.tabcCompanyProfile.SuspendLayout()
        Me.tabpCompanyInfo.SuspendLayout()
        Me.gbCompanyInformation.SuspendLayout()
        Me.tabcOtherInfo.SuspendLayout()
        Me.tabpCompamyPeriod.SuspendLayout()
        CType(Me.nudToDay, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpLocalization.SuspendLayout()
        Me.gbLocalization.SuspendLayout()
        Me.tabpLogo.SuspendLayout()
        Me.tabpOtherInfo.SuspendLayout()
        Me.tabpStamp.SuspendLayout()
        Me.tabpBankingInfo.SuspendLayout()
        Me.gbEmailSetup.SuspendLayout()
        Me.pnlEmail.SuspendLayout()
        Me.pnlExchangeWebService.SuspendLayout()
        Me.gbBankingInfo.SuspendLayout()
        Me.pnlBank.SuspendLayout()
        CType(Me.dgvBankInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.tabcCompanyProfile)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(681, 621)
        Me.pnlMainInfo.TabIndex = 0
        '
        'tabcCompanyProfile
        '
        Me.tabcCompanyProfile.Controls.Add(Me.tabpCompanyInfo)
        Me.tabcCompanyProfile.Controls.Add(Me.tabpBankingInfo)
        Me.tabcCompanyProfile.Location = New System.Drawing.Point(12, 12)
        Me.tabcCompanyProfile.Name = "tabcCompanyProfile"
        Me.tabcCompanyProfile.SelectedIndex = 0
        Me.tabcCompanyProfile.Size = New System.Drawing.Size(660, 550)
        Me.tabcCompanyProfile.TabIndex = 0
        '
        'tabpCompanyInfo
        '
        Me.tabpCompanyInfo.Controls.Add(Me.gbCompanyInformation)
        Me.tabpCompanyInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpCompanyInfo.Name = "tabpCompanyInfo"
        Me.tabpCompanyInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpCompanyInfo.Size = New System.Drawing.Size(652, 524)
        Me.tabpCompanyInfo.TabIndex = 0
        Me.tabpCompanyInfo.Text = "Company Info"
        Me.tabpCompanyInfo.UseVisualStyleBackColor = True
        '
        'gbCompanyInformation
        '
        Me.gbCompanyInformation.BorderColor = System.Drawing.Color.Black
        Me.gbCompanyInformation.Checked = False
        Me.gbCompanyInformation.CollapseAllExceptThis = False
        Me.gbCompanyInformation.CollapsedHoverImage = Nothing
        Me.gbCompanyInformation.CollapsedNormalImage = Nothing
        Me.gbCompanyInformation.CollapsedPressedImage = Nothing
        Me.gbCompanyInformation.CollapseOnLoad = False
        Me.gbCompanyInformation.Controls.Add(Me.cboZipcode)
        Me.gbCompanyInformation.Controls.Add(Me.cboState)
        Me.gbCompanyInformation.Controls.Add(Me.tabcOtherInfo)
        Me.gbCompanyInformation.Controls.Add(Me.cboCity)
        Me.gbCompanyInformation.Controls.Add(Me.lblCompanyRegNo)
        Me.gbCompanyInformation.Controls.Add(Me.lblCity)
        Me.gbCompanyInformation.Controls.Add(Me.txtCompamyRegNo)
        Me.gbCompanyInformation.Controls.Add(Me.lblPostal)
        Me.gbCompanyInformation.Controls.Add(Me.elOtherRegistrationNo)
        Me.gbCompanyInformation.Controls.Add(Me.txtCode)
        Me.gbCompanyInformation.Controls.Add(Me.lblCode)
        Me.gbCompanyInformation.Controls.Add(Me.txtPayrollNo)
        Me.gbCompanyInformation.Controls.Add(Me.lblPayWorkCheck)
        Me.gbCompanyInformation.Controls.Add(Me.lblPPFNo)
        Me.gbCompanyInformation.Controls.Add(Me.txtPPFNo)
        Me.gbCompanyInformation.Controls.Add(Me.lblDistrict)
        Me.gbCompanyInformation.Controls.Add(Me.txtDistrictNo)
        Me.gbCompanyInformation.Controls.Add(Me.lblNSSFNo)
        Me.gbCompanyInformation.Controls.Add(Me.txtNSSFNo)
        Me.gbCompanyInformation.Controls.Add(Me.lblVATNo)
        Me.gbCompanyInformation.Controls.Add(Me.lblTinNo)
        Me.gbCompanyInformation.Controls.Add(Me.txtVRNNo)
        Me.gbCompanyInformation.Controls.Add(Me.txtTinNo)
        Me.gbCompanyInformation.Controls.Add(Me.txtRegdNo)
        Me.gbCompanyInformation.Controls.Add(Me.lblRegNo)
        Me.gbCompanyInformation.Controls.Add(Me.elRegistrationInfo)
        Me.gbCompanyInformation.Controls.Add(Me.lblWebsite)
        Me.gbCompanyInformation.Controls.Add(Me.lblEmail)
        Me.gbCompanyInformation.Controls.Add(Me.lblFax)
        Me.gbCompanyInformation.Controls.Add(Me.txtWebsite)
        Me.gbCompanyInformation.Controls.Add(Me.txtFax)
        Me.gbCompanyInformation.Controls.Add(Me.txtCompanyEmail)
        Me.gbCompanyInformation.Controls.Add(Me.objLine1)
        Me.gbCompanyInformation.Controls.Add(Me.lblPhone3)
        Me.gbCompanyInformation.Controls.Add(Me.lblPhone2)
        Me.gbCompanyInformation.Controls.Add(Me.lblPhone1)
        Me.gbCompanyInformation.Controls.Add(Me.txtPhone3)
        Me.gbCompanyInformation.Controls.Add(Me.txtPhone2)
        Me.gbCompanyInformation.Controls.Add(Me.txtPhone1)
        Me.gbCompanyInformation.Controls.Add(Me.elContactInfo)
        Me.gbCompanyInformation.Controls.Add(Me.lblCountry)
        Me.gbCompanyInformation.Controls.Add(Me.cboCountry)
        Me.gbCompanyInformation.Controls.Add(Me.lblState)
        Me.gbCompanyInformation.Controls.Add(Me.txtAddressLine2)
        Me.gbCompanyInformation.Controls.Add(Me.txtAddressLine1)
        Me.gbCompanyInformation.Controls.Add(Me.lblAddress)
        Me.gbCompanyInformation.Controls.Add(Me.elAddressInfo)
        Me.gbCompanyInformation.Controls.Add(Me.txtCompanyName)
        Me.gbCompanyInformation.Controls.Add(Me.lblCompanyName)
        Me.gbCompanyInformation.ExpandedHoverImage = Nothing
        Me.gbCompanyInformation.ExpandedNormalImage = Nothing
        Me.gbCompanyInformation.ExpandedPressedImage = Nothing
        Me.gbCompanyInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCompanyInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCompanyInformation.HeaderHeight = 25
        Me.gbCompanyInformation.HeaderMessage = ""
        Me.gbCompanyInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCompanyInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCompanyInformation.HeightOnCollapse = 0
        Me.gbCompanyInformation.LeftTextSpace = 0
        Me.gbCompanyInformation.Location = New System.Drawing.Point(3, 3)
        Me.gbCompanyInformation.Name = "gbCompanyInformation"
        Me.gbCompanyInformation.OpenHeight = 300
        Me.gbCompanyInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCompanyInformation.ShowBorder = True
        Me.gbCompanyInformation.ShowCheckBox = False
        Me.gbCompanyInformation.ShowCollapseButton = False
        Me.gbCompanyInformation.ShowDefaultBorderColor = True
        Me.gbCompanyInformation.ShowDownButton = False
        Me.gbCompanyInformation.ShowHeader = True
        Me.gbCompanyInformation.Size = New System.Drawing.Size(645, 515)
        Me.gbCompanyInformation.TabIndex = 0
        Me.gbCompanyInformation.Temp = 0
        Me.gbCompanyInformation.Text = "Company Information"
        Me.gbCompanyInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboZipcode
        '
        Me.cboZipcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboZipcode.DropDownWidth = 120
        Me.cboZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboZipcode.FormattingEnabled = True
        Me.cboZipcode.Location = New System.Drawing.Point(238, 274)
        Me.cboZipcode.Name = "cboZipcode"
        Me.cboZipcode.Size = New System.Drawing.Size(70, 21)
        Me.cboZipcode.TabIndex = 15
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(111, 247)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(197, 21)
        Me.cboState.TabIndex = 11
        '
        'tabcOtherInfo
        '
        Me.tabcOtherInfo.Controls.Add(Me.tabpCompamyPeriod)
        Me.tabcOtherInfo.Controls.Add(Me.tabpLocalization)
        Me.tabcOtherInfo.Controls.Add(Me.tabpLogo)
        Me.tabcOtherInfo.Controls.Add(Me.tabpOtherInfo)
        Me.tabcOtherInfo.Controls.Add(Me.tabpStamp)
        Me.tabcOtherInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcOtherInfo.Location = New System.Drawing.Point(335, 346)
        Me.tabcOtherInfo.Multiline = True
        Me.tabcOtherInfo.Name = "tabcOtherInfo"
        Me.tabcOtherInfo.SelectedIndex = 0
        Me.tabcOtherInfo.Size = New System.Drawing.Size(307, 139)
        Me.tabcOtherInfo.TabIndex = 47
        '
        'tabpCompamyPeriod
        '
        Me.tabpCompamyPeriod.Controls.Add(Me.cboToYear)
        Me.tabpCompamyPeriod.Controls.Add(Me.cboFromYear)
        Me.tabpCompamyPeriod.Controls.Add(Me.lblYearTo)
        Me.tabpCompamyPeriod.Controls.Add(Me.lblFromYear)
        Me.tabpCompamyPeriod.Controls.Add(Me.cboToMonth)
        Me.tabpCompamyPeriod.Controls.Add(Me.lblToMonth)
        Me.tabpCompamyPeriod.Controls.Add(Me.nudToDay)
        Me.tabpCompamyPeriod.Controls.Add(Me.lblToDay)
        Me.tabpCompamyPeriod.Controls.Add(Me.cboFromMonth)
        Me.tabpCompamyPeriod.Controls.Add(Me.lblFromMonth)
        Me.tabpCompamyPeriod.Controls.Add(Me.nudDay)
        Me.tabpCompamyPeriod.Controls.Add(Me.lblFromDay)
        Me.tabpCompamyPeriod.Location = New System.Drawing.Point(4, 22)
        Me.tabpCompamyPeriod.Name = "tabpCompamyPeriod"
        Me.tabpCompamyPeriod.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpCompamyPeriod.Size = New System.Drawing.Size(299, 113)
        Me.tabpCompamyPeriod.TabIndex = 1
        Me.tabpCompamyPeriod.Text = "Finacial Period"
        Me.tabpCompamyPeriod.UseVisualStyleBackColor = True
        '
        'cboToYear
        '
        Me.cboToYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToYear.Enabled = False
        Me.cboToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToYear.FormattingEnabled = True
        Me.cboToYear.Location = New System.Drawing.Point(213, 80)
        Me.cboToYear.Name = "cboToYear"
        Me.cboToYear.Size = New System.Drawing.Size(79, 21)
        Me.cboToYear.TabIndex = 155
        '
        'cboFromYear
        '
        Me.cboFromYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromYear.FormattingEnabled = True
        Me.cboFromYear.Location = New System.Drawing.Point(213, 29)
        Me.cboFromYear.Name = "cboFromYear"
        Me.cboFromYear.Size = New System.Drawing.Size(79, 21)
        Me.cboFromYear.TabIndex = 152
        '
        'lblYearTo
        '
        Me.lblYearTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYearTo.Location = New System.Drawing.Point(213, 62)
        Me.lblYearTo.Name = "lblYearTo"
        Me.lblYearTo.Size = New System.Drawing.Size(46, 15)
        Me.lblYearTo.TabIndex = 159
        Me.lblYearTo.Text = "Year"
        Me.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFromYear
        '
        Me.lblFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromYear.Location = New System.Drawing.Point(213, 11)
        Me.lblFromYear.Name = "lblFromYear"
        Me.lblFromYear.Size = New System.Drawing.Size(52, 15)
        Me.lblFromYear.TabIndex = 158
        Me.lblFromYear.Text = "Year"
        Me.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToMonth
        '
        Me.cboToMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToMonth.Enabled = False
        Me.cboToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToMonth.FormattingEnabled = True
        Me.cboToMonth.Location = New System.Drawing.Point(68, 80)
        Me.cboToMonth.Name = "cboToMonth"
        Me.cboToMonth.Size = New System.Drawing.Size(139, 21)
        Me.cboToMonth.TabIndex = 154
        '
        'lblToMonth
        '
        Me.lblToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToMonth.Location = New System.Drawing.Point(68, 62)
        Me.lblToMonth.Name = "lblToMonth"
        Me.lblToMonth.Size = New System.Drawing.Size(139, 15)
        Me.lblToMonth.TabIndex = 154
        Me.lblToMonth.Text = "Month"
        Me.lblToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToDay
        '
        Me.nudToDay.Enabled = False
        Me.nudToDay.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.nudToDay.Location = New System.Drawing.Point(9, 80)
        Me.nudToDay.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.nudToDay.Name = "nudToDay"
        Me.nudToDay.ReadOnly = True
        Me.nudToDay.Size = New System.Drawing.Size(53, 21)
        Me.nudToDay.TabIndex = 153
        '
        'lblToDay
        '
        Me.lblToDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDay.Location = New System.Drawing.Point(8, 62)
        Me.lblToDay.Name = "lblToDay"
        Me.lblToDay.Size = New System.Drawing.Size(54, 15)
        Me.lblToDay.TabIndex = 152
        Me.lblToDay.Text = "To Day"
        Me.lblToDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromMonth
        '
        Me.cboFromMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromMonth.FormattingEnabled = True
        Me.cboFromMonth.Location = New System.Drawing.Point(68, 29)
        Me.cboFromMonth.Name = "cboFromMonth"
        Me.cboFromMonth.Size = New System.Drawing.Size(139, 21)
        Me.cboFromMonth.TabIndex = 151
        '
        'lblFromMonth
        '
        Me.lblFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromMonth.Location = New System.Drawing.Point(68, 11)
        Me.lblFromMonth.Name = "lblFromMonth"
        Me.lblFromMonth.Size = New System.Drawing.Size(139, 15)
        Me.lblFromMonth.TabIndex = 150
        Me.lblFromMonth.Text = "Month"
        Me.lblFromMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDay
        '
        Me.nudDay.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.nudDay.Location = New System.Drawing.Point(9, 29)
        Me.nudDay.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.nudDay.Name = "nudDay"
        Me.nudDay.ReadOnly = True
        Me.nudDay.Size = New System.Drawing.Size(53, 21)
        Me.nudDay.TabIndex = 149
        '
        'lblFromDay
        '
        Me.lblFromDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDay.Location = New System.Drawing.Point(6, 11)
        Me.lblFromDay.Name = "lblFromDay"
        Me.lblFromDay.Size = New System.Drawing.Size(56, 15)
        Me.lblFromDay.TabIndex = 148
        Me.lblFromDay.Text = "From Day"
        Me.lblFromDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpLocalization
        '
        Me.tabpLocalization.Controls.Add(Me.gbLocalization)
        Me.tabpLocalization.Location = New System.Drawing.Point(4, 22)
        Me.tabpLocalization.Name = "tabpLocalization"
        Me.tabpLocalization.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpLocalization.Size = New System.Drawing.Size(299, 113)
        Me.tabpLocalization.TabIndex = 3
        Me.tabpLocalization.Text = "Localization"
        Me.tabpLocalization.UseVisualStyleBackColor = True
        '
        'gbLocalization
        '
        Me.gbLocalization.BorderColor = System.Drawing.Color.Black
        Me.gbLocalization.Checked = False
        Me.gbLocalization.CollapseAllExceptThis = False
        Me.gbLocalization.CollapsedHoverImage = Nothing
        Me.gbLocalization.CollapsedNormalImage = Nothing
        Me.gbLocalization.CollapsedPressedImage = Nothing
        Me.gbLocalization.CollapseOnLoad = False
        Me.gbLocalization.Controls.Add(Me.txtDefaultCurrency)
        Me.gbLocalization.Controls.Add(Me.lblDefaultCurrency)
        Me.gbLocalization.Controls.Add(Me.Label1)
        Me.gbLocalization.Controls.Add(Me.cboLocalCountry)
        Me.gbLocalization.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbLocalization.ExpandedHoverImage = Nothing
        Me.gbLocalization.ExpandedNormalImage = Nothing
        Me.gbLocalization.ExpandedPressedImage = Nothing
        Me.gbLocalization.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLocalization.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLocalization.HeaderHeight = 25
        Me.gbLocalization.HeaderMessage = ""
        Me.gbLocalization.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLocalization.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLocalization.HeightOnCollapse = 0
        Me.gbLocalization.LeftTextSpace = 0
        Me.gbLocalization.Location = New System.Drawing.Point(3, 3)
        Me.gbLocalization.Name = "gbLocalization"
        Me.gbLocalization.OpenHeight = 300
        Me.gbLocalization.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLocalization.ShowBorder = True
        Me.gbLocalization.ShowCheckBox = False
        Me.gbLocalization.ShowCollapseButton = False
        Me.gbLocalization.ShowDefaultBorderColor = True
        Me.gbLocalization.ShowDownButton = False
        Me.gbLocalization.ShowHeader = True
        Me.gbLocalization.Size = New System.Drawing.Size(293, 107)
        Me.gbLocalization.TabIndex = 5
        Me.gbLocalization.Temp = 0
        Me.gbLocalization.Text = "Localization"
        Me.gbLocalization.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDefaultCurrency
        '
        Me.txtDefaultCurrency.Flags = 0
        Me.txtDefaultCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDefaultCurrency.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDefaultCurrency.Location = New System.Drawing.Point(104, 61)
        Me.txtDefaultCurrency.Name = "txtDefaultCurrency"
        Me.txtDefaultCurrency.ReadOnly = True
        Me.txtDefaultCurrency.Size = New System.Drawing.Size(185, 21)
        Me.txtDefaultCurrency.TabIndex = 8
        '
        'lblDefaultCurrency
        '
        Me.lblDefaultCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefaultCurrency.Location = New System.Drawing.Point(8, 63)
        Me.lblDefaultCurrency.Name = "lblDefaultCurrency"
        Me.lblDefaultCurrency.Size = New System.Drawing.Size(90, 16)
        Me.lblDefaultCurrency.TabIndex = 5
        Me.lblDefaultCurrency.Text = "Currency"
        Me.lblDefaultCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Country Name"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLocalCountry
        '
        Me.cboLocalCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLocalCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLocalCountry.FormattingEnabled = True
        Me.cboLocalCountry.Location = New System.Drawing.Point(104, 34)
        Me.cboLocalCountry.Name = "cboLocalCountry"
        Me.cboLocalCountry.Size = New System.Drawing.Size(185, 21)
        Me.cboLocalCountry.TabIndex = 4
        '
        'tabpLogo
        '
        Me.tabpLogo.Controls.Add(Me.ImgCompanyLogo)
        Me.tabpLogo.Location = New System.Drawing.Point(4, 22)
        Me.tabpLogo.Name = "tabpLogo"
        Me.tabpLogo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpLogo.Size = New System.Drawing.Size(299, 113)
        Me.tabpLogo.TabIndex = 0
        Me.tabpLogo.Text = "Logo"
        Me.tabpLogo.UseVisualStyleBackColor = True
        '
        'ImgCompanyLogo
        '
        Me.ImgCompanyLogo._FileMode = False
        Me.ImgCompanyLogo._Image = Nothing
        Me.ImgCompanyLogo._ShowAddButton = True
        Me.ImgCompanyLogo._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.ImgCompanyLogo._ShowDeleteButton = True
        Me.ImgCompanyLogo.BackColor = System.Drawing.Color.Transparent
        Me.ImgCompanyLogo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImgCompanyLogo.Location = New System.Drawing.Point(84, 2)
        Me.ImgCompanyLogo.Name = "ImgCompanyLogo"
        Me.ImgCompanyLogo.Size = New System.Drawing.Size(145, 109)
        Me.ImgCompanyLogo.TabIndex = 0
        '
        'tabpOtherInfo
        '
        Me.tabpOtherInfo.Controls.Add(Me.lnkEditCaption)
        Me.tabpOtherInfo.Controls.Add(Me.txtRegCaption2Value)
        Me.tabpOtherInfo.Controls.Add(Me.objlblCaption2)
        Me.tabpOtherInfo.Controls.Add(Me.objlblCaption3)
        Me.tabpOtherInfo.Controls.Add(Me.txtRegCaption3Value)
        Me.tabpOtherInfo.Controls.Add(Me.txtRegCaption1Value)
        Me.tabpOtherInfo.Controls.Add(Me.objlblCaption1)
        Me.tabpOtherInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherInfo.Name = "tabpOtherInfo"
        Me.tabpOtherInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpOtherInfo.Size = New System.Drawing.Size(299, 113)
        Me.tabpOtherInfo.TabIndex = 2
        Me.tabpOtherInfo.Text = "Other Info"
        Me.tabpOtherInfo.UseVisualStyleBackColor = True
        '
        'lnkEditCaption
        '
        Me.lnkEditCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEditCaption.Location = New System.Drawing.Point(201, 9)
        Me.lnkEditCaption.Name = "lnkEditCaption"
        Me.lnkEditCaption.Size = New System.Drawing.Size(90, 17)
        Me.lnkEditCaption.TabIndex = 156
        Me.lnkEditCaption.TabStop = True
        Me.lnkEditCaption.Text = "Edit Caption"
        '
        'txtRegCaption2Value
        '
        Me.txtRegCaption2Value.Flags = 0
        Me.txtRegCaption2Value.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegCaption2Value.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRegCaption2Value.Location = New System.Drawing.Point(107, 57)
        Me.txtRegCaption2Value.Name = "txtRegCaption2Value"
        Me.txtRegCaption2Value.Size = New System.Drawing.Size(185, 21)
        Me.txtRegCaption2Value.TabIndex = 158
        '
        'objlblCaption2
        '
        Me.objlblCaption2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption2.Location = New System.Drawing.Point(7, 59)
        Me.objlblCaption2.Name = "objlblCaption2"
        Me.objlblCaption2.Size = New System.Drawing.Size(94, 16)
        Me.objlblCaption2.TabIndex = 161
        Me.objlblCaption2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCaption3
        '
        Me.objlblCaption3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption3.Location = New System.Drawing.Point(8, 86)
        Me.objlblCaption3.Name = "objlblCaption3"
        Me.objlblCaption3.Size = New System.Drawing.Size(94, 16)
        Me.objlblCaption3.TabIndex = 160
        Me.objlblCaption3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRegCaption3Value
        '
        Me.txtRegCaption3Value.Flags = 0
        Me.txtRegCaption3Value.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegCaption3Value.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRegCaption3Value.Location = New System.Drawing.Point(107, 84)
        Me.txtRegCaption3Value.Name = "txtRegCaption3Value"
        Me.txtRegCaption3Value.Size = New System.Drawing.Size(185, 21)
        Me.txtRegCaption3Value.TabIndex = 159
        '
        'txtRegCaption1Value
        '
        Me.txtRegCaption1Value.BackColor = System.Drawing.Color.White
        Me.txtRegCaption1Value.Flags = 0
        Me.txtRegCaption1Value.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegCaption1Value.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRegCaption1Value.Location = New System.Drawing.Point(107, 30)
        Me.txtRegCaption1Value.Name = "txtRegCaption1Value"
        Me.txtRegCaption1Value.Size = New System.Drawing.Size(185, 21)
        Me.txtRegCaption1Value.TabIndex = 157
        '
        'objlblCaption1
        '
        Me.objlblCaption1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption1.Location = New System.Drawing.Point(7, 32)
        Me.objlblCaption1.Name = "objlblCaption1"
        Me.objlblCaption1.Size = New System.Drawing.Size(94, 16)
        Me.objlblCaption1.TabIndex = 157
        Me.objlblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpStamp
        '
        Me.tabpStamp.Controls.Add(Me.ImgCompanyStamp)
        Me.tabpStamp.Location = New System.Drawing.Point(4, 22)
        Me.tabpStamp.Name = "tabpStamp"
        Me.tabpStamp.Size = New System.Drawing.Size(299, 113)
        Me.tabpStamp.TabIndex = 4
        Me.tabpStamp.Text = "Stamp"
        Me.tabpStamp.UseVisualStyleBackColor = True
        '
        'ImgCompanyStamp
        '
        Me.ImgCompanyStamp._FileMode = False
        Me.ImgCompanyStamp._Image = Nothing
        Me.ImgCompanyStamp._ShowAddButton = True
        Me.ImgCompanyStamp._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.ImgCompanyStamp._ShowDeleteButton = True
        Me.ImgCompanyStamp.BackColor = System.Drawing.Color.Transparent
        Me.ImgCompanyStamp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImgCompanyStamp.Location = New System.Drawing.Point(77, 2)
        Me.ImgCompanyStamp.Name = "ImgCompanyStamp"
        Me.ImgCompanyStamp.Size = New System.Drawing.Size(145, 109)
        Me.ImgCompanyStamp.TabIndex = 1
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.DropDownWidth = 120
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(111, 274)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(78, 21)
        Me.cboCity.TabIndex = 13
        '
        'lblCompanyRegNo
        '
        Me.lblCompanyRegNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyRegNo.Location = New System.Drawing.Point(347, 55)
        Me.lblCompanyRegNo.Name = "lblCompanyRegNo"
        Me.lblCompanyRegNo.Size = New System.Drawing.Size(94, 16)
        Me.lblCompanyRegNo.TabIndex = 30
        Me.lblCompanyRegNo.Text = "Company Reg. no"
        Me.lblCompanyRegNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(32, 276)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(73, 16)
        Me.lblCity.TabIndex = 12
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCompamyRegNo
        '
        Me.txtCompamyRegNo.Flags = 0
        Me.txtCompamyRegNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompamyRegNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompamyRegNo.Location = New System.Drawing.Point(446, 53)
        Me.txtCompamyRegNo.Name = "txtCompamyRegNo"
        Me.txtCompamyRegNo.Size = New System.Drawing.Size(185, 21)
        Me.txtCompamyRegNo.TabIndex = 31
        '
        'lblPostal
        '
        Me.lblPostal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostal.Location = New System.Drawing.Point(195, 276)
        Me.lblPostal.Name = "lblPostal"
        Me.lblPostal.Size = New System.Drawing.Size(37, 16)
        Me.lblPostal.TabIndex = 14
        Me.lblPostal.Text = "Postal"
        Me.lblPostal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elOtherRegistrationNo
        '
        Me.elOtherRegistrationNo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elOtherRegistrationNo.Location = New System.Drawing.Point(332, 147)
        Me.elOtherRegistrationNo.Name = "elOtherRegistrationNo"
        Me.elOtherRegistrationNo.Size = New System.Drawing.Size(299, 16)
        Me.elOtherRegistrationNo.TabIndex = 36
        Me.elOtherRegistrationNo.Text = "Other Registration Info"
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(64), Global.Microsoft.VisualBasic.ChrW(35), Global.Microsoft.VisualBasic.ChrW(33), Global.Microsoft.VisualBasic.ChrW(94), Global.Microsoft.VisualBasic.ChrW(38), Global.Microsoft.VisualBasic.ChrW(40), Global.Microsoft.VisualBasic.ChrW(41), Global.Microsoft.VisualBasic.ChrW(45), Global.Microsoft.VisualBasic.ChrW(95), Global.Microsoft.VisualBasic.ChrW(61), Global.Microsoft.VisualBasic.ChrW(126), Global.Microsoft.VisualBasic.ChrW(96)}
        Me.txtCode.Location = New System.Drawing.Point(111, 34)
        Me.txtCode.MaxLength = 5
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(78, 21)
        Me.txtCode.TabIndex = 1
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 36)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(97, 16)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPayrollNo
        '
        Me.txtPayrollNo.Flags = 0
        Me.txtPayrollNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayrollNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPayrollNo.Location = New System.Drawing.Point(446, 247)
        Me.txtPayrollNo.Name = "txtPayrollNo"
        Me.txtPayrollNo.Size = New System.Drawing.Size(185, 21)
        Me.txtPayrollNo.TabIndex = 44
        '
        'lblPayWorkCheck
        '
        Me.lblPayWorkCheck.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayWorkCheck.Location = New System.Drawing.Point(347, 249)
        Me.lblPayWorkCheck.Name = "lblPayWorkCheck"
        Me.lblPayWorkCheck.Size = New System.Drawing.Size(94, 16)
        Me.lblPayWorkCheck.TabIndex = 43
        Me.lblPayWorkCheck.Text = "Payroll No."
        Me.lblPayWorkCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPPFNo
        '
        Me.lblPPFNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPPFNo.Location = New System.Drawing.Point(347, 195)
        Me.lblPPFNo.Name = "lblPPFNo"
        Me.lblPPFNo.Size = New System.Drawing.Size(94, 16)
        Me.lblPPFNo.TabIndex = 39
        Me.lblPPFNo.Text = "PPF No."
        Me.lblPPFNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPPFNo
        '
        Me.txtPPFNo.Flags = 0
        Me.txtPPFNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPPFNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPPFNo.Location = New System.Drawing.Point(446, 193)
        Me.txtPPFNo.Name = "txtPPFNo"
        Me.txtPPFNo.Size = New System.Drawing.Size(185, 21)
        Me.txtPPFNo.TabIndex = 40
        '
        'lblDistrict
        '
        Me.lblDistrict.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDistrict.Location = New System.Drawing.Point(347, 276)
        Me.lblDistrict.Name = "lblDistrict"
        Me.lblDistrict.Size = New System.Drawing.Size(94, 16)
        Me.lblDistrict.TabIndex = 45
        Me.lblDistrict.Text = "District"
        Me.lblDistrict.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDistrictNo
        '
        Me.txtDistrictNo.Flags = 0
        Me.txtDistrictNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDistrictNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDistrictNo.Location = New System.Drawing.Point(446, 274)
        Me.txtDistrictNo.Name = "txtDistrictNo"
        Me.txtDistrictNo.Size = New System.Drawing.Size(185, 21)
        Me.txtDistrictNo.TabIndex = 46
        '
        'lblNSSFNo
        '
        Me.lblNSSFNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNSSFNo.Location = New System.Drawing.Point(347, 168)
        Me.lblNSSFNo.Name = "lblNSSFNo"
        Me.lblNSSFNo.Size = New System.Drawing.Size(94, 16)
        Me.lblNSSFNo.TabIndex = 37
        Me.lblNSSFNo.Text = "NSSF No"
        Me.lblNSSFNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNSSFNo
        '
        Me.txtNSSFNo.Flags = 0
        Me.txtNSSFNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNSSFNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNSSFNo.Location = New System.Drawing.Point(446, 166)
        Me.txtNSSFNo.Name = "txtNSSFNo"
        Me.txtNSSFNo.Size = New System.Drawing.Size(185, 21)
        Me.txtNSSFNo.TabIndex = 38
        '
        'lblVATNo
        '
        Me.lblVATNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVATNo.Location = New System.Drawing.Point(347, 109)
        Me.lblVATNo.Name = "lblVATNo"
        Me.lblVATNo.Size = New System.Drawing.Size(94, 16)
        Me.lblVATNo.TabIndex = 34
        Me.lblVATNo.Text = "VRN No."
        Me.lblVATNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTinNo
        '
        Me.lblTinNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTinNo.Location = New System.Drawing.Point(347, 82)
        Me.lblTinNo.Name = "lblTinNo"
        Me.lblTinNo.Size = New System.Drawing.Size(94, 16)
        Me.lblTinNo.TabIndex = 32
        Me.lblTinNo.Text = "Tin No."
        Me.lblTinNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVRNNo
        '
        Me.txtVRNNo.Flags = 0
        Me.txtVRNNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVRNNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVRNNo.Location = New System.Drawing.Point(446, 107)
        Me.txtVRNNo.Name = "txtVRNNo"
        Me.txtVRNNo.Size = New System.Drawing.Size(185, 21)
        Me.txtVRNNo.TabIndex = 35
        '
        'txtTinNo
        '
        Me.txtTinNo.Flags = 0
        Me.txtTinNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTinNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTinNo.Location = New System.Drawing.Point(446, 80)
        Me.txtTinNo.Name = "txtTinNo"
        Me.txtTinNo.Size = New System.Drawing.Size(185, 21)
        Me.txtTinNo.TabIndex = 33
        '
        'txtRegdNo
        '
        Me.txtRegdNo.Flags = 0
        Me.txtRegdNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegdNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRegdNo.Location = New System.Drawing.Point(446, 220)
        Me.txtRegdNo.Name = "txtRegdNo"
        Me.txtRegdNo.Size = New System.Drawing.Size(185, 21)
        Me.txtRegdNo.TabIndex = 42
        '
        'lblRegNo
        '
        Me.lblRegNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRegNo.Location = New System.Drawing.Point(347, 222)
        Me.lblRegNo.Name = "lblRegNo"
        Me.lblRegNo.Size = New System.Drawing.Size(94, 16)
        Me.lblRegNo.TabIndex = 41
        Me.lblRegNo.Text = "RegdNo."
        Me.lblRegNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elRegistrationInfo
        '
        Me.elRegistrationInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elRegistrationInfo.Location = New System.Drawing.Point(332, 34)
        Me.elRegistrationInfo.Name = "elRegistrationInfo"
        Me.elRegistrationInfo.Size = New System.Drawing.Size(299, 16)
        Me.elRegistrationInfo.TabIndex = 29
        Me.elRegistrationInfo.Text = "Registration Info"
        '
        'lblWebsite
        '
        Me.lblWebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWebsite.Location = New System.Drawing.Point(32, 482)
        Me.lblWebsite.Name = "lblWebsite"
        Me.lblWebsite.Size = New System.Drawing.Size(73, 16)
        Me.lblWebsite.TabIndex = 27
        Me.lblWebsite.Text = "Website"
        Me.lblWebsite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(32, 455)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(73, 16)
        Me.lblEmail.TabIndex = 25
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(32, 428)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(73, 16)
        Me.lblFax.TabIndex = 23
        Me.lblFax.Text = "Fax"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWebsite
        '
        Me.txtWebsite.Flags = 0
        Me.txtWebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWebsite.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWebsite.Location = New System.Drawing.Point(111, 480)
        Me.txtWebsite.Name = "txtWebsite"
        Me.txtWebsite.Size = New System.Drawing.Size(197, 21)
        Me.txtWebsite.TabIndex = 28
        '
        'txtFax
        '
        Me.txtFax.Flags = 0
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFax.Location = New System.Drawing.Point(111, 426)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(197, 21)
        Me.txtFax.TabIndex = 24
        '
        'txtCompanyEmail
        '
        Me.txtCompanyEmail.Flags = 0
        Me.txtCompanyEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompanyEmail.Location = New System.Drawing.Point(111, 453)
        Me.txtCompanyEmail.Name = "txtCompanyEmail"
        Me.txtCompanyEmail.Size = New System.Drawing.Size(197, 21)
        Me.txtCompanyEmail.TabIndex = 26
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(317, 24)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(12, 488)
        Me.objLine1.TabIndex = 93
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'lblPhone3
        '
        Me.lblPhone3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone3.Location = New System.Drawing.Point(32, 401)
        Me.lblPhone3.Name = "lblPhone3"
        Me.lblPhone3.Size = New System.Drawing.Size(73, 16)
        Me.lblPhone3.TabIndex = 21
        Me.lblPhone3.Text = "Telephone 3"
        Me.lblPhone3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPhone2
        '
        Me.lblPhone2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone2.Location = New System.Drawing.Point(32, 374)
        Me.lblPhone2.Name = "lblPhone2"
        Me.lblPhone2.Size = New System.Drawing.Size(73, 16)
        Me.lblPhone2.TabIndex = 19
        Me.lblPhone2.Text = "Telephone 2"
        Me.lblPhone2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPhone1
        '
        Me.lblPhone1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone1.Location = New System.Drawing.Point(32, 347)
        Me.lblPhone1.Name = "lblPhone1"
        Me.lblPhone1.Size = New System.Drawing.Size(73, 16)
        Me.lblPhone1.TabIndex = 17
        Me.lblPhone1.Text = "Telephone 1"
        Me.lblPhone1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPhone3
        '
        Me.txtPhone3.Flags = 0
        Me.txtPhone3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone3.Location = New System.Drawing.Point(111, 399)
        Me.txtPhone3.Name = "txtPhone3"
        Me.txtPhone3.Size = New System.Drawing.Size(197, 21)
        Me.txtPhone3.TabIndex = 22
        '
        'txtPhone2
        '
        Me.txtPhone2.Flags = 0
        Me.txtPhone2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone2.Location = New System.Drawing.Point(111, 372)
        Me.txtPhone2.Name = "txtPhone2"
        Me.txtPhone2.Size = New System.Drawing.Size(197, 21)
        Me.txtPhone2.TabIndex = 20
        '
        'txtPhone1
        '
        Me.txtPhone1.Flags = 0
        Me.txtPhone1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone1.Location = New System.Drawing.Point(111, 345)
        Me.txtPhone1.Name = "txtPhone1"
        Me.txtPhone1.Size = New System.Drawing.Size(197, 21)
        Me.txtPhone1.TabIndex = 18
        '
        'elContactInfo
        '
        Me.elContactInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elContactInfo.Location = New System.Drawing.Point(8, 326)
        Me.elContactInfo.Name = "elContactInfo"
        Me.elContactInfo.Size = New System.Drawing.Size(300, 16)
        Me.elContactInfo.TabIndex = 16
        Me.elContactInfo.Text = "Contact Info"
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(32, 222)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(73, 16)
        Me.lblCountry.TabIndex = 8
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(111, 220)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(197, 21)
        Me.cboCountry.TabIndex = 9
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(32, 249)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(73, 16)
        Me.lblState.TabIndex = 10
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddressLine2
        '
        Me.txtAddressLine2.Flags = 0
        Me.txtAddressLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressLine2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddressLine2.Location = New System.Drawing.Point(111, 193)
        Me.txtAddressLine2.Name = "txtAddressLine2"
        Me.txtAddressLine2.Size = New System.Drawing.Size(197, 21)
        Me.txtAddressLine2.TabIndex = 7
        '
        'txtAddressLine1
        '
        Me.txtAddressLine1.Flags = 0
        Me.txtAddressLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressLine1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddressLine1.Location = New System.Drawing.Point(111, 166)
        Me.txtAddressLine1.Name = "txtAddressLine1"
        Me.txtAddressLine1.Size = New System.Drawing.Size(197, 21)
        Me.txtAddressLine1.TabIndex = 6
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(32, 168)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(73, 16)
        Me.lblAddress.TabIndex = 5
        Me.lblAddress.Text = "Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elAddressInfo
        '
        Me.elAddressInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elAddressInfo.Location = New System.Drawing.Point(8, 147)
        Me.elAddressInfo.Name = "elAddressInfo"
        Me.elAddressInfo.Size = New System.Drawing.Size(300, 16)
        Me.elAddressInfo.TabIndex = 4
        Me.elAddressInfo.Text = "Address Information"
        '
        'txtCompanyName
        '
        Me.txtCompanyName.Flags = 0
        Me.txtCompanyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompanyName.Location = New System.Drawing.Point(111, 61)
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.Size = New System.Drawing.Size(197, 21)
        Me.txtCompanyName.TabIndex = 3
        '
        'lblCompanyName
        '
        Me.lblCompanyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyName.Location = New System.Drawing.Point(8, 63)
        Me.lblCompanyName.Name = "lblCompanyName"
        Me.lblCompanyName.Size = New System.Drawing.Size(97, 16)
        Me.lblCompanyName.TabIndex = 2
        Me.lblCompanyName.Text = "Company Name"
        Me.lblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpBankingInfo
        '
        Me.tabpBankingInfo.Controls.Add(Me.gbEmailSetup)
        Me.tabpBankingInfo.Controls.Add(Me.gbBankingInfo)
        Me.tabpBankingInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpBankingInfo.Name = "tabpBankingInfo"
        Me.tabpBankingInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpBankingInfo.Size = New System.Drawing.Size(652, 524)
        Me.tabpBankingInfo.TabIndex = 1
        Me.tabpBankingInfo.Text = "Banking / Email Info"
        Me.tabpBankingInfo.UseVisualStyleBackColor = True
        '
        'gbEmailSetup
        '
        Me.gbEmailSetup.BorderColor = System.Drawing.Color.Black
        Me.gbEmailSetup.Checked = False
        Me.gbEmailSetup.CollapseAllExceptThis = False
        Me.gbEmailSetup.CollapsedHoverImage = Nothing
        Me.gbEmailSetup.CollapsedNormalImage = Nothing
        Me.gbEmailSetup.CollapsedPressedImage = Nothing
        Me.gbEmailSetup.CollapseOnLoad = False
        Me.gbEmailSetup.Controls.Add(Me.radExchangeWebService)
        Me.gbEmailSetup.Controls.Add(Me.radSMTP)
        Me.gbEmailSetup.Controls.Add(Me.pnlEmail)
        Me.gbEmailSetup.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbEmailSetup.ExpandedHoverImage = Nothing
        Me.gbEmailSetup.ExpandedNormalImage = Nothing
        Me.gbEmailSetup.ExpandedPressedImage = Nothing
        Me.gbEmailSetup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmailSetup.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmailSetup.HeaderHeight = 25
        Me.gbEmailSetup.HeaderMessage = ""
        Me.gbEmailSetup.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmailSetup.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmailSetup.HeightOnCollapse = 0
        Me.gbEmailSetup.LeftTextSpace = 0
        Me.gbEmailSetup.Location = New System.Drawing.Point(3, 234)
        Me.gbEmailSetup.Name = "gbEmailSetup"
        Me.gbEmailSetup.OpenHeight = 300
        Me.gbEmailSetup.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmailSetup.ShowBorder = True
        Me.gbEmailSetup.ShowCheckBox = False
        Me.gbEmailSetup.ShowCollapseButton = False
        Me.gbEmailSetup.ShowDefaultBorderColor = True
        Me.gbEmailSetup.ShowDownButton = False
        Me.gbEmailSetup.ShowHeader = True
        Me.gbEmailSetup.Size = New System.Drawing.Size(646, 287)
        Me.gbEmailSetup.TabIndex = 11
        Me.gbEmailSetup.Temp = 0
        Me.gbEmailSetup.Text = "Email Setup"
        Me.gbEmailSetup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radExchangeWebService
        '
        Me.radExchangeWebService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExchangeWebService.Location = New System.Drawing.Point(232, 2)
        Me.radExchangeWebService.Name = "radExchangeWebService"
        Me.radExchangeWebService.Size = New System.Drawing.Size(178, 24)
        Me.radExchangeWebService.TabIndex = 102
        Me.radExchangeWebService.Text = "Exchange Web Service (EWS)"
        Me.radExchangeWebService.UseVisualStyleBackColor = True
        '
        'radSMTP
        '
        Me.radSMTP.Checked = True
        Me.radSMTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSMTP.Location = New System.Drawing.Point(140, 1)
        Me.radSMTP.Name = "radSMTP"
        Me.radSMTP.Size = New System.Drawing.Size(86, 24)
        Me.radSMTP.TabIndex = 2
        Me.radSMTP.TabStop = True
        Me.radSMTP.Text = "SMTP"
        Me.radSMTP.UseVisualStyleBackColor = True
        '
        'pnlEmail
        '
        Me.pnlEmail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmail.AutoScroll = True
        Me.pnlEmail.Controls.Add(Me.cboAuthenticationProtocol)
        Me.pnlEmail.Controls.Add(Me.lblAuthenticationProtocol)
        Me.pnlEmail.Controls.Add(Me.chkBypassProxyServer)
        Me.pnlEmail.Controls.Add(Me.pnlExchangeWebService)
        Me.pnlEmail.Controls.Add(Me.chkCertificateAuthentication)
        Me.pnlEmail.Controls.Add(Me.btnTestMail)
        Me.pnlEmail.Controls.Add(Me.lblTestingMessage)
        Me.pnlEmail.Controls.Add(Me.lblTestingTitle)
        Me.pnlEmail.Controls.Add(Me.txtBody)
        Me.pnlEmail.Controls.Add(Me.lblBody)
        Me.pnlEmail.Controls.Add(Me.objLine2)
        Me.pnlEmail.Controls.Add(Me.txtPassword)
        Me.pnlEmail.Controls.Add(Me.txtUserName)
        Me.pnlEmail.Controls.Add(Me.lblUserName)
        Me.pnlEmail.Controls.Add(Me.lblPassword)
        Me.pnlEmail.Controls.Add(Me.chkSSL)
        Me.pnlEmail.Controls.Add(Me.lblUserInformation)
        Me.pnlEmail.Controls.Add(Me.txtMailServerPort)
        Me.pnlEmail.Controls.Add(Me.lblMailServerPort)
        Me.pnlEmail.Controls.Add(Me.lblMailServer)
        Me.pnlEmail.Controls.Add(Me.txtMailServer)
        Me.pnlEmail.Controls.Add(Me.lblServerInformation)
        Me.pnlEmail.Controls.Add(Me.lblReference)
        Me.pnlEmail.Controls.Add(Me.txtReference)
        Me.pnlEmail.Controls.Add(Me.lblSenderInformation)
        Me.pnlEmail.Controls.Add(Me.lblSenderName)
        Me.pnlEmail.Controls.Add(Me.lblEmailAddress)
        Me.pnlEmail.Controls.Add(Me.txtMailSenderEmail)
        Me.pnlEmail.Controls.Add(Me.txtMailSenderName)
        Me.pnlEmail.Location = New System.Drawing.Point(2, 26)
        Me.pnlEmail.Name = "pnlEmail"
        Me.pnlEmail.Size = New System.Drawing.Size(641, 259)
        Me.pnlEmail.TabIndex = 100
        '
        'chkBypassProxyServer
        '
        Me.chkBypassProxyServer.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkBypassProxyServer.Location = New System.Drawing.Point(263, 200)
        Me.chkBypassProxyServer.Name = "chkBypassProxyServer"
        Me.chkBypassProxyServer.Size = New System.Drawing.Size(106, 17)
        Me.chkBypassProxyServer.TabIndex = 132
        Me.chkBypassProxyServer.Text = "Bypass Proxy"
        Me.chkBypassProxyServer.UseVisualStyleBackColor = True
        '
        'pnlExchangeWebService
        '
        Me.pnlExchangeWebService.Controls.Add(Me.txtEWSDomain)
        Me.pnlExchangeWebService.Controls.Add(Me.txtEWSUrl)
        Me.pnlExchangeWebService.Controls.Add(Me.lblEWSDomain)
        Me.pnlExchangeWebService.Controls.Add(Me.lblEWSUrl)
        Me.pnlExchangeWebService.Location = New System.Drawing.Point(215, 122)
        Me.pnlExchangeWebService.Name = "pnlExchangeWebService"
        Me.pnlExchangeWebService.Size = New System.Drawing.Size(145, 57)
        Me.pnlExchangeWebService.TabIndex = 131
        Me.pnlExchangeWebService.Visible = False
        '
        'txtEWSDomain
        '
        Me.txtEWSDomain.BackColor = System.Drawing.Color.White
        Me.txtEWSDomain.Flags = 0
        Me.txtEWSDomain.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtEWSDomain.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEWSDomain.Location = New System.Drawing.Point(136, 31)
        Me.txtEWSDomain.Name = "txtEWSDomain"
        Me.txtEWSDomain.Size = New System.Drawing.Size(211, 21)
        Me.txtEWSDomain.TabIndex = 118
        Me.txtEWSDomain.Tag = "name"
        '
        'txtEWSUrl
        '
        Me.txtEWSUrl.BackColor = System.Drawing.Color.White
        Me.txtEWSUrl.Flags = 0
        Me.txtEWSUrl.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtEWSUrl.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEWSUrl.Location = New System.Drawing.Point(136, 4)
        Me.txtEWSUrl.Name = "txtEWSUrl"
        Me.txtEWSUrl.Size = New System.Drawing.Size(211, 21)
        Me.txtEWSUrl.TabIndex = 117
        Me.txtEWSUrl.Tag = "name"
        '
        'lblEWSDomain
        '
        Me.lblEWSDomain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEWSDomain.Location = New System.Drawing.Point(5, 34)
        Me.lblEWSDomain.Name = "lblEWSDomain"
        Me.lblEWSDomain.Size = New System.Drawing.Size(124, 15)
        Me.lblEWSDomain.TabIndex = 116
        Me.lblEWSDomain.Text = "EWS Domain"
        '
        'lblEWSUrl
        '
        Me.lblEWSUrl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEWSUrl.Location = New System.Drawing.Point(6, 8)
        Me.lblEWSUrl.Name = "lblEWSUrl"
        Me.lblEWSUrl.Size = New System.Drawing.Size(124, 15)
        Me.lblEWSUrl.TabIndex = 115
        Me.lblEWSUrl.Text = "EWS URL"
        '
        'chkCertificateAuthentication
        '
        Me.chkCertificateAuthentication.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkCertificateAuthentication.Location = New System.Drawing.Point(185, 155)
        Me.chkCertificateAuthentication.Name = "chkCertificateAuthentication"
        Me.chkCertificateAuthentication.Size = New System.Drawing.Size(170, 17)
        Me.chkCertificateAuthentication.TabIndex = 130
        Me.chkCertificateAuthentication.Text = "Use Certificate Authentication"
        Me.chkCertificateAuthentication.UseVisualStyleBackColor = True
        '
        'btnTestMail
        '
        Me.btnTestMail.BackColor = System.Drawing.Color.White
        Me.btnTestMail.BackgroundImage = CType(resources.GetObject("btnTestMail.BackgroundImage"), System.Drawing.Image)
        Me.btnTestMail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTestMail.BorderColor = System.Drawing.Color.Empty
        Me.btnTestMail.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnTestMail.FlatAppearance.BorderSize = 0
        Me.btnTestMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTestMail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTestMail.ForeColor = System.Drawing.Color.Black
        Me.btnTestMail.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTestMail.GradientForeColor = System.Drawing.Color.Black
        Me.btnTestMail.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestMail.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTestMail.Location = New System.Drawing.Point(383, 228)
        Me.btnTestMail.Name = "btnTestMail"
        Me.btnTestMail.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestMail.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTestMail.Size = New System.Drawing.Size(226, 44)
        Me.btnTestMail.TabIndex = 129
        Me.btnTestMail.Text = "Test Mail Settings"
        Me.btnTestMail.UseVisualStyleBackColor = True
        '
        'lblTestingMessage
        '
        Me.lblTestingMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTestingMessage.Location = New System.Drawing.Point(413, 149)
        Me.lblTestingMessage.Name = "lblTestingMessage"
        Me.lblTestingMessage.Size = New System.Drawing.Size(193, 70)
        Me.lblTestingMessage.TabIndex = 128
        Me.lblTestingMessage.Text = "After Filling out the information on this screen, we recommend you test your Emai" & _
            "l Information by clicking button below. (Required Internet connection)"
        '
        'lblTestingTitle
        '
        Me.lblTestingTitle.Location = New System.Drawing.Point(380, 122)
        Me.lblTestingTitle.Name = "lblTestingTitle"
        Me.lblTestingTitle.Size = New System.Drawing.Size(192, 17)
        Me.lblTestingTitle.TabIndex = 127
        Me.lblTestingTitle.Text = "Testing Email Settings"
        '
        'txtBody
        '
        Me.txtBody.Flags = 0
        Me.txtBody.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBody.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBody.Location = New System.Drawing.Point(383, 26)
        Me.txtBody.Multiline = True
        Me.txtBody.Name = "txtBody"
        Me.txtBody.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBody.Size = New System.Drawing.Size(226, 93)
        Me.txtBody.TabIndex = 126
        '
        'lblBody
        '
        Me.lblBody.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBody.Location = New System.Drawing.Point(380, 9)
        Me.lblBody.Name = "lblBody"
        Me.lblBody.Size = New System.Drawing.Size(220, 13)
        Me.lblBody.TabIndex = 125
        Me.lblBody.Text = "Body"
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(366, 9)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(8, 256)
        Me.objLine2.TabIndex = 124
        Me.objLine2.Text = "EZeeStraightLine2"
        '
        'txtPassword
        '
        Me.txtPassword.Flags = 0
        Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtPassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPassword.Location = New System.Drawing.Point(138, 225)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(119, 21)
        Me.txtPassword.TabIndex = 122
        Me.txtPassword.Tag = "name"
        '
        'txtUserName
        '
        Me.txtUserName.BackColor = System.Drawing.Color.White
        Me.txtUserName.Flags = 0
        Me.txtUserName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtUserName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtUserName.Location = New System.Drawing.Point(138, 198)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(119, 21)
        Me.txtUserName.TabIndex = 120
        Me.txtUserName.Tag = "name"
        '
        'lblUserName
        '
        Me.lblUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(8, 201)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(124, 15)
        Me.lblUserName.TabIndex = 119
        Me.lblUserName.Text = "User Name"
        '
        'lblPassword
        '
        Me.lblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(8, 226)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(124, 15)
        Me.lblPassword.TabIndex = 121
        Me.lblPassword.Text = "Password"
        '
        'chkSSL
        '
        Me.chkSSL.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkSSL.Location = New System.Drawing.Point(263, 227)
        Me.chkSSL.Name = "chkSSL"
        Me.chkSSL.Size = New System.Drawing.Size(106, 17)
        Me.chkSSL.TabIndex = 123
        Me.chkSSL.Text = "Login using SSL"
        Me.chkSSL.UseVisualStyleBackColor = True
        '
        'lblUserInformation
        '
        Me.lblUserInformation.Location = New System.Drawing.Point(6, 177)
        Me.lblUserInformation.Name = "lblUserInformation"
        Me.lblUserInformation.Size = New System.Drawing.Size(298, 17)
        Me.lblUserInformation.TabIndex = 118
        Me.lblUserInformation.Text = "Authentication Information"
        '
        'txtMailServerPort
        '
        Me.txtMailServerPort.Flags = 0
        Me.txtMailServerPort.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMailServerPort.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMailServerPort.Location = New System.Drawing.Point(138, 151)
        Me.txtMailServerPort.Name = "txtMailServerPort"
        Me.txtMailServerPort.Size = New System.Drawing.Size(41, 21)
        Me.txtMailServerPort.TabIndex = 117
        Me.txtMailServerPort.Tag = "name"
        Me.txtMailServerPort.Text = "25"
        '
        'lblMailServerPort
        '
        Me.lblMailServerPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMailServerPort.Location = New System.Drawing.Point(8, 156)
        Me.lblMailServerPort.Name = "lblMailServerPort"
        Me.lblMailServerPort.Size = New System.Drawing.Size(124, 15)
        Me.lblMailServerPort.TabIndex = 116
        Me.lblMailServerPort.Text = "Mail Server Port"
        '
        'lblMailServer
        '
        Me.lblMailServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMailServer.Location = New System.Drawing.Point(8, 129)
        Me.lblMailServer.Name = "lblMailServer"
        Me.lblMailServer.Size = New System.Drawing.Size(124, 15)
        Me.lblMailServer.TabIndex = 114
        Me.lblMailServer.Text = "Mail Server (SMTP) / IP"
        '
        'txtMailServer
        '
        Me.txtMailServer.BackColor = System.Drawing.Color.White
        Me.txtMailServer.Flags = 0
        Me.txtMailServer.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMailServer.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMailServer.Location = New System.Drawing.Point(138, 126)
        Me.txtMailServer.Name = "txtMailServer"
        Me.txtMailServer.Size = New System.Drawing.Size(211, 21)
        Me.txtMailServer.TabIndex = 115
        Me.txtMailServer.Tag = "name"
        '
        'lblServerInformation
        '
        Me.lblServerInformation.Location = New System.Drawing.Point(6, 104)
        Me.lblServerInformation.Name = "lblServerInformation"
        Me.lblServerInformation.Size = New System.Drawing.Size(298, 17)
        Me.lblServerInformation.TabIndex = 113
        Me.lblServerInformation.Text = "Server Information"
        '
        'lblReference
        '
        Me.lblReference.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReference.Location = New System.Drawing.Point(8, 83)
        Me.lblReference.Name = "lblReference"
        Me.lblReference.Size = New System.Drawing.Size(124, 15)
        Me.lblReference.TabIndex = 111
        Me.lblReference.Text = "Reference"
        '
        'txtReference
        '
        Me.txtReference.Flags = 0
        Me.txtReference.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtReference.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReference.Location = New System.Drawing.Point(138, 80)
        Me.txtReference.Name = "txtReference"
        Me.txtReference.Size = New System.Drawing.Size(211, 21)
        Me.txtReference.TabIndex = 112
        Me.txtReference.Tag = "name"
        '
        'lblSenderInformation
        '
        Me.lblSenderInformation.Location = New System.Drawing.Point(6, 9)
        Me.lblSenderInformation.Name = "lblSenderInformation"
        Me.lblSenderInformation.Size = New System.Drawing.Size(298, 13)
        Me.lblSenderInformation.TabIndex = 106
        Me.lblSenderInformation.Text = "Sender Information"
        '
        'lblSenderName
        '
        Me.lblSenderName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSenderName.Location = New System.Drawing.Point(8, 29)
        Me.lblSenderName.Name = "lblSenderName"
        Me.lblSenderName.Size = New System.Drawing.Size(124, 15)
        Me.lblSenderName.TabIndex = 107
        Me.lblSenderName.Text = "Sender Name"
        '
        'lblEmailAddress
        '
        Me.lblEmailAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmailAddress.Location = New System.Drawing.Point(8, 56)
        Me.lblEmailAddress.Name = "lblEmailAddress"
        Me.lblEmailAddress.Size = New System.Drawing.Size(124, 15)
        Me.lblEmailAddress.TabIndex = 109
        Me.lblEmailAddress.Text = "Email Address"
        '
        'txtMailSenderEmail
        '
        Me.txtMailSenderEmail.Flags = 0
        Me.txtMailSenderEmail.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMailSenderEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMailSenderEmail.Location = New System.Drawing.Point(138, 53)
        Me.txtMailSenderEmail.Name = "txtMailSenderEmail"
        Me.txtMailSenderEmail.Size = New System.Drawing.Size(211, 21)
        Me.txtMailSenderEmail.TabIndex = 110
        Me.txtMailSenderEmail.Tag = "name"
        '
        'txtMailSenderName
        '
        Me.txtMailSenderName.BackColor = System.Drawing.Color.White
        Me.txtMailSenderName.Flags = 0
        Me.txtMailSenderName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMailSenderName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMailSenderName.Location = New System.Drawing.Point(138, 26)
        Me.txtMailSenderName.Name = "txtMailSenderName"
        Me.txtMailSenderName.Size = New System.Drawing.Size(211, 21)
        Me.txtMailSenderName.TabIndex = 108
        Me.txtMailSenderName.Tag = "name"
        '
        'gbBankingInfo
        '
        Me.gbBankingInfo.BorderColor = System.Drawing.Color.Black
        Me.gbBankingInfo.Checked = False
        Me.gbBankingInfo.CollapseAllExceptThis = False
        Me.gbBankingInfo.CollapsedHoverImage = Nothing
        Me.gbBankingInfo.CollapsedNormalImage = Nothing
        Me.gbBankingInfo.CollapsedPressedImage = Nothing
        Me.gbBankingInfo.CollapseOnLoad = False
        Me.gbBankingInfo.Controls.Add(Me.pnlBank)
        Me.gbBankingInfo.Controls.Add(Me.btnDelete)
        Me.gbBankingInfo.Controls.Add(Me.btnEdit)
        Me.gbBankingInfo.Controls.Add(Me.btnAdd)
        Me.gbBankingInfo.Controls.Add(Me.objelLine1)
        Me.gbBankingInfo.Controls.Add(Me.objbtnAddAccType)
        Me.gbBankingInfo.Controls.Add(Me.cboAccountType)
        Me.gbBankingInfo.Controls.Add(Me.lblAccountType)
        Me.gbBankingInfo.Controls.Add(Me.objbtnAddBranch)
        Me.gbBankingInfo.Controls.Add(Me.objbtnAddBankName)
        Me.gbBankingInfo.Controls.Add(Me.cboBranch)
        Me.gbBankingInfo.Controls.Add(Me.cboBank)
        Me.gbBankingInfo.Controls.Add(Me.lblAccountNo)
        Me.gbBankingInfo.Controls.Add(Me.txtAccountNo)
        Me.gbBankingInfo.Controls.Add(Me.lblBranch)
        Me.gbBankingInfo.Controls.Add(Me.lblBankNo)
        Me.gbBankingInfo.Controls.Add(Me.txtBankNo)
        Me.gbBankingInfo.Controls.Add(Me.lblBankName)
        Me.gbBankingInfo.ExpandedHoverImage = Nothing
        Me.gbBankingInfo.ExpandedNormalImage = Nothing
        Me.gbBankingInfo.ExpandedPressedImage = Nothing
        Me.gbBankingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBankingInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBankingInfo.HeaderHeight = 25
        Me.gbBankingInfo.HeaderMessage = ""
        Me.gbBankingInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBankingInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBankingInfo.HeightOnCollapse = 0
        Me.gbBankingInfo.LeftTextSpace = 0
        Me.gbBankingInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbBankingInfo.Name = "gbBankingInfo"
        Me.gbBankingInfo.OpenHeight = 300
        Me.gbBankingInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBankingInfo.ShowBorder = True
        Me.gbBankingInfo.ShowCheckBox = False
        Me.gbBankingInfo.ShowCollapseButton = False
        Me.gbBankingInfo.ShowDefaultBorderColor = True
        Me.gbBankingInfo.ShowDownButton = False
        Me.gbBankingInfo.ShowHeader = True
        Me.gbBankingInfo.Size = New System.Drawing.Size(645, 225)
        Me.gbBankingInfo.TabIndex = 1
        Me.gbBankingInfo.Temp = 0
        Me.gbBankingInfo.Text = "Banking Information"
        Me.gbBankingInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlBank
        '
        Me.pnlBank.Controls.Add(Me.dgvBankInfo)
        Me.pnlBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlBank.Location = New System.Drawing.Point(11, 101)
        Me.pnlBank.Name = "pnlBank"
        Me.pnlBank.Size = New System.Drawing.Size(521, 116)
        Me.pnlBank.TabIndex = 151
        '
        'dgvBankInfo
        '
        Me.dgvBankInfo.AllowUserToAddRows = False
        Me.dgvBankInfo.AllowUserToDeleteRows = False
        Me.dgvBankInfo.AllowUserToResizeColumns = False
        Me.dgvBankInfo.AllowUserToResizeRows = False
        Me.dgvBankInfo.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvBankInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvBankInfo.ColumnHeadersHeight = 22
        Me.dgvBankInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvBankInfo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhBankGroup, Me.dgcolhBranch, Me.dgcolhAccType, Me.dgcolhAccTypeNo, Me.objdgcolhBankNo, Me.objdgcolhBankId, Me.objdgcolhBranchId, Me.objdgcolhAccTypeId, Me.objdgcolhCompanyId, Me.objdgcolhTranId, Me.objdgcolhGuid, Me.objdgcolhAUD, Me.objdgcolhIsBeingEdit})
        Me.dgvBankInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBankInfo.Location = New System.Drawing.Point(0, 0)
        Me.dgvBankInfo.MultiSelect = False
        Me.dgvBankInfo.Name = "dgvBankInfo"
        Me.dgvBankInfo.ReadOnly = True
        Me.dgvBankInfo.RowHeadersVisible = False
        Me.dgvBankInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBankInfo.Size = New System.Drawing.Size(521, 116)
        Me.dgvBankInfo.TabIndex = 149
        '
        'dgcolhBankGroup
        '
        Me.dgcolhBankGroup.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhBankGroup.HeaderText = "Bank"
        Me.dgcolhBankGroup.Name = "dgcolhBankGroup"
        Me.dgcolhBankGroup.ReadOnly = True
        '
        'dgcolhBranch
        '
        Me.dgcolhBranch.HeaderText = "Branch"
        Me.dgcolhBranch.Name = "dgcolhBranch"
        Me.dgcolhBranch.ReadOnly = True
        Me.dgcolhBranch.Width = 120
        '
        'dgcolhAccType
        '
        Me.dgcolhAccType.HeaderText = "Account Type"
        Me.dgcolhAccType.Name = "dgcolhAccType"
        Me.dgcolhAccType.ReadOnly = True
        Me.dgcolhAccType.Width = 120
        '
        'dgcolhAccTypeNo
        '
        Me.dgcolhAccTypeNo.HeaderText = "Account No"
        Me.dgcolhAccTypeNo.Name = "dgcolhAccTypeNo"
        Me.dgcolhAccTypeNo.ReadOnly = True
        Me.dgcolhAccTypeNo.Width = 120
        '
        'objdgcolhBankNo
        '
        Me.objdgcolhBankNo.HeaderText = "objdgcolhBankNo"
        Me.objdgcolhBankNo.Name = "objdgcolhBankNo"
        Me.objdgcolhBankNo.ReadOnly = True
        Me.objdgcolhBankNo.Visible = False
        '
        'objdgcolhBankId
        '
        Me.objdgcolhBankId.HeaderText = "objdgcolhBankId"
        Me.objdgcolhBankId.Name = "objdgcolhBankId"
        Me.objdgcolhBankId.ReadOnly = True
        Me.objdgcolhBankId.Visible = False
        '
        'objdgcolhBranchId
        '
        Me.objdgcolhBranchId.HeaderText = "objdgcolhBranchId"
        Me.objdgcolhBranchId.Name = "objdgcolhBranchId"
        Me.objdgcolhBranchId.ReadOnly = True
        Me.objdgcolhBranchId.Visible = False
        '
        'objdgcolhAccTypeId
        '
        Me.objdgcolhAccTypeId.HeaderText = "objdgcolhAccTypeId"
        Me.objdgcolhAccTypeId.Name = "objdgcolhAccTypeId"
        Me.objdgcolhAccTypeId.ReadOnly = True
        Me.objdgcolhAccTypeId.Visible = False
        '
        'objdgcolhCompanyId
        '
        Me.objdgcolhCompanyId.HeaderText = "objdgcolhCompanyId"
        Me.objdgcolhCompanyId.Name = "objdgcolhCompanyId"
        Me.objdgcolhCompanyId.ReadOnly = True
        Me.objdgcolhCompanyId.Visible = False
        '
        'objdgcolhTranId
        '
        Me.objdgcolhTranId.HeaderText = "objdgcolhTranId"
        Me.objdgcolhTranId.Name = "objdgcolhTranId"
        Me.objdgcolhTranId.ReadOnly = True
        Me.objdgcolhTranId.Visible = False
        '
        'objdgcolhGuid
        '
        Me.objdgcolhGuid.HeaderText = "objdgcolhGuid"
        Me.objdgcolhGuid.Name = "objdgcolhGuid"
        Me.objdgcolhGuid.ReadOnly = True
        Me.objdgcolhGuid.Visible = False
        '
        'objdgcolhAUD
        '
        Me.objdgcolhAUD.HeaderText = "objdgcolhAUD"
        Me.objdgcolhAUD.Name = "objdgcolhAUD"
        Me.objdgcolhAUD.ReadOnly = True
        Me.objdgcolhAUD.Visible = False
        '
        'objdgcolhIsBeingEdit
        '
        Me.objdgcolhIsBeingEdit.HeaderText = "objdgcolhIsBeingEdit"
        Me.objdgcolhIsBeingEdit.Name = "objdgcolhIsBeingEdit"
        Me.objdgcolhIsBeingEdit.ReadOnly = True
        Me.objdgcolhIsBeingEdit.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(538, 187)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 147
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(538, 151)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 146
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(538, 115)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 145
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 85)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(624, 13)
        Me.objelLine1.TabIndex = 144
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddAccType
        '
        Me.objbtnAddAccType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddAccType.BorderSelected = False
        Me.objbtnAddAccType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddAccType.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mini_Add
        Me.objbtnAddAccType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddAccType.Location = New System.Drawing.Point(254, 61)
        Me.objbtnAddAccType.Name = "objbtnAddAccType"
        Me.objbtnAddAccType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddAccType.TabIndex = 142
        '
        'cboAccountType
        '
        Me.cboAccountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountType.FormattingEnabled = True
        Me.cboAccountType.Location = New System.Drawing.Point(90, 61)
        Me.cboAccountType.Name = "cboAccountType"
        Me.cboAccountType.Size = New System.Drawing.Size(158, 21)
        Me.cboAccountType.TabIndex = 141
        '
        'lblAccountType
        '
        Me.lblAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountType.Location = New System.Drawing.Point(8, 63)
        Me.lblAccountType.Name = "lblAccountType"
        Me.lblAccountType.Size = New System.Drawing.Size(80, 16)
        Me.lblAccountType.TabIndex = 140
        Me.lblAccountType.Text = "Account Type"
        Me.lblAccountType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddBranch
        '
        Me.objbtnAddBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBranch.BorderSelected = False
        Me.objbtnAddBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBranch.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mini_Add
        Me.objbtnAddBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBranch.Location = New System.Drawing.Point(614, 33)
        Me.objbtnAddBranch.Name = "objbtnAddBranch"
        Me.objbtnAddBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBranch.TabIndex = 139
        '
        'objbtnAddBankName
        '
        Me.objbtnAddBankName.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBankName.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBankName.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBankName.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBankName.BorderSelected = False
        Me.objbtnAddBankName.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBankName.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mini_Add
        Me.objbtnAddBankName.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBankName.Location = New System.Drawing.Point(254, 34)
        Me.objbtnAddBankName.Name = "objbtnAddBankName"
        Me.objbtnAddBankName.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBankName.TabIndex = 138
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(356, 34)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(252, 21)
        Me.cboBranch.TabIndex = 3
        '
        'cboBank
        '
        Me.cboBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBank.FormattingEnabled = True
        Me.cboBank.Location = New System.Drawing.Point(90, 34)
        Me.cboBank.Name = "cboBank"
        Me.cboBank.Size = New System.Drawing.Size(158, 21)
        Me.cboBank.TabIndex = 1
        '
        'lblAccountNo
        '
        Me.lblAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountNo.Location = New System.Drawing.Point(281, 63)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.Size = New System.Drawing.Size(73, 16)
        Me.lblAccountNo.TabIndex = 6
        Me.lblAccountNo.Text = "Account No."
        Me.lblAccountNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAccountNo
        '
        Me.txtAccountNo.Flags = 0
        Me.txtAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAccountNo.Location = New System.Drawing.Point(356, 61)
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.Size = New System.Drawing.Size(106, 21)
        Me.txtAccountNo.TabIndex = 7
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(281, 36)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(69, 16)
        Me.lblBranch.TabIndex = 2
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBankNo
        '
        Me.lblBankNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankNo.Location = New System.Drawing.Point(467, 63)
        Me.lblBankNo.Name = "lblBankNo"
        Me.lblBankNo.Size = New System.Drawing.Size(48, 16)
        Me.lblBankNo.TabIndex = 4
        Me.lblBankNo.Text = "Bank #"
        Me.lblBankNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBankNo
        '
        Me.txtBankNo.Flags = 0
        Me.txtBankNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBankNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBankNo.Location = New System.Drawing.Point(519, 61)
        Me.txtBankNo.Name = "txtBankNo"
        Me.txtBankNo.Size = New System.Drawing.Size(89, 21)
        Me.txtBankNo.TabIndex = 5
        '
        'lblBankName
        '
        Me.lblBankName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankName.Location = New System.Drawing.Point(8, 36)
        Me.lblBankName.Name = "lblBankName"
        Me.lblBankName.Size = New System.Drawing.Size(76, 16)
        Me.lblBankName.TabIndex = 0
        Me.lblBankName.Text = "Bank Name"
        Me.lblBankName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 566)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(681, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(468, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(571, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblAuthenticationProtocol
        '
        Me.lblAuthenticationProtocol.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuthenticationProtocol.Location = New System.Drawing.Point(8, 254)
        Me.lblAuthenticationProtocol.Name = "lblAuthenticationProtocol"
        Me.lblAuthenticationProtocol.Size = New System.Drawing.Size(124, 17)
        Me.lblAuthenticationProtocol.TabIndex = 133
        Me.lblAuthenticationProtocol.Text = "Authentication Protocol"
        '
        'cboAuthenticationProtocol
        '
        Me.cboAuthenticationProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuthenticationProtocol.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAuthenticationProtocol.FormattingEnabled = True
        Me.cboAuthenticationProtocol.Location = New System.Drawing.Point(138, 252)
        Me.cboAuthenticationProtocol.Name = "cboAuthenticationProtocol"
        Me.cboAuthenticationProtocol.Size = New System.Drawing.Size(119, 21)
        Me.cboAuthenticationProtocol.TabIndex = 153
        '
        'frmCompany_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(681, 621)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCompany_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Company"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tabcCompanyProfile.ResumeLayout(False)
        Me.tabpCompanyInfo.ResumeLayout(False)
        Me.gbCompanyInformation.ResumeLayout(False)
        Me.gbCompanyInformation.PerformLayout()
        Me.tabcOtherInfo.ResumeLayout(False)
        Me.tabpCompamyPeriod.ResumeLayout(False)
        CType(Me.nudToDay, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpLocalization.ResumeLayout(False)
        Me.gbLocalization.ResumeLayout(False)
        Me.gbLocalization.PerformLayout()
        Me.tabpLogo.ResumeLayout(False)
        Me.tabpOtherInfo.ResumeLayout(False)
        Me.tabpOtherInfo.PerformLayout()
        Me.tabpStamp.ResumeLayout(False)
        Me.tabpBankingInfo.ResumeLayout(False)
        Me.gbEmailSetup.ResumeLayout(False)
        Me.pnlEmail.ResumeLayout(False)
        Me.pnlEmail.PerformLayout()
        Me.pnlExchangeWebService.ResumeLayout(False)
        Me.pnlExchangeWebService.PerformLayout()
        Me.gbBankingInfo.ResumeLayout(False)
        Me.gbBankingInfo.PerformLayout()
        Me.pnlBank.ResumeLayout(False)
        CType(Me.dgvBankInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tabcCompanyProfile As System.Windows.Forms.TabControl
    Friend WithEvents tabpCompanyInfo As System.Windows.Forms.TabPage
    Friend WithEvents gbCompanyInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tabpBankingInfo As System.Windows.Forms.TabPage
    Friend WithEvents gbBankingInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtCompanyName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompanyName As System.Windows.Forms.Label
    Friend WithEvents elAddressInfo As eZee.Common.eZeeLine
    Friend WithEvents txtAddressLine1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents txtAddressLine2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents lblPostal As System.Windows.Forms.Label
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents elContactInfo As eZee.Common.eZeeLine
    Friend WithEvents lblPhone3 As System.Windows.Forms.Label
    Friend WithEvents lblPhone2 As System.Windows.Forms.Label
    Friend WithEvents lblPhone1 As System.Windows.Forms.Label
    Friend WithEvents txtPhone3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPhone2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPhone1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblWebsite As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtWebsite As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCompanyEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents elRegistrationInfo As eZee.Common.eZeeLine
    Friend WithEvents lblVATNo As System.Windows.Forms.Label
    Friend WithEvents lblTinNo As System.Windows.Forms.Label
    Friend WithEvents txtVRNNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTinNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRegdNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRegNo As System.Windows.Forms.Label
    Friend WithEvents lblDistrict As System.Windows.Forms.Label
    Friend WithEvents txtDistrictNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNSSFNo As System.Windows.Forms.Label
    Friend WithEvents txtNSSFNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPPFNo As System.Windows.Forms.Label
    Friend WithEvents txtPPFNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPayrollNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPayWorkCheck As System.Windows.Forms.Label
    Friend WithEvents gbEmailSetup As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblBankName As System.Windows.Forms.Label
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents lblBankNo As System.Windows.Forms.Label
    Friend WithEvents txtBankNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAccountNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAccountNo As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents elOtherRegistrationNo As eZee.Common.eZeeLine
    Friend WithEvents lblCompanyRegNo As System.Windows.Forms.Label
    Friend WithEvents txtCompamyRegNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabcOtherInfo As System.Windows.Forms.TabControl
    Friend WithEvents tabpLogo As System.Windows.Forms.TabPage
    Friend WithEvents tabpCompamyPeriod As System.Windows.Forms.TabPage
    Friend WithEvents lblYearTo As System.Windows.Forms.Label
    Friend WithEvents lblFromYear As System.Windows.Forms.Label
    Friend WithEvents cboToMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblToMonth As System.Windows.Forms.Label
    Friend WithEvents nudToDay As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblToDay As System.Windows.Forms.Label
    Friend WithEvents cboFromMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromMonth As System.Windows.Forms.Label
    Friend WithEvents nudDay As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblFromDay As System.Windows.Forms.Label
    Friend WithEvents tabpOtherInfo As System.Windows.Forms.TabPage
    Friend WithEvents ImgCompanyLogo As eZee.Common.eZeeImageControl
    Friend WithEvents txtRegCaption2Value As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objlblCaption2 As System.Windows.Forms.Label
    Friend WithEvents objlblCaption3 As System.Windows.Forms.Label
    Friend WithEvents txtRegCaption3Value As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRegCaption1Value As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objlblCaption1 As System.Windows.Forms.Label
    Friend WithEvents lnkEditCaption As System.Windows.Forms.LinkLabel
    Friend WithEvents cboZipcode As System.Windows.Forms.ComboBox
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents cboBank As System.Windows.Forms.ComboBox
    Friend WithEvents cboToYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboFromYear As System.Windows.Forms.ComboBox
    Friend WithEvents pnlEmail As System.Windows.Forms.Panel
    Friend WithEvents lblReference As System.Windows.Forms.Label
    Friend WithEvents txtReference As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSenderInformation As System.Windows.Forms.Label
    Friend WithEvents lblSenderName As System.Windows.Forms.Label
    Friend WithEvents lblEmailAddress As System.Windows.Forms.Label
    Friend WithEvents txtMailSenderEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMailSenderName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMailServerPort As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMailServerPort As System.Windows.Forms.Label
    Friend WithEvents lblMailServer As System.Windows.Forms.Label
    Friend WithEvents txtMailServer As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblServerInformation As System.Windows.Forms.Label
    Friend WithEvents txtPassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtUserName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents chkSSL As System.Windows.Forms.CheckBox
    Friend WithEvents lblUserInformation As System.Windows.Forms.Label
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents btnTestMail As eZee.Common.eZeeLightButton
    Friend WithEvents lblTestingMessage As System.Windows.Forms.Label
    Friend WithEvents lblTestingTitle As System.Windows.Forms.Label
    Friend WithEvents txtBody As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBody As System.Windows.Forms.Label
    Friend WithEvents objbtnAddAccType As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAccountType As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountType As System.Windows.Forms.Label
    Friend WithEvents objbtnAddBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBankName As eZee.Common.eZeeGradientButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents dgvBankInfo As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhBankGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAccType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAccTypeNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBankNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBankId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBranchId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAccTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCompanyId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGuid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAUD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlBank As System.Windows.Forms.Panel
    Friend WithEvents objdgcolhIsBeingEdit As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents tabpLocalization As System.Windows.Forms.TabPage
    Friend WithEvents gbLocalization As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtDefaultCurrency As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDefaultCurrency As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboLocalCountry As System.Windows.Forms.ComboBox
    Friend WithEvents tabpStamp As System.Windows.Forms.TabPage
    Friend WithEvents ImgCompanyStamp As eZee.Common.eZeeImageControl
    Friend WithEvents chkCertificateAuthentication As System.Windows.Forms.CheckBox
    Friend WithEvents radExchangeWebService As System.Windows.Forms.RadioButton
    Friend WithEvents radSMTP As System.Windows.Forms.RadioButton
    Friend WithEvents pnlExchangeWebService As System.Windows.Forms.Panel
    Friend WithEvents txtEWSDomain As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEWSUrl As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEWSDomain As System.Windows.Forms.Label
    Friend WithEvents lblEWSUrl As System.Windows.Forms.Label
    Friend WithEvents chkBypassProxyServer As System.Windows.Forms.CheckBox
    Friend WithEvents cboAuthenticationProtocol As System.Windows.Forms.ComboBox
    Friend WithEvents lblAuthenticationProtocol As System.Windows.Forms.Label
End Class
