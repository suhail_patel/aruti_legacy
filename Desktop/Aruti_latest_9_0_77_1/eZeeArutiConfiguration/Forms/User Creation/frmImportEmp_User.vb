﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportEmp_User

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportEmp_User"
    Dim dvGriddata As DataView = Nothing
    Private mdt_ImportData As New DataTable
    Private m_dsImportData As DataSet
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Private mintYearUnkid As Integer = 0

#End Region

#Region " From's Events "

    Private Sub frmImportEmp_User_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            pnlInfo.Enabled = False
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmImportEmp_User_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany_Master
        Dim dsCombo As New DataSet
        Dim dTable As New DataTable
        Dim dRow As DataRow = Nothing
        Try
            dsCombo = objCompany.GetList("List", True)
            dRow = dsCombo.Tables(0).NewRow
            dRow.Item("companyunkid") = 0
            dRow.Item("code") = ""
            dRow.Item("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dsCombo.Tables(0).Rows.Add(dRow)
            dTable = New DataView(dsCombo.Tables(0), "", "companyunkid ASC", DataViewRowState.CurrentRows).ToTable
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dTable
                .SelectedValue = 0
            End With

            cboLanguage.Items.Clear()
            With cboLanguage
                .Items.Add(Language.getMessage("Language", 5, "Default English"))
                .Items.Add(Language.getMessage("Language", 6, "Custom 1"))
                .Items.Add(Language.getMessage("Language", 7, "Custom 2"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            mdt_ImportData = m_dsImportData.Tables(0).Clone
            mdt_ImportData.Rows.Clear() : objTotal.Text = "0"
            mdt_ImportData.Columns.Add("image", System.Type.GetType("System.Object")).DefaultValue = New Drawing.Bitmap(1, 1).Clone
            mdt_ImportData.Columns.Add("message", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData.Columns.Add("status", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData.Columns.Add("objStatus", System.Type.GetType("System.String")).DefaultValue = "0"

            For Each dtRow As DataRow In m_dsImportData.Tables(0).Rows
                dtRow.Item("password") = clsSecurity.Decrypt(CStr(dtRow.Item("password")), "ezee")
                mdt_ImportData.ImportRow(dtRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            colhEmployee.DataPropertyName = "displayname"
            colhMessage.DataPropertyName = "message"
            objcolhImage.DataPropertyName = "image"
            colhStatus.DataPropertyName = "status"
            objcolhstatus.DataPropertyName = "objStatus"
            dgData.AutoGenerateColumns = False
            dvGriddata = New DataView(mdt_ImportData)
            dgData.DataSource = dvGriddata
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        End Try
    End Sub

    Private Function Savefile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New IO.FileStream(fpath, IO.FileMode.Create, IO.FileAccess.Write)
        Dim strWriter As New IO.StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, IO.SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveExcelfile", "modGlobal")
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    Private Function Export_ErrorList(ByVal StrPath As String, ByVal strSourceName As String) As Boolean
        Dim strBuilder As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            strBuilder.Append(" <HTML> " & vbCrLf)
            strBuilder.Append(" <TITLE>" & strSourceName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=100%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            For j As Integer = 0 To dgData.ColumnCount - 1
                If dgData.Columns(j).Name.ToString.ToUpper.StartsWith("OBJ") = False Then
                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dgData.Columns(j).HeaderText & "</B></FONT></TD>" & vbCrLf)
                End If
            Next

            For i As Integer = 0 To dgData.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To dgData.ColumnCount - 1
                    If dgData.Columns(k).Name.ToString.ToUpper.StartsWith("OBJ") = False Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CStr(dgData.Rows(i).Cells(k).Value) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </BODY> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)

            If Savefile(StrPath, strBuilder) Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Export_ErrorList", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCompany.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboCompany.ValueMember
                .DisplayMember = cboCompany.DisplayMember
                .DataSource = CType(cboCompany.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboCompany.SelectedValue = frm.SelectedValue
            End If
            cboCompany.Focus()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchCompany_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Try

            'S.SANDEEP [21-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : No Checking was placed for Role Deletion, Inspite of being used in user master
            btnImport.Enabled = False : btnFilter.Enabled = False
            ezWait.Active = True
            'S.SANDEEP [21-MAR-2017] -- END

            Dim objUsr As clsUserAddEdit = Nothing
            Dim iUserId As Integer = 0

            'Varsha (10 Nov 2017) -- Start
            'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
            Dim objMasterData As New clsMasterData
            Dim intThemeId As Integer = objMasterData.GetDefaultThemeId(Company._Object._Companyunkid, Nothing)
            'Varsha (10 Nov 2017)) -- End

            For Each dtRow As DataRow In mdt_ImportData.Rows

                'S.SANDEEP [21-MAR-2017] -- START
                'ISSUE/ENHANCEMENT : No Checking was placed for Role Deletion, Inspite of being used in user master
                'Application.DoEvents()
                'ezWait.Refresh()
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData.Rows.IndexOf(dtRow) - 5
                Application.DoEvents()
                ezWait.Refresh()
                Catch ex As Exception
                End Try
                'S.SANDEEP [21-MAR-2017] -- END


                

                If objUsr IsNot Nothing Then objUsr = Nothing
                objUsr = New clsUserAddEdit

                'S.SANDEEP [ 01 APR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                iUserId = 0 : iUserId = objUsr.isUExist(CInt(dtRow.Item("employeeunkid")), CInt(cboCompany.SelectedValue))
                objUsr._Userunkid = iUserId
                'S.SANDEEP [ 01 APR 2013 ] -- END

                objUsr._Address1 = dtRow.Item("present_address1").ToString
                objUsr._Address2 = dtRow.Item("present_address2").ToString
                objUsr._CreatedById = User._Object._Userunkid
                objUsr._Creation_Date = CDate(dtRow.Item("TDate"))
                objUsr._Email = dtRow.Item("email").ToString
                objUsr._Firstname = dtRow.Item("firstname").ToString
                objUsr._Isactive = True
                objUsr._Languageunkid = CInt(cboLanguage.SelectedIndex)
                objUsr._Lastname = dtRow.Item("surname").ToString
                'S.SANDEEP [ 08 NOV 2013 ] -- START
                If dtRow.Item("password").ToString.Trim.Length <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Sorry, Password is blank.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                Else
                objUsr._Password = dtRow.Item("password").ToString
                End If
                'S.SANDEEP [ 08 NOV 2013 ] -- END
                objUsr._Phone = dtRow.Item("present_tel_no").ToString
                'objUsr._Roleunkid = 0
                objUsr._Username = dtRow.Item("displayname").ToString
                objUsr._EmployeeUnkid = CInt(dtRow.Item("employeeunkid"))
                objUsr._EmployeeCompanyUnkid = CInt(cboCompany.SelectedValue)
                objUsr._AssignCompanyPrivilegeIDs = mintYearUnkid.ToString
                If IsDBNull(dtRow.Item("suspended_from_date")) = False Then
                    objUsr._Suspended_From_Date = CDate(dtRow.Item("suspended_from_date"))
                Else
                    objUsr._Suspended_From_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("suspended_to_date")) = False Then
                    objUsr._Suspended_To_Date = CDate(dtRow.Item("suspended_to_date"))
                Else
                    objUsr._Suspended_To_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("termination_from_date")) = False Then
                    objUsr._Termination_Date = CDate(dtRow.Item("termination_from_date"))
                Else
                    objUsr._Termination_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("termination_to_date")) = False Then
                    objUsr._Retirement_Date = CDate(dtRow.Item("termination_to_date"))
                Else
                    objUsr._Retirement_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("probation_from_date")) = False Then
                    objUsr._Probation_From_Date = CDate(dtRow.Item("probation_from_date"))
                Else
                    objUsr._Probation_From_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("probation_to_date")) = False Then
                    objUsr._Probation_To_Date = CDate(dtRow.Item("probation_to_date"))
                Else
                    objUsr._Probation_To_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("empl_enddate")) = False Then
                    objUsr._Empl_Enddate = CDate(dtRow.Item("empl_enddate"))
                Else
                    objUsr._Empl_Enddate = Nothing
                End If

                If IsDBNull(dtRow.Item("appointeddate")) = False Then
                    objUsr._Appointeddate = CDate(dtRow.Item("appointeddate"))
                Else
                    objUsr._Appointeddate = Nothing
                End If

                If IsDBNull(dtRow.Item("reinstatement_date")) = False Then
                    objUsr._Reinstatement_Date = CDate(dtRow.Item("reinstatement_date"))
                Else
                    objUsr._Reinstatement_Date = Nothing
                End If

                'Varsha (10 Nov 2017) -- Start
                'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
                objUsr._Theme_Id = intThemeId
                'Varsha (10 Nov 2017) -- End

                'S.SANDEEP [ 01 APR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If iUserId > 0 Then
                    'S.SANDEEP [14 SEP 2016] -- START
                    'ENHANCEMENT : USER PRIVILEGE 
                    If objUsr._Roleunkid > 0 Then
                        Dim dsPrivilege As New DataSet
                        dsPrivilege = objUsr.getPrivilegeList(objUsr._Roleunkid, iUserId)
                        'S.SANDEEP [21-MAR-2017] -- START
                        'ISSUE/ENHANCEMENT : No Checking was placed for Role Deletion, Inspite of being used in user master
                        'If dsPrivilege.Tables(0).Rows.Count > 0 Then
                        '    objUsr._AssignPrivilegeIDs = String.Join(",", dsPrivilege.Tables(0).AsEnumerable().Where(Function(x) CBool(x.Field(Of Integer)("assign")) = True).Select(Function(x) x.Field(Of Integer)("privilegeunkid").ToString()).ToArray())
                        'End If
                        If dsPrivilege IsNot Nothing AndAlso dsPrivilege.Tables.Count > 0 Then
                        If dsPrivilege.Tables(0).Rows.Count > 0 Then
                            objUsr._AssignPrivilegeIDs = String.Join(",", dsPrivilege.Tables(0).AsEnumerable().Where(Function(x) CBool(x.Field(Of Integer)("assign")) = True).Select(Function(x) x.Field(Of Integer)("privilegeunkid").ToString()).ToArray())
                        End If
                    End If
                        'S.SANDEEP [21-MAR-2017] -- END
                    End If
                    'S.SANDEEP [14 SEP 2016] -- START

'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objUsr._FormName = mstrModuleName
                    objUsr._LoginEmployeeunkid = 0
                    objUsr._ClientIP = getIP()
                    objUsr._HostName = getHostName()
                    objUsr._FromWeb = False
                    objUsr._AuditUserId = User._Object._Userunkid
objUsr._CompanyUnkid = Company._Object._Companyunkid
                    objUsr._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objUsr.Update = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objUsr._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    Else
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 4, "Successfully Updated")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    End If
                Else

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objUsr._FormName = mstrModuleName
                    objUsr._LoginEmployeeunkid = 0
                    objUsr._ClientIP = getIP()
                    objUsr._HostName = getHostName()
                    objUsr._FromWeb = False
                    objUsr._AuditUserId = User._Object._Userunkid
objUsr._CompanyUnkid = Company._Object._Companyunkid
                    objUsr._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                If objUsr.Insert = False Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objUsr._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                Else
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 3, "Successfully Inserted")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                End If
                End If
                'S.SANDEEP [ 01 APR 2013 ] -- END
            Next
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
            'S.SANDEEP [21-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : No Checking was placed for Role Deletion, Inspite of being used in user master
            'btnImport.Enabled = False
            btnImport.Enabled = True : btnFilter.Enabled = True : ezWait.Active = False
            'S.SANDEEP [21-MAR-2017] -- END
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            If CInt(cboCompany.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                Dim objMaster As New clsMasterData
                Dim dsYear As New DataSet
                dsYear = objMaster.Get_Database_Year_List("List", False, CInt(cboCompany.SelectedValue))
                If dsYear.Tables(0).Rows.Count > 0 Then
                    mintYearUnkid = CInt(dsYear.Tables(0).Rows(0).Item("yearunkid"))
                    'S.SANDEEP [12 MAY 2015] -- START
                    'm_dsImportData = objEmp.GetEmployeeForUser(CStr(dsYear.Tables(0).Rows(0).Item("database_name")), CInt(cboCompany.SelectedValue))
                    Dim objConfig As New clsConfigOptions
                    objConfig._Companyunkid = CInt(cboCompany.SelectedValue)

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'm_dsImportData = objEmp.GetEmployeeForUser(CStr(dsYear.Tables(0).Rows(0).Item("database_name")), CInt(cboCompany.SelectedValue), User._Object._Userunkid, eZeeDate.convertDate(objConfig._EmployeeAsOnDate))
                    m_dsImportData = objEmp.GetEmployeeForUser(CStr(dsYear.Tables(0).Rows(0).Item("database_name")), CInt(cboCompany.SelectedValue), User._Object._Userunkid, eZeeDate.convertDate(objConfig._EmployeeAsOnDate), mintYearUnkid, objConfig._UserAccessModeSetting)
                    'S.SANDEEP [04 JUN 2015] -- END

                    'S.SANDEEP [12 MAY 2015] -- END
                    objConfig = Nothing
                    Call CreateDataTable()
                End If
                If btnImport.Enabled = False Then btnImport.Enabled = True
                objSuccess.Text = "0" : objError.Text = "0"
            Else
                dgData.DataSource = Nothing
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                Dim savDialog As New SaveFileDialog
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If Export_ErrorList(savDialog.FileName, "Importing Employee As User") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbImportEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbImportEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbImportEmployee.Text = Language._Object.getCaption(Me.gbImportEmployee.Name, Me.gbImportEmployee.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
			Me.lblLanguage.Text = Language._Object.getCaption(Me.lblLanguage.Name, Me.lblLanguage.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Fail")
			Language.setMessage(mstrModuleName, 3, "Successfully Inserted")
			Language.setMessage(mstrModuleName, 4, "Successfully Updated")
			Language.setMessage("Language", 5, "Default English")
			Language.setMessage("Language", 6, "Custom 1")
			Language.setMessage("Language", 7, "Custom 2")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class