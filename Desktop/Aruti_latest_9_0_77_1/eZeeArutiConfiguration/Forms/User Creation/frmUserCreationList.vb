﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmUserCreationList

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmUserCreationList"
    Private objUserAddEdit As clsUserAddEdit

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Dim objUserRole As New clsUserRole_Master
        Try
            'Nilay (01-Mar-2016) -- Start
            'dsList = objUserAddEdit.getNewComboList("List", , True)
            dsList = objUserAddEdit.getNewComboList("List", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            Dim dTable As New DataTable
            If User._Object.Privilege._AllowtoViewUserList = False Then
                dTable = New DataView(dsList.Tables(0), "userunkid IN (0," & User._Object._Userunkid & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = dsList.Tables(0)
            End If
            With cboUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dTable
            End With

            dsList = objUserRole.getComboList("List", True)
            With cboRole
                .ValueMember = "roleunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            dsList = Nothing
            objUserRole = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            Dim objOption As New clsPassowdOptions
            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            If dgvUserList.SelectedRows.Count > 0 Then
                If User._Object._Userunkid = CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value) Or CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value) = 1 Then
                    btnDelete.Enabled = False
                Else
                    btnDelete.Enabled = User._Object.Privilege._AllowDeleteUser
                End If
            End If


            If drOption.Length > 0 Then
                If objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                    mnuImportADusers.Enabled = User._Object.Privilege._ImportADUsers
                    'Pinkal (23-AUG-2017) -- Start
                    'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.
                    mnuEnableDisableADusers.Enabled = User._Object.Privilege._AllowToEnableDisableADUser
                    'Pinkal (23-AUG-2017) -- End

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    Dim mstrCreateADUserFromEmpMst As String = ""
                    Dim objConfig As New clsConfigOptions
                    objConfig.IsValue_Changed("CreateADUserFromEmpMst", "-999", mstrCreateADUserFromEmpMst)
                    objConfig = Nothing
                    mnuImportADAttributeMapping.Enabled = CBool(mstrCreateADUserFromEmpMst)
                    'Pinkal (18-Aug-2018) -- End

                Else
                    mnuImportADusers.Enabled = False
                    'Pinkal (23-AUG-2017) -- Start
                    'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.
                    mnuEnableDisableADusers.Enabled = False
                    'Pinkal (23-AUG-2017) -- End
                End If
            End If

            mnuEditUserAccess.Enabled = User._Object.Privilege._EditUserAccess
            mnuExportOperationalPrivilege.Enabled = User._Object.Privilege._ExportPrivilegs
            mnuAssignUserAccess.Enabled = User._Object.Privilege._AssignUserAccess
            mnuReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
            btnNew.Enabled = User._Object.Privilege._AllowAddUser
            btnEdit.Enabled = User._Object.Privilege._AllowEditUser

            Dim objPOption As New clsPassowdOptions
            If objPOption._IsEmployeeAsUser = True AndAlso User._Object.Privilege._AllowtoImportEmployee_User = True Then
                mnuImportEmp_User.Enabled = True
            Else
                mnuImportEmp_User.Enabled = False
            End If

            If objPOption._IsEmployeeAsUser = True Then
                btnDelete.Enabled = False : btnNew.Enabled = False
                If objPOption._EnableAllUser = True Then
                    btnNew.Enabled = True
                End If
            End If



            objPOption = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillUserList()
        Dim dtTable As DataTable = Nothing
        Dim StrSearching As String = ""
        Try

            If User._Object.Privilege._AllowtoViewUserList = False Then
                StrSearching &= "AND cfuser_master.userunkid = " & User._Object._Userunkid
            End If

            If CInt(cboUser.SelectedValue) > 0 Then
                Dim dtmp() As DataRow = CType(cboUser.DataSource, DataTable).Select("userunkid = '" & CInt(cboUser.SelectedValue) & "'")
                StrSearching &= "AND cfuser_master.userunkid = " & CInt(cboUser.SelectedValue) & " "
                If dtmp.Length > 0 Then
                    StrSearching &= " AND cfuser_master.companyunkid = '" & dtmp(0).Item("companyunkid").ToString & "' "
                End If
            End If

            If CInt(cboRole.SelectedValue) > 0 Then
                StrSearching &= "AND cfuser_master.roleunkid = " & CInt(cboRole.SelectedValue) & " "
            End If

            Dim iUserViewType As clsUserAddEdit.enViewUserType

            If radShowActiveUser.Checked = True Then
                iUserViewType = clsUserAddEdit.enViewUserType.SHOW_ACTIVE_USER
            ElseIf radShowInActiveUser.Checked = True Then
                iUserViewType = clsUserAddEdit.enViewUserType.SHOW_INACTIVE_USER
            ElseIf radShowAllUser.Checked = True Then
                iUserViewType = clsUserAddEdit.enViewUserType.SHOW_ALL_USER
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dtTable = objUserAddEdit.GetGroupedUserList("List", iUserViewType, Now, Now, StrSearching, , , True)

            dgvUserList.AutoGenerateColumns = False

            dgcolhUserName.DataPropertyName = "Username"
            dgcolhFirstName.DataPropertyName = "Firstname"
            dgcolhLastName.DataPropertyName = "Lastname"
            dgcolhEmail.DataPropertyName = "Email"
            dgcolhUserRole.DataPropertyName = "URole"
            objdgcolhUserUnkid.DataPropertyName = "UserUnkid"
            objdgcolhRoleUnkid.DataPropertyName = "RoleUnkid"
            objdgcolhEmployeeUnkid.DataPropertyName = "EmployeeUnkid"
            objdgcolhCompanyUnkid.DataPropertyName = "companyunkid"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            objdgcolhActiveStatus.DataPropertyName = "activestatus"
            dgcolhIsManager.DataPropertyName = "Manager"

            dgvUserList.DataSource = dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillUserList", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmUserCreationList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And dgvUserList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserCreationList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUserCreationList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserCreationList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUserCreationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUserAddEdit = New clsUserAddEdit
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call FillCombo()

            Call SetVisibility()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserCreationList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUserCreationList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objUserAddEdit = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUserAddEdit.SetMessages()
            objfrm._Other_ModuleNames = "clsUserAddEdit"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If dgvUserList.SelectedRows.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            dgvUserList.Select()
            Exit Sub
        End If

        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this User?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objUserAddEdit._FormName = mstrModuleName
                objUserAddEdit._LoginEmployeeunkid = 0
                objUserAddEdit._ClientIP = getIP()
                objUserAddEdit._HostName = getHostName()
                objUserAddEdit._FromWeb = False
                objUserAddEdit._AuditUserId = User._Object._Userunkid
objUserAddEdit._CompanyUnkid = Company._Object._Companyunkid
                objUserAddEdit._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objUserAddEdit.Delete(CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value))
                Call FillUserList()

                If dgvUserList.RowCount <= 0 Then
                    Exit Try
                End If
            End If
            dgvUserList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If dgvUserList.SelectedRows.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            dgvUserList.Select()
            Exit Sub
        End If
        Dim frm As New frmUser_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillUserList()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmUser_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillUserList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboUser.ValueMember
                .CodeMember = cboUser.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboUser.SelectedValue = objFrm.SelectedValue
            End If
            cboUser.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillUserList()
            If dgvUserList.DataSource IsNot Nothing Then
                objbtnSearch.ShowResult(CType(dgvUserList.DataSource, DataTable).Select("IsGrp = False").Length.ToString)
            Else
                objbtnReset.ShowResult("0")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboRole.SelectedValue = 0
            cboUser.SelectedValue = 0
            radShowActiveUser.Checked = True
            Call FillUserList()
            If dgvUserList.DataSource IsNot Nothing Then
                objbtnReset.ShowResult(CType(dgvUserList.DataSource, DataTable).Select("IsGrp = False").Length.ToString)
            Else
                objbtnReset.ShowResult("0")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Menu Events "

    Private Sub mnuReportPrivilege_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportPrivilege.Click
        Dim frm As New frmReportPrivilegeAssignment
        Try
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes

            'If dgvUserList.SelectedRows.Count < 1 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one user to assign Report Privilege."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Dim frm As New frmAssignReportPrivilege
            'frm._User_Name = dgvUserList.SelectedRows(0).Cells(dgcolhUserName.Index).Value.ToString.Trim
            'frm._UserId = CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value)
            'frm.ShowDialog()

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(-1, enAction.ADD_ONE)

            'S.SANDEEP [10 AUG 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuReportPrivilege_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    Private Sub mnuEditReportPrivilege_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditReportPrivilege.Click
        Dim frm As New frmReportPrivilegeAssignment
        Try

            'Pinkal (12-Apr-2016) -- Start
            'Enhancement - WORKED ON Edit Report Priviledge For User.
            If dgvUserList.SelectedRows.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvUserList.Select()
                Exit Sub
            End If
            'Pinkal (12-Apr-2016) -- End

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value), enAction.EDIT_ONE)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEditReportPrivilege_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [10 AUG 2015] -- END

    Private Sub mnuEditUserAccess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditUserAccess.Click
        Dim frm As New frmUserAccess
        Try
            'Pinkal (12-Apr-2016) -- Start
            'Enhancement - WORKED ON Edit Report Priviledge For User.
            If dgvUserList.SelectedRows.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvUserList.Select()
                Exit Sub
            End If
            'Pinkal (12-Apr-2016) -- End


            frm.displayDialog(CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value), enAction.EDIT_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEditUserAccess_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuAssignUserAccess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssignUserAccess.Click
        Dim frm As New frmUserAccess
        Try
            'S.SANDEEP [11-OCT-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 79
            ''Pinkal (12-Apr-2016) -- Start
            ''Enhancement - WORKED ON Edit Report Priviledge For User.
            'If dgvUserList.SelectedRows.Count < 1 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            '    dgvUserList.Select()
            '    Exit Sub
            'End If
            ''Pinkal (12-Apr-2016) -- End
            'S.SANDEEP [11-OCT-2017] -- END

            

            frm.displayDialog(-1, enAction.ADD_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssignUserAccess_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuImportADusers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportADusers.Click
        Try
            Dim objImport As New frmImportADUsers
            objImport.ShowDialog()
            Call FillUserList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportADusers_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuExportOperationalPrivilege_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportOperationalPrivilege.Click
        Try
            If dgvUserList.SelectedRows.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvUserList.Select()
                Exit Sub
            End If

            Dim intRoleId As Integer = 0
            If IsDBNull(dgvUserList.SelectedRows(0).Cells(objdgcolhRoleUnkid.Index).Value) = False Then
                intRoleId = CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhRoleUnkid.Index).Value)
            End If

            If intRoleId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, No Role is assigned to the selected user."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dsPrivilege As New DataSet
            dsPrivilege = objUserAddEdit.getPrivilegeList(intRoleId, CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value))

            'S.SANDEEP [21-SEP-2018] -- START
            'dsPrivilege.Tables(0).Columns.Remove("privilegeunkid")
            'dsPrivilege.Tables(0).Columns.Remove("privilegegroupunkid")

            'Dim strBuilder As New System.Text.StringBuilder
            'Dim StrPvGrp_Name As String = String.Empty

            'strBuilder.Append(" <HTML> " & vbCrLf)
            'strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            'strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH='75%'> " & vbCrLf)
            'strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='75%'> " & vbCrLf)
            'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '70%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 4, "Operational Privilege for User : ") & dgvUserList.SelectedRows(0).Cells(dgcolhUserName.Index).Value.ToString.Trim & "</B></FONT></TD>" & vbCrLf)
            'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '5%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 5, "User Role : ") & dgvUserList.SelectedRows(0).Cells(dgcolhUserRole.Index).Value.ToString.Trim & "</B></FONT></TD>" & vbCrLf)
            'strBuilder.Append(" </TR> " & vbCrLf)
            'strBuilder.Append(" <BR> " & vbCrLf)
            'strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP WIDTH='75%'> " & vbCrLf)
            'strBuilder.Append(" <TD BORDER=1 ALIGN='MIDDLE' WIDTH = '70%'><FONT SIZE=2><B>" & "PRIVILEGE(S)" & "</B></FONT></TD>" & vbCrLf)
            'strBuilder.Append(" <TD BORDER=1 ALIGN='MIDDLE' WIDTH = '5%'><FONT SIZE=2><B>" & "ASSIGNED" & "</B></FONT></TD>" & vbCrLf)
            'strBuilder.Append(" </TR> " & vbCrLf)

            'For i As Integer = 0 To dsPrivilege.Tables(0).Rows.Count - 1
            '    If StrPvGrp_Name <> CStr(dsPrivilege.Tables(0).Rows(i).Item("group_name")) Then
            '        strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='75%'> " & vbCrLf)
            '        strBuilder.Append(" <TD BORDER=1 ALIGN='LEFT' COLSPAN = 2 BGCOLOR = 'LightGreen'><FONT SIZE=2><B> Group : " & CStr(dsPrivilege.Tables(0).Rows(i).Item("group_name")) & "</B></FONT></TD>" & vbCrLf)
            '        strBuilder.Append(" </TR> " & vbCrLf)
            '        StrPvGrp_Name = CStr(dsPrivilege.Tables(0).Rows(i).Item("group_name"))
            '    End If
            '    strBuilder.Append(" <TR WIDTH='75%'> " & vbCrLf)
            '    For k As Integer = 0 To dsPrivilege.Tables(0).Columns.Count - 1
            '        If dsPrivilege.Tables(0).Columns(k).ColumnName.ToUpper = "GROUP_NAME" Then Continue For
            '        If dsPrivilege.Tables(0).Columns(k).ColumnName.ToUpper = "ASSIGN" Then
            '            Select Case CBool(dsPrivilege.Tables(0).Rows(i)(k))
            '                Case True
            '                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' WIDTH = '5%'><FONT SIZE=2> &nbsp;" & "YES" & "</FONT></TD>" & vbCrLf)
            '                Case False
            '                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' WIDTH = '5%'><FONT SIZE=2> &nbsp;" & "NO" & "</FONT></TD>" & vbCrLf)
            '            End Select
            '        Else
            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '70%'><FONT SIZE=2> &nbsp;&nbsp;&nbsp;&nbsp;" & CStr(dsPrivilege.Tables(0).Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
            '        End If
            '    Next
            '    strBuilder.Append(" </TR> " & vbCrLf)
            'Next
            'strBuilder.Append(" </TABLE> " & vbCrLf)
            'strBuilder.Append(" </BODY> " & vbCrLf)
            'strBuilder.Append(" </HTML> " & vbCrLf)

            'Dim svDialog As New SaveFileDialog
            'svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
            'If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            '    Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
            '    Dim strWriter As New IO.StreamWriter(fsFile)
            '    With strWriter
            '        .BaseStream.Seek(0, IO.SeekOrigin.End)
            '        .WriteLine(strBuilder)
            '        .Close()
            '    End With
            '    Diagnostics.Process.Start(svDialog.FileName)
            'End If
            Dim dt As DataTable = dsPrivilege.Tables(0).Copy
            Dim objUList As New ArutiReports.clsUserDetailsLogReport
            objUList.ExportPrivilegeList(dt, Me.Text, 3, "User Privilge", True, dgvUserList.SelectedRows(0).Cells(dgcolhUserName.Index).Value.ToString.Trim, dgvUserList.SelectedRows(0).Cells(dgcolhUserRole.Index).Value.ToString.Trim)
            'S.SANDEEP [21-SEP-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportOperationalPrivilege_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (11-Aug-2015) -- Start
    'Export User List
    Private Sub mnuExportUserList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportUserList.Click
        Try
            If dgvUserList.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, No user in the list to export."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim strBuilder As New System.Text.StringBuilder

            Dim StrCaption As String = ""
            If radShowActiveUser.Checked = True Then
                StrCaption = radShowActiveUser.Text
            ElseIf radShowInActiveUser.Checked = True Then
                StrCaption = radShowInActiveUser.Text
            ElseIf radShowAllUser.Checked = True Then
                StrCaption = radShowAllUser.Text
            End If

            Dim StrFilterUsed As String = String.Empty

            StrFilterUsed &= ", " & Language.getMessage(mstrModuleName, 10, "Export Datetime") & " : " & Now.Date.ToShortDateString & " " & Now.ToShortTimeString
            StrFilterUsed &= ", " & Language.getMessage(mstrModuleName, 7, "Export By") & " : " & User._Object._Username
            If CInt(cboUser.SelectedValue) > 0 Then
                StrFilterUsed &= "," & Language.getMessage(mstrModuleName, 8, "User Filtered") & " : " & cboUser.Text
            End If
            If CInt(cboRole.SelectedValue) > 0 Then
                StrFilterUsed &= ", " & Language.getMessage(mstrModuleName, 9, "Role Filtered") & " : " & cboRole.Text
            End If

            If StrFilterUsed.Trim.Length > 0 Then StrFilterUsed = Mid(StrFilterUsed, 2)

            If dgvUserList.DataSource IsNot Nothing Then
                strBuilder = objUserAddEdit.ExportUserList(CType(dgvUserList.DataSource, DataTable), StrCaption, StrFilterUsed)
            End If

            Dim svDialog As New SaveFileDialog
            svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
            If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                Dim strWriter As New IO.StreamWriter(fsFile)
                With strWriter
                    .BaseStream.Seek(0, IO.SeekOrigin.End)
                    .WriteLine(strBuilder)
                    .Close()
                End With
                Diagnostics.Process.Start(svDialog.FileName)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportUserList_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (11-Aug-2015) -- End

    Private Sub mnuImportEmp_User_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmp_User.Click
        Try
            Dim frm As New frmImportEmp_User
            frm.ShowDialog()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuImportEmp_User_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (23-AUG-2017) -- Start
    'Enhancement - Implemented Enable / Disable Active Directory User From Aruti  .
    Private Sub mnuEnableDisableADusers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEnableDisableADusers.Click
        Try
            Dim frm As New frmEnableDisableADUsers
            frm.ShowDialog()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuEnableDisableADusers_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (23-AUG-2017) -- End


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

    Private Sub mnuImportADAttributeMapping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportADAttributeMapping.Click
        Try
            Dim objADAttributeMapping As New frmADAttributeMapping
            objADAttributeMapping.ShowDialog()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuImportADAttributeMapping_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Aug-2018) -- End

    'S.SANDEEP |04-APR-2019| -- START
    Private Sub mnuMapUserRole_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMapUserRole.Click
        Dim frm As New frmMapUser_Role
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuMapUserRole_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP |04-APR-2019| -- END

#End Region

#Region " DataGrid Events "

    Private Sub dgvUserList_DataBindingComplete(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvUserList.DataBindingComplete
        Try
            For Each dgvRow As DataGridViewRow In dgvUserList.Rows
                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvRow.DefaultCellStyle.ForeColor = Color.White
                    dgvRow.DefaultCellStyle.BackColor = Color.Gray

                    dgvRow.DefaultCellStyle.SelectionForeColor = Color.White
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.Gray

                    dgvRow.Cells(objdgcolhCollapse.Index).Value = "-"
                ElseIf CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                    If CBool(dgvRow.Cells(objdgcolhActiveStatus.Index).Value) = False Then
                        dgvRow.DefaultCellStyle.ForeColor = Color.Maroon
                        dgvRow.DefaultCellStyle.BackColor = Color.AntiqueWhite
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvUserList_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvUserList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvUserList.SelectionChanged
        Dim objPOption As New clsPassowdOptions
        Try
            If dgvUserList.SelectedRows.Count > 0 Then
                If CBool(dgvUserList.SelectedRows(0).Cells(objdgcolhIsGrp.Index).Value) = False Then
                    If dgvUserList.SelectedRows(0).DefaultCellStyle.ForeColor <> Color.Maroon Then

                        btnEdit.Enabled = True : btnOperation.Enabled = True

                        If CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value) = 1 Then
                            If objPOption._IsEmployeeAsUser = True AndAlso User._Object.Privilege._AllowtoImportEmployee_User = True Then
                                mnuImportEmp_User.Enabled = True
                                btnOperation.Enabled = True
                                mnuReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
                                mnuAssignUserAccess.Enabled = User._Object.Privilege._AssignUserAccess
                                mnuEditUserAccess.Enabled = False : mnuEditReportPrivilege.Enabled = False

                                If objPOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objPOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                                    mnuImportADusers.Enabled = User._Object.Privilege._ImportADUsers
                                Else
                                    mnuImportADusers.Enabled = False
                                End If
                                mnuExportOperationalPrivilege.Enabled = User._Object.Privilege._ExportPrivilegs
                            Else
                                mnuImportEmp_User.Enabled = False : btnOperation.Enabled = False
                            End If
                        Else
                            btnOperation.Enabled = True
                            mnuEditUserAccess.Enabled = User._Object.Privilege._EditUserAccess
                            mnuExportOperationalPrivilege.Enabled = User._Object.Privilege._ExportPrivilegs
                            mnuAssignUserAccess.Enabled = User._Object.Privilege._AssignUserAccess
                            'S.SANDEEP [05-Mar-2018] -- START
                            'ISSUE/ENHANCEMENT : {#ARUTI-4}
                            'mnuReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
                            mnuEditReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
                            'S.SANDEEP [05-Mar-2018] -- END
                        End If

                        If User._Object._Userunkid = CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value) Or CInt(dgvUserList.SelectedRows(0).Cells(objdgcolhUserUnkid.Index).Value) = 1 Then
                            btnDelete.Enabled = False
                        Else
                            If objPOption._IsEmployeeAsUser = True Then
                                btnDelete.Enabled = False
                            Else
                                btnDelete.Enabled = User._Object.Privilege._AllowDeleteUser
                            End If
                        End If
                    Else
                        btnEdit.Enabled = False
                        btnOperation.Enabled = True
                        mnuEditUserAccess.Enabled = False
                        mnuExportOperationalPrivilege.Enabled = User._Object.Privilege._ExportPrivilegs
                        mnuExportUserList.Enabled = True
                    End If
                Else
                    btnEdit.Enabled = False
                    btnOperation.Enabled = True
                    mnuEditUserAccess.Enabled = False
                    mnuExportOperationalPrivilege.Enabled = User._Object.Privilege._ExportPrivilegs
                    mnuExportUserList.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvUserList_SelectionChanged", mstrModuleName)
        Finally
            objPOption = Nothing
        End Try
    End Sub

    Private Sub dgvUserList_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUserList.CellClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub
            If Me.dgvUserList.IsCurrentCellDirty Then
                Me.dgvUserList.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvUserList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then

                Select Case CInt(e.ColumnIndex)
                    Case objdgcolhCollapse.Index
                        If dgvUserList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is "-" Then
                            dgvUserList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "+"
                        Else
                            dgvUserList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "-"
                        End If

                        dgvUserList.BeginEdit(False)

                        For i = e.RowIndex + 1 To dgvUserList.RowCount - 1
                            If CInt(dgvUserList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvUserList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvUserList.Rows(i).Visible = False Then
                                    dgvUserList.Rows(i).Visible = True
                                Else
                                    dgvUserList.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next

                        dgvUserList.EndEdit()

                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvUserList_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " RadioButton Event(s) "

    Private Sub radShowAllUser_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radShowAllUser.CheckedChanged, radShowInActiveUser.CheckedChanged, radShowActiveUser.CheckedChanged
        Try
            dgvUserList.DataSource = Nothing
            If dgvUserList.DataSource IsNot Nothing Then
                objbtnReset.ShowResult(CType(dgvUserList.DataSource, DataTable).Select("IsGrp = False").Length.ToString)
            Else
                objbtnReset.ShowResult("0")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radShowAllUser_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
            Me.lblRole.Text = Language._Object.getCaption(Me.lblRole.Name, Me.lblRole.Text)
            Me.radShowInActiveUser.Text = Language._Object.getCaption(Me.radShowInActiveUser.Name, Me.radShowInActiveUser.Text)
            Me.radShowAllUser.Text = Language._Object.getCaption(Me.radShowAllUser.Name, Me.radShowAllUser.Text)
            Me.radShowActiveUser.Text = Language._Object.getCaption(Me.radShowActiveUser.Name, Me.radShowActiveUser.Text)
            Me.dgcolhUserName.HeaderText = Language._Object.getCaption(Me.dgcolhUserName.Name, Me.dgcolhUserName.HeaderText)
            Me.dgcolhFirstName.HeaderText = Language._Object.getCaption(Me.dgcolhFirstName.Name, Me.dgcolhFirstName.HeaderText)
            Me.dgcolhLastName.HeaderText = Language._Object.getCaption(Me.dgcolhLastName.Name, Me.dgcolhLastName.HeaderText)
            Me.dgcolhEmail.HeaderText = Language._Object.getCaption(Me.dgcolhEmail.Name, Me.dgcolhEmail.HeaderText)
            Me.dgcolhUserRole.HeaderText = Language._Object.getCaption(Me.dgcolhUserRole.Name, Me.dgcolhUserRole.HeaderText)
            Me.dgcolhIsManager.HeaderText = Language._Object.getCaption(Me.dgcolhIsManager.Name, Me.dgcolhIsManager.HeaderText)
            Me.mnuUserSpecificOperation.Text = Language._Object.getCaption(Me.mnuUserSpecificOperation.Name, Me.mnuUserSpecificOperation.Text)
            Me.mnuEditReportPrivilege.Text = Language._Object.getCaption(Me.mnuEditReportPrivilege.Name, Me.mnuEditReportPrivilege.Text)
            Me.mnuEditUserAccess.Text = Language._Object.getCaption(Me.mnuEditUserAccess.Name, Me.mnuEditUserAccess.Text)
            Me.mnuExportOperationalPrivilege.Text = Language._Object.getCaption(Me.mnuExportOperationalPrivilege.Name, Me.mnuExportOperationalPrivilege.Text)
            Me.mnuGlobalUserOperation.Text = Language._Object.getCaption(Me.mnuGlobalUserOperation.Name, Me.mnuGlobalUserOperation.Text)
            Me.mnuImportADusers.Text = Language._Object.getCaption(Me.mnuImportADusers.Name, Me.mnuImportADusers.Text)
            Me.mnuImportEmp_User.Text = Language._Object.getCaption(Me.mnuImportEmp_User.Name, Me.mnuImportEmp_User.Text)
            Me.mnuReportPrivilege.Text = Language._Object.getCaption(Me.mnuReportPrivilege.Name, Me.mnuReportPrivilege.Text)
            Me.mnuAssignUserAccess.Text = Language._Object.getCaption(Me.mnuAssignUserAccess.Name, Me.mnuAssignUserAccess.Text)
            Me.mnuExportUserList.Text = Language._Object.getCaption(Me.mnuExportUserList.Name, Me.mnuExportUserList.Text)
            Me.mnuEnableDisableADusers.Text = Language._Object.getCaption(Me.mnuEnableDisableADusers.Name, Me.mnuEnableDisableADusers.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select User from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this User?")
            Language.setMessage(mstrModuleName, 4, "Operational Privilege for User :")
            Language.setMessage(mstrModuleName, 5, "User Role :")
            Language.setMessage(mstrModuleName, 6, "Sorry, No Role is assigned to the selected user.")
            Language.setMessage(mstrModuleName, 7, "Export By")
            Language.setMessage(mstrModuleName, 8, "User Filtered")
            Language.setMessage(mstrModuleName, 9, "Role Filtered")
            Language.setMessage(mstrModuleName, 10, "Export Datetime")
            Language.setMessage(mstrModuleName, 11, "Sorry, No user in the list to export.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class


'---------------------------------------{CHANGED ON 10 AUG 2015 SANDEEP (Aruti SaaS)}---------------------------------------------------'
'
'
'
'
'
'
'Public Class frmUserCreationList

'#Region " Private Varaibles "
'    Private objUserAddEdit As clsUserAddEdit
'    Private ReadOnly mstrModuleName As String = "frmUserCreationList"
'#End Region

'#Region " Private Function "

'    Private Sub FillCombo()
'        Dim dsList As DataSet = Nothing
'        Dim objUserRole As New clsUserRole_Master

'        objUserAddEdit = New clsUserAddEdit
'        Try
'            Dim objOption As New clsPassowdOptions
'            If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
'                'S.SANDEEP [ 01 DEC 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'dsList = objUserAddEdit.getComboList("List", True, False)
'                Dim objPswd As New clsPassowdOptions
'                If objPswd._IsEmployeeAsUser Then
'                    dsList = objUserAddEdit.getComboList("List", True, False, True)
'                Else
'                    dsList = objUserAddEdit.getComboList("List", True, False)
'                End If
'                objPswd = Nothing
'                'S.SANDEEP [ 01 DEC 2012 ] -- END
'            ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
'                dsList = objUserAddEdit.getComboList("List", True, True)
'                'Pinkal (21-Jun-2012) -- Start
'                'Enhancement : TRA Changes
'            ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
'                dsList = objUserAddEdit.getComboList("List", True, True)
'                'Pinkal (21-Jun-2012) -- End
'            End If

'            'S.SANDEEP [ 23 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If User._Object._Userunkid <> 1 Then
'            'Dim DRow() As DataRow = dsList.Tables(0).Select("userunkid <> " & User._Object._Userunkid, "userunkid")
'            'If DRow.Length > 0 Then
'            'For i As Integer = 1 To DRow.Length - 1
'            'dsList.Tables(0).Rows.Remove(DRow(i))
'            'Next
'            'End If
'            'End If
'            Dim dTable As New DataTable
'            If User._Object.Privilege._AllowtoViewUserList = False Then
'                dTable = New DataView(dsList.Tables(0), "userunkid IN (0," & User._Object._Userunkid & ")", "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dTable = dsList.Tables(0)
'            End If
'            With cboUser
'                .ValueMember = "userunkid"
'                .DisplayMember = "name"
'                .DataSource = dTable
'            End With
'            'S.SANDEEP [ 23 NOV 2012 ] -- END


'            dsList = objUserRole.getComboList("List", True)
'            With cboRole
'                .ValueMember = "roleunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("List")
'            End With

'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
'        Finally
'            dsList.Dispose()
'            dsList = Nothing
'            objUserRole = Nothing
'        End Try
'    End Sub

'    'S.SANDEEP [ 23 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub SetVisibility()
'        Try
'            Dim objOption As New clsPassowdOptions
'            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

'            If lvUserList.SelectedItems.Count > 0 Then
'                If User._Object._Userunkid = CInt(lvUserList.SelectedItems(0).Tag) Or CInt(lvUserList.SelectedItems(0).Tag) = 1 Then
'                    btnDelete.Enabled = False
'                Else
'                    btnDelete.Enabled = User._Object.Privilege._AllowDeleteUser
'                End If
'            End If


'            If drOption.Length > 0 Then
'                If objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
'                    mnuImportADusers.Enabled = User._Object.Privilege._ImportADUsers
'                Else
'                    mnuImportADusers.Enabled = False
'                End If
'            End If

'            mnuEditUserAccess.Enabled = User._Object.Privilege._EditUserAccess
'            mnuExportOperationalPrivilege.Enabled = User._Object.Privilege._ExportPrivilegs
'            mnuAssignUserAccess.Enabled = User._Object.Privilege._AssignUserAccess
'            mnuReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
'            btnNew.Enabled = User._Object.Privilege._AllowAddUser
'            btnEdit.Enabled = User._Object.Privilege._AllowEditUser
'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Dim objPOption As New clsPassowdOptions
'            If objPOption._IsEmployeeAsUser = True AndAlso User._Object.Privilege._AllowtoImportEmployee_User = True Then
'                mnuImportEmp_User.Enabled = True
'            Else
'                mnuImportEmp_User.Enabled = False
'            End If
'            'S.SANDEEP [ 06 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If objPOption._IsEmployeeAsUser = True Then
'                btnDelete.Enabled = False : btnNew.Enabled = False
'            End If
'            'S.SANDEEP [ 06 DEC 2012 ] -- END
'            objPOption = Nothing
'            Call lvUserList_SelectedIndexChanged(Nothing, Nothing)
'            'S.SANDEEP [ 01 DEC 2012 ] -- END
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 23 NOV 2012 ] -- END

'    Private Sub fillList()
'        Dim dsRole As New DataSet
'        'Sandeep [ 07 FEB 2011 ] -- START
'        Dim dtTable As DataTable = Nothing
'        'Sandeep [ 07 FEB 2011 ] -- END 
'        Dim StrSearching As String = ""
'        Try


'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes

'            Dim objOption As New clsPassowdOptions
'            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

'            If drOption.Length > 0 Then


'                If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
'                    'S.SANDEEP [ 01 DEC 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'dsRole = objUserAddEdit.GetList("RoleList", True, False)
'                    Dim objPswd As New clsPassowdOptions
'                    If objPswd._IsEmployeeAsUser Then
'                        dsRole = objUserAddEdit.GetList("RoleList", True, False, True)
'                    Else
'                        dsRole = objUserAddEdit.GetList("RoleList", True, False)
'                    End If
'                    objPswd = Nothing
'                    'S.SANDEEP [ 01 DEC 2012 ] -- END
'                ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
'                    dsRole = objUserAddEdit.GetList("RoleList", True, True)
'                ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
'                    dsRole = objUserAddEdit.GetList("RoleList", True, True)
'                End If
'            End If

'            'S.SANDEEP [ 23 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If User._Object.Privilege._AllowtoViewUserList = False Then
'                StrSearching &= "AND userunkid = " & User._Object._Userunkid
'            End If
'            'S.SANDEEP [ 23 NOV 2012 ] -- END


'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes



'            'Pinkal (12-Oct-2011) -- End

'            'If User._Object._Userunkid = 1 Then
'            If CInt(cboUser.SelectedValue) > 0 Then
'                StrSearching &= "AND userunkid = " & CInt(cboUser.SelectedValue) & " "
'            End If
'            'End If

'            If CInt(cboRole.SelectedValue) > 0 Then
'                StrSearching &= "AND roleunkid = " & CInt(cboRole.SelectedValue) & " "
'            End If

'            If StrSearching.Length > 0 Then
'                StrSearching = StrSearching.Substring(3)
'            End If

'            'S.SANDEEP [ 23 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES

'            ''Sandeep [ 07 FEB 2011 ] -- START
'            'If User._Object._Userunkid <> 1 Then
'            '    If User._Object._Roleunkid <> 1 Then
'            '        If StrSearching.Trim.Length > 0 Then
'            '            dtTable = New DataView(dsRole.Tables("RoleList"), "userunkid = " & User._Object._Userunkid & " AND " & StrSearching, "", DataViewRowState.CurrentRows).ToTable
'            '        Else
'            '            dtTable = New DataView(dsRole.Tables("RoleList"), "userunkid = " & User._Object._Userunkid, "", DataViewRowState.CurrentRows).ToTable
'            '        End If
'            '    Else
'            '        dtTable = New DataView(dsRole.Tables("RoleList"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
'            '    End If
'            'ElseIf User._Object._Userunkid = 1 Then
'            '    dtTable = New DataView(dsRole.Tables("RoleList"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
'            'End If
'            ''Sandeep [ 07 FEB 2011 ] -- END 

'            If StrSearching.Trim.Length > 0 Then
'                dtTable = New DataView(dsRole.Tables("RoleList"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtTable = dsRole.Tables("RoleList")
'            End If
'            'S.SANDEEP [ 23 NOV 2012 ] -- END

'            Dim lvItem As ListViewItem

'            lvUserList.Items.Clear()
'            'Sandeep [ 07 FEB 2011 ] -- START
'            'For Each drRow As DataRow In dsRole.Tables(0).Rows
'            For Each drRow As DataRow In dtTable.Rows
'                'Sandeep [ 07 FEB 2011 ] -- END 
'                lvItem = New ListViewItem
'                lvItem.Text = drRow("username").ToString
'                lvItem.SubItems.Add(drRow("firstname").ToString)
'                lvItem.SubItems.Add(drRow("lastname").ToString)
'                lvItem.SubItems.Add(drRow("email").ToString)
'                lvItem.SubItems.Add(drRow("rolename").ToString)

'                'Pinkal (12-Oct-2011) -- Start
'                'Enhancement : TRA Changes
'                lvItem.SubItems.Add(drRow("isaduser").ToString)
'                'Pinkal (12-Oct-2011) -- End

'                'S.SANDEEP [ 21 MAY 2012 ] -- START
'                'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'                lvItem.SubItems.Add(drRow.Item("roleunkid").ToString)
'                'S.SANDEEP [ 21 MAY 2012 ] -- END

'                'S.SANDEEP [ 06 DEC 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                lvItem.SubItems.Add(drRow.Item("employeeunkid").ToString)
'                'S.SANDEEP [ 06 DEC 2012 ] -- END

'                lvItem.Tag = drRow("userunkid")
'                lvUserList.Items.Add(lvItem)
'            Next


'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes

'            'If lvUserList.Items.Count > 16 Then
'            '    colhUserName.Width = colhUserName.Width - 20
'            'Else
'            '    colhUserName.Width = colhUserName.Width
'            'End If

'            If lvUserList.Items.Count > 16 Then
'                colhUserRole.Width = 120 - 18
'            Else
'                colhUserRole.Width = 120
'            End If

'            'Pinkal (21-Jun-2012) -- End

'            'Call setAlternateColor(lvAirline)

'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
'        Finally
'            dsRole.Dispose()
'        End Try
'    End Sub

'    'Private Sub AssignContextMenuItemText()
'    '    Try
'    '        objtsmiNew.Text = btnNew.Text
'    '        objtsmiEdit.Text = btnEdit.Text
'    '        objtsmiDelete.Text = btnDelete.Text
'    '    Catch ex As Exception
'    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
'    '    End Try
'    'End Sub
'#End Region

'#Region " Form's Events "

'    Private Sub frmUserCreationList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
'        Try
'            If e.KeyCode = Keys.Delete And lvUserList.Focused = True Then
'                Call btnDelete.PerformClick()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmUserCreationList_KeyUp", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmUserCreationList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        Try
'            If Asc(e.KeyChar) = 27 Then
'                Me.Close()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmUserCreationList_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmUserCreationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objUserAddEdit = New clsUserAddEdit
'        Try
'            Call Set_Logo(Me, gApplicationType)

'            Call Language.setLanguage(Me.Name)

'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            Call OtherSettings()
'            'Anjan (02 Sep 2011)-End 

'            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

'            'Call OtherSettings()
'            Call FillCombo()
'            Call fillList()

'            'S.SANDEEP [ 23 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES

'            ''Sandeep [ 07 FEB 2011 ] -- START
'            'If User._Object._Userunkid <> 1 Then

'            '    'S.SANDEEP [ 24 JUNE 2011 ] -- START
'            '    'ISSUE : USER ACCESS LEVEL PRIVILEGE
'            '    'btnReportPrivilege.Visible = False
'            '    btnOperation.Visible = False
'            '    'S.SANDEEP [ 24 JUNE 2011 ] -- END


'            '    'S.SANDEEP [ 30 May 2011 ] -- START
'            '    'ISSUE : FINCA REQ.
'            '    btnNew.Enabled = User._Object.Privilege._AllowAddUser
'            '    btnEdit.Enabled = User._Object.Privilege._AllowEditUser
'            '    'S.SANDEEP [ 30 May 2011 ] -- END 
'            'ElseIf User._Object._Userunkid = 1 Then
'            '    'S.SANDEEP [ 24 JUNE 2011 ] -- START
'            '    'ISSUE : USER ACCESS LEVEL PRIVILEGE
'            '    'btnReportPrivilege.Visible = True
'            '    btnOperation.Visible = True
'            '    'S.SANDEEP [ 24 JUNE 2011 ] -- END
'            'End If
'            ''Sandeep [ 07 FEB 2011 ] -- END 

'            'S.SANDEEP [ 23 NOV 2012 ] -- END



'            If lvUserList.Items.Count > 0 Then lvUserList.Items(0).Selected = True
'            lvUserList.Select()

'            'S.SANDEEP [ 23 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Call SetVisibility()
'            'S.SANDEEP [ 23 NOV 2012 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmUserCreationList_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmUserCreationList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objUserAddEdit = Nothing
'    End Sub

'    'Anjan (02 Sep 2011)-Start
'    'Issue : Including Language Settings.
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsUserAddEdit.SetMessages()
'            objfrm._Other_ModuleNames = "clsUserAddEdit"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-End 


'#End Region

'#Region " Buttons "

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        If lvUserList.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvUserList.Select()
'            Exit Sub
'        End If
'        'If objUserAddEdit.isUsed(CInt(lvUserList.SelectedItems(0).Tag)) Then
'        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this User. Reason: This User Role is in use."), enMsgBoxStyle.Information) '?2
'        '    lvUserList.Select()
'        '    Exit Sub
'        'End If
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvUserList.SelectedItems(0).Index

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this User?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                objUserAddEdit.Delete(CInt(lvUserList.SelectedItems(0).Tag))
'                lvUserList.SelectedItems(0).Remove()

'                If lvUserList.Items.Count <= 0 Then
'                    Exit Try
'                End If

'                If lvUserList.Items.Count = intSelectedIndex Then
'                    intSelectedIndex = lvUserList.Items.Count - 1
'                    lvUserList.Items(intSelectedIndex).Selected = True
'                    lvUserList.EnsureVisible(intSelectedIndex)
'                ElseIf lvUserList.Items.Count <> 0 Then
'                    lvUserList.Items(intSelectedIndex).Selected = True
'                    lvUserList.EnsureVisible(intSelectedIndex)
'                End If
'            End If
'            lvUserList.Select()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        If lvUserList.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvUserList.Select()
'            Exit Sub
'        End If
'        Dim frm As New frmUser_AddEdit
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvUserList.SelectedItems(0).Index

'            'If User._Object._RightToLeft = True Then
'            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'            '    frm.RightToLeftLayout = True
'            '    Call Language.ctlRightToLeftlayOut(frm)
'            'End If

'            If frm.displayDialog(CInt(lvUserList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
'                Call fillList()
'            End If
'            frm = Nothing

'            lvUserList.Items(intSelectedIndex).Selected = True
'            lvUserList.EnsureVisible(intSelectedIndex)
'            lvUserList.Select()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Dim frm As New frmUser_AddEdit
'        Try
'            'If User._Object._RightToLeft = True Then
'            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'            '    frm.RightToLeftLayout = True
'            '    Call Language.ctlRightToLeftlayOut(frm)
'            'End If
'            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
'                Call fillList()
'            End If
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'#End Region

'    'Pinkal (27-oct-2010) ------------------- START

'#Region "ListView event"

'    'S.SANDEEP [ 23 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES

'    'S.SANDEEP [ 23 NOV 2012 ] -- END

'    'Private Sub lvUserList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvUserList.SelectedIndexChanged
'    '    Try
'    '        If lvUserList.SelectedItems.Count > 0 Then

'    '            Dim objOption As New clsPassowdOptions
'    '            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

'    '            If CInt(lvUserList.SelectedItems(0).Tag) = 1 Then
'    '                btnDelete.Enabled = False



'    '                'Pinkal (12-Oct-2011) -- Start
'    '                'Enhancement : TRA Changes


'    '                'S.SANDEEP [ 24 JUNE 2011 ] -- START
'    '                'ISSUE : USER ACCESS LEVEL PRIVILEGE
'    '                'btnReportPrivilege.Enabled = False
'    '                ''btnOperation.Enabled = False
'    '                'S.SANDEEP [ 24 JUNE 2011 ] -- END


'    '                mnuAssignUserAccess.Enabled = False
'    '                mnuEditUserAccess.Enabled = False
'    '                mnuReportPrivilege.Enabled = False

'    '                If drOption.Length > 0 Then

'    '                    'Pinkal (21-Jun-2012) -- Start
'    '                    'Enhancement : TRA Changes

'    '                    If objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
'    '                        mnuImportADusers.Visible = True
'    '                        mnuImportADusers.Enabled = True
'    '                    Else
'    '                        mnuImportADusers.Visible = False
'    '                    End If

'    '                    'Pinkal (21-Jun-2012) -- End

'    '                End If

'    '                'Pinkal (12-Oct-2011) -- End

'    '            Else
'    '                'Sandeep [ 07 FEB 2011 ] -- START
'    '                'btnDelete.Enabled = True

'    '                'S.SANDEEP [ 30 May 2011 ] -- START
'    '                'ISSUE : FINCA REQ.
'    '                'If User._Object._Userunkid <> 1 Then
'    '                If User._Object._Userunkid = CInt(lvUserList.SelectedItems(0).Tag) And User._Object._Userunkid <> 1 Then
'    '                    'S.SANDEEP [ 30 May 2011 ] -- END 
'    '                    btnDelete.Enabled = False
'    '                Else
'    'btnDelete.Enabled = True
'    '                End If
'    '                'Sandeep [ 07 FEB 2011 ] -- END 



'    '                'Pinkal (12-Oct-2011) -- Start
'    '                'Enhancement : TRA Changes


'    '                'S.SANDEEP [ 30 May 2011 ] -- START
'    '                'ISSUE : FINCA REQ.
'    '                'btnReportPrivilege.Enabled = True

'    '                'S.SANDEEP [ 24 JUNE 2011 ] -- START
'    '                'ISSUE : USER ACCESS LEVEL PRIVILEGE
'    '                'btnReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
'    '                'btnOperation.Enabled = True
'    '                'mnuReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
'    '                'S.SANDEEP [ 24 JUNE 2011 ] -- END

'    '                'S.SANDEEP [ 30 May 2011 ] -- END 

'    '                mnuAssignUserAccess.Enabled = True
'    '                mnuEditUserAccess.Enabled = True
'    '                mnuReportPrivilege.Enabled = True
'    '                If drOption.Length > 0 Then
'    '                    If objOption._UserLogingModeId <> enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId <> enAuthenticationMode.AD_SSO_AUTHENTICATION Then
'    '                        'S.SANDEEP [ 23 NOV 2012 ] -- START
'    '                        'ENHANCEMENT : TRA CHANGES
'    '                        mnuImportADusers.Enabled = False
'    '                        'S.SANDEEP [ 23 NOV 2012 ] -- END
'    '                    End If
'    '                End If

'    '                mnuReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
'    '                'Pinkal (12-Oct-2011) -- End


'    '            End If


'    '            'Pinkal (12-Oct-2011) -- Start
'    '            'Enhancement : TRA Changes
'    '            If CBool(lvUserList.SelectedItems(0).SubItems(objColhIsADUser.Index).Text) = True Then
'    '                btnDelete.Enabled = False
'    '            End If
'    '            'Pinkal (12-Oct-2011) -- End

'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "lvUserList_SelectedIndexChanged", mstrModuleName)
'    '    End Try
'    'End Sub

'#End Region

'    'Pinkal (27-oct-2010) ------------------- END


'    'S.SANDEEP [ 24 JUNE 2011 ] -- START
'    'ISSUE : USER ACCESS LEVEL PRIVILEGE
'    Private Sub mnuReportPrivilege_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportPrivilege.Click
'        Try
'            If lvUserList.SelectedItems.Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one user to assign Report Privilege."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If
'            Dim frm As New frmAssignReportPrivilege
'            frm._User_Name = lvUserList.SelectedItems(0).Text
'            frm._UserId = CInt(lvUserList.SelectedItems(0).Tag)
'            frm.ShowDialog()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuReportPrivilege_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub mnuEditUserAccess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditUserAccess.Click
'        Dim frm As New frmUserAccess
'        Try
'            frm.displayDialog(CInt(lvUserList.SelectedItems(0).Tag), enAction.EDIT_ONE)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuEditUserAccess_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub mnuAssignUserAccess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssignUserAccess.Click
'        Dim frm As New frmUserAccess
'        Try
'            frm.displayDialog(-1, enAction.ADD_ONE)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuAssignUserAccess_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub


'    'Pinkal (12-Oct-2011) -- Start
'    'Enhancement : TRA Changes

'    Private Sub mnuImportADusers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportADusers.Click
'        Try
'            Dim objImport As New frmImportADUsers
'            objImport.ShowDialog()
'            fillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuImportADusers_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (12-Oct-2011) -- End



'    'S.SANDEEP [ 24 JUNE 2011 ] -- END 



'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim objFrm As New frmCommonSearch
'        Try
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            If User._Object._Isrighttoleft = True Then
'                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objFrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objFrm)
'            End If
'            'Anjan (02 Sep 2011)-End 


'            With objFrm
'                .ValueMember = cboUser.ValueMember
'                'S.SANDEEP [ 06 DEC 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                '.DisplayMember = cboUser.DisplayMember
'                .CodeMember = cboUser.DisplayMember
'                .DisplayMember = "Display"
'                'S.SANDEEP [ 06 DEC 2012 ] -- END
'                .DataSource = CType(cboUser.DataSource, DataTable)
'            End With
'            If objFrm.DisplayDialog Then
'                cboUser.SelectedValue = objFrm.SelectedValue
'            End If
'            cboUser.Focus()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            Call fillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            cboRole.SelectedValue = 0
'            cboUser.SelectedValue = 0
'            Call fillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        End Try

'    End Sub


'    'S.SANDEEP [ 21 MAY 2012 ] -- START
'    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'    Private Sub mnuExportOperationalPrivilege_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportOperationalPrivilege.Click
'        Try
'            If lvUserList.SelectedItems.Count < 1 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'                lvUserList.Select()
'                Exit Sub
'            End If

'            Dim dsPrivilege As New DataSet
'            dsPrivilege = objUserAddEdit.getPrivilegeList(CInt(lvUserList.SelectedItems(0).SubItems(objcolhRoleId.Index).Text), CInt(lvUserList.SelectedItems(0).Tag))

'            dsPrivilege.Tables(0).Columns.Remove("privilegeunkid")
'            dsPrivilege.Tables(0).Columns.Remove("privilegegroupunkid")

'            Dim strBuilder As New System.Text.StringBuilder
'            Dim StrPvGrp_Name As String = String.Empty

'            strBuilder.Append(" <HTML> " & vbCrLf)
'            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH='75%'> " & vbCrLf)
'            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='75%'> " & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '70%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 4, "Operational Privilege for User : ") & lvUserList.SelectedItems(0).Text & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '5%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 5, "User Role : ") & lvUserList.SelectedItems(0).SubItems(colhUserRole.Index).Text & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)
'            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP WIDTH='75%'> " & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 ALIGN='MIDDLE' WIDTH = '70%'><FONT SIZE=2><B>" & "PRIVILEGE(S)" & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 ALIGN='MIDDLE' WIDTH = '5%'><FONT SIZE=2><B>" & "ASSIGNED" & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            For i As Integer = 0 To dsPrivilege.Tables(0).Rows.Count - 1
'                If StrPvGrp_Name <> CStr(dsPrivilege.Tables(0).Rows(i).Item("group_name")) Then
'                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='75%'> " & vbCrLf)
'                    strBuilder.Append(" <TD BORDER=1 ALIGN='LEFT' COLSPAN = 2 BGCOLOR = 'LightGreen'><FONT SIZE=2><B> Group : " & CStr(dsPrivilege.Tables(0).Rows(i).Item("group_name")) & "</B></FONT></TD>" & vbCrLf)
'                    strBuilder.Append(" </TR> " & vbCrLf)
'                    StrPvGrp_Name = CStr(dsPrivilege.Tables(0).Rows(i).Item("group_name"))
'                End If
'                strBuilder.Append(" <TR WIDTH='75%'> " & vbCrLf)
'                For k As Integer = 0 To dsPrivilege.Tables(0).Columns.Count - 1
'                    If dsPrivilege.Tables(0).Columns(k).ColumnName.ToUpper = "GROUP_NAME" Then Continue For
'                    If dsPrivilege.Tables(0).Columns(k).ColumnName.ToUpper = "ASSIGN" Then
'                        Select Case CBool(dsPrivilege.Tables(0).Rows(i)(k))
'                            Case True
'                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' WIDTH = '5%'><FONT SIZE=2> &nbsp;" & "YES" & "</FONT></TD>" & vbCrLf)
'                            Case False
'                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' WIDTH = '5%'><FONT SIZE=2> &nbsp;" & "NO" & "</FONT></TD>" & vbCrLf)
'                        End Select
'                    Else
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '70%'><FONT SIZE=2> &nbsp;&nbsp;&nbsp;&nbsp;" & CStr(dsPrivilege.Tables(0).Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
'                    End If
'                Next
'                strBuilder.Append(" </TR> " & vbCrLf)
'            Next
'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" </BODY> " & vbCrLf)
'            strBuilder.Append(" </HTML> " & vbCrLf)

'            Dim svDialog As New SaveFileDialog
'            svDialog.Filter = "Excel files (*.xlsx)|*.xlsx""
'            If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
'                Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
'                Dim strWriter As New IO.StreamWriter(fsFile)
'                With strWriter
'                    .BaseStream.Seek(0, IO.SeekOrigin.End)
'                    .WriteLine(strBuilder)
'                    .Close()
'                End With
'                Diagnostics.Process.Start(svDialog.FileName)
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuExportOperationalPrivilege_Click", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 21 MAY 2012 ] -- END

'    Private Sub lvUserList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvUserList.SelectedIndexChanged
'        Try
'            If lvUserList.SelectedItems.Count > 0 Then
'                'S.SANDEEP [ 26 NOV 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If CInt(lvUserList.SelectedItems(0).Tag) = 1 Then
'                    'S.SANDEEP [ 06 DEC 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'btnOperation.Enabled = False
'                    Dim objPOption As New clsPassowdOptions
'                    If objPOption._IsEmployeeAsUser = True AndAlso User._Object.Privilege._AllowtoImportEmployee_User = True Then
'                        mnuImportEmp_User.Enabled = True
'                        btnOperation.Enabled = True
'                        mnuReportPrivilege.Enabled = False
'                        mnuAssignUserAccess.Enabled = False
'                        mnuEditUserAccess.Enabled = False
'                        mnuImportADusers.Enabled = User._Object.Privilege._ImportADUsers
'                        mnuExportOperationalPrivilege.Enabled = User._Object.Privilege._ExportPrivilegs
'                    Else
'                        mnuImportEmp_User.Enabled = False : btnOperation.Enabled = False
'                    End If
'                    'S.SANDEEP [ 06 DEC 2012 ] -- END
'                Else
'                    btnOperation.Enabled = True
'                    'S.SANDEEP [ 10 DEC 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    mnuEditUserAccess.Enabled = User._Object.Privilege._EditUserAccess
'                    mnuExportOperationalPrivilege.Enabled = User._Object.Privilege._ExportPrivilegs
'                    mnuAssignUserAccess.Enabled = User._Object.Privilege._AssignUserAccess
'                    mnuReportPrivilege.Enabled = User._Object.Privilege._AssignReportPrivilege
'                    'S.SANDEEP [ 10 DEC 2012 ] -- END
'                End If
'                'S.SANDEEP [ 26 NOV 2012 ] -- END
'                If User._Object._Userunkid = CInt(lvUserList.SelectedItems(0).Tag) Or CInt(lvUserList.SelectedItems(0).Tag) = 1 Then
'                    btnDelete.Enabled = False
'                Else
'                    'S.SANDEEP [ 06 DEC 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    Dim objPOption As New clsPassowdOptions
'                    If objPOption._IsEmployeeAsUser = True Then
'                        btnDelete.Enabled = False
'                    Else
'                        btnDelete.Enabled = User._Object.Privilege._AllowDeleteUser
'                    End If
'                    objPOption = Nothing
'                    'S.SANDEEP [ 06 DEC 2012 ] -- END
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvUserList_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 01 DEC 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub mnuImportEmp_User_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmp_User.Click
'        Try
'            Dim frm As New frmImportEmp_User
'            frm.ShowDialog()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "mnuImportEmp_User_Click", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 01 DEC 2012 ] -- END
'End Class
