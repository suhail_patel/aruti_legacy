﻿#Region " Imports "

Imports System.Windows.Forms
Imports Aruti.Data
'Sandeep [ 10 FEB 2011 ] -- Start
Imports eZeeCommonLib
'Sandeep [ 10 FEB 2011 ] -- End 

#End Region

Public Class frmMDI

#Region " Private Variables "

    Public ReadOnly mstrModuleName As String = "frmMDI"
    Private frm As Form

#End Region

#Region " Form's Events "

    Private Sub frmMDI_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to exit?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
        '    e.Cancel = False
        'Else
        '    e.Cancel = True
        'End If

        If blnIsIdealTimeSet = False Then
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to exit?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
                e.Cancel = False
                'S.SANDEEP [ 21 MAY 2012 ] -- START
                'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                SetUserTracingInfo(True, enUserMode.Loging, enLogin_Mode.DESKTOP, , True)
                'S.SANDEEP [ 21 MAY 2012 ] -- END
            Else
                e.Cancel = True
            End If
        End If

    End Sub

    Private Sub frmMDI_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objAppSett As New clsApplicationSettings
        Dim objMMaster As New clsMasterData
        Try

            'S.SANDEEP [ 10 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objbtnWorkingDate.Text = Format(ConfigParameter._Object._CurrentDateAndTime, "dddd, MMM dd, yyyy")
            objbtnUserName.Text = User._Object._Username
            'S.SANDEEP [ 10 DEC 2012 ] -- END

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If IsNumeric(General_Settings._Object._WorkingMode) = False Then
            '    If General_Settings._Object._WorkingMode.ToUpper = enWorkingMode.Client.ToString.ToUpper Then
            '        mnuOption.Enabled = False
            '    End If
            'Else
            '    If General_Settings._Object._WorkingMode = enWorkingMode.Client Then
            '        mnuOption.Enabled = False
            '    End If
            'End If
            'S.SANDEEP [ 29 OCT 2012 ] -- END

            'Pinkal (21-Jun-2011) -- Start
            If User._Object.PWDOptions._IsApplication_Idle Then
                CheckForIllegalCrossThreadCalls = False
                RemoveHandler clsApplicationIdleTimer.ApplicationIdle, AddressOf Application_Idle
                AddHandler clsApplicationIdleTimer.ApplicationIdle, AddressOf Application_Idle
            End If

            'Pinkal (21-Jun-2011) -- End

            'Sandeep [ 10 FEB 2011 ] -- Start
            'tmrReminder.Enabled = True
            'tmrReminder.Start()
            'Sandeep [ 10 FEB 2011 ] -- End 
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Anjan (20 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            'Call SetVisibility()
            'Anjan (20 Jan 2012)-End 
            'S.SANDEEP [ 29 OCT 2012 ] -- END



            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 30 May 2011 ] -- START
            'If objMMaster.IsCompanyCreated = False Then
            objlblCaption.Text = ""
            If objMMaster.IsGroupCreated = False Then
                Dim objFrmWizard As New frmWizard
                frmSplash.Hide()
                If objFrmWizard.displayDialog = False Then
                    mnuConfiguration.Enabled = False
                    mnuOption.Enabled = False
                    Exit Sub
                Else
                    objAppSett._IsWizardRun = True

                End If
            End If
            'End If
            'S.SANDEEP [ 30 May 2011 ] -- END 

            Call mnuConfiguration_Click(sender, e)

            'Anjan (11 Jul 2011)-Start
            'Issue : Removing formula and setting total manually.

            If ConfigParameter._Object._IsArutiDemo Then
                'Anjan [17 June 2015] -- Start
                'ENHANCEMENT : Stopping Restore/Backup processes from client machine.
                If CBool(AppSettings._Object._IsClient) = False Then
                    'Anjan [17 June 2015] -- End
                    btnRestore.Enabled = True
                End If

                'btnBackUp.Enabled = False
            End If
            'Anjan (11 Jul 2011)-End 


            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If User._Object._Userunkid <> 1 Then
            '    btnUnlockUser.Enabled = False
            'End If
            'S.SANDEEP [ 29 OCT 2012 ] -- END



            Dim intDaysDiff As Integer = 0
            If User._Object._Exp_Days.ToString.Length = 8 Then
                Dim dtDate As Date = eZeeDate.convertDate(User._Object._Exp_Days.ToString).ToShortDateString
                intDaysDiff = DateDiff(DateInterval.Day, dtDate, ConfigParameter._Object._CurrentDateAndTime.AddDays(-1))
                'S.SANDEEP [ 30 May 2011 ] -- START
                'ISSUE : FINCA REQ.
                'If intDaysDiff.ToString <= "-2" Then
                '    Dim strMessage As String = Language.getMessage(mstrModuleName, 1, "There are only (") & _
                '    Math.Abs(intDaysDiff) & Language.getMessage(mstrModuleName, 2, ") Days Left for expiry of your password.") & vbCrLf & _
                '    Language.getMessage(mstrModuleName, 3, "Please change it or increase the notification date, Or contact Administrator.")
                '    eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                'End If
                If intDaysDiff.ToString = "-2" Or intDaysDiff.ToString = "-1" Then
                    Dim strMessage As String = Language.getMessage(mstrModuleName, 1, "There are only [") & _
                    Math.Abs(intDaysDiff) & Language.getMessage(mstrModuleName, 2, "] Days left before your password expires") & vbCrLf & _
                    Language.getMessage(mstrModuleName, 3, "Please change it or increase the notification date, Or contact Administrator.")
                    eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                End If
                'S.SANDEEP [ 30 May 2011 ] -- END 
            End If

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 29 OCT 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMDI_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMDI_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Try
            If Me.WindowState = FormWindowState.Minimized Then
                Me.Text = "Aruti Configuration"
            Else
                Me.Text = "Aruti configuration" & " " & _
                                              Language.getMessage(mstrModuleName, 4, "Version") & ": " & System.String.Format("{0}.{1:0}.{2:0}.{3:000}", _
                                              My.Application.Info.Version.Major, _
                                              My.Application.Info.Version.Minor, _
                                              My.Application.Info.Version.Build, _
                                              My.Application.Info.Version.Revision)
            End If

            If pnlMenuItems.Controls.Count > 0 Then
                Call SetDyanamicSize(pnlMenuItems.Controls(0))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMDI_Resize", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Menu Events "

    Private Sub mnuConfiguration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConfiguration.Click
        Try
            Call Set_Report_Setting()
            Call SetControlVisibility(fpnlConfiguration)
            objlblCaption.Text = mnuConfiguration.Text
            'S.SANDEEP [ 08 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call Blank_ModuleName()
            'StrModuleName1 = mnuConfiguration.Name
            'S.SANDEEP [ 08 NOV 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuConfiguration_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuOption_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOption.Click
        Try
            Call Set_Report_Setting()
            Dim frm As New frmConfigOptions
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuOption_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuAuditTrail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAuditTrail.Click
        Try
            Call Set_Report_Setting()

            'S.SHARMA [ 22 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim frm As New frmATLogView_New
            'frm.ShowDialog()
            Call SetControlVisibility(fpnlAuditTrails)
            objlblCaption.Text = mnuAuditTrail.Text
            'Call Blank_ModuleName()
            'StrModuleName1 = mnuAuditTrail.Name
            'S.SHARMA [ 22 JAN 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAuditTrail_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExit_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

#Region " Configuration "


    'S.SANDEEP [ 30 May 2011 ] -- START
    'ISSUE : FINCA REQ.
    'Private Sub btnCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompany.Click, btnUserCreation.Click, btnUserRole.Click, _
    '                                                                                                 btnAbilityLevel.Click, btnBackUp.Click, btnRestore.Click, _
    '                                                                                                 btnState.Click, btnCity.Click, btnZipcode.Click, _
    '                                                                                                 btnVoidReason.Click, btnReminderType.Click


    'S.SANDEEP [ 09 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub btnCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompany.Click, btnUserCreation.Click, btnUserRole.Click, _
    '                                                                                                 btnAbilityLevel.Click, btnBackUp.Click, btnRestore.Click, _
    '                                                                                                 btnState.Click, btnCity.Click, btnZipcode.Click, _
    '                                                                                                 btnVoidReason.Click, btnReminderType.Click, btnPasswordOption.Click, _
    '                                                                                                 btnUnlockUser.Click, btnUserLog.Click, btnGroupMaster.Click, btnDeviceList.Click
    Private Sub btnCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompany.Click, btnUserCreation.Click, btnUserRole.Click, _
                                                                                                     btnAbilityLevel.Click, btnBackUp.Click, btnRestore.Click, _
                                                                                                     btnState.Click, btnCity.Click, btnZipcode.Click, _
                                                                                                     btnVoidReason.Click, btnReminderType.Click, btnPasswordOption.Click, _
                                                                                                     btnUnlockUser.Click, btnUserLog.Click, btnGroupMaster.Click, btnDeviceList.Click, _
                                                                                                     btnEnableAutoUpdate.Click, btnForceLogout.Click, btnCloneData.Click, btnCountry.Click, btnEmailSetup.Click 'S.SANDEEP [27-NOV-2017] -- START {btnCloneData} -- END
        'Sohail (06 Oct 2021) - [btnEmailSetup]
        'Sohail (10 Jan 2020) - [btnCountry]
        'S.SANDEEP [ 09 APRIL 2012 ] -- END


        'S.SANDEEP [ 30 May 2011 ] -- END 

        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlConfiguration)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNCOMPANY"
                    frm = New frmCompanyProfileList
                Case "BTNUSERCREATION"
                    frm = New frmUserCreationList
                Case "BTNUSERROLE"
                    frm = New frmUserRoleList
                Case "BTNABILITYLEVEL"
                    frm = New frmAbilityLevel_Privilege
                Case "BTNBACKUP"
                    tmrReminder.Enabled = False
                    tmrReminder.Stop()
                    'frm = New frmBackUp
                    'frm._IsConfiguration = True
                    frm = New frmFullBackUp
                Case "BTNRESTORE"
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    tmrReminder.Enabled = False
                    tmrReminder.Stop()
                    'Dim rfrm As New frmRestore
                    'If rfrm.displayDialog(True) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    frm = New frmRestore
                    'Sandeep [ 10 FEB 2011 ] -- End 
                    'Sohail (10 Jan 2020) -- Start
                    'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
                Case btnCountry.Name.ToUpper
                    frm = New frmCountry
                    'Sohail (10 Jan 2020) -- End
                Case "BTNSTATE"
                    frm = New frmstateList
                Case "BTNCITY"
                    frm = New frmCityList
                Case "BTNZIPCODE"
                    frm = New frmZipCodeList
                    'Sohail (06 Oct 2021) -- Start
                    'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
                Case btnEmailSetup.Name.ToUpper
                    frm = New frmEmail_setupList
                    'Sohail (06 Oct 2021) -- End
                Case "BTNVOIDREASON"
                    frm = New frmVoidReason_List
                Case "BTNREMINDERTYPE"
                    frm = New frmReminderTypeList
                    'S.SANDEEP [ 30 May 2011 ] -- START
                    'ISSUE : FINCA REQ.
                Case "BTNPASSWORDOPTION"
                    frm = New frmPasswordOptions
                Case "BTNUNLOCKUSER"
                    frm = New frmUnlock_User
                    'S.SANDEEP [ 30 May 2011 ] -- END 

                    'Pinkal (23-Jun-2011) -- Start
                Case "BTNUSERLOG"
                    frm = New frmUserLogList
                    'Pinkal (23-Jun-2011) -- End

                    'S.SANDEEP [ 30 May 2011 ] -- START
                Case "BTNGROUPMASTER"
                    frm = New frmGroup_Master
                    'S.SANDEEP [ 30 May 2011 ] -- END 

                    'Anjan (21 Nov 2011)-Start
                    'ENHANCEMENT : NEw Device Settings included.
                Case "BTNDEVICELIST"
                    frm = New frmDeviceSettingList
                    'Anjan (21 Nov 2011)-End 

                    'S.SANDEEP [ 09 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNENABLEAUTOUPDATE"
                    frm = frmAutoUpdate_Settings
                    'S.SANDEEP [ 09 APRIL 2012 ] -- END

                    'Sohail (04 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                Case "BTNFORCELOGOUT"
                    frm = New frmForceLogout
                    'Sohail (04 Jul 2012) -- End

                    'S.SANDEEP [27-NOV-2017] -- START
                Case "BTNCLONEDATA"
                    frm = New frmTableCloning
                    'S.SANDEEP [27-NOV-2017] -- END

            End Select

            If frm IsNot Nothing Then
                'S.SANDEEP [ 30 May 2011 ] -- START
                Select Case frm.Name.ToString.ToUpper
                    Case "FRMGROUP_MASTER"
                        If frm.displayDialog(enAction.EDIT_ONE) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    Case Else
                        If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                End Select
                'S.SANDEEP [ 30 May 2011 ] -- END 
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCompany_Click", mstrModuleName)
        Finally
            tmrReminder.Enabled = True
            tmrReminder.Start()
        End Try
    End Sub

#End Region


    'S.SHARMA [ 22 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Audit Trails "
    Private Sub btnApplicationEventLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplicationEventLog.Click, _
                                                                                                                 btnUserAttempts.Click, _
                                                                                                                 btnUserAuthenticationLog.Click, _
                                                                                                                 btnLogView.Click, btnUserOptionLog.Click, _
                                                                                                                 btnSystemErrorLog.Click
        'Sohail (12 Apr 2019) - [btnSystemErrorLog]

        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlAuditTrails)
            Dim frm As Form = Nothing
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNAPPLICATIONEVENTLOG"
                    frm = New frmATLogView_New
                Case "BTNUSERATTEMPTS"
                    frm = New frmUserAttemptsLog
                Case "BTNUSERAUTHENTICATIONLOG"
                    frm = New frmUserLogList
                Case "BTNLOGVIEW"
                    frm = New frmAuditTrailViewer
                Case "BTNUSEROPTIONLOG"
                    frm = New frmUserConfigOptionLog
                    'Sohail (12 Apr 2019) -- Start
                    'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
                Case btnSystemErrorLog.Name.ToUpper
                    Dim ofrm As New frmErrorLog
                    ofrm.displayDialog("hrmsConfiguration")
                    'Sohail (12 Apr 2019) -- End
            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnApplicationEventLog_Click", mstrModuleName)
        End Try
    End Sub
#End Region
    'S.SHARMA [ 22 JAN 2013 ] -- END

#End Region

#Region " Private Function "

    Private Sub DoOperation(ByVal strName As String, ByVal fpnl As FlowLayoutPanel)
        For Each ctrl As Control In fpnl.Controls
            If TypeOf ctrl Is eZee.Common.eZeeLightButton Then
                If CStr(ctrl.Name).ToUpper = strName.ToUpper Then
                    CType(ctrl, eZee.Common.eZeeLightButton).Selected = True
                Else
                    CType(ctrl, eZee.Common.eZeeLightButton).Selected = False
                End If
            End If
        Next
    End Sub

    Private Sub SetDyanamicSize(ByVal fpnl As FlowLayoutPanel)
        Try
            Dim styles As TableLayoutColumnStyleCollection = Me.tblMDILayout.ColumnStyles
            For Each style As ColumnStyle In styles
                If style.SizeType = SizeType.Absolute Then
                    If fpnl.VerticalScroll.Visible = True Then
                        style.Width = 197
                    Else
                        style.Width = 180
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDyanamicSize", mstrModuleName)
        End Try
    End Sub

    Private Sub SetControlVisibility(ByVal fpnl As FlowLayoutPanel)
        Try
            If pnlMenuItems.Controls.Count > 0 Then
                pnlMenuItems.Controls(0).Visible = False
                pnlMenuItems.Controls.RemoveAt(0)
            End If
            fpnl.Dock = DockStyle.Fill
            fpnl.Visible = True
            fpnl.BringToFront()
            pnlMenuItems.Controls.Add(fpnl)
            Call SetDyanamicSize(fpnl)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetControlVisibility", mstrModuleName)
        End Try
    End Sub
    'Anjan (20 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub SetVisibility()
        Try
            btnUserLog.Enabled = User._Object.Privilege._AllowToViewUserLog
            btnForceLogout.Enabled = User._Object.Privilege._AllowToPerformForceLogout
            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuAuditTrail.Enabled = User._Object.Privilege._AllowtoViewAuditTrails
            btnUnlockUser.Enabled = User._Object.Privilege._AllowtoUnlockUser

            'Anjan [17 June 2015] -- Start
            'ENHANCEMENT : Stopping Restore/Backup processes from client machine.
            'btnBackUp.Enabled = User._Object.Privilege._AllowtoTakeDatabaseBackup
            'btnRestore.Enabled = User._Object.Privilege._AllowtoRestoreDatabase
            If CBool(AppSettings._Object._IsClient) = True Then
                btnRestore.Visible = False
                btnBackUp.Visible = False
            Else
                btnBackUp.Enabled = User._Object.Privilege._AllowtoTakeDatabaseBackup
                btnRestore.Enabled = User._Object.Privilege._AllowtoRestoreDatabase
            End If
            'Anjan [17 June 2015] -- End


            btnPasswordOption.Enabled = User._Object.Privilege._AllowtoSavePasswordOption
            btnGroupMaster.Enabled = User._Object.Privilege._AllowtoSaveCompanyGroup
            'If IsNumeric(General_Settings._Object._WorkingMode) = False Then
            '    If General_Settings._Object._WorkingMode.ToUpper = enWorkingMode.Server.ToString.ToUpper Then
            mnuOption.Enabled = User._Object.Privilege._AllowtoChangeCompanyOptions
            '    End If
            'Else
            'If General_Settings._Object._WorkingMode = enWorkingMode.Server Then
            '    mnuOption.Enabled = User._Object.Privilege._AllowtoChangeCompanyOptions
            'End If
            'End If

            'If IsNumeric(General_Settings._Object._WorkingMode) = False Then
            '    If General_Settings._Object._WorkingMode.ToUpper = enWorkingMode.Client.ToString.ToUpper Then
            '        mnuOption.Enabled = False
            '    End If
            'Else
            '    If General_Settings._Object._WorkingMode = enWorkingMode.Client Then
            '        mnuOption.Enabled = False
            '    End If
            'End If
            'If User._Object.Privilege._AllowToEditOptionMenu = False Then
            '    mnuOption.Enabled = False
            'End If
            'S.SANDEEP [ 29 OCT 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Anjan (20 Jan 2012)-End

#End Region

    'Sandeep [ 10 FEB 2011 ] -- Start
#Region " Controls "
    Public Sub tmrReminder_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrReminder.Tick
        Try

            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objUserTracing As New clsUser_tracking_Tran

            Dim minttrackingunkid As Integer = objUserTracing.GetTrackTranUnkId(User._Object._Userunkid, getIP(), enLogin_Mode.DESKTOP, True)
            If minttrackingunkid > 0 Then
                objUserTracing._Trackingunkid = minttrackingunkid
                If objUserTracing._Isforcelogout = True Then
                    SetUserTracingInfo(True, enUserMode.Loging, enLogin_Mode.DESKTOP, , True)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 123, "Sorry! You are forcefully logged out. Please Login later."), enMsgBoxStyle.Information)
                    blnIsIdealTimeSet = True
                    Application.Exit()
                End If
            End If
            'Sohail (04 Jul 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tmrReminder_Tick", mstrModuleName)
        Finally
            'tmrReminder.Enabled = True
        End Try
    End Sub
#End Region
    'Sandeep [ 10 FEB 2011 ] -- End 

    'Pinkal (21-Jun-2011) -- Start

#Region "Private Event"

    Private Sub Application_Idle(ByVal e As clsApplicationIdleTimer.ApplicationIdleEventArgs)
        Try
            If User._Object.PWDOptions._IsApplication_Idle Then
                If e.IdleDuration.Minutes >= User._Object.PWDOptions._Idletime Then
                    If clsApplicationIdleTimer.LastIdealTime.Enabled = True Then
                        clsApplicationIdleTimer.LastIdealTime.Stop()
                        clsApplicationIdleTimer.LastIdealTime.Enabled = False

                        'Pinkal (12-Oct-2011) -- Start

                        'S.SANDEEP [14-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {#2252|#ARUTI-162}
                        'Dim obj As New FrmMessageBox
                        'obj.LblMessage.Text = Language.getMessage(mstrModuleName, 5, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to login to the system, Press Ok.")
                        'MsgBox(Language.getMessage(mstrModuleName, 5, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to login to the system, Press Ok."), MsgBoxStyle.Information)
                        'If CBool(obj.ShowDialog(Me)) Then
                        '    blnIsIdealTimeSet = True
                        '    Application.Restart()
                        'End If
                        'blnIsIdealTimeSet = True
                        'Application.Restart()
                        Me.Enabled = False
                        Dim obj As New FrmMessageBox
                        obj.LblMessage.Text = Language.getMessage(mstrModuleName, 5, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok.")
                        obj.ShowDialog()
                        blnIsIdealTimeSet = True
                        Application.Restart()
                        'S.SANDEEP [14-May-2018] -- END
                        

                        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You are logged out of the system. Reason : Application was idle for more time than set in configuration. In order to relogin into system Press Ok."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly) Then
                        '    Application.Restart()
                        'End If

                        'Pinkal (12-Oct-2011) -- End

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Application_Idle", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (21-Jun-2011) -- End





    Private Sub mnuHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuHelp.Click
        Try
            Call Set_Report_Setting()
            Help.ShowHelp(Me, "help_config.chm", HelpNavigator.Topic)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuHelp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub ActiveEmployeesMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActiveEmployeesToolStripMenuItem.Click
        Try
            frm = New ArutiReports.frmCompanyActiveEmployeeReport
            If pnlSummary.Controls.Contains(frm) = True Then
                pnlSummary.Controls.Remove(frm)
            End If
            CType(frm, ArutiReports.frmCompanyActiveEmployeeReport)._IsConfig = True
            CType(frm, ArutiReports.frmCompanyActiveEmployeeReport)._mPanel = pnlMenuItems
            frm.TopLevel = False
            frm.Size = pnlSummary.Size
            pnlSummary.Controls.Add(frm)
            frm.Show()
            frm.BringToFront()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ActiveEmployeesMenuItem_Click", mstrModuleName)
        End Try
    End Sub

    'Shani [ 16 OCT 2014 ] -- START
    'Enhancement-New Report(User Details Log) has been Added To get details of user - requested by rutta
    Private Sub mnuUserDetailLogReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuUserDetailLogReport.Click
        Try
            frm = New ArutiReports.frmUserDetailsLogReport
            If pnlSummary.Controls.Contains(frm) = True Then
                pnlSummary.Controls.Remove(frm)
            End If
            'CType(frm, ArutiReports.frmCompanyActiveEmployeeReport)._IsConfig = True
            'CType(frm, ArutiReports.frmCompanyActiveEmployeeReport)._mPanel = pnlMenuItems
            frm.TopLevel = False
            frm.Size = pnlSummary.Size
            pnlSummary.Controls.Add(frm)
            frm.Show()
            frm.BringToFront()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUserDetailLogReport_Click", mstrModuleName)
        End Try
    End Sub
    'Shani [ 16 OCT 2014 ] -- END



    Private Sub Set_Report_Setting()
        If frm IsNot Nothing Then
            If pnlSummary.Controls.Contains(frm) = True Then
                pnlSummary.Controls.Remove(frm)
            End If
            pnlMenuItems.Enabled = True
        End If
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()
			
			Me.btnCompany.GradientBackColor = GUI._ButttonBackColor 
            Me.btnCompany.GradientForeColor = GUI._ButttonFontColor

			Me.btnUserCreation.GradientBackColor = GUI._ButttonBackColor 
            Me.btnUserCreation.GradientForeColor = GUI._ButttonFontColor

			Me.btnUserRole.GradientBackColor = GUI._ButttonBackColor 
            Me.btnUserRole.GradientForeColor = GUI._ButttonFontColor

			Me.btnAbilityLevel.GradientBackColor = GUI._ButttonBackColor 
            Me.btnAbilityLevel.GradientForeColor = GUI._ButttonFontColor

			Me.btnBackUp.GradientBackColor = GUI._ButttonBackColor 
            Me.btnBackUp.GradientForeColor = GUI._ButttonFontColor

			Me.btnRestore.GradientBackColor = GUI._ButttonBackColor 
            Me.btnRestore.GradientForeColor = GUI._ButttonFontColor

			Me.btnState.GradientBackColor = GUI._ButttonBackColor 
            Me.btnState.GradientForeColor = GUI._ButttonFontColor

			Me.btnCity.GradientBackColor = GUI._ButttonBackColor 
            Me.btnCity.GradientForeColor = GUI._ButttonFontColor

			Me.btnZipcode.GradientBackColor = GUI._ButttonBackColor 
            Me.btnZipcode.GradientForeColor = GUI._ButttonFontColor

			Me.btnVoidReason.GradientBackColor = GUI._ButttonBackColor 
            Me.btnVoidReason.GradientForeColor = GUI._ButttonFontColor

			Me.btnReminderType.GradientBackColor = GUI._ButttonBackColor 
            Me.btnReminderType.GradientForeColor = GUI._ButttonFontColor

			Me.btnPasswordOption.GradientBackColor = GUI._ButttonBackColor 
            Me.btnPasswordOption.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnlockUser.GradientBackColor = GUI._ButttonBackColor 
            Me.btnUnlockUser.GradientForeColor = GUI._ButttonFontColor

			Me.btnUserLog.GradientBackColor = GUI._ButttonBackColor 
            Me.btnUserLog.GradientForeColor = GUI._ButttonFontColor

			Me.btnGroupMaster.GradientBackColor = GUI._ButttonBackColor 
            Me.btnGroupMaster.GradientForeColor = GUI._ButttonFontColor

			Me.btnDeviceList.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDeviceList.GradientForeColor = GUI._ButttonFontColor

            Me.btnEnableAutoUpdate.GradientBackColor = GUI._ButttonBackColor
            Me.btnEnableAutoUpdate.GradientForeColor = GUI._ButttonFontColor

            Me.btnForceLogout.GradientBackColor = GUI._ButttonBackColor
            Me.btnForceLogout.GradientForeColor = GUI._ButttonFontColor

            Me.btnApplicationEventLog.GradientBackColor = GUI._ButttonBackColor
            Me.btnApplicationEventLog.GradientForeColor = GUI._ButttonFontColor

            Me.btnUserAuthenticationLog.GradientBackColor = GUI._ButttonBackColor
            Me.btnUserAuthenticationLog.GradientForeColor = GUI._ButttonFontColor

            Me.btnUserAttempts.GradientBackColor = GUI._ButttonBackColor
            Me.btnUserAttempts.GradientForeColor = GUI._ButttonFontColor

            Me.btnLogView.GradientBackColor = GUI._ButttonBackColor
            Me.btnLogView.GradientForeColor = GUI._ButttonFontColor

            Me.btnUserOptionLog.GradientBackColor = GUI._ButttonBackColor
            Me.btnUserOptionLog.GradientForeColor = GUI._ButttonFontColor

			Me.btnCloneData.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCloneData.GradientForeColor = GUI._ButttonFontColor

			Me.btnSystemErrorLog.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSystemErrorLog.GradientForeColor = GUI._ButttonFontColor

			Me.btnCountry.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCountry.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmailSetup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmailSetup.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.StatusStrip.Text = Language._Object.getCaption(Me.StatusStrip.Name, Me.StatusStrip.Text)
            Me.mnuConfiguration.Text = Language._Object.getCaption(Me.mnuConfiguration.Name, Me.mnuConfiguration.Text)
            Me.mnuOption.Text = Language._Object.getCaption(Me.mnuOption.Name, Me.mnuOption.Text)
            Me.mnuExit.Text = Language._Object.getCaption(Me.mnuExit.Name, Me.mnuExit.Text)
            Me.btnCompany.Text = Language._Object.getCaption(Me.btnCompany.Name, Me.btnCompany.Text)
            Me.btnUserCreation.Text = Language._Object.getCaption(Me.btnUserCreation.Name, Me.btnUserCreation.Text)
            Me.btnUserRole.Text = Language._Object.getCaption(Me.btnUserRole.Name, Me.btnUserRole.Text)
            Me.btnAbilityLevel.Text = Language._Object.getCaption(Me.btnAbilityLevel.Name, Me.btnAbilityLevel.Text)
            Me.btnBackUp.Text = Language._Object.getCaption(Me.btnBackUp.Name, Me.btnBackUp.Text)
            Me.btnRestore.Text = Language._Object.getCaption(Me.btnRestore.Name, Me.btnRestore.Text)
            Me.btnState.Text = Language._Object.getCaption(Me.btnState.Name, Me.btnState.Text)
            Me.btnCity.Text = Language._Object.getCaption(Me.btnCity.Name, Me.btnCity.Text)
            Me.btnZipcode.Text = Language._Object.getCaption(Me.btnZipcode.Name, Me.btnZipcode.Text)
            Me.btnVoidReason.Text = Language._Object.getCaption(Me.btnVoidReason.Name, Me.btnVoidReason.Text)
            Me.btnReminderType.Text = Language._Object.getCaption(Me.btnReminderType.Name, Me.btnReminderType.Text)
            Me.btnPasswordOption.Text = Language._Object.getCaption(Me.btnPasswordOption.Name, Me.btnPasswordOption.Text)
            Me.btnUnlockUser.Text = Language._Object.getCaption(Me.btnUnlockUser.Name, Me.btnUnlockUser.Text)
            Me.btnUserLog.Text = Language._Object.getCaption(Me.btnUserLog.Name, Me.btnUserLog.Text)
            Me.btnGroupMaster.Text = Language._Object.getCaption(Me.btnGroupMaster.Name, Me.btnGroupMaster.Text)
            Me.btnDeviceList.Text = Language._Object.getCaption(Me.btnDeviceList.Name, Me.btnDeviceList.Text)
            Me.btnEnableAutoUpdate.Text = Language._Object.getCaption(Me.btnEnableAutoUpdate.Name, Me.btnEnableAutoUpdate.Text)
            Me.btnForceLogout.Text = Language._Object.getCaption(Me.btnForceLogout.Name, Me.btnForceLogout.Text)
            Me.mnuAuditTrail.Text = Language._Object.getCaption(Me.mnuAuditTrail.Name, Me.mnuAuditTrail.Text)
            Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
            Me.lblWorkingDate.Text = Language._Object.getCaption(Me.lblWorkingDate.Name, Me.lblWorkingDate.Text)
            Me.btnApplicationEventLog.Text = Language._Object.getCaption(Me.btnApplicationEventLog.Name, Me.btnApplicationEventLog.Text)
            Me.btnUserAuthenticationLog.Text = Language._Object.getCaption(Me.btnUserAuthenticationLog.Name, Me.btnUserAuthenticationLog.Text)
            Me.btnUserAttempts.Text = Language._Object.getCaption(Me.btnUserAttempts.Name, Me.btnUserAttempts.Text)
            Me.btnLogView.Text = Language._Object.getCaption(Me.btnLogView.Name, Me.btnLogView.Text)
            Me.mnuHelp.Text = Language._Object.getCaption(Me.mnuHelp.Name, Me.mnuHelp.Text)
            Me.btnUserOptionLog.Text = Language._Object.getCaption(Me.btnUserOptionLog.Name, Me.btnUserOptionLog.Text)
            Me.ReportToolStripMenuItem.Text = Language._Object.getCaption(Me.ReportToolStripMenuItem.Name, Me.ReportToolStripMenuItem.Text)
            Me.ActiveEmployeesToolStripMenuItem.Text = Language._Object.getCaption(Me.ActiveEmployeesToolStripMenuItem.Name, Me.ActiveEmployeesToolStripMenuItem.Text)
            Me.mnuUserDetailLogReport.Text = Language._Object.getCaption(Me.mnuUserDetailLogReport.Name, Me.mnuUserDetailLogReport.Text)
			Me.btnCloneData.Text = Language._Object.getCaption(Me.btnCloneData.Name, Me.btnCloneData.Text)
			Me.btnSystemErrorLog.Text = Language._Object.getCaption(Me.btnSystemErrorLog.Name, Me.btnSystemErrorLog.Text)
			Me.btnCountry.Text = Language._Object.getCaption(Me.btnCountry.Name, Me.btnCountry.Text)
			Me.btnEmailSetup.Text = Language._Object.getCaption(Me.btnEmailSetup.Name, Me.btnEmailSetup.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There are only [")
            Language.setMessage(mstrModuleName, 2, "] Days left before your password expires")
            Language.setMessage(mstrModuleName, 3, "Please change it or increase the notification date, Or contact Administrator.")
            Language.setMessage(mstrModuleName, 4, "Version")
            Language.setMessage(mstrModuleName, 5, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to login to the system, Press Ok.")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to exit?")
            Language.setMessage(mstrModuleName, 123, "Sorry! You are forcefully logged out. Please Login later.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
