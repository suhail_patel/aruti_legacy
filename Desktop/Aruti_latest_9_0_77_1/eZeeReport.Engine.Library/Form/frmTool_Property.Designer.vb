﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTool_Property
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tlp = New System.Windows.Forms.TableLayoutPanel
        Me.tlpRow0 = New System.Windows.Forms.TableLayoutPanel
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnMaxMin = New System.Windows.Forms.Button
        Me.tlp.SuspendLayout()
        Me.tlpRow0.SuspendLayout()
        Me.SuspendLayout()
        '
        'tlp
        '
        Me.tlp.ColumnCount = 1
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp.Controls.Add(Me.tlpRow0, 0, 0)
        Me.tlp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp.Location = New System.Drawing.Point(0, 0)
        Me.tlp.Name = "tlp"
        Me.tlp.RowCount = 2
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp.Size = New System.Drawing.Size(150, 400)
        Me.tlp.TabIndex = 0
        '
        'tlpRow0
        '
        Me.tlpRow0.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.tlpRow0.ColumnCount = 3
        Me.tlpRow0.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.96429!))
        Me.tlpRow0.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.03571!))
        Me.tlpRow0.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tlpRow0.Controls.Add(Me.btnClose, 2, 0)
        Me.tlpRow0.Controls.Add(Me.btnMaxMin, 1, 0)
        Me.tlpRow0.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpRow0.Location = New System.Drawing.Point(3, 3)
        Me.tlpRow0.Name = "tlpRow0"
        Me.tlpRow0.RowCount = 1
        Me.tlpRow0.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpRow0.Size = New System.Drawing.Size(144, 30)
        Me.tlpRow0.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Close_16
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnClose.Location = New System.Drawing.Point(114, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(27, 24)
        Me.btnClose.TabIndex = 2
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnMaxMin
        '
        Me.btnMaxMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMaxMin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnMaxMin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMaxMin.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.min2
        Me.btnMaxMin.Location = New System.Drawing.Point(78, 3)
        Me.btnMaxMin.Name = "btnMaxMin"
        Me.btnMaxMin.Size = New System.Drawing.Size(30, 24)
        Me.btnMaxMin.TabIndex = 0
        Me.btnMaxMin.UseVisualStyleBackColor = True
        '
        'frmTool_Property
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(150, 400)
        Me.Controls.Add(Me.tlp)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmTool_Property"
        Me.Text = "frmTool_Property"
        Me.tlp.ResumeLayout(False)
        Me.tlpRow0.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tlp As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tlpRow0 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnMaxMin As System.Windows.Forms.Button
End Class
