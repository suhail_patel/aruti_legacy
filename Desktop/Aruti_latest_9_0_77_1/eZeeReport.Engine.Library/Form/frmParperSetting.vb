﻿Imports ArutiReport.Engine.Library.eZee_Sample
Public Class frmParperSetting

    Public lOk As Boolean = False

    Public sPaperSize As String
    Public lLandscape As Boolean

    Public nWidht As Decimal = 0
    Public nHeight As Decimal = 0

    Public nTop As Decimal = 0.25
    Public nLeft As Decimal = 0.25
    Public nRight As Decimal = 0.25
    Public nBottom As Decimal = 0.25

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        lOk = False
        Close()
    End Sub

    Private Sub frmParperSetting_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        lOk = TxtValidating()
        If lOk Then
            sPaperSize = cmbPaperSize.Text
            lLandscape = rbLandscape.Checked

            nWidht = txtWidth.Text
            nHeight = txtHeight.Text

            nTop = txtTop.Text
            nLeft = txtLeft.Text
            nRight = txtRight.Text
            nBottom = txtBottom.Text
        End If
        Close()
    End Sub

    Private Function TxtValidating()
        Dim lRetVal As Boolean = True
        'Left
        'If txtLeft.Text < 0.25 Then
        '    MessageBox.Show("Left could not be less than 0.25")
        '    lRetVal = False
        '    txtLeft.Focus()
        '    txtLeft.SelectAll()
        '    Return lRetVal
        '    Exit Function
        'Else
        If txtLeft.Text > 2.25 Then
            MessageBox.Show("Left could not be more than 2.25")
            txtLeft.Focus()
            txtLeft.SelectAll()
            lRetVal = False
            Return lRetVal
            Exit Function
        End If
        'Rigth
        'If txtRight.Text < 0.25 Then
        '    MessageBox.Show("Right could not be less than 0.25")
        '    lRetVal = False
        '    txtRight.Focus()
        '    txtRight.SelectAll()
        '    Return lRetVal
        '    Exit Function
        'Else
        If txtRight.Text > 2.25 Then
            MessageBox.Show("Right could not be more than 2.25")
            txtRight.Focus()
            txtRight.SelectAll()
            lRetVal = False
            Return lRetVal
            Exit Function
        End If
        'Top
        'If txtTop.Text < 0.25 Then
        '    MessageBox.Show("Top could not be less than 0.25")
        '    txtTop.Focus()
        '    txtTop.SelectAll()
        '    lRetVal = False
        '    Return lRetVal
        '    Exit Function
        'Else
        If txtTop.Text > 2.25 Then
            MessageBox.Show("Top could not be more than 2.25")
            txtTop.Focus()
            txtTop.SelectAll()
            lRetVal = False
            Return lRetVal
            Exit Function
        End If
        'bottom
        'If txtBottom.Text < 0.25 Then
        '    MessageBox.Show("Bottom could not be less than 0.25")
        '    txtBottom.Focus()
        '    txtBottom.SelectAll()
        '    lRetVal = False
        '    Return lRetVal
        '    Exit Function
        'Else
        If txtBottom.Text > 2.25 Then
            MessageBox.Show("Bottom could not be more than 2.25")
            txtBottom.Focus()
            txtBottom.SelectAll()
            lRetVal = False
            Return lRetVal
            Exit Function
        End If
        Return lRetVal
    End Function

    Private Sub LoadcmbPaperSize()
        cmbPaperSize.Items.Clear()
        cmbPaperSize.Items.Add("Manual")
        cmbPaperSize.Items.Add("A4")
        cmbPaperSize.Items.Add("Legal")
        cmbPaperSize.Items.Add("Letter")
    End Sub

    Private Sub frmParperSetting_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadcmbPaperSize()
        cmbPaperSize.Text = sPaperSize
        txtWidth.Text = nWidht
        txtHeight.Text = nHeight
        If Not lLandscape Then
            rbPortrait.Checked = True
            rbLandscape.Checked = False
            'tlpPage.Size = New Size((txtWidth.Text * 10) + 20, (txtHeight.Text * 10) + 20)
        Else
            rbPortrait.Checked = False
            rbLandscape.Checked = True
        End If
        tlpPage.Size = New Size((txtWidth.Text * 10) + 20, (txtHeight.Text * 10) + 20)
    End Sub

    Private Sub cmbPaperSize_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPaperSize.TextChanged
        If sender.Text = "Manual" Then
            EnableWidthHiegth()
            txtWidth.Text = 10
            txtHeight.Text = 12
        ElseIf sender.Text = "A4" Then
            DisableWidthHeigth()
            txtWidth.Text = 8.27
            txtHeight.Text = 11.69
        ElseIf sender.Text = "Legal" Then
            DisableWidthHeigth()
            txtWidth.Text = 8.5
            txtHeight.Text = 14
        ElseIf sender.Text = "Letter" Then
            DisableWidthHeigth()
            txtWidth.Text = 8.5
            txtHeight.Text = 11
        End If
        tlpPage.Size = New Size((txtWidth.Text * 10) + 20, (txtHeight.Text * 10) + 20)
        tlpPage.ColumnStyles(0).Width = IIf(txtLeft.Text = 0, 1, ((txtLeft.Text) * 4) + 10)
        tlpPage.ColumnStyles(2).Width = IIf(txtRight.Text = 0, 1, ((txtRight.Text) * 4) + 10)
        tlpPage.RowStyles(0).Height = IIf(txtTop.Text = 0, 1, ((txtTop.Text) * 4) + 10)
        tlpPage.RowStyles(2).Height = IIf(txtBottom.Text = 0, 1, ((txtBottom.Text) * 4) + 10)
        rbPortrait.Checked = True
        rbLandscape.Checked = False
    End Sub

    Private Sub EnableWidthHiegth()

        txtTop.Enabled = True
        txtLeft.Enabled = True
        txtRight.Enabled = True
        txtBottom.Enabled = True


        txtTop.Text = nTop
        txtLeft.Text = nLeft
        txtRight.Text = nRight
        txtBottom.Text = nBottom
        SideHiegth()
        txtWidth.Enabled = True
        txtHeight.Enabled = True
    End Sub

    Private Sub DisableWidthHeigth()
        txtTop.Enabled = True
        txtLeft.Enabled = True
        txtRight.Enabled = True
        txtBottom.Enabled = True

        txtTop.Text = nTop
        txtLeft.Text = nLeft
        txtRight.Text = nRight
        txtBottom.Text = nBottom
        SideHiegth()
        txtWidth.Enabled = False
        txtHeight.Enabled = False
    End Sub

    Private Sub txtBottom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBottom.KeyPress, txtHeight.KeyPress, txtLeft.KeyPress, txtRight.KeyPress, txtTop.KeyPress, txtWidth.KeyPress
        'e.Handled = IsNumberD(e.KeyChar)
        If InStr("0123456789.", e.KeyChar) = 0 Then
            If e.KeyChar.ToString.Split(".").Length > 1 Then
                If e.KeyChar.ToString.Split(".")(1).Length > 2 Then
                    e.KeyChar.ToString.Split(".")(1).Remove(e.KeyChar.ToString.Split(".")(1).Length - 1)
                End If
            End If
            If Asc(e.KeyChar) <> 8 Then
                e.Handled = True
            End If
        End If

    End Sub

    Public Function IsNumberD(ByVal Obj As Object)
        If InStr("0123456789.", Obj) = 0 Then
            If Asc(Obj) <> 8 Then
                Return True
            End If
        End If
        Return False
    End Function

    Private Sub txtWidth_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtWidth.Validating, txtHeight.Validating
        Dim lVld As Boolean = True
        If sender.Text.Split(".").Length > 2 Then
            lVld = False
        ElseIf sender.Text.Split(".").Length > 1 AndAlso sender.Text.Split(".")(1).Length > 2 Then
            lVld = False
        End If
        If Not lVld Then
            MsgBox("Please Insert Proper Number", MsgBoxStyle.Critical, "Numeric error")
            e.Cancel = True
        Else
            If sender.Text <> "0" + IIf(2 > 0, "." + replicate("0", 2), "") Then
                RemovingZero(sender)
            End If
            If sender.Name = "txtWidth" Then txtWidth.Text = sender.text Else txtHeight.Text = sender.text
            tlpPage.Size = New Size((txtWidth.Text * 10) + 20, (txtHeight.Text * 10) + 20)
        End If
    End Sub

    Private Sub rbLandscape_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbLandscape.Click
        If txtHeight.Text <> txtHeight.Tag Then
            txtHeight.Tag = txtWidth.Text
            txtWidth.Text = txtHeight.Text
            txtHeight.Text = txtHeight.Tag
            'tlpPage.Size = New Size((txtHeight.Text * 10) + 20, (txtWidth.Text * 10) + 20)
            tlpPage.Size = New Size((txtWidth.Text * 10) + 20, (txtHeight.Text * 10) + 20)
        End If
    End Sub

    Private Sub rbPortrait_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPortrait.Click
        If txtHeight.Text <> txtWidth.Tag Then
            txtWidth.Tag = txtWidth.Text
            txtWidth.Text = txtHeight.Text
            txtHeight.Text = txtWidth.Tag
            tlpPage.Size = New Size((txtWidth.Text * 10) + 20, (txtHeight.Text * 10) + 20)
        End If
    End Sub

    Private Sub txtBottom_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBottom.Leave, txtHeight.Leave, txtLeft.Leave, txtRight.Leave, txtTop.Leave, txtWidth.Leave
        Dim nDec As Integer = 0
        If sender.Text.Split(".").Length = 1 Then
            nDec = 2
            sender.Text += "." + replicate("0", nDec)
        Else
            nDec = 2 - sender.Text.Split(".")(1).Length
            sender.Text += replicate("0", nDec)
        End If
    End Sub

    Private Sub txtBottom_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtBottom.Validating, txtTop.Validating, txtRight.Validating, txtLeft.Validating
        Dim lVld As Boolean = True
        If sender.Text.Split(".").Length > 2 Then
            lVld = False
        ElseIf sender.Text.Split(".").Length > 1 AndAlso sender.Text.Split(".")(1).Length > 2 Then
            lVld = False
        End If
        If Not lVld Then
            MessageBox.Show("Please Insert Proper Number", "Numeric error", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, False)
            e.Cancel = True
        Else
            'If sender.Text <> "0" + IIf(2 > 0, "." + replicate("0", 2), "") Then
            '    RemovingZero(sender)
            'End If
            If sender.text > 2.25 Then
                MessageBox.Show("Text couldn't be more than  2.25")
                sender.focus()
                sender.Selectall()
            Else
                If sender.name = "txtLeft" Then
                    tlpPage.ColumnStyles(0).Width = IIf(sender.Text = 0, 1, ((sender.Text) * 4) + 10)
                ElseIf sender.name = "txtRight" Then
                    tlpPage.ColumnStyles(2).Width = IIf(sender.Text = 0, 1, ((sender.Text) * 4) + 10)
                ElseIf sender.name = "txtTop" Then
                    tlpPage.RowStyles(0).Height = IIf(sender.Text = 0, 1, ((sender.Text) * 4) + 10)
                ElseIf sender.name = "txtBottom" Then
                    tlpPage.RowStyles(2).Height = IIf(sender.Text = 0, 1, ((sender.Text) * 4) + 10)
                End If
            End If
        End If
    End Sub

    Private Sub SideHiegth()
        tlpPage.ColumnStyles(0).Width = ((txtLeft.Text) * 4) + 10
        tlpPage.ColumnStyles(2).Width = ((txtRight.Text) * 4) + 10
        tlpPage.RowStyles(0).Height = ((txtTop.Text) * 4) + 10
        tlpPage.RowStyles(2).Height = ((txtBottom.Text) * 4) + 10
    End Sub

    Sub RemovingZero(ByVal sender As Object)
        If sender.Text.ToString.Trim <> "0" Then
            If sender.Text.Substring(0, 1) = "0" Then
                sender.Text = sender.Text.Substring(1, sender.Text.Length - 1)
                RemovingZero(sender)
            End If
        End If
    End Sub
End Class