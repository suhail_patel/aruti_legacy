<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportDesigner_New
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportDesigner_New))
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.fpnlTop = New System.Windows.Forms.FlowLayoutPanel
        Me.tsFormattingBar = New System.Windows.Forms.ToolStrip
        Me.edit_Delete_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Align_Left = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Valign_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep3 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Align_Right_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep4 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Align_Top_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep5 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Halign_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep6 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Align_Bottom_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep7 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Same_Width_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep8 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Same_Height_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep9 = New System.Windows.Forms.ToolStripSeparator
        Me.format_Same_Size_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep10 = New System.Windows.Forms.ToolStripSeparator
        Me.edit_Cut_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep11 = New System.Windows.Forms.ToolStripSeparator
        Me.edit_Copy_Button = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep12 = New System.Windows.Forms.ToolStripSeparator
        Me.edit_Paste_Button = New System.Windows.Forms.ToolStripButton
        Me.tsOtherSettings = New System.Windows.Forms.ToolStrip
        Me.chkRepeatHeader = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep13 = New System.Windows.Forms.ToolStripSeparator
        Me.chkShowGrid = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep16 = New System.Windows.Forms.ToolStripSeparator
        Me.chkShowPageWise = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep18 = New System.Windows.Forms.ToolStripSeparator
        Me.chkSnapToGrid = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep20 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmibtnOrientation = New System.Windows.Forms.ToolStripDropDownButton
        Me.chkPortrait = New System.Windows.Forms.ToolStripMenuItem
        Me.chkLandscape = New System.Windows.Forms.ToolStripMenuItem
        Me.objtsmiSep17 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmilblPagesPerSheet = New System.Windows.Forms.ToolStripLabel
        Me.txtMultiPrint = New System.Windows.Forms.ToolStripTextBox
        Me.objtsmiSep19 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmilblPaperWidth = New System.Windows.Forms.ToolStripLabel
        Me.txtCanvasWidth = New System.Windows.Forms.ToolStripTextBox
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel
        Me.txtItemPrint = New System.Windows.Forms.ToolStripTextBox
        Me.cboField = New System.Windows.Forms.ComboBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.btnLoad = New System.Windows.Forms.Button
        Me.chkSaveAsNew = New System.Windows.Forms.CheckBox
        Me.cmbType = New System.Windows.Forms.ComboBox
        Me.lblType = New System.Windows.Forms.Label
        Me.btnConverter = New System.Windows.Forms.ToolStrip
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.WordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExeclToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PdfToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HTMLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.lblXML = New System.Windows.Forms.Label
        Me.txtMstXMLPath = New System.Windows.Forms.TextBox
        Me.btnMstPath = New System.Windows.Forms.Button
        Me.txtDetXMLPath = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.tsMainBar = New System.Windows.Forms.ToolStrip
        Me.btnPageSetUp = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep = New System.Windows.Forms.ToolStripSeparator
        Me.btnSave = New System.Windows.Forms.ToolStripButton
        Me.objtsmi14 = New System.Windows.Forms.ToolStripSeparator
        Me.btnPreview = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep15 = New System.Windows.Forms.ToolStripSeparator
        Me.btnExit = New System.Windows.Forms.ToolStripButton
        Me.cms = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cms_Copy = New System.Windows.Forms.ToolStripMenuItem
        Me.cms_Paste = New System.Windows.Forms.ToolStripMenuItem
        Me.cms_Delete = New System.Windows.Forms.ToolStripMenuItem
        Me.ilToolBarImage = New System.Windows.Forms.ImageList(Me.components)
        Me.oPrintDoc = New System.Drawing.Printing.PrintDocument
        Me.cmslbl = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmSuppress = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_PageSetup = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.tblMain = New System.Windows.Forms.TableLayoutPanel
        Me.tblProperty = New System.Windows.Forms.TableLayoutPanel
        Me.objlblProperty = New System.Windows.Forms.Label
        Me.pgTemplateField = New ArutiReport.Engine.Library.TemplatePropertyGrid
        Me.pnlPrint = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.pnlVScroll = New System.Windows.Forms.Panel
        Me.pnlCanvasRuler = New System.Windows.Forms.Panel
        Me.pnlCompHeaderRuler = New System.Windows.Forms.Panel
        Me.pnlGrpFooterRuler1 = New System.Windows.Forms.Panel
        Me.pnlHeaderRuler = New System.Windows.Forms.Panel
        Me.pnlGrpRuler1 = New System.Windows.Forms.Panel
        Me.pnlFooterRuler = New System.Windows.Forms.Panel
        Me.pnlDetFooterRuler = New System.Windows.Forms.Panel
        Me.pnlDetailRuler = New System.Windows.Forms.Panel
        Me.pnlMainCanvas = New System.Windows.Forms.Panel
        Me.pnlCanvas = New System.Windows.Forms.Panel
        Me.spltFooter = New System.Windows.Forms.Splitter
        Me.pnlFooter = New System.Windows.Forms.Panel
        Me.lblBottomFooter = New System.Windows.Forms.Label
        Me.spltDetailFooter = New System.Windows.Forms.Splitter
        Me.pnlDetailFooter = New System.Windows.Forms.Panel
        Me.lblFooter = New System.Windows.Forms.Label
        Me.spltGroupFooter1 = New System.Windows.Forms.Splitter
        Me.pnlGroupFooter1 = New System.Windows.Forms.Panel
        Me.lblGroupFooter1 = New System.Windows.Forms.Label
        Me.spltDetail = New System.Windows.Forms.Splitter
        Me.pnlDetail = New System.Windows.Forms.Panel
        Me.lblDetail = New System.Windows.Forms.Label
        Me.spltGoup1 = New System.Windows.Forms.Splitter
        Me.pnlGroup1 = New System.Windows.Forms.Panel
        Me.lblGroup1 = New System.Windows.Forms.Label
        Me.spltHeader = New System.Windows.Forms.Splitter
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.spltCompHeader = New System.Windows.Forms.Splitter
        Me.pnlCompHeader = New System.Windows.Forms.Panel
        Me.lblCompHeader = New System.Windows.Forms.Label
        Me.pnlRuler = New System.Windows.Forms.Panel
        Me.objbtnToggleToolbox = New System.Windows.Forms.Button
        Me.objbtnToggleProperty = New System.Windows.Forms.Button
        Me.tblToolbox = New System.Windows.Forms.TableLayoutPanel
        Me.tvControls = New System.Windows.Forms.TreeView
        Me.lblPrintingTools = New System.Windows.Forms.Label
        Me.lblNote = New System.Windows.Forms.Label
        Me.tsBottomBar = New System.Windows.Forms.ToolStrip
        Me.tsmilblTemplateName = New System.Windows.Forms.ToolStripLabel
        Me.tsmilblName = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel
        Me.tslblDelete = New System.Windows.Forms.ToolStripLabel
        Me.objtsmi1 = New System.Windows.Forms.ToolStripSeparator
        Me.tslblSelectAll = New System.Windows.Forms.ToolStripLabel
        Me.objtsmi2 = New System.Windows.Forms.ToolStripSeparator
        Me.tslblUnSelect = New System.Windows.Forms.ToolStripLabel
        Me.cmsFields = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NewFormulaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EditFormulaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteFormulaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlTop.SuspendLayout()
        Me.fpnlTop.SuspendLayout()
        Me.tsFormattingBar.SuspendLayout()
        Me.tsOtherSettings.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.btnConverter.SuspendLayout()
        Me.tsMainBar.SuspendLayout()
        Me.cms.SuspendLayout()
        Me.cmslbl.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.tblMain.SuspendLayout()
        Me.tblProperty.SuspendLayout()
        Me.pnlPrint.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.pnlVScroll.SuspendLayout()
        Me.pnlCanvasRuler.SuspendLayout()
        Me.pnlMainCanvas.SuspendLayout()
        Me.pnlCanvas.SuspendLayout()
        Me.tblToolbox.SuspendLayout()
        Me.tsBottomBar.SuspendLayout()
        Me.cmsFields.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTop
        '
        Me.pnlTop.Controls.Add(Me.fpnlTop)
        Me.pnlTop.Controls.Add(Me.Panel3)
        Me.pnlTop.Controls.Add(Me.Label2)
        Me.pnlTop.Controls.Add(Me.TextBox1)
        Me.pnlTop.Controls.Add(Me.tsMainBar)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(1142, 53)
        Me.pnlTop.TabIndex = 1
        '
        'fpnlTop
        '
        Me.fpnlTop.BackColor = System.Drawing.Color.White
        Me.fpnlTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.fpnlTop.Controls.Add(Me.tsFormattingBar)
        Me.fpnlTop.Controls.Add(Me.tsOtherSettings)
        Me.fpnlTop.Controls.Add(Me.cboField)
        Me.fpnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.fpnlTop.Location = New System.Drawing.Point(0, 25)
        Me.fpnlTop.Name = "fpnlTop"
        Me.fpnlTop.Size = New System.Drawing.Size(1142, 28)
        Me.fpnlTop.TabIndex = 85
        '
        'tsFormattingBar
        '
        Me.tsFormattingBar.BackColor = System.Drawing.Color.Transparent
        Me.tsFormattingBar.Dock = System.Windows.Forms.DockStyle.None
        Me.tsFormattingBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.edit_Delete_Button, Me.objtsmiSep1, Me.format_Align_Left, Me.objtsmiSep2, Me.format_Valign_Button, Me.objtsmiSep3, Me.format_Align_Right_Button, Me.objtsmiSep4, Me.format_Align_Top_Button, Me.objtsmiSep5, Me.format_Halign_Button, Me.objtsmiSep6, Me.format_Align_Bottom_Button, Me.objtsmiSep7, Me.format_Same_Width_Button, Me.objtsmiSep8, Me.format_Same_Height_Button, Me.objtsmiSep9, Me.format_Same_Size_Button, Me.objtsmiSep10, Me.edit_Cut_Button, Me.objtsmiSep11, Me.edit_Copy_Button, Me.objtsmiSep12, Me.edit_Paste_Button})
        Me.tsFormattingBar.Location = New System.Drawing.Point(0, 0)
        Me.tsFormattingBar.Name = "tsFormattingBar"
        Me.tsFormattingBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tsFormattingBar.Size = New System.Drawing.Size(296, 25)
        Me.tsFormattingBar.TabIndex = 4
        Me.tsFormattingBar.Text = "ToolStrip1"
        '
        'edit_Delete_Button
        '
        Me.edit_Delete_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.edit_Delete_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.delete_16x16
        Me.edit_Delete_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.edit_Delete_Button.Name = "edit_Delete_Button"
        Me.edit_Delete_Button.Size = New System.Drawing.Size(23, 21)
        Me.edit_Delete_Button.ToolTipText = "Delete Selected Items"
        '
        'objtsmiSep1
        '
        Me.objtsmiSep1.Name = "objtsmiSep1"
        Me.objtsmiSep1.Size = New System.Drawing.Size(6, 24)
        '
        'format_Align_Left
        '
        Me.format_Align_Left.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Align_Left.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_align_left_16x16
        Me.format_Align_Left.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Align_Left.Name = "format_Align_Left"
        Me.format_Align_Left.Size = New System.Drawing.Size(23, 21)
        Me.format_Align_Left.ToolTipText = "Align Left"
        '
        'objtsmiSep2
        '
        Me.objtsmiSep2.Name = "objtsmiSep2"
        Me.objtsmiSep2.Size = New System.Drawing.Size(6, 24)
        '
        'format_Valign_Button
        '
        Me.format_Valign_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Valign_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_align_hcentre_16x16
        Me.format_Valign_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Valign_Button.Name = "format_Valign_Button"
        Me.format_Valign_Button.Size = New System.Drawing.Size(23, 21)
        Me.format_Valign_Button.ToolTipText = "Align Centers"
        '
        'objtsmiSep3
        '
        Me.objtsmiSep3.Name = "objtsmiSep3"
        Me.objtsmiSep3.Size = New System.Drawing.Size(6, 24)
        '
        'format_Align_Right_Button
        '
        Me.format_Align_Right_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Align_Right_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_align_right_16x16
        Me.format_Align_Right_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Align_Right_Button.Name = "format_Align_Right_Button"
        Me.format_Align_Right_Button.Size = New System.Drawing.Size(23, 21)
        Me.format_Align_Right_Button.ToolTipText = "Align Rights"
        '
        'objtsmiSep4
        '
        Me.objtsmiSep4.Name = "objtsmiSep4"
        Me.objtsmiSep4.Size = New System.Drawing.Size(6, 24)
        '
        'format_Align_Top_Button
        '
        Me.format_Align_Top_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Align_Top_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_align_top_16x16
        Me.format_Align_Top_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Align_Top_Button.Name = "format_Align_Top_Button"
        Me.format_Align_Top_Button.Size = New System.Drawing.Size(23, 21)
        Me.format_Align_Top_Button.ToolTipText = "Align Tops"
        '
        'objtsmiSep5
        '
        Me.objtsmiSep5.Name = "objtsmiSep5"
        Me.objtsmiSep5.Size = New System.Drawing.Size(6, 24)
        '
        'format_Halign_Button
        '
        Me.format_Halign_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Halign_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_align_vcentre_16x16
        Me.format_Halign_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Halign_Button.Name = "format_Halign_Button"
        Me.format_Halign_Button.Size = New System.Drawing.Size(23, 21)
        Me.format_Halign_Button.ToolTipText = "Align Middles"
        '
        'objtsmiSep6
        '
        Me.objtsmiSep6.Name = "objtsmiSep6"
        Me.objtsmiSep6.Size = New System.Drawing.Size(6, 24)
        '
        'format_Align_Bottom_Button
        '
        Me.format_Align_Bottom_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Align_Bottom_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_align_bottom_16x16
        Me.format_Align_Bottom_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Align_Bottom_Button.Name = "format_Align_Bottom_Button"
        Me.format_Align_Bottom_Button.Size = New System.Drawing.Size(23, 21)
        Me.format_Align_Bottom_Button.ToolTipText = "Align Bottoms"
        '
        'objtsmiSep7
        '
        Me.objtsmiSep7.Name = "objtsmiSep7"
        Me.objtsmiSep7.Size = New System.Drawing.Size(6, 24)
        '
        'format_Same_Width_Button
        '
        Me.format_Same_Width_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Same_Width_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_same_width_16x16
        Me.format_Same_Width_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Same_Width_Button.Name = "format_Same_Width_Button"
        Me.format_Same_Width_Button.Size = New System.Drawing.Size(23, 21)
        Me.format_Same_Width_Button.ToolTipText = "Make Same Width"
        '
        'objtsmiSep8
        '
        Me.objtsmiSep8.Name = "objtsmiSep8"
        Me.objtsmiSep8.Size = New System.Drawing.Size(6, 24)
        '
        'format_Same_Height_Button
        '
        Me.format_Same_Height_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Same_Height_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_same_height_16x16
        Me.format_Same_Height_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Same_Height_Button.Name = "format_Same_Height_Button"
        Me.format_Same_Height_Button.Size = New System.Drawing.Size(23, 21)
        Me.format_Same_Height_Button.ToolTipText = "Make Same Height"
        '
        'objtsmiSep9
        '
        Me.objtsmiSep9.Name = "objtsmiSep9"
        Me.objtsmiSep9.Size = New System.Drawing.Size(6, 24)
        '
        'format_Same_Size_Button
        '
        Me.format_Same_Size_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.format_Same_Size_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.format_same_size_16x16
        Me.format_Same_Size_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.format_Same_Size_Button.Name = "format_Same_Size_Button"
        Me.format_Same_Size_Button.Size = New System.Drawing.Size(23, 21)
        Me.format_Same_Size_Button.ToolTipText = "Make Same Size"
        '
        'objtsmiSep10
        '
        Me.objtsmiSep10.Name = "objtsmiSep10"
        Me.objtsmiSep10.Size = New System.Drawing.Size(6, 24)
        Me.objtsmiSep10.Visible = False
        '
        'edit_Cut_Button
        '
        Me.edit_Cut_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.edit_Cut_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.cut_16x16
        Me.edit_Cut_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.edit_Cut_Button.Name = "edit_Cut_Button"
        Me.edit_Cut_Button.Size = New System.Drawing.Size(23, 21)
        Me.edit_Cut_Button.Text = "ToolStripButton1"
        Me.edit_Cut_Button.Visible = False
        '
        'objtsmiSep11
        '
        Me.objtsmiSep11.Name = "objtsmiSep11"
        Me.objtsmiSep11.Size = New System.Drawing.Size(6, 24)
        Me.objtsmiSep11.Visible = False
        '
        'edit_Copy_Button
        '
        Me.edit_Copy_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.edit_Copy_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.copy_16x16
        Me.edit_Copy_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.edit_Copy_Button.Name = "edit_Copy_Button"
        Me.edit_Copy_Button.Size = New System.Drawing.Size(23, 21)
        Me.edit_Copy_Button.Text = "ToolStripButton2"
        Me.edit_Copy_Button.Visible = False
        '
        'objtsmiSep12
        '
        Me.objtsmiSep12.Name = "objtsmiSep12"
        Me.objtsmiSep12.Size = New System.Drawing.Size(6, 24)
        Me.objtsmiSep12.Visible = False
        '
        'edit_Paste_Button
        '
        Me.edit_Paste_Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.edit_Paste_Button.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.paste_16x16
        Me.edit_Paste_Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.edit_Paste_Button.Name = "edit_Paste_Button"
        Me.edit_Paste_Button.Size = New System.Drawing.Size(23, 21)
        Me.edit_Paste_Button.Text = "ToolStripButton3"
        Me.edit_Paste_Button.Visible = False
        '
        'tsOtherSettings
        '
        Me.tsOtherSettings.BackColor = System.Drawing.Color.Transparent
        Me.tsOtherSettings.Dock = System.Windows.Forms.DockStyle.None
        Me.tsOtherSettings.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.chkRepeatHeader, Me.objtsmiSep13, Me.chkShowGrid, Me.objtsmiSep16, Me.chkShowPageWise, Me.objtsmiSep18, Me.chkSnapToGrid, Me.objtsmiSep20, Me.tsmibtnOrientation, Me.objtsmiSep17, Me.tsmilblPagesPerSheet, Me.txtMultiPrint, Me.objtsmiSep19, Me.tsmilblPaperWidth, Me.txtCanvasWidth, Me.ToolStripSeparator2, Me.ToolStripLabel2, Me.txtItemPrint})
        Me.tsOtherSettings.Location = New System.Drawing.Point(296, 0)
        Me.tsOtherSettings.Name = "tsOtherSettings"
        Me.tsOtherSettings.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tsOtherSettings.Size = New System.Drawing.Size(753, 25)
        Me.tsOtherSettings.TabIndex = 88
        Me.tsOtherSettings.Text = "ToolStrip2"
        '
        'chkRepeatHeader
        '
        Me.chkRepeatHeader.BackColor = System.Drawing.Color.Transparent
        Me.chkRepeatHeader.CheckOnClick = True
        Me.chkRepeatHeader.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.chkRepeatHeader.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.CheckBoxNone_24
        Me.chkRepeatHeader.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.chkRepeatHeader.Name = "chkRepeatHeader"
        Me.chkRepeatHeader.Size = New System.Drawing.Size(143, 22)
        Me.chkRepeatHeader.Text = "Repeat Company Header"
        '
        'objtsmiSep13
        '
        Me.objtsmiSep13.Name = "objtsmiSep13"
        Me.objtsmiSep13.Size = New System.Drawing.Size(6, 24)
        '
        'chkShowGrid
        '
        Me.chkShowGrid.CheckOnClick = True
        Me.chkShowGrid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.chkShowGrid.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.CheckBoxNone_24
        Me.chkShowGrid.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.chkShowGrid.Name = "chkShowGrid"
        Me.chkShowGrid.Size = New System.Drawing.Size(65, 22)
        Me.chkShowGrid.Text = "Show Grid"
        '
        'objtsmiSep16
        '
        Me.objtsmiSep16.Name = "objtsmiSep16"
        Me.objtsmiSep16.Size = New System.Drawing.Size(6, 24)
        '
        'chkShowPageWise
        '
        Me.chkShowPageWise.CheckOnClick = True
        Me.chkShowPageWise.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.chkShowPageWise.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.CheckBoxNone_24
        Me.chkShowPageWise.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.chkShowPageWise.Name = "chkShowPageWise"
        Me.chkShowPageWise.Size = New System.Drawing.Size(63, 22)
        Me.chkShowPageWise.Text = "Page wise"
        '
        'objtsmiSep18
        '
        Me.objtsmiSep18.Name = "objtsmiSep18"
        Me.objtsmiSep18.Size = New System.Drawing.Size(6, 24)
        '
        'chkSnapToGrid
        '
        Me.chkSnapToGrid.CheckOnClick = True
        Me.chkSnapToGrid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.chkSnapToGrid.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.CheckBoxNone_24
        Me.chkSnapToGrid.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.chkSnapToGrid.Name = "chkSnapToGrid"
        Me.chkSnapToGrid.Size = New System.Drawing.Size(76, 22)
        Me.chkSnapToGrid.Text = "Snap to Grid"
        '
        'objtsmiSep20
        '
        Me.objtsmiSep20.Name = "objtsmiSep20"
        Me.objtsmiSep20.Size = New System.Drawing.Size(6, 24)
        Me.objtsmiSep20.Visible = False
        '
        'tsmibtnOrientation
        '
        Me.tsmibtnOrientation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.chkPortrait, Me.chkLandscape})
        Me.tsmibtnOrientation.Image = CType(resources.GetObject("tsmibtnOrientation.Image"), System.Drawing.Image)
        Me.tsmibtnOrientation.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsmibtnOrientation.Name = "tsmibtnOrientation"
        Me.tsmibtnOrientation.Size = New System.Drawing.Size(96, 22)
        Me.tsmibtnOrientation.Text = "Orientation"
        Me.tsmibtnOrientation.Visible = False
        '
        'chkPortrait
        '
        Me.chkPortrait.Checked = True
        Me.chkPortrait.CheckOnClick = True
        Me.chkPortrait.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPortrait.Name = "chkPortrait"
        Me.chkPortrait.Size = New System.Drawing.Size(152, 22)
        Me.chkPortrait.Text = "Portrait"
        '
        'chkLandscape
        '
        Me.chkLandscape.CheckOnClick = True
        Me.chkLandscape.Name = "chkLandscape"
        Me.chkLandscape.Size = New System.Drawing.Size(152, 22)
        Me.chkLandscape.Text = "Landscape"
        '
        'objtsmiSep17
        '
        Me.objtsmiSep17.Name = "objtsmiSep17"
        Me.objtsmiSep17.Size = New System.Drawing.Size(6, 24)
        '
        'tsmilblPagesPerSheet
        '
        Me.tsmilblPagesPerSheet.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsmilblPagesPerSheet.Name = "tsmilblPagesPerSheet"
        Me.tsmilblPagesPerSheet.Size = New System.Drawing.Size(84, 21)
        Me.tsmilblPagesPerSheet.Text = "Page per sheet"
        Me.tsmilblPagesPerSheet.ToolTipText = "Page per sheet"
        '
        'txtMultiPrint
        '
        Me.txtMultiPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMultiPrint.Name = "txtMultiPrint"
        Me.txtMultiPrint.Size = New System.Drawing.Size(40, 24)
        Me.txtMultiPrint.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objtsmiSep19
        '
        Me.objtsmiSep19.Name = "objtsmiSep19"
        Me.objtsmiSep19.Size = New System.Drawing.Size(6, 24)
        '
        'tsmilblPaperWidth
        '
        Me.tsmilblPaperWidth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsmilblPaperWidth.Name = "tsmilblPaperWidth"
        Me.tsmilblPaperWidth.Size = New System.Drawing.Size(72, 21)
        Me.tsmilblPaperWidth.Text = "Paper Width"
        '
        'txtCanvasWidth
        '
        Me.txtCanvasWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCanvasWidth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCanvasWidth.Name = "txtCanvasWidth"
        Me.txtCanvasWidth.Size = New System.Drawing.Size(40, 24)
        Me.txtCanvasWidth.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 24)
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(76, 21)
        Me.ToolStripLabel2.Text = "Detail Height"
        '
        'txtItemPrint
        '
        Me.txtItemPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtItemPrint.Name = "txtItemPrint"
        Me.txtItemPrint.Size = New System.Drawing.Size(40, 24)
        Me.txtItemPrint.Text = "6.00"
        Me.txtItemPrint.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboField
        '
        Me.cboField.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cboField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboField.DropDownWidth = 250
        Me.cboField.FormattingEnabled = True
        Me.cboField.Location = New System.Drawing.Point(1052, 3)
        Me.cboField.Name = "cboField"
        Me.cboField.Size = New System.Drawing.Size(48, 21)
        Me.cboField.TabIndex = 81
        Me.cboField.Visible = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnLoad)
        Me.Panel3.Controls.Add(Me.chkSaveAsNew)
        Me.Panel3.Controls.Add(Me.cmbType)
        Me.Panel3.Controls.Add(Me.lblType)
        Me.Panel3.Controls.Add(Me.btnConverter)
        Me.Panel3.Controls.Add(Me.btnDelete)
        Me.Panel3.Controls.Add(Me.btnCancel)
        Me.Panel3.Controls.Add(Me.lblXML)
        Me.Panel3.Controls.Add(Me.txtMstXMLPath)
        Me.Panel3.Controls.Add(Me.btnMstPath)
        Me.Panel3.Controls.Add(Me.txtDetXMLPath)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Location = New System.Drawing.Point(10, 98)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(690, 52)
        Me.Panel3.TabIndex = 88
        '
        'btnLoad
        '
        Me.btnLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnLoad.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLoad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoad.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Load
        Me.btnLoad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLoad.Location = New System.Drawing.Point(385, 3)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(68, 25)
        Me.btnLoad.TabIndex = 68
        Me.btnLoad.Text = "&Load"
        Me.btnLoad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'chkSaveAsNew
        '
        Me.chkSaveAsNew.AutoSize = True
        Me.chkSaveAsNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSaveAsNew.ForeColor = System.Drawing.Color.OliveDrab
        Me.chkSaveAsNew.Location = New System.Drawing.Point(342, 32)
        Me.chkSaveAsNew.Name = "chkSaveAsNew"
        Me.chkSaveAsNew.Size = New System.Drawing.Size(106, 17)
        Me.chkSaveAsNew.TabIndex = 80
        Me.chkSaveAsNew.Text = " Save As New"
        Me.chkSaveAsNew.UseVisualStyleBackColor = True
        '
        'cmbType
        '
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.Location = New System.Drawing.Point(541, 6)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(143, 21)
        Me.cmbType.TabIndex = 67
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(3, 6)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(35, 13)
        Me.lblType.TabIndex = 66
        Me.lblType.Text = "Type"
        '
        'btnConverter
        '
        Me.btnConverter.BackColor = System.Drawing.Color.Lavender
        Me.btnConverter.Dock = System.Windows.Forms.DockStyle.None
        Me.btnConverter.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.btnConverter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSplitButton1})
        Me.btnConverter.Location = New System.Drawing.Point(459, 4)
        Me.btnConverter.Name = "btnConverter"
        Me.btnConverter.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.btnConverter.Size = New System.Drawing.Size(68, 25)
        Me.btnConverter.TabIndex = 65
        Me.btnConverter.Text = "ToolStrip1"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WordToolStripMenuItem, Me.ExeclToolStripMenuItem, Me.PdfToolStripMenuItem, Me.HTMLToolStripMenuItem})
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(65, 22)
        Me.ToolStripSplitButton1.Text = "Convert"
        '
        'WordToolStripMenuItem
        '
        Me.WordToolStripMenuItem.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Word_16
        Me.WordToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.WordToolStripMenuItem.Name = "WordToolStripMenuItem"
        Me.WordToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.WordToolStripMenuItem.Text = "Word"
        '
        'ExeclToolStripMenuItem
        '
        Me.ExeclToolStripMenuItem.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Excel_16
        Me.ExeclToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.ExeclToolStripMenuItem.Name = "ExeclToolStripMenuItem"
        Me.ExeclToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.ExeclToolStripMenuItem.Text = "Execl"
        '
        'PdfToolStripMenuItem
        '
        Me.PdfToolStripMenuItem.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.PDF_16
        Me.PdfToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.PdfToolStripMenuItem.Name = "PdfToolStripMenuItem"
        Me.PdfToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.PdfToolStripMenuItem.Text = "Pdf"
        '
        'HTMLToolStripMenuItem
        '
        Me.HTMLToolStripMenuItem.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.HTML_16
        Me.HTMLToolStripMenuItem.Name = "HTMLToolStripMenuItem"
        Me.HTMLToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.HTMLToolStripMenuItem.Text = "HTML"
        '
        'btnDelete
        '
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.delete_20
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(305, 3)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(72, 27)
        Me.btnDelete.TabIndex = 70
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.CancelCall_16
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(230, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(69, 27)
        Me.btnCancel.TabIndex = 76
        Me.btnCancel.Text = "C&ancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblXML
        '
        Me.lblXML.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblXML.ForeColor = System.Drawing.Color.OliveDrab
        Me.lblXML.Location = New System.Drawing.Point(230, 33)
        Me.lblXML.Name = "lblXML"
        Me.lblXML.Size = New System.Drawing.Size(106, 13)
        Me.lblXML.TabIndex = 74
        Me.lblXML.Text = "Master XML"
        Me.lblXML.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtMstXMLPath
        '
        Me.txtMstXMLPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMstXMLPath.ForeColor = System.Drawing.Color.Red
        Me.txtMstXMLPath.Location = New System.Drawing.Point(75, 4)
        Me.txtMstXMLPath.Name = "txtMstXMLPath"
        Me.txtMstXMLPath.Size = New System.Drawing.Size(116, 20)
        Me.txtMstXMLPath.TabIndex = 73
        '
        'btnMstPath
        '
        Me.btnMstPath.Enabled = False
        Me.btnMstPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMstPath.Location = New System.Drawing.Point(197, 4)
        Me.btnMstPath.Name = "btnMstPath"
        Me.btnMstPath.Size = New System.Drawing.Size(27, 20)
        Me.btnMstPath.TabIndex = 75
        Me.btnMstPath.Text = "..."
        Me.btnMstPath.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMstPath.UseVisualStyleBackColor = True
        Me.btnMstPath.Visible = False
        '
        'txtDetXMLPath
        '
        Me.txtDetXMLPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetXMLPath.ForeColor = System.Drawing.Color.Red
        Me.txtDetXMLPath.Location = New System.Drawing.Point(75, 28)
        Me.txtDetXMLPath.Name = "txtDetXMLPath"
        Me.txtDetXMLPath.Size = New System.Drawing.Size(149, 20)
        Me.txtDetXMLPath.TabIndex = 78
        Me.txtDetXMLPath.Visible = False
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.OliveDrab
        Me.Label5.Location = New System.Drawing.Point(0, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 13)
        Me.Label5.TabIndex = 79
        Me.Label5.Text = "Detail XML"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label5.Visible = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Gainsboro
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(1044, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 23)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Detail Height "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(1131, 34)
        Me.TextBox1.MaxLength = 5
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(41, 21)
        Me.TextBox1.TabIndex = 6
        '
        'tsMainBar
        '
        Me.tsMainBar.BackColor = System.Drawing.Color.White
        Me.tsMainBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsMainBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnPageSetUp, Me.objtsmiSep, Me.btnSave, Me.objtsmi14, Me.btnPreview, Me.objtsmiSep15, Me.btnExit})
        Me.tsMainBar.Location = New System.Drawing.Point(0, 0)
        Me.tsMainBar.Name = "tsMainBar"
        Me.tsMainBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tsMainBar.Size = New System.Drawing.Size(1142, 25)
        Me.tsMainBar.TabIndex = 87
        Me.tsMainBar.Text = "ToolStrip2"
        '
        'btnPageSetUp
        '
        Me.btnPageSetUp.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.PageSetup_16
        Me.btnPageSetUp.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnPageSetUp.Name = "btnPageSetUp"
        Me.btnPageSetUp.Size = New System.Drawing.Size(86, 22)
        Me.btnPageSetUp.Text = "Page Setup"
        '
        'objtsmiSep
        '
        Me.objtsmiSep.Name = "objtsmiSep"
        Me.objtsmiSep.Size = New System.Drawing.Size(6, 25)
        '
        'btnSave
        '
        Me.btnSave.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Save_16
        Me.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(51, 22)
        Me.btnSave.Text = "&Save"
        '
        'objtsmi14
        '
        Me.objtsmi14.Name = "objtsmi14"
        Me.objtsmi14.Size = New System.Drawing.Size(6, 25)
        '
        'btnPreview
        '
        Me.btnPreview.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.PrintPreview_16
        Me.btnPreview.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(68, 22)
        Me.btnPreview.Text = "&Preview"
        '
        'objtsmiSep15
        '
        Me.objtsmiSep15.Name = "objtsmiSep15"
        Me.objtsmiSep15.Size = New System.Drawing.Size(6, 25)
        '
        'btnExit
        '
        Me.btnExit.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Exit_App_16
        Me.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(45, 22)
        Me.btnExit.Text = "&Exit"
        '
        'cms
        '
        Me.cms.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cms_Copy, Me.cms_Paste, Me.cms_Delete})
        Me.cms.Name = "cms"
        Me.cms.Size = New System.Drawing.Size(108, 70)
        '
        'cms_Copy
        '
        Me.cms_Copy.Name = "cms_Copy"
        Me.cms_Copy.Size = New System.Drawing.Size(107, 22)
        Me.cms_Copy.Text = "&Copy"
        '
        'cms_Paste
        '
        Me.cms_Paste.Name = "cms_Paste"
        Me.cms_Paste.Size = New System.Drawing.Size(107, 22)
        Me.cms_Paste.Text = "&Paste"
        '
        'cms_Delete
        '
        Me.cms_Delete.Name = "cms_Delete"
        Me.cms_Delete.Size = New System.Drawing.Size(107, 22)
        Me.cms_Delete.Text = "&Delete"
        '
        'ilToolBarImage
        '
        Me.ilToolBarImage.ImageStream = CType(resources.GetObject("ilToolBarImage.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilToolBarImage.TransparentColor = System.Drawing.Color.Transparent
        Me.ilToolBarImage.Images.SetKeyName(0, "paste_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(1, "copy_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(2, "cut_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(3, "format_align_hcentre_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(4, "format_align_left_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(5, "format_align_right_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(6, "format_align_top_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(7, "format_align_vcentre_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(8, "format_same_height_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(9, "format_same_height_min_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(10, "format_same_size_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(11, "format_same_width_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(12, "format_same_width_min_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(13, "box_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(14, "delete_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(15, "line_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(16, "format_align_bottom_16x16.gif")
        '
        'cmslbl
        '
        Me.cmslbl.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmSuppress, Me.tsm_PageSetup})
        Me.cmslbl.Name = "cmslbl"
        Me.cmslbl.Size = New System.Drawing.Size(134, 48)
        '
        'tsmSuppress
        '
        Me.tsmSuppress.Name = "tsmSuppress"
        Me.tsmSuppress.Size = New System.Drawing.Size(133, 22)
        Me.tsmSuppress.Text = "&Suppress"
        '
        'tsm_PageSetup
        '
        Me.tsm_PageSetup.Name = "tsm_PageSetup"
        Me.tsm_PageSetup.Size = New System.Drawing.Size(133, 22)
        Me.tsm_PageSetup.Text = "&Page Setup"
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.tblMain)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 53)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1142, 607)
        Me.pnlMain.TabIndex = 4
        '
        'tblMain
        '
        Me.tblMain.ColumnCount = 5
        Me.tblMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.tblMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.tblMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.tblMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.tblMain.Controls.Add(Me.tblProperty, 4, 0)
        Me.tblMain.Controls.Add(Me.pnlPrint, 2, 0)
        Me.tblMain.Controls.Add(Me.objbtnToggleToolbox, 1, 0)
        Me.tblMain.Controls.Add(Me.objbtnToggleProperty, 3, 0)
        Me.tblMain.Controls.Add(Me.tblToolbox, 0, 0)
        Me.tblMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblMain.Location = New System.Drawing.Point(0, 0)
        Me.tblMain.Name = "tblMain"
        Me.tblMain.RowCount = 1
        Me.tblMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 607.0!))
        Me.tblMain.Size = New System.Drawing.Size(1142, 607)
        Me.tblMain.TabIndex = 0
        '
        'tblProperty
        '
        Me.tblProperty.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.tblProperty.ColumnCount = 1
        Me.tblProperty.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblProperty.Controls.Add(Me.objlblProperty, 0, 0)
        Me.tblProperty.Controls.Add(Me.pgTemplateField, 0, 1)
        Me.tblProperty.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblProperty.Location = New System.Drawing.Point(911, 3)
        Me.tblProperty.Name = "tblProperty"
        Me.tblProperty.RowCount = 2
        Me.tblProperty.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.tblProperty.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblProperty.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblProperty.Size = New System.Drawing.Size(228, 601)
        Me.tblProperty.TabIndex = 81
        '
        'objlblProperty
        '
        Me.objlblProperty.BackColor = System.Drawing.Color.Silver
        Me.objlblProperty.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblProperty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProperty.Location = New System.Drawing.Point(3, 3)
        Me.objlblProperty.Margin = New System.Windows.Forms.Padding(0)
        Me.objlblProperty.Name = "objlblProperty"
        Me.objlblProperty.Size = New System.Drawing.Size(222, 22)
        Me.objlblProperty.TabIndex = 80
        Me.objlblProperty.Text = "Property"
        Me.objlblProperty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pgTemplateField
        '
        Me.pgTemplateField.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pgTemplateField.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pgTemplateField.HelpVisible = False
        Me.pgTemplateField.Location = New System.Drawing.Point(3, 28)
        Me.pgTemplateField.Margin = New System.Windows.Forms.Padding(0)
        Me.pgTemplateField.Name = "pgTemplateField"
        Me.pgTemplateField.PropertySort = System.Windows.Forms.PropertySort.NoSort
        Me.pgTemplateField.Size = New System.Drawing.Size(222, 570)
        Me.pgTemplateField.TabIndex = 79
        Me.pgTemplateField.ToolbarVisible = False
        '
        'pnlPrint
        '
        Me.pnlPrint.Controls.Add(Me.SplitContainer1)
        Me.pnlPrint.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPrint.Location = New System.Drawing.Point(257, 3)
        Me.pnlPrint.Name = "pnlPrint"
        Me.pnlPrint.Size = New System.Drawing.Size(631, 601)
        Me.pnlPrint.TabIndex = 75
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.pnlVScroll)
        Me.SplitContainer1.Panel1MinSize = 10
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.pnlMainCanvas)
        Me.SplitContainer1.Panel2.Controls.Add(Me.pnlRuler)
        Me.SplitContainer1.Size = New System.Drawing.Size(631, 601)
        Me.SplitContainer1.SplitterDistance = 33
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 38
        '
        'pnlVScroll
        '
        Me.pnlVScroll.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlVScroll.AutoScroll = True
        Me.pnlVScroll.Controls.Add(Me.pnlCanvasRuler)
        Me.pnlVScroll.Location = New System.Drawing.Point(3, 23)
        Me.pnlVScroll.Name = "pnlVScroll"
        Me.pnlVScroll.Size = New System.Drawing.Size(44, 551)
        Me.pnlVScroll.TabIndex = 4
        '
        'pnlCanvasRuler
        '
        Me.pnlCanvasRuler.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlCanvasRuler.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCanvasRuler.Controls.Add(Me.pnlCompHeaderRuler)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlGrpFooterRuler1)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlHeaderRuler)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlGrpRuler1)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlFooterRuler)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlDetFooterRuler)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlDetailRuler)
        Me.pnlCanvasRuler.Location = New System.Drawing.Point(3, 2)
        Me.pnlCanvasRuler.Name = "pnlCanvasRuler"
        Me.pnlCanvasRuler.Size = New System.Drawing.Size(24, 700)
        Me.pnlCanvasRuler.TabIndex = 2
        '
        'pnlCompHeaderRuler
        '
        Me.pnlCompHeaderRuler.Location = New System.Drawing.Point(3, 24)
        Me.pnlCompHeaderRuler.Name = "pnlCompHeaderRuler"
        Me.pnlCompHeaderRuler.Size = New System.Drawing.Size(27, 91)
        Me.pnlCompHeaderRuler.TabIndex = 1
        '
        'pnlGrpFooterRuler1
        '
        Me.pnlGrpFooterRuler1.Location = New System.Drawing.Point(3, 406)
        Me.pnlGrpFooterRuler1.Name = "pnlGrpFooterRuler1"
        Me.pnlGrpFooterRuler1.Size = New System.Drawing.Size(27, 33)
        Me.pnlGrpFooterRuler1.TabIndex = 2
        '
        'pnlHeaderRuler
        '
        Me.pnlHeaderRuler.Location = New System.Drawing.Point(3, 139)
        Me.pnlHeaderRuler.Name = "pnlHeaderRuler"
        Me.pnlHeaderRuler.Size = New System.Drawing.Size(27, 72)
        Me.pnlHeaderRuler.TabIndex = 0
        '
        'pnlGrpRuler1
        '
        Me.pnlGrpRuler1.Location = New System.Drawing.Point(3, 234)
        Me.pnlGrpRuler1.Name = "pnlGrpRuler1"
        Me.pnlGrpRuler1.Size = New System.Drawing.Size(27, 32)
        Me.pnlGrpRuler1.TabIndex = 1
        '
        'pnlFooterRuler
        '
        Me.pnlFooterRuler.Location = New System.Drawing.Point(1, 529)
        Me.pnlFooterRuler.Name = "pnlFooterRuler"
        Me.pnlFooterRuler.Size = New System.Drawing.Size(27, 106)
        Me.pnlFooterRuler.TabIndex = 1
        '
        'pnlDetFooterRuler
        '
        Me.pnlDetFooterRuler.Location = New System.Drawing.Point(3, 460)
        Me.pnlDetFooterRuler.Name = "pnlDetFooterRuler"
        Me.pnlDetFooterRuler.Size = New System.Drawing.Size(27, 47)
        Me.pnlDetFooterRuler.TabIndex = 2
        '
        'pnlDetailRuler
        '
        Me.pnlDetailRuler.Location = New System.Drawing.Point(3, 290)
        Me.pnlDetailRuler.Name = "pnlDetailRuler"
        Me.pnlDetailRuler.Size = New System.Drawing.Size(27, 93)
        Me.pnlDetailRuler.TabIndex = 1
        '
        'pnlMainCanvas
        '
        Me.pnlMainCanvas.AutoScroll = True
        Me.pnlMainCanvas.BackColor = System.Drawing.Color.DimGray
        Me.pnlMainCanvas.Controls.Add(Me.pnlCanvas)
        Me.pnlMainCanvas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainCanvas.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMainCanvas.Location = New System.Drawing.Point(0, 23)
        Me.pnlMainCanvas.Name = "pnlMainCanvas"
        Me.pnlMainCanvas.Size = New System.Drawing.Size(594, 576)
        Me.pnlMainCanvas.TabIndex = 1
        '
        'pnlCanvas
        '
        Me.pnlCanvas.BackColor = System.Drawing.Color.Gray
        Me.pnlCanvas.Controls.Add(Me.spltFooter)
        Me.pnlCanvas.Controls.Add(Me.pnlFooter)
        Me.pnlCanvas.Controls.Add(Me.lblBottomFooter)
        Me.pnlCanvas.Controls.Add(Me.spltDetailFooter)
        Me.pnlCanvas.Controls.Add(Me.pnlDetailFooter)
        Me.pnlCanvas.Controls.Add(Me.lblFooter)
        Me.pnlCanvas.Controls.Add(Me.spltGroupFooter1)
        Me.pnlCanvas.Controls.Add(Me.pnlGroupFooter1)
        Me.pnlCanvas.Controls.Add(Me.lblGroupFooter1)
        Me.pnlCanvas.Controls.Add(Me.spltDetail)
        Me.pnlCanvas.Controls.Add(Me.pnlDetail)
        Me.pnlCanvas.Controls.Add(Me.lblDetail)
        Me.pnlCanvas.Controls.Add(Me.spltGoup1)
        Me.pnlCanvas.Controls.Add(Me.pnlGroup1)
        Me.pnlCanvas.Controls.Add(Me.lblGroup1)
        Me.pnlCanvas.Controls.Add(Me.spltHeader)
        Me.pnlCanvas.Controls.Add(Me.pnlHeader)
        Me.pnlCanvas.Controls.Add(Me.lblHeader)
        Me.pnlCanvas.Controls.Add(Me.spltCompHeader)
        Me.pnlCanvas.Controls.Add(Me.pnlCompHeader)
        Me.pnlCanvas.Controls.Add(Me.lblCompHeader)
        Me.pnlCanvas.Location = New System.Drawing.Point(-2, 0)
        Me.pnlCanvas.Name = "pnlCanvas"
        Me.pnlCanvas.Padding = New System.Windows.Forms.Padding(3)
        Me.pnlCanvas.Size = New System.Drawing.Size(614, 1000)
        Me.pnlCanvas.TabIndex = 0
        Me.pnlCanvas.Tag = "Canvas"
        '
        'spltFooter
        '
        Me.spltFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltFooter.Location = New System.Drawing.Point(3, 639)
        Me.spltFooter.MinExtra = 0
        Me.spltFooter.MinSize = 0
        Me.spltFooter.Name = "spltFooter"
        Me.spltFooter.Size = New System.Drawing.Size(608, 3)
        Me.spltFooter.TabIndex = 14
        Me.spltFooter.TabStop = False
        '
        'pnlFooter
        '
        Me.pnlFooter.AllowDrop = True
        Me.pnlFooter.BackColor = System.Drawing.Color.White
        Me.pnlFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlFooter.Location = New System.Drawing.Point(3, 534)
        Me.pnlFooter.Name = "pnlFooter"
        Me.pnlFooter.Size = New System.Drawing.Size(608, 105)
        Me.pnlFooter.TabIndex = 11
        '
        'lblBottomFooter
        '
        Me.lblBottomFooter.BackColor = System.Drawing.Color.Gainsboro
        Me.lblBottomFooter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBottomFooter.ContextMenuStrip = Me.cmslbl
        Me.lblBottomFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblBottomFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBottomFooter.Location = New System.Drawing.Point(3, 511)
        Me.lblBottomFooter.Name = "lblBottomFooter"
        Me.lblBottomFooter.Size = New System.Drawing.Size(608, 23)
        Me.lblBottomFooter.TabIndex = 10
        Me.lblBottomFooter.Text = "Footer"
        Me.lblBottomFooter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltDetailFooter
        '
        Me.spltDetailFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltDetailFooter.Location = New System.Drawing.Point(3, 508)
        Me.spltDetailFooter.MinExtra = 0
        Me.spltDetailFooter.MinSize = 0
        Me.spltDetailFooter.Name = "spltDetailFooter"
        Me.spltDetailFooter.Size = New System.Drawing.Size(608, 3)
        Me.spltDetailFooter.TabIndex = 11
        Me.spltDetailFooter.TabStop = False
        '
        'pnlDetailFooter
        '
        Me.pnlDetailFooter.AllowDrop = True
        Me.pnlDetailFooter.BackColor = System.Drawing.Color.White
        Me.pnlDetailFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlDetailFooter.Location = New System.Drawing.Point(3, 465)
        Me.pnlDetailFooter.Name = "pnlDetailFooter"
        Me.pnlDetailFooter.Size = New System.Drawing.Size(608, 43)
        Me.pnlDetailFooter.TabIndex = 9
        '
        'lblFooter
        '
        Me.lblFooter.BackColor = System.Drawing.Color.Gainsboro
        Me.lblFooter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFooter.ContextMenuStrip = Me.cmslbl
        Me.lblFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFooter.Location = New System.Drawing.Point(3, 442)
        Me.lblFooter.Name = "lblFooter"
        Me.lblFooter.Size = New System.Drawing.Size(608, 23)
        Me.lblFooter.TabIndex = 8
        Me.lblFooter.Text = "Detail Footer"
        Me.lblFooter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltGroupFooter1
        '
        Me.spltGroupFooter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltGroupFooter1.Location = New System.Drawing.Point(3, 440)
        Me.spltGroupFooter1.MinExtra = 0
        Me.spltGroupFooter1.MinSize = 0
        Me.spltGroupFooter1.Name = "spltGroupFooter1"
        Me.spltGroupFooter1.Size = New System.Drawing.Size(608, 2)
        Me.spltGroupFooter1.TabIndex = 20
        Me.spltGroupFooter1.TabStop = False
        '
        'pnlGroupFooter1
        '
        Me.pnlGroupFooter1.AllowDrop = True
        Me.pnlGroupFooter1.BackColor = System.Drawing.Color.White
        Me.pnlGroupFooter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlGroupFooter1.Location = New System.Drawing.Point(3, 410)
        Me.pnlGroupFooter1.Name = "pnlGroupFooter1"
        Me.pnlGroupFooter1.Size = New System.Drawing.Size(608, 30)
        Me.pnlGroupFooter1.TabIndex = 19
        '
        'lblGroupFooter1
        '
        Me.lblGroupFooter1.BackColor = System.Drawing.Color.Gainsboro
        Me.lblGroupFooter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGroupFooter1.ContextMenuStrip = Me.cmslbl
        Me.lblGroupFooter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblGroupFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroupFooter1.Location = New System.Drawing.Point(3, 387)
        Me.lblGroupFooter1.Name = "lblGroupFooter1"
        Me.lblGroupFooter1.Size = New System.Drawing.Size(608, 23)
        Me.lblGroupFooter1.TabIndex = 9
        Me.lblGroupFooter1.Text = "Group Footer 1"
        Me.lblGroupFooter1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltDetail
        '
        Me.spltDetail.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltDetail.Location = New System.Drawing.Point(3, 384)
        Me.spltDetail.MinExtra = 0
        Me.spltDetail.MinSize = 0
        Me.spltDetail.Name = "spltDetail"
        Me.spltDetail.Size = New System.Drawing.Size(608, 3)
        Me.spltDetail.TabIndex = 1
        Me.spltDetail.TabStop = False
        '
        'pnlDetail
        '
        Me.pnlDetail.AllowDrop = True
        Me.pnlDetail.BackColor = System.Drawing.Color.White
        Me.pnlDetail.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlDetail.Location = New System.Drawing.Point(3, 294)
        Me.pnlDetail.Name = "pnlDetail"
        Me.pnlDetail.Size = New System.Drawing.Size(608, 90)
        Me.pnlDetail.TabIndex = 7
        '
        'lblDetail
        '
        Me.lblDetail.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDetail.ContextMenuStrip = Me.cmslbl
        Me.lblDetail.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetail.Location = New System.Drawing.Point(3, 271)
        Me.lblDetail.Name = "lblDetail"
        Me.lblDetail.Size = New System.Drawing.Size(608, 23)
        Me.lblDetail.TabIndex = 4
        Me.lblDetail.Text = "Detail"
        Me.lblDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltGoup1
        '
        Me.spltGoup1.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltGoup1.Location = New System.Drawing.Point(3, 269)
        Me.spltGoup1.MinExtra = 0
        Me.spltGoup1.MinSize = 0
        Me.spltGoup1.Name = "spltGoup1"
        Me.spltGoup1.Size = New System.Drawing.Size(608, 2)
        Me.spltGoup1.TabIndex = 18
        Me.spltGoup1.TabStop = False
        '
        'pnlGroup1
        '
        Me.pnlGroup1.AllowDrop = True
        Me.pnlGroup1.BackColor = System.Drawing.Color.White
        Me.pnlGroup1.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlGroup1.Location = New System.Drawing.Point(3, 237)
        Me.pnlGroup1.Name = "pnlGroup1"
        Me.pnlGroup1.Size = New System.Drawing.Size(608, 32)
        Me.pnlGroup1.TabIndex = 17
        '
        'lblGroup1
        '
        Me.lblGroup1.BackColor = System.Drawing.Color.Gainsboro
        Me.lblGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGroup1.ContextMenuStrip = Me.cmslbl
        Me.lblGroup1.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblGroup1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroup1.Location = New System.Drawing.Point(3, 214)
        Me.lblGroup1.Name = "lblGroup1"
        Me.lblGroup1.Size = New System.Drawing.Size(608, 23)
        Me.lblGroup1.TabIndex = 9
        Me.lblGroup1.Text = "Group 1"
        Me.lblGroup1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltHeader
        '
        Me.spltHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltHeader.Location = New System.Drawing.Point(3, 212)
        Me.spltHeader.MinExtra = 30
        Me.spltHeader.MinSize = 0
        Me.spltHeader.Name = "spltHeader"
        Me.spltHeader.Size = New System.Drawing.Size(608, 2)
        Me.spltHeader.TabIndex = 0
        Me.spltHeader.TabStop = False
        '
        'pnlHeader
        '
        Me.pnlHeader.AllowDrop = True
        Me.pnlHeader.BackColor = System.Drawing.Color.White
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.Location = New System.Drawing.Point(3, 142)
        Me.pnlHeader.Margin = New System.Windows.Forms.Padding(10)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(608, 70)
        Me.pnlHeader.TabIndex = 0
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Gainsboro
        Me.lblHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeader.ContextMenuStrip = Me.cmslbl
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.Location = New System.Drawing.Point(3, 119)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(608, 23)
        Me.lblHeader.TabIndex = 3
        Me.lblHeader.Text = "Header"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltCompHeader
        '
        Me.spltCompHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltCompHeader.Location = New System.Drawing.Point(3, 116)
        Me.spltCompHeader.MinExtra = 0
        Me.spltCompHeader.MinSize = 0
        Me.spltCompHeader.Name = "spltCompHeader"
        Me.spltCompHeader.Size = New System.Drawing.Size(608, 3)
        Me.spltCompHeader.TabIndex = 23
        Me.spltCompHeader.TabStop = False
        '
        'pnlCompHeader
        '
        Me.pnlCompHeader.AllowDrop = True
        Me.pnlCompHeader.BackColor = System.Drawing.Color.White
        Me.pnlCompHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlCompHeader.Location = New System.Drawing.Point(3, 26)
        Me.pnlCompHeader.Margin = New System.Windows.Forms.Padding(10)
        Me.pnlCompHeader.Name = "pnlCompHeader"
        Me.pnlCompHeader.Size = New System.Drawing.Size(608, 90)
        Me.pnlCompHeader.TabIndex = 21
        '
        'lblCompHeader
        '
        Me.lblCompHeader.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCompHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCompHeader.ContextMenuStrip = Me.cmslbl
        Me.lblCompHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCompHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompHeader.Location = New System.Drawing.Point(3, 3)
        Me.lblCompHeader.Name = "lblCompHeader"
        Me.lblCompHeader.Size = New System.Drawing.Size(608, 23)
        Me.lblCompHeader.TabIndex = 22
        Me.lblCompHeader.Text = "Company Header"
        Me.lblCompHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlRuler
        '
        Me.pnlRuler.BackColor = System.Drawing.Color.Transparent
        Me.pnlRuler.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlRuler.Location = New System.Drawing.Point(0, 0)
        Me.pnlRuler.Name = "pnlRuler"
        Me.pnlRuler.Size = New System.Drawing.Size(594, 23)
        Me.pnlRuler.TabIndex = 30
        '
        'objbtnToggleToolbox
        '
        Me.objbtnToggleToolbox.BackColor = System.Drawing.Color.DimGray
        Me.objbtnToggleToolbox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.objbtnToggleToolbox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objbtnToggleToolbox.FlatAppearance.BorderSize = 0
        Me.objbtnToggleToolbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnToggleToolbox.ForeColor = System.Drawing.Color.Black
        Me.objbtnToggleToolbox.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.LeftArrow_10
        Me.objbtnToggleToolbox.Location = New System.Drawing.Point(240, 3)
        Me.objbtnToggleToolbox.Name = "objbtnToggleToolbox"
        Me.objbtnToggleToolbox.Size = New System.Drawing.Size(11, 601)
        Me.objbtnToggleToolbox.TabIndex = 2
        Me.objbtnToggleToolbox.UseVisualStyleBackColor = False
        '
        'objbtnToggleProperty
        '
        Me.objbtnToggleProperty.BackColor = System.Drawing.Color.DimGray
        Me.objbtnToggleProperty.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.objbtnToggleProperty.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objbtnToggleProperty.FlatAppearance.BorderSize = 0
        Me.objbtnToggleProperty.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnToggleProperty.ForeColor = System.Drawing.Color.Black
        Me.objbtnToggleProperty.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.RightArrow_10
        Me.objbtnToggleProperty.Location = New System.Drawing.Point(894, 3)
        Me.objbtnToggleProperty.Name = "objbtnToggleProperty"
        Me.objbtnToggleProperty.Size = New System.Drawing.Size(11, 601)
        Me.objbtnToggleProperty.TabIndex = 1
        Me.objbtnToggleProperty.UseVisualStyleBackColor = False
        '
        'tblToolbox
        '
        Me.tblToolbox.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.tblToolbox.ColumnCount = 1
        Me.tblToolbox.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblToolbox.Controls.Add(Me.tvControls, 0, 1)
        Me.tblToolbox.Controls.Add(Me.lblPrintingTools, 0, 0)
        Me.tblToolbox.Controls.Add(Me.lblNote, 0, 2)
        Me.tblToolbox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblToolbox.Location = New System.Drawing.Point(3, 3)
        Me.tblToolbox.Name = "tblToolbox"
        Me.tblToolbox.RowCount = 3
        Me.tblToolbox.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.tblToolbox.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblToolbox.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblToolbox.Size = New System.Drawing.Size(231, 601)
        Me.tblToolbox.TabIndex = 80
        '
        'tvControls
        '
        Me.tvControls.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tvControls.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvControls.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvControls.FullRowSelect = True
        Me.tvControls.Location = New System.Drawing.Point(3, 28)
        Me.tvControls.Margin = New System.Windows.Forms.Padding(0)
        Me.tvControls.Name = "tvControls"
        Me.tvControls.ShowNodeToolTips = True
        Me.tvControls.Size = New System.Drawing.Size(225, 547)
        Me.tvControls.TabIndex = 9
        '
        'lblPrintingTools
        '
        Me.lblPrintingTools.BackColor = System.Drawing.Color.Silver
        Me.lblPrintingTools.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblPrintingTools.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrintingTools.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPrintingTools.Location = New System.Drawing.Point(3, 3)
        Me.lblPrintingTools.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPrintingTools.Name = "lblPrintingTools"
        Me.lblPrintingTools.Size = New System.Drawing.Size(225, 22)
        Me.lblPrintingTools.TabIndex = 8
        Me.lblPrintingTools.Text = "Tool Box"
        Me.lblPrintingTools.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNote
        '
        Me.lblNote.AutoSize = True
        Me.lblNote.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote.Location = New System.Drawing.Point(3, 578)
        Me.lblNote.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(225, 20)
        Me.lblNote.TabIndex = 10
        Me.lblNote.Text = "* Double Click the Fields to create formula"
        Me.lblNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tsBottomBar
        '
        Me.tsBottomBar.BackColor = System.Drawing.Color.White
        Me.tsBottomBar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsBottomBar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsBottomBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsBottomBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmilblTemplateName, Me.tsmilblName, Me.ToolStripLabel1, Me.tslblDelete, Me.objtsmi1, Me.tslblSelectAll, Me.objtsmi2, Me.tslblUnSelect})
        Me.tsBottomBar.Location = New System.Drawing.Point(0, 660)
        Me.tsBottomBar.Name = "tsBottomBar"
        Me.tsBottomBar.Size = New System.Drawing.Size(1142, 25)
        Me.tsBottomBar.TabIndex = 5
        '
        'tsmilblTemplateName
        '
        Me.tsmilblTemplateName.Name = "tsmilblTemplateName"
        Me.tsmilblTemplateName.Size = New System.Drawing.Size(67, 22)
        Me.tsmilblTemplateName.Text = "Template :"
        '
        'tsmilblName
        '
        Me.tsmilblName.Name = "tsmilblName"
        Me.tsmilblName.Size = New System.Drawing.Size(16, 22)
        Me.tsmilblName.Text = "#"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(0, 22)
        '
        'tslblDelete
        '
        Me.tslblDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.tslblDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tslblDelete.Name = "tslblDelete"
        Me.tslblDelete.Size = New System.Drawing.Size(149, 22)
        Me.tslblDelete.Text = "'Shift + Delete'  = Delete "
        '
        'objtsmi1
        '
        Me.objtsmi1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.objtsmi1.Name = "objtsmi1"
        Me.objtsmi1.Size = New System.Drawing.Size(6, 25)
        '
        'tslblSelectAll
        '
        Me.tslblSelectAll.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.tslblSelectAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tslblSelectAll.Name = "tslblSelectAll"
        Me.tslblSelectAll.Size = New System.Drawing.Size(126, 22)
        Me.tslblSelectAll.Text = "'Ctrl + A'  = Select All"
        '
        'objtsmi2
        '
        Me.objtsmi2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.objtsmi2.Name = "objtsmi2"
        Me.objtsmi2.Size = New System.Drawing.Size(6, 25)
        '
        'tslblUnSelect
        '
        Me.tslblUnSelect.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.tslblUnSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tslblUnSelect.Name = "tslblUnSelect"
        Me.tslblUnSelect.Size = New System.Drawing.Size(123, 22)
        Me.tslblUnSelect.Text = "'Ctrl + U'  = Unselect"
        '
        'cmsFields
        '
        Me.cmsFields.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewFormulaToolStripMenuItem, Me.EditFormulaToolStripMenuItem, Me.DeleteFormulaToolStripMenuItem})
        Me.cmsFields.Name = "cmsFields"
        Me.cmsFields.Size = New System.Drawing.Size(155, 70)
        '
        'NewFormulaToolStripMenuItem
        '
        Me.NewFormulaToolStripMenuItem.Name = "NewFormulaToolStripMenuItem"
        Me.NewFormulaToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.NewFormulaToolStripMenuItem.Text = "New Formula"
        '
        'EditFormulaToolStripMenuItem
        '
        Me.EditFormulaToolStripMenuItem.Name = "EditFormulaToolStripMenuItem"
        Me.EditFormulaToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.EditFormulaToolStripMenuItem.Text = "Edit Formula"
        '
        'DeleteFormulaToolStripMenuItem
        '
        Me.DeleteFormulaToolStripMenuItem.Name = "DeleteFormulaToolStripMenuItem"
        Me.DeleteFormulaToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.DeleteFormulaToolStripMenuItem.Text = "Delete Formula"
        '
        'frmReportDesigner_New
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1142, 685)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.pnlTop)
        Me.Controls.Add(Me.tsBottomBar)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.Name = "frmReportDesigner_New"
        Me.ShowInTaskbar = False
        Me.Text = "Report Designer"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnlTop.ResumeLayout(False)
        Me.pnlTop.PerformLayout()
        Me.fpnlTop.ResumeLayout(False)
        Me.fpnlTop.PerformLayout()
        Me.tsFormattingBar.ResumeLayout(False)
        Me.tsFormattingBar.PerformLayout()
        Me.tsOtherSettings.ResumeLayout(False)
        Me.tsOtherSettings.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.btnConverter.ResumeLayout(False)
        Me.btnConverter.PerformLayout()
        Me.tsMainBar.ResumeLayout(False)
        Me.tsMainBar.PerformLayout()
        Me.cms.ResumeLayout(False)
        Me.cmslbl.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.tblMain.ResumeLayout(False)
        Me.tblProperty.ResumeLayout(False)
        Me.pnlPrint.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.pnlVScroll.ResumeLayout(False)
        Me.pnlCanvasRuler.ResumeLayout(False)
        Me.pnlMainCanvas.ResumeLayout(False)
        Me.pnlCanvas.ResumeLayout(False)
        Me.tblToolbox.ResumeLayout(False)
        Me.tblToolbox.PerformLayout()
        Me.tsBottomBar.ResumeLayout(False)
        Me.tsBottomBar.PerformLayout()
        Me.cmsFields.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents chkSaveAsNew As System.Windows.Forms.CheckBox
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents btnConverter As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents WordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExeclToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PdfToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HTMLToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblXML As System.Windows.Forms.Label
    Friend WithEvents txtMstXMLPath As System.Windows.Forms.TextBox
    Friend WithEvents btnMstPath As System.Windows.Forms.Button
    Friend WithEvents txtDetXMLPath As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cms As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cms_Copy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cms_Paste As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cms_Delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ilToolBarImage As System.Windows.Forms.ImageList
    Friend WithEvents oPrintDoc As System.Drawing.Printing.PrintDocument
    Friend WithEvents cmslbl As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmSuppress As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_PageSetup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsFormattingBar As System.Windows.Forms.ToolStrip
    Friend WithEvents objtsmiSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents edit_Delete_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents format_Align_Left As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents format_Valign_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents format_Align_Right_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents format_Align_Top_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents format_Halign_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents format_Align_Bottom_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents format_Same_Width_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents format_Same_Height_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents format_Same_Size_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents edit_Cut_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents edit_Copy_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents edit_Paste_Button As System.Windows.Forms.ToolStripButton
    Friend WithEvents fpnlTop As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents tsMainBar As System.Windows.Forms.ToolStrip
    Friend WithEvents btnSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmi14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnPreview As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnExit As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsOtherSettings As System.Windows.Forms.ToolStrip
    Friend WithEvents chkRepeatHeader As System.Windows.Forms.ToolStripButton
    Friend WithEvents chkShowGrid As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsmibtnOrientation As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents chkPortrait As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkLandscape As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkShowPageWise As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsmilblPaperWidth As System.Windows.Forms.ToolStripLabel
    Friend WithEvents txtCanvasWidth As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents tsmilblPagesPerSheet As System.Windows.Forms.ToolStripLabel
    Friend WithEvents txtMultiPrint As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents objtsmiSep13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtsmiSep19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents chkSnapToGrid As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents tsBottomBar As System.Windows.Forms.ToolStrip
    Friend WithEvents tslblDelete As System.Windows.Forms.ToolStripLabel
    Friend WithEvents objtsmi1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tslblSelectAll As System.Windows.Forms.ToolStripLabel
    Friend WithEvents objtsmi2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tslblUnSelect As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tblMain As System.Windows.Forms.TableLayoutPanel
    ' Friend WithEvents gbToolBox As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnToggleProperty As System.Windows.Forms.Button
    Friend WithEvents objbtnToggleToolbox As System.Windows.Forms.Button
    ' Friend WithEvents gbProperty As eZee.Common.eZeeCollapsibleContainer
    'Public WithEvents pgTemplateField As ArutiReport.Engine.Library.TemplatePropertyGrid
    Friend WithEvents pnlPrint As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents pnlVScroll As System.Windows.Forms.Panel
    Friend WithEvents pnlCanvasRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlCompHeaderRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlGrpFooterRuler1 As System.Windows.Forms.Panel
    Friend WithEvents pnlHeaderRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlGrpRuler1 As System.Windows.Forms.Panel
    Friend WithEvents pnlFooterRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlDetFooterRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlDetailRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlMainCanvas As System.Windows.Forms.Panel
    Friend WithEvents pnlCanvas As System.Windows.Forms.Panel
    Friend WithEvents spltFooter As System.Windows.Forms.Splitter
    Friend WithEvents pnlFooter As System.Windows.Forms.Panel
    Friend WithEvents lblBottomFooter As System.Windows.Forms.Label
    Friend WithEvents spltDetailFooter As System.Windows.Forms.Splitter
    Friend WithEvents pnlDetailFooter As System.Windows.Forms.Panel
    Friend WithEvents lblFooter As System.Windows.Forms.Label
    Friend WithEvents spltGroupFooter1 As System.Windows.Forms.Splitter
    Friend WithEvents pnlGroupFooter1 As System.Windows.Forms.Panel
    Friend WithEvents lblGroupFooter1 As System.Windows.Forms.Label
    Friend WithEvents spltDetail As System.Windows.Forms.Splitter
    Friend WithEvents pnlDetail As System.Windows.Forms.Panel
    Friend WithEvents lblDetail As System.Windows.Forms.Label
    Friend WithEvents spltGoup1 As System.Windows.Forms.Splitter
    Friend WithEvents pnlGroup1 As System.Windows.Forms.Panel
    Friend WithEvents lblGroup1 As System.Windows.Forms.Label
    Friend WithEvents spltHeader As System.Windows.Forms.Splitter
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents spltCompHeader As System.Windows.Forms.Splitter
    Friend WithEvents pnlCompHeader As System.Windows.Forms.Panel
    Friend WithEvents lblCompHeader As System.Windows.Forms.Label
    Friend WithEvents pnlRuler As System.Windows.Forms.Panel
    Public WithEvents pgTemplateField As ArutiReport.Engine.Library.TemplatePropertyGrid
    Friend WithEvents tblProperty As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tblToolbox As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblPrintingTools As System.Windows.Forms.Label
    Friend WithEvents objlblProperty As System.Windows.Forms.Label
    Friend WithEvents cboField As System.Windows.Forms.ComboBox
    Friend WithEvents tvControls As System.Windows.Forms.TreeView
    Friend WithEvents cmsFields As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents NewFormulaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditFormulaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteFormulaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents btnPageSetUp As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents txtItemPrint As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents tsmilblTemplateName As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tsmilblName As System.Windows.Forms.ToolStripLabel
End Class
