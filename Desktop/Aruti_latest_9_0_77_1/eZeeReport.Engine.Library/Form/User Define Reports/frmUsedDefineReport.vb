﻿#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmUsedDefineReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmUsedDefineReport"
    Private mintUserDefineReportId As Integer
    Private objUDR As clsUserDefineReport
    Private mstrTemplateName As String = String.Empty
    Private mStrDatabase As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New(ByVal intUDReportUnkid As Integer)
        InitializeComponent()
        mintUserDefineReportId = intUDReportUnkid
    End Sub

    Public Sub New(ByVal StrTemplateName As String)
        InitializeComponent()
        mstrTemplateName = StrTemplateName
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmUsedDefineReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objUDR = New clsUserDefineReport
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objUDR._Reportunkid = mintUserDefineReportId
            'eZeeHeader.Title = objUDR._Template_Name
            'rtbQry.Text = objUDR._Template_Query
            'rtbQry.Tag = mintUserDefineReportId

            eZeeHeader.Title = mstrTemplateName
            Dim dsList As New DataSet
            dsList = objUDR.GetList("List", True, -1, mstrTemplateName)

            If dsList.Tables("List").Rows.Count > 0 Then
                rtbQry.Text = dsList.Tables("List").Rows(0).Item("template_query")
                rtbQry.Tag = dsList.Tables("List").Rows(0).Item("reportunkid")
                mStrDatabase = dsList.Tables("List").Rows(0).Item("database_name")
            End If
            Call SetVisibility()
            'S.SANDEEP [ 19 JUNE 2012 ] -- END
            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUsedDefineReport_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Event's "

    Private Sub mnuSavQViewReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSavQViewReport.Click
        Try
            If IsValidData() = False Then Exit Sub


            Dim ds As New DataSet
            Dim StrQ As String = ""
            StrQ = "Select 1 AS id" & vbCrLf
            StrQ &= rtbQry.Text
            If mStrDatabase.ToString.ToUpper = "HRMSCONFIGURATION" Then
                eZeeDatabase.change_database(mStrDatabase)
            End If
            ds = objUDR.Execute_Report_Query(StrQ)

            If ds Is Nothing Then Exit Sub


            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objUDR._Reportunkid = mintUserDefineReportId
            objUDR._Reportunkid = rtbQry.Tag
            'S.SANDEEP [ 19 JUNE 2012 ] -- END
            objUDR._Template_Name = objUDR._Template_Name
            objUDR._Template_Query = rtbQry.Text
            objUDR._Isactive = True
            If objUDR.Update() Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Query Successfully Saved. Generating Report Please Wait."), enMsgBoxStyle.Information)
                Me.Cursor = Cursors.WaitCursor
                Call Display_Report()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSavQViewReport_Click", mstrModuleName)
        Finally
            If mStrDatabase.ToString.ToUpper = "HRMSCONFIGURATION" Then
                eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            End If
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub mnuViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuViewReport.Click
        Try
            If IsValidData() = False Then Exit Sub
            Call Display_Report()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewReport_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Display_Report()
        Try
            Dim objReport As New ArutiReport.Engine.Library.frmReportDesigner_New
            Dim ds As New DataSet
            Dim StrQ As String = ""
            StrQ = "Select 1 AS id" & vbCrLf
            StrQ &= rtbQry.Text
            If mStrDatabase.ToString.ToUpper = "HRMSCONFIGURATION" Then
                eZeeDatabase.change_database(mStrDatabase)
            End If
            ds = objUDR.Execute_Report_Query(StrQ)

            If ds Is Nothing Then Exit Sub

            If ds.Tables(0).Columns.Count > 0 Then
                If Not IO.Directory.Exists(AppSettings._Object._ApplicationPath + "DataXML") Then
                    IO.Directory.CreateDirectory(AppSettings._Object._ApplicationPath + "DataXML")
                End If
                ds.WriteXml(AppSettings._Object._ApplicationPath + "DataXML\" + mstrTemplateName, XmlWriteMode.WriteSchema)
            End If
            'objReport.MdiParent = Me '.MdiParent
            objReport.ShowMeNow(mstrTemplateName, mstrTemplateName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Display_Report", mstrModuleName)
        Finally
            If mStrDatabase.ToString.ToUpper = "HRMSCONFIGURATION" Then
                eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            End If
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If User._Object.Privilege._AllowToEditQueries = True Then
                rtbQry.ReadOnly = False
            Else
                rtbQry.ReadOnly = True
            End If
            mnuSavQViewReport.Enabled = User._Object.Privilege._AllowToEditQueries
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            If rtbQry.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Query is mandatory information. Please provide Query to Continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If rtbQry.Text.Trim.ToUpper.Contains("INSERT ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("DELETE ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("UPDATE ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("TRUNCATE ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("CREATE ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("ALTER ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("DROP ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("INTO ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("EXEC ") Or _
               rtbQry.Text.Trim.ToUpper.Contains("EXECUTE ") Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, you cannot change data. Please write only proper query."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

End Class