﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsedDefineReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUsedDefineReport))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnReportOperation = New eZee.Common.eZeeSplitButton
        Me.mnuReportOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuViewReport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSavQViewReport = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeLightButton(Me.components)
        Me.rtbQry = New System.Windows.Forms.RichTextBox
        Me.pnlQryData = New System.Windows.Forms.Panel
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuExportToPDF = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSavQExPDF = New System.Windows.Forms.ToolStripMenuItem
        Me.objFooter.SuspendLayout()
        Me.mnuReportOperation.SuspendLayout()
        Me.pnlQryData.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(681, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = ""
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnReportOperation)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.objbtnSort)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 455)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(681, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnReportOperation
        '
        Me.btnReportOperation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReportOperation.BorderColor = System.Drawing.Color.Black
        Me.btnReportOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReportOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReportOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnReportOperation.Location = New System.Drawing.Point(431, 13)
        Me.btnReportOperation.Name = "btnReportOperation"
        Me.btnReportOperation.ShowDefaultBorderColor = True
        Me.btnReportOperation.Size = New System.Drawing.Size(132, 30)
        Me.btnReportOperation.SplitButtonMenu = Me.mnuReportOperation
        Me.btnReportOperation.TabIndex = 4
        Me.btnReportOperation.Text = "Report Operation"
        '
        'mnuReportOperation
        '
        Me.mnuReportOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuViewReport, Me.mnuSavQViewReport, Me.objSep1, Me.mnuExportToPDF, Me.mnuSavQExPDF})
        Me.mnuReportOperation.Name = "mnuReportOperation"
        Me.mnuReportOperation.Size = New System.Drawing.Size(224, 98)
        '
        'mnuViewReport
        '
        Me.mnuViewReport.Name = "mnuViewReport"
        Me.mnuViewReport.Size = New System.Drawing.Size(223, 22)
        Me.mnuViewReport.Tag = "mnuViewReport"
        Me.mnuViewReport.Text = "&View Report"
        '
        'mnuSavQViewReport
        '
        Me.mnuSavQViewReport.Name = "mnuSavQViewReport"
        Me.mnuSavQViewReport.Size = New System.Drawing.Size(223, 22)
        Me.mnuSavQViewReport.Tag = "mnuSavQViewReport"
        Me.mnuSavQViewReport.Text = "&Save Query && View Report"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(569, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(100, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.White
        Me.objbtnSort.BackgroundImage = CType(resources.GetObject("objbtnSort.BackgroundImage"), System.Drawing.Image)
        Me.objbtnSort.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnSort.BorderColor = System.Drawing.Color.Empty
        Me.objbtnSort.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnSort.FlatAppearance.BorderSize = 0
        Me.objbtnSort.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.ForeColor = System.Drawing.Color.Black
        Me.objbtnSort.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnSort.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnSort.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSort.Location = New System.Drawing.Point(12, 13)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSort.Size = New System.Drawing.Size(30, 30)
        Me.objbtnSort.TabIndex = 0
        Me.objbtnSort.UseVisualStyleBackColor = False
        '
        'rtbQry
        '
        Me.rtbQry.AcceptsTab = True
        Me.rtbQry.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbQry.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbQry.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtbQry.Location = New System.Drawing.Point(0, 0)
        Me.rtbQry.Name = "rtbQry"
        Me.rtbQry.Size = New System.Drawing.Size(679, 393)
        Me.rtbQry.TabIndex = 5
        Me.rtbQry.Text = ""
        '
        'pnlQryData
        '
        Me.pnlQryData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlQryData.Controls.Add(Me.rtbQry)
        Me.pnlQryData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlQryData.Location = New System.Drawing.Point(0, 60)
        Me.pnlQryData.Name = "pnlQryData"
        Me.pnlQryData.Size = New System.Drawing.Size(681, 395)
        Me.pnlQryData.TabIndex = 6
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(220, 6)
        Me.objSep1.Visible = False
        '
        'mnuExportToPDF
        '
        Me.mnuExportToPDF.Name = "mnuExportToPDF"
        Me.mnuExportToPDF.Size = New System.Drawing.Size(223, 22)
        Me.mnuExportToPDF.Tag = "mnuExportToPDF"
        Me.mnuExportToPDF.Text = "&Export To PDF"
        Me.mnuExportToPDF.Visible = False
        '
        'mnuSavQExPDF
        '
        Me.mnuSavQExPDF.Name = "mnuSavQExPDF"
        Me.mnuSavQExPDF.Size = New System.Drawing.Size(223, 22)
        Me.mnuSavQExPDF.Tag = "mnuSavQExPDF"
        Me.mnuSavQExPDF.Text = "Save &Query && Export To PDF"
        Me.mnuSavQExPDF.Visible = False
        '
        'frmUsedDefineReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(681, 510)
        Me.Controls.Add(Me.pnlQryData)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmUsedDefineReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.objFooter.ResumeLayout(False)
        Me.mnuReportOperation.ResumeLayout(False)
        Me.pnlQryData.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSort As eZee.Common.eZeeLightButton
    Friend WithEvents rtbQry As System.Windows.Forms.RichTextBox
    Friend WithEvents pnlQryData As System.Windows.Forms.Panel
    Friend WithEvents btnReportOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuReportOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuViewReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSavQViewReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExportToPDF As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSavQExPDF As System.Windows.Forms.ToolStripMenuItem
End Class
