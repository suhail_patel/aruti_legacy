﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Imports System.Drawing.Drawing2D

'Public Delegate Sub MovedResizedImageEventHandler(ByVal sender As Object, ByVal e As EventArgs)
'Public Delegate Sub SelectedImageEventHandler(ByVal sender As Object, ByVal e As EventArgs)

#Region "With Tracker and Sizer"
Public Class ImageControls

    Inherits UserControl



#Region "Events Declaration"
    Public Event MovedResized As MovedResizedImageEventHandler
    Public Event Selected As SelectedImageEventHandler

    Public Delegate Sub TrackingEventHandler(ByVal sender As Object, ByVal e As TrackingEventArgs)
    Public Event Tracking As TrackingEventHandler
#End Region

#Region "Enum"
    Public Enum DragMode
        Move = 0
        ResizeBottom = 6
        ResizeLeft = 8
        ResizeLowerLeft = 7
        ResizeLowerRight = 5
        ResizeRight = 4
        ResizeTop = 2
        ResizeUpperLeft = 1
        ResizeUpperRight = 3
    End Enum

    Public Enum TrackerMode
        NONE
        CENTRE
        W
        E
        N
        S
        NE
        NW
        SE
        SW
    End Enum
    <FlagsAttribute()> _
    Public Enum TrackerFlag As UShort
        NONE = &H0
        ALL = &HFFFF
        CENTRE = &H1
        W = &H2
        E = &H4
        N = &H8
        S = &H10
    End Enum
#End Region

#Region "Variables"
    Private _bottomRect As Rectangle
    Private _dragMode As DragMode
    Private _horizontalMid As Single
    Private _isDragging As Boolean
    Private _isSelected As Boolean
    Private _lastMouseX As Integer
    Private _lastMouseY As Integer
    Private _leftRect As Rectangle
    Private _lowerLeftRect As Rectangle
    Private _lowerRightRect As Rectangle
    Private _minHeight As Integer = 10
    Private _minWidth As Integer = 10
    Private _resizedRect As Rectangle
    Private _rightRect As Rectangle
    Private _screenRect As Rectangle
    Private _sizerRectangles As ArrayList
    Private _sizerWidth As Integer = 6
    Private _topRect As Rectangle
    Private _upperLeftRect As Rectangle
    Private _upperRightRect As Rectangle
    Private _verticalMid As Single
    Private _textAlign As ContentAlignment
    Private _formatSting As StringFormat

    'Public blnSnaptoGrid As Boolean
    Public _lineThickness As Integer = 1
    Public _lineStyle As System.Drawing.Drawing2D.DashStyle
    Public _lineColor As Color = Color.Black

    Public intUpperLeftR As Integer = 1
    Public intUpperRightR As Integer = 1
    Public intLowerLeftR As Integer = 1
    Public intLowerRightR As Integer = 1
    Public intAllR As Integer = 5

    Public intFieldType As Integer

    Private rtrack_Region As Region
    Private Const intTracker_Size As Integer = 2
    Private Const intTracker_Hist As Integer = 4
    Private Const intTracker_HitSize As Integer = intTracker_Size + 1
    Public Const intMinSizeX As Integer = 10
    Public Const intMinSizeY As Integer = 10
    Private ptLastTrackPoint As Point = New Point(0, 0)
    Protected enTrackMode As TrackerMode = TrackerMode.NONE
    Protected enTrackFlag As TrackerFlag = TrackerFlag.ALL

#End Region

#Region "Properties"
    <Description("Minimum height of resizable control"), Category("Appearance")> _
       Public Property MinHeight() As Integer
        Get
            Return Me._minHeight
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "MinHeight", (value > 0))
            Me._minHeight = value
        End Set
    End Property

    <Description("Minimum width of resizable control"), Category("Appearance")> _
    Public Property MinWidth() As Integer
        Get
            Return Me._minWidth
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "MinWidth", (value > 0))
            Me._minWidth = value
        End Set
    End Property

    <Description("Width of resize handles"), Category("Appearance")> _
    Public Property SizerWidth() As Integer
        Get
            Return Me._sizerWidth
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "SizerWidth", (value > 0))
            Me._sizerWidth = value
            Me.UpdateSizers()
        End Set
    End Property

    <Browsable(True), DefaultValue("Text"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)> _
    Public Overrides Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set(ByVal value As String)
            MyBase.Text = value
            Me.Invalidate()
        End Set
    End Property
    Dim Texts As String = "Image"
    Public ReadOnly Property NamesText() As String
        Get
            Return Texts
        End Get
    End Property


    Private _FilePath As Image
    Public Property FilePath() As Image
        Get
            Return _FilePath
        End Get
        Set(ByVal value As Image)
            _FilePath = value
            Me.BackgroundImage = value
            'pbSample.Image = value
        End Set
    End Property

    <DefaultValue(&H10), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)> _
    Public Property TextAlign() As ContentAlignment
        Get
            Return Me._textAlign
        End Get
        Set(ByVal value As ContentAlignment)
            Me._textAlign = value
            Select Case Me._textAlign
                Case ContentAlignment.TopLeft
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.TopCenter
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.TopRight
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Far

                Case ContentAlignment.MiddleLeft
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.MiddleCenter
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.MiddleRight
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Far

                Case ContentAlignment.BottomLeft
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.BottomCenter
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.BottomRight
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Far
            End Select
            Me.Invalidate()
        End Set
    End Property

    <Description("Line Style"), Category("Appearance")> _
    Public Property LineStyle() As System.Drawing.Drawing2D.DashStyle
        Get
            Return Me._lineStyle
        End Get
        Set(ByVal value As System.Drawing.Drawing2D.DashStyle)
            Me._lineStyle = value
            'Invalidate()
            Me.Refresh()
        End Set
    End Property

    <Description("Line Thickness"), Category("Appearance")> _
       Public Property LineThickness() As Integer
        Get
            Return Me._lineThickness
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "MinHeight", (value > 0))
            If value > 6 Then
                value = 6
                Me._lineThickness = value
            Else
                Me._lineThickness = value
            End If
        End Set
    End Property

    <Description("Line Color"), Category("Appearance")> _
  Public Property LineColor() As Color
        Get
            Return Me._lineColor
        End Get
        Set(ByVal value As Color)
            Me._lineColor = value
            'Invalidate()
            Me.Refresh()
        End Set
    End Property

    Public Property UpperLeftR() As Integer
        Get
            Return intUpperLeftR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            'If Value > Me.Height Then Value = Me.Height
            intUpperLeftR = value
            Me.Invalidate()
        End Set
    End Property

    Public Property UpperRightR() As Integer
        Get
            Return intUpperRightR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            'If Value > Me.Height Then Value = Me.Height
            intUpperRightR = value
            Me.Invalidate()
        End Set
    End Property

    Public Property LowerLeftR() As Integer
        Get
            Return intLowerLeftR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            'If Value > Me.Height Then Value = Me.Height
            intLowerLeftR = value
            Me.Invalidate()
        End Set
    End Property

    Public Property LowerRightR() As Integer
        Get
            Return intLowerRightR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            'If Value > Me.Height Then Value = Me.Height
            intLowerRightR = value
            Me.Invalidate()
        End Set
    End Property

    Public Property AllR() As Integer
        Get
            Return intAllR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            'If Value > Me.Height Then Value = Me.Height
            intAllR = value
            intUpperLeftR = value
            intUpperRightR = value
            intLowerLeftR = value
            intLowerRightR = value
            Me.Invalidate()
        End Set

    End Property

    Public Property FieldType() As Integer
        Get
            Return intFieldType
        End Get
        Set(ByVal value As Integer)
            intFieldType = value
        End Set

    End Property

    'Public Property _SnaptoGrid() As Boolean
    '    Get
    '        Return blnSnaptoGrid
    '    End Get
    '    Set(ByVal value As Boolean)
    '        blnSnaptoGrid = value
    '    End Set
    'End Property
    'Vimal (16 Mar 2011) -- End

    Public Property Selectede() As Boolean
        Get
            Return Me._isSelected
        End Get
        Set(ByVal value As Boolean)
            If Me._isSelected = value Then
                Return
            End If
            'If (TypeOf Me.Parent Is BoxControl) AndAlso value Then
            '    DirectCast(Me.Parent, BoxControl).Selected = False
            'End If
            If value Then
                Me.Focus()
            End If
            Me._isSelected = value
            Me.Cursor = IIf(Me._isSelected, Cursors.SizeAll, Cursors.Default)
            Me.Invalidate(False)
        End Set
    End Property
    Private m_Suppress As Boolean = False
    <DefaultValue(False)> _
    Public Property Suppress() As Boolean
        Get
            Return m_Suppress
        End Get
        Set(ByVal value As Boolean)
            m_Suppress = value
            Me.Refresh()
        End Set
    End Property
    Public ReadOnly Property IsTracking() As Boolean
        Get
            Return Me._isDragging
        End Get
    End Property
#End Region

#Region "Constructor"
    Public Sub New()
        Me.InitializeComponent()

        MyBase.SetStyle((ControlStyles.OptimizedDoubleBuffer Or _
                        (ControlStyles.AllPaintingInWmPaint Or _
                        ControlStyles.UserPaint)), True)

        Me._resizedRect = MyBase.Bounds
        Me._topRect = New Rectangle(0, 0, 0, 0)
        Me._bottomRect = New Rectangle(0, 0, 0, 0)
        Me._leftRect = New Rectangle(0, 0, 0, 0)
        Me._rightRect = New Rectangle(0, 0, 0, 0)
        Me._upperLeftRect = New Rectangle(0, 0, 0, 0)
        Me._upperRightRect = New Rectangle(0, 0, 0, 0)
        Me._lowerLeftRect = New Rectangle(0, 0, 0, 0)
        Me._lowerRightRect = New Rectangle(0, 0, 0, 0)
        Me._sizerRectangles = New ArrayList
        Me._sizerRectangles.Add(New Sizer((Me._topRect), Cursors.SizeNS, DragMode.ResizeTop))
        Me._sizerRectangles.Add(New Sizer((Me._bottomRect), Cursors.SizeNS, DragMode.ResizeBottom))
        Me._sizerRectangles.Add(New Sizer((Me._leftRect), Cursors.SizeWE, DragMode.ResizeLeft))
        Me._sizerRectangles.Add(New Sizer((Me._rightRect), Cursors.SizeWE, DragMode.ResizeRight))
        Me._sizerRectangles.Add(New Sizer((Me._upperLeftRect), Cursors.SizeNWSE, DragMode.ResizeUpperLeft))
        Me._sizerRectangles.Add(New Sizer((Me._upperRightRect), Cursors.SizeNESW, DragMode.ResizeUpperRight))
        Me._sizerRectangles.Add(New Sizer((Me._lowerLeftRect), Cursors.SizeNESW, DragMode.ResizeLowerLeft))
        Me._sizerRectangles.Add(New Sizer((Me._lowerRightRect), Cursors.SizeNWSE, DragMode.ResizeLowerRight))
        Me._textAlign = ContentAlignment.MiddleLeft
        Me._formatSting = New StringFormat
        Me.UpdateSizers()

        Me.UpdateStyles()
        Me.rtrack_Region = New Region(Me.ClientRectangle)
        Me.rtrack_Region.Xor(New Rectangle(intTracker_Size, intTracker_Size, Me.ClientSize.Width - (intTracker_Size << 1), Me.ClientSize.Height - (intTracker_Size << 1)))
    End Sub

#End Region


#Region "Keyboard key Function"
    'Protected Overrides Sub OnLostFocus(ByVal e As System.EventArgs)
    '    Call Unselect()
    '    MyBase.OnLostFocus(e)
    'End Sub

    'Issue: Not Work Arrow key in keydown and keypress event.
    'Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
    '    MyBase.OnKeyDown(e)
    '    If Me._isSelected = True Then
    '        Select Case e.KeyData
    '            Case Keys.Right + Keys.Shift
    '                MyBase.Width += 10
    '                Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
    '                Me._screenRect = New Rectangle(0, 0, 0, 0)
    '                Me.UpdateSizers()
    '            Case Keys.Left + Keys.Shift
    '                MyBase.Width -= 10
    '                Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
    '                Me._screenRect = New Rectangle(0, 0, 0, 0)
    '                Me.UpdateSizers()
    '            Case Keys.Down + Keys.Shift
    '                MyBase.Height += 10
    '                Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
    '                Me._screenRect = New Rectangle(0, 0, 0, 0)
    '                Me.UpdateSizers()
    '            Case Keys.Up + Keys.Shift
    '                MyBase.Height -= 10
    '                Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
    '                Me._screenRect = New Rectangle(0, 0, 0, 0)
    '                Me.UpdateSizers()
    '        End Select
    '    End If
    'End Sub

    'Protected Overrides Sub OnKeyDown(ByVal e As System.Windows.Forms.KeyEventArgs)
    '    MyBase.OnKeyDown(e)
    '    If Me._isSelected = True Then
    '        If e.KeyCode = Keys.Delete Then
    '            MyBase.Parent.Controls.Remove(Me)
    '        End If
    '    End If
    'End Sub

    Protected Overrides Function ProcessCmdKey(ByRef msg As Message, ByVal keyData As Keys) As Boolean
        If Me._isSelected = True Then
            Dim intWidth As Integer
            'If Me.blnSnaptoGrid = True Then
            '    intWidth = 6
            'Else
            '    intWidth = 1
            'End If
            If mblnSnapToGrid = True Then
                intWidth = 6
            Else
                intWidth = 1
            End If

            Select Case keyData
                '**********For Change Height and Width.************
                Case Keys.Right + Keys.Shift
                    MyBase.Width += intWidth
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                Case Keys.Left + Keys.Shift
                    If MyBase.Width <> 12 Then
                        MyBase.Width -= intWidth
                        Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                        Me._screenRect = New Rectangle(0, 0, 0, 0)
                        Me.UpdateSizers()
                        MyBase.Refresh()
                    End If
                Case Keys.Down + Keys.Shift
                    MyBase.Height += intWidth
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                Case Keys.Up + Keys.Shift
                    If MyBase.Height <> 12 Then
                        MyBase.Height -= intWidth
                        Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                        Me._screenRect = New Rectangle(0, 0, 0, 0)
                        Me.UpdateSizers()
                        MyBase.Refresh()
                    End If
                    '*********For Change Location.***********
                Case Keys.Right
                    MyBase.Left += intWidth
                    'Me._resizedRect = New Rectangle(MyBase.Left, 0, MyBase.Width, MyBase.Height)
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                Case Keys.Left
                    MyBase.Left -= intWidth
                    'Me._resizedRect = New Rectangle(MyBase.Left, 0, MyBase.Width, MyBase.Height)
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                Case Keys.Up
                    MyBase.Top -= intWidth
                    'Me._resizedRect = New Rectangle(0, MyBase.Top, MyBase.Width, MyBase.Height)
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                Case Keys.Down
                    MyBase.Top += intWidth
                    'Me._resizedRect = New Rectangle(0, MyBase.Top, MyBase.Width, MyBase.Height)
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                    '**********For Delete Field************
                Case Keys.Delete
                    'MyBase.Parent.Controls.Remove(Me)
            End Select
        End If
        'pbSample.Size = New System.Drawing.Size(Me.Width - 10, Me.Height - 10)
    End Function
#End Region

#Region "Mouse Events"
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        MyBase.OnMouseDown(e)
        'Me.BringToFront()
        Dim flag As Boolean = False
        'Vimal (16 Mar 2011) -- Start 
        'Me._isSelected = True
        'Me.OnSelected()
        'Vimal (16 Mar 2011) -- End
        Me._isDragging = True
        Dim sizer As Sizer
        For Each sizer In Me._sizerRectangles
            If sizer._rect.Contains(e.X, e.Y) Then
                Me._dragMode = sizer._dragMode
                flag = True
                Exit For
            End If
        Next
        If Not flag Then
            Me._dragMode = DragMode.Move
        End If

        Me.enTrackMode = Me.TestTracker(e.X, e.Y)
        Me.ptLastTrackPoint = PointToScreen(New Point(e.X, e.Y))
        Me.Capture = True
        If (Control.ModifierKeys And Keys.Shift) <> 0 Then
            Me.Selectede = Not Me.Selectede
        Else
            Me.Selectede = True
        End If
        Me._isDragging = False
        If Me.enTrackMode = TrackerMode.NONE Then
            MyBase.OnMouseDown(e)
        End If

        Me._lastMouseX = e.X
        Me._lastMouseY = e.Y
        MyBase.Invalidate()
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        If Not Me._isSelected Then
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        Me._isDragging = False
    End Sub

    Protected Sub DoTrack(ByVal delta_X As Integer, ByVal delta_Y As Integer, ByVal tracker_mode__1 As TrackerMode)
        If Not Me.IsHandleCreated Then
            Return
        End If
        Dim new_location As Point = Me.Location
        Dim new_size As System.Drawing.Size = Me.Size
        Select Case tracker_mode__1
            Case TrackerMode.CENTRE
                ' Moving 
                new_location = New Point(Me.Location.X + delta_X, Me.Location.Y + delta_Y)
                Exit Select
            Case TrackerMode.W
                ' Resize left side
                new_location = New Point(Me.Location.X + delta_X, Me.Location.Y)
                new_size = New System.Drawing.Size(Me.Width - delta_X, Me.Height)
                Exit Select
            Case TrackerMode.E
                ' Resize right side
                new_size = New System.Drawing.Size(Me.Width + delta_X, Me.Height)
                Exit Select
            Case TrackerMode.N
                ' Resize up side
                new_location = New Point(Me.Location.X, Me.Location.Y + delta_Y)
                new_size = New System.Drawing.Size(Me.Width, Me.Height - delta_Y)
                Exit Select
            Case TrackerMode.S
                ' Resize down side
                new_size = New System.Drawing.Size(Me.Width, Me.Height + delta_Y)
                Exit Select
            Case TrackerMode.NW
                ' Resize left - up side
                new_location = New Point(Me.Location.X + delta_X, Me.Location.Y + delta_Y)
                new_size = New System.Drawing.Size(Me.Width - delta_X, Me.Height - delta_Y)
                Exit Select
            Case TrackerMode.NE
                ' Resize right - up side
                new_location = New Point(Me.Location.X, Me.Location.Y + delta_Y)
                new_size = New System.Drawing.Size(Me.Width + delta_X, Me.Height - delta_Y)
                Exit Select
            Case TrackerMode.SW
                ' Resize left - down side
                new_location = New Point(Me.Location.X + delta_X, Me.Location.Y)
                new_size = New System.Drawing.Size(Me.Width - delta_X, Me.Height + delta_Y)
                Exit Select
            Case TrackerMode.SE
                ' Resize left - down side
                new_size = New System.Drawing.Size(Me.Width + delta_X, Me.Height + delta_Y)
                Exit Select
        End Select
        If new_size.Width < intMinSizeX Then
            new_size.Width = intMinSizeX
        End If
        If new_size.Height < intMinSizeY Then
            new_size.Height = intMinSizeY
        End If
        'Dim track_bounds As Rectangle = DirectCast(Me.Parent, ITrackerControlContainer).TrackBounds

        'If new_location.X < track_bounds.Left Then
        '    new_location.X = track_bounds.Left
        'ElseIf new_location.X + new_size.Width > track_bounds.Right Then
        '    new_location.X = track_bounds.Right - new_size.Width
        'End If
        'If new_location.Y < track_bounds.Top Then
        '    new_location.Y = track_bounds.Top
        'ElseIf new_location.Y + new_size.Height > track_bounds.Bottom Then
        '    new_location.Y = track_bounds.Bottom - new_size.Height
        'End If

        Me.Location = new_location
        Me.Size = new_size
        'pbSample.Size = New System.Drawing.Size(Me.Width - 10, Me.Height - 10)
    End Sub

    Protected Function TestTracker(ByVal X As Integer, ByVal Y As Integer) As TrackerMode
        Dim def_tm As TrackerMode = IIf(((Me.enTrackFlag And TrackerFlag.CENTRE) = TrackerFlag.CENTRE), TrackerMode.CENTRE, TrackerMode.NONE)
        If X < intTracker_HitSize Then
            ' Check vertical
            If (Me.enTrackFlag And TrackerFlag.W) = TrackerFlag.NONE Then
                Return def_tm
            End If
            If Y < (intTracker_HitSize << 1) Then
                Return IIf(((Me.enTrackFlag And TrackerFlag.N) = TrackerFlag.N), TrackerMode.NW, TrackerMode.W)
            End If
            If Y > (Me.ClientSize.Height - (intTracker_HitSize << 1)) Then
                Return IIf(((Me.enTrackFlag And TrackerFlag.S) = TrackerFlag.S), TrackerMode.SW, TrackerMode.W)
            End If
            Return TrackerMode.W
        End If
        If X > (Me.ClientSize.Width - intTracker_HitSize) Then
            ' Check vertical
            If (Me.enTrackFlag And TrackerFlag.E) = TrackerFlag.NONE Then
                Return def_tm
            End If
            If Y < (intTracker_HitSize << 1) Then
                Return IIf(((Me.enTrackFlag And TrackerFlag.N) = TrackerFlag.N), TrackerMode.NE, TrackerMode.E)
            End If
            If Y > (Me.ClientSize.Height - (intTracker_HitSize << 1)) Then
                Return IIf(((Me.enTrackFlag And TrackerFlag.S) = TrackerFlag.S), TrackerMode.SE, TrackerMode.E)
            End If
            Return TrackerMode.E
        End If
        If Y < intTracker_HitSize Then
            ' Check vertical
            If (Me.enTrackFlag And TrackerFlag.N) = TrackerFlag.NONE Then
                Return def_tm
            End If
            If X < (intTracker_HitSize << 1) Then
                Return IIf(((Me.enTrackFlag And TrackerFlag.W) = TrackerFlag.W), TrackerMode.NW, TrackerMode.N)
            End If
            If X > (Me.ClientSize.Width - (intTracker_HitSize << 1)) Then
                Return IIf(((Me.enTrackFlag And TrackerFlag.E) = TrackerFlag.E), TrackerMode.NE, TrackerMode.N)
            End If
            Return TrackerMode.N
        End If
        If Y > (Me.ClientSize.Height - intTracker_HitSize) Then
            ' Check vertical
            If (Me.enTrackFlag And TrackerFlag.S) = TrackerFlag.NONE Then
                Return def_tm
            End If
            If X < (intTracker_HitSize << 1) Then
                Return IIf(((Me.enTrackFlag And TrackerFlag.W) = TrackerFlag.W), TrackerMode.SW, TrackerMode.S)
            End If
            If X > (Me.ClientSize.Width - (intTracker_HitSize << 1)) Then
                Return IIf(((Me.enTrackFlag And TrackerFlag.E) = TrackerFlag.E), TrackerMode.SE, TrackerMode.S)
            End If
            Return TrackerMode.S
        End If
        Return def_tm
    End Function

    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        MyBase.OnMouseMove(e)

        If Me._isSelected Then
            Dim flag As Boolean = False
            Dim sizer As Sizer
            For Each sizer In Me._sizerRectangles
                If sizer._rect.Contains(e.X, e.Y) Then
                    flag = True
                    Me.Cursor = sizer._cursor
                    Exit For
                End If
            Next
            If Not flag Then
                Me.Cursor = Cursors.SizeAll
            End If

            Select Case Me.TestTracker(e.X, e.Y)
                Case TrackerMode.NW, TrackerMode.SE
                    Me.Cursor = Cursors.SizeNWSE
                    Exit Select
                Case TrackerMode.NE, TrackerMode.SW
                    Me.Cursor = Cursors.SizeNESW
                    Exit Select
                Case TrackerMode.N, TrackerMode.S
                    Me.Cursor = Cursors.SizeNS
                    Exit Select
                Case TrackerMode.E, TrackerMode.W
                    Me.Cursor = Cursors.SizeWE
                    Exit Select
                Case TrackerMode.NONE
                    Me.Cursor = Cursors.[Default]
                    Exit Select
                Case TrackerMode.CENTRE
                    Me.Cursor = Cursors.SizeAll
                    Exit Select
                Case Else
                    Me.Cursor = Cursors.SizeAll
                    Exit Select
            End Select
        End If

        Dim act_pos As Point = PointToScreen(New Point(e.X, e.Y))
        If Me.enTrackMode <> TrackerMode.NONE Then
            If Me._isDragging OrElse ((Math.Abs(Me.ptLastTrackPoint.X - act_pos.X) > intTracker_Hist) OrElse (Math.Abs(Me.ptLastTrackPoint.Y - act_pos.Y) > intTracker_Hist)) Then
                Dim delta_X As Integer = act_pos.X - ptLastTrackPoint.X
                Dim delta_Y As Integer = act_pos.Y - ptLastTrackPoint.Y
                Me.DoTrack(delta_X, delta_Y, Me.enTrackMode)
                Me.ptLastTrackPoint = act_pos
                Me._isDragging = True
                If (delta_X <> 0) OrElse (delta_Y <> 0) Then
                    Me.OnTracking(delta_X, delta_Y, Me.enTrackMode)
                End If
            End If
            Return
        End If


        If Me._isDragging Then
            'ControlPaint.DrawReversibleFrame(Me._screenRect, Me.BackColor, FrameStyle.Dashed)
            Dim num As Integer = (e.X - Me._lastMouseX)
            Dim num2 As Integer = (e.Y - Me._lastMouseY)
            'MyBase.RectangleToClient(MyBase.Parent.RectangleToScreen(MyBase.Parent.ClientRectangle))
            'Dim originalRect As Rectangle = MyBase.RectangleToScreen(MyBase.ClientRectangle)

            'Select Case Me._dragMode
            '    Case DragMode.Move
            '        Me._resizedRect.X = (Me._resizedRect.X + num)
            '        Me._resizedRect.Y = (Me._resizedRect.Y + num2)
            '        Exit Select
            '    Case DragMode.ResizeUpperLeft
            '        Me._resizedRect.X = (Me._resizedRect.X + num)
            '        Me._resizedRect.Y = (Me._resizedRect.Y + num2)
            '        Me._resizedRect.Width = (Me._resizedRect.Width - num)
            '        Me._resizedRect.Height = (Me._resizedRect.Height - num2)
            '        Exit Select
            '    Case DragMode.ResizeTop
            '        Me._resizedRect.Y = (Me._resizedRect.Y + num2)
            '        Me._resizedRect.Height = (Me._resizedRect.Height - num2)
            '        Exit Select
            '    Case DragMode.ResizeUpperRight
            '        Me._resizedRect.Y = (Me._resizedRect.Y + num2)
            '        Me._resizedRect.Width = (Me._resizedRect.Width + num)
            '        Me._resizedRect.Height = (Me._resizedRect.Height - num2)
            '        Exit Select
            '    Case DragMode.ResizeRight
            '        Me._resizedRect.Width = (Me._resizedRect.Width + num)
            '        Exit Select
            '    Case DragMode.ResizeLowerRight
            '        Me._resizedRect.Width = (Me._resizedRect.Width + num)
            '        Me._resizedRect.Height = (Me._resizedRect.Height + num2)
            '        Exit Select
            '    Case DragMode.ResizeBottom
            '        Me._resizedRect.Height = (Me._resizedRect.Height + num2)
            '        Exit Select
            '    Case DragMode.ResizeLowerLeft
            '        Me._resizedRect.X = (Me._resizedRect.X + num)
            '        Me._resizedRect.Width = (Me._resizedRect.Width - num)
            '        Me._resizedRect.Height = (Me._resizedRect.Height + num2)
            '        Exit Select
            '    Case DragMode.ResizeLeft
            '        Me._resizedRect.X = (Me._resizedRect.X + num)
            '        Me._resizedRect.Width = (Me._resizedRect.Width - num)
            '        Exit Select
            'End Select
            'Me._screenRect = MyBase.RectangleToScreen(Me._resizedRect)
            'Dim bounds As Rectangle = MyBase.Parent.RectangleToScreen(MyBase.Parent.ClientRectangle)
            'If (TypeOf MyBase.Parent Is ScrollableControl AndAlso (Not MyBase.Parent.Parent Is Nothing)) Then
            '    bounds.Intersect(MyBase.Parent.Parent.RectangleToScreen(MyBase.Parent.Parent.ClientRectangle))
            'End If
            'Me._screenRect = Me.KeepInBounds(bounds, Me._screenRect, originalRect)
            'Me._resizedRect = MyBase.RectangleToClient(Me._screenRect)
            'Me._lastMouseX = e.X
            'Me._lastMouseY = e.Y

            'ControlPaint.DrawReversibleFrame(Me._screenRect, Me.BackColor, FrameStyle.Dashed)
        End If
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        'MyBase.OnMouseUp(e)
        'Me._isDragging = False
        'If (((Me._resizedRect.X <> 0) OrElse (Me._resizedRect.Y <> 0)) OrElse ((Me._resizedRect.Width <> MyBase.Bounds.Width) OrElse (Me._resizedRect.Height <> MyBase.Bounds.Height))) Then
        '    MyBase.SetBounds((MyBase.Bounds.X + Me._resizedRect.X), (MyBase.Bounds.Y + Me._resizedRect.Y), Me._resizedRect.Width, Me._resizedRect.Height, BoundsSpecified.All)
        '    MyBase.Invalidate()
        '    Me.OnMovedResized()
        'End If

        If Me.enTrackMode = TrackerMode.NONE Then
            MyBase.OnMouseUp(e)
        End If
        Me.enTrackMode = TrackerMode.NONE
        Me.Capture = False
        If Me._isDragging Then
            Me.OnTracking(0, 0, Me.enTrackMode)
        End If
        Me._isDragging = False
        Me.Focus()

        If mblnSnapToGrid = True Then
            If ((MyBase.Location.X) Mod 6) <> 0 Or ((MyBase.Location.Y) Mod 6) <> 0 Then
                MyBase.Location = New Point(MyBase.Location.X + (6 - ((MyBase.Location.X) Mod 6)), MyBase.Location.Y + (6 - ((MyBase.Location.Y) Mod 6)))
            End If
        End If
    End Sub

#End Region

#Region "Other Events"
    Protected Overrides Sub OnMove(ByVal e As EventArgs)
        Me.UpdateSizingData()
        MyBase.OnMove(e)
    End Sub

    Protected Overridable Sub OnMovedResized()
        'If (Not Me._MovedResized Is Nothing) Then
        RaiseEvent MovedResized(Me, EventArgs.Empty)
        'End If
    End Sub

    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        Me.UpdateSizingData()
        MyBase.OnSizeChanged(e)

        If Me.rtrack_Region IsNot Nothing Then
            Me.rtrack_Region.Dispose()
        End If
        Me.rtrack_Region = New Region(Me.ClientRectangle)
        Me.rtrack_Region.[Xor](New Rectangle(intTracker_Size, intTracker_Size, Me.ClientSize.Width - (intTracker_Size << 1), Me.ClientSize.Height - (intTracker_Size << 1)))
        Me.Invalidate(False)
    End Sub

    Protected Overridable Sub OnTracking(ByVal delta_X As Integer, ByVal delta_Y As Integer, ByVal mode As TrackerMode)
        RaiseEvent Tracking(Me, New TrackingEventArgs(delta_X, delta_Y, mode))
    End Sub

    Protected Overridable Sub OnSelected()
        RaiseEvent Selected(Me, EventArgs.Empty)
    End Sub

    Protected Overrides Sub [Select](ByVal directed As Boolean, ByVal forward As Boolean)
        Me._isSelected = True
        Me._isDragging = False
        'Vimal (16 Mar 2011) -- Start 
        'MyBase.Select(directed, forward)
        'Vimal (16 Mar 2011) -- End
        Me.OnSelected()
    End Sub
#End Region

#Region "Other Fuctions"
    Public Sub Unselect()
        Me._isSelected = False
        Me._isDragging = False
        MyBase.Invalidate()
    End Sub

    Private Sub UpdateSizers()
        If (Not Me._sizerRectangles Is Nothing) Then
            Me._horizontalMid = ((MyBase.Bounds.Width / 2) - (Me.SizerWidth / 2))
            Me._verticalMid = ((MyBase.Bounds.Height / 2) - (Me.SizerWidth / 2))


            Dim sizer As Sizer = DirectCast(Me._sizerRectangles.Item(0), Sizer)  ' Top Rectangle
            sizer._rect.X = CInt(Me._horizontalMid)
            sizer._rect.Y = 0
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth

            sizer = DirectCast(Me._sizerRectangles.Item(1), Sizer)  ' Bottom Rctangle
            sizer._rect.X = CInt(Me._horizontalMid)
            sizer._rect.Y = (MyBase.Bounds.Height - Me.SizerWidth)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth

            sizer = DirectCast(Me._sizerRectangles.Item(2), Sizer)  ' Left Rctangle
            sizer._rect.X = 0
            sizer._rect.Y = CInt(Me._verticalMid)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth

            sizer = DirectCast(Me._sizerRectangles.Item(3), Sizer)  ' Right Rctangle
            sizer._rect.X = (MyBase.Bounds.Width - Me.SizerWidth)
            sizer._rect.Y = CInt(Me._verticalMid)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth

            sizer = DirectCast(Me._sizerRectangles.Item(4), Sizer)  ' UpperLeft Rctangle
            sizer._rect.X = 0
            sizer._rect.Y = 0
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth

            sizer = DirectCast(Me._sizerRectangles.Item(5), Sizer)  ' UpperRight Rctangle
            sizer._rect.X = (MyBase.Bounds.Width - Me.SizerWidth)
            sizer._rect.Y = 0
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth

            sizer = DirectCast(Me._sizerRectangles.Item(6), Sizer)  ' LowerLeft Rctangle
            sizer._rect.X = 0
            sizer._rect.Y = (MyBase.Bounds.Height - Me.SizerWidth)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth

            sizer = DirectCast(Me._sizerRectangles.Item(7), Sizer)  ' LowerRight Rctangle
            sizer._rect.X = (MyBase.Bounds.Width - Me.SizerWidth)
            sizer._rect.Y = (MyBase.Bounds.Height - Me.SizerWidth)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
        End If
    End Sub

    Protected Sub UpdateSizingData()
        Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
        Me._screenRect = New Rectangle(0, 0, 0, 0)
        Me.UpdateSizers()
    End Sub

#End Region

#Region "Paint Events and Function"
    'Private Sub Box_Paint(ByVal sender As Object, ByVal e As PaintEventArgs)
    '    Dim r As Rectangle = Me.ClientRectangle
    '    Dim gp As New GraphicsPath
    '    Dim cs As Integer = 25

    '    r.Inflate(-5, -5)

    '    gp.AddArc(r.X, r.Y, cs, cs, 180, 90)
    '    gp.AddArc(r.X + r.Width - cs, r.Y, cs, cs, 270, 90)
    '    gp.AddArc(r.X + r.Width - cs, r.Y + r.Height - cs, cs, cs, 0, 90)
    '    gp.AddArc(r.X, r.Y + r.Height - cs, cs, cs, 90, 90)

    '    Dim t As Single = cs / 2 + r.Y
    '    gp.AddLine(r.X, r.Y + r.Height - cs, r.X, t)

    '    e.Graphics.SmoothingMode = SmoothingMode.AntiAlias
    '    e.Graphics.FillPath(Brushes.Transparent, gp)
    '    e.Graphics.DrawPath(Pens.Black, gp)
    'End Sub

    Private Function KeepInBounds(ByVal bounds As Rectangle, ByVal rect As Rectangle, ByVal originalRect As Rectangle) As Rectangle
        If (rect.Width < Me.MinWidth) Then
            If (((Me._dragMode = DragMode.ResizeLeft) OrElse (Me._dragMode = DragMode.ResizeLowerLeft)) OrElse (Me._dragMode = DragMode.ResizeUpperLeft)) Then
                rect.X = ((originalRect.X + originalRect.Width) - Me.MinWidth)
            Else
                rect.X = originalRect.X
            End If
            rect.Width = Me.MinWidth
        End If
        If (rect.Height < Me.MinHeight) Then
            If (((Me._dragMode = DragMode.ResizeTop) OrElse (Me._dragMode = DragMode.ResizeUpperLeft)) OrElse (Me._dragMode = DragMode.ResizeUpperRight)) Then
                rect.Y = ((originalRect.Y + originalRect.Height) - Me.MinHeight)
            Else
                rect.Y = originalRect.Y
            End If
            rect.Height = Me.MinHeight
        End If
        If (rect.X < bounds.X) Then
            If (Me._dragMode = DragMode.Move) Then
                rect.Width = Math.Min(originalRect.Width, bounds.Width)
                rect.X = bounds.X
            Else
                rect.X = bounds.X
                rect.Width = ((originalRect.Width + originalRect.X) - bounds.X)
            End If
        End If
        If ((rect.X + rect.Width) > (bounds.X + bounds.Width)) Then
            If (Me._dragMode = DragMode.Move) Then
                rect.Width = Math.Min(originalRect.Width, bounds.Width)
                rect.X = ((bounds.X + bounds.Width) - rect.Width)
            Else
                rect.X = originalRect.X
                rect.Width = ((bounds.X + bounds.Width) - rect.X)
            End If
        End If
        If (rect.Y < bounds.Y) Then
            If (Me._dragMode = DragMode.Move) Then
                rect.Height = Math.Min(originalRect.Height, bounds.Height)
                rect.Y = bounds.Y
            Else
                rect.Y = bounds.Y
                rect.Height = ((originalRect.Height + originalRect.Y) - bounds.Y)
            End If
        End If
        If ((rect.Y + rect.Height) > (bounds.Y + bounds.Height)) Then
            If (Me._dragMode = DragMode.Move) Then
                rect.Height = Math.Min(originalRect.Height, bounds.Height)
                rect.Y = ((bounds.Y + bounds.Height) - rect.Height)
                Return rect
            End If
            rect.Y = originalRect.Y
            rect.Height = ((bounds.Y + bounds.Height) - rect.Y)
        End If
        Return rect
    End Function

    Public Function DrawCurveRectangle(ByVal intX As Integer, ByVal intY As Integer, ByVal intWidth As Integer, ByVal intHeight As Integer, ByVal intUpperLeft As Integer _
                                  , ByVal intUpperRight As Integer, ByVal intLowerRight As Integer, ByVal intLowerLeft As Integer)

        Dim gpGraphicsPath As GraphicsPath = New GraphicsPath()
        If intUpperRight > Me.Height Then intUpperRight = Me.Height
        If intLowerRight > Me.Height Then intLowerRight = Me.Height
        If intLowerLeft > Me.Height Then intLowerLeft = Me.Height
        If intUpperLeft > Me.Height Then intUpperLeft = Me.Height


        ' Top Right Arc
        gpGraphicsPath.AddArc(intX + intWidth - (intUpperRight * 2), intY, intUpperRight * 2, intUpperRight * 2, 270, 90)
        ' Bottom Right Arc
        gpGraphicsPath.AddArc(intX + intWidth - (intLowerRight * 2), intY + intHeight - (intLowerRight * 2), intLowerRight * 2, intLowerRight * 2, 0, 90)
        ' Bottom Left Arc
        gpGraphicsPath.AddArc(intX, intY + intHeight - (intLowerLeft * 2), intLowerLeft * 2, intLowerLeft * 2, 90, 90)
        ' Top Left Arc
        gpGraphicsPath.AddArc(intX, intY, intUpperLeft * 2, intUpperLeft * 2, 180, 90)

        gpGraphicsPath.CloseFigure()
        Return gpGraphicsPath
        gpGraphicsPath.Dispose()

    End Function

    Public Sub DrawLine(ByVal g As Graphics, ByVal intX As Integer, ByVal intY As Integer, ByVal intWidth As Integer, ByVal intHeight As Integer)
        Using p As New Pen(Color.Black, 2)
            g.DrawLine(p, intX + intWidth - (2 * 2), intY, intX + intWidth, intY) 'TopRightHori
            g.DrawLine(p, intX + intWidth, intY, intX + intWidth, intY + (2 * 2)) 'TopRightVerti

            g.DrawLine(p, intX + intWidth - (2 * 2), intHeight, intX + intWidth, intHeight) 'BottomRightHori
            g.DrawLine(p, intX + intWidth, intHeight, intX + intWidth, intX + intHeight - (2 * 2))  'BottomRightVerti

            g.DrawLine(p, intX, intHeight, intX + (2 * 2), intHeight)  'BottomLeftHori
            g.DrawLine(p, intX, intHeight, intX, intHeight - (2 * 2))  'BottomLeftVerti

            g.DrawLine(p, intX, intY, intX + (2 * 2), intY) 'TopLeftHori
            g.DrawLine(p, intX, intY, intX, intY + (2 * 2)) 'TopLeftVerti
        End Using

    End Sub

    Private Sub DrawHandles(ByVal g As Graphics)
        Dim sizer As Sizer
        If Me.FieldType = 4 Then
            'If Me.Text = "Static Box" Then
            'If FieldType = 4 Then
            For Each sizer In Me._sizerRectangles
                ControlPaint.DrawGrabHandle(g, sizer._rect, True, True)
            Next
        Else
            For Each sizer In Me._sizerRectangles
                ControlPaint.DrawGrabHandle(g, sizer._rect, True, True)
            Next
        End If
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)
        e.Graphics.DrawString([Text], Me.Font, New SolidBrush(Me.ForeColor), New Rectangle(2, 2, MyBase.Width - 5, MyBase.Height - 5), _formatSting)

        Dim gpGraphicsPath As New GraphicsPath
        If Me.FieldType = 4 Then ''''''''''''Static Box''''''''''''''
            Using p As New Pen(_lineColor, _lineThickness)
                p.DashStyle = _lineStyle
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias
                If Me._isSelected Then
                    '******For Curved Rectangle*******
                    gpGraphicsPath = DrawCurveRectangle(2, 2, MyBase.Width - 5, MyBase.Height - 5, Me.intUpperLeftR, Me.intUpperRightR, Me.intLowerRightR, Me.intLowerLeftR)
                    e.Graphics.DrawPath(p, gpGraphicsPath)

                    Me.DrawHandles(e.Graphics)
                Else
                    '*******For Curved Rectangle*******
                    gpGraphicsPath = DrawCurveRectangle(2, 2, MyBase.Width - 5, MyBase.Height - 5, Me.intUpperLeftR, Me.intUpperRightR, Me.intLowerRightR, Me.intLowerLeftR)
                    e.Graphics.DrawPath(p, gpGraphicsPath)
                End If
            End Using

        ElseIf Me.FieldType = 0 Then     ''''''''''''Custom Text''''''''''''''
            If Me._isSelected Then
                'ControlPaint.DrawSelectionFrame(e.Graphics, True, New Rectangle(0, 0, MyBase.Width, MyBase.Height), New Rectangle(2, 2, (MyBase.Width - 5), (MyBase.Height - 5)), Me.BackColor)

                ControlPaint.DrawBorder(e.Graphics, New Rectangle(0, 0, MyBase.Width, MyBase.Height), Color.Black, ButtonBorderStyle.Dashed)
                Me.DrawHandles(e.Graphics)
            Else
                'ControlPaint.DrawSelectionFrame(e.Graphics, True, New Rectangle(0, 0, MyBase.Width, MyBase.Height), New Rectangle(2, 2, (MyBase.Width - 5), (MyBase.Height - 5)), Me.BackColor)
                'ControlPaint.DrawBorder(e.Graphics, New Rectangle(0, 0, MyBase.Width, MyBase.Height), Color.Black, ButtonBorderStyle.Dashed)

                DrawLine(e.Graphics, 0, 0, MyBase.Width, MyBase.Height)
            End If

        Else
            If Me._isSelected Then
                e.Graphics.DrawRectangle(Pens.Black, New Rectangle(0, 0, MyBase.Width - 1, MyBase.Height - 1))
                Me.DrawHandles(e.Graphics)
            Else
                e.Graphics.DrawRectangle(Pens.Black, New Rectangle(0, 0, MyBase.Width - 1, MyBase.Height - 1))
            End If
        End If
        If m_Suppress Then
            'e.Graphics.DrawString("Supress", New Font("Arial", 9, FontStyle.Bold), Brushes.LightGray, 10, 20)
            For i As Integer = 0 To MyBase.Width - 1
                i += 10
                e.Graphics.DrawLine(Pens.LightGray, i, 0, i, MyBase.Height - 1)
            Next
            For i As Integer = 0 To MyBase.Height - 1
                i += 10
                e.Graphics.DrawLine(Pens.LightGray, 0, i, MyBase.Width - 1, i)
            Next
        End If
    End Sub
#End Region

#Region "For Custom Text control"
    Private txt As New TextBox

    Private Sub BoxControl_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDoubleClick
        If Me.FieldType = 0 Then
            'Me._isSelected = False
            txt.Text = Text
            Select Case TextAlign
                Case ContentAlignment.TopLeft, ContentAlignment.MiddleLeft, ContentAlignment.BottomLeft
                    txt.TextAlign = HorizontalAlignment.Left
                Case ContentAlignment.TopCenter, ContentAlignment.MiddleCenter, ContentAlignment.BottomCenter
                    txt.TextAlign = HorizontalAlignment.Center
                Case ContentAlignment.TopRight, ContentAlignment.MiddleRight, ContentAlignment.BottomRight
                    txt.TextAlign = HorizontalAlignment.Right
            End Select
            txt.Name = "txt"
            txt.BorderStyle = Windows.Forms.BorderStyle.None
            txt.Multiline = True
            txt.Dock = DockStyle.Fill
            If Me.BackColor.ToArgb() = Color.Transparent.ToArgb() Then
                txt.BackColor = Color.White
            Else
                txt.BackColor = Me.BackColor
            End If
            Text = txt.Text
            Me.Controls.Add(txt)
            txt.Focus()
        End If
    End Sub

    Private Sub BoxControl_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        If Me.FieldType = 0 Then
            If txt.Text <> "" Then
                Text = txt.Text
            End If

            Me.Controls.Remove(txt)
        End If
    End Sub
#End Region

    Private Class Sizer
        Public Sub New(ByRef rect As Rectangle, ByVal cursor As Cursor, ByVal dragMode As DragMode)
            Me._rect = rect
            Me._cursor = cursor
            Me._dragMode = dragMode
        End Sub

        Public _cursor As Cursor
        Public _dragMode As DragMode
        Public _rect As Rectangle
    End Class

    Public Class TrackingEventArgs
        Inherits EventArgs
        Public Sub New(ByVal delta_X As Integer, ByVal delta_Y As Integer, ByVal mode As TrackerMode)
            Me.m_delta_X = delta_X
            Me.m_delta_Y = delta_Y
            Me.m_mode = mode
        End Sub
        Public m_delta_X As Integer
        Public m_delta_Y As Integer
        Public m_mode As TrackerMode
    End Class

    'Private Sub InitializeComponent()
    '    Me.SuspendLayout()
    '    '
    '    'ImageControl
    '    '
    '    Me.Name = "ImageControl"
    '    Me.Size = New System.Drawing.Size(150, 168)
    '    Me.ResumeLayout(False)

    'End Sub

End Class
#End Region

