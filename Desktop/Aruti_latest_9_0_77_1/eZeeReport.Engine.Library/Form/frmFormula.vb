﻿Imports ArutiReport.Engine.Library.eZee_Sample
Imports eZeeCommonLib

Public Class frmFormula

    Public mblnFlag As Boolean = False
    Public mstrExpression As String = ""
    Public mdtTable As New DataTable
    Public mintRow As Int32 = 0
    Public mstrSelect As String = ""
    Public mstrFormula As String = ""
    Dim mstrChkNumeric As String = ""
    Public mstrFormat As String = ""
    Private ReadOnly mstrModuleName As String = "frmFormula"
    Private mtnSelectedNode As TreeNode

    'Public Sub New()
    '    ' This call is required by the Windows Form Designer.
    '    InitializeComponent()
    '    ' Add any initialization after the InitializeComponent() call.
    'End Sub


#Region " Private Methods "

    Public Sub DisplayDialog(ByVal trNode As TreeNode)
        Try
            Dim intCtr As Int32 = 0
            tvReportFields.Nodes.Add(lblTree.Text)

            tvReportFields.Font = New Font("Arial", 8, FontStyle.Bold)

            For i As Integer = 0 To trNode.Nodes.Count - 1
                If Not trNode.Nodes(i).Text Like ".Formula_*" Then
                    tvReportFields.Nodes(0).Nodes.Add(trNode.Nodes(i).Text)
                    tvReportFields.Nodes(0).Nodes(intCtr).Tag = trNode.Nodes(i).Tag
                    tvReportFields.Nodes(0).Nodes(intCtr).ToolTipText = trNode.Nodes(i).ToolTipText
                    intCtr += 1
                End If
            Next
            tvReportFields.ExpandAll()
            tvOperators.ExpandAll()

            tvReportFields.Nodes.Add("Formula")
            tvReportFields.Nodes(1).Nodes.Add("iif(Exprression,True,False)")

            cboDecimalPlaces.Text = 0
            cboWordDec.Text = 0

            Me.ShowDialog()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ShowMe", mstrModuleName)
        End Try
    End Sub

    Public Sub ExecuteFormula()
        Try
            Dim strExpSubs As String = ""
            Dim blnRetvalue As Boolean = True
            If mstrFormula <> "" Then
                strExpSubs = mstrFormula
            Else
                strExpSubs = txtExpback.Text
            End If

            mstrSelect = ""

            While strExpSubs.Trim.EndsWith("+") OrElse strExpSubs.Trim.EndsWith("-") OrElse strExpSubs.Trim.EndsWith("*") OrElse strExpSubs.Trim.EndsWith("/")
                strExpSubs = strExpSubs.Trim.Substring(0, strExpSubs.Length - 2)
            End While

            If strExpSubs.Trim <> "" Then
                'For i As Integer = 0 To cExpSubs.Trim.Split(" ").Length - 1
                '    If txtExp.Tag.Split(" ")(i).ToString.Trim Like "Int32*" OrElse txtExp.Tag.Split(" ")(i).ToString.Trim Like "Decimal*" Then
                '        nNos += dtTable.Rows(0)(txtExp.Text.Trim.Split(" ")(i))
                '    Else
                '        If txtExp.Tag.Split(" ")(i).ToString.Trim = "+" Then
                '            nNos += dtTable.Rows(0)(txtExp.Text.Split(" ")(i + 1))
                '        ElseIf txtExp.Tag.Split(" ")(i).ToString.Trim = "-" Then
                '            nNos -= dtTable.Rows(0)(txtExp.Text.Split(" ")(i + 1))
                '        ElseIf txtExp.Tag.Split(" ")(i).ToString.Trim = "*" Then
                '            nNos *= dtTable.Rows(0)(txtExp.Text.Split(" ")(i + 1))
                '        ElseIf txtExp.Tag.Split(" ")(i).ToString.Trim = "/" Then
                '            nNos /= dtTable.Rows(0)(txtExp.Text.Split(" ")(i + 1))
                '        End If
                '        i += 1
                '    End If
                'Next
                If strExpSubs.Contains("ô") Then
                    Dim strSelect As String = ""
                    Dim blnFlag As Boolean = True
                    'For i As Integer = 0 To .Length - 1
                    If strExpSubs.Split("ô")(1).ToLower = "iif(" Then
                        Call InsideFormulaExec(strExpSubs.Split("ô")(2).Split("ö")(0))
                        If Not mstrChkNumeric.Contains("False") Then
                            If strExpSubs.Split("ô")(2).Split("ö").Length > 3 Then
                                blnFlag = (CType(mstrSelect, Decimal) <> CType(strExpSubs.Split("ô")(2).Split("ö")(strExpSubs.Split("ô")(2).Split("ö").Length - 1).Trim, Decimal))
                            Else
                                If strExpSubs.Split("ô")(2).Split("ö")(1) = "=" Then
                                    blnFlag = (CType(mstrSelect.Trim, Decimal) = CType(strExpSubs.Split("ô")(2).Split("ö")(strExpSubs.Split("ô")(2).Split("ö").Length - 1).Trim, Decimal))
                                ElseIf strExpSubs.Split("ô")(2).Split("ö")(1) = ">" Then
                                    blnFlag = (CType(mstrSelect, Decimal) > CType(strExpSubs.Split("ô")(2).Split("ö")(strExpSubs.Split("ô")(2).Split("ö").Length - 1).Trim, Decimal))
                                ElseIf strExpSubs.Split("ô")(2).Split("ö")(1) = "<" Then
                                    blnFlag = (CType(mstrSelect, Decimal) < CType(strExpSubs.Split("ô")(2).Split("ö")(strExpSubs.Split("ô")(2).Split("ö").Length - 1).Trim, Decimal))
                                End If
                            End If
                        Else
                            If strExpSubs.Split("ô")(2).Split("ö").Length > 3 Then
                                blnFlag = (mstrSelect.Trim.ToUpper <> strExpSubs.Split("ô")(2).Split("ö")(strExpSubs.Split("ô")(2).Split("ö").Length - 1).Trim.ToUpper)
                            Else
                                If strExpSubs.Split("ô")(2).Split("ö")(1) = "=" Then
                                    blnFlag = (mstrSelect.Trim.ToUpper = strExpSubs.Split("ô")(2).Split("ö")(strExpSubs.Split("ô")(2).Split("ö").Length - 1).Trim.ToUpper)
                                ElseIf strExpSubs.Split("ô")(2).Split("ö")(1) = ">" Then
                                    blnFlag = (mstrSelect.ToUpper > strExpSubs.Split("ô")(2).Split("ö")(strExpSubs.Split("ô")(2).Split("ö").Length - 1).Trim.ToUpper)
                                ElseIf strExpSubs.Split("ô")(2).Split("ö")(1) = "<" Then
                                    blnFlag = (mstrSelect.ToUpper < strExpSubs.Split("ô")(2).Split("ö")(strExpSubs.Split("ô")(2).Split("ö").Length - 1).Trim.ToUpper)
                                End If
                            End If
                        End If

                        mstrSelect = ""

                        If blnFlag Then
                            Call InsideFormulaExec(strExpSubs.Split("ô")(4))
                        Else
                            Call InsideFormulaExec(strExpSubs.Split("ô")(6))
                        End If
                    End If
                Else
                    Call InsideFormulaExec(strExpSubs)
                End If

            Else
                MessageBox.Show("Expression cannot be empty.... ", "eZee Message")
                blnRetvalue = False
            End If
            If blnRetvalue Then
                btnSave.Enabled = True
                btnIsNumeric.Enabled = True
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ExecuteFormula", mstrModuleName)
        End Try
    End Sub

    Private Sub InsideFormulaExec(ByVal strExpSubs As String)
        Try
            Dim nNos As Object = Nothing
            Dim blnValue As Boolean = True
            Dim objValue As Object = Nothing
            Dim strString As String = ""

            mstrSelect = ""

            If strExpSubs.Trim.Split(";").Length > 1 Then
                For i As Integer = 0 To strExpSubs.Trim.Split(";").Length - 1 Step 2
                    Dim nVal1 As Object = Nothing
                    Dim nVal2 As Object = Nothing

                    blnValue = True
                    If i = 0 Then
                        If mdtTable.Columns.Contains(strExpSubs.Split(";")(i).Trim) Then ' checking 1 st columns is there or not 
                            ' 1
                            'Value 1 
                            nVal1 = mdtTable.Rows(mintRow)(strExpSubs.Split(";")(i).ToString.Trim)
                            ' 2
                            If mdtTable.Columns(strExpSubs.Split(";")(i).ToString.Trim).DataType.Name Like "*Int*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i).ToString.Trim).DataType.Name Like "Decimal*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i).ToString.Trim).DataType.Name Like "float" OrElse mdtTable.Columns(strExpSubs.Split(";")(i).ToString.Trim).DataType.Name Like "Double*" Then
                            Else
                                blnValue = False
                                mstrChkNumeric += blnValue.ToString + " "
                            End If

                            If mdtTable.Columns.Contains(strExpSubs.Split(";")(i + 2).Trim) Then ' checking 1 st columns is there or not 
                                'Value 2 
                                nVal2 = mdtTable.Rows(mintRow)(strExpSubs.Split(";")(i + 2).ToString.Trim)
                                If Not (mdtTable.Columns(strExpSubs.Split(";")(i + 2).ToString.Trim).DataType.Name Like "*Int*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i + 2).ToString.Trim).DataType.Name Like "Decimal*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i + 2).ToString.Trim).DataType.Name Like "float" OrElse mdtTable.Columns(strExpSubs.Split(";")(i + 2).ToString.Trim).DataType.Name Like "Double*") Then
                                    blnValue = False
                                    mstrChkNumeric += blnValue.ToString + " "
                                End If
                            Else
                                If Not IsNumeric(strExpSubs.Split(";")(i + 2)) Then
                                    blnValue = False
                                    mstrChkNumeric += blnValue.ToString + " "
                                    If strExpSubs.Split(";")(i + 2).Trim.StartsWith("""") Then
                                        nVal2 = strExpSubs.Split(";")(i + 2).Trim.Replace("""", "")
                                    End If
                                Else
                                    'Value 2 
                                    nVal2 = CType(strExpSubs.Split(";")(i + 2), Decimal)
                                End If
                            End If
                        Else
                            If Not IsNumeric(strExpSubs.Split(";")(i)) Then
                                blnValue = False
                                mstrChkNumeric += blnValue.ToString + " "
                                If strExpSubs.Split(";")(i).Trim.StartsWith("""") Then
                                    nVal1 = strExpSubs.Split(";")(i).Trim.Replace("""", "")
                                End If
                            Else
                                'Value 1 
                                nVal1 = CType(strExpSubs.Split(";")(i), Decimal)
                            End If
                            If mdtTable.Columns.Contains(strExpSubs.Split(";")(i + 2).Trim) Then ' checking 1 st columns is there or not 
                                'Value 2 
                                nVal2 = mdtTable.Rows(mintRow)(strExpSubs.Split(";")(i + 2).ToString.Trim)
                                If Not (mdtTable.Columns(strExpSubs.Split(";")(i + 2).ToString.Trim).DataType.Name Like "*Int*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i + 2).ToString.Trim).DataType.Name Like "Decimal*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i + 2).ToString.Trim).DataType.Name Like "float" OrElse mdtTable.Columns(strExpSubs.Split(";")(i + 2).ToString.Trim).DataType.Name Like "Double*") Then
                                    blnValue = False
                                    mstrChkNumeric += blnValue.ToString + " "
                                End If
                            Else
                                If Not IsNumeric(strExpSubs.Split(";")(i + 2)) Then
                                    blnValue = False
                                    mstrChkNumeric += blnValue.ToString + " "
                                    If strExpSubs.Split(";")(i + 2).Trim.StartsWith("""") Then
                                        nVal2 = strExpSubs.Split(";")(i + 2).Trim.Replace("""", "")
                                    End If
                                Else
                                    'Value 2 
                                    nVal2 = CType(strExpSubs.Split(";")(i + 2), Decimal)
                                End If
                            End If
                        End If

                        If blnValue Then
                            If strExpSubs.Split(";")(i + 1) = "+" Then
                                objValue = nVal1 + nVal2
                            ElseIf strExpSubs.Split(";")(i + 1) = "-" Then
                                objValue = nVal1 - nVal2
                            ElseIf strExpSubs.Split(";")(i + 1) = "*" Then
                                objValue = nVal1 * nVal2
                            ElseIf strExpSubs.Split(";")(i + 1) = "/" Then
                                objValue = nVal1 / nVal2
                            End If
                            mstrSelect += objValue.ToString
                        Else
                            'Manish Tanna (12 May 2012) -- start 
                            If nVal2 IsNot Nothing Then
                                If strExpSubs.Split(";")(i + 1) = "+" Then
                                    strString = nVal1.ToString + nVal2.ToString
                                ElseIf strExpSubs.Split(";")(i + 1) = "-" Then
                                    strString = nVal1.ToString + nVal2.ToString
                                ElseIf strExpSubs.Split(";")(i + 1) = "*" Then
                                    strString = nVal1.ToString + nVal2.ToString
                                ElseIf strExpSubs.Split(";")(i + 1) = "/" Then
                                    strString = nVal1.ToString + nVal2.ToString
                                End If
                            End If
                            'Manish Tanna (12 May 2012) -- start 
                            mstrSelect += strString
                        End If
                        i += 2
                    Else
                        If mdtTable.Columns.Contains(strExpSubs.Split(";")(i).Trim) Then ' checking 1 st columns is there or not 
                            ' 1
                            'Value 1 
                            nVal1 = mdtTable.Rows(mintRow)(strExpSubs.Split(";")(i).ToString.Trim)
                            If mdtTable.Columns(strExpSubs.Split(";")(i).ToString.Trim).DataType.Name Like "*Int32*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i).ToString.Trim).DataType.Name Like "Decimal*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i).ToString.Trim).DataType.Name Like "float" OrElse mdtTable.Columns(strExpSubs.Split(";")(i).ToString.Trim).DataType.Name Like "Double*" Then
                                If mdtTable.Columns.Contains(strExpSubs.Split(";")(i - 2).Trim) Then ' checking 1 st columns is there or not 
                                    'Value 2 
                                    nVal2 = mdtTable.Rows(mintRow)(strExpSubs.Split(";")(i - 2).ToString.Trim)
                                    If Not (mdtTable.Columns(strExpSubs.Split(";")(i - 2).ToString.Trim).DataType.Name Like "Int32*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i - 2).ToString.Trim).DataType.Name Like "Decimal*") Then
                                        blnValue = False
                                        mstrChkNumeric += blnValue.ToString + " "
                                    End If
                                Else
                                    If Not IsNumeric(strExpSubs.Split(";")(i - 2).Trim) Then
                                        blnValue = False
                                        mstrChkNumeric += blnValue.ToString + " "
                                        If strExpSubs.Split(";")(i - 2).Trim.StartsWith("""") Then
                                            nVal2 = strExpSubs.Split(";")(i - 2).Trim.Replace("""", "")
                                        End If
                                    Else
                                        'Value 2 
                                        nVal2 = CType(strExpSubs.Split(";")(i - 2), Decimal)
                                    End If
                                End If
                            Else
                                blnValue = False
                                mstrChkNumeric += blnValue.ToString + " "
                            End If
                        Else
                            If Not IsNumeric(strExpSubs.Split(";")(i)) Then
                                blnValue = False
                                mstrChkNumeric += blnValue.ToString + " "
                                If strExpSubs.Split(";")(i).Trim.StartsWith("""") Then
                                    nVal1 = strExpSubs.Split(";")(i).Trim.Replace("""", "")
                                End If
                            Else
                                'Value 1 
                                nVal1 = CType(strExpSubs.Split(";")(i), Decimal)
                                If mdtTable.Columns.Contains(strExpSubs.Split(";")(i - 2).Trim) Then ' checking 1 st columns is there or not 
                                    'Value 2 
                                    nVal2 = mdtTable.Rows(mintRow)(strExpSubs.Split(";")(i - 2).ToString.Trim)
                                    If Not (mdtTable.Columns(strExpSubs.Split(";")(i - 2).ToString.Trim).DataType.Name Like "*Int*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i - 2).ToString.Trim).DataType.Name Like "Decimal*" OrElse mdtTable.Columns(strExpSubs.Split(";")(i - 2).ToString.Trim).DataType.Name Like "float") OrElse mdtTable.Columns(strExpSubs.Split(";")(i - 2).ToString.Trim).DataType.Name Like "Double*" Then
                                        blnValue = False
                                        mstrChkNumeric += blnValue.ToString + " "
                                    End If
                                Else
                                    If Not IsNumeric(strExpSubs.Split(";")(i - 2)) Then
                                        blnValue = False
                                        mstrChkNumeric += blnValue.ToString + " "
                                        If strExpSubs.Split(";")(i - 2).Trim.StartsWith("""") Then
                                            nVal2 = strExpSubs.Split(";")(i - 2).Trim.Replace("""", "")
                                        End If
                                    Else
                                        'Value 2 
                                        nVal2 = CType(strExpSubs.Split(";")(i - 2), Decimal)
                                    End If
                                End If
                            End If
                        End If
                        If blnValue Then
                            If strExpSubs.Split(";")(i - 1) = "+" Then
                                objValue += nVal1
                            ElseIf strExpSubs.Split(";")(i - 1) = "-" Then
                                objValue -= nVal1
                            ElseIf strExpSubs.Split(";")(i - 1) = "*" Then
                                objValue *= nVal1
                            ElseIf strExpSubs.Split(";")(i - 1) = "/" Then
                                objValue /= nVal1
                            End If
                            mstrSelect = objValue.ToString
                        Else
                            If nVal1 IsNot Nothing Then
                                If strExpSubs.Split(";")(i - 1) = "+" Then
                                    strString = nVal1.ToString
                                ElseIf strExpSubs.Split(";")(i - 1) = "-" Then
                                    strString = nVal1.ToString
                                ElseIf strExpSubs.Split(";")(i - 1) = "*" Then
                                    strString = nVal1.ToString
                                ElseIf strExpSubs.Split(";")(i - 1) = "/" Then
                                    strString = nVal1.ToString
                                End If
                            End If
                            mstrSelect += strString.ToString
                        End If
                    End If
                Next
            Else
                If mdtTable.Columns.Contains(strExpSubs.Split(";")(0).Trim) Then
                    nNos = mdtTable.Rows(mintRow)(strExpSubs.Split(";")(0).Trim)
                    If mdtTable.Rows(mintRow)(strExpSubs.Split(";")(0).Trim).GetType.Name = "String" Then
                        mstrChkNumeric = "False"
                    End If
                    mstrSelect += nNos.ToString
                Else
                    If IsNumeric(strExpSubs.Split(";")(0).Trim) Then
                        nNos = strExpSubs.Split(";")(0).Trim
                        mstrSelect += nNos.ToString
                    Else
                        strString = strExpSubs.Split(";")(0).Trim.Replace("""", "")
                        mstrChkNumeric = "False"
                        mstrSelect += strString
                    End If
                End If
            End If
            'Format Checking
            If mstrFormat <> "" Then
                If IsNumeric(mstrFormat) Then
                    If mstrFormat.Trim = 0 Then
                        mstrSelect = CInt(mstrSelect).ToString
                    Else
                        mstrSelect = Str(mstrSelect, 14, mstrFormat).ToString.Trim
                    End If
                Else
                    Dim sNo As String = ""
                    sNo = mstrSelect
                    mstrSelect = InWords(CInt(mstrSelect), "", "")
                    If mstrFormat.Split("|").Length > 1 Then
                        If mstrFormat.Split("|")(1) <> 0 Then
                            If sNo.Split(".").Length > 1 Then
                                If CType(sNo.Split(".")(1), Integer) <> 0 Then
                                    sNo = Str(sNo, 14, mstrFormat.Split("|")(1)).ToString.Trim
                                    mstrSelect += " " + sNo.Split(".")(1).ToString.Trim & "/" & "1" & replicate("0", CInt(mstrFormat.Split("|")(1)))
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "InsideFormulaExec", mstrModuleName)
        End Try
    End Sub

    Private Sub Reset()
        Try
            txtExpression.Text = ""
            btnSave.Enabled = False
            btnIsNumeric.Enabled = False
            chkContinousSum.Checked = False
            chkContinousSum.Enabled = False
            chkDecimal.Checked = False
            chkDecimal.Enabled = False
            chkInWord.Checked = False
            chkInWord.Enabled = False
            cboDecimalPlaces.Text = 0
            cboWordDec.Text = 0

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Reset", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form "

    Private Sub frmFormula_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            tvReportFields.Dispose()
            'Dispose()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmFormula_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFormula_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Checking Format
            If mstrFormat <> "" Then
                If IsNumeric(mstrFormat) Then
                    chkDecimal.Checked = True
                    cboDecimalPlaces.Text = mstrFormat
                    chkInWord.Checked = False
                    cboWordDec.Enabled = False
                    'Manish Tanna (15 May 2012) -- start 
                    chkContinousSum.Enabled = False
                    'Manish Tanna (15 May 2012) -- end
                    cboWordDec.Text = 0
                Else
                    chkInWord.Checked = True
                    chkDecimal.Checked = False
                    cboWordDec.Enabled = True
                    cboDecimalPlaces.Enabled = False
                    'Manish Tanna (15 May 2012) -- start 
                    chkContinousSum.Enabled = False
                    'Manish Tanna (15 May 2012) -- end
                    If mstrFormat.Split("|").Length > 1 Then
                        cboWordDec.Text = mstrFormat.Split("|")(1)
                    Else
                        cboWordDec.Text = 0
                    End If
                    cboDecimalPlaces.Text = 0
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmFormula_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnFlag = False
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If txtExpression.Text.Trim <> "" Then
                mblnFlag = True
                mstrExpression = txtExpression.Text
                mstrFormat = ""

                Call ExecuteFormula()

                If chkDecimal.Enabled And chkInWord.Enabled Then
                    If chkDecimal.Checked Then
                        mstrFormat = cboDecimalPlaces.Text
                        If cboDecimalPlaces.Text.Trim = 0 Then
                            mstrSelect = CInt(mstrSelect).ToString
                        Else
                            mstrSelect = Str(mstrSelect, 14, cboDecimalPlaces.Text).ToString.Trim
                        End If
                    Else
                        mstrFormat = chkInWord.Checked.ToString + "|" + cboWordDec.Text
                        mstrSelect = InWords(mstrSelect, "", "")
                    End If
                End If
            Else
                mblnFlag = False
            End If

            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExecute.Click
        Try
            Call ExecuteFormula()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnExecute_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNumeric_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIsNumeric.Click
        Try
            mstrChkNumeric = ""

            Call ExecuteFormula()

            If Not mstrChkNumeric.Contains("False") Then
                'grpNumericFormat.Enabled = True
                'Manish Tanna (15 May 2012) -- start 
                chkContinousSum.Enabled = True
                'Manish Tanna (15 May 2012) -- start 

                'Dipti (06 June 2012) - Start
                chkDecimal.Enabled = True
                chkInWord.Enabled = True
                'Dipti (06 June 2012) - End

            Else
                chkDecimal.Checked = False
                'Manish Tanna (15 May 2012) -- start 
                chkContinousSum.Enabled = False
                'Manish Tanna (15 May 2012) -- end
                cboDecimalPlaces.Text = 0
                chkInWord.Checked = False
                'grpNumericFormat.Enabled = False

                'Dipti (06 June 2012) - Start
                chkDecimal.Enabled = False
                chkInWord.Enabled = False
                'Dipti (06 June 2012) - End

                eZeeMsgBox.Show("Formula is not Numeric", enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNumeric_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub chkDecimal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDecimal.CheckedChanged, chkInWord.CheckedChanged
        Try
            If CType(sender, ToolStripButton).Name = chkDecimal.Name Then
                chkInWord.Checked = False
                cboWordDec.Enabled = False
                cboDecimalPlaces.Enabled = chkDecimal.Checked
                cboWordDec.Text = 0
                chkInWord.Image = My.Resources.CheckBoxNone_24
                If chkDecimal.Checked Then
                    chkDecimal.Image = My.Resources.CheckBoxSelect_24
                Else
                    chkDecimal.Image = My.Resources.CheckBoxNone_24
                End If
            Else
                chkDecimal.Checked = False
                cboDecimalPlaces.Enabled = False
                cboWordDec.Enabled = chkInWord.Checked
                cboDecimalPlaces.Text = 0

                chkDecimal.Image = My.Resources.CheckBoxNone_24
                If chkInWord.Checked Then
                    chkInWord.Image = My.Resources.CheckBoxSelect_24
                Else
                    chkInWord.Image = My.Resources.CheckBoxNone_24
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "chkDecimal_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        'txtExp.Text = ""
        ''txtExpback.Text = ""
        'btnOk.Enabled = False
        'btnNumeric.Enabled = False
        Try
            Call Reset()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClear_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Controls "

#Region " Tree View "

    Private Sub tvControl_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvReportFields.DoubleClick
        Try
            If tvReportFields.SelectedNode.Text <> lblTree.Text Then
                txtExpression.Text += tvReportFields.SelectedNode.Text + " "
            End If
            'txtExpback.Text += tvControl.SelectedNode.Tag + " "
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "tvControl_DoubleClick", mstrModuleName)
        End Try
    End Sub

    Private Sub tvOperators_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvOperators.DoubleClick
        Try
            txtExpression.Text += tvOperators.SelectedNode.Tag + " "
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "tvOperators_DoubleClick", mstrModuleName)
        End Try
    End Sub

#End Region

    Private Sub txtExpression_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtExpression.DragDrop
        Try
            txtExpression.Text += e.Data.GetData(System.Windows.Forms.DataFormats.Text) + " "
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "txtExpression_DragDrop", mstrModuleName)
        End Try
    End Sub

    Private Sub txtExpression_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtExpression.DragEnter
        Try
            If (e.Data.GetDataPresent(DataFormats.Text)) Then
                e.Effect = DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "txtExpression_DragEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub txtExp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtExpression.TextChanged
        txtExpback.Text = sender.text.ToString.Replace("+", ";+;")
        txtExpback.Text = txtExpback.Text.ToString.Replace("-", ";-;")
        txtExpback.Text = txtExpback.Text.ToString.Replace("*", ";*;")
        txtExpback.Text = txtExpback.Text.ToString.Replace("/", ";/;")
        txtExpback.Text = txtExpback.Text.ToString.Replace("iif(", "ôiif(ô").Replace("Iif(", "ôiif(ô").Replace("IIf(", "ôiif(ô").Replace("IIF(", "ôiif(ô").Replace("iIF(", "ôiif(ô").Replace("IiF(", "ôiif(ô").Replace("iIf(", "ôiif(ô").Replace("iiF(", "ôiif(ô").Replace(",", "ô,ô").Replace(")", "ô)ô")
        txtExpback.Text = txtExpback.Text.ToString.Replace("=", "ö=ö").Replace(">", "ö>ö").Replace("<", "ö<ö")
    End Sub

    Private Sub tvControls_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles tvReportFields.ItemDrag
        If e.Button = Windows.Forms.MouseButtons.Right Then Exit Sub
        mtnSelectedNode = CType(e.Item, TreeNode)
        tvReportFields.SelectedNode = mtnSelectedNode
        tvReportFields.DoDragDrop(tvReportFields.SelectedNode.Text, DragDropEffects.Copy Or DragDropEffects.Move)
    End Sub

    Private Sub tvOperators_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles tvOperators.ItemDrag
        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then Exit Sub
            mtnSelectedNode = CType(e.Item, TreeNode)
            tvOperators.SelectedNode = mtnSelectedNode
            tvOperators.DoDragDrop(tvOperators.SelectedNode.Tag, DragDropEffects.Copy Or DragDropEffects.Move)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "tvOperators_ItemDrag", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Old Code "
    'Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click, btnDivide.Click, btnMinus.Click, btnMulitiply.Click
    '    txtExp.Text += sender.Text + " "
    '    'txtExpback.Text += ";" + sender.Text + ";"
    'End Sub

    'Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    '    txtExp.Text = ""
    '    'txtExpback.Text = ""
    '    btnOk.Enabled = False
    '    btnNumeric.Enabled = False
    'End Sub
#End Region



End Class