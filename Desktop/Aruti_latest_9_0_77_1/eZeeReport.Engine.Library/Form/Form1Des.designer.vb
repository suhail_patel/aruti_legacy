<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1Des
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1Des))
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.pnlVScroll = New System.Windows.Forms.Panel
        Me.pnlCanvasRuler = New System.Windows.Forms.Panel
        Me.pnlCompHeaderRuler = New System.Windows.Forms.Panel
        Me.pnlGrpFooterRuler1 = New System.Windows.Forms.Panel
        Me.pnlHeaderRuler = New System.Windows.Forms.Panel
        Me.pnlGrpRuler1 = New System.Windows.Forms.Panel
        Me.pnlFooterRuler = New System.Windows.Forms.Panel
        Me.pnlDetFooterRuler = New System.Windows.Forms.Panel
        Me.pnlDetailRuler = New System.Windows.Forms.Panel
        Me.pnlRuler = New System.Windows.Forms.Panel
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.pnlToolBar = New System.Windows.Forms.Panel
        Me.tb_Designer_ToolBar = New System.Windows.Forms.ToolBar
        Me.edit_Delete_Button = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.format_Align_Left = New System.Windows.Forms.ToolBarButton
        Me.format_Valign_Button = New System.Windows.Forms.ToolBarButton
        Me.format_Align_Right_Button = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.format_Align_Top_Button = New System.Windows.Forms.ToolBarButton
        Me.format_Halign_Button = New System.Windows.Forms.ToolBarButton
        Me.format_Align_Bottom_Button = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.format_Same_Width_Button = New System.Windows.Forms.ToolBarButton
        Me.format_Same_Height_Button = New System.Windows.Forms.ToolBarButton
        Me.format_Same_Size_Button = New System.Windows.Forms.ToolBarButton
        Me.toolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.edit_Cut_Button = New System.Windows.Forms.ToolBarButton
        Me.edit_Copy_Button = New System.Windows.Forms.ToolBarButton
        Me.edit_Paste_Button = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.format_Box = New System.Windows.Forms.ToolBarButton
        Me.format_Line = New System.Windows.Forms.ToolBarButton
        Me.ilToolBarImage = New System.Windows.Forms.ImageList(Me.components)
        Me.chkSnapGrid = New System.Windows.Forms.CheckBox
        Me.pnlMainCanvas = New System.Windows.Forms.Panel
        Me.pnlCanvas = New System.Windows.Forms.Panel
        Me.spltFooter = New System.Windows.Forms.Splitter
        Me.pnlFooter = New System.Windows.Forms.Panel
        Me.lblBottomFooter = New System.Windows.Forms.Label
        Me.cmslbl = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmSuppress = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_PageSetup = New System.Windows.Forms.ToolStripMenuItem
        Me.spltDetailFooter = New System.Windows.Forms.Splitter
        Me.pnlDetailFooter = New System.Windows.Forms.Panel
        Me.lblFooter = New System.Windows.Forms.Label
        Me.spltGroupFooter1 = New System.Windows.Forms.Splitter
        Me.pnlGroupFooter1 = New System.Windows.Forms.Panel
        Me.lblGroupFooter1 = New System.Windows.Forms.Label
        Me.spltDetail = New System.Windows.Forms.Splitter
        Me.pnlDetail = New System.Windows.Forms.Panel
        Me.lblDetail = New System.Windows.Forms.Label
        Me.spltGoup1 = New System.Windows.Forms.Splitter
        Me.pnlGroup1 = New System.Windows.Forms.Panel
        Me.lblGroup1 = New System.Windows.Forms.Label
        Me.spltHeader = New System.Windows.Forms.Splitter
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.spltCompHeader = New System.Windows.Forms.Splitter
        Me.pnlCompHeader = New System.Windows.Forms.Panel
        Me.lblCompHeader = New System.Windows.Forms.Label
        Me.chkGrid = New System.Windows.Forms.CheckBox
        Me.tvControls = New System.Windows.Forms.TreeView
        Me.cboField = New System.Windows.Forms.ComboBox
        Me.objlblProperty = New System.Windows.Forms.Label
        Me.lblType = New System.Windows.Forms.Label
        Me.cmbType = New System.Windows.Forms.ComboBox
        Me.txtItemPrint = New System.Windows.Forms.TextBox
        Me.lblItemPrint = New System.Windows.Forms.Label
        Me.txtMstXMLPath = New System.Windows.Forms.TextBox
        Me.lblXML = New System.Windows.Forms.Label
        Me.btnMstPath = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.oPrintDoc = New System.Drawing.Printing.PrintDocument
        Me.cms = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cms_Copy = New System.Windows.Forms.ToolStripMenuItem
        Me.cms_Paste = New System.Windows.Forms.ToolStripMenuItem
        Me.cms_Delete = New System.Windows.Forms.ToolStripMenuItem
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.btnDetPath = New System.Windows.Forms.Button
        Me.txtDetXMLPath = New System.Windows.Forms.TextBox
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer
        Me.tlpRight = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.btnCloseR = New System.Windows.Forms.Button
        Me.btnMinMaxR = New System.Windows.Forms.Button
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.pgTemplateField = New ArutiReport.Engine.Library.TemplatePropertyGrid
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel
        Me.pnlPrint = New System.Windows.Forms.Panel
        Me.tlpLeft = New System.Windows.Forms.TableLayoutPanel
        Me.tlpRow0 = New System.Windows.Forms.TableLayoutPanel
        Me.lblPrintingTools = New System.Windows.Forms.Label
        Me.btnClosel = New System.Windows.Forms.Button
        Me.btnMaxMinl = New System.Windows.Forms.Button
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.chkSaveAsNew = New System.Windows.Forms.CheckBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtCanvasWidth = New System.Windows.Forms.TextBox
        Me.chkLandspace = New System.Windows.Forms.CheckBox
        Me.chkRepHeader = New System.Windows.Forms.CheckBox
        Me.txtMultPrint = New System.Windows.Forms.TextBox
        Me.lblMultPrint = New System.Windows.Forms.Label
        Me.chkPageWise = New System.Windows.Forms.CheckBox
        Me.btnConverter = New System.Windows.Forms.ToolStrip
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.WordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExeclToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PdfToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HTMLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Button1 = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnLoad = New System.Windows.Forms.Button
        Me.btnClearData = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.pnlVScroll.SuspendLayout()
        Me.pnlCanvasRuler.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlToolBar.SuspendLayout()
        Me.pnlMainCanvas.SuspendLayout()
        Me.pnlCanvas.SuspendLayout()
        Me.cmslbl.SuspendLayout()
        Me.cms.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.ToolStripContainer1.ContentPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        Me.tlpRight.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.pnlPrint.SuspendLayout()
        Me.tlpLeft.SuspendLayout()
        Me.tlpRow0.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.btnConverter.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Location = New System.Drawing.Point(81, 45)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.pnlVScroll)
        Me.SplitContainer1.Panel1MinSize = 10
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.pnlRuler)
        Me.SplitContainer1.Panel2.Controls.Add(Me.PictureBox1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.pnlToolBar)
        Me.SplitContainer1.Panel2.Controls.Add(Me.chkSnapGrid)
        Me.SplitContainer1.Panel2.Controls.Add(Me.pnlMainCanvas)
        Me.SplitContainer1.Panel2.Controls.Add(Me.chkGrid)
        Me.SplitContainer1.Size = New System.Drawing.Size(658, 580)
        Me.SplitContainer1.SplitterDistance = 36
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 38
        '
        'pnlVScroll
        '
        Me.pnlVScroll.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlVScroll.AutoScroll = True
        Me.pnlVScroll.Controls.Add(Me.pnlCanvasRuler)
        Me.pnlVScroll.Location = New System.Drawing.Point(3, 56)
        Me.pnlVScroll.Name = "pnlVScroll"
        Me.pnlVScroll.Size = New System.Drawing.Size(55, 530)
        Me.pnlVScroll.TabIndex = 4
        '
        'pnlCanvasRuler
        '
        Me.pnlCanvasRuler.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlCanvasRuler.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCanvasRuler.Controls.Add(Me.pnlCompHeaderRuler)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlGrpFooterRuler1)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlHeaderRuler)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlGrpRuler1)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlFooterRuler)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlDetFooterRuler)
        Me.pnlCanvasRuler.Controls.Add(Me.pnlDetailRuler)
        Me.pnlCanvasRuler.Location = New System.Drawing.Point(3, 2)
        Me.pnlCanvasRuler.Name = "pnlCanvasRuler"
        Me.pnlCanvasRuler.Size = New System.Drawing.Size(37, 700)
        Me.pnlCanvasRuler.TabIndex = 2
        '
        'pnlCompHeaderRuler
        '
        Me.pnlCompHeaderRuler.Location = New System.Drawing.Point(3, 24)
        Me.pnlCompHeaderRuler.Name = "pnlCompHeaderRuler"
        Me.pnlCompHeaderRuler.Size = New System.Drawing.Size(27, 91)
        Me.pnlCompHeaderRuler.TabIndex = 1
        '
        'pnlGrpFooterRuler1
        '
        Me.pnlGrpFooterRuler1.Location = New System.Drawing.Point(3, 406)
        Me.pnlGrpFooterRuler1.Name = "pnlGrpFooterRuler1"
        Me.pnlGrpFooterRuler1.Size = New System.Drawing.Size(27, 30)
        Me.pnlGrpFooterRuler1.TabIndex = 2
        '
        'pnlHeaderRuler
        '
        Me.pnlHeaderRuler.Location = New System.Drawing.Point(3, 141)
        Me.pnlHeaderRuler.Name = "pnlHeaderRuler"
        Me.pnlHeaderRuler.Size = New System.Drawing.Size(27, 69)
        Me.pnlHeaderRuler.TabIndex = 0
        '
        'pnlGrpRuler1
        '
        Me.pnlGrpRuler1.Location = New System.Drawing.Point(3, 234)
        Me.pnlGrpRuler1.Name = "pnlGrpRuler1"
        Me.pnlGrpRuler1.Size = New System.Drawing.Size(27, 32)
        Me.pnlGrpRuler1.TabIndex = 1
        '
        'pnlFooterRuler
        '
        Me.pnlFooterRuler.Location = New System.Drawing.Point(1, 524)
        Me.pnlFooterRuler.Name = "pnlFooterRuler"
        Me.pnlFooterRuler.Size = New System.Drawing.Size(27, 105)
        Me.pnlFooterRuler.TabIndex = 1
        '
        'pnlDetFooterRuler
        '
        Me.pnlDetFooterRuler.Location = New System.Drawing.Point(3, 455)
        Me.pnlDetFooterRuler.Name = "pnlDetFooterRuler"
        Me.pnlDetFooterRuler.Size = New System.Drawing.Size(27, 46)
        Me.pnlDetFooterRuler.TabIndex = 2
        '
        'pnlDetailRuler
        '
        Me.pnlDetailRuler.Location = New System.Drawing.Point(3, 293)
        Me.pnlDetailRuler.Name = "pnlDetailRuler"
        Me.pnlDetailRuler.Size = New System.Drawing.Size(27, 90)
        Me.pnlDetailRuler.TabIndex = 1
        '
        'pnlRuler
        '
        Me.pnlRuler.BackColor = System.Drawing.Color.Transparent
        Me.pnlRuler.Location = New System.Drawing.Point(5, 31)
        Me.pnlRuler.Name = "pnlRuler"
        Me.pnlRuler.Size = New System.Drawing.Size(930, 23)
        Me.pnlRuler.TabIndex = 30
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox1.TabIndex = 29
        Me.PictureBox1.TabStop = False
        '
        'pnlToolBar
        '
        Me.pnlToolBar.Controls.Add(Me.tb_Designer_ToolBar)
        Me.pnlToolBar.Location = New System.Drawing.Point(31, 4)
        Me.pnlToolBar.Name = "pnlToolBar"
        Me.pnlToolBar.Size = New System.Drawing.Size(421, 23)
        Me.pnlToolBar.TabIndex = 24
        '
        'tb_Designer_ToolBar
        '
        Me.tb_Designer_ToolBar.AllowDrop = True
        Me.tb_Designer_ToolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.tb_Designer_ToolBar.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.edit_Delete_Button, Me.ToolBarButton1, Me.format_Align_Left, Me.format_Valign_Button, Me.format_Align_Right_Button, Me.ToolBarButton2, Me.format_Align_Top_Button, Me.format_Halign_Button, Me.format_Align_Bottom_Button, Me.ToolBarButton3, Me.format_Same_Width_Button, Me.format_Same_Height_Button, Me.format_Same_Size_Button, Me.toolBarButton4, Me.edit_Cut_Button, Me.edit_Copy_Button, Me.edit_Paste_Button, Me.ToolBarButton7, Me.format_Box, Me.format_Line})
        Me.tb_Designer_ToolBar.DropDownArrows = True
        Me.tb_Designer_ToolBar.ImageList = Me.ilToolBarImage
        Me.tb_Designer_ToolBar.Location = New System.Drawing.Point(0, 0)
        Me.tb_Designer_ToolBar.Name = "tb_Designer_ToolBar"
        Me.tb_Designer_ToolBar.ShowToolTips = True
        Me.tb_Designer_ToolBar.Size = New System.Drawing.Size(421, 28)
        Me.tb_Designer_ToolBar.TabIndex = 0
        '
        'edit_Delete_Button
        '
        Me.edit_Delete_Button.ImageIndex = 14
        Me.edit_Delete_Button.Name = "edit_Delete_Button"
        Me.edit_Delete_Button.ToolTipText = "Delete selected items"
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.Name = "ToolBarButton1"
        Me.ToolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'format_Align_Left
        '
        Me.format_Align_Left.ImageIndex = 4
        Me.format_Align_Left.Name = "format_Align_Left"
        Me.format_Align_Left.ToolTipText = "Align Lefts"
        '
        'format_Valign_Button
        '
        Me.format_Valign_Button.ImageIndex = 3
        Me.format_Valign_Button.Name = "format_Valign_Button"
        Me.format_Valign_Button.ToolTipText = "Align Centers"
        '
        'format_Align_Right_Button
        '
        Me.format_Align_Right_Button.ImageIndex = 5
        Me.format_Align_Right_Button.Name = "format_Align_Right_Button"
        Me.format_Align_Right_Button.ToolTipText = "Align Rights"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.Name = "ToolBarButton2"
        Me.ToolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'format_Align_Top_Button
        '
        Me.format_Align_Top_Button.ImageIndex = 6
        Me.format_Align_Top_Button.Name = "format_Align_Top_Button"
        Me.format_Align_Top_Button.ToolTipText = "Align Tops"
        '
        'format_Halign_Button
        '
        Me.format_Halign_Button.ImageIndex = 7
        Me.format_Halign_Button.Name = "format_Halign_Button"
        Me.format_Halign_Button.ToolTipText = "Align Middles"
        '
        'format_Align_Bottom_Button
        '
        Me.format_Align_Bottom_Button.ImageIndex = 16
        Me.format_Align_Bottom_Button.Name = "format_Align_Bottom_Button"
        Me.format_Align_Bottom_Button.ToolTipText = "Align Bottoms"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.Name = "ToolBarButton3"
        Me.ToolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'format_Same_Width_Button
        '
        Me.format_Same_Width_Button.ImageIndex = 11
        Me.format_Same_Width_Button.Name = "format_Same_Width_Button"
        Me.format_Same_Width_Button.ToolTipText = "Make Same Width"
        '
        'format_Same_Height_Button
        '
        Me.format_Same_Height_Button.ImageIndex = 8
        Me.format_Same_Height_Button.Name = "format_Same_Height_Button"
        Me.format_Same_Height_Button.ToolTipText = "Make Same Height"
        '
        'format_Same_Size_Button
        '
        Me.format_Same_Size_Button.ImageIndex = 10
        Me.format_Same_Size_Button.Name = "format_Same_Size_Button"
        Me.format_Same_Size_Button.ToolTipText = "Make Same Size"
        '
        'toolBarButton4
        '
        Me.toolBarButton4.Name = "toolBarButton4"
        Me.toolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'edit_Cut_Button
        '
        Me.edit_Cut_Button.Enabled = False
        Me.edit_Cut_Button.ImageIndex = 2
        Me.edit_Cut_Button.Name = "edit_Cut_Button"
        Me.edit_Cut_Button.ToolTipText = "Cut selected items"
        '
        'edit_Copy_Button
        '
        Me.edit_Copy_Button.Enabled = False
        Me.edit_Copy_Button.ImageIndex = 1
        Me.edit_Copy_Button.Name = "edit_Copy_Button"
        Me.edit_Copy_Button.ToolTipText = "Copy selected items"
        '
        'edit_Paste_Button
        '
        Me.edit_Paste_Button.Enabled = False
        Me.edit_Paste_Button.ImageIndex = 0
        Me.edit_Paste_Button.Name = "edit_Paste_Button"
        Me.edit_Paste_Button.ToolTipText = "Paste selected items"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.Name = "ToolBarButton7"
        Me.ToolBarButton7.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'format_Box
        '
        Me.format_Box.ImageIndex = 13
        Me.format_Box.Name = "format_Box"
        Me.format_Box.Visible = False
        '
        'format_Line
        '
        Me.format_Line.ImageIndex = 15
        Me.format_Line.Name = "format_Line"
        Me.format_Line.Visible = False
        '
        'ilToolBarImage
        '
        Me.ilToolBarImage.ImageStream = CType(resources.GetObject("ilToolBarImage.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilToolBarImage.TransparentColor = System.Drawing.Color.Transparent
        Me.ilToolBarImage.Images.SetKeyName(0, "paste_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(1, "copy_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(2, "cut_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(3, "format_align_hcentre_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(4, "format_align_left_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(5, "format_align_right_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(6, "format_align_top_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(7, "format_align_vcentre_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(8, "format_same_height_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(9, "format_same_height_min_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(10, "format_same_size_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(11, "format_same_width_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(12, "format_same_width_min_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(13, "box_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(14, "delete_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(15, "line_16x16.gif")
        Me.ilToolBarImage.Images.SetKeyName(16, "format_align_bottom_16x16.gif")
        '
        'chkSnapGrid
        '
        Me.chkSnapGrid.AutoSize = True
        Me.chkSnapGrid.Location = New System.Drawing.Point(517, 8)
        Me.chkSnapGrid.Name = "chkSnapGrid"
        Me.chkSnapGrid.Size = New System.Drawing.Size(85, 17)
        Me.chkSnapGrid.TabIndex = 1
        Me.chkSnapGrid.Text = "Snap to Grid"
        Me.chkSnapGrid.UseVisualStyleBackColor = True
        '
        'pnlMainCanvas
        '
        Me.pnlMainCanvas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMainCanvas.AutoScroll = True
        Me.pnlMainCanvas.BackColor = System.Drawing.Color.DimGray
        Me.pnlMainCanvas.Controls.Add(Me.pnlCanvas)
        Me.pnlMainCanvas.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMainCanvas.Location = New System.Drawing.Point(3, 56)
        Me.pnlMainCanvas.Name = "pnlMainCanvas"
        Me.pnlMainCanvas.Size = New System.Drawing.Size(612, 533)
        Me.pnlMainCanvas.TabIndex = 1
        '
        'pnlCanvas
        '
        Me.pnlCanvas.BackColor = System.Drawing.Color.Gray
        Me.pnlCanvas.Controls.Add(Me.spltFooter)
        Me.pnlCanvas.Controls.Add(Me.pnlFooter)
        Me.pnlCanvas.Controls.Add(Me.lblBottomFooter)
        Me.pnlCanvas.Controls.Add(Me.spltDetailFooter)
        Me.pnlCanvas.Controls.Add(Me.pnlDetailFooter)
        Me.pnlCanvas.Controls.Add(Me.lblFooter)
        Me.pnlCanvas.Controls.Add(Me.spltGroupFooter1)
        Me.pnlCanvas.Controls.Add(Me.pnlGroupFooter1)
        Me.pnlCanvas.Controls.Add(Me.lblGroupFooter1)
        Me.pnlCanvas.Controls.Add(Me.spltDetail)
        Me.pnlCanvas.Controls.Add(Me.pnlDetail)
        Me.pnlCanvas.Controls.Add(Me.lblDetail)
        Me.pnlCanvas.Controls.Add(Me.spltGoup1)
        Me.pnlCanvas.Controls.Add(Me.pnlGroup1)
        Me.pnlCanvas.Controls.Add(Me.lblGroup1)
        Me.pnlCanvas.Controls.Add(Me.spltHeader)
        Me.pnlCanvas.Controls.Add(Me.pnlHeader)
        Me.pnlCanvas.Controls.Add(Me.lblHeader)
        Me.pnlCanvas.Controls.Add(Me.spltCompHeader)
        Me.pnlCanvas.Controls.Add(Me.pnlCompHeader)
        Me.pnlCanvas.Controls.Add(Me.lblCompHeader)
        Me.pnlCanvas.Location = New System.Drawing.Point(-2, 0)
        Me.pnlCanvas.Name = "pnlCanvas"
        Me.pnlCanvas.Padding = New System.Windows.Forms.Padding(3)
        Me.pnlCanvas.Size = New System.Drawing.Size(614, 1000)
        Me.pnlCanvas.TabIndex = 0
        Me.pnlCanvas.Tag = "Canvas"
        '
        'spltFooter
        '
        Me.spltFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltFooter.Location = New System.Drawing.Point(3, 625)
        Me.spltFooter.MinExtra = 0
        Me.spltFooter.MinSize = 0
        Me.spltFooter.Name = "spltFooter"
        Me.spltFooter.Size = New System.Drawing.Size(608, 3)
        Me.spltFooter.TabIndex = 14
        Me.spltFooter.TabStop = False
        '
        'pnlFooter
        '
        Me.pnlFooter.AllowDrop = True
        Me.pnlFooter.BackColor = System.Drawing.Color.White
        Me.pnlFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlFooter.Location = New System.Drawing.Point(3, 520)
        Me.pnlFooter.Name = "pnlFooter"
        Me.pnlFooter.Size = New System.Drawing.Size(608, 105)
        Me.pnlFooter.TabIndex = 11
        '
        'lblBottomFooter
        '
        Me.lblBottomFooter.BackColor = System.Drawing.Color.Gainsboro
        Me.lblBottomFooter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBottomFooter.ContextMenuStrip = Me.cmslbl
        Me.lblBottomFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblBottomFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBottomFooter.Location = New System.Drawing.Point(3, 503)
        Me.lblBottomFooter.Name = "lblBottomFooter"
        Me.lblBottomFooter.Size = New System.Drawing.Size(608, 17)
        Me.lblBottomFooter.TabIndex = 10
        Me.lblBottomFooter.Text = "Footer"
        Me.lblBottomFooter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmslbl
        '
        Me.cmslbl.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmSuppress, Me.tsm_PageSetup})
        Me.cmslbl.Name = "cmslbl"
        Me.cmslbl.Size = New System.Drawing.Size(141, 48)
        '
        'tsmSuppress
        '
        Me.tsmSuppress.Name = "tsmSuppress"
        Me.tsmSuppress.Size = New System.Drawing.Size(140, 22)
        Me.tsmSuppress.Text = "&Suppress"
        '
        'tsm_PageSetup
        '
        Me.tsm_PageSetup.Name = "tsm_PageSetup"
        Me.tsm_PageSetup.Size = New System.Drawing.Size(140, 22)
        Me.tsm_PageSetup.Text = "&Page Setup"
        '
        'spltDetailFooter
        '
        Me.spltDetailFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltDetailFooter.Location = New System.Drawing.Point(3, 500)
        Me.spltDetailFooter.MinExtra = 0
        Me.spltDetailFooter.MinSize = 0
        Me.spltDetailFooter.Name = "spltDetailFooter"
        Me.spltDetailFooter.Size = New System.Drawing.Size(608, 3)
        Me.spltDetailFooter.TabIndex = 11
        Me.spltDetailFooter.TabStop = False
        '
        'pnlDetailFooter
        '
        Me.pnlDetailFooter.AllowDrop = True
        Me.pnlDetailFooter.BackColor = System.Drawing.Color.White
        Me.pnlDetailFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlDetailFooter.Location = New System.Drawing.Point(3, 457)
        Me.pnlDetailFooter.Name = "pnlDetailFooter"
        Me.pnlDetailFooter.Size = New System.Drawing.Size(608, 43)
        Me.pnlDetailFooter.TabIndex = 9
        '
        'lblFooter
        '
        Me.lblFooter.BackColor = System.Drawing.Color.Gainsboro
        Me.lblFooter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFooter.ContextMenuStrip = Me.cmslbl
        Me.lblFooter.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFooter.Location = New System.Drawing.Point(3, 440)
        Me.lblFooter.Name = "lblFooter"
        Me.lblFooter.Size = New System.Drawing.Size(608, 17)
        Me.lblFooter.TabIndex = 8
        Me.lblFooter.Text = "Detail Footer"
        Me.lblFooter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltGroupFooter1
        '
        Me.spltGroupFooter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltGroupFooter1.Location = New System.Drawing.Point(3, 438)
        Me.spltGroupFooter1.MinExtra = 0
        Me.spltGroupFooter1.MinSize = 0
        Me.spltGroupFooter1.Name = "spltGroupFooter1"
        Me.spltGroupFooter1.Size = New System.Drawing.Size(608, 2)
        Me.spltGroupFooter1.TabIndex = 20
        Me.spltGroupFooter1.TabStop = False
        '
        'pnlGroupFooter1
        '
        Me.pnlGroupFooter1.AllowDrop = True
        Me.pnlGroupFooter1.BackColor = System.Drawing.Color.White
        Me.pnlGroupFooter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlGroupFooter1.Location = New System.Drawing.Point(3, 408)
        Me.pnlGroupFooter1.Name = "pnlGroupFooter1"
        Me.pnlGroupFooter1.Size = New System.Drawing.Size(608, 30)
        Me.pnlGroupFooter1.TabIndex = 19
        '
        'lblGroupFooter1
        '
        Me.lblGroupFooter1.BackColor = System.Drawing.Color.Gainsboro
        Me.lblGroupFooter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGroupFooter1.ContextMenuStrip = Me.cmslbl
        Me.lblGroupFooter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblGroupFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroupFooter1.Location = New System.Drawing.Point(3, 388)
        Me.lblGroupFooter1.Name = "lblGroupFooter1"
        Me.lblGroupFooter1.Size = New System.Drawing.Size(608, 20)
        Me.lblGroupFooter1.TabIndex = 9
        Me.lblGroupFooter1.Text = "Group Footer 1"
        Me.lblGroupFooter1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltDetail
        '
        Me.spltDetail.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltDetail.Location = New System.Drawing.Point(3, 385)
        Me.spltDetail.MinExtra = 0
        Me.spltDetail.MinSize = 0
        Me.spltDetail.Name = "spltDetail"
        Me.spltDetail.Size = New System.Drawing.Size(608, 3)
        Me.spltDetail.TabIndex = 1
        Me.spltDetail.TabStop = False
        '
        'pnlDetail
        '
        Me.pnlDetail.AllowDrop = True
        Me.pnlDetail.BackColor = System.Drawing.Color.White
        Me.pnlDetail.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlDetail.Location = New System.Drawing.Point(3, 295)
        Me.pnlDetail.Name = "pnlDetail"
        Me.pnlDetail.Size = New System.Drawing.Size(608, 90)
        Me.pnlDetail.TabIndex = 7
        '
        'lblDetail
        '
        Me.lblDetail.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDetail.ContextMenuStrip = Me.cmslbl
        Me.lblDetail.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetail.Location = New System.Drawing.Point(3, 268)
        Me.lblDetail.Name = "lblDetail"
        Me.lblDetail.Size = New System.Drawing.Size(608, 27)
        Me.lblDetail.TabIndex = 4
        Me.lblDetail.Text = "Detail"
        Me.lblDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltGoup1
        '
        Me.spltGoup1.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltGoup1.Location = New System.Drawing.Point(3, 266)
        Me.spltGoup1.MinExtra = 0
        Me.spltGoup1.MinSize = 0
        Me.spltGoup1.Name = "spltGoup1"
        Me.spltGoup1.Size = New System.Drawing.Size(608, 2)
        Me.spltGoup1.TabIndex = 18
        Me.spltGoup1.TabStop = False
        '
        'pnlGroup1
        '
        Me.pnlGroup1.AllowDrop = True
        Me.pnlGroup1.BackColor = System.Drawing.Color.White
        Me.pnlGroup1.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlGroup1.Location = New System.Drawing.Point(3, 234)
        Me.pnlGroup1.Name = "pnlGroup1"
        Me.pnlGroup1.Size = New System.Drawing.Size(608, 32)
        Me.pnlGroup1.TabIndex = 17
        '
        'lblGroup1
        '
        Me.lblGroup1.BackColor = System.Drawing.Color.Gainsboro
        Me.lblGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGroup1.ContextMenuStrip = Me.cmslbl
        Me.lblGroup1.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblGroup1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroup1.Location = New System.Drawing.Point(3, 214)
        Me.lblGroup1.Name = "lblGroup1"
        Me.lblGroup1.Size = New System.Drawing.Size(608, 20)
        Me.lblGroup1.TabIndex = 9
        Me.lblGroup1.Text = "Group 1"
        Me.lblGroup1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltHeader
        '
        Me.spltHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltHeader.Location = New System.Drawing.Point(3, 212)
        Me.spltHeader.MinExtra = 30
        Me.spltHeader.MinSize = 0
        Me.spltHeader.Name = "spltHeader"
        Me.spltHeader.Size = New System.Drawing.Size(608, 2)
        Me.spltHeader.TabIndex = 0
        Me.spltHeader.TabStop = False
        '
        'pnlHeader
        '
        Me.pnlHeader.AllowDrop = True
        Me.pnlHeader.BackColor = System.Drawing.Color.White
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.Location = New System.Drawing.Point(3, 142)
        Me.pnlHeader.Margin = New System.Windows.Forms.Padding(10)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(608, 70)
        Me.pnlHeader.TabIndex = 0
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Gainsboro
        Me.lblHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeader.ContextMenuStrip = Me.cmslbl
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.Location = New System.Drawing.Point(3, 119)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(608, 23)
        Me.lblHeader.TabIndex = 3
        Me.lblHeader.Text = "Header"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'spltCompHeader
        '
        Me.spltCompHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.spltCompHeader.Location = New System.Drawing.Point(3, 116)
        Me.spltCompHeader.MinExtra = 0
        Me.spltCompHeader.MinSize = 0
        Me.spltCompHeader.Name = "spltCompHeader"
        Me.spltCompHeader.Size = New System.Drawing.Size(608, 3)
        Me.spltCompHeader.TabIndex = 23
        Me.spltCompHeader.TabStop = False
        '
        'pnlCompHeader
        '
        Me.pnlCompHeader.AllowDrop = True
        Me.pnlCompHeader.BackColor = System.Drawing.Color.White
        Me.pnlCompHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlCompHeader.Location = New System.Drawing.Point(3, 26)
        Me.pnlCompHeader.Margin = New System.Windows.Forms.Padding(10)
        Me.pnlCompHeader.Name = "pnlCompHeader"
        Me.pnlCompHeader.Size = New System.Drawing.Size(608, 90)
        Me.pnlCompHeader.TabIndex = 21
        '
        'lblCompHeader
        '
        Me.lblCompHeader.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCompHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCompHeader.ContextMenuStrip = Me.cmslbl
        Me.lblCompHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCompHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompHeader.Location = New System.Drawing.Point(3, 3)
        Me.lblCompHeader.Name = "lblCompHeader"
        Me.lblCompHeader.Size = New System.Drawing.Size(608, 23)
        Me.lblCompHeader.TabIndex = 22
        Me.lblCompHeader.Text = "Company Header"
        Me.lblCompHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkGrid
        '
        Me.chkGrid.AutoSize = True
        Me.chkGrid.Location = New System.Drawing.Point(466, 8)
        Me.chkGrid.Name = "chkGrid"
        Me.chkGrid.Size = New System.Drawing.Size(45, 17)
        Me.chkGrid.TabIndex = 0
        Me.chkGrid.Text = "Grid"
        Me.chkGrid.UseVisualStyleBackColor = True
        '
        'tvControls
        '
        Me.tvControls.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tvControls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tvControls.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvControls.FullRowSelect = True
        Me.tvControls.Location = New System.Drawing.Point(3, 90)
        Me.tvControls.Name = "tvControls"
        Me.tvControls.ShowNodeToolTips = True
        Me.tvControls.Size = New System.Drawing.Size(138, 445)
        Me.tvControls.TabIndex = 1
        '
        'cboField
        '
        Me.cboField.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboField.DropDownWidth = 250
        Me.cboField.FormattingEnabled = True
        Me.cboField.Location = New System.Drawing.Point(3, 34)
        Me.cboField.Name = "cboField"
        Me.cboField.Size = New System.Drawing.Size(154, 21)
        Me.cboField.TabIndex = 0
        '
        'objlblProperty
        '
        Me.objlblProperty.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlblProperty.BackColor = System.Drawing.Color.Gainsboro
        Me.objlblProperty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objlblProperty.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProperty.Location = New System.Drawing.Point(3, 0)
        Me.objlblProperty.Name = "objlblProperty"
        Me.objlblProperty.Size = New System.Drawing.Size(92, 30)
        Me.objlblProperty.TabIndex = 2
        Me.objlblProperty.Text = "Property"
        Me.objlblProperty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(3, 12)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(35, 13)
        Me.lblType.TabIndex = 39
        Me.lblType.Text = "Type"
        '
        'cmbType
        '
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.Location = New System.Drawing.Point(43, 8)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(143, 21)
        Me.cmbType.TabIndex = 40
        '
        'txtItemPrint
        '
        Me.txtItemPrint.Location = New System.Drawing.Point(103, 3)
        Me.txtItemPrint.MaxLength = 5
        Me.txtItemPrint.Name = "txtItemPrint"
        Me.txtItemPrint.Size = New System.Drawing.Size(42, 20)
        Me.txtItemPrint.TabIndex = 6
        '
        'lblItemPrint
        '
        Me.lblItemPrint.BackColor = System.Drawing.Color.Gainsboro
        Me.lblItemPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblItemPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemPrint.Location = New System.Drawing.Point(3, 0)
        Me.lblItemPrint.Name = "lblItemPrint"
        Me.lblItemPrint.Size = New System.Drawing.Size(83, 23)
        Me.lblItemPrint.TabIndex = 5
        Me.lblItemPrint.Text = "Detail Height "
        Me.lblItemPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMstXMLPath
        '
        Me.txtMstXMLPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMstXMLPath.ForeColor = System.Drawing.Color.Red
        Me.txtMstXMLPath.Location = New System.Drawing.Point(736, 10)
        Me.txtMstXMLPath.Name = "txtMstXMLPath"
        Me.txtMstXMLPath.Size = New System.Drawing.Size(143, 20)
        Me.txtMstXMLPath.TabIndex = 46
        '
        'lblXML
        '
        Me.lblXML.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblXML.ForeColor = System.Drawing.Color.OliveDrab
        Me.lblXML.Location = New System.Drawing.Point(688, 8)
        Me.lblXML.Name = "lblXML"
        Me.lblXML.Size = New System.Drawing.Size(46, 26)
        Me.lblXML.TabIndex = 47
        Me.lblXML.Text = "Master XML"
        Me.lblXML.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnMstPath
        '
        Me.btnMstPath.Enabled = False
        Me.btnMstPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMstPath.Location = New System.Drawing.Point(875, 10)
        Me.btnMstPath.Name = "btnMstPath"
        Me.btnMstPath.Size = New System.Drawing.Size(27, 20)
        Me.btnMstPath.TabIndex = 48
        Me.btnMstPath.Text = "..."
        Me.btnMstPath.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMstPath.UseVisualStyleBackColor = True
        Me.btnMstPath.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(298, 625)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(517, 14)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = " 'Ctrl + A' To Select All                          'Ctrl+U'  to UnSelect         " & _
            "                ' Shift+Delete' to Delete "
        '
        'cms
        '
        Me.cms.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cms_Copy, Me.cms_Paste, Me.cms_Delete})
        Me.cms.Name = "cms"
        Me.cms.Size = New System.Drawing.Size(117, 70)
        '
        'cms_Copy
        '
        Me.cms_Copy.Name = "cms_Copy"
        Me.cms_Copy.Size = New System.Drawing.Size(116, 22)
        Me.cms_Copy.Text = "&Copy"
        '
        'cms_Paste
        '
        Me.cms_Paste.Name = "cms_Paste"
        Me.cms_Paste.Size = New System.Drawing.Size(116, 22)
        Me.cms_Paste.Text = "&Paste"
        '
        'cms_Delete
        '
        Me.cms_Delete.Name = "cms_Delete"
        Me.cms_Delete.Size = New System.Drawing.Size(116, 22)
        Me.cms_Delete.Text = "&Delete"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label2.Location = New System.Drawing.Point(249, 622)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 15)
        Me.Label2.TabIndex = 51
        Me.Label2.Text = "Note :"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.OliveDrab
        Me.Label5.Location = New System.Drawing.Point(915, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 28)
        Me.Label5.TabIndex = 54
        Me.Label5.Text = "Detail XML"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label5.Visible = False
        '
        'btnDetPath
        '
        Me.btnDetPath.Enabled = False
        Me.btnDetPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDetPath.Location = New System.Drawing.Point(100, 7)
        Me.btnDetPath.Name = "btnDetPath"
        Me.btnDetPath.Size = New System.Drawing.Size(27, 20)
        Me.btnDetPath.TabIndex = 55
        Me.btnDetPath.Text = "..."
        Me.btnDetPath.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDetPath.UseVisualStyleBackColor = True
        Me.btnDetPath.Visible = False
        '
        'txtDetXMLPath
        '
        Me.txtDetXMLPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetXMLPath.ForeColor = System.Drawing.Color.Red
        Me.txtDetXMLPath.Location = New System.Drawing.Point(961, 10)
        Me.txtDetXMLPath.Name = "txtDetXMLPath"
        Me.txtDetXMLPath.Size = New System.Drawing.Size(149, 20)
        Me.txtDetXMLPath.TabIndex = 53
        Me.txtDetXMLPath.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.ToolStripContainer1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 58)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1014, 585)
        Me.TableLayoutPanel1.TabIndex = 56
        '
        'ToolStripContainer1
        '
        Me.ToolStripContainer1.BottomToolStripPanelVisible = False
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.tlpRight)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.pnlPrint)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.tlpLeft)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(1008, 580)
        Me.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStripContainer1.Location = New System.Drawing.Point(3, 3)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.Size = New System.Drawing.Size(1008, 580)
        Me.ToolStripContainer1.TabIndex = 0
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        Me.ToolStripContainer1.TopToolStripPanelVisible = False
        '
        'tlpRight
        '
        Me.tlpRight.ColumnCount = 1
        Me.tlpRight.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpRight.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.tlpRight.Controls.Add(Me.TableLayoutPanel4, 0, 1)
        Me.tlpRight.Dock = System.Windows.Forms.DockStyle.Left
        Me.tlpRight.Location = New System.Drawing.Point(814, 0)
        Me.tlpRight.Name = "tlpRight"
        Me.tlpRight.RowCount = 2
        Me.tlpRight.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.tlpRight.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlpRight.Size = New System.Drawing.Size(166, 580)
        Me.tlpRight.TabIndex = 43
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.btnCloseR, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.objlblProperty, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnMinMaxR, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(160, 30)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'btnCloseR
        '
        Me.btnCloseR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnCloseR.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCloseR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCloseR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseR.ForeColor = System.Drawing.Color.Black
        Me.btnCloseR.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Close_16
        Me.btnCloseR.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnCloseR.Location = New System.Drawing.Point(131, 3)
        Me.btnCloseR.Name = "btnCloseR"
        Me.btnCloseR.Size = New System.Drawing.Size(26, 24)
        Me.btnCloseR.TabIndex = 2
        Me.btnCloseR.UseVisualStyleBackColor = True
        '
        'btnMinMaxR
        '
        Me.btnMinMaxR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMinMaxR.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnMinMaxR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinMaxR.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.min2
        Me.btnMinMaxR.Location = New System.Drawing.Point(101, 3)
        Me.btnMinMaxR.Name = "btnMinMaxR"
        Me.btnMinMaxR.Size = New System.Drawing.Size(24, 24)
        Me.btnMinMaxR.TabIndex = 0
        Me.btnMinMaxR.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 1
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.pgTemplateField, 0, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.cboField, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.TableLayoutPanel5, 0, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 39)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 3
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(160, 538)
        Me.TableLayoutPanel4.TabIndex = 1
        '
        'pgTemplateField
        '
        Me.pgTemplateField.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pgTemplateField.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pgTemplateField.HelpVisible = False
        Me.pgTemplateField.Location = New System.Drawing.Point(3, 63)
        Me.pgTemplateField.Name = "pgTemplateField"
        Me.pgTemplateField.PropertySort = System.Windows.Forms.PropertySort.NoSort
        Me.pgTemplateField.Size = New System.Drawing.Size(154, 472)
        Me.pgTemplateField.TabIndex = 1
        Me.pgTemplateField.ToolbarVisible = False
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.21739!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.78261!))
        Me.TableLayoutPanel5.Controls.Add(Me.lblItemPrint, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txtItemPrint, 1, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(154, 25)
        Me.TableLayoutPanel5.TabIndex = 2
        '
        'pnlPrint
        '
        Me.pnlPrint.Controls.Add(Me.SplitContainer1)
        Me.pnlPrint.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlPrint.Location = New System.Drawing.Point(150, 0)
        Me.pnlPrint.Name = "pnlPrint"
        Me.pnlPrint.Size = New System.Drawing.Size(664, 580)
        Me.pnlPrint.TabIndex = 41
        '
        'tlpLeft
        '
        Me.tlpLeft.ColumnCount = 1
        Me.tlpLeft.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpLeft.Controls.Add(Me.tlpRow0, 0, 0)
        Me.tlpLeft.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.tlpLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.tlpLeft.Location = New System.Drawing.Point(0, 0)
        Me.tlpLeft.Name = "tlpLeft"
        Me.tlpLeft.RowCount = 2
        Me.tlpLeft.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.tlpLeft.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlpLeft.Size = New System.Drawing.Size(150, 580)
        Me.tlpLeft.TabIndex = 39
        '
        'tlpRow0
        '
        Me.tlpRow0.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.tlpRow0.ColumnCount = 3
        Me.tlpRow0.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpRow0.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.tlpRow0.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tlpRow0.Controls.Add(Me.lblPrintingTools, 0, 0)
        Me.tlpRow0.Controls.Add(Me.btnClosel, 2, 0)
        Me.tlpRow0.Controls.Add(Me.btnMaxMinl, 1, 0)
        Me.tlpRow0.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpRow0.Location = New System.Drawing.Point(3, 3)
        Me.tlpRow0.Name = "tlpRow0"
        Me.tlpRow0.RowCount = 1
        Me.tlpRow0.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpRow0.Size = New System.Drawing.Size(144, 30)
        Me.tlpRow0.TabIndex = 0
        '
        'lblPrintingTools
        '
        Me.lblPrintingTools.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPrintingTools.BackColor = System.Drawing.Color.Gainsboro
        Me.lblPrintingTools.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPrintingTools.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrintingTools.Location = New System.Drawing.Point(3, 0)
        Me.lblPrintingTools.Name = "lblPrintingTools"
        Me.lblPrintingTools.Size = New System.Drawing.Size(76, 30)
        Me.lblPrintingTools.TabIndex = 7
        Me.lblPrintingTools.Text = "Tool Box"
        Me.lblPrintingTools.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClosel
        '
        Me.btnClosel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnClosel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnClosel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClosel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClosel.ForeColor = System.Drawing.Color.Black
        Me.btnClosel.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Close_16
        Me.btnClosel.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnClosel.Location = New System.Drawing.Point(115, 3)
        Me.btnClosel.Name = "btnClosel"
        Me.btnClosel.Size = New System.Drawing.Size(26, 24)
        Me.btnClosel.TabIndex = 2
        Me.btnClosel.UseVisualStyleBackColor = True
        '
        'btnMaxMinl
        '
        Me.btnMaxMinl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMaxMinl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnMaxMinl.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMaxMinl.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.min2
        Me.btnMaxMinl.Location = New System.Drawing.Point(85, 3)
        Me.btnMaxMinl.Name = "btnMaxMinl"
        Me.btnMaxMinl.Size = New System.Drawing.Size(24, 24)
        Me.btnMaxMinl.TabIndex = 0
        Me.btnMaxMinl.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.tvControls, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label4, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 39)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 451.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(144, 538)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label3.Location = New System.Drawing.Point(3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(134, 26)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "'Insert'or'DoubleClick' = Formula"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label4.Location = New System.Drawing.Point(3, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 27)
        Me.Label4.TabIndex = 55
        Me.Label4.Text = "'Delete'=Delete  Formula"
        '
        'chkSaveAsNew
        '
        Me.chkSaveAsNew.AutoSize = True
        Me.chkSaveAsNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSaveAsNew.ForeColor = System.Drawing.Color.OliveDrab
        Me.chkSaveAsNew.Location = New System.Drawing.Point(3, 33)
        Me.chkSaveAsNew.Name = "chkSaveAsNew"
        Me.chkSaveAsNew.Size = New System.Drawing.Size(106, 17)
        Me.chkSaveAsNew.TabIndex = 57
        Me.chkSaveAsNew.Text = " Save As New"
        Me.chkSaveAsNew.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(904, 11)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 18)
        Me.Label6.TabIndex = 58
        Me.Label6.Text = "Paper Width"
        Me.Label6.Visible = False
        '
        'txtCanvasWidth
        '
        Me.txtCanvasWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCanvasWidth.Location = New System.Drawing.Point(984, 11)
        Me.txtCanvasWidth.MaxLength = 4
        Me.txtCanvasWidth.Name = "txtCanvasWidth"
        Me.txtCanvasWidth.Size = New System.Drawing.Size(24, 20)
        Me.txtCanvasWidth.TabIndex = 59
        Me.txtCanvasWidth.Text = "10"
        Me.txtCanvasWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCanvasWidth.Visible = False
        '
        'chkLandspace
        '
        Me.chkLandspace.AutoSize = True
        Me.chkLandspace.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLandspace.Location = New System.Drawing.Point(708, 32)
        Me.chkLandspace.Name = "chkLandspace"
        Me.chkLandspace.Size = New System.Drawing.Size(94, 17)
        Me.chkLandspace.TabIndex = 60
        Me.chkLandspace.Text = "Land Space"
        Me.chkLandspace.UseVisualStyleBackColor = True
        Me.chkLandspace.Visible = False
        '
        'chkRepHeader
        '
        Me.chkRepHeader.AutoSize = True
        Me.chkRepHeader.Checked = True
        Me.chkRepHeader.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRepHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRepHeader.Location = New System.Drawing.Point(690, 33)
        Me.chkRepHeader.Name = "chkRepHeader"
        Me.chkRepHeader.Size = New System.Drawing.Size(112, 17)
        Me.chkRepHeader.TabIndex = 61
        Me.chkRepHeader.Text = "Repeat Header"
        Me.chkRepHeader.UseVisualStyleBackColor = True
        '
        'txtMultPrint
        '
        Me.txtMultPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMultPrint.Location = New System.Drawing.Point(991, 32)
        Me.txtMultPrint.MaxLength = 1
        Me.txtMultPrint.Name = "txtMultPrint"
        Me.txtMultPrint.Size = New System.Drawing.Size(17, 20)
        Me.txtMultPrint.TabIndex = 63
        Me.txtMultPrint.Text = "1"
        Me.txtMultPrint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMultPrint
        '
        Me.lblMultPrint.BackColor = System.Drawing.Color.Transparent
        Me.lblMultPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMultPrint.Location = New System.Drawing.Point(893, 35)
        Me.lblMultPrint.Name = "lblMultPrint"
        Me.lblMultPrint.Size = New System.Drawing.Size(103, 16)
        Me.lblMultPrint.TabIndex = 62
        Me.lblMultPrint.Text = "Page Per Sheet"
        '
        'chkPageWise
        '
        Me.chkPageWise.AutoSize = True
        Me.chkPageWise.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPageWise.Location = New System.Drawing.Point(805, 34)
        Me.chkPageWise.Name = "chkPageWise"
        Me.chkPageWise.Size = New System.Drawing.Size(87, 17)
        Me.chkPageWise.TabIndex = 64
        Me.chkPageWise.Text = "Page Wise"
        Me.chkPageWise.UseVisualStyleBackColor = True
        '
        'btnConverter
        '
        Me.btnConverter.BackColor = System.Drawing.Color.Lavender
        Me.btnConverter.Dock = System.Windows.Forms.DockStyle.None
        Me.btnConverter.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.btnConverter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSplitButton1})
        Me.btnConverter.Location = New System.Drawing.Point(120, 30)
        Me.btnConverter.Name = "btnConverter"
        Me.btnConverter.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.btnConverter.Size = New System.Drawing.Size(65, 25)
        Me.btnConverter.TabIndex = 3
        Me.btnConverter.Text = "ToolStrip1"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WordToolStripMenuItem, Me.ExeclToolStripMenuItem, Me.PdfToolStripMenuItem, Me.HTMLToolStripMenuItem})
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(62, 22)
        Me.ToolStripSplitButton1.Text = "Convert"
        '
        'WordToolStripMenuItem
        '
        Me.WordToolStripMenuItem.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Word_16
        Me.WordToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.WordToolStripMenuItem.Name = "WordToolStripMenuItem"
        Me.WordToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.WordToolStripMenuItem.Text = "Word"
        '
        'ExeclToolStripMenuItem
        '
        Me.ExeclToolStripMenuItem.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Excel_16
        Me.ExeclToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.ExeclToolStripMenuItem.Name = "ExeclToolStripMenuItem"
        Me.ExeclToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.ExeclToolStripMenuItem.Text = "Execl"
        '
        'PdfToolStripMenuItem
        '
        Me.PdfToolStripMenuItem.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.PDF_16
        Me.PdfToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.PdfToolStripMenuItem.Name = "PdfToolStripMenuItem"
        Me.PdfToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.PdfToolStripMenuItem.Text = "Pdf"
        '
        'HTMLToolStripMenuItem
        '
        Me.HTMLToolStripMenuItem.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.HTML_16
        Me.HTMLToolStripMenuItem.Name = "HTMLToolStripMenuItem"
        Me.HTMLToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.HTMLToolStripMenuItem.Text = "HTML"
        '
        'Button1
        '
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Exit_48
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(620, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 39)
        Me.Button1.TabIndex = 52
        Me.Button1.Text = "E&xit"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnPreview.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPreview.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Preview_24
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPreview.Location = New System.Drawing.Point(471, 3)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(76, 39)
        Me.btnPreview.TabIndex = 45
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.CancelCall_16
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(548, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(69, 39)
        Me.btnCancel.TabIndex = 50
        Me.btnCancel.Text = "C&ancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnLoad
        '
        Me.btnLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnLoad.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLoad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoad.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Load
        Me.btnLoad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLoad.Location = New System.Drawing.Point(187, 3)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(68, 39)
        Me.btnLoad.TabIndex = 41
        Me.btnLoad.Text = "&Load"
        Me.btnLoad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'btnClearData
        '
        Me.btnClearData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnClearData.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClearData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearData.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Cleanup_32
        Me.btnClearData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClearData.Location = New System.Drawing.Point(400, 3)
        Me.btnClearData.Name = "btnClearData"
        Me.btnClearData.Size = New System.Drawing.Size(72, 39)
        Me.btnClearData.TabIndex = 44
        Me.btnClearData.Text = "&Clear"
        Me.btnClearData.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClearData.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Save_24
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(257, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(68, 39)
        Me.btnSave.TabIndex = 42
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.delete_20
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(328, 3)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(72, 39)
        Me.btnDelete.TabIndex = 43
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblType)
        Me.Panel1.Controls.Add(Me.cmbType)
        Me.Panel1.Controls.Add(Me.btnDelete)
        Me.Panel1.Controls.Add(Me.chkPageWise)
        Me.Panel1.Controls.Add(Me.btnSave)
        Me.Panel1.Controls.Add(Me.chkLandspace)
        Me.Panel1.Controls.Add(Me.btnClearData)
        Me.Panel1.Controls.Add(Me.txtCanvasWidth)
        Me.Panel1.Controls.Add(Me.btnLoad)
        Me.Panel1.Controls.Add(Me.chkRepHeader)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.btnPreview)
        Me.Panel1.Controls.Add(Me.txtMultPrint)
        Me.Panel1.Controls.Add(Me.txtMstXMLPath)
        Me.Panel1.Controls.Add(Me.lblMultPrint)
        Me.Panel1.Controls.Add(Me.btnMstPath)
        Me.Panel1.Controls.Add(Me.chkSaveAsNew)
        Me.Panel1.Controls.Add(Me.lblXML)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.btnConverter)
        Me.Panel1.Controls.Add(Me.txtDetXMLPath)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1014, 58)
        Me.Panel1.TabIndex = 65
        '
        'Form1Des
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GhostWhite
        Me.ClientSize = New System.Drawing.Size(1014, 643)
        Me.Controls.Add(Me.btnDetPath)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1Des"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Print Preview"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.pnlVScroll.ResumeLayout(False)
        Me.pnlCanvasRuler.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlToolBar.ResumeLayout(False)
        Me.pnlToolBar.PerformLayout()
        Me.pnlMainCanvas.ResumeLayout(False)
        Me.pnlCanvas.ResumeLayout(False)
        Me.cmslbl.ResumeLayout(False)
        Me.cms.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ToolStripContainer1.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.tlpRight.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.pnlPrint.ResumeLayout(False)
        Me.tlpLeft.ResumeLayout(False)
        Me.tlpRow0.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.btnConverter.ResumeLayout(False)
        Me.btnConverter.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents cboField As System.Windows.Forms.ComboBox
    Public WithEvents pgTemplateField As TemplatePropertyGrid
    Friend WithEvents objlblProperty As System.Windows.Forms.Label
    Friend WithEvents tvControls As System.Windows.Forms.TreeView
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlToolBar As System.Windows.Forms.Panel
    Friend WithEvents chkSnapGrid As System.Windows.Forms.CheckBox
    Friend WithEvents pnlMainCanvas As System.Windows.Forms.Panel
    Friend WithEvents pnlCanvas As System.Windows.Forms.Panel
    Friend WithEvents spltFooter As System.Windows.Forms.Splitter
    Friend WithEvents pnlFooter As System.Windows.Forms.Panel
    Friend WithEvents lblBottomFooter As System.Windows.Forms.Label
    Friend WithEvents spltDetailFooter As System.Windows.Forms.Splitter
    Friend WithEvents pnlDetailFooter As System.Windows.Forms.Panel
    Friend WithEvents lblFooter As System.Windows.Forms.Label
    Friend WithEvents spltDetail As System.Windows.Forms.Splitter
    Friend WithEvents pnlDetail As System.Windows.Forms.Panel
    Friend WithEvents spltHeader As System.Windows.Forms.Splitter
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents chkGrid As System.Windows.Forms.CheckBox
    Friend WithEvents lblDetail As System.Windows.Forms.Label
    Friend WithEvents tb_Designer_ToolBar As System.Windows.Forms.ToolBar
    Friend WithEvents edit_Delete_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Align_Left As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Valign_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Align_Right_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Align_Top_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Halign_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Align_Bottom_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Same_Width_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Same_Height_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Same_Size_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents toolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents edit_Cut_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents edit_Copy_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents edit_Paste_Button As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Box As System.Windows.Forms.ToolBarButton
    Friend WithEvents format_Line As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents btnClearData As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents txtItemPrint As System.Windows.Forms.TextBox
    Friend WithEvents lblItemPrint As System.Windows.Forms.Label
    Friend WithEvents txtMstXMLPath As System.Windows.Forms.TextBox
    Friend WithEvents lblXML As System.Windows.Forms.Label
    Friend WithEvents btnMstPath As System.Windows.Forms.Button
    Friend WithEvents ilToolBarImage As System.Windows.Forms.ImageList
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents oPrintDoc As System.Drawing.Printing.PrintDocument
    Friend WithEvents cms As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cms_Copy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cms_Paste As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cms_Delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnDetPath As System.Windows.Forms.Button
    Friend WithEvents txtDetXMLPath As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents tlpLeft As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tlpRow0 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnClosel As System.Windows.Forms.Button
    Friend WithEvents btnMaxMinl As System.Windows.Forms.Button
    Friend WithEvents lblPrintingTools As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pnlPrint As System.Windows.Forms.Panel
    Friend WithEvents tlpRight As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnCloseR As System.Windows.Forms.Button
    Friend WithEvents btnMinMaxR As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pnlRuler As System.Windows.Forms.Panel
    Friend WithEvents chkSaveAsNew As System.Windows.Forms.CheckBox
    Friend WithEvents cmslbl As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmSuppress As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCanvasWidth As System.Windows.Forms.TextBox
    Friend WithEvents chkLandspace As System.Windows.Forms.CheckBox
    Friend WithEvents tsm_PageSetup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlVScroll As System.Windows.Forms.Panel
    Friend WithEvents pnlFooterRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlDetFooterRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlDetailRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlHeaderRuler As System.Windows.Forms.Panel
    Friend WithEvents pnlCanvasRuler As System.Windows.Forms.Panel
    Friend WithEvents spltGoup1 As System.Windows.Forms.Splitter
    Friend WithEvents pnlGroup1 As System.Windows.Forms.Panel
    Friend WithEvents lblGroup1 As System.Windows.Forms.Label
    Friend WithEvents pnlGroupFooter1 As System.Windows.Forms.Panel
    Friend WithEvents lblGroupFooter1 As System.Windows.Forms.Label
    Friend WithEvents spltGroupFooter1 As System.Windows.Forms.Splitter
    Friend WithEvents pnlGrpFooterRuler1 As System.Windows.Forms.Panel
    Friend WithEvents pnlGrpRuler1 As System.Windows.Forms.Panel
    Friend WithEvents chkRepHeader As System.Windows.Forms.CheckBox
    Friend WithEvents txtMultPrint As System.Windows.Forms.TextBox
    Friend WithEvents lblMultPrint As System.Windows.Forms.Label
    Friend WithEvents pnlCompHeader As System.Windows.Forms.Panel
    Friend WithEvents lblCompHeader As System.Windows.Forms.Label
    Friend WithEvents spltCompHeader As System.Windows.Forms.Splitter
    Friend WithEvents pnlCompHeaderRuler As System.Windows.Forms.Panel
    Friend WithEvents chkPageWise As System.Windows.Forms.CheckBox
    Friend WithEvents WordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExeclToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PdfToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HTMLToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnConverter As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
