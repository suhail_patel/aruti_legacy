﻿Public Class Util

#Region "PRN SAMPLE"
    Public Const PRN_BOLD_START = Chr(27) + "E"                   ' Setting Bold Printing ON in DOS Mode
    Public Const PRN_BOLD_END = Chr(27) + "F"                       ' Setting Bold Printing OFF in DOS Mode
    Public Const PRN_ENLARGE = Chr(27) + Chr(14)                   ' Setting Enlarge Printing in DOS Mode
    Public Const PRN_NORMAL_E = Chr(20)                             ' Setting Normal Printing from Enlarge Printing in DOS Mode
    Public Const PRN_CONDENSE = Chr(27) + Chr(15)                   ' Setting Condense Printing in DOS Mode
    Public Const PRN_NORMAL_C = Chr(18)                             ' Setting Normal Printing from Condense Printing in DOS Mode
    Public Const PRN_ELITE = Chr(27) + "M"                       ' Setting Elite Printing in DOS Mode (12 cpi)
    Public Const PRN_NORMAL_EL = Chr(27) + "P"                       ' Setting Normal Printing from Elite Printing in DOS Mode
    Public Const PRN_DBL_HT = Chr(14) + Chr(27) + Chr(119) + Chr(49)    ' Setting Double Height Printing in DOS Mode
    Public Const PRN_NORMAL_DBL = Chr(20) + Chr(27) + Chr(119) + Chr(48)    ' Setting Normal Height from Double Height Printing in DOS Mode
#End Region

#Region "Code "
    Public Shared Function GetCenterPos(ByVal cString As String, Optional ByVal lBold As Boolean = False) As Integer
        Dim nPos As Integer = 0
        If lBold Then
            nPos = (80 - (Len(cString) * 2)) / 2
        Else
            nPos = (80 - (Len(cString))) / 2
        End If
        GetCenterPos = nPos
    End Function
    Public Shared Function replicate(ByVal strChar As String, Optional ByVal nNoOfChr As Integer = 1) As String
        Dim iCtr As Integer
        Dim cRetString As String = ""
        For iCtr = 1 To nNoOfChr
            cRetString += strChar
        Next
        replicate = cRetString
    End Function
    Public Shared Function Str(ByVal nValue As Double, Optional ByVal nLen As Integer = 14, Optional ByVal nDecimal As Integer = 0) As String
        Dim cString As String = "#"
        cString = replicate("#", (nLen - IIf(nDecimal > 0, nDecimal + 1, 0)) - 1) + "0"
        If nDecimal > 0 Then
            cString += "." + replicate("0", nDecimal)
        End If
        Str = Padl(Format(nValue, cString), nLen)
    End Function
    Public Shared Function Padr(ByVal cString As String, ByVal nSpace As Integer) As String
        If nSpace > Len(cString) Then
            cString = cString + Space(nSpace - Len(cString))
        Else
            cString = SubString(cString, 1, nSpace)
        End If
        Padr = cString
    End Function
    Public Shared Function SubString(ByVal cString As String, ByVal nStart As Integer, ByVal nLen As Integer) As String
        Dim iCtr As Integer
        Dim cRetStr As String = ""
        If nStart > 0 Then
            For iCtr = nStart - 1 To nLen - 1
                cRetStr += cString.Chars(iCtr)
            Next
            SubString = cRetStr
        Else
            SubString = cString
        End If
    End Function
    Public Shared Function Padl(ByVal cString As String, ByVal nSpace As Integer) As String
        If nSpace > Len(cString) Then
            cString = Space(nSpace - Len(cString)) + cString
        Else
            cString = SubString(cString, 1, nSpace)
        End If
        Padl = cString
    End Function
#End Region

#Region "In Words"
    Public Shared Function InWords(ByVal MyNumber, ByVal cStartChar, ByVal cEndChar)
        Dim Temp
        Dim Rupees, Paisa As String
        Dim DecimalPlace, iCount
        Dim Hundreds, Words As String
        Dim place(9) As String
        place(0) = " Thousand "
        place(2) = " Lakh "
        place(4) = " Crore "
        place(6) = " Arab "
        place(8) = " Kharab "
        Paisa = ""
        On Error Resume Next
        ' Convert MyNumber to a string, trimming extra spaces.
        MyNumber = Trim(Str(MyNumber))

        ' Find decimal place.
        DecimalPlace = InStr(MyNumber, ".")

        ' If we find decimal place...
        If DecimalPlace > 0 Then
            ' Convert Paisa
            Temp = Left(Mid(MyNumber, DecimalPlace + 1) & "00", 2)
            Paisa = " and " & ConvertTens(Temp) & cEndChar

            ' Strip off paisa from remainder to convert.
            MyNumber = Trim(Left(MyNumber, DecimalPlace - 1))
        End If

        '===============================================================
        Dim TM As String  ' If MyNumber between Rs.1 To 99 Only.
        TM = Right(MyNumber, 2)
        Words = ""
        If Len(MyNumber) > 0 And Len(MyNumber) <= 2 Then
            If Len(TM) = 1 Then
                Words = ConvertDigit(TM)
                InWords = cStartChar + " " & Words & Paisa & " Only"

                Exit Function

            Else
                If Len(TM) = 2 Then
                    Words = ConvertTens(TM)
                    InWords = cStartChar + " " & Words & Paisa & " Only"
                    Exit Function

                End If
            End If
        End If
        '===============================================================


        ' Convert last 3 digits of MyNumber to ruppees in word.
        Hundreds = ConvertHundreds(Right(MyNumber, 3))
        ' Strip off last three digits
        MyNumber = Left(MyNumber, Len(MyNumber) - 3)

        iCount = 0
        Do While MyNumber <> ""
            'Strip last two digits
            Temp = Right(MyNumber, 2)
            If Len(MyNumber) = 1 Then


                If Trim(Words) = "Thousand" Or _
                Trim(Words) = "Lakh  Thousand" Or _
                Trim(Words) = "Lakh" Or _
                Trim(Words) = "Crore" Or _
                Trim(Words) = "Crore  Lakh  Thousand" Or _
                Trim(Words) = "Arab  Crore  Lakh  Thousand" Or _
                Trim(Words) = "Arab" Or _
                Trim(Words) = "Kharab  Arab  Crore  Lakh  Thousand" Or _
                Trim(Words) = "Kharab" Then

                    Words = ConvertDigit(Temp) & place(iCount)
                    MyNumber = Left(MyNumber, Len(MyNumber) - 1)

                Else

                    Words = ConvertDigit(Temp) & place(iCount) & Words
                    MyNumber = Left(MyNumber, Len(MyNumber) - 1)

                End If
            Else

                If Trim(Words) = "Thousand" Or _
                   Trim(Words) = "Lakh  Thousand" Or _
                   Trim(Words) = "Lakh" Or _
                   Trim(Words) = "Crore" Or _
                   Trim(Words) = "Crore  Lakh  Thousand" Or _
                   Trim(Words) = "Arab  Crore  Lakh  Thousand" Or _
                   Trim(Words) = "Arab" Then


                    Words = ConvertTens(Temp) & place(iCount)


                    MyNumber = Left(MyNumber, Len(MyNumber) - 2)
                Else

                    '=================================================================
                    ' if only Lakh, Crore, Arab, Kharab

                    If Trim(ConvertTens(Temp) & place(iCount)) = "Lakh" Or _
                       Trim(ConvertTens(Temp) & place(iCount)) = "Crore" Or _
                       Trim(ConvertTens(Temp) & place(iCount)) = "Arab" Then

                        Words = Words
                        MyNumber = Left(MyNumber, Len(MyNumber) - 2)
                    Else
                        Words = ConvertTens(Temp) & place(iCount) & Words
                        MyNumber = Left(MyNumber, Len(MyNumber) - 2)
                    End If

                End If
            End If

            iCount = iCount + 2
        Loop

        InWords = cStartChar & IIf(cStartChar <> "", " ", "") & Words & Hundreds & Paisa & " Only"
    End Function

    ' Conversion for hundreds
    '*****************************************
    Private Shared Function ConvertHundreds(ByVal MyNumber)
        Dim Result As String = ""

        ' Exit if there is nothing to convert.
        If Val(MyNumber) <> 0 Then

            ' Append leading zeros to number.
            MyNumber = Right("000" & MyNumber, 3)

            ' Do we have a hundreds place digit to convert?
            If Left(MyNumber, 1) <> "0" Then
                Result = ConvertDigit(Left(MyNumber, 1)) & " Hundreds "
            End If

            ' Do we have a tens place digit to convert?
            If Mid(MyNumber, 2, 1) <> "0" Then
                Result = Result & ConvertTens(Mid(MyNumber, 2))
            Else
                ' If not, then convert the ones place digit.
                Result = Result & ConvertDigit(Mid(MyNumber, 3))
            End If
        End If
        Return Trim(Result)
    End Function

    ' Conversion for tens
    '*****************************************
    Private Shared Function ConvertTens(ByVal MyTens)
        Dim Result As String = ""

        ' Is value between 10 and 19?
        If Val(Left(MyTens, 1)) = 1 Then
            Select Case Val(MyTens)
                Case 10 : Result = "Ten"
                Case 11 : Result = "Eleven"
                Case 12 : Result = "Twelve"
                Case 13 : Result = "Thirteen"
                Case 14 : Result = "Fourteen"
                Case 15 : Result = "Fifteen"
                Case 16 : Result = "Sixteen"
                Case 17 : Result = "Seventeen"
                Case 18 : Result = "Eighteen"
                Case 19 : Result = "Nineteen"
                Case Else
            End Select
        Else
            ' .. otherwise it's between 20 and 99.
            Select Case Val(Left(MyTens, 1))
                Case 2 : Result = "Twenty "
                Case 3 : Result = "Thirty "
                Case 4 : Result = "Forty "
                Case 5 : Result = "Fifty "
                Case 6 : Result = "Sixty "
                Case 7 : Result = "Seventy "
                Case 8 : Result = "Eighty "
                Case 9 : Result = "Ninety "
                Case Else
            End Select

            ' Convert ones place digit.
            Result = Result & ConvertDigit(Right(MyTens, 1))
        End If

        ConvertTens = Result
    End Function

    Private Shared Function ConvertDigit(ByVal MyDigit)
        Select Case Val(MyDigit)
            Case 1 : ConvertDigit = "One"
            Case 2 : ConvertDigit = "Two"
            Case 3 : ConvertDigit = "Three"
            Case 4 : ConvertDigit = "Four"
            Case 5 : ConvertDigit = "Five"
            Case 6 : ConvertDigit = "Six"
            Case 7 : ConvertDigit = "Seven"
            Case 8 : ConvertDigit = "Eight"
            Case 9 : ConvertDigit = "Nine"
            Case Else : ConvertDigit = ""
        End Select
    End Function
#End Region
End Class
