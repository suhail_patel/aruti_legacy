﻿#Region " Import "

Imports System.IO
Imports System.Timers
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.ServiceProcess
Imports System.Text
Imports System.Security.Cryptography

#End Region

Public Class ArutiSymmetry

#Region " Useful Information "

    'RecordRequest = 0 (Add/Modify)
    'RecordRequest = 1 (Modify)
    'RecordRequest = 2/5 (Force Inactive/Active)
    'RecordRequest = 3/4/6 (Add/Remove Access Rights)
    'RecordRequest = 23 (Update Card Usage Remaining)
    'RecordRequest = 24/25 (Visitor Sign in/Out/Closed)
    'RecordRequest = 42 (Delete)
    'RecordRequest = 72 (Add/Modify Card)
    '-------------------------------------------------------------------------------------------------------------------
    'APPOINTMENT DATE -----------------> ACTIVEDATE
    'REHIRE DATE	  -----------------> ACTIVEDATE
    'TITLE/SALUATION  -----------------> PERSONALDATA1
    'DOB              -----------------> PERSONALDATA2 -- Date
    'DEPARTMENT       -----------------> PERSONALDATA3
    'JOB NAME         -----------------> PERSONALDATA4
    'CLASS GROUP      -----------------> PERSONALDATA5
    'CLASS            -----------------> PERSONALDATA6
    'DESCRIPTION(TERM.REASON) ---------> PERSONALDATA7
    'EOC DATE         -----------------> PERSONALDATA8 -- Date
    'LEAVING DATE AS  -----------------> PERSONALDATA9 -- Date
    'RETIREMENT DATE AS  --------------> PERSONALDATA10 -- Date
    'SUSP. START DATE -----------------> PERSONALDATA11 -- Date
    'SUSP. END DATE   -----------------> PERSONALDATA12 -- Date

#End Region

#Region " Private Variable "

    Private timer As New Timer()
    Private strSymmetryConnString As String = ""
    Private strArutiConnString As String = ""
    Private blnIsSymmetry As Boolean = False
    Private blnIsAruti As Boolean = False
    Private mstrCurrentDatabaseName As String = ""
    Private mintSelecteIdentityTypeId As Integer = 0
    Private mblnIsMarkAsDefault As Boolean = False
    Private Shared bytIV() As Byte = {21, 54, 9, 13, 78, 19, 62, 45, 121, 195, 245, 35, 5, 1, 121, 212}
    Private mblnIsFirstTime As Boolean = False
    'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
    Private mdicTableName As Dictionary(Of Integer, String)
    'S.SANDEEP [11-AUG-2017] -- END

    'S.SANDEEP [06-Apr-2018] -- START
    Private mblnIsDatabaseAccessible As Boolean = True
    'S.SANDEEP [06-Apr-2018] -- END

#End Region

#Region " Emun(s) "

    Public Enum enRecordRequest
        ADD_MODIFY_DETAILS = 0
        MODIFY_DETAILS = 1
        INACTIVE_CARD = 2
        ACTIVATE_CARD = 5
        ADD_ACCESS_RIGHTS = 3
        REMOVE_ACCESS_RIGHTS = 6
        UPDATE_CARD_USAGE = 23
        VISITOR_IN_OUT = 24
        VISITOR_COLSED = 25
        DELETE_INFORMATION = 42
        ADD_MODIFY_CARD = 72
    End Enum

    Public Enum enCheckType
        TRANSFER = 1
        RECATEGORIZATION = 2
        SUSPENSION = 3
        REINSTATEMENT = 4
        TERMNATION = 5
        RETIREMENT = 6
        LEAVE = 7
    End Enum

#End Region

#Region " Private Method "

    Private Function IsConnect(ByVal strConnStr As String) As Boolean
        Using oSQL As New SqlConnection
            oSQL.ConnectionString = strConnStr
            Try
                oSQL.Open()
                Return True
            Catch ex As Exception
                WriteLog("IsConnect" & strConnStr, ex)
            End Try
        End Using
    End Function

    Private Sub WriteLog(ByVal strMethodName As String, ByVal ex As Exception)
        Dim m_strLogFile As String = "" : Dim strMessage As String = ""
        Dim m_strFileName As String = "ArutiSymmetry_LOG_" & Now.ToString("yyyyMMdd")
        Dim file As System.IO.StreamWriter
        m_strLogFile = IO.Path.Combine(My.Application.Info.DirectoryPath, m_strFileName & ".txt")
        file = New System.IO.StreamWriter(m_strLogFile, True)
        file.BaseStream.Seek(0, SeekOrigin.End)
        If ex IsNot Nothing Then
            strMessage = strMethodName & " : " & " Message : " & ex.Message & "; Trace: " & ex.StackTrace
        Else
            strMessage = strMethodName
        End If
        file.WriteLine(Format(Now, "yyyyMMddhhmmss") & " : " & strMessage)
        file.Close()
    End Sub

    Private Sub ChangeDatabase(ByVal strSourceConnString As String)
        Dim StrQ As String = String.Empty
        Try
            StrQ = "SELECT TOP 1 " & _
                   "    cffinancial_year_tran.database_name " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "    JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "    JOIN hrmsConfiguration..cfconfiguration ON cfcompany_master.companyunkid = cfconfiguration.companyunkid " & _
                   "WHERE cfconfiguration.key_name LIKE '%Symmetry%' AND cfconfiguration.key_name = 'IsSymmetryIntegrated' AND cffinancial_year_tran.isclosed = 0 AND CAST(cfconfiguration.key_value AS BIT) = 1 "
            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            ' ADDED - IsSymmetryIntegrated IN WHERE CLAUSE
            'S.SANDEEP [11-AUG-2017] -- END
            Using oScrc As New SqlConnection
                oScrc.ConnectionString = strSourceConnString
                Try
                    oScrc.Open()
                    Using oSqlCmd As New SqlCommand
                        oSqlCmd.Connection = oScrc
                        oSqlCmd.CommandTimeout = 0
                        oSqlCmd.CommandText = StrQ
                        mstrCurrentDatabaseName = oSqlCmd.ExecuteScalar()
                        If mstrCurrentDatabaseName.Trim.Length > 0 Then
                            'S.SANDEEP [06-Apr-2018] -- START
                            'strArutiConnString = "Data Source=(local)\APAYROLL;Initial Catalog=" & mstrCurrentDatabaseName & ";User ID=aruti_sa;Password=pRofessionalaRuti999"
                            mblnIsDatabaseAccessible = True
                            IsDatabaseAccessible(mstrCurrentDatabaseName, oSqlCmd)
                            If mblnIsDatabaseAccessible Then
                                strArutiConnString = "Data Source=(local)\APAYROLL;Initial Catalog=" & mstrCurrentDatabaseName & ";User ID=aruti_sa;Password=pRofessionalaRuti999"
                            End If
                            'S.SANDEEP [06-Apr-2018] -- END
                        End If
                    End Using
                Catch ex As Exception
                    WriteLog("ChangeDatabase", ex)
                End Try
            End Using

        Catch ex As Exception
            WriteLog("ChangeDatabase", ex)
        Finally
        End Try
    End Sub

    Private Sub GenerateSymmetryConnectionString(ByVal strSourceConnString As String)
        Dim StrQ As String = String.Empty
        Dim dsValues As New DataSet
        Try
            StrQ = "SELECT " & _
                   "    UPPER(cfconfiguration.key_name) AS key_name,cfconfiguration.key_value,cfcompany_master.companyunkid,cffinancial_year_tran.database_name " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "    JOIN cfcompany_master ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "    JOIN cfconfiguration ON cfcompany_master.companyunkid = cfconfiguration.companyunkid " & _
                   "WHERE UPPER(cfconfiguration.key_name) LIKE '%SYMMETRY%' " & _
                   " AND cffinancial_year_tran.isclosed = 0 AND cfcompany_master.companyunkid IN (SELECT companyunkid FROM cfconfiguration WHERE cfconfiguration.key_name LIKE '%Symmetry%' AND cfconfiguration.key_name = 'IsSymmetryIntegrated' AND CAST(cfconfiguration.key_value AS BIT) = 1) "
            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            ' ADDED : AND cfcompany_master.companyunkid IN (SELECT companyunkid FROM cfconfiguration WHERE cfconfiguration.key_name LIKE '%Symmetry%' AND cfconfiguration.key_name = 'IsSymmetryIntegrated' AND CAST(cfconfiguration.key_value AS BIT) = 1)
            'S.SANDEEP [11-AUG-2017] -- END

            Using oScrc As New SqlConnection
                oScrc.ConnectionString = strSourceConnString
                Try
                    oScrc.Open()
                    Using oSqlCmd As New SqlCommand
                        oSqlCmd.Connection = oScrc
                        oSqlCmd.CommandTimeout = 0
                        oSqlCmd.CommandText = StrQ
                        Using oDa As New SqlDataAdapter()
                            oDa.SelectCommand = oSqlCmd
                            oDa.Fill(dsValues)
                        End Using
                        If dsValues.Tables(0).Rows.Count > 0 Then
                            strSymmetryConnString = "Data Source="
                            Dim tmp As DataRow() = Nothing
                            tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYDATASERVERADDRESS'")
                            If tmp.Length > 0 Then strSymmetryConnString &= tmp(0)("key_value") & ";"

                            tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYDATABASENAME'")
                            If tmp.Length > 0 Then strSymmetryConnString &= "Initial Catalog=" & tmp(0)("key_value") & ";"

                            tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYAUTHENTICATIONMODEID'")
                            If tmp.Length > 0 Then
                                If CInt(tmp(0)("key_value")) = 0 Then   'WINDOWS
                                    strSymmetryConnString &= "Integrated Security=True "
                                ElseIf CInt(tmp(0)("key_value")) = 1 Then   'USER
                                    tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYDATABASEUSERNAME'")
                                    If tmp.Length > 0 Then strSymmetryConnString &= "User ID=" & tmp(0)("key_value") & ";"

                                    tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYDATABASEPASSWORD'")
                                    If tmp.Length > 0 Then strSymmetryConnString &= "Password=" & Decrypt(tmp(0)("key_value"), "ezee") & ";"
                                End If
                            Else
                                tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYDATABASEUSERNAME'")
                                If tmp.Length > 0 Then strSymmetryConnString &= "User ID=" & tmp(0)("key_value") & ";"

                                tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYDATABASEPASSWORD'")
                                If tmp.Length > 0 Then strSymmetryConnString &= "Password=" & Decrypt(tmp(0)("key_value"), "ezee") & ";"
                            End If


                            tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYCARDNUMBERTYPEID'")
                            If tmp.Length > 0 Then mintSelecteIdentityTypeId = tmp(0)("key_value")

                            tmp = dsValues.Tables(0).Select("key_name = 'SYMMETRYMARKASDEFAULT'")
                            If tmp.Length > 0 Then mblnIsMarkAsDefault = tmp(0)("key_value")
                        End If
                    End Using
                Catch ex As Exception
                    WriteLog("GenerateSymmetryConnectionString", ex)
                End Try
            End Using

        Catch ex As Exception
            WriteLog("GenerateSymmetryConnectionString", ex)
        Finally
        End Try
    End Sub

    Private Function GetArutiEmployeeDetails(ByVal oCmd As SqlCommand) As DataSet
        Dim dsEmp As New DataSet
        Dim StrQ As String = String.Empty
        Try
            'StrQ = "DECLARE @efdate as nvarchar(8), @mtdate as nvarchar(8) " & _
            '       "SET @efdate = CONVERT(NVARCHAR(8),GETDATE(),112) " & _
            '       "SET @mtdate = CONVERT(NVARCHAR(8),GETDATE(),112) " & _
            '       "SELECT " & _
            '       "     hremployee_master.employeecode AS EmployeeReference " & _
            '       "    ,hremployee_master.firstname AS Firstname " & _
            '       "    ,CASE WHEN LEN(LTRIM(RTRIM(hremployee_master.othername))) > 0 " & _
            '       "          THEN CASE WHEN LEN(LTRIM(RTRIM(hremployee_master.othername))) > 2 THEN UPPER(SUBSTRING(LTRIM(RTRIM(hremployee_master.othername)),0,2)) + '.' " & _
            '       "               ELSE CASE WHEN CHARINDEX('.',UPPER(LTRIM(RTRIM(hremployee_master.othername)))) > 0 THEN UPPER(LTRIM(RTRIM(hremployee_master.othername))) " & _
            '       "                    ELSE UPPER(LTRIM(RTRIM(hremployee_master.othername)))+'.' END " & _
            '       "               END " & _
            '       "     ELSE '' END AS InitLet " & _
            '       "    ,hremployee_master.surname AS LastName " & _
            '       "    ,CASE WHEN hremployee_master.accessunkid < = 0 THEN 0 ELSE hremployee_master.accessunkid END AS CardNumber " & _
            '       "    ,CASE WHEN CONVERT(NVARCHAR(8),RH.reinstatment_date,112) > CONVERT(NVARCHAR(8),hremployee_master.appointeddate,112) THEN RH.reinstatment_date ELSE hremployee_master.appointeddate END AS ActiveDate " & _
            '       "    ,ISNULL(tt.name,'') AS PersonalData1 " & _
            '       "    ,hremployee_master.birthdate AS PersonalData2 " & _
            '       "    ,ISNULL(hrdepartment_master.name,'') AS PersonalData3 " & _
            '       "    ,ISNULL(hrjob_master.job_name,'') AS PersonalData4 " & _
            '       "    ,ISNULL(hrclassgroup_master.name,'') AS PersonalData5 " & _
            '       "    ,ISNULL(hrclasses_master.name,'') AS PersonalData6 " & _
            '       "    ,ISNULL(treason,'') AS PersonalData7 " & _
            '       "    ,ED.date1 AS PersonalData8 " & _
            '       "    ,ED.date2 AS PersonalData9 " & _
            '       "    ,RT.date1 AS PersonalData10 " & _
            '       "    ,SP.date1 AS PersonalData11 " & _
            '       "    ,SP.date2 AS PersonalData12 " & _
            '       "    ,ISNULL(ED.CheckType,0) AS eCheckType " & _
            '       "    ,ISNULL(LV.CheckType,0) AS lCheckType " & _
            '       "    ,ISNULL(RC.CheckType,0) AS cCheckType " & _
            '       "    ,ISNULL(RH.CheckType,0) AS hCheckType " & _
            '       "    ,ISNULL(RT.CheckType,0) AS rCheckType " & _
            '       "    ,ISNULL(SP.CheckType,0) AS sCheckType " & _
            '       "    ,ISNULL(TF.CheckType,0) AS tCheckType " & _
            '       "    ,ldate AS ldate " & _
            '       "FROM hremployee_master " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         hremployee_dates_tran.employeeunkid " & _
            '       "        ,hremployee_dates_tran.date1 " & _
            '       "        ,hremployee_dates_tran.date2 " & _
            '       "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
            '       "        ,CASE WHEN hremployee_dates_tran.rehiretranunkid > 0 THEN RTE.name ELSE TEC.name END AS treason " & _
            '       "        ,5 AS CheckType " & _
            '       "    FROM hremployee_dates_tran " & _
            '       "        LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = hremployee_dates_tran.changereasonunkid AND RTE.mastertype = '49' " & _
            '       "        LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = hremployee_dates_tran.changereasonunkid AND TEC.mastertype = '48' " & _
            '       "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
            '       "    AND hremployee_dates_tran.datetypeunkid = 4 " & _
            '       ") AS ED ON ED.employeeunkid = hremployee_master.employeeunkid AND ED.rno = 1 AND ED.date1 IS NOT NULL " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         hremployee_dates_tran.employeeunkid " & _
            '       "        ,hremployee_dates_tran.date1 " & _
            '       "        ,hremployee_dates_tran.date2 " & _
            '       "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
            '       "        ,3 AS CheckType " & _
            '       "    FROM hremployee_dates_tran " & _
            '       "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
            '       "    AND hremployee_dates_tran.datetypeunkid = 3 " & _
            '       ") AS SP ON SP.employeeunkid = hremployee_master.employeeunkid AND SP.rno = 1 " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         hremployee_dates_tran.employeeunkid " & _
            '       "        ,hremployee_dates_tran.date1 " & _
            '       "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
            '       "        ,6 AS CheckType " & _
            '       "    FROM hremployee_dates_tran " & _
            '       "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
            '       "    AND hremployee_dates_tran.datetypeunkid = 6 " & _
            '       ") AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         hremployee_categorization_tran.employeeunkid " & _
            '       "        ,hremployee_categorization_tran.jobunkid " & _
            '       "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
            '       "        ,2 AS CheckType " & _
            '       "    FROM hremployee_categorization_tran " & _
            '       "    WHERE hremployee_categorization_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @efdate " & _
            '       ") AS RC ON RC.employeeunkid = hremployee_master.employeeunkid AND RC.rno = 1 " & _
            '       "LEFT JOIN hrjob_master ON RC.jobunkid = hrjob_master.jobunkid " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         hremployee_transfer_tran.employeeunkid " & _
            '       "        ,hremployee_transfer_tran.departmentunkid " & _
            '       "        ,hremployee_transfer_tran.classgroupunkid " & _
            '       "        ,hremployee_transfer_tran.classunkid " & _
            '       "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
            '       "        ,1 AS CheckType " & _
            '       "FROM hremployee_transfer_tran " & _
            '       "WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @efdate " & _
            '       ") AS TF ON TF.employeeunkid = hremployee_master.employeeunkid AND TF.rno = 1 " & _
            '       "LEFT JOIN hrdepartment_master ON TF.departmentunkid = hrdepartment_master.departmentunkid " & _
            '       "LEFT JOIN hrclassgroup_master ON TF.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
            '       "LEFT JOIN hrclasses_master ON TF.classunkid = hrclasses_master.classesunkid " & _
            '       "LEFT JOIN cfcommon_master AS tt ON tt.masterunkid = hremployee_master.titleunkid AND tt.mastertype = 24 " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         hremployee_rehire_tran.employeeunkid " & _
            '       "        ,hremployee_rehire_tran.reinstatment_date " & _
            '       "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_rehire_tran.employeeunkid ORDER BY hremployee_rehire_tran.effectivedate DESC) AS rno " & _
            '       "        ,4 AS CheckType " & _
            '       "    FROM hremployee_rehire_tran " & _
            '       "    WHERE hremployee_rehire_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_rehire_tran.effectivedate,112) <= @efdate " & _
            '       ") AS RH ON RH.employeeunkid = hremployee_master.employeeunkid AND RH.rno = 1 " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         lvleaveIssue_master.employeeunkid " & _
            '       "        ,lvleaveIssue_tran.leavedate ldate " & _
            '       "        ,7 AS CheckType " & _
            '       "    FROM lvleaveIssue_tran " & _
            '       "    JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
            '       "    WHERE lvleaveIssue_tran.isvoid = 0 AND lvleaveIssue_master.isvoid = 0 AND CONVERT(NVARCHAR(8),lvleaveIssue_tran.leavedate,112) = @efdate " & _
            '       ") AS LV ON LV.employeeunkid = hremployee_master.employeeunkid " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         T.EmpId " & _
            '       "        ,T.EOC " & _
            '       "        ,T.LEAVING " & _
            '       "        ,T.isexclude_payroll AS IsExPayroll " & _
            '       "    FROM " & _
            '       "    ( " & _
            '       "        SELECT " & _
            '       "             employeeunkid AS EmpId " & _
            '       "            ,date1 AS EOC " & _
            '       "            ,date2 AS LEAVING " & _
            '       "            ,effectivedate " & _
            '       "            ,isexclude_payroll " & _
            '       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
            '       "        FROM  hremployee_dates_tran WHERE datetypeunkid IN(4) AND isvoid = 0 " & _
            '       "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
            '       "    ) AS T WHERE T.xNo = 1 " & _
            '       ") AS TRM ON TRM.EmpId = hremployee_master.employeeunkid " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         R.EmpId " & _
            '       "        ,R.RETIRE " & _
            '       "    FROM " & _
            '       "    ( " & _
            '       "        SELECT " & _
            '       "             employeeunkid AS EmpId " & _
            '       "            ,date1 AS RETIRE " & _
            '       "            ,effectivedate " & _
            '       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
            '       "        FROM  hremployee_dates_tran WHERE datetypeunkid IN(6) AND isvoid = 0 " & _
            '       "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
            '       "    ) AS R WHERE R.xNo = 1 " & _
            '       ") AS RET ON RET.EmpId = hremployee_master.employeeunkid " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         RH.EmpId " & _
            '       "        ,RH.REHIRE " & _
            '       "    FROM " & _
            '       "    ( " & _
            '       "        SELECT " & _
            '       "             employeeunkid AS EmpId " & _
            '       "            ,reinstatment_date AS REHIRE " & _
            '       "            ,effectivedate " & _
            '       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
            '       "        FROM  hremployee_rehire_tran WHERE isvoid = 0 " & _
            '       "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
            '       "    ) AS RH WHERE RH.xNo = 1 " & _
            '       ") AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
            '       "WHERE hremployee_master.isapproved = 1 "

            StrQ = "DECLARE @efdate as nvarchar(8), @mtdate as nvarchar(8) " & _
                   "SET @efdate = CONVERT(NVARCHAR(8),GETDATE(),112) " & _
                   "SET @mtdate = CONVERT(NVARCHAR(8),GETDATE(),112) " & _
                   "SELECT " & _
                   "     hremployee_master.employeecode AS EmployeeReference " & _
                   "    ,hremployee_master.firstname AS Firstname " & _
                   "    ,CASE WHEN LEN(LTRIM(RTRIM(hremployee_master.othername))) > 0 " & _
                   "          THEN CASE WHEN LEN(LTRIM(RTRIM(hremployee_master.othername))) > 2 THEN UPPER(SUBSTRING(LTRIM(RTRIM(hremployee_master.othername)),0,2)) + '.' " & _
                   "               ELSE CASE WHEN CHARINDEX('.',UPPER(LTRIM(RTRIM(hremployee_master.othername)))) > 0 THEN UPPER(LTRIM(RTRIM(hremployee_master.othername))) " & _
                   "                    ELSE UPPER(LTRIM(RTRIM(hremployee_master.othername)))+'.' END " & _
                   "               END " & _
                   "     ELSE '' END AS InitLet " & _
                   "    ,hremployee_master.surname AS LastName " & _
                   "    ,CASE WHEN hremployee_master.accessunkid < = 0 THEN 0 ELSE hremployee_master.accessunkid END AS CardNumber " & _
                   "    ,CASE WHEN CONVERT(NVARCHAR(8),RH.reinstatment_date,112) > CONVERT(NVARCHAR(8),hremployee_master.appointeddate,112) THEN RH.reinstatment_date ELSE hremployee_master.appointeddate END AS ActiveDate " & _
                   "    ,ISNULL(tt.name,'') AS PersonalData1 " & _
                   "    ,ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),hremployee_master.birthdate,106),' ','-'),NULL) AS PersonalData2 " & _
                   "    ,ISNULL(hrdepartment_master.name,'') AS PersonalData3 " & _
                   "    ,ISNULL(hrjob_master.job_name,'') AS PersonalData4 " & _
                   "    ,ISNULL(hrclassgroup_master.name,'') AS PersonalData5 " & _
                   "    ,ISNULL(hrclasses_master.name,'') AS PersonalData6 " & _
                   "    ,ISNULL(treason,'') AS PersonalData7 " & _
                   "    ,ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),ED.date1,106),' ','-'),NULL) AS PersonalData8 " & _
                   "    ,ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),ED.date2,106),' ','-'),NULL) AS PersonalData9 " & _
                   "    ,ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),RT.date1,106),' ','-'),NULL) AS PersonalData10 " & _
                   "    ,ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),SP.date1,106),' ','-'),NULL) AS PersonalData11 " & _
                   "    ,ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),SP.date2,106),' ','-'),NULL) AS PersonalData12 " & _
                   "    ,ISNULL(ED.CheckType,0) AS eCheckType " & _
                   "    ,ISNULL(LV.CheckType,0) AS lCheckType " & _
                   "    ,ISNULL(RC.CheckType,0) AS cCheckType " & _
                   "    ,ISNULL(RH.CheckType,0) AS hCheckType " & _
                   "    ,ISNULL(RT.CheckType,0) AS rCheckType " & _
                   "    ,ISNULL(SP.CheckType,0) AS sCheckType " & _
                   "    ,ISNULL(TF.CheckType,0) AS tCheckType " & _
                   "    ,ldate AS ldate " & _
                   "    ,eimg.photo " & _
                   "    ,eimg.symsignature " & _
                   "    ,ISNULL(CASE WHEN CONVERT(CHAR(8), hremployee_master.appointeddate,112) <= @efdate " & _
                   "            AND (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) >= @efdate " & _
                   "            AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) >= @efdate " & _
                   "            AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) >= @efdate " & _
                   "     THEN 1 ELSE 0 END,0) AS actstatus " & _
                   "    ,hremployee_master.companyunkid AS PersonalData14 " & _
                   "    ,hremployee_master.employeeunkid AS PersonalData15 " & _
                   "FROM hremployee_master " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_dates_tran.employeeunkid " & _
                   "        ,hremployee_dates_tran.date1 " & _
                   "        ,hremployee_dates_tran.date2 " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                   "        ,CASE WHEN hremployee_dates_tran.rehiretranunkid > 0 THEN RTE.name ELSE TEC.name END AS treason " & _
                   "        ,5 AS CheckType " & _
                   "    FROM hremployee_dates_tran " & _
                   "        LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = hremployee_dates_tran.changereasonunkid AND RTE.mastertype = '49' " & _
                   "        LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = hremployee_dates_tran.changereasonunkid AND TEC.mastertype = '48' " & _
                   "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
                   "    AND hremployee_dates_tran.datetypeunkid = 4 " & _
                   ") AS ED ON ED.employeeunkid = hremployee_master.employeeunkid AND ED.rno = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_dates_tran.employeeunkid " & _
                   "        ,hremployee_dates_tran.date1 " & _
                   "        ,hremployee_dates_tran.date2 " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                   "        ,3 AS CheckType " & _
                   "    FROM hremployee_dates_tran " & _
                   "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
                   "    AND hremployee_dates_tran.datetypeunkid = 3 " & _
                   ") AS SP ON SP.employeeunkid = hremployee_master.employeeunkid AND SP.rno = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_dates_tran.employeeunkid " & _
                   "        ,hremployee_dates_tran.date1 " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                   "        ,6 AS CheckType " & _
                   "    FROM hremployee_dates_tran " & _
                   "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
                   "    AND hremployee_dates_tran.datetypeunkid = 6 " & _
                   ") AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_categorization_tran.employeeunkid " & _
                   "        ,hremployee_categorization_tran.jobunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                   "        ,2 AS CheckType " & _
                   "    FROM hremployee_categorization_tran " & _
                   "    WHERE hremployee_categorization_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @efdate " & _
                   ") AS RC ON RC.employeeunkid = hremployee_master.employeeunkid AND RC.rno = 1 " & _
                   "LEFT JOIN hrjob_master ON RC.jobunkid = hrjob_master.jobunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_transfer_tran.employeeunkid " & _
                   "        ,hremployee_transfer_tran.departmentunkid " & _
                   "        ,hremployee_transfer_tran.classgroupunkid " & _
                   "        ,hremployee_transfer_tran.classunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                   "        ,1 AS CheckType " & _
                   "FROM hremployee_transfer_tran " & _
                   "WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @efdate " & _
                   ") AS TF ON TF.employeeunkid = hremployee_master.employeeunkid AND TF.rno = 1 " & _
                   "LEFT JOIN hrdepartment_master ON TF.departmentunkid = hrdepartment_master.departmentunkid " & _
                   "LEFT JOIN hrclassgroup_master ON TF.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                   "LEFT JOIN hrclasses_master ON TF.classunkid = hrclasses_master.classesunkid " & _
                   "LEFT JOIN cfcommon_master AS tt ON tt.masterunkid = hremployee_master.titleunkid AND tt.mastertype = 24 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_rehire_tran.employeeunkid " & _
                   "        ,hremployee_rehire_tran.reinstatment_date " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_rehire_tran.employeeunkid ORDER BY hremployee_rehire_tran.effectivedate DESC) AS rno " & _
                   "        ,4 AS CheckType " & _
                   "    FROM hremployee_rehire_tran " & _
                   "    WHERE hremployee_rehire_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_rehire_tran.effectivedate,112) <= @efdate " & _
                   ") AS RH ON RH.employeeunkid = hremployee_master.employeeunkid AND RH.rno = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         lvleaveIssue_master.employeeunkid " & _
                   "        ,lvleaveIssue_tran.leavedate ldate " & _
                   "        ,7 AS CheckType " & _
                   "    FROM lvleaveIssue_tran " & _
                   "    JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                   "    WHERE lvleaveIssue_tran.isvoid = 0 AND lvleaveIssue_master.isvoid = 0 AND CONVERT(NVARCHAR(8),lvleaveIssue_tran.leavedate,112) = @efdate " & _
                   ") AS LV ON LV.employeeunkid = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         T.EmpId " & _
                   "        ,T.EOC " & _
                   "        ,T.LEAVING " & _
                   "        ,T.isexclude_payroll AS IsExPayroll " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             employeeunkid AS EmpId " & _
                   "            ,date1 AS EOC " & _
                   "            ,date2 AS LEAVING " & _
                   "            ,effectivedate " & _
                   "            ,isexclude_payroll " & _
                   "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                   "        FROM  hremployee_dates_tran WHERE datetypeunkid IN(4) AND isvoid = 0 " & _
                   "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
                   "    ) AS T WHERE T.xNo = 1 " & _
                   ") AS TRM ON TRM.EmpId = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         R.EmpId " & _
                   "        ,R.RETIRE " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             employeeunkid AS EmpId " & _
                   "            ,date1 AS RETIRE " & _
                   "            ,effectivedate " & _
                   "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                   "        FROM  hremployee_dates_tran WHERE datetypeunkid IN(6) AND isvoid = 0 " & _
                   "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
                   "    ) AS R WHERE R.xNo = 1 " & _
                   ") AS RET ON RET.EmpId = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         RH.EmpId " & _
                   "        ,RH.REHIRE " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             employeeunkid AS EmpId " & _
                   "            ,reinstatment_date AS REHIRE " & _
                   "            ,effectivedate " & _
                   "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                   "        FROM  hremployee_rehire_tran WHERE isvoid = 0 " & _
                   "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
                   "    ) AS RH WHERE RH.xNo = 1 " & _
                   ") AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
                   "LEFT JOIN arutiimages..hremployee_images AS eimg ON eimg.employeeunkid = hremployee_master.employeeunkid AND eimg.companyunkid = hremployee_master.companyunkid " & _
                   "WHERE hremployee_master.isapproved = 1 "

            'S.SANDEEP [05-Feb-2018] -- START
            'ENHANCEMENT : {TRIGGER FOR IMAGE EXPORT}
            ' AND ED.date1 IS NOT NULL ---------- REMOVED IF DATE IS SET TO NULL 
            'S.SANDEEP [05-Feb-2018] -- END



            'S.SANDEEP [13-NOV-2017] -- START
            'If mblnIsFirstTime = True Then
            '    StrQ &= "AND CONVERT(CHAR(8), hremployee_master.appointeddate,112) <= @efdate " & _
            '            "AND (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) >= @efdate " & _
            '            "AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) >= @efdate " & _
            '            "AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) >= @efdate "
            'End If

            '**************************** ADDED **************************** -> START
            '",CASE WHEN CONVERT(CHAR(8), hremployee_master.appointeddate,112) <= @efdate " & _
            '"AND (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) >= @efdate " & _
            '"AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) >= @efdate " & _
            '"AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN @efdate ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) >= @efdate " & _
            '"THEN 1 ELSE 0 END AS actstatus " & _
            '",hremployee_master.companyunkid -- PersonalData14 {DataImportTable/ImageImportTable} " & _
            '",hremployee_master.employeeunkid -- PersonalData15 {DataImportTable/ImageImportTable} "
            '**************************** ADDED **************************** -> END

            'S.SANDEEP [13-NOV-2017] -- END


            '"    ,eimg.symsignature " & _

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            ' ADDED ==> START
            ' -- LEFT JOIN arutiimages..hremployee_images AS eimg ON eimg.employeeunkid = hremployee_master.employeeunkid AND eimg.companyunkid = hremployee_master.companyunkid
            ' -- ,eimg.photo " & _
            ' -- ,eimg.symsignature " & _
            ' ADDED ==> END
            'S.SANDEEP [11-AUG-2017] -- END

            Try
                Dim oConn As New SqlConnection
                oConn = oCmd.Connection
                Select Case oConn.State
                    Case ConnectionState.Broken, ConnectionState.Closed
                        oConn.Open()
                End Select
            Catch ex As Exception
                Throw ex
            End Try

            Using oDa As New SqlDataAdapter()
                oCmd.CommandText = StrQ
                oDa.SelectCommand = oCmd
                oDa.Fill(dsEmp)
            End Using

        Catch ex As Exception
            WriteLog("GetArutiEmployeeDetails", ex)
        Finally
        End Try
        Return dsEmp
    End Function

    ''' <summary>
    ''' INSERT INTO SYMMETRY DATABASE TABLE (DataImportTable)
    ''' </summary>
    ''' <param name="dr">DAtA ROW FOR EMPLOYEE</param>
    ''' <param name="oCmd">SQL COMMAND OBJECT PARAMETER</param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function Insert_DataTo_Symmetry(ByVal dr As DataRow, ByVal oCmd As SqlCommand, ByVal eRecRequest As enRecordRequest, ByVal strTableName As String) As Boolean 'S.SANDEEP [11-AUG-2017] -- START {strTableName} -- END
        Dim StrQ As String = String.Empty
        Dim EmpRefNo As String = ""
        Try
            'S.SANDEEP [13-NOV-2017] -- START
            If strTableName.ToUpper = "IMAGEIMPORTTABLE" Then
                'S.SANDEEP |19-AUG-2020| -- START
                'ISSUE/ENHANCEMENT : Language Changes
                'If CInt(dr.Item("actstatus")) = 0 Then
                '    Return True
                'End If
                If IsDBNull(dr.Item("actstatus")) = False Then
                    If CInt(dr.Item("actstatus")) = 0 Then
                        Return True
                    End If
                Else
                    Return True
                End If
                'S.SANDEEP |19-AUG-2020| -- END
            End If
            'S.SANDEEP [13-NOV-2017] -- END

            StrQ = "INSERT INTO " & strTableName & " " & _
                   "( " & _
                   "     EmployeeReference " & _
                   "    ,FirstName " & _
                   "    ,InitLet " & _
                   "    ,LastName " & _
                   "    ,CardNumber " & _
                   "    ,ActiveDate " & _
                   "    ,PersonalData1 " & _
                   "    ,PersonalData2 " & _
                   "    ,PersonalData3 " & _
                   "    ,PersonalData4 " & _
                   "    ,PersonalData5 " & _
                   "    ,PersonalData6 " & _
                   "    ,PersonalData7 " & _
                   "    ,PersonalData8 " & _
                   "    ,PersonalData9 " & _
                   "    ,PersonalData10 " & _
                   "    ,PersonalData11 " & _
                   "    ,PersonalData12 " & _
                   "    ,PersonalData13 " & _
                   "    ,PersonalData14 " & _
                   "    ,PersonalData15 " & _
                   "    ,RecordRequest " & _
                   "    ,RecordStatus " & _
                   "    ,ImportNow " & _
                   "    ,ExpiryDate "
            If strTableName.ToUpper = "IMAGEIMPORTTABLE" Then
                StrQ &= "   ,photo "
                '& _
                '"   ,symsignature "
            End If
            StrQ &= ") " & _
                    " Values " & _
                    "( " & _
                    "     @EmployeeReference " & _
                    "    ,@FirstName " & _
                    "    ,@InitLet " & _
                    "    ,@LastName " & _
                    "    ,@CardNumber " & _
                    "    ,@ActiveDate " & _
                    "    ,@PersonalData1 " & _
                    "    ,@PersonalData2 " & _
                    "    ,@PersonalData3 " & _
                    "    ,@PersonalData4 " & _
                    "    ,@PersonalData5 " & _
                    "    ,@PersonalData6 " & _
                    "    ,@PersonalData7 " & _
                    "    ,@PersonalData8 " & _
                    "    ,@PersonalData9 " & _
                    "    ,@PersonalData10 " & _
                    "    ,@PersonalData11 " & _
                    "    ,@PersonalData12" & _
                    "    ,ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),@PersonalData13,106),' ','-'),@PersonalData13) " & _
                    "    ,@PersonalData14 " & _
                    "    ,@PersonalData15 " & _
                    "    ,@RecordRequest " & _
                    "    ,@RecordStatus  " & _
                    "    ,@ImportNow " & _
                    "    ,@ExpiryDate "
            If strTableName.ToUpper = "IMAGEIMPORTTABLE" Then
                StrQ &= "   ,@photo "
                '& _
                '"   ,@symsignature "
            End If
            StrQ &= " ) "

            'S.SANDEEP [13-NOV-2017] -- START
            '****************** ADDED ***************** -> START
            '"    ,@PersonalData14 "
            '"    ,@PersonalData15 "
            '****************** ADDED ***************** -> END
            'S.SANDEEP [13-NOV-2017] -- END


            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            ' REPLACED "DataImportTable" WITH strTableName VARIABLE
            ' ADDED LOGIC FOR PHOTO & SIGNATURE
            'S.SANDEEP [11-AUG-2017] -- END


            EmpRefNo = dr.Item("EmployeeReference").ToString()

            oCmd.Parameters.Clear()
            oCmd.Parameters.AddWithValue("@EmployeeReference", dr.Item("EmployeeReference"))
            oCmd.Parameters.AddWithValue("@FirstName", dr.Item("FirstName"))
            oCmd.Parameters.AddWithValue("@InitLet", dr.Item("InitLet"))
            oCmd.Parameters.AddWithValue("@LastName", dr.Item("LastName"))
            oCmd.Parameters.AddWithValue("@CardNumber", dr.Item("CardNumber"))
            oCmd.Parameters.AddWithValue("@ActiveDate", dr.Item("ActiveDate"))
            oCmd.Parameters.AddWithValue("@PersonalData1", dr.Item("PersonalData1").ToString.Substring(0, IIf(Len(dr.Item("PersonalData1").ToString) < 40, Len(dr.Item("PersonalData1").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData2", dr.Item("PersonalData2"))
            oCmd.Parameters.AddWithValue("@PersonalData3", dr.Item("PersonalData3").ToString.Substring(0, IIf(Len(dr.Item("PersonalData3").ToString) < 40, Len(dr.Item("PersonalData3").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData4", dr.Item("PersonalData4").ToString.Substring(0, IIf(Len(dr.Item("PersonalData4").ToString) < 40, Len(dr.Item("PersonalData4").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData5", dr.Item("PersonalData5").ToString.Substring(0, IIf(Len(dr.Item("PersonalData5").ToString) < 40, Len(dr.Item("PersonalData5").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData6", dr.Item("PersonalData6").ToString.Substring(0, IIf(Len(dr.Item("PersonalData6").ToString) < 40, Len(dr.Item("PersonalData6").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData7", dr.Item("PersonalData7").ToString.Substring(0, IIf(Len(dr.Item("PersonalData7").ToString) < 40, Len(dr.Item("PersonalData7").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData8", dr.Item("PersonalData8"))
            oCmd.Parameters.AddWithValue("@PersonalData9", dr.Item("PersonalData9"))
            oCmd.Parameters.AddWithValue("@PersonalData10", dr.Item("PersonalData10"))
            oCmd.Parameters.AddWithValue("@PersonalData11", dr.Item("PersonalData11"))
            oCmd.Parameters.AddWithValue("@PersonalData12", dr.Item("PersonalData12"))

            'S.SANDEEP [13-NOV-2017] -- START
            oCmd.Parameters.AddWithValue("@PersonalData14", dr.Item("PersonalData14"))
            oCmd.Parameters.AddWithValue("@PersonalData15", dr.Item("PersonalData15"))
            'S.SANDEEP [13-NOV-2017] -- END

            Dim dtDate As Date = Nothing
            If IsDBNull(dr.Item("PersonalData8")) = False AndAlso IsDBNull(dr.Item("PersonalData9")) = False Then
                If Convert.ToDateTime(dr.Item("PersonalData8")) <= Convert.ToDateTime(dr.Item("PersonalData9")) Then
                    dtDate = Convert.ToDateTime(dr.Item("PersonalData8")).Date
                Else
                    dtDate = Convert.ToDateTime(dr.Item("PersonalData9")).Date
                End If
                ''IMPLMENTED DBNULL CHECKPOINT ON 07-JAN-2022
                If IsDBNull(dr.Item("PersonalData10")) = False Then
                    If dtDate.Date > Convert.ToDateTime(dr.Item("PersonalData10")) Then
                        dtDate = Convert.ToDateTime(dr.Item("PersonalData10")).Date
                    End If
                Else
                    WriteLog("PersonalData10(RetirmentDate) not found for :{" & EmpRefNo & "}", Nothing)
                End If
            Else
                ''IMPLMENTED DBNULL CHECKPOINT ON 07-JAN-2022
                If IsDBNull(dr.Item("PersonalData10")) = False Then
                    dtDate = Convert.ToDateTime(dr.Item("PersonalData10")).Date
                Else
                    WriteLog("PersonalData10(RetirmentDate) not found for :{" & EmpRefNo & "}", Nothing)
                End If
            End If
            oCmd.Parameters.AddWithValue("@ExpiryDate", IIf(dtDate = Nothing, DBNull.Value, dtDate))
            oCmd.Parameters.AddWithValue("@RecordRequest", eRecRequest)
            oCmd.Parameters.AddWithValue("@RecordStatus", 0)
            oCmd.Parameters.AddWithValue("@ImportNow", 1)
            oCmd.Parameters.AddWithValue("@PersonalData13", IIf(dtDate = Nothing, DBNull.Value, dtDate))

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            If strTableName.ToUpper = "IMAGEIMPORTTABLE" Then
                'oCmd.Parameters.Add("@photo", SqlDbType.Binary, dr.Item("photo").ToString().Length)
                'oCmd.Parameters("@photo").Value = dr.Item("photo")

                If IsDBNull(dr.Item("photo")) = False Then
                    oCmd.Parameters.Add("@photo", SqlDbType.VarBinary, DirectCast(dr.Item("photo"), Byte()).Length)
                    oCmd.Parameters("@photo").Value = dr.Item("photo")
                Else
                    oCmd.Parameters.Add("@photo", SqlDbType.Binary, 0)
                    oCmd.Parameters("@photo").Value = DBNull.Value
                End If

                'oCmd.Parameters.Add("@symsignature", SqlDbType.Binary, dr.Item("symsignature").ToString().Length)
                'oCmd.Parameters("@symsignature").Value = dr.Item("symsignature")
            End If
            'S.SANDEEP [11-AUG-2017] -- END

            ''S.SANDEEP [11-AUG-2017] -- START
            ''ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            'If strTableName.ToUpper = "IMAGEIMPORTTABLE" Then
            '    oCmd.Parameters.AddWithValue("@photo", dr.Item("photo"))
            '    oCmd.Parameters.AddWithValue("@symsignature", dr.Item("symsignature"))
            'End If
            ''S.SANDEEP [11-AUG-2017] -- END

            Try
                Dim oConn As New SqlConnection
                oConn = oCmd.Connection
                Select Case oConn.State
                    Case ConnectionState.Broken, ConnectionState.Closed
                        oConn.Open()
                End Select
            Catch ex As Exception
                Throw ex
            End Try

            oCmd.CommandText = StrQ
            oCmd.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            WriteLog("QUERY FOR (RefNo:" & EmpRefNo & ") {Insert_DataTo_Symmetry} : " & StrQ, Nothing)
            WriteLog("Insert_DataTo_Symmetry", ex)
        Finally
        End Try
    End Function

    Private Function Update_DataTo_Symmetry(ByVal dr As DataRow, ByVal oCmd As SqlCommand, ByVal eRecRequest As enRecordRequest, ByVal strTableName As String, Optional ByVal dtRow As DataRow = Nothing) As Boolean 'S.SANDEEP [11-AUG-2017] -- START {strTableName} -- END
        Dim StrQ As String = String.Empty
        Dim EmpRefNo As String = ""
        Try
            StrQ = "UPDATE " & strTableName & " SET " & _
                   "     EmployeeReference = @EmployeeReference " & _
                   "    ,FirstName = @FirstName " & _
                   "    ,InitLet = @InitLet " & _
                   "    ,LastName = @LastName " & _
                   "    ,ActiveDate = @ActiveDate " & _
                   "    ,PersonalData1 = @PersonalData1 " & _
                   "    ,PersonalData2 = ISNULL(ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),@PersonalData2,106),' ','-'),NULL),@PersonalData2) " & _
                   "    ,PersonalData3 = @PersonalData3 " & _
                   "    ,PersonalData4 = @PersonalData4 " & _
                   "    ,PersonalData5 = @PersonalData5 " & _
                   "    ,PersonalData6 = @PersonalData6 " & _
                   "    ,PersonalData7 = @PersonalData7 " & _
                   "    ,PersonalData8 = ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),@PersonalData8,106),' ','-'),@PersonalData8) " & _
                   "    ,PersonalData9 = ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),@PersonalData9,106),' ','-'),@PersonalData9) " & _
                   "    ,PersonalData10 = ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),@PersonalData10,106),' ','-'),@PersonalData10) " & _
                   "    ,PersonalData11 = ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),@PersonalData11,106),' ','-'),@PersonalData11) " & _
                   "    ,PersonalData12 = ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),@PersonalData12,106),' ','-'),@PersonalData12) " & _
                   "    ,PersonalData13 = ISNULL(REPLACE(CONVERT(NVARCHAR(MAX),@PersonalData13,106),' ','-'),@PersonalData13) " & _
                   "    ,PersonalData14 = @PersonalData14 " & _
                   "    ,PersonalData15 = @PersonalData15 " & _
                   "    ,RecordRequest = @RecordRequest " & _
                   "    ,RecordStatus = @RecordStatus " & _
                   "    ,ImportNow = @ImportNow " & _
                   "    ,ExpiryDate = @ExpiryDate "

            If strTableName.ToUpper = "IMAGEIMPORTTABLE" Then
                StrQ &= "   ,photo = CASE WHEN photo IS NULL THEN @photo ELSE photo END " & _
                        "   ,symsignature = CASE WHEN symsignature IS NULL THEN @symsignature ELSE symsignature END "
            End If

            StrQ &= "WHERE RecordCount = @RecordCount "

            'S.SANDEEP [13-NOV-2017] -- START
            '****************** ADDED ***************** -> START
            '"    ,@PersonalData14 "
            '"    ,@PersonalData15 "
            '****************** ADDED ***************** -> END
            'S.SANDEEP [13-NOV-2017] -- END

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            ' REPLACED "DataImportTable" WITH strTableName VARIABLE
            ' ADDED LOGIC FOR PHOTO & SIGNATURE
            '"   ,symsignature = CASE WHEN symsignature IS NULL THEN @symsignature ELSE symsignature END "
            'S.SANDEEP [11-AUG-2017] -- END


            EmpRefNo = dr.Item("EmployeeReference").ToString()

            oCmd.Parameters.Clear()
            oCmd.Parameters.AddWithValue("@RecordCount", dr.Item("RecordCount"))
            oCmd.Parameters.AddWithValue("@EmployeeReference", dr.Item("EmployeeReference"))
            oCmd.Parameters.AddWithValue("@FirstName", dr.Item("FirstName"))
            oCmd.Parameters.AddWithValue("@InitLet", dr.Item("InitLet"))
            oCmd.Parameters.AddWithValue("@LastName", dr.Item("LastName"))
            oCmd.Parameters.AddWithValue("@CardNumber", dr.Item("CardNumber"))
            oCmd.Parameters.AddWithValue("@ActiveDate", dr.Item("ActiveDate"))
            oCmd.Parameters.AddWithValue("@PersonalData1", dr.Item("PersonalData1").ToString.Substring(0, IIf(Len(dr.Item("PersonalData1").ToString) < 40, Len(dr.Item("PersonalData1").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData2", dr.Item("PersonalData2"))
            oCmd.Parameters.AddWithValue("@PersonalData3", dr.Item("PersonalData3").ToString.Substring(0, IIf(Len(dr.Item("PersonalData3").ToString) < 40, Len(dr.Item("PersonalData3").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData4", dr.Item("PersonalData4").ToString.Substring(0, IIf(Len(dr.Item("PersonalData4").ToString) < 40, Len(dr.Item("PersonalData4").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData5", dr.Item("PersonalData5").ToString.Substring(0, IIf(Len(dr.Item("PersonalData5").ToString) < 40, Len(dr.Item("PersonalData5").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData6", dr.Item("PersonalData6").ToString.Substring(0, IIf(Len(dr.Item("PersonalData6").ToString) < 40, Len(dr.Item("PersonalData6").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData7", dr.Item("PersonalData7").ToString.Substring(0, IIf(Len(dr.Item("PersonalData7").ToString) < 40, Len(dr.Item("PersonalData7").ToString), 40)))
            oCmd.Parameters.AddWithValue("@PersonalData8", dr.Item("PersonalData8"))
            oCmd.Parameters.AddWithValue("@PersonalData9", dr.Item("PersonalData9"))
            oCmd.Parameters.AddWithValue("@PersonalData10", dr.Item("PersonalData10"))
            oCmd.Parameters.AddWithValue("@PersonalData11", dr.Item("PersonalData11"))
            oCmd.Parameters.AddWithValue("@PersonalData12", dr.Item("PersonalData12"))

            'S.SANDEEP [13-NOV-2017] -- START
            Dim PersonalData14, PersonalData15 As String
            PersonalData14 = "" : PersonalData15 = ""
            If IsDBNull(dr.Item("PersonalData14")) = False Then
                PersonalData14 = dr.Item("PersonalData14")
            Else
                PersonalData14 = dtRow.Item("PersonalData14")
            End If
            If IsDBNull(dr.Item("PersonalData15")) = False Then
                PersonalData15 = dr.Item("PersonalData15")
            Else
                PersonalData15 = dtRow.Item("PersonalData15")
            End If
            oCmd.Parameters.AddWithValue("@PersonalData14", PersonalData14)
            oCmd.Parameters.AddWithValue("@PersonalData15", PersonalData15)
            'S.SANDEEP [13-NOV-2017] -- END

            Dim dtDate As Date = Nothing
            If IsDBNull(dr.Item("PersonalData8")) = False AndAlso IsDBNull(dr.Item("PersonalData9")) = False Then
                If Convert.ToDateTime(dr.Item("PersonalData8")) <= Convert.ToDateTime(dr.Item("PersonalData9")) Then
                    dtDate = Convert.ToDateTime(dr.Item("PersonalData8")).Date
                Else
                    dtDate = Convert.ToDateTime(dr.Item("PersonalData9")).Date
                End If
                ''IMPLMENTED DBNULL CHECKPOINT ON 07-JAN-2022
                If IsDBNull(dr.Item("PersonalData10")) = False Then
                    If dtDate.Date > Convert.ToDateTime(dr.Item("PersonalData10")) Then
                        dtDate = Convert.ToDateTime(dr.Item("PersonalData10")).Date
                    End If
                Else
                    WriteLog("PersonalData10(RetirmentDate) not found for :{" & EmpRefNo & "}", Nothing)
                End If
            Else
                ''IMPLMENTED DBNULL CHECKPOINT ON 07-JAN-2022
                If IsDBNull(dr.Item("PersonalData10")) = False Then
                    dtDate = Convert.ToDateTime(dr.Item("PersonalData10")).Date
                Else
                    WriteLog("PersonalData10(RetirmentDate) not found for :{" & EmpRefNo & "}", Nothing)
                End If
            End If
            oCmd.Parameters.AddWithValue("@ExpiryDate", IIf(dtDate = Nothing, DBNull.Value, dtDate))
            oCmd.Parameters.AddWithValue("@RecordRequest", eRecRequest)
            oCmd.Parameters.AddWithValue("@RecordStatus", 0)
            oCmd.Parameters.AddWithValue("@ImportNow", 1)
            oCmd.Parameters.AddWithValue("@PersonalData13", IIf(dtDate = Nothing, DBNull.Value, dtDate))

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            If strTableName.ToUpper = "IMAGEIMPORTTABLE" Then
                If IsDBNull(dr.Item("photo")) = False Then
                    oCmd.Parameters.Add("@photo", SqlDbType.VarBinary, DirectCast(dr.Item("photo"), Byte()).Length)
                    oCmd.Parameters("@photo").Value = dr.Item("photo")
                Else
                    oCmd.Parameters.Add("@photo", SqlDbType.Binary, 0)
                    oCmd.Parameters("@photo").Value = DBNull.Value
                End If

                If IsDBNull(dr.Item("symsignature")) = False Then
                    oCmd.Parameters.Add("@symsignature", SqlDbType.VarBinary, DirectCast(dr.Item("symsignature"), Byte()).Length)
                    oCmd.Parameters("@symsignature").Value = dr.Item("symsignature")
                Else
                    oCmd.Parameters.Add("@symsignature", SqlDbType.Binary, 0)
                    oCmd.Parameters("@symsignature").Value = DBNull.Value
                End If
            End If
            'S.SANDEEP [11-AUG-2017] -- END

            Try
                Dim oConn As New SqlConnection
                oConn = oCmd.Connection
                Select Case oConn.State
                    Case ConnectionState.Broken, ConnectionState.Closed
                        oConn.Open()
                End Select
            Catch ex As Exception
                Throw ex
            End Try

            oCmd.CommandText = StrQ
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            WriteLog("QUERY FOR (RefNo:" & EmpRefNo & ") {Update_DataTo_Symmetry} : " & StrQ, Nothing)
            WriteLog("Update_DataTo_Symmetry : " & strTableName, ex)
        Finally
        End Try
    End Function

    Private Function GetSymmetryEmployeeDetails(ByVal strECode As String, ByVal oCmd As SqlCommand, ByVal strTableName As String) As DataSet 'S.SANDEEP [11-AUG-2017] -- START {strTableName} -- END
        Dim dsEmp As New DataSet
        Dim StrQ As String = String.Empty
        Try
            StrQ = "SELECT " & _
                   "     ISNULL(" & strTableName & ".EmployeeReference,'') AS EmployeeReference" & _
                   "    ,ISNULL(" & strTableName & ".FirstName,'') AS FirstName " & _
                   "    ,ISNULL(" & strTableName & ".InitLet,'') AS InitLet " & _
                   "    ,ISNULL(" & strTableName & ".LastName,'') AS LastName " & _
                   "    ,ISNULL(" & strTableName & ".CardNumber,0) AS CardNumber " & _
                   "    ," & strTableName & ".ActiveDate " & _
                   "    ," & strTableName & ".PersonalData1 " & _
                   "    ,CASE WHEN " & strTableName & ".PersonalData2 <> 'NULL' THEN CAST(" & strTableName & ".PersonalData2 AS DATETIME) ELSE NULL END AS PersonalData2 " & _
                   "    ," & strTableName & ".PersonalData3 " & _
                   "    ," & strTableName & ".PersonalData4 " & _
                   "    ," & strTableName & ".PersonalData5 " & _
                   "    ," & strTableName & ".PersonalData6 " & _
                   "    ," & strTableName & ".PersonalData7 " & _
                   "    ,CASE WHEN " & strTableName & ".PersonalData8 <> 'NULL' THEN CAST(" & strTableName & ".PersonalData8 AS DATETIME) ELSE NULL END AS PersonalData8 " & _
                   "    ,CASE WHEN " & strTableName & ".PersonalData9 <> 'NULL' THEN CAST(" & strTableName & ".PersonalData9 AS DATETIME) ELSE NULL END AS PersonalData9 " & _
                   "    ,CASE WHEN " & strTableName & ".PersonalData10 <> 'NULL' THEN CAST(" & strTableName & ".PersonalData10 AS DATETIME) ELSE NULL END AS PersonalData10 " & _
                   "    ,CASE WHEN " & strTableName & ".PersonalData11 <> 'NULL' THEN CAST(" & strTableName & ".PersonalData11 AS DATETIME) ELSE NULL END AS PersonalData11 " & _
                   "    ,CASE WHEN " & strTableName & ".PersonalData12 <> 'NULL' THEN CAST(" & strTableName & ".PersonalData12 AS DATETIME) ELSE NULL END AS PersonalData12 " & _
                   "    ," & strTableName & ".PersonalData14 " & _
                   "    ," & strTableName & ".PersonalData15 " & _
                   "    ," & strTableName & ".RecordRequest " & _
                   "    ," & strTableName & ".RecordStatus " & _
                   "    ," & strTableName & ".ImportNow " & _
                   "    ," & strTableName & ".RecordCount "
            If strTableName.ToUpper = "IMAGEIMPORTTABLE" Then
                StrQ &= "   ," & strTableName & ".photo " & _
                        "   ," & strTableName & ".symsignature "
            End If
            StrQ &= "FROM " & strTableName & " WHERE " & strTableName & ".EmployeeReference = '" & strECode & "' "

            'S.SANDEEP [24-NOV-2017] -- START
            ' ---------------------------------- REMOVED
            '"    ,CAST(" & strTableName & ".PersonalData2 AS DATETIME) AS PersonalData2 " & _
            '"    ,CAST(" & strTableName & ".PersonalData8 AS DATETIME) AS PersonalData8 " & _
            '"    ,CAST(" & strTableName & ".PersonalData9 AS DATETIME) AS PersonalData9 " & _
            '"    ,CAST(" & strTableName & ".PersonalData10 AS DATETIME) AS PersonalData10 " & _
            '"    ,CAST(" & strTableName & ".PersonalData11 AS DATETIME) AS PersonalData11 " & _
            '"    ,CAST(" & strTableName & ".PersonalData12 AS DATETIME) AS PersonalData12 " & _
            '"    ," & strTableName & ".PersonalData14 " & _

            ' ---------------------------------- ADDED
            ',CASE WHEN PersonalData2 <> 'NULL' THEN CAST(PersonalData2 AS DATETIME) ELSE NULL END AS PersonalData2 " & _
            ',CASE WHEN PersonalData8 <> 'NULL' THEN CAST(PersonalData8 AS DATETIME) ELSE NULL END AS PersonalData8
            ',CASE WHEN PersonalData9 <> 'NULL' THEN CAST(PersonalData9 AS DATETIME) ELSE NULL END AS PersonalData9
            ',CASE WHEN PersonalData10 <> 'NULL' THEN CAST(PersonalData10 AS DATETIME) ELSE NULL END AS PersonalData10
            ',CASE WHEN PersonalData11 <> 'NULL' THEN CAST(PersonalData11 AS DATETIME) ELSE NULL END AS PersonalData11
            ',CASE WHEN PersonalData12 <> 'NULL' THEN CAST(PersonalData12 AS DATETIME) ELSE NULL END AS PersonalData12
            ',CASE WHEN PersonalData13 <> 'NULL' THEN CAST(PersonalData13 AS DATETIME) ELSE NULL END AS PersonalData13
            'S.SANDEEP [24-NOV-2017] -- END


            'S.SANDEEP [13-NOV-2017] -- START
            '****************** ADDED ***************** -> START
            '"    ,PersonalData14 "
            '"    ,PersonalData15 "
            '****************** ADDED ***************** -> END


            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            ' REPLACED THE "DataImportTable" WITH strTableName VARIABLE
            ' ADDED LOGIC FOR PHOTO & SIGNATURE
            'S.SANDEEP [11-AUG-2017] -- END

            Try
                Dim oConn As New SqlConnection
                oConn = oCmd.Connection
                Select Case oConn.State
                    Case ConnectionState.Broken, ConnectionState.Closed
                        oConn.Open()
                End Select
            Catch ex As Exception
                Throw ex
            End Try

            Using oDa As New SqlDataAdapter()
                oCmd.CommandText = StrQ
                oDa.SelectCommand = oCmd
                oDa.Fill(dsEmp)
            End Using

        Catch ex As Exception
            WriteLog("QUERY {GetSymmetryEmployeeDetails} : " & StrQ, Nothing)
            WriteLog("GetSymmetryEmployeeDetails", ex)
        Finally
        End Try
        Return dsEmp
    End Function

    Private Function UpdateRecordStatus(ByVal strSourceConnString As String, _
                                        ByVal strDestinationConnString As String) As Boolean
        Try
            Using oScrc As New SqlConnection
                oScrc.ConnectionString = strSourceConnString
                Try
                    'oScrc.Open()
                    'Using oSqlCmd As New SqlCommand
                    '    oSqlCmd.Connection = oScrc
                    '    oSqlCmd.CommandTimeout = 0
                    '    Dim dsEmpLst As New DataSet
                    '    dsEmpLst = GetArutiEmployeeDetails(oSqlCmd)
                    '    If dsEmpLst.Tables(0).Rows.Count > 0 Then
                    '        Using oDest As New SqlConnection
                    '            oDest.ConnectionString = strDestinationConnString
                    '            Try
                    '                oDest.Open() : Dim dsList As New DataSet
                    '                For Each dr As DataRow In dsEmpLst.Tables(0).Rows
                    '                    Using oCmd As New SqlCommand
                    '                        oCmd.Connection = oDest
                    '                        oCmd.CommandTimeout = 0
                    '                        dsList = GetSymmetryEmployeeDetails(dr.Item("EmployeeReference"), oCmd)
                    '                        If dsList.Tables(0).Rows.Count <= 0 Then
                    '                            Call Insert_DataTo_Symmetry(dr, oCmd, enRecordRequest.ADD_MODIFY_DETAILS)
                    '                        Else
                    '                            With dsList.Tables(0).Rows(0)
                    '                                If .Item("EmployeeReference") <> dr.Item("EmployeeReference") Then
                    '                                    .Item("EmployeeReference") = dr.Item("EmployeeReference")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("FirstName") <> dr.Item("FirstName") Then
                    '                                    .Item("FirstName") = dr.Item("FirstName")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("InitLet") <> dr.Item("InitLet") Then
                    '                                    .Item("InitLet") = dr.Item("InitLet")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("LastName") <> dr.Item("LastName") Then
                    '                                    .Item("LastName") = dr.Item("LastName")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("CardNumber") <> dr.Item("CardNumber") Then
                    '                                    .Item("CardNumber") = dr.Item("CardNumber")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If CDate(.Item("ActiveDate")).Date <> CDate(dr.Item("ActiveDate")).Date Then
                    '                                    .Item("ActiveDate") = dr.Item("ActiveDate")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("PersonalData1") <> dr.Item("PersonalData1") Then
                    '                                    .Item("PersonalData1") = dr.Item("PersonalData1")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If IsDBNull(dr.Item("PersonalData2")) = False Then
                    '                                    If IsDBNull(.Item("PersonalData2")) = False Then
                    '                                        If Convert.ToDateTime(.Item("PersonalData2")).Date <> Convert.ToDateTime(dr.Item("PersonalData2")).Date Then
                    '                                            .Item("PersonalData2") = dr.Item("PersonalData2")
                    '                                            If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                            .Item("ImportNow") = 1
                    '                                        End If
                    '                                    Else
                    '                                        .Item("PersonalData2") = dr.Item("PersonalData2")
                    '                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                        .Item("ImportNow") = 1
                    '                                    End If
                    '                                End If
                    '                                If .Item("PersonalData3") <> dr.Item("PersonalData3") Then
                    '                                    .Item("PersonalData3") = dr.Item("PersonalData3")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("PersonalData4") <> dr.Item("PersonalData4") Then
                    '                                    .Item("PersonalData4") = dr.Item("PersonalData4")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("PersonalData5") <> dr.Item("PersonalData5") Then
                    '                                    .Item("PersonalData5") = dr.Item("PersonalData5")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("PersonalData6") <> dr.Item("PersonalData6") Then
                    '                                    .Item("PersonalData6") = dr.Item("PersonalData6")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If .Item("PersonalData7") <> dr.Item("PersonalData7") Then
                    '                                    .Item("PersonalData7") = dr.Item("PersonalData7")
                    '                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                    .Item("ImportNow") = 1
                    '                                End If
                    '                                If IsDBNull(dr.Item("PersonalData8")) = False Then
                    '                                    If IsDBNull(.Item("PersonalData8")) = False Then
                    '                                        If Convert.ToDateTime(.Item("PersonalData8")).Date <> Convert.ToDateTime(dr.Item("PersonalData8")).Date Then
                    '                                            .Item("PersonalData8") = dr.Item("PersonalData8")
                    '                                            If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                            .Item("ImportNow") = 1
                    '                                        End If
                    '                                    Else
                    '                                        .Item("PersonalData8") = dr.Item("PersonalData8")
                    '                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                        .Item("ImportNow") = 1
                    '                                    End If
                    '                                End If
                    '                                If IsDBNull(dr.Item("PersonalData9")) = False Then
                    '                                    If IsDBNull(.Item("PersonalData9")) = False Then
                    '                                        If Convert.ToDateTime(.Item("PersonalData9")).Date <> Convert.ToDateTime(dr.Item("PersonalData9")).Date Then
                    '                                            .Item("PersonalData9") = dr.Item("PersonalData9")
                    '                                            If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                            .Item("ImportNow") = 1
                    '                                        End If
                    '                                    Else
                    '                                        .Item("PersonalData9") = dr.Item("PersonalData9")
                    '                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                        .Item("ImportNow") = 1
                    '                                    End If
                    '                                End If

                    '                                If IsDBNull(dr.Item("PersonalData10")) = False Then
                    '                                    If IsDBNull(.Item("PersonalData10")) = False Then
                    '                                        If Convert.ToDateTime(.Item("PersonalData10")).Date <> Convert.ToDateTime(dr.Item("PersonalData10")).Date Then
                    '                                            .Item("PersonalData10") = dr.Item("PersonalData10")
                    '                                            If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                            .Item("ImportNow") = 1
                    '                                        End If
                    '                                    Else
                    '                                        .Item("PersonalData10") = dr.Item("PersonalData10")
                    '                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                        .Item("ImportNow") = 1
                    '                                    End If
                    '                                End If

                    '                                If IsDBNull(dr.Item("PersonalData11")) = False Then
                    '                                    If IsDBNull(.Item("PersonalData11")) = False Then
                    '                                        If Convert.ToDateTime(.Item("PersonalData11")).Date <> Convert.ToDateTime(dr.Item("PersonalData11")).Date Then
                    '                                            .Item("PersonalData11") = dr.Item("PersonalData11")
                    '                                            If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                            .Item("ImportNow") = 1
                    '                                        End If
                    '                                    Else
                    '                                        .Item("PersonalData11") = dr.Item("PersonalData11")
                    '                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                        .Item("ImportNow") = 1
                    '                                    End If
                    '                                End If

                    '                                If IsDBNull(dr.Item("PersonalData12")) = False Then
                    '                                    If IsDBNull(.Item("PersonalData12")) = False Then
                    '                                        If Convert.ToDateTime(.Item("PersonalData12")).Date <> Convert.ToDateTime(dr.Item("PersonalData12")).Date Then
                    '                                            .Item("PersonalData12") = dr.Item("PersonalData12")
                    '                                            If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                            .Item("ImportNow") = 1
                    '                                        End If
                    '                                    Else
                    '                                        .Item("PersonalData12") = dr.Item("PersonalData12")
                    '                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                    '                                        .Item("ImportNow") = 1
                    '                                    End If
                    '                                End If
                    '                            End With

                    '                            Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, IIf(dsList.Tables(0).Rows(0)("RecordRequest") <> 2, enRecordRequest.ADD_MODIFY_DETAILS, dsList.Tables(0).Rows(0)("RecordRequest")))


                    '                            With dsList.Tables(0).Rows(0)
                    '                                '/******* [SUSPENSION] *******/- START -/
                    '                                If IsDBNull(dr.Item("PersonalData11")) = False AndAlso IsDBNull(dr.Item("PersonalData12")) = False Then
                    '                                    If ((Now.Date >= Convert.ToDateTime(dr.Item("PersonalData12")).Date) Or (Now.Date <= Convert.ToDateTime(dr.Item("PersonalData11")).Date)) Then
                    '                                        .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD
                    '                                        .Item("ImportNow") = 1
                    '                                        Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD)
                    '                                        Continue For
                    '                                    Else
                    '                                        If .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD Then
                    '                                            .Item("RecordRequest") = enRecordRequest.ACTIVATE_CARD
                    '                                            .Item("ImportNow") = 1
                    '                                            Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD)
                    '                                            Continue For
                    '                                        End If
                    '                                    End If
                    '                                End If
                    '                                '/******* [SUSPENSION] *******/- END -/

                    '                                '/******* [TERMINATION] *******/- START -/
                    '                                If IsDBNull(dr.Item("PersonalData8")) = False AndAlso IsDBNull(dr.Item("PersonalData9")) = False Then
                    '                                    If ((Now.Date >= Convert.ToDateTime(dr.Item("PersonalData9")).Date) Or (Now.Date <= Convert.ToDateTime(dr.Item("PersonalData8")).Date)) Then
                    '                                        .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD
                    '                                        .Item("ImportNow") = 1
                    '                                        Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD)
                    '                                        Continue For
                    '                                    End If
                    '                                End If
                    '                                '/******* [TERMINATION] *******/- END -/

                    '                                '/******* [RETIREMENT] *******/- START -/
                    '                                If IsDBNull(dr.Item("PersonalData10")) = False Then
                    '                                    If ((Now.Date >= Convert.ToDateTime(dr.Item("PersonalData10")).Date)) Then
                    '                                        .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD
                    '                                        .Item("ImportNow") = 1
                    '                                        Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD)
                    '                                        Continue For
                    '                                    Else
                    '                                        If .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD Then
                    '                                            .Item("RecordRequest") = enRecordRequest.ACTIVATE_CARD
                    '                                            .Item("ImportNow") = 1
                    '                                            Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD)
                    '                                            Continue For
                    '                                        End If
                    '                                    End If
                    '                                End If
                    '                                '/******* [RETIREMENT] *******/- END -/

                    '                                '/******* [LEAVE] *******/- START -/
                    '                                If IsDBNull(dr.Item("ldate")) = False Then
                    '                                    If ((Now.Date = Convert.ToDateTime(dr.Item("ldate")).Date)) Then
                    '                                        .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD
                    '                                        .Item("ImportNow") = 1
                    '                                        Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD)
                    '                                        Continue For
                    '                                    Else
                    '                                        If .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD Then
                    '                                            .Item("RecordRequest") = enRecordRequest.ACTIVATE_CARD
                    '                                            .Item("ImportNow") = 1
                    '                                            Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD)
                    '                                            Continue For
                    '                                        End If
                    '                                    End If
                    '                                End If
                    '                                '/******* [LEAVE] *******/- END -/
                    '                            End With
                    '                        End If
                    '                    End Using
                    '                Next
                    '            Catch ex As Exception
                    '                WriteLog("UpdateRecordStatus Destination Side", ex)
                    '            End Try
                    '        End Using
                    '    End If
                    'End Using

                    oScrc.Open()
                    Using oSqlCmd As New SqlCommand
                        oSqlCmd.Connection = oScrc
                        oSqlCmd.CommandTimeout = 0
                        Dim dsEmpLst As New DataSet
                        dsEmpLst = GetArutiEmployeeDetails(oSqlCmd)

                        'WriteLog("GetArutiEmployeeDetails", Nothing)

                        If dsEmpLst.Tables(0).Rows.Count > 0 Then
                            Using oDest As New SqlConnection
                                oDest.ConnectionString = strDestinationConnString
                                Try
                                    oDest.Open() : Dim dsList As New DataSet
                                    For Each dr As DataRow In dsEmpLst.Tables(0).Rows
                                        For Each iKey As Integer In mdicTableName.Keys
                                            Using oCmd As New SqlCommand
                                                oCmd.Connection = oDest
                                                oCmd.CommandTimeout = 0

                                                If IsDBNull(dr.Item("EmployeeReference")) Then Continue For

                                                'WriteLog("EmployeeReferenceT : " & dr.Item("EmployeeReference"), Nothing)
                                                'WriteLog("Table : " & mdicTableName(iKey), Nothing)

                                                dsList = GetSymmetryEmployeeDetails(dr.Item("EmployeeReference"), oCmd, mdicTableName(iKey).ToString())

                                                'WriteLog("GetSymmetryEmployeeDetails", Nothing)


                                                'WriteLog("Data Found : " & dsList.Tables(0).Rows.Count.ToString(), Nothing)

                                                If dsList.Tables(0).Rows.Count <= 0 Then
                                                    'WriteLog("Insert_DataTo_Symmetry -- START", Nothing)
                                                    Call Insert_DataTo_Symmetry(dr, oCmd, enRecordRequest.ADD_MODIFY_DETAILS, mdicTableName(iKey).ToString())
                                                    'WriteLog("Insert_DataTo_Symmetry -- END", Nothing)
                                                Else
                                                    With dsList.Tables(0).Rows(0)
                                                        'WriteLog("A", Nothing)

                                                        'WriteLog("EmployeeReferenceS : " & .Item("EmployeeReference"), Nothing)
                                                        If IsDBNull(.Item("EmployeeReference")) Then Continue For

                                                        If .Item("EmployeeReference") <> dr.Item("EmployeeReference") Then
                                                            'WriteLog("1", Nothing)
                                                            .Item("EmployeeReference") = dr.Item("EmployeeReference")
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If

                                                        If .Item("FirstName") <> dr.Item("FirstName") Then
                                                            .Item("FirstName") = dr.Item("FirstName")
                                                            'WriteLog("2", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If .Item("InitLet") <> dr.Item("InitLet") Then
                                                            .Item("InitLet") = dr.Item("InitLet")
                                                            'WriteLog("3", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If .Item("LastName") <> dr.Item("LastName") Then
                                                            .Item("LastName") = dr.Item("LastName")
                                                            'WriteLog("4", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If IsDBNull(.Item("CardNumber")) Then
                                                            .Item("CardNumber") = 0
                                                        End If
                                                        If .Item("CardNumber") <> dr.Item("CardNumber") Then
                                                            .Item("CardNumber") = dr.Item("CardNumber")
                                                            'WriteLog("5", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If CDate(.Item("ActiveDate")).Date <> CDate(dr.Item("ActiveDate")).Date Then
                                                            .Item("ActiveDate") = dr.Item("ActiveDate")
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If .Item("PersonalData1") <> dr.Item("PersonalData1") Then
                                                            .Item("PersonalData1") = dr.Item("PersonalData1")
                                                            'WriteLog("6", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If IsDBNull(dr.Item("PersonalData2")) = False Then
                                                            If IsDBNull(.Item("PersonalData2")) = False Then
                                                                If Convert.ToDateTime(.Item("PersonalData2")).Date <> Convert.ToDateTime(dr.Item("PersonalData2")).Date Then
                                                                    .Item("PersonalData2") = dr.Item("PersonalData2")
                                                                    'WriteLog("7", Nothing)
                                                                    'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    If IsDBNull(.Item("RecordRequest")) = False Then
                                                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    Else
                                                                        .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    End If
                                                                    .Item("ImportNow") = 1
                                                                End If
                                                            Else
                                                                'WriteLog("7a", Nothing)
                                                                .Item("PersonalData2") = dr.Item("PersonalData2")
                                                                'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                If IsDBNull(.Item("RecordRequest")) = False Then
                                                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                Else
                                                                    .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                End If
                                                                .Item("ImportNow") = 1
                                                            End If
                                                        End If
                                                        If .Item("PersonalData3") <> dr.Item("PersonalData3") Then
                                                            .Item("PersonalData3") = dr.Item("PersonalData3")
                                                            'WriteLog("8", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If .Item("PersonalData4") <> dr.Item("PersonalData4") Then
                                                            .Item("PersonalData4") = dr.Item("PersonalData4")
                                                            'WriteLog("9", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If .Item("PersonalData5") <> dr.Item("PersonalData5") Then
                                                            .Item("PersonalData5") = dr.Item("PersonalData5")
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If .Item("PersonalData6") <> dr.Item("PersonalData6") Then
                                                            .Item("PersonalData6") = dr.Item("PersonalData6")
                                                            'WriteLog("10", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If .Item("PersonalData7") <> dr.Item("PersonalData7") Then
                                                            .Item("PersonalData7") = dr.Item("PersonalData7")
                                                            'WriteLog("11", Nothing)
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If IsDBNull(dr.Item("PersonalData8")) = False Then
                                                            If IsDBNull(.Item("PersonalData8")) = False AndAlso .Item("PersonalData8").ToString() <> "NULL" Then 'S.SANDEEP [24-NOV-2017] -- START {Additional Condition} -- END
                                                                If Convert.ToDateTime(.Item("PersonalData8")).Date <> Convert.ToDateTime(dr.Item("PersonalData8")).Date Then
                                                                    .Item("PersonalData8") = dr.Item("PersonalData8")
                                                                    'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    'WriteLog("12", Nothing)
                                                                    If IsDBNull(.Item("RecordRequest")) = False Then
                                                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    Else
                                                                        .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    End If
                                                                    .Item("ImportNow") = 1
                                                                End If
                                                            Else
                                                                .Item("PersonalData8") = dr.Item("PersonalData8")
                                                                'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                'WriteLog("12a", Nothing)
                                                                If IsDBNull(.Item("RecordRequest")) = False Then
                                                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                Else
                                                                    .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                End If
                                                                .Item("ImportNow") = 1
                                                            End If
                                                        Else
                                                            'WriteLog("12b", Nothing)
                                                            .Item("PersonalData8") = dr.Item("PersonalData8")
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If
                                                        If IsDBNull(dr.Item("PersonalData9")) = False Then
                                                            If IsDBNull(.Item("PersonalData9")) = False AndAlso .Item("PersonalData9").ToString() <> "NULL" Then 'S.SANDEEP [24-NOV-2017] -- START {Additional Condition} -- END
                                                                If Convert.ToDateTime(.Item("PersonalData9")).Date <> Convert.ToDateTime(dr.Item("PersonalData9")).Date Then
                                                                    .Item("PersonalData9") = dr.Item("PersonalData9")
                                                                    'WriteLog("13", Nothing)
                                                                    'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    If IsDBNull(.Item("RecordRequest")) = False Then
                                                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    Else
                                                                        .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    End If
                                                                    .Item("ImportNow") = 1
                                                                End If
                                                            Else
                                                                'WriteLog("13a", Nothing)
                                                                .Item("PersonalData9") = dr.Item("PersonalData9")
                                                                'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                If IsDBNull(.Item("RecordRequest")) = False Then
                                                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                Else
                                                                    .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                End If
                                                                .Item("ImportNow") = 1
                                                            End If
                                                        Else
                                                            'WriteLog("13b", Nothing)
                                                            .Item("PersonalData9") = dr.Item("PersonalData9")
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If

                                                        If IsDBNull(dr.Item("PersonalData10")) = False Then
                                                            If IsDBNull(.Item("PersonalData10")) = False AndAlso .Item("PersonalData10").ToString() <> "NULL" Then 'S.SANDEEP [24-NOV-2017] -- START {Additional Condition} -- END
                                                                If Convert.ToDateTime(.Item("PersonalData10")).Date <> Convert.ToDateTime(dr.Item("PersonalData10")).Date Then
                                                                    .Item("PersonalData10") = dr.Item("PersonalData10")
                                                                    'WriteLog("14", Nothing)
                                                                    'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    If IsDBNull(.Item("RecordRequest")) = False Then
                                                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    Else
                                                                        .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    End If
                                                                    .Item("ImportNow") = 1
                                                                End If
                                                            Else
                                                                'WriteLog("14a", Nothing)
                                                                .Item("PersonalData10") = dr.Item("PersonalData10")
                                                                'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                If IsDBNull(.Item("RecordRequest")) = False Then
                                                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                Else
                                                                    .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                End If
                                                                .Item("ImportNow") = 1
                                                            End If
                                                        Else
                                                            'WriteLog("14b", Nothing)
                                                            .Item("PersonalData10") = dr.Item("PersonalData10")
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If

                                                        If IsDBNull(dr.Item("PersonalData11")) = False Then
                                                            If IsDBNull(.Item("PersonalData11")) = False AndAlso .Item("PersonalData11").ToString() <> "NULL" Then 'S.SANDEEP [24-NOV-2017] -- START {Additional Condition} -- END
                                                                If Convert.ToDateTime(.Item("PersonalData11")).Date <> Convert.ToDateTime(dr.Item("PersonalData11")).Date Then
                                                                    .Item("PersonalData11") = dr.Item("PersonalData11")
                                                                    'WriteLog("15", Nothing)
                                                                    'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    If IsDBNull(.Item("RecordRequest")) = False Then
                                                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    Else
                                                                        .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    End If
                                                                    .Item("ImportNow") = 1
                                                                End If
                                                            Else
                                                                'WriteLog("15a", Nothing)
                                                                .Item("PersonalData11") = dr.Item("PersonalData11")
                                                                'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                If IsDBNull(.Item("RecordRequest")) = False Then
                                                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                Else
                                                                    .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                End If
                                                                .Item("ImportNow") = 1
                                                            End If
                                                        Else
                                                            'WriteLog("15b", Nothing)
                                                            .Item("PersonalData11") = dr.Item("PersonalData11")
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If

                                                        If IsDBNull(dr.Item("PersonalData12")) = False Then
                                                            If IsDBNull(.Item("PersonalData12")) = False AndAlso .Item("PersonalData12").ToString() <> "NULL" Then 'S.SANDEEP [24-NOV-2017] -- START {Additional Condition} -- END
                                                                If Convert.ToDateTime(.Item("PersonalData12")).Date <> Convert.ToDateTime(dr.Item("PersonalData12")).Date Then
                                                                    .Item("PersonalData12") = dr.Item("PersonalData12")
                                                                    'WriteLog("16", Nothing)
                                                                    'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    If IsDBNull(.Item("RecordRequest")) = False Then
                                                                        If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    Else
                                                                        .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                    End If
                                                                    .Item("ImportNow") = 1
                                                                End If
                                                            Else
                                                                'WriteLog("16a", Nothing)
                                                                .Item("PersonalData12") = dr.Item("PersonalData12")
                                                                'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                If IsDBNull(.Item("RecordRequest")) = False Then
                                                                    If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                Else
                                                                    .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                                End If
                                                                .Item("ImportNow") = 1
                                                            End If
                                                        Else
                                                            'WriteLog("16b", Nothing)
                                                            .Item("PersonalData12") = dr.Item("PersonalData12")
                                                            'If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            If IsDBNull(.Item("RecordRequest")) = False Then
                                                                If CInt(.Item("RecordRequest")) <> 2 Then .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            Else
                                                                .Item("RecordRequest") = enRecordRequest.ADD_MODIFY_DETAILS
                                                            End If
                                                            .Item("ImportNow") = 1
                                                        End If

                                                        'S.SANDEEP [11-AUG-2017] -- START
                                                        'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                                                        If mdicTableName(iKey).ToString().ToUpper = "IMAGEIMPORTTABLE" Then
                                                            'WriteLog("17", Nothing)
                                                            If IsDBNull(.Item("photo")) = True Then .Item("photo") = dr.Item("photo")
                                                            If IsDBNull(.Item("symsignature")) = True Then .Item("symsignature") = dr.Item("symsignature")
                                                        End If
                                                        'S.SANDEEP [11-AUG-2017] -- END
                                                    End With

                                                    'Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, IIf(dsList.Tables(0).Rows(0)("RecordRequest") <> 2, enRecordRequest.ADD_MODIFY_DETAILS, dsList.Tables(0).Rows(0)("RecordRequest")), mdicTableName(iKey).ToString(), dr)
                                                    If IsDBNull(dsList.Tables(0).Rows(0)("RecordRequest")) = False Then
                                                        'WriteLog("18", Nothing)
                                                        Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, IIf(dsList.Tables(0).Rows(0)("RecordRequest") <> 2, enRecordRequest.ADD_MODIFY_DETAILS, dsList.Tables(0).Rows(0)("RecordRequest")), mdicTableName(iKey).ToString(), dr)
                                                    Else
                                                        'WriteLog("19", Nothing)
                                                        Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.ADD_MODIFY_DETAILS, mdicTableName(iKey).ToString(), dr)
                                                    End If



                                                    With dsList.Tables(0).Rows(0)
                                                        '/******* [SUSPENSION] *******/- START -/
                                                        If IsDBNull(dr.Item("PersonalData11")) = False AndAlso IsDBNull(dr.Item("PersonalData12")) = False Then
                                                            'S.SANDEEP [04-SEP-2017] -- START
                                                            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                                                            'If ((Now.Date >= Convert.ToDateTime(dr.Item("PersonalData12")).Date) Or (Now.Date <= Convert.ToDateTime(dr.Item("PersonalData11")).Date)) Then

                                                            'WriteLog("20", Nothing)

                                                            If ((Now.Date >= Convert.ToDateTime(dr.Item("PersonalData11")).Date) Or (Now.Date <= Convert.ToDateTime(dr.Item("PersonalData12")).Date)) Then
                                                                'S.SANDEEP [04-SEP-2017] -- END
                                                                .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD
                                                                .Item("ImportNow") = 1
                                                                'WriteLog("20a", Nothing)
                                                                Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD, mdicTableName(iKey).ToString(), dr)
                                                                Continue For
                                                            Else
                                                                If .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD Then
                                                                    .Item("RecordRequest") = enRecordRequest.ACTIVATE_CARD
                                                                    .Item("ImportNow") = 1
                                                                    'S.SANDEEP [04-SEP-2017] -- START
                                                                    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                                                                    'Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD, mdicTableName(iKey).ToString())
                                                                    'WriteLog("20b", Nothing)
                                                                    Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.ACTIVATE_CARD, mdicTableName(iKey).ToString(), dr)
                                                                    'S.SANDEEP [04-SEP-2017] -- END
                                                                    Continue For
                                                                End If
                                                            End If
                                                        End If
                                                        '/******* [SUSPENSION] *******/- END -/

                                                        '/******* [TERMINATION] *******/- START -/
                                                        If IsDBNull(dr.Item("PersonalData8")) = False AndAlso IsDBNull(dr.Item("PersonalData9")) = False Then
                                                            'S.SANDEEP [04-SEP-2017] -- START
                                                            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                                                            'If ((Now.Date >= Convert.ToDateTime(dr.Item("PersonalData9")).Date) Or (Now.Date <= Convert.ToDateTime(dr.Item("PersonalData8")).Date)) Then
                                                            'WriteLog("21", Nothing)
                                                            If ((Convert.ToDateTime(dr.Item("PersonalData9")).Date <= Now.Date) Or (Convert.ToDateTime(dr.Item("PersonalData8")).Date < Now.Date)) Then
                                                                'S.SANDEEP [04-SEP-2017] -- END
                                                                .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD
                                                                .Item("ImportNow") = 1
                                                                'WriteLog("21a", Nothing)
                                                                Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD, mdicTableName(iKey).ToString(), dr)
                                                                Continue For
                                                                'S.SANDEEP [04-SEP-2017] -- START
                                                                'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                                                            Else
                                                                If .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD Then
                                                                    .Item("RecordRequest") = enRecordRequest.ACTIVATE_CARD
                                                                    .Item("ImportNow") = 1
                                                                    'WriteLog("21b", Nothing)
                                                                    Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.ACTIVATE_CARD, mdicTableName(iKey).ToString(), dr)
                                                                    Continue For
                                                                End If
                                                                'S.SANDEEP [04-SEP-2017] -- END
                                                            End If
                                                        End If
                                                        '/******* [TERMINATION] *******/- END -/

                                                        '/******* [RETIREMENT] *******/- START -/
                                                        If IsDBNull(dr.Item("PersonalData10")) = False Then
                                                            'WriteLog("22", Nothing)
                                                            If ((Convert.ToDateTime(dr.Item("PersonalData10")).Date) <= Now.Date) Then
                                                                .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD
                                                                .Item("ImportNow") = 1
                                                                'WriteLog("22a", Nothing)
                                                                Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD, mdicTableName(iKey).ToString(), dr)
                                                                Continue For
                                                            Else
                                                                If .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD Then
                                                                    .Item("RecordRequest") = enRecordRequest.ACTIVATE_CARD
                                                                    .Item("ImportNow") = 1
                                                                    'S.SANDEEP [04-SEP-2017] -- START
                                                                    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                                                                    'Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD, mdicTableName(iKey).ToString())
                                                                    'WriteLog("22b", Nothing)
                                                                    Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.ACTIVATE_CARD, mdicTableName(iKey).ToString(), dr)
                                                                    'S.SANDEEP [04-SEP-2017] -- END
                                                                    Continue For
                                                                End If
                                                            End If
                                                        End If
                                                        '/******* [RETIREMENT] *******/- END -/

                                                        '/******* [LEAVE] *******/- START -/
                                                        If IsDBNull(dr.Item("ldate")) = False Then
                                                            'WriteLog("23", Nothing)
                                                            If ((Now.Date = Convert.ToDateTime(dr.Item("ldate")).Date)) Then
                                                                .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD
                                                                .Item("ImportNow") = 1
                                                                'WriteLog("23a", Nothing)
                                                                Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD, mdicTableName(iKey).ToString(), dr)
                                                                Continue For
                                                            Else
                                                                If .Item("RecordRequest") = enRecordRequest.INACTIVE_CARD Then
                                                                    .Item("RecordRequest") = enRecordRequest.ACTIVATE_CARD
                                                                    .Item("ImportNow") = 1
                                                                    'S.SANDEEP [04-SEP-2017] -- START
                                                                    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                                                                    'Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.INACTIVE_CARD, mdicTableName(iKey).ToString())
                                                                    'WriteLog("23b", Nothing)
                                                                    Call Update_DataTo_Symmetry(dsList.Tables(0).Rows(0), oCmd, enRecordRequest.ACTIVATE_CARD, mdicTableName(iKey).ToString(), dr)
                                                                    'S.SANDEEP [04-SEP-2017] -- END
                                                                    Continue For
                                                                End If
                                                            End If
                                                        End If
                                                        '/******* [LEAVE] *******/- END -/
                                                    End With
                                                End If
                                            End Using
                                        Next
                                    Next
                                Catch ex As Exception
                                    WriteLog("UpdateRecordStatus Destination Side", ex)
                                End Try
                            End Using
                        End If
                    End Using

                Catch ex As Exception
                    WriteLog("UpdateRecordStatus Source Side", ex)
                End Try
            End Using
        Catch ex As Exception
            WriteLog("UpdateRecordStatus", ex)
        Finally
        End Try
    End Function

    Public Function Decrypt(ByVal vstrStringToBeDecrypted As String, ByVal vstrDecryptionKey As String) As String

        Dim bytDataToBeDecrypted() As Byte = Nothing
        Dim bytTemp() As Byte = Nothing
        Dim bytDecryptionKey() As Byte = Nothing

        Dim objRijndaelManaged As New RijndaelManaged()
        Dim objMemoryStream As MemoryStream = Nothing
        Dim objCryptoStream As CryptoStream = Nothing

        Dim strReturnString As String = String.Empty

        Try
            bytDataToBeDecrypted = Convert.FromBase64String(vstrStringToBeDecrypted)

            vstrDecryptionKey = vstrDecryptionKey.PadRight(32, "X"c)
            bytDecryptionKey = Encoding.ASCII.GetBytes(vstrDecryptionKey.ToCharArray())

            bytTemp = New Byte(bytDataToBeDecrypted.Length) {}

            objMemoryStream = New MemoryStream(bytDataToBeDecrypted)

            objCryptoStream = New CryptoStream(objMemoryStream, objRijndaelManaged.CreateDecryptor(bytDecryptionKey, bytIV), CryptoStreamMode.Read)
            objCryptoStream.Read(bytTemp, 0, bytTemp.Length)

            Return StripNullCharacters(Encoding.Unicode.GetString(bytTemp, 0, bytTemp.Length))

        Catch ex As Exception
            WriteLog("Decrypt", ex)
            Return ""
        Finally
            bytDataToBeDecrypted = Nothing
            bytTemp = Nothing
            bytDecryptionKey = Nothing

            If objMemoryStream IsNot Nothing Then
                objMemoryStream.Close()
            End If
            objMemoryStream = Nothing

            If objCryptoStream IsNot Nothing Then
                objCryptoStream.Close()
            End If
            objCryptoStream = Nothing

            If objRijndaelManaged IsNot Nothing Then
                objRijndaelManaged.Clear()
            End If
            objRijndaelManaged = Nothing
        End Try
    End Function

    Private Function StripNullCharacters(ByVal vstrStringWithNulls As String) As String
        Dim intPosition As Integer = 0
        Dim strStringWithOutNulls As String = Nothing
        intPosition = 1
        strStringWithOutNulls = vstrStringWithNulls

        Do While intPosition > 0
            intPosition = (vstrStringWithNulls.IndexOf(Constants.vbNullChar, intPosition - 1) + 1)

            If intPosition > 0 Then
                strStringWithOutNulls = strStringWithOutNulls.Substring(0, intPosition - 1) & strStringWithOutNulls.Substring(strStringWithOutNulls.Length - (strStringWithOutNulls.Length - intPosition))
            End If

            If intPosition > strStringWithOutNulls.Length Then
                Exit Do
            End If
        Loop

        Return strStringWithOutNulls
    End Function

    Private Function Update_DataTo_Aruti(ByVal strSourceConnString As String, _
                                         ByVal strDestinationConnString As String) As Boolean
        Try
            Using oScrc As New SqlConnection
                oScrc.ConnectionString = strSourceConnString
                Try
                    oScrc.Open()
                    Using oSqlCmd As New SqlCommand
                        oSqlCmd.Connection = oScrc
                        oSqlCmd.CommandTimeout = 0
                        Dim dsEmpLst As New DataSet
                        dsEmpLst = GetSymmetryEmployeeImageDetails(oSqlCmd)
                        If dsEmpLst IsNot Nothing AndAlso dsEmpLst.Tables.Count > 0 Then
                            Using oDest As New SqlConnection
                                oDest.ConnectionString = strDestinationConnString
                                Try
                                    oDest.Open()
                                    For Each dr As DataRow In dsEmpLst.Tables(0).Rows
                                        Using oCmd As New SqlCommand
                                            oCmd.Connection = oDest
                                            oCmd.CommandTimeout = 0
                                            UpdateEmplyeeImage_Signature(dr, oCmd, "arutiimages")
                                            UpdateEmplyeeIdentity(dr, oCmd)
                                        End Using
                                    Next
                                Catch ex As Exception
                                    WriteLog("Update_DataTo_Aruti - Destination Side", ex)
                                End Try
                            End Using
                        End If
                    End Using
                Catch ex As Exception
                    WriteLog("Update_DataTo_Aruti - Source Side", ex)
                End Try
            End Using
        Catch ex As Exception
            WriteLog("Update_DataTo_Aruti", ex)
        Finally
        End Try
    End Function

    Private Sub IsFirstTime(ByVal strSymmetryConnString As String)
        Try
            Using oSQL As New SqlConnection
                oSQL.ConnectionString = strSymmetryConnString
                oSQL.Open()
                Using oSqlCmd As New SqlCommand
                    oSqlCmd.Connection = oSQL
                    oSqlCmd.CommandTimeout = 0
                    Dim xCnt As Integer = 0
                    oSqlCmd.CommandText = "SELECT COUNT(1) FROM DataImportTable "
                    xCnt = oSqlCmd.ExecuteScalar()
                    If xCnt <= 0 Then mblnIsFirstTime = True

                    'S.SANDEEP [05-Feb-2018] -- START
                    'ENHANCEMENT : {TRIGGER FOR IMAGE EXPORT}
                    oSqlCmd.CommandText = "EXEC sp_configure 'show advanced options', 1; " & _
                                          "RECONFIGURE; "
                    oSqlCmd.ExecuteNonQuery()

                    oSqlCmd.CommandText = "EXEC sp_configure 'Ole Automation Procedures', 1; " & _
                                          "RECONFIGURE; "
                    oSqlCmd.ExecuteNonQuery()


                    oSqlCmd.CommandText = "IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_Img_Exp]')) " & _
                                          "DROP TRIGGER [trg_Img_Exp] "
                    oSqlCmd.ExecuteNonQuery()

                    oSqlCmd.CommandText = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_Img_Exp]')) " & _
                                          "BEGIN " & _
                                          " EXEC dbo.sp_executesql @statement = N' " & _
                                          "     CREATE TRIGGER [dbo].[trg_Img_Exp] ON [dbo].[ImageImportTable] " & _
                                          "     AFTER INSERT,UPDATE " & _
                                          "     AS " & _
                                          "     BEGIN " & _
                                          "         IF UPDATE(symsignature) OR UPDATE(photo) " & _
                                          "         BEGIN " & _
                                          "             DECLARE @PathF AS NVARCHAR(MAX),@PathS AS NVARCHAR(MAX),@DataF AS VARBINARY(MAX),@DataS AS VARBINARY(MAX) " & _
                                          "             DECLARE @FileName AS NVARCHAR(MAX),@Obj INT,@OutPath AS NVARCHAR(MAX),@ERefNo AS NVARCHAR(MAX),@result INT " & _
                                          "             SET @PathF = ''C:\Program Data\Security Management System\Import\Faces'' " & _
                                          "             SET @PathS = ''C:\Program Data\Security Management System\Import\Sigs'' " & _
                                          "             SELECT " & _
                                          "                  @FileName = E.EmployeeReference + ''.jpg'' " & _
                                          "                 ,@ERefNo = E.EmployeeReference " & _
                                          "                 ,@DataF = CONVERT(VARBINARY(MAX), E.photo, 1) " & _
                                          "                 ,@DataS = CONVERT(VARBINARY(MAX), E.symsignature, 1) " & _
                                          "             FROM ImageImportTable AS E " & _
                                          "             JOIN INSERTED ON INSERTED.EmployeeReference = E.EmployeeReference " & _
                                          "             WHERE ISNULL(E.FaceFile,'''') = '''' " & _
                                          "             SET @OutPath = '''' " & _
                                          "             IF @DataF IS NOT NULL " & _
                                          "             BEGIN " & _
                                          "                 SET @OutPath = @PathF + ''\''+ @FileName " & _
                                          "                 BEGIN TRY " & _
                                          "                    EXEC sp_OACreate ''ADODB.Stream'' ,@Obj OUTPUT; " & _
                                          "                    EXEC sp_OASetProperty @Obj ,''Type'',1; " & _
                                          "                    EXEC sp_OAMethod @Obj,''Open''; " & _
                                          "                    EXEC sp_OAMethod @Obj,''Write'', NULL, @DataF; " & _
                                          "                    EXEC sp_OAMethod @Obj,''SaveToFile'', NULL,@OutPath, 2; " & _
                                          "                    EXEC sp_OAMethod @Obj,''Close''; " & _
                                          "                    EXEC sp_OADestroy @Obj; " & _
                                          "                 END TRY " & _
                                          "                 BEGIN CATCH " & _
                                          "                    EXEC sp_OADestroy @Obj; " & _
                                          "                 END CATCH " & _
                                          "                 DECLARE @Path AS NVARCHAR(4000) " & _
                                          "                 SET @Path = @PathF + ''\''+ @FileName " & _
                                          "                 EXEC master.dbo.xp_fileexist @Path, @result OUTPUT " & _
                                          "                 IF @result = 1 " & _
                                          "                 BEGIN " & _
                                          "                     UPDATE DataImportTable SET FaceFile = @FileName WHERE EmployeeReference = @ERefNo " & _
                                          "                     UPDATE ImageImportTable SET FaceFile = @FileName WHERE EmployeeReference = @ERefNo " & _
                                          "                 END " & _
                                          "             END " & _
                                          "             SET @OutPath = '''' " & _
                                          "             SET @Path = '''' " & _
                                          "             IF @DataS IS NOT NULL " & _
                                          "             BEGIN " & _
                                          "                 SET @OutPath = @PathS + ''\''+ ''S''+ @FileName " & _
                                          "                 BEGIN TRY " & _
                                          "                     EXEC sp_OACreate ''ADODB.Stream'' ,@Obj OUTPUT; " & _
                                          "                     EXEC sp_OASetProperty @Obj ,''Type'',1; " & _
                                          "                     EXEC sp_OAMethod @Obj,''Open''; " & _
                                          "                     EXEC sp_OAMethod @Obj,''Write'', NULL, @DataS; " & _
                                          "                     EXEC sp_OAMethod @Obj,''SaveToFile'', NULL,@OutPath, 2; " & _
                                          "                     EXEC sp_OAMethod @Obj,''Close''; " & _
                                          "                     EXEC sp_OADestroy @Obj; " & _
                                          "                 END TRY " & _
                                          "                 BEGIN CATCH " & _
                                          "                     EXEC sp_OADestroy @Obj; " & _
                                          "                 END CATCH " & _
                                          "                 SET @Path = @PathF + ''\''+ @FileName " & _
                                          "                 EXEC master.dbo.xp_fileexist @Path, @result OUTPUT " & _
                                          "                 IF @result = 1 " & _
                                          "                 BEGIN " & _
                                          "                     UPDATE DataImportTable SET SignatureFile = ''S''+ @FileName WHERE EmployeeReference = @ERefNo " & _
                                          "                     UPDATE ImageImportTable SET SignatureFile = ''S''+ @FileName WHERE EmployeeReference = @ERefNo " & _
                                          "                 END " & _
                                          "             END " & _
                                          "         END " & _
                                          "     END' " & _
                                          "END "


                    Try
                        Dim oConn As New SqlConnection
                        oConn = oSqlCmd.Connection
                        Select Case oConn.State
                            Case ConnectionState.Broken, ConnectionState.Closed
                                oConn.Open()
                        End Select
                    Catch ex As Exception
                        Throw ex
                    End Try

                    oSqlCmd.ExecuteNonQuery()

                    'S.SANDEEP [05-Feb-2018] -- END

                End Using
            End Using
        Catch ex As Exception
            WriteLog("IsFirstTime : " & strSymmetryConnString, ex)
        Finally
        End Try
    End Sub

    'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
    Private Sub CreateImageImportTable(ByVal strSymmetryConnString As String)
        Try
            Using oSQL As New SqlConnection
                oSQL.ConnectionString = strSymmetryConnString
                oSQL.Open()
                Using oSqlCmd As New SqlCommand
                    oSqlCmd.Connection = oSQL
                    oSqlCmd.CommandTimeout = 0
                    Dim xCnt As Integer = 0
                    oSqlCmd.CommandText = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ImageImportTable]') AND type in (N'U')) " & _
                                            "BEGIN " & _
                                            "CREATE TABLE [dbo].[ImageImportTable]( " & _
                                                 "[RecordCount] [int] IDENTITY(1,1) NOT NULL, " & _
                                                 "[LastName] [nvarchar](40) NULL, " & _
                                                 "[FirstName] [nvarchar](40) NULL, " & _
                                                 "[CardNumber] [int] NULL, " & _
                                                 "[CompanyID] [int] NULL, " & _
                                                 "[CardIssueLevel] [tinyint] NULL, " & _
                                                 "[EmployeeReference] [nvarchar](128) NULL, " & _
                                                 "[PIN] [nvarchar](10) NULL, " & _
                                                 "[PersonalData1] [nvarchar](40) NULL, " & _
                                                 "[PersonalData2] [nvarchar](40) NULL, " & _
                                                 "[PersonalData3] [nvarchar](40) NULL, " & _
                                                 "[PersonalData4] [nvarchar](40) NULL, " & _
                                                 "[PersonalData5] [nvarchar](40) NULL, " & _
                                                 "[PersonalData6] [nvarchar](40) NULL, " & _
                                                 "[PersonalData7] [nvarchar](40) NULL, " & _
                                                 "[PersonalData8] [nvarchar](40) NULL, " & _
                                                 "[PersonalData9] [nvarchar](40) NULL, " & _
                                                 "[PersonalData10] [nvarchar](40) NULL, " & _
                                                 "[ActiveDate] [datetime] NULL, " & _
                                                 "[ExpiryDate] [datetime] NULL, " & _
                                                 "[ReaderGroupID] [int] NULL, " & _
                                                 "[TimeCodeID] [int] NULL, " & _
                                                 "[RecordRequest] [smallint] NULL, " & _
                                                 "[RecordStatus] [smallint] NULL, " & _
                                                 "[InactiveComment] [nvarchar](40) NULL, " & _
                                                 "[Encryption] [int] NULL, " & _
                                                 "[CustomerCode] [int] NULL, " & _
                                                 "[FaceFile] [nvarchar](128) NULL, " & _
                                                 "[SignatureFile] [nvarchar](128) NULL, " & _
                                                 "[InitLet] [nvarchar](40) NULL, " & _
                                                 "[BadgeFormatID] [int] NULL, " & _
                                                 "[PersonalData11] [nvarchar](40) NULL, " & _
                                                 "[PersonalData12] [nvarchar](40) NULL, " & _
                                                 "[PersonalData13] [nvarchar](40) NULL, " & _
                                                 "[PersonalData14] [nvarchar](40) NULL, " & _
                                                 "[PersonalData15] [nvarchar](40) NULL, " & _
                                                 "[PersonalData16] [nvarchar](40) NULL, " & _
                                                 "[PersonalData17] [nvarchar](40) NULL, " & _
                                                 "[PersonalData18] [nvarchar](40) NULL, " & _
                                                 "[PersonalData19] [nvarchar](40) NULL, " & _
                                                 "[PersonalData20] [nvarchar](40) NULL, " & _
                                                 "[PersonalData21] [nvarchar](40) NULL, " & _
                                                 "[PersonalData22] [nvarchar](40) NULL, " & _
                                                 "[PersonalData23] [nvarchar](40) NULL, " & _
                                                 "[PersonalData24] [nvarchar](40) NULL, " & _
                                                 "[PersonalData25] [nvarchar](40) NULL, " & _
                                                 "[PersonalData26] [nvarchar](40) NULL, " & _
                                                 "[PersonalData27] [nvarchar](40) NULL, " & _
                                                 "[PersonalData28] [nvarchar](40) NULL, " & _
                                                 "[PersonalData29] [nvarchar](40) NULL, " & _
                                                 "[PersonalData30] [nvarchar](40) NULL, " & _
                                                 "[PersonalData31] [nvarchar](40) NULL, " & _
                                                 "[PersonalData32] [nvarchar](40) NULL, " & _
                                                 "[PersonalData33] [nvarchar](40) NULL, " & _
                                                 "[PersonalData34] [nvarchar](40) NULL, " & _
                                                 "[PersonalData35] [nvarchar](40) NULL, " & _
                                                 "[PersonalData36] [nvarchar](40) NULL, " & _
                                                 "[PersonalData37] [nvarchar](40) NULL, " & _
                                                 "[PersonalData38] [nvarchar](40) NULL, " & _
                                                 "[PersonalData39] [nvarchar](40) NULL, " & _
                                                 "[PersonalData40] [nvarchar](40) NULL, " & _
                                                 "[PersonalData41] [nvarchar](40) NULL, " & _
                                                 "[PersonalData42] [nvarchar](40) NULL, " & _
                                                 "[PersonalData43] [nvarchar](40) NULL, " & _
                                                 "[PersonalData44] [nvarchar](40) NULL, " & _
                                                 "[PersonalData45] [nvarchar](40) NULL, " & _
                                                 "[PersonalData46] [nvarchar](40) NULL, " & _
                                                 "[PersonalData47] [nvarchar](40) NULL, " & _
                                                 "[PersonalData48] [nvarchar](40) NULL, " & _
                                                 "[PersonalData49] [nvarchar](40) NULL, " & _
                                                 "[PersonalData50] [nvarchar](40) NULL, " & _
                                                 "[HandTemplateValue1] [tinyint] NULL, " & _
                                                 "[HandTemplateValue2] [tinyint] NULL, " & _
                                                 "[HandTemplateValue3] [tinyint] NULL, " & _
                                                 "[HandTemplateValue4] [tinyint] NULL, " & _
                                                 "[HandTemplateValue5] [tinyint] NULL, " & _
                                                 "[HandTemplateValue6] [tinyint] NULL, " & _
                                                 "[HandTemplateValue7] [tinyint] NULL, " & _
                                                 "[HandTemplateValue8] [tinyint] NULL, " & _
                                                 "[HandTemplateValue9] [tinyint] NULL, " & _
                                                 "[ReaderID] [int] NULL, " & _
                                                 "[AccessCodeID] [int] NULL, " & _
                                                 "[ImportNow] [bit] NULL, " & _
                                                 "[BatchReference] [uniqueidentifier] ROWGUIDCOL  NULL, " & _
                                                 "[DefaultBadge] [bit] NULL, " & _
                                                 "[IDSCode] [nvarchar](10) NULL, " & _
                                                 "[AreaID] [int] NULL, " & _
                                                 "[DeactivateAtThreatLevel] [int] NULL, " & _
                                                 "[CardUsageRemaining] [int] NULL, " & _
                                                 "[Priority] [int] NOT NULL, " & _
                                                 "[Lost] [bit] NULL, " & _
                                                 "[AreaOccupancyCard] [bit] NULL, " & _
                                                 "[CardWatch] [bit] NULL, " & _
                                                 "[CommandCard] [bit] NULL, " & _
                                                 "[ConditionalCard] [bit] NULL, " & _
                                                 "[ExecutivePriv] [bit] NULL, " & _
                                                 "[ExtendedAccess] [bit] NULL, " & _
                                                 "[KeyCard] [bit] NULL, " & _
                                                 "[PatrolCard] [bit] NULL, " & _
                                                 "[VisitorEscort] [bit] NULL, " & _
                                                 "[photo] [image] NULL, " & _
                                                 "[symsignature] [image] NULL, " & _
                                             "CONSTRAINT [PK_ImageImportTable] PRIMARY KEY CLUSTERED " & _
                                            "( " & _
                                                 "[RecordCount] ASC " & _
                                            ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] " & _
                                            ") ON [PRIMARY] " & _
                                            "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF__ImageImpor__Prior__37A5467C]') AND type = 'D') " & _
                                            "   BEGIN " & _
                                            "       ALTER TABLE [dbo].[ImageImportTable] ADD  DEFAULT ((100)) FOR [Priority] " & _
                                            "   END " & _
                                            "END "

                    Try
                        Dim oConn As New SqlConnection
                        oConn = oSqlCmd.Connection
                        Select Case oConn.State
                            Case ConnectionState.Broken, ConnectionState.Closed
                                oConn.Open()
                        End Select
                    Catch ex As Exception
                        Throw ex
                    End Try

                    oSqlCmd.ExecuteNonQuery()
                End Using
            End Using
        Catch ex As Exception
            WriteLog("CreateImageImportTable : ", ex)
        Finally
        End Try
    End Sub

    Private Function GetSymmetryEmployeeImageDetails(ByVal oCmd As SqlCommand) As DataSet
        Dim dsEmp As New DataSet
        Dim StrQ As String = String.Empty
        Try
            StrQ = "SELECT " & _
                   "	 DIT.EmployeeReference " & _
                   "    ,ISNULL(CAST(DIT.PIN AS NVARCHAR(MAX)),'') AS  Pin " & _
                   "	,IIT.photo " & _
                   "	,IIT.symsignature " & _
                   "    ,IIT.PersonalData14 AS companyid " & _
                   "    ,IIT.PersonalData15 AS employeeid " & _
                   "FROM ImageImportTable AS IIT " & _
                   " JOIN DataImportTable DIT ON DIT.EmployeeReference = IIT.EmployeeReference "

            'S.SANDEEP [13-NOV-2017] -- START
            '****************** ADDED ******************* -> START
            ',IIT.PersonalData14
            ',IIT.PersonalData15
            '****************** ADDED ******************* -> END
            'S.SANDEEP [13-NOV-2017] -- END

            Try
                Dim oConn As New SqlConnection
                oConn = oCmd.Connection
                Select Case oConn.State
                    Case ConnectionState.Broken, ConnectionState.Closed
                        oConn.Open()
                End Select
            Catch ex As Exception
                Throw ex
            End Try

            Using oDa As New SqlDataAdapter()
                oCmd.CommandText = StrQ
                oDa.SelectCommand = oCmd
                oDa.Fill(dsEmp)
            End Using

        Catch ex As Exception
            WriteLog("QUERY {GetSymmetryEmployeeImageDetails} : " & StrQ, Nothing)
            WriteLog("GetSymmetryEmployeeImageDetails", ex)
        End Try
        Return dsEmp
    End Function

    Private Sub UpdateEmplyeeImage_Signature(ByVal dr As DataRow, ByVal oCmd As SqlCommand, ByVal strDatabaseName As String)
        Dim StrQ As String = String.Empty
        Dim EmpRefNo As String = ""
        Try
            'WriteLog("UpdateEmplyeeImage_Signature - > EmployeeReference : " & dr.Item("EmployeeReference").ToString().Trim, Nothing)

            EmpRefNo = dr.Item("EmployeeReference").ToString()
            Dim iStrQ As String = String.Empty : Dim iRetValue As Integer = -1
            iStrQ = "SELECT " & _
                    "   employeeimgunkid " & _
                    "FROM arutiimages..hremployee_images " & _
                    "   JOIN hremployee_master ON hremployee_images.employeeunkid = hremployee_master.employeeunkid " & _
                    "       AND hremployee_images.companyunkid = hremployee_master.companyunkid " & _
                    "WHERE hremployee_master.employeecode = @employeecode AND arutiimages..hremployee_images.isvoid = 0 "

            oCmd.CommandText = iStrQ
            oCmd.Parameters.Clear()
            oCmd.Parameters.AddWithValue("@employeecode", dr.Item("EmployeeReference"))
            iRetValue = oCmd.ExecuteScalar()

            'WriteLog("UpdateEmplyeeImage_Signature - > iRetValue : " & iRetValue.ToString().Trim, Nothing)

            If iRetValue > 0 Then
                iStrQ = "    UPDATE " & strDatabaseName & "..hremployee_images SET " & _
                        "        photo = CASE WHEN photo IS NULL THEN @photo ELSE photo END " & _
                        "       ,symsignature = CASE WHEN symsignature IS NULL THEN @symsignature ELSE symsignature END " & _
                        "    FROM hremployee_master WHERE hremployee_images.employeeunkid = hremployee_master.employeeunkid " & _
                        "    AND hremployee_images.companyunkid = hremployee_master.companyunkid AND employeecode = @employeecode "

                'iStrQ = "    UPDATE " & strDatabaseName & "..hremployee_images SET " & _
                '        "        photo = CASE WHEN photo IS NULL THEN @photo ELSE photo END " & _
                '        "    FROM hremployee_master WHERE hremployee_images.employeeunkid = hremployee_master.employeeunkid " & _
                '        "    AND hremployee_images.companyunkid = hremployee_master.companyunkid AND employeecode = @employeecode "

            Else
                iStrQ = "INSERT INTO arutiimages..hremployee_images " & _
                        "(companyunkid,employeeunkid,photo,userunkid,isvoid,voiduserunkid,voiddatetime,loginemployeeunkid,voidloginemployeeunkid,symsignature) " & _
                        "VALUES(@companyunkid,@employeeunkid,@photo,@userunkid,@isvoid,@voiduserunkid,@voiddatetime,@loginemployeeunkid,@voidloginemployeeunkid,@symsignature) "

                'iStrQ = "   INSERT INTO " & strDatabaseName & "..hremployee_images " & _
                '        "   (companyunkid,employeeunkid,photo,userunkid,isvoid,voiduserunkid,voiddatetime,loginemployeeunkid,voidloginemployeeunkid) " & _
                '        "   VALUES(@companyunkid,@employeeunkid,@photo,@userunkid,@isvoid,@voiduserunkid,@voiddatetime,@loginemployeeunkid,@voidloginemployeeunkid) "
            End If

            StrQ = "IF EXISTS(SELECT * FROM sys.databases where name = '" & strDatabaseName & "') " & _
                   "BEGIN " & vbCrLf
            StrQ &= " " & iStrQ & " " & vbCrLf
            StrQ &= "END "

            oCmd.Parameters.Clear()
            If iRetValue > 0 Then
                oCmd.Parameters.AddWithValue("@employeecode", dr.Item("EmployeeReference"))
            Else
                oCmd.Parameters.AddWithValue("@companyunkid", dr.Item("companyid"))
                oCmd.Parameters.AddWithValue("@employeeunkid", dr.Item("employeeid"))
                oCmd.Parameters.AddWithValue("@userunkid", 1)
                oCmd.Parameters.AddWithValue("@isvoid", False)
                oCmd.Parameters.AddWithValue("@voiduserunkid", -1)
                oCmd.Parameters.AddWithValue("@voiddatetime", DBNull.Value)
                oCmd.Parameters.AddWithValue("@loginemployeeunkid", 0)
                oCmd.Parameters.AddWithValue("@voidloginemployeeunkid", 0)
            End If

            If IsDBNull(dr.Item("photo")) = False Then
                oCmd.Parameters.Add("@photo", SqlDbType.Binary, DirectCast(dr.Item("photo"), Byte()).Length)
                oCmd.Parameters("@photo").Value = dr.Item("photo")
            Else
                oCmd.Parameters.Add("@photo", SqlDbType.Binary, 0)
                oCmd.Parameters("@photo").Value = dr.Item("photo")
            End If

            If IsDBNull(dr.Item("symsignature")) = False Then
                oCmd.Parameters.Add("@symsignature", SqlDbType.Binary, DirectCast(dr.Item("symsignature"), Byte()).Length)
                oCmd.Parameters("@symsignature").Value = dr.Item("photo")
            Else
                oCmd.Parameters.Add("@symsignature", SqlDbType.Binary, 0)
                oCmd.Parameters("@symsignature").Value = dr.Item("symsignature")
            End If

            Try
                Dim oConn As New SqlConnection
                oConn = oCmd.Connection
                Select Case oConn.State
                    Case ConnectionState.Broken, ConnectionState.Closed
                        oConn.Open()
                End Select
            Catch ex As Exception
                Throw ex
            End Try

            oCmd.CommandText = StrQ
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            WriteLog("QUERY For (RefNo: " & EmpRefNo & ") {UpdateEmplyeeImage_Signature} : " & StrQ, Nothing)
            WriteLog("UpdateEmplyeeImage_Signature", ex)
        End Try
    End Sub

    Private Sub UpdateEmplyeeIdentity(ByVal dr As DataRow, ByVal oCmd As SqlCommand)
        Dim StrQ As String = String.Empty
        Dim intIdTranId As Integer = -1
        Dim EmpRefNo As String = ""
        Try
            EmpRefNo = dr("EmployeeReference").ToString()

            StrQ = "SELECT " & _
                   "    identitytranunkid " & _
                   "FROM hremployee_idinfo_tran " & _
                   "    JOIN hremployee_master ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "WHERE employeecode = @employeecode AND idtypeunkid = @idtypeunkid "

            oCmd.Parameters.Clear()
            oCmd.Parameters.AddWithValue("@employeecode", dr.Item("EmployeeReference"))
            oCmd.Parameters.AddWithValue("@idtypeunkid", mintSelecteIdentityTypeId)
            oCmd.Parameters.AddWithValue("@isdefault", mblnIsMarkAsDefault)

            oCmd.CommandText = StrQ
            intIdTranId = oCmd.ExecuteScalar()

            StrQ = ""

            If intIdTranId <= 0 Then
                StrQ = "INSERT INTO hremployee_idinfo_tran " & _
                       "(employeeunkid,idtypeunkid,identity_no,countryunkid,serial_no,issued_place,dl_class,issue_date,expiry_date,isdefault) " & _
                       "SELECT employeeunkid,@idtypeunkid,@identity_no,0,0,'','',NULL,NULL,@isdefault FROM hremployee_master " & _
                       "WHERE employeecode = @employeecode "

                oCmd.Parameters.Clear()
                oCmd.Parameters.AddWithValue("@employeecode", dr.Item("EmployeeReference"))
                oCmd.Parameters.AddWithValue("@idtypeunkid", mintSelecteIdentityTypeId)
                oCmd.Parameters.AddWithValue("@isdefault", mblnIsMarkAsDefault)
                oCmd.Parameters.AddWithValue("@identity_no", dr.Item("Pin").ToString())

            Else

                StrQ = "UPDATE hremployee_idinfo_tran SET identity_no = @identity_no,isdefault = @isdefault " & _
                       "WHERE identitytranunkid = @identitytranunkid "

                oCmd.Parameters.Clear()
                oCmd.Parameters.AddWithValue("@identitytranunkid", intIdTranId)
                oCmd.Parameters.AddWithValue("@idtypeunkid", mintSelecteIdentityTypeId)
                oCmd.Parameters.AddWithValue("@isdefault", mblnIsMarkAsDefault)
                oCmd.Parameters.AddWithValue("@identity_no", dr.Item("Pin").ToString())

            End If

            Try
                Dim oConn As New SqlConnection
                oConn = oCmd.Connection
                Select Case oConn.State
                    Case ConnectionState.Broken, ConnectionState.Closed
                        oConn.Open()
                End Select
            Catch ex As Exception
                Throw ex
            End Try

            If StrQ.Trim.Length > 0 Then
                oCmd.CommandText = StrQ
                intIdTranId = oCmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
            WriteLog("QUERY For (RefNo : " & EmpRefNo & ") {UpdateEmplyeeIdentity} : " & StrQ, Nothing)
            WriteLog("UpdateEmplyeeIdentity", ex)
        End Try
    End Sub
    'S.SANDEEP [11-AUG-2017] -- END

    'S.SANDEEP [06-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {#}
    Private Sub IsDatabaseAccessible(ByVal strDBName As String, ByVal oCmd As SqlCommand)
        Dim StrQ As String = String.Empty
        Dim intDBId As Integer = 0
        Try
            StrQ = "SELECT database_id " & _
                   "FROM sys.databases " & _
                   "WHERE name = '" & strDBName & "' AND user_access_desc = 'MULTI_USER' "

            oCmd.Parameters.Clear()
            oCmd.CommandText = StrQ

            intDBId = oCmd.ExecuteScalar()

            If intDBId <= 0 Then
                mblnIsDatabaseAccessible = False
            End If

        Catch ex As Exception
            WriteLog("IsMultiuserStatus", ex)
        Finally
        End Try
    End Sub
    'S.SANDEEP [06-Apr-2018] -- END

#End Region

#Region " Service Methods "

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            AddHandler timer.Elapsed, AddressOf OnElapsedTime
            timer.Interval = 600000
            timer.Enabled = True
            EventLog1.WriteEntry("Service Started")
        Catch ex As Exception
            WriteLog("OnStart", ex)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            EventLog1.WriteEntry("Service Stopped")
            timer.Enabled = False
        Catch ex As Exception
            WriteLog("OnStop", ex)
        End Try
    End Sub

    Private Sub OnElapsedTime(ByVal source As Object, ByVal e As ElapsedEventArgs)
        Try
            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            mdicTableName = New Dictionary(Of Integer, String)
            mdicTableName.Add(1, "DataImportTable") : mdicTableName.Add(2, "ImageImportTable")
            'S.SANDEEP [11-AUG-2017] -- END
            mblnIsFirstTime = False
            strArutiConnString = "Data Source=(local)\APAYROLL;Initial Catalog=hrmsConfiguration;User ID=aruti_sa;Password=****"
            strArutiConnString = strArutiConnString.Replace("****", Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee"))
            blnIsAruti = IsConnect(strArutiConnString)

            If blnIsAruti Then
                Call GenerateSymmetryConnectionString(strArutiConnString)
                Call ChangeDatabase(strArutiConnString)
            End If


            'S.SANDEEP [06-Apr-2018] -- START
            'blnIsSymmetry = IsConnect(strSymmetryConnString)
            ''S.SANDEEP [11-AUG-2017] -- START
            ''ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            'If blnIsSymmetry Then
            '    Call CreateImageImportTable(strSymmetryConnString)
            'End If
            ''S.SANDEEP [11-AUG-2017] -- END
            'Call IsFirstTime(strSymmetryConnString)
            'If blnIsSymmetry = True AndAlso blnIsAruti = True Then
            '    timer.Stop()
            '    Call UpdateRecordStatus(strArutiConnString, strSymmetryConnString) 'ARUTI TO SYMMETRY
            '    'S.SANDEEP [11-AUG-2017] -- START
            '    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            '    Call Update_DataTo_Aruti(strSymmetryConnString, strArutiConnString) 'SYMMETRY TO ARUTI
            '    'S.SANDEEP [11-AUG-2017] -- END
            'End If
            If mblnIsDatabaseAccessible Then
                blnIsSymmetry = IsConnect(strSymmetryConnString)
                If blnIsSymmetry Then
                    Call CreateImageImportTable(strSymmetryConnString)
                End If
                Call IsFirstTime(strSymmetryConnString)
                If blnIsSymmetry = True AndAlso blnIsAruti = True Then
                    timer.Stop()
                    Call UpdateRecordStatus(strArutiConnString, strSymmetryConnString) 'ARUTI TO SYMMETRY
                    Call Update_DataTo_Aruti(strSymmetryConnString, strArutiConnString) 'SYMMETRY TO ARUTI
                End If
            End If
            'S.SANDEEP [06-Apr-2018] -- END


        Catch ex As Exception
            WriteLog("OnElapsedTime", ex)
        Finally
            timer.Start()
        End Try
    End Sub

#End Region

End Class
