﻿Imports System
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Globalization
Imports System.IO
Imports System.IO.Compression
Imports System.IO.Packaging
Imports System.Linq
Imports System.Text
Imports System.Xml

Public Class XamlToHtmlConverter
    Private Const mstrModuleName As String = "XamlToHtmlConverter"
    Private _zip As Package
    Private _xamlReader As XmlTextReader
    Private _htmlResult As HtmlResult
    Private _contentUriPrefix As String = String.Empty
    Private AsFullDocument As Boolean

    Public Sub New()
        AsFullDocument = True
    End Sub


    Public Property _AsFullDocument() As Boolean
        Get
            Return AsFullDocument
        End Get
        Set(ByVal value As Boolean)
            AsFullDocument = value
        End Set
    End Property

    Public Property ContentUriPrefix() As String
        Get
            Return _contentUriPrefix
        End Get
        Set(ByVal value As String)
            _contentUriPrefix = If(value, String.Empty)
            If (Not String.IsNullOrEmpty(_contentUriPrefix)) AndAlso (Not _contentUriPrefix.EndsWith("/")) Then
                _contentUriPrefix &= "/"
            End If
        End Set
    End Property

    Public Function ConvertXamlToHtml(ByVal xamlPackageStream As MemoryStream) As HtmlResult
        _zip = IO.Packaging.Package.Open(xamlPackageStream)
        Dim zippart As ZipPackagePart = _zip.GetParts().FirstOrDefault(Function(x) x.Uri.OriginalString = "/Xaml/Document.xaml")
        Using stream = zippart.GetStream()
            Using reader = New StreamReader(stream)
                Dim xaml = reader.ReadToEnd()
                _htmlResult = New HtmlResult()
                ConvertXamlToHtml(xaml)
                Return _htmlResult
            End Using
        End Using
    End Function

    Private Sub ConvertXamlToHtml(ByVal xamlString As String)
        Dim safeXaml = WrapIntoFlowDocument(xamlString)

        _xamlReader = New XmlTextReader(New StringReader(safeXaml))
        Dim htmlStringBuilder = New StringBuilder(100)
        Dim htmlWriter = New XmlTextWriter(New StringWriter(htmlStringBuilder))

        If Not WriteFlowDocument(htmlWriter) Then
            Return
        End If
        _htmlResult._HTML = htmlStringBuilder.ToString()
    End Sub

    Public Shared Function WrapIntoFlowDocument(ByVal xamlString As String) As String
        If String.IsNullOrEmpty(xamlString) Then
            Return String.Empty
        End If
        If xamlString.StartsWith("<FlowDocument") Then
            Return xamlString
        End If
        Return String.Concat("<FlowDocument xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">", xamlString, "</FlowDocument>")
    End Function

    Private Function WriteFlowDocument(ByVal htmlWriter As XmlTextWriter) As Boolean
        ' Xaml content is empty - nothing to convert
        If Not ReadNextToken() Then
            Return False
        End If

        ' Root FlowDocument element is missing
        If _xamlReader.NodeType <> XmlNodeType.Element OrElse _xamlReader.Name <> "FlowDocument" Then
            Return False
        End If

        ' Create a buffer StringBuilder for collecting css properties for inline STYLE attributes
        ' on every element level (it will be re-initialized on every level).
        Dim inlineStyle = New StringBuilder()


        If AsFullDocument Then
            htmlWriter.WriteStartElement("html")

            WriteHead(htmlWriter)

            htmlWriter.WriteStartElement("body")
        End If

        WriteFormattingProperties(htmlWriter, inlineStyle)
        WriteElementContent(htmlWriter, inlineStyle)

        If AsFullDocument Then
            htmlWriter.WriteEndElement()
            htmlWriter.WriteEndElement()
        End If

        Return True
    End Function

    Private Shared Sub WriteHead(ByVal htmlWriter As XmlWriter)
        htmlWriter.WriteStartElement("head")

        htmlWriter.WriteStartElement("meta")
        htmlWriter.WriteAttributeString("http-equiv", "Content-Type")
        htmlWriter.WriteAttributeString("content", "text/html; charset=UTF-8")
        htmlWriter.WriteEndElement()

        htmlWriter.WriteEndElement()
    End Sub

    Private Sub WriteFormattingProperties(ByVal htmlWriter As XmlWriter, ByVal inlineStyle As StringBuilder)
        Debug.Assert(_xamlReader.NodeType = XmlNodeType.Element)

        ' Clear string builder for the inline style
        inlineStyle.Remove(0, inlineStyle.Length)

        If Not _xamlReader.HasAttributes Then
            Return
        End If

        Dim borderSet = False

        Do While _xamlReader.MoveToNextAttribute()
            Dim css As String = Nothing

            Select Case _xamlReader.Name
                ' Character fomatting properties
                ' ------------------------------
                Case "Background"
                    css = "background-color:" & ParseXamlColor(_xamlReader.Value) & ";"
                Case "FontFamily"
                    css = "font-family:" & _xamlReader.Value & ";"
                Case "FontStyle"
                    css = "font-style:" & _xamlReader.Value.ToLower() & ";"
                Case "FontWeight"
                    css = "font-weight:" & _xamlReader.Value.ToLower() & ";"
                Case "FontStretch"
                Case "FontSize"
                    css = "font-size:" & _xamlReader.Value & ";"
                Case "Foreground"
                    css = "color:" & ParseXamlColor(_xamlReader.Value) & ";"
                Case "TextDecorations"
                    css = If(_xamlReader.Value.ToLower() = "strikethrough", "text-decoration:line-through;", "text-decoration:underline;")
                Case "TextEffects"
                Case "Emphasis"
                Case "StandardLigatures"
                Case "Variants"
                Case "Capitals"
                Case "Fraction"

                    ' Paragraph formatting properties
                    ' -------------------------------
                Case "Padding"
                    css = "padding:" & ParseXamlThickness(_xamlReader.Value) & ";"
                Case "Margin"
                    css = "margin:" & ParseXamlThickness(_xamlReader.Value) & ";"
                Case "BorderThickness"
                    css = "border-width:" & ParseXamlThickness(_xamlReader.Value) & ";"
                    borderSet = True
                Case "BorderBrush"
                    css = "border-color:" & ParseXamlColor(_xamlReader.Value) & ";"
                    borderSet = True
                Case "LineHeight"
                Case "TextIndent"
                    css = "text-indent:" & _xamlReader.Value & ";"
                Case "TextAlignment"
                    css = "text-align:" & _xamlReader.Value & ";"
                Case "IsKeptTogether"
                Case "IsKeptWithNext"
                Case "ColumnBreakBefore"
                Case "PageBreakBefore"
                Case "FlowDirection"

                    ' Table/Image attributes
                    ' ----------------
                Case "Width"
                    css = "width:" & _xamlReader.Value & "px;"
                Case "Height"
                    css = "height:" & _xamlReader.Value & "px;"
                Case "ColumnSpan"
                    htmlWriter.WriteAttributeString("colspan", _xamlReader.Value)
                Case "RowSpan"
                    htmlWriter.WriteAttributeString("rowspan", _xamlReader.Value)

                    ' Hyperlink Attributes
                Case "NavigateUri"
                    htmlWriter.WriteAttributeString("href", _xamlReader.Value)

                Case "TargetName"
                    htmlWriter.WriteAttributeString("target", _xamlReader.Value)
            End Select

            If css IsNot Nothing Then
                inlineStyle.Append(css)
            End If
        Loop

        If borderSet Then
            inlineStyle.Append("border-style:solid;mso-element:para-border-div;")
        End If

        ' Return the xamlReader back to element level
        _xamlReader.MoveToElement()
        Debug.Assert(_xamlReader.NodeType = XmlNodeType.Element)
    End Sub

    Private Shared Function ParseXamlColor(ByVal color As String) As String
        ' Remove transparency value
        If color.StartsWith("#") Then
            color = "#" & color.Substring(3)
        End If
        Return color
    End Function

    Private Shared Function ParseXamlThickness(ByVal thickness As String) As String
        Dim values = thickness.Split(","c)
        For i = 0 To values.Length - 1
            Dim value As Double
            If Double.TryParse(values(i), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, value) Then
                values(i) = Math.Ceiling(value).ToString(CultureInfo.InvariantCulture)
            Else
                values(i) = "1"
            End If
        Next i

        Dim cssThickness As String
        Select Case values.Length
            Case 1
                cssThickness = thickness
            Case 2
                cssThickness = values(1) & " " & values(0)
            Case 4
                cssThickness = values(1) & " " & values(2) & " " & values(3) & " " & values(0)
            Case Else
                cssThickness = values(0)
        End Select

        Return cssThickness
    End Function

    Private Sub WriteElementContent(ByVal htmlWriter As XmlTextWriter, ByVal inlineStyle As StringBuilder)
        Try
            Debug.Assert(_xamlReader.NodeType = XmlNodeType.Element)

            Dim elementContentStarted = False

            If _xamlReader.IsEmptyElement Then
                If htmlWriter Is Nothing OrElse inlineStyle.Length <= 0 Then
                    Return
                End If
                ' Output STYLE attribute and clear inlineStyle buffer.
                htmlWriter.WriteAttributeString("style", inlineStyle.ToString())
                inlineStyle.Remove(0, inlineStyle.Length)
            Else
                Do While ReadNextToken() AndAlso _xamlReader.NodeType <> XmlNodeType.EndElement
                    Select Case _xamlReader.NodeType
                        Case XmlNodeType.Element
                            If _xamlReader.Name.Contains(".") Then
                                AddComplexProperty(htmlWriter, inlineStyle)
                            Else
                                If htmlWriter IsNot Nothing AndAlso (Not elementContentStarted) AndAlso inlineStyle.Length > 0 Then
                                    ' Output STYLE attribute and clear inlineStyle buffer.
                                    htmlWriter.WriteAttributeString("style", inlineStyle.ToString())
                                    inlineStyle.Remove(0, inlineStyle.Length)
                                End If
                                elementContentStarted = True
                                WriteElement(htmlWriter, inlineStyle)
                            End If
                            Debug.Assert(_xamlReader.NodeType = XmlNodeType.EndElement OrElse _xamlReader.NodeType = XmlNodeType.Element AndAlso _xamlReader.IsEmptyElement)
                        Case XmlNodeType.Comment
                            If htmlWriter IsNot Nothing Then
                                If (Not elementContentStarted) AndAlso inlineStyle.Length > 0 Then
                                    htmlWriter.WriteAttributeString("style", inlineStyle.ToString())
                                    inlineStyle.Remove(0, inlineStyle.Length)
                                End If
                                htmlWriter.WriteComment(_xamlReader.Value)
                            End If
                            elementContentStarted = True
                        Case XmlNodeType.CDATA, XmlNodeType.Text, XmlNodeType.SignificantWhitespace
                            If htmlWriter IsNot Nothing Then
                                If (Not elementContentStarted) AndAlso inlineStyle.Length > 0 Then
                                    htmlWriter.WriteAttributeString("style", inlineStyle.ToString())
                                    inlineStyle.Remove(0, inlineStyle.Length)
                                End If
                                htmlWriter.WriteString(_xamlReader.Value)
                            End If
                            elementContentStarted = True
                    End Select
                Loop

                Debug.Assert(_xamlReader.NodeType = XmlNodeType.EndElement)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: WriteElementContent; Module Name: " & "XamlToHtmlConverter")
        Finally
        End Try
    End Sub

    Private Sub AddComplexProperty(ByVal htmlWriter As XmlWriter, ByVal inlineStyle As StringBuilder)
        Try
            Debug.Assert(_xamlReader.NodeType = XmlNodeType.Element)

            If htmlWriter IsNot Nothing AndAlso _xamlReader.Name = "Image.Source" Then
                If ReadNextToken() AndAlso _xamlReader.Name = "BitmapImage" Then
                    Dim imageUri = _xamlReader.GetAttribute("UriSource")
                    If Not String.IsNullOrEmpty(imageUri) Then
                        ' check new image
                        If Not _htmlResult._Content.ContainsKey(imageUri) Then
                            Dim entryPath = String.Concat("/Xaml/", imageUri).Replace("/./", "/")
                            Dim zippart As ZipPackagePart = _zip.GetParts().FirstOrDefault(Function(x) x.Uri.OriginalString = entryPath)
                            Using stream = zippart.GetStream()
                                Dim image = ToByteArray(stream)
                                Dim identicalContent = _htmlResult._Content.FirstOrDefault(Function(kvp) image.SequenceEqual(kvp.Value))
                                Dim isIdentical = False
                                'If identicalContent.Key <> Nothing Then
                                '    identicalContent = Not Nothing.Equals(identicalContent)
                                'End If
                                If isIdentical Then
                                    imageUri = identicalContent.Key
                                Else
                                    imageUri = String.Concat(ContentUriPrefix, imageUri).Replace("/./", "/")
                                    _htmlResult._Content(imageUri) = image
                                End If
                            End Using
                        End If

                        ' width & height
                        If inlineStyle IsNot Nothing AndAlso inlineStyle.Length > 0 Then
                            htmlWriter.WriteAttributeString("style", inlineStyle.ToString())
                            inlineStyle.Remove(0, inlineStyle.Length)
                        End If

                        htmlWriter.WriteAttributeString("src", imageUri)

                        Do While _xamlReader.NodeType <> XmlNodeType.EndElement
                            ReadNextToken()
                        Loop
                        Return
                    End If
                End If
            End If

            If inlineStyle IsNot Nothing AndAlso _xamlReader.Name.EndsWith(".TextDecorations") Then
                inlineStyle.Append("text-decoration:underline;")
            End If

            ' Skip the element representing the complex property
            WriteElementContent(Nothing, Nothing)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AddComplexProperty; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub WriteElement(ByVal htmlWriter As XmlTextWriter, ByVal inlineStyle As StringBuilder)
        Debug.Assert(_xamlReader.NodeType = XmlNodeType.Element)

        If htmlWriter Is Nothing Then
            ' Skipping mode; recurse into the xaml element without any output
            WriteElementContent(Nothing, Nothing)
        Else
            Dim htmlElementName As String

            Select Case _xamlReader.Name
                Case "Run", "Span"
                    htmlElementName = "span"
                Case "InlineUIContainer"
                    htmlElementName = "span"
                Case "Bold"
                    htmlElementName = "b"
                Case "Italic"
                    htmlElementName = "i"
                Case "Paragraph"
                    htmlElementName = "p"
                Case "BlockUIContainer"
                    htmlElementName = "div"
                Case "Section"
                    htmlElementName = "div"
                Case "Table"
                    htmlElementName = "table"
                Case "TableColumn"
                    htmlElementName = "col"
                Case "TableRowGroup"
                    htmlElementName = "tbody"
                Case "TableRow"
                    htmlElementName = "tr"
                Case "TableCell"
                    htmlElementName = "td"
                Case "List"
                    Dim marker = _xamlReader.GetAttribute("MarkerStyle")
                    If marker Is Nothing OrElse marker Is "None" OrElse marker Is "Disc" OrElse marker Is "Circle" OrElse marker Is "Square" OrElse marker Is "Box" Then
                        htmlElementName = "ul"
                    Else
                        htmlElementName = "ol"
                    End If
                Case "ListItem"
                    htmlElementName = "li"
                Case "Hyperlink"
                    htmlElementName = "a"
                Case "Image"
                    htmlElementName = "img"
                Case Else
                    htmlElementName = Nothing ' Ignore the element
            End Select

            If htmlElementName IsNot Nothing Then
                htmlWriter.WriteStartElement(htmlElementName)
                WriteFormattingProperties(htmlWriter, inlineStyle)
                WriteElementContent(htmlWriter, inlineStyle)
                htmlWriter.WriteEndElement()
            Else
                ' Skip this unrecognized xaml element
                WriteElementContent(Nothing, Nothing)
            End If
        End If
    End Sub

    Private Function ReadNextToken() As Boolean
        Do While _xamlReader.Read()
            Debug.Assert(_xamlReader.ReadState = ReadState.Interactive, "Reader is expected to be in Interactive state (" & _xamlReader.ReadState & ")")
            Select Case _xamlReader.NodeType
                Case XmlNodeType.Element, XmlNodeType.EndElement, XmlNodeType.None, XmlNodeType.CDATA, XmlNodeType.Text, XmlNodeType.SignificantWhitespace
                    Return True

                Case XmlNodeType.Whitespace
                    If _xamlReader.XmlSpace = XmlSpace.Preserve Then
                        Return True
                    End If
                    ' ignore insignificant whitespace

                Case XmlNodeType.EndEntity, XmlNodeType.EntityReference
                    '  Implement entity reading
                    'xamlReader.ResolveEntity();
                    'xamlReader.Read();
                    'ReadChildNodes( parent, parentBaseUri, xamlReader, positionInfo);

                Case XmlNodeType.Comment
                    Return True
            End Select
        Loop
        Return False
    End Function

    Public Shared Sub CopyTo(ByVal input As Stream, ByVal output As Stream)
        Dim buffer((16 * 1024) - 1) As Byte ' Fairly arbitrary size
        Dim bytesRead As Integer

        'INSTANT VB WARNING: An assignment within expression was extracted from the following statement:
        'ORIGINAL LINE: while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
        bytesRead = input.Read(buffer, 0, buffer.Length)
        Do While bytesRead > 0
            output.Write(buffer, 0, bytesRead)
            bytesRead = input.Read(buffer, 0, buffer.Length)
        Loop
    End Sub

    Private Shared Function ToByteArray(ByVal stream As Stream) As Byte()
        Dim memoryStream = TryCast(stream, MemoryStream)
        If memoryStream IsNot Nothing Then
            Return memoryStream.ToArray()
        End If
        memoryStream = New MemoryStream()
        Using memoryStream
            CopyTo(stream, memoryStream)
            Return memoryStream.ToArray()
        End Using
    End Function

End Class
