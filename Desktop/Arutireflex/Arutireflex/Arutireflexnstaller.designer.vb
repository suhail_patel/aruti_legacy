﻿<System.ComponentModel.RunInstaller(True)> Partial Class Arutireflexnstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ArutiReflexInstaller1 = New System.ServiceProcess.ServiceProcessInstaller
        Me.ArutiReflexInstaller = New System.ServiceProcess.ServiceInstaller
        '
        'ServiceProcessInstaller1
        '
        Me.ArutiReflexInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.ArutiReflexInstaller1.Password = Nothing
        Me.ArutiReflexInstaller1.Username = Nothing
        '
        'ArutiReflexInstaller
        '
        Me.ArutiReflexInstaller.ServiceName = "Arutireflex"
        Me.ArutiReflexInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ArutiReflexInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.ArutiReflexInstaller1, Me.ArutiReflexInstaller})

    End Sub
    Friend WithEvents ArutiReflexInstaller1 As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents ArutiReflexInstaller As System.ServiceProcess.ServiceInstaller

End Class
