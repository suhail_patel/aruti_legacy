﻿Imports System.ComponentModel
Imports System.Configuration.Install
Imports System.ServiceProcess

Public Class Arutireflexnstaller
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add initialization code after the call to InitializeComponent

    End Sub

    Private Sub ArutiReflexInstaller_AfterInstall(ByVal sender As System.Object, ByVal e As System.Configuration.Install.InstallEventArgs) Handles ArutiReflexInstaller.AfterInstall
        Dim sc As New ServiceController(ArutiReflexInstaller.ServiceName)
        sc.Start()
    End Sub
End Class
