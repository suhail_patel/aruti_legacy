﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization
Imports System.Runtime.InteropServices

'Last Message Index = 5

Public Class frmHRLogin

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmHRLogin"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objlogin As clslogin_Tran
    Private mintLoginUnkid As Integer = -1
    Private dvEmployee As DataView
    Friend mblnIsSelfAssessment As Boolean = False
    Dim objFinger As FingerPrintDevice
    Dim objConnection As Object = Nothing
    'Dim mintMachineSrNo As Integer = 1
    Dim mStrtimeSeparator As String = DateTimeFormatInfo.CurrentInfo.TimeSeparator
    ' Private WithEvents ThreadSecuGen As eZee.Common.BackgroundProcessor  ''Pinkal (16-Feb-2011) -- Start
    Private blnIsExit As Boolean = False   'Pinkal (10-Mar-2011) -- Start

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Dim dsMachineSetting As DataSet
    Dim objDictConnection As New Dictionary(Of String, Object)
    'Pinkal (20-Jan-2012) -- End

    'Sohail (02 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Dim gstrIP As String = ""
    Dim gstrPort As String = ""
    'Sohail (02 Nov 2013) -- End

#End Region

    'Pinkal (10-Mar-2011) -- Start

    Public ReadOnly Property _IsExit() As Boolean
        Get
            Return blnIsExit
        End Get
    End Property

    'Pinkal (10-Mar-2011) -- End

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintLoginUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintLoginUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'Pinkal (03-Jan-2011) -- Start

    Private Sub InitializeDevice()
        Try
            Timer1.Enabled = False 'Sohail (02 Nov 2013)

            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then

                dsMachineSetting = New DataSet
                dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

                For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                    'Dim DeviceId As enFingerPrintDevice = CType(ConfigParameter._Object._DeviceId, enFingerPrintDevice)
                    Dim DeviceId As Integer = CInt(dsMachineSetting.Tables(0).Rows(i)("commdeviceid").ToString())

                    'Dim ConnetionTypeId As enFingerPrintConnetionType = CType(ConfigParameter._Object._ConnectionTypeId, enFingerPrintConnetionType)
                    Dim ConnetionTypeId As Integer = CInt(dsMachineSetting.Tables(0).Rows(i)("connectiontypeid").ToString())

                    'If dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString() <> "" Then mintMachineSrNo = CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString())

                    If DeviceId > 0 Then

                        objFinger = New FingerPrintDevice

                        Select Case DeviceId

                            Case enFingerPrintDevice.DigitalPersona 'DigitalPersona

                            Case enFingerPrintDevice.SecuGen 'SecuGen

                            Case enFingerPrintDevice.ZKSoftware 'ZKSoftware
                                objConnection = CType(objFinger.SearchDevice(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), dsMachineSetting.Tables(0).Rows(i)("port").ToString(), DeviceId, ConnetionTypeId), zkemkeeper.CZKEM)

                                If objConnection IsNot Nothing Then
                                    If CType(objConnection, zkemkeeper.CZKEM).RegEvent(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), 65535) Then   'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)

                                        If objDictConnection.ContainsKey(dsMachineSetting.Tables(0).Rows(i)("ip").ToString()) = False Then
                                            objDictConnection.Add(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), objConnection)
                                        End If

                                        If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 1 Then  'FINGERPRINT
                                            AddHandler CType(objConnection, zkemkeeper.CZKEM).OnVerify, AddressOf zkSoftware_OnVerify

                                            'Pinkal (3-Aug-2013) -- Start
                                            'Enhancement : TRA Changes

                                            Dim sOption As String = "~ZKFPVersion"
                                            Dim sValue As String = ""

                                            If CType(objConnection, zkemkeeper.CZKEM).GetSysOption(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), sOption, sValue) Then
                                                If sValue = "" OrElse sValue = "9" Then
                                                    AddHandler CType(objConnection, zkemkeeper.CZKEM).OnAttTransaction, AddressOf zkSoftware_OnAttTransaction
                                                ElseIf sValue = "10" Then
                                                    AddHandler CType(objConnection, zkemkeeper.CZKEM).OnAttTransactionEx, AddressOf zkSoftware_OnAttTransactionEx
                                                End If
                                            End If

                                            'Pinkal (3-Aug-2013) -- End



                                        ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 2 Then    'RFID
                                            AddHandler CType(objConnection, zkemkeeper.CZKEM).OnHIDNum, AddressOf zkSoftware_OnHIDNum
                                        End If

                                    End If
                                End If

                                'Sohail (02 Nov 2013) -- Start
                                'TRA - ENHANCEMENT
                            Case enFingerPrintDevice.BioStar
                                Dim result As Integer
                                Dim m_Handle As Integer = 0
                                Dim device_Id As UInteger = 0
                                Dim deviceType As Integer = 0

                                result = clsBioStar.BS_OpenSocket(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), CInt(dsMachineSetting.Tables(0).Rows(i)("port").ToString()), m_Handle)
                                If result <> clsBioStar.BS_SUCCESS Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Unable to connect the device.") & ":" & result.ToString, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If

                                clsBioStar.BS_GetDeviceID(m_Handle, device_Id, deviceType)
                                If device_Id <= 0 Then
                                    result = clsBioStar.BS_CloseSocket(m_Handle)
                                    Exit Sub
                                End If
                                clsBioStar.BS_SetDeviceID(m_Handle, device_Id, deviceType)

                                result = clsBioStar.BS_ClearLogCache(m_Handle)
                                If result <> clsBioStar.BS_SUCCESS Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Unable to clear log cache.") & ":" & result.ToString, enMsgBoxStyle.Information)
                                    result = clsBioStar.BS_CloseSocket(m_Handle)
                                    Exit Sub
                                End If
                                result = clsBioStar.BS_CloseSocket(m_Handle)

                                gstrIP = dsMachineSetting.Tables(0).Rows(i)("ip").ToString
                                gstrPort = dsMachineSetting.Tables(0).Rows(i)("port").ToString

                                Timer1.Enabled = True
                                'Sohail (02 Nov 2013) -- End

                        End Select

                    End If

                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InitializeDevice", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End

    Private Sub SetColor()
        Try
            txtpassword.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetEmployeeLoginValue(ByVal intEmployeeunkid As Integer, ByVal intShiftunkid As Integer, ByVal intSourceType As Integer, Optional ByVal _logindate As Date = Nothing, Optional ByVal _logintime As DateTime = Nothing)
        Try
            objlogin._Employeeunkid = intEmployeeunkid
            objlogin._Shiftunkid = intShiftunkid

            objlogin._InOutType = 0
            objlogin._SourceType = intSourceType
            objlogin._Userunkid = User._Object._Userunkid


            If dsMachineSetting IsNot Nothing Then

                If _logindate <> Nothing And _logintime <> Nothing Then
                    objlogin._Logindate = _logindate.Date
                    objlogin._checkintime = _logintime
                Else
                    objlogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
                    objlogin._checkintime = CDate(ConfigParameter._Object._CurrentDateAndTime)
                End If


            Else

                objlogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
                objlogin._checkintime = ConfigParameter._Object._CurrentDateAndTime

            End If

            objlogin._Checkouttime = Nothing


            'Pinkal (15-Nov-2013) -- Start
            'Enhancement : Oman Changes
            objlogin._Original_InTime = objlogin._checkintime
            objlogin._Original_OutTime = objlogin._Checkouttime
            'Pinkal (15-Nov-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetEmployeeLogoutValue(ByVal intEmployeeunkid As Integer, ByVal intShiftunkid As Integer, ByVal intSourceType As Integer, Optional ByVal _logoutdate As Date = Nothing, Optional ByVal _logouttime As DateTime = Nothing)
        Try
            objlogin._Employeeunkid = intEmployeeunkid
            objlogin._Shiftunkid = intShiftunkid

            If dsMachineSetting IsNot Nothing Then

                If _logoutdate <> Nothing And _logouttime <> Nothing Then
                    objlogin._Logindate = _logoutdate.Date
                    objlogin._Checkouttime = _logouttime
                Else
                    objlogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
                    objlogin._Checkouttime = ConfigParameter._Object._CurrentDateAndTime

                End If

            Else
                objlogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
                objlogin._Checkouttime = ConfigParameter._Object._CurrentDateAndTime
            End If


            'Pinkal (15-Nov-2013) -- Start
            'Enhancement : Oman Changes
            objlogin._Original_InTime = objlogin._checkintime
            objlogin._Original_OutTime = objlogin._Checkouttime
            'Pinkal (15-Nov-2013) -- End

            Dim wkmins As Double = DateDiff(DateInterval.Minute, objlogin._checkintime, objlogin._Checkouttime)
            objlogin._Workhour = CInt(wkmins * 60)
            objlogin._InOutType = 1
            objlogin._SourceType = intSourceType
            objlogin._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsFill As DataSet
        Try
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetList("Employee", , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetList("Employee")
            'End If

            'If dsFill.Tables("Employee").Rows.Count > 0 Then
            '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '        dsFill.Tables("Employee").DefaultView.RowFilter = "isactive = 1"
            '    End If
            '    dvEmployee = dsFill.Tables("Employee").DefaultView
            'End If

            dsFill = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._IsIncludeInactiveEmp, _
                                         "Employee", _
                                         ConfigParameter._Object._ShowFirstAppointmentDate)

            dvEmployee = dsFill.Tables("Employee").DefaultView

            'S.SANDEEP [04 JUN 2015] -- END

            dgEmployee.AutoGenerateColumns = False

            dgcolEmployeeName.DataPropertyName = "displayname"
            objcolEmployeeunkid.DataPropertyName = "employeeunkid"
            objcolshiftunkid.DataPropertyName = "shiftunkid"
            dgcolEmpName.DataPropertyName = "name"
            dgEmployee.DataSource = dvEmployee

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If dgEmployee.SelectedCells.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
                dgEmployee.Select()
                Return False
            ElseIf txtpassword.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Password cannot be blank. Password is required information."), enMsgBoxStyle.Information)
                txtpassword.Select()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub SetClockInOutStatus(ByVal intEmpUnkId As Integer)
        Try
            If objlogin.GetLoginType(intEmpUnkId) = 1 Then
                btnclockin.Visible = False
                btnClockout.Visible = True
            Else
                btnclockin.Visible = True
                btnClockout.Visible = False
            End If
            txtpassword.Text = ""

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetClockInOutStatus", mstrModuleName)
            btnclockin.Visible = False
            btnClockout.Visible = False
        End Try

    End Sub

    Private Function CheckPassword() As Boolean
        Try
            If Not objlogin.IsValidPassword(CInt(dgEmployee.SelectedRows(0).Cells(objcolEmployeeunkid.Index).Value), Trim(txtpassword.Text)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, password you have entered is incorrect. Please enter the correct password."), enMsgBoxStyle.Information) '?1
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function


    Private Sub EmployeeLogin(ByVal Employeeunkid As Integer, ByVal shiftunkid As Integer, ByVal intSourceType As Integer, ByVal mstrEmployeeName As String, Optional ByVal _logindate As Date = Nothing, Optional ByVal _logintime As DateTime = Nothing)
        Try
            If mblnIsSelfAssessment = False Then

                SetEmployeeLoginValue(Employeeunkid, shiftunkid, intSourceType, _logindate, _logintime)
                objlogin._Holdunkid = objlogin.CheckforHoldEmployee(Employeeunkid, ConfigParameter._Object._CurrentDateAndTime)


                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                'If objlogin.Insert(FinancialYear._Object._DatabaseName, _
                '                   User._Object._Userunkid, _
                '                   FinancialYear._Object._YearUnkid, _
                '                   Company._Object._Companyunkid, _
                '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                '                   True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                '                   ConfigParameter._Object._FirstCheckInLastCheckOut, True, -1, Nothing, "", "") Then

                If objlogin.Insert(FinancialYear._Object._DatabaseName _
                                       , User._Object._Userunkid _
                                       , FinancialYear._Object._YearUnkid _
                                       , Company._Object._Companyunkid _
                                       , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                       , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                       , ConfigParameter._Object._UserAccessModeSetting, True _
                                       , ConfigParameter._Object._IsIncludeInactiveEmp _
                                       , ConfigParameter._Object._PolicyManagementTNA _
                                       , ConfigParameter._Object._DonotAttendanceinSeconds _
                                       , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                       , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                       , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                       , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                       , True, -1, Nothing, "", "") Then

                    'Pinkal (11-AUG-2017) -- End


                    Dim objfrmtimemessagebox As New frmTimeMessageBox
                    objfrmtimemessagebox.mstrMessage = Language.getMessage(mstrModuleName, 4, "You are Successfully Logged in at :")

                    If mstrEmployeeName <> "" Then
                        objfrmtimemessagebox.displayDialog(mstrEmployeeName)
                    Else
                        objfrmtimemessagebox.displayDialog(dgEmployee.SelectedRows(0).Cells(dgcolEmployeeName.Index).Value.ToString)
                    End If
                    objfrmtimemessagebox.Activate()
                End If

            Else
                'Me.Hide()
                'dgEmployee.Rows(0).Selected = True
                'txtpassword.Text = ""
                'txtSearchEmployee.Text = ""
                'Dim objAssessment As New frmSelfAssessmentList
                'objAssessment.StartPosition = FormStartPosition.CenterScreen
                'objAssessment.mblnLogin = True
                'objAssessment.btnAssesorAssessment.Visible = False
                'objAssessment.mintemployeeunkid = mintEmployeeunkid
                'objAssessment.ShowDialog()
                'Me.Show()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EmployeeLogin", mstrModuleName)
        End Try
    End Sub

    Private Sub EmployeeLogout(ByVal Employeeunkid As Integer, ByVal shiftunkid As Integer, ByVal intSourceType As Integer, ByVal mstrEmployeeName As String, Optional ByVal _logoutdate As Date = Nothing, Optional ByVal _logouttime As DateTime = Nothing)
        Try
            If mblnIsSelfAssessment = False Then


                SetEmployeeLogoutValue(Employeeunkid, shiftunkid, intSourceType, _logoutdate, _logouttime)
                objlogin._Holdunkid = objlogin.CheckforHoldEmployee(CInt(dgEmployee.SelectedRows(0).Cells(objcolEmployeeunkid.Index).Value), ConfigParameter._Object._CurrentDateAndTime)


                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                'If objlogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                '                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                '                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                '                        ConfigParameter._Object._DonotAttendanceinSeconds, _
                '                        ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") Then

                If objlogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                          , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                          , ConfigParameter._Object._DonotAttendanceinSeconds _
                                          , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                          , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                          , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                          , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                          , False, -1, Nothing, "", "") Then

                    'Pinkal (11-AUG-2017) -- End

                    Dim objfrmtimemessagebox As New frmTimeMessageBox
                    objfrmtimemessagebox.mstrMessage = Language.getMessage(mstrModuleName, 5, "You are Successfully Logged out at :")

                    If mstrEmployeeName <> "" Then
                        objfrmtimemessagebox.displayDialog(mstrEmployeeName)
                    Else
                        objfrmtimemessagebox.displayDialog(dgEmployee.SelectedRows(0).Cells(dgcolEmployeeName.Index).Value.ToString)
                    End If

                End If

            Else
                'Me.Hide()
                'dgEmployee.Rows(0).Selected = True
                'txtpassword.Text = ""
                'txtSearchEmployee.Text = ""
                'Dim objAssessment As New frmSelfAssessmentList
                'objAssessment.StartPosition = FormStartPosition.CenterScreen
                'objAssessment.mblnLogin = True
                'objAssessment.btnAssesorAssessment.Visible = False
                'objAssessment.mintemployeeunkid = mintEmployeeunkid
                'objAssessment.ShowDialog()
                'Me.Show()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EmployeeLogout", mstrModuleName)
        End Try
    End Sub


#End Region


#Region "Private Events"

    Public Sub zkSoftware_OnVerify(ByVal mintUserId As Integer)

        If mintUserId <> -1 Then

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                    If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 1 Then  'FINGERPRINT


                        btnclose_Click(Nothing, Nothing)
                    End If

                Next

            End If

        End If

    End Sub

    Public Sub zkSoftware_OnHIDNum(ByVal mintCardNo As Integer)

        If mintCardNo <> -1 Then
            Dim intEmpUnkId As Integer = 0
            Dim strName As String = ""
            Dim intShiftunkid As Integer = -1


            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                    If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 2 Then  'RFID

                        Dim objCard As New clsSwipeCardData

                        objCard.VerifyData(mintCardNo.ToString, intEmpUnkId, intShiftunkid, strName)

                        If intEmpUnkId > 0 Then
                            If objlogin.GetLoginType(intEmpUnkId) = 1 Then
                                Call EmployeeLogout(intEmpUnkId, intShiftunkid, enInOutSource.Swipecard, strName)
                            Else
                                Call EmployeeLogin(intEmpUnkId, intShiftunkid, enInOutSource.Swipecard, strName)
                            End If

                        End If


                    End If

                Next

            End If

            btnclose_Click(Nothing, Nothing)


        End If

    End Sub

    Public Sub zkSoftware_OnAttTransaction(ByVal iEnrollNumber As Integer, ByVal iIsInValid As Integer, ByVal iAttState As Integer, ByVal iVerifyMethod As Integer, _
                      ByVal iYear As Integer, ByVal iMonth As Integer, ByVal iDay As Integer, ByVal iHour As Integer, ByVal iMinute As Integer, ByVal iSecond As Integer)
        Try
            Dim strName As String = ""
            Dim intShiftunkid As Integer = -1

            If CInt(iEnrollNumber) > 0 Then

                Dim objEmp As New clsEmployee_Master

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = iEnrollNumber
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iEnrollNumber
                'S.SANDEEP [04 JUN 2015] -- END

                strName = objEmp._Displayname

                If objlogin.GetLoginType(iEnrollNumber) = 1 Then
                    Dim _Logoutdate As Date = CDate(eZeeDate.convertDate(iYear & iMonth.ToString("#00") & iDay.ToString("#00")))
                    Dim _LogoutTime As DateTime = CDate(eZeeDate.convertDate(iYear & iMonth.ToString("#00") & iDay.ToString("#00")) & " " & iHour & mStrtimeSeparator & iMinute & mStrtimeSeparator & iSecond)
                    Call EmployeeLogout(iEnrollNumber, intShiftunkid, enInOutSource.FingerPrint, strName, _Logoutdate, _LogoutTime)
                Else
                    Dim _Logindate As Date = CDate(eZeeDate.convertDate(iYear & iMonth.ToString("#00") & iDay.ToString("#00")))
                    Dim _LoginTime As DateTime = CDate(eZeeDate.convertDate(iYear & iMonth.ToString("#00") & iDay.ToString("#00")) & " " & iHour & mStrtimeSeparator & iMinute & mStrtimeSeparator & iSecond)
                    Call EmployeeLogin(iEnrollNumber, intShiftunkid, enInOutSource.FingerPrint, strName, _Logindate, _LoginTime)
                End If
            End If

            btnclose_Click(Nothing, Nothing)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "zkSoftware_OnAttTransaction", mstrModuleName)
        End Try
    End Sub

    Public Sub zkSoftware_OnAttTransactionEx(ByVal iEnrollNumber As String, ByVal iIsInValid As Integer, ByVal iAttState As Integer, ByVal iVerifyMethod As Integer, _
                      ByVal iYear As Integer, ByVal iMonth As Integer, ByVal iDay As Integer, ByVal iHour As Integer, ByVal iMinute As Integer, ByVal iSecond As Integer, ByVal intWorkCode As Integer)
        Try
            Dim strName As String = ""
            Dim intShiftunkid As Integer = -1

            If CInt(iEnrollNumber) > 0 Then

                Dim objEmp As New clsEmployee_Master

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(iEnrollNumber)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(iEnrollNumber)
                'S.SANDEEP [04 JUN 2015] -- END

                strName = objEmp._Displayname

                If objlogin.GetLoginType(CInt(iEnrollNumber)) = 1 Then
                    Dim _Logoutdate As Date = CDate(eZeeDate.convertDate(iYear & iMonth.ToString("#00") & iDay.ToString("#00")))
                    Dim _LogoutTime As DateTime = CDate(eZeeDate.convertDate(iYear & iMonth.ToString("#00") & iDay.ToString("#00")) & " " & iHour & mStrtimeSeparator & iMinute & mStrtimeSeparator & iSecond)
                    Call EmployeeLogout(CInt(iEnrollNumber), intShiftunkid, enInOutSource.FingerPrint, strName, _Logoutdate, _LogoutTime)
                Else
                    Dim _Logindate As Date = CDate(eZeeDate.convertDate(iYear & iMonth.ToString("#00") & iDay.ToString("#00")))
                    Dim _LoginTime As DateTime = CDate(eZeeDate.convertDate(iYear & iMonth.ToString("#00") & iDay.ToString("#00")) & " " & iHour & mStrtimeSeparator & iMinute & mStrtimeSeparator & iSecond)
                    Call EmployeeLogin(CInt(iEnrollNumber), intShiftunkid, enInOutSource.FingerPrint, strName, _Logindate, _LoginTime)
                End If
            End If

            btnclose_Click(Nothing, Nothing)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "zkSoftware_OnAttTransactionEx", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmHRLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objlogin = New clslogin_Tran
        Try
            Call Set_Logo(Me, enArutiApplicatinType.Aruti_TimeSheet)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            SetColor()

            'Pinkal (10-Mar-2011) -- Start
            'ntfLogin.Visible = False
            objntfLogin.Text = Me.Text
            'Pinkal (10-Mar-2011) -- End

            'Pinkal (03-Jan-2011) -- Start

            InitializeDevice()

            'Pinkal (03-Jan-2011) -- End

            If menAction = enAction.EDIT_ONE Then
                objlogin._Loginunkid = mintLoginUnkid
            End If
            FillList()
            dgEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHRLogin_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHRLogin_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Or dgEmployee.Focused = True Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHRLogin_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHRLogin_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try

            If objConnection IsNot Nothing Then
                CType(objConnection, zkemkeeper.CZKEM).Disconnect()
                GC.RemoveMemoryPressure(objConnection.ToString.Length)
                objConnection = Nothing
            End If
            Me.Dispose()
        Catch ex As AccessViolationException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHRLogin_FormClosed", mstrModuleName)
        End Try
        objlogin = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clslogin_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clslogin_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnclockin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclockin.Click
        Try
            If Validation() Then
                If CheckPassword() Then

                    'Pinkal (03-Jan-2011) -- Start

                    EmployeeLogin(CInt(dgEmployee.SelectedRows(0).Cells(objcolEmployeeunkid.Index).Value), CInt(dgEmployee.SelectedRows(0).Cells(objcolshiftunkid.Index).Value), enInOutSource.LoginPassword, "")

                    'Pinkal (03-Jan-2011) -- End

                    SetClockInOutStatus(CInt(dgEmployee.SelectedRows(0).Cells(objcolEmployeeunkid.Index).Value))
                    If dgEmployee.Rows.Count > 0 Then dgEmployee.Rows(0).Selected = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnclockin_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClockout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClockout.Click
        Try
            If Validation() Then
                If CheckPassword() Then

                    'Pinkal (03-Jan-2011) -- Start

                    EmployeeLogout(CInt(dgEmployee.SelectedRows(0).Cells(objcolEmployeeunkid.Index).Value), CInt(dgEmployee.SelectedRows(0).Cells(objcolshiftunkid.Index).Value), enInOutSource.LoginPassword, "")

                    'Pinkal (03-Jan-2011) -- End

                    SetClockInOutStatus(CInt(dgEmployee.SelectedRows(0).Cells(objcolEmployeeunkid.Index).Value))
                    If dgEmployee.Rows.Count > 0 Then dgEmployee.Rows(0).Selected = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnclockin_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Try
            If dgEmployee.Rows.Count > 0 Then dgEmployee.Rows(0).Selected = True Else dgEmployee.Select()
            txtpassword.Text = ""

            'Pinkal (10-Mar-2011) -- Start
            Me.Hide()
            Me.WindowState = FormWindowState.Minimized
            objntfLogin.Visible = True
            'Pinkal (10-Mar-2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnclose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnswipecard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnswipecard.Click
        Try
            tmrSwipecard.Enabled = True
            txtSwipedata.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnswipecard_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexit.Click
        If objConnection IsNot Nothing Then
            GC.RemoveMemoryPressure(objConnection.ToString.Length)
        End If
        blnIsExit = True 'Pinkal (10-Mar-2011) -- Start
        Me.Close()
    End Sub

#End Region

#Region " TextBox's Event "

    Private Sub txtSearchEmployee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmployee.TextChanged
        Try
            If dvEmployee IsNot Nothing Then
                If dvEmployee.Table.Rows.Count > 0 Then
                    dvEmployee.RowFilter = "displayname like '%" & txtSearchEmployee.Text.Trim & "%'"
                    dgEmployee.Refresh()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmployee_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGridView's Event"

    Private Sub dgEmployee_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgEmployee.SelectionChanged
        Try
            If dgEmployee.SelectedRows.Count > 0 Then
                SetClockInOutStatus(CInt(dgEmployee.SelectedRows(0).Cells(objcolEmployeeunkid.Index).Value))
            Else
                btnclockin.Visible = False
                btnClockout.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Timer Event"

    Private Sub tmrSwipecard_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrSwipecard.Tick
        Dim objEmpswipedata As New clsSwipeCardData
        Dim intEmpUnkId As Integer = 0
        Dim intshiftunkId As Integer = 0
        Dim strName As String = ""
        Try
            tmrSwipecard.Enabled = False
            objEmpswipedata.VerifyData(txtSwipedata.Text, intEmpUnkId, intshiftunkId)

            'Pinkal (03-Jan-2011) -- Start

            If intEmpUnkId > 0 Then
                If objlogin.GetLoginType(intEmpUnkId) = 1 Then
                    Call EmployeeLogout(intEmpUnkId, intshiftunkId, enInOutSource.Swipecard, strName)
                Else
                    Call EmployeeLogin(intEmpUnkId, intshiftunkId, enInOutSource.Swipecard, strName)
                End If
                SetClockInOutStatus(intEmpUnkId)
                Call btnclose_Click(Nothing, Nothing)
            End If

            'Pinkal (03-Jan-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tmrSwipecard_Tick", mstrModuleName)
        Finally
            txtSwipedata.Text = ""
            '   tmrSwipecard.Enabled = True
        End Try
    End Sub

    'Sohail (02 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim m_Handle As Integer = 0
        Dim result As Integer
        Dim numOfLog As Integer
        Dim logTotalCount As Integer = 0
        Dim iEnrollNumber As Integer = 0
        Dim deviceId As UInteger = 0
        Dim deviceType As Integer = 0

        Try

            Timer1.Enabled = False

            result = clsBioStar.BS_OpenSocket(gstrIP, CInt(gstrPort), m_Handle)
            If result <> clsBioStar.BS_SUCCESS Then
                Exit Sub
            End If
            clsBioStar.BS_GetDeviceID(m_Handle, deviceId, deviceType)
            If deviceId <= 0 Then
                result = clsBioStar.BS_CloseSocket(m_Handle)
                Exit Sub
            End If
            clsBioStar.BS_SetDeviceID(m_Handle, deviceId, deviceType)

            Dim logRecord As IntPtr = Marshal.AllocHGlobal(10 * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord)))
            'Dim logRecord As IntPtr = New IntPtr(128)

            Dim buf As New IntPtr(logRecord.ToInt32() + logTotalCount * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord)))

            result = clsBioStar.BS_ReadLogCache(m_Handle, numOfLog, buf)

            If result <> clsBioStar.BS_SUCCESS Then
                Marshal.FreeHGlobal(logRecord)
                Me.Text = "Cannot read log records " & result.ToString
                Exit Sub
            Else
                Me.Text = "Log Count : " & numOfLog.ToString
            End If

            Dim objDeviceUser As New clsEmpid_devicemapping
            Dim eventTime As New DateTime(1970, 1, 1)
            Dim dtDate As Date = Nothing

            If numOfLog > 0 Then
                Cursor.Current = Cursors.WaitCursor
                For i = 0 To numOfLog - 1
                    Dim record As clsBioStar.BSLogRecord = CType(Marshal.PtrToStructure(New IntPtr(logRecord.ToInt32() + i * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord))), GetType(clsBioStar.BSLogRecord)), clsBioStar.BSLogRecord)
                    iEnrollNumber = objDeviceUser.GetEmployeeUnkID(record.userID.ToString)
                    eventTime = New DateTime(1970, 1, 1)
                    dtDate = eventTime.AddSeconds(record.eventTime)

                    Dim strName As String = ""
                    Dim intShiftunkid As Integer = -1

                    If CInt(iEnrollNumber) > 0 Then

                        Dim objEmp As New clsEmployee_Master

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp._Employeeunkid = iEnrollNumber
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iEnrollNumber
                        'S.SANDEEP [04 JUN 2015] -- END


                        If objEmp._Employeecode.Trim = "" AndAlso objEmp._Firstname.Trim = "" Then
                            Continue For
                        End If

                        strName = objEmp._Displayname

                        Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                        intShiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(dtDate.Date, iEnrollNumber)

                        If objlogin.GetLoginType(iEnrollNumber) = 1 Then
                            Dim _Logoutdate As Date = dtDate.Date
                            Dim _LogoutTime As DateTime = dtDate
                            Call EmployeeLogout(iEnrollNumber, intShiftunkid, enInOutSource.FingerPrint, strName, _Logoutdate, _LogoutTime)
                        Else
                            Dim _Logindate As Date = dtDate.Date
                            Dim _LoginTime As DateTime = dtDate
                            Call EmployeeLogin(iEnrollNumber, intShiftunkid, enInOutSource.FingerPrint, strName, _Logindate, _LoginTime)
                        End If
                    End If


                Next

                Marshal.FreeHGlobal(logRecord)
                'Call FillList()
            End If

            'Timer1.Enabled = True

        Catch ex As IndexOutOfRangeException

        Catch ex As OutOfMemoryException

        Catch ex As AccessViolationException

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Timer1_Tick")
        Finally
            result = clsBioStar.BS_CloseSocket(m_Handle)
            Timer1.Enabled = True
        End Try
    End Sub
    'Sohail (02 Nov 2013) -- End

#End Region

    'Pinkal (10-Mar-2011) -- Start

#Region "NotifyIcon Event"

    Private Sub ntfLogin_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objntfLogin.MouseDoubleClick
        Try
            Me.Show()
            Me.WindowState = FormWindowState.Normal
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ntfLogin_MouseDoubleClick", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (10-Mar-2011) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.btnclose.GradientBackColor = GUI._ButttonBackColor
            Me.btnclose.GradientForeColor = GUI._ButttonFontColor

            Me.btnexit.GradientBackColor = GUI._ButttonBackColor
            Me.btnexit.GradientForeColor = GUI._ButttonFontColor

            Me.btnswipecard.GradientBackColor = GUI._ButttonBackColor
            Me.btnswipecard.GradientForeColor = GUI._ButttonFontColor

            Me.btnclockin.GradientBackColor = GUI._ButttonBackColor
            Me.btnclockin.GradientForeColor = GUI._ButttonFontColor

            Me.btnClockout.GradientBackColor = GUI._ButttonBackColor
            Me.btnClockout.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.GroupBox1.Text = Language._Object.getCaption(Me.GroupBox1.Name, Me.GroupBox1.Text)
            Me.btnclose.Text = Language._Object.getCaption(Me.btnclose.Name, Me.btnclose.Text)
            Me.btnexit.Text = Language._Object.getCaption(Me.btnexit.Name, Me.btnexit.Text)
            Me.lblpassword.Text = Language._Object.getCaption(Me.lblpassword.Name, Me.lblpassword.Text)
            Me.btnswipecard.Text = Language._Object.getCaption(Me.btnswipecard.Name, Me.btnswipecard.Text)
            Me.btnclockin.Text = Language._Object.getCaption(Me.btnclockin.Name, Me.btnclockin.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.btnClockout.Text = Language._Object.getCaption(Me.btnClockout.Name, Me.btnClockout.Text)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.dgcolEmployeeName.HeaderText = Language._Object.getCaption(Me.dgcolEmployeeName.Name, Me.dgcolEmployeeName.HeaderText)
            Me.dgcolEmpName.HeaderText = Language._Object.getCaption(Me.dgcolEmpName.Name, Me.dgcolEmpName.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Employee from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Password cannot be blank. Password is required information.")
            Language.setMessage(mstrModuleName, 3, "Sorry, password you have entered is incorrect. Please enter the correct password.")
            Language.setMessage(mstrModuleName, 4, "You are Successfully Logged in at :")
            Language.setMessage(mstrModuleName, 5, "You are Successfully Logged out at :")
            Language.setMessage(mstrModuleName, 6, "Unable to connect the device.")
            Language.setMessage(mstrModuleName, 7, "Unable to clear log cache.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

