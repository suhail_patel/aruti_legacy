﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public NotInheritable Class frmSplash

    Private ReadOnly mstrModuleName As String = "frmSplash"

    'TODO: This form can easily be set as the splash screen for the application by going to the "Application" tab
    '  of the Project Designer ("Properties" under the "Project" menu).


    Private Sub SplashScreen1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up the dialog text at runtime according to the application's assembly information.  

        'TODO: Customize the application's assembly information in the "Application" pane of the project 
        '  properties dialog (under the "Project" menu).

        'Application title
        If My.Application.Info.Title <> "" Then
            'If the application title is missing, use the application name, without the extension
            'ApplicationTitle.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If

        'Format the version information using the text set into the Version control at design time as the
        '  formatting string.  This allows for effective localization if desired.
        '  Build and revision information could be included by using the following code and changing the 
        '  Version control's designtime text to "Version {0}.{1:00}.{2}.{3}" or something similar.  See
        '  String.Format() in Help for more information.
        '
        '    Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)

        Version.Text = System.String.Format("Version {0}.{1:0}.{2:0}.{3:000}", My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)


        tmrSplash.Enabled = False
        'Copyright info
        Copyright.Text = My.Application.Info.Copyright
        Dim objmain As New clsMain
        Try

            If objmain.DO_Main(enArutiApplicatinType.Aruti_Configuration) Then
                tmrSplash.Enabled = True
            Else
                Me.Close()
                Exit Sub
            End If

            'Dim objMasterData As New clsMasterData
            'Call objMasterData.InsertDefaultData()

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SplashScreen1_Load", mstrModuleName)
        End Try
    End Sub

    'Sandeep | 17 JAN 2011 | -- START
    '    Private Sub tmrSplash_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrSplash.Tick
    '        Try
    '            tmrSplash.Enabled = False
    '            If ConfigParameter._Object.isParamFill Then
    '10:             Dim objMasterData As New clsMasterData
    '                Call objMasterData.InsertDefaultData(True)
    '                Dim frm As New frmLogin
    '                'If gRightToLeft = True Then
    '                '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                '    frm.RightToLeftLayout = True
    '                'End If
    '                Dim blnResult As Boolean = frm.displayDialog
    '                frm = Nothing
    '                'Language._Object._LangId = User._Object._LanguageId
    '                If blnResult Then
    '                    gfrmMDI = New frmMDI
    '                    'If User._Object._RightToLeft = True Then
    '                    '    gfrmMDI.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                    '    gfrmMDI.RightToLeftLayout = True
    '                    '    Call Language.ctlRightToLeftlayOut(gfrmMDI)
    '                    'End If
    '                    gfrmMDI.ShowDialog()
    '                    gfrmMDI = Nothing
    '                Else
    '                    Application.Exit()
    '                End If
    '            Else
    '                '<TODO>
    '                GoTo 10
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "tmrSplash_Tick", mstrModuleName)
    '        Finally
    '            Me.Close()
    '        End Try
    '    End Sub

    Private Sub tmrSplash_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrSplash.Tick
        Try
            tmrSplash.Enabled = False
            Dim objMasterData As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call objMasterData.InsertDefaultData(True)
            Call objMasterData.InsertDefaultData(True, 0)
            'S.SANDEEP [04 JUN 2015] -- END


            If objMasterData.IsGroupCreated = True Then


                'If gRightToLeft = True Then   '<TODO PUT ONe Setting Of Left To Right IN CONFIG. SETTINGS>
                '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                '    frm.RightToLeftLayout = True
                'End If


                'Pinkal (21-Jun-2012) -- Start
                'Enhancement : TRA Changes

                Dim frm As New frmLogin
                Dim blnResult As Boolean = False

                Dim objPassword As New clsPassowdOptions
                If objPassword._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                    Dim objUser As New clsUserAddEdit
                    'S.SANDEEP [24 SEP 2015] -- START
                    'Dim dsList As DataSet = objUser.GetList("List", True, True)
                    Dim dsList As DataSet = objUser.GetList("List")
                    'S.SANDEEP [24 SEP 2015] -- END


                    Dim drRow As DataRow() = dsList.Tables(0).Select("username='" & Environment.UserName.ToString() & "' AND userdomain ='" & Environment.UserDomainName.ToString() & "'")
                    If drRow.Length > 0 Then
                        blnResult = True
                        User._Object._Userunkid = CInt(drRow(0)("userunkid"))
                    Else
                        blnResult = frm.displayDialog
                    End If
                Else
                    blnResult = frm.displayDialog
                End If

                'Pinkal (21-Jun-2012) -- End





                frm = Nothing
                gobjLocalization._LangId = User._Object._Languageunkid
                If blnResult Then
                    gfrmMDI = New frmMDI
                    If User._Object._Isrighttoleft = True Then
                        gfrmMDI.RightToLeft = Windows.Forms.RightToLeft.Yes
                        gfrmMDI.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(gfrmMDI)
                    End If
                    gfrmMDI.ShowDialog()
                    gfrmMDI = Nothing
                Else
                    Application.Exit()
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "User Name is admin and Password is admin. You can change the Password later."), enMsgBoxStyle.Information) '?1
                User._Object._Userunkid = 1
                gfrmMDI = New frmMDI
                If User._Object._Isrighttoleft = True Then
                    gfrmMDI.RightToLeft = Windows.Forms.RightToLeft.Yes
                    gfrmMDI.RightToLeftLayout = True
                End If
                gfrmMDI.ShowDialog()
                gfrmMDI = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tmrSplash_Tick", mstrModuleName)
        Finally
            Me.Close()
        End Try
    End Sub
    'Sandeep | 17 JAN 2011 | -- END

End Class
