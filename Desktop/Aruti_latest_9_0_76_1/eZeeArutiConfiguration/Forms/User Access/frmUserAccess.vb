﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmUserAccess

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmUserAccess"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintUserUnkid As Integer = -1
    Private objUserAddEdit As clsUserAddEdit
    Private mdtTran As DataTable
    Private mSize As Size
    Private dgViewUser As DataView
    Private dtUserList As DataTable
    Private mintAllocationId As Integer = -1

    'S.SANDEEP [ 31 MAY 2012 ] -- START
    'ENHANCEMENT : TRA DISCIPLINE CHANGES
    Private mstrAllocations As String = String.Empty
    'S.SANDEEP [ 31 MAY 2012 ] -- END

    'S.SANDEEP [11-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 79
    Private dvUserAccess As DataView
    'S.SANDEEP [11-OCT-2017] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintUserUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintUserUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmUserAccess_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objUserAddEdit = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserAccess_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUserAccess_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objUserAddEdit = New clsUserAddEdit
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()


            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'gbAllocation.Collapse() : gbUserList.Collapse()
            'mSize = fpnlFlow.Size
            Call FillCombo()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            Call FillUserList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserAccess_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            'S.SANDEEP [06 OCT 2015] -- START
            dtUserList.AcceptChanges()
            'S.SANDEEP [06 OCT 2015] -- END

            Dim dTemp() As DataRow = dtUserList.Select("IsUCheck=true")

            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one user to assign Access."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtTemp As DataRow() = mdtTran.Select("IsAssign = True AND IsGrp = False")

            'S.SANDEEP [ 20 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If dtTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one access to assign."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 20 NOV 2012 ] -- END


            Dim intAlreadyAssigned As Integer : Dim blnFlag As Boolean : Dim blnIsDelFlag As Boolean

            'S.SANDEEP [ 26 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objCmp As New clsCompany_Master
            Dim dList As New DataSet
            dList = objCmp.GetFinancialYearList(CInt(cboCompany.SelectedValue), , "List")
            'S.SANDEEP [ 26 NOV 2012 ] -- END


            For j As Integer = 0 To dTemp.Length - 1
                'S.SANDEEP [ 26 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'For Each intKey As Integer In mstrAllocations.Split(CChar(","))
                '    blnIsDelFlag = False
                '    'S.SANDEEP [ 01 JUNE 2012 ] -- START
                '    'ENHANCEMENT : TRA DISCIPLINE CHANGES
                '    'blnIsDelFlag = objUserAddEdit.DeleteAccess(CInt(dTemp(j)("userunkid")), intKey)
                '    blnIsDelFlag = objUserAddEdit.DeleteAccess(CInt(dTemp(j)("userunkid")), intKey, CInt(dtTemp(j).Item("CompanyId")), CInt(dtTemp(j).Item("YearId")))
                '    'S.SANDEEP [ 01 JUNE 2012 ] -- END
                'Next
                If dList.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dList.Tables("List").Rows
                For Each intKey As Integer In mstrAllocations.Split(CChar(","))
                blnIsDelFlag = False
                            blnIsDelFlag = objUserAddEdit.DeleteAccess(CInt(dTemp(j)("userunkid")), intKey, CInt(dRow.Item("companyunkid")), CInt(dRow.Item("yearunkid")))
                        Next
                Next
                End If
                'S.SANDEEP [ 26 NOV 2012 ] -- END
                If blnIsDelFlag Then
                    For i As Integer = 0 To dtTemp.Length - 1
                        intAlreadyAssigned = -1 : blnFlag = False
                        intAlreadyAssigned = objUserAddEdit.IsAccessExist(CInt(dTemp(j)("userunkid")), _
                                                                          CInt(dtTemp(i).Item("CompanyId")), _
                                                                          CInt(dtTemp(i).Item("YearId")), _
                                                                          CInt(dtTemp(i).Item("AllocationId")), _
                                                                          CInt(dtTemp(i).Item("AllocationRefId")), _
                                                                          CInt(dtTemp(i).Item("AllocationLevel")))
                        If intAlreadyAssigned <= 0 Then
                            If CBool(dtTemp(i).Item("IsGrp")) = True Then Continue For
                            blnFlag = objUserAddEdit.InsertAccess(CInt(dTemp(j)("userunkid")), _
                                                                  CInt(dtTemp(i).Item("CompanyId")), _
                                                                  CInt(dtTemp(i).Item("YearId")), _
                                                                  CInt(dtTemp(i).Item("AllocationId")), _
                                                                  CInt(dtTemp(i).Item("AllocationLevel")), _
                                                                  CInt(dtTemp(i).Item("AllocationRefId")))
                            If blnFlag = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Error in Saving Access for User : ") & dTemp(j).Item("username").ToString, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    Next
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Error in Deleting Access for User : ") & dTemp(j).Item("username").ToString, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Next


            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Information saved successfully."), enMsgBoxStyle.Information)

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillUserList()
        Dim dsList As New DataSet
        Try

            'S.SANDEEP [24 SEP 2015] -- START
            'Dim objOption As New clsPassowdOptions
            'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
            'If drOption.Length > 0 Then
            '    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
            '        'S.SANDEEP [ 11 DEC 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        'dsList = objUserAddEdit.GetList("User", True, False)
            '        Dim objPswd As New clsPassowdOptions
            '        If objPswd._IsEmployeeAsUser Then
            '            dsList = objUserAddEdit.GetList("User", True, False, True)
            '        Else
                    'dsList = objUserAddEdit.GetList("User", True, False)
            '        End If
            '        objPswd = Nothing
            '        'S.SANDEEP [ 11 DEC 2012 ] -- END
            '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
            '        dsList = objUserAddEdit.GetList("User", True, True)
            '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
            '        dsList = objUserAddEdit.GetList("User", True, True)
            '    End If
            'End If
            'objOption = Nothing

            dsList = objUserAddEdit.GetList("User")
            'S.SANDEEP [24 SEP 2015] -- END


            If dsList.Tables("User").Rows.Count > 0 Then
                dsList.Tables("User").Columns.Add("IsUCheck", System.Type.GetType("System.Boolean"))

                For i As Integer = 0 To dsList.Tables("User").Rows.Count - 1
                    dsList.Tables("User").Rows(i)("IsUCheck") = False
                Next

                If mintUserUnkid > 0 Then
                    dtUserList = New DataView(dsList.Tables(0), "userunkid = '" & mintUserUnkid & "'", "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtUserList = New DataView(dsList.Tables(0), "userunkid NOT IN(1)", "", DataViewRowState.CurrentRows).ToTable
                End If

                dgViewUser = dtUserList.DefaultView

                dgvUser.AutoGenerateColumns = False

                objdgcolhUserCheck.DataPropertyName = "IsUCheck"
                dgcolhUserName.DataPropertyName = "username"
                objdgcolhUserId.DataPropertyName = "userunkid"

                dgvUser.DataSource = dgViewUser

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillUserList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            dgvUserAccess.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsAssign"
            objdgcolhCompId.DataPropertyName = "CompanyId"
            objdgcolhIsGroup.DataPropertyName = "IsGrp"
            objdgcolhJobId.DataPropertyName = "AllocationId"
            objdgcolhJobLevel.DataPropertyName = "AllocationLevel"
            objdgcolhYearId.DataPropertyName = "YearId"
            dgcolhUserAccess.DataPropertyName = "UserAccess"

            'S.SANDEEP [11-OCT-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 79
            'dgvUserAccess.DataSource = mdtTran
            dvUserAccess = mdtTran.DefaultView
            dgvUserAccess.DataSource = dvUserAccess
            'S.SANDEEP [11-OCT-2017] -- END

            Call SetCollapseValue()
            Call SetGridColor()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.SelectionForeColor = Color.Black
            dgvcsChild.SelectionBackColor = Color.White
            dgvcsChild.BackColor = Color.White


            For i As Integer = 0 To dgvUserAccess.RowCount - 1
                If CBool(dgvUserAccess.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    dgvUserAccess.Rows(i).DefaultCellStyle = dgvcsHeader
                Else
                    dgvUserAccess.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvUserAccess.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsCollapseChild As New DataGridViewCellStyle
            objdgvsCollapseChild.Font = New Font(dgvUserAccess.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseChild.ForeColor = Color.Black
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


            For i As Integer = 0 To dgvUserAccess.RowCount - 1
                If CBool(dgvUserAccess.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    If dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Value Is Nothing Then
                        dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Value = "-"
                    End If
                    dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Style = objdgvsCollapseHeader
                Else
                    dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Value = ""
                    dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Style = objdgvsCollapseChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub FillCombo()
        Dim objCompany As New clsCompany_Master
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCompany.GetList("Company", True)
            Dim dtRow As DataRow = dsCombos.Tables(0).NewRow
            dtRow.Item("companyunkid") = 0
            dtRow.Item("name") = Language.getMessage(mstrModuleName, 6, "Select")
            dsCombos.Tables(0).Rows.Add(dtRow)
            Dim dView As DataView = dsCombos.Tables(0).DefaultView
            dView.Sort = "companyunkid"
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dView.ToTable
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#End Region

#Region " Controls "

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub objbtnSearchCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCompany.Click
        Dim ObjFrm As New frmCommonSearch
        Try
            With ObjFrm
                .ValueMember = cboCompany.ValueMember
                .DisplayMember = cboCompany.DisplayMember
                .DataSource = CType(cboCompany.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If ObjFrm.DisplayDialog Then
                cboCompany.SelectedValue = ObjFrm.SelectedValue
                cboCompany.Focus()
        End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Private Sub gbAllocation_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbAllocation.HeaderClick
    '    If gbAllocation.Size.Height + gbUserList.Size.Height > mSize.Height Then
    '        gbAllocation.Size = New Size(280, gbAllocation.Height)
    '        gbUserList.Size = New Size(280, gbUserList.Height)
    '    Else
    '        gbAllocation.Size = New Size(297, gbAllocation.Height)
    '        gbUserList.Size = New Size(297, gbUserList.Height)
    '    End If
    'End Sub

    'Private Sub gbUserList_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbUserList.HeaderClick
    '    If gbAllocation.Size.Height + gbUserList.Size.Height > mSize.Height Then
    '        gbAllocation.Size = New Size(280, gbAllocation.Height)
    '        gbUserList.Size = New Size(280, gbUserList.Height)
    '    Else
    '        gbAllocation.Size = New Size(297, gbAllocation.Height)
    '        gbUserList.Size = New Size(297, gbUserList.Height)
    '    End If
    'End Sub
    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            If CInt(cboCompany.SelectedValue) > 0 Then
                Dim objConfig As New clsConfigOptions
                objConfig._Companyunkid = CInt(cboCompany.SelectedValue)
                mstrAllocations = objConfig._UserAccessModeSetting
                mdtTran = objUserAddEdit.GetUserAccessList(objConfig._UserAccessModeSetting, mintUserUnkid, CInt(cboCompany.SelectedValue))
                Call FillGrid()
                'S.SANDEEP [11-OCT-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 79
                txtSearchAccess.Enabled = True
                'S.SANDEEP [11-OCT-2017] -- END
                objConfig = Nothing
        Else
                dgvUser.DataSource = Nothing
                'S.SANDEEP [11-OCT-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 79
                txtSearchAccess.Enabled = False
                'S.SANDEEP [11-OCT-2017] -- END
        End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboCompany_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvUser.Rows.Count > 0 Then
                        If dgvUser.SelectedRows(0).Index = dgvUser.Rows(dgvUser.RowCount - 1).Index Then Exit Sub
                        dgvUser.Rows(dgvUser.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvUser.Rows.Count > 0 Then
                        If dgvUser.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvUser.Rows(dgvUser.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim strSearch As String = ""
        Try

            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhUserName.DataPropertyName & " LIKE '%" & txtSearch.Text & "%'"
            End If

            dgViewUser.RowFilter = ""
            If strSearch.Trim.Length > 0 Then
                If mintUserUnkid > 0 Then
                    dgViewUser.RowFilter = "userunkid = '" & mintUserUnkid & "' AND " & strSearch
                Else
                    dgViewUser.RowFilter = "userunkid NOT IN(1) AND " & strSearch
                End If
            Else
                If mintUserUnkid > 0 Then
                    dgViewUser.RowFilter = "userunkid = '" & mintUserUnkid & "'"
                Else
                    dgViewUser.RowFilter = "userunkid NOT IN(1) "
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler dgvUser.CellContentClick, AddressOf dgvUser_CellContentClick

            'S.SANDEEP [06 OCT 2015] -- START
            'If objchkAll.CheckState <> CheckState.Indeterminate Then
            'For Each dr As DataRow In dtUserList.Rows
            '    dr.Item("IsUCheck") = CBool(objchkAll.CheckState)
            'Next
            'dgViewUser = dtUserList.DefaultView
            'End If

            For Each dr As DataRowView In dgViewUser
                    dr.Item("IsUCheck") = CBool(objchkAll.CheckState)
                Next
            dgvUser.Refresh()
            'S.SANDEEP [06 OCT 2015] -- END


            AddHandler dgvUser.CellContentClick, AddressOf dgvUser_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvUser_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUser.CellContentClick, dgvUser.CellContentDoubleClick
        ', dgvUser.CellContentDoubleClick -- 'S.SANDEEP [24 SEP 2015] -- START -- END
        Try
            If e.RowIndex <= -1 Then Exit Sub

            'S.SANDEEP [24 SEP 2015] -- START
            If Me.dgvUser.IsCurrentCellDirty Then
                Me.dgvUser.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            'S.SANDEEP [24 SEP 2015] -- END

            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If e.ColumnIndex = objdgcolhUserCheck.Index Then

                'S.SANDEEP [24 SEP 2015] -- START
                'dtUserList.Rows(e.RowIndex)(objdgcolhUserCheck.DataPropertyName) = Not CBool(dtUserList.Rows(e.RowIndex)(objdgcolhUserCheck.DataPropertyName))
                'dgvUser.RefreshEdit()
                'Dim drRow As DataRow() = dtUserList.Select("IsUCheck = true", "")
                'If drRow.Length > 0 Then
                '    If dtUserList.Rows.Count = drRow.Length Then
                '        objchkAll.CheckState = CheckState.Checked
                '    Else
                '        objchkAll.CheckState = CheckState.Indeterminate
                '    End If
                'Else
                '    objchkAll.CheckState = CheckState.Unchecked
                'End If

                Dim drRow As DataRow() = dgViewUser.ToTable.Select("IsUCheck = true", "")
                If drRow.Length > 0 Then
                    If dtUserList.Rows.Count = drRow.Length Then
                        objchkAll.CheckState = CheckState.Checked
                    Else
                        objchkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAll.CheckState = CheckState.Unchecked
                End If

                'S.SANDEEP [24 SEP 2015] -- END
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvUser_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub


    'S.SANDEEP [ 31 MAY 2012 ] -- START
    'ENHANCEMENT : TRA DISCIPLINE CHANGES
    'Private Sub radJobs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radJobs.CheckedChanged, radDepartment.CheckedChanged
    '    Try
    '        Me.Cursor = Cursors.WaitCursor
    '        If CType(sender, RadioButton).Checked = True Then
    '            Select Case CType(sender, RadioButton).Name.ToUpper
    '                Case "RADJOBS"
    '                    mintAllocationId = enAllocation.JOBS
    '                Case "RADDEPARTMENT"
    '                    mintAllocationId = enAllocation.DEPARTMENT
    '            End Select
    '        End If

    '        'S.SANDEEP [ 16 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    '        'mdtTran = objUserAddEdit.GetUserAccessList(CType(mintAllocationId, enAllocation), mintUserUnkid)
    '        mdtTran = objUserAddEdit.GetUserAccessList(CType(mintAllocationId, enAllocation), mintUserUnkid, CInt(cboCompany.SelectedValue))
    '        'S.SANDEEP [ 16 MAY 2012 ] -- END


    '        Call FillGrid()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radJobs_CheckedChanged", mstrModuleName)
    '    Finally
    '        Me.Cursor = Cursors.Default
    '    End Try
    'End Sub
    'S.SANDEEP [ 31 MAY 2012 ] -- END



    Private Sub dgvUserAccess_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUserAccess.CellContentClick, dgvUserAccess.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvUserAccess.IsCurrentCellDirty Then
                Me.dgvUserAccess.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value.ToString = "-" Then
                            dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "+"
                        Else
                            dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvUserAccess.RowCount - 1
                            'S.SANDEEP [10 AUG 2015] -- START
                            'ENHANCEMENT : Aruti SaaS Changes
                            'If CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value) = CInt(dgvUserAccess.Rows(i).Cells(objdgcolhYearId.Index).Value) Then
                            If CInt(mdtTran.Rows(e.RowIndex).Item("AllocationRefId")) = CInt(mdtTran.Rows(i).Item("AllocationRefId")) Then
                                'S.SANDEEP [10 AUG 2015] -- END
                                If dgvUserAccess.Rows(i).Visible = False Then
                                    dgvUserAccess.Rows(i).Visible = True
                                Else
                                    dgvUserAccess.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case 1
                        For i = e.RowIndex + 1 To dgvUserAccess.RowCount - 1
                            Dim blnFlg As Boolean = CBool(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                            'S.SANDEEP [10 AUG 2015] -- START
                            'ENHANCEMENT : Aruti SaaS Changes
                            'If CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value) = CInt(dgvUserAccess.Rows(i).Cells(objdgcolhYearId.Index).Value) Then
                            '    dgvUserAccess.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlg
                            'End If

                            If CInt(mdtTran.Rows(e.RowIndex).Item("AllocationRefId")) = CInt(mdtTran.Rows(i).Item("AllocationRefId")) AndAlso _
                               CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value) = CInt(dgvUserAccess.Rows(i).Cells(objdgcolhYearId.Index).Value) Then
                                dgvUserAccess.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlg
                            End If
                            'S.SANDEEP [10 AUG 2015] -- END
                        Next
                End Select
            ElseIf CBool(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = False Then
                Dim blnFlg As Boolean = CBool(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                Dim dtTemp As DataRow() = mdtTran.Select("CompanyId = '" & dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCompId.Index).Value.ToString & "' AND IsGrp = 1 AND YearId = '" & dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value.ToString & "'")
                If blnFlg = False Then
                    If dtTemp.Length > 0 Then
                        Dim idx As Integer = mdtTran.Rows.IndexOf(dtTemp(0))
                        dgvUserAccess.Rows(idx).Cells(objdgcolhCheck.Index).Value = blnFlg
                    End If
                Else
                    Dim dtNewTran As DataTable = mdtTran.Copy
                    dtNewTran.Rows(e.RowIndex).Item("IsAssign") = blnFlg
                    dtNewTran.AcceptChanges()
                    Dim dtTemp1 As DataRow() = dtNewTran.Select("CompanyId = " & CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCompId.Index).Value) & " AND IsGrp = 0 AND IsAssign = 1 AND YearId = " & CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value))
                    If dtTemp1.Length > 0 And dtTemp.Length > 0 Then
                        If CInt(dtTemp(0)("TotAllocation")) = dtTemp1.Length Then
                            Dim idx As Integer = mdtTran.Rows.IndexOf(dtTemp(0))
                            dgvUserAccess.Rows(idx).Cells(objdgcolhCheck.Index).Value = blnFlg
                        End If
                    End If
                    dtNewTran.Dispose()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvUserAccess_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvUserAccess_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvUserAccess.DataBindingComplete
        Try
            Call SetGridColor()
            Call SetCollapseValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvUserAccess_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [11-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 79
    Private Sub txtSearchAccess_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchAccess.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvUserAccess.Rows.Count > 0 Then
                        If dgvUserAccess.SelectedRows(0).Index = dgvUserAccess.Rows(dgvUserAccess.RowCount - 1).Index Then Exit Sub
                        dgvUserAccess.Rows(dgvUserAccess.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvUserAccess.Rows.Count > 0 Then
                        If dgvUserAccess.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvUserAccess.Rows(dgvUserAccess.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchAccess_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchAccess_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchAccess.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSearchAccess.Text.Trim.Length > 0 Then
                strSearch = dgcolhUserAccess.DataPropertyName & " LIKE '%" & txtSearchAccess.Text & "%' OR " & objdgcolhIsGroup.DataPropertyName & " = true "
            End If
            dvUserAccess.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchAccess_TextChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [11-OCT-2017] -- END

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.objgbSearching.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.objgbSearching.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbAllocation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAllocation.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbUserList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUserList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.dgcolhUserAccess.HeaderText = Language._Object.getCaption(Me.dgcolhUserAccess.Name, Me.dgcolhUserAccess.HeaderText)
			Me.gbAllocation.Text = Language._Object.getCaption(Me.gbAllocation.Name, Me.gbAllocation.Text)
			Me.gbUserList.Text = Language._Object.getCaption(Me.gbUserList.Name, Me.gbUserList.Text)
			Me.radDepartment.Text = Language._Object.getCaption(Me.radDepartment.Name, Me.radDepartment.Text)
			Me.radJobs.Text = Language._Object.getCaption(Me.radJobs.Name, Me.radJobs.Text)
			Me.dgcolhUserName.HeaderText = Language._Object.getCaption(Me.dgcolhUserName.Name, Me.dgcolhUserName.HeaderText)
			Me.lblSelectCompany.Text = Language._Object.getCaption(Me.lblSelectCompany.Name, Me.lblSelectCompany.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please check atleast one user to assign Access.")
			Language.setMessage(mstrModuleName, 2, "Error in Deleting Access for User :")
			Language.setMessage(mstrModuleName, 3, "Error in Saving Access for User :")
			Language.setMessage(mstrModuleName, 4, "Information saved successfully.")
			Language.setMessage(mstrModuleName, 5, "Please check atleast one access to assign.")
			Language.setMessage(mstrModuleName, 6, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'Public Class frmUserAccess1

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmUserAccess"
'    Private mblnCancel As Boolean = True
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintUserUnkid As Integer = -1
'    Private objUserAddEdit As clsUserAddEdit
'    Private mdtTran As DataTable

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
'        Try
'            mintUserUnkid = intUnkId
'            menAction = eAction

'            Me.ShowDialog()

'            intUnkId = mintUserUnkid

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        Finally
'        End Try
'    End Function

'#End Region

'#Region " Form's Events "

'    Private Sub frmUserAccess_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        Try
'            objUserAddEdit = Nothing
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmUserAccess_FormClosed", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmUserAccess_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            objUserAddEdit = New clsUserAddEdit

'            Call Set_Logo(Me, gApplicationType)
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'Anjan (02 Sep 2011)-End 

'            Call FillUserList()

'            Call FillGrid()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmUserAccess_Load", mstrModuleName)
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-Start
'    'Issue : Including Language Settings.
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-End 

'#End Region

'#Region " Button's Events "

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Try
'            If lvUserAccess.CheckedItems.Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one user to assign Access."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim dtTemp As DataRow() = mdtTran.Select("IsAssign = True")

'            'If dtTemp.Length <= 0 Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check atleast one Access to assign User."), enMsgBoxStyle.Information)
'            '    Exit Sub
'            'End If

'            Dim intAlreadyAssigned As Integer
'            Dim blnFlag As Boolean

'            For Each lvItem As ListViewItem In lvUserAccess.CheckedItems

'                If objUserAddEdit.DeleteAccess(CInt(lvItem.Tag)) = True Then
'                    For i As Integer = 0 To dtTemp.Length - 1
'                        intAlreadyAssigned = -1
'                        blnFlag = False
'                        intAlreadyAssigned = objUserAddEdit.IsAccessExist(CInt(lvItem.Tag), _
'                                                                          CInt(dtTemp(i).Item("CompanyId")), _
'                                                                          CInt(dtTemp(i).Item("YearId")), _
'                                                                          CInt(dtTemp(i).Item("JobId")), _
'                                                                          CInt(dtTemp(i).Item("JobLevel")))
'                        If intAlreadyAssigned <= 0 Then

'                            If CBool(dtTemp(i).Item("IsGrp")) = True Then Continue For

'                            blnFlag = objUserAddEdit.InsertAccess(CInt(lvItem.Tag), _
'                                                                  CInt(dtTemp(i).Item("CompanyId")), _
'                                                                  CInt(dtTemp(i).Item("YearId")), _
'                                                                  CInt(dtTemp(i).Item("JobId")), _
'                                                                  CInt(dtTemp(i).Item("JobLevel")))
'                            If blnFlag = False Then
'                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Error in Saving Access for User : ") & lvItem.SubItems(colhUser.Index).Text.ToString, enMsgBoxStyle.Information)
'                                Exit Sub
'                            End If
'                        End If
'                    Next
'                Else
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Error in Deleting Access for User : ") & lvItem.SubItems(colhUser.Index).Text.ToString, enMsgBoxStyle.Information)
'                    Exit Sub
'                End If
'            Next

'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Information saved successfully."), enMsgBoxStyle.Information)

'            Me.Close()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Private Methods "

'    Private Sub FillUserList()
'        Dim dsList As New DataSet
'        Dim dtFilter As DataTable = Nothing
'        Dim lvItem As ListViewItem
'        Try
'            dsList = objUserAddEdit.GetList("User", True)
'            lvUserAccess.Items.Clear()
'            If dsList.Tables("User").Rows.Count <= 0 Then Exit Sub

'            If mintUserUnkid > 0 Then
'                dtFilter = New DataView(dsList.Tables("User"), "userunkid = '" & mintUserUnkid & "'", "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtFilter = New DataView(dsList.Tables("User"), "userunkid NOT IN(1)", "", DataViewRowState.CurrentRows).ToTable
'            End If

'            For Each dtRow As DataRow In dtFilter.Rows
'                lvItem = New ListViewItem

'                lvItem.Text = ""
'                lvItem.SubItems.Add(dtRow.Item("username").ToString)
'                lvItem.Tag = dtRow.Item("userunkid")

'                If CInt(lvItem.Tag) = mintUserUnkid Then
'                    lvItem.Checked = True
'                End If

'                lvUserAccess.Items.Add(lvItem)
'            Next


'            If lvUserAccess.Items.Count > 17 Then
'                colhUser.Width = colhUser.Width - 15
'            Else
'                colhUser.Width = colhUser.Width
'            End If

'            lvUserAccess.GridLines = False

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillUserList", mstrModuleName)
'        Finally
'            dsList.Dispose()
'            dtFilter.Dispose()
'            lvItem = Nothing
'        End Try
'    End Sub

'    Private Sub SetGridColor()
'        Try
'            Dim dgvcsHeader As New DataGridViewCellStyle
'            dgvcsHeader.ForeColor = Color.White
'            dgvcsHeader.SelectionBackColor = Color.Gray
'            dgvcsHeader.SelectionForeColor = Color.White
'            dgvcsHeader.BackColor = Color.Gray

'            Dim dgvcsChild As New DataGridViewCellStyle
'            dgvcsChild.ForeColor = Color.Black
'            dgvcsChild.SelectionForeColor = Color.Black
'            dgvcsChild.SelectionBackColor = Color.White
'            dgvcsChild.BackColor = Color.White


'            For i As Integer = 0 To dgvUserAccess.RowCount - 1
'                If CBool(dgvUserAccess.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
'                    dgvUserAccess.Rows(i).DefaultCellStyle = dgvcsHeader
'                Else
'                    dgvUserAccess.Rows(i).DefaultCellStyle = dgvcsChild
'                End If
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub SetCollapseValue()
'        Try
'            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
'            objdgvsCollapseHeader.Font = New Font(dgvUserAccess.Font.FontFamily, 12, FontStyle.Bold)
'            objdgvsCollapseHeader.ForeColor = Color.White
'            objdgvsCollapseHeader.BackColor = Color.Gray
'            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
'            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

'            Dim objdgvsCollapseChild As New DataGridViewCellStyle
'            objdgvsCollapseChild.Font = New Font(dgvUserAccess.Font.FontFamily, 12, FontStyle.Bold)
'            objdgvsCollapseChild.ForeColor = Color.Black
'            objdgvsCollapseChild.SelectionBackColor = Color.White
'            objdgvsCollapseChild.SelectionBackColor = Color.White
'            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


'            For i As Integer = 0 To dgvUserAccess.RowCount - 1
'                If CBool(dgvUserAccess.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
'                    If dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Value Is Nothing Then
'                        dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Value = "-"
'                    End If
'                    dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Style = objdgvsCollapseHeader
'                Else
'                    dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Value = ""
'                    dgvUserAccess.Rows(i).Cells(objdgcolhCollaps.Index).Style = objdgvsCollapseChild
'                End If
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub FillGrid()
'        Try
'            mdtTran = objUserAddEdit.GetUserAccessList(mintUserUnkid)

'            dgvUserAccess.AutoGenerateColumns = False

'            objdgcolhCheck.DataPropertyName = "IsAssign"
'            objdgcolhCompId.DataPropertyName = "CompanyId"
'            objdgcolhIsGroup.DataPropertyName = "IsGrp"
'            objdgcolhJobId.DataPropertyName = "JobId"
'            objdgcolhJobLevel.DataPropertyName = "JobLevel"
'            objdgcolhYearId.DataPropertyName = "YearId"
'            dgcolhUserAccess.DataPropertyName = "UserAccess"

'            dgvUserAccess.DataSource = mdtTran

'            Call SetCollapseValue()
'            Call SetGridColor()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Controls "

'    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
'        Try
'            RemoveHandler lvUserAccess.ItemChecked, AddressOf lvUserAccess_ItemChecked
'            For Each lvItem As ListViewItem In lvUserAccess.Items
'                lvItem.Checked = CBool(objchkAll.Checked)
'            Next
'            AddHandler lvUserAccess.ItemChecked, AddressOf lvUserAccess_ItemChecked
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvUserAccess_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
'        Try
'            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
'            If lvUserAccess.CheckedItems.Count <= 0 Then
'                objchkAll.CheckState = CheckState.Unchecked
'            ElseIf lvUserAccess.CheckedItems.Count < lvUserAccess.Items.Count Then
'                objchkAll.CheckState = CheckState.Indeterminate
'            ElseIf lvUserAccess.CheckedItems.Count = lvUserAccess.Items.Count Then
'                objchkAll.CheckState = CheckState.Checked
'            End If
'            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvUserAccess_ItemChecked", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub dgvUserAccess_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUserAccess.CellContentClick, dgvUserAccess.CellContentDoubleClick
'        Try
'            'RemoveHandler dgvUserAccess.DataBindingComplete, AddressOf dgvUserAccess_DataBindingComplete
'            Dim i As Integer
'            If e.RowIndex = -1 Then Exit Sub

'            If Me.dgvUserAccess.IsCurrentCellDirty Then
'                Me.dgvUserAccess.CommitEdit(DataGridViewDataErrorContexts.Commit)
'            End If

'            If CBool(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
'                Select Case CInt(e.ColumnIndex)
'                    Case 0
'                        If dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value.ToString = "-" Then
'                            dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "+"
'                        Else
'                            dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "-"
'                        End If

'                        For i = e.RowIndex + 1 To dgvUserAccess.RowCount - 1
'                            If CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value) = CInt(dgvUserAccess.Rows(i).Cells(objdgcolhYearId.Index).Value) Then
'                                If dgvUserAccess.Rows(i).Visible = False Then
'                                    dgvUserAccess.Rows(i).Visible = True
'                                Else
'                                    dgvUserAccess.Rows(i).Visible = False
'                                End If
'                            Else
'                                Exit For
'                            End If
'                        Next
'                    Case 1
'                        For i = e.RowIndex + 1 To dgvUserAccess.RowCount - 1
'                            Dim blnFlg As Boolean = CBool(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
'                            If CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value) = CInt(dgvUserAccess.Rows(i).Cells(objdgcolhYearId.Index).Value) Then
'                                dgvUserAccess.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlg
'                            End If
'                        Next
'                End Select
'            ElseIf CBool(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = False Then
'                Dim blnFlg As Boolean = CBool(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
'                Dim dtTemp As DataRow() = mdtTran.Select("CompanyId = '" & dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCompId.Index).Value.ToString & "' AND IsGrp = 1 AND YearId = '" & dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value.ToString & "'")
'                If blnFlg = False Then
'                    If dtTemp.Length > 0 Then
'                        Dim idx As Integer = mdtTran.Rows.IndexOf(dtTemp(0))
'                        dgvUserAccess.Rows(idx).Cells(objdgcolhCheck.Index).Value = blnFlg
'                    End If
'                Else
'                    Dim dtNewTran As DataTable = mdtTran.Copy
'                    dtNewTran.Rows(e.RowIndex).Item("IsAssign") = blnFlg
'                    dtNewTran.AcceptChanges()
'                    Dim dtTemp1 As DataRow() = dtNewTran.Select("CompanyId = " & CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhCompId.Index).Value) & " AND IsGrp = 0 AND IsAssign = 1 AND YearId = " & CInt(dgvUserAccess.Rows(e.RowIndex).Cells(objdgcolhYearId.Index).Value))
'                    If dtTemp1.Length > 0 And dtTemp.Length > 0 Then
'                        If CInt(dtTemp(0)("TotJob")) = dtTemp1.Length Then
'                            Dim idx As Integer = mdtTran.Rows.IndexOf(dtTemp(0))
'                            dgvUserAccess.Rows(idx).Cells(objdgcolhCheck.Index).Value = blnFlg
'                        End If
'                    End If
'                    dtNewTran.Dispose()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvUserAccess_CellContentClick", mstrModuleName)
'        Finally
'            'AddHandler dgvUserAccess.DataBindingComplete, AddressOf dgvUserAccess_DataBindingComplete
'        End Try
'    End Sub

'    Private Sub dgvUserAccess_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvUserAccess.DataBindingComplete
'        Try
'            Call SetGridColor()
'            Call SetCollapseValue()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvUserAccess_DataBindingComplete", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'End Class