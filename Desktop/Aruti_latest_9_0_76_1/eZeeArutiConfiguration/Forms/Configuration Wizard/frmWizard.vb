﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmWizard

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmWizard"
    Private blnIsWizardFinished As Boolean = False
    Private blnIsFormLoad As Boolean = False

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return blnIsWizardFinished
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : displayDialog ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()

            objlblGenInfo.Text = Language.getMessage(mstrModuleName, 1, "Congratulations, you have successfully installed Aruti. We thank you for considering Aruti for your Human Resource Management requirements.") & vbCrLf & vbCrLf & "        " & Language.getMessage(mstrModuleName, 2, " Aruti requires that you go through this step by step wizard to configure your Product. With this wizard you can enter the Group Information as this information is required before you can do the normal activities like Company creation and more.") & vbCrLf & vbCrLf & "        " & Language.getMessage(mstrModuleName, 3, "After completion of this wizard, you will be presented with Company Creation form. Please add a Company before trying out various features of this product.") & vbCrLf & vbCrLf & "        " & Language.getMessage(mstrModuleName, 4, "To continue please click on Next button, to cancel the wizard please click on Cancel button.")
            objeZeeHeader.Title = Language.getMessage(mstrModuleName, 5, "Welcome to") & " " & "Aruti Configuration."
            objeZeeHeader.Message = ""
            dtpStartDate.Value = Today.Date
            dtpStartDate.MaxDate = Today.Date
            dtpStartDate.Enabled = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmWizard_Load ; Module Name : " & mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCMaster As New clsMasterData
        Try
            dsList = objCMaster.getCountryList("List", True)
            With cboPropCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsList.Tables("List")
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillCombo ; Module Name : " & mstrModuleName)
        Finally
            dsList.Dispose()
            objCMaster = Nothing
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtAddress1.BackColor = GUI.ColorOptional
            txtAddress2.BackColor = GUI.ColorOptional
            txtCity.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional
            txtFax.BackColor = GUI.ColorOptional
            txtGroupName.BackColor = GUI.ColorComp
            txtId.BackColor = GUI.ColorOptional
            txtPhone.BackColor = GUI.ColorOptional
            txtState.BackColor = GUI.ColorOptional
            txtWebsite.BackColor = GUI.ColorOptional
            txtZipcode.BackColor = GUI.ColorOptional
            cboPropCountry.BackColor = GUI.ColorOptional
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetColor ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Wizard Configuration "

    Private Sub eZeeConfigWiz_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeConfigWiz.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeConfigWiz.Pages.IndexOf(ewpGroupInfo)
                    If txtGroupName.Text.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Group Name cannot be blank. Group Name is mandatory information."), enMsgBoxStyle.Information)
                        txtGroupName.Focus()
                        blnIsWizardFinished = False
                        e.Cancel = True
                    Else
                        Dim objGroup As New clsGroup_Master(True)

                        objGroup._Address1 = txtAddress1.Text
                        objGroup._Address2 = txtAddress2.Text
                        objGroup._City = txtCity.Text
                        objGroup._Country = cboPropCountry.Text
                        objGroup._Email = txtEmail.Text
                        objGroup._Fax = txtFax.Text
                        objGroup._Freezdate = dtpStartDate.Value.AddDays(-1)
                        objGroup._Groupname = txtGroupName.Text
                        objGroup._Phone = txtPhone.Text
                        objGroup._Startdate = dtpStartDate.Value
                        objGroup._State = txtState.Text
                        objGroup._Website = txtWebsite.Text
                        objGroup._Zipcode = txtZipcode.Text

                        If objGroup.Insert Then
                            blnIsWizardFinished = True
                            eZeeConfigWiz.CancelText = "Finish"
                            objlblFinishCaption.Text = Language.getMessage(mstrModuleName, 7, "Congratulations, you have successfully configured Aruti.")
                        Else
                            blnIsWizardFinished = False
                        End If
                    End If
                Case eZeeConfigWiz.Pages.IndexOf(ewpFinish)
                    Me.Close()
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : eZeeConfigWiz_BeforeSwitchPages ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbGeneralInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGeneralInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbGroupInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGroupInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.objeZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.objeZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.objeZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.objeZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.objeZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeConfigWiz.CancelText = Language._Object.getCaption(Me.eZeeConfigWiz.Name & "_CancelText" , Me.eZeeConfigWiz.CancelText)
			Me.eZeeConfigWiz.NextText = Language._Object.getCaption(Me.eZeeConfigWiz.Name & "_NextText" , Me.eZeeConfigWiz.NextText)
			Me.eZeeConfigWiz.BackText = Language._Object.getCaption(Me.eZeeConfigWiz.Name & "_BackText" , Me.eZeeConfigWiz.BackText)
			Me.eZeeConfigWiz.FinishText = Language._Object.getCaption(Me.eZeeConfigWiz.Name & "_FinishText" , Me.eZeeConfigWiz.FinishText)
			Me.gbGeneralInfo.Text = Language._Object.getCaption(Me.gbGeneralInfo.Name, Me.gbGeneralInfo.Text)
			Me.lblStarrtDate.Text = Language._Object.getCaption(Me.lblStarrtDate.Name, Me.lblStarrtDate.Text)
			Me.gbGroupInfo.Text = Language._Object.getCaption(Me.gbGroupInfo.Name, Me.gbGroupInfo.Text)
			Me.lblWebsite.Text = Language._Object.getCaption(Me.lblWebsite.Name, Me.lblWebsite.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblAddress2.Text = Language._Object.getCaption(Me.lblAddress2.Name, Me.lblAddress2.Text)
			Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
			Me.lblPhone.Text = Language._Object.getCaption(Me.lblPhone.Name, Me.lblPhone.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblZipCode.Text = Language._Object.getCaption(Me.lblZipCode.Name, Me.lblZipCode.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Congratulations, you have successfully installed Aruti. We thank you for considering Aruti for your Human Resource Management requirements.")
			Language.setMessage(mstrModuleName, 2, " Aruti requires that you go through this step by step wizard to configure your Product. With this wizard you can enter the Group Information as this information is required before you can do the normal activities like Company creation and more.")
			Language.setMessage(mstrModuleName, 3, "After completion of this wizard, you will be presented with Company Creation form. Please add a Company before trying out various features of this product.")
			Language.setMessage(mstrModuleName, 4, "To continue please click on Next button, to cancel the wizard please click on Cancel button.")
			Language.setMessage(mstrModuleName, 5, "Welcome to")
			Language.setMessage(mstrModuleName, 6, "Group Name cannot be blank. Group Name is mandatory information.")
			Language.setMessage(mstrModuleName, 7, "Congratulations, you have successfully configured Aruti.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class