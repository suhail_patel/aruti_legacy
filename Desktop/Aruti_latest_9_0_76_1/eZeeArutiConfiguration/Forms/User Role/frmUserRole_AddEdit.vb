﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmUserRole_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmUserRole_AddEdit"
    Private mblnCancel As Boolean = True
    Private objRoleMaster As clsUserRole_Master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintUserRoleUnkid As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintUserRoleUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintUserRoleUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtRole.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objRoleMaster._Code
        txtRole.Text = objRoleMaster._Name
        txtDescription.Text = objRoleMaster._Description
    End Sub

    Private Sub SetValue()
        objRoleMaster._Code = txtCode.Text
        objRoleMaster._Name = txtRole.Text
        objRoleMaster._Description = txtDescription.Text
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmUserRole_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objRoleMaster = Nothing
    End Sub

    Private Sub frmUserRole_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmUserRole_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmUserRole_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objRoleMaster = New clsUserRole_Master
        'Call Language.setLanguage(Me.Name)
        'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Call OtherSettings()
            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objRoleMaster._Roleunkid = mintUserRoleUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserRole_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUserRole_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsUserRole_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtRole.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "User Role cannot be blank. User Role is required information."), enMsgBoxStyle.Information) '?1
                txtRole.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objRoleMaster.Update()
            Else
                blnFlag = objRoleMaster.Insert()
            End If

            If blnFlag = False And objRoleMaster._Message <> "" Then
                eZeeMsgBox.Show(objRoleMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objRoleMaster = Nothing
                    objRoleMaster = New clsUserRole_Master
                    Call GetValue()
                    txtCode.Focus()
                Else
                    mintUserRoleUnkid = objRoleMaster._Roleunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'If User._Object._RightToLeft = True Then
            '    objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objFrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objFrm)
            'End If
            Call objFrm.displayDialog(txtRole.Text, objRoleMaster._Name1, objRoleMaster._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbUserRole.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUserRole.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbUserRole.Text = Language._Object.getCaption(Me.gbUserRole.Name, Me.gbUserRole.Text)
			Me.lblRole.Text = Language._Object.getCaption(Me.lblRole.Name, Me.lblRole.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "User Role cannot be blank. User Role is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class