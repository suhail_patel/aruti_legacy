﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssignReportPrivilege

#Region " Private Vaiables "

    Private mstrModuleName As String = ""
    Private mstrUserName As String = String.Empty
    Private mintUserId As Integer = -1
    Private mdtTran As DataTable
    Private objReport As clsArutiReportClass

#End Region

#Region " Properties "

    Public WriteOnly Property _User_Name() As String
        Set(ByVal value As String)
            mstrUserName = value
        End Set
    End Property

    Public WriteOnly Property _UserId() As Integer
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim ObjComp As New clsCompany_Master
        Try
            dsList = ObjComp.GetList("Company", True)
            'RemoveHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Company")
                .SelectedIndex = 0
            End With
            'AddHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjComp = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub FillReportList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'dsList = objReport.getList("", False, False)
            'dsList = objReport.getList("hrmsConfiguration", User._Object._Userunkid, Company._Object._Companyunkid, False, "", False)
            dsList = objReport.getList("hrmsConfiguration", User._Object._Userunkid, Company._Object._Companyunkid, False, "", False, User._Object._Languageunkid)
            'S.SANDEEP [10 AUG 2015] -- END


            lvReports.Items.Clear()
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("DisplayName"))
                lvItem.SubItems.Add(dtRow.Item("CategoryName"))
                lvItem.SubItems.Add(dtRow.Item("reportcategoryunkid"))
                lvItem.Tag = dtRow.Item("reportunkid")
                lvReports.Items.Add(lvItem)

                lvItem = Nothing
            Next
            lvReports.GroupingColumn = objcolhCategory
            lvReports.DisplayGroups(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillReportList", mstrModuleName)
        Finally
            dsList.Dispose()
            lvItem = Nothing
        End Try
    End Sub

    Private Sub CreateDataTable()
        mdtTran = New DataTable("ReportPrivilege")
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("ReportCatId")
            dCol.DataType = Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ReportName")
            dCol.DataType = Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ReportCateName")
            dCol.DataType = Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ReportId")
            dCol.DataType = Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("sort_key")
            dCol.DataType = Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("Assign")
            dCol.DataType = Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isfavorite")
            dCol.DataType = Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("CompanyId")
            dCol.DataType = Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("UserId")
            dCol.DataType = Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("CompanyName")
            dCol.DataType = Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
            dCol = Nothing
        End Try
    End Sub

    Private Sub FillAssignedReportList()
        Dim lvItem As ListViewItem
        Try
            lvAssignedReports.Items.Clear()
            For Each dtRow As DataRow In mdtTran.Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("ReportName").ToString)
                lvItem.SubItems.Add(dtRow.Item("CompanyName").ToString)
                lvItem.SubItems.Add(dtRow.Item("CompanyId").ToString)
                lvItem.Tag = dtRow.Item("ReportId")
                If CBool(dtRow.Item("Assign")) = True Then lvItem.Checked = True

                lvAssignedReports.Items.Add(lvItem)
            Next
            lvAssignedReports.GridLines = False
            lvAssignedReports.GroupingColumn = objcolhCompanyName
            lvAssignedReports.DisplayGroups(True)
            Call ResetReportList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAssignedReportList", mstrModuleName)
        Finally
            lvItem = Nothing
        End Try
    End Sub

    Private Sub ResetReportList()
        Try
            For i As Integer = 0 To lvReports.CheckedItems.Count - 1
                lvReports.CheckedItems(0).Checked = False
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetReportList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetOperation(ByVal blnOperation As Boolean)
        Try
            For Each Item As ListViewItem In lvReports.Items
                Item.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetOperation", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAssignReportPrivilege_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objReport = New clsArutiReportClass
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call FillCombo()
            Call CreateDataTable()
            Call FillReportList()
            lvAssignedReports.GridLines = False
            txtUser.Text = mstrUserName
            mdtTran = objReport.getReportList(mintUserId, CInt(cboCompany.SelectedValue)).Tables(0)
            If objReport.getReportList(mintUserId, CInt(cboCompany.SelectedValue)).Tables(0).Rows.Count <= 0 Then
                'RemoveHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
            Else
                AddHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
            End If
            Call FillAssignedReportList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignReportPrivilege_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignReportPrivilege_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignReportPrivilege_FormClosed", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " Buttons Events "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnMoveRight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnMoveRight.Click
        Try
            If lvReports.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Check atleast one report to assign privilege."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim dtRow As DataRow
            Dim drRow() As DataRow
            For Each Item As ListViewItem In lvReports.CheckedItems
                drRow = mdtTran.Select("ReportId = '" & Item.Tag & "' AND CompanyId ='" & CInt(cboCompany.SelectedValue) & "'")
                If drRow.Length > 0 Then Continue For
                dtRow = mdtTran.NewRow
                dtRow.Item("UserId") = mintUserId
                dtRow.Item("CompanyId") = cboCompany.SelectedValue
                dtRow.Item("ReportId") = CInt(Item.Tag)
                dtRow.Item("ReportCatId") = CInt(Item.SubItems(objcolhCatId.Index).Text)
                dtRow.Item("ReportName") = Item.SubItems(colhReports.Index).Text
                dtRow.Item("ReportCateName") = Item.SubItems(objcolhCategory.Index).Text
                dtRow.Item("Assign") = False
                dtRow.Item("CompanyName") = cboCompany.Text
                mdtTran.Rows.Add(dtRow)
            Next
            Call FillAssignedReportList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnMoveRight_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnMoveLeft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnMoveLeft.Click
        Try
            If lvAssignedReports.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check atleast one report to usassign from user."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If lvAssignedReports.CheckedItems.Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to remove the selected privilege(s). from the selected user?"), enMsgBoxStyle.YesNo + enMsgBoxStyle.Information) = Windows.Forms.DialogResult.Yes Then
                    Dim dtRow() As DataRow
                    If lvAssignedReports.CheckedItems.Count > 1 Then
                        For Each lvItem As ListViewItem In lvAssignedReports.CheckedItems
                            objReport.RemovePrivilege(mintUserId, lvItem.Tag, lvItem.SubItems(objcolhCompanyId.Index).Text)
                            dtRow = mdtTran.Select("UserId='" & mintUserId & "' AND ReportId = '" & lvItem.Tag & "' AND CompanyId = '" & lvItem.SubItems(objcolhCompanyId.Index).Text & "'")
                            If dtRow.Length > 0 Then
                                mdtTran.Rows.Remove(dtRow(0))
                            End If
                            lvItem.Remove()
                        Next
                    Else
                        objReport.RemovePrivilege(mintUserId, lvAssignedReports.CheckedItems(0).Tag, lvAssignedReports.CheckedItems(0).SubItems(objcolhCompanyId.Index).Text)
                        dtRow = mdtTran.Select("UserId='" & mintUserId & "' AND ReportId = '" & lvAssignedReports.CheckedItems(0).Tag & "' AND CompanyId = '" & lvAssignedReports.CheckedItems(0).SubItems(objcolhCompanyId.Index).Text & "'")
                        If dtRow.Length > 0 Then
                            mdtTran.Rows.Remove(dtRow(0))
                        End If
                        lvAssignedReports.CheckedItems(0).Remove()
                    End If
                Else
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnMoveLeft_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            blnFlag = objReport.InsertReportPrivilege(mdtTran)
            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Privilege was assigned successfully to selected user."), enMsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objChkAllReports_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAllReports.CheckedChanged
        Try
            Call SetOperation(CBool(objChkAllReports.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAllReports_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvReports_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvReports.ItemChecked
        Try
            If lvReports.CheckedItems.Count <= 0 Then
                RemoveHandler objChkAllReports.CheckedChanged, AddressOf objChkAllReports_CheckedChanged
                objChkAllReports.CheckState = CheckState.Unchecked
                AddHandler objChkAllReports.CheckedChanged, AddressOf objChkAllReports_CheckedChanged
            ElseIf lvReports.CheckedItems.Count < lvReports.Items.Count Then
                RemoveHandler objChkAllReports.CheckedChanged, AddressOf objChkAllReports_CheckedChanged
                objChkAllReports.CheckState = CheckState.Indeterminate
                AddHandler objChkAllReports.CheckedChanged, AddressOf objChkAllReports_CheckedChanged
            ElseIf lvReports.CheckedItems.Count = lvReports.Items.Count Then
                RemoveHandler objChkAllReports.CheckedChanged, AddressOf objChkAllReports_CheckedChanged
                objChkAllReports.CheckState = CheckState.Checked
                AddHandler objChkAllReports.CheckedChanged, AddressOf objChkAllReports_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvReports_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Try

            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes
            Company._Object._Companyunkid = CInt(cboCompany.SelectedValue)
            FillReportList()
            'Pinkal (27-Jun-2013) -- End

            mdtTran = objReport.getReportList(mintUserId, CInt(cboCompany.SelectedValue)).Tables(0)
            Call FillAssignedReportList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompany_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbReportPrivilege.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbReportPrivilege.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnMoveRight.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnMoveRight.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnMoveLeft.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnMoveLeft.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.gbReportPrivilege.Text = Language._Object.getCaption(Me.gbReportPrivilege.Name, Me.gbReportPrivilege.Text)
			Me.colhReports.Text = Language._Object.getCaption(CStr(Me.colhReports.Tag), Me.colhReports.Text)
			Me.colhAssignedReports.Text = Language._Object.getCaption(CStr(Me.colhAssignedReports.Tag), Me.colhAssignedReports.Text)
			Me.lblSelectCompany.Text = Language._Object.getCaption(Me.lblSelectCompany.Name, Me.lblSelectCompany.Text)
			Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Check atleast one report to assign privilege.")
			Language.setMessage(mstrModuleName, 2, "Please check atleast one report to usassign from user.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to remove the selected privilege(s). from the selected user?")
			Language.setMessage(mstrModuleName, 4, "Privilege was assigned successfully to selected user.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class