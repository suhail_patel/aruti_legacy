﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports System.IO.Ports
Imports Microsoft.Win32
Imports System.Text.RegularExpressions
Imports System.Net
Imports System
Imports System.Runtime.InteropServices

#End Region


'Last Message index = 12

Public Class frmDeviceSettings

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDeviceSettings"
    Dim dtSetting As DataSet = Nothing
    Dim mintDeviceID As Integer = -1
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim objAppSetting As clsApplicationSettings
    Dim h As IntPtr = IntPtr.Zero

    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
    Dim mintLogsInterval As Integer = 0
    Dim mstrDaysWeek As String = ""
    Dim mstrUserTime As String = ""
    'Pinkal (06-May-2016) -- End


    'Pinkal (04-Oct-2017) -- Start
    'Enhancement - Working SAGEM Database Device Integration.
    Dim mstrDatabaseServer As String = ""
    Dim mstrDatabaseName As String = ""
    Dim mstrDatabaseUserName As String = ""
    Dim mstrDatabasePassword As String = ""
    'Pinkal (04-Oct-2017) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintDeviceID = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintDeviceID
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form Events"

    Private Sub frmDeviceSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)


            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End


            objAppSetting = New clsApplicationSettings
            FillCombo()
            FillDatatable()
            If IO.File.Exists(objAppSetting._ApplicationPath & "Devicesetting.xml") Then
                dtSetting.ReadXml(objAppSetting._ApplicationPath & "Devicesetting.xml")
            End If
            If menAction = enAction.EDIT_ONE Then
                Getvalue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDeviceSettings_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDeviceSettings_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDeviceSettings_KeyDown", mstrModuleName)
        End Try
    End Sub


    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Pinkal (10-Jul-2014) -- End

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objMasterData As New clsMasterData
        Dim dtList As DataTable = Nothing
        Try

            '======================== START FOR COMMUNICATION TYPE=================

            dtList = objMasterData.GetCommunictionType().Tables(0)
            cboCommunicationType.ValueMember = "Id"
            cboCommunicationType.DisplayMember = "CommType"
            cboCommunicationType.DataSource = dtList

            '======================== END FOR COMMUNICATION TYPE=================


            '======================== START FOR DEVICE============================

            dtList = objMasterData.GetCommunictionDevice().Tables(0)
            cboComDevice.ValueMember = "Id"
            cboComDevice.DisplayMember = "Device"
            cboComDevice.DataSource = dtList

            '======================== END FOR DEVICE==============================


            '======================== START FOR CONNECTION TYPE=====================

            dtList = objMasterData.GetConnectionType.Tables(0)
            cboConnectionType.ValueMember = "Id"
            cboConnectionType.DisplayMember = "Connection"
            cboConnectionType.DataSource = dtList

            '======================== END FOR CONNECTION TYPE=======================



            '======================== START FOR PORT==============================

            Dim drRow As DataRow = Nothing
            Dim ar() As String = SerialPort.GetPortNames()
            If ar.Length > 0 Then
                dtList = New DataTable()
                dtList.Columns.Add("SerialPort", Type.GetType("System.String"))
                drRow = dtList.NewRow
                drRow("SerialPort") = Language.getMessage(mstrModuleName, 1, "Select")
                dtList.Rows.Add(drRow)
                For i As Integer = 0 To ar.Length - 1
                    drRow = dtList.NewRow
                    drRow("SerialPort") = ar(i).ToString
                    dtList.Rows.Add(drRow)
                Next
            End If
            cboPort.ValueMember = "SerialPort"
            cboPort.DisplayMember = "SerialPort"
            cboPort.DataSource = dtList

            '======================== END FOR PORT================================


            '======================== START FOR BAUD RATE===========================

            dtList = New DataTable
            dtList.Columns.Add("BaudRate", Type.GetType("System.String"))
            drRow = Nothing

            drRow = dtList.NewRow
            drRow("BaudRate") = Language.getMessage(mstrModuleName, 1, "Select")
            dtList.Rows.Add(drRow)

            drRow = dtList.NewRow
            drRow("BaudRate") = "9600"
            dtList.Rows.Add(drRow)

            drRow = dtList.NewRow
            drRow("BaudRate") = "19200"
            dtList.Rows.Add(drRow)

            drRow = dtList.NewRow
            drRow("BaudRate") = "38400"
            dtList.Rows.Add(drRow)

            drRow = dtList.NewRow
            drRow("BaudRate") = "57600"
            dtList.Rows.Add(drRow)

            drRow = dtList.NewRow
            drRow("BaudRate") = "115200"
            dtList.Rows.Add(drRow)

            cboBaudRate.ValueMember = "BaudRate"
            cboBaudRate.DisplayMember = "BaudRate"
            cboBaudRate.DataSource = dtList

            '======================== END FOR BAUD RATE=============================

            Dim objCompany As New clsCompany_Master
            Dim dsCombos As New DataSet
            dsCombos = objCompany.GetList("Company", True)
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Company")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function Validation() As Boolean
        Try

            Dim objRegx As New Regex("^(([01]?\d\d?|2[0-4]\d|25[0-5])\.){3}([01]?\d\d?|25[0-5]|2[0-4]\d)$")

            If CInt(cboCommunicationType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Communication Type is compulsory information.Please Select Communication Type."), enMsgBoxStyle.Information)
                cboCommunicationType.Select()
                Return False

            ElseIf txtDeviceCode.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Device Code cannot blank.Device Code is required information."), enMsgBoxStyle.Information)
                txtDeviceCode.Select()
                Return False

            ElseIf CInt(cboComDevice.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Communication Device is compulsory information.Please Select Communication Device."), enMsgBoxStyle.Information)
                cboComDevice.Select()
                Return False

            ElseIf CInt(cboConnectionType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Connection Type is compulsory information.Please Select Connection Type."), enMsgBoxStyle.Information)
                cboConnectionType.Select()
                Return False



                'Pinkal (04-Oct-2017) -- Start
                'Enhancement - Working SAGEM Database Device Integration.

                'ElseIf CInt(cboComDevice.SelectedValue) <> enFingerPrintDevice.Handpunch AndAlso txtIp.Text.Trim = "" And CInt(cboConnectionType.SelectedValue) = 1 Then  'FOR TCP / IP
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "IP Address cannot be blank.IP Address is required information."), enMsgBoxStyle.Information)
                '    txtIp.Select()
                '    Return False


                'ElseIf CInt(cboComDevice.SelectedValue) <> enFingerPrintDevice.Handpunch AndAlso objRegx.IsMatch(txtIp.Text.Trim) = False AndAlso CInt(cboConnectionType.SelectedValue) = 1 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Invalid IP Address.Please Enter Valid IP Address."), enMsgBoxStyle.Information)
                '    txtIp.Select()
                '    Return False


                'ElseIf CInt(cboComDevice.SelectedValue) <> enFingerPrintDevice.Handpunch AndAlso txtPort.Text.Trim = "" And CInt(cboConnectionType.SelectedValue) = 1 Then 'FOR TCP / IP
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Port No cannot be blank.Port No is required information."), enMsgBoxStyle.Information)
                '    txtPort.Select()
                '    Return False

                'ElseIf CInt(cboComDevice.SelectedValue) <> enFingerPrintDevice.Handpunch AndAlso txtMachineSN.Text.Trim = "" And (CInt(cboConnectionType.SelectedValue) = 2 Or CInt(cboConnectionType.SelectedValue) = 3) Then  'FOR RS232 / 485
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Machine Serial No cannot be blank.Machine Serial No is required information."), enMsgBoxStyle.Information)
                '    txtMachineSN.Select()
                '    Return False


            ElseIf CInt(cboComDevice.SelectedValue) <> enFingerPrintDevice.Handpunch AndAlso CInt(cboComDevice.SelectedValue) <> enFingerPrintDevice.SAGEM Then

                If txtIp.Text.Trim = "" And CInt(cboConnectionType.SelectedValue) = 1 Then  'FOR TCP / IP
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "IP Address cannot be blank.IP Address is required information."), enMsgBoxStyle.Information)
                txtIp.Select()
                Return False


                ElseIf objRegx.IsMatch(txtIp.Text.Trim) = False AndAlso CInt(cboConnectionType.SelectedValue) = 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Invalid IP Address.Please Enter Valid IP Address."), enMsgBoxStyle.Information)
                txtIp.Select()
                Return False


                ElseIf txtPort.Text.Trim = "" And CInt(cboConnectionType.SelectedValue) = 1 Then 'FOR TCP / IP
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Port No cannot be blank.Port No is required information."), enMsgBoxStyle.Information)
                txtPort.Select()
                Return False

                ElseIf txtMachineSN.Text.Trim = "" And (CInt(cboConnectionType.SelectedValue) = 2 Or CInt(cboConnectionType.SelectedValue) = 3) Then  'FOR RS232 / 485
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Machine Serial No cannot be blank.Machine Serial No is required information."), enMsgBoxStyle.Information)
                txtMachineSN.Select()
                Return False

                End If

                'Pinkal (04-Oct-2017) -- End

                'Sohail (29 Jun 2019) -- Start
                'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                If CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.FingerTec AndAlso txtDeviceModel.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Device Model is compulsory information.Please define Device Model."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtDeviceModel.Focus()
                    Return False
                End If
                'Sohail (29 Jun 2019) -- End

            ElseIf CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.ACTAtek AndAlso (txtUserID.Visible AndAlso txtUserID.Text.Trim.Length <= 0) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "User ID cannot be blank.User ID is required information."), enMsgBoxStyle.Information)
                txtUserID.Focus()
                Return False

            ElseIf CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.ACTAtek AndAlso (txtPassword.Visible AndAlso txtPassword.Text.Trim.Length <= 0) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Password cannot be blank.Password is required information."), enMsgBoxStyle.Information)
                txtPassword.Focus()
                Return False


                'Pinkal (04-Oct-2017) -- Start
                'Enhancement - Working SAGEM Database Device Integration.
            ElseIf CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.SAGEM Then

                If mstrDatabaseServer.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage("frmSagem_Configuration", 1, "Database Server is compulsory information.Please define database server."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False

                ElseIf mstrDatabaseName.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage("frmSagem_Configuration", 2, "Database Name is compulsory information.Please define database name."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False

                ElseIf mstrDatabaseUserName.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage("frmSagem_Configuration", 3, "Database User Name is compulsory information.Please define database user name."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False

                ElseIf mstrDatabasePassword.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage("frmSagem_Configuration", 4, "Database User Password is compulsory information.Please define database user password."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False
                End If
                'Pinkal (04-Oct-2017) -- End

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Public Sub FillDatatable()
        Try
            If dtSetting Is Nothing Then
                dtSetting = New DataSet()
                dtSetting.Tables.Add("Device")
                dtSetting.Tables(0).Columns.Add("deviceunkid", Type.GetType("System.Int32")).AutoIncrement = True
                dtSetting.Tables(0).Columns("deviceunkid").AutoIncrementSeed = 1
                dtSetting.Tables(0).Columns("deviceunkid").AutoIncrementStep = 1
                dtSetting.Tables(0).Columns.Add("devicecode", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("companyunkid", Type.GetType("System.Int32"))
                dtSetting.Tables(0).Columns.Add("commtype", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("commdevice", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("connectiontype", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("ip", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("port", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("baudrate", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("machinesrno", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("commtypeid", Type.GetType("System.Int32"))
                dtSetting.Tables(0).Columns.Add("commdeviceid", Type.GetType("System.Int32"))
                dtSetting.Tables(0).Columns.Add("connectiontypeid", Type.GetType("System.Int32"))

                'Pinkal (04-Jun-2015) -- Start
                'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
                dtSetting.Tables(0).Columns.Add("commkey", Type.GetType("System.String"))
                'Pinkal (04-Jun-2015) -- End


                'Pinkal (07-Jul-2015) -- Start
                'Enhancement - IMPLEMENTING INTEGRATION OF ACTATEK FOR SEAMIC IN TNA MODULE.
                dtSetting.Tables(0).Columns.Add("userid", Type.GetType("System.String"))
                dtSetting.Tables(0).Columns.Add("password", Type.GetType("System.String"))
                'Pinkal (07-Jul-2015) -- End

                'Sohail (29 Jun 2019) -- Start
                'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                dtSetting.Tables(0).Columns.Add("devicemodel", Type.GetType("System.String"))
                'Sohail (29 Jun 2019) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDatatable", mstrModuleName)
        End Try
    End Sub

    Public Sub SetValue()
        Try

            If menAction = enAction.ADD_CONTINUE Then

                Dim dr As DataRow = dtSetting.Tables(0).NewRow
                dr("companyunkid") = CInt(cboCompany.SelectedValue)
                dr("devicecode") = txtDeviceCode.Text.Trim & "||" & getIP()
                dr("commType") = cboCommunicationType.Text.Trim
                dr("commdevice") = cboComDevice.Text.Trim
                dr("connectiontype") = cboConnectionType.Text.Trim
                dr("commtypeid") = CInt(cboCommunicationType.SelectedValue)
                dr("commdeviceid") = CInt(cboComDevice.SelectedValue)
                dr("connectiontypeid") = CInt(cboConnectionType.SelectedValue)

                If txtIp.Enabled = True And txtIp.Visible Then
                    dr("ip") = txtIp.Text.Trim
                Else
                    'Pinkal (04-Oct-2017) -- Start
                    'Enhancement - Working SAGEM Database Device Integration.
                    If CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.SAGEM Then
                        dr("ip") = mstrDatabaseServer
                    Else
                    dr("ip") = ""
                End If
                    'Pinkal (04-Oct-2017) -- End
                End If

                If txtPort.Enabled = True And txtPort.Visible Then
                    dr("port") = txtPort.Text.Trim
                ElseIf cboPort.Enabled = True And cboPort.Visible Then
                    dr("port") = cboPort.Text.Trim
                    'Pinkal (04-Oct-2017) -- Start
                    'Enhancement - Working SAGEM Database Device Integration.
                Else
                    dr("port") = ""
                    'Pinkal (04-Oct-2017) -- End
                End If

                If cboBaudRate.Enabled = True And cboBaudRate.Visible Then
                    dr("baudrate") = cboBaudRate.Text.Trim
                Else
                    dr("baudrate") = ""
                End If

                dr("machinesrno") = txtMachineSN.Text.Trim

                'Pinkal (04-Jun-2015) -- Start
                'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
                dr("commkey") = txtCommunicationKey.Text.Trim
                'Pinkal (04-Jun-2015) -- End


                'Pinkal (07-Jul-2015) -- Start
                'Enhancement - IMPLEMENTING INTEGRATION OF ACTATEK FOR SEAMIC IN TNA MODULE.
                dr("userid") = txtUserID.Text.Trim
                dr("password") = txtPassword.Text.Trim
                'Pinkal (07-Jul-2015) -- End

                'Sohail (29 Jun 2019) -- Start
                'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                dr("devicemodel") = txtDeviceModel.Text.Trim
                'Sohail (29 Jun 2019) -- End

                dtSetting.Tables(0).Rows.Add(dr)

            ElseIf menAction = enAction.EDIT_ONE Then

                If dtSetting.Tables(0).Rows.Count > 0 Then
                    Dim drRow As DataRow() = dtSetting.Tables(0).Select("deviceunkid = " & mintDeviceID)
                    If drRow.Length > 0 Then
                        drRow(0)("companyunkid") = cboCompany.SelectedValue
                        drRow(0)("devicecode") = txtDeviceCode.Text.Trim & "||" & getIP()
                        drRow(0)("commType") = cboCommunicationType.Text.Trim
                        drRow(0)("commdevice") = cboComDevice.Text.Trim
                        drRow(0)("connectiontype") = cboConnectionType.Text.Trim
                        drRow(0)("commtypeid") = CInt(cboCommunicationType.SelectedValue)
                        drRow(0)("commdeviceid") = CInt(cboComDevice.SelectedValue)
                        drRow(0)("connectiontypeid") = CInt(cboConnectionType.SelectedValue)

                        If txtIp.Enabled = True And txtIp.Visible Then
                            drRow(0)("ip") = txtIp.Text.Trim
                        Else
                            'Pinkal (04-Oct-2017) -- Start
                            'Enhancement - Working SAGEM Database Device Integration.
                            If CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.SAGEM Then
                                drRow(0)("ip") = mstrDatabaseServer
                            Else
                            drRow(0)("ip") = ""
                        End If
                            'Pinkal (04-Oct-2017) -- End
                        End If

                        If txtPort.Enabled = True And txtPort.Visible Then
                            drRow(0)("port") = txtPort.Text.Trim
                        ElseIf cboPort.Enabled = True And cboPort.Visible Then
                            drRow(0)("port") = cboPort.Text.Trim
                        End If

                        If cboBaudRate.Enabled = True Then
                            drRow(0)("baudrate") = cboBaudRate.Text.Trim
                        Else
                            drRow(0)("baudrate") = ""
                        End If

                        drRow(0)("machinesrno") = txtMachineSN.Text.Trim

                        'Pinkal (04-Jun-2015) -- Start
                        'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
                        drRow(0)("commkey") = txtCommunicationKey.Text.Trim
                        'Pinkal (04-Jun-2015) -- End

                        'Pinkal (07-Jul-2015) -- Start
                        'Enhancement - IMPLEMENTING INTEGRATION OF ACTATEK FOR SEAMIC IN TNA MODULE.
                        drRow(0)("userid") = txtUserID.Text.Trim
                        drRow(0)("password") = txtPassword.Text.Trim
                        'Pinkal (07-Jul-2015) -- End

                        'Sohail (29 Jun 2019) -- Start
                        'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                        drRow(0)("devicemodel") = txtDeviceModel.Text.Trim
                        'Sohail (29 Jun 2019) -- End

                        dtSetting.AcceptChanges()

                    End If

                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Public Sub Getvalue()
        Try
            If dtSetting IsNot Nothing AndAlso dtSetting.Tables(0).Rows.Count > 0 Then
                Dim drRow As DataRow() = dtSetting.Tables(0).Select("deviceunkid = " & mintDeviceID)

                If drRow.Length > 0 Then
                    cboCompany.SelectedValue = CInt(drRow(0)("Companyunkid"))
                    txtDeviceCode.Text = drRow(0)("devicecode").ToString().Trim.Remove(drRow(0)("devicecode").ToString().IndexOf("||"), drRow(0)("devicecode").ToString().Trim.Length - drRow(0)("devicecode").ToString().IndexOf("||"))
                    cboCommunicationType.SelectedValue = drRow(0)("commtypeid").ToString
                    cboComDevice.SelectedValue = drRow(0)("commdeviceid").ToString
                    cboConnectionType.SelectedValue = drRow(0)("connectiontypeid").ToString

                    If txtIp.Enabled = True Then
                        txtIp.Text = drRow(0)("ip").ToString
                    End If

                    If txtPort.Enabled = True And txtPort.Visible Then
                        txtPort.Text = drRow(0)("port").ToString
                    ElseIf cboPort.Enabled = True And cboPort.Visible Then
                        cboPort.Text = drRow(0)("port").ToString
                    End If

                    If cboBaudRate.Enabled = True Then
                        cboBaudRate.Text = drRow(0)("baudrate").ToString
                    End If

                    txtMachineSN.Text = drRow(0)("machinesrno").ToString

                    'Pinkal (04-Jun-2015) -- Start
                    'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
                    If dtSetting.Tables(0).Columns.Contains("commkey") Then
                        txtCommunicationKey.Text = drRow(0)("commkey").ToString()
                    End If
                    'Pinkal (04-Jun-2015) -- End


                    'Pinkal (07-Jul-2015) -- Start
                    'Enhancement - IMPLEMENTING INTEGRATION OF ACTATEK FOR SEAMIC IN TNA MODULE.
                    If dtSetting.Tables(0).Columns.Contains("userid") Then
                        txtUserID.Text = drRow(0)("userid").ToString()
                    End If
                    If dtSetting.Tables(0).Columns.Contains("password") Then
                        txtPassword.Text = drRow(0)("password").ToString()
                    End If
                    'Pinkal (07-Jul-2015) -- End


                    cboCompany.Enabled = False
                    objbtnSearchCompany.Enabled = False


                    'Pinkal (04-Oct-2017) -- Start
                    'Enhancement - Working SAGEM Database Device Integration.
                    If CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.SAGEM Then
                        Dim objConfig As New clsConfigOptions
                        objConfig._Companyunkid = CInt(cboCompany.SelectedValue)
                        mstrDatabaseServer = objConfig._SagemDataServerAddress
                        mstrDatabaseName = objConfig._SagemDatabaseName
                        mstrDatabaseUserName = objConfig._SagemDatabaseUserName
                        mstrDatabasePassword = clsSecurity.Decrypt(objConfig._SagemDatabasePassword, "ezee")
                        objConfig = Nothing
                    End If
                    'Pinkal (04-Oct-2017) -- End

                    'Sohail (29 Jun 2019) -- Start
                    'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                    If dtSetting.Tables(0).Columns.Contains("devicemodel") Then
                        txtDeviceModel.Text = drRow(0)("devicemodel").ToString()
                    End If
                    'Sohail (29 Jun 2019) -- End

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Getvalue", mstrModuleName)
        End Try
    End Sub

    Private Sub Comm_TcpIp(ByVal intComDevice As Integer)
        Try

            Cursor = Cursors.WaitCursor
            Select Case intComDevice

                Case enFingerPrintDevice.DigitalPersona 'Digital Persona

                Case enFingerPrintDevice.SecuGen  'SecuGen

                Case enFingerPrintDevice.ZKSoftware  'ZK software

                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    Dim bIsConnected = False
                    Dim iMachineNumber As Integer
                    Dim idwErrorCode As Integer

                    'Pinkal (04-Jun-2015) -- Start
                    'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
                    If txtCommunicationKey.Text.Trim.Length > 0 Then
                        axCZKEM1.SetCommPassword(CInt(txtCommunicationKey.Text))
                    End If
                    'Pinkal (04-Jun-2015) -- End

                    bIsConnected = axCZKEM1.Connect_Net(txtIp.Text.Trim(), Convert.ToInt32(txtPort.Text.Trim()))

                    If bIsConnected = True Then
                        ' iMachineNumber = 1  'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                        iMachineNumber = CInt(txtMachineSN.Text.Trim)
                        Dim strVer As String = ""
                        axCZKEM1.Disconnect()
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        axCZKEM1.Disconnect()
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device."), enMsgBoxStyle.Information)
                    End If

                    axCZKEM1 = Nothing

                Case enFingerPrintDevice.BioStar 'BioStar
                    Dim addr As IPAddress = IPAddress.Parse(txtIp.Text.Trim)
                    Dim handle As Integer = 0

                    Cursor.Current = Cursors.WaitCursor

                    Dim result As Integer
                    result = clsBioStar.BS_OpenSocket(txtIp.Text.Trim, Convert.ToInt32(txtPort.Text.Trim()), handle)

                    If result <> clsBioStar.BS_SUCCESS Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & ":" & result.ToString, enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)

                        result = clsBioStar.BS_CloseSocket(handle)
                    End If


                Case enFingerPrintDevice.ZKAcessControl
                    Dim ret As Integer = 0
                    Dim str As String = "protocol=TCP,ipaddress=" & txtIp.Text.Trim & ",port=" & txtPort.Text.Trim & ",timeout=2000,passwd="
                    h = clsZKAccessControl.Connect(str)
                    If h <> IntPtr.Zero Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                    Else
                        ret = clsZKAccessControl.PullLastError
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Connect device Failed! The error id is: ") & ret, enMsgBoxStyle.Information)
                        clsZKAccessControl.Disconnect(h)
                        h = IntPtr.Zero
                    End If

                    If IntPtr.Zero <> h Then
                        clsZKAccessControl.Disconnect(h)
                        h = IntPtr.Zero
                    End If


                Case enFingerPrintDevice.Anviz  'Anviz

                    Dim mintErrorNo As Integer = clsAnviz.CKT_RegisterNet(CInt(txtMachineSN.Text.Trim), txtIp.Text.Trim)

                    If mintErrorNo = clsAnviz.CKT_RESULT_OK Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                        clsAnviz.CKT_Disconnect()
                    Else

                        Select Case mintErrorNo

                            Case clsAnviz.CKT_ERROR_INVPARAM
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Invalid Parameter."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case clsAnviz.CKT_ERROR_NETDAEMONREADY
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Error in Netdaemonreday."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case clsAnviz.CKT_ERROR_CHECKSUMERR
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Checksum Error."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case clsAnviz.CKT_ERROR_MEMORYFULL
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Memory Full."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case clsAnviz.CKT_ERROR_INVFILENAME
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Invalid File Name."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case clsAnviz.CKT_ERROR_FILECANNOTOPEN
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "File Cannot Open."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case clsAnviz.CKT_ERROR_FILECONTENTBAD
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "File Content Bad."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case clsAnviz.CKT_ERROR_FILECANNOTCREATED
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "File Cannot Create."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case clsAnviz.CKT_ERROR_NOTHISPERSON
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Not this Person."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                            Case Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device."), enMsgBoxStyle.Information)
                                clsAnviz.CKT_Disconnect()
                                Exit Sub
                        End Select
                    End If


                    'Pinkal (07-Jul-2015) -- Start
                    'Enhancement - IMPLEMENTING INTEGRATION OF ACTATEK FOR SEAMIC IN TNA MODULE.

                Case enFingerPrintDevice.ACTAtek  'ACTAtek

                    Dim ActaService As New ACTAtekWSDL.ACTAtek
                    ActaService.Url = "http://" & txtIp.Text.Trim & "/cgi-bin/rpcrouter"
                    If ActaService.login(txtUserID.Text.Trim, txtPassword.Text.Trim) > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device."), enMsgBoxStyle.Information)
                    End If
                    'Pinkal (07-Jul-2015) -- End

                    'Pinkal (12-Jun-2018) -- Start
                    'Enhancement - Integrating biostar 2 with Aruti for voltamp.
                Case enFingerPrintDevice.BioStar2
                    Dim objConnection As New ArutiBS2.BS2Integration()
                    Dim xDeviceID As UInteger = 0
                    Dim xErrorMsg As String = ""
                    If objConnection.IsConnected(txtIp.Text.Trim(), CUShort(txtPort.Text.Trim()), xDeviceID, xErrorMsg) = False Then
                        eZeeMsgBox.Show(xErrorMsg, enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                    End If
                    'Pinkal (12-Jun-2018) -- End

                    'Sohail (29 Jun 2019) -- Start
                    'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                Case enFingerPrintDevice.FingerTec
                    Dim objFingerTec As New BioBridgeSDKDLL.BioBridgeSDKClass
                    Dim xDeviceID As Integer = 1
                    Dim xCommKey As Integer = 0
                    Integer.TryParse(txtCommunicationKey.Text, xCommKey)
                    If objFingerTec.Connect_TCPIP(txtDeviceModel.Text, xDeviceID, txtIp.Text.Trim, CUShort(txtPort.Text.Trim()), xCommKey) = 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device."), enMsgBoxStyle.Information)
                    End If
                    'Sohail (29 Jun 2019) -- End

                    'Pinkal (22-Apr-2020) -- Start
                    'Enhancement - New Anviz Integration for Gran Melia.
                Case enFingerPrintDevice.NewAnviz
                    Dim anviz_handle As IntPtr
                    clsAnvizNew.CChex_Init()
                    anviz_handle = clsAnvizNew.CChex_Start()
                    If anviz_handle = IntPtr.Zero Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                        clsAnvizNew.CChex_Stop(anviz_handle)
                    End If
                    'Pinkal (22-Apr-2020) -- End


                    'Pinkal (30-Sep-2021)-- Start
                    'DERM Enhancement : HIK Vision Biometric Integration.
                Case enFingerPrintDevice.HIKVision

                    CHCNetSDK.NET_DVR_Init()

                    Dim struLoginInfo As New CHCNetSDK.NET_DVR_USER_LOGIN_INFO()
                    Dim struDeviceInfoV40 As New CHCNetSDK.NET_DVR_DEVICEINFO_V40()
                    struDeviceInfoV40.struDeviceV30.sSerialNumber = New Byte(CHCNetSDK.SERIALNO_LEN - 1) {}

                    struLoginInfo.sDeviceAddress = txtIp.Text.Trim
                    struLoginInfo.sUserName = txtUserID.Text.Trim
                    struLoginInfo.sPassword = txtPassword.Text.Trim
                    UShort.TryParse(txtPort.Text.Trim, struLoginInfo.wPort)

                    Dim lUserID As Integer = -1
                    lUserID = CHCNetSDK.NET_DVR_Login_V40(struLoginInfo, struDeviceInfoV40)

                    If lUserID >= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                    Else
                        Dim nErr As UInteger = CHCNetSDK.NET_DVR_GetLastError()

                        If nErr = CHCNetSDK.NET_DVR_PASSWORD_ERROR Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Invalid User Name or password."), enMsgBoxStyle.Information)
                            If 1 = struDeviceInfoV40.bySupportLock Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "you have left") & " " & struDeviceInfoV40.byRetryLoginTime & " " & Language.getMessage(mstrModuleName, 31, "try opportunity."), enMsgBoxStyle.Information)
                            End If
                        ElseIf nErr = CHCNetSDK.NET_DVR_USER_LOCKED Then
                            If 1 = struDeviceInfoV40.bySupportLock Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "User is locked now.The Remaining Lock Time is") & " " & struDeviceInfoV40.dwSurplusLockTime, enMsgBoxStyle.Information)
                            End If
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "net error or dvr is busy!"), enMsgBoxStyle.Information)
                        End If
                    End If

                    If (lUserID >= 0) Then
                        CHCNetSDK.NET_DVR_Logout_V30(lUserID)
                        lUserID = -1
                    End If
                    CHCNetSDK.NET_DVR_Cleanup()

                    'Pinkal (30-Sep-2021) -- End

            End Select

            Cursor = Cursors.Default

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Comm_TcpIp", mstrModuleName)
        End Try
    End Sub

    Private Sub Comm_RS(ByVal intComDevice As Integer)
        Try

            Cursor = Cursors.WaitCursor

            Select Case intComDevice

                Case enFingerPrintDevice.DigitalPersona 'Digital Persona

                Case enFingerPrintDevice.SecuGen  'SecuGen

                Case enFingerPrintDevice.ZKSoftware  'ZK software

                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    Dim iMachineNumber As Integer
                    Dim iPort As Integer
                    Dim bIsConnected = False
                    Dim idwErrorCode As Integer

                    Dim sPort As String = cboPort.Text.Trim()
                    For iPort = 1 To 9
                        If sPort.IndexOf(iPort.ToString()) > -1 Then
                            Exit For
                        End If
                    Next

                    iMachineNumber = Convert.ToInt32(txtMachineSN.Text.Trim()) '//when you are using the serial port communication,you can distinguish different devices by their serial port number.

                    'Pinkal (04-Jun-2015) -- Start
                    'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
                    If txtCommunicationKey.Text.Trim.Length > 0 Then
                        axCZKEM1.SetCommPassword(CInt(txtCommunicationKey.Text))
                    End If
                    'Pinkal (04-Jun-2015) -- End

                    bIsConnected = axCZKEM1.Connect_Com(iPort, iMachineNumber, Convert.ToInt32(cboBaudRate.Text.Trim()))

                    If bIsConnected = True Then
                        axCZKEM1.Disconnect()
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        axCZKEM1.Disconnect()
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device."), enMsgBoxStyle.Information)
                    End If
                    axCZKEM1 = Nothing
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Comm_RS", mstrModuleName)
        End Try

    End Sub

    Private Sub Comm_UsbClient(ByVal intComDevice As Integer)
        Try

            Cursor = Cursors.WaitCursor

            Select Case intComDevice

                Case enFingerPrintDevice.DigitalPersona 'Digital Persona

                Case enFingerPrintDevice.SecuGen  'SecuGen

                Case enFingerPrintDevice.ZKSoftware  'ZK software

                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    Dim iMachineNumber As Integer
                    Dim idwErrorCode As Integer
                    Dim bIsConnected As Boolean = False

                    Cursor = Cursors.WaitCursor
                    axCZKEM1.Disconnect()

                    bIsConnected = False

                    Dim sCom As String = ""
                    Dim bSearch As Boolean
                    bSearch = SearchforCom(sCom)

                    If bSearch = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Can not find the virtual serial port that can be used"), enMsgBoxStyle.Exclamation)
                        Cursor = Cursors.Default
                        Return
                    End If

                    Dim iPort As Integer
                    For iPort = 1 To 9
                        If sCom.IndexOf(iPort.ToString()) > -1 Then
                            Exit For
                        End If
                    Next

                    iMachineNumber = Convert.ToInt32(txtMachineSN.Text.Trim)
                    If iMachineNumber = 0 Or iMachineNumber > 255 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "The Machine Number is invalid."), enMsgBoxStyle.Exclamation)
                        Cursor = Cursors.Default
                        Return
                    End If

                    'Pinkal (04-Jun-2015) -- Start
                    'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
                    If txtCommunicationKey.Text.Trim.Length > 0 Then
                        axCZKEM1.SetCommPassword(CInt(txtCommunicationKey.Text))
                    End If
                    'Pinkal (04-Jun-2015) -- End

                    Dim iBaudRate = 9600 '115200 is one possible baudrate value(its value cannot be 0)
                    bIsConnected = axCZKEM1.Connect_Com(iPort, iMachineNumber, iBaudRate)

                    If bIsConnected = True Then
                        axCZKEM1.Disconnect()
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        axCZKEM1.Disconnect()
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device."), enMsgBoxStyle.Information)
                    End If

                    axCZKEM1 = Nothing

            End Select

            Cursor = Cursors.Default

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Comm_UsbClient", mstrModuleName)
        End Try
    End Sub

    'Search for the virtual serial port created by usbclient.

    Public Function SearchforCom(ByRef sCom As String) As Boolean
        Dim sComValue As String
        Dim sTmpara As String
        Dim myReg As RegistryKey
        myReg = Registry.LocalMachine.OpenSubKey("HARDWARE\\DEVICEMAP\\SERIALCOMM")

        Dim sComName() As String
        sComName = myReg.GetValueNames() 'strings array composed of the key name holded by the subkey "SERIALCOMM"
        Dim i As Integer
        For i = 0 To sComName.Length - 1
            sComValue = myReg.GetValue(sComName(i)).ToString() 'obtain the key value of the corresponding key name
            If sComValue = "" Then
                Continue For
            End If

            sCom = ""
            Dim j As Integer
            If sComName(i) = "\Device\USBSER000" Then 'find the virtual serial port created by usbclient
                For j = 0 To 10
                    sTmpara = ""
                    Dim myReg2 As RegistryKey
                    myReg2 = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum\\USB\\VID_1B55&PID_B400\\" & j.ToString() & "\\Device Parameters")

                    If myReg2 IsNot Nothing Then
                        sTmpara = myReg2.GetValue("PortName").ToString()

                        If sComValue = sTmpara Then
                            sCom = sTmpara
                            Return True
                        End If
                    End If
                Next
            End If
        Next
        Return False
    End Function

#End Region

#Region "Button's Event"

    Private Sub btnTestConnection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestConnection.Click
        Try

            If Validation() Then

                If CInt(cboConnectionType.SelectedValue) = 1 Then   'TCP/ IP
                    Comm_TcpIp(CInt(cboComDevice.SelectedValue))

                ElseIf CInt(cboConnectionType.SelectedValue) = 2 Then  'RS 232 / 485
                    Comm_RS(CInt(cboComDevice.SelectedValue))

                ElseIf CInt(cboConnectionType.SelectedValue) = 3 Then   'USB Client 
                    Comm_UsbClient(CInt(cboComDevice.SelectedValue))
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTestConnection_Click", mstrModuleName)
        End Try
        Cursor = Cursors.Default
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Validation() Then

                If dtSetting IsNot Nothing Then

                    If dtSetting.Tables(0).Rows.Count > 0 Then

                        Dim drRow As DataRow() = dtSetting.Tables(0).Select("devicecode = '" & txtDeviceCode.Text.Trim & "||" & getIP() & "' and companyunkid = " & CInt(cboCompany.SelectedValue) _
                                                                            & " AND deviceunkid <> " & mintDeviceID & " AND deviceunkid <> -1")

                        If drRow.Length > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "This Device code already exists. Please define new device code."), enMsgBoxStyle.Information)
                            txtDeviceCode.Select()
                            Exit Sub
                        End If
                    End If

                    SetValue()


                    'Pinkal (04-Oct-2017) -- Start
                    'Enhancement - Working SAGEM Database Device Integration.
                    Dim objConfig As New clsConfigOptions
                    'Pinkal (04-Oct-2017) -- End

                    'Pinkal (06-May-2016) -- Start
                    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                    If (menAction = enAction.ADD_CONTINUE OrElse menAction = enAction.ADD_ONE) AndAlso CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.Handpunch Then

                        If objConfig.Create_Handpunch_Database() = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, Unable to create database for handpunch."), enMsgBoxStyle.Information)
                        End If
                    End If

                    If CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.Handpunch Then

                        'Dim objConfig As New clsConfigOptions

                        If mintLogsInterval <= 0 Then
                            Dim dtTable As DataTable = objConfig.GetHandpunchConfig()

                            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                                Dim drRow() As DataRow = dtTable.Select("VariableName = 'DownloadLogsInterval'")
                                If drRow.Length > 0 Then
                                    mintLogsInterval = CInt(drRow(0)("VariableValue"))
                                End If

                                drRow = Nothing
                                drRow = dtTable.Select("VariableName = 'DownloadUsersDayOfWeek'")
                                If drRow.Length > 0 Then
                                    mstrDaysWeek = drRow(0)("VariableValue").ToString()
                                End If

                                drRow = Nothing
                                drRow = dtTable.Select("VariableName = 'DownloadUsersTime'")
                                If drRow.Length > 0 Then
                                    mstrUserTime = drRow(0)("VariableValue").ToString()
                                End If

                            End If
                        End If

                        If (menAction = enAction.ADD_CONTINUE OrElse menAction = enAction.ADD_ONE) Then
                            If objConfig.InsertHandpunchDevice(txtDeviceCode.Text.Trim, txtIp.Text.Trim, txtPort.Text.Trim) = False Then
                                Exit Sub
                            End If
                        End If
                        If objConfig.UpdateHandpunchConfig(mintLogsInterval, mstrDaysWeek, mstrUserTime) = False Then
                            Exit Sub
                        End If


                        'Pinkal (04-Oct-2017) -- Start
                        'Enhancement - Working SAGEM Database Device Integration.
                    ElseIf CInt(cboComDevice.SelectedValue) = enFingerPrintDevice.SAGEM Then
                        objConfig._Companyunkid = CInt(cboCompany.SelectedValue)
                        objConfig._SagemDataServerAddress = mstrDatabaseServer
                        objConfig._SagemDatabaseName = mstrDatabaseName
                        objConfig._SagemDatabaseUserName = mstrDatabaseUserName
                        objConfig._SagemDatabasePassword = mstrDatabasePassword
                        objConfig.updateParam()
                    End If

                    'Pinkal (06-May-2016) -- End

                    objConfig = Nothing
                    'Pinkal (04-Oct-2017) -- End


                    If IO.File.Exists(objAppSetting._ApplicationPath & "DeviceSetting.xml") Then
                        Dim fl As New IO.FileInfo(objAppSetting._ApplicationPath & "DeviceSetting.xml")
                        fl.Attributes = IO.FileAttributes.Archive
                    End If

                    dtSetting.WriteXml(objAppSetting._ApplicationPath & "DeviceSetting.xml")

                    If IO.File.Exists(objAppSetting._ApplicationPath & "DeviceSetting.xml") Then
                        Dim fl As New IO.FileInfo(objAppSetting._ApplicationPath & "DeviceSetting.xml")
                        fl.Attributes = IO.FileAttributes.ReadOnly
                        fl.Attributes = IO.FileAttributes.Hidden
                    End If

                    mblnCancel = False

                    If menAction = enAction.ADD_CONTINUE Then
                        If cboCompany.Items.Count > 0 Then cboCompany.SelectedIndex = 0
                        If cboComDevice.Items.Count > 0 Then cboComDevice.SelectedIndex = 0
                        If cboCommunicationType.Items.Count > 0 Then cboCommunicationType.SelectedIndex = 0
                        If cboConnectionType.Items.Count > 0 Then cboConnectionType.SelectedIndex = 0
                        If cboPort.Items.Count > 0 Then cboPort.SelectedIndex = 0
                        If cboBaudRate.Items.Count > 0 Then cboBaudRate.SelectedIndex = 0
                        txtIp.Text = ""
                        txtDeviceCode.Text = ""
                        txtMachineSN.Text = "1"
                        txtPort.Text = ""
                        'Sohail (29 Jun 2019) -- Start
                        'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                        txtDeviceModel.Text = ""
                        'Sohail (29 Jun 2019) -- End
                    Else
                        Me.Close()
                    End If

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCompany.Click
        Dim ObjFrm As New frmCommonSearch
        Try
            With ObjFrm
                .ValueMember = cboCompany.ValueMember
                .DisplayMember = cboCompany.DisplayMember
                .DataSource = CType(cboCompany.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If ObjFrm.DisplayDialog Then
                cboCompany.SelectedValue = ObjFrm.SelectedValue
                cboCompany.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCompany_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub cboConnectionType_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboConnectionType.SelectedValueChanged
        Try

            Select Case CInt(cboConnectionType.SelectedValue)

                Case 0  'None
                    txtIp.Enabled = True
                    txtPort.Visible = True
                    txtPort.Enabled = True
                    cboPort.Visible = True
                    cboPort.Enabled = True
                    cboBaudRate.Enabled = True

                Case 1   'TCP / IP
                    txtIp.Enabled = True
                    txtIp.Enabled = True
                    txtPort.Visible = True
                    txtPort.Enabled = True
                    cboPort.Visible = False
                    cboPort.Enabled = False
                    cboBaudRate.Enabled = False
                    'txtPort.Text = "4370" 'Sohail (02 Nov 2013)

                Case 2 'RS232 / RS485
                    txtIp.Enabled = False
                    txtPort.Visible = False
                    txtPort.Enabled = False
                    cboPort.Visible = True
                    cboPort.Enabled = True
                    cboBaudRate.Enabled = True
                    txtIp.Text = ""
                    txtPort.Text = ""

                Case 3 'USBCLIENT
                    txtIp.Enabled = False
                    txtPort.Enabled = False
                    cboPort.Enabled = False
                    cboBaudRate.Enabled = False
                    txtIp.Text = ""
                    txtPort.Text = ""


            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboConnectionType_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboComDevice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboComDevice.SelectedIndexChanged
        Try

            'Pinkal (04-Jun-2015) -- Start
            'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
            txtCommunicationKey.Text = ""
            lblCommunicationKey.Visible = False
            txtCommunicationKey.Visible = False
            'Pinkal (04-Jun-2015) -- End


            'Pinkal (07-Jul-2015) -- Start
            'Enhancement - IMPLEMENTING INTEGRATION OF ACTATEK FOR SEAMIC IN TNA MODULE.
            LblUserID.Visible = False
            txtUserID.Visible = False
            txtUserID.Text = ""
            LblPassword.Visible = False
            txtPassword.Visible = False
            txtPassword.Text = ""
            'Pinkal (07-Jul-2015) -- End


            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
            If cboConnectionType.SelectedIndex >= 0 Then cboConnectionType.SelectedIndex = 0
            cboConnectionType.Enabled = True
            txtIp.Text = "192.168.1.201"
            txtIp.BringToFront()
            cboPort.Visible = True
            cboBaudRate.Enabled = True
            btnTestConnection.Enabled = True
            lnkHandpunchConfig.Visible = False
            'Pinkal (06-May-2016) -- End


            'Pinkal (04-Oct-2017) -- Start
            'Enhancement - Working SAGEM Database Device Integration.
            lnkConfigureSagem.Visible = False
            txtIp.Enabled = True
            'Pinkal (04-Oct-2017) -- End



            Select Case CInt(cboComDevice.SelectedIndex)
                Case enFingerPrintDevice.DigitalPersona 'Digital Persona
                    txtPort.Text = "4370"

                Case enFingerPrintDevice.SecuGen  'SecuGen
                    txtPort.Text = "4370"

                Case enFingerPrintDevice.ZKSoftware  'ZK software
                    txtPort.Text = "4370"

                    'Pinkal (04-Jun-2015) -- Start
                    'Enhancement - WORKING ON CITIONE CHANGES REQUIRED COMMUNICATION KEY AS PASSWORD.
                    lblCommunicationKey.Visible = True
                    txtCommunicationKey.Visible = True
                    'Pinkal (04-Jun-2015) -- End

                Case enFingerPrintDevice.BioStar 'Bio Star
                    txtPort.Text = "1470"


                    'Pinkal (07-Jul-2015) -- Start
                    'Enhancement - IMPLEMENTING INTEGRATION OF ACTATEK FOR SEAMIC IN TNA MODULE.

                Case enFingerPrintDevice.ACTAtek
                    LblUserID.Visible = True
                    txtUserID.Visible = True
                    txtUserID.Text = ""
                    LblPassword.Visible = True
                    txtPassword.Visible = True
                    txtPassword.Text = ""
                    'Pinkal (07-Jul-2015) -- End

                    'Pinkal (06-May-2016) -- Start
                    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                Case enFingerPrintDevice.Handpunch
                    cboConnectionType.SelectedIndex = 1
                    cboConnectionType.Enabled = False
                    txtPort.Text = "3001"
                    cboPort.Visible = False
                    cboBaudRate.Enabled = False
                    btnTestConnection.Enabled = False
                    lnkHandpunchConfig.Visible = True
                    'Pinkal (06-May-2016) -- End


                    'Pinkal (04-Oct-2017) -- Start
                    'Enhancement - Working SAGEM Database Device Integration.
                Case enFingerPrintDevice.SAGEM
                    cboConnectionType.SelectedIndex = 1
                    cboConnectionType.Enabled = False
                    txtPort.Text = ""
                    txtPort.Enabled = False
                    cboPort.Visible = False
                    cboBaudRate.Enabled = False
                    btnTestConnection.Enabled = False
                    lnkHandpunchConfig.Visible = False
                    lnkConfigureSagem.Visible = True
                    txtIp.Text = ""
                    txtIp.Enabled = False
                    'Pinkal (04-Oct-2017) -- End

                    'Sohail (29 Jun 2019) -- Start
                    'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                Case enFingerPrintDevice.FingerTec
                    txtPort.Text = "4370"
                    lblCommunicationKey.Visible = True
                    txtCommunicationKey.Visible = True
                    'Sohail (29 Jun 2019) -- End

                    'Pinkal (22-Apr-2020) -- Start
                    'Enhancement - New Anviz Integration for Gran Melia.
                Case enFingerPrintDevice.NewAnviz
                    txtPort.Text = "5010"
                    cboPort.Visible = False
                    cboBaudRate.Enabled = False
                    lblCommunicationKey.Visible = False
                    txtCommunicationKey.Visible = False
                    lnkHandpunchConfig.Visible = False
                    lnkConfigureSagem.Visible = False
                    'Pinkal (22-Apr-2020) -- End

                    'Pinkal (30-Sep-2021)-- Start
                    'DERM Enhancement : HIK Vision Biometric Integration.
                Case enFingerPrintDevice.HIKVision
                    txtPort.Text = "8000"
                    cboPort.Visible = False
                    cboBaudRate.Enabled = False
                    lblCommunicationKey.Visible = False
                    txtCommunicationKey.Visible = False
                    lnkHandpunchConfig.Visible = False
                    lnkConfigureSagem.Visible = False
                    LblUserID.Visible = True
                    txtUserID.Visible = True
                    txtUserID.Text = ""
                    LblPassword.Visible = True
                    txtPassword.Visible = True
                    txtPassword.Text = ""
                    'Pinkal (30-Sep-2021) -- End

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboComDevice_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkHandpunchConfig_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkHandpunchConfig.LinkClicked
        Dim frmHandpunch As New frmHandpunch_Configuration
        Try
            frmHandpunch.displayDialog(mintLogsInterval, mstrDaysWeek, mstrUserTime)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkHandpunchConfig_LinkClicked", mstrModuleName)
        Finally
            frmHandpunch = Nothing
        End Try
    End Sub

    'Pinkal (04-Oct-2017) -- Start
    'Enhancement - Working SAGEM Database Device Integration.

    Private Sub lnkConfigureSagem_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkConfigureSagem.LinkClicked
        Dim frmSagem As New frmSagem_Configuration
        Try
            frmSagem.displayDialog(mstrDatabaseServer, mstrDatabaseName, mstrDatabaseUserName, mstrDatabasePassword)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkConfigureSagem_LinkClicked", mstrModuleName)
        Finally
            frmSagem_Configuration = Nothing
        End Try
    End Sub

    'Pinkal (04-Oct-2017) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbCompanyInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCompanyInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnTestConnection.GradientBackColor = GUI._ButttonBackColor
            Me.btnTestConnection.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnTestConnection.Text = Language._Object.getCaption(Me.btnTestConnection.Name, Me.btnTestConnection.Text)
            Me.lblSelectCompany.Text = Language._Object.getCaption(Me.lblSelectCompany.Name, Me.lblSelectCompany.Text)
            Me.lblComProduct.Text = Language._Object.getCaption(Me.lblComProduct.Name, Me.lblComProduct.Text)
            Me.lblConnectionType.Text = Language._Object.getCaption(Me.lblConnectionType.Name, Me.lblConnectionType.Text)
            Me.lblCommType.Text = Language._Object.getCaption(Me.lblCommType.Name, Me.lblCommType.Text)
            Me.lblIP.Text = Language._Object.getCaption(Me.lblIP.Name, Me.lblIP.Text)
            Me.lblPort.Text = Language._Object.getCaption(Me.lblPort.Name, Me.lblPort.Text)
            Me.lblBaudrate.Text = Language._Object.getCaption(Me.lblBaudrate.Name, Me.lblBaudrate.Text)
            Me.lblRsMachineSN.Text = Language._Object.getCaption(Me.lblRsMachineSN.Name, Me.lblRsMachineSN.Text)
            Me.LblUserID.Text = Language._Object.getCaption(Me.LblUserID.Name, Me.LblUserID.Text)
            Me.lblDeviceCode.Text = Language._Object.getCaption(Me.lblDeviceCode.Name, Me.lblDeviceCode.Text)
            Me.LblPassword.Text = Language._Object.getCaption(Me.LblPassword.Name, Me.LblPassword.Text)
            Me.lblCommunicationKey.Text = Language._Object.getCaption(Me.lblCommunicationKey.Name, Me.lblCommunicationKey.Text)
            Me.gbCompanyInformation.Text = Language._Object.getCaption(Me.gbCompanyInformation.Name, Me.gbCompanyInformation.Text)
            Me.lnkHandpunchConfig.Text = Language._Object.getCaption(Me.lnkHandpunchConfig.Name, Me.lnkHandpunchConfig.Text)
            Me.lnkConfigureSagem.Text = Language._Object.getCaption(Me.lnkConfigureSagem.Name, Me.lnkConfigureSagem.Text)
            Me.lblDeviceModel.Text = Language._Object.getCaption(Me.lblDeviceModel.Name, Me.lblDeviceModel.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Communication Device is compulsory information.Please Select Communication Device.")
            Language.setMessage(mstrModuleName, 3, "Connection Type is compulsory information.Please Select Connection Type.")
            Language.setMessage(mstrModuleName, 4, "IP Address cannot be blank.IP Address is required information.")
            Language.setMessage(mstrModuleName, 5, "Port No cannot be blank.Port No is required information.")
            Language.setMessage(mstrModuleName, 6, "Machine Serial No cannot be blank.Machine Serial No is required information.")
            Language.setMessage(mstrModuleName, 7, "Connection is done successfully.")
            Language.setMessage(mstrModuleName, 8, "Unable to connect the device.")
            Language.setMessage(mstrModuleName, 9, "Can not find the virtual serial port that can be used")
            Language.setMessage(mstrModuleName, 10, "The Machine Number is invalid.")
            Language.setMessage(mstrModuleName, 11, "Device Code cannot blank.Device Code is required information.")
            Language.setMessage(mstrModuleName, 12, "This Device code already exists. Please define new device code.")
            Language.setMessage(mstrModuleName, 13, "Communication Type is compulsory information.Please Select Communication Type.")
            Language.setMessage(mstrModuleName, 14, "Invalid IP Address.Please Enter Valid IP Address.")
            Language.setMessage(mstrModuleName, 15, "Connect device Failed! The error id is:")
            Language.setMessage(mstrModuleName, 16, "Invalid Parameter.")
            Language.setMessage(mstrModuleName, 17, "Error in Netdaemonreday.")
            Language.setMessage(mstrModuleName, 18, "Checksum Error.")
            Language.setMessage(mstrModuleName, 19, "Memory Full.")
            Language.setMessage(mstrModuleName, 20, "Invalid File Name.")
            Language.setMessage(mstrModuleName, 21, "File Cannot Open.")
            Language.setMessage(mstrModuleName, 22, "File Content Bad.")
            Language.setMessage(mstrModuleName, 23, "File Cannot Create.")
            Language.setMessage(mstrModuleName, 24, "Not this Person.")
            Language.setMessage(mstrModuleName, 25, "User ID cannot be blank.User ID is required information.")
            Language.setMessage(mstrModuleName, 26, "Password cannot be blank.Password is required information.")
            Language.setMessage(mstrModuleName, 27, "Sorry, Unable to create database for handpunch.")
			Language.setMessage(mstrModuleName, 28, "Device Model is compulsory information.Please define Device Model.")
			Language.setMessage(mstrModuleName, 29, "Invalid User Name or password.")
			Language.setMessage(mstrModuleName, 30, "you have left")
			Language.setMessage(mstrModuleName, 31, "try opportunity.")
			Language.setMessage(mstrModuleName, 32, "User is locked now.The Remaining Lock Time is")
			Language.setMessage(mstrModuleName, 33, "net error or dvr is busy!")
			Language.setMessage("frmSagem_Configuration", 1, "Database Server is compulsory information.Please define database server.")
			Language.setMessage("frmSagem_Configuration", 2, "Database Name is compulsory information.Please define database name.")
			Language.setMessage("frmSagem_Configuration", 3, "Database User Name is compulsory information.Please define database user name.")
			Language.setMessage("frmSagem_Configuration", 4, "Database User Password is compulsory information.Please define database user password.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class