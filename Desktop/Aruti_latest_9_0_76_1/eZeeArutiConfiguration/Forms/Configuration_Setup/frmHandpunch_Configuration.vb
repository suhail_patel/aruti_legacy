﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization


#End Region

Public Class frmHandpunch_Configuration

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmHandpunch_Configuration"
    Private mblnCancel As Boolean = True
    Dim mintLogsInterval As Integer = 30
    Dim mstrDaysWeek As String = "SA;WE"
    Dim mstrUserTime As String = "3:00"

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intLogsInterval As Integer, ByRef strDaysWeek As String, ByRef strUserTime As String) As Boolean
        Try
            Me.ShowDialog()
            intLogsInterval = mintLogsInterval
            strDaysWeek = mstrDaysWeek
            strUserTime = mstrUserTime
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmHandpunch_Configuration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillDays()
            GetHandpunchConfig()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHandpunch_Configuration_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillDays()
        Try
            Dim days() As String = CultureInfo.CurrentCulture.DateTimeFormat.DayNames
            For i As Integer = 0 To 6
                chkDays.Items.Add(days(i).ToString())
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDays", mstrModuleName)
        End Try
    End Sub

    Public Sub GetHandpunchConfig()
        Try
            Dim objConfig As New clsConfigOptions
            Dim dtTable As DataTable = objConfig.GetHandpunchConfig()

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("VariableName = 'DownloadLogsInterval'")
                If drRow.Length > 0 Then
                    nudLogsInterval.Value = CInt(drRow(0)("VariableValue"))
                End If

                drRow = Nothing
                drRow = dtTable.Select("VariableName = 'DownloadUsersDayOfWeek'")
                If drRow.Length > 0 Then
                    mstrDaysWeek = drRow(0)("VariableValue").ToString()
                End If

                drRow = Nothing
                drRow = dtTable.Select("VariableName = 'DownloadUsersTime'")
                If drRow.Length > 0 Then
                    dtpDownloadUserTime.Value = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & drRow(0)("VariableValue").ToString())
                End If

            Else
                nudLogsInterval.Value = CInt(mintLogsInterval)
                dtpDownloadUserTime.Value = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & mstrUserTime)
            End If


            If mstrDaysWeek.Trim.Length > 0 Then

                Dim arDaysWeek() As String = mstrDaysWeek.Trim.Split(CChar(";"))

                If arDaysWeek.Length > 0 Then

                    For i As Integer = 0 To arDaysWeek.Length - 1
                        Dim index As Integer = 0
                        Select Case arDaysWeek(i).ToString().ToUpper
                            Case "SU"
                                index = chkDays.FindString(CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Sunday))
                            Case "MO"
                                index = chkDays.FindString(CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Monday))
                            Case "TU"
                                index = chkDays.FindString(CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Tuesday))
                            Case "WE"
                                index = chkDays.FindString(CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Wednesday))
                            Case "TH"
                                index = chkDays.FindString(CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Thursday))
                            Case "FR"
                                index = chkDays.FindString(CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Friday))
                            Case "SA"
                                index = chkDays.FindString(CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Saturday))
                        End Select
                        chkDays.SetItemChecked(index, True)
                    Next

                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetHandpunchConfig", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Dim mblnFlag As Boolean = False
        Try
            mblnFlag = chkSelectAll.Checked
            For i As Integer = 0 To chkDays.Items.Count - 1
                chkDays.SetItemChecked(i, mblnFlag)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If chkDays.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one day to download the User information from device."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                chkDays.Focus()
                Exit Sub
            End If

            If chkDays.CheckedItems.Count > 0 Then

                mstrDaysWeek = ""
                For i As Integer = 0 To chkDays.CheckedItems.Count - 1
                    Select Case chkDays.GetItemText(chkDays.CheckedItems(i))

                        Case CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Sunday)
                            mstrDaysWeek &= "SU;"
                        Case CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Monday)
                            mstrDaysWeek &= "MO;"
                        Case CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Tuesday)
                            mstrDaysWeek &= "TU;"
                        Case CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Wednesday)
                            mstrDaysWeek &= "WE;"
                        Case CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Thursday)
                            mstrDaysWeek &= "TH;"
                        Case CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Friday)
                            mstrDaysWeek &= "FR;"
                        Case CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Saturday)
                            mstrDaysWeek &= "SA;"
                    End Select

                Next

                If mstrDaysWeek.Trim.Length > 0 Then
                    mstrDaysWeek = mstrDaysWeek.Trim.ToString.Substring(0, mstrDaysWeek.Trim.ToString.Length - 1)
                End If

                mintLogsInterval = CInt(nudLogsInterval.Value)
                mstrUserTime = dtpDownloadUserTime.Value.ToString("HH:mm")

            End If

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbHandpunchConfig.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbHandpunchConfig.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbHandpunchConfig.Text = Language._Object.getCaption(Me.gbHandpunchConfig.Name, Me.gbHandpunchConfig.Text)
			Me.LblDownloadUserDOW.Text = Language._Object.getCaption(Me.LblDownloadUserDOW.Name, Me.LblDownloadUserDOW.Text)
			Me.chkDays.Text = Language._Object.getCaption(Me.chkDays.Name, Me.chkDays.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.LblDownloadUserTime.Text = Language._Object.getCaption(Me.LblDownloadUserTime.Name, Me.LblDownloadUserTime.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please check atleast one day to download the User information from device.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class