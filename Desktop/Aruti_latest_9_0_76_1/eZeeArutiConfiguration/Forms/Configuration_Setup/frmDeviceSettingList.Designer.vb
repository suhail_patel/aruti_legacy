﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeviceSettingList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDeviceSettingList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvDeviceSetting = New eZee.Common.eZeeListView(Me.components)
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhDeviceCode = New System.Windows.Forms.ColumnHeader
        Me.colhCommType = New System.Windows.Forms.ColumnHeader
        Me.colhCommDevice = New System.Windows.Forms.ColumnHeader
        Me.colhConnectionType = New System.Windows.Forms.ColumnHeader
        Me.colhIp = New System.Windows.Forms.ColumnHeader
        Me.colhPort = New System.Windows.Forms.ColumnHeader
        Me.colhBaudrate = New System.Windows.Forms.ColumnHeader
        Me.colhMachineSrNo = New System.Windows.Forms.ColumnHeader
        Me.objColhCompanyunkid = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvDeviceSetting)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(688, 374)
        Me.pnlMainInfo.TabIndex = 1
        '
        'lvDeviceSetting
        '
        Me.lvDeviceSetting.BackColorOnChecked = True
        Me.lvDeviceSetting.ColumnHeaders = Nothing
        Me.lvDeviceSetting.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCompany, Me.colhDeviceCode, Me.colhCommType, Me.colhCommDevice, Me.colhConnectionType, Me.colhIp, Me.colhPort, Me.colhBaudrate, Me.colhMachineSrNo, Me.objColhCompanyunkid})
        Me.lvDeviceSetting.CompulsoryColumns = ""
        Me.lvDeviceSetting.FullRowSelect = True
        Me.lvDeviceSetting.GridLines = True
        Me.lvDeviceSetting.GroupingColumn = Nothing
        Me.lvDeviceSetting.HideSelection = False
        Me.lvDeviceSetting.Location = New System.Drawing.Point(13, 67)
        Me.lvDeviceSetting.MinColumnWidth = 50
        Me.lvDeviceSetting.MultiSelect = False
        Me.lvDeviceSetting.Name = "lvDeviceSetting"
        Me.lvDeviceSetting.OptionalColumns = ""
        Me.lvDeviceSetting.ShowMoreItem = False
        Me.lvDeviceSetting.ShowSaveItem = False
        Me.lvDeviceSetting.ShowSelectAll = True
        Me.lvDeviceSetting.ShowSizeAllColumnsToFit = True
        Me.lvDeviceSetting.Size = New System.Drawing.Size(663, 246)
        Me.lvDeviceSetting.Sortable = True
        Me.lvDeviceSetting.TabIndex = 6
        Me.lvDeviceSetting.UseCompatibleStateImageBehavior = False
        Me.lvDeviceSetting.View = System.Windows.Forms.View.Details
        '
        'colhCompany
        '
        Me.colhCompany.Tag = "colhCompany"
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 0
        '
        'colhDeviceCode
        '
        Me.colhDeviceCode.Tag = "colhDeviceCode"
        Me.colhDeviceCode.Text = "Code"
        Me.colhDeviceCode.Width = 80
        '
        'colhCommType
        '
        Me.colhCommType.Tag = "colhCommType"
        Me.colhCommType.Text = "Communication Type"
        Me.colhCommType.Width = 115
        '
        'colhCommDevice
        '
        Me.colhCommDevice.Tag = "colhCommDevice"
        Me.colhCommDevice.Text = "Communcation Device"
        Me.colhCommDevice.Width = 125
        '
        'colhConnectionType
        '
        Me.colhConnectionType.Tag = "colhConnectionType"
        Me.colhConnectionType.Text = "Connection Type"
        Me.colhConnectionType.Width = 100
        '
        'colhIp
        '
        Me.colhIp.Tag = "colhIp"
        Me.colhIp.Text = "IP"
        Me.colhIp.Width = 95
        '
        'colhPort
        '
        Me.colhPort.Tag = "colhPort"
        Me.colhPort.Text = "Port"
        Me.colhPort.Width = 70
        '
        'colhBaudrate
        '
        Me.colhBaudrate.Tag = "colhBaudrate"
        Me.colhBaudrate.Text = "Baud Rate"
        Me.colhBaudrate.Width = 70
        '
        'colhMachineSrNo
        '
        Me.colhMachineSrNo.Tag = "colhMachineSrNo"
        Me.colhMachineSrNo.Text = "Device No"
        Me.colhMachineSrNo.Width = 80
        '
        'objColhCompanyunkid
        '
        Me.objColhCompanyunkid.Tag = "objColhCompanyunkid"
        Me.objColhCompanyunkid.Text = "Companyunkid"
        Me.objColhCompanyunkid.Width = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(688, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Device List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 319)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(688, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(476, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(373, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(270, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(579, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmDeviceSettingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 374)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDeviceSettingList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Device Setting List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents lvDeviceSetting As eZee.Common.eZeeListView
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCommType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhConnectionType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCommDevice As System.Windows.Forms.ColumnHeader
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents colhIp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPort As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBaudrate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMachineSrNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDeviceCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhCompanyunkid As System.Windows.Forms.ColumnHeader
End Class
