﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmERCUserSelection

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmConfigOptions"
    Private mdtUsers As DataTable = Nothing
    Private mstrcsvUserIds As String = String.Empty
    Private mblnCancel As Boolean = True
    Private mdUserView As DataView
    'S.SANDEEP [14-APR-2017] -- START
    'ISSUE/ENHANCEMENT : 'ISSUE/ENHANCEMENT : GET USER BASED ON COMPANY ACCESS PRIVILEGE IN OPTION SCREEN
    Private mintCompanyId As Integer = 0
    'S.SANDEEP [14-APR-2017] -- START

    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Private mstrPrivilegIds As String = ""
    Private mblnIsManager As Boolean = False
    Private ePrivilegeCondition As enApplyPrivilegeCondition
    'S.SANDEEP [20-JUN-2018] -- END

#End Region

#Region " Display Dialog "

    'S.SANDEEP [14-APR-2017] -- START
    'ISSUE/ENHANCEMENT : 'ISSUE/ENHANCEMENT : GET USER BASED ON COMPANY ACCESS PRIVILEGE IN OPTION SCREEN
    Public Function displayDialog(ByRef strcsvUserIds As String, ByVal intCompanyId As Integer, Optional ByVal strPrivilegIds As String = "", Optional ByVal blnIsManage As Boolean = False, Optional ByVal eCondition As enApplyPrivilegeCondition = enApplyPrivilegeCondition.APPLY_IN) As Boolean 'S.SANDEEP [20-JUN-2018] -- START {Ref#244} -- END
        'Public Function displayDialog(ByRef strcsvUserIds As String, ByVal intCompanyId As Integer) As Boolean
        'Public Function displayDialog(ByRef strcsvUserIds As String) As Boolean
        'S.SANDEEP [14-APR-2017] -- END
        Try
            mstrcsvUserIds = strcsvUserIds
            mintCompanyId = intCompanyId
            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            mstrPrivilegIds = strPrivilegIds
            mblnIsManager = blnIsManage
            ePrivilegeCondition = eCondition
            'S.SANDEEP [20-JUN-2018] -- END
            Call FillNotificationUsers()
            Me.ShowDialog()
            strcsvUserIds = mstrcsvUserIds
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form Events "

    Private Sub frmERCUserSelection_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetCheckedUser(mstrcsvUserIds)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmERCUserSelection_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsMasterData.SetMessages()
            objfrm._Other_ModuleNames = "clsMasterData"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillNotificationUsers()
        Dim dsList As New DataSet
        Dim objUser As New clsUserAddEdit
        Try
            'S.SANDEEP [14-APR-2017] -- START
            'ISSUE/ENHANCEMENT : 'ISSUE/ENHANCEMENT : GET USER BASED ON COMPANY ACCESS PRIVILEGE IN OPTION SCREEN
            'dsList = objUser.GetList("List")

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'dsList = objUser.GetCompanyBasedUserList("List", mintCompanyId)

            'S.SANDEEP [07-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002513|ARUTI-325}
            'dsList = objUser.GetCompanyBasedUserList("List", mintCompanyId, "", mstrPrivilegIds, mblnIsManager)
            dsList = objUser.GetCompanyBasedUserList("List", mintCompanyId, "", mstrPrivilegIds, mblnIsManager, ePrivilegeCondition)
            'S.SANDEEP [07-SEP-2018] -- END

            'S.SANDEEP [20-JUN-2018] -- END

            'S.SANDEEP [14-APR-2017] -- END

            Dim dCol As New DataColumn
            dCol.ColumnName = "ischeck"
            dCol.DataType = GetType(Boolean)
            dCol.DefaultValue = False
            dsList.Tables("List").Columns.Add(dCol)
            mdtUsers = New DataView(dsList.Tables("List"), "email <> '' ", "username ASC", DataViewRowState.CurrentRows).ToTable.Copy

            mdUserView = mdtUsers.DefaultView
            dgvDatesUser.AutoGenerateColumns = False

            objdgcolhDCheck.DataPropertyName = "ischeck"
            dgcolhDUser.DataPropertyName = "username"
            objdgcolhDUserunkid.DataPropertyName = "userunkid"
            dgvDatesUser.DataSource = mdUserView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillNotificationUsers", mstrModuleName)
        Finally
            dsList.Dispose()
            objUser = Nothing
        End Try
    End Sub

    Private Sub SetCheckedUser(ByVal strcsvIds As String)
        Try
            If strcsvIds.Trim.Length > 0 Then
                mdtUsers.AcceptChanges()
                strcsvIds.ToString.Split(CChar(",")).ToList().ForEach(Function(x) Check_DataRow(CInt(x), mdtUsers))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckedUser", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Check_DataRow(ByVal intUserId As Integer, ByVal dtUser As DataTable) As Boolean
        Try
            Dim xrow() As DataRow = Nothing
            xrow = dtUser.Select("userunkid = '" & intUserId & "'")
            If xrow.Length > 0 Then
                xrow(0)("ischeck") = True
                dtUser.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Check_DataRow", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch1.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvDatesUser.Rows.Count > 0 Then
                        If dgvDatesUser.SelectedRows(0).Index = dgvDatesUser.Rows(dgvDatesUser.RowCount - 1).Index Then Exit Sub
                        dgvDatesUser.Rows(dgvDatesUser.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvDatesUser.Rows.Count > 0 Then
                        If dgvDatesUser.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvDatesUser.Rows(dgvDatesUser.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch1_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch1.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSearch1.Text.Trim.Length > 0 Then
                strSearch = "username LIKE '%" & txtSearch1.Text & "%' "
            End If
            mdUserView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch1_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub objlnkClose_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles objlnkClose.LinkClicked
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlnkClose_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSetUser_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetUser.LinkClicked
        Try
            mdtUsers.AcceptChanges()
            If mdtUsers.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1000, "Sorry, Please tick atleast one user to send notification."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mstrcsvUserIds = String.Join(",", mdtUsers.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("userunkid").ToString).ToArray)
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetUser_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkDCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkDCheck.CheckedChanged
        Try
            RemoveHandler dgvDatesUser.CellContentClick, AddressOf dgvDatesUser_CellContentClick
            For Each dRow As DataRowView In mdUserView
                dRow.Item("ischeck") = CBool(objchkDCheck.CheckState)
            Next
            AddHandler dgvDatesUser.CellContentClick, AddressOf dgvDatesUser_CellContentClick
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objchkDCheck_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvDatesUser_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDatesUser.CellContentClick, dgvDatesUser.CellContentDoubleClick
        Try
            RemoveHandler objchkDCheck.CheckedChanged, AddressOf objchkDCheck_CheckedChanged
            If e.ColumnIndex = objdgcolhDCheck.Index Then
                If Me.dgvDatesUser.IsCurrentCellDirty Then
                    Me.dgvDatesUser.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                mdtUsers.AcceptChanges()

                Dim drRow As DataRow() = mdtUsers.Select("ischeck = True")
                If drRow.Length > 0 Then
                    If mdtUsers.Rows.Count = drRow.Length Then
                        objchkDCheck.CheckState = CheckState.Checked
                    Else
                        objchkDCheck.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkDCheck.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkDCheck.CheckedChanged, AddressOf objchkDCheck_CheckedChanged
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "dgvDatesUser_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbUserSelection.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbUserSelection.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbUserSelection.Text = Language._Object.getCaption(Me.gbUserSelection.Name, Me.gbUserSelection.Text)
            Me.objlnkClose.Text = Language._Object.getCaption(Me.objlnkClose.Name, Me.objlnkClose.Text)
            Me.dgcolhDUser.HeaderText = Language._Object.getCaption(Me.dgcolhDUser.Name, Me.dgcolhDUser.HeaderText)
            Me.lnkSetUser.Text = Language._Object.getCaption(Me.lnkSetUser.Name, Me.lnkSetUser.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class