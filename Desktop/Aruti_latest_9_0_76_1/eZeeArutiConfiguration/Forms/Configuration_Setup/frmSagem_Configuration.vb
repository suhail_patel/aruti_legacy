﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

#End Region

Public Class frmSagem_Configuration

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSagem_Configuration"
    Private mblnCancel As Boolean = True
    Dim mstrDatabaseServer As String = ""
    Dim mstrDatabaseName As String = ""
    Dim mstrDatabaseUserName As String = ""
    Dim mstrDatabasePassword As String = ""
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef _DatabaseServer As String, ByRef _DatabaseName As String, ByRef _DatabaseUserName As String, ByRef _DatabasePassword As String) As Boolean
        Try
            mstrDatabaseServer = _DatabaseServer
            mstrDatabaseName = _DatabaseName
            mstrDatabaseUserName = _DatabaseUserName
            mstrDatabasePassword = _DatabasePassword
            Me.ShowDialog()
            _DatabaseServer = mstrDatabaseServer
            _DatabaseName = mstrDatabaseName
            _DatabaseUserName = mstrDatabaseUserName
            _DatabasePassword = mstrDatabasePassword
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmSagem_Configuration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSagem_Configuration_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Function Validation() As Boolean
        Dim mblnFlag As Boolean = False
        Try
            If txtSagemDbServer.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Database Server is compulsory information.Please define database server."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtSagemDbServer.Focus()
                Return False
            ElseIf txtSagemDBName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Database Name is compulsory information.Please define database name."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtSagemDBName.Focus()
                Return False
            ElseIf txtSagemDBUserName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Database User Name is compulsory information.Please define database user name."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtSagemDBUserName.Focus()
                Exit Function
            ElseIf txtSagemDBUserPwd.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Database User Password is compulsory information.Please define database user password."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtSagemDBUserPwd.Focus()
                Exit Function
            End If
            mblnFlag = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    Private Sub GetValue()
        Try
            txtSagemDbServer.Text = mstrDatabaseServer
            txtSagemDBName.Text = mstrDatabaseName
            txtSagemDBUserName.Text = mstrDatabaseUserName
            txtSagemDBUserPwd.Text = mstrDatabasePassword

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnTestConnection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestConnection.Click
        Try
            If Validation() = False Then Exit Sub
            Dim objConfig As New clsConfigOptions
            If objConfig.TestConnection_Sagem(txtSagemDbServer.Text.Trim, txtSagemDBName.Text.Trim, txtSagemDBUserName.Text.Trim, txtSagemDBUserPwd.Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Connection is done successfully."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Unable to connect the database server."), enMsgBoxStyle.Information)
            End If
            objConfig = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTestConnection_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If Validation() = False Then Exit Sub

            mstrDatabaseServer = txtSagemDbServer.Text.Trim
            mstrDatabaseName = txtSagemDBName.Text.Trim
            mstrDatabaseUserName = txtSagemDBUserName.Text.Trim
            mstrDatabasePassword = txtSagemDBUserPwd.Text

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnTestConnection.GradientBackColor = GUI._ButttonBackColor
            Me.btnTestConnection.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblSagemDBUserPwd.Text = Language._Object.getCaption(Me.lblSagemDBUserPwd.Name, Me.lblSagemDBUserPwd.Text)
            Me.lblSagemDBUserName.Text = Language._Object.getCaption(Me.lblSagemDBUserName.Name, Me.lblSagemDBUserName.Text)
            Me.lblSagemDBName.Text = Language._Object.getCaption(Me.lblSagemDBName.Name, Me.lblSagemDBName.Text)
            Me.lblSagemDbServer.Text = Language._Object.getCaption(Me.lblSagemDbServer.Name, Me.lblSagemDbServer.Text)
            Me.btnTestConnection.Text = Language._Object.getCaption(Me.btnTestConnection.Name, Me.btnTestConnection.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Database Server is compulsory information.Please define database server.")
            Language.setMessage(mstrModuleName, 2, "Database Name is compulsory information.Please define database name.")
            Language.setMessage(mstrModuleName, 3, "Database User Name is compulsory information.Please define database user name.")
            Language.setMessage(mstrModuleName, 4, "Database User Password is compulsory information.Please define database user password.")
            Language.setMessage(mstrModuleName, 5, "Connection is done successfully.")
            Language.setMessage(mstrModuleName, 6, "Unable to connect the database server.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class