﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMapUser_Role
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMapUser_Role))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhUser = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRole = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFileInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkFill = New System.Windows.Forms.LinkLabel
        Me.lblUserRole = New System.Windows.Forms.Label
        Me.lblUsername = New System.Windows.Forms.Label
        Me.cboUserRole = New System.Windows.Forms.ComboBox
        Me.cboUserName = New System.Windows.Forms.ComboBox
        Me.objbtnOpenFile = New eZee.Common.eZeeGradientButton
        Me.lnkAllocationFormat = New System.Windows.Forms.LinkLabel
        Me.txtFilePath = New System.Windows.Forms.TextBox
        Me.lblFileName = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.elMapping = New eZee.Common.eZeeLine
        Me.chkOverwriteNewRole = New System.Windows.Forms.CheckBox
        Me.pnlMain.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFileInfo.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.pnlData)
        Me.pnlMain.Controls.Add(Me.gbFileInfo)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(586, 501)
        Me.pnlMain.TabIndex = 0
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgData)
        Me.pnlData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(0, 113)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(586, 338)
        Me.pnlData.TabIndex = 22
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhUser, Me.dgcolhRole, Me.colhStatus, Me.colhMessage, Me.objcolhstatus})
        Me.dgData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgData.Location = New System.Drawing.Point(0, 0)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(586, 338)
        Me.dgData.TabIndex = 20
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 25
        '
        'colhUser
        '
        Me.colhUser.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhUser.HeaderText = "User"
        Me.colhUser.Name = "colhUser"
        Me.colhUser.ReadOnly = True
        Me.colhUser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRole
        '
        Me.dgcolhRole.HeaderText = "Role"
        Me.dgcolhRole.Name = "dgcolhRole"
        Me.dgcolhRole.ReadOnly = True
        Me.dgcolhRole.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRole.Width = 115
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhStatus.Width = 90
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhMessage.Width = 170
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'gbFileInfo
        '
        Me.gbFileInfo.BorderColor = System.Drawing.Color.Black
        Me.gbFileInfo.Checked = False
        Me.gbFileInfo.CollapseAllExceptThis = False
        Me.gbFileInfo.CollapsedHoverImage = Nothing
        Me.gbFileInfo.CollapsedNormalImage = Nothing
        Me.gbFileInfo.CollapsedPressedImage = Nothing
        Me.gbFileInfo.CollapseOnLoad = False
        Me.gbFileInfo.Controls.Add(Me.chkOverwriteNewRole)
        Me.gbFileInfo.Controls.Add(Me.elMapping)
        Me.gbFileInfo.Controls.Add(Me.lnkFill)
        Me.gbFileInfo.Controls.Add(Me.lblUserRole)
        Me.gbFileInfo.Controls.Add(Me.lblUsername)
        Me.gbFileInfo.Controls.Add(Me.cboUserRole)
        Me.gbFileInfo.Controls.Add(Me.cboUserName)
        Me.gbFileInfo.Controls.Add(Me.objbtnOpenFile)
        Me.gbFileInfo.Controls.Add(Me.lnkAllocationFormat)
        Me.gbFileInfo.Controls.Add(Me.txtFilePath)
        Me.gbFileInfo.Controls.Add(Me.lblFileName)
        Me.gbFileInfo.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbFileInfo.ExpandedHoverImage = Nothing
        Me.gbFileInfo.ExpandedNormalImage = Nothing
        Me.gbFileInfo.ExpandedPressedImage = Nothing
        Me.gbFileInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFileInfo.HeaderHeight = 25
        Me.gbFileInfo.HeaderMessage = ""
        Me.gbFileInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFileInfo.HeightOnCollapse = 0
        Me.gbFileInfo.LeftTextSpace = 0
        Me.gbFileInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbFileInfo.Name = "gbFileInfo"
        Me.gbFileInfo.OpenHeight = 300
        Me.gbFileInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFileInfo.ShowBorder = True
        Me.gbFileInfo.ShowCheckBox = False
        Me.gbFileInfo.ShowCollapseButton = False
        Me.gbFileInfo.ShowDefaultBorderColor = True
        Me.gbFileInfo.ShowDownButton = False
        Me.gbFileInfo.ShowHeader = True
        Me.gbFileInfo.Size = New System.Drawing.Size(586, 113)
        Me.gbFileInfo.TabIndex = 4
        Me.gbFileInfo.Temp = 0
        Me.gbFileInfo.Text = "Import File && Mapping Parameters"
        Me.gbFileInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkFill
        '
        Me.lnkFill.BackColor = System.Drawing.Color.Transparent
        Me.lnkFill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkFill.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkFill.Location = New System.Drawing.Point(481, 84)
        Me.lnkFill.Name = "lnkFill"
        Me.lnkFill.Size = New System.Drawing.Size(91, 17)
        Me.lnkFill.TabIndex = 249
        Me.lnkFill.TabStop = True
        Me.lnkFill.Text = "[ Fill List ]"
        Me.lnkFill.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblUserRole
        '
        Me.lblUserRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserRole.Location = New System.Drawing.Point(249, 84)
        Me.lblUserRole.Name = "lblUserRole"
        Me.lblUserRole.Size = New System.Drawing.Size(67, 17)
        Me.lblUserRole.TabIndex = 247
        Me.lblUserRole.Text = "User Role"
        Me.lblUserRole.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUsername
        '
        Me.lblUsername.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsername.Location = New System.Drawing.Point(10, 84)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(74, 17)
        Me.lblUsername.TabIndex = 246
        Me.lblUsername.Text = "Username"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUserRole
        '
        Me.cboUserRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUserRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUserRole.FormattingEnabled = True
        Me.cboUserRole.Location = New System.Drawing.Point(322, 82)
        Me.cboUserRole.Name = "cboUserRole"
        Me.cboUserRole.Size = New System.Drawing.Size(153, 21)
        Me.cboUserRole.TabIndex = 245
        '
        'cboUserName
        '
        Me.cboUserName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUserName.FormattingEnabled = True
        Me.cboUserName.Location = New System.Drawing.Point(90, 82)
        Me.cboUserName.Name = "cboUserName"
        Me.cboUserName.Size = New System.Drawing.Size(153, 21)
        Me.cboUserName.TabIndex = 244
        '
        'objbtnOpenFile
        '
        Me.objbtnOpenFile.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOpenFile.BorderSelected = False
        Me.objbtnOpenFile.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOpenFile.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mini_Search
        Me.objbtnOpenFile.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOpenFile.Location = New System.Drawing.Point(551, 33)
        Me.objbtnOpenFile.Name = "objbtnOpenFile"
        Me.objbtnOpenFile.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOpenFile.TabIndex = 243
        '
        'lnkAllocationFormat
        '
        Me.lnkAllocationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocationFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocationFormat.Location = New System.Drawing.Point(416, 4)
        Me.lnkAllocationFormat.Name = "lnkAllocationFormat"
        Me.lnkAllocationFormat.Size = New System.Drawing.Size(161, 17)
        Me.lnkAllocationFormat.TabIndex = 239
        Me.lnkAllocationFormat.TabStop = True
        Me.lnkAllocationFormat.Text = "Get Import Format"
        Me.lnkAllocationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.Location = New System.Drawing.Point(90, 33)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(455, 21)
        Me.txtFilePath.TabIndex = 3
        '
        'lblFileName
        '
        Me.lblFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(10, 35)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(74, 17)
        Me.lblFileName.TabIndex = 3
        Me.lblFileName.Text = "File Name"
        Me.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objlblProgress)
        Me.EZeeFooter1.Controls.Add(Me.btnImport)
        Me.EZeeFooter1.Controls.Add(Me.btnFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 451)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(586, 50)
        Me.EZeeFooter1.TabIndex = 1
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(116, 18)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(255, 15)
        Me.objlblProgress.TabIndex = 241
        Me.objlblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(377, 11)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(94, 30)
        Me.btnImport.TabIndex = 240
        Me.btnImport.Text = "Import"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 11)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(98, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 24
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        Me.tsmShowWarning.Visible = False
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(477, 11)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'elMapping
        '
        Me.elMapping.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elMapping.Location = New System.Drawing.Point(3, 60)
        Me.elMapping.Name = "elMapping"
        Me.elMapping.Size = New System.Drawing.Size(394, 16)
        Me.elMapping.TabIndex = 251
        Me.elMapping.Text = "Mapping Parameters"
        '
        'chkOverwriteNewRole
        '
        Me.chkOverwriteNewRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwriteNewRole.ForeColor = System.Drawing.Color.Red
        Me.chkOverwriteNewRole.Location = New System.Drawing.Point(403, 59)
        Me.chkOverwriteNewRole.Name = "chkOverwriteNewRole"
        Me.chkOverwriteNewRole.Size = New System.Drawing.Size(169, 17)
        Me.chkOverwriteNewRole.TabIndex = 252
        Me.chkOverwriteNewRole.Text = "Overwrite Existing Role"
        Me.chkOverwriteNewRole.UseVisualStyleBackColor = True
        '
        'frmMapUser_Role
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 501)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMapUser_Role"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Map User With Role"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFileInfo.ResumeLayout(False)
        Me.gbFileInfo.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents gbFileInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkAllocationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents txtFilePath As System.Windows.Forms.TextBox
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents objbtnOpenFile As eZee.Common.eZeeGradientButton
    Friend WithEvents cboUserRole As System.Windows.Forms.ComboBox
    Friend WithEvents cboUserName As System.Windows.Forms.ComboBox
    Friend WithEvents lblUserRole As System.Windows.Forms.Label
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRole As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lnkFill As System.Windows.Forms.LinkLabel
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents elMapping As eZee.Common.eZeeLine
    Friend WithEvents chkOverwriteNewRole As System.Windows.Forms.CheckBox
End Class
