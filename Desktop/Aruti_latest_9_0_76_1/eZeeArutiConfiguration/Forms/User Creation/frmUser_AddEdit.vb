﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Linq
#End Region

Public Class frmUser_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmUser_AddEdit"
    Private mblnCancel As Boolean = True
    Private objUserAddEdit As clsUserAddEdit
    Private menAction As enAction = enAction.ADD_ONE
    Private mintUserUnkid As Integer = -1
    'Sandeep [ 21Aug 2010 ] -- Start
    Private clrChk As Color
    Private clrUnChk As Color
    Private clrPartChk As Color
    Private isStartChk As Boolean = True
    'Sandeep [ 21 Aug 2010 ] -- End 

    'Sandeep [ 29 NOV 2010 ] -- Start
    Private mstrCompanyIds As String = String.Empty
    'Sandeep [ 29 NOV 2010 ] -- End 



    'Gajanan [18-OCT-2019] -- Start    
    'Enhancement:Provide Privileges "allowto Add/edit Opertional privileges".
    Dim objUserPrivilege As clsUserPrivilege
    'Gajanan [18-OCT-2019] -- End

    'Gajanan [19-OCT-2019] -- Start    
    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
    Dim mstrCompanyPrivilege As List(Of String)
    Dim mstrOperationalPrivilege As List(Of String)
    Dim mstrFinalRemovedOperationalPrivilege As String = String.Empty
    Dim mstrFinalNewlyAddedOperationalPrivilege As String = String.Empty
    Dim mstrFinalRemovedCompanyPrivilege As String = String.Empty
    Dim mstrFinalNewlyAddedCompanyPrivilege As String = String.Empty
    Dim mblnUserRoleChange As Boolean = False
    'Gajanan [18-OCT-2019] -- End
    'Sohail (31 Oct 2019) -- Start
    'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
    Private mstrOldRoleName As String = ""
    'Sohail (31 Oct 2019) -- End

    'Hemant (25 May 2021) -- Start
    'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
    Private mstrOldPassword As String = ""
    'Hemant (25 May 2021) -- End
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintUserUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintUserUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try

            clrChk = Color.Blue
            clrUnChk = tvOperationalPrivilege.ForeColor
            clrPartChk = Color.Brown

            txtUser_Name.BackColor = GUI.ColorComp
            txtPassword.BackColor = GUI.ColorOptional
            txtRetypePassword.BackColor = GUI.ColorOptional
            'Sandeep [ 10 FEB 2011 ] -- Start
            'nudDays.BackColor = GUI.ColorOptional
            'Sandeep [ 10 FEB 2011 ] -- End 
            cboUserRole.BackColor = GUI.ColorComp
            cboLanguage.BackColor = GUI.ColorComp

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtAddress1.BackColor = GUI.ColorOptional
            txtAddress2.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional
            txtFirstName.BackColor = GUI.ColorOptional
            txtLastName.BackColor = GUI.ColorOptional
            txtPhone.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 07 NOV 2011 ] -- END`

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtUser_Name.Text = objUserAddEdit._Username
        txtPassword.Text = objUserAddEdit._Password
        'Hemant (25 May 2021) -- Start
        'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
        mstrOldPassword = objUserAddEdit._Password
        'Hemant (25 May 2021) -- End
        txtRetypePassword.Text = objUserAddEdit._Password
        If objUserAddEdit._Ispasswordexpire Then
            radYes.Checked = True
        Else
            radNo.Checked = True
        End If

        'Sandeep [ 10 FEB 2011 ] -- Start
        'nudDays.Value = objUserAddEdit._Exp_Days


        'S.SANDEEP [ 30 May 2011 ] -- START
        'ISSUE : FINCA REQ.
        'If objUserAddEdit._Exp_Days.ToString.Length = 8 Then
        '    dtpExpirydate.Value = CDate(eZeeDate.convertDate(objUserAddEdit._Exp_Days.ToString).ToShortDateString)
        'End If
        'S.SANDEEP [ 30 May 2011 ] -- END 

        'Sandeep [ 10 FEB 2011 ] -- End 



        'Pinkal (12-Oct-2011) -- Start
        'Enhancement : TRA Changes
        'cboUserRole.SelectedValue = IIf(objUserAddEdit._Roleunkid = 0, 1, objUserAddEdit._Roleunkid)
        cboUserRole.SelectedValue = objUserAddEdit._Roleunkid
        'Pinkal (12-Oct-2011) -- End
        'Sohail (31 Oct 2019) -- Start
        'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
        'Sohail (01 Nov 2019) -- Start
        'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below.
        'mstrOldRoleName = cboUserRole.Text
        If CInt(cboUserRole.SelectedValue) > 0 Then
        mstrOldRoleName = cboUserRole.Text
        Else
            mstrOldRoleName = ""
        End If
        'Sohail (01 Nov 2019) -- End
        'Sohail (31 Oct 2019) -- End

        cboLanguage.SelectedIndex = objUserAddEdit._Languageunkid

        'Sandeep | 17 JAN 2011 | -- START
        chkRightToLeft.Checked = CBool(objUserAddEdit._Isrighttoleft)
        'Sandeep | 17 JAN 2011 | -- END 

        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        txtAddress1.Text = objUserAddEdit._Address1
        txtAddress2.Text = objUserAddEdit._Address2
        txtEmail.Text = objUserAddEdit._Email
        txtFirstName.Text = objUserAddEdit._Firstname
        txtLastName.Text = objUserAddEdit._Lastname
        txtPhone.Text = objUserAddEdit._Phone
        'S.SANDEEP [ 07 NOV 2011 ] -- END


        'Pinkal (21-Jun-2012) -- Start
        'Enhancement : TRA Changes
        If objUserAddEdit._Userunkid = 1 Then
            chkIsManager.Checked = True
            chkIsManager.Enabled = False
        Else
            chkIsManager.Enabled = True
            chkIsManager.Checked = objUserAddEdit._IsManager
        End If

        'Pinkal (21-Jun-2012) -- End

        'Sohail (04 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        If objUserAddEdit._Relogin_Date <> Nothing AndAlso objUserAddEdit._Relogin_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
            dtpReloginDate.Value = objUserAddEdit._Relogin_Date
            dtpReloginDate.Checked = True
            dtpReloginDate.Enabled = True
        Else
            dtpReloginDate.Checked = False
            dtpReloginDate.Enabled = False
        End If
        'Sohail (04 Jul 2012) -- End

    End Sub

    Private Sub SetValue()
        objUserAddEdit._Username = txtUser_Name.Text
        objUserAddEdit._Password = txtPassword.Text

        'S.SANDEEP [ 23 NOV 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objUserAddEdit._Isactive = chkActiveUser.Checked
        'S.SANDEEP [ 23 NOV 2012 ] -- END

        If radYes.Checked Then
            objUserAddEdit._Ispasswordexpire = True
        Else
            objUserAddEdit._Ispasswordexpire = False
        End If

        'Sandeep [ 10 FEB 2011 ] -- Start
        'objUserAddEdit._Exp_Days = CInt(nudDays.Value)
        If radYes.Checked = True Then
            objUserAddEdit._Exp_Days = CInt(eZeeDate.convertDate(dtpExpirydate.Value))
        Else
            objUserAddEdit._Exp_Days = 0
        End If


        'Gajanan [19-OCT-2019] -- Start    
        'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
        If objUserAddEdit._Roleunkid <> CInt(cboUserRole.SelectedValue) Then
            mblnUserRoleChange = True
        End If
        'Gajanan [19-OCT-2019] -- End


        'Sandeep [ 10 FEB 2011 ] -- End 
        objUserAddEdit._Roleunkid = CInt(cboUserRole.SelectedValue)
        objUserAddEdit._Languageunkid = CInt(cboLanguage.SelectedIndex)
        'objUserAddEdit._CompanyUnkid = CInt(cboCompany.SelectedValue)
        'objUserAddEdit._YearId = CInt(cboSelectYear.SelectedValue)
        Dim i As Integer
        Dim j As Integer
        Dim strPrivilege As String = ""

        'S.SANDEEP [ 10 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'For i = 0 To tvOperationalPrivilege.Nodes.Count - 1
        '    For j = 0 To tvOperationalPrivilege.Nodes(i).Nodes.Count - 1
        '        If tvOperationalPrivilege.Nodes(i).Nodes(j).Checked = True Then
        '            strPrivilege += "," & tvOperationalPrivilege.Nodes(i).Nodes(j).Name.ToString
        '        End If
        '    Next
        'Next

        For i = 0 To tvOperationalPrivilege.Nodes.Count - 1
            For j = 0 To tvOperationalPrivilege.Nodes(i).Nodes.Count - 1
                If tvOperationalPrivilege.Nodes(i).Nodes(j).ForeColor = Color.Blue Then
                    strPrivilege += "," & tvOperationalPrivilege.Nodes(i).Nodes(j).Name.ToString
                End If
            Next
        Next
        'S.SANDEEP [ 10 JULY 2012 ] -- END
        strPrivilege = Mid(strPrivilege, 2)
        objUserAddEdit._AssignPrivilegeIDs = strPrivilege

        'Gajanan [19-OCT-2019] -- Start    
        'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
        If strPrivilege.Length > 0 Then
            Dim mstrNewOperationalPrivilege As List(Of String) = strPrivilege.Split(","c).ToList()

            If IsNothing(mstrOperationalPrivilege) = False AndAlso mstrOperationalPrivilege.Count > 0 Then
                mstrFinalNewlyAddedOperationalPrivilege = String.Join(",", mstrNewOperationalPrivilege.Except(mstrOperationalPrivilege).ToArray())
                mstrFinalRemovedOperationalPrivilege = String.Join(",", mstrOperationalPrivilege.Except(mstrNewOperationalPrivilege).ToArray())
            Else
                mstrFinalNewlyAddedOperationalPrivilege = String.Join(",", mstrNewOperationalPrivilege.ToArray())
            End If
        End If
        'Gajanan [19-OCT-2019] -- End

        Dim strCompAccess As String = ""

        'S.SANDEEP [ 10 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'For i = 0 To tvCompanyPrivilege.Nodes.Count - 1
        '    For j = 0 To tvCompanyPrivilege.Nodes(i).Nodes.Count - 1
        '        If tvCompanyPrivilege.Nodes(i).Nodes(j).Checked = True Then
        '            strCompAccess += "," & tvCompanyPrivilege.Nodes(i).Nodes(j).Name.ToString
        '        End If
        '    Next
        'Next


        'Gajanan [19-OCT-2019] -- Start    
        'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
        Dim AssignCompany As New List(Of String)
        'Gajanan [19-OCT-2019] -- End

        For i = 0 To tvCompanyPrivilege.Nodes.Count - 1
            For j = 0 To tvCompanyPrivilege.Nodes(i).Nodes.Count - 1
                If tvCompanyPrivilege.Nodes(i).Nodes(j).ForeColor = Color.Blue Then
                    strCompAccess += "," & tvCompanyPrivilege.Nodes(i).Nodes(j).Name.ToString

                    'Gajanan [19-OCT-2019] -- Start    
                    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                    If AssignCompany.Contains(tvCompanyPrivilege.Nodes(i).Name) = False Then
                        AssignCompany.Add(tvCompanyPrivilege.Nodes(i).Name)
                    End If
                    'Gajanan [19-OCT-2019] -- End

                End If
            Next
        Next
        'S.SANDEEP [ 10 JULY 2012 ] -- END

        strCompAccess = Mid(strCompAccess, 2)
        objUserAddEdit._AssignCompanyPrivilegeIDs = strCompAccess

        'Gajanan [19-OCT-2019] -- Start    
        'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
        If IsNothing(AssignCompany) = False AndAlso AssignCompany.Count > 0 Then
            Dim mstrNewCompanyPrivilege As List(Of String) = String.Join(",", AssignCompany.ToArray()).Split(","c).ToList()

            If IsNothing(mstrCompanyPrivilege) = False AndAlso mstrCompanyPrivilege.Count > 0 Then
                mstrFinalNewlyAddedCompanyPrivilege = String.Join(",", mstrNewCompanyPrivilege.Except(mstrCompanyPrivilege).ToArray())
                mstrFinalRemovedCompanyPrivilege = String.Join(",", mstrCompanyPrivilege.Except(mstrNewCompanyPrivilege).ToArray())
            Else
                mstrFinalNewlyAddedCompanyPrivilege = String.Join(",", mstrNewCompanyPrivilege.ToArray())
            End If

        End If
        'Gajanan [19-OCT-2019] -- End




        'Dim strUserAccess As String = ""
        'Dim strUserAccessLevel As String = ""
        'For i = 0 To tvAccessLevel.Nodes.Count - 1
        '    For j = 0 To tvAccessLevel.Nodes(i).Nodes.Count - 1
        '        If tvAccessLevel.Nodes(i).Nodes(j).Checked = True Then
        '            strUserAccess += "," & tvAccessLevel.Nodes(i).Nodes(j).Name.ToString
        '            strUserAccessLevel += "," & tvAccessLevel.Nodes(i).Nodes(j).Text.ToString
        '        End If
        '    Next
        'Next
        'strUserAccess = Mid(strUserAccess, 2)
        'strUserAccessLevel = Mid(strUserAccessLevel, 2)

        'objUserAddEdit._AssignAccessPrivilegeIDs = strUserAccess
        'objUserAddEdit._UserAccessLevels = strUserAccessLevel

        'Sandeep | 17 JAN 2011 | -- START
        objUserAddEdit._Isrighttoleft = chkRightToLeft.Checked
        'Sandeep | 17 JAN 2011 | -- END 

        'S.SANDEEP [ 30 May 2011 ] -- START
        'ISSUE : FINCA REQ.
        If menAction = enAction.ADD_CONTINUE Then
            objUserAddEdit._Creation_Date = ConfigParameter._Object._CurrentDateAndTime
            objUserAddEdit._CreatedById = User._Object._Userunkid
        End If
        'S.SANDEEP [ 30 May 2011 ] -- END 

        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objUserAddEdit._Address1 = txtAddress1.Text
        objUserAddEdit._Address2 = txtAddress2.Text
        objUserAddEdit._Email = txtEmail.Text
        objUserAddEdit._Firstname = txtFirstName.Text
        objUserAddEdit._Lastname = txtLastName.Text
        objUserAddEdit._Phone = txtPhone.Text
        'S.SANDEEP [ 07 NOV 2011 ] -- END


        'Pinkal (21-Jun-2012) -- Start
        'Enhancement : TRA Changes
        objUserAddEdit._IsManager = chkIsManager.Checked
        'Pinkal (21-Jun-2012) -- End

        'Sohail (04 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        If dtpReloginDate.Checked = True AndAlso dtpReloginDate.Value.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
            objUserAddEdit._Relogin_Date = dtpReloginDate.Value.Date
        Else
            objUserAddEdit._Relogin_Date = Nothing
        End If
        'Sohail (04 Jul 2012) -- End
        If mintUserUnkid = 1 Then
            objUserAddEdit._Exp_Days = 0
        End If

        ''Varsha (10 Nov 2017) -- Start
        ''Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
        Dim objMasterData As New clsMasterData
        objUserAddEdit._Theme_Id = objMasterData.GetDefaultThemeId(0, Nothing)
        ''Varsha (10 Nov 2017)) -- End

        'Hemant (25 May 2021) -- Start
        'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
        objUserAddEdit._OldPassword = mstrOldPassword
        objUserAddEdit._ChangedUserunkid = User._Object._Userunkid
        objUserAddEdit._Ip = getIP()
        objUserAddEdit._Hostname = getHostName()
        objUserAddEdit._Isweb = False
        'Hemant (25 May 2021) -- End

    End Sub

    Private Function IsValidate() As Boolean
        Try
            If Trim(txtUser_Name.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Username cannot be blank. Username is a mandatory information."), enMsgBoxStyle.Information)
                txtUser_Name.Focus()
                Return False
            End If

            'S.SANDEEP [ 01 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim mRegx As System.Text.RegularExpressions.Regex

            'Anjan (21 Dec 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            'mRegx = New System.Text.RegularExpressions.Regex("^[a-zA-Z0-9 ]*$")
            'Issue: This has been removed now due IDAMS (Oracle), so it has to be allowed -> andrew muga.
            'mRegx = New System.Text.RegularExpressions.Regex("^.*[\%'*'+?\\<>:].*$")
            'If mRegx.IsMatch(txtUser_Name.Text.Trim) = True Then
            '    'Anjan (21 Dec 2012)-End 
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "User Name cannot contain any special characters."), enMsgBoxStyle.Information)
            '    txtUser_Name.Focus()
            '    Return False
            'End If
            'S.SANDEEP [ 01 NOV 2012 ] -- END

            If Trim(txtPassword.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Password cannot be blank. Password is required information."), enMsgBoxStyle.Information)
                txtPassword.Focus()
                Return False
            End If

            If Trim(txtRetypePassword.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Verify Password cannot be blank. Verify Password is required information."), enMsgBoxStyle.Information)
                txtRetypePassword.Focus()
                Return False
            End If

            If CInt(cboUserRole.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "User Role is compulsory information. Please select User Role."), enMsgBoxStyle.Information)
                cboUserRole.Focus()
                Return False
            End If


            'S.SANDEEP [ 30 May 2011 ] -- START
            If mintUserUnkid <> User._Object._Userunkid Or mintUserUnkid <> 1 Then
                If menAction = enAction.ADD_CONTINUE Then
                    Dim objPswd As New clsPassowdOptions
                    If objPswd._IsPasswordLenghtSet Then
                        If txtPassword.Text.Trim.Length < objPswd._PasswordLength Then
                            'S.SANDEEP [ 05 NOV 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password length cannot be less than ") & User._Object.PWDOptions._PasswordLength & " " & Language.getMessage(mstrModuleName, 3, "character(s)"), enMsgBoxStyle.Information)
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password length cannot be less than ") & objPswd._PasswordLength & " " & Language.getMessage(mstrModuleName, 3, "character(s)"), enMsgBoxStyle.Information)
                            'S.SANDEEP [ 05 NOV 2012 ] -- END
                            txtPassword.Focus()
                            Return False
                        End If
                    End If
                    objPswd = Nothing
                ElseIf menAction = enAction.EDIT_ONE Then
                    Dim objPswd As New clsPassowdOptions
                    If objPswd._IsPasswordLenghtSet Then
                        If txtPassword.Text.Trim.Length < objPswd._PasswordLength Then
                            'S.SANDEEP [ 05 NOV 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password length cannot be less than ") & User._Object.PWDOptions._PasswordLength & " " & Language.getMessage(mstrModuleName, 3, "character(s)"), enMsgBoxStyle.Information)
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password length cannot be less than ") & objPswd._PasswordLength & " " & Language.getMessage(mstrModuleName, 3, "character(s)"), enMsgBoxStyle.Information)
                            'S.SANDEEP [ 05 NOV 2012 ] -- END
                            txtPassword.Focus()
                            Return False
                        End If
                    End If
                    objPswd = Nothing
                End If
                If radYes.Checked = True Then
                    If menAction = enAction.ADD_CONTINUE Then
                        Dim objPswd As New clsPassowdOptions
                        If objPswd._IsPasswordExpiredSet Then
                            If objPswd._PasswordMaxLen > 0 Then
                                Dim dtExDate As Date = ConfigParameter._Object._CurrentDateAndTime
                                dtExDate = dtExDate.AddDays(objPswd._PasswordMaxLen)
                                If dtpExpirydate.Value.Date < dtExDate.Date Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Expiry date cannot be less than the maximum password expiration days."), enMsgBoxStyle.Information)
                                    dtpExpirydate.Focus()
                                    Return False
                                End If
                            End If
                        End If
                        objPswd = Nothing
                    ElseIf menAction = enAction.EDIT_ONE Then
                        Dim objPswd As New clsPassowdOptions
                        If objPswd._IsPasswordExpiredSet Then
                            Dim dtExDate As Date = objUserAddEdit._Creation_Date
                            dtExDate = dtExDate.AddDays(objPswd._PasswordMaxLen)
                            If dtpExpirydate.Value.Date < dtExDate.Date Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Expiry date cannot be less than the maximum password expiration days."), enMsgBoxStyle.Information)
                                dtpExpirydate.Focus()
                                Return False
                            End If
                        End If
                        objPswd = Nothing
                    End If
                End If
                'S.SANDEEP [ 01 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If menAction = enAction.ADD_CONTINUE Or menAction = enAction.EDIT_ONE Then
                    Dim objPswd As New clsPassowdOptions
                    If objPswd._IsUsrNameLengthSet Then

                        'S.SANDEEP [ 14 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES

                        'S.SANDEEP [05-Mar-2018] -- START
                        'ISSUE/ENHANCEMENT : {#ARUTI-6}
                        'If objPswd._IsEmployeeAsUser = False Then
                        '    If txtUser_Name.Text.Trim.Length < objPswd._UsrNameLength Then
                        '        'S.SANDEEP [ 05 NOV 2012 ] -- START
                        '        'ENHANCEMENT : TRA CHANGES
                        '        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Username length cannot be less than ") & User._Object.PWDOptions._UsrNameLength & " " & Language.getMessage(mstrModuleName, 3, "character(s)"), enMsgBoxStyle.Information)
                        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Username length cannot be less than ") & objPswd._UsrNameLength & " " & Language.getMessage(mstrModuleName, 3, "character(s)"), enMsgBoxStyle.Information)
                        '        'S.SANDEEP [ 05 NOV 2012 ] -- END
                        '        txtUser_Name.Focus()
                        '        Return False
                        '    End If
                        'End If
                            If txtUser_Name.Text.Trim.Length < objPswd._UsrNameLength Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Username length cannot be less than ") & objPswd._UsrNameLength & " " & Language.getMessage(mstrModuleName, 3, "character(s)"), enMsgBoxStyle.Information)
                                txtUser_Name.Focus()
                                Return False
                            End If
                        'S.SANDEEP [05-Mar-2018] -- END


                        'S.SANDEEP [ 14 DEC 2012 ] -- END
                    End If
                End If
                'S.SANDEEP [ 01 NOV 2012 ] -- END
            End If
            'S.SANDEEP [ 30 May 2011 ] -- END 


            'S.SANDEEP [ 01 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If txtPassword.Text.Trim.Length > 0 Then
                Dim objPswd As New clsPassowdOptions
                If objPswd._IsPasswdPolicySet Then
                    If objPswd._IsUpperCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
                        If mRegx.IsMatch(txtPassword.Text.Trim) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Password must contain atleast one upper case character."), enMsgBoxStyle.Information)
                            txtPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsLowerCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
                        If mRegx.IsMatch(txtPassword.Text.Trim) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Password must contain atleast one lower case character."), enMsgBoxStyle.Information)
                            txtPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsNumeric_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
                        If mRegx.IsMatch(txtPassword.Text.Trim) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Password must contain atleast one numeric digit."), enMsgBoxStyle.Information)
                            txtPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsSpecalChars_Mandatory Then
                        'S.SANDEEP [ 07 MAY 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'mRegx = New System.Text.RegularExpressions.Regex("^.*[\[\]^$.|?*+()\\~`!@#%&\-_+={}'&quot;&lt;&gt;:;,\ ].*$")
                        mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
                        'S.SANDEEP [ 07 MAY 2013 ] -- END
                        If mRegx.IsMatch(txtPassword.Text.Trim) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Password must contain atleast one special character."), enMsgBoxStyle.Information)
                            txtPassword.Focus()
                            Return False
                        End If
                    End If

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    If objPswd._IsPasswordComplexity AndAlso txtUser_Name.Text.Trim.Length > 0 Then
                        If System.Text.RegularExpressions.Regex.IsMatch(txtPassword.Text.Trim, "(" & txtUser_Name.Text.Trim & ")", System.Text.RegularExpressions.RegexOptions.IgnoreCase) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Password cannot be set for") & " " & txtUser_Name.Text.Trim() & " ." & Language.getMessage(mstrModuleName, 20, "This Password does not meet the password complexity requirement as per the password policy."), enMsgBoxStyle.Information)
                            txtPassword.Focus()
                            Return False
                        End If
                    End If
                    'Pinkal (18-Aug-2018) -- End

                End If
            End If
            'S.SANDEEP [ 01 NOV 2012 ] -- END


            If txtPassword.Text <> txtRetypePassword.Text Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Password does not match. Please type correct password."), enMsgBoxStyle.Information)
                txtRetypePassword.Focus()
                Return False
            End If

            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If dtpReloginDate.Checked = True AndAlso dtpReloginDate.Value.Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry! Relogin date should be greater than current date."), enMsgBoxStyle.Information)
                dtpReloginDate.Focus()
                Return False
            End If
            'Sohail (04 Jul 2012) -- End

            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            If menAction = enAction.EDIT_ONE AndAlso txtPassword.Text.Trim.Length > 0 AndAlso mstrOldPassword <> txtPassword.Text.Trim Then
                Dim objPswd As New clsPassowdOptions
                If objPswd._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION AndAlso objPswd._IsPasswordHistory AndAlso _
                   (objPswd._PasswdLastUsedNumber > 0 OrElse objPswd._PasswdLastUsedDays > 0) Then
                    Dim objPasswordHistory As New clsPasswordHistory
                    Dim intTopCount As Integer
                    Dim strFilter As String = String.Empty
                    Dim strOrderBy As String = String.Empty
                    If objPswd._PasswdLastUsedNumber > 0 Then
                        intTopCount = objPswd._PasswdLastUsedNumber
                    End If

                    If objPswd._PasswdLastUsedDays > 0 Then
                        Dim dtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
                        Dim dtStartDate As Date = dtEndDate.AddDays(-1 * objPswd._PasswdLastUsedDays)
                        strFilter &= "AND Convert(char(8),transactiondate,112) >= '" & eZeeDate.convertDate(dtStartDate) & "' AND Convert(char(8),transactiondate,112) <= '" & eZeeDate.convertDate(dtEndDate) & "' "
                    End If

                    strOrderBy = " order by transactiondate desc "
                    Dim dsList As DataSet = objPasswordHistory.GetFilterDataList("List", mintUserUnkid, -1, -1, intTopCount, strFilter, strOrderBy)
                    If dsList.Tables(0).Rows.Count > 0 Then
                        Dim drPassword() As DataRow = dsList.Tables(0).Select("new_password = '" & clsSecurity.Encrypt(txtPassword.Text, "ezee").ToString & "' ")
                        If drPassword.Length > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Entered New Password is already Used. Please Enter another Password!!!"), enMsgBoxStyle.Information)
                            txtPassword.Focus()
                            Exit Function
                        End If
                    End If
                    objPasswordHistory = Nothing
                End If
            End If
            'Hemant (25 May 2021) -- End

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            ' SET LANGUAGE FOR LOGIN PAGE OF SELF SERVICE AS PER NMB REQUIREMENT.
            Dim strMessage As String = ""
            strMessage = Language.getMessage(mstrModuleName, 21, "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator.")
            strMessage = Language.getMessage(mstrModuleName, 22, "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator.")
            strMessage = Language.getMessage(mstrModuleName, 23, "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator.")
            strMessage = Language.getMessage(mstrModuleName, 24, "Sorry, you cannot login. Reason : You are retired. Please contact Administrator.")
            strMessage = ""
            'Pinkal (25-May-2019) -- End



            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objRole As New clsUserRole_Master
        Dim objCompany As New clsCompany_Master
        Try
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombos = objRole.getComboList("Role", False)
            'With cboUserRole
            '    .ValueMember = "roleunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Role")
            'End With

            RemoveHandler cboUserRole.SelectedIndexChanged, AddressOf cboUserRole_SelectedIndexChanged


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            'dsCombos = objRole.getComboList("Role", False)
            dsCombos = objRole.getComboList("Role", True)

            'Pinkal (12-Oct-2011) -- End

            With cboUserRole
                .ValueMember = "roleunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Role")
            End With
            AddHandler cboUserRole.SelectedIndexChanged, AddressOf cboUserRole_SelectedIndexChanged
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            cboLanguage.Items.Clear()
            With cboLanguage
                .Items.Add(Language.getMessage("Language", 5, "Default English"))
                .Items.Add(Language.getMessage("Language", 6, "Custom 1"))
                .Items.Add(Language.getMessage("Language", 7, "Custom 2"))
            End With

            'dsCombos = objCompany.GetList("Company", True)
            'With cboCompany
            '    .ValueMember = "companyunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Company")
            'End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objRole = Nothing
            objCompany = Nothing
        End Try
    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub FillTreeView()


    'Gajanan [19-OCT-2019] -- Start    
    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
    'Private Sub FillTreeView(ByVal blnfillOperational As Boolean, ByVal blnfillCompanyAccess As Boolean)
    Private Sub FillTreeView(ByVal blnfillOperational As Boolean, ByVal blnfillCompanyAccess As Boolean, Optional ByVal isFromload As Boolean = False)
        'Gajanan [19-OCT-2019] -- End

        Dim dsPrivilegeList As DataSet = Nothing
        Dim strOldPrivilegeGroupName As String = ""
        Dim i As Integer = 0
        Dim nGroupNode As New TreeNode
        Dim nCurrentNode As New TreeNode
        Try
            '---------------- Operational Privelege
            If blnfillOperational Then
                tvOperationalPrivilege.Nodes.Clear()
                'S.SANDEEP [ 04 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsPrivilegeList = objUserAddEdit.getPrivilegeList(CInt(cboUserRole.SelectedValue), mintUserUnkid)
                If objUserAddEdit._Roleunkid <> CInt(cboUserRole.SelectedValue) Then
                    dsPrivilegeList = objUserAddEdit.getPrivilegeList(CInt(cboUserRole.SelectedValue), -1)
                Else
                    dsPrivilegeList = objUserAddEdit.getPrivilegeList(CInt(cboUserRole.SelectedValue), mintUserUnkid)
                End If
                'S.SANDEEP [ 04 FEB 2012 ] -- END
                If dsPrivilegeList.Tables.Count > 0 Then
                    For i = 0 To dsPrivilegeList.Tables(0).Rows.Count - 1
                        If strOldPrivilegeGroupName <> dsPrivilegeList.Tables(0).Rows(i).Item("group_name").ToString Then
                            nGroupNode = New TreeNode
                            nGroupNode = tvOperationalPrivilege.Nodes.Add(CStr(dsPrivilegeList.Tables(0).Rows(i).Item("privilegegroupunkid")), CStr(dsPrivilegeList.Tables(0).Rows(i).Item("group_name")))
                            strOldPrivilegeGroupName = CStr(dsPrivilegeList.Tables(0).Rows(i).Item("group_name"))
                        End If
                        nCurrentNode = nGroupNode.Nodes.Add(CStr(dsPrivilegeList.Tables(0).Rows(i).Item("privilegeunkid")), CStr(dsPrivilegeList.Tables(0).Rows(i).Item("privilege_name")))
                        nCurrentNode.Checked = CBool(IIf(CInt(dsPrivilegeList.Tables(0).Rows(i).Item("assign")) = 1, True, False))
                    Next
                    If tvOperationalPrivilege.Nodes.Count > 0 Then
                        tvOperationalPrivilege.TopNode = tvOperationalPrivilege.Nodes(0)
                    End If

                    'Gajanan [19-OCT-2019] -- Start    
                    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                    If isFromload = True AndAlso menAction = enAction.EDIT_ONE Then
                        mstrOperationalPrivilege = dsPrivilegeList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("assign") = 1).Select(Function(x) x.Field(Of Integer)("privilegeunkid").ToString()).ToList()
                    End If
                    'Gajanan [18-OCT-2019] -- End
                End If
            End If

            '---------------- Company Access Privelege
            If blnfillCompanyAccess Then
                strOldPrivilegeGroupName = ""
                i = 0
                nGroupNode = New TreeNode
                nCurrentNode = New TreeNode

                tvCompanyPrivilege.Nodes.Clear()
                dsPrivilegeList = objUserAddEdit.getCompanyPrivilegeList(mintUserUnkid)
                    'Gajanan [19-OCT-2019] -- Start    
                    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                mstrCompanyIds = String.Join(",", dsPrivilegeList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("companyunkid").ToString()).Distinct().ToArray())
                    'Gajanan [19-OCT-2019] -- End


                dsPrivilegeList = objUserAddEdit.getCompanyPrivilegeList(mintUserUnkid)
                If dsPrivilegeList.Tables.Count > 0 Then
                    For i = 0 To dsPrivilegeList.Tables(0).Rows.Count - 1
                        If strOldPrivilegeGroupName <> dsPrivilegeList.Tables(0).Rows(i).Item("group_name").ToString Then
                            nGroupNode = New TreeNode
                            nGroupNode = tvCompanyPrivilege.Nodes.Add(CStr(dsPrivilegeList.Tables(0).Rows(i).Item("privilegegroupunkid")), CStr(dsPrivilegeList.Tables(0).Rows(i).Item("group_name")))
                            strOldPrivilegeGroupName = CStr(dsPrivilegeList.Tables(0).Rows(i).Item("group_name"))
                        End If
                        nCurrentNode = nGroupNode.Nodes.Add(CStr(dsPrivilegeList.Tables(0).Rows(i).Item("privilegeunkid")), CStr(dsPrivilegeList.Tables(0).Rows(i).Item("privilege_name")))
                        nCurrentNode.Checked = CBool(IIf(CInt(dsPrivilegeList.Tables(0).Rows(i).Item("assign")) = 1, True, False))
                    Next
                    If tvCompanyPrivilege.Nodes.Count > 0 Then
                        tvCompanyPrivilege.TopNode = tvCompanyPrivilege.Nodes(0)
                    End If

                    'Gajanan [19-OCT-2019] -- Start    
                    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                    If isFromload = True AndAlso menAction = enAction.EDIT_ONE Then
                        mstrCompanyPrivilege = dsPrivilegeList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("assign") = 1).Select(Function(x) x.Field(Of Integer)("privilegegroupunkid").ToString()).ToList()
                End If
                    'Gajanan [19-OCT-2019] -- End

                End If





            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillTreeView", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    Private Sub ProcessStateChange(ByVal nodeCurrent As TreeNode)
        Try
            CheckChildren(nodeCurrent)
            CheckParent(nodeCurrent)
        Catch ex As System.Exception
            Call DisplayError.Show(CStr(-1), ex.Message, "ProcessStateChange", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckParent(ByVal nodeSource As TreeNode)

        If nodeSource.Parent Is Nothing Then Exit Sub

        Dim boolChecked As Boolean = False
        Dim boolUnChecked As Boolean = False
        Dim CurrentNode As TreeNode = nodeSource.Parent

        Try
            For Each ChieldNode As TreeNode In CurrentNode.Nodes
                Select Case ChieldNode.ForeColor
                    Case clrChk
                        boolChecked = True
                    Case clrPartChk
                        boolChecked = True
                        boolUnChecked = True
                    Case Else
                        boolUnChecked = True
                End Select
                If boolChecked And boolUnChecked Then Exit For
            Next

            If boolChecked And boolUnChecked Then
                CurrentNode.Checked = False
                CurrentNode.ForeColor = clrPartChk
            ElseIf boolChecked Then
                CurrentNode.Checked = True
                CurrentNode.ForeColor = clrChk
            Else
                CurrentNode.Checked = False
                CurrentNode.ForeColor = clrUnChk
            End If

            'Recursion Process
            Call CheckParent(CurrentNode)

        Catch ex As System.Exception
            Call DisplayError.Show(CStr(-1), ex.Message, "CheckParent", mstrModuleName)
        Finally
            CurrentNode = Nothing
        End Try
    End Sub

    Private Sub CheckChildren(ByVal nodeSource As TreeNode)
        If nodeSource.Nodes Is Nothing Then Exit Sub

        Try
            For Each nNode As TreeNode In nodeSource.Nodes
                nNode.Checked = nodeSource.Checked

                If nodeSource.Checked Then
                    nNode.ForeColor = clrChk
                Else
                    nNode.ForeColor = clrUnChk
                End If
                'Recursion Process
                Call CheckChildren(nNode)
            Next

        Catch ex As System.Exception
            Call DisplayError.Show(CStr(-1), ex.Message, "CheckChildren", mstrModuleName)
        End Try
    End Sub

    'Private Sub FillUserAccessTreeView(ByVal intCompanyId As Integer, ByVal intUserId As Integer, ByVal intYearId As Integer)
    '    Try
    '        Dim dsList As New DataSet
    '        dsList = objUserAddEdit.GetCompanyJobsList(intCompanyId, intUserId, intYearId)
    '        Dim strOldPrivilegeGroupName As String = ""
    '        Dim i As Integer = 0
    '        Dim nGroupNode As New TreeNode
    '        Dim nCurrentNode As New TreeNode
    '        tvAccessLevel.Nodes.Clear()
    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            For i = 0 To dsList.Tables(0).Rows.Count - 1
    '                If strOldPrivilegeGroupName <> dsList.Tables(0).Rows(i).Item("job_name").ToString Then
    '                    nGroupNode = New TreeNode
    '                    nGroupNode = tvAccessLevel.Nodes.Add(CStr(dsList.Tables(0).Rows(i).Item("jobunkid")), CStr(dsList.Tables(0).Rows(i).Item("job_name")))
    '                    strOldPrivilegeGroupName = CStr(dsList.Tables(0).Rows(i).Item("job_name"))
    '                End If
    '                nCurrentNode = nGroupNode.Nodes.Add(CStr(dsList.Tables(0).Rows(i).Item("jobunkid")), CStr(dsList.Tables(0).Rows(i).Item("job_level")))
    '                nCurrentNode.Checked = CBool(IIf(CInt(dsList.Tables(0).Rows(i).Item("assign")) = 1, True, False))
    '            Next
    '        End If
    '        If tvAccessLevel.Nodes.Count > 0 Then
    '            tvAccessLevel.TopNode = tvAccessLevel.Nodes(0)
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillUserAccessTreeView", mstrModuleName)
    '    End Try
    'End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 

#End Region

#Region " Form's Events "

    Private Sub frmUser_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objUserAddEdit = Nothing
    End Sub

    Private Sub frmUser_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUser_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUser_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUser_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUser_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUserAddEdit = New clsUserAddEdit
        'Call Language.setLanguage(Me.Name)
        'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

        'Gajanan [18-OCT-2019] -- Start    
        'Enhancement:Provide Privileges "allowto Add/edit Opertional privileges".
        objUserPrivilege = New clsUserPrivilege
        'Gajanan [18-OCT-2019] -- End


        Try

            'Sandeep [ 10 FEB 2011 ] -- Start
            dtpExpirydate.Enabled = False
            dtpExpirydate.Value = ConfigParameter._Object._CurrentDateAndTime

            'Anjan (25 Oct 2012)/Pinkal-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            'dtpExpirydate.MinDate = ConfigParameter._Object._CurrentDateAndTime
            'Anjan (25 Oct 2012)/Pinkal-End 


            'S.SANDEEP [ 30 May 2011 ] -- START
            'ISSUE : FINCA REQ.
            'Dim dtMaxDate As Date = dtpExpirydate.Value.Date.AddDays(60)
            'S.SANDEEP [ 30 May 2011 ] -- END 

            'Sandeep [ 10 FEB 2011 ] -- End 

            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Call OtherSettings()
            Call setColor()
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges
            cboLanguage.Enabled = User._Object.Privilege._ChangeLanguage
            'Varsha Rana (17-Oct-2017) -- End
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objUserAddEdit._Userunkid = mintUserUnkid
                'S.SANDEEP [ 30 May 2011 ] -- START
                'ISSUE : FINCA REQ.
                If objUserAddEdit._Exp_Days.ToString.Length = 8 Then
                    dtpExpirydate.MinDate = CDate(eZeeDate.convertDate(objUserAddEdit._Exp_Days.ToString).ToShortDateString)
                    'S.SANDEEP [ 09 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dtpExpirydate.Enabled = False
                    pnlExPassword.Enabled = False
                    'S.SANDEEP [ 09 NOV 2012 ] -- END
                End If
                'S.SANDEEP [ 30 May 2011 ] -- END 

                'S.SANDEEP [ 01 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If objUserAddEdit._EmployeeUnkid > 0 Then
                    objpnlPrivacy.Enabled = False
                    objpnlPersonal.Enabled = False
                End If
                'S.SANDEEP [ 01 DEC 2012 ] -- END

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                If objUserAddEdit._IsAduser Then
                    txtUser_Name.ReadOnly = True
                    txtFirstName.ReadOnly = True
                    txtLastName.ReadOnly = True
                    txtAddress1.ReadOnly = True
                    txtAddress2.ReadOnly = True
                    txtEmail.ReadOnly = True
                    txtPhone.ReadOnly = True
                    chkActiveUser.Enabled = False
                End If

                'Pinkal (12-Oct-2011) -- End


                'Gajanan [18-OCT-2019] -- Start    
                'Enhancement:Provide Privileges "allowto Add/edit Opertional privileges".
                objUserPrivilege.setUserPrivilege(User._Object._Userunkid)
                tabcPrivilegeInformation.Enabled = objUserPrivilege._AllowToAddEditOprationalPrivileges
                'Gajanan [18-OCT-2019] -- End

            End If

            Call GetValue()


            'S.SANDEEP [ 09 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction <> enAction.EDIT_ONE Then
            Dim objPwdOp As New clsPassowdOptions
            If mintUserUnkid <> 1 Then
                If objPwdOp._IsPasswordExpiredSet = True Then
                    radYes.Checked = True
                    dtpExpirydate.Enabled = False
                    pnlExPassword.Enabled = False
                End If
            Else
                radNo.Checked = True
                dtpExpirydate.Enabled = False
                pnlExPassword.Enabled = False
                'End If
            End If


            'S.SANDEEP [ 09 NOV 2012 ] -- END


            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call FillTreeView()

            'Gajanan [19-OCT-2019] -- Start    
            'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
            'Call FillTreeView(True, True)
            Call FillTreeView(True, True, True)
            'Gajanan [19-OCT-2019] -- End

            'S.SANDEEP [ 07 NOV 2011 ] -- END



            'Pinkal (27-oct-2010) ------------------- START

            'Sandeep [ 07 FEB 2011 ] -- START
            'If objUserAddEdit._Userunkid = 1 Then
            If mintUserUnkid = User._Object._Userunkid Or mintUserUnkid = 1 Then
                'Sandeep [ 07 FEB 2011 ] -- END 
                'AddHandler tvAccessLevel.BeforeCheck, AddressOf treeview_BeforeCheck
                AddHandler tvCompanyPrivilege.BeforeCheck, AddressOf treeview_BeforeCheck
                AddHandler tvOperationalPrivilege.BeforeCheck, AddressOf treeview_BeforeCheck

                'S.SANDEEP [ 04 JULY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If menAction = enAction.EDIT_ONE Then
                    tvCompanyPrivilege.CheckBoxes = False
                    tvOperationalPrivilege.CheckBoxes = False
                Else
                    tvCompanyPrivilege.CheckBoxes = True
                    tvOperationalPrivilege.CheckBoxes = True
                End If
                'S.SANDEEP [ 04 JULY 2012 ] -- END

                chkActiveUser.Enabled = False
                'cboCompany.Enabled = False
                'cboSelectYear.Enabled = False
                radYes.Enabled = False
                radNo.Enabled = False
                'Sandeep [ 10 FEB 2011 ] -- Start
                'nudDays.Enabled = False
                'objbtnAddReminder.Enabled = False
                dtpExpirydate.Enabled = False
                'Sandeep [ 10 FEB 2011 ] -- End 
                cboUserRole.Enabled = False
                objbtnAddRole.Enabled = False

                ''S.SANDEEP [ 30 May 2011 ] -- START
                ''ISSUE : FINCA REQ.
                'If User._Object.Privilege._AllowChangePassword = True Then
                '    txtPassword.Enabled = True
                '    txtRetypePassword.Enabled = True
                'Else
                '    txtPassword.Enabled = False
                '    txtRetypePassword.Enabled = False
                'End If
                ''S.SANDEEP [ 30 May 2011 ] -- END 
            End If


            'Anjan/sohail [ 16 april 2013 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            'If User._Object.Privilege._AllowChangePassword = True AndAlso ((mintUserUnkid > 0 AndAlso mintUserUnkid = User._Object._Userunkid) OrElse User._Object._Userunkid = 1 OrElse mintUserUnkid <= 0) Then
            If mintUserUnkid <= 0 OrElse User._Object._Userunkid = 1 OrElse (User._Object.Privilege._AllowChangePassword = True AndAlso mintUserUnkid > 0 AndAlso mintUserUnkid = User._Object._Userunkid) Then
                txtPassword.Enabled = True
                txtRetypePassword.Enabled = True

                'Pinkal (06-May-2014) -- Start
                'Enhancement : TRA Changes
            ElseIf mintUserUnkid <> 1 AndAlso User._Object.Privilege._AllowToChangeOtherUserPassword = True AndAlso mintUserUnkid > 0 AndAlso mintUserUnkid <> User._Object._Userunkid Then
                txtPassword.Enabled = True
                txtRetypePassword.Enabled = True
                'Pinkal (06-May-2014) -- End

            Else
                txtPassword.Enabled = False
                txtRetypePassword.Enabled = False
            End If

            'Pinkal (27-oct-2010) ------------------- END


            txtUser_Name.Focus()

            'Anjan (23 Nov 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            'issue : this is not of use as delete button can be use to make user inactive.
            chkActiveUser.Visible = False
            'Anjan (23 Nov 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUser_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUserAddEdit.SetMessages()
            objfrm._Other_ModuleNames = "clsUserAddEdit"
            objfrm.displayDialog(Me)




            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()
            'Gajanan [19-OCT-2019] -- Start    
            'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
            ConfigParameter._Object._Companyunkid = Company._Object._Companyunkid
            'Gajanan [19-OCT-2019] -- End
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objUserAddEdit.Update()
            'Gajanan [19-OCT-2019] -- Start    
            'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.



                Dim objcompany As New clsCompany_Master
                Dim objConfig As New clsConfigOptions
                For Each iCompnay As String In mstrCompanyIds.Split(","c).ToList()
                    objcompany._Companyunkid = CInt(iCompnay)

                    'Gajanan [19-OCT-2019] -- Start    
                    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                    If mblnUserRoleChange = True OrElse mstrFinalRemovedOperationalPrivilege.Length > 0 _
                    OrElse mstrFinalNewlyAddedOperationalPrivilege.Length > 0 _
                    OrElse mstrFinalRemovedCompanyPrivilege.Length > 0 _
                    OrElse mstrFinalNewlyAddedCompanyPrivilege.Length > 0 Then
                        'Gajanan [19-OCT-2019] -- End

                    Dim EmailUserids As String = objConfig.GetKeyValue(objcompany._Companyunkid, "AddEditUserNotificationUserIds", Nothing)

                    If EmailUserids.Length > 0 Then
                        'Sohail (01 Nov 2019) -- Start
                        'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below.
                        Dim strUserName As String = User._Object._Firstname & " " & User._Object._Lastname
                        If strUserName.Trim = "" Then
                            strUserName = User._Object._Username
                        End If
                        strUserName = getTitleCase(strUserName)

                        Dim strAssignedUserName As String = txtFirstName.Text & " " & txtLastName.Text
                        If strAssignedUserName.Trim = "" Then
                            strAssignedUserName = txtUser_Name.Text
                        End If
                        strAssignedUserName = getTitleCase(strAssignedUserName)
                        'Sohail (01 Nov 2019) -- End
                        objUserAddEdit.SendNotification(EmailUserids, objcompany._Companyunkid, objcompany._Name, _
                                                        User._Object._Username, getIP(), getHostName(), mstrForm_Name, _
                                                        enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Username, _
                                                        txtUser_Name.Text, False, mstrFinalNewlyAddedOperationalPrivilege, _
                                                        mstrFinalRemovedOperationalPrivilege, mstrFinalNewlyAddedCompanyPrivilege, _
                                                        mstrFinalRemovedCompanyPrivilege, If(mblnUserRoleChange, cboUserRole.Text, ""), mstrOldRoleName, ConfigParameter._Object._CurrentDateAndTime)
                        'Sohail (01 Nov 2019) - [User._Object._Username = strUserName, User._Object._Username = If(User._Object._Email.Trim <> "", User._Object._Email, strUserName), txtUser_Name.Text = strAssignedUserName]
                    'Sohail (31 Oct 2019) - [mstrOldRoleName, _CurrentDateAndTime]
                        End If
                    End If
                Next
                objcompany = Nothing
            'Gajanan [19-OCT-2019] -- End
            Else
                blnFlag = objUserAddEdit.Insert()
            'Gajanan [19-OCT-2019] -- Start    
            'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                Dim objcompany As New clsCompany_Master
                Dim objConfig As New clsConfigOptions
                For Each iCompnay As String In mstrCompanyIds.Split(","c).ToList()

                    'Gajanan [19-OCT-2019] -- Start    
                    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                    objcompany._Companyunkid = CInt(iCompnay)
                    If mblnUserRoleChange = True OrElse mstrFinalRemovedOperationalPrivilege.Length > 0 _
                    OrElse mstrFinalNewlyAddedOperationalPrivilege.Length > 0 _
                    OrElse mstrFinalRemovedCompanyPrivilege.Length > 0 _
                    OrElse mstrFinalNewlyAddedCompanyPrivilege.Length > 0 Then
                        'Gajanan [19-OCT-2019] -- End    


                    Dim EmailUserids As String = objConfig.GetKeyValue(objcompany._Companyunkid, "AddEditUserNotificationUserIds", Nothing)
                    If EmailUserids.Length > 0 Then
                        'Sohail (01 Nov 2019) -- Start
                        'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below.
                        Dim strUserName As String = User._Object._Firstname & " " & User._Object._Lastname
                        If strUserName.Trim = "" Then
                            strUserName = User._Object._Username
                        End If
                        strUserName = getTitleCase(strUserName)

                        Dim strAssignedUserName As String = txtFirstName.Text & " " & txtLastName.Text
                        If strAssignedUserName.Trim = "" Then
                            strAssignedUserName = txtUser_Name.Text
                        End If
                        strAssignedUserName = getTitleCase(strAssignedUserName)
                        'Sohail (01 Nov 2019) -- End
                        objUserAddEdit.SendNotification(EmailUserids, _
                                                  objcompany._Companyunkid, objcompany._Name, _
                                                  strUserName, getIP(), getHostName(), _
                                                  mstrForm_Name, enLogin_Mode.DESKTOP, User._Object._Userunkid, _
                                                  If(User._Object._Email.Trim <> "", User._Object._Email, strUserName), strAssignedUserName, True, mstrFinalNewlyAddedOperationalPrivilege, _
                                                  "", mstrFinalNewlyAddedCompanyPrivilege, "", If(mblnUserRoleChange, cboUserRole.Text, ""), mstrOldRoleName, ConfigParameter._Object._CurrentDateAndTime)
                        'Sohail (01 Nov 2019) - [User._Object._Username = strUserName, User._Object._Username = If(User._Object._Email.Trim <> "", User._Object._Email, strUserName), txtUser_Name.Text = strAssignedUserName, mstrOldRoleName, _CurrentDateAndTime]
                        End If
                    End If
                Next
                objcompany = Nothing
            'Gajanan [19-OCT-2019] -- End
            End If

            If blnFlag = False And objUserAddEdit._Message <> "" Then
                eZeeMsgBox.Show(objUserAddEdit._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objUserAddEdit = Nothing
                    objUserAddEdit = New clsUserAddEdit
                    Call GetValue()
                    txtUser_Name.Focus()
                Else
                    mintUserUnkid = objUserAddEdit._Roleunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnAddRole_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddRole.Click
        Dim objfrm As New frmUserRole_AddEdit
        Dim dsCombos As New DataSet
        Dim objRole As New clsUserRole_Master
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 
            If objfrm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objRole.getComboList("Role", True)
                With cboUserRole
                    .ValueMember = "roleunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Role")
                End With
                cboUserRole.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddRole_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnAddReminder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objfrm As New frmReminder_AddEdit
        'Sandeep [ 10 FEB 2011 ] -- Start
        objfrm._TitleName = Language.getMessage(mstrModuleName, 2, "Password Expiry")
        'Sandeep [ 10 FEB 2011 ] -- End 
        objfrm.displayDialog(-1, enAction.ADD_ONE)
    End Sub


    'Pinkal (18-Jan-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Dim k As Integer = 0
            Dim FirstNode As TreeNode = Nothing
            tvOperationalPrivilege.SelectedNode = Nothing
            Dim blnSearch As Boolean = False
            Dim tnc As TreeNodeCollection
            tnc = tvOperationalPrivilege.Nodes
            If TxtPrivilegeSearch.Text.Length > 0 Then
                For i = 0 To tvOperationalPrivilege.Nodes.Count - 1
                    tnc(i).Collapse()
                    For j = 0 To tvOperationalPrivilege.Nodes(i).Nodes.Count - 1
                        tnc(i).Nodes(j).BackColor = Color.White
                        If tnc(i).Nodes(j).Text.Trim.ToUpper.Contains(TxtPrivilegeSearch.Text.Trim.ToUpper) Then
                            tnc(i).Nodes(j).BackColor = Color.SkyBlue
                            If k = 0 Then
                                FirstNode = tnc(i).Nodes(j)
                                k += 1
                            End If
                            tnc(i).Expand()
                            blnSearch = True
                        End If
                    Next
                Next

                If blnSearch = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Not found. Please try again with correct text."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    TxtPrivilegeSearch.SelectAll()
                    Exit Sub
                Else
                    If FirstNode IsNot Nothing Then tvOperationalPrivilege.SelectedNode = FirstNode
                End If

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "There is no text for searching privilege(s).Please Enter text for searching privilege(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                TxtPrivilegeSearch.Select()
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            Dim tnc As TreeNodeCollection
            tnc = tvOperationalPrivilege.Nodes
            For i = 0 To tvOperationalPrivilege.Nodes.Count - 1
                For j = 0 To tvOperationalPrivilege.Nodes(i).Nodes.Count - 1
                    tnc(i).Nodes(j).BackColor = Color.White
                Next
            Next
            tvOperationalPrivilege.CollapseAll()
            TxtPrivilegeSearch.Text = ""
            tvOperationalPrivilege.SelectedNode = Nothing
            TxtPrivilegeSearch.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Jan-2013) -- End


#End Region

    'Sandeep [ 21 Aug 2010 ] -- Start
#Region " Other Controls Events "
    Private Sub tvPrivilege_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvOperationalPrivilege.AfterCheck, tvCompanyPrivilege.AfterCheck
        If Not isStartChk Then Exit Sub
        Try
            isStartChk = False

            If e.Node.Checked Then
                e.Node.ForeColor = clrChk
            Else
                e.Node.ForeColor = clrUnChk
            End If

            Call ProcessStateChange(e.Node)

        Catch ex As System.Exception
            Call DisplayError.Show(CStr(-1), ex.Message, "tvPrivilege_AfterCheck", mstrModuleName)
        Finally
            If CType(sender, TreeView).Name = "tvOperationalPrivilege" Then
                tvOperationalPrivilege.Refresh()
            ElseIf CType(sender, TreeView).Name = "tvCompanyPrivilege" Then
                tvCompanyPrivilege.Refresh()
            End If
            isStartChk = True
        End Try
    End Sub

    Private Sub treeview_BeforeCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs)
        e.Cancel = True
    End Sub

    Private Sub cboUserRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUserRole.SelectedIndexChanged
        Try
            If CInt(cboUserRole.SelectedValue) > 0 Then
                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Call FillTreeView()
                Call FillTreeView(True, False)
                'S.SANDEEP [ 07 NOV 2011 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUserRole_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub cboCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If CInt(cboCompany.SelectedValue) > 0 Then
    '            Dim dsList As New DataSet
    '            Dim objCompany As New clsCompany_Master

    '            dsList = objCompany.GetFinancialYearList(CInt(cboCompany.SelectedValue), -1, "List")
    '            With cboSelectYear
    '                .ValueMember = "yearunkid"
    '                .DisplayMember = "financialyear_name"
    '                .DataSource = dsList.Tables(0)
    '            End With
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboCompany_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboSelectYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If CInt(cboSelectYear.SelectedValue) > 0 Then
    '            Call FillUserAccessTreeView(CInt(cboCompany.SelectedValue), mintUserUnkid, CInt(cboSelectYear.SelectedValue))
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboSelectYear_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

#End Region
    'Sandeep [ 21 Aug 2010 ] -- End


    'Sandeep [ 10 FEB 2011 ] -- Start
    Private Sub radNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radNo.CheckedChanged, radYes.CheckedChanged
        Try
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case "RADNO"
                    dtpExpirydate.Enabled = False
                Case "RADYES"
                    dtpExpirydate.Enabled = True
                    'S.SANDEEP [ 30 May 2011 ] -- START
                    'ISSUE : FINCA REQ.
                    If radYes.Checked = True Then
                        If menAction = enAction.ADD_CONTINUE Then
                            Dim objPswd As New clsPassowdOptions
                            If objPswd._IsPasswordExpiredSet Then
                                If objPswd._PasswordMaxLen > 0 Then
                                    Dim dtExDate As Date = ConfigParameter._Object._CurrentDateAndTime
                                    dtExDate = dtExDate.AddDays(objPswd._PasswordMaxLen)
                                    dtpExpirydate.Value = dtExDate

                                    'S.SANDEEP [ 09 NOV 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    dtpExpirydate.Enabled = False
                                    'S.SANDEEP [ 09 NOV 2012 ] -- END

                                End If
                            End If
                            objPswd = Nothing
                        ElseIf menAction = enAction.EDIT_ONE Then
                            Dim objPswd As New clsPassowdOptions
                            If objUserAddEdit._Exp_Days.ToString.Length <> 8 Then
                                If objPswd._PasswordMaxLen > 0 Then


                                    'Pinkal (09-Nov-2012) -- Start
                                    'Enhancement : TRA Changes

                                    'Dim dtExDate As Date = objUserAddEdit._Creation_Date

                                    Dim dtExDate As Date = Nothing
                                    If eZeeDate.convertDate(objUserAddEdit._Creation_Date) = "19000101" Then
                                        dtExDate = ConfigParameter._Object._CurrentDateAndTime
                                    Else
                                        dtExDate = objUserAddEdit._Creation_Date
                                    End If

                                    'Pinkal (09-Nov-2012) -- End

                                    dtExDate = dtExDate.AddDays(objPswd._PasswordMaxLen)
                                    dtpExpirydate.Value = dtExDate
                                End If
                            Else
                                dtpExpirydate.Value = CDate(eZeeDate.convertDate(objUserAddEdit._Exp_Days.ToString).ToShortDateString)
                            End If
                            objPswd = Nothing
                        End If
                    End If
                    'S.SANDEEP [ 30 May 2011 ] -- END 
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radNo_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 10 FEB 2011 ] -- End 
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbUserDetails.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbUserDetails.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbUserDetails.Text = Language._Object.getCaption(Me.gbUserDetails.Name, Me.gbUserDetails.Text)
            Me.chkActiveUser.Text = Language._Object.getCaption(Me.chkActiveUser.Name, Me.chkActiveUser.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.tabpOperationalPrivilege.Text = Language._Object.getCaption(Me.tabpOperationalPrivilege.Name, Me.tabpOperationalPrivilege.Text)
            Me.tabpCompanySelection.Text = Language._Object.getCaption(Me.tabpCompanySelection.Name, Me.tabpCompanySelection.Text)
            Me.radYes.Text = Language._Object.getCaption(Me.radYes.Name, Me.radYes.Text)
            Me.radNo.Text = Language._Object.getCaption(Me.radNo.Name, Me.radNo.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblExpirePassword.Text = Language._Object.getCaption(Me.lblExpirePassword.Name, Me.lblExpirePassword.Text)
            Me.lblLanguage.Text = Language._Object.getCaption(Me.lblLanguage.Name, Me.lblLanguage.Text)
            Me.EZeeLine2.Text = Language._Object.getCaption(Me.EZeeLine2.Name, Me.EZeeLine2.Text)
            Me.chkRightToLeft.Text = Language._Object.getCaption(Me.chkRightToLeft.Name, Me.chkRightToLeft.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblAbilityLevel.Text = Language._Object.getCaption(Me.lblAbilityLevel.Name, Me.lblAbilityLevel.Text)
            Me.EZeeLine3.Text = Language._Object.getCaption(Me.EZeeLine3.Name, Me.EZeeLine3.Text)
            Me.lblPassword.Text = Language._Object.getCaption(Me.lblPassword.Name, Me.lblPassword.Text)
            Me.lblVerifyPassword.Text = Language._Object.getCaption(Me.lblVerifyPassword.Name, Me.lblVerifyPassword.Text)
            Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
            Me.elUserDetails.Text = Language._Object.getCaption(Me.elUserDetails.Name, Me.elUserDetails.Text)
            Me.elPersonalDetails.Text = Language._Object.getCaption(Me.elPersonalDetails.Name, Me.elPersonalDetails.Text)
            Me.lblAddress2.Text = Language._Object.getCaption(Me.lblAddress2.Name, Me.lblAddress2.Text)
            Me.lblAddress1.Text = Language._Object.getCaption(Me.lblAddress1.Name, Me.lblAddress1.Text)
            Me.lblLastName.Text = Language._Object.getCaption(Me.lblLastName.Name, Me.lblLastName.Text)
            Me.lblFirstname.Text = Language._Object.getCaption(Me.lblFirstname.Name, Me.lblFirstname.Text)
            Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
            Me.lblPhone.Text = Language._Object.getCaption(Me.lblPhone.Name, Me.lblPhone.Text)
            Me.chkIsManager.Text = Language._Object.getCaption(Me.chkIsManager.Name, Me.chkIsManager.Text)
            Me.lblReloginDate.Text = Language._Object.getCaption(Me.lblReloginDate.Name, Me.lblReloginDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Username cannot be blank. Username is a mandatory information.")
            Language.setMessage(mstrModuleName, 2, "Password Expiry")
            Language.setMessage(mstrModuleName, 3, "character(s)")
            Language.setMessage(mstrModuleName, 4, "User Role is compulsory information. Please select User Role.")
            Language.setMessage(mstrModuleName, 5, "Password does not match. Please type correct password.")
            Language.setMessage(mstrModuleName, 6, "Password length cannot be less than")
            Language.setMessage(mstrModuleName, 7, "Expiry date cannot be less than the maximum password expiration days.")
            Language.setMessage(mstrModuleName, 9, "Sorry! Relogin date should be greater than current date.")
            Language.setMessage(mstrModuleName, 10, "Username length cannot be less than")
            Language.setMessage(mstrModuleName, 11, "Verify Password cannot be blank. Verify Password is required information.")
            Language.setMessage(mstrModuleName, 12, "Password must contain atleast one upper case character.")
            Language.setMessage(mstrModuleName, 13, "Password must contain atleast one lower case character.")
            Language.setMessage(mstrModuleName, 14, "Password must contain atleast one numeric digit.")
            Language.setMessage(mstrModuleName, 15, "Password must contain atleast one special character.")
            Language.setMessage(mstrModuleName, 16, "Not found. Please try again with correct text.")
            Language.setMessage(mstrModuleName, 17, "There is no text for searching privilege(s).Please Enter text for searching privilege(s).")
            Language.setMessage(mstrModuleName, 18, "Password cannot be blank. Password is required information.")
			Language.setMessage(mstrModuleName, 19, "Sorry, Password cannot be set for")
			Language.setMessage(mstrModuleName, 20, "This Password does not meet the password complexity requirement as per the password policy.")
			Language.setMessage(mstrModuleName, 21, "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator.")
			Language.setMessage(mstrModuleName, 22, "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator.")
			Language.setMessage(mstrModuleName, 23, "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator.")
			Language.setMessage(mstrModuleName, 24, "Sorry, you cannot login. Reason : You are retired. Please contact Administrator.")
			Language.setMessage("Language", 5, "Default English")
			Language.setMessage("Language", 6, "Custom 1")
			Language.setMessage("Language", 7, "Custom 2")
			Language.setMessage(mstrModuleName, 25, "Sorry, Entered New Password is already Used. Please Enter another Password!!!")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class