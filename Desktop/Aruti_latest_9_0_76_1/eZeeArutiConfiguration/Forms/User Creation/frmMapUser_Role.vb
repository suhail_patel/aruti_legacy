﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmMapUser_Role

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMapUser_Role"
    Private mblnCancel As Boolean = True
    Private mds_ImportData As DataSet
    Private dvGriddata As DataView = Nothing
    Private mdtImportData As New DataTable
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmMapUser_Role_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmMapUser_Role_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDataCombo()
        Try
            For Each cr As Control In gbFileInfo.Controls
                If TypeOf cr Is ComboBox Then
                    ClearCombo(CType(cr, ComboBox))
                End If
            Next
            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboUserName.Items.Add(dtColumns.ColumnName)
                cboUserRole.Items.Add(dtColumns.ColumnName)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboUserName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Username cannot be blank. Please set Username in order to map user with role."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboUserRole.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "User role cannot be blank. Please set User role in order to to map user with role."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            mdtImportData.Columns.Add("uname", System.Type.GetType("System.String"))
            mdtImportData.Columns.Add("urole", System.Type.GetType("System.String"))
            mdtImportData.Columns.Add("image", System.Type.GetType("System.Object"))
            mdtImportData.Columns.Add("message", System.Type.GetType("System.String"))
            mdtImportData.Columns.Add("status", System.Type.GetType("System.String"))
            mdtImportData.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboUserName.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow
            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdtImportData.NewRow

                drNewRow.Item("uname") = dtRow.Item(cboUserName.Text).ToString.Trim
                drNewRow.Item("urole") = dtRow.Item(cboUserRole.Text).ToString.Trim
                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdtImportData.Rows.Add(drNewRow)
            Next

            If blnIsNotThrown = True Then
                colhUser.DataPropertyName = "uname"
                dgcolhRole.DataPropertyName = "urole"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = mdtImportData.DefaultView
                dgData.DataSource = dvGriddata
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Import_Data()
        Me.Enabled = False
        Try
            btnFilter.Enabled = False
            If mdtImportData.Rows.Count <= 0 Then
                Exit Sub
            End If
            Dim iUserId, iRoleId As Integer
            iUserId = 0 : iRoleId = 0
            Dim objUser As New clsUserAddEdit
            Dim objRole As New clsUserRole_Master

            For Each dtRow As DataRow In mdtImportData.Rows
                objlblProgress.Text = Language.getMessage(mstrModuleName, 14, "Processed : ") & " " & mdtImportData.Rows.IndexOf(dtRow).ToString() & " / " & mdtImportData.Rows.Count
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdtImportData.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                Catch ex As Exception
                End Try

                If dtRow.Item("uname").ToString.Trim.Length > 0 Then
                    iUserId = objUser.Return_UserId(dtRow.Item("uname").ToString().Trim(), "hrmsConfiguration", enLoginMode.USER)
                    If iUserId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 3, "User Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 4, "Fail")
                        dtRow.Item("objStatus") = 2
                        Continue For
                    Else
                        If iUserId = 1 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Cannot assign role to predefined user.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 4, "Fail")
                            dtRow.Item("objStatus") = 2
                            Continue For
                        Else
                            If chkOverwriteNewRole.Checked = False Then
                                objUser._Userunkid = iUserId
                                If objUser._Roleunkid > 0 Then
                                    dtRow.Item("image") = imgError
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Role already assigned.")
                                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 4, "Fail")
                                    dtRow.Item("objStatus") = 2
                                    Continue For
                                End If
                            End If
                        End If
                    End If
                End If

                Dim strCsvPrivilgeIds As String = String.Empty
                If dtRow.Item("urole").ToString.Trim.Length > 0 Then
                    iRoleId = objRole.GetRoleUnkid(dtRow.Item("urole").ToString.Trim, strCsvPrivilgeIds)
                    If iRoleId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Role Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 4, "Fail")
                        dtRow.Item("objStatus") = 2
                        Continue For
                    Else
                        If strCsvPrivilgeIds.Trim.Length <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Privilege not binded to role.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 4, "Fail")
                            dtRow.Item("objStatus") = 2
                            Continue For
                        End If
                    End If
                End If

                objUser._Userunkid = iUserId
                objUser._Roleunkid = iRoleId
                If strCsvPrivilgeIds.Trim.Length > 0 Then
                    objUser._AssignPrivilegeIDs = strCsvPrivilgeIds
                End If
                If objUser.Update() Then
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Success")
                    dtRow.Item("objStatus") = 1
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objUser._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 4, "Fail")
                    dtRow.Item("objStatus") = 2
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
            Me.Enabled = True : objlblProgress.Text = ""
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Try
            Call Import_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.Filter = "Excel files(*.xlsx)|*.xlsx"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                mds_ImportData = OpenXML_Import(txtFilePath.Text)
                Call SetDataCombo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link's Event "

    Private Sub lnkFill_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkFill.LinkClicked
        Try
            If cboUserName.SelectedIndex < 0 OrElse cboUserRole.SelectedIndex < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Please map username and role in order to map user and role."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call CreateDataTable()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkFill_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Try
            Dim dsList As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("UserName", GetType(System.String))
            dt.Columns.Add("UserRole", GetType(System.String))
            dsList.Tables.Add(dt.Copy)
            Dim savDialog As New SaveFileDialog : savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                Aruti.Data.modGlobal.OpenXML_Export(savDialog.FileName, dsList)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Template exported to selected path."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Context Menu's Event "

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            If dvGriddata IsNot Nothing Then
                dvGriddata.RowFilter = "objStatus = 2"
                Dim savDialog As New SaveFileDialog
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image") : dtTable.Columns.Remove("objstatus")
                        Dim ds As New DataSet : ds.Tables.Add(dtTable)
                        Aruti.Data.modGlobal.OpenXML_Export(savDialog.FileName, ds)
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 3"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFileInfo.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Name <> cmb.Name Then
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.lblUserRole.Text = Language._Object.getCaption(Me.lblUserRole.Name, Me.lblUserRole.Text)
            Me.lblUsername.Text = Language._Object.getCaption(Me.lblUsername.Name, Me.lblUsername.Text)
            Me.colhUser.HeaderText = Language._Object.getCaption(Me.colhUser.Name, Me.colhUser.HeaderText)
            Me.dgcolhRole.HeaderText = Language._Object.getCaption(Me.dgcolhRole.Name, Me.dgcolhRole.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.lnkFill.Text = Language._Object.getCaption(Me.lnkFill.Name, Me.lnkFill.Text)
            Me.elMapping.Text = Language._Object.getCaption(Me.elMapping.Name, Me.elMapping.Text)
            Me.chkOverwriteNewRole.Text = Language._Object.getCaption(Me.chkOverwriteNewRole.Name, Me.chkOverwriteNewRole.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Username cannot be blank. Please set Username in order to map user with role.")
            Language.setMessage(mstrModuleName, 2, "User role cannot be blank. Please set User role in order to to map user with role.")
            Language.setMessage(mstrModuleName, 3, "User Not Found.")
            Language.setMessage(mstrModuleName, 4, "Fail")
            Language.setMessage(mstrModuleName, 5, "Role already assigned.")
            Language.setMessage(mstrModuleName, 6, "Role Not Found.")
            Language.setMessage(mstrModuleName, 7, "Privilege not binded to role.")
            Language.setMessage(mstrModuleName, 8, "Success")
            Language.setMessage(mstrModuleName, 9, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 10, "Sorry, Please map username and role in order to map user and role.")
            Language.setMessage(mstrModuleName, 11, "Cannot assign role to predefined user.")
            Language.setMessage(mstrModuleName, 12, "Template exported to selected path.")
            Language.setMessage(mstrModuleName, 14, "Processed :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class