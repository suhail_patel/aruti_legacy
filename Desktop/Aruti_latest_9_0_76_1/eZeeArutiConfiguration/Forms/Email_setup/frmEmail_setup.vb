﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Microsoft.Exchange.WebServices.Data

#End Region

Public Class frmEmail_setup

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmail_setup"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim mintEmailsetupunkid As Integer = -1
    Dim objEmail As clsEmail_setup

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintEmailsetupunkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintEmailsetupunkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form Events "

    Private Sub frmEmail_setup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmail = New clsEmail_setup
        Try
            Call Set_Logo(Me, gApplicationType)


            Language.setLanguage(Me.Name)
            OtherSettings()

            Call SetColor()
            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            Call FillCombo()
            'Hemant (01 Feb 2022) -- End

            pnlExchangeWebService.Location = New System.Drawing.Point(lblServerInformation.Location.X - 3, lblServerInformation.Location.Y + 17)
            pnlExchangeWebService.Size = New System.Drawing.Size(357, 57)

            If menAction = enAction.EDIT_ONE Then
                objEmail._Emailsetupunkid = mintEmailsetupunkid
            End If

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmail_setup_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmail_setup_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            ElseIf e.KeyCode = Keys.S AndAlso e.Control = True Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmail_setup_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmail_setup_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmail = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmail_setup_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmail_setup.SetMessages()
            objfrm._Other_ModuleNames = "clsEmail_setup"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtMailSenderName.BackColor = GUI.ColorOptional
            txtMailSenderEmail.BackColor = GUI.ColorOptional
            txtReference.BackColor = GUI.ColorOptional
            txtMailServer.BackColor = GUI.ColorOptional
            txtMailServerPort.BackColor = GUI.ColorOptional
            txtUserName.BackColor = GUI.ColorOptional
            txtPassword.BackColor = GUI.ColorOptional
            txtEWSUrl.BackColor = GUI.ColorOptional
            txtEWSDomain.BackColor = GUI.ColorOptional
            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            cboAuthenticationProtocol.BackColor = GUI.ColorOptional
            'Hemant (01 Feb 2022) -- End
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Public Sub SetValue()
        Try
            With objEmail
                ._Sendername = txtMailSenderName.Text
                ._Senderaddress = txtMailSenderEmail.Text
                ._Reference = txtReference.Text
                ._Mailserverip = txtMailServer.Text
                Dim intPort As Integer = 0
                Integer.TryParse(txtMailServerPort.Text, intPort)
                ._Mailserverport = intPort
                ._Username = txtUserName.Text
                ._Password = clsSecurity.Encrypt(txtPassword.Text, "ezee")
                ._Mail_Body = txtBody.Text
                If radSMTP.Checked = True Then
                    ._Email_Type = 0 '0 = SMTP, 1 = EWS
                ElseIf radExchangeWebService.Checked = True Then
                    ._Email_Type = 1 '0 = SMTP, 1 = EWS
                Else
                    ._Email_Type = 0 '0 = SMTP, 1 = EWS
                End If
                ._EWS_URL = txtEWSUrl.Text
                ._Ews_Domain = txtEWSDomain.Text
                ._Isloginssl = chkSSL.Checked
                ._Iscrt_authenticated = chkCertificateAuthentication.Checked
                ._Isbypassproxy = chkBypassProxyServer.Checked


                ._Userunkid = User._Object._Userunkid
                ._Isvoid = False
                ._Voiduserunkid = 0
                ._Voiddatetime = Nothing
                ._Voidreason = ""

                ._AuditUserId = User._Object._Userunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._Isweb = False
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FormName = Me.Name
                'Hemant (01 Feb 2022) -- Start
                'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                ._Protocolunkid = CInt(cboAuthenticationProtocol.SelectedValue)
                'Hemant (01 Feb 2022) -- End
            End With
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Public Sub GetValue()
        Try
            With objEmail
                txtMailSenderName.Text = ._Sendername
                txtMailSenderEmail.Text = ._Senderaddress
                txtReference.Text = ._Reference
                txtMailServer.Text = ._Mailserverip
                txtMailServerPort.Text = ._Mailserverport.ToString
                txtUserName.Text = ._Username
                If ._Password.Trim <> "" Then
                    txtPassword.Text = clsSecurity.Decrypt(._Password, "ezee")
                Else
                    txtPassword.Text = ._Password
                End If
                txtBody.Text = ._Mail_Body
                If ._Email_Type = 0 Then '0 = SMTP, 1 = EWS
                    radSMTP.Checked = True
                ElseIf ._Email_Type = 1 Then '0 = SMTP, 1 = EWS
                    radExchangeWebService.Checked = True
                Else '0 = SMTP, 1 = EWS
                    radSMTP.Checked = True
                End If
                txtEWSUrl.Text = ._Ews_Url
                txtEWSDomain.Text = ._Ews_Domain
                chkSSL.Checked = ._Isloginssl
                chkCertificateAuthentication.Checked = ._Iscrt_authenticated
                chkBypassProxyServer.Checked = ._Isbypassproxy
                'Hemant (01 Feb 2022) -- Start
                'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                cboAuthenticationProtocol.SelectedValue = CInt(._Protocolunkid)
                'Hemant (01 Feb 2022) -- End
            End With
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try
            If txtMailSenderEmail.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please enter From Email."), enMsgBoxStyle.Information)
                txtMailSenderEmail.Focus()
                Return False

            ElseIf txtUserName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter Email."), enMsgBoxStyle.Information)
                txtUserName.Focus()
                Return False

            ElseIf txtPassword.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter Password."), enMsgBoxStyle.Information)
                txtPassword.Focus()
                Return False

            End If

            If radExchangeWebService.Checked = True Then

                If txtEWSUrl.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter EWS URL."), enMsgBoxStyle.Information)
                    txtEWSUrl.Focus()
                    Return False

                ElseIf txtEWSDomain.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter EWS Domain."), enMsgBoxStyle.Information)
                    txtEWSDomain.Focus()
                    Return False

                End If

            Else

                If txtMailServer.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please enter Mail Server."), enMsgBoxStyle.Information)
                    txtMailServer.Focus()
                    Return False

                ElseIf txtMailServerPort.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter Mail Server Port."), enMsgBoxStyle.Information)
                    txtMailServerPort.Focus()
                    Return False

                End If

            End If

            Return True

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "IsValidated", mstrModuleName)
        End Try
    End Function

    'Hemant (01 Feb 2022) -- Start
    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
    Private Sub FillCombo()
        Dim ObjMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjMasterData.getComboListForEmailProtocol("List")
            With cboAuthenticationProtocol
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List").Copy
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjMasterData = Nothing
            dsCombos = Nothing
        End Try
    End Sub
    'Hemant (01 Feb 2022) -- End


#End Region

#Region " Radio Buttons Events "

    Private Sub radSMTP_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSMTP.CheckedChanged
        Try
            pnlExchangeWebService.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radSMTP_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radExchangeWebService_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExchangeWebService.CheckedChanged
        Try
            pnlExchangeWebService.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExchangeWebService_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnTestMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestMail.Click
        'Hemant (01 Feb 2022) -- Start
        'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.  
        Dim _Tls12 As System.Security.Authentication.SslProtocols = CType(&HC00, System.Security.Authentication.SslProtocols)
        Dim Tls12 As System.Net.SecurityProtocolType = CType(_Tls12, System.Net.SecurityProtocolType)
        'Hemant (01 Feb 2022) -- End
        Try
            If IsValidated() = False Then Exit Try

            If txtToEmail.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter To Email."), enMsgBoxStyle.Information)
                txtToEmail.Focus()
                Exit Try
            End If

            If radSMTP.Checked = True Then

                Dim strSenderDispName As String = txtMailSenderName.Text
                If strSenderDispName.Trim.Length <= 0 Then
                    strSenderDispName = txtMailSenderEmail.Text
                End If
                Dim objMail As New System.Net.Mail.MailMessage(New System.Net.Mail.MailAddress(txtMailSenderEmail.Text, strSenderDispName), New System.Net.Mail.MailAddress(txtToEmail.Text))
                objMail.Body = txtBody.Text
                objMail.Subject = txtReference.Text
                objMail.IsBodyHtml = True

                Dim SmtpMail As New System.Net.Mail.SmtpClient()
                SmtpMail.Host = txtMailServer.Text
                SmtpMail.Port = CInt(Val(txtMailServerPort.Text))
                If txtUserName.Text <> "" Then
                    SmtpMail.Credentials = New System.Net.NetworkCredential(txtUserName.Text, txtPassword.Text)
                End If
                SmtpMail.EnableSsl = chkSSL.Checked

                If chkBypassProxyServer.Checked Then
                    System.Net.ServicePointManager.Expect100Continue = False
                End If

                If chkCertificateAuthentication.Checked Then
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
                End If

                'Hemant (01 Feb 2022) -- Start
                'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                If SmtpMail.Port = 587 Then
                    If CInt(cboAuthenticationProtocol.SelectedValue) = enAuthenticationProtocol.TLS Then
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls
                    ElseIf CInt(cboAuthenticationProtocol.SelectedValue) = enAuthenticationProtocol.SSL3 Then
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3
                    ElseIf CInt(cboAuthenticationProtocol.SelectedValue) = enAuthenticationProtocol.TLS12 Then
                        System.Net.ServicePointManager.SecurityProtocol = Tls12
                    Else
                    End If
                End If
                'Hemant (01 Feb 2022) -- End

                SmtpMail.Send(objMail)

            ElseIf radExchangeWebService.Checked = True Then

                'Creating the ExchangeService Object 
                Dim objService As New ExchangeService()

                'Seting Credentials to be used
                objService.Credentials = New WebCredentials(txtUserName.Text, txtPassword.Text, txtEWSDomain.Text)

                'Setting the EWS Uri
                objService.Url = New Uri(txtEWSUrl.Text)

                'Creating an Email Service and passing in the service
                Dim objMessage As New EmailMessage(objService)

                'Setting Email message properties
                objMessage.Subject = txtReference.Text
                objMessage.Body = txtBody.Text
                objMessage.ToRecipients.Add(txtToEmail.Text)

                'Sending the email and saving a copy.
                'objMessage.Send()
                objMessage.SendAndSaveCopy()

            End If

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Mail sent Successfully..."))

        Catch ex As Exception
            If ex.Message.Contains("Fail") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 10, "Please check your Internet settings."), enMsgBoxStyle.Information)
            ElseIf ex.Message.Contains("secure") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 11, "Please Uncheck Login using SSL setting."), enMsgBoxStyle.Information)
            Else
                DisplayError.Show("-1", ex.Message, "btnTestMail_Click", mstrModuleName)
            End If

        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidated() = False Then Exit Try

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objEmail.Update(Nothing)
            Else
                blnFlag = objEmail.Insert(Nothing)
            End If

            If blnFlag = False And objEmail._Message <> "" Then
                eZeeMsgBox.Show(objEmail._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objEmail = Nothing
                    objEmail = New clsEmail_setup
                    Call GetValue()
                    txtMailSenderName.Focus()
                Else
                    mintEmailsetupunkid = objEmail._Emailsetupunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

#End Region



    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmailSetup.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmailSetup.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnTestMail.GradientBackColor = GUI._ButttonBackColor 
			Me.btnTestMail.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbEmailSetup.Text = Language._Object.getCaption(Me.gbEmailSetup.Name, Me.gbEmailSetup.Text)
			Me.radExchangeWebService.Text = Language._Object.getCaption(Me.radExchangeWebService.Name, Me.radExchangeWebService.Text)
			Me.radSMTP.Text = Language._Object.getCaption(Me.radSMTP.Name, Me.radSMTP.Text)
			Me.chkBypassProxyServer.Text = Language._Object.getCaption(Me.chkBypassProxyServer.Name, Me.chkBypassProxyServer.Text)
			Me.lblEWSDomain.Text = Language._Object.getCaption(Me.lblEWSDomain.Name, Me.lblEWSDomain.Text)
			Me.lblEWSUrl.Text = Language._Object.getCaption(Me.lblEWSUrl.Name, Me.lblEWSUrl.Text)
			Me.chkCertificateAuthentication.Text = Language._Object.getCaption(Me.chkCertificateAuthentication.Name, Me.chkCertificateAuthentication.Text)
			Me.btnTestMail.Text = Language._Object.getCaption(Me.btnTestMail.Name, Me.btnTestMail.Text)
			Me.lblTestingMessage.Text = Language._Object.getCaption(Me.lblTestingMessage.Name, Me.lblTestingMessage.Text)
			Me.lblTestingTitle.Text = Language._Object.getCaption(Me.lblTestingTitle.Name, Me.lblTestingTitle.Text)
			Me.lblBody.Text = Language._Object.getCaption(Me.lblBody.Name, Me.lblBody.Text)
			Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
			Me.lblPassword.Text = Language._Object.getCaption(Me.lblPassword.Name, Me.lblPassword.Text)
			Me.chkSSL.Text = Language._Object.getCaption(Me.chkSSL.Name, Me.chkSSL.Text)
			Me.lblUserInformation.Text = Language._Object.getCaption(Me.lblUserInformation.Name, Me.lblUserInformation.Text)
			Me.lblMailServerPort.Text = Language._Object.getCaption(Me.lblMailServerPort.Name, Me.lblMailServerPort.Text)
			Me.lblMailServer.Text = Language._Object.getCaption(Me.lblMailServer.Name, Me.lblMailServer.Text)
			Me.lblServerInformation.Text = Language._Object.getCaption(Me.lblServerInformation.Name, Me.lblServerInformation.Text)
			Me.lblReference.Text = Language._Object.getCaption(Me.lblReference.Name, Me.lblReference.Text)
			Me.lblSenderInformation.Text = Language._Object.getCaption(Me.lblSenderInformation.Name, Me.lblSenderInformation.Text)
			Me.lblSenderName.Text = Language._Object.getCaption(Me.lblSenderName.Name, Me.lblSenderName.Text)
			Me.lblEmailAddress.Text = Language._Object.getCaption(Me.lblEmailAddress.Name, Me.lblEmailAddress.Text)
			Me.lblToEmail.Text = Language._Object.getCaption(Me.lblToEmail.Name, Me.lblToEmail.Text)
            Me.lblAuthenticationProtocol.Text = Language._Object.getCaption(Me.lblAuthenticationProtocol.Name, Me.lblAuthenticationProtocol.Text)
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please enter From Email.")
			Language.setMessage(mstrModuleName, 2, "Please enter Email.")
			Language.setMessage(mstrModuleName, 3, "Please enter Password.")
			Language.setMessage(mstrModuleName, 4, "Please enter EWS URL.")
			Language.setMessage(mstrModuleName, 5, "Please enter EWS Domain.")
			Language.setMessage(mstrModuleName, 6, "Please enter Mail Server.")
			Language.setMessage(mstrModuleName, 7, "Please enter Mail Server Port.")
			Language.setMessage(mstrModuleName, 8, "Please enter To Email.")
			Language.setMessage(mstrModuleName, 9, "Mail sent Successfully...")
			Language.setMessage(mstrModuleName, 10, "Please check your Internet settings.")
			Language.setMessage(mstrModuleName, 11, "Please Uncheck Login using SSL setting.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class