﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmail_setup
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmail_setup))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmailSetup = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtToEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblToEmail = New System.Windows.Forms.Label
        Me.chkBypassProxyServer = New System.Windows.Forms.CheckBox
        Me.pnlExchangeWebService = New System.Windows.Forms.Panel
        Me.txtEWSDomain = New eZee.TextBox.AlphanumericTextBox
        Me.txtEWSUrl = New eZee.TextBox.AlphanumericTextBox
        Me.lblEWSDomain = New System.Windows.Forms.Label
        Me.lblEWSUrl = New System.Windows.Forms.Label
        Me.chkCertificateAuthentication = New System.Windows.Forms.CheckBox
        Me.btnTestMail = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblTestingMessage = New System.Windows.Forms.Label
        Me.lblTestingTitle = New System.Windows.Forms.Label
        Me.txtBody = New eZee.TextBox.AlphanumericTextBox
        Me.lblBody = New System.Windows.Forms.Label
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.txtPassword = New eZee.TextBox.AlphanumericTextBox
        Me.txtUserName = New eZee.TextBox.AlphanumericTextBox
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblPassword = New System.Windows.Forms.Label
        Me.chkSSL = New System.Windows.Forms.CheckBox
        Me.lblUserInformation = New System.Windows.Forms.Label
        Me.txtMailServerPort = New eZee.TextBox.AlphanumericTextBox
        Me.lblMailServerPort = New System.Windows.Forms.Label
        Me.lblMailServer = New System.Windows.Forms.Label
        Me.txtMailServer = New eZee.TextBox.AlphanumericTextBox
        Me.lblServerInformation = New System.Windows.Forms.Label
        Me.lblReference = New System.Windows.Forms.Label
        Me.txtReference = New eZee.TextBox.AlphanumericTextBox
        Me.lblSenderInformation = New System.Windows.Forms.Label
        Me.lblSenderName = New System.Windows.Forms.Label
        Me.lblEmailAddress = New System.Windows.Forms.Label
        Me.txtMailSenderEmail = New eZee.TextBox.AlphanumericTextBox
        Me.txtMailSenderName = New eZee.TextBox.AlphanumericTextBox
        Me.radExchangeWebService = New System.Windows.Forms.RadioButton
        Me.radSMTP = New System.Windows.Forms.RadioButton
        Me.cboAuthenticationProtocol = New System.Windows.Forms.ComboBox
        Me.lblAuthenticationProtocol = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.gbEmailSetup.SuspendLayout()
        Me.pnlExchangeWebService.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 396)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(653, 55)
        Me.EZeeFooter1.TabIndex = 18
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(551, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(455, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbEmailSetup
        '
        Me.gbEmailSetup.BorderColor = System.Drawing.Color.Black
        Me.gbEmailSetup.Checked = False
        Me.gbEmailSetup.CollapseAllExceptThis = False
        Me.gbEmailSetup.CollapsedHoverImage = Nothing
        Me.gbEmailSetup.CollapsedNormalImage = Nothing
        Me.gbEmailSetup.CollapsedPressedImage = Nothing
        Me.gbEmailSetup.CollapseOnLoad = False
        Me.gbEmailSetup.Controls.Add(Me.cboAuthenticationProtocol)
        Me.gbEmailSetup.Controls.Add(Me.lblAuthenticationProtocol)
        Me.gbEmailSetup.Controls.Add(Me.txtToEmail)
        Me.gbEmailSetup.Controls.Add(Me.lblToEmail)
        Me.gbEmailSetup.Controls.Add(Me.chkBypassProxyServer)
        Me.gbEmailSetup.Controls.Add(Me.pnlExchangeWebService)
        Me.gbEmailSetup.Controls.Add(Me.chkCertificateAuthentication)
        Me.gbEmailSetup.Controls.Add(Me.btnTestMail)
        Me.gbEmailSetup.Controls.Add(Me.lblTestingMessage)
        Me.gbEmailSetup.Controls.Add(Me.lblTestingTitle)
        Me.gbEmailSetup.Controls.Add(Me.txtBody)
        Me.gbEmailSetup.Controls.Add(Me.lblBody)
        Me.gbEmailSetup.Controls.Add(Me.objLine2)
        Me.gbEmailSetup.Controls.Add(Me.txtPassword)
        Me.gbEmailSetup.Controls.Add(Me.txtUserName)
        Me.gbEmailSetup.Controls.Add(Me.lblUserName)
        Me.gbEmailSetup.Controls.Add(Me.lblPassword)
        Me.gbEmailSetup.Controls.Add(Me.chkSSL)
        Me.gbEmailSetup.Controls.Add(Me.lblUserInformation)
        Me.gbEmailSetup.Controls.Add(Me.txtMailServerPort)
        Me.gbEmailSetup.Controls.Add(Me.lblMailServerPort)
        Me.gbEmailSetup.Controls.Add(Me.lblMailServer)
        Me.gbEmailSetup.Controls.Add(Me.txtMailServer)
        Me.gbEmailSetup.Controls.Add(Me.lblServerInformation)
        Me.gbEmailSetup.Controls.Add(Me.lblReference)
        Me.gbEmailSetup.Controls.Add(Me.txtReference)
        Me.gbEmailSetup.Controls.Add(Me.lblSenderInformation)
        Me.gbEmailSetup.Controls.Add(Me.lblSenderName)
        Me.gbEmailSetup.Controls.Add(Me.lblEmailAddress)
        Me.gbEmailSetup.Controls.Add(Me.txtMailSenderEmail)
        Me.gbEmailSetup.Controls.Add(Me.txtMailSenderName)
        Me.gbEmailSetup.Controls.Add(Me.radExchangeWebService)
        Me.gbEmailSetup.Controls.Add(Me.radSMTP)
        Me.gbEmailSetup.ExpandedHoverImage = Nothing
        Me.gbEmailSetup.ExpandedNormalImage = Nothing
        Me.gbEmailSetup.ExpandedPressedImage = Nothing
        Me.gbEmailSetup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmailSetup.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmailSetup.HeaderHeight = 25
        Me.gbEmailSetup.HeaderMessage = ""
        Me.gbEmailSetup.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmailSetup.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmailSetup.HeightOnCollapse = 0
        Me.gbEmailSetup.LeftTextSpace = 0
        Me.gbEmailSetup.Location = New System.Drawing.Point(12, 12)
        Me.gbEmailSetup.Name = "gbEmailSetup"
        Me.gbEmailSetup.OpenHeight = 300
        Me.gbEmailSetup.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmailSetup.ShowBorder = True
        Me.gbEmailSetup.ShowCheckBox = False
        Me.gbEmailSetup.ShowCollapseButton = False
        Me.gbEmailSetup.ShowDefaultBorderColor = True
        Me.gbEmailSetup.ShowDownButton = False
        Me.gbEmailSetup.ShowHeader = True
        Me.gbEmailSetup.Size = New System.Drawing.Size(629, 378)
        Me.gbEmailSetup.TabIndex = 19
        Me.gbEmailSetup.Temp = 0
        Me.gbEmailSetup.Text = "Email Setup"
        Me.gbEmailSetup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtToEmail
        '
        Me.txtToEmail.BackColor = System.Drawing.Color.White
        Me.txtToEmail.Flags = 0
        Me.txtToEmail.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtToEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtToEmail.Location = New System.Drawing.Point(382, 50)
        Me.txtToEmail.Name = "txtToEmail"
        Me.txtToEmail.Size = New System.Drawing.Size(226, 21)
        Me.txtToEmail.TabIndex = 162
        Me.txtToEmail.Tag = "name"
        '
        'lblToEmail
        '
        Me.lblToEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToEmail.Location = New System.Drawing.Point(381, 33)
        Me.lblToEmail.Name = "lblToEmail"
        Me.lblToEmail.Size = New System.Drawing.Size(124, 15)
        Me.lblToEmail.TabIndex = 161
        Me.lblToEmail.Text = "To Email"
        '
        'chkBypassProxyServer
        '
        Me.chkBypassProxyServer.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkBypassProxyServer.Location = New System.Drawing.Point(140, 331)
        Me.chkBypassProxyServer.Name = "chkBypassProxyServer"
        Me.chkBypassProxyServer.Size = New System.Drawing.Size(106, 17)
        Me.chkBypassProxyServer.TabIndex = 159
        Me.chkBypassProxyServer.Text = "Bypass Proxy"
        Me.chkBypassProxyServer.UseVisualStyleBackColor = True
        '
        'pnlExchangeWebService
        '
        Me.pnlExchangeWebService.Controls.Add(Me.txtEWSDomain)
        Me.pnlExchangeWebService.Controls.Add(Me.txtEWSUrl)
        Me.pnlExchangeWebService.Controls.Add(Me.lblEWSDomain)
        Me.pnlExchangeWebService.Controls.Add(Me.lblEWSUrl)
        Me.pnlExchangeWebService.Location = New System.Drawing.Point(217, 152)
        Me.pnlExchangeWebService.Name = "pnlExchangeWebService"
        Me.pnlExchangeWebService.Size = New System.Drawing.Size(145, 57)
        Me.pnlExchangeWebService.TabIndex = 158
        Me.pnlExchangeWebService.Visible = False
        '
        'txtEWSDomain
        '
        Me.txtEWSDomain.BackColor = System.Drawing.Color.White
        Me.txtEWSDomain.Flags = 0
        Me.txtEWSDomain.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtEWSDomain.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEWSDomain.Location = New System.Drawing.Point(136, 31)
        Me.txtEWSDomain.Name = "txtEWSDomain"
        Me.txtEWSDomain.Size = New System.Drawing.Size(211, 21)
        Me.txtEWSDomain.TabIndex = 118
        Me.txtEWSDomain.Tag = "name"
        '
        'txtEWSUrl
        '
        Me.txtEWSUrl.BackColor = System.Drawing.Color.White
        Me.txtEWSUrl.Flags = 0
        Me.txtEWSUrl.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtEWSUrl.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEWSUrl.Location = New System.Drawing.Point(136, 4)
        Me.txtEWSUrl.Name = "txtEWSUrl"
        Me.txtEWSUrl.Size = New System.Drawing.Size(211, 21)
        Me.txtEWSUrl.TabIndex = 117
        Me.txtEWSUrl.Tag = "name"
        '
        'lblEWSDomain
        '
        Me.lblEWSDomain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEWSDomain.Location = New System.Drawing.Point(5, 34)
        Me.lblEWSDomain.Name = "lblEWSDomain"
        Me.lblEWSDomain.Size = New System.Drawing.Size(124, 15)
        Me.lblEWSDomain.TabIndex = 116
        Me.lblEWSDomain.Text = "EWS Domain"
        '
        'lblEWSUrl
        '
        Me.lblEWSUrl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEWSUrl.Location = New System.Drawing.Point(6, 8)
        Me.lblEWSUrl.Name = "lblEWSUrl"
        Me.lblEWSUrl.Size = New System.Drawing.Size(124, 15)
        Me.lblEWSUrl.TabIndex = 115
        Me.lblEWSUrl.Text = "EWS URL"
        '
        'chkCertificateAuthentication
        '
        Me.chkCertificateAuthentication.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkCertificateAuthentication.Location = New System.Drawing.Point(140, 308)
        Me.chkCertificateAuthentication.Name = "chkCertificateAuthentication"
        Me.chkCertificateAuthentication.Size = New System.Drawing.Size(170, 17)
        Me.chkCertificateAuthentication.TabIndex = 157
        Me.chkCertificateAuthentication.Text = "Use Certificate Authentication"
        Me.chkCertificateAuthentication.UseVisualStyleBackColor = True
        '
        'btnTestMail
        '
        Me.btnTestMail.BackColor = System.Drawing.Color.White
        Me.btnTestMail.BackgroundImage = CType(resources.GetObject("btnTestMail.BackgroundImage"), System.Drawing.Image)
        Me.btnTestMail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTestMail.BorderColor = System.Drawing.Color.Empty
        Me.btnTestMail.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnTestMail.FlatAppearance.BorderSize = 0
        Me.btnTestMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTestMail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTestMail.ForeColor = System.Drawing.Color.Black
        Me.btnTestMail.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTestMail.GradientForeColor = System.Drawing.Color.Black
        Me.btnTestMail.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestMail.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTestMail.Location = New System.Drawing.Point(382, 288)
        Me.btnTestMail.Name = "btnTestMail"
        Me.btnTestMail.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestMail.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTestMail.Size = New System.Drawing.Size(226, 44)
        Me.btnTestMail.TabIndex = 156
        Me.btnTestMail.Text = "Test Mail Settings"
        Me.btnTestMail.UseVisualStyleBackColor = True
        '
        'lblTestingMessage
        '
        Me.lblTestingMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTestingMessage.Location = New System.Drawing.Point(412, 214)
        Me.lblTestingMessage.Name = "lblTestingMessage"
        Me.lblTestingMessage.Size = New System.Drawing.Size(193, 68)
        Me.lblTestingMessage.TabIndex = 155
        Me.lblTestingMessage.Text = "After Filling out the information on this screen, we recommend you test your Emai" & _
            "l Information by clicking button below. (Required Internet connection)"
        '
        'lblTestingTitle
        '
        Me.lblTestingTitle.Location = New System.Drawing.Point(379, 189)
        Me.lblTestingTitle.Name = "lblTestingTitle"
        Me.lblTestingTitle.Size = New System.Drawing.Size(192, 17)
        Me.lblTestingTitle.TabIndex = 154
        Me.lblTestingTitle.Text = "Testing Email Settings"
        '
        'txtBody
        '
        Me.txtBody.Flags = 0
        Me.txtBody.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBody.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBody.Location = New System.Drawing.Point(382, 107)
        Me.txtBody.Multiline = True
        Me.txtBody.Name = "txtBody"
        Me.txtBody.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBody.Size = New System.Drawing.Size(226, 75)
        Me.txtBody.TabIndex = 153
        '
        'lblBody
        '
        Me.lblBody.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBody.Location = New System.Drawing.Point(379, 90)
        Me.lblBody.Name = "lblBody"
        Me.lblBody.Size = New System.Drawing.Size(220, 13)
        Me.lblBody.TabIndex = 152
        Me.lblBody.Text = "Body"
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(368, 36)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(8, 337)
        Me.objLine2.TabIndex = 151
        Me.objLine2.Text = "EZeeStraightLine2"
        '
        'txtPassword
        '
        Me.txtPassword.Flags = 0
        Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtPassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPassword.Location = New System.Drawing.Point(140, 258)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(211, 21)
        Me.txtPassword.TabIndex = 149
        Me.txtPassword.Tag = "name"
        '
        'txtUserName
        '
        Me.txtUserName.BackColor = System.Drawing.Color.White
        Me.txtUserName.Flags = 0
        Me.txtUserName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtUserName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtUserName.Location = New System.Drawing.Point(140, 231)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(211, 21)
        Me.txtUserName.TabIndex = 147
        Me.txtUserName.Tag = "name"
        '
        'lblUserName
        '
        Me.lblUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(10, 234)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(124, 15)
        Me.lblUserName.TabIndex = 146
        Me.lblUserName.Text = "Email"
        '
        'lblPassword
        '
        Me.lblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(10, 261)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(124, 15)
        Me.lblPassword.TabIndex = 148
        Me.lblPassword.Text = "Password"
        '
        'chkSSL
        '
        Me.chkSSL.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkSSL.Location = New System.Drawing.Point(140, 285)
        Me.chkSSL.Name = "chkSSL"
        Me.chkSSL.Size = New System.Drawing.Size(106, 17)
        Me.chkSSL.TabIndex = 150
        Me.chkSSL.Text = "Login using SSL"
        Me.chkSSL.UseVisualStyleBackColor = True
        '
        'lblUserInformation
        '
        Me.lblUserInformation.Location = New System.Drawing.Point(8, 210)
        Me.lblUserInformation.Name = "lblUserInformation"
        Me.lblUserInformation.Size = New System.Drawing.Size(298, 17)
        Me.lblUserInformation.TabIndex = 145
        Me.lblUserInformation.Text = "Authentication Information"
        '
        'txtMailServerPort
        '
        Me.txtMailServerPort.Flags = 0
        Me.txtMailServerPort.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMailServerPort.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMailServerPort.Location = New System.Drawing.Point(140, 181)
        Me.txtMailServerPort.Name = "txtMailServerPort"
        Me.txtMailServerPort.Size = New System.Drawing.Size(41, 21)
        Me.txtMailServerPort.TabIndex = 144
        Me.txtMailServerPort.Tag = "name"
        Me.txtMailServerPort.Text = "25"
        Me.txtMailServerPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMailServerPort
        '
        Me.lblMailServerPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMailServerPort.Location = New System.Drawing.Point(10, 186)
        Me.lblMailServerPort.Name = "lblMailServerPort"
        Me.lblMailServerPort.Size = New System.Drawing.Size(124, 15)
        Me.lblMailServerPort.TabIndex = 143
        Me.lblMailServerPort.Text = "Mail Server Port"
        '
        'lblMailServer
        '
        Me.lblMailServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMailServer.Location = New System.Drawing.Point(10, 159)
        Me.lblMailServer.Name = "lblMailServer"
        Me.lblMailServer.Size = New System.Drawing.Size(124, 15)
        Me.lblMailServer.TabIndex = 141
        Me.lblMailServer.Text = "Mail Server (SMTP) / IP"
        '
        'txtMailServer
        '
        Me.txtMailServer.BackColor = System.Drawing.Color.White
        Me.txtMailServer.Flags = 0
        Me.txtMailServer.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMailServer.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMailServer.Location = New System.Drawing.Point(140, 156)
        Me.txtMailServer.Name = "txtMailServer"
        Me.txtMailServer.Size = New System.Drawing.Size(211, 21)
        Me.txtMailServer.TabIndex = 142
        Me.txtMailServer.Tag = "name"
        '
        'lblServerInformation
        '
        Me.lblServerInformation.Location = New System.Drawing.Point(8, 134)
        Me.lblServerInformation.Name = "lblServerInformation"
        Me.lblServerInformation.Size = New System.Drawing.Size(298, 17)
        Me.lblServerInformation.TabIndex = 140
        Me.lblServerInformation.Text = "Server Information"
        '
        'lblReference
        '
        Me.lblReference.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReference.Location = New System.Drawing.Point(10, 110)
        Me.lblReference.Name = "lblReference"
        Me.lblReference.Size = New System.Drawing.Size(124, 15)
        Me.lblReference.TabIndex = 138
        Me.lblReference.Text = "Subject"
        '
        'txtReference
        '
        Me.txtReference.Flags = 0
        Me.txtReference.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtReference.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReference.Location = New System.Drawing.Point(140, 107)
        Me.txtReference.Name = "txtReference"
        Me.txtReference.Size = New System.Drawing.Size(211, 21)
        Me.txtReference.TabIndex = 139
        Me.txtReference.Tag = "name"
        '
        'lblSenderInformation
        '
        Me.lblSenderInformation.Location = New System.Drawing.Point(8, 36)
        Me.lblSenderInformation.Name = "lblSenderInformation"
        Me.lblSenderInformation.Size = New System.Drawing.Size(298, 13)
        Me.lblSenderInformation.TabIndex = 133
        Me.lblSenderInformation.Text = "Sender Information"
        '
        'lblSenderName
        '
        Me.lblSenderName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSenderName.Location = New System.Drawing.Point(10, 56)
        Me.lblSenderName.Name = "lblSenderName"
        Me.lblSenderName.Size = New System.Drawing.Size(124, 15)
        Me.lblSenderName.TabIndex = 134
        Me.lblSenderName.Text = "Sender Display Name"
        '
        'lblEmailAddress
        '
        Me.lblEmailAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmailAddress.Location = New System.Drawing.Point(10, 83)
        Me.lblEmailAddress.Name = "lblEmailAddress"
        Me.lblEmailAddress.Size = New System.Drawing.Size(124, 15)
        Me.lblEmailAddress.TabIndex = 136
        Me.lblEmailAddress.Text = "From Email"
        '
        'txtMailSenderEmail
        '
        Me.txtMailSenderEmail.Flags = 0
        Me.txtMailSenderEmail.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMailSenderEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMailSenderEmail.Location = New System.Drawing.Point(140, 80)
        Me.txtMailSenderEmail.Name = "txtMailSenderEmail"
        Me.txtMailSenderEmail.Size = New System.Drawing.Size(211, 21)
        Me.txtMailSenderEmail.TabIndex = 137
        Me.txtMailSenderEmail.Tag = "name"
        '
        'txtMailSenderName
        '
        Me.txtMailSenderName.BackColor = System.Drawing.Color.White
        Me.txtMailSenderName.Flags = 0
        Me.txtMailSenderName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMailSenderName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMailSenderName.Location = New System.Drawing.Point(140, 53)
        Me.txtMailSenderName.Name = "txtMailSenderName"
        Me.txtMailSenderName.Size = New System.Drawing.Size(211, 21)
        Me.txtMailSenderName.TabIndex = 135
        Me.txtMailSenderName.Tag = "name"
        '
        'radExchangeWebService
        '
        Me.radExchangeWebService.BackColor = System.Drawing.Color.Transparent
        Me.radExchangeWebService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExchangeWebService.Location = New System.Drawing.Point(244, 0)
        Me.radExchangeWebService.Name = "radExchangeWebService"
        Me.radExchangeWebService.Size = New System.Drawing.Size(178, 24)
        Me.radExchangeWebService.TabIndex = 104
        Me.radExchangeWebService.Text = "Exchange Web Service (EWS)"
        Me.radExchangeWebService.UseVisualStyleBackColor = False
        '
        'radSMTP
        '
        Me.radSMTP.BackColor = System.Drawing.Color.Transparent
        Me.radSMTP.Checked = True
        Me.radSMTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSMTP.Location = New System.Drawing.Point(152, 0)
        Me.radSMTP.Name = "radSMTP"
        Me.radSMTP.Size = New System.Drawing.Size(86, 24)
        Me.radSMTP.TabIndex = 103
        Me.radSMTP.TabStop = True
        Me.radSMTP.Text = "SMTP"
        Me.radSMTP.UseVisualStyleBackColor = False
        '
        'cboAuthenticationProtocol
        '
        Me.cboAuthenticationProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuthenticationProtocol.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAuthenticationProtocol.FormattingEnabled = True
        Me.cboAuthenticationProtocol.Location = New System.Drawing.Point(140, 351)
        Me.cboAuthenticationProtocol.Name = "cboAuthenticationProtocol"
        Me.cboAuthenticationProtocol.Size = New System.Drawing.Size(119, 21)
        Me.cboAuthenticationProtocol.TabIndex = 165
        '
        'lblAuthenticationProtocol
        '
        Me.lblAuthenticationProtocol.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuthenticationProtocol.Location = New System.Drawing.Point(10, 354)
        Me.lblAuthenticationProtocol.Name = "lblAuthenticationProtocol"
        Me.lblAuthenticationProtocol.Size = New System.Drawing.Size(124, 17)
        Me.lblAuthenticationProtocol.TabIndex = 164
        Me.lblAuthenticationProtocol.Text = "Authentication Protocol"
        '
        'frmEmail_setup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 451)
        Me.Controls.Add(Me.gbEmailSetup)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmail_setup"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Email Setup"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbEmailSetup.ResumeLayout(False)
        Me.gbEmailSetup.PerformLayout()
        Me.pnlExchangeWebService.ResumeLayout(False)
        Me.pnlExchangeWebService.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmailSetup As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radExchangeWebService As System.Windows.Forms.RadioButton
    Friend WithEvents radSMTP As System.Windows.Forms.RadioButton
    Friend WithEvents chkBypassProxyServer As System.Windows.Forms.CheckBox
    Friend WithEvents pnlExchangeWebService As System.Windows.Forms.Panel
    Friend WithEvents txtEWSDomain As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEWSUrl As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEWSDomain As System.Windows.Forms.Label
    Friend WithEvents lblEWSUrl As System.Windows.Forms.Label
    Friend WithEvents chkCertificateAuthentication As System.Windows.Forms.CheckBox
    Friend WithEvents btnTestMail As eZee.Common.eZeeLightButton
    Friend WithEvents lblTestingMessage As System.Windows.Forms.Label
    Friend WithEvents lblTestingTitle As System.Windows.Forms.Label
    Friend WithEvents txtBody As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBody As System.Windows.Forms.Label
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents txtPassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtUserName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents chkSSL As System.Windows.Forms.CheckBox
    Friend WithEvents lblUserInformation As System.Windows.Forms.Label
    Friend WithEvents txtMailServerPort As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMailServerPort As System.Windows.Forms.Label
    Friend WithEvents lblMailServer As System.Windows.Forms.Label
    Friend WithEvents txtMailServer As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblServerInformation As System.Windows.Forms.Label
    Friend WithEvents lblReference As System.Windows.Forms.Label
    Friend WithEvents txtReference As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSenderInformation As System.Windows.Forms.Label
    Friend WithEvents lblSenderName As System.Windows.Forms.Label
    Friend WithEvents lblEmailAddress As System.Windows.Forms.Label
    Friend WithEvents txtMailSenderEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMailSenderName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtToEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblToEmail As System.Windows.Forms.Label
    Friend WithEvents cboAuthenticationProtocol As System.Windows.Forms.ComboBox
    Friend WithEvents lblAuthenticationProtocol As System.Windows.Forms.Label
End Class
