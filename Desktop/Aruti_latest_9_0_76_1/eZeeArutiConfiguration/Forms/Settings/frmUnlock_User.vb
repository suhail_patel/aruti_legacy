﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmUnlock_User

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmUnlock_User"
    Private objUAL As clsUser_Acc_Lock
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem = Nothing
        Try
            dsList = objUAL.GetList("List")

            lvUserList.Items.Clear()

            If dsList.Tables(0).Rows.Count <= 0 Then Exit Sub

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = ""

                lvItem.SubItems.Add(dtRow.Item("Uname").ToString)
                lvItem.SubItems.Add(dtRow.Item("ip").ToString)
                lvItem.SubItems.Add(dtRow.Item("machine_name").ToString)
                'S.SANDEEP [ 09 MAR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If dtRow.Item("attempt_date").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("attempt_date").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If
                'S.SANDEEP [ 09 MAR 2013 ] -- END

                lvItem.Tag = dtRow.Item("userunkid")

                lvUserList.Items.Add(lvItem)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillList ; Module Name : " & mstrModuleName)
        Finally
            dsList.Dispose()
            lvItem = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmUnlock_User_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objUAL = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmUnlock_User_FormClosed ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmUnlock_User_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objUAL = New clsUser_Acc_Lock
        Try
            Call Set_Logo(Me, enArutiApplicatinType.Aruti_Configuration)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call FillList()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmUnlock_User_Load ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure :  ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnUnlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Try
            For Each lvItem As ListViewItem In lvUserList.CheckedItems
                'S.SANDEEP [ 09 MAR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If lvItem.SubItems(colhAttempt_Date.Index).Text.Trim.Length > 0 Then
                    objUAL._Attempt_Date = CDate(lvItem.SubItems(colhAttempt_Date.Index).Text)
                End If
                'S.SANDEEP [ 09 MAR 2013 ] -- END
                objUAL._Userunkid = CInt(lvItem.Tag)
                objUAL._Isunlocked = True
                objUAL._Unlockuserunkid = User._Object._Userunkid
                Call objUAL.Update()

                Dim objUser As New clsUserAddEdit

                objUser._Userunkid = CInt(lvItem.Tag)
                objUser._IsAcc_Locked = False
                objUser._Locked_Time = Nothing
                objUser._LockedDuration = Nothing

                'S.SANDEEP [ 24 JUNE 2011 ] -- START
                'ISSUE : DELETION OF PRIVILEGE WHILE UNLOCKING USER
                'objUser.Update()
                objUser.Update(True)
                'S.SANDEEP [ 24 JUNE 2011 ] -- END 
            Next

            Call FillList()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure :  ; Module Name : " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvUserList.ItemChecked, AddressOf lvUserList_ItemChecked
            For Each lvItem As ListViewItem In lvUserList.Items
                lvItem.Checked = CBool(objchkAll.CheckState)
            Next
            AddHandler lvUserList.ItemChecked, AddressOf lvUserList_ItemChecked
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : objchkAll_CheckedChanged ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvUserList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvUserList.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvUserList.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvUserList.CheckedItems.Count < lvUserList.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvUserList.CheckedItems.Count = lvUserList.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : lvUserList_ItemChecked ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnUnlock.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnlock.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnUnlock.Text = Language._Object.getCaption(Me.btnUnlock.Name, Me.btnUnlock.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhUser.Text = Language._Object.getCaption(CStr(Me.colhUser.Tag), Me.colhUser.Text)
			Me.colhIP.Text = Language._Object.getCaption(CStr(Me.colhIP.Tag), Me.colhIP.Text)
			Me.colhHost.Text = Language._Object.getCaption(CStr(Me.colhHost.Tag), Me.colhHost.Text)
			Me.colhAttempt_Date.Text = Language._Object.getCaption(CStr(Me.colhAttempt_Date.Tag), Me.colhAttempt_Date.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class