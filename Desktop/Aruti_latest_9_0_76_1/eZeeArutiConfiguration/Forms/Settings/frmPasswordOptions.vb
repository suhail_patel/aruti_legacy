﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.DirectoryServices
Imports System.IO

#End Region

Public Class frmPasswordOptions

    Private ReadOnly mstrModuleName As String = "frmPasswordOptions"
    Private mblnCancel As Boolean = True
    Private objPasswdOption As clsPassowdOptions
    Private mdtTranTable As DataTable
    'S.SANDEEP [20-Feb-2018] -- START
    'ISSUE/ENHANCEMENT : {#0001984}
    Private mintExpDays As Integer = 0
    Private mintOldExpDays As Integer = 0
    'S.SANDEEP [20-Feb-2018] -- END


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mblnATADUserFromEmpMst As Boolean = False
    Private mblnATUserMustchangePwdOnNextLogon As Boolean = False
    'Pinkal (18-Aug-2018) -- End


    'Pinkal (27-Nov-2020) -- Start
    'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
    Private mblnATAllowEmpSystemAccessOnActualEOCRetirementDate As Boolean = False
    'Pinkal (27-Nov-2020) -- End


    'Pinkal (04-Jan-2021) -- Start
    'Enhancement NMB - Working Announcements for Self service Login Screen.
    Private mdtAnnouncementDoc As DataTable = Nothing
    Dim rtbTemp As New RichTextBox
    'Pinkal (04-Jan-2021) -- End


#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : displayDialog ; Module Name : " & mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            nudAttempts.BackColor = GUI.ColorComp
            nudMaxDays.BackColor = GUI.ColorComp
            nudMinDays.BackColor = GUI.ColorComp
            nudMinLength.BackColor = GUI.ColorComp
            txtRetrydurattion.BackColor = GUI.ColorComp
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetColor ; Module Name : " & mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If mdtTranTable.Rows.Count > 0 Then
                For Each dtRow As DataRow In mdtTranTable.Rows
                    Select Case CInt(dtRow.Item("passwdoptionid"))

                        Case enPasswordOption.PASSWD_EXPIRATION
                            gbPassowordExpiration.Checked = True

                            'Pinkal (20-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            If CInt(dtRow.Item("passwdminlen")) <= nudMinDays.Minimum Then
                                nudMinDays.Minimum = CInt(dtRow.Item("passwdminlen"))
                            End If

                            If CInt(dtRow.Item("passwdminlen")) >= nudMinDays.Maximum Then
                                nudMinDays.Maximum = CInt(dtRow.Item("passwdminlen"))
                            End If

                            If CInt(dtRow.Item("passwdminlen")) <= nudMaxDays.Minimum Then
                                nudMaxDays.Minimum = CInt(dtRow.Item("passwdminlen"))
                            End If

                            If CInt(dtRow.Item("passwdmaxlen")) >= nudMaxDays.Maximum Then
                                nudMaxDays.Maximum = CInt(dtRow.Item("passwdmaxlen"))
                            End If
                            'Pinkal (20-Aug-2020) -- End

                            nudMinDays.Value = CInt(dtRow.Item("passwdminlen"))
                            nudMaxDays.Value = CInt(dtRow.Item("passwdmaxlen"))

                        Case enPasswordOption.PASSWD_LENGTH
                            gbPasswordLength.Checked = True


                            'Pinkal (20-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            If CInt(dtRow.Item("passwdminlen")) <= nudMinLength.Minimum Then
                                nudMinLength.Minimum = CInt(dtRow.Item("passwdminlen"))
                            End If
                            If CInt(dtRow.Item("passwdminlen")) >= nudMinLength.Maximum Then
                                nudMinLength.Maximum = CInt(dtRow.Item("passwdminlen"))
                            End If
                            'Pinkal (20-Aug-2020) -- End

                            nudMinLength.Value = CInt(dtRow.Item("passwdminlen"))

                        Case enPasswordOption.PASSWD_ACC_LOCKOUT
                            gbAccountLock.Checked = True

                            'Pinkal (20-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            If CInt(dtRow.Item("lockuserafter")) <= nudAttempts.Minimum Then
                                nudAttempts.Minimum = CInt(dtRow.Item("lockuserafter"))
                            End If
                            If CInt(dtRow.Item("lockuserafter")) >= nudAttempts.Maximum Then
                                nudAttempts.Maximum = CInt(dtRow.Item("lockuserafter"))
                            End If
                            'Pinkal (20-Aug-2020) -- End


                            nudAttempts.Value = CInt(dtRow.Item("lockuserafter"))

                            If CInt(dtRow.Item("allowuserretry")) > 0 Then
                                txtRetrydurattion.Int = CInt(dtRow.Item("allowuserretry"))
                                txtRetrydurattion.Enabled = True
                                radLoginRetry.Checked = True
                                radAdminUnlock.Checked = False
                            Else
                                txtRetrydurattion.Int = 20
                                txtRetrydurattion.Enabled = False
                                radLoginRetry.Checked = False
                                radAdminUnlock.Checked = True
                            End If

                            'Pinkal (21-jun-2011)  --Start
                        Case enPasswordOption.APPLIC_IDLE_STATE

                            gbApplicationIdleSetting.Checked = True
                            txtIdleMinutes.Text = dtRow.Item("passwdminlen").ToString
                            'Pinkal (21-jun-2011)  --End

                            'S.SANDEEP [ 07 NOV 2011 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                        Case enPasswordOption.USER_LOGIN_MODE
                            gbUserLoginOptions.Checked = True

                            Select Case CInt(dtRow.Item("passwdminlen"))
                                Case enAuthenticationMode.BASIC_AUTHENTICATION
                                    radBasicAuthentication.Checked = True
                                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    chkImportEmployee.Checked = CBool(dtRow.Item("isemp_user"))
                                    'S.SANDEEP [ 01 DEC 2012 ] -- END
                                Case enAuthenticationMode.AD_BASIC_AUTHENTICATION
                                    radADAuthentication.Checked = True
                                Case enAuthenticationMode.AD_SSO_AUTHENTICATION
                                    radSSOAuthentication.Checked = True
                                Case Else
                                    radBasicAuthentication.Checked = True
                            End Select
                            'S.SANDEEP [ 07 NOV 2011 ] -- END

                            'S.SANDEEP [10 AUG 2015] -- START
                            'ENHANCEMENT : Aruti SaaS Changes
                            chkEnableAllUser.Checked = CBool(dtRow.Item("isenablealluser"))
                            'S.SANDEEP [10 AUG 2015] -- END


                            'S.SANDEEP [ 01 NOV 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                        Case enPasswordOption.USER_NAME_LENGTH
                            gbUnameLength.Checked = True

                            'Pinkal (20-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            If CInt(dtRow.Item("passwdminlen")) <= nudMinLength.Minimum Then
                                nudUMinLength.Minimum = CInt(dtRow.Item("passwdminlen"))
                            End If
                            If CInt(dtRow.Item("passwdminlen")) >= nudMinLength.Maximum Then
                                nudUMinLength.Maximum = CInt(dtRow.Item("passwdminlen"))
                            End If
                            'Pinkal (20-Aug-2020) -- End

                            nudUMinLength.Value = CInt(dtRow.Item("passwdminlen"))

                        Case enPasswordOption.ENF_PASSWD_POLICY
                            gbPasswordPolicy.Checked = True
                            Select Case CInt(dtRow.Item("passwdminlen"))
                                Case enPasswordPolicy.PASSWD_UCASE_CMPL
                                    chkUpperCaseMandatory.Checked = True
                                Case enPasswordPolicy.PASSWD_LCASE_CMPL
                                    chkLowerCaseMandatory.Checked = True
                                Case enPasswordPolicy.PASSWD_NCASE_CMPL
                                    chkNumericMandatory.Checked = True
                                Case enPasswordPolicy.PASSWD_SPLCH_CMPL
                                    chkSpecialCharsMandatory.Checked = True

                                    'Pinkal (18-Aug-2018) -- Start
                                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                Case enPasswordPolicy.PASSWD_COMPLEXITY
                                    chkPasswordComplexity.Checked = True
                                    'Pinkal (18-Aug-2018) -- End

                            End Select
                            'S.SANDEEP [ 01 NOV 2012 ] -- END

                            'Sohail (15 Apr 2013) -- Start
                            'TRA - ENHANCEMENT
                        Case enPasswordOption.PROHIBIT_PASSWD_CHANGE
                            chkProhibitPasswordChange.Checked = CBool(dtRow.Item("passwdminlen"))
                            'Sohail (15 Apr 2013) -- End

                            'Hemant (25 May 2021) -- Start
                            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
                        Case enPasswordOption.PASSWD_HISTORY
                            gbPasswordHistorySettings.Checked = True
                            nudNumberPassword.Value = CInt(dtRow.Item("passwdlastusednumber"))
                            nudDaysPassword.Value = CInt(dtRow.Item("passwdlastuseddays"))

                            If CInt(dtRow.Item("passwdlastusednumber")) > 0 Then
                                chkDonotAllowToReuseAnyOfTheLast.Checked = True
                            Else
                                chkDonotAllowToReuseAnyOfTheLast.Checked = False
                            End If

                            If CInt(dtRow.Item("passwdlastuseddays")) > 0 Then
                                chkDonotAllowReuseOfPasswordUsedInTheLast.Checked = True
                            Else
                                chkDonotAllowReuseOfPasswordUsedInTheLast.Checked = False
                            End If
                            'Hemant (25 May 2021) -- End

                    End Select
                Next
            Else
                gbPassowordExpiration.Checked = False
                gbPasswordLength.Checked = False
                gbAccountLock.Checked = False
                radLoginRetry.Checked = True
                txtRetrydurattion.Enabled = False
            End If



            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.

            'If radBasicAuthentication.Checked = False Then
            '    Dim objGSettings As New clsGeneralSettings
            '    objGSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
            '    txtADServerIP.Text = objGSettings._AD_ServerIP.Trim
            'End If

            'Pinkal (27-Nov-2020) -- Start
            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
            Dim objConfig As New clsConfigOptions
            'Pinkal (27-Nov-2020) -- End

            If radBasicAuthentication.Checked = False Then
                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                'Dim objConfig As New clsConfigOptions
                'Pinkal (27-Nov-2020) -- End

                objConfig.IsValue_Changed("ADIPAddress", "-999", txtADServerIP.Text)
                objConfig.IsValue_Changed("ADDomain", "-999", txtDomainName.Text)
                objConfig.IsValue_Changed("ADDomainUser", "-999", txtDomainUser.Text)

                'Pinkal (21-AUG-2017) -- Start
                'Bug solved when Active Directory Password is getting from database gave error.
                'objConfig.IsValue_Changed("ADDomainUserPwd", "-999", txtDomainPassword.Text.Trim())
                Dim mstrPassword As String = ""
                objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrPassword)
                If mstrPassword.Trim.Length > 0 Then txtDomainPassword.Text = clsSecurity.Decrypt(mstrPassword.Trim(), "ezee")
                'Pinkal (21-AUG-2017) -- Endf

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                Dim mstrADUserFromEmpMst As String = ""
                objConfig.IsValue_Changed("CreateADUserFromEmpMst", "-999", mstrADUserFromEmpMst)
                chkADUserFromEmpMst.Checked = CBool(IIf(mstrADUserFromEmpMst.Length <= 0, False, mstrADUserFromEmpMst))
                mblnATADUserFromEmpMst = chkADUserFromEmpMst.Checked

                If chkADUserFromEmpMst.Checked Then chkADUserFromEmpMst.Enabled = False

                Dim mstrUserMustchangePwdOnNextLogon As String = ""
                objConfig.IsValue_Changed("UserMustChangePwdOnNextLogon", "-999", mstrUserMustchangePwdOnNextLogon)
                chkUserChangePwdnextLogon.Checked = CBool(IIf(mstrUserMustchangePwdOnNextLogon.Length <= 0, False, mstrUserMustchangePwdOnNextLogon))
                mblnATUserMustchangePwdOnNextLogon = chkUserChangePwdnextLogon.Checked
                'Pinkal (18-Aug-2018) -- End


                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                'objConfig = Nothing
                'Pinkal (27-Nov-2020) -- End
                'Hemant (25 May 2021) -- Start
                'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
                gbPasswordHistorySettings.Enabled = False
                gbPasswordHistorySettings.Checked = False
                chkDonotAllowToReuseAnyOfTheLast.Checked = False
                chkDonotAllowReuseOfPasswordUsedInTheLast.Checked = False
                'Hemant (25 May 2021) -- End
            End If

            'Pinkal (03-Apr-2017) -- End

            'Pinkal (27-Nov-2020) -- Start
            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
            Dim mstrAllowEmpSystemAccessOnActualEOCRetirementDate As String = ""
            objConfig.IsValue_Changed("AllowEmpSystemAccessOnActualEOCRetirementDate", "-999", mstrAllowEmpSystemAccessOnActualEOCRetirementDate)
            chkAllowLoginOnActualEOCRetireDate.Checked = CBool(IIf(mstrAllowEmpSystemAccessOnActualEOCRetirementDate.Length <= 0, False, mstrAllowEmpSystemAccessOnActualEOCRetirementDate))
            mblnATAllowEmpSystemAccessOnActualEOCRetirementDate = chkAllowLoginOnActualEOCRetireDate.Checked

            objConfig = Nothing

            'Pinkal (27-Nov-2020) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetValue ; Module Name : " & mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            Dim dtRow As DataRow = Nothing
            '===========================================| PASSWD_EXPIRATION START |==========================================='
            If gbPassowordExpiration.Checked = True Then

                'S.SANDEEP [20-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001984}
                mintExpDays = CInt(nudMaxDays.Value)
                'S.SANDEEP [20-Feb-2018] -- END

                If mdtTranTable.Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    Dim drTemp() As DataRow = objPasswdOption._DataTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_EXPIRATION & "'")
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_EXPIRATION & "'")

                    If drTemp.Length > 0 Then
                        If dtTemp.Length > 0 Then
                            dtTemp(0)("passwdminlen") = nudMinDays.Value
                            dtTemp(0)("passwdmaxlen") = nudMaxDays.Value

                            If CInt(drTemp(0)("passwdminlen")) <> CInt(dtTemp(0)("passwdminlen")) Then
                                blnFlag = True
                            ElseIf CInt(drTemp(0)("passwdmaxlen")) <> CInt(dtTemp(0)("passwdmaxlen")) Then
                                mintOldExpDays = CInt(drTemp(0)("passwdmaxlen"))
                                blnFlag = True
                            End If

                            If blnFlag = True Then dtTemp(0)("AUD") = "U"

                            dtTemp(0)("GUID") = Guid.NewGuid.ToString
                            mdtTranTable.AcceptChanges()
                        End If
                    Else
                        dtRow = mdtTranTable.NewRow
                        dtRow.Item("passwdoptionid") = enPasswordOption.PASSWD_EXPIRATION
                        dtRow.Item("passwdminlen") = nudMinDays.Value
                        dtRow.Item("passwdmaxlen") = nudMaxDays.Value
                        dtRow.Item("AUD") = "A"
                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtTranTable.Rows.Add(dtRow)
                    End If
                Else
                    dtRow = mdtTranTable.NewRow
                    dtRow.Item("passwdoptionid") = enPasswordOption.PASSWD_EXPIRATION
                    dtRow.Item("passwdminlen") = nudMinDays.Value
                    dtRow.Item("passwdmaxlen") = nudMaxDays.Value
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtTranTable.Rows.Add(dtRow)
                End If
            Else
                If mdtTranTable.Rows.Count > 0 Then
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_EXPIRATION & "'")
                    If dtTemp.Length > 0 Then
                        dtTemp(0)("AUD") = "D"
                        mdtTranTable.AcceptChanges()
                    End If
                End If
                'S.SANDEEP [20-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001984}
                mintExpDays = -1
                'S.SANDEEP [20-Feb-2018] -- END
            End If
            '===========================================| PASSWD_EXPIRATION END |==========================================='

            '===========================================| PASSWD_LENGTH START |==========================================='
            If gbPasswordLength.Checked = True Then
                If mdtTranTable.Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    Dim drTemp() As DataRow = objPasswdOption._DataTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_LENGTH & "'")
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_LENGTH & "'")

                    If drTemp.Length > 0 Then
                        If dtTemp.Length > 0 Then
                            dtTemp(0)("passwdminlen") = nudMinLength.Value

                            If CInt(drTemp(0)("passwdminlen")) <> CInt(dtTemp(0)("passwdminlen")) Then
                                blnFlag = True
                            End If

                            If blnFlag = True Then dtTemp(0)("AUD") = "U"
                            dtTemp(0)("GUID") = Guid.NewGuid.ToString
                            mdtTranTable.AcceptChanges()
                        End If
                    Else
                        dtRow = mdtTranTable.NewRow
                        dtRow.Item("passwdoptionid") = enPasswordOption.PASSWD_LENGTH
                        dtRow.Item("passwdminlen") = nudMinLength.Value
                        dtRow.Item("AUD") = "A"
                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtTranTable.Rows.Add(dtRow)
                    End If
                Else
                    dtRow = mdtTranTable.NewRow
                    dtRow.Item("passwdoptionid") = enPasswordOption.PASSWD_LENGTH
                    dtRow.Item("passwdminlen") = nudMinLength.Value
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtTranTable.Rows.Add(dtRow)
                End If
            Else
                If mdtTranTable.Rows.Count > 0 Then
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_LENGTH & "'")
                    If dtTemp.Length > 0 Then
                        dtTemp(0)("AUD") = "D"
                        mdtTranTable.AcceptChanges()
                    End If
                End If
            End If
            '===========================================| PASSWD_LENGTH END |==========================================='

            '===========================================| PASSWD_ACC_LOCKOUT START |==========================================='
            If gbAccountLock.Checked = True Then

                If mdtTranTable.Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    Dim drTemp() As DataRow = objPasswdOption._DataTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_ACC_LOCKOUT & "'")
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_ACC_LOCKOUT & "'")

                    If drTemp.Length > 0 Then

                        If dtTemp.Length > 0 Then
                            dtTemp(0)("lockuserafter") = nudAttempts.Value
                            If radLoginRetry.Checked = True Then
                                dtTemp(0)("allowuserretry") = txtRetrydurattion.Int
                            Else
                                dtTemp(0)("allowuserretry") = -1
                            End If
                            dtTemp(0)("isunlockedbyadmin") = radAdminUnlock.Checked

                            If CInt(drTemp(0)("allowuserretry")) <> CInt(dtTemp(0)("allowuserretry")) Then
                                blnFlag = True
                            ElseIf CBool(drTemp(0)("isunlockedbyadmin")) <> CBool(dtTemp(0)("isunlockedbyadmin")) Then
                                blnFlag = True
                            End If

                            If blnFlag = True Then dtTemp(0)("AUD") = "U"

                            dtTemp(0)("GUID") = Guid.NewGuid.ToString
                            mdtTranTable.AcceptChanges()
                        End If

                    Else
                        dtRow = mdtTranTable.NewRow

                        dtRow.Item("passwdoptionid") = enPasswordOption.PASSWD_ACC_LOCKOUT
                        dtRow.Item("lockuserafter") = nudAttempts.Value
                        If radLoginRetry.Checked = True Then
                            dtRow.Item("allowuserretry") = txtRetrydurattion.Int
                        End If
                        dtRow.Item("isunlockedbyadmin") = radAdminUnlock.Checked
                        dtRow.Item("AUD") = "A"
                        dtRow.Item("GUID") = Guid.NewGuid.ToString

                        mdtTranTable.Rows.Add(dtRow)
                    End If
                Else
                    dtRow = mdtTranTable.NewRow

                    dtRow.Item("passwdoptionid") = enPasswordOption.PASSWD_ACC_LOCKOUT
                    dtRow.Item("lockuserafter") = nudAttempts.Value
                    If radLoginRetry.Checked = True Then
                        dtRow.Item("allowuserretry") = txtRetrydurattion.Int
                    End If
                    dtRow.Item("isunlockedbyadmin") = radAdminUnlock.Checked
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString

                    mdtTranTable.Rows.Add(dtRow)
                End If
            Else
                If mdtTranTable.Rows.Count > 0 Then
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_ACC_LOCKOUT & "'")
                    If dtTemp.Length > 0 Then
                        dtTemp(0)("AUD") = "D"
                        mdtTranTable.AcceptChanges()
                    End If
                End If
            End If
            '===========================================| PASSWD_ACC_LOCKOUT END |==========================================='

            'Pinkal (21-jun-2011)  --Start

            '===========================================| APPLICATION IDLE SETTING START |==========================================='


            If gbApplicationIdleSetting.Checked = True Then

                If mdtTranTable.Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    Dim drTemp() As DataRow = objPasswdOption._DataTable.Select("passwdoptionid = '" & enPasswordOption.APPLIC_IDLE_STATE & "'")
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.APPLIC_IDLE_STATE & "'")

                    If drTemp.Length > 0 Then
                        If dtTemp.Length > 0 Then
                            dtTemp(0)("passwdminlen") = CInt(txtIdleMinutes.Text)

                            If CInt(drTemp(0)("passwdminlen")) <> CInt(dtTemp(0)("passwdminlen")) Then
                                blnFlag = True
                            End If

                            If blnFlag = True Then dtTemp(0)("AUD") = "U"

                            dtTemp(0)("GUID") = Guid.NewGuid.ToString
                            mdtTranTable.AcceptChanges()
                        End If
                    Else
                        dtRow = mdtTranTable.NewRow
                        dtRow.Item("passwdoptionid") = enPasswordOption.APPLIC_IDLE_STATE
                        dtRow.Item("passwdminlen") = CInt(txtIdleMinutes.Text)
                        dtRow.Item("AUD") = "A"
                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtTranTable.Rows.Add(dtRow)
                    End If

                Else
                    dtRow = mdtTranTable.NewRow
                    dtRow.Item("passwdoptionid") = enPasswordOption.APPLIC_IDLE_STATE
                    dtRow.Item("passwdminlen") = CInt(txtIdleMinutes.Text)
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtTranTable.Rows.Add(dtRow)
                End If

            Else

                If mdtTranTable.Rows.Count > 0 Then
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.APPLIC_IDLE_STATE & "'")
                    If dtTemp.Length > 0 Then
                        dtTemp(0)("AUD") = "D"
                        mdtTranTable.AcceptChanges()
                    End If
                End If

            End If

            '===========================================| APPLICATION IDLE SETTING END |==========================================='

            'Pinkal (21-jun-2011)  --End



            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            '===========================================| USER LOGIN OPTION START |==========================================='
            If mdtTranTable.Rows.Count > 0 Then
                Dim blnFlag As Boolean = False
                Dim drTemp() As DataRow = objPasswdOption._DataTable.Select("passwdoptionid = '" & enPasswordOption.USER_LOGIN_MODE & "'")
                Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.USER_LOGIN_MODE & "'")
                If drTemp.Length > 0 Then
                    If dtTemp.Length > 0 Then
                        If radBasicAuthentication.Checked = True Then
                            dtTemp(0)("passwdminlen") = enAuthenticationMode.BASIC_AUTHENTICATION
                            'S.SANDEEP [ 01 DEC 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            dtTemp(0)("isemp_user") = chkImportEmployee.Checked
                            blnFlag = True
                            'S.SANDEEP [ 01 DEC 2012 ] -- END
                        ElseIf radADAuthentication.Checked = True Then
                            dtTemp(0)("passwdminlen") = enAuthenticationMode.AD_BASIC_AUTHENTICATION
                            'S.SANDEEP [ 01 DEC 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            dtTemp(0)("isemp_user") = False : blnFlag = True
                            'S.SANDEEP [ 01 DEC 2012 ] -- END
                        ElseIf radSSOAuthentication.Checked = True Then
                            dtTemp(0)("passwdminlen") = enAuthenticationMode.AD_SSO_AUTHENTICATION
                            'S.SANDEEP [ 01 DEC 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            dtTemp(0)("isemp_user") = False : blnFlag = True
                            'S.SANDEEP [ 01 DEC 2012 ] -- END
                        End If
                        'S.SANDEEP [10 AUG 2015] -- START
                        'ENHANCEMENT : Aruti SaaS Changes
                        dtTemp(0)("isenablealluser") = chkEnableAllUser.Checked
                        'S.SANDEEP [10 AUG 2015] -- END
                        If CInt(drTemp(0)("passwdminlen")) <> CInt(dtTemp(0)("passwdminlen")) Then
                            blnFlag = True
                        End If
                        If blnFlag = True Then dtTemp(0)("AUD") = "U"
                        dtTemp(0)("GUID") = Guid.NewGuid.ToString
                        mdtTranTable.AcceptChanges()
                    End If
                Else
                    dtRow = mdtTranTable.NewRow
                    dtRow.Item("passwdoptionid") = enPasswordOption.USER_LOGIN_MODE
                    If radBasicAuthentication.Checked = True Then
                        dtRow.Item("passwdminlen") = enAuthenticationMode.BASIC_AUTHENTICATION
                        'S.SANDEEP [ 01 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        dtTemp(0)("isemp_user") = chkImportEmployee.Checked
                        'S.SANDEEP [ 01 DEC 2012 ] -- END
                    ElseIf radADAuthentication.Checked = True Then
                        dtRow.Item("passwdminlen") = enAuthenticationMode.AD_BASIC_AUTHENTICATION
                        'S.SANDEEP [ 01 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        dtTemp(0)("isemp_user") = False
                        'S.SANDEEP [ 01 DEC 2012 ] -- END
                    ElseIf radSSOAuthentication.Checked = True Then
                        dtRow.Item("passwdminlen") = enAuthenticationMode.AD_SSO_AUTHENTICATION
                        'S.SANDEEP [ 01 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        dtTemp(0)("isemp_user") = False
                        'S.SANDEEP [ 01 DEC 2012 ] -- END
                    End If
                    'S.SANDEEP [10 AUG 2015] -- START
                    'ENHANCEMENT : Aruti SaaS Changes
                    dtTemp(0)("isenablealluser") = chkEnableAllUser.Checked
                    'S.SANDEEP [10 AUG 2015] -- END
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtTranTable.Rows.Add(dtRow)
                End If
            Else
                dtRow = mdtTranTable.NewRow
                dtRow.Item("passwdoptionid") = enPasswordOption.USER_LOGIN_MODE
                If radBasicAuthentication.Checked = True Then
                    dtRow.Item("passwdminlen") = enAuthenticationMode.BASIC_AUTHENTICATION
                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dtRow.Item("isemp_user") = chkImportEmployee.Checked
                    'S.SANDEEP [ 01 DEC 2012 ] -- END
                ElseIf radADAuthentication.Checked = True Then
                    dtRow.Item("passwdminlen") = enAuthenticationMode.AD_BASIC_AUTHENTICATION
                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dtRow.Item("isemp_user") = False
                    'S.SANDEEP [ 01 DEC 2012 ] -- END
                ElseIf radSSOAuthentication.Checked = True Then
                    dtRow.Item("passwdminlen") = enAuthenticationMode.AD_SSO_AUTHENTICATION
                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dtRow.Item("isemp_user") = False
                    'S.SANDEEP [ 01 DEC 2012 ] -- END
                End If
                'S.SANDEEP [10 AUG 2015] -- START
                'ENHANCEMENT : Aruti SaaS Changes
                dtRow.Item("isenablealluser") = chkEnableAllUser.Checked
                'S.SANDEEP [10 AUG 2015] -- END
                dtRow.Item("AUD") = "A"
                dtRow.Item("GUID") = Guid.NewGuid.ToString
                mdtTranTable.Rows.Add(dtRow)
            End If

            'Sohail (15 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            If mdtTranTable.Rows.Count > 0 Then
                Dim blnFlag As Boolean = False
                Dim drTemp() As DataRow = objPasswdOption._DataTable.Select("passwdoptionid = '" & enPasswordOption.PROHIBIT_PASSWD_CHANGE & "'")
                Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PROHIBIT_PASSWD_CHANGE & "'")

                If drTemp.Length > 0 Then
                    If dtTemp.Length > 0 Then
                        dtTemp(0)("passwdminlen") = chkProhibitPasswordChange.Checked

                        If CInt(drTemp(0)("passwdminlen")) <> CInt(dtTemp(0)("passwdminlen")) Then
                            blnFlag = True
                        End If

                        If blnFlag = True Then dtTemp(0)("AUD") = "U"

                        dtTemp(0)("GUID") = Guid.NewGuid.ToString
                        mdtTranTable.AcceptChanges()
                    End If
                Else
                    dtRow = mdtTranTable.NewRow
                    dtRow.Item("passwdoptionid") = enPasswordOption.PROHIBIT_PASSWD_CHANGE
                    dtRow.Item("passwdminlen") = chkProhibitPasswordChange.Checked
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtTranTable.Rows.Add(dtRow)
                End If

            Else
                dtRow = mdtTranTable.NewRow
                dtRow.Item("passwdoptionid") = enPasswordOption.PROHIBIT_PASSWD_CHANGE
                dtRow.Item("passwdminlen") = chkProhibitPasswordChange.Checked
                dtRow.Item("AUD") = "A"
                dtRow.Item("GUID") = Guid.NewGuid.ToString
                mdtTranTable.Rows.Add(dtRow)
            End If
            'Sohail (15 Apr 2013) -- End

            '===========================================| USER LOGIN OPTION END |==========================================='
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            'S.SANDEEP [ 01 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            '===========================================| USER NAME LENGTH OPTION START |==========================================='
            If gbUnameLength.Checked = True Then
                If mdtTranTable.Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    Dim drTemp() As DataRow = objPasswdOption._DataTable.Select("passwdoptionid = '" & enPasswordOption.USER_NAME_LENGTH & "'")
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.USER_NAME_LENGTH & "'")
                    If drTemp.Length > 0 Then
                        If dtTemp.Length > 0 Then
                            dtTemp(0)("passwdminlen") = nudUMinLength.Value

                            If CInt(drTemp(0)("passwdminlen")) <> CInt(dtTemp(0)("passwdminlen")) Then
                                blnFlag = True
                            End If
                            If blnFlag = True Then dtTemp(0)("AUD") = "U"
                            dtTemp(0)("GUID") = Guid.NewGuid.ToString
                            mdtTranTable.AcceptChanges()
                        End If
                    Else
                        dtRow = mdtTranTable.NewRow
                        dtRow.Item("passwdoptionid") = enPasswordOption.USER_NAME_LENGTH
                        dtRow.Item("passwdminlen") = nudUMinLength.Value
                        dtRow.Item("AUD") = "A"
                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtTranTable.Rows.Add(dtRow)
                    End If
                Else
                    dtRow = mdtTranTable.NewRow
                    dtRow.Item("passwdoptionid") = enPasswordOption.USER_NAME_LENGTH
                    dtRow.Item("passwdminlen") = nudUMinLength.Value
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtTranTable.Rows.Add(dtRow)
                End If
            Else
                If mdtTranTable.Rows.Count > 0 Then
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.USER_NAME_LENGTH & "'")
                    If dtTemp.Length > 0 Then
                        dtTemp(0)("AUD") = "D"
                        mdtTranTable.AcceptChanges()
                    End If
                End If
            End If
            '===========================================| USER NAME LENGTH OPTION END |==========================================='

            '===========================================| PASSWORD POLICY START |==========================================='
            If gbPasswordPolicy.Checked = True Then
                'S.SANDEEP [ 06 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim blnFlag1 As Boolean = False
                Dim blnFlag2 As Boolean = False
                Dim blnFlag3 As Boolean = False
                Dim blnFlag4 As Boolean = False

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                Dim blnFlag5 As Boolean = False
                'Pinkal (18-Aug-2018) -- End

                'S.SANDEEP [ 06 DEC 2012 ] -- END
                If mdtTranTable.Rows.Count > 0 Then
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.ENF_PASSWD_POLICY & "'")
                    If dtTemp.Length > 0 Then
                        For iCnt As Integer = 0 To dtTemp.Length - 1
                            Select Case CInt(dtTemp(iCnt).Item("passwdminlen"))
                                Case enPasswordPolicy.PASSWD_UCASE_CMPL
                                    blnFlag1 = True 'S.SANDEEP [ 06 DEC 2012 ]
                                    If chkUpperCaseMandatory.Checked = True Then
                                        dtTemp(iCnt).Item("passwdminlen") = enPasswordPolicy.PASSWD_UCASE_CMPL
                                        dtTemp(iCnt)("AUD") = ""
                                        dtTemp(iCnt)("GUID") = Guid.NewGuid.ToString
                                    Else
                                        dtTemp(iCnt)("AUD") = "D"
                                    End If
                                Case enPasswordPolicy.PASSWD_LCASE_CMPL
                                    blnFlag2 = True 'S.SANDEEP [ 06 DEC 2012 ]
                                    If chkLowerCaseMandatory.Checked = True Then
                                        dtTemp(iCnt).Item("passwdminlen") = enPasswordPolicy.PASSWD_LCASE_CMPL
                                        dtTemp(iCnt)("AUD") = ""
                                        dtTemp(iCnt)("GUID") = Guid.NewGuid.ToString
                                    Else
                                        dtTemp(iCnt)("AUD") = "D"
                                    End If
                                Case enPasswordPolicy.PASSWD_NCASE_CMPL
                                    blnFlag3 = True 'S.SANDEEP [ 06 DEC 2012 ]
                                    If chkNumericMandatory.Checked = True Then
                                        dtTemp(iCnt).Item("passwdminlen") = enPasswordPolicy.PASSWD_NCASE_CMPL
                                        dtTemp(iCnt)("AUD") = ""
                                        dtTemp(iCnt)("GUID") = Guid.NewGuid.ToString
                                    Else
                                        dtTemp(iCnt)("AUD") = "D"
                                    End If
                                Case enPasswordPolicy.PASSWD_SPLCH_CMPL
                                    blnFlag4 = True 'S.SANDEEP [ 06 DEC 2012 ]
                                    If chkSpecialCharsMandatory.Checked = True Then
                                        dtTemp(iCnt).Item("passwdminlen") = enPasswordPolicy.PASSWD_SPLCH_CMPL
                                        dtTemp(iCnt)("AUD") = ""
                                        dtTemp(iCnt)("GUID") = Guid.NewGuid.ToString
                                    Else
                                        dtTemp(iCnt)("AUD") = "D"
                                    End If

                                    'Pinkal (18-Aug-2018) -- Start
                                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                Case enPasswordPolicy.PASSWD_COMPLEXITY
                                    blnFlag5 = True
                                    If chkPasswordComplexity.Checked = True Then
                                        dtTemp(iCnt).Item("passwdminlen") = enPasswordPolicy.PASSWD_COMPLEXITY
                                        dtTemp(iCnt)("AUD") = ""
                                        dtTemp(iCnt)("GUID") = Guid.NewGuid.ToString
                                    Else
                                        dtTemp(iCnt)("AUD") = "D"
                                    End If
                                    'Pinkal (18-Aug-2018) -- End


                            End Select
                            mdtTranTable.AcceptChanges()
                        Next

                        'S.SANDEEP [ 06 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        For Each iCtrl As Control In gbPasswordPolicy.Controls
                            If TypeOf iCtrl Is CheckBox Then
                                If CType(iCtrl, CheckBox).Checked = True Then
                                    Select Case CType(iCtrl, CheckBox).Name.ToUpper
                                        Case "CHKUPPERCASEMANDATORY"
                                            If blnFlag1 = True Then Continue For
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_UCASE_CMPL
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)
                                        Case "CHKLOWERCASEMANDATORY"
                                            If blnFlag2 = True Then Continue For
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_LCASE_CMPL
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)
                                        Case "CHKNUMERICMANDATORY"
                                            If blnFlag3 = True Then Continue For
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_NCASE_CMPL
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)
                                        Case "CHKSPECIALCHARSMANDATORY"
                                            If blnFlag4 = True Then Continue For
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_SPLCH_CMPL
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)

                                            'Pinkal (18-Aug-2018) -- Start
                                            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                        Case "CHKPASSWORDCOMPLEXITY"
                                            If blnFlag5 = True Then Continue For
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_COMPLEXITY
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)
                                            'Pinkal (18-Aug-2018) -- End

                                    End Select
                                End If
                            End If
                        Next
                        'S.SANDEEP [ 06 DEC 2012 ] -- END
                    Else
                        For Each iCtrl As Control In gbPasswordPolicy.Controls
                            If TypeOf iCtrl Is CheckBox Then
                                If CType(iCtrl, CheckBox).Checked = True Then
                                    Select Case CType(iCtrl, CheckBox).Name.ToUpper
                                        Case "CHKUPPERCASEMANDATORY"
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_UCASE_CMPL
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)
                                        Case "CHKLOWERCASEMANDATORY"
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_LCASE_CMPL
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)
                                        Case "CHKNUMERICMANDATORY"
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_NCASE_CMPL
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)
                                        Case "CHKSPECIALCHARSMANDATORY"
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_SPLCH_CMPL
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)

                                            'Pinkal (18-Aug-2018) -- Start
                                            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                        Case "CHKPASSWORDCOMPLEXITY"
                                            If blnFlag5 = True Then Continue For
                                            dtRow = mdtTranTable.NewRow
                                            dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                            dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_COMPLEXITY
                                            dtRow.Item("AUD") = "A"
                                            dtRow.Item("GUID") = Guid.NewGuid.ToString
                                            mdtTranTable.Rows.Add(dtRow)
                                            'Pinkal (18-Aug-2018) -- End

                                    End Select
                                End If
                            End If
                        Next
                    End If
                Else
                    For Each iCtrl As Control In gbPasswordPolicy.Controls
                        If TypeOf iCtrl Is CheckBox Then
                            If CType(iCtrl, CheckBox).Checked = True Then
                                Select Case CType(iCtrl, CheckBox).Name.ToUpper
                                    Case "CHKUPPERCASEMANDATORY"
                                        dtRow = mdtTranTable.NewRow
                                        dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                        dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_UCASE_CMPL
                                        dtRow.Item("AUD") = "A"
                                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                                        mdtTranTable.Rows.Add(dtRow)
                                    Case "CHKLOWERCASEMANDATORY"
                                        dtRow = mdtTranTable.NewRow
                                        dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                        dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_LCASE_CMPL
                                        dtRow.Item("AUD") = "A"
                                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                                        mdtTranTable.Rows.Add(dtRow)
                                    Case "CHKNUMERICMANDATORY"
                                        dtRow = mdtTranTable.NewRow
                                        dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                        dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_NCASE_CMPL
                                        dtRow.Item("AUD") = "A"
                                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                                        mdtTranTable.Rows.Add(dtRow)
                                    Case "CHKSPECIALCHARSMANDATORY"
                                        dtRow = mdtTranTable.NewRow
                                        dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                        dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_SPLCH_CMPL
                                        dtRow.Item("AUD") = "A"
                                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                                        mdtTranTable.Rows.Add(dtRow)
                                        'Pinkal (18-Aug-2018) -- Start
                                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                    Case "CHKPASSWORDCOMPLEXITY"
                                        If blnFlag5 = True Then Continue For
                                        dtRow = mdtTranTable.NewRow
                                        dtRow.Item("passwdoptionid") = enPasswordOption.ENF_PASSWD_POLICY
                                        dtRow.Item("passwdminlen") = enPasswordPolicy.PASSWD_COMPLEXITY
                                        dtRow.Item("AUD") = "A"
                                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                                        mdtTranTable.Rows.Add(dtRow)
                                        'Pinkal (18-Aug-2018) -- End
                                End Select
                            End If
                        End If
                    Next
                End If
            Else
                If mdtTranTable.Rows.Count > 0 Then
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.ENF_PASSWD_POLICY & "'")
                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To dtTemp.Length - 1
                            dtTemp(i)("AUD") = "D"
                            mdtTranTable.AcceptChanges()
                        Next
                    End If
                End If
            End If
            '===========================================| PASSWORD POLICY END |==========================================='
            'S.SANDEEP [ 01 NOV 2012 ] -- END

            '===========================================| PASSWD_HISTORY START |==========================================='
            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            If gbPasswordHistorySettings.Checked = True AndAlso (chkDonotAllowToReuseAnyOfTheLast.Checked OrElse chkDonotAllowReuseOfPasswordUsedInTheLast.Checked) Then

                If mdtTranTable.Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    Dim drTemp() As DataRow = objPasswdOption._DataTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_HISTORY & "'")
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_HISTORY & "'")

                    If drTemp.Length > 0 Then

                        If dtTemp.Length > 0 Then
                            dtTemp(0)("AUD") = ""
                            If chkDonotAllowToReuseAnyOfTheLast.Checked = True Then
                                dtTemp(0)("passwdlastusednumber") = nudNumberPassword.Value
                            Else
                                dtTemp(0)("passwdlastusednumber") = 0
                            End If

                            If chkDonotAllowReuseOfPasswordUsedInTheLast.Checked = True Then
                                dtTemp(0)("passwdlastuseddays") = nudDaysPassword.Value
                            Else
                                dtTemp(0)("passwdlastuseddays") = 0
                            End If


                            If CInt(drTemp(0)("passwdlastusednumber")) <> CInt(dtTemp(0)("passwdlastusednumber")) Then
                                blnFlag = True
                            ElseIf CInt(drTemp(0)("passwdlastuseddays")) <> CInt(dtTemp(0)("passwdlastuseddays")) Then
                                blnFlag = True
                            End If

                            If blnFlag = True Then dtTemp(0)("AUD") = "U"

                            dtTemp(0)("GUID") = Guid.NewGuid.ToString
                            mdtTranTable.AcceptChanges()

                        End If

                    Else
                        dtRow = mdtTranTable.NewRow

                        dtRow.Item("passwdoptionid") = enPasswordOption.PASSWD_HISTORY

                        If chkDonotAllowToReuseAnyOfTheLast.Checked = True Then
                            dtRow.Item("passwdlastusednumber") = nudNumberPassword.Value
                        End If

                        If chkDonotAllowReuseOfPasswordUsedInTheLast.Checked = True Then
                            dtRow.Item("passwdlastuseddays") = nudDaysPassword.Value
                        End If

                        dtRow.Item("AUD") = "A"
                        dtRow.Item("GUID") = Guid.NewGuid.ToString

                        mdtTranTable.Rows.Add(dtRow)

                    End If
                Else

                    dtRow = mdtTranTable.NewRow

                    dtRow.Item("passwdoptionid") = enPasswordOption.PASSWD_HISTORY
                    If chkDonotAllowToReuseAnyOfTheLast.Checked = True Then
                        dtRow.Item("passwdlastusednumber") = nudNumberPassword.Value
                    End If

                    If chkDonotAllowReuseOfPasswordUsedInTheLast.Checked = True Then
                        dtRow.Item("passwdlastuseddays") = nudDaysPassword.Value
                    End If
                    dtRow.Item("isunlockedbyadmin") = radAdminUnlock.Checked
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString

                    mdtTranTable.Rows.Add(dtRow)
                End If
            Else
                If mdtTranTable.Rows.Count > 0 Then
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & enPasswordOption.PASSWD_HISTORY & "'")
                    If dtTemp.Length > 0 Then
                        dtTemp(0)("AUD") = "D"
                        mdtTranTable.AcceptChanges()
                    End If
                End If
            End If
            'Hemant (25 May 2021) -- End
            '===========================================| PASSWD_HISTORY END |==========================================='


            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.

            'If radBasicAuthentication.Checked = False Then
            '    If txtADServerIP.Text.Trim.Length > 0 Then
            '        Dim objGSettings As New clsGeneralSettings
            '        objGSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
            '        objGSettings._AD_ServerIP = txtADServerIP.Text.Trim
            '    End If
            'End If

            Dim objconfig As New clsConfigOptions

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            Dim mblnFlag As Boolean = False
            objconfig._Companyunkid = -999
            objconfig._UserMustchangePwdOnNextLogon = chkUserChangePwdnextLogon.Checked
            'Pinkal (18-Aug-2018) -- End

            'Pinkal (27-Nov-2020) -- Start
            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
            objconfig._AllowEmpSystemAccessOnActualEOCRetirementDate = chkAllowLoginOnActualEOCRetireDate.Checked
            'Pinkal (27-Nov-2020) -- End

            If radBasicAuthentication.Checked = False Then
                objconfig._ADIPAddress = txtADServerIP.Text.Trim
                objconfig._ADDomain = txtDomainName.Text.Trim
                objconfig._ADDomainUser = txtDomainUser.Text.Trim
                objconfig._ADDomainUserPwd = clsSecurity.Encrypt(txtDomainPassword.Text.Trim, "ezee")

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                objconfig._CreateADUserFromEmpMst = chkADUserFromEmpMst.Checked
                'Pinkal (18-Aug-2018) -- End
            Else
                objconfig._ADIPAddress = ""
                objconfig._ADDomain = ""
                objconfig._ADDomainUser = ""
                objconfig._ADDomainUserPwd = ""
                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                objconfig._CreateADUserFromEmpMst = False
                'Pinkal (18-Aug-2018) -- End
            End If

            mblnFlag = objconfig.updateParam()
            ConfigParameter._Object.Refresh()
            'Pinkal (03-Apr-2017) -- End



            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnFlag Then
                Dim objatConfigOption As New clsAtconfigoption
                objatConfigOption._Companyunkid = -999

                If chkADUserFromEmpMst.Checked <> mblnATADUserFromEmpMst Then
                    objconfig.GetData(-999, "CreateADUserFromEmpMst")
                    objatConfigOption._Configunkid = objconfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Text & " --> " & gbUserLoginOptions.Text & " --> " & chkADUserFromEmpMst.Text & " -->  Old Value :- " & mblnATADUserFromEmpMst & " | New Value :- " & chkADUserFromEmpMst.Checked
                    objatConfigOption.Insert()
                End If

                If chkUserChangePwdnextLogon.Checked <> mblnATUserMustchangePwdOnNextLogon Then
                    objconfig.GetData(-999, "UserMustChangePwdOnNextLogon")
                    objatConfigOption._Configunkid = objconfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Text & " --> " & gbUserLoginOptions.Text & " --> " & chkUserChangePwdnextLogon.Text & " -->  Old Value :- " & mblnATUserMustchangePwdOnNextLogon & " | New Value :- " & chkUserChangePwdnextLogon.Checked
                    objatConfigOption.Insert()
                End If


                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.

                If chkAllowLoginOnActualEOCRetireDate.Checked <> mblnATAllowEmpSystemAccessOnActualEOCRetirementDate Then
                    objconfig.GetData(-999, "AllowEmpSystemAccessOnActualEOCRetirementDate")
                    objatConfigOption._Configunkid = objconfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Text & " --> " & gbUserLoginOptions.Text & " --> " & chkAllowLoginOnActualEOCRetireDate.Text & " --> Old Value :- " & mblnATAllowEmpSystemAccessOnActualEOCRetirementDate.ToString() & " | New Value :- " & chkAllowLoginOnActualEOCRetireDate.Checked
                    objatConfigOption.Insert()
                End If

                'Pinkal (27-Nov-2020) -- End

                objatConfigOption = Nothing
            End If

            objconfig = Nothing
            'Pinkal (18-Aug-2018) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetValue ; Module Name : " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    Private Function ADValidation() As Boolean
        Try
            If txtADServerIP.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Active directory server address is mandatory information. Please set Active directory server address."), enMsgBoxStyle.Information)
                txtADServerIP.Focus()
                Return False

            ElseIf txtDomainName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Domain Name is mandatory information for active directory server. Please set domain Name for active directory server."), enMsgBoxStyle.Information)
                txtDomainName.Focus()
                Return False

            ElseIf txtDomainUser.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Domain User is mandatory information for active directory server. Please set domain user for active directory server."), enMsgBoxStyle.Information)
                txtDomainUser.Focus()
                Return False

            ElseIf txtDomainPassword.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Domain Password is mandatory information for active directory server. Please set domain password for active directory server."), enMsgBoxStyle.Information)
                txtDomainPassword.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ADValidation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function
    'Pinkal (18-Aug-2018) -- End


    'Pinkal (04-Jan-2021) -- Start
    'Enhancement NMB - Working Announcements for Self service Login Screen.

    Private Sub FillCombo()
        Try
            objcboFont.Items.Clear()
            For Each obj As FontFamily In FontFamily.Families()
                objcboFont.Items.Add(obj.Name)
            Next

            If FontSizes.Count <= 0 Then
                InitializeFontSizes()
            End If

            objcboFontSize.Items.Clear()
            For i As Integer = 0 To FontSizes.Count - 1
                objcboFontSize.Items.Add(FontSizes.Item(i).ToString)
            Next

            RemoveHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
            objcboFont.SelectedIndex = objcboFont.FindStringExact("Arial")
            objcboFontSize.SelectedIndex = objcboFontSize.FindStringExact("8")
            AddHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillAnnouncements()
        Dim objAnnouncement As New clsAnnouncements_master
        Try
            Dim dsList As DataSet = objAnnouncement.GetList("List", True, -1)

            dgvAnnouncements.AutoGenerateColumns = False

            dgcolhTitle.DataPropertyName = "title"
            'dgcolhAnnouncement.DataPropertyName = "announcement"
            objdgcolhMarkAsClose.DataPropertyName = "ismarkasclose"
            objdgcolhAnnouncementId.DataPropertyName = "announcementunkid"

            dgvAnnouncements.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAnnouncements", mstrModuleName)
        Finally
            objAnnouncement = Nothing
        End Try
    End Sub

    Private Sub ResetAnnouncement()
        Try
            txtTitle.Tag = 0
            txtTitle.Text = ""
            txtAnnouncement.Text = ""
            If mdtAnnouncementDoc IsNot Nothing Then mdtAnnouncementDoc.Rows.Clear()
            chkRequireAcknowledgement.Checked = False
            imgAnnouncement._Image = Nothing
            imgAnnouncement._OriginalFilePath = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetAnnouncement", mstrModuleName)
        End Try
    End Sub


    Public Shared ReadOnly FontSizes As List(Of Single) = New List(Of Single)()
    Private Shared Sub InitializeFontSizes()
        FontSizes.AddRange(New Single() {8, 9, 10, 10.5F, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72})
    End Sub



    Private Sub ChangeFontSize(ByVal mFSize As Double)
        Try
            If mFSize <= 0 Then
                Exit Sub
            End If
            Dim intStart As Integer = txtAnnouncement.SelectionStart
            Dim intEnd As Integer = txtAnnouncement.SelectionLength
            Dim rtbTempStart As Integer = 0

            If intEnd <= 1 And txtAnnouncement.SelectionFont IsNot Nothing Then
                txtAnnouncement.SelectionFont = New Font(txtAnnouncement.SelectionFont.FontFamily, CInt(mFSize), txtAnnouncement.SelectionFont.Style)
                Return
            End If

            rtbTemp.Rtf = txtAnnouncement.SelectedRtf
            For i As Integer = 0 To intEnd
                rtbTemp.Select(rtbTempStart + i, 1)
                Dim mStyle As New FontStyle
                If txtAnnouncement.SelectionLength > 0 Then
                    mStyle = GetDefaultStyle(rtbTemp.SelectionFont.FontFamily.Name)
                Else
                    mStyle = GetDefaultStyle(objcboFont.Text)
                End If
                If IsNothing(rtbTemp.SelectionFont) Then
                    rtbTemp.SelectionFont = New Font(objcboFont.Text, CInt(mFSize), mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont.FontFamily, CInt(mFSize), mStyle)
                End If
            Next
            rtbTemp.Select(rtbTempStart, intEnd)
            txtAnnouncement.SelectedRtf = rtbTemp.SelectedRtf
            txtAnnouncement.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ChangeFontSize", mstrModuleName)
        End Try
    End Sub

    Private Sub ChangeFont(ByVal mFontName As String)
        Try
            Dim intStart As Integer = txtAnnouncement.SelectionStart
            Dim intEnd As Integer = txtAnnouncement.SelectionLength
            Dim rtbTempStart As Integer = 0

            Dim mStyle As New FontStyle
            mStyle = GetDefaultStyle(mFontName)

            If intEnd <= 1 And txtAnnouncement.SelectionFont IsNot Nothing Then
                txtAnnouncement.SelectionFont = New Font(CStr(mFontName), CInt(IIf(txtAnnouncement.SelectionFont.Size <> Nothing, txtAnnouncement.SelectionFont.Size, 8)), mStyle)
                Return
            End If

            rtbTemp.Rtf = txtAnnouncement.SelectedRtf
            For i As Integer = 0 To intEnd - 1
                rtbTemp.Select(rtbTempStart + i, 1)
                If IsNothing(rtbTemp.SelectionFont) Then
                    rtbTemp.SelectionFont = New Font(mFontName, CInt(objcboFontSize.Text), mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbTemp.SelectionFont.Size <> Nothing, rtbTemp.SelectionFont.Size, 8)), mStyle)
                End If
            Next
            rtbTemp.Select(rtbTempStart, intEnd)
            txtAnnouncement.SelectedRtf = rtbTemp.SelectedRtf
            txtAnnouncement.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "ChangeFont", mstrModuleName)
        End Try
    End Sub

    Private Sub RegainOldStyle()
        Try
            If objtlbbtnBold.Checked = True Then
                Call ApplyStyle(FontStyle.Bold, True)
            Else
                Call ApplyStyle(FontStyle.Bold, False)
            End If
            If objtlbbtnItalic.Checked = True Then
                Call ApplyStyle(FontStyle.Italic, True)
            Else
                Call ApplyStyle(FontStyle.Italic, False)
            End If
            If objtlbbtnUnderline.Checked = True Then
                Call ApplyStyle(FontStyle.Underline, True)
            Else
                Call ApplyStyle(FontStyle.Underline, False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RegainOldStyle", mstrModuleName)
        End Try
    End Sub

    Private Sub ApplyStyle(ByVal mStyle As FontStyle, ByVal mAddStyle As Boolean)
        Try
            Dim intStart As Integer = txtAnnouncement.SelectionStart
            Dim intEnd As Integer = txtAnnouncement.SelectionLength
            Dim rtbTempStart As Integer = 0

            If intEnd <= 1 And txtAnnouncement.SelectionFont IsNot Nothing Then
                If mAddStyle Then
                    txtAnnouncement.SelectionFont = New Font(txtAnnouncement.SelectionFont, txtAnnouncement.SelectionFont.Style Or mStyle)
                Else
                    txtAnnouncement.SelectionFont = New Font(txtAnnouncement.SelectionFont, txtAnnouncement.SelectionFont.Style And (Not mStyle))
                End If
                Return
            End If

            rtbTemp.Rtf = txtAnnouncement.SelectedRtf

            For i As Integer = 0 To intEnd
                rtbTemp.Select(rtbTempStart + i, 1)
                If mAddStyle Then
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style Or mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style And (Not mStyle))
                End If
            Next

            rtbTemp.Select(rtbTempStart, intEnd)
            txtAnnouncement.SelectedRtf = rtbTemp.SelectedRtf
            txtAnnouncement.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ApplyStyle", mstrModuleName)
        End Try
    End Sub

    Private Function GetDefaultStyle(ByVal mFontName As String) As FontStyle
        For Each fntName As FontFamily In FontFamily.Families
            If fntName.Name.Equals(mFontName) Then
                If fntName.IsStyleAvailable(FontStyle.Regular) Then
                    Return FontStyle.Regular
                ElseIf fntName.IsStyleAvailable(FontStyle.Bold) Then
                    Return FontStyle.Bold
                ElseIf fntName.IsStyleAvailable(FontStyle.Italic) Then
                    Return FontStyle.Italic
                ElseIf fntName.IsStyleAvailable(FontStyle.Underline) Then
                    Return FontStyle.Underline
                End If
            End If
        Next
    End Function

    'Pinkal (04-Jan-2021) -- End


#End Region

#Region " Form's Events "

    Private Sub frmPasswordOptions_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPasswdOption = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmPasswordOptions_FormClosed ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPasswordOptions_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmPasswordOptions_KeyDown ; Module Name : " & mstrModuleName)
        End Try
    End Sub

    Private Sub frmPasswordOptions_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmPasswordOptions_KeyPress ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPasswordOptions_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPasswdOption = New clsPassowdOptions
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            mdtTranTable = objPasswdOption._DataTable.Copy
            Call GetValue()
            Call chkImportEmployee_CheckedChanged(New Object, New EventArgs)

            'Pinkal (04-Jan-2021) -- Start
            'Enhancement NMB - Working Announcements for Self service Login Screen.

            FillCombo()

            Dim objAnnDoc As New clsAnnouncement_document
            objAnnDoc.GetAnnouncementDocument("List", True, 0, Nothing)
            mdtAnnouncementDoc = objAnnDoc._dtDocumentList.Copy()
            FillAnnouncements()

            'Pinkal (04-Jan-2021) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmPasswordOptions_Load ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnClose_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnPrompt As Boolean = False
        Try
            If radLoginRetry.Checked = True Then
                If txtRetrydurattion.Int <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Login retry cannot be zero. Please give some number which is greater than zero."), enMsgBoxStyle.Information)
                    txtRetrydurattion.Focus()
                    Exit Sub
                End If

            ElseIf gbApplicationIdleSetting.Checked Then
                If txtIdleMinutes.Int <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Auto Log out Idle Minutes cannot be zero. Please give some number which is greater than zero."), enMsgBoxStyle.Information)
                    txtIdleMinutes.Focus()
                    Exit Sub
                End If
            End If

            If radBasicAuthentication.Checked = False Then

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'If txtADServerIP.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Active directory server address is mandatory information. Please set Active directory server address."), enMsgBoxStyle.Information)
                '    txtADServerIP.Focus()
                '    Exit Sub

                '    'Pinkal (03-Apr-2017) -- Start
                '    'Enhancement - Working On Active directory Changes for PACRA.

                'ElseIf txtDomainName.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Domain Name is mandatory information for active directory server. Please set domain Name for active directory server."), enMsgBoxStyle.Information)
                '    txtDomainName.Focus()
                '    Exit Sub

                'ElseIf txtDomainUser.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Domain User is mandatory information for active directory server. Please set domain user for active directory server."), enMsgBoxStyle.Information)
                '    txtDomainUser.Focus()
                '    Exit Sub

                'ElseIf txtDomainPassword.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Domain Password is mandatory information for active directory server. Please set domain password for active directory server."), enMsgBoxStyle.Information)
                '    txtDomainPassword.Focus()
                '    Exit Sub
                '    'Pinkal (03-Apr-2017) -- End

                'End If
                If ADValidation() = False Then Exit Sub
                'Pinkal (18-Aug-2018) -- End
            End If

            Call SetValue()

            If objPasswdOption._DataTable.Rows.Count > 0 Then
                For Each OlddtRow As DataRow In objPasswdOption._DataTable.Rows
                    Dim dtTemp() As DataRow = mdtTranTable.Select("passwdoptionid = '" & OlddtRow.Item("passwdoptionid").ToString & "'")
                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To dtTemp(0).ItemArray.Length - 3
                            If dtTemp(0)(i).ToString <> OlddtRow.Item(i).ToString Then
                                blnPrompt = True
                                Exit For
                            End If
                        Next
                    Else
                        blnPrompt = True
                        Exit For
                    End If
                Next
            ElseIf mdtTranTable.Rows.Count > 0 Then
                blnPrompt = True
            End If

            objPasswdOption._DataTable = mdtTranTable

            'S.SANDEEP [20-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : {#0001984}
            'Dim blnYes As Boolean = False
            'If mintOldExpDays <> mintExpDays Then
            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Your have set password expiry option, this will effect all user(s) defined in the system. Would you like to update expiry days for all user?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '        blnYes = True
            '    End If
            'End If
            'S.SANDEEP [20-Feb-2018] -- END

            If objPasswdOption.InsertUpdateDeletePasswordOption = True Then
                'S.SANDEEP [20-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001984}
                'If blnYes Then
                Dim objUser As New clsUserAddEdit
                Dim intDay As Integer = 0
                If mintExpDays = -1 Then
                    intDay = mintExpDays
                ElseIf mintOldExpDays > 0 Then
                    intDay = mintExpDays - mintOldExpDays
                Else
                    intDay = mintExpDays
                End If
                objUser.UpdateGlobalPasswordExpiryDate(intDay, ConfigParameter._Object._CurrentDateAndTime, User._Object._Userunkid, getIP(), getHostName())
                objUser = Nothing
                'End If
                'S.SANDEEP [20-Feb-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Information saved successfully."), enMsgBoxStyle.Information)

                'Pinkal (04-Jan-2021) -- Start
                'Enhancement NMB - Working Announcements for Self service Login Screen.
                'Me.Close()
                'Pinkal (04-Jan-2021) -- End
            End If


            'Pinkal (04-Jan-2021) -- Start
            'Enhancement NMB - Working Announcements for Self service Login Screen.
            'Me.Close()
            'Pinkal (04-Jan-2021) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnSave_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (04-Jan-2021) -- Start
    'Enhancement NMB - Working Announcements for Self service Login Screen.

    Private Function ValidAnnouncement() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If txtTitle.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Title is compulsory information.Please enter Title."), enMsgBoxStyle.Information)
                txtTitle.Focus()
                mblnFlag = False
            ElseIf txtAnnouncement.Text.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Announcement is compulsory information.Please enter Announcement."), enMsgBoxStyle.Information)
                txtAnnouncement.Focus()
                mblnFlag = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ValidAnnouncement", mstrModuleName)
            mblnFlag = False
        End Try
        Return mblnFlag
    End Function

    Private Sub btnAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttachment.Click
        Dim objFrm As New frmAnnouncement_document
        Try
            If ValidAnnouncement() = False Then Exit Sub

            objFrm._dtDocument = mdtAnnouncementDoc.Copy
            If objFrm.displayDialog(txtTitle.Text.Trim(), enAction.ADD_ONE, CInt(txtTitle.Tag)) Then
                mdtAnnouncementDoc = objFrm._dtDocument
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAttachment_Click", mstrModuleName)
        Finally
            objFrm = Nothing
        End Try
    End Sub

    Private Sub btnAnnoucementSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnnoucementSave.Click, btnMarkAsClose.Click
        Dim objAnnouncement As New clsAnnouncements_master
        Try

            If ValidAnnouncement() = False Then Exit Sub

            If txtTitle.Tag IsNot Nothing AndAlso CInt(txtTitle.Tag) > 0 Then
                objAnnouncement._Announcementunkid = CInt(txtTitle.Tag)
            End If

            objAnnouncement._Title = txtTitle.Text.Trim()
            objAnnouncement._Announcement = txtAnnouncement.Rtf

            If imgAnnouncement._Image IsNot Nothing Then

                'Pinkal (03-May-2021)-- Start
                'NMB Enhancement  -  Working on AD Enhancement for NMB.
                'Dim mintByes As Integer = 1048576    ' 1 MB = 1048576 Bytes (in binary)
                Dim mintByes As Integer = 6291456    ' 6 MB = 6291456 Bytes
                'Pinkal (03-May-2021) -- End

                If imgAnnouncement._OriginalFilePath.Trim.Length > 0 Then
                    Dim fl As New System.IO.FileInfo(imgAnnouncement._OriginalFilePath)
                    If fl.Length > mintByes Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry,You cannot set image file greater than ") & (mintByes / 1024) / 1024 & Language.getMessage(mstrModuleName, 11, " MB."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                Dim objConverter As New ImageConverter
                objAnnouncement._Photo = CType(objConverter.ConvertTo(imgAnnouncement._Image, GetType(Byte())), Byte())
                objConverter = Nothing
            Else
                objAnnouncement._Photo = Nothing
            End If

            objAnnouncement._Visiblefromdate = Nothing
            objAnnouncement._Visibletodate = Nothing
            objAnnouncement._RequireAcknowledgement = chkRequireAcknowledgement.Checked
            If CType(sender, eZee.Common.eZeeLightButton).Name = btnAnnoucementSave.Name Then
                objAnnouncement._Ismarkasclose = False
            ElseIf CType(sender, eZee.Common.eZeeLightButton).Name = btnMarkAsClose.Name Then
                objAnnouncement._Ismarkasclose = True
            End If

            If mdtAnnouncementDoc IsNot Nothing AndAlso mdtAnnouncementDoc.Rows.Count > 0 Then
                objAnnouncement._dtDocumentAttachment = mdtAnnouncementDoc
            End If

            objAnnouncement._Userunkid = User._Object._Userunkid
            objAnnouncement._Isvoid = False
            objAnnouncement._Voiduserunkid = 0
            objAnnouncement._Voiddatetime = Nothing
            objAnnouncement._Voidreason = ""
            objAnnouncement._WebFormName = Me.Name
            objAnnouncement._IsWeb = False

            Dim mblnFlag As Boolean = False
            If objAnnouncement._Announcementunkid > 0 Then
                mblnFlag = objAnnouncement.Update()
            Else
                mblnFlag = objAnnouncement.Insert()
            End If

            If mblnFlag = False And objAnnouncement._Message <> "" Then
                eZeeMsgBox.Show(objAnnouncement._Message, enMsgBoxStyle.Information)
            End If

            If mblnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Announcement saved successfully."), enMsgBoxStyle.Information)
                ResetAnnouncement()
                FillAnnouncements()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAnnoucementSave_Click", mstrModuleName)
        Finally
            objAnnouncement = Nothing
        End Try
    End Sub
    'Pinkal (04-Jan-2021) -- End


#End Region

#Region " Controls "

    Private Sub radAdminUnlock_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAdminUnlock.CheckedChanged, radLoginRetry.CheckedChanged
        Try
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case "RADADMINUNLOCK"
                    txtRetrydurattion.Enabled = False
                Case "RADLOGINRETRY"
                    txtRetrydurattion.Enabled = True
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : radAdminUnlock_CheckedChanged ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 01 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub gbPasswordPolicy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbPasswordPolicy.CheckedChanged, gbPasswordPolicy.CheckedChanged
        Try
            'S.SANDEEP [ 06 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'For Each ctrl As Control In gbPasswordPolicy.Controls
            '    If TypeOf ctrl Is CheckBox Then
            '        CType(ctrl, CheckBox).Checked = gbPasswordPolicy.Checked
            '    End If
            'Next

            If gbPasswordPolicy.Checked = False Then
                For Each ctrl As Control In gbPasswordPolicy.Controls
                    If TypeOf ctrl Is CheckBox Then
                        CType(ctrl, CheckBox).Checked = gbPasswordPolicy.Checked
                    End If
                Next
            End If
            'S.SANDEEP [ 06 DEC 2012 ] -- END
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "gbPasswordPolicy_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 01 DEC 2012 ] -- END

#End Region

#Region "Radio Button'S Event"

    Private Sub radADAuthentication_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radADAuthentication.CheckedChanged, radSSOAuthentication.CheckedChanged
        Try

            If radADAuthentication.Checked Or radSSOAuthentication.Checked Then
                txtADServerIP.Enabled = True

                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.
                txtDomainUser.Enabled = True
                txtDomainPassword.Enabled = True
                txtDomainName.Enabled = True
                'Pinkal (03-Apr-2017) -- End


                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'chkImportEmployee.Visible = False
                chkADUserFromEmpMst.Enabled = True
                chkImportEmployee.Enabled = False
                'Pinkal (18-Aug-2018) -- End
                'Hemant (25 May 2021) -- Start
                'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
                gbPasswordHistorySettings.Enabled = False
                gbPasswordHistorySettings.Checked = False
                chkDonotAllowToReuseAnyOfTheLast.Checked = False
                nudNumberPassword.Value = 0
                chkDonotAllowReuseOfPasswordUsedInTheLast.Checked = False
                nudDaysPassword.Value = 0
                'Hemant (25 May 2021) -- End
            Else
                txtADServerIP.Text = ""
                txtADServerIP.Enabled = False

                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.
                txtDomainUser.Text = ""
                txtDomainUser.Enabled = False
                txtDomainPassword.Text = ""
                txtDomainPassword.Enabled = False
                txtDomainName.Text = ""
                txtDomainName.Enabled = False
                'Pinkal (03-Apr-2017) -- End

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                ' chkImportEmployee.Visible = True
                chkImportEmployee.Enabled = True
                chkADUserFromEmpMst.Checked = False
                chkADUserFromEmpMst.Enabled = False
                'Pinkal (18-Aug-2018) -- End

                'Hemant (25 May 2021) -- Start
                'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
                gbPasswordHistorySettings.Enabled = True
                'Hemant (25 May 2021) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radADAuthentication_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radBasicAuthentication_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radBasicAuthentication.CheckedChanged
        Try

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'If radBasicAuthentication.Checked = True Then
            '    chkImportEmployee.Visible = True
            'Else
            '    chkImportEmployee.Visible = False
            'End If
            If radBasicAuthentication.Checked = True Then
                chkImportEmployee.Enabled = True
            Else
                chkImportEmployee.Enabled = False
            End If
            'Pinkal (18-Aug-2018) -- End
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "radBasicAuthentication_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Events"

    Private Sub chkImportEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkImportEmployee.CheckedChanged
        Try
            chkEnableAllUser.Enabled = chkImportEmployee.Checked
            If chkImportEmployee.Checked = False Then
                chkEnableAllUser.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkImportEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

    Private Sub chkPasswordComplexity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPasswordComplexity.CheckedChanged
        Try
            LblPasswordComplexity.Visible = chkPasswordComplexity.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkPasswordComplexity_CheckedChanged", mstrModuleName)
        End Try
    End Sub



    'Pinkal (15-Feb-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

    Private Sub chkADUserFromEmpMst_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkADUserFromEmpMst.CheckedChanged
        Try
            If chkADUserFromEmpMst.Checked Then
                If ADValidation() = False Then
                    RemoveHandler chkADUserFromEmpMst.CheckedChanged, AddressOf chkADUserFromEmpMst_CheckedChanged
                    chkADUserFromEmpMst.Checked = False
                    AddHandler chkADUserFromEmpMst.CheckedChanged, AddressOf chkADUserFromEmpMst_CheckedChanged
                    Exit Sub
                End If
                Dim objConfig As New clsConfigOptions

                Dim mstrADIPAddress As String = ""
                Dim mstrADDomain As String = ""
                Dim mstrADUserName As String = ""
                Dim mstrADUserPwd As String = ""

                objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress)
                objConfig.IsValue_Changed("ADDomain", "-999", mstrADDomain)
                objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADUserName)
                objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADUserPwd)

                If mstrADUserPwd.Trim.Length > 0 Then
                    mstrADUserPwd = clsSecurity.Decrypt(mstrADUserPwd, "ezee")
                End If

                Dim ar() As String = Nothing
                If mstrADDomain.Trim.Length > 0 AndAlso mstrADDomain.Trim.Contains(".") Then
                    ar = mstrADDomain.Trim.Split(CChar("."))
                    mstrADDomain = ""
                    If ar.Length > 0 Then
                        For i As Integer = 0 To ar.Length - 1
                            mstrADDomain &= ",DC=" & ar(i)
                        Next
                    End If
                End If

                If mstrADDomain.Trim.Length > 0 Then
                    mstrADDomain = mstrADDomain.Trim.Substring(1)
                End If

                Dim entry As DirectoryEntry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim)
                Dim objSearch As DirectorySearcher = New DirectorySearcher(entry, "(objectClass=*)", Nothing)
                Dim sr As SearchResult = objSearch.FindOne()

                If sr.Properties.Contains("minPwdAge") Then

                    If Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdAge")(0))).Days) <= nudMinDays.Minimum Then
                        nudMinDays.Minimum = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdAge")(0))).Days)
                    End If
                    If Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdAge")(0))).Days) >= nudMinDays.Maximum Then
                        nudMinDays.Maximum = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdAge")(0))).Days)
                    End If

                    nudMinDays.Value = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdAge")(0))).Days)
                    gbPassowordExpiration.Checked = True
                    gbPassowordExpiration.Enabled = False
                End If

                If sr.Properties.Contains("maxPwdAge") Then

                    If Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("maxPwdAge")(0))).Days) <= nudMaxDays.Minimum Then
                        nudMaxDays.Minimum = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("maxPwdAge")(0))).Days)
                    End If
                    If Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("maxPwdAge")(0))).Days) >= nudMaxDays.Maximum Then
                        nudMaxDays.Maximum = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("maxPwdAge")(0))).Days)
                    End If
                    nudMaxDays.Value = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("maxPwdAge")(0))).Days)
                    gbPassowordExpiration.Checked = True
                    gbPassowordExpiration.Enabled = False
                End If

                If sr.Properties.Contains("minPwdLength") Then
                    If Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdLength")(0))).Days) <= nudMinLength.Minimum Then
                        nudMinLength.Minimum = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdLength")(0))).Days)
                    End If
                    If Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdLength")(0))).Days) >= nudMinLength.Maximum Then
                        nudMinLength.Maximum = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("minPwdLength")(0))).Days)
                    End If
                    nudMinLength.Value = CInt(sr.Properties("minPwdLength")(0).ToString())
                    gbPasswordLength.Checked = True
                    gbPasswordLength.Enabled = False
                End If

                If sr.Properties.Contains("pwdProperties") Then
                    If Convert.ToBoolean(sr.Properties("pwdProperties")(0)) Then
                        chkSpecialCharsMandatory.Checked = True
                        chkUpperCaseMandatory.Checked = True
                        chkLowerCaseMandatory.Checked = True
                        chkNumericMandatory.Checked = True
                        chkPasswordComplexity.Checked = True
                        gbPasswordPolicy.Checked = True
                        gbPasswordPolicy.Enabled = False
                    End If
                End If


                If sr.Properties.Contains("lockoutDuration") Then
                    txtRetrydurattion.Text = Math.Abs(TimeSpan.FromTicks(Convert.ToInt64(sr.Properties("lockoutDuration")(0))).Minutes).ToString()
                    radLoginRetry.Checked = True
                    gbAccountLock.Checked = True
                    gbAccountLock.Enabled = False
                End If

                If sr.Properties.Contains("lockoutThreshold") Then

                    If CInt(sr.Properties("lockoutThreshold")(0)) <= nudAttempts.Minimum Then
                        nudAttempts.Minimum = CInt(sr.Properties("lockoutThreshold")(0))
                    End If

                    If CInt(sr.Properties("lockoutThreshold")(0)) >= nudAttempts.Maximum Then
                        nudAttempts.Maximum = CInt(sr.Properties("lockoutThreshold")(0))
                    End If

                    nudAttempts.Value = CInt(sr.Properties("lockoutThreshold")(0))
                    gbAccountLock.Checked = True
                    gbAccountLock.Enabled = False
                End If


                chkUserChangePwdnextLogon.Enabled = True

                'If (sr.Properties.Contains("lockOutObservationWindow")) Then
                '    mintLockOutDuration = TimeSpan.FromTicks((long)sr.Properties["lockOutObservationWindow"][0])
                '    MessageBox.Show("lockOutObservationWindow : " & mintLockOutDuration.Duration())
                'End If



                'If sr.Properties.Contains("pwdHistoryLength")) Then
                '      MessageBox.Show("pwdHistoryLength  : " & sr.Properties("pwdHistoryLength")(0).ToString());
                'End If
            Else
                chkUserChangePwdnextLogon.Checked = False
                chkUserChangePwdnextLogon.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkADUserFromEmpMst_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (15-Feb-2020) -- End

    'Pinkal (18-Aug-2018) -- End

#End Region


    'Pinkal (04-Jan-2021) -- Start
    'Enhancement NMB - Working Announcements for Self service Login Screen.

#Region "DataGrid Events"

    Private Sub dgvAnnouncements_DataBindingComplete(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvAnnouncements.DataBindingComplete
        Try
            Dim imgBlank As Drawing.Bitmap = CType(New Drawing.Bitmap(1, 1).Clone, Bitmap)
            For Each xdgvr As DataGridViewRow In dgvAnnouncements.Rows
                If CBool(xdgvr.Cells(objdgcolhMarkAsClose.Index).Value) Then
                    xdgvr.DefaultCellStyle.ForeColor = Color.Red
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAnnouncements_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvAnnouncements_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAnnouncements.CellClick
        Dim objAnnouncement As New clsAnnouncements_master
        Try
            If e.RowIndex <= -1 Then Exit Sub

            Select Case e.ColumnIndex

                Case objdgcolhEdit.Index

                    ResetAnnouncement()

                    objAnnouncement._Announcementunkid = CInt(dgvAnnouncements.Rows(e.RowIndex).Cells(objdgcolhAnnouncementId.Index).Value)

                    If objAnnouncement._Ismarkasclose Then Exit Sub

                    txtTitle.Text = objAnnouncement._Title.Trim()
                    txtTitle.Tag = objAnnouncement._Announcementunkid
                    txtAnnouncement.Rtf = objAnnouncement._Announcement.Trim()
                    chkRequireAcknowledgement.Checked = objAnnouncement._RequireAcknowledgement

                    If objAnnouncement._Photo IsNot Nothing Then
                        Dim ms As New System.IO.MemoryStream(objAnnouncement._Photo, 0, objAnnouncement._Photo.Length)
                        imgAnnouncement._Image = Image.FromStream(ms)
                    End If

                    Dim objDoc As New clsAnnouncement_document
                    objDoc.GetAnnouncementDocument("List", True, objAnnouncement._Announcementunkid, Nothing)
                    mdtAnnouncementDoc = objDoc._dtDocumentList.Copy()
                    objDoc = Nothing

                Case objdgcolhDelete.Index

                    ResetAnnouncement()

                    objAnnouncement._Announcementunkid = CInt(dgvAnnouncements.Rows(e.RowIndex).Cells(objdgcolhAnnouncementId.Index).Value)

                    If objAnnouncement._Ismarkasclose Then Exit Sub

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Are you sure you want to delete this announcement ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If

                    objAnnouncement._Userunkid = User._Object._Userunkid
                    objAnnouncement._Isvoid = True
                    objAnnouncement._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objAnnouncement._Voiduserunkid = User._Object._Userunkid
                    objAnnouncement._WebFormName = Me.Name
                    objAnnouncement._IsWeb = False

                    If objAnnouncement.Delete(CInt(dgvAnnouncements.Rows(e.RowIndex).Cells(objdgcolhAnnouncementId.Index).Value)) = False Then
                        eZeeMsgBox.Show(objAnnouncement._Message)
                        Exit Sub
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Announcement deleted successfully."), enMsgBoxStyle.Information)
                        ResetAnnouncement()
                        FillAnnouncements()
                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAnnouncements_CellClick", mstrModuleName)
        Finally
            objAnnouncement = Nothing
        End Try
    End Sub

#End Region

#Region " ToolStrip Events "

#Region " Font And Size "
    Private Sub cboFontSize_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles objcboFontSize.KeyPress
        Try
            If Not (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Then
                If (Asc(e.KeyChar) = Asc(GUI.DecimalSeparator) And InStr(Trim(objcboFontSize.Text), GUI.DecimalSeparator) = 0) Or Asc(e.KeyChar) = 8 Then Exit Sub
                e.KeyChar = CChar("")
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFontSize_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFontSize.TextChanged
        Try

            If objcboFontSize.Text = "" Or objcboFontSize.Text = "." Then
                Exit Sub
            Else
                Call ChangeFontSize(CDec(objcboFontSize.Text))
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFont_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFont.SelectedIndexChanged
        Try
            Call ChangeFont(objcboFont.Text)
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFont_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Alignments "

    Private Sub tlbbtnLeftAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftAlign.Click
        Try
            Dim strFormat As New StringFormat(StringFormatFlags.NoClip)
            objtlbbtnCenterAlign.Checked = False
            objtlbbtnRightAlign.Checked = False
            objtlbbtnLeftAlign.Checked = True

            If txtAnnouncement.SelectionAlignment <> HorizontalAlignment.Left Then
                txtAnnouncement.SelectionAlignment = HorizontalAlignment.Left
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnLeftAlign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tlbbtnCenterAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCenterAlign.Click
        Try
            objtlbbtnCenterAlign.Checked = True
            objtlbbtnRightAlign.Checked = False
            objtlbbtnLeftAlign.Checked = False

            If txtAnnouncement.SelectionAlignment <> HorizontalAlignment.Center Then
                txtAnnouncement.SelectionAlignment = HorizontalAlignment.Center
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnCenterAlign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tlbbtnRightAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightAlign.Click
        Try
            objtlbbtnCenterAlign.Checked = False
            objtlbbtnRightAlign.Checked = True
            objtlbbtnLeftAlign.Checked = False
            If txtAnnouncement.SelectionAlignment <> HorizontalAlignment.Right Then
                txtAnnouncement.SelectionAlignment = HorizontalAlignment.Right
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnRightAlign_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Styles "

    Private Sub objtlbbtnBold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBold.Click
        Try
            If objtlbbtnBold.Checked = True Then
                Call ApplyStyle(FontStyle.Bold, True)
            Else
                Call ApplyStyle(FontStyle.Bold, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBold_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnItalic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnItalic.Click
        Try
            If objtlbbtnItalic.Checked = True Then
                Call ApplyStyle(FontStyle.Italic, True)
            Else
                Call ApplyStyle(FontStyle.Italic, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnItalic_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnUnderline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUnderline.Click
        Try
            If objtlbbtnUnderline.Checked = True Then
                Call ApplyStyle(FontStyle.Underline, True)
            Else
                Call ApplyStyle(FontStyle.Underline, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUnderline_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Operations "

    Private Sub objtlbbtnUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUndo.Click
        Try
            txtAnnouncement.Undo()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUndo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnRedo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRedo.Click
        Try
            txtAnnouncement.Redo()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRedo_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objtlbbtnBulletSimple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBulletSimple.Click
        Try
            If objtlbbtnBulletSimple.Checked = True Then
                txtAnnouncement.SelectionBullet = True
            Else
                txtAnnouncement.SelectionBullet = False
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBulletSimple_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnLeftIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftIndent.Click
        Try
            If txtAnnouncement.SelectionIndent = 0 Then
                txtAnnouncement.SelectionIndent = 5
            Else
                txtAnnouncement.SelectionIndent += 10
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnLeftIndent_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnRightIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightIndent.Click
        Try
            If txtAnnouncement.SelectionRightIndent = 0 Then
                txtAnnouncement.SelectionRightIndent = 5
            Else
                txtAnnouncement.SelectionRightIndent += 10
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRightIndent_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCut.Click
        Try
            If txtAnnouncement.SelectedText.Length > 0 Then
                My.Computer.Clipboard.SetText(txtAnnouncement.SelectedText)
                txtAnnouncement.SelectedText = ""
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnCut_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCopy.Click
        Try
            If txtAnnouncement.SelectedText.Length > 0 Then
                My.Computer.Clipboard.SetText(txtAnnouncement.SelectedText)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnCopy_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnPaste.Click
        Try
            If My.Computer.Clipboard.ContainsText Then
                txtAnnouncement.AppendText(My.Computer.Clipboard.GetText())
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnPaste_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnColor.Click
        If pnlColor.Visible = True Then
            pnlColor.Hide()
            pnlColor.SendToBack()
        End If
        pnlColor.BringToFront()
        pnlColor.Show()
    End Sub

    Private Sub SetColor(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objbtn1.MouseClick, _
                                objbtn2.MouseClick, objbtn3.MouseClick, objbtn4.MouseClick, objbtn5.MouseClick, objbtn6.MouseClick, objbtn7.MouseClick, _
                                objbtn8.MouseClick, objbtn9.MouseClick, objbtn10.MouseClick, objbtn11.MouseClick, objbtn12.MouseClick, objbtn13.MouseClick, objbtn14.MouseClick, _
                                 objbtn15.MouseClick, objbtn16.MouseClick, objbtn17.MouseClick, objbtn18.MouseClick, objbtn19.MouseClick, _
                                objbtn20.MouseClick, objbtn21.MouseClick, objbtn22.MouseClick, objbtn23.MouseClick, objbtn24.MouseClick, objbtn25.MouseClick, _
                                objbtn26.MouseClick, objbtn27.MouseClick, objbtn28.MouseClick, objbtn29.MouseClick, objbtn30.MouseClick, objbtn31.MouseClick, _
                                objbtn32.MouseClick, objbtn33.MouseClick, objbtn34.MouseClick, objbtn35.MouseClick, objbtn36.MouseClick, objbtn37.MouseClick, _
                                objbtn38.MouseClick, objbtn39.MouseClick, objbtn40.MouseClick, objbtn41.MouseClick, objbtn42.MouseClick, objbtn43.MouseClick, _
                                 objbtn44.MouseClick, objbtn45.MouseClick, objbtn46.MouseClick, objbtn47.MouseClick, objbtn48.MouseClick

        Try
            txtAnnouncement.SelectionColor = CType(sender, Button).BackColor
            Call RegainOldStyle()
            pnlColor.Hide()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMoreColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoreColor.Click
        Try
            ColorDialog1.ShowDialog()
            txtAnnouncement.SelectionColor = ColorDialog1.Color
            pnlColor.Hide()
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMoreColor_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#End Region

    'Pinkal (04-Jan-2021) -- End





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbPasswordHistorySettings.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPasswordHistorySettings.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPassowordExpiration.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPassowordExpiration.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPasswordPolicy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPasswordPolicy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPasswordLength.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPasswordLength.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbUserLoginOptions.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbUserLoginOptions.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbUnameLength.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbUnameLength.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAccountLock.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAccountLock.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbApplicationIdleSetting.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApplicationIdleSetting.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnAttachment.GradientBackColor = GUI._ButttonBackColor
            Me.btnAttachment.GradientForeColor = GUI._ButttonFontColor

            Me.btnMarkAsClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnMarkAsClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAnnoucementSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnAnnoucementSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.tbpOtherOptions.Text = Language._Object.getCaption(Me.tbpOtherOptions.Name, Me.tbpOtherOptions.Text)
            Me.tbpAnnouncements.Text = Language._Object.getCaption(Me.tbpAnnouncements.Name, Me.tbpAnnouncements.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.gbPasswordHistorySettings.Text = Language._Object.getCaption(Me.gbPasswordHistorySettings.Name, Me.gbPasswordHistorySettings.Text)
            Me.chkDonotAllowReuseOfPasswordUsedInTheLast.Text = Language._Object.getCaption(Me.chkDonotAllowReuseOfPasswordUsedInTheLast.Name, Me.chkDonotAllowReuseOfPasswordUsedInTheLast.Text)
            Me.lblDays.Text = Language._Object.getCaption(Me.lblDays.Name, Me.lblDays.Text)
            Me.chkDonotAllowToReuseAnyOfTheLast.Text = Language._Object.getCaption(Me.chkDonotAllowToReuseAnyOfTheLast.Name, Me.chkDonotAllowToReuseAnyOfTheLast.Text)
            Me.lblPasswordsUsed.Text = Language._Object.getCaption(Me.lblPasswordsUsed.Name, Me.lblPasswordsUsed.Text)
            Me.gbPassowordExpiration.Text = Language._Object.getCaption(Me.gbPassowordExpiration.Name, Me.gbPassowordExpiration.Text)
            Me.lblMaximumDays.Text = Language._Object.getCaption(Me.lblMaximumDays.Name, Me.lblMaximumDays.Text)
            Me.lblMinimumDays.Text = Language._Object.getCaption(Me.lblMinimumDays.Name, Me.lblMinimumDays.Text)
            Me.gbPasswordPolicy.Text = Language._Object.getCaption(Me.gbPasswordPolicy.Name, Me.gbPasswordPolicy.Text)
            Me.LblPasswordComplexity.Text = Language._Object.getCaption(Me.LblPasswordComplexity.Name, Me.LblPasswordComplexity.Text)
            Me.chkPasswordComplexity.Text = Language._Object.getCaption(Me.chkPasswordComplexity.Name, Me.chkPasswordComplexity.Text)
            Me.chkSpecialCharsMandatory.Text = Language._Object.getCaption(Me.chkSpecialCharsMandatory.Name, Me.chkSpecialCharsMandatory.Text)
            Me.chkNumericMandatory.Text = Language._Object.getCaption(Me.chkNumericMandatory.Name, Me.chkNumericMandatory.Text)
            Me.chkLowerCaseMandatory.Text = Language._Object.getCaption(Me.chkLowerCaseMandatory.Name, Me.chkLowerCaseMandatory.Text)
            Me.chkUpperCaseMandatory.Text = Language._Object.getCaption(Me.chkUpperCaseMandatory.Name, Me.chkUpperCaseMandatory.Text)
            Me.gbPasswordLength.Text = Language._Object.getCaption(Me.gbPasswordLength.Name, Me.gbPasswordLength.Text)
            Me.lblMinimumLength.Text = Language._Object.getCaption(Me.lblMinimumLength.Name, Me.lblMinimumLength.Text)
            Me.gbUserLoginOptions.Text = Language._Object.getCaption(Me.gbUserLoginOptions.Name, Me.gbUserLoginOptions.Text)
            Me.chkAllowLoginOnActualEOCRetireDate.Text = Language._Object.getCaption(Me.chkAllowLoginOnActualEOCRetireDate.Name, Me.chkAllowLoginOnActualEOCRetireDate.Text)
            Me.EZeeLine2.Text = Language._Object.getCaption(Me.EZeeLine2.Name, Me.EZeeLine2.Text)
            Me.chkUserChangePwdnextLogon.Text = Language._Object.getCaption(Me.chkUserChangePwdnextLogon.Name, Me.chkUserChangePwdnextLogon.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.chkADUserFromEmpMst.Text = Language._Object.getCaption(Me.chkADUserFromEmpMst.Name, Me.chkADUserFromEmpMst.Text)
            Me.LblDomain.Text = Language._Object.getCaption(Me.LblDomain.Name, Me.LblDomain.Text)
            Me.LblDomainPassword.Text = Language._Object.getCaption(Me.LblDomainPassword.Name, Me.LblDomainPassword.Text)
            Me.CheckBox1.Text = Language._Object.getCaption(Me.CheckBox1.Name, Me.CheckBox1.Text)
            Me.chkEnableAllUser.Text = Language._Object.getCaption(Me.chkEnableAllUser.Name, Me.chkEnableAllUser.Text)
            Me.LblDomainUser.Text = Language._Object.getCaption(Me.LblDomainUser.Name, Me.LblDomainUser.Text)
            Me.chkProhibitPasswordChange.Text = Language._Object.getCaption(Me.chkProhibitPasswordChange.Name, Me.chkProhibitPasswordChange.Text)
            Me.chkImportEmployee.Text = Language._Object.getCaption(Me.chkImportEmployee.Name, Me.chkImportEmployee.Text)
            Me.lblADSeverIP.Text = Language._Object.getCaption(Me.lblADSeverIP.Name, Me.lblADSeverIP.Text)
            Me.radSSOAuthentication.Text = Language._Object.getCaption(Me.radSSOAuthentication.Name, Me.radSSOAuthentication.Text)
            Me.radADAuthentication.Text = Language._Object.getCaption(Me.radADAuthentication.Name, Me.radADAuthentication.Text)
            Me.radBasicAuthentication.Text = Language._Object.getCaption(Me.radBasicAuthentication.Name, Me.radBasicAuthentication.Text)
            Me.gbUnameLength.Text = Language._Object.getCaption(Me.gbUnameLength.Name, Me.gbUnameLength.Text)
            Me.lblUMinLength.Text = Language._Object.getCaption(Me.lblUMinLength.Name, Me.lblUMinLength.Text)
            Me.gbAccountLock.Text = Language._Object.getCaption(Me.gbAccountLock.Name, Me.gbAccountLock.Text)
            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
            Me.radAdminUnlock.Text = Language._Object.getCaption(Me.radAdminUnlock.Name, Me.radAdminUnlock.Text)
            Me.lblMinutes.Text = Language._Object.getCaption(Me.lblMinutes.Name, Me.lblMinutes.Text)
            Me.radLoginRetry.Text = Language._Object.getCaption(Me.radLoginRetry.Name, Me.radLoginRetry.Text)
            Me.lblAttempts.Text = Language._Object.getCaption(Me.lblAttempts.Name, Me.lblAttempts.Text)
            Me.lblLockAttempts.Text = Language._Object.getCaption(Me.lblLockAttempts.Name, Me.lblLockAttempts.Text)
            Me.gbApplicationIdleSetting.Text = Language._Object.getCaption(Me.gbApplicationIdleSetting.Name, Me.gbApplicationIdleSetting.Text)
            Me.lblIdleMinutes.Text = Language._Object.getCaption(Me.lblIdleMinutes.Name, Me.lblIdleMinutes.Text)
            Me.lblAutoLogout.Text = Language._Object.getCaption(Me.lblAutoLogout.Name, Me.lblAutoLogout.Text)
            Me.dgcolhTitle.HeaderText = Language._Object.getCaption(Me.dgcolhTitle.Name, Me.dgcolhTitle.HeaderText)
            Me.btnAttachment.Text = Language._Object.getCaption(Me.btnAttachment.Name, Me.btnAttachment.Text)
            Me.chkRequireAcknowledgement.Text = Language._Object.getCaption(Me.chkRequireAcknowledgement.Name, Me.chkRequireAcknowledgement.Text)
            Me.btnMarkAsClose.Text = Language._Object.getCaption(Me.btnMarkAsClose.Name, Me.btnMarkAsClose.Text)
            Me.LblAnnouncement.Text = Language._Object.getCaption(Me.LblAnnouncement.Name, Me.LblAnnouncement.Text)
            Me.LblTitle.Text = Language._Object.getCaption(Me.LblTitle.Name, Me.LblTitle.Text)
            Me.imgAnnouncement.Text = Language._Object.getCaption(Me.imgAnnouncement.Name, Me.imgAnnouncement.Text)
            Me.btnAnnoucementSave.Text = Language._Object.getCaption(Me.btnAnnoucementSave.Name, Me.btnAnnoucementSave.Text)
            Me.btnMoreColor.Text = Language._Object.getCaption(Me.btnMoreColor.Name, Me.btnMoreColor.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Login retry cannot be zero. Please give some number which is greater than zero.")
            Language.setMessage(mstrModuleName, 2, "Information saved successfully.")
            Language.setMessage(mstrModuleName, 3, "Auto Log out Idle Minutes cannot be zero. Please give some number which is greater than zero.")
            Language.setMessage(mstrModuleName, 4, "Active directory server address is mandatory information. Please set Active directory server address.")
            Language.setMessage(mstrModuleName, 5, "Domain User is mandatory information for active directory server. Please set domain user for active directory server.")
            Language.setMessage(mstrModuleName, 6, "Domain Password is mandatory information for active directory server. Please set domain password for active directory server.")
            Language.setMessage(mstrModuleName, 7, "Domain Name is mandatory information for active directory server. Please set domain Name for active directory server.")
            Language.setMessage(mstrModuleName, 8, "Title is compulsory information.Please enter Title.")
            Language.setMessage(mstrModuleName, 9, "Announcement is compulsory information.Please enter Announcement.")
            Language.setMessage(mstrModuleName, 10, "Sorry,You cannot set image file greater than")
            Language.setMessage(mstrModuleName, 11, " MB.")
            Language.setMessage(mstrModuleName, 12, "Announcement saved successfully.")
            Language.setMessage(mstrModuleName, 13, "Are you sure you want to delete this announcement ?")
            Language.setMessage(mstrModuleName, 14, "Announcement deleted successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class