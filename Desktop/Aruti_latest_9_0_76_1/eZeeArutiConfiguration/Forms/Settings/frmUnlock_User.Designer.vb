﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUnlock_User
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUnlock_User))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnUnlock = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvUserList = New System.Windows.Forms.ListView
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhUser = New System.Windows.Forms.ColumnHeader
        Me.colhIP = New System.Windows.Forms.ColumnHeader
        Me.colhHost = New System.Windows.Forms.ColumnHeader
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.colhAttempt_Date = New System.Windows.Forms.ColumnHeader
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(521, 304)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnUnlock)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 249)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(521, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnUnlock
        '
        Me.btnUnlock.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnlock.BackColor = System.Drawing.Color.White
        Me.btnUnlock.BackgroundImage = CType(resources.GetObject("btnUnlock.BackgroundImage"), System.Drawing.Image)
        Me.btnUnlock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnlock.BorderColor = System.Drawing.Color.Empty
        Me.btnUnlock.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUnlock.FlatAppearance.BorderSize = 0
        Me.btnUnlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnlock.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnlock.ForeColor = System.Drawing.Color.Black
        Me.btnUnlock.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnlock.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlock.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.Location = New System.Drawing.Point(309, 13)
        Me.btnUnlock.Name = "btnUnlock"
        Me.btnUnlock.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlock.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.Size = New System.Drawing.Size(97, 30)
        Me.btnUnlock.TabIndex = 132
        Me.btnUnlock.Text = "&Unlock"
        Me.btnUnlock.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(412, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 131
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvUserList
        '
        Me.lvUserList.CheckBoxes = True
        Me.lvUserList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhUser, Me.colhIP, Me.colhHost, Me.colhAttempt_Date})
        Me.lvUserList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvUserList.FullRowSelect = True
        Me.lvUserList.GridLines = True
        Me.lvUserList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvUserList.Location = New System.Drawing.Point(0, 0)
        Me.lvUserList.Name = "lvUserList"
        Me.lvUserList.Size = New System.Drawing.Size(521, 249)
        Me.lvUserList.TabIndex = 2
        Me.lvUserList.UseCompatibleStateImageBehavior = False
        Me.lvUserList.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 26
        '
        'colhUser
        '
        Me.colhUser.Tag = "colhUser"
        Me.colhUser.Text = "User"
        Me.colhUser.Width = 175
        '
        'colhIP
        '
        Me.colhIP.Tag = "colhIP"
        Me.colhIP.Text = "IP Address"
        Me.colhIP.Width = 110
        '
        'colhHost
        '
        Me.colhHost.Tag = "colhHost"
        Me.colhHost.Text = "Host"
        Me.colhHost.Width = 113
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(8, 6)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 3
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'colhAttempt_Date
        '
        Me.colhAttempt_Date.Tag = "colhAttempt_Date"
        Me.colhAttempt_Date.Text = "Attempt Date"
        Me.colhAttempt_Date.Width = 90
        '
        'frmUnlock_User
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 304)
        Me.Controls.Add(Me.objchkAll)
        Me.Controls.Add(Me.lvUserList)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUnlock_User"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Unlock User"
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnUnlock As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvUserList As System.Windows.Forms.ListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUser As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIP As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhHost As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents colhAttempt_Date As System.Windows.Forms.ColumnHeader
End Class
