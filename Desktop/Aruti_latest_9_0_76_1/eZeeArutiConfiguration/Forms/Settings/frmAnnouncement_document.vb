﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO

Public Class frmAnnouncement_document


#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAnnouncement_document"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAnnouncementId As Integer = 0
    Private mdtTable As DataTable = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal xTitle As String, ByVal eAction As enAction, Optional ByVal xAnnouncemetId As Integer = -1) As Boolean
        Try
            menAction = eAction
            objLblAnnouncementTitle.Text = xTitle.ToString()
            mintAnnouncementId = xAnnouncemetId
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Property"

    Public Property _dtDocument() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

#End Region

#Region "Form Events"

    Private Sub frmAnnouncement_document_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillDocumentList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAnnouncement_document_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow = Nothing
        Try
            Dim dtRow() As DataRow = mdtTable.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtTable.NewRow
                dRow("anndocumentunkid") = -1
                dRow("announcementunkid") = mintAnnouncementId
                dRow("attached_date") = ConfigParameter._Object._CurrentDateAndTime
                dRow("filename") = f.Name
                dRow("fileuniquename") = objLblAnnouncementTitle.Text.Trim & "_" & Guid.NewGuid.ToString() & f.Extension
                dRow("file_data") = System.IO.File.ReadAllBytes(strfullpath)
                dRow("userunkid") = User._Object._Userunkid
                dRow("isvoid") = False
                dRow("voiddatetime") = DBNull.Value
                dRow("voiduserunkid") = -1
                dRow("voidreason") = ""
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
            End If
            mdtTable.Rows.Add(dRow)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
        End Try
    End Sub

    Private Sub FillDocumentList()
        Dim dtView As DataView
        Try
            If mdtTable Is Nothing Then Exit Sub

            dtView = New DataView(mdtTable, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgDocument.AutoGenerateColumns = False
            dgcolhFileName.DataPropertyName = "filename"
            objdgcolhDocumentId.DataPropertyName = "anndocumentunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            dgDocument.DataSource = dtView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDocumentList", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                Dim f As New System.IO.FileInfo(ofdAttachment.FileName)
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "You can not attach .exe(Executable File) for the security reason."))
                    Exit Sub
                End If
                AddDocumentAttachment(f, ofdAttachment.FileName.ToString)
                FillDocumentList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnBrowse_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Event"

    Private Sub dgDocument_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgDocument.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If e.ColumnIndex = objdgcolhDelete.Index Then
                If CInt(dgDocument.Rows(e.RowIndex).Cells(objdgcolhDocumentId.Index).Value) > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this document ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
                    Dim drRow = mdtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("anndocumentunkid") = CInt(dgDocument.Rows(e.RowIndex).Cells(objdgcolhDocumentId.Index).Value))
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        drRow(0)("isvoid") = True
                        drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        drRow(0)("voiduserunkid") = User._Object._Userunkid
                        drRow(0)("voidreason") = ""
                        drRow(0)("AUD") = "D"
                    End If
                ElseIf dgDocument.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString().Trim().Length > 0 Then
                    Dim drRow = mdtTable.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") = dgDocument.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString().Trim())
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        drRow(0)("isvoid") = True
                        drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        drRow(0)("voiduserunkid") = User._Object._Userunkid
                        drRow(0)("voidreason") = ""
                        drRow(0)("AUD") = "D"
                    End If
                End If
                FillDocumentList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgDocument_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnBrowse.GradientBackColor = GUI._ButttonBackColor 
			Me.btnBrowse.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.LblAnnouncementTitle.Text = Language._Object.getCaption(Me.LblAnnouncementTitle.Name, Me.LblAnnouncementTitle.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnBrowse.Text = Language._Object.getCaption(Me.btnBrowse.Name, Me.btnBrowse.Text)
			Me.dgcolhFileName.HeaderText = Language._Object.getCaption(Me.dgcolhFileName.Name, Me.dgcolhFileName.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "You can not attach .exe(Executable File) for the security reason.")
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class