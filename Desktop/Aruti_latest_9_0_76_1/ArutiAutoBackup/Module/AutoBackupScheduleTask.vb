﻿#Region " Imports "

Imports eZeeCommonLib
Imports Microsoft.SqlServer
Imports System.IO
Imports Aruti.Data
Imports System.Runtime.InteropServices

#End Region

Module AutoBackupScheduleTask

#Region " Private Variables "

    'S.SANDEEP [16 FEB 2015] -- START
    'Private objDatabase As New eZeeDatabase
    Private WithEvents objDatabase As New eZeeDatabase
    'S.SANDEEP [16 FEB 2015] -- END
    Private m_strLogFile As String
    Private ReadOnly mstrModuleName As String = "AutoBackupScheduleTask"
    Private objCompany As New clsCompany_Master
    Private mdtTable As DataTable

    'Sohail (04 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrTaskName As String = ""
    Private mintCompanyUnkId As Integer = 0
    Private mstrEmail1 As String = ""
    Private mstrEmail2 As String = ""
    Private mstrEmail3 As String = ""

    Private Const MF_BYCOMMAND As Integer = &H0
    Public Const SC_CLOSE As Integer = &HF060

    <DllImport("user32.dll")> _
    Public Function DeleteMenu(ByVal hMenu As IntPtr, ByVal nPosition As Integer, ByVal wFlags As Integer) As Integer
    End Function

    <DllImport("user32.dll")> _
    Private Function GetSystemMenu(ByVal hWnd As IntPtr, ByVal bRevert As Boolean) As IntPtr
    End Function

    <DllImport("kernel32.dll", ExactSpelling:=True)> _
    Private Function GetConsoleWindow() As IntPtr
    End Function
    'Sohail (04 Jul 2012) -- End

#End Region

#Region " Main "

    Public Sub Main()
        Dim objBackup As New clsBackup 'Sohail (04 Jul 2012)
        Dim m_strPath As String = String.Empty
        Try

            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), False), SC_CLOSE, MF_BYCOMMAND)
            'Sohail (04 Jul 2012) -- End

            m_strLogFile = IO.Path.Combine(My.Application.Info.DirectoryPath, "AutoBackup_LOG.txt")

            '===================================================================================
            System.Console.Title = "Backup schedule task"
            Call WriteLog("Backup schedule task:.....", ConsoleColor.Magenta)
            Call WriteLog(String.Format("> started at {0}", Now.ToString))
            '===================================================================================

            '===================================================================================
            '***** Check Command Line parameter
            '===================================================================================
            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT - Now task name included to send email on failure for disasier recovery.
            If My.Application.CommandLineArgs.Count < 1 Then
                'If My.Application.CommandLineArgs.Count <> 1 Then
                'Sohail (04 Jul 2012) -- End
                Call WriteLog(String.Format("> Error: Invalid Parameters!", Now.ToString), ConsoleColor.Red)
                Exit Sub
            Else
                m_strPath = My.Application.CommandLineArgs(0)

                If Not IO.Directory.Exists(m_strPath) Then
                    Call WriteLog(String.Format("> Error: Invalid Directory path!", Now.ToString), ConsoleColor.Red)
                    Exit Sub
                End If

                'Sohail (04 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                If My.Application.CommandLineArgs.Count > 1 Then
                    mstrTaskName = My.Application.CommandLineArgs(1)
                End If
                'Sohail (04 Jul 2012) -- End

            End If
            '===================================================================================
            '***** Connect to Local Database
            '===================================================================================
            objDatabase.ServerName = "(Local)"
            If Not objDatabase.Connect() Then
                Call WriteLog("> Error: Database Connection Fail.", ConsoleColor.Red)
                Exit Sub
            End If

            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If mstrTaskName.Trim <> "" Then
                Dim objDisRecov As New clsDisaster_recovery

                Dim intUnkId As Integer = objDisRecov.GetUnkId(mstrTaskName, getIP().ToString)

                If intUnkId > 0 Then
                    objDisRecov._Disasterrecoveryunkid = intUnkId

                    mintCompanyUnkId = objDisRecov._Companyunkid
                    mstrEmail1 = objDisRecov._Email1
                    mstrEmail2 = objDisRecov._Email2
                    mstrEmail3 = objDisRecov._Email3

                End If
            End If
            'Sohail (04 Jul 2012) -- End

            mdtTable = objCompany.Get_DataBaseList("DailyBackup", True, True)
            Dim strFinalPath As String = String.Empty

            If mdtTable.Rows.Count > 0 Then
                For Each dtRow As DataRow In mdtTable.Rows
                    strFinalPath = ""

                    '===================================================================================
                    '***** Start Backup
                    '===================================================================================



                    'Pinkal (04-Apr-2013) -- Start
                    'Enhancement : TRA Changes

                    'Sohail (04 Jul 2012) -- Start
                    'TRA - ENHANCEMENT

                    'If dtRow.Item("Database").ToString.ToUpper = "HRMSCONFIGURATION" OrElse _
                    '    dtRow.Item("Database").ToString.ToUpper = "ARUTIHRMS" Then
                    '    'If dtRow.Item("Database").ToString.ToUpper = "HRMSCONFIGURATION" Then
                    '    'Sohail (04 Jul 2012) -- End
                    '    System.IO.Directory.CreateDirectory(m_strPath & "\" & dtRow.Item("Database").ToString)
                    '    strFinalPath = m_strPath & "\" & dtRow.Item("Database").ToString

                    '    If objBackup.Is_DatabasePresent(dtRow.Item("Database").ToString.Trim) = False Then
                    '        If dtRow.Item("Database").ToString.Trim.ToUpper <> "ARUTIHRMS" Then
                    '            Call WriteLog(dtRow.Item("Database").ToString.Trim & " Database does not exist.")
                    '            Continue For
                    '        Else
                    '            Continue For
                    '        End If
                    '    End If

                    If dtRow.Item("Database").ToString.ToUpper = "HRMSCONFIGURATION" OrElse _
                      dtRow.Item("Database").ToString.ToUpper = "ARUTIHRMS" OrElse dtRow.Item("Database").ToString.ToUpper = "ARUTIIMAGES" Then
                        System.IO.Directory.CreateDirectory(m_strPath & "\" & dtRow.Item("Database").ToString)
                        strFinalPath = m_strPath & "\" & dtRow.Item("Database").ToString

                        If objBackup.Is_DatabasePresent(dtRow.Item("Database").ToString.Trim) = False Then
                            If dtRow.Item("Database").ToString.Trim.ToUpper <> "ARUTIHRMS" OrElse dtRow.Item("Database").ToString.ToUpper <> "ARUTIIMAGES" Then
                                Call WriteLog(dtRow.Item("Database").ToString.Trim & " Database does not exist.")
                                Continue For
                            Else
                                Continue For
                            End If
                        End If


                        'Pinkal (04-Apr-2013) -- End
                        System.Console.ForegroundColor = ConsoleColor.Gray
                        System.Console.Write(dtRow.Item("Database").ToString)

                        'S.SANDEEP [16 FEB 2015] -- START
                        System.Console.WriteLine("")
                        'S.SANDEEP [16 FEB 2015] -- END

                        System.Console.Write("> Backup Progressing: 0%")
                        'AddHandler objDatabase.PercentComplete, AddressOf objDatabase_PercentComplete
                        'AddHandler objDatabase.Completed, AddressOf objDatabase_Completed

                        eZeeDatabase.change_database(dtRow.Item("Database").ToString.ToString)
                        'S.SANDEEP [16 FEB 2015] -- START
                        'strFinalPath = objDatabase.Backup(strFinalPath)
                        Dim objSettings As New clsGeneralSettings
                        objSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
                        strFinalPath = objDatabase.Backup(strFinalPath, objSettings._ServerName)
                        objSettings = Nothing
                        'S.SANDEEP [16 FEB 2015] -- END

                        System.Console.WriteLine("")
                        Call WriteLog(String.Format("> Backup File Name: {0}", strFinalPath), ConsoleColor.Green)
                        strFinalPath = ""

                        'S.SANDEEP [16 FEB 2015] -- START
                        System.Console.WriteLine("")
                        'S.SANDEEP [16 FEB 2015] -- END
                    ElseIf CBool(dtRow.Item("IsGrp")) = False Then
                        If Not System.IO.Directory.Exists(m_strPath & "\" & dtRow.Item("Database").ToString.Trim) Then
                            System.IO.Directory.CreateDirectory(m_strPath & "\" & dtRow.Item("Database").ToString.Trim)
                        End If
                        strFinalPath = m_strPath & "\" & dtRow.Item("Database").ToString.Trim

                        If objBackup.Is_DatabasePresent(dtRow.Item("Database").ToString.Trim) = False Then

                            If dtRow.Item("Database").ToString.Trim.ToUpper <> "ARUTIHRMS" Then
                                Call WriteLog(dtRow.Item("Database").ToString.Trim & " Database does not exist.")
                                Continue For
                            Else
                                Continue For
                            End If

                        End If

                        'S.SANDEEP [16 FEB 2015] -- START
                        System.Console.ForegroundColor = ConsoleColor.Gray
                        System.Console.Write(dtRow.Item("Database").ToString)
                        System.Console.WriteLine("")
                        'S.SANDEEP [16 FEB 2015] -- END

                        System.Console.Write("> Backup Progressing: 0%")
                        'AddHandler objDatabase.PercentComplete, AddressOf objDatabase_PercentComplete
                        'AddHandler objDatabase.Completed, AddressOf objDatabase_Completed

                        eZeeDatabase.change_database(dtRow.Item("Database").ToString.Trim)
                        'S.SANDEEP [16 FEB 2015] -- START
                        'strFinalPath = objDatabase.Backup(strFinalPath)
                        Dim objSettings As New clsGeneralSettings
                        objSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
                        strFinalPath = objDatabase.Backup(strFinalPath, objSettings._ServerName)
                        objSettings = Nothing
                        'S.SANDEEP [16 FEB 2015] -- END
                        System.Console.WriteLine("")
                        Call WriteLog(String.Format("> Backup File Name: {0}", strFinalPath), ConsoleColor.Green)
                        strFinalPath = ""

                        'S.SANDEEP [16 FEB 2015] -- START
                        System.Console.WriteLine("")
                        'S.SANDEEP [16 FEB 2015] -- END

                    End If
                Next
            End If
        Catch ex As Exception
            Call WriteLog("Error:" & ex.Message)

            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If mstrEmail1.Trim <> "" Then
                Call SendFailureEmail(ex.Message)
            End If

            'Sohail (04 Jul 2012) -- End
        Finally
            '===================================================================================
            Call WriteLog(String.Format("> stopped at {0}", Now.ToString))
            Call WriteLog(New String("=", 50))
            '===================================================================================
            Threading.Thread.Sleep(5000)
        End Try
    End Sub

#End Region
    
#Region " Private Function "

    'S.SANDEEP [16 FEB 2015] -- START
    'Private Sub objDatabase_PercentComplete(ByVal sender As Object, ByVal e As PercentCompleteEventArgs)
    '    System.Console.SetCursorPosition(22, System.Console.CursorTop)
    '    System.Console.Write("{0}%", e.Percent)
    'End Sub

    'Private Sub objDatabase_Completed(ByVal sender As Object, ByVal e As ServerMessageEventArgs)
    '    System.Console.WriteLine("")
    '    Call WriteLog(e.Error.Message)
    'End Sub
    Private Sub objDatabase_Progress(ByVal sender As Object, ByVal e As System.Data.SqlClient.SqlInfoMessageEventArgs) Handles objDatabase.Progress
        System.Console.SetCursorPosition(22, System.Console.CursorTop)
        System.Console.ForegroundColor = ConsoleColor.Magenta
        For Each info As SqlClient.SqlError In e.Errors
            If IsNumeric(info.Message.Split(CChar(" "))(0)) Then
                System.Console.Write("{0}%", CInt(info.Message.Split(CChar(" "))(0)))
            End If
        Next
    End Sub
    'S.SANDEEP [16 FEB 2015] -- END



    Private Sub WriteLog(ByVal strMsg As String, Optional ByVal color As System.ConsoleColor = System.ConsoleColor.Gray)
        If m_strLogFile <> "" Then System.IO.File.AppendAllText(m_strLogFile, strMsg & vbCrLf)
        System.Console.ForegroundColor = color
        System.Console.WriteLine(strMsg)
    End Sub

    'Sohail (04 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub SendFailureEmail(ByVal strErrorMessage As String)
        Dim objCompany As New clsCompany_Master
        Dim strToEmail As String = mstrEmail1
        Dim strMessage As String
        'Hemant (01 Feb 2022) -- Start
        'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.  
        Dim _Tls12 As System.Security.Authentication.SslProtocols = CType(&HC00, System.Security.Authentication.SslProtocols)
        Dim Tls12 As System.Net.SecurityProtocolType = CType(_Tls12, System.Net.SecurityProtocolType)
        'Hemant (01 Feb 2022) -- End
        Try
            objCompany._Companyunkid = mintCompanyUnkId

            If mstrEmail2.Trim <> "" Then
                strToEmail &= ";" & mstrEmail2.Trim
            End If
            If mstrEmail3.Trim <> "" Then
                strToEmail &= ";" & mstrEmail3.Trim
            End If
            strMessage = "Dear sir / madam"
            strMessage &= "<BR>"
            strMessage &= "<BR> Auto Backup process for Disaster Recovery failed"
            strMessage &= "<BR> Error : " & strErrorMessage
            strMessage &= "<BR>"
            strMessage &= "<BR>"

            'S.SANDEEP |05-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'strMessage &= "<BR><Center><B><I>PPOWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.</I></B></Center>"
            Dim objConfig As New clsConfigOptions
            Dim blnValue As Boolean = True
            blnValue = objConfig.GetKeyValue(objCompany._Companyunkid, "ShowArutisignature")
            If blnValue = True Then
                strMessage &= "<BR><Center><B><I>POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.</I></B></Center>"
            End If
            objConfig = Nothing
            'S.SANDEEP |05-MAR-2019| -- END

            'Sohail (18 Jul 2019) -- Start
            'Internal Enhancement - Ref #  - 76.1 - Show Sender Name instead of sender email in mail box.
            'Dim objMail As New System.Net.Mail.MailMessage(objCompany._Senderaddress, strToEmail)
            Dim strSenderDispName As String = objCompany._Sendername
            If strSenderDispName.Trim.Length <= 0 Then
                strSenderDispName = objCompany._Senderaddress
            End If
            Dim objMail As New System.Net.Mail.MailMessage(New System.Net.Mail.MailAddress(objCompany._Senderaddress, strSenderDispName), New System.Net.Mail.MailAddress(strToEmail))
            'Sohail (18 Jul 2019) -- End
            objMail.Body = strMessage
            objMail.Subject = "Disaster Recovery Auto Backup process failed"
            objMail.DeliveryNotificationOptions = Net.Mail.DeliveryNotificationOptions.OnSuccess Or Net.Mail.DeliveryNotificationOptions.OnFailure
            objMail.Headers.Add("Disposition-Notification-To", objCompany._Senderaddress)
            objMail.Headers.Add("Return-Receipt-To", objCompany._Senderaddress)
            objMail.IsBodyHtml = True
            Dim SmtpMail As New System.Net.Mail.SmtpClient()
            SmtpMail.Host = objCompany._Mailserverip
            SmtpMail.Port = objCompany._Mailserverport
            If objCompany._Username.Trim <> "" Then
                SmtpMail.Credentials = New System.Net.NetworkCredential(objCompany._Username, objCompany._Password)
            End If
            SmtpMail.EnableSsl = objCompany._Isloginssl
            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            If SmtpMail.Port = 587 Then
                If CInt(objCompany._Protocolunkid) = enAuthenticationProtocol.TLS Then
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls
                ElseIf CInt(objCompany._Protocolunkid) = enAuthenticationProtocol.SSL3 Then
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3
                ElseIf CInt(objCompany._Protocolunkid) = enAuthenticationProtocol.TLS12 Then
                    System.Net.ServicePointManager.SecurityProtocol = Tls12
                Else
                End If
            End If
            'Hemant (01 Feb 2022) -- End
            SmtpMail.Send(objMail)


        Catch ex As Exception
            Call WriteLog("Error in SendFailureEmail:" & ex.Message)
        End Try
    End Sub
    'Sohail (04 Jul 2012) -- End

#End Region

End Module
