﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransferEmployee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransferEmployee))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.EZeeSearchResetButton1 = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.cboService = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.lblService = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.cboAccess = New System.Windows.Forms.ComboBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.lblAccess = New System.Windows.Forms.Label
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.gbTransferEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSelectToAll = New System.Windows.Forms.LinkLabel
        Me.lnkSelectall = New System.Windows.Forms.LinkLabel
        Me.objbtnMultiReTransfer = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvToEmployee = New System.Windows.Forms.ListView
        Me.colhToEmployee = New System.Windows.Forms.ColumnHeader
        Me.lvFromEmployee = New System.Windows.Forms.ListView
        Me.colhFromEmployee = New System.Windows.Forms.ColumnHeader
        Me.objbtnOneTransfer = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.cboToCompany = New System.Windows.Forms.ComboBox
        Me.lblToCompany = New System.Windows.Forms.Label
        Me.txtFromCompany = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompanyName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnTransfer = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.EZeeSearchResetButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbTransferEmployee.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.gbTransferEmployee)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(662, 611)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeSearchResetButton1)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrade)
        Me.gbFilterCriteria.Controls.Add(Me.cboService)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrade)
        Me.gbFilterCriteria.Controls.Add(Me.lblService)
        Me.gbFilterCriteria.Controls.Add(Me.cboClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboAccess)
        Me.gbFilterCriteria.Controls.Add(Me.lblClass)
        Me.gbFilterCriteria.Controls.Add(Me.lblAccess)
        Me.gbFilterCriteria.Controls.Add(Me.cboSections)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(639, 116)
        Me.gbFilterCriteria.TabIndex = 109
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(606, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 196
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(466, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(134, 21)
        Me.cboEmployee.TabIndex = 75
        '
        'cboJob
        '
        Me.cboJob.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboJob.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Items.AddRange(New Object() {"Select Department", "Developer", "Tester", "Sales and Support"})
        Me.cboJob.Location = New System.Drawing.Point(466, 34)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(134, 21)
        Me.cboJob.TabIndex = 74
        '
        'EZeeSearchResetButton1
        '
        Me.EZeeSearchResetButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EZeeSearchResetButton1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeSearchResetButton1.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.EZeeSearchResetButton1.Image = CType(resources.GetObject("EZeeSearchResetButton1.Image"), System.Drawing.Image)
        Me.EZeeSearchResetButton1.Location = New System.Drawing.Point(613, 1)
        Me.EZeeSearchResetButton1.Name = "EZeeSearchResetButton1"
        Me.EZeeSearchResetButton1.Size = New System.Drawing.Size(24, 23)
        Me.EZeeSearchResetButton1.TabIndex = 4
        Me.EZeeSearchResetButton1.TabStop = False
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(402, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(58, 15)
        Me.lblEmployee.TabIndex = 70
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(402, 37)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(58, 15)
        Me.lblJob.TabIndex = 69
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboGrade.Location = New System.Drawing.Point(274, 88)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(122, 21)
        Me.cboGrade.TabIndex = 18
        '
        'cboService
        '
        Me.cboService.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboService.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboService.FormattingEnabled = True
        Me.cboService.Items.AddRange(New Object() {"Select Department", "Developer", "Tester", "Sales and Support"})
        Me.cboService.Location = New System.Drawing.Point(274, 61)
        Me.cboService.Name = "cboService"
        Me.cboService.Size = New System.Drawing.Size(122, 21)
        Me.cboService.TabIndex = 17
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(210, 91)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(58, 15)
        Me.lblGrade.TabIndex = 16
        Me.lblGrade.Text = "Grade"
        '
        'lblService
        '
        Me.lblService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblService.Location = New System.Drawing.Point(210, 64)
        Me.lblService.Name = "lblService"
        Me.lblService.Size = New System.Drawing.Size(58, 15)
        Me.lblService.TabIndex = 15
        Me.lblService.Text = "Service"
        '
        'cboClass
        '
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboClass.Location = New System.Drawing.Point(274, 34)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(122, 21)
        Me.cboClass.TabIndex = 14
        '
        'cboAccess
        '
        Me.cboAccess.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAccess.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAccess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccess.FormattingEnabled = True
        Me.cboAccess.Items.AddRange(New Object() {"Select Department", "Developer", "Tester", "Sales and Support"})
        Me.cboAccess.Location = New System.Drawing.Point(82, 87)
        Me.cboAccess.Name = "cboAccess"
        Me.cboAccess.Size = New System.Drawing.Size(122, 21)
        Me.cboAccess.TabIndex = 13
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(210, 37)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(58, 15)
        Me.lblClass.TabIndex = 12
        Me.lblClass.Text = "Class"
        '
        'lblAccess
        '
        Me.lblAccess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccess.Location = New System.Drawing.Point(8, 90)
        Me.lblAccess.Name = "lblAccess"
        Me.lblAccess.Size = New System.Drawing.Size(68, 15)
        Me.lblAccess.TabIndex = 11
        Me.lblAccess.Text = "Access"
        '
        'cboSections
        '
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboSections.Location = New System.Drawing.Point(82, 60)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(122, 21)
        Me.cboSections.TabIndex = 9
        '
        'cboDepartment
        '
        Me.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Items.AddRange(New Object() {"Select Department", "Developer", "Tester", "Sales and Support"})
        Me.cboDepartment.Location = New System.Drawing.Point(82, 33)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(122, 21)
        Me.cboDepartment.TabIndex = 8
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(8, 63)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(68, 15)
        Me.lblSection.TabIndex = 4
        Me.lblSection.Text = "Section"
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 36)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(68, 15)
        Me.lblDepartment.TabIndex = 3
        Me.lblDepartment.Text = "Department"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(612, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(589, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'gbTransferEmployee
        '
        Me.gbTransferEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbTransferEmployee.Checked = False
        Me.gbTransferEmployee.CollapseAllExceptThis = False
        Me.gbTransferEmployee.CollapsedHoverImage = Nothing
        Me.gbTransferEmployee.CollapsedNormalImage = Nothing
        Me.gbTransferEmployee.CollapsedPressedImage = Nothing
        Me.gbTransferEmployee.CollapseOnLoad = False
        Me.gbTransferEmployee.Controls.Add(Me.lnkSelectToAll)
        Me.gbTransferEmployee.Controls.Add(Me.lnkSelectall)
        Me.gbTransferEmployee.Controls.Add(Me.objbtnMultiReTransfer)
        Me.gbTransferEmployee.Controls.Add(Me.lvToEmployee)
        Me.gbTransferEmployee.Controls.Add(Me.lvFromEmployee)
        Me.gbTransferEmployee.Controls.Add(Me.objbtnOneTransfer)
        Me.gbTransferEmployee.Controls.Add(Me.objelLine1)
        Me.gbTransferEmployee.Controls.Add(Me.cboToCompany)
        Me.gbTransferEmployee.Controls.Add(Me.lblToCompany)
        Me.gbTransferEmployee.Controls.Add(Me.txtFromCompany)
        Me.gbTransferEmployee.Controls.Add(Me.lblCompanyName)
        Me.gbTransferEmployee.ExpandedHoverImage = Nothing
        Me.gbTransferEmployee.ExpandedNormalImage = Nothing
        Me.gbTransferEmployee.ExpandedPressedImage = Nothing
        Me.gbTransferEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTransferEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTransferEmployee.HeaderHeight = 25
        Me.gbTransferEmployee.HeightOnCollapse = 0
        Me.gbTransferEmployee.LeftTextSpace = 0
        Me.gbTransferEmployee.Location = New System.Drawing.Point(12, 134)
        Me.gbTransferEmployee.Name = "gbTransferEmployee"
        Me.gbTransferEmployee.OpenHeight = 300
        Me.gbTransferEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTransferEmployee.ShowBorder = True
        Me.gbTransferEmployee.ShowCheckBox = False
        Me.gbTransferEmployee.ShowCollapseButton = False
        Me.gbTransferEmployee.ShowDefaultBorderColor = True
        Me.gbTransferEmployee.ShowDownButton = False
        Me.gbTransferEmployee.ShowHeader = True
        Me.gbTransferEmployee.Size = New System.Drawing.Size(639, 417)
        Me.gbTransferEmployee.TabIndex = 5
        Me.gbTransferEmployee.Temp = 0
        Me.gbTransferEmployee.Text = "Employee Transfer"
        Me.gbTransferEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSelectToAll
        '
        Me.lnkSelectToAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSelectToAll.Location = New System.Drawing.Point(552, 72)
        Me.lnkSelectToAll.Name = "lnkSelectToAll"
        Me.lnkSelectToAll.Size = New System.Drawing.Size(75, 16)
        Me.lnkSelectToAll.TabIndex = 103
        Me.lnkSelectToAll.TabStop = True
        Me.lnkSelectToAll.Text = "Select All"
        Me.lnkSelectToAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSelectall
        '
        Me.lnkSelectall.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSelectall.Location = New System.Drawing.Point(11, 72)
        Me.lnkSelectall.Name = "lnkSelectall"
        Me.lnkSelectall.Size = New System.Drawing.Size(75, 16)
        Me.lnkSelectall.TabIndex = 102
        Me.lnkSelectall.TabStop = True
        Me.lnkSelectall.Text = "Select All"
        Me.lnkSelectall.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnMultiReTransfer
        '
        Me.objbtnMultiReTransfer.BackColor = System.Drawing.Color.White
        Me.objbtnMultiReTransfer.BackgroundImage = CType(resources.GetObject("objbtnMultiReTransfer.BackgroundImage"), System.Drawing.Image)
        Me.objbtnMultiReTransfer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnMultiReTransfer.BorderColor = System.Drawing.Color.Empty
        Me.objbtnMultiReTransfer.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnMultiReTransfer.FlatAppearance.BorderSize = 0
        Me.objbtnMultiReTransfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnMultiReTransfer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnMultiReTransfer.ForeColor = System.Drawing.Color.Black
        Me.objbtnMultiReTransfer.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnMultiReTransfer.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnMultiReTransfer.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMultiReTransfer.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMultiReTransfer.Image = Global.Aruti.Main.My.Resources.Resources.Previous_24x24
        Me.objbtnMultiReTransfer.Location = New System.Drawing.Point(297, 255)
        Me.objbtnMultiReTransfer.Name = "objbtnMultiReTransfer"
        Me.objbtnMultiReTransfer.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMultiReTransfer.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMultiReTransfer.Size = New System.Drawing.Size(42, 40)
        Me.objbtnMultiReTransfer.TabIndex = 101
        Me.objbtnMultiReTransfer.UseVisualStyleBackColor = True
        '
        'lvToEmployee
        '
        Me.lvToEmployee.CheckBoxes = True
        Me.lvToEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhToEmployee})
        Me.lvToEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvToEmployee.Location = New System.Drawing.Point(345, 97)
        Me.lvToEmployee.Name = "lvToEmployee"
        Me.lvToEmployee.Size = New System.Drawing.Size(282, 315)
        Me.lvToEmployee.TabIndex = 97
        Me.lvToEmployee.UseCompatibleStateImageBehavior = False
        Me.lvToEmployee.View = System.Windows.Forms.View.Details
        '
        'colhToEmployee
        '
        Me.colhToEmployee.Tag = "colhToEmployee"
        Me.colhToEmployee.Text = "Employee"
        Me.colhToEmployee.Width = 260
        '
        'lvFromEmployee
        '
        Me.lvFromEmployee.CheckBoxes = True
        Me.lvFromEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFromEmployee})
        Me.lvFromEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvFromEmployee.Location = New System.Drawing.Point(11, 97)
        Me.lvFromEmployee.Name = "lvFromEmployee"
        Me.lvFromEmployee.Size = New System.Drawing.Size(280, 315)
        Me.lvFromEmployee.TabIndex = 96
        Me.lvFromEmployee.UseCompatibleStateImageBehavior = False
        Me.lvFromEmployee.View = System.Windows.Forms.View.Details
        '
        'colhFromEmployee
        '
        Me.colhFromEmployee.Tag = "colhFromEmployee"
        Me.colhFromEmployee.Text = "Employee"
        Me.colhFromEmployee.Width = 255
        '
        'objbtnOneTransfer
        '
        Me.objbtnOneTransfer.BackColor = System.Drawing.Color.White
        Me.objbtnOneTransfer.BackgroundImage = CType(resources.GetObject("objbtnOneTransfer.BackgroundImage"), System.Drawing.Image)
        Me.objbtnOneTransfer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnOneTransfer.BorderColor = System.Drawing.Color.Empty
        Me.objbtnOneTransfer.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnOneTransfer.FlatAppearance.BorderSize = 0
        Me.objbtnOneTransfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnOneTransfer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnOneTransfer.ForeColor = System.Drawing.Color.Black
        Me.objbtnOneTransfer.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnOneTransfer.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnOneTransfer.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnOneTransfer.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnOneTransfer.Image = Global.Aruti.Main.My.Resources.Resources.Next_24x24
        Me.objbtnOneTransfer.Location = New System.Drawing.Point(297, 209)
        Me.objbtnOneTransfer.Name = "objbtnOneTransfer"
        Me.objbtnOneTransfer.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnOneTransfer.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnOneTransfer.Size = New System.Drawing.Size(42, 40)
        Me.objbtnOneTransfer.TabIndex = 98
        Me.objbtnOneTransfer.UseVisualStyleBackColor = True
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 57)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(616, 11)
        Me.objelLine1.TabIndex = 95
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToCompany
        '
        Me.cboToCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboToCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboToCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToCompany.FormattingEnabled = True
        Me.cboToCompany.Location = New System.Drawing.Point(422, 33)
        Me.cboToCompany.Name = "cboToCompany"
        Me.cboToCompany.Size = New System.Drawing.Size(205, 21)
        Me.cboToCompany.TabIndex = 94
        '
        'lblToCompany
        '
        Me.lblToCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToCompany.Location = New System.Drawing.Point(342, 36)
        Me.lblToCompany.Name = "lblToCompany"
        Me.lblToCompany.Size = New System.Drawing.Size(74, 15)
        Me.lblToCompany.TabIndex = 93
        Me.lblToCompany.Text = "To Company"
        '
        'txtFromCompany
        '
        Me.txtFromCompany.BackColor = System.Drawing.SystemColors.Control
        Me.txtFromCompany.Flags = 0
        Me.txtFromCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFromCompany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFromCompany.Location = New System.Drawing.Point(95, 33)
        Me.txtFromCompany.Name = "txtFromCompany"
        Me.txtFromCompany.ReadOnly = True
        Me.txtFromCompany.Size = New System.Drawing.Size(196, 21)
        Me.txtFromCompany.TabIndex = 92
        Me.txtFromCompany.Text = "Uninor Co. Ltd."
        '
        'lblCompanyName
        '
        Me.lblCompanyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyName.Location = New System.Drawing.Point(8, 36)
        Me.lblCompanyName.Name = "lblCompanyName"
        Me.lblCompanyName.Size = New System.Drawing.Size(81, 15)
        Me.lblCompanyName.TabIndex = 91
        Me.lblCompanyName.Text = "From Company"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnTransfer)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 556)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(662, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnTransfer
        '
        Me.btnTransfer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTransfer.BackColor = System.Drawing.Color.White
        Me.btnTransfer.BackgroundImage = CType(resources.GetObject("btnTransfer.BackgroundImage"), System.Drawing.Image)
        Me.btnTransfer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTransfer.BorderColor = System.Drawing.Color.Empty
        Me.btnTransfer.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnTransfer.FlatAppearance.BorderSize = 0
        Me.btnTransfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTransfer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTransfer.ForeColor = System.Drawing.Color.Black
        Me.btnTransfer.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTransfer.GradientForeColor = System.Drawing.Color.Black
        Me.btnTransfer.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTransfer.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTransfer.Location = New System.Drawing.Point(450, 13)
        Me.btnTransfer.Name = "btnTransfer"
        Me.btnTransfer.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTransfer.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTransfer.Size = New System.Drawing.Size(97, 30)
        Me.btnTransfer.TabIndex = 0
        Me.btnTransfer.Text = "&Transfer"
        Me.btnTransfer.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(553, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmTransferEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(662, 611)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTransferEmployee"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Transfer Employee"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.EZeeSearchResetButton1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbTransferEmployee.ResumeLayout(False)
        Me.gbTransferEmployee.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnTransfer As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbTransferEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCompanyName As System.Windows.Forms.Label
    Friend WithEvents lblToCompany As System.Windows.Forms.Label
    Friend WithEvents txtFromCompany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboToCompany As System.Windows.Forms.ComboBox
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents lvToEmployee As System.Windows.Forms.ListView
    Friend WithEvents lvFromEmployee As System.Windows.Forms.ListView
    Friend WithEvents colhToEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFromEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnMultiReTransfer As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOneTransfer As eZee.Common.eZeeLightButton
    Friend WithEvents lnkSelectToAll As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSelectall As System.Windows.Forms.LinkLabel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeSearchResetButton1 As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents cboService As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents lblService As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboAccess As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents lblAccess As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
End Class
