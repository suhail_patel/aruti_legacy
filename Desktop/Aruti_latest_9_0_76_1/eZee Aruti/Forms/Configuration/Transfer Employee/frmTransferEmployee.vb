﻿Imports Aruti.Data
Imports eZeeCommonLib
Public Class frmTransferEmployee
    Private ReadOnly mstrModuleName As String = "frmTransferEmployee"

    Private Sub frmTransferEmployee_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Dim lvItem As ListViewItem
            lvFromEmployee.Items.Clear()
            For i As Integer = 0 To 100
                lvItem = New ListViewItem
                lvItem.Text = "Employee " & i.ToString
                lvFromEmployee.Items.Add(lvItem)
            Next
            cboToCompany.Items.Clear()
            cboToCompany.Items.Add("eZee Technosys")
            cboToCompany.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransferEmployee_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOneTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOneTransfer.Click
        Try
            For Each lvItem As ListViewItem In lvFromEmployee.CheckedItems
                Dim lvToItems As New ListViewItem
                lvToItems = lvItem
                lvFromEmployee.Items.Remove(lvItem)
                lvToEmployee.Items.Add(lvToItems)
                lvToItems.Checked = False
                lvToItems = Nothing
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOneTransfer_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnMultiReTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnMultiReTransfer.Click
        Try
            For Each lvItem As ListViewItem In lvToEmployee.CheckedItems
                Dim lvFromItems As New ListViewItem
                lvFromItems = lvItem
                lvToEmployee.Items.Remove(lvItem)
                lvFromEmployee.Items.Add(lvFromItems)
                lvFromItems.Checked = False
                lvFromItems = Nothing
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnMultiReTransfer_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSelectall_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSelectall.LinkClicked
        Try
            For i As Integer = 0 To lvFromEmployee.Items.Count - 1
                lvFromEmployee.Items(i).Checked = True
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSelectall_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSelectToAll_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSelectToAll.LinkClicked
        Try
            For i As Integer = 0 To lvToEmployee.Items.Count - 1
                lvToEmployee.Items(i).Checked = True
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSelectToAll_LinkClicked", mstrModuleName)
        End Try
    End Sub
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbTransferEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTransferEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnTransfer.GradientBackColor = GUI._ButttonBackColor 
			Me.btnTransfer.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnMultiReTransfer.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnMultiReTransfer.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnOneTransfer.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnOneTransfer.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnTransfer.Text = Language._Object.getCaption(Me.btnTransfer.Name, Me.btnTransfer.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbTransferEmployee.Text = Language._Object.getCaption(Me.gbTransferEmployee.Name, Me.gbTransferEmployee.Text)
			Me.lblCompanyName.Text = Language._Object.getCaption(Me.lblCompanyName.Name, Me.lblCompanyName.Text)
			Me.lblToCompany.Text = Language._Object.getCaption(Me.lblToCompany.Name, Me.lblToCompany.Text)
			Me.colhToEmployee.Text = Language._Object.getCaption(CStr(Me.colhToEmployee.Tag), Me.colhToEmployee.Text)
			Me.colhFromEmployee.Text = Language._Object.getCaption(CStr(Me.colhFromEmployee.Tag), Me.colhFromEmployee.Text)
			Me.lnkSelectToAll.Text = Language._Object.getCaption(Me.lnkSelectToAll.Name, Me.lnkSelectToAll.Text)
			Me.lnkSelectall.Text = Language._Object.getCaption(Me.lnkSelectall.Name, Me.lnkSelectall.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.EZeeSearchResetButton1.Text = Language._Object.getCaption(Me.EZeeSearchResetButton1.Name, Me.EZeeSearchResetButton1.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.lblService.Text = Language._Object.getCaption(Me.lblService.Name, Me.lblService.Text)
			Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
			Me.lblAccess.Text = Language._Object.getCaption(Me.lblAccess.Name, Me.lblAccess.Text)
			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class