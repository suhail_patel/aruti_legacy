﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportEmpAccConfigMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportEmpAccConfigMapping))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblShortName3 = New System.Windows.Forms.Label
        Me.cboShortName3 = New System.Windows.Forms.ComboBox
        Me.cboMapRefType = New System.Windows.Forms.ComboBox
        Me.lblMapRefType = New System.Windows.Forms.Label
        Me.cboMapRefName = New System.Windows.Forms.ComboBox
        Me.lblMapRefName = New System.Windows.Forms.Label
        Me.cboMapRefCode = New System.Windows.Forms.ComboBox
        Me.lblMapRefCode = New System.Windows.Forms.Label
        Me.lblShortName2 = New System.Windows.Forms.Label
        Me.cboShortName2 = New System.Windows.Forms.ComboBox
        Me.lblShortName = New System.Windows.Forms.Label
        Me.cboShortName = New System.Windows.Forms.ComboBox
        Me.lblAccountCode = New System.Windows.Forms.Label
        Me.cboAccountCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblHeadCode = New System.Windows.Forms.Label
        Me.cboHeadCode = New System.Windows.Forms.ComboBox
        Me.lblTransactionType = New System.Windows.Forms.Label
        Me.cboTransactionType = New System.Windows.Forms.ComboBox
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbFieldMapping)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(394, 403)
        Me.pnlMainInfo.TabIndex = 2
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.cboPeriod)
        Me.gbFieldMapping.Controls.Add(Me.lblPeriod)
        Me.gbFieldMapping.Controls.Add(Me.lblShortName3)
        Me.gbFieldMapping.Controls.Add(Me.cboShortName3)
        Me.gbFieldMapping.Controls.Add(Me.cboMapRefType)
        Me.gbFieldMapping.Controls.Add(Me.lblMapRefType)
        Me.gbFieldMapping.Controls.Add(Me.cboMapRefName)
        Me.gbFieldMapping.Controls.Add(Me.lblMapRefName)
        Me.gbFieldMapping.Controls.Add(Me.cboMapRefCode)
        Me.gbFieldMapping.Controls.Add(Me.lblMapRefCode)
        Me.gbFieldMapping.Controls.Add(Me.lblShortName2)
        Me.gbFieldMapping.Controls.Add(Me.cboShortName2)
        Me.gbFieldMapping.Controls.Add(Me.lblShortName)
        Me.gbFieldMapping.Controls.Add(Me.cboShortName)
        Me.gbFieldMapping.Controls.Add(Me.lblAccountCode)
        Me.gbFieldMapping.Controls.Add(Me.cboAccountCode)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblHeadCode)
        Me.gbFieldMapping.Controls.Add(Me.cboHeadCode)
        Me.gbFieldMapping.Controls.Add(Me.lblTransactionType)
        Me.gbFieldMapping.Controls.Add(Me.cboTransactionType)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(12, 12)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(355, 330)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShortName3
        '
        Me.lblShortName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShortName3.Location = New System.Drawing.Point(8, 227)
        Me.lblShortName3.Name = "lblShortName3"
        Me.lblShortName3.Size = New System.Drawing.Size(120, 14)
        Me.lblShortName3.TabIndex = 256
        Me.lblShortName3.Text = "Short Name3"
        Me.lblShortName3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboShortName3
        '
        Me.cboShortName3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShortName3.DropDownWidth = 215
        Me.cboShortName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShortName3.FormattingEnabled = True
        Me.cboShortName3.Location = New System.Drawing.Point(134, 224)
        Me.cboShortName3.Name = "cboShortName3"
        Me.cboShortName3.Size = New System.Drawing.Size(202, 21)
        Me.cboShortName3.TabIndex = 255
        '
        'cboMapRefType
        '
        Me.cboMapRefType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMapRefType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMapRefType.FormattingEnabled = True
        Me.cboMapRefType.Location = New System.Drawing.Point(134, 305)
        Me.cboMapRefType.Name = "cboMapRefType"
        Me.cboMapRefType.Size = New System.Drawing.Size(202, 21)
        Me.cboMapRefType.TabIndex = 250
        '
        'lblMapRefType
        '
        Me.lblMapRefType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMapRefType.Location = New System.Drawing.Point(11, 308)
        Me.lblMapRefType.Name = "lblMapRefType"
        Me.lblMapRefType.Size = New System.Drawing.Size(118, 15)
        Me.lblMapRefType.TabIndex = 253
        Me.lblMapRefType.Text = "Map Reference Type"
        '
        'cboMapRefName
        '
        Me.cboMapRefName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMapRefName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMapRefName.FormattingEnabled = True
        Me.cboMapRefName.Location = New System.Drawing.Point(134, 278)
        Me.cboMapRefName.Name = "cboMapRefName"
        Me.cboMapRefName.Size = New System.Drawing.Size(202, 21)
        Me.cboMapRefName.TabIndex = 249
        '
        'lblMapRefName
        '
        Me.lblMapRefName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMapRefName.Location = New System.Drawing.Point(11, 281)
        Me.lblMapRefName.Name = "lblMapRefName"
        Me.lblMapRefName.Size = New System.Drawing.Size(118, 15)
        Me.lblMapRefName.TabIndex = 252
        Me.lblMapRefName.Text = "Map Reference Name"
        '
        'cboMapRefCode
        '
        Me.cboMapRefCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMapRefCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMapRefCode.FormattingEnabled = True
        Me.cboMapRefCode.Location = New System.Drawing.Point(134, 251)
        Me.cboMapRefCode.Name = "cboMapRefCode"
        Me.cboMapRefCode.Size = New System.Drawing.Size(202, 21)
        Me.cboMapRefCode.TabIndex = 248
        '
        'lblMapRefCode
        '
        Me.lblMapRefCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMapRefCode.Location = New System.Drawing.Point(11, 254)
        Me.lblMapRefCode.Name = "lblMapRefCode"
        Me.lblMapRefCode.Size = New System.Drawing.Size(118, 15)
        Me.lblMapRefCode.TabIndex = 251
        Me.lblMapRefCode.Text = "Map Reference Code"
        '
        'lblShortName2
        '
        Me.lblShortName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShortName2.Location = New System.Drawing.Point(8, 200)
        Me.lblShortName2.Name = "lblShortName2"
        Me.lblShortName2.Size = New System.Drawing.Size(120, 14)
        Me.lblShortName2.TabIndex = 247
        Me.lblShortName2.Text = "Short Name2"
        Me.lblShortName2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboShortName2
        '
        Me.cboShortName2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShortName2.DropDownWidth = 215
        Me.cboShortName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShortName2.FormattingEnabled = True
        Me.cboShortName2.Location = New System.Drawing.Point(134, 197)
        Me.cboShortName2.Name = "cboShortName2"
        Me.cboShortName2.Size = New System.Drawing.Size(202, 21)
        Me.cboShortName2.TabIndex = 246
        '
        'lblShortName
        '
        Me.lblShortName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShortName.Location = New System.Drawing.Point(8, 173)
        Me.lblShortName.Name = "lblShortName"
        Me.lblShortName.Size = New System.Drawing.Size(120, 14)
        Me.lblShortName.TabIndex = 245
        Me.lblShortName.Text = "Short Name"
        Me.lblShortName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboShortName
        '
        Me.cboShortName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShortName.DropDownWidth = 215
        Me.cboShortName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShortName.FormattingEnabled = True
        Me.cboShortName.Location = New System.Drawing.Point(134, 170)
        Me.cboShortName.Name = "cboShortName"
        Me.cboShortName.Size = New System.Drawing.Size(202, 21)
        Me.cboShortName.TabIndex = 244
        '
        'lblAccountCode
        '
        Me.lblAccountCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountCode.Location = New System.Drawing.Point(8, 146)
        Me.lblAccountCode.Name = "lblAccountCode"
        Me.lblAccountCode.Size = New System.Drawing.Size(120, 14)
        Me.lblAccountCode.TabIndex = 243
        Me.lblAccountCode.Text = "Account Code"
        Me.lblAccountCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAccountCode
        '
        Me.cboAccountCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountCode.DropDownWidth = 215
        Me.cboAccountCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountCode.FormattingEnabled = True
        Me.cboAccountCode.Location = New System.Drawing.Point(134, 143)
        Me.cboAccountCode.Name = "cboAccountCode"
        Me.cboAccountCode.Size = New System.Drawing.Size(202, 21)
        Me.cboAccountCode.TabIndex = 242
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(8, 119)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(120, 14)
        Me.lblEmployeeCode.TabIndex = 241
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.DropDownWidth = 215
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(134, 116)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(202, 21)
        Me.cboEmployeeCode.TabIndex = 240
        '
        'lblHeadCode
        '
        Me.lblHeadCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeadCode.Location = New System.Drawing.Point(8, 92)
        Me.lblHeadCode.Name = "lblHeadCode"
        Me.lblHeadCode.Size = New System.Drawing.Size(120, 14)
        Me.lblHeadCode.TabIndex = 239
        Me.lblHeadCode.Text = "Head Code"
        Me.lblHeadCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboHeadCode
        '
        Me.cboHeadCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeadCode.DropDownWidth = 215
        Me.cboHeadCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeadCode.FormattingEnabled = True
        Me.cboHeadCode.Location = New System.Drawing.Point(134, 89)
        Me.cboHeadCode.Name = "cboHeadCode"
        Me.cboHeadCode.Size = New System.Drawing.Size(202, 21)
        Me.cboHeadCode.TabIndex = 238
        '
        'lblTransactionType
        '
        Me.lblTransactionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionType.Location = New System.Drawing.Point(8, 65)
        Me.lblTransactionType.Name = "lblTransactionType"
        Me.lblTransactionType.Size = New System.Drawing.Size(120, 14)
        Me.lblTransactionType.TabIndex = 233
        Me.lblTransactionType.Text = "Transaction Type"
        Me.lblTransactionType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTransactionType
        '
        Me.cboTransactionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionType.DropDownWidth = 215
        Me.cboTransactionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionType.FormattingEnabled = True
        Me.cboTransactionType.Location = New System.Drawing.Point(134, 62)
        Me.cboTransactionType.Name = "cboTransactionType"
        Me.cboTransactionType.Size = New System.Drawing.Size(202, 21)
        Me.cboTransactionType.TabIndex = 232
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.lnkAutoMap)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnOk)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 348)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(394, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Location = New System.Drawing.Point(20, 22)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(124, 13)
        Me.lnkAutoMap.TabIndex = 2
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "&Auto Map"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(285, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(182, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(134, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(202, 21)
        Me.cboPeriod.TabIndex = 260
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(120, 15)
        Me.lblPeriod.TabIndex = 261
        Me.lblPeriod.Text = "Effective Period"
        '
        'frmImportEmpAccConfigMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(394, 403)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportEmpAccConfigMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import Employee Account Configuration Mapping"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblTransactionType As System.Windows.Forms.Label
    Friend WithEvents cboTransactionType As System.Windows.Forms.ComboBox
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents lblHeadCode As System.Windows.Forms.Label
    Friend WithEvents cboHeadCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblShortName As System.Windows.Forms.Label
    Friend WithEvents cboShortName As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountCode As System.Windows.Forms.Label
    Friend WithEvents cboAccountCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblShortName2 As System.Windows.Forms.Label
    Friend WithEvents cboShortName2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboMapRefType As System.Windows.Forms.ComboBox
    Friend WithEvents lblMapRefType As System.Windows.Forms.Label
    Friend WithEvents cboMapRefName As System.Windows.Forms.ComboBox
    Friend WithEvents lblMapRefName As System.Windows.Forms.Label
    Friend WithEvents cboMapRefCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblMapRefCode As System.Windows.Forms.Label
    Friend WithEvents lblShortName3 As System.Windows.Forms.Label
    Friend WithEvents cboShortName3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
End Class
