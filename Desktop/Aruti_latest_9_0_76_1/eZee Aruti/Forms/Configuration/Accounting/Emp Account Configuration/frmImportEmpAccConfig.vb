﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmImportEmpAccConfig

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportEmpAccConfig"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    Private mdtFilteredTable As DataTable
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As ExcelData
    'S.SANDEEP [12-Jan-2018] -- END
    Private m_Dataview As DataView
    'Sohail (25 Jul 2020) -- Start
    'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    'Sohail (25 Jul 2020) -- End

    Private mblnCopyPreviousEDSlab As Boolean
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Try

            dgvImportInfo.AutoGenerateColumns = False

            colhIsChecked.DataPropertyName = "IsChecked"
            objcolhTransactiontype_id.DataPropertyName = "transactiontype_id"
            colhTransactiontype_name.DataPropertyName = "transactiontype_name"
            objcolhHeadunkid.DataPropertyName = "headunkid"
            colhHeadcode.DataPropertyName = "headcode"
            objcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            colhEmployeecode.DataPropertyName = "employeecode"
            colhEmployeename.DataPropertyName = "employeename"
            objcolhAccountunkid.DataPropertyName = "accountunkid"
            colhAccountcode.DataPropertyName = "accountcode"
            colhShortname.DataPropertyName = "shortname"
            colhShortname2.DataPropertyName = "shortname2"
            colhShortname3.DataPropertyName = "shortname3" 'Sohail (03 Mar 2020)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objcolhPeriodunkid.DataPropertyName = "periodunkid"
            colhPeriodName.DataPropertyName = "period_name"
            'Sohail (03 Jul 2020) -- End
            colhMessage.DataPropertyName = "message"

            m_Dataview = New DataView(mdtTable)

            m_Dataview.RowFilter = "rowtypeid <> 0 "
            If m_Dataview.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Some transactions will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button."), enMsgBoxStyle.Information)
            End If

            m_Dataview.RowFilter = "rowtypeid = 0 "

            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmImportEmpAccConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'objIExcel = New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            txtFilePath.ReadOnly = True
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportEmpAccConfig_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsaccountconfig_employee.SetMessages()
            objfrm._Other_ModuleNames = "clsaccountconfig_employee"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSet.Click

    End Sub

    'Varsha Rana (11-Oct-2017) -- Start
    'Enhancement - Replace to search button with open file button.
    'Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
    'Varsha Rana (11-Oct-2017) -- End
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath
            ofdlgOpen.Filter = "XML files (*.xml)|*.xml|Excel files(*.xlsx)|*.xlsx"
            ofdlgOpen.FilterIndex = 2
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                Select Case ofdlgOpen.FilterIndex
                    Case 1
                        dsList.ReadXml(txtFilePath.Text)
                    Case 2
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'dsList = objIExcel.Import(txtFilePath.Text)
                        dsList = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                Dim frm As New frmImportEmpAccConfigMapping
                If frm.displayDialog(dsList) = False Then
                    mblnCancel = False
                    Exit Sub
                End If
                mdtTable = frm._DataTable
                mdtPeriodStart = frm._Period_StartDate
                mdtPeriodEnd = frm._Period_EndDate

                Call FillGirdView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim blnFlag As Boolean = False

        mblnCancel = True

        If mdtTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot import this file. Reason : No transactions are not there in this file to import."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        m_Dataview.RowFilter = "rowtypeid = 0 "
        mdtFilteredTable = m_Dataview.ToTable

        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot import this file. Reason : Some head codes, account codes or Employee Codes are not there in system."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please tick atleast one transaction from list to Import."), enMsgBoxStyle.Information)
            Exit Sub
        End If


        Try

            Cursor.Current = Cursors.WaitCursor

            Dim objEmpAcc As New clsaccountconfig_employee

            blnFlag = objEmpAcc.InsertAll(mdtFilteredTable, False, mdtPeriodEnd)
            'Sohail (25 Jul 2020) - [mdtPeriodEnd]

            If blnFlag = False And objEmpAcc._Message <> "" Then
                eZeeMsgBox.Show(objEmpAcc._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True Then
                Cursor.Current = Cursors.Default
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Account Configuration imported successfully!"), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub


    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            btnImport.Enabled = True
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            dgvImportInfo.DataSource = m_Dataview

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowUnsuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowUnsuccessful.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid <> 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            dgvImportInfo.DataSource = m_Dataview

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowUnsuccessful_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim dt As DataTable = m_Dataview.ToTable.Copy

                dt.Columns.Remove("Ischecked")
                dt.Columns.Remove("Accountconfigempunkid")
                dt.Columns.Remove("Transactiontype_Id")
                dt.Columns.Remove("Headunkid")
                dt.Columns.Remove("Headname")
                dt.Columns.Remove("Employeeunkid")
                dt.Columns.Remove("Accountunkid")
                dt.Columns.Remove("Accountname")
                dt.Columns.Remove("Referencecodeid")
                dt.Columns.Remove("Referencenameid")
                dt.Columns.Remove("Referencetypeid")
                dt.Columns.Remove("Rowtypeid")
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                dt.Columns.Remove("periodunkid")
                'Sohail (03 Jul 2020) -- End

                If modGlobal.Export_ErrorList(savDialog.FileName, dt, "Import Emp Account Config") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = objchkSelectAll.Checked
            Next
            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.objbtnSet.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnSet.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
			Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
			Me.elMandatoryInfo.Text = Language._Object.getCaption(Me.elMandatoryInfo.Name, Me.elMandatoryInfo.Text)
			Me.radApplySelected.Text = Language._Object.getCaption(Me.radApplySelected.Name, Me.radApplySelected.Text)
			Me.lblHeadTypeId.Text = Language._Object.getCaption(Me.lblHeadTypeId.Name, Me.lblHeadTypeId.Text)
			Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
			Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
			Me.lblTypeOfId.Text = Language._Object.getCaption(Me.lblTypeOfId.Name, Me.lblTypeOfId.Text)
			Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
			Me.mnuShowUnsuccessful.Text = Language._Object.getCaption(Me.mnuShowUnsuccessful.Name, Me.mnuShowUnsuccessful.Text)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.colhIsChecked.HeaderText = Language._Object.getCaption(Me.colhIsChecked.Name, Me.colhIsChecked.HeaderText)
			Me.colhPeriodName.HeaderText = Language._Object.getCaption(Me.colhPeriodName.Name, Me.colhPeriodName.HeaderText)
			Me.colhTransactiontype_name.HeaderText = Language._Object.getCaption(Me.colhTransactiontype_name.Name, Me.colhTransactiontype_name.HeaderText)
			Me.colhHeadcode.HeaderText = Language._Object.getCaption(Me.colhHeadcode.Name, Me.colhHeadcode.HeaderText)
			Me.colhEmployeecode.HeaderText = Language._Object.getCaption(Me.colhEmployeecode.Name, Me.colhEmployeecode.HeaderText)
			Me.colhEmployeename.HeaderText = Language._Object.getCaption(Me.colhEmployeename.Name, Me.colhEmployeename.HeaderText)
			Me.colhAccountcode.HeaderText = Language._Object.getCaption(Me.colhAccountcode.Name, Me.colhAccountcode.HeaderText)
			Me.colhShortname.HeaderText = Language._Object.getCaption(Me.colhShortname.Name, Me.colhShortname.HeaderText)
			Me.colhShortname2.HeaderText = Language._Object.getCaption(Me.colhShortname2.Name, Me.colhShortname2.HeaderText)
            Me.colhShortname3.HeaderText = Language._Object.getCaption(Me.colhShortname3.Name, Me.colhShortname3.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot import this file. Reason : No transactions are not there in this file to import.")
			Language.setMessage(mstrModuleName, 2, "Sorry, you cannot import this file. Reason : Some head codes, account codes or Employee Codes are not there in system.")
			Language.setMessage(mstrModuleName, 3, "Please tick atleast one transaction from list to Import.")
			Language.setMessage(mstrModuleName, 4, "Employee Account Configuration imported successfully!")
			Language.setMessage(mstrModuleName, 5, "Some transactions will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class