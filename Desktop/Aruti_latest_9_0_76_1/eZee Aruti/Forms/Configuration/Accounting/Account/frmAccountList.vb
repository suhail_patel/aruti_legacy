﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAccountList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAccountList"

    Private objAccount As clsAccount_master
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboAccountName.BackColor = GUI.ColorOptional
            cboAccountGroup.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objAccount.getComboList("Accounts")
            With cboAccountName
                .ValueMember = "accountunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Accounts")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListAccountGroup("AccountGroup")
            With cboAccountGroup
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("AccountGroup")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Dim intAcGrpSelValue As Integer = 0
        Dim strFilter As String = ""
        Try

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If User._Object.Privilege._AllowToViewAccountList = False Then Exit Sub
            'Anjan (25 Oct 2012)-End 



            lvAccount.Items.Clear()

            Dim lvItem As ListViewItem
            intAcGrpSelValue = CInt(cboAccountGroup.SelectedValue)

            dsList = objAccount.GetList("AccountGroups", True, CInt(cboAccountName.SelectedValue), intAcGrpSelValue)

            For Each dsRow As DataRow In dsList.Tables("AccountGroups").Rows
                lvItem = New ListViewItem

                lvItem.Text = dsRow.Item("account_code").ToString
                lvItem.Tag = dsRow.Item("accountunkid").ToString

                lvItem.SubItems.Add(dsRow.Item("account_name").ToString)

                cboAccountGroup.SelectedValue = CInt(dsRow.Item("accountgroup_id").ToString)
                lvItem.SubItems.Add(cboAccountGroup.Text)

                lvAccount.Items.Add(lvItem)
            Next
            cboAccountGroup.SelectedValue = intAcGrpSelValue

            If lvAccount.Items.Count > 15 Then
                colhAccGroup.Width = 280 - 18
            Else
                colhAccGroup.Width = 280
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub


    'Anjan (25 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AllowToAddAccount
            btnEdit.Enabled = User._Object.Privilege._AllowToEditAccount
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteAccount
            mnuExportAccount.Enabled = User._Object.Privilege._AllowtoExportAccounts
            mnuImportAccount.Enabled = User._Object.Privilege._AllowtoImportAccounts

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Anjan (25 Oct 2012)-End 


#End Region

#Region " Form's Events "

    Private Sub frmAccountList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAccount = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAccountList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    btnClose.PerformClick()
                Case Keys.Delete
                    If lvAccount.Focus = True Then btnDelete.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAccountList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAccount = New clsAccount_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            Call SetVisibility()
            'Anjan (25 Oct 2012)-End 

            Call SetColor()
            Call FillCombo()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountList_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAccount_master.SetMessages()
            objfrm._Other_ModuleNames = "clsAccount_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrm As New frmAccount_AddEdit
            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvAccount.DoubleClick
        Try
            If lvAccount.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Account from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvAccount.Select()
                Exit Sub
            End If
            Dim objFrm As New frmAccount_AddEdit
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvAccount.SelectedItems(0).Index
                If objFrm.displayDialog(CInt(lvAccount.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call FillList()
                End If
                objFrm = Nothing

                lvAccount.Items(intSelectedIndex).Selected = True
                lvAccount.EnsureVisible(intSelectedIndex)
                lvAccount.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objFrm IsNot Nothing Then objFrm.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAccount.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Account from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAccount.Select()
            Exit Sub
        End If
        'Sohail (28 Dec 2010) -- Start
        If objAccount.isUsed(CInt(lvAccount.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Account. Reason: This Account is in use."), enMsgBoxStyle.Information) '?2
            lvAccount.Select()
            Exit Sub
        End If
        'Sohail (28 Dec 2010) -- End
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAccount.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Account?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objAccount.Delete(CInt(lvAccount.SelectedItems(0).Tag))
                lvAccount.SelectedItems(0).Remove()

                If lvAccount.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAccount.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAccount.Items.Count - 1
                    lvAccount.Items(intSelectedIndex).Selected = True
                    lvAccount.EnsureVisible(intSelectedIndex)
                ElseIf lvAccount.Items.Count <> 0 Then
                    lvAccount.Items(intSelectedIndex).Selected = True
                    lvAccount.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvAccount.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAccountName.SelectedValue = 0
            cboAccountGroup.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAccount.Click
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet
        Try
            'If User._Object._RightToLeft = True Then
            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objfrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objfrm)
            'End If
            dsList = objAccount.GetList("AccountList")
            With cboAccountName
                objfrm.DataSource = dsList.Tables("AccountList")
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = "account_name"
                objfrm.CodeMember = "account_code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAccount_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Sohail (04 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub mnuExportAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAccount.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'S.SANDEEP [12-Jan-2018] -- END
                strFilePath &= ObjFile.Extension

                dsList = objAccount.GetList("List")

                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                    Case 2   'XML
                        dsList.WriteXml(strFilePath)

                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "File exported successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : mnuExportAccount_Click ; Module Name : " & mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList.Dispose()
            path = String.Empty
            strFilePath = String.Empty
            dlgSaveFile = Nothing
            ObjFile = Nothing
        End Try
    End Sub

    Private Sub mnuImportAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportAccount.Click
        Try
            Dim frm As New frmImportAccount
            frm.ShowDialog()
            Call FillList()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : mnuImportAccount_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (04 Feb 2012) -- End
#End Region

#Region " Message "
    '1, "Please select Account from the list to perform further operation on it."
    '2, "Sorry, You cannot delete this Account. Reason: This Account is in use."
    '3, "Are you sure you want to delete this Account?"
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblAccuntName.Text = Language._Object.getCaption(Me.lblAccuntName.Name, Me.lblAccuntName.Text)
            Me.lblAccountGroup.Text = Language._Object.getCaption(Me.lblAccountGroup.Name, Me.lblAccountGroup.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhAccName.Text = Language._Object.getCaption(CStr(Me.colhAccName.Tag), Me.colhAccName.Text)
            Me.colhAccGroup.Text = Language._Object.getCaption(CStr(Me.colhAccGroup.Tag), Me.colhAccGroup.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuExportAccount.Text = Language._Object.getCaption(Me.mnuExportAccount.Name, Me.mnuExportAccount.Text)
            Me.mnuImportAccount.Text = Language._Object.getCaption(Me.mnuImportAccount.Name, Me.mnuImportAccount.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Account from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Account. Reason: This Account is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Account?")
            Language.setMessage(mstrModuleName, 4, "File exported successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class