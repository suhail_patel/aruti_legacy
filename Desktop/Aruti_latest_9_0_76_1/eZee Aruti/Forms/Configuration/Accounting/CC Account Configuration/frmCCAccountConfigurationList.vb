﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmCCAccountConfigurationList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCCAccountConfigurationList"
    Private objCCAccountConfig As clsaccountconfig_costcenter
    'Sohail (02 Nov 2019) -- Start
    'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
    Private mdtView As DataView
    'Sohail (02 Nov 2019) -- End

    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Private mdtPeriod_startdate As Date
    Private mdtPeriod_enddate As Date
    'Sohail (03 Jul 2020) -- End

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboTransactionType.BackColor = GUI.ColorOptional
            cboTrnHead.BackColor = GUI.ColorOptional
            cboCostCenter.BackColor = GUI.ColorOptional
            cboAccountName.BackColor = GUI.ColorOptional
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            cboAllocation.BackColor = GUI.ColorOptional
            cboPeriod.BackColor = GUI.ColorComp
            'Sohail (03 Jul 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objAccount As New clsAccount_master
        'Dim objTransactionHead As New clsTransactionHead 'Sohail (02 Aug 2017)
        Dim objCostCenter As New clscostcenter_master
        'Sohail (03 Jul 2020) -- Start
        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
        Dim objPeriod As New clscommom_period_Tran
        Dim mintFirstPeriodID As Integer = 0
        'Sohail (03 Jul 2020) -- End

        Dim dsCombo As DataSet
        Try
            dsCombo = objMaster.getComboListJVTransactionType("TranType")
            With cboTransactionType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("TranType")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            'Sohail (21 Mar 2014) -- Start
            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            'dsCombo = objTransactionHead.getComboList("TranHead", True)
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsCombo = objTransactionHead.getComboList("TranHead", True, , , , True, , , , True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTransactionHead.getComboList("TranHead", True, , , , True, , , , True, True)
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'dsCombo = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , True, , , True, True)
            'dsCombo = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , True, , , True, True, True) 'Sohail (02 Aug 2017)
            'Sohail (18 Apr 2016) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (17 Sep 2014) -- End
            'Sohail (21 Mar 2014) -- End
            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            'With cboTrnHead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("TranHead")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With
            'Sohail (02 Aug 2017) -- End

            dsCombo = objAccount.getComboList("Accounts")
            With cboAccountName
                .ValueMember = "accountunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Accounts")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            dsCombo = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombo.Tables("CostCenter")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            'dsCombo = objMaster.GetEAllocation_Notification("List", "", False, True)
            'With cboAllocation
            '    .ValueMember = "id"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables(0)
            '    .SelectedValue = 0
            'End With

            mintFirstPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, False, True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = mintFirstPeriodID
            End With
            'Sohail (03 Jul 2020) -- End


            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            dsCombo = objMaster.getComboListTranHeadActiveInActive("ActiveInactive", False)
            With cboActiveInactive
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombo.Tables("ActiveInactive")
                .SelectedIndex = 0
            End With
            'Gajanan [24-Aug-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objAccount = Nothing
            'objTransactionHead = Nothing 'Sohail (02 Aug 2017)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objPeriod = Nothing
            'Sohail (03 Jul 2020) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Dim mstrFilter As String = ""
        Try
            'Sohail (02 Nov 2019) -- Start
            'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
            'lvTrnHeadAccount.Items.Clear() 'Sohail (02 Aug 2017)
            'Sohail (02 Nov 2019) -- End
            If User._Object.Privilege._AllowToViewCostcenterAccountConfigurationList = True Then   'Pinkal (09-Jul-2012) -- Start

                'lvTrnHeadAccount.Items.Clear() 'Sohail (02 Aug 2017)
                objchkSelectAll.Checked = False 'Sohail (30 Aug 2013)

                'Sohail (02 Nov 2019) -- Start
                'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
                'Dim lvItem As ListViewItem
                'Sohail (02 Nov 2019) -- End

                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dsList = objCCAccountConfig.GetList("AccountConfig", True)

                'If CInt(cboTransactionType.SelectedValue) > 0 Then
                '    mstrFilter &= "AND transactiontype_Id = " & CInt(cboTransactionType.SelectedValue) & " "
                'End If

                'If CInt(cboTrnHead.SelectedValue) > 0 Then
                '    mstrFilter &= "AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " "
                'End If

                'If CInt(cboCostCenter.SelectedValue) > 0 Then
                '    mstrFilter &= "AND costcenterunkid = " & CInt(cboCostCenter.SelectedValue) & " "
                'End If

                'If CInt(cboAccountName.SelectedValue) > 0 Then
                '    mstrFilter &= "AND accountunkid = " & CInt(cboAccountName.SelectedValue) & " "
                'End If

                'If mstrFilter.Length > 0 Then
                '    mstrFilter = mstrFilter.Substring(3)
                '    dtList = New DataView(dsList.Tables("AccountConfig"), mstrFilter, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtList = dsList.Tables("AccountConfig")
                'End If
                If CInt(cboTransactionType.SelectedValue) > 0 Then
                    mstrFilter &= "AND praccount_configuration_costcenter.transactiontype_Id = " & CInt(cboTransactionType.SelectedValue) & " "
                End If

                If CInt(cboTrnHead.SelectedValue) > 0 Then
                    mstrFilter &= "AND praccount_configuration_costcenter.tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " "
                End If

                If CInt(cboCostCenter.SelectedValue) > 0 Then
                    mstrFilter &= "AND praccount_configuration_costcenter.costcenterunkid = " & CInt(cboCostCenter.SelectedValue) & " "
                End If

                If CInt(cboAccountName.SelectedValue) > 0 Then
                    mstrFilter &= "AND praccount_configuration_costcenter.accountunkid = " & CInt(cboAccountName.SelectedValue) & " "
                End If

                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                If CInt(cboAllocation.SelectedValue) > 0 Then
                    mstrFilter &= "AND praccount_configuration_costcenter.allocationbyid = " & CInt(cboAllocation.SelectedValue) & " "
                End If
                'Sohail (03 Jul 2020) -- End

                If mstrFilter.Length > 0 Then
                    mstrFilter = mstrFilter.Substring(3)
                End If

                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                'dsList = objCCAccountConfig.GetList("AccountConfig", True, mstrFilter, True)


                'Gajanan [24-Aug-2020] -- Start
                'NMB Enhancement : Allow to set account configuration mapping 
                'inactive for closed period to allow to map head on other account 
                'configuration screen from new period
                'dsList = objCCAccountConfig.GetList("AccountConfig", True, mstrFilter, True, mdtPeriod_enddate)
                dsList = objCCAccountConfig.GetList("AccountConfig", True, mstrFilter, True, mdtPeriod_enddate, "", CInt(cboActiveInactive.SelectedValue))
                'Gajanan [24-Aug-2020] -- End

                'Sohail (03 Jul 2020) -- End
                'Sohail (02 Nov 2019) - [blnAddGrouping]
                'Sohail (06 Aug 2016) -- End

                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'For Each dsRow As DataRow In dtList.Rows
                'Sohail (02 Nov 2019) -- Start
                'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
                'For Each dsRow As DataRow In dsList.Tables("AccountConfig").Rows
                '    'Sohail (06 Aug 2016) -- End
                '    lvItem = New ListViewItem
                '    lvItem.Tag = CInt(dsRow.Item("accountconfigccunkid").ToString)
                '    lvItem.Text = ""
                '    lvItem.SubItems.Add(dsRow.Item("costcentername").ToString)
                '    'cboTransactionType.SelectedValue = CInt(dsRow.Item("transactiontype_id").ToString)

                '    'Sohail (06 Aug 2016) -- Start
                '    'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                '    'Select Case CInt(dsRow.Item("transactiontype_id").ToString)
                '    '    Case enJVTransactionType.TRANSACTION_HEAD
                '    '        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 141, "Transaction Head"))
                '    '    Case enJVTransactionType.LOAN
                '    '        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 143, "Loan"))
                '    '    Case enJVTransactionType.ADVANCE
                '    '        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 144, "Advance"))
                '    '    Case enJVTransactionType.SAVINGS
                '    '        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 145, "Savings"))
                '    '    Case enJVTransactionType.CASH
                '    '        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 146, "Cash"))
                '    '    Case enJVTransactionType.BANK
                '    '        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 147, "Bank"))
                '    '    Case enJVTransactionType.COST_CENTER
                '    '        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 148, "Cost Center"))
                '    '        'Sohail (21 Jun 2013) -- Start
                '    '        'TRA - ENHANCEMENT
                '    '    Case enJVTransactionType.PAY_PER_ACTIVITY
                '    '        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 558, "Pay Per Activity"))
                '    '        'Sohail (21 Jun 2013) -- End
                '    'End Select
                '    lvItem.SubItems.Add(dsRow.Item("transactiontype_name").ToString)
                '    'Sohail (06 Aug 2016) -- End

                '    lvItem.SubItems.Add(dsRow.Item("trnheadname").ToString)
                '    lvItem.SubItems.Add(dsRow.Item("account_code").ToString) 'Sohail (08 Nov 2011)
                '    lvItem.SubItems.Add(dsRow.Item("account_name").ToString)
                '    lvItem.SubItems.Add(dsRow.Item("shortname").ToString) 'Sohail (13 Mar 2013)
                '    lvItem.SubItems.Add(dsRow.Item("shortname2").ToString) 'Sohail (08 Jul 2017)

                '    RemoveHandler lvTrnHeadAccount.ItemChecked, AddressOf lvTrnHeadAccount_ItemChecked 'Sohail (30 Aug 2013)
                '    lvTrnHeadAccount.Items.Add(lvItem)
                '    AddHandler lvTrnHeadAccount.ItemChecked, AddressOf lvTrnHeadAccount_ItemChecked 'Sohail (30 Aug 2013)
                'Next

                'lvTrnHeadAccount.GridLines = False
                'lvTrnHeadAccount.GroupingColumn = colhCostCenter
                'lvTrnHeadAccount.DisplayGroups(True)

                'If lvTrnHeadAccount.Items.Count > 5 Then 'Sohail (08 Nov 2011)
                '    colhTrnHead.Width = 210 - 18 'Sohail (08 Nov 2011)
                'Else
                '    colhTrnHead.Width = 210 'Sohail (08 Nov 2011)
                'End If
                mdtView = dsList.Tables(0).DefaultView

                objdgcolhCheck.DataPropertyName = "IsChecked"
                objdgcolhIsGroup.DataPropertyName = "IsGrp"
                objdgcolhID.DataPropertyName = "accountconfigccunkid"
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                dgColhCCName.DataPropertyName = "costcentername"
                'dgColhCCName.DataPropertyName = "costcentercolumn"
                'Sohail (03 Jul 2020) -- End
                objdgcolhCCid.DataPropertyName = "costcenterunkid"
                objdgcolhCCCode.DataPropertyName = "costcentercode"
                dgcolhTrnHead.DataPropertyName = "trnheadname"
                dgcolhTranType.DataPropertyName = "transactiontype_name"
                dgcolhAccountCode.DataPropertyName = "account_code"
                dgcolhAccountName.DataPropertyName = "account_name"
                dgcolhShortname.DataPropertyName = "shortname"
                dgcolhShortname2.DataPropertyName = "shortname2"
                dgcolhShortname3.DataPropertyName = "shortname3" 'Sohail (03 Mar 2020)
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                dgcolhPeriod.DataPropertyName = "period_name"
                objdgcolhPeriodStatusID.DataPropertyName = "statusid"
                dgcolhAllocationByName.DataPropertyName = "costcentercolumn"
                objdgcolhAllocationByID.DataPropertyName = "allocationbyid"

                'Gajanan [24-Aug-2020] -- Start
                'NMB Enhancement : Allow to set account configuration mapping 
                'inactive for closed period to allow to map head on other account 
                'configuration screen from new period
                colhactiveinactive.DataPropertyName = "isinactive"
                'Gajanan [24-Aug-2020] -- End

                'Sohail (03 Jul 2020) -- End
                dgColhCCName.Visible = False
                objdgcolhCCCode.Visible = False

                With dgvAccount
                    .AutoGenerateColumns = False

                    .DataSource = mdtView

                End With

                For Each dgvc As DataGridViewColumn In dgvAccount.Columns
                    dgvc.SortMode = DataGridViewColumnSortMode.NotSortable
                Next
                'Sohail (02 Nov 2019) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
        Finally
            'Sohail (02 Nov 2019) -- Start
            'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
            'objbtnSearch.ShowResult(lvTrnHeadAccount.Items.Count.ToString())
            If mdtView IsNot Nothing Then
                Call objbtnSearch.ShowResult(mdtView.ToTable.Select("IsGrp = 0").Length.ToString)
            Else
                Call objbtnSearch.ShowResult("0")
            End If
            'Sohail (02 Nov 2019) -- End
            'Sohail (02 Aug 2017) -- End
        End Try
    End Sub

    'Anjan (25 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AllowToAddCCConfig
            btnEdit.Enabled = User._Object.Privilege._AllowToEditCCConfig
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteCCConfig
            btnSetInactive.Enabled = User._Object.Privilege._AllowToInactiveCostCenterAccountConfiguration 'Sohail (25 Jul 2020)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Anjan (25 Oct 2012)-End 

    'Sohail (30 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (02 Nov 2019) -- Start
            'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
            'For Each lvItem As ListViewItem In lvTrnHeadAccount.Items
            '    RemoveHandler lvTrnHeadAccount.ItemChecked, AddressOf lvTrnHeadAccount_ItemChecked
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvTrnHeadAccount.ItemChecked, AddressOf lvTrnHeadAccount_ItemChecked
            'Next
            If mdtView Is Nothing Then Exit Sub
            RemoveHandler dgvAccount.CellContentClick, AddressOf dgvAccount_CellContentClick
            For Each dr As DataRowView In mdtView
                dr.Item(objdgcolhCheck.DataPropertyName) = blnCheckAll
            Next
            dgvAccount.Refresh()
            AddHandler dgvAccount.CellContentClick, AddressOf dgvAccount_CellContentClick
            'Sohail (02 Nov 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub
    'Sohail (30 Aug 2013) -- End
#End Region

#Region " Form's Events "

    Private Sub frmCCAccountConfigurationList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            objCCAccountConfig = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCCAccountConfigurationList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCCAccountConfigurationList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    btnClose.PerformClick()
                Case Keys.Delete
                    'Sohail (02 Nov 2019) -- Start
                    'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
                    'If lvTrnHeadAccount.Focus = True Then btnDelete.PerformClick()
                    If dgvAccount.Focus = True Then btnDelete.PerformClick()
                    'Sohail (02 Nov 2019) -- End
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCCAccountConfigurationList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCCAccountConfigurationList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCCAccountConfig = New clsaccountconfig_costcenter
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 



            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            Call SetVisibility()
            'Anjan (25 Oct 2012)-End 


            Call SetColor()
            Call FillCombo()
            Call FillList()
            'Sohail (02 Nov 2019) -- Start
            'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
            'lvTrnHeadAccount.GridLines = False
            'Sohail (02 Nov 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCCAccountConfigurationList_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsaccountconfig_costcenter.SetMessages()
            objfrm._Other_ModuleNames = "clsaccountconfig_costcenter"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

    'Sohail (02 Aug 2017) -- Start
    'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
#Region " Combobox Events "

    Private Sub cboTransactionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTransactionType.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try

            cboTrnHead.DataSource = Nothing
            Select Case CInt(cboTransactionType.SelectedValue)
                Case enJVTransactionType.TRANSACTION_HEAD, 0
                    Dim objTransactionHead As New clsTransactionHead
                    dsCombo = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, , , , True, , , True, True, True)
                    cboTrnHead.ValueMember = "tranheadunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.LOAN
                    Dim objLoan As New clsLoan_Scheme
                    'Sohail (02 Apr 2018) -- Start
                    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
                    'dsCombo = objLoan.getComboList(True, "List")
                    dsCombo = objLoan.getComboList(True, "List", -1, "", False, True)
                    'Sohail (02 Apr 2018) -- End
                    cboTrnHead.ValueMember = "loanschemeunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.SAVINGS
                    Dim objSaving As New clsSavingScheme
                    dsCombo = objSaving.getComboList(True, "List")
                    cboTrnHead.ValueMember = "savingschemeunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.PAY_PER_ACTIVITY
                    Dim objActivity As New clsActivity_Master
                    dsCombo = objActivity.getComboList("List", True)
                    cboTrnHead.ValueMember = "activityunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.CR_EXPENSE
                    Dim objExpense As New clsExpense_Master
                    dsCombo = objExpense.getComboList(0, True, "List")
                    cboTrnHead.ValueMember = "Id"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.BANK
                    Dim objBankBranch As New clsbankbranch_master
                    dsCombo = objBankBranch.getListForCombo("List", True)
                    cboTrnHead.ValueMember = "branchunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                    'Sohail (03 Jan 2019) -- Start
                    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                Case enJVTransactionType.COMPANY_BANK
                    Dim objBankBranch As New clsCompany_Bank_tran
                    'Sohail (21 May 2020) -- Start
                    'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
                    'dsCombo = objBankBranch.GetComboList(Company._Object._Companyunkid, 2, "List", "")
                    dsCombo = objBankBranch.GetComboList(Company._Object._Companyunkid, 4, "List", "")
                    'Sohail (21 May 2020) -- End
                    cboTrnHead.ValueMember = "Id"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")
                    'Sohail (03 Jan 2019) -- End

                Case Else


            End Select

            If cboTrnHead.Items.Count > 0 Then cboTrnHead.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTransactionType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
            Else
                mdtPeriod_startdate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriod_enddate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Jul 2020) -- End


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private Sub cboActiveInactive_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboActiveInactive.SelectedIndexChanged
        Try
            If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
                btnSetInactive.Visible = True
            Else
                btnSetInactive.Visible = False
            End If

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboActiveInactive_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Gajanan [24-Aug-2020] -- End

#End Region
    'Sohail (02 Aug 2017) -- End

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrm As New frmCCAccountConfiguration
            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            'Sohail (02 Nov 2019) -- Start
            'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
            'If lvTrnHeadAccount.SelectedItems.Count < 1 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            '    lvTrnHeadAccount.Select()
            '    Exit Sub
            'End If
            If dgvAccount.SelectedRows.Count <= 0 OrElse CBool(dgvAccount.SelectedRows(0).Cells(objdgcolhIsGroup.Index).Value) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvAccount.Select()
                Exit Sub
            End If
            'Sohail (02 Nov 2019) -- End
            Dim objFrm As New frmCCAccountConfiguration
            Try
                Dim intSelectedIndex As Integer
                'Sohail (02 Nov 2019) -- Start
                'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
                'intSelectedIndex = lvTrnHeadAccount.SelectedItems(0).Index
                'If objFrm.displayDialog(CInt(lvTrnHeadAccount.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                intSelectedIndex = dgvAccount.SelectedRows(0).Index
                If objFrm.displayDialog(CInt(dgvAccount.SelectedRows(0).Cells(objdgcolhID.Index).Value), enAction.EDIT_ONE) Then
                    'Sohail (02 Nov 2019) -- End
                    Call FillList()
                End If
                objFrm = Nothing

                'Sohail (02 Nov 2019) -- Start
                'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
                'lvTrnHeadAccount.Items(intSelectedIndex).Selected = True
                'lvTrnHeadAccount.EnsureVisible(intSelectedIndex)
                'lvTrnHeadAccount.Select()
                dgvAccount.FirstDisplayedScrollingRowIndex = intSelectedIndex
                'Sohail (02 Nov 2019) -- End
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objFrm IsNot Nothing Then objFrm.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Sohail (30 Aug 2013) -- Start
        'TRA - ENHANCEMENT
        'If lvTrnHeadAccount.SelectedItems.Count < 1 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
        '    lvTrnHeadAccount.Select()
        '    Exit Sub
        'End If
        'Sohail (02 Nov 2019) -- Start
        'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
        'If lvTrnHeadAccount.CheckedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please tick atleast one Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
        '    lvTrnHeadAccount.Select()
        '    Exit Sub
        'End If
        If mdtView Is Nothing Then Exit Sub
        If mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please tick atleast one Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            dgvAccount.Select()
            Exit Sub
        End If
        'Sohail (02 Nov 2019) -- End

        'Sohail (03 Jul 2020) -- Start
        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
        Dim objPeriod As New clscommom_period_Tran
        Dim arrDistPeriod() As String = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p.Item("periodunkid").ToString)).Distinct().ToArray
        For Each strPeriodID As String In arrDistPeriod
            objPeriod = New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(strPeriodID)
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Some of the periods of selected transactions are already closed."), enMsgBoxStyle.Information)
                dgvAccount.Select()
                Exit Sub
            End If
        Next
        'Sohail (03 Jul 2020) -- End

        Dim strIDs As String = ""
        'Sohail (02 Nov 2019) -- Start
        'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
        'Dim allEmp As List(Of String) = (From lv In lvTrnHeadAccount.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
        Dim allEmp As List(Of String) = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p.Item("accountconfigccunkid").ToString)).Distinct().ToList
        'Sohail (02 Nov 2019) -- End
        strIDs = String.Join(",", allEmp.ToArray)
        'Sohail (30 Aug 2013) -- End

        Try
            'Sohail (30 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Dim intSelectedIndex As Integer
            'intSelectedIndex = lvTrnHeadAccount.SelectedItems(0).Index

            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Account Configuration?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '    objCCAccountConfig.Delete(CInt(lvTrnHeadAccount.SelectedItems(0).Tag))
            '    lvTrnHeadAccount.SelectedItems(0).Remove()

            '    If lvTrnHeadAccount.Items.Count <= 0 Then
            '        Exit Try
            '    End If

            '    If lvTrnHeadAccount.Items.Count = intSelectedIndex Then
            '        intSelectedIndex = lvTrnHeadAccount.Items.Count - 1
            '        lvTrnHeadAccount.Items(intSelectedIndex).Selected = True
            '        lvTrnHeadAccount.EnsureVisible(intSelectedIndex)
            '    ElseIf lvTrnHeadAccount.Items.Count <> 0 Then
            '        lvTrnHeadAccount.Items(intSelectedIndex).Selected = True
            '        lvTrnHeadAccount.EnsureVisible(intSelectedIndex)
            '    End If
            'End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete ticked Account Configurations?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Cursor.Current = Cursors.WaitCursor
                If objCCAccountConfig.VoidAll(strIDs) = True Then
                    Call FillList()
                End If
            End If
            'Sohail (30 Aug 2013) -- End
            'Sohail (02 Nov 2019) -- Start
            'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
            'lvTrnHeadAccount.Select()
            dgvAccount.Select()
            'Sohail (02 Nov 2019) -- End
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
            'Sohail (30 Aug 2013) -- Start
            'TRA - ENHANCEMENT
        Finally
            Cursor.Current = Cursors.Default
            'Sohail (30 Aug 2013) -- End
        End Try
    End Sub


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private Sub btnSetInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetInactive.Click
        If mdtView Is Nothing Then Exit Sub
        If mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please tick atleast one Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            dgvAccount.Select()
            Exit Sub
        End If

        Dim objPeriod As New clscommom_period_Tran
        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
        If objPeriod._Statusid = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, selected period is already closed."), enMsgBoxStyle.Information)
            cboPeriod.Focus()
            Exit Sub
        End If
        objPeriod = Nothing

        Dim arrDistPeriod() As String = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p.Item("periodunkid").ToString)).Distinct().ToArray
        For Each strPeriodID As String In arrDistPeriod
            objPeriod = New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(strPeriodID)
            If objPeriod._Statusid = enStatusType.Open Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Some of the periods of selected transactions are already opened."), enMsgBoxStyle.Information)
                dgvAccount.Select()
                Exit Sub
            End If
            objPeriod = Nothing
        Next

        Dim arrRow As List(Of DataRow) = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p)).ToList
        For Each dr As DataRow In arrRow
            If objCCAccountConfig.isExist(CInt(dr.Item("transactiontype_Id")), CInt(dr.Item("tranheadunkid")), CInt(dr.Item("costcenterunkid")), CInt(cboPeriod.SelectedValue), CInt(dr.Item("allocationbyid")), CInt(dr.Item("allocationunkid"))) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, #headname# is already mapped in selected period #periodname#.").Replace("#headname#", dr.Item("transactiontype_name").ToString & " : " & dr.Item("trnheadname").ToString).Replace("#periodname#", cboPeriod.Text), enMsgBoxStyle.Information)
                dgvAccount.Select()
                Exit Sub
            End If
        Next

        Dim strIDs As String = ""
        Dim allEmp As List(Of String) = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p.Item("accountconfigccunkid").ToString)).Distinct().ToList
        strIDs = String.Join(",", allEmp.ToArray)

        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to inactive ticked Account Configurations from #periodname#?").Replace("#periodname#", cboPeriod.Text), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Cursor.Current = Cursors.WaitCursor
                If objCCAccountConfig.InactiveAll(strIDs, CInt(cboPeriod.SelectedValue)) = True Then
                    Call FillList()
                End If
            End If
            dgvAccount.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
            'Sohail (30 Aug 2013) -- Start
            'TRA - ENHANCEMENT
        Finally
            Cursor.Current = Cursors.Default
            'Sohail (30 Aug 2013) -- End
        End Try
    End Sub
    'Gajanan [24-Aug-2020] -- End


#End Region

    'Sohail (30 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    'Sohail (02 Nov 2019) -- Start
    'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
    '#Region " Listview's Events "
    '    Private Sub lvTrnHeadAccount_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)

    '        Try
    '            If lvTrnHeadAccount.CheckedItems.Count <= 0 Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Unchecked
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            ElseIf lvTrnHeadAccount.CheckedItems.Count < lvTrnHeadAccount.Items.Count Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Indeterminate
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            ElseIf lvTrnHeadAccount.CheckedItems.Count = lvTrnHeadAccount.Items.Count Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Checked
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lvTrnHeadAccount_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub
    '#End Region
    'Sohail (02 Nov 2019) -- End

    'Sohail (02 Nov 2019) -- Start
    'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
#Region " Datagridview Events "

    Private Sub dgvAccount_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAccount.CellContentClick
        Try
            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If e.ColumnIndex = objdgcolhCheck.Index Then
                If Me.dgvAccount.IsCurrentCellDirty Then
                    Me.dgvAccount.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = Nothing
                If CBool(dgvAccount.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    'Sohail (03 Jul 2020) -- Start
                    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                    'drRow = mdtView.Table.Select(objdgcolhCCid.DataPropertyName & " = '" & dgvAccount.Rows(e.RowIndex).Cells(objdgcolhCCid.Index).Value.ToString() & "'", "")
                    drRow = mdtView.Table.Select(objdgcolhCCid.DataPropertyName & " = '" & dgvAccount.Rows(e.RowIndex).Cells(objdgcolhCCid.Index).Value.ToString() & "'", "")
                    'Sohail (03 Jul 2020) -- End
                    If drRow.Length > 0 Then
                        For index As Integer = 0 To drRow.Length - 1
                            drRow(index)("isChecked") = CBool(dgvAccount.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                        Next
                    End If
                End If

                drRow = mdtView.ToTable.Select("isChecked = true", "")

                If drRow.Length > 0 Then
                    If mdtView.ToTable.Rows.Count = drRow.Length Then
                        objchkSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkSelectAll.CheckState = CheckState.Unchecked
                End If

            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAccount_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvAccount_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvAccount.DataError

    End Sub

    Private Sub dgvAccount_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvAccount.CellPainting
        Try
            If e.RowIndex >= 0 AndAlso e.RowIndex < dgvAccount.RowCount - 1 AndAlso CBool(dgvAccount.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True AndAlso e.ColumnIndex > 0 Then
                If (e.ColumnIndex = objdgColhBlank.Index) Then
                    Dim backColorBrush As Brush = New SolidBrush(Color.Gray)
                    Dim totWidth As Integer = 0
                    For i As Integer = 1 To dgvAccount.Columns.Count - 1
                        totWidth += dgvAccount.Columns(i).Width
                    Next
                    Dim r As New RectangleF(e.CellBounds.Left, e.CellBounds.Top, totWidth, e.CellBounds.Height)
                    e.Graphics.FillRectangle(backColorBrush, r)

                    'Sohail (03 Jul 2020) -- Start
                    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                    'e.Graphics.DrawString(CType(dgvAccount.Rows(e.RowIndex).Cells(dgColhCCName.Index).Value.ToString & " : [" & dgvAccount.Rows(e.RowIndex).Cells(objdgcolhCCCode.Index).Value.ToString & "]", String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)
                    e.Graphics.DrawString(CType(dgvAccount.Rows(e.RowIndex).Cells(dgColhCCName.Index).Value.ToString, String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)
                    'Sohail (03 Jul 2020) -- End

                End If

                e.Handled = True

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAccount_CellPainting", mstrModuleName)
        End Try
    End Sub

    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Private Sub dgvAccount_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvAccount.Scroll
        Try
            If e.ScrollOrientation = ScrollOrientation.HorizontalScroll Then
                For i = dgvAccount.FirstDisplayedScrollingRowIndex To dgvAccount.Rows.Count - 1
                    If dgvAccount.Rows(i).Displayed = False Then Exit For
                    If CBool(dgvAccount.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                        dgvAccount.ClearSelection()
                        dgvAccount.Rows(i).Selected = True
                        dgvAccount.ClearSelection()
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAccount_Scroll", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Jul 2020) -- End

#End Region
    'Sohail (02 Nov 2019) -- End

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAll(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (30 Aug 2013) -- End

#Region " Other Control's Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboTransactionType.SelectedValue = 0
            cboCostCenter.SelectedValue = 0
            cboTrnHead.SelectedValue = 0
            cboAccountName.SelectedValue = 0
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            cboAllocation.SelectedValue = 0
            'Sohail (03 Jul 2020) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        'Sohail (21 Mar 2014) -- Start
        'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
        'Dim objTranHead As New clsTransactionHead
        'Dim dsList As DataSet
        'Sohail (21 Mar 2014) -- End
        If cboTrnHead.DataSource Is Nothing Then Exit Sub 'Sohail (02 Aug 2017)
        Dim objfrm As New frmCommonSearch

        Try
            'dsList = objTranHead.getComboList("TranHead", False) 'Sohail (21 Mar 2014) 
            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            'With cboTrnHead
            '    objfrm.ValueMember = "tranheadunkid"
            '    objfrm.DisplayMember = "Name"
            '    objfrm.CodeMember = "code"
            '    'Sohail (21 Mar 2014) -- Start
            '    'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            '    'objfrm.DataSource = dsList.Tables("TranHead")
            '    objfrm.DataSource = CType(cboTrnHead.DataSource, DataTable)
            '    'Sohail (21 Mar 2014) -- End
            '    If objfrm.DisplayDialog Then
            '        .SelectedValue = objfrm.SelectedValue
            '    End If
            '    .Focus()
            'End With
            With cboTrnHead
                objfrm.ValueMember = cboTrnHead.ValueMember
                objfrm.DisplayMember = cboTrnHead.DisplayMember
                objfrm.CodeMember = "code"
                objfrm.DataSource = CType(cboTrnHead.DataSource, DataTable)
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
            'Sohail (02 Aug 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Sohail (08 Nov 2011) -- Start
    Private Sub objbtnSearchAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAccount.Click
        Dim objfrm As New frmCommonSearch
        Dim objAccount As New clsAccount_master
        Dim dsList As DataSet
        Try
            'If User._Object._RightToLeft = True Then
            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objfrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objfrm)
            'End If
            dsList = objAccount.GetList("AccountList")
            With cboAccountName
                objfrm.DataSource = dsList.Tables("AccountList")
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = "account_name"
                objfrm.CodeMember = "account_code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAccount_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objAccount = Nothing
        End Try
    End Sub
    'Sohail (08 Nov 2011) -- End


    'Anjan [ 15 Feb 2013 ] -- Start
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCostCenter.Click
        Dim objfrm As New frmCommonSearch
        Dim objCostCenter As New clscostcenter_master
        Dim dsList As DataSet
        Try
            dsList = objCostCenter.GetList("CostCenterList")
            With cboCostCenter
                objfrm.DataSource = dsList.Tables("CostCenterList")
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = "costcentername"
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                objfrm.CodeMember = "costcentercode"
                If CInt(cboAllocation.SelectedValue) = enAllocation.COST_CENTER Then
                    objfrm.CodeMember = "costcentercode"
                ElseIf (CInt(cboAllocation.SelectedValue) = enAllocation.CLASS_GROUP OrElse CInt(cboAllocation.SelectedValue) = enAllocation.CLASSES) Then
                    objfrm.CodeMember = "code"
                Else

                End If
                'Sohail (03 Jul 2020) -- End

                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCostCenter_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objCostCenter = Nothing
        End Try
    End Sub
    'Anjan [ 15 Feb 2013 ] -- End


    'Anjan [20 Feb 2014] -- Start
    'ENHANCEMENT : Requested by Rutta
    Private Sub objbtnSearchTransactionType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTransactionType.Click
        Dim objMaster As New clsMasterData
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet
        Try
            dsList = objMaster.getComboListJVTransactionType("HeadType")
            With cboTransactionType
                objfrm.ValueMember = "Id"
                objfrm.DisplayMember = "Name"
                objfrm.CodeMember = "Name"
                objfrm.DataSource = dsList.Tables("HeadType")
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrantype_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan [20 Feb 2014 ] -- End


#End Region

#Region " Message "
    '1, "Please select Account Configuration from the list to perform further operation on it."
    '2, "Sorry, You cannot delete this Account Configuration. Reason: This Account Configuration is in use."
    '3, "Are you sure you want to delete this Account Configuration?"
#End Region



    
   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbTranAccountList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTranAccountList.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSetInactive.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSetInactive.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbTranAccountList.Text = Language._Object.getCaption(Me.gbTranAccountList.Name, Me.gbTranAccountList.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAccuntName.Text = Language._Object.getCaption(Me.lblAccuntName.Name, Me.lblAccuntName.Text)
			Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblTransactionType.Text = Language._Object.getCaption(Me.lblTransactionType.Name, Me.lblTransactionType.Text)
			Me.lblCostcenter.Text = Language._Object.getCaption(Me.lblCostcenter.Name, Me.lblCostcenter.Text)
			Me.EZeeGradientButton1.Text = Language._Object.getCaption(Me.EZeeGradientButton1.Name, Me.EZeeGradientButton1.Text)
			Me.lblAllocatoion.Text = Language._Object.getCaption(Me.lblAllocatoion.Name, Me.lblAllocatoion.Text)
			Me.EZeeGradientButton2.Text = Language._Object.getCaption(Me.EZeeGradientButton2.Name, Me.EZeeGradientButton2.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
			Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
			Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
			Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
			Me.btnSetInactive.Text = Language._Object.getCaption(Me.btnSetInactive.Name, Me.btnSetInactive.Text)
			Me.lblactiveinactive.Text = Language._Object.getCaption(Me.lblactiveinactive.Name, Me.lblactiveinactive.Text)
			Me.dgColhCCName.HeaderText = Language._Object.getCaption(Me.dgColhCCName.Name, Me.dgColhCCName.HeaderText)
			Me.dgcolhTranType.HeaderText = Language._Object.getCaption(Me.dgcolhTranType.Name, Me.dgcolhTranType.HeaderText)
			Me.dgcolhTrnHead.HeaderText = Language._Object.getCaption(Me.dgcolhTrnHead.Name, Me.dgcolhTrnHead.HeaderText)
			Me.dgcolhAllocationByName.HeaderText = Language._Object.getCaption(Me.dgcolhAllocationByName.Name, Me.dgcolhAllocationByName.HeaderText)
			Me.dgcolhAccountCode.HeaderText = Language._Object.getCaption(Me.dgcolhAccountCode.Name, Me.dgcolhAccountCode.HeaderText)
			Me.dgcolhAccountName.HeaderText = Language._Object.getCaption(Me.dgcolhAccountName.Name, Me.dgcolhAccountName.HeaderText)
			Me.dgcolhPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhPeriod.Name, Me.dgcolhPeriod.HeaderText)
			Me.dgcolhShortname.HeaderText = Language._Object.getCaption(Me.dgcolhShortname.Name, Me.dgcolhShortname.HeaderText)
			Me.dgcolhShortname2.HeaderText = Language._Object.getCaption(Me.dgcolhShortname2.Name, Me.dgcolhShortname2.HeaderText)
			Me.dgcolhShortname3.HeaderText = Language._Object.getCaption(Me.dgcolhShortname3.Name, Me.dgcolhShortname3.HeaderText)
			Me.colhactiveinactive.HeaderText = Language._Object.getCaption(Me.colhactiveinactive.Name, Me.colhactiveinactive.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Account Configuration from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete ticked Account Configurations?")
			Language.setMessage(mstrModuleName, 3, "Please tick atleast one Account Configuration from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Some of the periods of selected transactions are already closed.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Some of the periods of selected transactions are already opened.")
			Language.setMessage(mstrModuleName, 6, "Are you sure you want to inactive ticked Account Configurations from #periodname#?")
			Language.setMessage(mstrModuleName, 7, "Sorry, selected period is already closed.")
			Language.setMessage(mstrModuleName, 8, "Sorry, #headname# is already mapped in selected period #periodname#.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class