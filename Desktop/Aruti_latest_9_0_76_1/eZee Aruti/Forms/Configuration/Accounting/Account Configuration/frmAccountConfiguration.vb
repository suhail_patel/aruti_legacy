﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAccountConfiguration

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAccountConfiguration"
    Private mblnCancel As Boolean = True
    Private mintAccountConfigUnkid As Integer = -1
    Private menAction As enAction = enAction.ADD_ONE
    'Sohail (31 Jul 2017) -- Start
    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
    Private dvTranHead As DataView
    Private mintTotalHead As Integer = 0
    Private mintCount As Integer = 0
    Private mstrSearchHeadText As String = ""
    Private mstrSearchText As String = ""
    Private mintCheckedHeads As Integer = 0
    'Sohail (31 Jul 2017) -- End
    'Sohail (25 Jul 2020) -- Start
    'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    'Sohail (25 Jul 2020) -- End

    Private objAccountConfig As clsAccountConfiguration
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intUnkId As Integer, _
                              ByVal pAction As enAction) As Boolean

        Try

            mintAccountConfigUnkid = intUnkId
            menAction = pAction

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboTransactionType.BackColor = GUI.ColorComp
            'Nilay (20 Mar 2017) -- Start
            cboHeadType.BackColor = GUI.ColorOptional
            'Nilay (20 Mar 2017) -- End
            'cboTrnHead.BackColor = GUI.ColorComp 'Sohail (12 Oct 2011)
            cboAccount.BackColor = GUI.ColorComp
            cboAccountGroup.BackColor = GUI.ColorComp
            'Sohail (14 Nov 2011) -- Start
            cboMapRefCode.BackColor = GUI.ColorComp
            cboMapRefName.BackColor = GUI.ColorComp
            'Sohail (14 Nov 2011) -- End
            txtShortName.BackColor = GUI.ColorOptional 'Sohail (13 Mar 2013)
            txtShortName2.BackColor = GUI.ColorOptional 'Sohail (08 Jul 2017)
            txtShortName3.BackColor = GUI.ColorOptional 'Sohail (03 Mar 2020)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            cboPeriod.BackColor = GUI.ColorComp
            'Sohail (03 Jul 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            With objAccountConfig
                cboTransactionType.SelectedValue = ._Transactiontype_Id
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                cboPeriod.SelectedValue = ._Periodunkid
                'Sohail (03 Jul 2020) -- End
                'Sohail (12 Oct 2011) -- Start
                'If ._Transactiontype_Id = enJVTransactionType.TRANSACTION_HEAD Then
                '    cboTrnHead.SelectedValue = ._Tranheadunkid
                'Else
                '    cboTrnHead.SelectedValue = 0 'For Loan, Advance, Savings
                'End If
                'Sohail (12 Oct 2011) -- End

                'Sohail (03 Jan 2019) -- Start
                'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                If menAction = enAction.EDIT_ONE AndAlso objAccountConfig._Transactiontype_Id = CInt(enJVTransactionType.TRANSACTION_HEAD) Then
                    Dim objHead As New clsTransactionHead
                    objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = objAccountConfig._Tranheadunkid
                    If objHead._Calctype_Id = CInt(enCalcType.NET_PAY) Then
                        Call FillRefCodeName(True)
                    End If
                    objHead = Nothing
                End If
                'Sohail (03 Jan 2019) -- End

                cboAccountGroup.SelectedValue = ._AccountGroupunkid
                cboAccount.SelectedValue = ._Accountunkid
                'Sohail (14 Nov 2011) -- Start
                cboMapRefCode.SelectedValue = ._Referencecodeid
                cboMapRefName.SelectedValue = ._Referencenameid
                'Sohail (14 Nov 2011) -- End
                txtShortName.Text = ._Shortname 'Sohail (13 Mar 2013)
                txtShortName2.Text = ._Shortname2 'Sohail (08 Jul 2017)
                txtShortName3.Text = ._Shortname3 'Sohail (03 Mar 2020)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            With objAccountConfig
                ._Transactiontype_Id = CInt(cboTransactionType.SelectedValue)
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                ._Periodunkid = CInt(cboPeriod.SelectedValue)
                'Sohail (03 Jul 2020) -- End
                'Sohail (12 Oct 2011) -- Start
                'If CInt(cboTransactionType.SelectedValue) = enJVTransactionType.TRANSACTION_HEAD Then
                '    ._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
                'Else
                '    ._Tranheadunkid = -1 'For Loan, Advance, Savings
                'End If
                'Sohail (12 Oct 2011) -- End

                ._Accountunkid = CInt(cboAccount.SelectedValue)
                'Sohail (14 Nov 2011) -- Start
                ._Referencecodeid = CInt(cboMapRefCode.SelectedValue)
                ._Referencenameid = CInt(cboMapRefName.SelectedValue)
                'Sohail (14 Nov 2011) -- End
                ._Shortname = txtShortName.Text.Trim 'Sohail (13 Mar 2013)
                ._Shortname2 = txtShortName2.Text.Trim 'Sohail (08 Jul 2017)
                ._Shortname3 = txtShortName3.Text.Trim 'Sohail (03 Mar 2020)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objTransactionHead As New clsTransactionHead
        'Sohail (03 Jul 2020) -- Start
        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
        Dim objPeriod As New clscommom_period_Tran
        Dim mintFirstPeriodID As Integer = 0
        'Sohail (03 Jul 2020) -- End
        Dim dsCombo As DataSet
        Try
            dsCombo = objMaster.getComboListJVTransactionType("HeadType")

            'SHANI (06 MAY 2015) -- Start
            ' 
            Dim dtTable As DataTable
            If menAction <> enAction.EDIT_ONE Then
                dtTable = New DataView(dsCombo.Tables("HeadType"), "Id <> " & enJVTransactionType.PAY_PER_ACTIVITY & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables("HeadType")).ToTable
            End If
            'SHANI (06 MAY 2015) -- End

            With cboTransactionType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            'SHANI (06 MAY 2015) -- .DataSource = dsCombo.Tables("HeadType")

            'Nilay (20 Mar 2017) -- Start
            dsCombo = objMaster.getComboListForHeadType("HeadType")
            With cboHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("HeadType")
                .SelectedValue = 0
            End With
            'Nilay (20 Mar 2017) -- End

            'Sohail (12 Oct 2011) -- Start
            ''Sohail (02 Aug 2011) -- Start
            ''dsCombo = objTransactionHead.getComboList("TranHead", True)
            'Dim dtTable As DataTable
            'Dim strHeadIDs As String = objTransactionHead.GetTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION)
            'dsCombo = objTransactionHead.getComboList("TranHead", True, , , , True)
            'dtTable = New DataView(dsCombo.Tables("TranHead"), "tranheadunkid NOT IN (" & strHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
            ''Sohail (02 Aug 2011) -- End
            'With cboTrnHead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dtTable ' dsCombo.Tables("TranHead") 'Sohail (02 Aug 2011)
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With
            'Sohail (12 Oct 2011) -- End


            dsCombo = objMaster.getComboListAccountGroup("AccountGroup")
            With cboAccountGroup
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("AccountGroup")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            'Sohail (03 Jan 2019) -- Start
            'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
            ''Sohail (14 Nov 2011) -- Start
            'dsCombo = objMaster.getComboListJVCompanyConfigRefCode(True, "RefCode")
            'With cboMapRefCode
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("RefCode")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With

            'dsCombo = objMaster.getComboListJVCompanyConfigRefName(True, "RefName")
            'With cboMapRefName
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("RefName")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With
            ''Sohail (14 Nov 2011) -- End
            Call FillRefCodeName(False)
            'Sohail (03 Jan 2019) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            mintFirstPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, False, True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = mintFirstPeriodID
            End With
            'Sohail (03 Jul 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objTransactionHead = Nothing
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objPeriod = Nothing
            'Sohail (03 Jul 2020) -- End
        End Try
    End Sub

    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    Private Sub FillRefCodeName(ByVal blnIncludeCompBankDetails As Boolean)
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objMaster.getComboListJVCompanyConfigRefCode(True, "RefCode", blnIncludeCompBankDetails)
            With cboMapRefCode
                .DataSource = Nothing
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("RefCode")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListJVCompanyConfigRefName(True, "RefName", blnIncludeCompBankDetails)
            With cboMapRefName
                .DataSource = Nothing
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("RefName")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillRefCodeName", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Jan 2019) -- End

    'Sohail (12 Oct 2011) -- Start
    Private Sub FillList(Optional ByVal TranHeadunkId As Integer = -1)
        Dim dsTranHead As DataSet
        Dim dtTranHead As DataTable = Nothing
        Try

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            objlblHeadCount.Text = "( 0 / 0 )"
            mintCount = 0
            'Sohail (31 Jul 2017) -- End

            Dim objTranHead As New clsTransactionHead
            Dim strHeadIDs As String = objTranHead.GetTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, mdtPeriodEnd)
            'Sohail (25 Jul 2020) - [mdtPeriodEnd]
            'Sohail (21 Mar 2014) -- Start
            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            'dsTranHead = objTranHead.GetList("Tranhead", , , , True)
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsTranHead = objTranHead.GetList("Tranhead", , , , True, , , , True)
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'dsTranHead = objTranHead.GetList("Tranhead", , , , True, , , , True, True)

            'Nilay (20 Mar 2017) -- Start
            'dsTranHead = objTranHead.GetList("Tranhead", , , , True, , , , True, True, True)
            dsTranHead = objTranHead.GetList("Tranhead", , CInt(cboHeadType.SelectedValue), , True, , , , True, True, True)
            'Nilay (20 Mar 2017) -- End

            'Sohail (18 Apr 2016) -- End
            'Sohail (17 Sep 2014) -- End
            'Sohail (21 Mar 2014) -- End

            If TranHeadunkId > 0 Then
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtTranHead = New DataView(dsTranHead.Tables("TranHead"), "tranheadunkid= " & TranHeadunkId, "", DataViewRowState.CurrentRows).ToTable
                dtTranHead = New DataView(dsTranHead.Tables("TranHead"), "tranheadunkid= " & TranHeadunkId, "trnheadname", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            Else
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtTranHead = New DataView(dsTranHead.Tables("TranHead"), "tranheadunkid NOT IN (" & strHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
                dtTranHead = New DataView(dsTranHead.Tables("TranHead"), "tranheadunkid NOT IN (" & strHeadIDs & ")", "trnheadname", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            End If

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'Dim lvItem As ListViewItem
            'lvTranHeadList.Items.Clear()
            dgTransactionHead.DataSource = Nothing
            Call SetDefaultSearchHeadText()
            RemoveHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            Call SetDefaultSearchHeadText()
            AddHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            mintTotalHead = dtTranHead.Rows.Count
            objlblHeadCount.Text = "( 0 / " & mintTotalHead.ToString & " )"
            'Sohail (31 Jul 2017) -- End

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'For Each drRow As DataRow In dtTranHead.Rows

            '    lvItem = New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.Tag = drRow("tranheadunkid")
            '    lvItem.SubItems.Add(drRow("trnheadcode").ToString)
            '    lvItem.SubItems.Add(drRow("trnheadname").ToString)
            '    lvTranHeadList.Items.Add(lvItem)
            'Next

            'If lvTranHeadList.Items.Count <= 0 Then
            '    objchkSelectAll.Enabled = False
            '    objchkSelectAll.Checked = False
            'Else
            '    objchkSelectAll.Enabled = True
            '    lvTranHeadList.Enabled = True
            'End If

            'If lvTranHeadList.Items.Count > 15 Then 'Sohail (14 Nov 2011)
            '    colhName.Width = 180 - 18
            'Else
            '    colhName.Width = 180
            'End If
            If dtTranHead.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTranHead.Columns.Add(dtCol)
            End If
            dtTranHead.Columns("tranheadunkid").ColumnName = "unkid"
            dtTranHead.Columns("trnheadcode").ColumnName = "code"
            dtTranHead.Columns("trnheadname").ColumnName = "name"

            dvTranHead = dtTranHead.DefaultView
            dgTransactionHead.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhUnkid.DataPropertyName = "unkid"
            dgColhCode.DataPropertyName = "code"
            dgColhName.DataPropertyName = "name"

            dgTransactionHead.DataSource = dvTranHead
            dvTranHead.Sort = "IsChecked DESC, name "
            'Sohail (31 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try

    End Sub

    Private Sub CheckAllTransactionHead(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'For Each lvItem As ListViewItem In lvTranHeadList.Items
            '    RemoveHandler lvTranHeadList.ItemChecked, AddressOf lvTranHeadList_ItemChecked
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvTranHeadList.ItemChecked, AddressOf lvTranHeadList_ItemChecked
            'Next
            If dvTranHead IsNot Nothing Then
                For Each dr As DataRowView In dvTranHead
                    'RemoveHandler dgTransactionHead.CellContentClick, AddressOf dgTransactionHead_CellContentClick
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                    'AddHandler dgTransactionHead.CellContentClick, AddressOf dgTransactionHead_CellContentClick
                Next
                dvTranHead.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvTranHead.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblHeadCount.Text = "( " & mintCount.ToString & " / " & mintTotalHead.ToString & " )"

                'Sohail (03 Jan 2019) -- Start
                'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                If dvTranHead Is Nothing AndAlso Not (CInt(cboTransactionType.SelectedValue) = enJVTransactionType.ADVANCE _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.CASH _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.COST_CENTER) _
                Then
                    Exit Try
                Else
                    If mintCount = 1 AndAlso dvTranHead.Table.Select("IsChecked = 1 AND calctype_id = " & CInt(enCalcType.NET_PAY) & " ").Length = 1 Then
                        Call FillRefCodeName(True)
                    Else
                        Call FillRefCodeName(False)
                    End If
                End If
                'Sohail (03 Jan 2019) -- End

            End If
            'Sohail (31 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Oct 2011) -- End

    'Sohail (14 Nov 2011) -- Start
    Private Sub FillLoanList(Optional ByVal LoanSchemeUnkId As Integer = -1)
        Dim objLoan As New clsLoan_Scheme
        Dim dsLoan As DataSet
        Dim dtLoan As DataTable = Nothing
        Try

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            objlblHeadCount.Text = "( 0 / 0 )"
            mintCount = 0
            'Sohail (31 Jul 2017) -- End

            Dim strHeadIDs As String = objLoan.GetLoanSchemeIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, mdtPeriodEnd)
            'Sohail (25 Jul 2020) - [mdtPeriodEnd]
            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            'dsLoan = objLoan.GetList("Loan", True)
            dsLoan = objLoan.GetList("Loan", True, True)
            'Sohail (02 Apr 2018) -- End

            If LoanSchemeUnkId >= 0 Then
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtLoan = New DataView(dsLoan.Tables("Loan"), "loanschemeunkid= " & LoanSchemeUnkId, "", DataViewRowState.CurrentRows).ToTable
                dtLoan = New DataView(dsLoan.Tables("Loan"), "loanschemeunkid= " & LoanSchemeUnkId, "name", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            Else
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtLoan = New DataView(dsLoan.Tables("Loan"), "loanschemeunkid NOT IN (" & strHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
                dtLoan = New DataView(dsLoan.Tables("Loan"), "loanschemeunkid NOT IN (" & strHeadIDs & ")", "name", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            End If

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'Dim lvItem As ListViewItem
            'lvTranHeadList.Items.Clear()
            dgTransactionHead.DataSource = Nothing
            Call SetDefaultSearchHeadText()
            RemoveHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            Call SetDefaultSearchHeadText()
            AddHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            mintTotalHead = dtLoan.Rows.Count
            objlblHeadCount.Text = "( 0 / " & mintTotalHead.ToString & " )"
            'Sohail (31 Jul 2017) -- End

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'For Each drRow As DataRow In dtLoan.Rows

            '    lvItem = New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.Tag = drRow("loanschemeunkid")
            '    lvItem.SubItems.Add(drRow("code").ToString)
            '    lvItem.SubItems.Add(drRow("name").ToString)
            '    lvTranHeadList.Items.Add(lvItem)
            'Next

            'If lvTranHeadList.Items.Count <= 0 Then
            '    objchkSelectAll.Enabled = False
            '    objchkSelectAll.Checked = False
            'Else
            '    objchkSelectAll.Enabled = True
            '    lvTranHeadList.Enabled = True
            'End If

            'If lvTranHeadList.Items.Count > 15 Then
            '    colhName.Width = 180 - 18
            'Else
            '    colhName.Width = 180
            'End If
            If dtLoan.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtLoan.Columns.Add(dtCol)
            End If
            dtLoan.Columns("loanschemeunkid").ColumnName = "unkid"

            dvTranHead = dtLoan.DefaultView
            dgTransactionHead.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhUnkid.DataPropertyName = "unkid"
            dgColhCode.DataPropertyName = "code"
            dgColhName.DataPropertyName = "name"

            dgTransactionHead.DataSource = dvTranHead
            dvTranHead.Sort = "IsChecked DESC, name "
            'Sohail (31 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLoanList", mstrModuleName)
        Finally
            objLoan = Nothing
        End Try

    End Sub

    Private Sub FillSavingList(Optional ByVal SavingSchemeUnkId As Integer = -1)
        Dim objSaving As New clsSavingScheme
        Dim dsSaving As DataSet
        Dim dtSaving As DataTable = Nothing
        Try

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            objlblHeadCount.Text = "( 0 / 0 )"
            mintCount = 0
            'Sohail (31 Jul 2017) -- End

            Dim strHeadIDs As String = objSaving.GetSavingSchemeIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, mdtPeriodEnd)
            'Sohail (25 Jul 2020) - [mdtPeriodEnd]
            dsSaving = objSaving.GetList("Saving", True)

            If SavingSchemeUnkId >= 0 Then
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtSaving = New DataView(dsSaving.Tables("Saving"), "savingschemeunkid= " & SavingSchemeUnkId, "", DataViewRowState.CurrentRows).ToTable
                dtSaving = New DataView(dsSaving.Tables("Saving"), "savingschemeunkid= " & SavingSchemeUnkId, "savingschemename", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            Else
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtSaving = New DataView(dsSaving.Tables("Saving"), "savingschemeunkid NOT IN (" & strHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
                dtSaving = New DataView(dsSaving.Tables("Saving"), "savingschemeunkid NOT IN (" & strHeadIDs & ")", "savingschemename", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            End If

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'Dim lvItem As ListViewItem
            'lvTranHeadList.Items.Clear()
            dgTransactionHead.DataSource = Nothing
            Call SetDefaultSearchHeadText()
            RemoveHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            Call SetDefaultSearchHeadText()
            AddHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            mintTotalHead = dtSaving.Rows.Count
            objlblHeadCount.Text = "( 0 / " & mintTotalHead.ToString & " )"
            'Sohail (31 Jul 2017) -- End

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'For Each drRow As DataRow In dtSaving.Rows

            '    lvItem = New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.Tag = drRow("savingschemeunkid")
            '    lvItem.SubItems.Add(drRow("savingschemecode").ToString)
            '    lvItem.SubItems.Add(drRow("savingschemename").ToString)
            '    lvTranHeadList.Items.Add(lvItem)
            'Next

            'If lvTranHeadList.Items.Count <= 0 Then
            '    objchkSelectAll.Enabled = False
            '    objchkSelectAll.Checked = False
            'Else
            '    objchkSelectAll.Enabled = True
            '    lvTranHeadList.Enabled = True
            'End If

            'If lvTranHeadList.Items.Count > 15 Then
            '    colhName.Width = 180 - 18
            'Else
            '    colhName.Width = 180
            'End If
            If dtSaving.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtSaving.Columns.Add(dtCol)
            End If
            dtSaving.Columns("savingschemeunkid").ColumnName = "unkid"
            dtSaving.Columns("savingschemecode").ColumnName = "code"
            dtSaving.Columns("savingschemename").ColumnName = "name"

            dvTranHead = dtSaving.DefaultView
            dgTransactionHead.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhUnkid.DataPropertyName = "unkid"
            dgColhCode.DataPropertyName = "code"
            dgColhName.DataPropertyName = "name"

            dgTransactionHead.DataSource = dvTranHead
            dvTranHead.Sort = "IsChecked DESC, name "
            'Sohail (31 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillSavingList", mstrModuleName)
        Finally
            objSaving = Nothing
        End Try

    End Sub
    'Sohail (14 Nov 2011) -- End

    'Sohail (21 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillActivityList(Optional ByVal ActivityUnkId As Integer = -1)
        Dim objActivity As New clsActivity_Master
        Dim dsActivity As DataSet
        Dim dtActivity As DataTable = Nothing
        Try
            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            objlblHeadCount.Text = "( 0 / 0 )"
            mintCount = 0
            'Sohail (31 Jul 2017) -- End

            Dim strHeadIDs As String = objActivity.GetActivityIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, mdtPeriodEnd) 'Sohail (25 Jul 2020) - [mdtPeriodEnd]
            dsActivity = objActivity.GetList("Activity", True)

            If ActivityUnkId >= 0 Then
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtActivity = New DataView(dsActivity.Tables("Activity"), "activityunkid= " & ActivityUnkId, "", DataViewRowState.CurrentRows).ToTable
                dtActivity = New DataView(dsActivity.Tables("Activity"), "activityunkid= " & ActivityUnkId, "name", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            Else
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtActivity = New DataView(dsActivity.Tables("Activity"), "activityunkid NOT IN (" & strHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
                dtActivity = New DataView(dsActivity.Tables("Activity"), "activityunkid NOT IN (" & strHeadIDs & ")", "name", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            End If

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'Dim lvItem As ListViewItem
            'lvTranHeadList.Items.Clear()
            dgTransactionHead.DataSource = Nothing
            Call SetDefaultSearchHeadText()
            RemoveHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            Call SetDefaultSearchHeadText()
            AddHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            mintTotalHead = dtActivity.Rows.Count
            objlblHeadCount.Text = "( 0 / " & mintTotalHead.ToString & " )"
            'Sohail (31 Jul 2017) -- End

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'For Each drRow As DataRow In dtActivity.Rows

            '    lvItem = New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.Tag = drRow("activityunkid")
            '    lvItem.SubItems.Add(drRow("code").ToString)
            '    lvItem.SubItems.Add(drRow("name").ToString)
            '    lvTranHeadList.Items.Add(lvItem)
            'Next

            'If lvTranHeadList.Items.Count <= 0 Then
            '    objchkSelectAll.Enabled = False
            '    objchkSelectAll.Checked = False
            'Else
            '    objchkSelectAll.Enabled = True
            '    lvTranHeadList.Enabled = True
            'End If

            'If lvTranHeadList.Items.Count > 15 Then
            '    colhName.Width = 180 - 18
            'Else
            '    colhName.Width = 180
            'End If
            If dtActivity.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtActivity.Columns.Add(dtCol)
            End If
            dtActivity.Columns("activityunkid").ColumnName = "unkid"

            dvTranHead = dtActivity.DefaultView
            dgTransactionHead.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhUnkid.DataPropertyName = "unkid"
            dgColhCode.DataPropertyName = "code"
            dgColhName.DataPropertyName = "name"

            dgTransactionHead.DataSource = dvTranHead
            dvTranHead.Sort = "IsChecked DESC, name "
            'Sohail (31 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillActivityList", mstrModuleName)
        Finally
            objActivity = Nothing
        End Try

    End Sub
    'Sohail (21 Jun 2013) -- End

    'Sohail (12 Nov 2014) -- Start
    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
    Private Sub FillCRExpenseList(Optional ByVal ExpenseUnkId As Integer = -1)
        Dim objExpense As New clsExpense_Master
        Dim dsExpense As DataSet
        Dim dtExpense As DataTable = Nothing
        Try
            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            objlblHeadCount.Text = "( 0 / 0 )"
            mintCount = 0
            'Sohail (31 Jul 2017) -- End

            Dim strHeadIDs As String = objExpense.GetExpenseIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, mdtPeriodEnd) 'Sohail (25 Jul 2020) - [mdtPeriodEnd]
            dsExpense = objExpense.GetList("Expense", True)

            If ExpenseUnkId >= 0 Then
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtExpense = New DataView(dsExpense.Tables("Expense"), "expenseunkid= " & ExpenseUnkId, "", DataViewRowState.CurrentRows).ToTable
                dtExpense = New DataView(dsExpense.Tables("Expense"), "expenseunkid= " & ExpenseUnkId, "name", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            Else
                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'dtExpense = New DataView(dsExpense.Tables("Expense"), "expenseunkid NOT IN (" & strHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
                dtExpense = New DataView(dsExpense.Tables("Expense"), "expenseunkid NOT IN (" & strHeadIDs & ")", "name", DataViewRowState.CurrentRows).ToTable
                'Sohail (06 Aug 2016) -- End
            End If

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'Dim lvItem As ListViewItem
            'lvTranHeadList.Items.Clear()
            dgTransactionHead.DataSource = Nothing
            Call SetDefaultSearchHeadText()
            RemoveHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            Call SetDefaultSearchHeadText()
            AddHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            mintTotalHead = dtExpense.Rows.Count
            objlblHeadCount.Text = "( 0 / " & mintTotalHead.ToString & " )"
            'Sohail (31 Jul 2017) -- End

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'For Each drRow As DataRow In dtExpense.Rows

            '    lvItem = New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.Tag = drRow("expenseunkid")
            '    lvItem.SubItems.Add(drRow("code").ToString)
            '    lvItem.SubItems.Add(drRow("name").ToString)
            '    lvTranHeadList.Items.Add(lvItem)
            'Next

            'If lvTranHeadList.Items.Count <= 0 Then
            '    objchkSelectAll.Enabled = False
            '    objchkSelectAll.Checked = False
            'Else
            '    objchkSelectAll.Enabled = True
            '    lvTranHeadList.Enabled = True
            'End If

            'If lvTranHeadList.Items.Count > 15 Then
            '    colhName.Width = 180 - 18
            'Else
            '    colhName.Width = 180
            'End If
            If dtExpense.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtExpense.Columns.Add(dtCol)
            End If
            dtExpense.Columns("expenseunkid").ColumnName = "unkid"

            dvTranHead = dtExpense.DefaultView
            dgTransactionHead.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhUnkid.DataPropertyName = "unkid"
            dgColhCode.DataPropertyName = "code"
            dgColhName.DataPropertyName = "name"

            dgTransactionHead.DataSource = dvTranHead
            dvTranHead.Sort = "IsChecked DESC, name "
            'Sohail (31 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCRExpenseList", mstrModuleName)
        Finally
            objExpense = Nothing
        End Try

    End Sub
    'Sohail (12 Nov 2014) -- End

    'Sohail (06 Aug 2016) -- Start
    'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
    Private Sub FillBankBranchList(Optional ByVal BranchUnkId As Integer = -1)
        Dim objBankBranch As New clsbankbranch_master
        Dim dsBankBranch As DataSet
        Dim dtBankBranch As DataTable = Nothing
        Try
            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            objlblHeadCount.Text = "( 0 / 0 )"
            mintCount = 0
            'Sohail (31 Jul 2017) -- End

            Dim strHeadIDs As String = objBankBranch.GetBankBranchIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, mdtPeriodEnd) 'Sohail (25 Jul 2020) - [mdtPeriodEnd]
            dsBankBranch = objBankBranch.GetList("BankBranch", True)

            If BranchUnkId >= 0 Then
                dtBankBranch = New DataView(dsBankBranch.Tables("BankBranch"), "branchunkid= " & BranchUnkId, "branchname", DataViewRowState.CurrentRows).ToTable
            Else
                dtBankBranch = New DataView(dsBankBranch.Tables("BankBranch"), "branchunkid NOT IN (" & strHeadIDs & ")", "branchname", DataViewRowState.CurrentRows).ToTable
            End If

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'Dim lvItem As ListViewItem
            'lvTranHeadList.Items.Clear()
            dgTransactionHead.DataSource = Nothing
            Call SetDefaultSearchHeadText()
            RemoveHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            Call SetDefaultSearchHeadText()
            AddHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            mintTotalHead = dtBankBranch.Rows.Count
            objlblHeadCount.Text = "( 0 / " & mintTotalHead.ToString & " )"
            'Sohail (31 Jul 2017) -- End

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'For Each drRow As DataRow In dtBankBranch.Rows

            '    lvItem = New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.Tag = drRow("branchunkid")
            '    lvItem.SubItems.Add(drRow("branchcode").ToString)
            '    lvItem.SubItems.Add(drRow("branchname").ToString)
            '    lvTranHeadList.Items.Add(lvItem)
            'Next

            'If lvTranHeadList.Items.Count <= 0 Then
            '    objchkSelectAll.Enabled = False
            '    objchkSelectAll.Checked = False
            'Else
            '    objchkSelectAll.Enabled = True
            '    lvTranHeadList.Enabled = True
            'End If

            'If lvTranHeadList.Items.Count > 15 Then
            '    colhName.Width = 180 - 18
            'Else
            '    colhName.Width = 180
            'End If
            If dtBankBranch.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtBankBranch.Columns.Add(dtCol)
            End If
            dtBankBranch.Columns("branchunkid").ColumnName = "unkid"
            dtBankBranch.Columns("branchcode").ColumnName = "code"
            dtBankBranch.Columns("branchname").ColumnName = "name"

            dvTranHead = dtBankBranch.DefaultView
            dgTransactionHead.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhUnkid.DataPropertyName = "unkid"
            dgColhCode.DataPropertyName = "code"
            dgColhName.DataPropertyName = "name"

            dgTransactionHead.DataSource = dvTranHead
            dvTranHead.Sort = "IsChecked DESC, name "
            'Sohail (31 Jul 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBankBranchList", mstrModuleName)
        Finally
            objBankBranch = Nothing
        End Try

    End Sub
    'Sohail (06 Aug 2016) -- End

    'Sohail (31 Jul 2017) -- Start
    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgTransactionHead.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblHeadCount.Text = "( " & mintCount.ToString & " / " & mintTotalHead.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgTransactionHead.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgTransactionHead.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchHeadText()
        Try
            mstrSearchHeadText = lblSearchTranhead.Text
            With txtSearchTranHead
                .ForeColor = Color.Gray
                .Text = mstrSearchHeadText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchHeadText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 8, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpComboText", mstrModuleName)
        End Try
    End Sub
    'Sohail (31 Jul 2017) -- End

    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    Private Sub FillCompanyBankBranchList(Optional ByVal BranchUnkId As Integer = -1)
        Dim objCBankTran As New clsCompany_Bank_tran
        Dim objBankBranch As New clsbankbranch_master
        Dim dsBankBranch As DataTable
        Dim dtBankBranch As DataTable = Nothing
        Try
            objlblHeadCount.Text = "( 0 / 0 )"
            mintCount = 0

            Dim strHeadIDs As String = objBankBranch.GetCompanyBankBranchIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, mdtPeriodEnd) 'Sohail (25 Jul 2020) - [mdtPeriodEnd]
            objCBankTran._CompanyId = Company._Object._Companyunkid
            dsBankBranch = objCBankTran._SetDataTable

            If BranchUnkId >= 0 Then
                'Sohail (21 May 2020) -- Start
                'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
                'dtBankBranch = New DataView(dsBankBranch, "branchunkid= " & BranchUnkId, "branchname", DataViewRowState.CurrentRows).ToTable
                dtBankBranch = New DataView(dsBankBranch, "companybanktranunkid= " & BranchUnkId, "branchname", DataViewRowState.CurrentRows).ToTable
                'Sohail (21 May 2020) -- End
            Else
                'Sohail (21 May 2020) -- Start
                'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
                'dtBankBranch = New DataView(dsBankBranch, "branchunkid NOT IN (" & strHeadIDs & ")", "branchname", DataViewRowState.CurrentRows).ToTable
                dtBankBranch = New DataView(dsBankBranch, "companybanktranunkid NOT IN (" & strHeadIDs & ")", "branchname", DataViewRowState.CurrentRows).ToTable
                'Sohail (21 May 2020) -- End
            End If

            dgTransactionHead.DataSource = Nothing
            Call SetDefaultSearchHeadText()
            RemoveHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            Call SetDefaultSearchHeadText()
            AddHandler txtSearchTranHead.TextChanged, AddressOf txtSearchTranHead_TextChanged
            mintTotalHead = dtBankBranch.Rows.Count
            objlblHeadCount.Text = "( 0 / " & mintTotalHead.ToString & " )"

            If dtBankBranch.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtBankBranch.Columns.Add(dtCol)
            End If
            'Sohail (21 May 2020) -- Start
            'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
            'dtBankBranch.Columns("branchunkid").ColumnName = "unkid"
            dtBankBranch.Columns("companybanktranunkid").ColumnName = "unkid"
            'Sohail (21 May 2020) -- End
            dtBankBranch.Columns("branchcode").ColumnName = "code"
            'Sohail (21 May 2020) -- Start
            'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
            'dtBankBranch.Columns("branchname").ColumnName = "name"
            dtBankBranch.Columns("account_no").ColumnName = "name"
            'Sohail (21 May 2020) -- End

            dvTranHead = dtBankBranch.DefaultView
            dgTransactionHead.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhUnkid.DataPropertyName = "unkid"
            dgColhCode.DataPropertyName = "code"
            dgColhName.DataPropertyName = "name"

            dgTransactionHead.DataSource = dvTranHead
            dvTranHead.Sort = "IsChecked DESC, name "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCompanyBankBranchList", mstrModuleName)
        Finally
            objBankBranch = Nothing
        End Try

    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Jan 2019) -- End

#End Region

#Region " Form's Events "

    Private Sub frmAccountConfiguration_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAccountConfig = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountConfiguration_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAccountConfiguration_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.S
                    If e.Control = True Then
                        btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountConfiguration_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAccountConfiguration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAccountConfig = New clsAccountConfiguration
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objAccountConfig._Accountconfigunkid = mintAccountConfigUnkid
                cboTransactionType.Enabled = False
                'Nilay (20 Mar 2017) -- Start
                cboHeadType.Enabled = False
                objbtnSearchHeadType.Enabled = False
                'Nilay (20 Mar 2017) -- End
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                cboPeriod.Enabled = False
                'Sohail (03 Jul 2020) -- End
                'Sohail (12 Oct 2011) -- Start
                'cboTrnHead.Enabled = False
                'objbtnSearchTranHead.Enabled = False
                'Sohail (14 Nov 2011) -- Start
                chkIsExist.Visible = False
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                Call GetValue()
                'Sohail (03 Jul 2020) -- End
                'If objAccountConfig._Transactiontype_Id = enJVTransactionType.TRANSACTION_HEAD Then FillList(objAccountConfig._Tranheadunkid)
                If objAccountConfig._Transactiontype_Id = enJVTransactionType.TRANSACTION_HEAD Then
                    FillList(objAccountConfig._Tranheadunkid)
                ElseIf objAccountConfig._Transactiontype_Id = enJVTransactionType.LOAN Then
                    FillLoanList(objAccountConfig._Tranheadunkid)
                ElseIf objAccountConfig._Transactiontype_Id = enJVTransactionType.SAVINGS Then
                    FillSavingList(objAccountConfig._Tranheadunkid)

                    'Sohail (21 Jun 2013) -- Start
                    'TRA - ENHANCEMENT
                ElseIf objAccountConfig._Transactiontype_Id = enJVTransactionType.PAY_PER_ACTIVITY Then
                    FillActivityList(objAccountConfig._Tranheadunkid)
                    'Sohail (21 Jun 2013) -- End

                    'Sohail (12 Nov 2014) -- Start
                    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                ElseIf objAccountConfig._Transactiontype_Id = enJVTransactionType.CR_EXPENSE Then
                    FillCRExpenseList(objAccountConfig._Tranheadunkid)
                    'Sohail (12 Nov 2014) -- End

                    'Sohail (06 Aug 2016) -- Start
                    'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                ElseIf objAccountConfig._Transactiontype_Id = enJVTransactionType.BANK Then
                    FillBankBranchList(objAccountConfig._Tranheadunkid)
                    'Sohail (06 Aug 2016) -- End

                    'Sohail (03 Jan 2019) -- Start
                    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                ElseIf objAccountConfig._Transactiontype_Id = enJVTransactionType.COMPANY_BANK Then
                    FillCompanyBankBranchList(objAccountConfig._Tranheadunkid)
                    'Sohail (03 Jan 2019) -- End

                End If
                'Sohail (14 Nov 2011) -- End

                'Sohail (31 Jul 2017) -- Start
                'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                'If lvTranHeadList.Items.Count > 0 Then lvTranHeadList.Items(0).Checked = True
                If dgTransactionHead.RowCount > 0 Then dvTranHead.ToTable().Rows(0)("IsChecked") = True
                'Sohail (31 Jul 2017) -- End
                'Sohail (12 Oct 2011) -- End

            End If
            'Call GetValue() 'Sohail (03 Jul 2020)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountConfiguration_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAccountConfiguration.SetMessages()
            objfrm._Other_ModuleNames = "clsAccountConfiguration"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " ComboBox's Events "
    Private Sub cboAccountGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAccountGroup.SelectedIndexChanged 'Sohail (25 Jan 2011)
        Dim objAccount As New clsAccount_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objAccount.getComboList("Accounts", True, CInt(cboAccountGroup.SelectedValue))
            With cboAccount
                .ValueMember = "accountunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Accounts")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            Call SetDefaultSearchText(cboAccount)
            'Sohail (31 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAccountGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objAccount = Nothing
        End Try
    End Sub

    Private Sub cboTransactionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTransactionType.SelectedIndexChanged 'Sohail (25 Jan 2011)
        Try
            'Sohail (03 Jan 2019) -- Start
            'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
            Call FillRefCodeName(False)
            'Sohail (03 Jan 2019) -- End

            If menAction <> enAction.EDIT_ONE Then

                'Nilay (20 Mar 2017) -- Start
                If CInt(cboTransactionType.SelectedValue) = enJVTransactionType.TRANSACTION_HEAD Then
                    cboHeadType.Enabled = True
                    objbtnSearchHeadType.Enabled = True
                Else
                    cboHeadType.SelectedValue = 0
                    cboHeadType.Enabled = False
                    objbtnSearchHeadType.Enabled = False
                End If
                'Nilay (20 Mar 2017) -- End

                Select Case CInt(cboTransactionType.SelectedValue)
                    Case enJVTransactionType.TRANSACTION_HEAD
                        'Sohail (12 Oct 2011) -- Start
                        'cboTrnHead.Enabled = True
                        'objbtnSearchTranHead.Enabled = True
                        Call FillList()
                        'Sohail (12 Oct 2011) -- End
                        'Sohail (14 Nov 2011) -- Start
                    Case enJVTransactionType.LOAN
                        Call FillLoanList()
                    Case enJVTransactionType.SAVINGS
                        Call FillSavingList()
                        'Sohail (14 Nov 2011) -- End

                        'Sohail (21 Jun 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case enJVTransactionType.PAY_PER_ACTIVITY
                        Call FillActivityList()
                        'Sohail (21 Jun 2013) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                    Case enJVTransactionType.CR_EXPENSE
                        Call FillCRExpenseList()
                        'Sohail (12 Nov 2014) -- End

                        'Sohail (06 Aug 2016) -- Start
                        'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                    Case enJVTransactionType.BANK
                        Call FillBankBranchList()
                        'Sohail (06 Aug 2016) -- End

                        'Sohail (03 Jan 2019) -- Start
                        'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                    Case enJVTransactionType.COMPANY_BANK
                        Call FillCompanyBankBranchList()
                        'Sohail (03 Jan 2019) -- End

                    Case Else
                        'Sohail (12 Oct 2011) -- Start
                        'cboTrnHead.SelectedValue = 0
                        'cboTrnHead.Enabled = False
                        'objbtnSearchTranHead.Enabled = False
                        'Sohail (31 Jul 2017) -- Start
                        'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                        'lvTranHeadList.Items.Clear()
                        'lvTranHeadList.Enabled = False
                        dgTransactionHead.DataSource = Nothing
                        dvTranHead = Nothing
                        objlblHeadCount.Text = "( 0 / 0 )"
                        mintCount = 0
                        'Sohail (31 Jul 2017) -- End
                        'Sohail (12 Oct 2011) -- End
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTransactionType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Nilay (20 Mar 2017) -- Start
    Private Sub cboHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboHeadType.SelectedIndexChanged
        Try
            If CInt(cboHeadType.SelectedValue) > 0 Then
                Call FillList()
            Else
                Call cboTransactionType_SelectedIndexChanged(cboTransactionType, New EventArgs())
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Nilay (20 Mar 2017) -- End

    'Sohail (31 Jul 2017) -- Start
    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
    Private Sub cboAccount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboAccount.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboAccount.ValueMember
                    .DisplayMember = cboAccount.DisplayMember
                    .DataSource = CType(cboAccount.DataSource, DataTable)
                    .CodeMember = "account_code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboAccount.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboAccount.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAccount_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAccount.SelectedIndexChanged
        Try
            'Sohail (03 Jan 2019) -- Start
            'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
            'If CInt(cboAccount.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboAccount)
            If CInt(cboAccount.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cboAccount)
            Else
                Call SetRegularFont(cboAccount)
            End If
            'Sohail (03 Jan 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAccount_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAccount_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAccount.GotFocus
        Try
            With cboAccount
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAccount_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAccount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAccount.Leave
        Try
            If CInt(cboAccount.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboAccount)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAccount_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (31 Jul 2017) -- End

    'Sohail (25 Jul 2020) -- Start
    'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                objPeriod = Nothing
            Else
                mdtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
                mdtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 Jul 2020) -- End

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'Sohail (31 Aug 2017) -- Start
            'Issue - 69.1 - Bind transaction issue in inserting employee account configuration.
            'If dvTranHead Is Nothing Then Exit Try
            'mintCheckedHeads = dvTranHead.Table.Select("IsChecked = 1 ").Length
            If dvTranHead Is Nothing AndAlso Not (CInt(cboTransactionType.SelectedValue) = enJVTransactionType.ADVANCE _
                OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.CASH _
                OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.COST_CENTER) _
                Then
                Exit Try
            Else
                If dvTranHead IsNot Nothing Then mintCheckedHeads = dvTranHead.Table.Select("IsChecked = 1 ").Length
            End If
            'Sohail (31 Aug 2017) -- End
            'Sohail (31 Jul 2017) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            Dim objPeriod As New clscommom_period_Tran
            If cboPeriod.SelectedValue IsNot Nothing Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Period is already closed."), enMsgBoxStyle.Information)
                    cboPeriod.Focus()
                    Exit Sub
                End If
            End If
            'Sohail (03 Jul 2020) -- End

            If CInt(cboTransactionType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Head Type. Head Type is mandatory information."), enMsgBoxStyle.Information)
                cboTransactionType.Focus()
                Exit Sub
                'Sohail (12 Oct 2011) -- Start
                'ElseIf cboTrnHead.Enabled = True And CInt(cboTrnHead.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Transaction Head. Transaction Head is mandatory information."), enMsgBoxStyle.Information)
                '    cboTrnHead.Focus()
                '    Exit Sub
                'Sohail (12 Oct 2011) -- End
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            ElseIf cboPeriod.Enabled = True AndAlso CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            ElseIf cboPeriod.Enabled = False AndAlso cboPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Period is already closed."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
                'Sohail (03 Jul 2020) -- End
            ElseIf CInt(cboAccountGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Account Group. Account Group is mandatory information."), enMsgBoxStyle.Information)
                cboAccountGroup.Focus()
                Exit Sub
            ElseIf CInt(cboAccount.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Account. Account is mandatory information."), enMsgBoxStyle.Information)
                cboAccount.Focus()
                Exit Sub
                'Sohail (14 Nov 2011) -- Start
            ElseIf CInt(cboMapRefCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Reference Code. Reference Code is mandatory information."), enMsgBoxStyle.Information)
                cboMapRefCode.Focus()
                Exit Sub
            ElseIf CInt(cboMapRefName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Reference Name. Reference Name is mandatory information."), enMsgBoxStyle.Information)
                cboMapRefName.Focus()
                Exit Sub
                'Sohail (14 Nov 2011) -- End
                'Sohail (12 Oct 2011) -- Start
                'Sohail (14 Nov 2011) -- Start
                'Sohail (31 Jul 2017) -- Start
                'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                'ElseIf lvTranHeadList.CheckedItems.Count <= 0 AndAlso _
                '        (CInt(cboTransactionType.SelectedValue) = enJVTransactionType.TRANSACTION_HEAD _
                '            OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.LOAN _
                '            OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.SAVINGS _
                '            OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.PAY_PER_ACTIVITY _
                '            OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.CR_EXPENSE _
                '            OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.BANK _
                '            ) Then
                '    'Sohail (06 Aug 2016) - [BANK]
                '    'Sohail (12 Nov 2014) - [CR_EXPENSE]
                '    'Sohail (21 Jun 2013) - [PAY_PER_ACTIVITY]
                '    'ElseIf lvTranHeadList.CheckedItems.Count <= 0 AndAlso CInt(cboTransactionType.SelectedValue) = enJVTransactionType.TRANSACTION_HEAD Then
                '    'Sohail (14 Nov 2011) -- End
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select atleast one Transaction Head. Transaction Head is mandatory information."), enMsgBoxStyle.Information)
                '    lvTranHeadList.Focus()
                '    Exit Sub
            ElseIf mintCheckedHeads <= 0 AndAlso _
                (CInt(cboTransactionType.SelectedValue) = enJVTransactionType.TRANSACTION_HEAD _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.LOAN _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.SAVINGS _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.PAY_PER_ACTIVITY _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.CR_EXPENSE _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.BANK _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.COMPANY_BANK _
                    ) Then
                'Sohail (03 Jan 2019) - [COMPANY_BANK]
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select atleast one Transaction Head. Transaction Head is mandatory information."), enMsgBoxStyle.Information)
                dgTransactionHead.Focus()
                Exit Sub
                'Sohail (31 Jul 2017) -- End
                'Sohail (12 Oct 2011) -- End
            End If

            Dim mstrTranheadID As String = "" 'Sohail (12 Oct 2011)
            'Sohail (31 Aug 2017) -- Start
            'Issue - 69.1 - Bind transaction issue in inserting employee account configuration.
            'mstrTranheadID = String.Join(",", (From p In dvTranHead.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("unkid").ToString)).ToArray) 'Sohail (31 Jul 2017)
            If dvTranHead IsNot Nothing Then
                mstrTranheadID = String.Join(",", (From p In dvTranHead.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("unkid").ToString)).ToArray)
            End If
            'Sohail (31 Aug 2017) -- End
            'Sohail (02 Aug 2011) -- Start
            Dim objTranHead As New clsTransactionHead
            Dim blnUsed As Boolean
            Select Case CInt(cboTransactionType.SelectedValue)
                Case enJVTransactionType.TRANSACTION_HEAD
                    'Sohail (12 Oct 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.TRANSACTION_HEAD, CInt(cboTrnHead.SelectedValue))
                    'Sohail (31 Jul 2017) -- Start
                    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                    'For Each lvItem As ListViewItem In lvTranHeadList.CheckedItems
                    '    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.TRANSACTION_HEAD, CInt(lvItem.Tag))
                    '    mstrTranheadID &= ", " & lvItem.Tag.ToString
                    'Next
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.TRANSACTION_HEAD, mdtPeriodEnd, 0, mstrTranheadID)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    'Sohail (31 Jul 2017) -- End
                    'Sohail (12 Oct 2011) -- End
                Case enJVTransactionType.LOAN
                    'Sohail (12 Oct 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.LOAN, CInt(cboTrnHead.SelectedValue))
                    'Sohail (14 Nov 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.LOAN, 0)
                    'Sohail (31 Jul 2017) -- Start
                    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                    'For Each lvItem As ListViewItem In lvTranHeadList.CheckedItems
                    '    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.LOAN, CInt(lvItem.Tag))
                    '    mstrTranheadID &= ", " & lvItem.Tag.ToString
                    'Next
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.LOAN, mdtPeriodEnd, 0, mstrTranheadID)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    'Sohail (31 Jul 2017) -- End
                    'Sohail (14 Nov 2011) -- End
                    'Sohail (12 Oct 2011) -- End
                Case enJVTransactionType.ADVANCE
                    'Sohail (12 Oct 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.ADVANCE, CInt(cboTrnHead.SelectedValue))
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.ADVANCE, mdtPeriodEnd, 0)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    mstrTranheadID = "" 'Sohail (31 Jul 2017)
                    'Sohail (12 Oct 2011) -- End
                Case enJVTransactionType.SAVINGS
                    'Sohail (12 Oct 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.SAVINGS, CInt(cboTrnHead.SelectedValue))
                    'Sohail (14 Nov 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.SAVINGS, 0)
                    'Sohail (31 Jul 2017) -- Start
                    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                    'For Each lvItem As ListViewItem In lvTranHeadList.CheckedItems
                    '    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.SAVINGS, CInt(lvItem.Tag))
                    '    mstrTranheadID &= ", " & lvItem.Tag.ToString
                    'Next
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.SAVINGS, mdtPeriodEnd, 0, mstrTranheadID)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    'Sohail (31 Jul 2017) -- End
                    'Sohail (14 Nov 2011) -- End

                    'Sohail (21 Jun 2013) -- Start
                    'TRA - ENHANCEMENT
                Case enJVTransactionType.PAY_PER_ACTIVITY
                    'Sohail (31 Jul 2017) -- Start
                    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                    'For Each lvItem As ListViewItem In lvTranHeadList.CheckedItems
                    '    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.PAY_PER_ACTIVITY, CInt(lvItem.Tag))
                    '    mstrTranheadID &= ", " & lvItem.Tag.ToString
                    'Next
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.PAY_PER_ACTIVITY, mdtPeriodEnd, 0, mstrTranheadID)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    'Sohail (31 Jul 2017) -- End
                    'Sohail (21 Jun 2013) -- End

                    'Sohail (12 Nov 2014) -- Start
                    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                Case enJVTransactionType.CR_EXPENSE
                    'Sohail (31 Jul 2017) -- Start
                    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                    'For Each lvItem As ListViewItem In lvTranHeadList.CheckedItems
                    '    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.CR_EXPENSE, CInt(lvItem.Tag))
                    '    mstrTranheadID &= ", " & lvItem.Tag.ToString
                    'Next
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.CR_EXPENSE, mdtPeriodEnd, 0, mstrTranheadID)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    'Sohail (31 Jul 2017) -- End
                    'Sohail (12 Nov 2014) -- End

                    'Sohail (12 Oct 2011) -- End
                Case enJVTransactionType.CASH
                    'Sohail (12 Oct 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.CASH, CInt(cboTrnHead.SelectedValue))
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.CASH, mdtPeriodEnd, 0)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    mstrTranheadID = "" 'Sohail (31 Jul 2017)
                    'Sohail (12 Oct 2011) -- End
                Case enJVTransactionType.BANK
                    'Sohail (12 Oct 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.BANK, CInt(cboTrnHead.SelectedValue))
                    'Sohail (06 Aug 2016) -- Start
                    'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.BANK, 0)
                    'Sohail (31 Jul 2017) -- Start
                    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
                    'For Each lvItem As ListViewItem In lvTranHeadList.CheckedItems
                    '    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.BANK, CInt(lvItem.Tag))
                    '    mstrTranheadID &= ", " & lvItem.Tag.ToString
                    'Next
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.BANK, mdtPeriodEnd, 0, mstrTranheadID)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    'Sohail (31 Jul 2017) -- End
                    'Sohail (06 Aug 2016) -- End
                    'Sohail (12 Oct 2011) -- End
                Case enJVTransactionType.COST_CENTER
                    'Sohail (12 Oct 2011) -- Start
                    'blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.COST_CENTER, CInt(cboTrnHead.SelectedValue))
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.COST_CENTER, mdtPeriodEnd, 0)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    mstrTranheadID = "" 'Sohail (31 Jul 2017)
                    'Sohail (12 Oct 2011) -- End

                    'Sohail (03 Jan 2019) -- Start
                    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                Case enJVTransactionType.COMPANY_BANK
                    blnUsed = objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, enJVTransactionType.COMPANY_BANK, mdtPeriodEnd, 0, mstrTranheadID)
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    'Sohail (03 Jan 2019) -- End

            End Select
            objTranHead = Nothing
            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'If blnUsed = True AndAlso (CInt(cboTransactionType.SelectedValue) <> enJVTransactionType.TRANSACTION_HEAD OrElse lvTranHeadList.CheckedItems.Count = 1) Then 'Sohail (12 Oct 2011)
            If blnUsed = True AndAlso (CInt(cboTransactionType.SelectedValue) <> enJVTransactionType.TRANSACTION_HEAD OrElse mintCheckedHeads = 1) Then
                'Sohail (31 Jul 2017) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! This Transaction Head is already mapped in another account configuration."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (12 Oct 2011) -- Start
            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            'If mstrTranheadID.Length > 0 Then
            '    mstrTranheadID = Mid(mstrTranheadID, 3)
            'End If
            'Sohail (31 Jul 2017) -- End
            'Sohail (12 Oct 2011) -- End
            'Sohail (02 Aug 2011) -- End

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                'Sohail (12 Oct 2011) -- Start
                'blnFlag = objAccountConfig.Update()
                blnFlag = objAccountConfig.Update(chkIsExist.Checked)
                'Sohail (12 Oct 2011) -- End
            Else
                'Sohail (12 Oct 2011) -- Start
                'blnFlag = objAccountConfig.Insert()
                blnFlag = objAccountConfig.Insert(chkIsExist.Checked, CInt(cboTransactionType.SelectedValue), mstrTranheadID)
                'Sohail (12 Oct 2011) -- End
            End If

            If blnFlag = False And objAccountConfig._Message <> "" Then
                eZeeMsgBox.Show(objAccountConfig._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAccountConfig = Nothing
                    objAccountConfig = New clsAccountConfiguration
                    Call GetValue()
                    'Sohail (12 Oct 2011) -- Start
                    'cboTrnHead.Focus()
                    cboTransactionType.Focus()
                    'Sohail (12 Oct 2011) -- End
                Else
                    mintAccountConfigUnkid = objAccountConfig._Accountunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (20 Mar 2017) -- Start
    Private Sub objbtnSearchHeadType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchHeadType.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboHeadType.ValueMember
                .DisplayMember = cboHeadType.DisplayMember
                .DataSource = CType(cboHeadType.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboHeadType.SelectedValue = frm.SelectedValue
                cboHeadType.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchHeadType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Nilay (20 Mar 2017) -- End

#End Region

#Region " Other Control's Events "
    'Sohail (12 Oct 2011) -- Start
    'Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objTranHead As New clsTransactionHead
    '    Dim objfrm As New frmCommonSearch
    '    Dim dsList As DataSet
    '    Try
    '        'If User._Object._RightToLeft = True Then
    '        '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '        '    objfrm.RightToLeftLayout = True
    '        '    Call Language.ctlRightToLeftlayOut(objfrm)
    '        'End If

    '        'Sohail (02 Aug 2011) -- Start
    '        Dim dtTable As DataTable
    '        Dim strHeadIDs As String = objTranHead.GetTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION)
    '        'dsList = objTranHead.getComboList("TranHead", False)
    '        dsList = objTranHead.getComboList("TranHead", False, , , , True)
    '        dtTable = New DataView(dsList.Tables("TranHead"), "tranheadunkid NOT IN (" & strHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
    '        'Sohail (02 Aug 2011) -- End
    '        With cboTrnHead
    '            objfrm.ValueMember = "tranheadunkid"
    '            objfrm.DisplayMember = "Name"
    '            objfrm.CodeMember = "code"
    '            objfrm.DataSource = dtTable 'dsList.Tables("TranHead") 'Sohail (02 Aug 2011)
    '            If objfrm.DisplayDialog Then
    '                .SelectedValue = objfrm.SelectedValue
    'End If
    '            .Focus()
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
    '    Finally
    '        objTranHead = Nothing
    '        objfrm = Nothing
    '    End Try
    'End Sub
    'Sohail (12 Oct 2011) -- End

    Private Sub objbtnAddAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddAccount.Click
        Dim frm As New frmAccount_AddEdit
        Dim intRefId As Integer = -1
        Dim objAccount As New clsAccount_master
        Dim dsCombo As DataSet
        Try
            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                objAccount._Accountunkid = intRefId
                cboAccountGroup.SelectedValue = objAccount._Accountgroup_Id
                dsCombo = objAccount.getComboList("Accounts", True, CInt(cboAccountGroup.SelectedValue))
                With cboAccount
                    .ValueMember = "accountunkid"
                    .DisplayMember = "Name"
                    .DataSource = dsCombo.Tables("Accounts")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddAccount_Click", mstrModuleName)
        Finally
            objAccount = Nothing
        End Try
    End Sub

    'Sohail (12 Oct 2011) -- Start
    Private Sub objbtnSearchTrantype_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrantype.Click
        Dim objMaster As New clsMasterData
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet
        Try
            dsList = objMaster.getComboListJVTransactionType("HeadType")
            With cboTransactionType
                objfrm.ValueMember = "Id"
                objfrm.DisplayMember = "Name"
                objfrm.CodeMember = "Name"
                objfrm.DataSource = dsList.Tables("HeadType")
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrantype_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Oct 2011) -- End


    'SHANI (06 MAY 2015) -- Start

    'Sohail (31 Jul 2017) -- Start
    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
    'Private Sub txtSearchTranHead_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchTranHead.TextChanged
    '    Try
    '        If lvTranHeadList.Items.Count <= 0 Then Exit Sub
    '        lvTranHeadList.SelectedIndices.Clear()
    '        Dim lvFoundItem As ListViewItem = lvTranHeadList.FindItemWithText(txtSearchTranHead.Text, True, 0, True)
    '        If lvFoundItem IsNot Nothing Then
    '            lvTranHeadList.TopItem = lvFoundItem
    '            lvFoundItem.Selected = True
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtSearchTranHead_TextChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub txtSearchTranHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchTranHead.GotFocus
        Try
            With txtSearchTranHead
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchHeadText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchTranHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchTranHead.Leave
        Try
            If txtSearchTranHead.Text.Trim = "" Then
                Call SetDefaultSearchHeadText()
            End If
            'Call dgTransactionHead_SelectionChanged(dgTransactionHead, New System.EventArgs)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchTranHead_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchTranHead.TextChanged
        Try
            If txtSearchTranHead.Text.Trim = mstrSearchHeadText Then Exit Sub
            If dvTranHead IsNot Nothing Then
                'RemoveHandler dgTransactionHead.SelectionChanged, AddressOf dgTransactionHead_SelectionChanged
                dvTranHead.RowFilter = "code LIKE '%" & txtSearchTranHead.Text.Replace("'", "''") & "%'  OR name LIKE '%" & txtSearchTranHead.Text.Replace("'", "''") & "%'"
                dgTransactionHead.Refresh()
                'AddHandler dgTransactionHead.SelectionChanged, AddressOf dgTransactionHead_SelectionChanged
            End If
            'Sohail (26 Aug 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchTranHead_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (31 Jul 2017) -- End

    'SHANI (06 MAY 2015) -- End 

    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    Private Sub objbtnKeywords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywords.Click, objbtnKeywordsSN1.Click, objbtnKeywordsSN2.Click
        'Sohail (26 Mar 2020) - [objbtnKeywordsSN1, objbtnKeywordsSN2]
        Dim frm As New frmRemark
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 9, "Awailable Keywords")
            frm.Text = frm.objgbRemarks.Text
            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            'Dim strRemarks As String = getKeyWordsCompanyAccountJV()
            Dim strRemarks As String = ""
            If CType(sender, eZee.Common.eZeeGradientButton).Name = objbtnKeywords.Name Then
                strRemarks = getKeyWordsCompanyAccountJV()
            Else
                strRemarks = getKeyWordsCompanyAccountJV(False, False)
            End If
            'Sohail (26 Mar 2020) -- End

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, eZee.Common.eZeeGradientButton).Name & "_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Mar 2020) -- End

#End Region

    'Sohail (12 Oct 2011) -- Start
#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            CheckAllTransactionHead(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sohail (31 Jul 2017) -- Start
    'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
    '#Region "ListView Event"

    '    Private Sub lvTranHeadList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '        Try
    '            If lvTranHeadList.CheckedItems.Count <= 0 Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Unchecked
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            ElseIf lvTranHeadList.CheckedItems.Count < lvTranHeadList.Items.Count Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Indeterminate
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            ElseIf lvTranHeadList.CheckedItems.Count = lvTranHeadList.Items.Count Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Checked
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lvTranHeadList_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub

    '#End Region
#Region " GridView Events "

    Private Sub dgTransactionHead_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgTransactionHead.CurrentCellDirtyStateChanged
        Try
            If dgTransactionHead.IsCurrentCellDirty Then
                dgTransactionHead.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgTransactionHead_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgTransactionHead_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgTransactionHead.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()

                'Sohail (03 Jan 2019) -- Start
                'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                If dvTranHead Is Nothing AndAlso Not (CInt(cboTransactionType.SelectedValue) = enJVTransactionType.ADVANCE _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.CASH _
                    OrElse CInt(cboTransactionType.SelectedValue) = enJVTransactionType.COST_CENTER) _
                Then
                    Exit Try
                Else
                    If dvTranHead IsNot Nothing Then
                        If CInt(cboTransactionType.SelectedValue) = CInt(enJVTransactionType.TRANSACTION_HEAD) AndAlso CInt(dvTranHead.ToTable.Rows(e.RowIndex).Item("calctype_id")) = CInt(enCalcType.NET_PAY) Then

                            If mintCount = 1 Then
                                Call FillRefCodeName(True)
                            Else
                                Call FillRefCodeName(False)
                            End If
                        ElseIf CInt(cboTransactionType.SelectedValue) = CInt(enJVTransactionType.TRANSACTION_HEAD) Then
                            If mintCount = 1 AndAlso dvTranHead.Table.Select("IsChecked = 1 AND calctype_id = " & CInt(enCalcType.NET_PAY) & " ").Length = 1 Then
                                Call FillRefCodeName(True)
                            ElseIf mintCount = 2 AndAlso dvTranHead.Table.Select("IsChecked = 1 ").Length = 1 Then
                                Call FillRefCodeName(False)
                            End If
                        Else
                            Call FillRefCodeName(False)
                        End If
                    End If

                End If
                'Sohail (03 Jan 2019) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgTransactionHead_CellValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (31 Jul 2017) -- End

    'Sohail (12 Oct 2011) -- End

#Region " Message "
    '1, "Please select Head Type. Head Type is mandatory information."
    '2, "Please select Transaction Head. Transaction Head is mandatory information."
    '3, "Please select Account Group. Account Group is mandatory information."
    '4, "Please select Account. Account is mandatory information."
    '5, "Sorry! This Transaction Head is already mapped in another account configuration."
    '6, "Please select atleast one Transaction Head. Transaction is mandatory information."
    '7, "Please select Reference Code. Reference Code is mandatory information."
    '8, "Please select Reference Name. Reference Name is mandatory information."
#End Region

    

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbAccountConfigInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAccountConfigInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbTranHeadList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTranHeadList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbAccountConfigInfo.Text = Language._Object.getCaption(Me.gbAccountConfigInfo.Name, Me.gbAccountConfigInfo.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblAccount.Text = Language._Object.getCaption(Me.lblAccount.Name, Me.lblAccount.Text)
			Me.lblAccountGroup.Text = Language._Object.getCaption(Me.lblAccountGroup.Name, Me.lblAccountGroup.Text)
			Me.lblTransactionType.Text = Language._Object.getCaption(Me.lblTransactionType.Name, Me.lblTransactionType.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkIsExist.Text = Language._Object.getCaption(Me.chkIsExist.Name, Me.chkIsExist.Text)
			Me.lblMapRefCode.Text = Language._Object.getCaption(Me.lblMapRefCode.Name, Me.lblMapRefCode.Text)
			Me.lblMapRefName.Text = Language._Object.getCaption(Me.lblMapRefName.Name, Me.lblMapRefName.Text)
			Me.lblShortName.Text = Language._Object.getCaption(Me.lblShortName.Name, Me.lblShortName.Text)
			Me.lblSearchTranhead.Text = Language._Object.getCaption(Me.lblSearchTranhead.Name, Me.lblSearchTranhead.Text)
			Me.lblHeadType.Text = Language._Object.getCaption(Me.lblHeadType.Name, Me.lblHeadType.Text)
			Me.lblShortName2.Text = Language._Object.getCaption(Me.lblShortName2.Name, Me.lblShortName2.Text)
			Me.gbTranHeadList.Text = Language._Object.getCaption(Me.gbTranHeadList.Name, Me.gbTranHeadList.Text)
			Me.dgColhCode.HeaderText = Language._Object.getCaption(Me.dgColhCode.Name, Me.dgColhCode.HeaderText)
			Me.dgColhName.HeaderText = Language._Object.getCaption(Me.dgColhName.Name, Me.dgColhName.HeaderText)
			Me.lblShortName3.Text = Language._Object.getCaption(Me.lblShortName3.Name, Me.lblShortName3.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Head Type. Head Type is mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Please select Account Group. Account Group is mandatory information.")
			Language.setMessage(mstrModuleName, 3, "Please select Account. Account is mandatory information.")
			Language.setMessage(mstrModuleName, 4, "Please select Reference Code. Reference Code is mandatory information.")
			Language.setMessage(mstrModuleName, 5, "Please select Reference Name. Reference Name is mandatory information.")
			Language.setMessage(mstrModuleName, 6, "Please select atleast one Transaction Head. Transaction Head is mandatory information.")
			Language.setMessage(mstrModuleName, 7, "Sorry! This Transaction Head is already mapped in another account configuration.")
			Language.setMessage(mstrModuleName, 8, "Type to Search")
			Language.setMessage(mstrModuleName, 9, "Awailable Keywords")
			Language.setMessage(mstrModuleName, 10, "Sorry, Period is already closed.")
			Language.setMessage(mstrModuleName, 11, "Please select Period. Period is mandatory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class