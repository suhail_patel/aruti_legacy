﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCourseScheduling
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCourseScheduling))
        Me.pnlCourseScheduling = New System.Windows.Forms.Panel
        Me.tabcCourseScheduling = New System.Windows.Forms.TabControl
        Me.tabpCourseScheduling = New System.Windows.Forms.TabPage
        Me.gbCourseSchedule = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchCourse = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchInstitute = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchResult = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchQualification = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchQGrp = New eZee.Common.eZeeGradientButton
        Me.objbtnAddCourse = New eZee.Common.eZeeGradientButton
        Me.cboCourseMaster = New System.Windows.Forms.ComboBox
        Me.objbtnAddResultGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnAddInstitute = New eZee.Common.eZeeGradientButton
        Me.objbtnAddQualification = New eZee.Common.eZeeGradientButton
        Me.objbtnAddQGroup = New eZee.Common.eZeeGradientButton
        Me.lblTraningTitle = New System.Windows.Forms.Label
        Me.tabcRemark = New System.Windows.Forms.TabControl
        Me.tabpEligibilityInfo = New System.Windows.Forms.TabPage
        Me.txtEligibility = New eZee.TextBox.AlphanumericTextBox
        Me.tabpTrainingRemark = New System.Windows.Forms.TabPage
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.tabpCancellationRemark = New System.Windows.Forms.TabPage
        Me.txtCancelRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblQualificationAward = New System.Windows.Forms.Label
        Me.cboQualifiedAward = New System.Windows.Forms.ComboBox
        Me.lblVenue = New System.Windows.Forms.Label
        Me.txtVenue = New eZee.TextBox.AlphanumericTextBox
        Me.objstLine1 = New eZee.Common.eZeeStraightLine
        Me.cboResultGroup = New System.Windows.Forms.ComboBox
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.lblContactNo = New System.Windows.Forms.Label
        Me.lblContactPerson = New System.Windows.Forms.Label
        Me.nudAvailablePostion = New System.Windows.Forms.NumericUpDown
        Me.lblMaxCandidate = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpEndTime = New System.Windows.Forms.DateTimePicker
        Me.txtContactPerson = New eZee.TextBox.AlphanumericTextBox
        Me.cboInstitute = New System.Windows.Forms.ComboBox
        Me.txtContactNo = New eZee.TextBox.AlphanumericTextBox
        Me.dtpStartTime = New System.Windows.Forms.DateTimePicker
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.cboTraningYear = New System.Windows.Forms.ComboBox
        Me.cboQualification = New System.Windows.Forms.ComboBox
        Me.lblTraningInstitute = New System.Windows.Forms.Label
        Me.lblTraningCourse = New System.Windows.Forms.Label
        Me.lblTraningYear = New System.Windows.Forms.Label
        Me.tabpBudgets = New System.Windows.Forms.TabPage
        Me.gbBudgetsFundings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.elActuals = New eZee.Common.eZeeLine
        Me.elBudgets = New eZee.Common.eZeeLine
        Me.elSourceFundings = New eZee.Common.eZeeLine
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.lvSourceFunding = New eZee.Common.eZeeListView(Me.components)
        Me.colhFundingSource = New System.Windows.Forms.ColumnHeader
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.txtActualTraningFee = New eZee.TextBox.NumericTextBox
        Me.txtTraningFee = New eZee.TextBox.NumericTextBox
        Me.txtActualFinalAmount = New eZee.TextBox.NumericTextBox
        Me.lblActualAllowance = New System.Windows.Forms.Label
        Me.txtFinalAmount = New eZee.TextBox.NumericTextBox
        Me.lblAllowance = New System.Windows.Forms.Label
        Me.lblActualAmount = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.lblActualFees = New System.Windows.Forms.Label
        Me.lblFees = New System.Windows.Forms.Label
        Me.lblActualExam = New System.Windows.Forms.Label
        Me.lblExam = New System.Windows.Forms.Label
        Me.lblActualMisc = New System.Windows.Forms.Label
        Me.lblTraningMisc = New System.Windows.Forms.Label
        Me.txtActualExamAmount = New eZee.TextBox.NumericTextBox
        Me.lblActualTravel = New System.Windows.Forms.Label
        Me.txtExamAmount = New eZee.TextBox.NumericTextBox
        Me.lblTravel = New System.Windows.Forms.Label
        Me.txtActualMaterialAmount = New eZee.TextBox.NumericTextBox
        Me.txtActualMiscAmount = New eZee.TextBox.NumericTextBox
        Me.txtMaterialAmount = New eZee.TextBox.NumericTextBox
        Me.lblActualMaterialAmount = New System.Windows.Forms.Label
        Me.txtMiscAmount = New eZee.TextBox.NumericTextBox
        Me.lblMaterialAmount = New System.Windows.Forms.Label
        Me.txtActualTravelAmount = New eZee.TextBox.NumericTextBox
        Me.txtActualAllowanceAmount = New eZee.TextBox.NumericTextBox
        Me.txtTravelAmount = New eZee.TextBox.NumericTextBox
        Me.lblActualAccomodation = New System.Windows.Forms.Label
        Me.txtAllowanceAmount = New eZee.TextBox.NumericTextBox
        Me.lblAccomodation = New System.Windows.Forms.Label
        Me.txtActualMealAmount = New eZee.TextBox.NumericTextBox
        Me.txtActualAccomodationAmount = New eZee.TextBox.NumericTextBox
        Me.txtMealAmount = New eZee.TextBox.NumericTextBox
        Me.lblActualMeals = New System.Windows.Forms.Label
        Me.txtAccomodationAmount = New eZee.TextBox.NumericTextBox
        Me.lblMeals = New System.Windows.Forms.Label
        Me.tabpTrainingResources = New System.Windows.Forms.TabPage
        Me.gbResources = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddResources = New eZee.Common.eZeeGradientButton
        Me.pnlResources = New System.Windows.Forms.Panel
        Me.lvResources = New eZee.Common.eZeeListView(Me.components)
        Me.colhResource = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhResourceUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.btnAddResources = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDeleteResources = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.txtResourseRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblResourcesRemark = New System.Windows.Forms.Label
        Me.cboResources = New System.Windows.Forms.ComboBox
        Me.lblResources = New System.Windows.Forms.Label
        Me.tabpTrainerInfo = New System.Windows.Forms.TabPage
        Me.gbTrainerInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblLevelCaption = New System.Windows.Forms.Label
        Me.nudLevel = New System.Windows.Forms.NumericUpDown
        Me.lblTrainerLevel = New System.Windows.Forms.Label
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.objStLine2 = New eZee.Common.eZeeStraightLine
        Me.btnAddTrainer = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvTrainerInfo = New eZee.Common.eZeeListView(Me.components)
        Me.colhLevel = New System.Windows.Forms.ColumnHeader
        Me.colhEmpName = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhContactNo = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhTrainerGUID = New System.Windows.Forms.ColumnHeader
        Me.btnEditTrainer = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDeleteTrainer = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlOthers = New System.Windows.Forms.Panel
        Me.lblTrainerContactNo = New System.Windows.Forms.Label
        Me.txtTrainersContactNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblPosition = New System.Windows.Forms.Label
        Me.txtComany = New eZee.TextBox.AlphanumericTextBox
        Me.txtPosition = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.radOthers = New System.Windows.Forms.RadioButton
        Me.objColon3 = New System.Windows.Forms.Label
        Me.objColon2 = New System.Windows.Forms.Label
        Me.objColon1 = New System.Windows.Forms.Label
        Me.lblEmpContact = New System.Windows.Forms.Label
        Me.lblEmpCompany = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.objlblEmployeeContactNo = New System.Windows.Forms.Label
        Me.objlblCompanyValue = New System.Windows.Forms.Label
        Me.objlblDepartmentValue = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnMakeActive = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnkBindAllocation = New System.Windows.Forms.LinkLabel
        Me.pnlCourseScheduling.SuspendLayout()
        Me.tabcCourseScheduling.SuspendLayout()
        Me.tabpCourseScheduling.SuspendLayout()
        Me.gbCourseSchedule.SuspendLayout()
        Me.tabcRemark.SuspendLayout()
        Me.tabpEligibilityInfo.SuspendLayout()
        Me.tabpTrainingRemark.SuspendLayout()
        Me.tabpCancellationRemark.SuspendLayout()
        CType(Me.nudAvailablePostion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpBudgets.SuspendLayout()
        Me.gbBudgetsFundings.SuspendLayout()
        Me.tabpTrainingResources.SuspendLayout()
        Me.gbResources.SuspendLayout()
        Me.pnlResources.SuspendLayout()
        Me.tabpTrainerInfo.SuspendLayout()
        Me.gbTrainerInfo.SuspendLayout()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOthers.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlCourseScheduling
        '
        Me.pnlCourseScheduling.Controls.Add(Me.tabcCourseScheduling)
        Me.pnlCourseScheduling.Controls.Add(Me.objFooter)
        Me.pnlCourseScheduling.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCourseScheduling.Location = New System.Drawing.Point(0, 0)
        Me.pnlCourseScheduling.Name = "pnlCourseScheduling"
        Me.pnlCourseScheduling.Size = New System.Drawing.Size(776, 451)
        Me.pnlCourseScheduling.TabIndex = 0
        '
        'tabcCourseScheduling
        '
        Me.tabcCourseScheduling.Controls.Add(Me.tabpCourseScheduling)
        Me.tabcCourseScheduling.Controls.Add(Me.tabpBudgets)
        Me.tabcCourseScheduling.Controls.Add(Me.tabpTrainingResources)
        Me.tabcCourseScheduling.Controls.Add(Me.tabpTrainerInfo)
        Me.tabcCourseScheduling.Location = New System.Drawing.Point(3, 64)
        Me.tabcCourseScheduling.Name = "tabcCourseScheduling"
        Me.tabcCourseScheduling.SelectedIndex = 0
        Me.tabcCourseScheduling.Size = New System.Drawing.Size(770, 331)
        Me.tabcCourseScheduling.TabIndex = 0
        '
        'tabpCourseScheduling
        '
        Me.tabpCourseScheduling.Controls.Add(Me.gbCourseSchedule)
        Me.tabpCourseScheduling.Location = New System.Drawing.Point(4, 22)
        Me.tabpCourseScheduling.Name = "tabpCourseScheduling"
        Me.tabpCourseScheduling.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpCourseScheduling.Size = New System.Drawing.Size(762, 305)
        Me.tabpCourseScheduling.TabIndex = 0
        Me.tabpCourseScheduling.Text = "Training Information"
        Me.tabpCourseScheduling.UseVisualStyleBackColor = True
        '
        'gbCourseSchedule
        '
        Me.gbCourseSchedule.BackColor = System.Drawing.Color.Transparent
        Me.gbCourseSchedule.BorderColor = System.Drawing.Color.Black
        Me.gbCourseSchedule.Checked = False
        Me.gbCourseSchedule.CollapseAllExceptThis = False
        Me.gbCourseSchedule.CollapsedHoverImage = Nothing
        Me.gbCourseSchedule.CollapsedNormalImage = Nothing
        Me.gbCourseSchedule.CollapsedPressedImage = Nothing
        Me.gbCourseSchedule.CollapseOnLoad = False
        Me.gbCourseSchedule.Controls.Add(Me.lnkBindAllocation)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnSearchCourse)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnSearchInstitute)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnSearchResult)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnSearchQualification)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnSearchQGrp)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnAddCourse)
        Me.gbCourseSchedule.Controls.Add(Me.cboCourseMaster)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnAddResultGroup)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnAddInstitute)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnAddQualification)
        Me.gbCourseSchedule.Controls.Add(Me.objbtnAddQGroup)
        Me.gbCourseSchedule.Controls.Add(Me.lblTraningTitle)
        Me.gbCourseSchedule.Controls.Add(Me.tabcRemark)
        Me.gbCourseSchedule.Controls.Add(Me.lblQualificationAward)
        Me.gbCourseSchedule.Controls.Add(Me.cboQualifiedAward)
        Me.gbCourseSchedule.Controls.Add(Me.lblVenue)
        Me.gbCourseSchedule.Controls.Add(Me.txtVenue)
        Me.gbCourseSchedule.Controls.Add(Me.objstLine1)
        Me.gbCourseSchedule.Controls.Add(Me.cboResultGroup)
        Me.gbCourseSchedule.Controls.Add(Me.lblResultGroup)
        Me.gbCourseSchedule.Controls.Add(Me.lblContactNo)
        Me.gbCourseSchedule.Controls.Add(Me.lblContactPerson)
        Me.gbCourseSchedule.Controls.Add(Me.nudAvailablePostion)
        Me.gbCourseSchedule.Controls.Add(Me.lblMaxCandidate)
        Me.gbCourseSchedule.Controls.Add(Me.lblEndDate)
        Me.gbCourseSchedule.Controls.Add(Me.lblStartDate)
        Me.gbCourseSchedule.Controls.Add(Me.dtpEndTime)
        Me.gbCourseSchedule.Controls.Add(Me.txtContactPerson)
        Me.gbCourseSchedule.Controls.Add(Me.cboInstitute)
        Me.gbCourseSchedule.Controls.Add(Me.txtContactNo)
        Me.gbCourseSchedule.Controls.Add(Me.dtpStartTime)
        Me.gbCourseSchedule.Controls.Add(Me.dtpEndDate)
        Me.gbCourseSchedule.Controls.Add(Me.dtpStartDate)
        Me.gbCourseSchedule.Controls.Add(Me.cboTraningYear)
        Me.gbCourseSchedule.Controls.Add(Me.cboQualification)
        Me.gbCourseSchedule.Controls.Add(Me.lblTraningInstitute)
        Me.gbCourseSchedule.Controls.Add(Me.lblTraningCourse)
        Me.gbCourseSchedule.Controls.Add(Me.lblTraningYear)
        Me.gbCourseSchedule.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbCourseSchedule.ExpandedHoverImage = Nothing
        Me.gbCourseSchedule.ExpandedNormalImage = Nothing
        Me.gbCourseSchedule.ExpandedPressedImage = Nothing
        Me.gbCourseSchedule.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCourseSchedule.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCourseSchedule.HeaderHeight = 25
        Me.gbCourseSchedule.HeaderMessage = ""
        Me.gbCourseSchedule.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCourseSchedule.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCourseSchedule.HeightOnCollapse = 0
        Me.gbCourseSchedule.LeftTextSpace = 0
        Me.gbCourseSchedule.Location = New System.Drawing.Point(3, 3)
        Me.gbCourseSchedule.Name = "gbCourseSchedule"
        Me.gbCourseSchedule.OpenHeight = 283
        Me.gbCourseSchedule.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCourseSchedule.ShowBorder = True
        Me.gbCourseSchedule.ShowCheckBox = False
        Me.gbCourseSchedule.ShowCollapseButton = False
        Me.gbCourseSchedule.ShowDefaultBorderColor = True
        Me.gbCourseSchedule.ShowDownButton = False
        Me.gbCourseSchedule.ShowHeader = True
        Me.gbCourseSchedule.Size = New System.Drawing.Size(756, 299)
        Me.gbCourseSchedule.TabIndex = 0
        Me.gbCourseSchedule.Temp = 0
        Me.gbCourseSchedule.Text = "Training Info"
        Me.gbCourseSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCourse
        '
        Me.objbtnSearchCourse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCourse.BorderSelected = False
        Me.objbtnSearchCourse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCourse.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCourse.Location = New System.Drawing.Point(482, 32)
        Me.objbtnSearchCourse.Name = "objbtnSearchCourse"
        Me.objbtnSearchCourse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCourse.TabIndex = 3
        '
        'objbtnSearchInstitute
        '
        Me.objbtnSearchInstitute.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitute.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitute.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitute.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInstitute.BorderSelected = False
        Me.objbtnSearchInstitute.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInstitute.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInstitute.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInstitute.Location = New System.Drawing.Point(482, 167)
        Me.objbtnSearchInstitute.Name = "objbtnSearchInstitute"
        Me.objbtnSearchInstitute.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchInstitute.TabIndex = 23
        '
        'objbtnSearchResult
        '
        Me.objbtnSearchResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResult.BorderSelected = False
        Me.objbtnSearchResult.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResult.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResult.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResult.Location = New System.Drawing.Point(482, 140)
        Me.objbtnSearchResult.Name = "objbtnSearchResult"
        Me.objbtnSearchResult.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResult.TabIndex = 19
        '
        'objbtnSearchQualification
        '
        Me.objbtnSearchQualification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQualification.BorderSelected = False
        Me.objbtnSearchQualification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQualification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQualification.Location = New System.Drawing.Point(482, 113)
        Me.objbtnSearchQualification.Name = "objbtnSearchQualification"
        Me.objbtnSearchQualification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQualification.TabIndex = 15
        '
        'objbtnSearchQGrp
        '
        Me.objbtnSearchQGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQGrp.BorderSelected = False
        Me.objbtnSearchQGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQGrp.Location = New System.Drawing.Point(482, 86)
        Me.objbtnSearchQGrp.Name = "objbtnSearchQGrp"
        Me.objbtnSearchQGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQGrp.TabIndex = 11
        '
        'objbtnAddCourse
        '
        Me.objbtnAddCourse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCourse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCourse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCourse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCourse.BorderSelected = False
        Me.objbtnAddCourse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCourse.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCourse.Location = New System.Drawing.Point(455, 32)
        Me.objbtnAddCourse.Name = "objbtnAddCourse"
        Me.objbtnAddCourse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCourse.TabIndex = 2
        '
        'cboCourseMaster
        '
        Me.cboCourseMaster.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourseMaster.DropDownWidth = 360
        Me.cboCourseMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourseMaster.FormattingEnabled = True
        Me.cboCourseMaster.Location = New System.Drawing.Point(118, 32)
        Me.cboCourseMaster.Name = "cboCourseMaster"
        Me.cboCourseMaster.Size = New System.Drawing.Size(331, 21)
        Me.cboCourseMaster.TabIndex = 1
        '
        'objbtnAddResultGroup
        '
        Me.objbtnAddResultGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResultGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResultGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResultGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResultGroup.BorderSelected = False
        Me.objbtnAddResultGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResultGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResultGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResultGroup.Location = New System.Drawing.Point(455, 140)
        Me.objbtnAddResultGroup.Name = "objbtnAddResultGroup"
        Me.objbtnAddResultGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResultGroup.TabIndex = 18
        '
        'objbtnAddInstitute
        '
        Me.objbtnAddInstitute.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddInstitute.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddInstitute.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddInstitute.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddInstitute.BorderSelected = False
        Me.objbtnAddInstitute.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddInstitute.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddInstitute.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddInstitute.Location = New System.Drawing.Point(455, 167)
        Me.objbtnAddInstitute.Name = "objbtnAddInstitute"
        Me.objbtnAddInstitute.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddInstitute.TabIndex = 22
        '
        'objbtnAddQualification
        '
        Me.objbtnAddQualification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddQualification.BorderSelected = False
        Me.objbtnAddQualification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddQualification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddQualification.Location = New System.Drawing.Point(455, 113)
        Me.objbtnAddQualification.Name = "objbtnAddQualification"
        Me.objbtnAddQualification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddQualification.TabIndex = 14
        '
        'objbtnAddQGroup
        '
        Me.objbtnAddQGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddQGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddQGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddQGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddQGroup.BorderSelected = False
        Me.objbtnAddQGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddQGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddQGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddQGroup.Location = New System.Drawing.Point(455, 86)
        Me.objbtnAddQGroup.Name = "objbtnAddQGroup"
        Me.objbtnAddQGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddQGroup.TabIndex = 10
        '
        'lblTraningTitle
        '
        Me.lblTraningTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningTitle.Location = New System.Drawing.Point(8, 35)
        Me.lblTraningTitle.Name = "lblTraningTitle"
        Me.lblTraningTitle.Size = New System.Drawing.Size(104, 15)
        Me.lblTraningTitle.TabIndex = 0
        Me.lblTraningTitle.Text = "Course"
        Me.lblTraningTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcRemark
        '
        Me.tabcRemark.Controls.Add(Me.tabpEligibilityInfo)
        Me.tabcRemark.Controls.Add(Me.tabpTrainingRemark)
        Me.tabcRemark.Controls.Add(Me.tabpCancellationRemark)
        Me.tabcRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcRemark.Location = New System.Drawing.Point(521, 89)
        Me.tabcRemark.Name = "tabcRemark"
        Me.tabcRemark.SelectedIndex = 0
        Me.tabcRemark.Size = New System.Drawing.Size(232, 203)
        Me.tabcRemark.TabIndex = 37
        '
        'tabpEligibilityInfo
        '
        Me.tabpEligibilityInfo.Controls.Add(Me.txtEligibility)
        Me.tabpEligibilityInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpEligibilityInfo.Name = "tabpEligibilityInfo"
        Me.tabpEligibilityInfo.Size = New System.Drawing.Size(224, 177)
        Me.tabpEligibilityInfo.TabIndex = 0
        Me.tabpEligibilityInfo.Text = "Eligibility Info"
        Me.tabpEligibilityInfo.UseVisualStyleBackColor = True
        '
        'txtEligibility
        '
        Me.txtEligibility.BackColor = System.Drawing.Color.White
        Me.txtEligibility.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEligibility.Flags = 0
        Me.txtEligibility.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEligibility.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEligibility.Location = New System.Drawing.Point(0, 0)
        Me.txtEligibility.Multiline = True
        Me.txtEligibility.Name = "txtEligibility"
        Me.txtEligibility.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEligibility.Size = New System.Drawing.Size(224, 177)
        Me.txtEligibility.TabIndex = 0
        '
        'tabpTrainingRemark
        '
        Me.tabpTrainingRemark.Controls.Add(Me.txtRemark)
        Me.tabpTrainingRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpTrainingRemark.Name = "tabpTrainingRemark"
        Me.tabpTrainingRemark.Size = New System.Drawing.Size(224, 177)
        Me.tabpTrainingRemark.TabIndex = 1
        Me.tabpTrainingRemark.Text = "Training Remark"
        Me.tabpTrainingRemark.UseVisualStyleBackColor = True
        '
        'txtRemark
        '
        Me.txtRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(224, 177)
        Me.txtRemark.TabIndex = 155
        '
        'tabpCancellationRemark
        '
        Me.tabpCancellationRemark.Controls.Add(Me.txtCancelRemark)
        Me.tabpCancellationRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpCancellationRemark.Name = "tabpCancellationRemark"
        Me.tabpCancellationRemark.Size = New System.Drawing.Size(224, 177)
        Me.tabpCancellationRemark.TabIndex = 2
        Me.tabpCancellationRemark.Text = "Cancellation Remark"
        Me.tabpCancellationRemark.UseVisualStyleBackColor = True
        '
        'txtCancelRemark
        '
        Me.txtCancelRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtCancelRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCancelRemark.Flags = 0
        Me.txtCancelRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCancelRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCancelRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtCancelRemark.Multiline = True
        Me.txtCancelRemark.Name = "txtCancelRemark"
        Me.txtCancelRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCancelRemark.Size = New System.Drawing.Size(224, 177)
        Me.txtCancelRemark.TabIndex = 156
        '
        'lblQualificationAward
        '
        Me.lblQualificationAward.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationAward.Location = New System.Drawing.Point(8, 116)
        Me.lblQualificationAward.Name = "lblQualificationAward"
        Me.lblQualificationAward.Size = New System.Drawing.Size(104, 15)
        Me.lblQualificationAward.TabIndex = 12
        Me.lblQualificationAward.Text = "Qualification"
        Me.lblQualificationAward.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQualifiedAward
        '
        Me.cboQualifiedAward.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualifiedAward.DropDownWidth = 120
        Me.cboQualifiedAward.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualifiedAward.FormattingEnabled = True
        Me.cboQualifiedAward.Location = New System.Drawing.Point(118, 113)
        Me.cboQualifiedAward.Name = "cboQualifiedAward"
        Me.cboQualifiedAward.Size = New System.Drawing.Size(331, 21)
        Me.cboQualifiedAward.TabIndex = 13
        '
        'lblVenue
        '
        Me.lblVenue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVenue.Location = New System.Drawing.Point(8, 224)
        Me.lblVenue.Name = "lblVenue"
        Me.lblVenue.Size = New System.Drawing.Size(104, 15)
        Me.lblVenue.TabIndex = 28
        Me.lblVenue.Text = "Venue"
        Me.lblVenue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVenue
        '
        Me.txtVenue.Flags = 0
        Me.txtVenue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVenue.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVenue.Location = New System.Drawing.Point(118, 221)
        Me.txtVenue.Multiline = True
        Me.txtVenue.Name = "txtVenue"
        Me.txtVenue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtVenue.Size = New System.Drawing.Size(331, 71)
        Me.txtVenue.TabIndex = 29
        '
        'objstLine1
        '
        Me.objstLine1.BackColor = System.Drawing.Color.Transparent
        Me.objstLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstLine1.Location = New System.Drawing.Point(509, 32)
        Me.objstLine1.Name = "objstLine1"
        Me.objstLine1.Size = New System.Drawing.Size(6, 260)
        Me.objstLine1.TabIndex = 30
        Me.objstLine1.Text = "EZeeStraightLine1"
        '
        'cboResultGroup
        '
        Me.cboResultGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGroup.DropDownWidth = 120
        Me.cboResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGroup.FormattingEnabled = True
        Me.cboResultGroup.Location = New System.Drawing.Point(118, 140)
        Me.cboResultGroup.Name = "cboResultGroup"
        Me.cboResultGroup.Size = New System.Drawing.Size(331, 21)
        Me.cboResultGroup.TabIndex = 17
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(8, 143)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(104, 15)
        Me.lblResultGroup.TabIndex = 16
        Me.lblResultGroup.Text = "Result Group"
        Me.lblResultGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContactNo
        '
        Me.lblContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNo.Location = New System.Drawing.Point(250, 197)
        Me.lblContactNo.Name = "lblContactNo"
        Me.lblContactNo.Size = New System.Drawing.Size(67, 15)
        Me.lblContactNo.TabIndex = 26
        Me.lblContactNo.Text = "Tel. No."
        Me.lblContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContactPerson
        '
        Me.lblContactPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactPerson.Location = New System.Drawing.Point(8, 197)
        Me.lblContactPerson.Name = "lblContactPerson"
        Me.lblContactPerson.Size = New System.Drawing.Size(104, 15)
        Me.lblContactPerson.TabIndex = 24
        Me.lblContactPerson.Text = "Contact Person"
        Me.lblContactPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudAvailablePostion
        '
        Me.nudAvailablePostion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAvailablePostion.Location = New System.Drawing.Point(383, 59)
        Me.nudAvailablePostion.Name = "nudAvailablePostion"
        Me.nudAvailablePostion.Size = New System.Drawing.Size(66, 21)
        Me.nudAvailablePostion.TabIndex = 7
        '
        'lblMaxCandidate
        '
        Me.lblMaxCandidate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxCandidate.Location = New System.Drawing.Point(281, 62)
        Me.lblMaxCandidate.Name = "lblMaxCandidate"
        Me.lblMaxCandidate.Size = New System.Drawing.Size(96, 15)
        Me.lblMaxCandidate.TabIndex = 6
        Me.lblMaxCandidate.Text = "Participants"
        Me.lblMaxCandidate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(527, 60)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(70, 15)
        Me.lblEndDate.TabIndex = 34
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(527, 32)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(70, 15)
        Me.lblStartDate.TabIndex = 31
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndTime
        '
        Me.dtpEndTime.Checked = False
        Me.dtpEndTime.CustomFormat = "HH:mm"
        Me.dtpEndTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEndTime.Location = New System.Drawing.Point(695, 56)
        Me.dtpEndTime.Name = "dtpEndTime"
        Me.dtpEndTime.ShowUpDown = True
        Me.dtpEndTime.Size = New System.Drawing.Size(54, 21)
        Me.dtpEndTime.TabIndex = 36
        '
        'txtContactPerson
        '
        Me.txtContactPerson.Flags = 0
        Me.txtContactPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContactPerson.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtContactPerson.Location = New System.Drawing.Point(118, 194)
        Me.txtContactPerson.Name = "txtContactPerson"
        Me.txtContactPerson.Size = New System.Drawing.Size(126, 21)
        Me.txtContactPerson.TabIndex = 25
        '
        'cboInstitute
        '
        Me.cboInstitute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstitute.DropDownWidth = 120
        Me.cboInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstitute.FormattingEnabled = True
        Me.cboInstitute.Location = New System.Drawing.Point(118, 167)
        Me.cboInstitute.Name = "cboInstitute"
        Me.cboInstitute.Size = New System.Drawing.Size(331, 21)
        Me.cboInstitute.TabIndex = 21
        '
        'txtContactNo
        '
        Me.txtContactNo.Flags = 0
        Me.txtContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContactNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtContactNo.Location = New System.Drawing.Point(323, 194)
        Me.txtContactNo.Name = "txtContactNo"
        Me.txtContactNo.Size = New System.Drawing.Size(126, 21)
        Me.txtContactNo.TabIndex = 27
        '
        'dtpStartTime
        '
        Me.dtpStartTime.Checked = False
        Me.dtpStartTime.CustomFormat = "HH:mm"
        Me.dtpStartTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartTime.Location = New System.Drawing.Point(695, 29)
        Me.dtpStartTime.Name = "dtpStartTime"
        Me.dtpStartTime.ShowUpDown = True
        Me.dtpStartTime.Size = New System.Drawing.Size(54, 21)
        Me.dtpStartTime.TabIndex = 33
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(603, 57)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(86, 21)
        Me.dtpEndDate.TabIndex = 35
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(603, 29)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(86, 21)
        Me.dtpStartDate.TabIndex = 32
        '
        'cboTraningYear
        '
        Me.cboTraningYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTraningYear.DropDownWidth = 120
        Me.cboTraningYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTraningYear.FormattingEnabled = True
        Me.cboTraningYear.Location = New System.Drawing.Point(118, 59)
        Me.cboTraningYear.Name = "cboTraningYear"
        Me.cboTraningYear.Size = New System.Drawing.Size(157, 21)
        Me.cboTraningYear.TabIndex = 5
        '
        'cboQualification
        '
        Me.cboQualification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualification.DropDownWidth = 120
        Me.cboQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualification.FormattingEnabled = True
        Me.cboQualification.Location = New System.Drawing.Point(118, 86)
        Me.cboQualification.Name = "cboQualification"
        Me.cboQualification.Size = New System.Drawing.Size(331, 21)
        Me.cboQualification.TabIndex = 9
        '
        'lblTraningInstitute
        '
        Me.lblTraningInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningInstitute.Location = New System.Drawing.Point(8, 170)
        Me.lblTraningInstitute.Name = "lblTraningInstitute"
        Me.lblTraningInstitute.Size = New System.Drawing.Size(104, 15)
        Me.lblTraningInstitute.TabIndex = 20
        Me.lblTraningInstitute.Text = "Training Provider"
        Me.lblTraningInstitute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTraningCourse
        '
        Me.lblTraningCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningCourse.Location = New System.Drawing.Point(8, 89)
        Me.lblTraningCourse.Name = "lblTraningCourse"
        Me.lblTraningCourse.Size = New System.Drawing.Size(104, 15)
        Me.lblTraningCourse.TabIndex = 8
        Me.lblTraningCourse.Text = "Qualification Group"
        Me.lblTraningCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTraningYear
        '
        Me.lblTraningYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningYear.Location = New System.Drawing.Point(8, 62)
        Me.lblTraningYear.Name = "lblTraningYear"
        Me.lblTraningYear.Size = New System.Drawing.Size(104, 15)
        Me.lblTraningYear.TabIndex = 4
        Me.lblTraningYear.Text = "Training Year"
        Me.lblTraningYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpBudgets
        '
        Me.tabpBudgets.Controls.Add(Me.gbBudgetsFundings)
        Me.tabpBudgets.Location = New System.Drawing.Point(4, 22)
        Me.tabpBudgets.Name = "tabpBudgets"
        Me.tabpBudgets.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpBudgets.Size = New System.Drawing.Size(762, 305)
        Me.tabpBudgets.TabIndex = 3
        Me.tabpBudgets.Tag = "tabpBudgets"
        Me.tabpBudgets.Text = "Budgets && Fundings"
        Me.tabpBudgets.UseVisualStyleBackColor = True
        '
        'gbBudgetsFundings
        '
        Me.gbBudgetsFundings.BorderColor = System.Drawing.Color.Black
        Me.gbBudgetsFundings.Checked = False
        Me.gbBudgetsFundings.CollapseAllExceptThis = False
        Me.gbBudgetsFundings.CollapsedHoverImage = Nothing
        Me.gbBudgetsFundings.CollapsedNormalImage = Nothing
        Me.gbBudgetsFundings.CollapsedPressedImage = Nothing
        Me.gbBudgetsFundings.CollapseOnLoad = False
        Me.gbBudgetsFundings.Controls.Add(Me.elActuals)
        Me.gbBudgetsFundings.Controls.Add(Me.elBudgets)
        Me.gbBudgetsFundings.Controls.Add(Me.elSourceFundings)
        Me.gbBudgetsFundings.Controls.Add(Me.EZeeStraightLine1)
        Me.gbBudgetsFundings.Controls.Add(Me.lvSourceFunding)
        Me.gbBudgetsFundings.Controls.Add(Me.EZeeStraightLine2)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualTraningFee)
        Me.gbBudgetsFundings.Controls.Add(Me.txtTraningFee)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualFinalAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualAllowance)
        Me.gbBudgetsFundings.Controls.Add(Me.txtFinalAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblAllowance)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualFees)
        Me.gbBudgetsFundings.Controls.Add(Me.lblFees)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualExam)
        Me.gbBudgetsFundings.Controls.Add(Me.lblExam)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualMisc)
        Me.gbBudgetsFundings.Controls.Add(Me.lblTraningMisc)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualExamAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualTravel)
        Me.gbBudgetsFundings.Controls.Add(Me.txtExamAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblTravel)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualMaterialAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualMiscAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.txtMaterialAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualMaterialAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.txtMiscAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblMaterialAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualTravelAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualAllowanceAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.txtTravelAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualAccomodation)
        Me.gbBudgetsFundings.Controls.Add(Me.txtAllowanceAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblAccomodation)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualMealAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.txtActualAccomodationAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.txtMealAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblActualMeals)
        Me.gbBudgetsFundings.Controls.Add(Me.txtAccomodationAmount)
        Me.gbBudgetsFundings.Controls.Add(Me.lblMeals)
        Me.gbBudgetsFundings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbBudgetsFundings.ExpandedHoverImage = Nothing
        Me.gbBudgetsFundings.ExpandedNormalImage = Nothing
        Me.gbBudgetsFundings.ExpandedPressedImage = Nothing
        Me.gbBudgetsFundings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBudgetsFundings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBudgetsFundings.HeaderHeight = 25
        Me.gbBudgetsFundings.HeaderMessage = ""
        Me.gbBudgetsFundings.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbBudgetsFundings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBudgetsFundings.HeightOnCollapse = 0
        Me.gbBudgetsFundings.LeftTextSpace = 0
        Me.gbBudgetsFundings.Location = New System.Drawing.Point(3, 3)
        Me.gbBudgetsFundings.Name = "gbBudgetsFundings"
        Me.gbBudgetsFundings.OpenHeight = 300
        Me.gbBudgetsFundings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBudgetsFundings.ShowBorder = True
        Me.gbBudgetsFundings.ShowCheckBox = False
        Me.gbBudgetsFundings.ShowCollapseButton = False
        Me.gbBudgetsFundings.ShowDefaultBorderColor = True
        Me.gbBudgetsFundings.ShowDownButton = False
        Me.gbBudgetsFundings.ShowHeader = True
        Me.gbBudgetsFundings.Size = New System.Drawing.Size(756, 299)
        Me.gbBudgetsFundings.TabIndex = 0
        Me.gbBudgetsFundings.Temp = 0
        Me.gbBudgetsFundings.Text = "Budgets  && Fundings"
        Me.gbBudgetsFundings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elActuals
        '
        Me.elActuals.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elActuals.Location = New System.Drawing.Point(504, 34)
        Me.elActuals.Name = "elActuals"
        Me.elActuals.Size = New System.Drawing.Size(241, 17)
        Me.elActuals.TabIndex = 23
        Me.elActuals.Text = "Actuals"
        '
        'elBudgets
        '
        Me.elBudgets.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elBudgets.Location = New System.Drawing.Point(241, 34)
        Me.elBudgets.Name = "elBudgets"
        Me.elBudgets.Size = New System.Drawing.Size(241, 17)
        Me.elBudgets.TabIndex = 3
        Me.elBudgets.Text = "Budgets"
        '
        'elSourceFundings
        '
        Me.elSourceFundings.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elSourceFundings.Location = New System.Drawing.Point(11, 34)
        Me.elSourceFundings.Name = "elSourceFundings"
        Me.elSourceFundings.Size = New System.Drawing.Size(212, 17)
        Me.elSourceFundings.TabIndex = 0
        Me.elSourceFundings.Text = "Source Fundings"
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(488, 34)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(8, 260)
        Me.EZeeStraightLine1.TabIndex = 22
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'lvSourceFunding
        '
        Me.lvSourceFunding.BackColorOnChecked = False
        Me.lvSourceFunding.CheckBoxes = True
        Me.lvSourceFunding.ColumnHeaders = Nothing
        Me.lvSourceFunding.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFundingSource})
        Me.lvSourceFunding.CompulsoryColumns = ""
        Me.lvSourceFunding.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvSourceFunding.FullRowSelect = True
        Me.lvSourceFunding.GridLines = True
        Me.lvSourceFunding.GroupingColumn = Nothing
        Me.lvSourceFunding.HideSelection = False
        Me.lvSourceFunding.Location = New System.Drawing.Point(10, 57)
        Me.lvSourceFunding.MinColumnWidth = 50
        Me.lvSourceFunding.MultiSelect = False
        Me.lvSourceFunding.Name = "lvSourceFunding"
        Me.lvSourceFunding.OptionalColumns = ""
        Me.lvSourceFunding.ShowMoreItem = False
        Me.lvSourceFunding.ShowSaveItem = False
        Me.lvSourceFunding.ShowSelectAll = True
        Me.lvSourceFunding.ShowSizeAllColumnsToFit = True
        Me.lvSourceFunding.Size = New System.Drawing.Size(212, 237)
        Me.lvSourceFunding.Sortable = True
        Me.lvSourceFunding.TabIndex = 1
        Me.lvSourceFunding.UseCompatibleStateImageBehavior = False
        Me.lvSourceFunding.View = System.Windows.Forms.View.Details
        '
        'colhFundingSource
        '
        Me.colhFundingSource.Tag = "colhFundingSource"
        Me.colhFundingSource.Text = "Funding Sources"
        Me.colhFundingSource.Width = 200
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(227, 34)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(8, 260)
        Me.EZeeStraightLine2.TabIndex = 2
        Me.EZeeStraightLine2.Text = "EZeeStraightLine1"
        '
        'txtActualTraningFee
        '
        Me.txtActualTraningFee.AllowNegative = False
        Me.txtActualTraningFee.BackColor = System.Drawing.SystemColors.Window
        Me.txtActualTraningFee.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualTraningFee.DigitsInGroup = 0
        Me.txtActualTraningFee.Flags = 65536
        Me.txtActualTraningFee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualTraningFee.Location = New System.Drawing.Point(586, 57)
        Me.txtActualTraningFee.MaxDecimalPlaces = 6
        Me.txtActualTraningFee.MaxWholeDigits = 21
        Me.txtActualTraningFee.Name = "txtActualTraningFee"
        Me.txtActualTraningFee.Prefix = ""
        Me.txtActualTraningFee.RangeMax = 1.7976931348623157E+308
        Me.txtActualTraningFee.RangeMin = -1.7976931348623157E+308
        Me.txtActualTraningFee.Size = New System.Drawing.Size(159, 21)
        Me.txtActualTraningFee.TabIndex = 25
        Me.txtActualTraningFee.Text = "0"
        Me.txtActualTraningFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTraningFee
        '
        Me.txtTraningFee.AllowNegative = False
        Me.txtTraningFee.BackColor = System.Drawing.SystemColors.Window
        Me.txtTraningFee.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTraningFee.DigitsInGroup = 0
        Me.txtTraningFee.Flags = 65536
        Me.txtTraningFee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTraningFee.Location = New System.Drawing.Point(323, 57)
        Me.txtTraningFee.MaxDecimalPlaces = 6
        Me.txtTraningFee.MaxWholeDigits = 21
        Me.txtTraningFee.Name = "txtTraningFee"
        Me.txtTraningFee.Prefix = ""
        Me.txtTraningFee.RangeMax = 1.7976931348623157E+308
        Me.txtTraningFee.RangeMin = -1.7976931348623157E+308
        Me.txtTraningFee.Size = New System.Drawing.Size(159, 21)
        Me.txtTraningFee.TabIndex = 5
        Me.txtTraningFee.Text = "0"
        Me.txtTraningFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtActualFinalAmount
        '
        Me.txtActualFinalAmount.AllowNegative = False
        Me.txtActualFinalAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualFinalAmount.DigitsInGroup = 0
        Me.txtActualFinalAmount.Flags = 65536
        Me.txtActualFinalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualFinalAmount.Location = New System.Drawing.Point(586, 273)
        Me.txtActualFinalAmount.MaxDecimalPlaces = 6
        Me.txtActualFinalAmount.MaxWholeDigits = 21
        Me.txtActualFinalAmount.Name = "txtActualFinalAmount"
        Me.txtActualFinalAmount.Prefix = ""
        Me.txtActualFinalAmount.RangeMax = 1.7976931348623157E+308
        Me.txtActualFinalAmount.RangeMin = -1.7976931348623157E+308
        Me.txtActualFinalAmount.ReadOnly = True
        Me.txtActualFinalAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtActualFinalAmount.TabIndex = 41
        Me.txtActualFinalAmount.Text = "0"
        Me.txtActualFinalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblActualAllowance
        '
        Me.lblActualAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualAllowance.Location = New System.Drawing.Point(504, 168)
        Me.lblActualAllowance.Name = "lblActualAllowance"
        Me.lblActualAllowance.Size = New System.Drawing.Size(76, 15)
        Me.lblActualAllowance.TabIndex = 32
        Me.lblActualAllowance.Text = "Allowance"
        Me.lblActualAllowance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFinalAmount
        '
        Me.txtFinalAmount.AllowNegative = False
        Me.txtFinalAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtFinalAmount.DigitsInGroup = 0
        Me.txtFinalAmount.Flags = 65536
        Me.txtFinalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinalAmount.Location = New System.Drawing.Point(323, 273)
        Me.txtFinalAmount.MaxDecimalPlaces = 6
        Me.txtFinalAmount.MaxWholeDigits = 21
        Me.txtFinalAmount.Name = "txtFinalAmount"
        Me.txtFinalAmount.Prefix = ""
        Me.txtFinalAmount.RangeMax = 1.7976931348623157E+308
        Me.txtFinalAmount.RangeMin = -1.7976931348623157E+308
        Me.txtFinalAmount.ReadOnly = True
        Me.txtFinalAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtFinalAmount.TabIndex = 21
        Me.txtFinalAmount.Text = "0"
        Me.txtFinalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAllowance
        '
        Me.lblAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllowance.Location = New System.Drawing.Point(241, 168)
        Me.lblAllowance.Name = "lblAllowance"
        Me.lblAllowance.Size = New System.Drawing.Size(76, 15)
        Me.lblAllowance.TabIndex = 12
        Me.lblAllowance.Text = "Allowance"
        Me.lblAllowance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblActualAmount
        '
        Me.lblActualAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualAmount.Location = New System.Drawing.Point(504, 276)
        Me.lblActualAmount.Name = "lblActualAmount"
        Me.lblActualAmount.Size = New System.Drawing.Size(76, 15)
        Me.lblActualAmount.TabIndex = 40
        Me.lblActualAmount.Text = "Amount"
        Me.lblActualAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(241, 276)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(76, 15)
        Me.lblAmount.TabIndex = 20
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblActualFees
        '
        Me.lblActualFees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualFees.Location = New System.Drawing.Point(504, 60)
        Me.lblActualFees.Name = "lblActualFees"
        Me.lblActualFees.Size = New System.Drawing.Size(76, 15)
        Me.lblActualFees.TabIndex = 24
        Me.lblActualFees.Text = "Fees"
        Me.lblActualFees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFees
        '
        Me.lblFees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFees.Location = New System.Drawing.Point(241, 60)
        Me.lblFees.Name = "lblFees"
        Me.lblFees.Size = New System.Drawing.Size(76, 15)
        Me.lblFees.TabIndex = 4
        Me.lblFees.Text = "Fees"
        Me.lblFees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblActualExam
        '
        Me.lblActualExam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualExam.Location = New System.Drawing.Point(504, 249)
        Me.lblActualExam.Name = "lblActualExam"
        Me.lblActualExam.Size = New System.Drawing.Size(76, 15)
        Me.lblActualExam.TabIndex = 38
        Me.lblActualExam.Text = "Exam"
        Me.lblActualExam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExam
        '
        Me.lblExam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExam.Location = New System.Drawing.Point(241, 249)
        Me.lblExam.Name = "lblExam"
        Me.lblExam.Size = New System.Drawing.Size(76, 15)
        Me.lblExam.TabIndex = 18
        Me.lblExam.Text = "Exam"
        Me.lblExam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblActualMisc
        '
        Me.lblActualMisc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualMisc.Location = New System.Drawing.Point(504, 87)
        Me.lblActualMisc.Name = "lblActualMisc"
        Me.lblActualMisc.Size = New System.Drawing.Size(76, 15)
        Me.lblActualMisc.TabIndex = 26
        Me.lblActualMisc.Text = "Misc"
        Me.lblActualMisc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTraningMisc
        '
        Me.lblTraningMisc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningMisc.Location = New System.Drawing.Point(241, 87)
        Me.lblTraningMisc.Name = "lblTraningMisc"
        Me.lblTraningMisc.Size = New System.Drawing.Size(76, 15)
        Me.lblTraningMisc.TabIndex = 6
        Me.lblTraningMisc.Text = "Misc"
        Me.lblTraningMisc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtActualExamAmount
        '
        Me.txtActualExamAmount.AllowNegative = False
        Me.txtActualExamAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualExamAmount.DigitsInGroup = 0
        Me.txtActualExamAmount.Flags = 65536
        Me.txtActualExamAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualExamAmount.Location = New System.Drawing.Point(586, 246)
        Me.txtActualExamAmount.MaxDecimalPlaces = 6
        Me.txtActualExamAmount.MaxWholeDigits = 21
        Me.txtActualExamAmount.Name = "txtActualExamAmount"
        Me.txtActualExamAmount.Prefix = ""
        Me.txtActualExamAmount.RangeMax = 1.7976931348623157E+308
        Me.txtActualExamAmount.RangeMin = -1.7976931348623157E+308
        Me.txtActualExamAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtActualExamAmount.TabIndex = 39
        Me.txtActualExamAmount.Text = "0"
        Me.txtActualExamAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblActualTravel
        '
        Me.lblActualTravel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualTravel.Location = New System.Drawing.Point(504, 114)
        Me.lblActualTravel.Name = "lblActualTravel"
        Me.lblActualTravel.Size = New System.Drawing.Size(76, 15)
        Me.lblActualTravel.TabIndex = 28
        Me.lblActualTravel.Text = "Travel"
        Me.lblActualTravel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExamAmount
        '
        Me.txtExamAmount.AllowNegative = False
        Me.txtExamAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtExamAmount.DigitsInGroup = 0
        Me.txtExamAmount.Flags = 65536
        Me.txtExamAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExamAmount.Location = New System.Drawing.Point(323, 246)
        Me.txtExamAmount.MaxDecimalPlaces = 6
        Me.txtExamAmount.MaxWholeDigits = 21
        Me.txtExamAmount.Name = "txtExamAmount"
        Me.txtExamAmount.Prefix = ""
        Me.txtExamAmount.RangeMax = 1.7976931348623157E+308
        Me.txtExamAmount.RangeMin = -1.7976931348623157E+308
        Me.txtExamAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtExamAmount.TabIndex = 19
        Me.txtExamAmount.Text = "0"
        Me.txtExamAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTravel
        '
        Me.lblTravel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTravel.Location = New System.Drawing.Point(241, 114)
        Me.lblTravel.Name = "lblTravel"
        Me.lblTravel.Size = New System.Drawing.Size(76, 15)
        Me.lblTravel.TabIndex = 8
        Me.lblTravel.Text = "Travel"
        Me.lblTravel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtActualMaterialAmount
        '
        Me.txtActualMaterialAmount.AllowNegative = False
        Me.txtActualMaterialAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualMaterialAmount.DigitsInGroup = 0
        Me.txtActualMaterialAmount.Flags = 65536
        Me.txtActualMaterialAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualMaterialAmount.Location = New System.Drawing.Point(586, 219)
        Me.txtActualMaterialAmount.MaxDecimalPlaces = 6
        Me.txtActualMaterialAmount.MaxWholeDigits = 21
        Me.txtActualMaterialAmount.Name = "txtActualMaterialAmount"
        Me.txtActualMaterialAmount.Prefix = ""
        Me.txtActualMaterialAmount.RangeMax = 1.7976931348623157E+308
        Me.txtActualMaterialAmount.RangeMin = -1.7976931348623157E+308
        Me.txtActualMaterialAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtActualMaterialAmount.TabIndex = 37
        Me.txtActualMaterialAmount.Text = "0"
        Me.txtActualMaterialAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtActualMiscAmount
        '
        Me.txtActualMiscAmount.AllowNegative = False
        Me.txtActualMiscAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualMiscAmount.DigitsInGroup = 0
        Me.txtActualMiscAmount.Flags = 65536
        Me.txtActualMiscAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualMiscAmount.Location = New System.Drawing.Point(586, 84)
        Me.txtActualMiscAmount.MaxDecimalPlaces = 6
        Me.txtActualMiscAmount.MaxWholeDigits = 21
        Me.txtActualMiscAmount.Name = "txtActualMiscAmount"
        Me.txtActualMiscAmount.Prefix = ""
        Me.txtActualMiscAmount.RangeMax = 1.7976931348623157E+308
        Me.txtActualMiscAmount.RangeMin = -1.7976931348623157E+308
        Me.txtActualMiscAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtActualMiscAmount.TabIndex = 27
        Me.txtActualMiscAmount.Text = "0"
        Me.txtActualMiscAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMaterialAmount
        '
        Me.txtMaterialAmount.AllowNegative = False
        Me.txtMaterialAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaterialAmount.DigitsInGroup = 0
        Me.txtMaterialAmount.Flags = 65536
        Me.txtMaterialAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaterialAmount.Location = New System.Drawing.Point(323, 219)
        Me.txtMaterialAmount.MaxDecimalPlaces = 6
        Me.txtMaterialAmount.MaxWholeDigits = 21
        Me.txtMaterialAmount.Name = "txtMaterialAmount"
        Me.txtMaterialAmount.Prefix = ""
        Me.txtMaterialAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMaterialAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMaterialAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtMaterialAmount.TabIndex = 17
        Me.txtMaterialAmount.Text = "0"
        Me.txtMaterialAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblActualMaterialAmount
        '
        Me.lblActualMaterialAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualMaterialAmount.Location = New System.Drawing.Point(504, 222)
        Me.lblActualMaterialAmount.Name = "lblActualMaterialAmount"
        Me.lblActualMaterialAmount.Size = New System.Drawing.Size(76, 15)
        Me.lblActualMaterialAmount.TabIndex = 36
        Me.lblActualMaterialAmount.Text = "Material"
        Me.lblActualMaterialAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMiscAmount
        '
        Me.txtMiscAmount.AllowNegative = False
        Me.txtMiscAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMiscAmount.DigitsInGroup = 0
        Me.txtMiscAmount.Flags = 65536
        Me.txtMiscAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMiscAmount.Location = New System.Drawing.Point(323, 84)
        Me.txtMiscAmount.MaxDecimalPlaces = 6
        Me.txtMiscAmount.MaxWholeDigits = 21
        Me.txtMiscAmount.Name = "txtMiscAmount"
        Me.txtMiscAmount.Prefix = ""
        Me.txtMiscAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMiscAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMiscAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtMiscAmount.TabIndex = 7
        Me.txtMiscAmount.Text = "0"
        Me.txtMiscAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaterialAmount
        '
        Me.lblMaterialAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaterialAmount.Location = New System.Drawing.Point(241, 222)
        Me.lblMaterialAmount.Name = "lblMaterialAmount"
        Me.lblMaterialAmount.Size = New System.Drawing.Size(76, 15)
        Me.lblMaterialAmount.TabIndex = 16
        Me.lblMaterialAmount.Text = "Material"
        Me.lblMaterialAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtActualTravelAmount
        '
        Me.txtActualTravelAmount.AllowNegative = False
        Me.txtActualTravelAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualTravelAmount.DigitsInGroup = 0
        Me.txtActualTravelAmount.Flags = 65536
        Me.txtActualTravelAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualTravelAmount.Location = New System.Drawing.Point(586, 111)
        Me.txtActualTravelAmount.MaxDecimalPlaces = 6
        Me.txtActualTravelAmount.MaxWholeDigits = 21
        Me.txtActualTravelAmount.Name = "txtActualTravelAmount"
        Me.txtActualTravelAmount.Prefix = ""
        Me.txtActualTravelAmount.RangeMax = 1.7976931348623157E+308
        Me.txtActualTravelAmount.RangeMin = -1.7976931348623157E+308
        Me.txtActualTravelAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtActualTravelAmount.TabIndex = 29
        Me.txtActualTravelAmount.Text = "0"
        Me.txtActualTravelAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtActualAllowanceAmount
        '
        Me.txtActualAllowanceAmount.AllowNegative = False
        Me.txtActualAllowanceAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualAllowanceAmount.DigitsInGroup = 0
        Me.txtActualAllowanceAmount.Flags = 65536
        Me.txtActualAllowanceAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualAllowanceAmount.Location = New System.Drawing.Point(586, 165)
        Me.txtActualAllowanceAmount.MaxDecimalPlaces = 6
        Me.txtActualAllowanceAmount.MaxWholeDigits = 21
        Me.txtActualAllowanceAmount.Name = "txtActualAllowanceAmount"
        Me.txtActualAllowanceAmount.Prefix = ""
        Me.txtActualAllowanceAmount.RangeMax = 1.7976931348623157E+308
        Me.txtActualAllowanceAmount.RangeMin = -1.7976931348623157E+308
        Me.txtActualAllowanceAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtActualAllowanceAmount.TabIndex = 33
        Me.txtActualAllowanceAmount.Text = "0"
        Me.txtActualAllowanceAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTravelAmount
        '
        Me.txtTravelAmount.AllowNegative = False
        Me.txtTravelAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTravelAmount.DigitsInGroup = 0
        Me.txtTravelAmount.Flags = 65536
        Me.txtTravelAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTravelAmount.Location = New System.Drawing.Point(323, 111)
        Me.txtTravelAmount.MaxDecimalPlaces = 6
        Me.txtTravelAmount.MaxWholeDigits = 21
        Me.txtTravelAmount.Name = "txtTravelAmount"
        Me.txtTravelAmount.Prefix = ""
        Me.txtTravelAmount.RangeMax = 1.7976931348623157E+308
        Me.txtTravelAmount.RangeMin = -1.7976931348623157E+308
        Me.txtTravelAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtTravelAmount.TabIndex = 9
        Me.txtTravelAmount.Text = "0"
        Me.txtTravelAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblActualAccomodation
        '
        Me.lblActualAccomodation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualAccomodation.Location = New System.Drawing.Point(504, 141)
        Me.lblActualAccomodation.Name = "lblActualAccomodation"
        Me.lblActualAccomodation.Size = New System.Drawing.Size(76, 15)
        Me.lblActualAccomodation.TabIndex = 30
        Me.lblActualAccomodation.Text = "Accomodation"
        Me.lblActualAccomodation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAllowanceAmount
        '
        Me.txtAllowanceAmount.AllowNegative = False
        Me.txtAllowanceAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAllowanceAmount.DigitsInGroup = 0
        Me.txtAllowanceAmount.Flags = 65536
        Me.txtAllowanceAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAllowanceAmount.Location = New System.Drawing.Point(323, 165)
        Me.txtAllowanceAmount.MaxDecimalPlaces = 6
        Me.txtAllowanceAmount.MaxWholeDigits = 21
        Me.txtAllowanceAmount.Name = "txtAllowanceAmount"
        Me.txtAllowanceAmount.Prefix = ""
        Me.txtAllowanceAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAllowanceAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAllowanceAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtAllowanceAmount.TabIndex = 13
        Me.txtAllowanceAmount.Text = "0"
        Me.txtAllowanceAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAccomodation
        '
        Me.lblAccomodation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccomodation.Location = New System.Drawing.Point(241, 141)
        Me.lblAccomodation.Name = "lblAccomodation"
        Me.lblAccomodation.Size = New System.Drawing.Size(76, 15)
        Me.lblAccomodation.TabIndex = 10
        Me.lblAccomodation.Text = "Accomodation"
        Me.lblAccomodation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtActualMealAmount
        '
        Me.txtActualMealAmount.AllowNegative = False
        Me.txtActualMealAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualMealAmount.DigitsInGroup = 0
        Me.txtActualMealAmount.Flags = 65536
        Me.txtActualMealAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualMealAmount.Location = New System.Drawing.Point(586, 192)
        Me.txtActualMealAmount.MaxDecimalPlaces = 6
        Me.txtActualMealAmount.MaxWholeDigits = 21
        Me.txtActualMealAmount.Name = "txtActualMealAmount"
        Me.txtActualMealAmount.Prefix = ""
        Me.txtActualMealAmount.RangeMax = 1.7976931348623157E+308
        Me.txtActualMealAmount.RangeMin = -1.7976931348623157E+308
        Me.txtActualMealAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtActualMealAmount.TabIndex = 35
        Me.txtActualMealAmount.Text = "0"
        Me.txtActualMealAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtActualAccomodationAmount
        '
        Me.txtActualAccomodationAmount.AllowNegative = False
        Me.txtActualAccomodationAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtActualAccomodationAmount.DigitsInGroup = 0
        Me.txtActualAccomodationAmount.Flags = 65536
        Me.txtActualAccomodationAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualAccomodationAmount.Location = New System.Drawing.Point(586, 138)
        Me.txtActualAccomodationAmount.MaxDecimalPlaces = 6
        Me.txtActualAccomodationAmount.MaxWholeDigits = 21
        Me.txtActualAccomodationAmount.Name = "txtActualAccomodationAmount"
        Me.txtActualAccomodationAmount.Prefix = ""
        Me.txtActualAccomodationAmount.RangeMax = 1.7976931348623157E+308
        Me.txtActualAccomodationAmount.RangeMin = -1.7976931348623157E+308
        Me.txtActualAccomodationAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtActualAccomodationAmount.TabIndex = 31
        Me.txtActualAccomodationAmount.Text = "0"
        Me.txtActualAccomodationAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMealAmount
        '
        Me.txtMealAmount.AllowNegative = False
        Me.txtMealAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMealAmount.DigitsInGroup = 0
        Me.txtMealAmount.Flags = 65536
        Me.txtMealAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMealAmount.Location = New System.Drawing.Point(323, 192)
        Me.txtMealAmount.MaxDecimalPlaces = 6
        Me.txtMealAmount.MaxWholeDigits = 21
        Me.txtMealAmount.Name = "txtMealAmount"
        Me.txtMealAmount.Prefix = ""
        Me.txtMealAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMealAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMealAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtMealAmount.TabIndex = 15
        Me.txtMealAmount.Text = "0"
        Me.txtMealAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblActualMeals
        '
        Me.lblActualMeals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualMeals.Location = New System.Drawing.Point(504, 195)
        Me.lblActualMeals.Name = "lblActualMeals"
        Me.lblActualMeals.Size = New System.Drawing.Size(76, 15)
        Me.lblActualMeals.TabIndex = 34
        Me.lblActualMeals.Text = "Meals"
        Me.lblActualMeals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAccomodationAmount
        '
        Me.txtAccomodationAmount.AllowNegative = False
        Me.txtAccomodationAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAccomodationAmount.DigitsInGroup = 0
        Me.txtAccomodationAmount.Flags = 65536
        Me.txtAccomodationAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccomodationAmount.Location = New System.Drawing.Point(323, 138)
        Me.txtAccomodationAmount.MaxDecimalPlaces = 6
        Me.txtAccomodationAmount.MaxWholeDigits = 21
        Me.txtAccomodationAmount.Name = "txtAccomodationAmount"
        Me.txtAccomodationAmount.Prefix = ""
        Me.txtAccomodationAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAccomodationAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAccomodationAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtAccomodationAmount.TabIndex = 11
        Me.txtAccomodationAmount.Text = "0"
        Me.txtAccomodationAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMeals
        '
        Me.lblMeals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeals.Location = New System.Drawing.Point(241, 195)
        Me.lblMeals.Name = "lblMeals"
        Me.lblMeals.Size = New System.Drawing.Size(76, 15)
        Me.lblMeals.TabIndex = 14
        Me.lblMeals.Text = "Meals"
        Me.lblMeals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpTrainingResources
        '
        Me.tabpTrainingResources.Controls.Add(Me.gbResources)
        Me.tabpTrainingResources.Location = New System.Drawing.Point(4, 22)
        Me.tabpTrainingResources.Name = "tabpTrainingResources"
        Me.tabpTrainingResources.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpTrainingResources.Size = New System.Drawing.Size(762, 305)
        Me.tabpTrainingResources.TabIndex = 2
        Me.tabpTrainingResources.Text = "Training Resources"
        Me.tabpTrainingResources.UseVisualStyleBackColor = True
        '
        'gbResources
        '
        Me.gbResources.BorderColor = System.Drawing.Color.Black
        Me.gbResources.Checked = False
        Me.gbResources.CollapseAllExceptThis = False
        Me.gbResources.CollapsedHoverImage = Nothing
        Me.gbResources.CollapsedNormalImage = Nothing
        Me.gbResources.CollapsedPressedImage = Nothing
        Me.gbResources.CollapseOnLoad = False
        Me.gbResources.Controls.Add(Me.objbtnAddResources)
        Me.gbResources.Controls.Add(Me.pnlResources)
        Me.gbResources.Controls.Add(Me.btnAddResources)
        Me.gbResources.Controls.Add(Me.btnDeleteResources)
        Me.gbResources.Controls.Add(Me.objelLine1)
        Me.gbResources.Controls.Add(Me.txtResourseRemark)
        Me.gbResources.Controls.Add(Me.lblResourcesRemark)
        Me.gbResources.Controls.Add(Me.cboResources)
        Me.gbResources.Controls.Add(Me.lblResources)
        Me.gbResources.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbResources.ExpandedHoverImage = Nothing
        Me.gbResources.ExpandedNormalImage = Nothing
        Me.gbResources.ExpandedPressedImage = Nothing
        Me.gbResources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbResources.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbResources.HeaderHeight = 25
        Me.gbResources.HeaderMessage = ""
        Me.gbResources.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbResources.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbResources.HeightOnCollapse = 0
        Me.gbResources.LeftTextSpace = 0
        Me.gbResources.Location = New System.Drawing.Point(3, 3)
        Me.gbResources.Name = "gbResources"
        Me.gbResources.OpenHeight = 300
        Me.gbResources.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbResources.ShowBorder = True
        Me.gbResources.ShowCheckBox = False
        Me.gbResources.ShowCollapseButton = False
        Me.gbResources.ShowDefaultBorderColor = True
        Me.gbResources.ShowDownButton = False
        Me.gbResources.ShowHeader = True
        Me.gbResources.Size = New System.Drawing.Size(756, 299)
        Me.gbResources.TabIndex = 0
        Me.gbResources.Temp = 0
        Me.gbResources.Text = "Resources"
        Me.gbResources.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddResources
        '
        Me.objbtnAddResources.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResources.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResources.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResources.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResources.BorderSelected = False
        Me.objbtnAddResources.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResources.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResources.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResources.Location = New System.Drawing.Point(260, 33)
        Me.objbtnAddResources.Name = "objbtnAddResources"
        Me.objbtnAddResources.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResources.TabIndex = 2
        '
        'pnlResources
        '
        Me.pnlResources.Controls.Add(Me.lvResources)
        Me.pnlResources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlResources.Location = New System.Drawing.Point(11, 78)
        Me.pnlResources.Name = "pnlResources"
        Me.pnlResources.Size = New System.Drawing.Size(733, 178)
        Me.pnlResources.TabIndex = 185
        '
        'lvResources
        '
        Me.lvResources.BackColorOnChecked = True
        Me.lvResources.ColumnHeaders = Nothing
        Me.lvResources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhResource, Me.colhRemark, Me.objcolhResourceUnkid, Me.objcolhGUID})
        Me.lvResources.CompulsoryColumns = ""
        Me.lvResources.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvResources.FullRowSelect = True
        Me.lvResources.GridLines = True
        Me.lvResources.GroupingColumn = Nothing
        Me.lvResources.HideSelection = False
        Me.lvResources.Location = New System.Drawing.Point(0, 0)
        Me.lvResources.MinColumnWidth = 50
        Me.lvResources.MultiSelect = False
        Me.lvResources.Name = "lvResources"
        Me.lvResources.OptionalColumns = ""
        Me.lvResources.ShowMoreItem = False
        Me.lvResources.ShowSaveItem = False
        Me.lvResources.ShowSelectAll = True
        Me.lvResources.ShowSizeAllColumnsToFit = True
        Me.lvResources.Size = New System.Drawing.Size(733, 178)
        Me.lvResources.Sortable = True
        Me.lvResources.TabIndex = 0
        Me.lvResources.UseCompatibleStateImageBehavior = False
        Me.lvResources.View = System.Windows.Forms.View.Details
        '
        'colhResource
        '
        Me.colhResource.Tag = "colhResource"
        Me.colhResource.Text = "Resource"
        Me.colhResource.Width = 190
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 535
        '
        'objcolhResourceUnkid
        '
        Me.objcolhResourceUnkid.Tag = "objcolhResourceUnkid"
        Me.objcolhResourceUnkid.Text = ""
        Me.objcolhResourceUnkid.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = ""
        Me.objcolhGUID.Width = 0
        '
        'btnAddResources
        '
        Me.btnAddResources.BackColor = System.Drawing.Color.White
        Me.btnAddResources.BackgroundImage = CType(resources.GetObject("btnAddResources.BackgroundImage"), System.Drawing.Image)
        Me.btnAddResources.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddResources.BorderColor = System.Drawing.Color.Empty
        Me.btnAddResources.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddResources.FlatAppearance.BorderSize = 0
        Me.btnAddResources.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddResources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddResources.ForeColor = System.Drawing.Color.Black
        Me.btnAddResources.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddResources.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddResources.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddResources.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddResources.Location = New System.Drawing.Point(544, 262)
        Me.btnAddResources.Name = "btnAddResources"
        Me.btnAddResources.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddResources.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddResources.Size = New System.Drawing.Size(97, 30)
        Me.btnAddResources.TabIndex = 6
        Me.btnAddResources.Text = "&Add"
        Me.btnAddResources.UseVisualStyleBackColor = True
        '
        'btnDeleteResources
        '
        Me.btnDeleteResources.BackColor = System.Drawing.Color.White
        Me.btnDeleteResources.BackgroundImage = CType(resources.GetObject("btnDeleteResources.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteResources.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteResources.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteResources.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteResources.FlatAppearance.BorderSize = 0
        Me.btnDeleteResources.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteResources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteResources.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteResources.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteResources.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteResources.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteResources.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteResources.Location = New System.Drawing.Point(647, 262)
        Me.btnDeleteResources.Name = "btnDeleteResources"
        Me.btnDeleteResources.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteResources.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteResources.Size = New System.Drawing.Size(97, 30)
        Me.btnDeleteResources.TabIndex = 7
        Me.btnDeleteResources.Text = "&Delete"
        Me.btnDeleteResources.UseVisualStyleBackColor = True
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 57)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(733, 18)
        Me.objelLine1.TabIndex = 5
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtResourseRemark
        '
        Me.txtResourseRemark.Flags = 0
        Me.txtResourseRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResourseRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtResourseRemark.Location = New System.Drawing.Point(368, 33)
        Me.txtResourseRemark.Name = "txtResourseRemark"
        Me.txtResourseRemark.Size = New System.Drawing.Size(376, 21)
        Me.txtResourseRemark.TabIndex = 4
        '
        'lblResourcesRemark
        '
        Me.lblResourcesRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResourcesRemark.Location = New System.Drawing.Point(287, 36)
        Me.lblResourcesRemark.Name = "lblResourcesRemark"
        Me.lblResourcesRemark.Size = New System.Drawing.Size(75, 15)
        Me.lblResourcesRemark.TabIndex = 3
        Me.lblResourcesRemark.Text = "Remark"
        Me.lblResourcesRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResources
        '
        Me.cboResources.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResources.FormattingEnabled = True
        Me.cboResources.Location = New System.Drawing.Point(92, 33)
        Me.cboResources.Name = "cboResources"
        Me.cboResources.Size = New System.Drawing.Size(162, 21)
        Me.cboResources.TabIndex = 1
        '
        'lblResources
        '
        Me.lblResources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResources.Location = New System.Drawing.Point(11, 36)
        Me.lblResources.Name = "lblResources"
        Me.lblResources.Size = New System.Drawing.Size(75, 15)
        Me.lblResources.TabIndex = 0
        Me.lblResources.Text = "Resources"
        Me.lblResources.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpTrainerInfo
        '
        Me.tabpTrainerInfo.Controls.Add(Me.gbTrainerInfo)
        Me.tabpTrainerInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpTrainerInfo.Name = "tabpTrainerInfo"
        Me.tabpTrainerInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpTrainerInfo.Size = New System.Drawing.Size(762, 305)
        Me.tabpTrainerInfo.TabIndex = 1
        Me.tabpTrainerInfo.Text = "Trainers Information"
        Me.tabpTrainerInfo.UseVisualStyleBackColor = True
        '
        'gbTrainerInfo
        '
        Me.gbTrainerInfo.BackColor = System.Drawing.Color.Transparent
        Me.gbTrainerInfo.BorderColor = System.Drawing.Color.Black
        Me.gbTrainerInfo.Checked = False
        Me.gbTrainerInfo.CollapseAllExceptThis = False
        Me.gbTrainerInfo.CollapsedHoverImage = Nothing
        Me.gbTrainerInfo.CollapsedNormalImage = Nothing
        Me.gbTrainerInfo.CollapsedPressedImage = Nothing
        Me.gbTrainerInfo.CollapseOnLoad = False
        Me.gbTrainerInfo.Controls.Add(Me.lblLevelCaption)
        Me.gbTrainerInfo.Controls.Add(Me.nudLevel)
        Me.gbTrainerInfo.Controls.Add(Me.lblTrainerLevel)
        Me.gbTrainerInfo.Controls.Add(Me.objelLine2)
        Me.gbTrainerInfo.Controls.Add(Me.objStLine2)
        Me.gbTrainerInfo.Controls.Add(Me.btnAddTrainer)
        Me.gbTrainerInfo.Controls.Add(Me.lvTrainerInfo)
        Me.gbTrainerInfo.Controls.Add(Me.btnEditTrainer)
        Me.gbTrainerInfo.Controls.Add(Me.btnDeleteTrainer)
        Me.gbTrainerInfo.Controls.Add(Me.pnlOthers)
        Me.gbTrainerInfo.Controls.Add(Me.radOthers)
        Me.gbTrainerInfo.Controls.Add(Me.objColon3)
        Me.gbTrainerInfo.Controls.Add(Me.objColon2)
        Me.gbTrainerInfo.Controls.Add(Me.objColon1)
        Me.gbTrainerInfo.Controls.Add(Me.lblEmpContact)
        Me.gbTrainerInfo.Controls.Add(Me.lblEmpCompany)
        Me.gbTrainerInfo.Controls.Add(Me.lblDepartment)
        Me.gbTrainerInfo.Controls.Add(Me.objlblEmployeeContactNo)
        Me.gbTrainerInfo.Controls.Add(Me.objlblCompanyValue)
        Me.gbTrainerInfo.Controls.Add(Me.objlblDepartmentValue)
        Me.gbTrainerInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbTrainerInfo.Controls.Add(Me.cboEmployee)
        Me.gbTrainerInfo.Controls.Add(Me.radEmployee)
        Me.gbTrainerInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTrainerInfo.ExpandedHoverImage = Nothing
        Me.gbTrainerInfo.ExpandedNormalImage = Nothing
        Me.gbTrainerInfo.ExpandedPressedImage = Nothing
        Me.gbTrainerInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTrainerInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTrainerInfo.HeaderHeight = 25
        Me.gbTrainerInfo.HeaderMessage = ""
        Me.gbTrainerInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTrainerInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTrainerInfo.HeightOnCollapse = 0
        Me.gbTrainerInfo.LeftTextSpace = 0
        Me.gbTrainerInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbTrainerInfo.Name = "gbTrainerInfo"
        Me.gbTrainerInfo.OpenHeight = 283
        Me.gbTrainerInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTrainerInfo.ShowBorder = True
        Me.gbTrainerInfo.ShowCheckBox = False
        Me.gbTrainerInfo.ShowCollapseButton = False
        Me.gbTrainerInfo.ShowDefaultBorderColor = True
        Me.gbTrainerInfo.ShowDownButton = False
        Me.gbTrainerInfo.ShowHeader = True
        Me.gbTrainerInfo.Size = New System.Drawing.Size(756, 299)
        Me.gbTrainerInfo.TabIndex = 0
        Me.gbTrainerInfo.Temp = 0
        Me.gbTrainerInfo.Text = "Trainer Info"
        Me.gbTrainerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLevelCaption
        '
        Me.lblLevelCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelCaption.ForeColor = System.Drawing.Color.Red
        Me.lblLevelCaption.Location = New System.Drawing.Point(166, 35)
        Me.lblLevelCaption.Name = "lblLevelCaption"
        Me.lblLevelCaption.Size = New System.Drawing.Size(121, 14)
        Me.lblLevelCaption.TabIndex = 23
        Me.lblLevelCaption.Text = "(1 Means Lower Level)"
        Me.lblLevelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudLevel
        '
        Me.nudLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLevel.Location = New System.Drawing.Point(122, 32)
        Me.nudLevel.Maximum = New Decimal(New Integer() {30, 0, 0, 0})
        Me.nudLevel.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudLevel.Name = "nudLevel"
        Me.nudLevel.Size = New System.Drawing.Size(40, 21)
        Me.nudLevel.TabIndex = 22
        Me.nudLevel.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblTrainerLevel
        '
        Me.lblTrainerLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainerLevel.Location = New System.Drawing.Point(8, 34)
        Me.lblTrainerLevel.Name = "lblTrainerLevel"
        Me.lblTrainerLevel.Size = New System.Drawing.Size(108, 15)
        Me.lblTrainerLevel.TabIndex = 21
        Me.lblTrainerLevel.Text = "Level"
        Me.lblTrainerLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(8, 56)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(273, 6)
        Me.objelLine2.TabIndex = 20
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine2
        '
        Me.objStLine2.BackColor = System.Drawing.Color.Transparent
        Me.objStLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine2.Location = New System.Drawing.Point(287, 34)
        Me.objStLine2.Name = "objStLine2"
        Me.objStLine2.Size = New System.Drawing.Size(8, 256)
        Me.objStLine2.TabIndex = 14
        Me.objStLine2.Text = "EZeeStraightLine1"
        '
        'btnAddTrainer
        '
        Me.btnAddTrainer.BackColor = System.Drawing.Color.White
        Me.btnAddTrainer.BackgroundImage = CType(resources.GetObject("btnAddTrainer.BackgroundImage"), System.Drawing.Image)
        Me.btnAddTrainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddTrainer.BorderColor = System.Drawing.Color.Empty
        Me.btnAddTrainer.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddTrainer.FlatAppearance.BorderSize = 0
        Me.btnAddTrainer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddTrainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddTrainer.ForeColor = System.Drawing.Color.Black
        Me.btnAddTrainer.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddTrainer.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddTrainer.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddTrainer.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddTrainer.Location = New System.Drawing.Point(442, 260)
        Me.btnAddTrainer.Name = "btnAddTrainer"
        Me.btnAddTrainer.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddTrainer.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddTrainer.Size = New System.Drawing.Size(97, 30)
        Me.btnAddTrainer.TabIndex = 16
        Me.btnAddTrainer.Text = "&Add"
        Me.btnAddTrainer.UseVisualStyleBackColor = True
        '
        'lvTrainerInfo
        '
        Me.lvTrainerInfo.BackColorOnChecked = True
        Me.lvTrainerInfo.ColumnHeaders = Nothing
        Me.lvTrainerInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLevel, Me.colhEmpName, Me.colhDepartment, Me.colhCompany, Me.colhContactNo, Me.objcolhEmpunkid, Me.objcolhTrainerGUID})
        Me.lvTrainerInfo.CompulsoryColumns = ""
        Me.lvTrainerInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTrainerInfo.FullRowSelect = True
        Me.lvTrainerInfo.GridLines = True
        Me.lvTrainerInfo.GroupingColumn = Nothing
        Me.lvTrainerInfo.HideSelection = False
        Me.lvTrainerInfo.Location = New System.Drawing.Point(299, 34)
        Me.lvTrainerInfo.MinColumnWidth = 50
        Me.lvTrainerInfo.MultiSelect = False
        Me.lvTrainerInfo.Name = "lvTrainerInfo"
        Me.lvTrainerInfo.OptionalColumns = ""
        Me.lvTrainerInfo.ShowMoreItem = False
        Me.lvTrainerInfo.ShowSaveItem = False
        Me.lvTrainerInfo.ShowSelectAll = True
        Me.lvTrainerInfo.ShowSizeAllColumnsToFit = True
        Me.lvTrainerInfo.Size = New System.Drawing.Size(446, 220)
        Me.lvTrainerInfo.Sortable = True
        Me.lvTrainerInfo.TabIndex = 15
        Me.lvTrainerInfo.UseCompatibleStateImageBehavior = False
        Me.lvTrainerInfo.View = System.Windows.Forms.View.Details
        '
        'colhLevel
        '
        Me.colhLevel.Tag = "colhLevel"
        Me.colhLevel.Text = "Level"
        Me.colhLevel.Width = 40
        '
        'colhEmpName
        '
        Me.colhEmpName.Tag = "colhEmpName"
        Me.colhEmpName.Text = "Emp Name"
        Me.colhEmpName.Width = 100
        '
        'colhDepartment
        '
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Dept/Position"
        Me.colhDepartment.Width = 100
        '
        'colhCompany
        '
        Me.colhCompany.Tag = "colhCompany"
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 100
        '
        'colhContactNo
        '
        Me.colhContactNo.Tag = "colhContactNo"
        Me.colhContactNo.Text = "Contact No"
        Me.colhContactNo.Width = 100
        '
        'objcolhEmpunkid
        '
        Me.objcolhEmpunkid.Tag = "objcolhEmpunkid"
        Me.objcolhEmpunkid.Text = ""
        Me.objcolhEmpunkid.Width = 0
        '
        'objcolhTrainerGUID
        '
        Me.objcolhTrainerGUID.Tag = "objcolhTrainerGUID"
        Me.objcolhTrainerGUID.Text = ""
        Me.objcolhTrainerGUID.Width = 0
        '
        'btnEditTrainer
        '
        Me.btnEditTrainer.BackColor = System.Drawing.Color.White
        Me.btnEditTrainer.BackgroundImage = CType(resources.GetObject("btnEditTrainer.BackgroundImage"), System.Drawing.Image)
        Me.btnEditTrainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditTrainer.BorderColor = System.Drawing.Color.Empty
        Me.btnEditTrainer.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditTrainer.FlatAppearance.BorderSize = 0
        Me.btnEditTrainer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditTrainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditTrainer.ForeColor = System.Drawing.Color.Black
        Me.btnEditTrainer.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditTrainer.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditTrainer.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditTrainer.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditTrainer.Location = New System.Drawing.Point(545, 260)
        Me.btnEditTrainer.Name = "btnEditTrainer"
        Me.btnEditTrainer.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditTrainer.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditTrainer.Size = New System.Drawing.Size(97, 30)
        Me.btnEditTrainer.TabIndex = 17
        Me.btnEditTrainer.Text = "&Edit"
        Me.btnEditTrainer.UseVisualStyleBackColor = True
        '
        'btnDeleteTrainer
        '
        Me.btnDeleteTrainer.BackColor = System.Drawing.Color.White
        Me.btnDeleteTrainer.BackgroundImage = CType(resources.GetObject("btnDeleteTrainer.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteTrainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteTrainer.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteTrainer.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteTrainer.FlatAppearance.BorderSize = 0
        Me.btnDeleteTrainer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteTrainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteTrainer.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteTrainer.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteTrainer.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteTrainer.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteTrainer.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteTrainer.Location = New System.Drawing.Point(648, 260)
        Me.btnDeleteTrainer.Name = "btnDeleteTrainer"
        Me.btnDeleteTrainer.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteTrainer.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteTrainer.Size = New System.Drawing.Size(97, 30)
        Me.btnDeleteTrainer.TabIndex = 18
        Me.btnDeleteTrainer.Text = "&Delete"
        Me.btnDeleteTrainer.UseVisualStyleBackColor = True
        '
        'pnlOthers
        '
        Me.pnlOthers.Controls.Add(Me.lblTrainerContactNo)
        Me.pnlOthers.Controls.Add(Me.txtTrainersContactNo)
        Me.pnlOthers.Controls.Add(Me.lblCompany)
        Me.pnlOthers.Controls.Add(Me.lblPosition)
        Me.pnlOthers.Controls.Add(Me.txtComany)
        Me.pnlOthers.Controls.Add(Me.txtPosition)
        Me.pnlOthers.Controls.Add(Me.lblName)
        Me.pnlOthers.Controls.Add(Me.txtName)
        Me.pnlOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOthers.Location = New System.Drawing.Point(11, 177)
        Me.pnlOthers.Name = "pnlOthers"
        Me.pnlOthers.Size = New System.Drawing.Size(270, 108)
        Me.pnlOthers.TabIndex = 13
        '
        'lblTrainerContactNo
        '
        Me.lblTrainerContactNo.Location = New System.Drawing.Point(9, 87)
        Me.lblTrainerContactNo.Name = "lblTrainerContactNo"
        Me.lblTrainerContactNo.Size = New System.Drawing.Size(93, 15)
        Me.lblTrainerContactNo.TabIndex = 6
        Me.lblTrainerContactNo.Text = "Contact No"
        Me.lblTrainerContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTrainersContactNo
        '
        Me.txtTrainersContactNo.Flags = 0
        Me.txtTrainersContactNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTrainersContactNo.Location = New System.Drawing.Point(107, 84)
        Me.txtTrainersContactNo.Name = "txtTrainersContactNo"
        Me.txtTrainersContactNo.Size = New System.Drawing.Size(148, 21)
        Me.txtTrainersContactNo.TabIndex = 7
        '
        'lblCompany
        '
        Me.lblCompany.Location = New System.Drawing.Point(9, 60)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(97, 15)
        Me.lblCompany.TabIndex = 4
        Me.lblCompany.Text = "Institute/Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition
        '
        Me.lblPosition.Location = New System.Drawing.Point(9, 33)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(93, 15)
        Me.lblPosition.TabIndex = 2
        Me.lblPosition.Text = "Position"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComany
        '
        Me.txtComany.Flags = 0
        Me.txtComany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtComany.Location = New System.Drawing.Point(107, 57)
        Me.txtComany.Name = "txtComany"
        Me.txtComany.Size = New System.Drawing.Size(148, 21)
        Me.txtComany.TabIndex = 5
        '
        'txtPosition
        '
        Me.txtPosition.Flags = 0
        Me.txtPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPosition.Location = New System.Drawing.Point(107, 30)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.Size = New System.Drawing.Size(148, 21)
        Me.txtPosition.TabIndex = 3
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(9, 6)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(93, 15)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(107, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(148, 21)
        Me.txtName.TabIndex = 1
        '
        'radOthers
        '
        Me.radOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOthers.Location = New System.Drawing.Point(8, 159)
        Me.radOthers.Name = "radOthers"
        Me.radOthers.Size = New System.Drawing.Size(108, 17)
        Me.radOthers.TabIndex = 12
        Me.radOthers.TabStop = True
        Me.radOthers.Text = "Other"
        Me.radOthers.UseVisualStyleBackColor = True
        '
        'objColon3
        '
        Me.objColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon3.Location = New System.Drawing.Point(103, 133)
        Me.objColon3.Name = "objColon3"
        Me.objColon3.Size = New System.Drawing.Size(8, 15)
        Me.objColon3.TabIndex = 10
        Me.objColon3.Text = ":"
        Me.objColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon2
        '
        Me.objColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon2.Location = New System.Drawing.Point(103, 114)
        Me.objColon2.Name = "objColon2"
        Me.objColon2.Size = New System.Drawing.Size(8, 15)
        Me.objColon2.TabIndex = 7
        Me.objColon2.Text = ":"
        Me.objColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon1
        '
        Me.objColon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon1.Location = New System.Drawing.Point(103, 97)
        Me.objColon1.Name = "objColon1"
        Me.objColon1.Size = New System.Drawing.Size(8, 15)
        Me.objColon1.TabIndex = 4
        Me.objColon1.Text = ":"
        Me.objColon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpContact
        '
        Me.lblEmpContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContact.Location = New System.Drawing.Point(20, 133)
        Me.lblEmpContact.Name = "lblEmpContact"
        Me.lblEmpContact.Size = New System.Drawing.Size(77, 15)
        Me.lblEmpContact.TabIndex = 9
        Me.lblEmpContact.Text = "Contact No"
        Me.lblEmpContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpCompany
        '
        Me.lblEmpCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCompany.Location = New System.Drawing.Point(20, 114)
        Me.lblEmpCompany.Name = "lblEmpCompany"
        Me.lblEmpCompany.Size = New System.Drawing.Size(77, 15)
        Me.lblEmpCompany.TabIndex = 6
        Me.lblEmpCompany.Text = "Company"
        Me.lblEmpCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(20, 97)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(77, 15)
        Me.lblDepartment.TabIndex = 3
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmployeeContactNo
        '
        Me.objlblEmployeeContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmployeeContactNo.Location = New System.Drawing.Point(117, 133)
        Me.objlblEmployeeContactNo.Name = "objlblEmployeeContactNo"
        Me.objlblEmployeeContactNo.Size = New System.Drawing.Size(164, 15)
        Me.objlblEmployeeContactNo.TabIndex = 11
        Me.objlblEmployeeContactNo.Text = "#Contact No"
        Me.objlblEmployeeContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCompanyValue
        '
        Me.objlblCompanyValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCompanyValue.Location = New System.Drawing.Point(117, 114)
        Me.objlblCompanyValue.Name = "objlblCompanyValue"
        Me.objlblCompanyValue.Size = New System.Drawing.Size(164, 15)
        Me.objlblCompanyValue.TabIndex = 8
        Me.objlblCompanyValue.Text = "#Company"
        Me.objlblCompanyValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblDepartmentValue
        '
        Me.objlblDepartmentValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDepartmentValue.Location = New System.Drawing.Point(117, 97)
        Me.objlblDepartmentValue.Name = "objlblDepartmentValue"
        Me.objlblDepartmentValue.Size = New System.Drawing.Size(164, 15)
        Me.objlblDepartmentValue.TabIndex = 5
        Me.objlblDepartmentValue.Text = "#DepartmentName"
        Me.objlblDepartmentValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(260, 71)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(122, 71)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(132, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'radEmployee
        '
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(8, 73)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(108, 17)
        Me.radEmployee.TabIndex = 0
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnMakeActive)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 396)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(776, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(393, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(165, 30)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel Training Schedule"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(564, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(667, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnMakeActive
        '
        Me.btnMakeActive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMakeActive.BackColor = System.Drawing.Color.White
        Me.btnMakeActive.BackgroundImage = CType(resources.GetObject("btnMakeActive.BackgroundImage"), System.Drawing.Image)
        Me.btnMakeActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMakeActive.BorderColor = System.Drawing.Color.Empty
        Me.btnMakeActive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMakeActive.FlatAppearance.BorderSize = 0
        Me.btnMakeActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMakeActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMakeActive.ForeColor = System.Drawing.Color.Black
        Me.btnMakeActive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMakeActive.GradientForeColor = System.Drawing.Color.Black
        Me.btnMakeActive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMakeActive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMakeActive.Location = New System.Drawing.Point(461, 13)
        Me.btnMakeActive.Name = "btnMakeActive"
        Me.btnMakeActive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMakeActive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMakeActive.Size = New System.Drawing.Size(97, 30)
        Me.btnMakeActive.TabIndex = 3
        Me.btnMakeActive.Text = "&Make Active"
        Me.btnMakeActive.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(776, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Training Schedule"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Position"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Company"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Contact No"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'lnkBindAllocation
        '
        Me.lnkBindAllocation.ActiveLinkColor = System.Drawing.Color.Blue
        Me.lnkBindAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkBindAllocation.Location = New System.Drawing.Point(581, 4)
        Me.lnkBindAllocation.Name = "lnkBindAllocation"
        Me.lnkBindAllocation.Size = New System.Drawing.Size(168, 16)
        Me.lnkBindAllocation.TabIndex = 39
        Me.lnkBindAllocation.TabStop = True
        Me.lnkBindAllocation.Text = "Bind Allocation"
        Me.lnkBindAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkBindAllocation.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'frmCourseScheduling
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(776, 451)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.pnlCourseScheduling)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCourseScheduling"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Schedule"
        Me.pnlCourseScheduling.ResumeLayout(False)
        Me.tabcCourseScheduling.ResumeLayout(False)
        Me.tabpCourseScheduling.ResumeLayout(False)
        Me.gbCourseSchedule.ResumeLayout(False)
        Me.gbCourseSchedule.PerformLayout()
        Me.tabcRemark.ResumeLayout(False)
        Me.tabpEligibilityInfo.ResumeLayout(False)
        Me.tabpEligibilityInfo.PerformLayout()
        Me.tabpTrainingRemark.ResumeLayout(False)
        Me.tabpTrainingRemark.PerformLayout()
        Me.tabpCancellationRemark.ResumeLayout(False)
        Me.tabpCancellationRemark.PerformLayout()
        CType(Me.nudAvailablePostion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpBudgets.ResumeLayout(False)
        Me.gbBudgetsFundings.ResumeLayout(False)
        Me.gbBudgetsFundings.PerformLayout()
        Me.tabpTrainingResources.ResumeLayout(False)
        Me.gbResources.ResumeLayout(False)
        Me.gbResources.PerformLayout()
        Me.pnlResources.ResumeLayout(False)
        Me.tabpTrainerInfo.ResumeLayout(False)
        Me.gbTrainerInfo.ResumeLayout(False)
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOthers.ResumeLayout(False)
        Me.pnlOthers.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlCourseScheduling As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbCourseSchedule As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboQualification As System.Windows.Forms.ComboBox
    Friend WithEvents cboInstitute As System.Windows.Forms.ComboBox
    Friend WithEvents cboTraningYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblTraningCourse As System.Windows.Forms.Label
    Friend WithEvents lblTraningInstitute As System.Windows.Forms.Label
    Friend WithEvents lblTraningYear As System.Windows.Forms.Label
    Friend WithEvents dtpStartTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtVenue As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpEndTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblContactPerson As System.Windows.Forms.Label
    Friend WithEvents lblVenue As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents txtContactNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtContactPerson As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblContactNo As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblMaxCandidate As System.Windows.Forms.Label
    Friend WithEvents txtEligibility As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents nudAvailablePostion As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
    Friend WithEvents cboResultGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objstLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents tabcCourseScheduling As System.Windows.Forms.TabControl
    Friend WithEvents tabpCourseScheduling As System.Windows.Forms.TabPage
    Friend WithEvents tabpTrainerInfo As System.Windows.Forms.TabPage
    Friend WithEvents lblQualificationAward As System.Windows.Forms.Label
    Friend WithEvents gbTrainerInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnAddTrainer As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditTrainer As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeleteTrainer As eZee.Common.eZeeLightButton
    Friend WithEvents pnlOthers As System.Windows.Forms.Panel
    Friend WithEvents lblTrainerContactNo As System.Windows.Forms.Label
    Friend WithEvents txtTrainersContactNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents txtComany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents radOthers As System.Windows.Forms.RadioButton
    Friend WithEvents objColon3 As System.Windows.Forms.Label
    Friend WithEvents objColon2 As System.Windows.Forms.Label
    Friend WithEvents objColon1 As System.Windows.Forms.Label
    Friend WithEvents lblEmpContact As System.Windows.Forms.Label
    Friend WithEvents lblEmpCompany As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objlblEmployeeContactNo As System.Windows.Forms.Label
    Friend WithEvents objlblCompanyValue As System.Windows.Forms.Label
    Friend WithEvents objlblDepartmentValue As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboQualifiedAward As System.Windows.Forms.ComboBox
    Friend WithEvents lblTraningTitle As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tabpTrainingResources As System.Windows.Forms.TabPage
    Friend WithEvents gbResources As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlResources As System.Windows.Forms.Panel
    Friend WithEvents lvResources As eZee.Common.eZeeListView
    Friend WithEvents colhResource As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAddResources As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeleteResources As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents txtResourseRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblResourcesRemark As System.Windows.Forms.Label
    Friend WithEvents cboResources As System.Windows.Forms.ComboBox
    Friend WithEvents lblResources As System.Windows.Forms.Label
    Friend WithEvents objbtnAddResources As eZee.Common.eZeeGradientButton
    Friend WithEvents lvTrainerInfo As eZee.Common.eZeeListView
    Friend WithEvents colhEmpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents tabcRemark As System.Windows.Forms.TabControl
    Friend WithEvents tabpEligibilityInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpTrainingRemark As System.Windows.Forms.TabPage
    Friend WithEvents tabpCancellationRemark As System.Windows.Forms.TabPage
    Friend WithEvents txtCancelRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objcolhResourceUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmpunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhTrainerGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objStLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents btnMakeActive As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents lblTrainerLevel As System.Windows.Forms.Label
    Friend WithEvents nudLevel As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLevelCaption As System.Windows.Forms.Label
    Friend WithEvents colhLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddQualification As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddQGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddInstitute As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddResultGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCourseMaster As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddCourse As eZee.Common.eZeeGradientButton
    Friend WithEvents tabpBudgets As System.Windows.Forms.TabPage
    Friend WithEvents objbtnSearchCourse As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchInstitute As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchResult As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchQualification As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchQGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents txtFinalAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblExam As System.Windows.Forms.Label
    Friend WithEvents txtExamAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtMaterialAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMaterialAmount As System.Windows.Forms.Label
    Friend WithEvents txtAllowanceAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtMealAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMeals As System.Windows.Forms.Label
    Friend WithEvents lblAllowance As System.Windows.Forms.Label
    Friend WithEvents txtAccomodationAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAccomodation As System.Windows.Forms.Label
    Friend WithEvents txtTravelAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtTraningFee As eZee.TextBox.NumericTextBox
    Friend WithEvents txtMiscAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTravel As System.Windows.Forms.Label
    Friend WithEvents lblTraningMisc As System.Windows.Forms.Label
    Friend WithEvents lblFees As System.Windows.Forms.Label
    Friend WithEvents gbBudgetsFundings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents txtActualTraningFee As eZee.TextBox.NumericTextBox
    Friend WithEvents txtActualFinalAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblActualAllowance As System.Windows.Forms.Label
    Friend WithEvents lblActualAmount As System.Windows.Forms.Label
    Friend WithEvents lblActualFees As System.Windows.Forms.Label
    Friend WithEvents lblActualExam As System.Windows.Forms.Label
    Friend WithEvents lblActualMisc As System.Windows.Forms.Label
    Friend WithEvents txtActualExamAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblActualTravel As System.Windows.Forms.Label
    Friend WithEvents txtActualMaterialAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtActualMiscAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblActualMaterialAmount As System.Windows.Forms.Label
    Friend WithEvents txtActualTravelAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtActualAllowanceAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblActualAccomodation As System.Windows.Forms.Label
    Friend WithEvents txtActualMealAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtActualAccomodationAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblActualMeals As System.Windows.Forms.Label
    Friend WithEvents elActuals As eZee.Common.eZeeLine
    Friend WithEvents elBudgets As eZee.Common.eZeeLine
    Friend WithEvents elSourceFundings As eZee.Common.eZeeLine
    Friend WithEvents lvSourceFunding As eZee.Common.eZeeListView
    Friend WithEvents colhFundingSource As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkBindAllocation As System.Windows.Forms.LinkLabel
End Class
