﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmCourseScheduling

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmCourseScheduling"
    Private mblnCancel As Boolean = True
    Private objTriningSchedling As clsTraining_Scheduling
    Private objResourcesTran As clsTraining_Resources_Tran
    Private objTrainersTran As clsTraining_Trainers_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintTrainingSchedulingUnkid As Integer = -1
    Private mblnFormLoad As Boolean = False
    Private mdtResources As DataTable
    Private mdtTrainers As DataTable
    Private intResourceSelectedIdx As Integer = -1
    Private intTrainersSelectedIdx As Integer = -1
    Dim dRView As New DataView
    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private objAllocation As clsTraining_Allocation_Tran
    Private mdtAllocTran As DataTable
    Private mintTrainingAllocTypeId As Integer = 0
    'S.SANDEEP [08-FEB-2017] -- END
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintTrainingSchedulingUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintTrainingSchedulingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            txtAccomodationAmount.BackColor = GUI.ColorOptional
            txtAllowanceAmount.BackColor = GUI.ColorOptional
            txtComany.BackColor = GUI.ColorOptional
            txtContactNo.BackColor = GUI.ColorOptional
            txtContactPerson.BackColor = GUI.ColorOptional

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'txtCourseTitle.BackColor = GUI.ColorComp
            cboCourseMaster.BackColor = GUI.ColorComp
            'Anjan (10 Feb 2012)-End 


            txtEligibility.BackColor = GUI.ColorOptional
            txtExamAmount.BackColor = GUI.ColorOptional
            txtFinalAmount.BackColor = GUI.ColorOptional
            txtMaterialAmount.BackColor = GUI.ColorOptional
            txtMealAmount.BackColor = GUI.ColorOptional
            txtMiscAmount.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtPosition.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            txtResourseRemark.BackColor = GUI.ColorOptional
            txtTrainersContactNo.BackColor = GUI.ColorOptional
            txtTraningFee.BackColor = GUI.ColorComp
            txtTravelAmount.BackColor = GUI.ColorOptional
            txtVenue.BackColor = GUI.ColorOptional
            cboQualification.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboInstitute.BackColor = GUI.ColorOptional
            cboQualifiedAward.BackColor = GUI.ColorComp
            cboResources.BackColor = GUI.ColorComp
            cboResultGroup.BackColor = GUI.ColorComp
            cboTraningYear.BackColor = GUI.ColorComp
            nudAvailablePostion.BackColor = GUI.ColorComp
            txtCancelRemark.BackColor = GUI.ColorComp

            'S.SANDEEP [ 24 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES [<TRAINING>]
            txtActualAccomodationAmount.BackColor = GUI.ColorOptional
            txtActualAllowanceAmount.BackColor = GUI.ColorOptional
            txtActualExamAmount.BackColor = GUI.ColorOptional
            txtActualFinalAmount.BackColor = GUI.ColorOptional
            txtActualMaterialAmount.BackColor = GUI.ColorOptional
            txtActualMealAmount.BackColor = GUI.ColorOptional
            txtActualMiscAmount.BackColor = GUI.ColorOptional
            txtActualTraningFee.BackColor = GUI.ColorOptional
            txtActualTravelAmount.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 24 FEB 2012 ] -- END 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objYear As New clsMasterData
        Dim objCommon As New clsCommon_Master
        Dim objInstitute As New clsinstitute_master
        Dim objEmp As New clsEmployee_Master
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objYear.getComboListPAYYEAR("Year", True)
            dsList = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboTraningYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Year")
                .SelectedValue = 0
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "ResultGroup")
            With cboResultGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("ResultGroup")
                .SelectedValue = 0
            End With
            dRView = dsList.Tables("ResultGroup").DefaultView

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "Course")
            With cboQualification
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Course")
                .SelectedValue = 0
            End With

            

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_RESOURCES, True, "Resources")
            With cboResources
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Resources")
                .SelectedValue = 0
            End With

            dsList = objInstitute.getListForCombo(False, "Institute", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Institute")
                .SelectedValue = 0
            End With

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboCourseMaster
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Anjan (10 Feb 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'objTriningSchedling._Accomodation = txtAccomodationAmount.Double
            'objTriningSchedling._Allowance = txtAllowanceAmount.Double

            objTriningSchedling._Accomodation = txtAccomodationAmount.Decimal
            objTriningSchedling._Allowance = txtAllowanceAmount.Decimal
            'S.SANDEEP [ 18 May 2011 ] -- END

            objTriningSchedling._Contact_No = txtContactNo.Text
            objTriningSchedling._Contact_Person = txtContactPerson.Text


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'objTriningSchedling._Course_Title = txtCourseTitle.Text
            objTriningSchedling._Course_Title = CStr(cboCourseMaster.SelectedValue)
            'Anjan (10 Feb 2012)-End 




            objTriningSchedling._Eligibility_Criteria = txtEligibility.Text
            objTriningSchedling._End_Date = dtpEndDate.Value
            objTriningSchedling._End_Time = dtpEndTime.Value
            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'objTriningSchedling._Exam = txtExamAmount.Double
            'objTriningSchedling._Fees = txtTraningFee.Double

            objTriningSchedling._Exam = txtExamAmount.Decimal
            objTriningSchedling._Fees = txtTraningFee.Decimal
            'S.SANDEEP [ 18 May 2011 ] -- END 
            objTriningSchedling._Isvoid = objTriningSchedling._Isvoid
            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'objTriningSchedling._Material = txtMaterialAmount.Double
            'objTriningSchedling._Meals = txtMealAmount.Double
            'objTriningSchedling._Misc = txtMiscAmount.Double

            objTriningSchedling._Material = txtMaterialAmount.Decimal
            objTriningSchedling._Meals = txtMealAmount.Decimal
            objTriningSchedling._Misc = txtMiscAmount.Decimal
            'S.SANDEEP [ 18 May 2011 ] -- END 
            objTriningSchedling._Postion_No = CInt(nudAvailablePostion.Value)
            objTriningSchedling._Qualificationgroupunkid = CInt(cboQualification.SelectedValue)
            objTriningSchedling._Qualificationunkid = CInt(cboQualifiedAward.SelectedValue)
            objTriningSchedling._Resultgroupunkid = CInt(cboResultGroup.SelectedValue)
            objTriningSchedling._Start_Date = dtpStartDate.Value
            objTriningSchedling._Start_Time = dtpStartTime.Value
            objTriningSchedling._Traning_Venue = txtVenue.Text
            objTriningSchedling._Traninginstituteunkid = CInt(cboInstitute.SelectedValue)
            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'objTriningSchedling._Travel = txtTravelAmount.Double

            objTriningSchedling._Travel = txtTravelAmount.Decimal
            'S.SANDEEP [ 18 May 2011 ] -- END 
            objTriningSchedling._Training_Remark = txtRemark.Text
            If mintTrainingSchedulingUnkid = -1 Then
                objTriningSchedling._Userunkid = User._Object._Userunkid
                objTriningSchedling._Isvoid = False
                objTriningSchedling._Voiddatetime = Nothing
                objTriningSchedling._Voidreason = ""
                objTriningSchedling._Voiduserunkid = -1
            Else
                objTriningSchedling._Userunkid = objTriningSchedling._Userunkid
                objTriningSchedling._Isvoid = objTriningSchedling._Isvoid
                objTriningSchedling._Voiddatetime = objTriningSchedling._Voiddatetime
                objTriningSchedling._Voidreason = objTriningSchedling._Voidreason
                objTriningSchedling._Voiduserunkid = objTriningSchedling._Voiduserunkid
            End If
            objTriningSchedling._Yearunkid = CInt(cboTraningYear.SelectedValue)
            objTriningSchedling._Iscancel = objTriningSchedling._Iscancel
            objTriningSchedling._Cancellationdate = objTriningSchedling._Cancellationdate
            objTriningSchedling._Cancellationreason = txtCancelRemark.Text
            objTriningSchedling._Cancellationuserunkid = objTriningSchedling._Cancellationuserunkid

            'S.SANDEEP [ 24 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES [<TRAINING>]
            objTriningSchedling._Actual_Accomodation = txtActualAccomodationAmount.Decimal
            objTriningSchedling._Actual_Allowance = txtActualAllowanceAmount.Decimal
            objTriningSchedling._Actual_Exam = txtActualExamAmount.Decimal
            objTriningSchedling._Actual_Fees = txtActualTraningFee.Decimal
            objTriningSchedling._Actual_Material = txtActualMaterialAmount.Decimal
            objTriningSchedling._Actual_Meals = txtActualMealAmount.Decimal
            objTriningSchedling._Actual_Misc = txtActualMiscAmount.Decimal
            objTriningSchedling._Actual_Travel = txtActualTravelAmount.Decimal
            Dim StrSources As String = String.Empty
            For Each LvItems As ListViewItem In lvSourceFunding.CheckedItems
                StrSources &= "," & LvItems.Tag.ToString
            Next
            If StrSources.Trim.Length > 0 Then StrSources = Mid(StrSources, 2)
            objTriningSchedling._Fundingids = StrSources
            'S.SANDEEP [ 24 FEB 2012 ] -- END 
            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            objTriningSchedling._TrainingAllocationTypeId = mintTrainingAllocTypeId
            'S.SANDEEP [08-FEB-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'txtAccomodationAmount.Double = objTriningSchedling._Accomodation
            'txtAllowanceAmount.Double = objTriningSchedling._Allowance

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            txtAccomodationAmount.Tag = objTriningSchedling._Accomodation
            txtAllowanceAmount.Tag = objTriningSchedling._Allowance
            'S.SANDEEP [ 05 JULY 2011 ] -- END

            txtAccomodationAmount.Text = Format(objTriningSchedling._Accomodation, GUI.fmtCurrency)
            txtAllowanceAmount.Text = Format(objTriningSchedling._Allowance, GUI.fmtCurrency)
            'S.SANDEEP [ 18 May 2011 ] -- END

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'txtCourseTitle.Text = objTriningSchedling._Course_Title
            cboCourseMaster.SelectedValue = CInt(objTriningSchedling._Course_Title)
            'Anjan (10 Feb 2012)-End 



            txtEligibility.Text = objTriningSchedling._Eligibility_Criteria
            If objTriningSchedling._End_Date <> Nothing Then
                dtpEndDate.Value = objTriningSchedling._End_Date
            End If
            If objTriningSchedling._End_Time <> Nothing Then
                dtpEndTime.Value = objTriningSchedling._End_Time
            End If

            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'txtExamAmount.Double = objTriningSchedling._Exam
            'txtTraningFee.Double = objTriningSchedling._Fees
            'txtMaterialAmount.Double = objTriningSchedling._Material
            'txtMealAmount.Double = objTriningSchedling._Meals
            'txtMiscAmount.Double = objTriningSchedling._Misc


            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            txtExamAmount.Tag = objTriningSchedling._Exam
            txtTraningFee.Tag = objTriningSchedling._Fees
            txtMaterialAmount.Tag = objTriningSchedling._Material
            txtMealAmount.Tag = objTriningSchedling._Meals
            txtMiscAmount.Tag = objTriningSchedling._Misc
            'S.SANDEEP [ 05 JULY 2011 ] -- END 

            txtExamAmount.Text = Format(objTriningSchedling._Exam, GUI.fmtCurrency)
            txtTraningFee.Text = Format(objTriningSchedling._Fees, GUI.fmtCurrency)
            txtMaterialAmount.Text = Format(objTriningSchedling._Material, GUI.fmtCurrency)
            txtMealAmount.Text = Format(objTriningSchedling._Meals, GUI.fmtCurrency)
            txtMiscAmount.Text = Format(objTriningSchedling._Misc, GUI.fmtCurrency)
            'S.SANDEEP [ 18 May 2011 ] -- END

            nudAvailablePostion.Value = objTriningSchedling._Postion_No
            cboQualification.SelectedValue = objTriningSchedling._Qualificationgroupunkid
            cboQualifiedAward.SelectedValue = objTriningSchedling._Qualificationunkid
            cboResultGroup.SelectedValue = objTriningSchedling._Resultgroupunkid
            If objTriningSchedling._Start_Date <> Nothing Then
                dtpStartDate.Value = objTriningSchedling._Start_Date
            End If
            If objTriningSchedling._Start_Time <> Nothing Then
                dtpStartTime.Value = objTriningSchedling._Start_Time
            End If
            txtVenue.Text = objTriningSchedling._Traning_Venue
            txtRemark.Text = objTriningSchedling._Training_Remark
            cboInstitute.SelectedValue = objTriningSchedling._Traninginstituteunkid
            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'txtTravelAmount.Double = objTriningSchedling._Travel


            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            txtTravelAmount.Tag = objTriningSchedling._Travel
            'S.SANDEEP [ 05 JULY 2011 ] -- END 
            txtTravelAmount.Text = Format(objTriningSchedling._Travel, GUI.fmtCurrency)

            'S.SANDEEP [ 18 May 2011 ] -- END 
            objTriningSchedling._Userunkid = objTriningSchedling._Userunkid
            objTriningSchedling._Isvoid = objTriningSchedling._Isvoid
            objTriningSchedling._Voiddatetime = objTriningSchedling._Voiddatetime
            objTriningSchedling._Voidreason = objTriningSchedling._Voidreason
            objTriningSchedling._Voiduserunkid = objTriningSchedling._Voiduserunkid
            cboTraningYear.SelectedValue = objTriningSchedling._Yearunkid
            objTriningSchedling._Iscancel = objTriningSchedling._Iscancel
            objTriningSchedling._Cancellationdate = objTriningSchedling._Cancellationdate
            txtCancelRemark.Text = objTriningSchedling._Cancellationreason
            objTriningSchedling._Cancellationuserunkid = objTriningSchedling._Cancellationuserunkid

            'S.SANDEEP [ 24 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES [<TRAINING>]
            txtActualAccomodationAmount.Text = Format(objTriningSchedling._Actual_Accomodation, GUI.fmtCurrency)
            txtActualAllowanceAmount.Text = Format(objTriningSchedling._Actual_Allowance, GUI.fmtCurrency)
            txtActualExamAmount.Text = Format(objTriningSchedling._Actual_Exam, GUI.fmtCurrency)
            txtActualTraningFee.Text = Format(objTriningSchedling._Actual_Fees, GUI.fmtCurrency)
            txtActualMaterialAmount.Text = Format(objTriningSchedling._Actual_Material, GUI.fmtCurrency)
            txtActualMealAmount.Text = Format(objTriningSchedling._Actual_Meals, GUI.fmtCurrency)
            txtActualMiscAmount.Text = Format(objTriningSchedling._Actual_Misc, GUI.fmtCurrency)
            txtActualTravelAmount.Text = Format(objTriningSchedling._Actual_Travel, GUI.fmtCurrency)
            'S.SANDEEP [ 24 FEB 2012 ] -- END 

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            mintTrainingAllocTypeId = objTriningSchedling._TrainingAllocationTypeId
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If mintTrainingSchedulingUnkid = -1 Then
                tabcRemark.TabPages.Remove(tabpCancellationRemark)
                btnCancel.Visible = False
                btnMakeActive.Visible = False
                'Sandeep ( 18 JAN 2011 ) -- START
                If FinancialYear._Object._Database_Start_Date <> Nothing Then
                    dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
                End If
                'Sandeep ( 18 JAN 2011 ) -- END 
            Else
                If objTriningSchedling._Iscancel = True Then
                    btnMakeActive.Visible = True
                    btnCancel.Visible = False
                Else
                    btnCancel.Visible = True
                    btnMakeActive.Visible = False
                End If
            End If
            objbtnAddInstitute.Enabled = User._Object.Privilege._AddTrainingInstitute
            objbtnAddQGroup.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddQualification.Enabled = User._Object.Privilege._AddQualification_Course
            objbtnAddResultGroup.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddResources.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If txtCourseTitle.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Title cannot be blank. Title is compulsory information."), enMsgBoxStyle.Information)
            '    txtCourseTitle.Focus()
            '    Return False
            'End If
            'Anjan (10 Feb 2012)-End 



            If CInt(cboTraningYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Training Year is compulsory information. Please select Training Year to continue."), enMsgBoxStyle.Information)
                cboTraningYear.Focus()
                Return False
            End If

            If CInt(cboQualification.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Training Course is compulsory information. Please select Training Course to continue."), enMsgBoxStyle.Information)
                cboQualification.Focus()
                Return False
            End If

            If CInt(cboQualifiedAward.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Qualify Award is complusory information. Please select Qualify Award to continue."), enMsgBoxStyle.Information)
                cboQualifiedAward.Focus()
                Return False
            End If

            If CInt(cboResultGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Result Group is compulsory information. Please select Result Group to continue."), enMsgBoxStyle.Information)
                cboResultGroup.Focus()
                Return False
            End If

            If nudAvailablePostion.Value <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "No. of Seats cannot be set to zero. Please set appropriate available seats."), enMsgBoxStyle.Information)
                nudAvailablePostion.Focus()
                Return False
            End If

            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'If txtTraningFee.Text.Trim = "" Or txtTraningFee.Double = 0 Then

            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS , this they wanted to be optional.

            'If txtTraningFee.Text.Trim = "" Or txtTraningFee.Decimal = 0 Then
            '    'S.SANDEEP [ 18 May 2011 ] -- END
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Training Fee cannot be blank. Training Fee is compulsory information."), enMsgBoxStyle.Information)
            '    txtTraningFee.Focus()
            '    Return False
            'End If
            'Anjan (21 Nov 2011)-End 



            'Sandeep [ 01 FEB 2011 ] -- START
            ''Sandeep | 04 JAN 2010 | -- Start
            'If (CDate(dtpEndTime.Tag.ToString) & " " & dtpEndTime.Value.ToShortTimeString) <= (CDate(dtpStartTime.Tag.ToString) & " " & dtpStartTime.Value.ToShortTimeString) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "End time cannot be less than or equal to Start time."), enMsgBoxStyle.Information)
            '    dtpEndTime.Focus()
            '    Return False
            'End If
            ''If CDate(dtpEndTime.Value.ToShortTimeString) <= CDate(dtpStartTime.Value.ToShortTimeString) Then

            ''End If
            ''Sandeep | 04 JAN 2010 | -- END 
            If dtpEndDate.Value.Date < dtpStartDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "End date cannot be less than Start Date."), enMsgBoxStyle.Information)
                dtpEndDate.Focus()
                Return False
            ElseIf dtpEndDate.Value.Date = dtpStartDate.Value.Date Then

                'S.SANDEEP [11 AUG 2015] -- START
                'If dtpEndTime.Value.ToShortTimeString <= dtpStartTime.Value.ToShortTimeString Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "End time cannot be less than or equal to Start time."), enMsgBoxStyle.Information)
                '    dtpEndTime.Focus()
                '    Return False
                'End If

                If dtpEndTime.Value <= dtpStartTime.Value Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "End time cannot be less than or equal to Start time."), enMsgBoxStyle.Information)
                    dtpEndTime.Focus()
                    Return False
                End If
                'S.SANDEEP [11 AUG 2015] -- END

            End If


            'Sandeep [ 01 FEB 2011 ] -- END 



            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

#Region " Training Resources "
    Private Sub FillResources()
        Try
            lvResources.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtResources.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    Dim objComm_Mster As New clsCommon_Master
                    objComm_Mster._Masterunkid = CInt(dtRow.Item("resourceunkid"))
                    lvItem.Text = objComm_Mster._Name
                    objComm_Mster = Nothing
                    lvItem.SubItems.Add(dtRow.Item("resources_remark").ToString)
                    lvItem.SubItems.Add(dtRow.Item("resourceunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

                    lvItem.Tag = dtRow.Item("resourcestranunkid")

                    lvResources.Items.Add(lvItem)

                    lvItem = Nothing
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetItems()
        Try
            cboResources.SelectedValue = 0
            txtResourseRemark.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Training Trainers "
    Private Sub FillTrainers()
        Try
            lvTrainerInfo.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtTrainers.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    If CInt(dtRow.Item("employeeunkid")) <> -1 Then
                        lvItem.Text = dtRow.Item("trainer_level_id").ToString

                        Dim objEmp As New clsEmployee_Master

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp._Employeeunkid = CInt(dtRow.Item("employeeunkid"))
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dtRow.Item("employeeunkid"))
                        'S.SANDEEP [04 JUN 2015] -- END

                        lvItem.SubItems.Add(objEmp._Firstname & " " & objEmp._Surname)
                        Dim objDept As New clsDepartment
                        objDept._Departmentunkid = objEmp._Departmentunkid
                        lvItem.SubItems.Add(objDept._Name.ToString)
                        objDept = Nothing
                        lvItem.SubItems.Add(Company._Object._Name.ToString)
                        lvItem.SubItems.Add(objEmp._Present_Tel_No)
                        lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                        lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                        lvItem.SubItems.Add(dtRow.Item("trainer_level_id").ToString)
                        objEmp = Nothing
                    Else
                        lvItem.Text = dtRow.Item("trainer_level_id").ToString
                        lvItem.SubItems.Add(dtRow.Item("other_name").ToString)
                        lvItem.SubItems.Add(dtRow.Item("other_position").ToString)
                        lvItem.SubItems.Add(dtRow.Item("other_company").ToString)
                        lvItem.SubItems.Add(dtRow.Item("other_contactno").ToString)
                        lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                        lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    End If

                    lvItem.Tag = dtRow.Item("trainerstranunkid")

                    lvTrainerInfo.Items.Add(lvItem)

                    lvItem = Nothing
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillTrainers", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetTrainers()
        Try
            radEmployee.Checked = True
            cboEmployee.SelectedValue = 0
            objlblDepartmentValue.Text = ""
            objlblCompanyValue.Text = ""
            objlblEmployeeContactNo.Text = ""
            radOthers.Checked = False
            txtName.Text = ""
            txtPosition.Text = ""
            txtComany.Text = ""
            txtTrainersContactNo.Text = ""
            nudLevel.Value = 1
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetTrainers", mstrModuleName)
        End Try
    End Sub
#End Region

    'S.SANDEEP [ 24 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES [<TRAINING>]
#Region " Budgets & Fundings "

    Private Sub Sources_List()
        Dim dsFunding As New DataSet
        Dim objCMaster As New clsCommon_Master
        Dim lvItem As ListViewItem
        Try
            dsFunding = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS, False, "List")
            lvSourceFunding.Items.Clear()
            For Each dRow As DataRow In dsFunding.Tables("List").Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("name").ToString
                lvItem.Tag = dRow.Item("masterunkid")

                If mintTrainingSchedulingUnkid > 0 Then
                    If objTriningSchedling._Fundingids.Trim.Length > 0 Then
                        For Each StrId As String In objTriningSchedling._Fundingids.Split(CChar(","))
                            If CInt(dRow.Item("masterunkid")) = CInt(StrId) Then lvItem.Checked = True
                        Next
                    End If
                End If

                lvSourceFunding.Items.Add(lvItem)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Sources_List", mstrModuleName)
        Finally
            dsFunding.Dispose() : objCMaster = Nothing : lvItem = Nothing
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 24 FEB 2012 ] -- END 

#End Region

#Region " Form's Events "
    Private Sub frmCourseScheduling_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objTriningSchedling = Nothing
    End Sub

    Private Sub frmCourseScheduling_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmCourseScheduling_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmCourseScheduling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTriningSchedling = New clsTraining_Scheduling
        objResourcesTran = New clsTraining_Resources_Tran
        objTrainersTran = New clsTraining_Trainers_Tran
        'S.SANDEEP [08-FEB-2017] -- START
        'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
        objAllocation = New clsTraining_Allocation_Tran
        'S.SANDEEP [08-FEB-2017] -- END
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objTriningSchedling._Trainingschedulingunkid = mintTrainingSchedulingUnkid
            End If

            Call GetValue()

            Call SetVisibility()

            objResourcesTran._Training_SchedulingUnkid = mintTrainingSchedulingUnkid
            mdtResources = objResourcesTran._DataTable
            Call FillResources()

            objTrainersTran._Training_SchedulingUnkid = mintTrainingSchedulingUnkid
            mdtTrainers = objTrainersTran._DataTable
            Call FillTrainers()

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            objAllocation._Training_SchedulingUnkid = mintTrainingSchedulingUnkid
            mdtAllocTran = objAllocation._DataTable
            'S.SANDEEP [08-FEB-2017] -- END


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'txtCourseTitle.Focus()
            cboCourseMaster.Focus()

            'Anjan (10 Feb 2012)-End 


            'S.SANDEEP [ 24 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES [<TRAINING>]
            Call Sources_List()
            lvSourceFunding.GridLines = False
            'S.SANDEEP [ 24 FEB 2012 ] -- END 


            mblnFormLoad = True
            radEmployee.Checked = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCourseScheduling_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Scheduling.SetMessages()
            clsTraining_Trainers_Tran.SetMessages()

            objfrm._Other_ModuleNames = "clsTraining_Scheduling,clsTraining_Trainers_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If txtCancelRemark.Text.Trim = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please write cancellation remark in order to cancel Training Scheduling."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Dim blnFlag As Boolean = False
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Are you sure you want to cancel schedule for this training?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            objTriningSchedling._Iscancel = True
            objTriningSchedling._Cancellationdate = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
            objTriningSchedling._Cancellationreason = txtCancelRemark.Text
            objTriningSchedling._Cancellationuserunkid = User._Object._Userunkid

            blnFlag = objTriningSchedling.Cancel(mintTrainingSchedulingUnkid)

            If blnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You have successfully cancelled the scheduled training."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            Else
                eZeeMsgBox.Show(objTriningSchedling._Message, enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMakeActive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMakeActive.Click
        Dim blnFlag As Boolean = False
        Try
            objTriningSchedling._Iscancel = False
            objTriningSchedling._Cancellationdate = Nothing
            objTriningSchedling._Cancellationreason = ""
            objTriningSchedling._Cancellationuserunkid = -1

            blnFlag = objTriningSchedling.MakeActive(mintTrainingSchedulingUnkid)

            If blnFlag Then
                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You have sucessfully cancelled the scheduled trainig."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You have succcessfully cancelled the scheduled training."), enMsgBoxStyle.Information)
                'Sandeep [ 09 Oct 2010 ] -- End 
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMakeActive_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            'Sandeep [ 30 September 2010 ] -- Start
            If IsValid() = False Then Exit Sub
            'Sandeep [ 30 September 2010 ] -- End

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                'S.SANDEEP [08-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                '    blnFlag = objTriningSchedling.Update(mdtResources, mdtTrainers)
                'Else
                '    blnFlag = objTriningSchedling.Insert(mdtResources, mdtTrainers)
                blnFlag = objTriningSchedling.Update(mdtResources, mdtTrainers, mdtAllocTran)
            Else
                blnFlag = objTriningSchedling.Insert(mdtResources, mdtTrainers, mdtAllocTran)
                'S.SANDEEP [08-FEB-2017] -- END
            End If

            If blnFlag = False And objTriningSchedling._Message <> "" Then
                eZeeMsgBox.Show(objTriningSchedling._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objTriningSchedling = Nothing
                    objTriningSchedling = New clsTraining_Scheduling
                    Call GetValue()


                    'Anjan (10 Feb 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'txtCourseTitle.Focus()
                    cboCourseMaster.Focus()

                    'Anjan (10 Feb 2012)-End 

                Else
                    mintTrainingSchedulingUnkid = objTriningSchedling._Trainingschedulingunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 23 Oct 2010 ] -- Start
    Private Sub objbtnAddInstitute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInstitute.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmTraningInstitutes_AddEdit

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            frm.displayDialog(intRefId, enAction.ADD_ONE, False)

            If intRefId > -1 Then
                Dim objInstitute As New clsinstitute_master
                Dim dsList As New DataSet
                dsList = objInstitute.getListForCombo(False, "Training", True)
                With cboInstitute
                    .ValueMember = "instituteunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Training")
                    .SelectedValue = intRefId
                End With
                objInstitute = Nothing
                dsList = Nothing
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddInstitute_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQualification.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmQualificationCourse_AddEdit
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objQualification As New clsqualification_master
                Dim dsList As New DataSet
                dsList = objQualification.GetComboList("Qualification", True, CInt(cboQualification.SelectedValue))
                With cboQualifiedAward
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Qualification")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddQualification_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddQGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQGroup.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmCommonMaster
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objQGroup As New clsCommon_Master
                Dim dsList As New DataSet
                dsList = objQGroup.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QGroup")
                With cboQualification
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("QGroup")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddQGroup_Click", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 23 Oct 2010 ] -- End 

#Region " Traning Resources "
    Private Sub btnAddResources_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddResources.Click
        If CInt(cboResources.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Resource is compulsory information, it can not be blank. Please select Resource to continue."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Dim dtRow As DataRow() = mdtResources.Select("resourceunkid = " & CInt(cboResources.SelectedValue) & " AND AUD <> 'D' ")
        If dtRow.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Selected Resource is already added to the list."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            Dim dtIdRow As DataRow
            dtIdRow = mdtResources.NewRow

            dtIdRow.Item("resourcestranunkid") = -1
            dtIdRow.Item("trainingschedulingunkid") = mintTrainingSchedulingUnkid
            dtIdRow.Item("resourceunkid") = CInt(cboResources.SelectedValue)
            dtIdRow.Item("resources_remark") = txtResourseRemark.Text
            dtIdRow.Item("iscancel") = False
            dtIdRow.Item("cancellationuserunkid") = -1
            dtIdRow.Item("cancellationreason") = ""
            dtIdRow.Item("cancellationdate") = DBNull.Value
            dtIdRow.Item("isvoid") = False
            dtIdRow.Item("voiddatetime") = DBNull.Value
            dtIdRow.Item("voiduserunkid") = -1
            dtIdRow.Item("voidreason") = ""
            dtIdRow.Item("AUD") = "A"
            dtIdRow.Item("GUID") = Guid.NewGuid.ToString

            mdtResources.Rows.Add(dtIdRow)

            Call FillResources()
            Call ResetItems()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddResources_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteResources_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteResources.Click
        Try
            If lvResources.SelectedItems.Count > 0 Then
                If intResourceSelectedIdx > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvResources.Items(intResourceSelectedIdx).Tag) = -1 Then
                        drTemp = mdtResources.Select("GUID = '" & lvResources.Items(intResourceSelectedIdx).SubItems(objcolhGUID.Index).Text & "'")
                    Else
                        drTemp = mdtResources.Select("resourcestranunkid = '" & CInt(lvResources.Items(intResourceSelectedIdx).Tag) & "'")
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        Call FillResources()
                    End If
                End If
            End If
            Call ResetItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteResources_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvResources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvResources.Click
        Try
            If lvResources.SelectedItems.Count > 0 Then
                intResourceSelectedIdx = lvResources.SelectedItems(0).Index
                cboResources.SelectedValue = 0
                cboResources.SelectedValue = CInt(lvResources.SelectedItems(0).SubItems(objcolhResourceUnkid.Index).Text)
                txtResourseRemark.Text = lvResources.SelectedItems(0).SubItems(colhRemark.Index).Text
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvResources_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddResources_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResources.Click
        Dim frm As New frmCommonMaster
        Dim dsList As New DataSet
        Dim objCommon As New clsCommon_Master
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.TRAINING_RESOURCES, enAction.ADD_ONE)
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_RESOURCES, True, "Resources")
            With cboResources
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Resources")
                .SelectedValue = intRefId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddResources_Click", mstrModuleName)
        Finally
            dsList = Nothing
            objCommon = Nothing
        End Try
    End Sub
#End Region

#Region " Training Trainers "
    Private Sub btnAddTrainer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddTrainer.Click
        If radEmployee.Checked = True Then
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Employee is compulsory information, it cannot be blank. Please select an Employee to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim dtRow As DataRow() = mdtTrainers.Select("employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND AUD <> 'D' ")
            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Selected Employee is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        ElseIf radOthers.Checked = True Then
            If txtName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Name is compulsory information, it can not be blank. Please enter Name to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim dtRow As DataRow() = mdtTrainers.Select("other_name LIKE '" & txtName.Text & "' AND AUD <> 'D' ")
            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Entered Trainer is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        'Dim dtCRow As DataRow() = mdtTrainers.Select("other_company = '" & Company._Object._Name & "' AND AUD <> 'D' ")
        'If dtCRow.Length > 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Comapany is already added to the list."), enMsgBoxStyle.Information)
        '    Exit Sub
        'End If

        If txtComany.Text.Trim = Company._Object._Name Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Company cannot be same. Please enter another company name."), enMsgBoxStyle.Information)
            txtComany.Focus()
            Exit Sub
        End If

        Dim dtLevelRow As DataRow() = mdtTrainers.Select("trainer_level_id = '" & CDec(nudLevel.Value) & "' AND AUD <> 'D'")
        If dtLevelRow.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Selected Level is already assigned to employee. Please assigned new level."), enMsgBoxStyle.Information)
            nudLevel.Focus()
            Exit Sub
        End If

        Try
            Dim dtTRow As DataRow
            dtTRow = mdtTrainers.NewRow

            dtTRow.Item("trainerstranunkid") = -1
            dtTRow.Item("trainingschedulingunkid") = mintTrainingSchedulingUnkid
            If radEmployee.Checked = True Then
                dtTRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            Else
                dtTRow.Item("employeeunkid") = -1
            End If
            dtTRow.Item("other_name") = txtName.Text
            dtTRow.Item("other_position") = txtPosition.Text
            dtTRow.Item("other_company") = txtComany.Text
            dtTRow.Item("other_contactno") = txtTrainersContactNo.Text
            dtTRow.Item("iscancel") = False
            dtTRow.Item("cancellationuserunkid") = -1
            dtTRow.Item("cancellationreason") = ""
            dtTRow.Item("cancellationdate") = DBNull.Value
            dtTRow.Item("isvoid") = False
            dtTRow.Item("voiddatetime") = DBNull.Value
            dtTRow.Item("voiduserunkid") = -1
            dtTRow.Item("voidreason") = ""
            dtTRow.Item("AUD") = "A"
            dtTRow.Item("GUID") = Guid.NewGuid.ToString
            dtTRow.Item("trainer_level_id") = nudLevel.Value

            mdtTrainers.Rows.Add(dtTRow)

            Call FillTrainers()
            Call ResetTrainers()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddTrainer_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditTrainer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditTrainer.Click
        Try
            If lvTrainerInfo.SelectedItems.Count > 0 Then
                If intTrainersSelectedIdx > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvTrainerInfo.Items(intTrainersSelectedIdx).Tag) = -1 Then
                        drTemp = mdtTrainers.Select("GUID = '" & lvTrainerInfo.Items(intTrainersSelectedIdx).SubItems(objcolhTrainerGUID.Index).Text & "'")
                    Else
                        drTemp = mdtTrainers.Select("trainerstranunkid = " & CInt(lvTrainerInfo.Items(intTrainersSelectedIdx).Tag))
                    End If
                    Dim dtLevelRow As DataRow() = mdtTrainers.Select("trainer_level_id = '" & CInt(nudLevel.Value) & "' AND AUD <> 'D'")
                    If dtLevelRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Selected Level is already assigned to employee. Please assigned new level."), enMsgBoxStyle.Information)
                        nudLevel.Focus()
                        Exit Sub
                    End If

                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("trainerstranunkid") = lvTrainerInfo.Items(intTrainersSelectedIdx).Tag
                            .Item("trainingschedulingunkid") = mintTrainingSchedulingUnkid
                            If CInt(.Item("employeeunkid")) <> -1 Then
                                .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                            Else
                                .Item("employeeunkid") = -1
                            End If
                            .Item("other_name") = txtName.Text
                            .Item("other_position") = txtPosition.Text
                            .Item("other_company") = txtComany.Text
                            .Item("other_contactno") = txtTrainersContactNo.Text
                            .Item("iscancel") = False
                            .Item("cancellationuserunkid") = -1
                            .Item("cancellationreason") = ""
                            .Item("cancellationdate") = DBNull.Value
                            .Item("isvoid") = False
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voiduserunkid") = -1
                            .Item("voidreason") = ""
                            .Item("GUID") = Guid.NewGuid().ToString
                            .Item("trainer_level_id") = nudLevel.Value
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()
                        End With
                        Call FillTrainers()
                    End If
                End If
                Call ResetTrainers()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditTrainer_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteTrainer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteTrainer.Click
        Try
            If lvTrainerInfo.SelectedItems.Count > 0 Then
                If intTrainersSelectedIdx > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvTrainerInfo.Items(intTrainersSelectedIdx).Tag) = -1 Then
                        drTemp = mdtTrainers.Select("GUID = '" & lvTrainerInfo.Items(intTrainersSelectedIdx).SubItems(objcolhTrainerGUID.Index).Text & "'")
                    Else
                        'S.SANDEEP [ 12 OCT 2011 ] -- START
                        Dim objAnalysisTran As New clsTraining_Analysis_Tran
                        If objAnalysisTran.CheckTrainerTrans(CInt(lvTrainerInfo.Items(intTrainersSelectedIdx).Tag)) = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot remove this Trainer. Reason : This trainer is already linked with some of the transactions."))
                            Call ResetTrainers()
                            Exit Sub
                        End If
                        'S.SANDEEP [ 12 OCT 2011 ] -- END 
                        drTemp = mdtTrainers.Select("trainerstranunkid = '" & CInt(lvTrainerInfo.Items(intTrainersSelectedIdx).Tag) & "'")
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        Call FillTrainers()
                    End If
                End If
            End If
            Call ResetTrainers()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteTrainer_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvTrainerInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvTrainerInfo.Click
        Try
            If lvTrainerInfo.SelectedItems.Count > 0 Then
                intTrainersSelectedIdx = lvTrainerInfo.SelectedItems(0).Index
                cboEmployee.SelectedValue = 0
                If CInt(lvTrainerInfo.SelectedItems(0).SubItems(objcolhEmpunkid.Index).Text) <> -1 Then
                    radEmployee.Checked = True
                    cboEmployee.SelectedValue = CInt(lvTrainerInfo.SelectedItems(0).SubItems(objcolhEmpunkid.Index).Text)
                Else
                    radOthers.Checked = True
                    txtName.Text = lvTrainerInfo.SelectedItems(0).SubItems(colhEmpName.Index).Text
                    txtPosition.Text = lvTrainerInfo.SelectedItems(0).SubItems(colhDepartment.Index).Text
                    txtComany.Text = lvTrainerInfo.SelectedItems(0).SubItems(colhCompany.Index).Text
                    txtTrainersContactNo.Text = lvTrainerInfo.SelectedItems(0).SubItems(colhContactNo.Index).Text
                End If
                If lvTrainerInfo.SelectedItems(0).SubItems(colhLevel.Index).Text = "" Then
                    nudLevel.Value = nudLevel.Value
                Else
                    nudLevel.Value = CDec(lvTrainerInfo.SelectedItems(0).SubItems(colhLevel.Index).Text)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTrainerInfo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#End Region

#Region " Controls "
    Private Sub cboCourse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQualification.SelectedIndexChanged
        Try
            If CInt(cboQualification.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objQualif As New clsqualification_master
                dsList = objQualif.GetComboList("Qualif", True, CInt(cboQualification.SelectedValue))
                With cboQualifiedAward
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Qualif")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCourse_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtTraningFee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTraningFee.TextChanged, txtMiscAmount.TextChanged, txtTravelAmount.TextChanged, txtAccomodationAmount.TextChanged, txtAllowanceAmount.TextChanged, txtMealAmount.TextChanged, txtMaterialAmount.TextChanged, txtExamAmount.TextChanged
        Try
            'If mblnFormLoad = True Then

            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'txtFinalAmount.Double = txtTraningFee.Double + txtMiscAmount.Double + txtTravelAmount.Double + txtAccomodationAmount.Double + txtAllowanceAmount.Double + txtMealAmount.Double + txtMaterialAmount.Double + txtExamAmount.Double

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'txtFinalAmount.Decimal = txtTraningFee.Decimal + txtMiscAmount.Decimal + txtTravelAmount.Decimal + txtAccomodationAmount.Decimal + txtAllowanceAmount.Decimal + txtMealAmount.Decimal + txtMaterialAmount.Decimal + txtExamAmount.Decimal
            If mintTrainingSchedulingUnkid > 0 Then
                txtTraningFee.Tag = txtTraningFee.Decimal : txtTravelAmount.Tag = txtTravelAmount.Decimal : txtAccomodationAmount.Tag = txtAccomodationAmount.Decimal : txtAllowanceAmount.Tag = txtAllowanceAmount.Decimal : txtMealAmount.Tag = txtMealAmount.Decimal : txtMaterialAmount.Tag = txtMaterialAmount.Decimal : txtExamAmount.Tag = txtExamAmount.Decimal
                txtFinalAmount.Decimal = CDec(txtTraningFee.Tag) + CDec(txtMiscAmount.Tag) + CDec(txtTravelAmount.Tag) + CDec(txtAccomodationAmount.Tag) + CDec(txtAllowanceAmount.Tag) + CDec(txtMealAmount.Tag) + CDec(txtMaterialAmount.Tag) + CDec(txtExamAmount.Tag)
            Else
                txtFinalAmount.Decimal = CDec(txtTraningFee.Decimal) + CDec(txtMiscAmount.Decimal) + CDec(txtTravelAmount.Decimal) + CDec(txtAccomodationAmount.Decimal) + CDec(txtAllowanceAmount.Decimal) + CDec(txtMealAmount.Decimal) + CDec(txtMaterialAmount.Decimal) + CDec(txtExamAmount.Decimal)
            End If
            txtFinalAmount.Text = Format(txtFinalAmount.Decimal, GUI.fmtCurrency)
            'S.SANDEEP [ 05 JULY 2011 ] -- END 

            'S.SANDEEP [ 18 May 2011 ] -- END 
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub radEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radEmployee.CheckedChanged, radOthers.CheckedChanged
        Try
            Dim radbtn As RadioButton = CType(sender, RadioButton)
            If radbtn.Name = radEmployee.Name Then
                txtName.Text = ""
                txtPosition.Text = ""
                txtComany.Text = ""
                txtTrainersContactNo.Text = ""
                cboEmployee.Enabled = True
                pnlOthers.Enabled = False
                'Sandeep [ 30 September 2010 ] -- Start
                objbtnSearchEmployee.Enabled = True
                'Sandeep [ 30 September 2010 ] -- End
            ElseIf radbtn.Name = radOthers.Name Then
                pnlOthers.Enabled = True
                cboEmployee.SelectedValue = 0
                cboEmployee.Enabled = False
                'Sandeep [ 30 September 2010 ] -- Start
                objbtnSearchEmployee.Enabled = False
                'Sandeep [ 30 September 2010 ] -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                Dim objDept As New clsDepartment

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                objDept._Departmentunkid = objEmp._Departmentunkid
                objlblDepartmentValue.Text = objDept._Name
                objlblCompanyValue.Text = Company._Object._Name
                objlblEmployeeContactNo.Text = objEmp._Present_Tel_No
            Else
                objlblDepartmentValue.Text = ""
                objlblCompanyValue.Text = ""
                objlblEmployeeContactNo.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 16 Oct 2010 ] -- Start
    Private Sub cboInstitute_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboInstitute.SelectedIndexChanged
        Try
            If CInt(cboInstitute.SelectedValue) > 0 Then
                Dim objInstitute As New clsinstitute_master
                objInstitute._Instituteunkid = CInt(cboInstitute.SelectedValue)
                txtContactPerson.Text = objInstitute._Contact_Person
                txtContactNo.Text = objInstitute._Telephoneno
                'S.SANDEEP [19-MAY-2017] -- START
                If txtContactNo.Text.Trim.Length <= 0 Then txtContactNo.Text = objTriningSchedling._Contact_No
                If txtContactPerson.Text.Trim.Length <= 0 Then txtContactPerson.Text = objTriningSchedling._Contact_Person
                'S.SANDEEP [19-MAY-2017] -- END
            Else
                'S.SANDEEP [19-MAY-2017] -- START
                'txtContactPerson.Text = ""
                'txtContactNo.Text = ""
                txtContactNo.Text = objTriningSchedling._Contact_No
                txtContactPerson.Text = objTriningSchedling._Contact_Person
                'S.SANDEEP [19-MAY-2017] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboInstitute_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 16 Oct 2010 ] -- End 

    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub txtActualTraningFee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtActualTraningFee.TextChanged, txtActualMiscAmount.TextChanged, txtActualTravelAmount.TextChanged, txtActualAccomodationAmount.TextChanged, txtActualAllowanceAmount.TextChanged, txtActualMealAmount.TextChanged, txtActualMaterialAmount.TextChanged, txtActualExamAmount.TextChanged
        Try
            If mintTrainingSchedulingUnkid > 0 Then
                txtActualTraningFee.Tag = txtActualTraningFee.Decimal : txtActualTravelAmount.Tag = txtActualTravelAmount.Decimal : txtActualAccomodationAmount.Tag = txtActualAccomodationAmount.Decimal : txtActualAllowanceAmount.Tag = txtActualAllowanceAmount.Decimal : txtActualMealAmount.Tag = txtActualMealAmount.Decimal : txtActualMaterialAmount.Tag = txtActualMaterialAmount.Decimal : txtActualExamAmount.Tag = txtActualExamAmount.Decimal
                txtActualFinalAmount.Decimal = CDec(txtActualTraningFee.Tag) + CDec(txtActualMiscAmount.Tag) + CDec(txtActualTravelAmount.Tag) + CDec(txtActualAccomodationAmount.Tag) + CDec(txtActualAllowanceAmount.Tag) + CDec(txtActualMealAmount.Tag) + CDec(txtActualMaterialAmount.Tag) + CDec(txtActualExamAmount.Tag)
            Else
                txtActualFinalAmount.Decimal = CDec(txtActualTraningFee.Decimal) + CDec(txtActualMiscAmount.Decimal) + CDec(txtActualTravelAmount.Decimal) + CDec(txtActualAccomodationAmount.Decimal) + CDec(txtActualAllowanceAmount.Decimal) + CDec(txtActualMealAmount.Decimal) + CDec(txtActualMaterialAmount.Decimal) + CDec(txtActualExamAmount.Decimal)
            End If
            txtActualFinalAmount.Text = Format(txtActualFinalAmount.Decimal, GUI.fmtCurrency)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtActualTraningFee_TextChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 18 FEB 2012 ] -- END

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT :
    Private Sub lnkBindAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkBindAllocation.LinkClicked
        Dim frm As New frmBindAllocation
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(mintTrainingAllocTypeId, mdtAllocTran, menAction, mintTrainingSchedulingUnkid)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [08-FEB-2017] -- End

#End Region

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private Sub objbtnAddResultGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResultGroup.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.RESULT_GROUP, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "RGRP")
                With cboResultGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("RGRP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddResultGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 


    'Sandeep | 04 JAN 2010 | -- Start
    Private Sub dtpEndDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpEndDate.ValueChanged
        Try
            dtpEndTime.Tag = dtpEndDate.Value.Date
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpEndDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dtpStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged
        Try
            dtpStartTime.Tag = dtpStartDate.Value.Date
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartDate_ValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep | 04 JAN 2010 | -- END 

    'Anjan (10 Feb 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private Sub objbtnAddCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCourse.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "CM")
                With cboResultGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("CM")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCourse_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan (10 Feb 2012)-End 

    Private Sub objbtnSearchCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCourse.Click, _
                                                                                                             objbtnSearchQGrp.Click, _
                                                                                                             objbtnSearchQualification.Click, _
                                                                                                             objbtnSearchResult.Click, _
                                                                                                             objbtnSearchInstitute.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DataSource = Nothing
                Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                    Case "OBJBTNSEARCHCOURSE"
                        .ValueMember = cboCourseMaster.ValueMember
                        .DisplayMember = cboCourseMaster.DisplayMember
                        .DataSource = CType(cboCourseMaster.DataSource, DataTable)
                    Case "OBJBTNSEARCHQGRP"
                        .ValueMember = cboQualification.ValueMember
                        .DisplayMember = cboQualification.DisplayMember
                        .DataSource = CType(cboQualification.DataSource, DataTable)
                    Case "OBJBTNSEARCHQUALIFICATION"
                        If cboQualifiedAward.DataSource Is Nothing Then Exit Sub
                        .ValueMember = cboQualifiedAward.ValueMember
                        .DisplayMember = cboQualifiedAward.DisplayMember
                        .DataSource = CType(cboQualifiedAward.DataSource, DataTable)
                    Case "OBJBTNSEARCHRESULT"
                        .ValueMember = cboResultGroup.ValueMember
                        .DisplayMember = cboResultGroup.DisplayMember
                        .DataSource = CType(cboResultGroup.DataSource, DataTable)
                    Case "OBJBTNSEARCHINSTITUTE"
                        .ValueMember = cboInstitute.ValueMember
                        .DisplayMember = cboInstitute.DisplayMember
                        .DataSource = CType(cboInstitute.DataSource, DataTable)
                End Select
            End With
            If frm.DisplayDialog Then
                Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                    Case "OBJBTNSEARCHCOURSE"
                        cboCourseMaster.SelectedValue = frm.SelectedValue
                        cboCourseMaster.Focus()
                    Case "OBJBTNSEARCHQGRP"
                        cboQualification.SelectedValue = frm.SelectedValue
                        cboQualification.Focus()
                    Case "OBJBTNSEARCHQUALIFICATION"
                        cboQualifiedAward.SelectedValue = frm.SelectedValue
                        cboQualifiedAward.Focus()
                    Case "OBJBTNSEARCHRESULT"
                        cboResultGroup.SelectedValue = frm.SelectedValue
                        cboResultGroup.Focus()
                    Case "OBJBTNSEARCHINSTITUTE"
                        cboInstitute.SelectedValue = frm.SelectedValue
                        cboInstitute.Focus()
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCourse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub



    'S.SANDEEP [ 27 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboQualifiedAward_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboQualifiedAward.SelectedIndexChanged
        Try
            If CInt(cboQualifiedAward.SelectedValue) > 0 Then
                Dim objQMaster As New clsqualification_master
                objQMaster._Qualificationunkid = CInt(cboQualifiedAward.SelectedValue)

                If IsDBNull(objQMaster._ResultGroupunkid) = True Then objQMaster._ResultGroupunkid = 0

                Dim dFTab As DataTable = New DataView(dRView.ToTable, "masterunkid IN(0," & objQMaster._ResultGroupunkid.ToString & ")", "masterunkid", DataViewRowState.CurrentRows).ToTable
                With cboResultGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dFTab
                    .SelectedValue = 0
                End With
            Else
                With cboResultGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dRView.ToTable
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 27 FEB 2012 ] -- END



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbCourseSchedule.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCourseSchedule.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbTrainerInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbTrainerInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbResources.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbResources.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBudgetsFundings.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBudgetsFundings.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddTrainer.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddTrainer.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditTrainer.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditTrainer.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteTrainer.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteTrainer.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddResources.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddResources.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteResources.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteResources.GradientForeColor = GUI._ButttonFontColor

            Me.btnMakeActive.GradientBackColor = GUI._ButttonBackColor
            Me.btnMakeActive.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbCourseSchedule.Text = Language._Object.getCaption(Me.gbCourseSchedule.Name, Me.gbCourseSchedule.Text)
            Me.lblTraningCourse.Text = Language._Object.getCaption(Me.lblTraningCourse.Name, Me.lblTraningCourse.Text)
            Me.lblTraningInstitute.Text = Language._Object.getCaption(Me.lblTraningInstitute.Name, Me.lblTraningInstitute.Text)
            Me.lblTraningYear.Text = Language._Object.getCaption(Me.lblTraningYear.Name, Me.lblTraningYear.Text)
            Me.lblContactPerson.Text = Language._Object.getCaption(Me.lblContactPerson.Name, Me.lblContactPerson.Text)
            Me.lblVenue.Text = Language._Object.getCaption(Me.lblVenue.Name, Me.lblVenue.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblContactNo.Text = Language._Object.getCaption(Me.lblContactNo.Name, Me.lblContactNo.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblMaxCandidate.Text = Language._Object.getCaption(Me.lblMaxCandidate.Name, Me.lblMaxCandidate.Text)
            Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
            Me.tabpCourseScheduling.Text = Language._Object.getCaption(Me.tabpCourseScheduling.Name, Me.tabpCourseScheduling.Text)
            Me.tabpTrainerInfo.Text = Language._Object.getCaption(Me.tabpTrainerInfo.Name, Me.tabpTrainerInfo.Text)
            Me.lblQualificationAward.Text = Language._Object.getCaption(Me.lblQualificationAward.Name, Me.lblQualificationAward.Text)
            Me.gbTrainerInfo.Text = Language._Object.getCaption(Me.gbTrainerInfo.Name, Me.gbTrainerInfo.Text)
            Me.btnAddTrainer.Text = Language._Object.getCaption(Me.btnAddTrainer.Name, Me.btnAddTrainer.Text)
            Me.btnEditTrainer.Text = Language._Object.getCaption(Me.btnEditTrainer.Name, Me.btnEditTrainer.Text)
            Me.btnDeleteTrainer.Text = Language._Object.getCaption(Me.btnDeleteTrainer.Name, Me.btnDeleteTrainer.Text)
            Me.lblTrainerContactNo.Text = Language._Object.getCaption(Me.lblTrainerContactNo.Name, Me.lblTrainerContactNo.Text)
            Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
            Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.radOthers.Text = Language._Object.getCaption(Me.radOthers.Name, Me.radOthers.Text)
            Me.lblEmpContact.Text = Language._Object.getCaption(Me.lblEmpContact.Name, Me.lblEmpContact.Text)
            Me.lblEmpCompany.Text = Language._Object.getCaption(Me.lblEmpCompany.Name, Me.lblEmpCompany.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.lblTraningTitle.Text = Language._Object.getCaption(Me.lblTraningTitle.Name, Me.lblTraningTitle.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.tabpTrainingResources.Text = Language._Object.getCaption(Me.tabpTrainingResources.Name, Me.tabpTrainingResources.Text)
            Me.gbResources.Text = Language._Object.getCaption(Me.gbResources.Name, Me.gbResources.Text)
            Me.colhResource.Text = Language._Object.getCaption(CStr(Me.colhResource.Tag), Me.colhResource.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.btnAddResources.Text = Language._Object.getCaption(Me.btnAddResources.Name, Me.btnAddResources.Text)
            Me.btnDeleteResources.Text = Language._Object.getCaption(Me.btnDeleteResources.Name, Me.btnDeleteResources.Text)
            Me.lblResourcesRemark.Text = Language._Object.getCaption(Me.lblResourcesRemark.Name, Me.lblResourcesRemark.Text)
            Me.lblResources.Text = Language._Object.getCaption(Me.lblResources.Name, Me.lblResources.Text)
            Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
            Me.colhContactNo.Text = Language._Object.getCaption(CStr(Me.colhContactNo.Tag), Me.colhContactNo.Text)
            Me.tabpEligibilityInfo.Text = Language._Object.getCaption(Me.tabpEligibilityInfo.Name, Me.tabpEligibilityInfo.Text)
            Me.tabpTrainingRemark.Text = Language._Object.getCaption(Me.tabpTrainingRemark.Name, Me.tabpTrainingRemark.Text)
            Me.tabpCancellationRemark.Text = Language._Object.getCaption(Me.tabpCancellationRemark.Name, Me.tabpCancellationRemark.Text)
            Me.btnMakeActive.Text = Language._Object.getCaption(Me.btnMakeActive.Name, Me.btnMakeActive.Text)
            Me.lblTrainerLevel.Text = Language._Object.getCaption(Me.lblTrainerLevel.Name, Me.lblTrainerLevel.Text)
            Me.lblLevelCaption.Text = Language._Object.getCaption(Me.lblLevelCaption.Name, Me.lblLevelCaption.Text)
            Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
            Me.tabpBudgets.Text = Language._Object.getCaption(Me.tabpBudgets.Name, Me.tabpBudgets.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblExam.Text = Language._Object.getCaption(Me.lblExam.Name, Me.lblExam.Text)
            Me.lblMaterialAmount.Text = Language._Object.getCaption(Me.lblMaterialAmount.Name, Me.lblMaterialAmount.Text)
            Me.lblMeals.Text = Language._Object.getCaption(Me.lblMeals.Name, Me.lblMeals.Text)
            Me.lblAllowance.Text = Language._Object.getCaption(Me.lblAllowance.Name, Me.lblAllowance.Text)
            Me.lblAccomodation.Text = Language._Object.getCaption(Me.lblAccomodation.Name, Me.lblAccomodation.Text)
            Me.lblTravel.Text = Language._Object.getCaption(Me.lblTravel.Name, Me.lblTravel.Text)
            Me.lblTraningMisc.Text = Language._Object.getCaption(Me.lblTraningMisc.Name, Me.lblTraningMisc.Text)
            Me.lblFees.Text = Language._Object.getCaption(Me.lblFees.Name, Me.lblFees.Text)
            Me.gbBudgetsFundings.Text = Language._Object.getCaption(Me.gbBudgetsFundings.Name, Me.gbBudgetsFundings.Text)
            Me.lblActualAllowance.Text = Language._Object.getCaption(Me.lblActualAllowance.Name, Me.lblActualAllowance.Text)
            Me.lblActualAmount.Text = Language._Object.getCaption(Me.lblActualAmount.Name, Me.lblActualAmount.Text)
            Me.lblActualFees.Text = Language._Object.getCaption(Me.lblActualFees.Name, Me.lblActualFees.Text)
            Me.lblActualExam.Text = Language._Object.getCaption(Me.lblActualExam.Name, Me.lblActualExam.Text)
            Me.lblActualMisc.Text = Language._Object.getCaption(Me.lblActualMisc.Name, Me.lblActualMisc.Text)
            Me.lblActualTravel.Text = Language._Object.getCaption(Me.lblActualTravel.Name, Me.lblActualTravel.Text)
            Me.lblActualMaterialAmount.Text = Language._Object.getCaption(Me.lblActualMaterialAmount.Name, Me.lblActualMaterialAmount.Text)
            Me.lblActualAccomodation.Text = Language._Object.getCaption(Me.lblActualAccomodation.Name, Me.lblActualAccomodation.Text)
            Me.lblActualMeals.Text = Language._Object.getCaption(Me.lblActualMeals.Name, Me.lblActualMeals.Text)
            Me.elActuals.Text = Language._Object.getCaption(Me.elActuals.Name, Me.elActuals.Text)
            Me.elBudgets.Text = Language._Object.getCaption(Me.elBudgets.Name, Me.elBudgets.Text)
            Me.elSourceFundings.Text = Language._Object.getCaption(Me.elSourceFundings.Name, Me.elSourceFundings.Text)
            Me.colhFundingSource.Text = Language._Object.getCaption(CStr(Me.colhFundingSource.Tag), Me.colhFundingSource.Text)
            Me.lnkBindAllocation.Text = Language._Object.getCaption(Me.lnkBindAllocation.Name, Me.lnkBindAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please write cancellation remark in order to cancel Training Scheduling.")
            Language.setMessage(mstrModuleName, 2, "You have successfully cancelled the scheduled training.")
            Language.setMessage(mstrModuleName, 3, "You have succcessfully cancelled the scheduled training.")
            Language.setMessage(mstrModuleName, 4, "Training Year is compulsory information. Please select Training Year to continue.")
            Language.setMessage(mstrModuleName, 5, "Training Course is compulsory information. Please select Training Course to continue.")
            Language.setMessage(mstrModuleName, 6, "Qualify Award is complusory information. Please select Qualify Award to continue.")
            Language.setMessage(mstrModuleName, 7, "Result Group is compulsory information. Please select Result Group to continue.")
            Language.setMessage(mstrModuleName, 8, "No. of Seats cannot be set to zero. Please set appropriate available seats.")
            Language.setMessage(mstrModuleName, 10, "Resource is compulsory information, it can not be blank. Please select Resource to continue.")
            Language.setMessage(mstrModuleName, 11, "Selected Resource is already added to the list.")
            Language.setMessage(mstrModuleName, 12, "Employee is compulsory information, it cannot be blank. Please select an Employee to continue.")
            Language.setMessage(mstrModuleName, 13, "Selected Employee is already added to the list.")
            Language.setMessage(mstrModuleName, 14, "Name is compulsory information, it can not be blank. Please enter Name to continue.")
            Language.setMessage(mstrModuleName, 15, "Entered Trainer is already added to the list.")
            Language.setMessage(mstrModuleName, 16, "Company cannot be same. Please enter another company name.")
            Language.setMessage(mstrModuleName, 17, "Selected Level is already assigned to employee. Please assigned new level.")
            Language.setMessage(mstrModuleName, 18, "End time cannot be less than or equal to Start time.")
            Language.setMessage(mstrModuleName, 19, "End date cannot be less than Start Date.")
            Language.setMessage(mstrModuleName, 20, "Are you sure you want to cancel schedule for this training?")
            Language.setMessage(mstrModuleName, 21, "Sorry, you cannot remove this Trainer. Reason : This trainer is already linked with some of the transactions.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class