﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmTraningRegistraionCancelList

#Region " Private Varaibles "
    Private objEnrollment As New clsTraining_Enrollment_Tran
    Private ReadOnly mstrModuleName As String = "frmTraningRegistraionCancelList"
    Private mblnFormLoad As Boolean = False
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 
#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master


        'S.SANDEEP [ 18 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objCourse As New clsTraining_Scheduling
        Dim objCourse As New clsCommon_Master
        'S.SANDEEP [ 18 FEB 2012 ] -- END




        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        'S.SANDEEP [ 17 AUG 2011 ] -- START
        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
        Dim objBranch As New clsStation
        'S.SANDEEP [ 17 AUG 2011 ] -- END 
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With



            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objCourse.getComboList("Title", True)
            'With cboCourse
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables("Title")
            '    .SelectedValue = 0
            'End With

            dsList = objCourse.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "Title")
            With cboCourse
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Title")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 18 FEB 2012 ] -- END




            dsList = objMaster.GetTraining_Status("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("Status")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "Stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objCourse = Nothing
            objMaster = Nothing
            dsList = Nothing
            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objBranch = Nothing
            'S.SANDEEP [ 17 AUG 2011 ] -- END 
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewTrainingEnrollmentList = True Then                'Pinkal (02-Jul-2012) -- Start

                'Sohail (24 Feb 2012) -- Start
                'TRA - ENHANCEMENT
                Dim objMaster As New clsMasterData
                dsList = objMaster.getComboListTrainingAwardMode("Mode", False)
                Dim objDic As New Dictionary(Of Integer, String)
                For Each dsRow As DataRow In dsList.Tables("Mode").Rows
                    objDic.Add(CInt(dsRow.Item("Id")), dsRow.Item("Name").ToString)
                Next
                objMaster = Nothing
                'Sohail (24 Feb 2012) -- End

                ''S.SANDEEP [ 24 FEB 2012 ] -- START
                ''ENHANCEMENT : TRA CHANGES [<TRAINING>]
                ''dsList = objEnrollment.GetList("Enrollment")

                ''S.SANDEEP [04 JUN 2015] -- START
                ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                ''dsList = objEnrollment.GetList("Enrollment", True)
                ''S.SANDEEP [ 24 FEB 2012 ] -- END 

                ''S.SANDEEP [ 18 FEB 2012 ] -- START
                ''ENHANCEMENT : TRA CHANGES
                ''If CInt(cboCourse.SelectedValue) > 0 Then
                ''    StrSearching &= "AND trainingschedulingunkid = " & CInt(cboCourse.SelectedValue) & " "
                ''End If

                'If CInt(cboCourse.SelectedValue) > 0 Then
                '    StrSearching &= "AND TitleId = " & CInt(cboCourse.SelectedValue) & " "
                'End If
                ''S.SANDEEP [ 18 FEB 2012 ] -- END

                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                'End If

                'If CInt(cboStatus.SelectedValue) > 0 Then
                '    StrSearching &= "AND status_id = " & CInt(cboStatus.SelectedValue) & " "
                'End If

                'If dtpCancellationDate.Checked = True And dtpCancellationTo.Checked = True Then
                '    StrSearching &= "AND CancelDate >= " & eZeeDate.convertDate(dtpCancellationDate.Value) & " AND CancelDate <= " & eZeeDate.convertDate(dtpCancellationTo.Value) & " "
                'End If

                'If dtpEnrollmentDateFrom.Checked = True And dtpEnrollmentTo.Checked = True Then
                '    StrSearching &= "AND EnrollDate >= " & eZeeDate.convertDate(dtpEnrollmentDateFrom.Value) & " AND EnrollDate <= " & eZeeDate.convertDate(dtpEnrollmentTo.Value) & " "
                'End If

                'If txtRemark.Text.Trim <> "" Then
                '    StrSearching &= "AND enroll_remark LIKE '%" & txtRemark.Text & "%'" & " "
                'End If


                ''S.SANDEEP [ 18 FEB 2012 ] -- START
                ''ENHANCEMENT : TRA CHANGES
                ''If radEnrolled.Checked = True Then
                ''    StrSearching &= "AND isvoid = 0 AND iscancel = 0 "
                ''Else
                ''S.SANDEEP [ 18 FEB 2012 ] -- END


                'If radCancelled.Checked = True Then
                '    StrSearching &= "AND iscancel = 1 "
                'ElseIf radVoid.Checked = True Then
                '    StrSearching &= "AND isvoid = 1 "
                'End If


                ''S.SANDEEP [ 17 AUG 2011 ] -- START
                ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                'If CInt(cboBranch.SelectedValue) > 0 Then
                '    StrSearching &= "AND BranchId = " & CInt(cboBranch.SelectedValue)
                'End If
                ''S.SANDEEP [ 17 AUG 2011 ] -- END 



                ''Sandeep [ 02 Oct 2010 ] -- Start
                ''If StrSearching.Length > 0 Then
                ''    StrSearching = StrSearching.Substring(3)
                ''    dtTable = New DataView(dsList.Tables("Enrollment"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
                ''Else
                ''    dtTable = dsList.Tables("Enrollment")
                ''End If

                ''Anjan (21 Nov 2011)-Start
                ''ENHANCEMENT : TRA COMMENTS
                'If mstrAdvanceFilter.Length > 0 Then
                '    StrSearching &= "AND " & mstrAdvanceFilter
                'End If
                ''Anjan (21 Nov 2011)-End 

                'If StrSearching.Length > 0 Then
                '    StrSearching = StrSearching.Substring(3)
                '    dtTable = New DataView(dsList.Tables("Enrollment"), StrSearching, "Title", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsList.Tables("Enrollment"), "", "Title", DataViewRowState.CurrentRows).ToTable
                'End If
                ''Sandeep [ 02 Oct 2010 ] -- End

                'S.SANDEEP [ 24 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES [<TRAINING>]
                'dsList = objEnrollment.GetList("Enrollment")

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objEnrollment.GetList("Enrollment", True)
                'S.SANDEEP [ 24 FEB 2012 ] -- END 

                ''S.SANDEEP [ 18 FEB 2012 ] -- START
                ''ENHANCEMENT : TRA CHANGES
                ''If CInt(cboCourse.SelectedValue) > 0 Then
                ''    StrSearching &= "AND trainingschedulingunkid = " & CInt(cboCourse.SelectedValue) & " "
                ''End If

                'If CInt(cboCourse.SelectedValue) > 0 Then
                '    StrSearching &= "AND TitleId = " & CInt(cboCourse.SelectedValue) & " "
                'End If
                ''S.SANDEEP [ 18 FEB 2012 ] -- END

                If CInt(cboCourse.SelectedValue) > 0 Then
                    StrSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboCourse.SelectedValue) & " "
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hrtraining_enrollment_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    StrSearching &= "AND hrtraining_enrollment_tran.status_id = " & CInt(cboStatus.SelectedValue) & " "
                End If

                If dtpCancellationDate.Checked = True And dtpCancellationTo.Checked = True Then
                    StrSearching &= "AND CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,112) BETWEEN '" & eZeeDate.convertDate(dtpCancellationDate.Value) & "' AND '" & eZeeDate.convertDate(dtpCancellationTo.Value) & "' "
                End If

                If dtpEnrollmentDateFrom.Checked = True And dtpEnrollmentTo.Checked = True Then
                    StrSearching &= "AND CONVERT(CHAR(8),hrtraining_enrollment_tran.enroll_date,112) BETWEEN '" & eZeeDate.convertDate(dtpEnrollmentDateFrom.Value) & "' AND '" & eZeeDate.convertDate(dtpEnrollmentTo.Value) & "' "
                End If

                If txtRemark.Text.Trim <> "" Then
                    StrSearching &= "AND hrtraining_enrollment_tran.enroll_remark LIKE '%" & txtRemark.Text & "%'" & " "
                End If

                If radCancelled.Checked = True Then
                    'Anjan (28 Aug 2017) --Start
                    'StrSearching &= "AND hrtraining_enrollment_tran.iscancel = 1 "
                    StrSearching &= "AND hrtraining_enrollment_tran.iscancel = 1 AND hrtraining_enrollment_tran.isvoid = 0 "
                    'Anjan (28 Aug 2017) --End
                ElseIf radVoid.Checked = True Then
                    StrSearching &= "AND hrtraining_enrollment_tran.isvoid = 1 "
                End If

                If CInt(cboBranch.SelectedValue) > 0 Then
                    StrSearching &= "AND hremployee_master.stationunkid = " & CInt(cboBranch.SelectedValue)
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If

                dsList = objEnrollment.GetList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, "Enrollment", True, StrSearching)

                dtTable = New DataView(dsList.Tables("Enrollment"), "", "Title", DataViewRowState.CurrentRows).ToTable
                'S.SANDEEP [04 JUN 2015] -- END


                lvEnrollment.Items.Clear()
                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    If CBool(dtRow.Item("isvoid")) = True Then
                        lvItem.ForeColor = Color.Red
                    ElseIf CBool(dtRow.Item("iscancel")) = True Then
                        lvItem.ForeColor = Color.Purple
                    ElseIf CInt(dtRow.Item("status_id")) = 3 Then
                        lvItem.ForeColor = Color.Blue
                    End If

                    'Sohail (24 Feb 2012) -- Start
                    'TRA - ENHANCEMENT
                    'lvItem.Text = dtRow.Item("EmpName").ToString
                    lvItem.Text = ""
                    lvItem.Tag = CInt(dtRow.Item("trainingenrolltranunkid"))

                    lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                    lvItem.SubItems(colhEmployee.Index).Tag = CInt(dtRow.Item("employeeunkid"))
                    'Sohail (24 Feb 2012) -- End
                    lvItem.SubItems.Add(dtRow.Item("Title").ToString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EnrollDate").ToString).ToShortDateString)
                    If dtRow.Item("CancelDate").ToString = Nothing Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("CancelDate").ToString).ToShortDateString)
                    End If
                    lvItem.SubItems.Add(dtRow.Item("STATUS").ToString)
                    'Sohail (24 Feb 2012) -- Start
                    'TRA - ENHANCEMENT
                    If CInt(dtRow.Item("operationmodeid")) > 0 AndAlso CInt(dtRow.Item("operationmodeid")) <> enTrainingAward_Modes.RE_CATEGORIZE AndAlso objDic.ContainsKey(CInt(dtRow.Item("operationmodeid"))) = True Then
                        lvItem.SubItems.Add(objDic.Item(CInt(dtRow.Item("operationmodeid"))).ToString)
                        lvItem.SubItems(colhAwardStatus.Index).Tag = CInt(dtRow.Item("operationmodeid"))
                    Else
                        lvItem.SubItems.Add("")
                        lvItem.SubItems(colhAwardStatus.Index).Tag = CInt(dtRow.Item("operationmodeid"))
                    End If
                    'Sohail (24 Feb 2012) -- End
                    lvItem.SubItems.Add(dtRow.Item("enroll_remark").ToString)

                    'S.SANDEEP [ 15 SEP 2011 ] -- START
                    'ENHANCEMENT : CODE OPTIMIZATION
                    lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                    'S.SANDEEP [ 15 SEP 2011 ] -- END 


                    'Sohail (24 Feb 2012) -- Start
                    'TRA - ENHANCEMENT
                    'lvItem.Tag = dtRow.Item("trainingenrolltranunkid")
                    lvItem.SubItems.Add(dtRow.Item("periodunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("trainingschedulingunkid").ToString)
                    If CBool(dtRow.Item("isqualificaionupdated").ToString) = True Then
                        lvItem.SubItems.Add("Yes")
                    Else
                        lvItem.SubItems.Add("No")
                    End If
                    lvItem.SubItems(colhUpdateQuali.Index).Tag = dtRow.Item("isqualificaionupdated").ToString
                    'Sohail (24 Feb 2012) -- End

                    RemoveHandler lvEnrollment.ItemChecked, AddressOf lvEnrollment_ItemChecked
                    lvEnrollment.Items.Add(lvItem)
                    AddHandler lvEnrollment.ItemChecked, AddressOf lvEnrollment_ItemChecked

                    lvItem = Nothing
                Next

                lvEnrollment.GroupingColumn = colhCourse
                lvEnrollment.DisplayGroups(True)

                'S.SANDEEP [ 17 AUG 2011 ] -- START
                'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                lvEnrollment.GridLines = False
                'S.SANDEEP [ 17 AUG 2011 ] -- END 


                If lvEnrollment.Items.Count > 10 Then
                    colhRemark.Width = 155 - 28
                Else
                    colhRemark.Width = 155
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'mnuAnalyse.Enabled = User._Object.Privilege._AddAssessmentAnalysis
            mnuAnalyse.Enabled = User._Object.Privilege._AddTrainingAnalysis
            'S.SANDEEP [28 MAY 2015] -- END

            btnNew.Enabled = User._Object.Privilege._AddTrainingEnrollment
            btnEdit.Enabled = User._Object.Privilege._EditTrainingEnrollment
            btnDelete.Enabled = User._Object.Privilege._DeleteTrainingEnrollment
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            mnuUpdateQualification.Enabled = User._Object.Privilege._AllowToUpdateQualifictaion
            mnuRecategorize.Enabled = User._Object.Privilege._AllowToRe_Categorize
            mnuSalaryIncrement.Enabled = User._Object.Privilege._AllowSalaryIncrementFromEnrollment
            mnuVoidSalaryIncrement.Enabled = User._Object.Privilege._AllowToVoidSalaryIncrementFromEnrollment
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuScanAttachDocument.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            'Anjan (25 Oct 2012)-End 

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            'mnuSalaryIncrement.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
            'mnuVoidSalaryIncrement.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
            If ConfigParameter._Object._IsArutiDemo = False Then
                mnuUpdateQualification.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management)
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            mnuEnrollEmployee.Enabled = User._Object.Privilege._AllowToImportTrainingEnrollment
            'Varsha Rana (17-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "
    Private Sub frmTraningRegistraionCancelList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvEnrollment.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmTraningRegistraionCancelList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmTraningRegistraionCancelList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEnrollment = New clsTraining_Enrollment_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            Call OtherSettings()

            Call FillCombo()


            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'radEnrolled.Checked = True
            'S.SANDEEP [ 18 FEB 2012 ] -- END

            'Call FillList()

            If lvEnrollment.Items.Count <= 0 Then
                mnuAnalyse.Enabled = False
                mnuAttendance.Enabled = False
            End If

            Call SetVisibility()

            mblnFormLoad = True

            If lvEnrollment.Items.Count > 0 Then lvEnrollment.Items(0).Selected = True
            lvEnrollment.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTraningRegistraionCancelList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTraningRegistraionCancelList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEnrollment = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Enrollment_Tran.SetMessages()

            objfrm._Other_ModuleNames = "clsTraining_Enrollment_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvEnrollment.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvEnrollment.Select()
            Exit Sub
        End If
        'If objEnrollment.isUsed(CInt(lvEnrollment.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Training Schedule. Reason: This Training Schedule is in use."), enMsgBoxStyle.Information) '?2
        '    lvEnrollment.Select()
        '    Exit Sub
        'End If

        If lvEnrollment.SelectedItems(0).ForeColor = Color.Blue Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Delete the completed Analysis."), enMsgBoxStyle.Information)
            lvEnrollment.Select()
            Exit Sub
        End If


        If lvEnrollment.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot Void already voided transaction."), enMsgBoxStyle.Information) '?1
            lvEnrollment.Select()
            Exit Sub
        End If

        If lvEnrollment.SelectedItems(0).ForeColor = Color.Purple Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Void cancelled transaction."), enMsgBoxStyle.Information) '?1
            lvEnrollment.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvEnrollment.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objEnrollment._Isvoid = True
                'Sandeep [ 16 Oct 2010 ] -- Start
                'objEnrollment._Voidreason = "TESTING"
                'objEnrollment._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objEnrollment._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objEnrollment._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 

                objEnrollment._Voiduserunkid = User._Object._Userunkid

                objEnrollment.Delete(CInt(lvEnrollment.SelectedItems(0).Tag))
                lvEnrollment.SelectedItems(0).Remove()

                If lvEnrollment.Items.Count <= 0 Then
                    Exit Try
                Else
                    Call FillList()
                End If

                'If lvEnrollment.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvEnrollment.Items.Count - 1
                '    lvEnrollment.Items(intSelectedIndex).Selected = True
                '    lvEnrollment.EnsureVisible(intSelectedIndex)
                'ElseIf lvEnrollment.Items.Count <> 0 Then
                '    lvEnrollment.Items(intSelectedIndex).Selected = True
                '    lvEnrollment.EnsureVisible(intSelectedIndex)
                'End If
            End If
            lvEnrollment.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvEnrollment.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvEnrollment.Select()
            Exit Sub
        End If

        If lvEnrollment.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot Void already voided transaction."), enMsgBoxStyle.Information) '?1
            lvEnrollment.Select()
            Exit Sub
        End If

        If lvEnrollment.SelectedItems(0).ForeColor = Color.Purple Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Void cancelled transaction."), enMsgBoxStyle.Information) '?1
            lvEnrollment.Select()
            Exit Sub
        End If

        If lvEnrollment.SelectedItems(0).ForeColor = Color.Blue Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot Edit the completed Analysis."), enMsgBoxStyle.Information)
            lvEnrollment.Select()
            Exit Sub
        End If

        Dim frm As New frmEnrollmentAndCancellation
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvEnrollment.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvEnrollment.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            'If lvEnrollment.Items.Count > 0 Then
            '    lvEnrollment.Items(intSelectedIndex).Selected = True
            '    lvEnrollment.EnsureVisible(intSelectedIndex)
            '    lvEnrollment.Select()
            'End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmNewEnrollCancellation
        Try
            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            'If frm.displayDialog(-1, enAction.ADD_ONE) Then
            '    Call FillList()
            'End If


            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, enAction.ADD_ONE)
            Call FillList()
            'S.SANDEEP [ 18 FEB 2012 ] -- END
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCourse.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            txtRemark.Text = ""
            dtpCancellationDate.Checked = False
            dtpCancellationTo.Checked = False
            dtpEnrollmentDateFrom.Checked = False
            dtpEnrollmentTo.Checked = False


            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'radEnrolled.Checked = True
            'S.SANDEEP [ 18 FEB 2012 ] -- END

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            cboBranch.SelectedValue = 0
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 

            'Anjan (28 Aug 2017)-Start
            'Issue : reset button was not refreshing when any radion button was selected.
            radShowAll.Checked = False
            radVoid.Checked = False
            radCancelled.Checked = False
            'Anjan (28 Aug 2017)-End 

            
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub radEnrolled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radCancelled.CheckedChanged, radVoid.CheckedChanged, radShowAll.CheckedChanged
        Try
            Dim radbtn As RadioButton = CType(sender, RadioButton)
            If radbtn.Checked = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEnrolled_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEnrollment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvEnrollment.Click
        Try
            If lvEnrollment.SelectedItems.Count > 0 Then
                mnuAnalyse.Enabled = True
                mnuEvaluation.Enabled = True
            End If
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEnrollment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuAnalyse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAnalyse.Click
        If lvEnrollment.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information)
            lvEnrollment.Select()
            Exit Sub
        End If
        If lvEnrollment.SelectedItems(0).ForeColor = Color.Blue Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Analyze the completed Analysis."), enMsgBoxStyle.Information)
            lvEnrollment.Select()
            Exit Sub
        End If
        Dim frm As New frmFinalEvaluation
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, enAction.ADD_ONE, CInt(lvEnrollment.SelectedItems(0).Tag))
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTrainingAwards_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuAttendance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAttendance.Click
        Dim frm As New frmCourseAttendance_AddEdit
        Try
            If lvEnrollment.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Please select atleast one transaction to do Attendance."), enMsgBoxStyle.Information)
                lvEnrollment.Select()
                Exit Sub
            End If

            If frm.displayDialog(-1, enAction.ADD_ONE, CInt(lvEnrollment.SelectedItems(0).Tag)) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AttendanceToolStripMenuItem_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuScanAttachDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanAttachDocument.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP |26-APR-2019| -- START
            'frm.displayDialog(Language.getMessage(mstrModuleName, 8, "Select Employee"), enImg_Email_RefId.Training_Module, enAction.ADD_ONE, "", mstrModuleName) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            frm.displayDialog(Language.getMessage(mstrModuleName, 8, "Select Employee"), enImg_Email_RefId.Training_Module, enAction.ADD_ONE, "", mstrModuleName, True) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            'S.SANDEEP |26-APR-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuScanAttachDocument_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub mnuScanAttachDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanAttachDocument.Click

    '    If lvEnrollment.SelectedItems.Count < 1 Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
    '        lvEnrollment.Select()
    '        Exit Sub
    '    End If

    '    If lvEnrollment.SelectedItems(0).ForeColor = Color.Red Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You cannot Scan/Attach document(s) on voided transaction."), enMsgBoxStyle.Information) '?1
    '        lvEnrollment.Select()
    '        Exit Sub
    '    End If

    '    If lvEnrollment.SelectedItems(0).ForeColor = Color.Purple Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You cannot Scan/Attach document(s) on cancelled transaction."), enMsgBoxStyle.Information) '?1
    '        lvEnrollment.Select()
    '        Exit Sub
    '    End If

    '    Dim frm As New frmScanOrAttachmentInfo
    '    Try
    '        Dim dsList As New DataSet : Dim dtTable As DataTable = Nothing
    '        dsList = objEnrollment.GetList("List")
    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            dtTable = New DataView(dsList.Tables(0), "employeeunkid='" & CInt(lvEnrollment.SelectedItems(0).SubItems(objcolhEmpId.Index).Text) & "'", "", DataViewRowState.CurrentRows).ToTable
    '            If dtTable.Rows.Count > 0 Then
    '                dsList.Tables.Remove("List")
    '                dsList.Tables.Add(dtTable)

    '                frm._Dataset = dsList
    '                frm._LableCaption = Language.getMessage(mstrModuleName, 12, "Select Course")
    '                frm._RefId = enScanAttactRefId.EMPLOYEE_TRAINING
    '                frm._EmployeeUnkid = CInt(lvEnrollment.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)
    '                frm._IsComboVisible = True
    '                frm._EmployeeName = lvEnrollment.SelectedItems(0).SubItems(colhEmployee.Index).Text

    '                frm.ShowDialog()
    '            End If
    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuScanAttachDocument_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub

    'S.SANDEEP [ 18 FEB 2012 ] -- END

    Private Sub lvEnrollment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvEnrollment.SelectedIndexChanged
        Try
            If lvEnrollment.SelectedItems.Count > 0 Then
                If lvEnrollment.SelectedItems(0).ForeColor = Color.Blue Then
                    mnuAnalyse.Enabled = False
                Else
                    mnuAnalyse.Enabled = True

                End If
                If lvEnrollment.SelectedItems(0).ForeColor = Color.Purple Then
                    btnAwardStatus.Enabled = False
                    mnuAnalyse.Enabled = False
                Else
                    btnAwardStatus.Enabled = True
                    mnuAnalyse.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEnrollment_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (24 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCheckAll.CheckedChanged
        Try
            If chkCheckAll.Checked = True Then
                For Each lvItem As ListViewItem In lvEnrollment.Items
                    If lvItem.ForeColor = Color.Blue Then
                        RemoveHandler lvEnrollment.ItemChecked, AddressOf lvEnrollment_ItemChecked
                        lvItem.Checked = True
                        AddHandler lvEnrollment.ItemChecked, AddressOf lvEnrollment_ItemChecked
                    End If
                Next
            Else
                For Each lvItem As ListViewItem In lvEnrollment.CheckedItems
                    RemoveHandler lvEnrollment.ItemChecked, AddressOf lvEnrollment_ItemChecked
                    lvItem.Checked = False
                    AddHandler lvEnrollment.ItemChecked, AddressOf lvEnrollment_ItemChecked
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEnrollment_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEnrollment.ItemChecked
        Try
            If e.Item.Checked = True AndAlso e.Item.ForeColor <> Color.Blue Then
                e.Item.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEnrollment_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuSalaryIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSalaryIncrement.Click
        Dim strEmpList As String = ""
        Dim intPeriodId As Integer = 0
        'Sohail (29 Dec 2012) -- Start
        'TRA - ENHANCEMENT
        Dim dicAppraisal As New Dictionary(Of String, ArrayList) 'Sohail (29 Dec 2012)
        Dim arrList As ArrayList
        'Sohail (29 Dec 2012) -- End

        Try
            If lvEnrollment.CheckedItems.Count > 0 Then
                For Each lvItem As ListViewItem In lvEnrollment.CheckedItems
                    If CInt(lvItem.SubItems(colhAwardStatus.Index).Tag) = enTrainingAward_Modes.SALARY_INCREMENT Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry! Salary Increment for some of the employees has already been done. Please Void Salary Increment first to do Salary Increment again."), enMsgBoxStyle.Information)
                        Exit Sub
                    ElseIf CInt(lvItem.SubItems(colhAwardStatus.Index).Tag) = enTrainingAward_Modes.RE_CATEGORIZE Then
                        If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Re-categorize award action has already been assigned and now their award action will be changed." & vbCrLf & vbCrLf & " Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    End If
                Next

                For Each lvItem As ListViewItem In lvEnrollment.CheckedItems
                    If strEmpList.Trim = "" Then
                        strEmpList = lvItem.SubItems(colhEmployee.Index).Tag.ToString
                    Else
                        strEmpList &= ", " & lvItem.SubItems(colhEmployee.Index).Tag.ToString
                    End If
                    'Sohail (29 Dec 2012) -- Start
                    'TRA - ENHANCEMENT
                    If dicAppraisal.ContainsKey(lvItem.SubItems(colhEmployee.Index).Tag.ToString) = False Then
                        arrList = New ArrayList
                        arrList.Add(-1) 'trainingenrolltranunkid
                        arrList.Add(0) 'SalaryincrementUnkId
                        dicAppraisal.Add(lvItem.SubItems(colhEmployee.Index).Tag.ToString, arrList)
                    End If
                    'Sohail (29 Dec 2012) -- End
                Next

                Dim objFrm As New frmSalaryGlobalAssign
                If objFrm.DisplayDialog(strEmpList, intPeriodId, dicAppraisal) = True Then
                    'Sohail (29 Dec 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Dim arrEmp() As String = strEmpList.Split(",".ToCharArray)
                    'For Each lvItem As ListViewItem In lvEnrollment.CheckedItems
                    '    If arrEmp.Contains(lvItem.SubItems(colhEmployee.Index).Tag.ToString) = True Then
                    '        objEnrollment = New clsTraining_Enrollment_Tran
                    '        objEnrollment._Trainingenrolltranunkid = CInt(lvItem.Tag)
                    '        objEnrollment._Operationmodeid = enTrainingAward_Modes.SALARY_INCREMENT
                    '        objEnrollment._Periodunkid = intPeriodId
                    '        If objEnrollment.Update() = False Then
                    '            If objEnrollment._Message <> "" Then
                    '                eZeeMsgBox.Show(objEnrollment._Message, enMsgBoxStyle.Information)
                    '            Else
                    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Employee Salary Increment process failed."), enMsgBoxStyle.Information)
                    '            End If
                    '            Exit Sub
                    '        End If
                    '    End If
                    'Next
                    For Each lvItem As ListViewItem In lvEnrollment.CheckedItems
                        If dicAppraisal.ContainsKey(lvItem.SubItems(colhEmployee.Index).Tag.ToString) = True Then
                            arrList = dicAppraisal.Item(lvItem.SubItems(colhEmployee.Index).Tag.ToString)
                            objEnrollment = New clsTraining_Enrollment_Tran
                            objEnrollment._Trainingenrolltranunkid = CInt(lvItem.Tag)
                            objEnrollment._Operationmodeid = enTrainingAward_Modes.SALARY_INCREMENT
                            objEnrollment._Periodunkid = intPeriodId
                            objEnrollment._SalaryincrementtranUnkid = CInt(arrList(1))
                            If objEnrollment.Update() = False Then
                                If objEnrollment._Message <> "" Then
                                    eZeeMsgBox.Show(objEnrollment._Message, enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Employee Salary Increment process failed."), enMsgBoxStyle.Information)
                                End If
                                Exit Sub
                            End If
                        End If
                    Next
                    'Sohail (29 Dec 2012) -- End

                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Employee Salary Increment process completed successfully."), enMsgBoxStyle.Information)
                    Call FillList()

                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Check atleast one employee to do further operation."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSalaryIncrement_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuVoidSalaryIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVoidSalaryIncrement.Click
        Dim strFilter As String = ""
        Dim dicEmpPeriodID As New Dictionary(Of Integer, Integer)
        Dim dsList As DataSet
        Dim objEnrollTemp As New clsTraining_Enrollment_Tran
        Try
            If lvEnrollment.CheckedItems.Count > 0 Then
                For Each lvItem As ListViewItem In lvEnrollment.CheckedItems
                    If CInt(lvItem.SubItems(colhAwardStatus.Index).Tag) <> enTrainingAward_Modes.SALARY_INCREMENT Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select employee(s) having Salary Increment award action."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Next

                For Each lvItem As ListViewItem In lvEnrollment.CheckedItems
                    If strFilter.Trim = "" Then
                        strFilter = " AND ( ( employeeunkid = " & CInt(lvItem.SubItems(colhEmployee.Index).Tag) & "  AND periodunkid = " & CInt(lvItem.SubItems(objcolhPeriod.Index).Text) & ") "
                    Else
                        strFilter &= "OR ( employeeunkid = " & CInt(lvItem.SubItems(colhEmployee.Index).Tag) & "  AND periodunkid = " & CInt(lvItem.SubItems(objcolhPeriod.Index).Text) & ") "
                    End If
                Next
                If strFilter.Trim <> "" Then strFilter &= ")"

                Dim objFrm As New frmSalaryIncrementList
                If objFrm.DisplayDialog(strFilter, dicEmpPeriodID) = True Then
                    If dicEmpPeriodID.Count > 0 Then
                        For Each lvItem As ListViewItem In lvEnrollment.CheckedItems
                            If dicEmpPeriodID.ContainsKey(CInt(lvItem.SubItems(colhEmployee.Index).Tag)) = True Then
                                objEnrollment = New clsTraining_Enrollment_Tran
                                objEnrollment._Trainingenrolltranunkid = CInt(lvItem.Tag)
                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'dsList = objEnrollment.GetList("Enrollment", True, , , , "salaryincrementtranunkid = " & objEnrollment._SalaryincrementtranUnkid & " ")

                                dsList = objEnrollment.GetList(FinancialYear._Object._DatabaseName, _
                                                               User._Object._Userunkid, _
                                                               FinancialYear._Object._YearUnkid, _
                                                               Company._Object._Companyunkid, _
                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                                               ConfigParameter._Object._IsIncludeInactiveEmp, "Enrollment", True, _
                                                               "salaryincrementtranunkid = " & objEnrollment._SalaryincrementtranUnkid & " ")
                                'S.SANDEEP [04 JUN 2015] -- END

                                For Each dsRow As DataRow In dsList.Tables("Enrollment").Rows
                                    objEnrollTemp._Trainingenrolltranunkid = CInt(dsRow.Item("trainingenrolltranunkid"))
                                    objEnrollTemp._Operationmodeid = 0
                                    objEnrollTemp._Periodunkid = 0
                                    objEnrollTemp._SalaryincrementtranUnkid = 0
                                    If objEnrollTemp.Update() = False Then
                                        If objEnrollTemp._Message <> "" Then
                                            eZeeMsgBox.Show(objEnrollTemp._Message, enMsgBoxStyle.Information)
                                        Else
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Void Salary Increment process failed."), enMsgBoxStyle.Information)
                                        End If
                                        Exit Try
                                    End If
                                Next
                            End If
                        Next

                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Void Salary Increment process completed."), enMsgBoxStyle.Information)
                        Call FillList()
                    End If

                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Check atleast one employee to do further operation."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVoidSalaryIncrement_Click", mstrModuleName)
        Finally
            objEnrollTemp = Nothing
        End Try
    End Sub

    Private Sub mnuRecategorize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRecategorize.Click
        Dim intOldJobID As Integer = 0
        Try

            If lvEnrollment.SelectedItems.Count <= 0 OrElse lvEnrollment.SelectedItems(0).ForeColor <> Color.Blue Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please select atleast one employee with complete status to do further operation."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf CInt(lvEnrollment.SelectedItems(0).SubItems(colhAwardStatus.Index).Tag) > 0 Then
                If CInt(lvEnrollment.SelectedItems(0).SubItems(colhAwardStatus.Index).Tag) = enTrainingAward_Modes.SALARY_INCREMENT Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry! Employee has been awarded with Salary Increment. Please Void Salary Increment first to do further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                ElseIf CInt(lvEnrollment.SelectedItems(0).SubItems(colhAwardStatus.Index).Tag) = enTrainingAward_Modes.RE_CATEGORIZE Then
                    If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Re-categorize award action has already been assigned and now their award action will be changed." & vbCrLf & vbCrLf & "Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If
            End If


            'Pinkal (10-May-2017) -- Start
            'Enhancement - Opening Wrong form (Employee Master) From Traning Registration Cancel List On Employee Recategorization .

            'Dim objEmployee As New clsEmployee_Master
            'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(lvEnrollment.SelectedItems(0).SubItems(colhEmployee.Index).Tag)
            'intOldJobID = objEmployee._Jobunkid
            'Dim objFrm As New frmEmployeeMaster
            'Dim intNewJobID As Integer = -1
            'If objFrm.displayDialog(CInt(lvEnrollment.SelectedItems(0).SubItems(colhEmployee.Index).Tag), enAction.EDIT_ONE, intNewJobID) = True Then
            '    If intNewJobID > 0 AndAlso intOldJobID <> intNewJobID Then
            '        objEnrollment = New clsTraining_Enrollment_Tran
            '        objEnrollment._Trainingenrolltranunkid = CInt(lvEnrollment.SelectedItems(0).Tag)
            '        objEnrollment._Operationmodeid = enTrainingAward_Modes.RE_CATEGORIZE
            '        objEnrollment._Periodunkid = 0
            '        objEnrollment._Jobunkid = intNewJobID
            '        If objEnrollment.Update() = False Then
            '            If objEnrollment._Message <> "" Then
            '                eZeeMsgBox.Show(objEnrollment._Message, enMsgBoxStyle.Information)
            '            Else
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Re-categorize process failed."), enMsgBoxStyle.Information)
            '            End If
            '            Exit Sub
            '        End If

            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Re-categorize process completed successfully."), enMsgBoxStyle.Information)
            '        Call FillList()
            '    End If
            'End if
            Dim intNewJobID As Integer = -1
            Dim objEmpRecategorize As New frmEmployeeRecategorize()
            objEmpRecategorize._EmployeeId = CInt(lvEnrollment.SelectedItems(0).SubItems(colhEmployee.Index).Tag)

            If objEmpRecategorize.displayDialog(enAction.ADD_ONE, True, intNewJobID) Then
                If intNewJobID > 0 AndAlso intOldJobID <> intNewJobID Then
                    objEnrollment = New clsTraining_Enrollment_Tran
                    objEnrollment._Trainingenrolltranunkid = CInt(lvEnrollment.SelectedItems(0).Tag)
                    objEnrollment._Operationmodeid = enTrainingAward_Modes.RE_CATEGORIZE
                    objEnrollment._Periodunkid = 0
                    objEnrollment._Jobunkid = intNewJobID
                    If objEnrollment.Update() = False Then
                        If objEnrollment._Message <> "" Then
                            eZeeMsgBox.Show(objEnrollment._Message, enMsgBoxStyle.Information)
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Re-categorize process failed."), enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If

                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Re-categorize process completed successfully."), enMsgBoxStyle.Information)
                    Call FillList()
                End If
            End If

            'Pinkal (10-May-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRecategorize_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuUpdateQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdateQualification.Click
        Dim objScheduling As New clsTraining_Scheduling
        Try
            If lvEnrollment.SelectedItems.Count <= 0 OrElse lvEnrollment.SelectedItems(0).ForeColor <> Color.Blue Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please select atleast one employee with complete status to do further operation."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf CBool(lvEnrollment.SelectedItems(0).SubItems(colhUpdateQuali.Index).Tag) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry! Update Qualification has already been done for this Employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'S.SANDEEP [ 27 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dsQData As New DataSet
            Dim objAnalysis As New clsTraining_Analysis_Master
            dsQData = objAnalysis.Get_Analysis_Data("List", CInt(lvEnrollment.SelectedItems(0).Tag))
            'S.SANDEEP [ 27 FEB 2012 ] -- END


            objScheduling._Trainingschedulingunkid = CInt(lvEnrollment.SelectedItems(0).SubItems(objcolhscheduleunkid.Index).Text)
            Dim objQualification As New clsEmp_Qualification_Tran
            objQualification._Employeeunkid = CInt(lvEnrollment.SelectedItems(0).SubItems(colhEmployee.Index).Tag)
            objQualification._Instituteunkid = objScheduling._Traninginstituteunkid
            objQualification._Reference_No = ""
            objQualification._Transaction_Date = ConfigParameter._Object._CurrentDateAndTime
            objQualification._Qualificationgroupunkid = objScheduling._Qualificationgroupunkid
            objQualification._Qualificationunkid = objScheduling._Qualificationunkid
            'S.SANDEEP [ 27 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If dsQData.Tables(0).Rows.Count > 0 Then
                objQualification._Resultunkid = Convert.ToInt32(dsQData.Tables(0).Rows(0)("resultunkid"))
                objQualification._GPAcode = Convert.ToDecimal(dsQData.Tables(0).Rows(0)("gpa_value"))
            End If
            'S.SANDEEP [ 27 FEB 2012 ] -- END

            'Pinkal (06-Aug-2012) -- Start
            'Enhancement : TRA Changes
            objQualification._Userunkid = User._Object._Userunkid
            'Pinkal (06-Aug-2012) -- End

            If objQualification.Insert() = True Then
                objEnrollment = New clsTraining_Enrollment_Tran
                objEnrollment._Trainingenrolltranunkid = CInt(lvEnrollment.SelectedItems(0).Tag)
                objEnrollment._Isqualificaionupdated = True
                If objEnrollment.Update() = False Then
                    If objEnrollment._Message <> "" Then
                        eZeeMsgBox.Show(objEnrollment._Message, enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Update Qualification process failed."), enMsgBoxStyle.Information)
                    End If
                    Exit Sub
                End If

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Update Qualification process completed successfully."), enMsgBoxStyle.Information)
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub
    'Sohail (24 Feb 2012) -- End


    'S.SANDEEP [ 01 OCT 2014 ] -- START
    Private Sub mnuGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension
                Dim dTable As New DataTable
                dTable.Columns.Add("coursename", System.Type.GetType("System.String"))
                dTable.Columns.Add("course_startdate", System.Type.GetType("System.DateTime"))
                dTable.Columns.Add("course_enddate", System.Type.GetType("System.DateTime"))
                dTable.Columns.Add("training_institute", System.Type.GetType("System.String"))
                dTable.Columns.Add("employeecode", System.Type.GetType("System.String"))
                dTable.Columns.Add("enrollmentdate", System.Type.GetType("System.DateTime"))
                dsList.Tables.Add(dTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(strFilePath, dsList)
                OpenXML_Export(strFilePath, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList = Nothing
        End Try
    End Sub

    Private Sub mnuEnrollEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEnrollEmployee.Click
        Dim frm As New frmImportTrainingEnrollmentWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportEmployeeRefree_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 01 OCT 2014 ] -- END


#End Region

    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnEvaluation.GradientBackColor = GUI._ButttonBackColor
            Me.btnEvaluation.GradientForeColor = GUI._ButttonFontColor

            Me.btnAwardStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnAwardStatus.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblCancellationDateTo.Text = Language._Object.getCaption(Me.lblCancellationDateTo.Name, Me.lblCancellationDateTo.Text)
            Me.lblCancellationDateFrom.Text = Language._Object.getCaption(Me.lblCancellationDateFrom.Name, Me.lblCancellationDateFrom.Text)
            Me.lblEnrollmentDateTo.Text = Language._Object.getCaption(Me.lblEnrollmentDateTo.Name, Me.lblEnrollmentDateTo.Text)
            Me.lblEnrollmentdateFrom.Text = Language._Object.getCaption(Me.lblEnrollmentdateFrom.Name, Me.lblEnrollmentdateFrom.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.radCancelled.Text = Language._Object.getCaption(Me.radCancelled.Name, Me.radCancelled.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.radShowAll.Text = Language._Object.getCaption(Me.radShowAll.Name, Me.radShowAll.Text)
            Me.btnEvaluation.Text = Language._Object.getCaption(Me.btnEvaluation.Name, Me.btnEvaluation.Text)
            Me.mnuAnalyse.Text = Language._Object.getCaption(Me.mnuAnalyse.Name, Me.mnuAnalyse.Text)
            Me.radVoid.Text = Language._Object.getCaption(Me.radVoid.Name, Me.radVoid.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhCourse.Text = Language._Object.getCaption(CStr(Me.colhCourse.Tag), Me.colhCourse.Text)
            Me.colhEnrollDate.Text = Language._Object.getCaption(CStr(Me.colhEnrollDate.Tag), Me.colhEnrollDate.Text)
            Me.colhCancelDate.Text = Language._Object.getCaption(CStr(Me.colhCancelDate.Tag), Me.colhCancelDate.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.mnuAttendance.Text = Language._Object.getCaption(Me.mnuAttendance.Name, Me.mnuAttendance.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.mnuScanAttachDocument.Text = Language._Object.getCaption(Me.mnuScanAttachDocument.Name, Me.mnuScanAttachDocument.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnAwardStatus.Text = Language._Object.getCaption(Me.btnAwardStatus.Name, Me.btnAwardStatus.Text)
            Me.mnuUpdateQualification.Text = Language._Object.getCaption(Me.mnuUpdateQualification.Name, Me.mnuUpdateQualification.Text)
            Me.mnuRecategorize.Text = Language._Object.getCaption(Me.mnuRecategorize.Name, Me.mnuRecategorize.Text)
            Me.mnuSalaryIncrement.Text = Language._Object.getCaption(Me.mnuSalaryIncrement.Name, Me.mnuSalaryIncrement.Text)
            Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
            Me.chkCheckAll.Text = Language._Object.getCaption(Me.chkCheckAll.Name, Me.chkCheckAll.Text)
            Me.colhAwardStatus.Text = Language._Object.getCaption(CStr(Me.colhAwardStatus.Tag), Me.colhAwardStatus.Text)
            Me.mnuVoidSalaryIncrement.Text = Language._Object.getCaption(Me.mnuVoidSalaryIncrement.Name, Me.mnuVoidSalaryIncrement.Text)
            Me.colhUpdateQuali.Text = Language._Object.getCaption(CStr(Me.colhUpdateQuali.Tag), Me.colhUpdateQuali.Text)
            Me.mnuEnrollEmployee.Text = Language._Object.getCaption(Me.mnuEnrollEmployee.Name, Me.mnuEnrollEmployee.Text)
            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)



        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot Void already voided transaction.")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot Void cancelled transaction.")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot Delete the completed Analysis.")
            Language.setMessage(mstrModuleName, 6, "Sorry, You cannot Edit the completed Analysis.")
            Language.setMessage(mstrModuleName, 7, "Sorry, You cannot Analyze the completed Analysis.")
            Language.setMessage(mstrModuleName, 8, "Select Employee")
            Language.setMessage(mstrModuleName, 9, "Employee Salary Increment process completed successfully.")
            Language.setMessage(mstrModuleName, 10, "Employee Salary Increment process failed.")
            Language.setMessage(mstrModuleName, 11, "Sorry! Salary Increment for some of the employees has already been done. Please Void Salary Increment first to do Salary Increment again.")
            Language.setMessage(mstrModuleName, 12, "Please select employee(s) having Salary Increment award action.")
            Language.setMessage(mstrModuleName, 13, "Void Salary Increment process failed.")
            Language.setMessage(mstrModuleName, 14, "Void Salary Increment process completed.")
            Language.setMessage(mstrModuleName, 15, "Please Check atleast one employee to do further operation.")
            Language.setMessage(mstrModuleName, 16, "Please select atleast one employee with complete status to do further operation.")
            Language.setMessage(mstrModuleName, 17, "Sorry! Employee has been awarded with Salary Increment. Please Void Salary Increment first to do further operation.")
            Language.setMessage(mstrModuleName, 18, "Re-categorize award action has already been assigned and now their award action will be changed." & vbCrLf & vbCrLf & "Do you want to proceed?")
            Language.setMessage(mstrModuleName, 19, "Sorry! Update Qualification has already been done for this Employee.")
            Language.setMessage(mstrModuleName, 20, "Re-categorize award action has already been assigned and now their award action will be changed." & vbCrLf & vbCrLf & " Do you want to proceed?")
            Language.setMessage(mstrModuleName, 21, "Re-categorize process failed.")
            Language.setMessage(mstrModuleName, 22, "Re-categorize process completed successfully.")
            Language.setMessage(mstrModuleName, 23, "Update Qualification process completed successfully.")
            Language.setMessage(mstrModuleName, 24, "Please select atleast one transaction to do Attendance.")
            Language.setMessage(mstrModuleName, 25, "Update Qualification process failed.")
            Language.setMessage(mstrModuleName, 26, "Template Exported Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class