﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmNewEnrollCancellation

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEnrollmentAndCancellation"
    Private mblnCancel As Boolean = True
    Private objEnrollment As clsTraining_Enrollment_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintTraningEnrollmentTranUnkid As Integer = -1
    Private mblnFormLoad As Boolean = False
    Private intSelectedEmployeeId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintTraningEnrollmentTranUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintTraningEnrollmentTranUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboCourse.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            txtDepartment.BackColor = GUI.ColorComp
            txtEligibility.BackColor = GUI.ColorOptional
            txtEnrollRemark.BackColor = GUI.ColorComp
            txtFilledPosition.BackColor = GUI.ColorOptional
            txtJob.BackColor = GUI.ColorComp
            txtTotalPosition.BackColor = GUI.ColorComp
            cboTrainingYears.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objEnrollment._Trainingschedulingunkid = CInt(cboCourse.SelectedValue)
            objEnrollment._Status_Id = CInt(cboStatus.SelectedValue)
            objEnrollment._Cancellationreason = ""
            objEnrollment._Enroll_Remark = txtEnrollRemark.Text
            objEnrollment._Enroll_Date = dtpEnrolldate.Value
            objEnrollment._Userunkid = User._Object._Userunkid
            If mintTraningEnrollmentTranUnkid = -1 Then
                objEnrollment._Iscancel = False
                objEnrollment._Cancellationdate = Nothing
                objEnrollment._Cancellationreason = ""
                objEnrollment._Cancellationuserunkid = -1
                objEnrollment._Isvoid = False
                objEnrollment._Voiddatetime = Nothing
                objEnrollment._Voidreason = ""
                objEnrollment._Voiduserunkid = -1
            Else
                objEnrollment._Iscancel = objEnrollment._Iscancel
                objEnrollment._Cancellationdate = objEnrollment._Cancellationdate
                objEnrollment._Cancellationreason = objEnrollment._Cancellationreason
                objEnrollment._Cancellationuserunkid = objEnrollment._Cancellationuserunkid
                objEnrollment._Isvoid = objEnrollment._Isvoid
                objEnrollment._Voiddatetime = objEnrollment._Voiddatetime
                objEnrollment._Voidreason = objEnrollment._Voidreason
                objEnrollment._Voiduserunkid = objEnrollment._Voiduserunkid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = objMaster.GetTraining_Status("Status", True)
            Dim dtTable As DataTable
            If mintTraningEnrollmentTranUnkid = -1 Then
                dtTable = New DataView(dsList.Tables("Status"), "Id < 2", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("Status")
            End If
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objMaster.getComboListPAYYEAR("Year", True)
            dsList = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboTrainingYears
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Year")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : objMaster = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objbtnAddTraining.Enabled = User._Object.Privilege._AddCourseScheduling
            'S.SANDEEP [ 18 FEB 2012 ] -- END

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                chkTnAEnrollment.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis)
                chkTnAEnrollment.Checked = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis)
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboCourse.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Course is compulsory information. Please select Course to continue."), enMsgBoxStyle.Information)
                cboCourse.Focus()
                Return False
            End If

            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
            '    cboEmployee.Focus()
            '    Return False
            'End If

            If lvEnrollEmployee.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information. Please Add Employee into list to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Status is compulsory information. Please select Status to continue."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            End If

            'S.SANDEEP [ 03 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(txtTotalPosition.Text.Trim) <= CInt(txtFilledPosition.Text.Trim) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Maximum Position limit is reached. Please increase the postion limit in order to enroll new employee."), enMsgBoxStyle.Information)
            '    txtFilledPosition.Focus()
            '    Return False
            'End If

            If CInt(txtTotalPosition.Text.Trim) < CInt(txtFilledPosition.Text.Trim) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Maximum Position limit is reached. Please increase the postion limit in order to enroll new employee."), enMsgBoxStyle.Information)
                txtFilledPosition.Focus()
                Return False
            End If
            'S.SANDEEP [ 03 AUG 2013 ] -- END


            

            'S.SANDEEP [ 24 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES [<TRAINING>]
            If dtpEnrolldate.Value.Date > dtpEndDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, enrolment date cannot be greater then training schduling end date."), enMsgBoxStyle.Information)
                dtpEnrolldate.Focus()
                Return False
            End If
            'S.SANDEEP [ 24 FEB 2012 ] -- END 

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmNewEnrollCancellation_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEnrollment = Nothing
    End Sub

    Private Sub frmNewEnrollCancellation_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnEnroll.PerformClick()
        End If
    End Sub

    Private Sub frmNewEnrollCancellation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEnrollmentAndCancellation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEnrollment = New clsTraining_Enrollment_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objEnrollment._Trainingenrolltranunkid = mintTraningEnrollmentTranUnkid
            End If

            Call SetVisibility()

            cboEmployee.Focus()

            mblnFormLoad = True
            lvEnrollEmployee.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEnrollmentAndCancellation_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Enrollment_Tran.SetMessages()

            objfrm._Other_ModuleNames = "clsTraining_Enrollment_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEnroll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnroll.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub
            Call SetValue()
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objEnrollment.Update()
            Else
                For Each lvItem As ListViewItem In lvEnrollEmployee.Items
                    objEnrollment._Employeeunkid = CInt(lvItem.Tag)
                    blnFlag = objEnrollment.Insert()
                    'S.SANDEEP [06-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : Training Module Notification
                    If blnFlag Then
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objEnrollment.SendNotification(CInt(cboCourse.SelectedValue), CInt(lvItem.Tag), _
                        '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                               txtEnrollRemark.Text, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Username)
                        objEnrollment.SendNotification(CInt(cboCourse.SelectedValue), CInt(lvItem.Tag), _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       txtEnrollRemark.Text, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Username, Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    End If
                    'S.SANDEEP [06-MAR-2017] -- END
                Next
            End If

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEnroll_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub



    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub objbtnAddTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmCourseScheduling
    '    Dim intRefId As Integer = -1
    '    Try
    '        If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
    '            Dim dsList As New DataSet
    '            Dim objCourse As New clsTraining_Scheduling
    '            'S.SANDEEP [ 18 FEB 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'dsList = objCourse.getComboList("Title", True)
    '            dsList = objCourse.getComboList("Title", True, , True)
    '            'S.SANDEEP [ 18 FEB 2012 ] -- END
    '            With cboCourse
    '                .ValueMember = "Id"
    '                .DisplayMember = "Name"
    '                .DataSource = dsList.Tables("Title")
    '                .SelectedValue = intRefId
    '            End With
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnAddTraining_Click", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub objbtnSearchCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCourse.Click
        Dim frm As New frmCommonSearch
        Try
            If cboCourse.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cboCourse.ValueMember
                .DisplayMember = cboCourse.DisplayMember
                .DataSource = CType(cboCourse.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboCourse.SelectedValue = frm.SelectedValue
                cboCourse.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCourse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 18 FEB 2012 ] -- END


    

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                If CInt(txtFilledPosition.Text) >= CInt(txtTotalPosition.Text) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Maximum Position limit is reached. Please increase the postion limit in order to enroll new employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim dTemp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                Dim intPosition As Integer = CInt(txtFilledPosition.Text)
                If dTemp.Length > 0 Then
                    If lvEnrollEmployee.FindItemWithText(CStr(dTemp(0)("employeecode"))) Is Nothing Then
                        Dim lvItem As New ListViewItem
                        lvItem.Text = dTemp(0)("employeecode").ToString
                        lvItem.SubItems.Add(cboEmployee.Text.ToString)
                        lvItem.SubItems.Add("0")
                        lvItem.Tag = cboEmployee.SelectedValue
                        lvEnrollEmployee.Items.Add(lvItem)
                        intPosition = intPosition + 1 'Anjan ( 28 Aug 2017) - Start Issue enrolled position was getting added as many as times user was clicking ADD button for same employee.
                    End If
                End If
                'Anjan(28 Aug 2017) - Start
                'intPosition += 1
                'Anjan ( 28 Aug 2017) - End

                txtFilledPosition.Text = CStr(intPosition)
                If lvEnrollEmployee.Items.Count > 11 Then
                    colhEName.Width = 270 - 20
                Else
                    colhEName.Width = 270
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim intPosition As Integer = CInt(txtFilledPosition.Text)
            For Each lvItem As ListViewItem In lvEnrollEmployee.CheckedItems
                lvItem.Remove()
                intPosition -= 1
            Next
            txtFilledPosition.Text = CStr(intPosition)
            lvEnrollEmployee.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Contols "

    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            intSelectedEmployeeId = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCourse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCourse.SelectedIndexChanged
        Try
            If CInt(cboCourse.SelectedValue) > 0 Then
                Dim intTotal As Integer = 0
                Dim dTemp() As DataRow = CType(cboCourse.DataSource, DataTable).Select("Id = '" & CInt(cboCourse.SelectedValue) & "'")
                If dTemp.Length > 0 Then
                    txtEligibility.Text = dTemp(0)("eligibility_criteria").ToString
                    txtTotalPosition.Text = dTemp(0)("position_no").ToString
                    dtpStartDate.Value = CDate(dTemp(0)("start_date"))
                    dtpEndDate.Value = CDate(dTemp(0)("end_date"))
                    objEnrollment.Get_Filled_Position(CInt(cboCourse.SelectedValue), intTotal)
                    txtFilledPosition.Text = CStr(intTotal)
                    dtpEnrolldate.MaxDate = dtpEndDate.Value
                    If chkTnAEnrollment.CheckState = CheckState.Checked Then
                        Dim objEmpCourse As New clshrtnaemp_course_Tran
                        Dim dsData As New DataSet

                        dsData = objEmpCourse.GetEmployeeBasedOnCourse(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                       ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                       CInt(dTemp(0)("courseunkid")), "List")
                        lvEnrollEmployee.Items.Clear()
                        If dsData.Tables("List").Rows.Count > 0 Then
                            For Each dRow As DataRow In dsData.Tables("List").Rows
                                Dim lvItem As New ListViewItem

                                lvItem.Text = dRow.Item("ECode").ToString
                                lvItem.SubItems.Add(dRow.Item("EName").ToString)
                                lvItem.SubItems.Add("1")
                                lvItem.Tag = dRow.Item("EmpId").ToString
                                lvItem.ForeColor = Color.Gray

                                lvEnrollEmployee.Items.Add(lvItem)
                            Next
                            If intTotal <= 0 Then
                                intTotal += lvEnrollEmployee.Items.Count
                                txtFilledPosition.Text = CStr(intTotal)
                            End If
                        End If
                    End If
                End If
            Else
                lvEnrollEmployee.Items.Clear()
                txtFilledPosition.Text = CStr(0)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCourse_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim ObjEmp As New clsEmployee_Master
                Dim ObjDept As New clsDepartment
                Dim ObjJob As New clsJobs

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'ObjEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                ObjEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                ObjDept._Departmentunkid = ObjEmp._Departmentunkid
                ObjJob._Jobunkid = ObjEmp._Jobunkid

                txtDepartment.Text = ObjDept._Name
                txtJob.Text = ObjJob._Job_Name

                ObjEmp = Nothing
                ObjDept = Nothing
                ObjJob = Nothing
            Else
                txtDepartment.Text = ""
                txtJob.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTrainingYears_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrainingYears.SelectedIndexChanged
        Try
            If CInt(cboTrainingYears.SelectedValue) > 0 Then
                Dim objCourse As New clsTraining_Scheduling
                Dim dsList As New DataSet
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objCourse.getComboList("Title", True, CInt(cboTrainingYears.SelectedValue))
                dsList = objCourse.getComboList("Title", True, CInt(cboTrainingYears.SelectedValue), True)
                'S.SANDEEP [ 18 FEB 2012 ] -- END
                With cboCourse
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Title")
                    If mintTraningEnrollmentTranUnkid = -1 Then
                        .SelectedValue = 0
                    Else
                        .SelectedValue = objEnrollment._Trainingschedulingunkid
                    End If
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrainingYears_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEnrollEmployee_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEnrollEmployee.ItemChecked
        Try
            If e.Item.ForeColor = Color.Gray Then
                e.Item.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEnrollEmployee_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.gbCourseDetails.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCourseDetails.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmployeeInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEnroll.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEnroll.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbCourseDetails.Text = Language._Object.getCaption(Me.gbCourseDetails.Name, Me.gbCourseDetails.Text)
			Me.lblTrainingYear.Text = Language._Object.getCaption(Me.lblTrainingYear.Name, Me.lblTrainingYear.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblEnrollmentDate.Text = Language._Object.getCaption(Me.lblEnrollmentDate.Name, Me.lblEnrollmentDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblEligibility.Text = Language._Object.getCaption(Me.lblEligibility.Name, Me.lblEligibility.Text)
			Me.lblNoOfPositons.Text = Language._Object.getCaption(Me.lblNoOfPositons.Name, Me.lblNoOfPositons.Text)
			Me.lblFilledPositions.Text = Language._Object.getCaption(Me.lblFilledPositions.Name, Me.lblFilledPositions.Text)
			Me.chkTnAEnrollment.Text = Language._Object.getCaption(Me.chkTnAEnrollment.Name, Me.chkTnAEnrollment.Text)
			Me.gbEmployeeInformation.Text = Language._Object.getCaption(Me.gbEmployeeInformation.Name, Me.gbEmployeeInformation.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.EZeeLine2.Text = Language._Object.getCaption(Me.EZeeLine2.Name, Me.EZeeLine2.Text)
			Me.lblEnrollmentRemark.Text = Language._Object.getCaption(Me.lblEnrollmentRemark.Name, Me.lblEnrollmentRemark.Text)
			Me.colhECode.Text = Language._Object.getCaption(CStr(Me.colhECode.Tag), Me.colhECode.Text)
			Me.colhEName.Text = Language._Object.getCaption(CStr(Me.colhEName.Tag), Me.colhEName.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnEnroll.Text = Language._Object.getCaption(Me.btnEnroll.Name, Me.btnEnroll.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Course is compulsory information. Please select Course to continue.")
			Language.setMessage(mstrModuleName, 2, "Employee is compulsory information. Please Add Employee into list to continue.")
			Language.setMessage(mstrModuleName, 3, "Status is compulsory information. Please select Status to continue.")
			Language.setMessage(mstrModuleName, 4, "Maximum Position limit is reached. Please increase the postion limit in order to enroll new employee.")
			Language.setMessage(mstrModuleName, 5, "Sorry, enrolment date cannot be greater then training schduling end date.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class