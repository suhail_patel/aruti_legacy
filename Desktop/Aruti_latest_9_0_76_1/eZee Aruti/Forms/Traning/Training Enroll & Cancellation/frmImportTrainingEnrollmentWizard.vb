﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportTrainingEnrollmentWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportTrainingEnrollmentWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboCourse.Items.Add(dtColumns.ColumnName)
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboEndDate.Items.Add(dtColumns.ColumnName)
                cboEnrollmentDate.Items.Add(dtColumns.ColumnName)
                cboInstitue.Items.Add(dtColumns.ColumnName)
                cboStartDate.Items.Add(dtColumns.ColumnName)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True
            mdt_ImportData_Others.Columns.Add("coursename", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("course_startdate", System.Type.GetType("System.DateTime"))
            mdt_ImportData_Others.Columns.Add("course_enddate", System.Type.GetType("System.DateTime"))
            mdt_ImportData_Others.Columns.Add("training_institute", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("enrollmentdate", System.Type.GetType("System.DateTime"))
            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow
            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("coursename") = dtRow.Item(cboCourse.Text).ToString.Trim
                drNewRow.Item("course_startdate") = dtRow.Item(cboStartDate.Text).ToString.Trim
                drNewRow.Item("course_enddate") = dtRow.Item(cboEndDate.Text).ToString.Trim
                drNewRow.Item("training_institute") = dtRow.Item(cboInstitue.Text).ToString.Trim
                drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                drNewRow.Item("enrollmentdate") = dtRow.Item(cboEnrollmentDate.Text).ToString.Trim
                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""
                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next
            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "employeecode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizImportEnrollment.BackEnabled = False
            eZeeWizImportEnrollment.CancelText = "Finish"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboCourse.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Course cannot be blank. Please set Course in order to import Training Enrollment(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboStartDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Training Start Date cannot be blank. Please set Training Start Date in order to import Training Enrollment(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEndDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Training End Date cannot be blank. Please set Training End Date in order to import Training Enrollment(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboInstitue.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Training Institue cannot be blank. Please set Training Institue in order to import Training Enrollment(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee Code cannot be blank. Please set Employee Code in order to import Training Enrollment(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEnrollmentDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Enrollment Date cannot be blank. Please set Enrollment Date in order to import Training Enrollment(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        btnFilter.Enabled = False
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If
            Dim objCMaster As New clsCommon_Master
            Dim objEMaster As New clsEmployee_Master
            Dim objInstitute As New clsinstitute_master
            Dim objScheduling As clsTraining_Scheduling

            Dim xCourseMasterId, xEmployeeId, xInstituteId, xScheduleTranId, xTotalCount As Integer
            xTotalCount = 1
            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                xCourseMasterId = 0 : xEmployeeId = 0 : xInstituteId = 0 : xScheduleTranId = 0

                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                '------------------------------ CHECKING IF COURSE PRESENT.
                If dtRow.Item("coursename").ToString.Trim.Length > 0 Then
                    xCourseMasterId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, dtRow.Item("coursename").ToString.Trim)
                    If xCourseMasterId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Training Course Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING IF INSTITUTE PRESENT.
                If dtRow.Item("training_institute").ToString.Trim.Length > 0 Then
                    xInstituteId = objInstitute.GetInstituteUnkid(True, dtRow.Item("training_institute").ToString.Trim)
                    If xInstituteId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Training Institute Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("employeecode").ToString.Trim.Length > 0 Then
                    xEmployeeId = objEMaster.GetEmployeeUnkid("", dtRow.Item("employeecode").ToString.Trim)
                    If xEmployeeId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING IF VALID START DATE.
                If dtRow.Item("course_startdate").ToString.Trim.Length > 0 Then
                    Try
                        If IsDate(CDate(dtRow.Item("course_startdate"))) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Invalid Start Date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Invalid Start Date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End Try
                End If

                '------------------------------ CHECKING IF VALID END DATE.
                If dtRow.Item("course_enddate").ToString.Trim.Length > 0 Then
                    Try
                        If IsDate(CDate(dtRow.Item("course_enddate"))) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Invalid End Date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Invalid End Date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End Try
                End If

                '------------------------------ CHECKING IF VALID ENROLLMENT DATE.
                If dtRow.Item("enrollmentdate").ToString.Trim.Length > 0 Then
                    Try
                        If IsDate(CDate(dtRow.Item("enrollmentdate"))) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Invalid Enrollment Date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Invalid Enrollment Date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End Try
                End If

                '------------------------------ CHECKING IF START & END DATES ARE VALID.
                If CDate(dtRow.Item("course_enddate")).Date < CDate(dtRow.Item("course_startdate")).Date Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "End Date Cannot be less then Start Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                '------------------------------ CHECKING IF ENROLLMENT DATE & END DATE ARE VALID.
                If CDate(dtRow.Item("enrollmentdate")).Date > CDate(dtRow.Item("course_enddate")).Date Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Enrollment Date Cannot be Greater then End Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If


                objScheduling = New clsTraining_Scheduling
                xScheduleTranId = objScheduling.GetTrainingSchecduleId(xCourseMasterId, CDate(dtRow.Item("course_startdate")).Date, CDate(dtRow.Item("course_enddate")).Date, xInstituteId)
                If xScheduleTranId <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Scheduled Training Not Found.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                Else

                    objScheduling._Trainingschedulingunkid = xScheduleTranId

                    If xTotalCount > objScheduling._Postion_No Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Maximum Position limit is reached.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Exit Sub
                    End If

                    Dim objEnrollment As New clsTraining_Enrollment_Tran
                    objEnrollment._Cancellationdate = Nothing
                    objEnrollment._Cancellationreason = ""
                    objEnrollment._Employeeunkid = xEmployeeId
                    objEnrollment._Enroll_Date = CDate(dtRow.Item("enrollmentdate"))
                    objEnrollment._Enroll_Remark = ""
                    objEnrollment._Iscancel = False
                    objEnrollment._Isqualificaionupdated = False
                    objEnrollment._Isvoid = False
                    objEnrollment._Jobunkid = 0
                    objEnrollment._Operationmodeid = 0
                    objEnrollment._Periodunkid = 0
                    objEnrollment._SalaryincrementtranUnkid = 0
                    objEnrollment._Status_Id = 1    'IN PROGRESS
                    objEnrollment._Trainingschedulingunkid = xScheduleTranId
                    objEnrollment._Userunkid = User._Object._Userunkid
                    objEnrollment._Voiddatetime = Nothing
                    objEnrollment._Voidreason = ""
                    objEnrollment._Voiduserunkid = -1

                    If objEnrollment.Insert Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 21, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                        xTotalCount += 1
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objEnrollment._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEnrollment_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEnrollment.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportEnrollment.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEnrollment_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportEnrollment_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEnrollment.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEnrollment.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select the correct file to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select the correct file to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Call SetDataCombo()
                Case eZeeWizImportEnrollment.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case eZeeWizImportEnrollment.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEnrollment_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmImportTrainingEnrollmentWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportTrainingEnrollmentWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Event's "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Name <> cmb.Name Then
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor

            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizImportEnrollment.CancelText = Language._Object.getCaption(Me.eZeeWizImportEnrollment.Name & "_CancelText", Me.eZeeWizImportEnrollment.CancelText)
            Me.eZeeWizImportEnrollment.NextText = Language._Object.getCaption(Me.eZeeWizImportEnrollment.Name & "_NextText", Me.eZeeWizImportEnrollment.NextText)
            Me.eZeeWizImportEnrollment.BackText = Language._Object.getCaption(Me.eZeeWizImportEnrollment.Name & "_BackText", Me.eZeeWizImportEnrollment.BackText)
            Me.eZeeWizImportEnrollment.FinishText = Language._Object.getCaption(Me.eZeeWizImportEnrollment.Name & "_FinishText", Me.eZeeWizImportEnrollment.FinishText)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCoursename.Text = Language._Object.getCaption(Me.lblCoursename.Name, Me.lblCoursename.Text)
            Me.lblEnrollmentDate.Text = Language._Object.getCaption(Me.lblEnrollmentDate.Name, Me.lblEnrollmentDate.Text)
            Me.lblInstitue.Text = Language._Object.getCaption(Me.lblInstitue.Name, Me.lblInstitue.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 2, "Course cannot be blank. Please set Course in order to import Training Enrollment(s).")
            Language.setMessage(mstrModuleName, 3, "Training Start Date cannot be blank. Please set Training Start Date in order to import Training Enrollment(s).")
            Language.setMessage(mstrModuleName, 4, "Training End Date cannot be blank. Please set Training End Date in order to import Training Enrollment(s).")
            Language.setMessage(mstrModuleName, 5, "Training Institue cannot be blank. Please set Training Institue in order to import Training Enrollment(s).")
            Language.setMessage(mstrModuleName, 6, "Employee Code cannot be blank. Please set Employee Code in order to import Training Enrollment(s).")
            Language.setMessage(mstrModuleName, 7, "Enrollment Date cannot be blank. Please set Enrollment Date in order to import Training Enrollment(s).")
            Language.setMessage(mstrModuleName, 8, "Please select the correct file to Import Data.")
            Language.setMessage(mstrModuleName, 9, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 10, "Training Course Not Found.")
            Language.setMessage(mstrModuleName, 11, "Fail")
            Language.setMessage(mstrModuleName, 12, "Training Institute Not Found.")
            Language.setMessage(mstrModuleName, 13, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 14, "Invalid Start Date.")
            Language.setMessage(mstrModuleName, 15, "Invalid End Date.")
            Language.setMessage(mstrModuleName, 16, "Invalid Enrollment Date.")
            Language.setMessage(mstrModuleName, 17, "End Date Cannot be less then Start Date.")
            Language.setMessage(mstrModuleName, 18, "Enrollment Date Cannot be Greater then End Date.")
            Language.setMessage(mstrModuleName, 19, "Scheduled Training Not Found.")
            Language.setMessage(mstrModuleName, 20, "Maximum Position limit is reached.")
            Language.setMessage(mstrModuleName, 21, "Success")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class