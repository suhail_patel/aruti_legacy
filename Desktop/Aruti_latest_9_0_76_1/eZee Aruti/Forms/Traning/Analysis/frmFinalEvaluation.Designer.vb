﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinalEvaluation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFinalEvaluation))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbTrainingAnalysis = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtGPA = New eZee.TextBox.IntegerTextBox
        Me.lblGPA = New System.Windows.Forms.Label
        Me.elEvaluationInfo = New eZee.Common.eZeeLine
        Me.elCourseInfo = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtQualification = New eZee.TextBox.AlphanumericTextBox
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.txtQualificationGroup = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblQualificationAward = New System.Windows.Forms.Label
        Me.cboTrainer = New System.Windows.Forms.ComboBox
        Me.lblTrainer = New System.Windows.Forms.Label
        Me.lblTraningCourse = New System.Windows.Forms.Label
        Me.cboScore = New System.Windows.Forms.ComboBox
        Me.dtpFinalDate = New System.Windows.Forms.DateTimePicker
        Me.lblScore = New System.Windows.Forms.Label
        Me.txtCourseName = New eZee.TextBox.AlphanumericTextBox
        Me.txtTaineeName = New eZee.TextBox.AlphanumericTextBox
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblCourseName = New System.Windows.Forms.Label
        Me.lblTraineeName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbTrainingAnalysis.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbTrainingAnalysis)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(541, 356)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbTrainingAnalysis
        '
        Me.gbTrainingAnalysis.BorderColor = System.Drawing.Color.Black
        Me.gbTrainingAnalysis.Checked = False
        Me.gbTrainingAnalysis.CollapseAllExceptThis = False
        Me.gbTrainingAnalysis.CollapsedHoverImage = Nothing
        Me.gbTrainingAnalysis.CollapsedNormalImage = Nothing
        Me.gbTrainingAnalysis.CollapsedPressedImage = Nothing
        Me.gbTrainingAnalysis.CollapseOnLoad = False
        Me.gbTrainingAnalysis.Controls.Add(Me.txtGPA)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblGPA)
        Me.gbTrainingAnalysis.Controls.Add(Me.elEvaluationInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.elCourseInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtQualification)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtRemark)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtQualificationGroup)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblRemark)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblQualificationAward)
        Me.gbTrainingAnalysis.Controls.Add(Me.cboTrainer)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblTrainer)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblTraningCourse)
        Me.gbTrainingAnalysis.Controls.Add(Me.cboScore)
        Me.gbTrainingAnalysis.Controls.Add(Me.dtpFinalDate)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblScore)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtCourseName)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtTaineeName)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblDate)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblCourseName)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblTraineeName)
        Me.gbTrainingAnalysis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTrainingAnalysis.ExpandedHoverImage = Nothing
        Me.gbTrainingAnalysis.ExpandedNormalImage = Nothing
        Me.gbTrainingAnalysis.ExpandedPressedImage = Nothing
        Me.gbTrainingAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTrainingAnalysis.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTrainingAnalysis.HeaderHeight = 25
        Me.gbTrainingAnalysis.HeaderMessage = ""
        Me.gbTrainingAnalysis.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTrainingAnalysis.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTrainingAnalysis.HeightOnCollapse = 0
        Me.gbTrainingAnalysis.LeftTextSpace = 0
        Me.gbTrainingAnalysis.Location = New System.Drawing.Point(0, 0)
        Me.gbTrainingAnalysis.Name = "gbTrainingAnalysis"
        Me.gbTrainingAnalysis.OpenHeight = 64
        Me.gbTrainingAnalysis.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTrainingAnalysis.ShowBorder = True
        Me.gbTrainingAnalysis.ShowCheckBox = False
        Me.gbTrainingAnalysis.ShowCollapseButton = False
        Me.gbTrainingAnalysis.ShowDefaultBorderColor = True
        Me.gbTrainingAnalysis.ShowDownButton = False
        Me.gbTrainingAnalysis.ShowHeader = True
        Me.gbTrainingAnalysis.Size = New System.Drawing.Size(541, 356)
        Me.gbTrainingAnalysis.TabIndex = 0
        Me.gbTrainingAnalysis.Temp = 0
        Me.gbTrainingAnalysis.Text = "Training Awards"
        Me.gbTrainingAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGPA
        '
        Me.txtGPA.AllowNegative = False
        Me.txtGPA.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtGPA.DigitsInGroup = 0
        Me.txtGPA.Flags = 65536
        Me.txtGPA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGPA.Location = New System.Drawing.Point(441, 218)
        Me.txtGPA.MaxDecimalPlaces = 2
        Me.txtGPA.MaxWholeDigits = 3
        Me.txtGPA.Name = "txtGPA"
        Me.txtGPA.Prefix = ""
        Me.txtGPA.RangeMax = 2147483647
        Me.txtGPA.RangeMin = -2147483648
        Me.txtGPA.Size = New System.Drawing.Size(88, 21)
        Me.txtGPA.TabIndex = 191
        Me.txtGPA.Text = "0"
        Me.txtGPA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblGPA
        '
        Me.lblGPA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGPA.Location = New System.Drawing.Point(385, 221)
        Me.lblGPA.Name = "lblGPA"
        Me.lblGPA.Size = New System.Drawing.Size(50, 15)
        Me.lblGPA.TabIndex = 190
        Me.lblGPA.Text = "GPA"
        '
        'elEvaluationInfo
        '
        Me.elEvaluationInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEvaluationInfo.Location = New System.Drawing.Point(11, 166)
        Me.elEvaluationInfo.Name = "elEvaluationInfo"
        Me.elEvaluationInfo.Size = New System.Drawing.Size(518, 17)
        Me.elEvaluationInfo.TabIndex = 189
        Me.elEvaluationInfo.Text = "Evaluation Information"
        Me.elEvaluationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elCourseInfo
        '
        Me.elCourseInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elCourseInfo.Location = New System.Drawing.Point(8, 33)
        Me.elCourseInfo.Name = "elCourseInfo"
        Me.elCourseInfo.Size = New System.Drawing.Size(521, 17)
        Me.elCourseInfo.TabIndex = 188
        Me.elCourseInfo.Text = "Trainee and Course Information"
        Me.elCourseInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(508, 191)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 187
        '
        'txtQualification
        '
        Me.txtQualification.BackColor = System.Drawing.Color.White
        Me.txtQualification.Flags = 0
        Me.txtQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQualification.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtQualification.Location = New System.Drawing.Point(146, 137)
        Me.txtQualification.Name = "txtQualification"
        Me.txtQualification.ReadOnly = True
        Me.txtQualification.Size = New System.Drawing.Size(383, 21)
        Me.txtQualification.TabIndex = 21
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(146, 245)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(383, 51)
        Me.txtRemark.TabIndex = 15
        '
        'txtQualificationGroup
        '
        Me.txtQualificationGroup.BackColor = System.Drawing.Color.White
        Me.txtQualificationGroup.Flags = 0
        Me.txtQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQualificationGroup.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtQualificationGroup.Location = New System.Drawing.Point(146, 110)
        Me.txtQualificationGroup.Name = "txtQualificationGroup"
        Me.txtQualificationGroup.ReadOnly = True
        Me.txtQualificationGroup.Size = New System.Drawing.Size(383, 21)
        Me.txtQualificationGroup.TabIndex = 20
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(36, 248)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(104, 15)
        Me.lblRemark.TabIndex = 14
        Me.lblRemark.Text = "Remark"
        '
        'lblQualificationAward
        '
        Me.lblQualificationAward.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationAward.Location = New System.Drawing.Point(36, 140)
        Me.lblQualificationAward.Name = "lblQualificationAward"
        Me.lblQualificationAward.Size = New System.Drawing.Size(104, 15)
        Me.lblQualificationAward.TabIndex = 19
        Me.lblQualificationAward.Text = "Qualification"
        Me.lblQualificationAward.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrainer
        '
        Me.cboTrainer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainer.FormattingEnabled = True
        Me.cboTrainer.Location = New System.Drawing.Point(146, 191)
        Me.cboTrainer.Name = "cboTrainer"
        Me.cboTrainer.Size = New System.Drawing.Size(356, 21)
        Me.cboTrainer.TabIndex = 1
        '
        'lblTrainer
        '
        Me.lblTrainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainer.Location = New System.Drawing.Point(36, 194)
        Me.lblTrainer.Name = "lblTrainer"
        Me.lblTrainer.Size = New System.Drawing.Size(104, 15)
        Me.lblTrainer.TabIndex = 0
        Me.lblTrainer.Text = "Trainer"
        '
        'lblTraningCourse
        '
        Me.lblTraningCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningCourse.Location = New System.Drawing.Point(36, 113)
        Me.lblTraningCourse.Name = "lblTraningCourse"
        Me.lblTraningCourse.Size = New System.Drawing.Size(104, 15)
        Me.lblTraningCourse.TabIndex = 16
        Me.lblTraningCourse.Text = "Qualification Group"
        Me.lblTraningCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboScore
        '
        Me.cboScore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScore.FormattingEnabled = True
        Me.cboScore.Location = New System.Drawing.Point(146, 218)
        Me.cboScore.Name = "cboScore"
        Me.cboScore.Size = New System.Drawing.Size(233, 21)
        Me.cboScore.TabIndex = 6
        '
        'dtpFinalDate
        '
        Me.dtpFinalDate.Checked = False
        Me.dtpFinalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFinalDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFinalDate.Location = New System.Drawing.Point(441, 56)
        Me.dtpFinalDate.Name = "dtpFinalDate"
        Me.dtpFinalDate.Size = New System.Drawing.Size(88, 21)
        Me.dtpFinalDate.TabIndex = 5
        '
        'lblScore
        '
        Me.lblScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore.Location = New System.Drawing.Point(36, 221)
        Me.lblScore.Name = "lblScore"
        Me.lblScore.Size = New System.Drawing.Size(104, 15)
        Me.lblScore.TabIndex = 5
        Me.lblScore.Text = "Score"
        '
        'txtCourseName
        '
        Me.txtCourseName.BackColor = System.Drawing.Color.White
        Me.txtCourseName.Flags = 0
        Me.txtCourseName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCourseName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCourseName.Location = New System.Drawing.Point(146, 83)
        Me.txtCourseName.Name = "txtCourseName"
        Me.txtCourseName.ReadOnly = True
        Me.txtCourseName.Size = New System.Drawing.Size(383, 21)
        Me.txtCourseName.TabIndex = 3
        '
        'txtTaineeName
        '
        Me.txtTaineeName.BackColor = System.Drawing.Color.White
        Me.txtTaineeName.Flags = 0
        Me.txtTaineeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTaineeName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTaineeName.Location = New System.Drawing.Point(146, 56)
        Me.txtTaineeName.Name = "txtTaineeName"
        Me.txtTaineeName.ReadOnly = True
        Me.txtTaineeName.Size = New System.Drawing.Size(233, 21)
        Me.txtTaineeName.TabIndex = 1
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(385, 59)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(50, 15)
        Me.lblDate.TabIndex = 4
        Me.lblDate.Text = "Date"
        '
        'lblCourseName
        '
        Me.lblCourseName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourseName.Location = New System.Drawing.Point(36, 86)
        Me.lblCourseName.Name = "lblCourseName"
        Me.lblCourseName.Size = New System.Drawing.Size(104, 15)
        Me.lblCourseName.TabIndex = 2
        Me.lblCourseName.Text = "Course Name"
        '
        'lblTraineeName
        '
        Me.lblTraineeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraineeName.Location = New System.Drawing.Point(36, 59)
        Me.lblTraineeName.Name = "lblTraineeName"
        Me.lblTraineeName.Size = New System.Drawing.Size(104, 15)
        Me.lblTraineeName.TabIndex = 0
        Me.lblTraineeName.Text = "Trainee Name"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 301)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(541, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(329, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(432, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmFinalEvaluation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(541, 356)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFinalEvaluation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Awards"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbTrainingAnalysis.ResumeLayout(False)
        Me.gbTrainingAnalysis.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbTrainingAnalysis As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents txtCourseName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTaineeName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblCourseName As System.Windows.Forms.Label
    Friend WithEvents lblTraineeName As System.Windows.Forms.Label
    Friend WithEvents dtpFinalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboScore As System.Windows.Forms.ComboBox
    Friend WithEvents lblScore As System.Windows.Forms.Label
    Friend WithEvents cboTrainer As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainer As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblQualificationAward As System.Windows.Forms.Label
    Friend WithEvents lblTraningCourse As System.Windows.Forms.Label
    Friend WithEvents elCourseInfo As eZee.Common.eZeeLine
    Friend WithEvents txtQualification As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtQualificationGroup As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents elEvaluationInfo As eZee.Common.eZeeLine
    Friend WithEvents txtGPA As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblGPA As System.Windows.Forms.Label
End Class
