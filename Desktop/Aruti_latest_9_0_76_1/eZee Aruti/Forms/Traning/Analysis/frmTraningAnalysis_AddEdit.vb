﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmTraningAnalysis_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTraningAnalysis_AddEdit"
    Private mblnCancel As Boolean = True
    Private objAnalysisMaster As clsTraining_Analysis_Master
    Private objScheduling As clsTraining_Scheduling
    Private objEnrollemt As clsTraining_Enrollment_Tran
    Private objTrainers As clsTraining_Trainers_Tran
    Private objEmployee As clsEmployee_Master
    Private objAnaLysisTran As clsTraining_Analysis_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAnalysisMasterUnkid As Integer = -1
    Private mintEnrollmentUnkid As Integer = -1
    Private mdtTran As DataTable
    Private intAnalysisSeletedIdx As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intEnrollId As Integer) As Boolean
        Try
            mintAnalysisMasterUnkid = intUnkId
            menAction = eAction
            mintEnrollmentUnkid = intEnrollId

            Me.ShowDialog()

            intUnkId = mintAnalysisMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtCourse.BackColor = GUI.ColorComp
            txtEmployeeName.BackColor = GUI.ColorComp
            txtTraineeRemark.BackColor = GUI.ColorOptional
            txtRevewier_remark.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorComp
            cboOtherEmployee.BackColor = GUI.ColorComp
            cboScore.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try

            'Sandeep [ 02 Oct 2010 ] -- Start
            'If objAnalysisMaster._Analysis_Date <> Nothing Then
            '    dtpAnalysisDate.Value = objAnalysisMaster._Analysis_Date
            'End If
            dtpAnalysisDate.Value = Now
            'Sandeep [ 02 Oct 2010 ] -- End 
            txtTraineeRemark.Text = objAnalysisMaster._Trainee_Remark
            objAnalysisMaster._Userinkid = objAnalysisMaster._Userinkid
            objAnalysisMaster._Isvoid = objAnalysisMaster._Isvoid
            objAnalysisMaster._Voidatetime = objAnalysisMaster._Voidatetime
            objAnalysisMaster._Voidreason = objAnalysisMaster._Voidreason
            objAnalysisMaster._Voiduserunkid = objAnalysisMaster._Voiduserunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Sandeep [ 02 Oct 2010 ] -- Start
            'objAnalysisMaster._Analysis_Date = dtpAnalysisDate.Value
            'Sandeep [ 02 Oct 2010 ] -- End 
            objAnalysisMaster._Trainee_Remark = txtTraineeRemark.Text
            If menAction = enAction.EDIT_ONE Then
                objAnalysisMaster._Trainingenrolltranunkid = objAnalysisMaster._Trainingenrolltranunkid
            Else
                objAnalysisMaster._Trainingenrolltranunkid = mintEnrollmentUnkid
            End If
            If mintAnalysisMasterUnkid = -1 Then
                objAnalysisMaster._Isvoid = False
                objAnalysisMaster._Voidatetime = Nothing
                objAnalysisMaster._Voidreason = ""
                objAnalysisMaster._Voiduserunkid = -1
                objAnalysisMaster._Userinkid = User._Object._Userunkid
            Else
                objAnalysisMaster._Userinkid = objAnalysisMaster._Userinkid
                objAnalysisMaster._Isvoid = objAnalysisMaster._Isvoid
                objAnalysisMaster._Voidatetime = objAnalysisMaster._Voidatetime
                objAnalysisMaster._Voidreason = objAnalysisMaster._Voidreason
                objAnalysisMaster._Voiduserunkid = objAnalysisMaster._Voiduserunkid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim dtEmpTable As DataTable
        Dim dtOthTable As DataTable
        Dim objResult As New clsresult_master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTrainers.getComboList(objScheduling._Trainingschedulingunkid, objEnrollemt._Trainingenrolltranunkid, True, "Trainers", False)

            dsList = objTrainers.getComboList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, _
                                              objScheduling._Trainingschedulingunkid, objEnrollemt._Trainingenrolltranunkid, True, "Trainers", False)
            'S.SANDEEP [04 JUN 2015] -- END

            dtEmpTable = New DataView(dsList.Tables("Trainers"), "EmpId <> -1", "", DataViewRowState.CurrentRows).ToTable
            With cboEmployee
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtEmpTable
                .SelectedValue = 0
            End With

            dtOthTable = New DataView(dsList.Tables("Trainers"), "EmpId <= 0", "", DataViewRowState.CurrentRows).ToTable
            With cboOtherEmployee
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtOthTable
                .SelectedValue = 0
            End With

            dsList = objResult.getComboList("Result", True, objScheduling._Resultgroupunkid)
            With cboScore
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Result")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillInfo()
        Try
            txtEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            txtCourse.Text = objScheduling._Course_Title
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillAnalysisTran()
        Try
            lvAnalysis.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    objTrainers._TrainerUnkid = CInt(dtRow.Item("trainerstranunkid"))

                    'Sandeep [ 02 Oct 2010 ] -- Start
                    'If objTrainers._EmployeeId <> -1 Then
                    '    Dim objEmp As New clsEmployee_Master
                    '    Dim objDept As New clsDepartment
                    '    objEmp._Employeeunkid = objTrainers._EmployeeId
                    '    objDept._Departmentunkid = objEmp._Departmentunkid
                    '    lvItem.Text = objEmp._Firstname & " " & objEmp._Surname & " " & objEmp._Othername
                    '    lvItem.SubItems.Add(objDept._Name.ToString)
                    '    lvItem.SubItems.Add(Company._Object._Name.ToString)
                    '    lvItem.SubItems.Add(objEmp._Present_Tel_No.ToString)
                    '    objEmp = Nothing
                    '    objDept = Nothing
                    '    Dim objResult As New clsresult_master
                    '    objResult._Resultunkid = CInt(dtRow.Item("resultunkid"))
                    '    lvItem.SubItems.Add(objResult._Resultname.ToString)
                    '    objResult = Nothing
                    '    lvItem.SubItems.Add(dtRow.Item("reviewer_remark").ToString)
                    '    lvItem.SubItems.Add(dtRow.Item("resultunkid").ToString)
                    '    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    '    lvItem.SubItems.Add(dtRow.Item("trainerstranunkid").ToString)
                    'ElseIf objTrainers._EmployeeId = -1 Then
                    '    lvItem.Text = objTrainers._Other_Name.ToString
                    '    lvItem.SubItems.Add(objTrainers._Other_Department.ToString)
                    '    lvItem.SubItems.Add(objTrainers._Other_Company.ToString)
                    '    lvItem.SubItems.Add(objTrainers._Other_Contact.ToString)
                    '    Dim objResult As New clsresult_master
                    '    objResult._Resultunkid = CInt(dtRow.Item("resultunkid"))
                    '    lvItem.SubItems.Add(objResult._Resultname.ToString)
                    '    objResult = Nothing
                    '    lvItem.SubItems.Add(dtRow.Item("reviewer_remark").ToString)
                    '    lvItem.SubItems.Add(dtRow.Item("resultunkid").ToString)
                    '    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    '    lvItem.SubItems.Add(dtRow.Item("trainerstranunkid").ToString)
                    'End If
                    If objTrainers._EmployeeId <> -1 Then
                        Dim objEmp As New clsEmployee_Master
                        Dim objDept As New clsDepartment

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp._Employeeunkid = objTrainers._EmployeeId
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objTrainers._EmployeeId
                        'S.SANDEEP [04 JUN 2015] -- END

                        objDept._Departmentunkid = objEmp._Departmentunkid
                        lvItem.Text = objEmp._Firstname & " " & objEmp._Surname & " " & objEmp._Othername
                        lvItem.SubItems.Add(objDept._Name.ToString)
                        lvItem.SubItems.Add(Company._Object._Name.ToString)
                        lvItem.SubItems.Add(objEmp._Present_Tel_No.ToString)
                        objEmp = Nothing
                        objDept = Nothing
                    ElseIf objTrainers._EmployeeId = -1 Then
                        lvItem.Text = objTrainers._Other_Name.ToString
                        lvItem.SubItems.Add(objTrainers._Other_Department.ToString)
                        lvItem.SubItems.Add(objTrainers._Other_Company.ToString)
                        lvItem.SubItems.Add(objTrainers._Other_Contact.ToString)
                    End If
                        Dim objResult As New clsresult_master
                        objResult._Resultunkid = CInt(dtRow.Item("resultunkid"))
                        lvItem.SubItems.Add(objResult._Resultname.ToString)
                        objResult = Nothing
                        lvItem.SubItems.Add(dtRow.Item("reviewer_remark").ToString)
                        lvItem.SubItems.Add(dtRow.Item("resultunkid").ToString)

                        lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                        lvItem.SubItems.Add(dtRow.Item("trainerstranunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("analysis_date").ToString)
                    'Sandeep [ 02 Oct 2010 ] -- End 

                    lvItem.Tag = dtRow.Item("analysistranunkid")

                    lvAnalysis.Items.Add(lvItem)
                    lvItem = Nothing
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAnalysisTran", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetItems()
        Try
            If radEmployee.Checked = True Then
                cboEmployee.SelectedValue = 0
            ElseIf radOthers.Checked = True Then
                cboOtherEmployee.SelectedValue = 0
            End If
            cboScore.SelectedValue = 0
            txtRevewier_remark.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetItems", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            objbtnAddResult.Enabled = User._Object.Privilege._AddResultCode
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmTraningAnalysis_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAnalysisMaster = Nothing
    End Sub

    Private Sub frmTraningAnalysis_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmTraningAnalysis_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmTraningAnalysis_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAnalysisMaster = New clsTraining_Analysis_Master
        objScheduling = New clsTraining_Scheduling
        objEnrollemt = New clsTraining_Enrollment_Tran
        objTrainers = New clsTraining_Trainers_Tran
        objEmployee = New clsEmployee_Master
        objAnaLysisTran = New clsTraining_Analysis_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            Call SetColor()
            radEmployee.Checked = True
            If menAction = enAction.EDIT_ONE Then
                objAnalysisMaster._Analysisunkid = mintAnalysisMasterUnkid
                objEnrollemt._Trainingenrolltranunkid = objAnalysisMaster._Trainingenrolltranunkid
                objScheduling._Trainingschedulingunkid = objEnrollemt._Trainingschedulingunkid

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = objEnrollemt._Employeeunkid
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objEnrollemt._Employeeunkid
                'S.SANDEEP [04 JUN 2015] -- END

            Else
                objEnrollemt._Trainingenrolltranunkid = mintEnrollmentUnkid
                objScheduling._Trainingschedulingunkid = objEnrollemt._Trainingschedulingunkid

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = objEnrollemt._Employeeunkid
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objEnrollemt._Employeeunkid
                'S.SANDEEP [04 JUN 2015] -- END

            End If
            Call FillCombo()
            Call FillInfo()
            Call GetValue()

            objAnaLysisTran._AnalysisUnkid = mintAnalysisMasterUnkid
            mdtTran = objAnaLysisTran._DataTable
            Call FillAnalysisTran()

            txtEmployeeName.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTraningAnalysis_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Analysis_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsTraining_Analysis_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If radEmployee.Checked = True Then
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information, it can not be blank. Please select a Employee to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow() = mdtTran.Select("trainerstranunkid = " & CInt(cboEmployee.SelectedValue) & " AND AUD <> 'D' ")
            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected Employee is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        ElseIf radOthers.Checked = True Then
            If CInt(cboOtherEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information, it can not be blank. Please select a Employee to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow() = mdtTran.Select("trainerstranunkid = " & CInt(cboOtherEmployee.SelectedValue) & " AND AUD <> 'D' ")
            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected Employee is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        Try
            Dim dtARow As DataRow
            dtARow = mdtTran.NewRow

            dtARow.Item("analysistranunkid") = -1
            dtARow.Item("analysisunkid") = mintAnalysisMasterUnkid
            If radEmployee.Checked = True Then
                dtARow.Item("trainerstranunkid") = CInt(cboEmployee.SelectedValue)
            ElseIf radOthers.Checked = True Then
                dtARow.Item("trainerstranunkid") = CInt(cboOtherEmployee.SelectedValue)
            End If
            dtARow.Item("resultunkid") = CInt(cboScore.SelectedValue)
            dtARow.Item("reviewer_remark") = txtRevewier_remark.Text
            'Sandeep [ 02 Oct 2010 ] -- Start
            dtARow.Item("analysis_date") = dtpAnalysisDate.Value
            'Sandeep [ 02 Oct 2010 ] -- End 
            dtARow.Item("AUD") = "A"
            dtARow.Item("GUID") = Guid.NewGuid.ToString

            mdtTran.Rows.Add(dtARow)

            Call FillAnalysisTran()
            Call ResetItems()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvAnalysis.SelectedItems.Count > 0 Then
                If intAnalysisSeletedIdx > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvAnalysis.Items(intAnalysisSeletedIdx).Tag) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & lvAnalysis.Items(intAnalysisSeletedIdx).SubItems(objcolhGUID.Index).Text & "'")
                    Else
                        drTemp = mdtTran.Select("analysistranunkid = " & CInt(lvAnalysis.Items(intAnalysisSeletedIdx).Tag))
                    End If
                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("analysistranunkid") = lvAnalysis.Items(intAnalysisSeletedIdx).Tag
                            .Item("analysisunkid") = mintAnalysisMasterUnkid
                            If radEmployee.Checked = True Then
                                .Item("trainerstranunkid") = CInt(cboEmployee.SelectedValue)
                            ElseIf radOthers.Checked = True Then
                                .Item("trainerstranunkid") = CInt(cboOtherEmployee.SelectedValue)
                            End If
                            .Item("resultunkid") = CInt(cboScore.SelectedValue)
                            .Item("reviewer_remark") = txtRevewier_remark.Text
                            'Sandeep [ 02 Oct 2010 ] -- Start
                            .Item("analysis_date") = dtpAnalysisDate.Value
                            'Sandeep [ 02 Oct 2010 ] -- End 
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .Item("GUID") = Guid.NewGuid.ToString
                        End With
                        Call FillAnalysisTran()
                    End If
                End If
                Call ResetItems()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvAnalysis.SelectedItems.Count > 0 Then
                If intAnalysisSeletedIdx > -1 Then
                    Dim drTemp As DataRow()

                    If CInt(lvAnalysis.Items(intAnalysisSeletedIdx).Tag) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & lvAnalysis.Items(intAnalysisSeletedIdx).SubItems(objcolhGUID.Index).Text & "'")
                    Else
                        drTemp = mdtTran.Select("analysistranunkid = " & CInt(lvAnalysis.Items(intAnalysisSeletedIdx).Tag))
                    End If

                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        'S.SANDEEP [ 12 OCT 2011 ] -- START
                        drTemp(0).Item("voidreason") = Language.getMessage(mstrModuleName, 4, "Incorrect Analysis.")
                        drTemp(0).Item("isvoid") = True
                        drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                        drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [ 12 OCT 2011 ] -- END 
                        Call FillAnalysisTran()
                    End If
                End If
            End If
            Call ResetItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If lvAnalysis.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is nothing to save in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAnalysisMaster.Update(mdtTran)
            Else
                blnFlag = objAnalysisMaster.Insert(mdtTran)
            End If

            If blnFlag = False And objAnalysisMaster._Message <> "" Then
                eZeeMsgBox.Show(objAnalysisMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAnalysisMaster = Nothing
                    objAnalysisMaster = New clsTraining_Analysis_Master
                    Call GetValue()
                    txtEmployeeName.Focus()
                Else
                    mintAnalysisMasterUnkid = objAnalysisMaster._Analysisunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            With objFrm
                .ValueMember = cboOtherEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboOtherEmployee.DataSource, DataTable)
                .CodeMember = ""
            End With

            If objFrm.DisplayDialog Then
                cboOtherEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = ""
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmployee._Departmentunkid
                objlblDepartmentValue.Text = objDept._Name
                objlblEmployeeContactNo.Text = objEmployee._Present_Tel_No
                objlblCompanyValue.Text = Company._Object._Name
            Else
                objlblDepartmentValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                objlblCompanyValue.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub cboOtherEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOtherEmployee.SelectedIndexChanged
        Try
            If CInt(cboOtherEmployee.SelectedValue) > 0 Then
                objTrainers._TrainerUnkid = CInt(cboOtherEmployee.SelectedValue)
                objlblOtherCompany.Text = objTrainers._Other_Company
                objlblOtherDeptName.Text = objTrainers._Other_Department
                objlblOtherEmpContactNo.Text = objTrainers._Other_Contact
            Else
                objlblOtherCompany.Text = ""
                objlblOtherDeptName.Text = ""
                objlblOtherEmpContactNo.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub radEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEmployee.CheckedChanged, radOthers.CheckedChanged
        If radEmployee.Checked = True Then
            cboOtherEmployee.SelectedValue = 0
            pnlOtherInfo.Visible = False
            pnlEmployeeInfo.Visible = True
        ElseIf radOthers.Checked = True Then
            cboEmployee.SelectedValue = 0
            pnlOtherInfo.Visible = True
            pnlEmployeeInfo.Visible = False
        End If
    End Sub

    Private Sub lvAnalysis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvAnalysis.Click
        Try
            If lvAnalysis.SelectedItems.Count > 0 Then
                intAnalysisSeletedIdx = lvAnalysis.SelectedItems(0).Index
                objTrainers._TrainerUnkid = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhTrainerId.Index).Text)
                If objTrainers._EmployeeId <> -1 Then
                    radEmployee.Checked = True
                    cboEmployee.SelectedValue = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhTrainerId.Index).Text)
                Else
                    radOthers.Checked = True
                    cboOtherEmployee.SelectedValue = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhTrainerId.Index).Text)
                End If
                cboScore.SelectedValue = CInt(lvAnalysis.SelectedItems(0).SubItems(objcolhResultId.Index).Text)
                txtRevewier_remark.Text = lvAnalysis.SelectedItems(0).SubItems(colhRemark.Index).Text
                dtpAnalysisDate.Value = CDate(lvAnalysis.SelectedItems(0).SubItems(objcolhanalysisdate.Index).Text)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAnalysis_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private Sub objbtnAddResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResult.Click
        Dim frm As New frmResultCode_AddEdit
        Dim intRefId As Integer = -1
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objResult As New clsresult_master
                dsList = objResult.getComboList("Result", True, objScheduling._Resultgroupunkid)
                With cboScore
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Result")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objResult = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddResult_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbTrainingAnalysis.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTrainingAnalysis.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblAnalysisDate.Text = Language._Object.getCaption(Me.lblAnalysisDate.Name, Me.lblAnalysisDate.Text)
			Me.gbTrainingAnalysis.Text = Language._Object.getCaption(Me.gbTrainingAnalysis.Name, Me.gbTrainingAnalysis.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.lnTraineeInfo.Text = Language._Object.getCaption(Me.lnTraineeInfo.Name, Me.lnTraineeInfo.Text)
			Me.lnReviewerInfo.Text = Language._Object.getCaption(Me.lnReviewerInfo.Name, Me.lnReviewerInfo.Text)
			Me.lblTraineeRemark.Text = Language._Object.getCaption(Me.lblTraineeRemark.Name, Me.lblTraineeRemark.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblScore.Text = Language._Object.getCaption(Me.lblScore.Name, Me.lblScore.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.radOthers.Text = Language._Object.getCaption(Me.radOthers.Name, Me.radOthers.Text)
			Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
			Me.lblTrainerContactNo.Text = Language._Object.getCaption(Me.lblTrainerContactNo.Name, Me.lblTrainerContactNo.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblEmpContact.Text = Language._Object.getCaption(Me.lblEmpContact.Name, Me.lblEmpContact.Text)
			Me.lblEmpCompany.Text = Language._Object.getCaption(Me.lblEmpCompany.Name, Me.lblEmpCompany.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhPosition.Text = Language._Object.getCaption(CStr(Me.colhPosition.Tag), Me.colhPosition.Text)
			Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
			Me.colhContactNo.Text = Language._Object.getCaption(CStr(Me.colhContactNo.Tag), Me.colhContactNo.Text)
			Me.colhScore.Text = Language._Object.getCaption(CStr(Me.colhScore.Tag), Me.colhScore.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information, it can not be blank. Please select a Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Selected Employee is already added to the list.")
			Language.setMessage(mstrModuleName, 3, "There is nothing to save in the list.")
                        Language.setMessage(mstrModuleName, 4, "Incorrect Analysis.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class