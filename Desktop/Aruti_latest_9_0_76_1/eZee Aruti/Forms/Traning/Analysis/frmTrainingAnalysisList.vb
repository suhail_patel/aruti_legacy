﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmTrainingAnalysisList

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmTrainingAnalysisList"
    Private objAnalysis As clsTraining_Analysis_Master
    Private objAnalysisTran As New clsTraining_Analysis_Tran
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objCourse As New clsTraining_Scheduling
        Dim objTrainer As New clsTraining_Trainers_Tran
        Dim objResult As New clsresult_master
        Dim dsList As New DataSet
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With



            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objCourse.getComboList("Title", True)
            dsList = objCourse.getComboList("Title", True, , True)
            'S.SANDEEP [ 18 FEB 2012 ] -- END
            With cboCourse
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Title")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTrainer.getComboList(-1, -1, True, "Trainer", False, True)

            dsList = objTrainer.getComboList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, _
                                              -1, -1, True, "Trainer", False, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboTrainer
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("Trainer")
            End With

            dsList = objResult.getComboList("Result", True)
            With cboResult
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Result")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objCourse = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'dsList = objAnalysis.GetList("AnalysisData")

            'If CInt(cboCourse.SelectedValue) > 0 Then
            '    StrSearching &= "AND CourseId = " & CInt(cboCourse.SelectedValue) & " "
            'End If

            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
            'End If

            'If dtpDateFrom.Checked = True And dtpDateTo.Checked = True Then
            '    StrSearching &= "AND Date >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND Date <= '" & eZeeDate.convertDate(dtpDateTo.Value) & "'" & " "
            'End If

            'If CInt(cboTrainer.SelectedValue) > 0 Then
            '    StrSearching &= "AND trainerstranunkid = " & CInt(cboTrainer.SelectedValue) & " "
            'End If

            'If CInt(cboResult.SelectedValue) > 0 Then
            '    StrSearching &= "AND resultunkid = " & CInt(cboResult.SelectedValue) & " "
            'End If


            'If chkComplete.Checked = True And chkIncomplete.Checked = True Then
            '    StrSearching &= ""
            'ElseIf chkComplete.Checked = True Then
            '    StrSearching &= "AND iscomplete = 1 "
            'ElseIf chkIncomplete.Checked = True Then
            '    StrSearching &= "AND iscomplete = 0 "
            'End If


            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables("AnalysisData"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables("AnalysisData"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'End If


            If CInt(cboCourse.SelectedValue) > 0 Then
                StrSearching &= "AND ISNULL(hrtraining_scheduling.course_title,'0') = " & CInt(cboCourse.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If dtpDateFrom.Checked = True And dtpDateTo.Checked = True Then
                StrSearching &= "AND CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) <= '" & eZeeDate.convertDate(dtpDateTo.Value) & "'" & " "
            End If

            If CInt(cboTrainer.SelectedValue) > 0 Then
                StrSearching &= "AND hrtraining_analysis_tran.trainerstranunkid = " & CInt(cboTrainer.SelectedValue) & " "
            End If

            If CInt(cboResult.SelectedValue) > 0 Then
                StrSearching &= "AND hrtraining_analysis_tran.resultunkid = " & CInt(cboResult.SelectedValue) & " "
            End If


            If chkComplete.Checked = True And chkIncomplete.Checked = True Then
                StrSearching &= ""
            ElseIf chkComplete.Checked = True Then
                StrSearching &= "AND hrtraining_analysis_master.iscomplete = 1 "
            ElseIf chkIncomplete.Checked = True Then
                StrSearching &= "AND hrtraining_analysis_master.iscomplete = 0 "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dsList = objAnalysis.GetList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._IsIncludeInactiveEmp, "AnalysisData", StrSearching)

            dtTable = New DataView(dsList.Tables("AnalysisData"), "", "EmpName", DataViewRowState.CurrentRows).ToTable

            lvAnalysisList.BeginUpdate()
            'S.SANDEEP [04 JUN 2015] -- END

            lvAnalysisList.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                If CInt(dtRow.Item("StatusId")) = 1 Then
                    lvItem.ForeColor = Color.Blue
                End If

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(dtRow.Item("Course").ToString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("Trainer").ToString)
                lvItem.SubItems.Add(dtRow.Item("Result").ToString)
                If CBool(dtRow.Item("iscomplete")) = True Then
                    lvItem.SubItems.Add(chkComplete.Text)
                Else
                    lvItem.SubItems.Add(chkIncomplete.Text)
                End If
                lvItem.SubItems.Add(dtRow.Item("Remark").ToString)
                lvItem.SubItems.Add(dtRow.Item("trainingenrolltranunkid").ToString)
                lvItem.SubItems.Add(dtRow.Item("analysistranunkid").ToString)
                lvItem.Tag = dtRow.Item("analysisunkid")


                'Sandeep [ 16 Oct 2010 ] -- Start
                If CBool(dtRow.Item("iscomplete")) = True Then
                    If CInt(dtRow.Item("LevelId")) = CInt(dtTable.Compute("MAX(LevelId)", "CourseId =" & CInt(dtRow.Item("CourseId")))) Then
                        If CInt(dtRow.Item("analysistranunkid")) = CInt(dtTable.Compute("MAX(analysistranunkid)", "CourseId =" & CInt(dtRow.Item("CourseId")))) Then
                            lvItem.ForeColor = Color.Magenta
                        End If
                    End If
                End If

                'Sandeep [ 16 Oct 2010 ] -- End 
                lvAnalysisList.Items.Add(lvItem)

                lvItem = Nothing
            Next

            lvAnalysisList.GroupingColumn = colhEmployee
            lvAnalysisList.DisplayGroups(True)

            If lvAnalysisList.Items.Count > 5 Then
                colhRemark.Width = 180 - 20
            Else
                colhRemark.Width = 180
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            lvAnalysisList.EndUpdate()
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnFinalEvaluation.Enabled = User._Object.Privilege._AllowFinalAnalysis
            btnEdit.Enabled = User._Object.Privilege._EditTrainingAnalysis
            btnDelete.Enabled = User._Object.Privilege._DeleteTrainingAnalysis
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

#Region " Form's Events "
    Private Sub frmTrainingAnalysisList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvAnalysisList.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmTrainingAnalysisList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmTrainingAnalysisList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAnalysis = New clsTraining_Analysis_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()

            Call FillCombo()
            'Call FillList()

            If lvAnalysisList.Items.Count > 0 Then lvAnalysisList.Items(0).Selected = True
            lvAnalysisList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCourseSchedulingList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTraningRegistraionCancelList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAnalysis = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Analysis_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsTraining_Analysis_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Buttons "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
        Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAnalysisList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvAnalysisList.Select()
            Exit Sub
        End If
        'If objAnalysis.isUsed(CInt(lvAnalysisList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Training Schedule. Reason: This Training Schedule is in use."), enMsgBoxStyle.Information) '?2
        '    lvAnalysisList.Select()
        '    Exit Sub
        'End If


        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Void already voided transaction."), enMsgBoxStyle.Information) '?1
            lvAnalysisList.Select()
            Exit Sub
        End If

        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Purple Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Void cancelled transaction."), enMsgBoxStyle.Information) '?1
            lvAnalysisList.Select()
            Exit Sub
        End If

        'Sandeep [ 23 Oct 2010 ] -- Start
        'If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Then
        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Or lvAnalysisList.SelectedItems(0).ForeColor = Color.Magenta Then
            'Sandeep [ 23 Oct 2010 ] -- End 

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot Delete the completed Analysis."), enMsgBoxStyle.Information)
            lvAnalysisList.Select()
            Exit Sub
        End If

       
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAnalysisList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'objAnalysis._Isvoid = True
                'objAnalysis._Voidatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                'objAnalysis._Voidreason = "TESTING"
                'objAnalysis._Voiduserunkid = User._Object._Userunkid

                'objAnalysis.Delete(CInt(lvAnalysisList.SelectedItems(0).Tag))
                'lvAnalysisList.SelectedItems(0).Remove()

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objAnalysisTran.Void_Single_Analysis(CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhAnalysisTranId.Index).Text), _
                '                                                            True, _
                '                                                            CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt")), _
                '                                                            "TESTING", _
                '                                                            1)
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                frm = Nothing
                objAnalysisTran.Void_Single_Analysis(CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhAnalysisTranId.Index).Text), _
                                                                            True, _
                                                                            ConfigParameter._Object._CurrentDateAndTime, _
                                                                            mstrVoidReason, _
                                                                            User._Object._Userunkid)
                'Sandeep [ 16 Oct 2010 ] -- End 
                If lvAnalysisList.Items.Count <= 0 Then
                    Exit Try
                Else
                    Call FillList()
                End If

                'If lvAnalysisList.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvAnalysisList.Items.Count - 1
                '    lvAnalysisList.Items(intSelectedIndex).Selected = True
                '    lvAnalysisList.EnsureVisible(intSelectedIndex)
                'ElseIf lvAnalysisList.Items.Count <> 0 Then
                '    lvAnalysisList.Items(intSelectedIndex).Selected = True
                '    lvAnalysisList.EnsureVisible(intSelectedIndex)
                'End If
            End If
            lvAnalysisList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvAnalysisList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvAnalysisList.Select()
            Exit Sub
        End If

        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot edit Void transaction."), enMsgBoxStyle.Information) '?1
            lvAnalysisList.Select()
            Exit Sub
        End If

        'Sandeep [ 23 Oct 2010 ] -- Start
        'If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Then
        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Or lvAnalysisList.SelectedItems(0).ForeColor = Color.Magenta Then
            'Sandeep [ 23 Oct 2010 ] -- End 

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Edit the completed Analysis."), enMsgBoxStyle.Information)
            lvAnalysisList.Select()
            Exit Sub
        End If

        Dim frm As New frmTraningAnalysis_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAnalysisList.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvAnalysisList.SelectedItems(0).Tag), enAction.EDIT_ONE, -1) Then
                Call FillList()
            End If
            frm = Nothing

            If lvAnalysisList.Items.Count > 0 Then
                lvAnalysisList.Items(intSelectedIndex).Selected = True
                lvAnalysisList.EnsureVisible(intSelectedIndex)
                lvAnalysisList.Select()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
    '    Dim frm As New frmTraningAnalysis_AddEdit
    '    Try
    '        'If User._Object._RightToLeft = True Then
    '        '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '        '    frm.RightToLeftLayout = True
    '        '    Call Language.ctlRightToLeftlayOut(frm)
    '        'End If
    '        If frm.displayDialog(-1, enAction.ADD_ONE) Then
    '            Call FillList()
    '        End If
    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCourse.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            dtpDateFrom.Checked = False
            dtpDateTo.Checked = False
            chkIncomplete.Checked = False
            chkComplete.Checked = False
            cboResult.SelectedValue = 0
            cboTrainer.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub btnFinalEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalEvaluation.Click

        If lvAnalysisList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information)
            lvAnalysisList.Select()
            Exit Sub
        End If

        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Void already voided transaction."), enMsgBoxStyle.Information) '?1
            lvAnalysisList.Select()
            Exit Sub
        End If

        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Purple Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Void cancelled transaction."), enMsgBoxStyle.Information) '?1
            lvAnalysisList.Select()
            Exit Sub
        End If

        'Sandeep [ 23 Oct 2010 ] -- Start
        'If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Then
        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Or lvAnalysisList.SelectedItems(0).ForeColor = Color.Magenta Then
            'Sandeep [ 23 Oct 2010 ] -- End 
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You cannot Analyze the completed Analysis."), enMsgBoxStyle.Information)
            lvAnalysisList.Select()
            Exit Sub
        End If

        Try
            Dim frm As New frmFinalEvaluation
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            frm.displayDialog(CInt(lvAnalysisList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhEnrollId.Index).Text))
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFinalEvaluation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResult.Click
        Dim objFrm As New frmCommonSearch
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            With objFrm
                .ValueMember = cboResult.ValueMember
                .DisplayMember = cboResult.DisplayMember
                .DataSource = CType(cboResult.DataSource, DataTable)
                .CodeMember = "Code"
            End With

            If objFrm.DisplayDialog Then
                cboResult.SelectedValue = objFrm.SelectedValue
                cboResult.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResult_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTrainer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrainer.Click
        Dim objFrm As New frmCommonSearch
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            With objFrm
                .ValueMember = cboTrainer.ValueMember
                .DisplayMember = cboTrainer.DisplayMember
                .DataSource = CType(cboTrainer.DataSource, DataTable)
                .CodeMember = ""
            End With

            If objFrm.DisplayDialog Then
                cboTrainer.SelectedValue = objFrm.SelectedValue
                cboTrainer.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrainer_Click", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnFinalEvaluation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFinalEvaluation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
			Me.lblCourseDateTo.Text = Language._Object.getCaption(Me.lblCourseDateTo.Name, Me.lblCourseDateTo.Text)
			Me.lblAnalysisDateFrom.Text = Language._Object.getCaption(Me.lblAnalysisDateFrom.Name, Me.lblAnalysisDateFrom.Text)
			Me.btnFinalEvaluation.Text = Language._Object.getCaption(Me.btnFinalEvaluation.Name, Me.btnFinalEvaluation.Text)
			Me.chkIncomplete.Text = Language._Object.getCaption(Me.chkIncomplete.Name, Me.chkIncomplete.Text)
			Me.chkComplete.Text = Language._Object.getCaption(Me.chkComplete.Name, Me.chkComplete.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhCourse.Text = Language._Object.getCaption(CStr(Me.colhCourse.Tag), Me.colhCourse.Text)
			Me.colhAnalysisDate.Text = Language._Object.getCaption(CStr(Me.colhAnalysisDate.Tag), Me.colhAnalysisDate.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.colhTrainer.Text = Language._Object.getCaption(CStr(Me.colhTrainer.Tag), Me.colhTrainer.Text)
			Me.colhResult.Text = Language._Object.getCaption(CStr(Me.colhResult.Tag), Me.colhResult.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
			Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
			Me.lblTrainer.Text = Language._Object.getCaption(Me.lblTrainer.Name, Me.lblTrainer.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot edit Void transaction.")
			Language.setMessage(mstrModuleName, 4, "Sorry, You cannot Void already voided transaction.")
			Language.setMessage(mstrModuleName, 5, "Sorry, You cannot Void cancelled transaction.")
			Language.setMessage(mstrModuleName, 6, "Sorry, You cannot Delete the completed Analysis.")
			Language.setMessage(mstrModuleName, 7, "Sorry, You cannot Edit the completed Analysis.")
			Language.setMessage(mstrModuleName, 8, "Sorry, You cannot Analyze the completed Analysis.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class