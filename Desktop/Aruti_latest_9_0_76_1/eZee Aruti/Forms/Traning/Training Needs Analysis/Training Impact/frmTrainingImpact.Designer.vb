﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrainingImpact
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTrainingImpact))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objBtnLineManager = New eZee.Common.eZeeGradientButton
        Me.cboLineManager = New System.Windows.Forms.ComboBox
        Me.lblLineManager = New System.Windows.Forms.Label
        Me.txtEnddate = New eZee.TextBox.AlphanumericTextBox
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.txtStartDate = New eZee.TextBox.AlphanumericTextBox
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.objBtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCourse = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboCourse = New System.Windows.Forms.ComboBox
        Me.lblCourse = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnFinalSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.tbImpactEvaluation = New System.Windows.Forms.TabControl
        Me.tbppartA = New System.Windows.Forms.TabPage
        Me.pnlPartA = New System.Windows.Forms.Panel
        Me.lvItemBKPIList = New eZee.Common.eZeeListView(Me.components)
        Me.colhPartAItemBKPI = New System.Windows.Forms.ColumnHeader
        Me.objcolhItemBKPIGUID = New System.Windows.Forms.ColumnHeader
        Me.btnEditItemAKPI = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDeleteItemAKPI = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddItemAKPI = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvItemAJobCapability = New eZee.Common.eZeeListView(Me.components)
        Me.colhItemAJobCapability = New System.Windows.Forms.ColumnHeader
        Me.objcolhItemAJobCapGUID = New System.Windows.Forms.ColumnHeader
        Me.btnEditItemB = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEditItemAGAP = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDeleteItemB = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddItemB = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvItemBList = New eZee.Common.eZeeListView(Me.components)
        Me.colhItemBTrainingObjective = New System.Windows.Forms.ColumnHeader
        Me.objColhItemBGUID = New System.Windows.Forms.ColumnHeader
        Me.txtItemBTrainingObjective = New System.Windows.Forms.TextBox
        Me.lblItemBTrainingObjective = New System.Windows.Forms.Label
        Me.txtItemBKPI = New System.Windows.Forms.TextBox
        Me.lblItemBKPI = New System.Windows.Forms.Label
        Me.LblItemBCaption = New System.Windows.Forms.Label
        Me.LblItemB = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.btnDeleteItemAGAP = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddItemAGAP = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvItemAKPIList = New eZee.Common.eZeeListView(Me.components)
        Me.colhItemAKPI = New System.Windows.Forms.ColumnHeader
        Me.objcolhItemAKPIGUID = New System.Windows.Forms.ColumnHeader
        Me.txtItemAJobCapability = New System.Windows.Forms.TextBox
        Me.lblItemAJobCapability = New System.Windows.Forms.Label
        Me.txtItemAKPI = New System.Windows.Forms.TextBox
        Me.lblItemAKPI = New System.Windows.Forms.Label
        Me.LblItemACaption = New System.Windows.Forms.Label
        Me.LblItemA = New System.Windows.Forms.Label
        Me.tbppartB = New System.Windows.Forms.TabPage
        Me.pnlPartB = New System.Windows.Forms.Panel
        Me.lvPartBKRA = New eZee.Common.eZeeListView(Me.components)
        Me.colhPartBKRA = New System.Windows.Forms.ColumnHeader
        Me.txtPartBNameTask = New System.Windows.Forms.TextBox
        Me.lblNametask = New System.Windows.Forms.Label
        Me.lblPartBJobCapabilities = New System.Windows.Forms.Label
        Me.lblPartBDesc = New System.Windows.Forms.Label
        Me.tbppartC = New System.Windows.Forms.TabPage
        Me.pnlPactC = New System.Windows.Forms.Panel
        Me.txtFeedbackComment = New System.Windows.Forms.TextBox
        Me.lvFeedBackList = New eZee.Common.eZeeListView(Me.components)
        Me.colhQuestion = New System.Windows.Forms.ColumnHeader
        Me.colhAnswer = New System.Windows.Forms.ColumnHeader
        Me.colhFeedBackComment = New System.Windows.Forms.ColumnHeader
        Me.objColhQuestionunkid = New System.Windows.Forms.ColumnHeader
        Me.objColhAnswerunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhPartCGUID = New System.Windows.Forms.ColumnHeader
        Me.btnEditFeedBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblComments = New System.Windows.Forms.Label
        Me.lblPartCDesc = New System.Windows.Forms.Label
        Me.btnAddFeedback = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblQuestion = New System.Windows.Forms.Label
        Me.objBtnSearchAnswer = New eZee.Common.eZeeGradientButton
        Me.btnDeleteFeedBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboQuestion = New System.Windows.Forms.ComboBox
        Me.cboAnswer = New System.Windows.Forms.ComboBox
        Me.objBtnSearchQuestion = New eZee.Common.eZeeGradientButton
        Me.lblAnswer = New System.Windows.Forms.Label
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.tbImpactEvaluation.SuspendLayout()
        Me.tbppartA.SuspendLayout()
        Me.pnlPartA.SuspendLayout()
        Me.tbppartB.SuspendLayout()
        Me.pnlPartB.SuspendLayout()
        Me.tbppartC.SuspendLayout()
        Me.pnlPactC.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objBtnLineManager)
        Me.pnlMain.Controls.Add(Me.cboLineManager)
        Me.pnlMain.Controls.Add(Me.lblLineManager)
        Me.pnlMain.Controls.Add(Me.txtEnddate)
        Me.pnlMain.Controls.Add(Me.lblEndDate)
        Me.pnlMain.Controls.Add(Me.txtStartDate)
        Me.pnlMain.Controls.Add(Me.lblStartDate)
        Me.pnlMain.Controls.Add(Me.objBtnSearchEmployee)
        Me.pnlMain.Controls.Add(Me.objbtnSearchCourse)
        Me.pnlMain.Controls.Add(Me.cboEmployee)
        Me.pnlMain.Controls.Add(Me.lblEmployee)
        Me.pnlMain.Controls.Add(Me.cboCourse)
        Me.pnlMain.Controls.Add(Me.lblCourse)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Controls.Add(Me.tbImpactEvaluation)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(772, 516)
        Me.pnlMain.TabIndex = 0
        '
        'objBtnLineManager
        '
        Me.objBtnLineManager.BackColor = System.Drawing.Color.Transparent
        Me.objBtnLineManager.BackColor1 = System.Drawing.Color.Transparent
        Me.objBtnLineManager.BackColor2 = System.Drawing.Color.Transparent
        Me.objBtnLineManager.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objBtnLineManager.BorderSelected = False
        Me.objBtnLineManager.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objBtnLineManager.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objBtnLineManager.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objBtnLineManager.Location = New System.Drawing.Point(739, 45)
        Me.objBtnLineManager.Name = "objBtnLineManager"
        Me.objBtnLineManager.Size = New System.Drawing.Size(21, 21)
        Me.objBtnLineManager.TabIndex = 261
        '
        'cboLineManager
        '
        Me.cboLineManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLineManager.DropDownWidth = 350
        Me.cboLineManager.FormattingEnabled = True
        Me.cboLineManager.Location = New System.Drawing.Point(437, 45)
        Me.cboLineManager.Name = "cboLineManager"
        Me.cboLineManager.Size = New System.Drawing.Size(296, 21)
        Me.cboLineManager.TabIndex = 260
        '
        'lblLineManager
        '
        Me.lblLineManager.Location = New System.Drawing.Point(293, 47)
        Me.lblLineManager.Name = "lblLineManager"
        Me.lblLineManager.Size = New System.Drawing.Size(137, 16)
        Me.lblLineManager.TabIndex = 259
        Me.lblLineManager.Text = "Line Manager/Supervisor"
        '
        'txtEnddate
        '
        Me.txtEnddate.BackColor = System.Drawing.Color.White
        Me.txtEnddate.Flags = 0
        Me.txtEnddate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEnddate.Location = New System.Drawing.Point(80, 70)
        Me.txtEnddate.Name = "txtEnddate"
        Me.txtEnddate.ReadOnly = True
        Me.txtEnddate.Size = New System.Drawing.Size(129, 21)
        Me.txtEnddate.TabIndex = 258
        '
        'lblEndDate
        '
        Me.lblEndDate.Location = New System.Drawing.Point(11, 73)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(62, 16)
        Me.lblEndDate.TabIndex = 257
        Me.lblEndDate.Text = "End Date"
        '
        'txtStartDate
        '
        Me.txtStartDate.BackColor = System.Drawing.Color.White
        Me.txtStartDate.Flags = 0
        Me.txtStartDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtStartDate.Location = New System.Drawing.Point(80, 42)
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.ReadOnly = True
        Me.txtStartDate.Size = New System.Drawing.Size(129, 21)
        Me.txtStartDate.TabIndex = 256
        '
        'lblStartDate
        '
        Me.lblStartDate.Location = New System.Drawing.Point(11, 45)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(62, 16)
        Me.lblStartDate.TabIndex = 255
        Me.lblStartDate.Text = "Start Date"
        '
        'objBtnSearchEmployee
        '
        Me.objBtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objBtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objBtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objBtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objBtnSearchEmployee.BorderSelected = False
        Me.objBtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objBtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objBtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objBtnSearchEmployee.Location = New System.Drawing.Point(739, 15)
        Me.objBtnSearchEmployee.Name = "objBtnSearchEmployee"
        Me.objBtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objBtnSearchEmployee.TabIndex = 254
        '
        'objbtnSearchCourse
        '
        Me.objbtnSearchCourse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCourse.BorderSelected = False
        Me.objbtnSearchCourse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCourse.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCourse.Location = New System.Drawing.Point(341, 15)
        Me.objbtnSearchCourse.Name = "objbtnSearchCourse"
        Me.objbtnSearchCourse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCourse.TabIndex = 253
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(437, 15)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(296, 21)
        Me.cboEmployee.TabIndex = 5
        '
        'lblEmployee
        '
        Me.lblEmployee.Location = New System.Drawing.Point(368, 17)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(62, 16)
        Me.lblEmployee.TabIndex = 4
        Me.lblEmployee.Text = "Employee"
        '
        'cboCourse
        '
        Me.cboCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourse.DropDownWidth = 450
        Me.cboCourse.FormattingEnabled = True
        Me.cboCourse.Location = New System.Drawing.Point(80, 15)
        Me.cboCourse.Name = "cboCourse"
        Me.cboCourse.Size = New System.Drawing.Size(255, 21)
        Me.cboCourse.TabIndex = 3
        '
        'lblCourse
        '
        Me.lblCourse.Location = New System.Drawing.Point(11, 17)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(62, 16)
        Me.lblCourse.TabIndex = 2
        Me.lblCourse.Text = "Course"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnFinalSave)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 461)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(772, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnFinalSave
        '
        Me.btnFinalSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinalSave.BackColor = System.Drawing.Color.White
        Me.btnFinalSave.BackgroundImage = CType(resources.GetObject("btnFinalSave.BackgroundImage"), System.Drawing.Image)
        Me.btnFinalSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinalSave.BorderColor = System.Drawing.Color.Empty
        Me.btnFinalSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinalSave.FlatAppearance.BorderSize = 0
        Me.btnFinalSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinalSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalSave.ForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinalSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Location = New System.Drawing.Point(478, 12)
        Me.btnFinalSave.Name = "btnFinalSave"
        Me.btnFinalSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Size = New System.Drawing.Size(90, 30)
        Me.btnFinalSave.TabIndex = 2
        Me.btnFinalSave.Text = "&Final Save"
        Me.btnFinalSave.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(574, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(670, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'tbImpactEvaluation
        '
        Me.tbImpactEvaluation.Controls.Add(Me.tbppartA)
        Me.tbImpactEvaluation.Controls.Add(Me.tbppartB)
        Me.tbImpactEvaluation.Controls.Add(Me.tbppartC)
        Me.tbImpactEvaluation.Location = New System.Drawing.Point(4, 97)
        Me.tbImpactEvaluation.Name = "tbImpactEvaluation"
        Me.tbImpactEvaluation.SelectedIndex = 0
        Me.tbImpactEvaluation.Size = New System.Drawing.Size(768, 352)
        Me.tbImpactEvaluation.TabIndex = 0
        '
        'tbppartA
        '
        Me.tbppartA.Controls.Add(Me.pnlPartA)
        Me.tbppartA.Location = New System.Drawing.Point(4, 22)
        Me.tbppartA.Name = "tbppartA"
        Me.tbppartA.Padding = New System.Windows.Forms.Padding(3)
        Me.tbppartA.Size = New System.Drawing.Size(760, 326)
        Me.tbppartA.TabIndex = 0
        Me.tbppartA.Text = "Part A"
        Me.tbppartA.UseVisualStyleBackColor = True
        '
        'pnlPartA
        '
        Me.pnlPartA.AutoScroll = True
        Me.pnlPartA.Controls.Add(Me.lvItemBKPIList)
        Me.pnlPartA.Controls.Add(Me.btnEditItemAKPI)
        Me.pnlPartA.Controls.Add(Me.btnDeleteItemAKPI)
        Me.pnlPartA.Controls.Add(Me.btnAddItemAKPI)
        Me.pnlPartA.Controls.Add(Me.lvItemAJobCapability)
        Me.pnlPartA.Controls.Add(Me.btnEditItemB)
        Me.pnlPartA.Controls.Add(Me.btnEditItemAGAP)
        Me.pnlPartA.Controls.Add(Me.btnDeleteItemB)
        Me.pnlPartA.Controls.Add(Me.btnAddItemB)
        Me.pnlPartA.Controls.Add(Me.lvItemBList)
        Me.pnlPartA.Controls.Add(Me.txtItemBTrainingObjective)
        Me.pnlPartA.Controls.Add(Me.lblItemBTrainingObjective)
        Me.pnlPartA.Controls.Add(Me.txtItemBKPI)
        Me.pnlPartA.Controls.Add(Me.lblItemBKPI)
        Me.pnlPartA.Controls.Add(Me.LblItemBCaption)
        Me.pnlPartA.Controls.Add(Me.LblItemB)
        Me.pnlPartA.Controls.Add(Me.objLine1)
        Me.pnlPartA.Controls.Add(Me.btnDeleteItemAGAP)
        Me.pnlPartA.Controls.Add(Me.btnAddItemAGAP)
        Me.pnlPartA.Controls.Add(Me.lvItemAKPIList)
        Me.pnlPartA.Controls.Add(Me.txtItemAJobCapability)
        Me.pnlPartA.Controls.Add(Me.lblItemAJobCapability)
        Me.pnlPartA.Controls.Add(Me.txtItemAKPI)
        Me.pnlPartA.Controls.Add(Me.lblItemAKPI)
        Me.pnlPartA.Controls.Add(Me.LblItemACaption)
        Me.pnlPartA.Controls.Add(Me.LblItemA)
        Me.pnlPartA.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPartA.Location = New System.Drawing.Point(3, 3)
        Me.pnlPartA.Name = "pnlPartA"
        Me.pnlPartA.Size = New System.Drawing.Size(754, 320)
        Me.pnlPartA.TabIndex = 0
        '
        'lvItemBKPIList
        '
        Me.lvItemBKPIList.BackColorOnChecked = True
        Me.lvItemBKPIList.ColumnHeaders = Nothing
        Me.lvItemBKPIList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPartAItemBKPI, Me.objcolhItemBKPIGUID})
        Me.lvItemBKPIList.CompulsoryColumns = ""
        Me.lvItemBKPIList.FullRowSelect = True
        Me.lvItemBKPIList.GridLines = True
        Me.lvItemBKPIList.GroupingColumn = Nothing
        Me.lvItemBKPIList.HideSelection = False
        Me.lvItemBKPIList.Location = New System.Drawing.Point(23, 457)
        Me.lvItemBKPIList.MinColumnWidth = 50
        Me.lvItemBKPIList.MultiSelect = False
        Me.lvItemBKPIList.Name = "lvItemBKPIList"
        Me.lvItemBKPIList.OptionalColumns = ""
        Me.lvItemBKPIList.ShowMoreItem = False
        Me.lvItemBKPIList.ShowSaveItem = False
        Me.lvItemBKPIList.ShowSelectAll = True
        Me.lvItemBKPIList.ShowSizeAllColumnsToFit = True
        Me.lvItemBKPIList.Size = New System.Drawing.Size(348, 124)
        Me.lvItemBKPIList.Sortable = True
        Me.lvItemBKPIList.TabIndex = 262
        Me.lvItemBKPIList.UseCompatibleStateImageBehavior = False
        Me.lvItemBKPIList.View = System.Windows.Forms.View.Details
        '
        'colhPartAItemBKPI
        '
        Me.colhPartAItemBKPI.Tag = "colhPartAItemBKPI"
        Me.colhPartAItemBKPI.Text = ""
        Me.colhPartAItemBKPI.Width = 342
        '
        'objcolhItemBKPIGUID
        '
        Me.objcolhItemBKPIGUID.Tag = "objcolhItemBKPIGUID"
        Me.objcolhItemBKPIGUID.Text = "objcolhItemBKPIGUID"
        Me.objcolhItemBKPIGUID.Width = 0
        '
        'btnEditItemAKPI
        '
        Me.btnEditItemAKPI.BackColor = System.Drawing.Color.White
        Me.btnEditItemAKPI.BackgroundImage = CType(resources.GetObject("btnEditItemAKPI.BackgroundImage"), System.Drawing.Image)
        Me.btnEditItemAKPI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditItemAKPI.BorderColor = System.Drawing.Color.Empty
        Me.btnEditItemAKPI.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditItemAKPI.FlatAppearance.BorderSize = 0
        Me.btnEditItemAKPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditItemAKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditItemAKPI.ForeColor = System.Drawing.Color.Black
        Me.btnEditItemAKPI.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditItemAKPI.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemAKPI.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditItemAKPI.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemAKPI.Location = New System.Drawing.Point(186, 269)
        Me.btnEditItemAKPI.Name = "btnEditItemAKPI"
        Me.btnEditItemAKPI.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditItemAKPI.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemAKPI.Size = New System.Drawing.Size(90, 30)
        Me.btnEditItemAKPI.TabIndex = 261
        Me.btnEditItemAKPI.Text = "Edit"
        Me.btnEditItemAKPI.UseVisualStyleBackColor = True
        '
        'btnDeleteItemAKPI
        '
        Me.btnDeleteItemAKPI.BackColor = System.Drawing.Color.White
        Me.btnDeleteItemAKPI.BackgroundImage = CType(resources.GetObject("btnDeleteItemAKPI.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteItemAKPI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteItemAKPI.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteItemAKPI.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteItemAKPI.FlatAppearance.BorderSize = 0
        Me.btnDeleteItemAKPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteItemAKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteItemAKPI.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemAKPI.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteItemAKPI.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemAKPI.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteItemAKPI.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemAKPI.Location = New System.Drawing.Point(282, 269)
        Me.btnDeleteItemAKPI.Name = "btnDeleteItemAKPI"
        Me.btnDeleteItemAKPI.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteItemAKPI.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemAKPI.Size = New System.Drawing.Size(90, 30)
        Me.btnDeleteItemAKPI.TabIndex = 260
        Me.btnDeleteItemAKPI.Text = "Delete"
        Me.btnDeleteItemAKPI.UseVisualStyleBackColor = True
        '
        'btnAddItemAKPI
        '
        Me.btnAddItemAKPI.BackColor = System.Drawing.Color.White
        Me.btnAddItemAKPI.BackgroundImage = CType(resources.GetObject("btnAddItemAKPI.BackgroundImage"), System.Drawing.Image)
        Me.btnAddItemAKPI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddItemAKPI.BorderColor = System.Drawing.Color.Empty
        Me.btnAddItemAKPI.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddItemAKPI.FlatAppearance.BorderSize = 0
        Me.btnAddItemAKPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddItemAKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddItemAKPI.ForeColor = System.Drawing.Color.Black
        Me.btnAddItemAKPI.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddItemAKPI.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemAKPI.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddItemAKPI.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemAKPI.Location = New System.Drawing.Point(90, 269)
        Me.btnAddItemAKPI.Name = "btnAddItemAKPI"
        Me.btnAddItemAKPI.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddItemAKPI.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemAKPI.Size = New System.Drawing.Size(90, 30)
        Me.btnAddItemAKPI.TabIndex = 259
        Me.btnAddItemAKPI.Text = "Add"
        Me.btnAddItemAKPI.UseVisualStyleBackColor = True
        '
        'lvItemAJobCapability
        '
        Me.lvItemAJobCapability.BackColorOnChecked = True
        Me.lvItemAJobCapability.ColumnHeaders = Nothing
        Me.lvItemAJobCapability.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhItemAJobCapability, Me.objcolhItemAJobCapGUID})
        Me.lvItemAJobCapability.CompulsoryColumns = ""
        Me.lvItemAJobCapability.FullRowSelect = True
        Me.lvItemAJobCapability.GridLines = True
        Me.lvItemAJobCapability.GroupingColumn = Nothing
        Me.lvItemAJobCapability.HideSelection = False
        Me.lvItemAJobCapability.Location = New System.Drawing.Point(381, 139)
        Me.lvItemAJobCapability.MinColumnWidth = 50
        Me.lvItemAJobCapability.MultiSelect = False
        Me.lvItemAJobCapability.Name = "lvItemAJobCapability"
        Me.lvItemAJobCapability.OptionalColumns = ""
        Me.lvItemAJobCapability.ShowMoreItem = False
        Me.lvItemAJobCapability.ShowSaveItem = False
        Me.lvItemAJobCapability.ShowSelectAll = True
        Me.lvItemAJobCapability.ShowSizeAllColumnsToFit = True
        Me.lvItemAJobCapability.Size = New System.Drawing.Size(348, 124)
        Me.lvItemAJobCapability.Sortable = True
        Me.lvItemAJobCapability.TabIndex = 258
        Me.lvItemAJobCapability.UseCompatibleStateImageBehavior = False
        Me.lvItemAJobCapability.View = System.Windows.Forms.View.Details
        '
        'colhItemAJobCapability
        '
        Me.colhItemAJobCapability.Tag = "colhItemAJobCapability"
        Me.colhItemAJobCapability.Text = ""
        Me.colhItemAJobCapability.Width = 375
        '
        'objcolhItemAJobCapGUID
        '
        Me.objcolhItemAJobCapGUID.Tag = "objcolhItemAJobCapGUID"
        Me.objcolhItemAJobCapGUID.Text = "objcolhItemAJobCapGUID"
        Me.objcolhItemAJobCapGUID.Width = 0
        '
        'btnEditItemB
        '
        Me.btnEditItemB.BackColor = System.Drawing.Color.White
        Me.btnEditItemB.BackgroundImage = CType(resources.GetObject("btnEditItemB.BackgroundImage"), System.Drawing.Image)
        Me.btnEditItemB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditItemB.BorderColor = System.Drawing.Color.Empty
        Me.btnEditItemB.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditItemB.FlatAppearance.BorderSize = 0
        Me.btnEditItemB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditItemB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditItemB.ForeColor = System.Drawing.Color.Black
        Me.btnEditItemB.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditItemB.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemB.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditItemB.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemB.Location = New System.Drawing.Point(543, 586)
        Me.btnEditItemB.Name = "btnEditItemB"
        Me.btnEditItemB.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditItemB.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemB.Size = New System.Drawing.Size(90, 30)
        Me.btnEditItemB.TabIndex = 257
        Me.btnEditItemB.Text = "Edit"
        Me.btnEditItemB.UseVisualStyleBackColor = True
        '
        'btnEditItemAGAP
        '
        Me.btnEditItemAGAP.BackColor = System.Drawing.Color.White
        Me.btnEditItemAGAP.BackgroundImage = CType(resources.GetObject("btnEditItemAGAP.BackgroundImage"), System.Drawing.Image)
        Me.btnEditItemAGAP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditItemAGAP.BorderColor = System.Drawing.Color.Empty
        Me.btnEditItemAGAP.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditItemAGAP.FlatAppearance.BorderSize = 0
        Me.btnEditItemAGAP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditItemAGAP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditItemAGAP.ForeColor = System.Drawing.Color.Black
        Me.btnEditItemAGAP.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditItemAGAP.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemAGAP.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditItemAGAP.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemAGAP.Location = New System.Drawing.Point(543, 269)
        Me.btnEditItemAGAP.Name = "btnEditItemAGAP"
        Me.btnEditItemAGAP.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditItemAGAP.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditItemAGAP.Size = New System.Drawing.Size(90, 30)
        Me.btnEditItemAGAP.TabIndex = 256
        Me.btnEditItemAGAP.Text = "Edit"
        Me.btnEditItemAGAP.UseVisualStyleBackColor = True
        '
        'btnDeleteItemB
        '
        Me.btnDeleteItemB.BackColor = System.Drawing.Color.White
        Me.btnDeleteItemB.BackgroundImage = CType(resources.GetObject("btnDeleteItemB.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteItemB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteItemB.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteItemB.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteItemB.FlatAppearance.BorderSize = 0
        Me.btnDeleteItemB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteItemB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteItemB.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemB.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteItemB.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemB.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteItemB.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemB.Location = New System.Drawing.Point(639, 586)
        Me.btnDeleteItemB.Name = "btnDeleteItemB"
        Me.btnDeleteItemB.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteItemB.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemB.Size = New System.Drawing.Size(90, 30)
        Me.btnDeleteItemB.TabIndex = 255
        Me.btnDeleteItemB.Text = "Delete"
        Me.btnDeleteItemB.UseVisualStyleBackColor = True
        '
        'btnAddItemB
        '
        Me.btnAddItemB.BackColor = System.Drawing.Color.White
        Me.btnAddItemB.BackgroundImage = CType(resources.GetObject("btnAddItemB.BackgroundImage"), System.Drawing.Image)
        Me.btnAddItemB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddItemB.BorderColor = System.Drawing.Color.Empty
        Me.btnAddItemB.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddItemB.FlatAppearance.BorderSize = 0
        Me.btnAddItemB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddItemB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddItemB.ForeColor = System.Drawing.Color.Black
        Me.btnAddItemB.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddItemB.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemB.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddItemB.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemB.Location = New System.Drawing.Point(447, 586)
        Me.btnAddItemB.Name = "btnAddItemB"
        Me.btnAddItemB.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddItemB.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemB.Size = New System.Drawing.Size(90, 30)
        Me.btnAddItemB.TabIndex = 253
        Me.btnAddItemB.Text = "Add"
        Me.btnAddItemB.UseVisualStyleBackColor = True
        '
        'lvItemBList
        '
        Me.lvItemBList.BackColorOnChecked = True
        Me.lvItemBList.ColumnHeaders = Nothing
        Me.lvItemBList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhItemBTrainingObjective, Me.objColhItemBGUID})
        Me.lvItemBList.CompulsoryColumns = ""
        Me.lvItemBList.FullRowSelect = True
        Me.lvItemBList.GridLines = True
        Me.lvItemBList.GroupingColumn = Nothing
        Me.lvItemBList.HideSelection = False
        Me.lvItemBList.Location = New System.Drawing.Point(381, 457)
        Me.lvItemBList.MinColumnWidth = 50
        Me.lvItemBList.MultiSelect = False
        Me.lvItemBList.Name = "lvItemBList"
        Me.lvItemBList.OptionalColumns = ""
        Me.lvItemBList.ShowMoreItem = False
        Me.lvItemBList.ShowSaveItem = False
        Me.lvItemBList.ShowSelectAll = True
        Me.lvItemBList.ShowSizeAllColumnsToFit = True
        Me.lvItemBList.Size = New System.Drawing.Size(348, 124)
        Me.lvItemBList.Sortable = True
        Me.lvItemBList.TabIndex = 254
        Me.lvItemBList.UseCompatibleStateImageBehavior = False
        Me.lvItemBList.View = System.Windows.Forms.View.Details
        '
        'colhItemBTrainingObjective
        '
        Me.colhItemBTrainingObjective.Tag = "colhItemBTrainingObjective"
        Me.colhItemBTrainingObjective.Text = "Training Objective"
        Me.colhItemBTrainingObjective.Width = 370
        '
        'objColhItemBGUID
        '
        Me.objColhItemBGUID.Tag = "objColhItemBGUID"
        Me.objColhItemBGUID.Text = "objColhItemBGUID"
        Me.objColhItemBGUID.Width = 0
        '
        'txtItemBTrainingObjective
        '
        Me.txtItemBTrainingObjective.Location = New System.Drawing.Point(22, 411)
        Me.txtItemBTrainingObjective.Multiline = True
        Me.txtItemBTrainingObjective.Name = "txtItemBTrainingObjective"
        Me.txtItemBTrainingObjective.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtItemBTrainingObjective.Size = New System.Drawing.Size(707, 39)
        Me.txtItemBTrainingObjective.TabIndex = 252
        '
        'lblItemBTrainingObjective
        '
        Me.lblItemBTrainingObjective.Location = New System.Drawing.Point(20, 382)
        Me.lblItemBTrainingObjective.Name = "lblItemBTrainingObjective"
        Me.lblItemBTrainingObjective.Size = New System.Drawing.Size(706, 27)
        Me.lblItemBTrainingObjective.TabIndex = 251
        Me.lblItemBTrainingObjective.Text = "Training Objective"
        '
        'txtItemBKPI
        '
        Me.txtItemBKPI.Location = New System.Drawing.Point(23, 411)
        Me.txtItemBKPI.Multiline = True
        Me.txtItemBKPI.Name = "txtItemBKPI"
        Me.txtItemBKPI.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtItemBKPI.Size = New System.Drawing.Size(350, 39)
        Me.txtItemBKPI.TabIndex = 250
        Me.txtItemBKPI.Visible = False
        '
        'lblItemBKPI
        '
        Me.lblItemBKPI.Location = New System.Drawing.Point(23, 381)
        Me.lblItemBKPI.Name = "lblItemBKPI"
        Me.lblItemBKPI.Size = New System.Drawing.Size(313, 28)
        Me.lblItemBKPI.TabIndex = 249
        Me.lblItemBKPI.Text = "KPI / KRA"
        Me.lblItemBKPI.Visible = False
        '
        'LblItemBCaption
        '
        Me.LblItemBCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblItemBCaption.Location = New System.Drawing.Point(19, 332)
        Me.LblItemBCaption.Name = "LblItemBCaption"
        Me.LblItemBCaption.Size = New System.Drawing.Size(709, 45)
        Me.LblItemBCaption.TabIndex = 248
        Me.LblItemBCaption.Text = resources.GetString("LblItemBCaption.Text")
        '
        'LblItemB
        '
        Me.LblItemB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblItemB.Location = New System.Drawing.Point(7, 313)
        Me.LblItemB.Name = "LblItemB"
        Me.LblItemB.Size = New System.Drawing.Size(88, 15)
        Me.LblItemB.TabIndex = 247
        Me.LblItemB.Text = "Item 1 (b) :"
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(7, 304)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(724, 4)
        Me.objLine1.TabIndex = 246
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDeleteItemAGAP
        '
        Me.btnDeleteItemAGAP.BackColor = System.Drawing.Color.White
        Me.btnDeleteItemAGAP.BackgroundImage = CType(resources.GetObject("btnDeleteItemAGAP.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteItemAGAP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteItemAGAP.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteItemAGAP.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteItemAGAP.FlatAppearance.BorderSize = 0
        Me.btnDeleteItemAGAP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteItemAGAP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteItemAGAP.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemAGAP.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteItemAGAP.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemAGAP.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteItemAGAP.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemAGAP.Location = New System.Drawing.Point(639, 269)
        Me.btnDeleteItemAGAP.Name = "btnDeleteItemAGAP"
        Me.btnDeleteItemAGAP.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteItemAGAP.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteItemAGAP.Size = New System.Drawing.Size(90, 30)
        Me.btnDeleteItemAGAP.TabIndex = 7
        Me.btnDeleteItemAGAP.Text = "Delete"
        Me.btnDeleteItemAGAP.UseVisualStyleBackColor = True
        '
        'btnAddItemAGAP
        '
        Me.btnAddItemAGAP.BackColor = System.Drawing.Color.White
        Me.btnAddItemAGAP.BackgroundImage = CType(resources.GetObject("btnAddItemAGAP.BackgroundImage"), System.Drawing.Image)
        Me.btnAddItemAGAP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddItemAGAP.BorderColor = System.Drawing.Color.Empty
        Me.btnAddItemAGAP.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddItemAGAP.FlatAppearance.BorderSize = 0
        Me.btnAddItemAGAP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddItemAGAP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddItemAGAP.ForeColor = System.Drawing.Color.Black
        Me.btnAddItemAGAP.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddItemAGAP.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemAGAP.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddItemAGAP.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemAGAP.Location = New System.Drawing.Point(447, 269)
        Me.btnAddItemAGAP.Name = "btnAddItemAGAP"
        Me.btnAddItemAGAP.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddItemAGAP.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddItemAGAP.Size = New System.Drawing.Size(90, 30)
        Me.btnAddItemAGAP.TabIndex = 2
        Me.btnAddItemAGAP.Text = "Add"
        Me.btnAddItemAGAP.UseVisualStyleBackColor = True
        '
        'lvItemAKPIList
        '
        Me.lvItemAKPIList.BackColorOnChecked = True
        Me.lvItemAKPIList.ColumnHeaders = Nothing
        Me.lvItemAKPIList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhItemAKPI, Me.objcolhItemAKPIGUID})
        Me.lvItemAKPIList.CompulsoryColumns = ""
        Me.lvItemAKPIList.FullRowSelect = True
        Me.lvItemAKPIList.GridLines = True
        Me.lvItemAKPIList.GroupingColumn = Nothing
        Me.lvItemAKPIList.HideSelection = False
        Me.lvItemAKPIList.Location = New System.Drawing.Point(24, 139)
        Me.lvItemAKPIList.MinColumnWidth = 50
        Me.lvItemAKPIList.MultiSelect = False
        Me.lvItemAKPIList.Name = "lvItemAKPIList"
        Me.lvItemAKPIList.OptionalColumns = ""
        Me.lvItemAKPIList.ShowMoreItem = False
        Me.lvItemAKPIList.ShowSaveItem = False
        Me.lvItemAKPIList.ShowSelectAll = True
        Me.lvItemAKPIList.ShowSizeAllColumnsToFit = True
        Me.lvItemAKPIList.Size = New System.Drawing.Size(348, 124)
        Me.lvItemAKPIList.Sortable = True
        Me.lvItemAKPIList.TabIndex = 6
        Me.lvItemAKPIList.UseCompatibleStateImageBehavior = False
        Me.lvItemAKPIList.View = System.Windows.Forms.View.Details
        '
        'colhItemAKPI
        '
        Me.colhItemAKPI.Tag = "colhItemAKPI"
        Me.colhItemAKPI.Text = ""
        Me.colhItemAKPI.Width = 342
        '
        'objcolhItemAKPIGUID
        '
        Me.objcolhItemAKPIGUID.Tag = "objcolhItemAKPIGUID"
        Me.objcolhItemAKPIGUID.Text = "objcolhItemAKPIGUID"
        Me.objcolhItemAKPIGUID.Width = 0
        '
        'txtItemAJobCapability
        '
        Me.txtItemAJobCapability.Location = New System.Drawing.Point(381, 94)
        Me.txtItemAJobCapability.Multiline = True
        Me.txtItemAJobCapability.Name = "txtItemAJobCapability"
        Me.txtItemAJobCapability.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtItemAJobCapability.Size = New System.Drawing.Size(347, 39)
        Me.txtItemAJobCapability.TabIndex = 5
        '
        'lblItemAJobCapability
        '
        Me.lblItemAJobCapability.Location = New System.Drawing.Point(381, 62)
        Me.lblItemAJobCapability.Name = "lblItemAJobCapability"
        Me.lblItemAJobCapability.Size = New System.Drawing.Size(348, 28)
        Me.lblItemAJobCapability.TabIndex = 4
        Me.lblItemAJobCapability.Text = "Identified Job Capability or Training Related Performance Deficiency/Gap"
        '
        'txtItemAKPI
        '
        Me.txtItemAKPI.Location = New System.Drawing.Point(25, 94)
        Me.txtItemAKPI.Multiline = True
        Me.txtItemAKPI.Name = "txtItemAKPI"
        Me.txtItemAKPI.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtItemAKPI.Size = New System.Drawing.Size(347, 39)
        Me.txtItemAKPI.TabIndex = 3
        '
        'lblItemAKPI
        '
        Me.lblItemAKPI.Location = New System.Drawing.Point(24, 63)
        Me.lblItemAKPI.Name = "lblItemAKPI"
        Me.lblItemAKPI.Size = New System.Drawing.Size(348, 27)
        Me.lblItemAKPI.TabIndex = 2
        Me.lblItemAKPI.Text = "KPI / KRA"
        '
        'LblItemACaption
        '
        Me.LblItemACaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblItemACaption.Location = New System.Drawing.Point(19, 28)
        Me.LblItemACaption.Name = "LblItemACaption"
        Me.LblItemACaption.Size = New System.Drawing.Size(708, 35)
        Me.LblItemACaption.TabIndex = 1
        Me.LblItemACaption.Text = "Job Capability or Training Related Performance Gap identified during Continuous P" & _
            "erformance Assessment or Appraisal Process."
        '
        'LblItemA
        '
        Me.LblItemA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblItemA.Location = New System.Drawing.Point(7, 6)
        Me.LblItemA.Name = "LblItemA"
        Me.LblItemA.Size = New System.Drawing.Size(88, 15)
        Me.LblItemA.TabIndex = 0
        Me.LblItemA.Text = "Item 1 (a) :"
        '
        'tbppartB
        '
        Me.tbppartB.Controls.Add(Me.pnlPartB)
        Me.tbppartB.Location = New System.Drawing.Point(4, 22)
        Me.tbppartB.Name = "tbppartB"
        Me.tbppartB.Padding = New System.Windows.Forms.Padding(3)
        Me.tbppartB.Size = New System.Drawing.Size(760, 326)
        Me.tbppartB.TabIndex = 1
        Me.tbppartB.Text = "Part B"
        Me.tbppartB.UseVisualStyleBackColor = True
        '
        'pnlPartB
        '
        Me.pnlPartB.Controls.Add(Me.lvPartBKRA)
        Me.pnlPartB.Controls.Add(Me.txtPartBNameTask)
        Me.pnlPartB.Controls.Add(Me.lblNametask)
        Me.pnlPartB.Controls.Add(Me.lblPartBJobCapabilities)
        Me.pnlPartB.Controls.Add(Me.lblPartBDesc)
        Me.pnlPartB.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPartB.Location = New System.Drawing.Point(3, 3)
        Me.pnlPartB.Name = "pnlPartB"
        Me.pnlPartB.Size = New System.Drawing.Size(754, 320)
        Me.pnlPartB.TabIndex = 0
        '
        'lvPartBKRA
        '
        Me.lvPartBKRA.BackColorOnChecked = True
        Me.lvPartBKRA.ColumnHeaders = Nothing
        Me.lvPartBKRA.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPartBKRA})
        Me.lvPartBKRA.CompulsoryColumns = ""
        Me.lvPartBKRA.FullRowSelect = True
        Me.lvPartBKRA.GridLines = True
        Me.lvPartBKRA.GroupingColumn = Nothing
        Me.lvPartBKRA.HideSelection = False
        Me.lvPartBKRA.Location = New System.Drawing.Point(22, 112)
        Me.lvPartBKRA.MinColumnWidth = 50
        Me.lvPartBKRA.MultiSelect = False
        Me.lvPartBKRA.Name = "lvPartBKRA"
        Me.lvPartBKRA.OptionalColumns = ""
        Me.lvPartBKRA.ShowMoreItem = False
        Me.lvPartBKRA.ShowSaveItem = False
        Me.lvPartBKRA.ShowSelectAll = True
        Me.lvPartBKRA.ShowSizeAllColumnsToFit = True
        Me.lvPartBKRA.Size = New System.Drawing.Size(256, 200)
        Me.lvPartBKRA.Sortable = True
        Me.lvPartBKRA.TabIndex = 255
        Me.lvPartBKRA.UseCompatibleStateImageBehavior = False
        Me.lvPartBKRA.View = System.Windows.Forms.View.Details
        '
        'colhPartBKRA
        '
        Me.colhPartBKRA.Tag = "colhPartBKRA"
        Me.colhPartBKRA.Text = "Key Performance Indicator or Key Results Area for Senior Position"
        Me.colhPartBKRA.Width = 250
        '
        'txtPartBNameTask
        '
        Me.txtPartBNameTask.Location = New System.Drawing.Point(284, 112)
        Me.txtPartBNameTask.Multiline = True
        Me.txtPartBNameTask.Name = "txtPartBNameTask"
        Me.txtPartBNameTask.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPartBNameTask.Size = New System.Drawing.Size(462, 200)
        Me.txtPartBNameTask.TabIndex = 251
        '
        'lblNametask
        '
        Me.lblNametask.Location = New System.Drawing.Point(19, 86)
        Me.lblNametask.Name = "lblNametask"
        Me.lblNametask.Size = New System.Drawing.Size(719, 23)
        Me.lblNametask.TabIndex = 250
        Me.lblNametask.Text = "Name or Describe tasks that you are able to perform competently to the required s" & _
            "tandards after attending the Training in relation to Items 1(a)."
        '
        'lblPartBJobCapabilities
        '
        Me.lblPartBJobCapabilities.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartBJobCapabilities.Location = New System.Drawing.Point(13, 55)
        Me.lblPartBJobCapabilities.Name = "lblPartBJobCapabilities"
        Me.lblPartBJobCapabilities.Size = New System.Drawing.Size(733, 20)
        Me.lblPartBJobCapabilities.TabIndex = 3
        Me.lblPartBJobCapabilities.Text = "Job capabilities enhanced/Improved after attending the training/programme."
        '
        'lblPartBDesc
        '
        Me.lblPartBDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartBDesc.Location = New System.Drawing.Point(3, 5)
        Me.lblPartBDesc.Name = "lblPartBDesc"
        Me.lblPartBDesc.Size = New System.Drawing.Size(746, 49)
        Me.lblPartBDesc.TabIndex = 2
        Me.lblPartBDesc.Text = resources.GetString("lblPartBDesc.Text")
        '
        'tbppartC
        '
        Me.tbppartC.Controls.Add(Me.pnlPactC)
        Me.tbppartC.Location = New System.Drawing.Point(4, 22)
        Me.tbppartC.Name = "tbppartC"
        Me.tbppartC.Size = New System.Drawing.Size(760, 326)
        Me.tbppartC.TabIndex = 2
        Me.tbppartC.Text = "Part C"
        Me.tbppartC.UseVisualStyleBackColor = True
        '
        'pnlPactC
        '
        Me.pnlPactC.AutoScroll = True
        Me.pnlPactC.Controls.Add(Me.txtFeedbackComment)
        Me.pnlPactC.Controls.Add(Me.lvFeedBackList)
        Me.pnlPactC.Controls.Add(Me.btnEditFeedBack)
        Me.pnlPactC.Controls.Add(Me.lblComments)
        Me.pnlPactC.Controls.Add(Me.lblPartCDesc)
        Me.pnlPactC.Controls.Add(Me.btnAddFeedback)
        Me.pnlPactC.Controls.Add(Me.lblQuestion)
        Me.pnlPactC.Controls.Add(Me.objBtnSearchAnswer)
        Me.pnlPactC.Controls.Add(Me.btnDeleteFeedBack)
        Me.pnlPactC.Controls.Add(Me.cboQuestion)
        Me.pnlPactC.Controls.Add(Me.cboAnswer)
        Me.pnlPactC.Controls.Add(Me.objBtnSearchQuestion)
        Me.pnlPactC.Controls.Add(Me.lblAnswer)
        Me.pnlPactC.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPactC.Location = New System.Drawing.Point(0, 0)
        Me.pnlPactC.Name = "pnlPactC"
        Me.pnlPactC.Size = New System.Drawing.Size(760, 326)
        Me.pnlPactC.TabIndex = 273
        '
        'txtFeedbackComment
        '
        Me.txtFeedbackComment.Location = New System.Drawing.Point(457, 97)
        Me.txtFeedbackComment.Multiline = True
        Me.txtFeedbackComment.Name = "txtFeedbackComment"
        Me.txtFeedbackComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFeedbackComment.Size = New System.Drawing.Size(283, 45)
        Me.txtFeedbackComment.TabIndex = 261
        '
        'lvFeedBackList
        '
        Me.lvFeedBackList.BackColorOnChecked = True
        Me.lvFeedBackList.ColumnHeaders = Nothing
        Me.lvFeedBackList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhQuestion, Me.colhAnswer, Me.colhFeedBackComment, Me.objColhQuestionunkid, Me.objColhAnswerunkid, Me.objcolhPartCGUID})
        Me.lvFeedBackList.CompulsoryColumns = ""
        Me.lvFeedBackList.FullRowSelect = True
        Me.lvFeedBackList.GridLines = True
        Me.lvFeedBackList.GroupingColumn = Nothing
        Me.lvFeedBackList.HideSelection = False
        Me.lvFeedBackList.Location = New System.Drawing.Point(6, 182)
        Me.lvFeedBackList.MinColumnWidth = 50
        Me.lvFeedBackList.MultiSelect = False
        Me.lvFeedBackList.Name = "lvFeedBackList"
        Me.lvFeedBackList.OptionalColumns = ""
        Me.lvFeedBackList.ShowMoreItem = False
        Me.lvFeedBackList.ShowSaveItem = False
        Me.lvFeedBackList.ShowSelectAll = True
        Me.lvFeedBackList.ShowSizeAllColumnsToFit = True
        Me.lvFeedBackList.Size = New System.Drawing.Size(734, 151)
        Me.lvFeedBackList.Sortable = True
        Me.lvFeedBackList.TabIndex = 256
        Me.lvFeedBackList.UseCompatibleStateImageBehavior = False
        Me.lvFeedBackList.View = System.Windows.Forms.View.Details
        '
        'colhQuestion
        '
        Me.colhQuestion.Tag = "colhQuestion"
        Me.colhQuestion.Text = "Question"
        Me.colhQuestion.Width = 325
        '
        'colhAnswer
        '
        Me.colhAnswer.Tag = "colhAnswer"
        Me.colhAnswer.Text = "Answer"
        Me.colhAnswer.Width = 175
        '
        'colhFeedBackComment
        '
        Me.colhFeedBackComment.DisplayIndex = 5
        Me.colhFeedBackComment.Tag = "colhFeedBackComment"
        Me.colhFeedBackComment.Text = "Comment"
        Me.colhFeedBackComment.Width = 225
        '
        'objColhQuestionunkid
        '
        Me.objColhQuestionunkid.DisplayIndex = 2
        Me.objColhQuestionunkid.Tag = "objColhQuestionunkid"
        Me.objColhQuestionunkid.Text = "objColhQuestionunkid"
        Me.objColhQuestionunkid.Width = 0
        '
        'objColhAnswerunkid
        '
        Me.objColhAnswerunkid.DisplayIndex = 3
        Me.objColhAnswerunkid.Tag = "objColhAnswerunkid"
        Me.objColhAnswerunkid.Text = "objColhAnswerunkid"
        Me.objColhAnswerunkid.Width = 0
        '
        'objcolhPartCGUID
        '
        Me.objcolhPartCGUID.DisplayIndex = 4
        Me.objcolhPartCGUID.Tag = "objcolhPartCGUID"
        Me.objcolhPartCGUID.Text = "objcolhPartCGUID"
        Me.objcolhPartCGUID.Width = 0
        '
        'btnEditFeedBack
        '
        Me.btnEditFeedBack.BackColor = System.Drawing.Color.White
        Me.btnEditFeedBack.BackgroundImage = CType(resources.GetObject("btnEditFeedBack.BackgroundImage"), System.Drawing.Image)
        Me.btnEditFeedBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditFeedBack.BorderColor = System.Drawing.Color.Empty
        Me.btnEditFeedBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditFeedBack.FlatAppearance.BorderSize = 0
        Me.btnEditFeedBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditFeedBack.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditFeedBack.ForeColor = System.Drawing.Color.Black
        Me.btnEditFeedBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditFeedBack.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditFeedBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditFeedBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditFeedBack.Location = New System.Drawing.Point(553, 148)
        Me.btnEditFeedBack.Name = "btnEditFeedBack"
        Me.btnEditFeedBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditFeedBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditFeedBack.Size = New System.Drawing.Size(90, 30)
        Me.btnEditFeedBack.TabIndex = 260
        Me.btnEditFeedBack.Text = "Edit"
        Me.btnEditFeedBack.UseVisualStyleBackColor = True
        '
        'lblComments
        '
        Me.lblComments.Location = New System.Drawing.Point(389, 97)
        Me.lblComments.Name = "lblComments"
        Me.lblComments.Size = New System.Drawing.Size(62, 16)
        Me.lblComments.TabIndex = 272
        Me.lblComments.Text = "Comment"
        '
        'lblPartCDesc
        '
        Me.lblPartCDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartCDesc.Location = New System.Drawing.Point(7, 8)
        Me.lblPartCDesc.Name = "lblPartCDesc"
        Me.lblPartCDesc.Size = New System.Drawing.Size(729, 77)
        Me.lblPartCDesc.TabIndex = 265
        Me.lblPartCDesc.Text = resources.GetString("lblPartCDesc.Text")
        '
        'btnAddFeedback
        '
        Me.btnAddFeedback.BackColor = System.Drawing.Color.White
        Me.btnAddFeedback.BackgroundImage = CType(resources.GetObject("btnAddFeedback.BackgroundImage"), System.Drawing.Image)
        Me.btnAddFeedback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddFeedback.BorderColor = System.Drawing.Color.Empty
        Me.btnAddFeedback.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddFeedback.FlatAppearance.BorderSize = 0
        Me.btnAddFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddFeedback.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddFeedback.ForeColor = System.Drawing.Color.Black
        Me.btnAddFeedback.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddFeedback.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddFeedback.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddFeedback.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddFeedback.Location = New System.Drawing.Point(457, 148)
        Me.btnAddFeedback.Name = "btnAddFeedback"
        Me.btnAddFeedback.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddFeedback.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddFeedback.Size = New System.Drawing.Size(90, 30)
        Me.btnAddFeedback.TabIndex = 258
        Me.btnAddFeedback.Text = "Add"
        Me.btnAddFeedback.UseVisualStyleBackColor = True
        '
        'lblQuestion
        '
        Me.lblQuestion.Location = New System.Drawing.Point(11, 96)
        Me.lblQuestion.Name = "lblQuestion"
        Me.lblQuestion.Size = New System.Drawing.Size(62, 16)
        Me.lblQuestion.TabIndex = 262
        Me.lblQuestion.Text = "Question"
        '
        'objBtnSearchAnswer
        '
        Me.objBtnSearchAnswer.BackColor = System.Drawing.Color.Transparent
        Me.objBtnSearchAnswer.BackColor1 = System.Drawing.Color.Transparent
        Me.objBtnSearchAnswer.BackColor2 = System.Drawing.Color.Transparent
        Me.objBtnSearchAnswer.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objBtnSearchAnswer.BorderSelected = False
        Me.objBtnSearchAnswer.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objBtnSearchAnswer.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objBtnSearchAnswer.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objBtnSearchAnswer.Location = New System.Drawing.Point(362, 122)
        Me.objBtnSearchAnswer.Name = "objBtnSearchAnswer"
        Me.objBtnSearchAnswer.Size = New System.Drawing.Size(21, 21)
        Me.objBtnSearchAnswer.TabIndex = 271
        '
        'btnDeleteFeedBack
        '
        Me.btnDeleteFeedBack.BackColor = System.Drawing.Color.White
        Me.btnDeleteFeedBack.BackgroundImage = CType(resources.GetObject("btnDeleteFeedBack.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteFeedBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteFeedBack.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteFeedBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteFeedBack.FlatAppearance.BorderSize = 0
        Me.btnDeleteFeedBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteFeedBack.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteFeedBack.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteFeedBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteFeedBack.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFeedBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteFeedBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFeedBack.Location = New System.Drawing.Point(649, 148)
        Me.btnDeleteFeedBack.Name = "btnDeleteFeedBack"
        Me.btnDeleteFeedBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteFeedBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFeedBack.Size = New System.Drawing.Size(90, 30)
        Me.btnDeleteFeedBack.TabIndex = 259
        Me.btnDeleteFeedBack.Text = "Delete"
        Me.btnDeleteFeedBack.UseVisualStyleBackColor = True
        '
        'cboQuestion
        '
        Me.cboQuestion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQuestion.DropDownWidth = 600
        Me.cboQuestion.FormattingEnabled = True
        Me.cboQuestion.Location = New System.Drawing.Point(76, 94)
        Me.cboQuestion.Name = "cboQuestion"
        Me.cboQuestion.Size = New System.Drawing.Size(282, 21)
        Me.cboQuestion.TabIndex = 263
        '
        'cboAnswer
        '
        Me.cboAnswer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnswer.DropDownWidth = 600
        Me.cboAnswer.FormattingEnabled = True
        Me.cboAnswer.Location = New System.Drawing.Point(76, 121)
        Me.cboAnswer.Name = "cboAnswer"
        Me.cboAnswer.Size = New System.Drawing.Size(282, 21)
        Me.cboAnswer.TabIndex = 270
        '
        'objBtnSearchQuestion
        '
        Me.objBtnSearchQuestion.BackColor = System.Drawing.Color.Transparent
        Me.objBtnSearchQuestion.BackColor1 = System.Drawing.Color.Transparent
        Me.objBtnSearchQuestion.BackColor2 = System.Drawing.Color.Transparent
        Me.objBtnSearchQuestion.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objBtnSearchQuestion.BorderSelected = False
        Me.objBtnSearchQuestion.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objBtnSearchQuestion.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objBtnSearchQuestion.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objBtnSearchQuestion.Location = New System.Drawing.Point(362, 94)
        Me.objBtnSearchQuestion.Name = "objBtnSearchQuestion"
        Me.objBtnSearchQuestion.Size = New System.Drawing.Size(21, 21)
        Me.objBtnSearchQuestion.TabIndex = 264
        '
        'lblAnswer
        '
        Me.lblAnswer.Location = New System.Drawing.Point(11, 123)
        Me.lblAnswer.Name = "lblAnswer"
        Me.lblAnswer.Size = New System.Drawing.Size(62, 16)
        Me.lblAnswer.TabIndex = 269
        Me.lblAnswer.Text = "Answer"
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Tag = "colhKPA"
        Me.ColumnHeader1.Text = "Key Performance Indicator or Key Results Area"
        Me.ColumnHeader1.Width = 245
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Tag = "colhItemBTrainingObjective"
        Me.ColumnHeader2.Text = "Training Objective"
        Me.ColumnHeader2.Width = 370
        '
        'frmTrainingImpact
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(772, 516)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTrainingImpact"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Impact Evaluation"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.tbImpactEvaluation.ResumeLayout(False)
        Me.tbppartA.ResumeLayout(False)
        Me.pnlPartA.ResumeLayout(False)
        Me.pnlPartA.PerformLayout()
        Me.tbppartB.ResumeLayout(False)
        Me.pnlPartB.ResumeLayout(False)
        Me.pnlPartB.PerformLayout()
        Me.tbppartC.ResumeLayout(False)
        Me.pnlPactC.ResumeLayout(False)
        Me.pnlPactC.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tbImpactEvaluation As System.Windows.Forms.TabControl
    Friend WithEvents tbppartA As System.Windows.Forms.TabPage
    Friend WithEvents tbppartB As System.Windows.Forms.TabPage
    Friend WithEvents tbppartC As System.Windows.Forms.TabPage
    Friend WithEvents lblCourse As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboCourse As System.Windows.Forms.ComboBox
    Friend WithEvents objBtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchCourse As eZee.Common.eZeeGradientButton
    Friend WithEvents txtStartDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents txtEnddate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents pnlPartA As System.Windows.Forms.Panel
    Friend WithEvents LblItemACaption As System.Windows.Forms.Label
    Friend WithEvents LblItemA As System.Windows.Forms.Label
    Friend WithEvents txtItemAKPI As System.Windows.Forms.TextBox
    Friend WithEvents lblItemAKPI As System.Windows.Forms.Label
    Friend WithEvents txtItemAJobCapability As System.Windows.Forms.TextBox
    Friend WithEvents lblItemAJobCapability As System.Windows.Forms.Label
    Friend WithEvents btnDeleteItemAGAP As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddItemAGAP As eZee.Common.eZeeLightButton
    Friend WithEvents lvItemAKPIList As eZee.Common.eZeeListView
    Friend WithEvents colhItemAKPI As System.Windows.Forms.ColumnHeader
    Friend WithEvents LblItemB As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents txtItemBTrainingObjective As System.Windows.Forms.TextBox
    Friend WithEvents lblItemBTrainingObjective As System.Windows.Forms.Label
    Friend WithEvents LblItemBCaption As System.Windows.Forms.Label
    Friend WithEvents btnDeleteItemB As eZee.Common.eZeeLightButton
    Friend WithEvents lvItemBList As eZee.Common.eZeeListView
    Friend WithEvents colhItemBTrainingObjective As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlPartB As System.Windows.Forms.Panel
    Friend WithEvents lblPartBDesc As System.Windows.Forms.Label
    Friend WithEvents lblPartBJobCapabilities As System.Windows.Forms.Label
    Friend WithEvents lblNametask As System.Windows.Forms.Label
    Friend WithEvents txtPartBNameTask As System.Windows.Forms.TextBox
    Friend WithEvents lvPartBKRA As eZee.Common.eZeeListView
    Friend WithEvents colhPartBKRA As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEditItemB As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditItemAGAP As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddItemB As eZee.Common.eZeeLightButton
    Friend WithEvents lvItemAJobCapability As eZee.Common.eZeeListView
    Friend WithEvents colhItemAJobCapability As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEditItemAKPI As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeleteItemAKPI As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddItemAKPI As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhItemAKPIGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhItemAJobCapGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhItemBGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objBtnLineManager As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLineManager As System.Windows.Forms.ComboBox
    Friend WithEvents lblLineManager As System.Windows.Forms.Label
    Friend WithEvents btnEditFeedBack As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeleteFeedBack As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddFeedback As eZee.Common.eZeeLightButton
    Friend WithEvents lvFeedBackList As eZee.Common.eZeeListView
    Friend WithEvents colhQuestion As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtFeedbackComment As System.Windows.Forms.TextBox
    Friend WithEvents txtItemBKPI As System.Windows.Forms.TextBox
    Friend WithEvents lblItemBKPI As System.Windows.Forms.Label
    Friend WithEvents lvItemBKPIList As eZee.Common.eZeeListView
    Friend WithEvents colhPartAItemBKPI As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhItemBKPIGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objBtnSearchQuestion As eZee.Common.eZeeGradientButton
    Friend WithEvents cboQuestion As System.Windows.Forms.ComboBox
    Friend WithEvents lblQuestion As System.Windows.Forms.Label
    Friend WithEvents lblPartCDesc As System.Windows.Forms.Label
    Friend WithEvents objBtnSearchAnswer As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAnswer As System.Windows.Forms.ComboBox
    Friend WithEvents lblAnswer As System.Windows.Forms.Label
    Friend WithEvents lblComments As System.Windows.Forms.Label
    Friend WithEvents pnlPactC As System.Windows.Forms.Panel
    Friend WithEvents colhAnswer As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhQuestionunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhAnswerunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPartCGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFeedBackComment As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnFinalSave As eZee.Common.eZeeLightButton
End Class
