﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports System.IO
Imports System.Text

#End Region


Public Class frmTrainingImpactList

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmTrainingImpactList"
    Private objImpact As clshrtnatraining_impact_master

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            Dim objCourse As New clsTraining_Scheduling
            dsList = objCourse.getComboList("List", True, -1, True)
            cboCourse.DisplayMember = "Name"
            cboCourse.ValueMember = "Id"
            cboCourse.DataSource = dsList.Tables(0)

            dsList = Nothing
            Dim objEmployee As New clsEmployee_Master
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'Sohail (23 Nov 2012) -- End
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables(0)

            cboLineManager.DisplayMember = "employeename"
            cboLineManager.ValueMember = "employeeunkid"
            cboLineManager.DataSource = dsList.Tables(0).Copy()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsImpact As DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try

            If User._Object.Privilege._AllowToViewLevel3EvaluationList = True Then                'Pinkal (02-Jul-2012) -- Start

                dsImpact = objImpact.GetList("List", True)

                If CInt(cboCourse.SelectedValue) > 0 Then
                    StrSearching &= "AND trainingschedulingunkid = " & CInt(cboCourse.SelectedValue)
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue)
                End If

                If CInt(cboLineManager.SelectedValue) > 0 Then
                    StrSearching &= "AND managerunkid = " & CInt(cboLineManager.SelectedValue)
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    dtTable = New DataView(dsImpact.Tables("List"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsImpact.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
                End If

                lvImpactList.Items.Clear()
                Dim lvItem As ListViewItem
                For Each drRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Tag = drRow.Item("impactunkid")
                    lvItem.Text = drRow.Item("course").ToString & " ( " & CDate(drRow.Item("start_date")).ToShortDateString & " - " & CDate(drRow.Item("end_date")).ToShortDateString & " ) "
                    lvItem.SubItems.Add(drRow.Item("Manager").ToString)
                    lvItem.SubItems.Add(drRow.Item("employee").ToString)
                    lvItem.SubItems.Add(drRow.Item("status").ToString)
                    lvItem.SubItems.Add(drRow.Item("iscomplete").ToString)
                    lvImpactList.Items.Add(lvItem)
                Next

                lvImpactList.GroupingColumn = colhCourse
                lvImpactList.DisplayGroups(True)

                If lvImpactList.Items.Count > 15 Then
                    colhStatus.Width = 130 - 18
                Else
                    colhStatus.Width = 130
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddLevelIIIEvaluation
            btnEdit.Enabled = User._Object.Privilege._AllowToEditLevelIIIEvaluation
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteLevelIIIEvaluation
            mnuPreview.Enabled = User._Object.Privilege._AllowToPreviewLevelIIIEvaluation
            mnuPrint.Enabled = User._Object.Privilege._AllowToPrintLevelIIIEvaluation
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#End Region

#Region " Form's Event(s) "

    Private Sub frmTrainingImpactList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            objImpact = New clshrtnatraining_impact_master
            FillCombo()
            lvImpactList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingImpactList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshrtnatraining_impact_master.SetMessages()
            'clshrtnatraining_impact_kra.SetMessages()
            'clshrtnatraining_impact_development.SetMessages()
            'clshrtnatraining_impact_objective.SetMessages()
            'clshrtnatraining_impact_feedback.SetMessages()
            ',clshrtnatraining_impact_kra,clshrtnatraining_impact_development,clshrtnatraining_impact_objective,clshrtnatraining_impact_feedback

            objfrm._Other_ModuleNames = "clshrtnatraining_impact_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearchCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCourse.Click
        Dim frm As New frmCommonSearch
        Try
            If cboCourse.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboCourse.ValueMember
                    .DisplayMember = cboCourse.DisplayMember
                    .DataSource = CType(cboCourse.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboCourse.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objBtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If cboEmployee.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objBtnLineManager_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnLineManager.Click
        Dim frm As New frmCommonSearch
        Try
            If cboLineManager.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboLineManager.ValueMember
                    .DisplayMember = cboLineManager.DisplayMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboLineManager.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboLineManager.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnLineManager_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try

    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCourse.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboLineManager.SelectedIndex = 0
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmTrainingImpact
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvImpactList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Training Impact Evaluation from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvImpactList.Select()
            Exit Sub
        End If
        Dim frm As New frmTrainingImpact
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvImpactList.SelectedItems(0).Index

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(CInt(lvImpactList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvImpactList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Training Impact Evaluation from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvImpactList.Select()
                Exit Sub
            End If
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvImpactList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Training Impact Evaluation ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objImpact._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objImpact._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objImpact._Voiduserunkid = User._Object._Userunkid
                objImpact.Delete(CInt(lvImpactList.SelectedItems(0).Tag))
                lvImpactList.SelectedItems(0).Remove()

                If lvImpactList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvImpactList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvImpactList.Items.Count - 1
                    lvImpactList.Items(intSelectedIndex).Selected = True
                    lvImpactList.EnsureVisible(intSelectedIndex)
                ElseIf lvImpactList.Items.Count <> 0 Then
                    lvImpactList.Items(intSelectedIndex).Selected = True
                    lvImpactList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvImpactList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " ListView's Event(s) "

    Private Sub lvImpactList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvImpactList.SelectedIndexChanged
        Try
            If lvImpactList.SelectedItems.Count > 0 Then

                If CBool(lvImpactList.SelectedItems(0).SubItems(objcolhIsComplete.Index).Text) = True Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                Else
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvImpactList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ContextMenu's Event(s)"

    Private Sub mnuPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreview.Click
        Try
            If lvImpactList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Training Impact Evaluation from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvImpactList.Select()
                Exit Sub
            End If
            Dim objImpact As New ArutiReports.clsTrainingImpact
            objImpact._Impactunkid = CInt(lvImpactList.SelectedItems(0).Tag)
            objImpact.generateReport(0, enPrintAction.Preview)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreview_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        Try
            If lvImpactList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Training Impact Evaluation from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvImpactList.Select()
                Exit Sub
            End If
            Dim objImpact As New ArutiReports.clsTrainingImpact
            objImpact._Impactunkid = CInt(lvImpactList.SelectedItems(0).Tag)
            objImpact.generateReport(0, enPrintAction.Print)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrint_Click", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.lblLineManager.Text = Language._Object.getCaption(Me.lblLineManager.Name, Me.lblLineManager.Text)
            Me.colhCourse.Text = Language._Object.getCaption(CStr(Me.colhCourse.Tag), Me.colhCourse.Text)
            Me.colhLineManager.Text = Language._Object.getCaption(CStr(Me.colhLineManager.Tag), Me.colhLineManager.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuPreview.Text = Language._Object.getCaption(Me.mnuPreview.Name, Me.mnuPreview.Text)
            Me.mnuPrint.Text = Language._Object.getCaption(Me.mnuPrint.Name, Me.mnuPrint.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Training Impact Evaluation from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Training Impact Evaluation ?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class