﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeFeedback
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeFeedback))
        Me.cboResult = New System.Windows.Forms.ComboBox
        Me.lblResult = New System.Windows.Forms.Label
        Me.cboFeedbackGroup = New System.Windows.Forms.ComboBox
        Me.lblFeedbackGroup = New System.Windows.Forms.Label
        Me.cboFeedbackItem = New System.Windows.Forms.ComboBox
        Me.lblFeedbackItems = New System.Windows.Forms.Label
        Me.objbtnSearchItems = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchResult = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchFeedbackGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchFdbkSubItems = New eZee.Common.eZeeGradientButton
        Me.cboFdbkSubItems = New System.Windows.Forms.ComboBox
        Me.lblFeedbackSubItems = New System.Windows.Forms.Label
        Me.objbtnSearchCourseTitle = New eZee.Common.eZeeGradientButton
        Me.cboCourseTitle = New System.Windows.Forms.ComboBox
        Me.lblCourseTitle = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbFeedbackInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnDeleteFeedback = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEditFeedback = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddFeedback = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvFeedback = New eZee.Common.eZeeListView(Me.components)
        Me.colhFeedbackItem = New System.Windows.Forms.ColumnHeader
        Me.colhFeedbackSubItems = New System.Windows.Forms.ColumnHeader
        Me.colhResult = New System.Windows.Forms.ColumnHeader
        Me.colhTextResult = New System.Windows.Forms.ColumnHeader
        Me.objcolhFdbkGrp = New System.Windows.Forms.ColumnHeader
        Me.objcolhFdbkGrpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhFdbkItemId = New System.Windows.Forms.ColumnHeader
        Me.objcolhFdbkSubItemId = New System.Windows.Forms.ColumnHeader
        Me.objcolhResultId = New System.Windows.Forms.ColumnHeader
        Me.objcolhGuid = New System.Windows.Forms.ColumnHeader
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtTextResult = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSaveComplete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFeedbackInformation.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboResult
        '
        Me.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResult.DropDownWidth = 500
        Me.cboResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResult.FormattingEnabled = True
        Me.cboResult.Location = New System.Drawing.Point(433, 86)
        Me.cboResult.Name = "cboResult"
        Me.cboResult.Size = New System.Drawing.Size(202, 21)
        Me.cboResult.TabIndex = 8
        '
        'lblResult
        '
        Me.lblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(338, 89)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(89, 15)
        Me.lblResult.TabIndex = 7
        Me.lblResult.Text = "Result"
        Me.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFeedbackGroup
        '
        Me.cboFeedbackGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFeedbackGroup.DropDownWidth = 500
        Me.cboFeedbackGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFeedbackGroup.FormattingEnabled = True
        Me.cboFeedbackGroup.Location = New System.Drawing.Point(103, 87)
        Me.cboFeedbackGroup.Name = "cboFeedbackGroup"
        Me.cboFeedbackGroup.Size = New System.Drawing.Size(202, 21)
        Me.cboFeedbackGroup.TabIndex = 6
        '
        'lblFeedbackGroup
        '
        Me.lblFeedbackGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFeedbackGroup.Location = New System.Drawing.Point(8, 90)
        Me.lblFeedbackGroup.Name = "lblFeedbackGroup"
        Me.lblFeedbackGroup.Size = New System.Drawing.Size(89, 15)
        Me.lblFeedbackGroup.TabIndex = 5
        Me.lblFeedbackGroup.Text = "Feedback Group"
        Me.lblFeedbackGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFeedbackItem
        '
        Me.cboFeedbackItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFeedbackItem.DropDownWidth = 500
        Me.cboFeedbackItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFeedbackItem.FormattingEnabled = True
        Me.cboFeedbackItem.Location = New System.Drawing.Point(433, 32)
        Me.cboFeedbackItem.Name = "cboFeedbackItem"
        Me.cboFeedbackItem.Size = New System.Drawing.Size(202, 21)
        Me.cboFeedbackItem.TabIndex = 215
        '
        'lblFeedbackItems
        '
        Me.lblFeedbackItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFeedbackItems.Location = New System.Drawing.Point(338, 35)
        Me.lblFeedbackItems.Name = "lblFeedbackItems"
        Me.lblFeedbackItems.Size = New System.Drawing.Size(89, 15)
        Me.lblFeedbackItems.TabIndex = 216
        Me.lblFeedbackItems.Text = "Feedback Item"
        Me.lblFeedbackItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchItems
        '
        Me.objbtnSearchItems.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchItems.BorderSelected = False
        Me.objbtnSearchItems.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchItems.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchItems.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchItems.Location = New System.Drawing.Point(641, 32)
        Me.objbtnSearchItems.Name = "objbtnSearchItems"
        Me.objbtnSearchItems.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchItems.TabIndex = 219
        '
        'objbtnSearchResult
        '
        Me.objbtnSearchResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResult.BorderSelected = False
        Me.objbtnSearchResult.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResult.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResult.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResult.Location = New System.Drawing.Point(641, 86)
        Me.objbtnSearchResult.Name = "objbtnSearchResult"
        Me.objbtnSearchResult.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResult.TabIndex = 219
        '
        'objbtnSearchFeedbackGroup
        '
        Me.objbtnSearchFeedbackGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFeedbackGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFeedbackGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFeedbackGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFeedbackGroup.BorderSelected = False
        Me.objbtnSearchFeedbackGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFeedbackGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFeedbackGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFeedbackGroup.Location = New System.Drawing.Point(311, 87)
        Me.objbtnSearchFeedbackGroup.Name = "objbtnSearchFeedbackGroup"
        Me.objbtnSearchFeedbackGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFeedbackGroup.TabIndex = 219
        '
        'objbtnSearchFdbkSubItems
        '
        Me.objbtnSearchFdbkSubItems.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFdbkSubItems.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFdbkSubItems.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFdbkSubItems.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFdbkSubItems.BorderSelected = False
        Me.objbtnSearchFdbkSubItems.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFdbkSubItems.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFdbkSubItems.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFdbkSubItems.Location = New System.Drawing.Point(641, 59)
        Me.objbtnSearchFdbkSubItems.Name = "objbtnSearchFdbkSubItems"
        Me.objbtnSearchFdbkSubItems.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFdbkSubItems.TabIndex = 222
        '
        'cboFdbkSubItems
        '
        Me.cboFdbkSubItems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFdbkSubItems.DropDownWidth = 500
        Me.cboFdbkSubItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFdbkSubItems.FormattingEnabled = True
        Me.cboFdbkSubItems.Location = New System.Drawing.Point(433, 59)
        Me.cboFdbkSubItems.Name = "cboFdbkSubItems"
        Me.cboFdbkSubItems.Size = New System.Drawing.Size(202, 21)
        Me.cboFdbkSubItems.TabIndex = 220
        '
        'lblFeedbackSubItems
        '
        Me.lblFeedbackSubItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFeedbackSubItems.Location = New System.Drawing.Point(338, 62)
        Me.lblFeedbackSubItems.Name = "lblFeedbackSubItems"
        Me.lblFeedbackSubItems.Size = New System.Drawing.Size(89, 15)
        Me.lblFeedbackSubItems.TabIndex = 221
        Me.lblFeedbackSubItems.Text = "SubItem"
        Me.lblFeedbackSubItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCourseTitle
        '
        Me.objbtnSearchCourseTitle.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCourseTitle.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourseTitle.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourseTitle.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCourseTitle.BorderSelected = False
        Me.objbtnSearchCourseTitle.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCourseTitle.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCourseTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCourseTitle.Location = New System.Drawing.Point(311, 33)
        Me.objbtnSearchCourseTitle.Name = "objbtnSearchCourseTitle"
        Me.objbtnSearchCourseTitle.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCourseTitle.TabIndex = 225
        '
        'cboCourseTitle
        '
        Me.cboCourseTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourseTitle.DropDownWidth = 500
        Me.cboCourseTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourseTitle.FormattingEnabled = True
        Me.cboCourseTitle.Location = New System.Drawing.Point(103, 33)
        Me.cboCourseTitle.Name = "cboCourseTitle"
        Me.cboCourseTitle.Size = New System.Drawing.Size(202, 21)
        Me.cboCourseTitle.TabIndex = 224
        '
        'lblCourseTitle
        '
        Me.lblCourseTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourseTitle.Location = New System.Drawing.Point(8, 36)
        Me.lblCourseTitle.Name = "lblCourseTitle"
        Me.lblCourseTitle.Size = New System.Drawing.Size(89, 15)
        Me.lblCourseTitle.TabIndex = 223
        Me.lblCourseTitle.Text = "Course Title"
        Me.lblCourseTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(311, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 228
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 500
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(103, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(202, 21)
        Me.cboEmployee.TabIndex = 227
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(89, 15)
        Me.lblEmployee.TabIndex = 226
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFeedbackInformation
        '
        Me.gbFeedbackInformation.BorderColor = System.Drawing.Color.Black
        Me.gbFeedbackInformation.Checked = False
        Me.gbFeedbackInformation.CollapseAllExceptThis = False
        Me.gbFeedbackInformation.CollapsedHoverImage = Nothing
        Me.gbFeedbackInformation.CollapsedNormalImage = Nothing
        Me.gbFeedbackInformation.CollapsedPressedImage = Nothing
        Me.gbFeedbackInformation.CollapseOnLoad = False
        Me.gbFeedbackInformation.Controls.Add(Me.btnDeleteFeedback)
        Me.gbFeedbackInformation.Controls.Add(Me.btnEditFeedback)
        Me.gbFeedbackInformation.Controls.Add(Me.btnAddFeedback)
        Me.gbFeedbackInformation.Controls.Add(Me.lvFeedback)
        Me.gbFeedbackInformation.Controls.Add(Me.objLine1)
        Me.gbFeedbackInformation.Controls.Add(Me.lblRemark)
        Me.gbFeedbackInformation.Controls.Add(Me.txtTextResult)
        Me.gbFeedbackInformation.Controls.Add(Me.cboCourseTitle)
        Me.gbFeedbackInformation.Controls.Add(Me.lblFeedbackGroup)
        Me.gbFeedbackInformation.Controls.Add(Me.objbtnSearchFdbkSubItems)
        Me.gbFeedbackInformation.Controls.Add(Me.cboFdbkSubItems)
        Me.gbFeedbackInformation.Controls.Add(Me.lblCourseTitle)
        Me.gbFeedbackInformation.Controls.Add(Me.cboFeedbackGroup)
        Me.gbFeedbackInformation.Controls.Add(Me.lblFeedbackSubItems)
        Me.gbFeedbackInformation.Controls.Add(Me.objbtnSearchCourseTitle)
        Me.gbFeedbackInformation.Controls.Add(Me.lblResult)
        Me.gbFeedbackInformation.Controls.Add(Me.objbtnSearchFeedbackGroup)
        Me.gbFeedbackInformation.Controls.Add(Me.lblEmployee)
        Me.gbFeedbackInformation.Controls.Add(Me.cboResult)
        Me.gbFeedbackInformation.Controls.Add(Me.objbtnSearchResult)
        Me.gbFeedbackInformation.Controls.Add(Me.cboEmployee)
        Me.gbFeedbackInformation.Controls.Add(Me.objbtnSearchItems)
        Me.gbFeedbackInformation.Controls.Add(Me.lblFeedbackItems)
        Me.gbFeedbackInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFeedbackInformation.Controls.Add(Me.cboFeedbackItem)
        Me.gbFeedbackInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFeedbackInformation.ExpandedHoverImage = Nothing
        Me.gbFeedbackInformation.ExpandedNormalImage = Nothing
        Me.gbFeedbackInformation.ExpandedPressedImage = Nothing
        Me.gbFeedbackInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFeedbackInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFeedbackInformation.HeaderHeight = 25
        Me.gbFeedbackInformation.HeaderMessage = ""
        Me.gbFeedbackInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFeedbackInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFeedbackInformation.HeightOnCollapse = 0
        Me.gbFeedbackInformation.LeftTextSpace = 0
        Me.gbFeedbackInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbFeedbackInformation.Name = "gbFeedbackInformation"
        Me.gbFeedbackInformation.OpenHeight = 300
        Me.gbFeedbackInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFeedbackInformation.ShowBorder = True
        Me.gbFeedbackInformation.ShowCheckBox = False
        Me.gbFeedbackInformation.ShowCollapseButton = False
        Me.gbFeedbackInformation.ShowDefaultBorderColor = True
        Me.gbFeedbackInformation.ShowDownButton = False
        Me.gbFeedbackInformation.ShowHeader = True
        Me.gbFeedbackInformation.Size = New System.Drawing.Size(866, 441)
        Me.gbFeedbackInformation.TabIndex = 231
        Me.gbFeedbackInformation.Temp = 0
        Me.gbFeedbackInformation.Text = "Feedback Information"
        Me.gbFeedbackInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDeleteFeedback
        '
        Me.btnDeleteFeedback.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteFeedback.BackColor = System.Drawing.Color.White
        Me.btnDeleteFeedback.BackgroundImage = CType(resources.GetObject("btnDeleteFeedback.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteFeedback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteFeedback.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteFeedback.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteFeedback.FlatAppearance.BorderSize = 0
        Me.btnDeleteFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteFeedback.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteFeedback.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteFeedback.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteFeedback.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFeedback.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteFeedback.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFeedback.Location = New System.Drawing.Point(766, 124)
        Me.btnDeleteFeedback.Name = "btnDeleteFeedback"
        Me.btnDeleteFeedback.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteFeedback.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFeedback.Size = New System.Drawing.Size(88, 30)
        Me.btnDeleteFeedback.TabIndex = 237
        Me.btnDeleteFeedback.Text = "&Delete"
        Me.btnDeleteFeedback.UseVisualStyleBackColor = True
        '
        'btnEditFeedback
        '
        Me.btnEditFeedback.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditFeedback.BackColor = System.Drawing.Color.White
        Me.btnEditFeedback.BackgroundImage = CType(resources.GetObject("btnEditFeedback.BackgroundImage"), System.Drawing.Image)
        Me.btnEditFeedback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditFeedback.BorderColor = System.Drawing.Color.Empty
        Me.btnEditFeedback.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditFeedback.FlatAppearance.BorderSize = 0
        Me.btnEditFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditFeedback.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditFeedback.ForeColor = System.Drawing.Color.Black
        Me.btnEditFeedback.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditFeedback.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditFeedback.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditFeedback.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditFeedback.Location = New System.Drawing.Point(672, 124)
        Me.btnEditFeedback.Name = "btnEditFeedback"
        Me.btnEditFeedback.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditFeedback.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditFeedback.Size = New System.Drawing.Size(88, 30)
        Me.btnEditFeedback.TabIndex = 236
        Me.btnEditFeedback.Text = "&Edit"
        Me.btnEditFeedback.UseVisualStyleBackColor = True
        '
        'btnAddFeedback
        '
        Me.btnAddFeedback.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddFeedback.BackColor = System.Drawing.Color.White
        Me.btnAddFeedback.BackgroundImage = CType(resources.GetObject("btnAddFeedback.BackgroundImage"), System.Drawing.Image)
        Me.btnAddFeedback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddFeedback.BorderColor = System.Drawing.Color.Empty
        Me.btnAddFeedback.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddFeedback.FlatAppearance.BorderSize = 0
        Me.btnAddFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddFeedback.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddFeedback.ForeColor = System.Drawing.Color.Black
        Me.btnAddFeedback.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddFeedback.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddFeedback.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddFeedback.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddFeedback.Location = New System.Drawing.Point(578, 124)
        Me.btnAddFeedback.Name = "btnAddFeedback"
        Me.btnAddFeedback.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddFeedback.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddFeedback.Size = New System.Drawing.Size(88, 30)
        Me.btnAddFeedback.TabIndex = 235
        Me.btnAddFeedback.Text = "&Add"
        Me.btnAddFeedback.UseVisualStyleBackColor = True
        '
        'lvFeedback
        '
        Me.lvFeedback.BackColorOnChecked = False
        Me.lvFeedback.ColumnHeaders = Nothing
        Me.lvFeedback.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFeedbackItem, Me.colhFeedbackSubItems, Me.colhResult, Me.colhTextResult, Me.objcolhFdbkGrp, Me.objcolhFdbkGrpId, Me.objcolhFdbkItemId, Me.objcolhFdbkSubItemId, Me.objcolhResultId, Me.objcolhGuid})
        Me.lvFeedback.CompulsoryColumns = ""
        Me.lvFeedback.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvFeedback.FullRowSelect = True
        Me.lvFeedback.GridLines = True
        Me.lvFeedback.GroupingColumn = Nothing
        Me.lvFeedback.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvFeedback.HideSelection = False
        Me.lvFeedback.Location = New System.Drawing.Point(11, 160)
        Me.lvFeedback.MinColumnWidth = 50
        Me.lvFeedback.MultiSelect = False
        Me.lvFeedback.Name = "lvFeedback"
        Me.lvFeedback.OptionalColumns = ""
        Me.lvFeedback.ShowMoreItem = False
        Me.lvFeedback.ShowSaveItem = False
        Me.lvFeedback.ShowSelectAll = True
        Me.lvFeedback.ShowSizeAllColumnsToFit = True
        Me.lvFeedback.Size = New System.Drawing.Size(843, 275)
        Me.lvFeedback.Sortable = True
        Me.lvFeedback.TabIndex = 234
        Me.lvFeedback.UseCompatibleStateImageBehavior = False
        Me.lvFeedback.View = System.Windows.Forms.View.Details
        '
        'colhFeedbackItem
        '
        Me.colhFeedbackItem.Tag = "colhFeedbackItem"
        Me.colhFeedbackItem.Text = "Feedback Items"
        Me.colhFeedbackItem.Width = 250
        '
        'colhFeedbackSubItems
        '
        Me.colhFeedbackSubItems.Tag = "colhFeedbackSubItems"
        Me.colhFeedbackSubItems.Text = "SubItems"
        Me.colhFeedbackSubItems.Width = 250
        '
        'colhResult
        '
        Me.colhResult.Tag = "colhResult"
        Me.colhResult.Text = "Result"
        Me.colhResult.Width = 130
        '
        'colhTextResult
        '
        Me.colhTextResult.Tag = "colhTextResult"
        Me.colhTextResult.Text = "Text Result"
        Me.colhTextResult.Width = 208
        '
        'objcolhFdbkGrp
        '
        Me.objcolhFdbkGrp.Tag = "objcolhFdbkGrp"
        Me.objcolhFdbkGrp.Text = ""
        Me.objcolhFdbkGrp.Width = 0
        '
        'objcolhFdbkGrpId
        '
        Me.objcolhFdbkGrpId.Tag = "objcolhFdbkGrpId"
        Me.objcolhFdbkGrpId.Text = ""
        Me.objcolhFdbkGrpId.Width = 0
        '
        'objcolhFdbkItemId
        '
        Me.objcolhFdbkItemId.Tag = "objcolhFdbkItemId"
        Me.objcolhFdbkItemId.Text = ""
        Me.objcolhFdbkItemId.Width = 0
        '
        'objcolhFdbkSubItemId
        '
        Me.objcolhFdbkSubItemId.Tag = "objcolhFdbkSubItemId"
        Me.objcolhFdbkSubItemId.Text = ""
        Me.objcolhFdbkSubItemId.Width = 0
        '
        'objcolhResultId
        '
        Me.objcolhResultId.Tag = "objcolhResultId"
        Me.objcolhResultId.Text = ""
        Me.objcolhResultId.Width = 0
        '
        'objcolhGuid
        '
        Me.objcolhGuid.Tag = "objcolhGuid"
        Me.objcolhGuid.Text = ""
        Me.objcolhGuid.Width = 0
        '
        'objLine1
        '
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(11, 111)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(843, 10)
        Me.objLine1.TabIndex = 233
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(668, 35)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(175, 15)
        Me.lblRemark.TabIndex = 232
        Me.lblRemark.Text = "Text Result"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTextResult
        '
        Me.txtTextResult.Flags = 0
        Me.txtTextResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTextResult.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTextResult.Location = New System.Drawing.Point(668, 59)
        Me.txtTextResult.Multiline = True
        Me.txtTextResult.Name = "txtTextResult"
        Me.txtTextResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTextResult.Size = New System.Drawing.Size(186, 49)
        Me.txtTextResult.TabIndex = 231
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbFeedbackInformation)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(866, 496)
        Me.Panel1.TabIndex = 232
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSaveComplete)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 441)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(866, 55)
        Me.objFooter.TabIndex = 232
        '
        'btnSaveComplete
        '
        Me.btnSaveComplete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveComplete.BackColor = System.Drawing.Color.White
        Me.btnSaveComplete.BackgroundImage = CType(resources.GetObject("btnSaveComplete.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveComplete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveComplete.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveComplete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveComplete.FlatAppearance.BorderSize = 0
        Me.btnSaveComplete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveComplete.ForeColor = System.Drawing.Color.Black
        Me.btnSaveComplete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveComplete.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveComplete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveComplete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveComplete.Location = New System.Drawing.Point(536, 13)
        Me.btnSaveComplete.Name = "btnSaveComplete"
        Me.btnSaveComplete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveComplete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveComplete.Size = New System.Drawing.Size(112, 30)
        Me.btnSaveComplete.TabIndex = 128
        Me.btnSaveComplete.Text = "Save && &Complete"
        Me.btnSaveComplete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(654, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(757, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmEmployeeFeedback
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(866, 496)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeFeedback"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Feedback"
        Me.gbFeedbackInformation.ResumeLayout(False)
        Me.gbFeedbackInformation.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cboResult As System.Windows.Forms.ComboBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents cboFeedbackGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblFeedbackGroup As System.Windows.Forms.Label
    Friend WithEvents cboFeedbackItem As System.Windows.Forms.ComboBox
    Friend WithEvents lblFeedbackItems As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchItems As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchResult As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchFeedbackGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchFdbkSubItems As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFdbkSubItems As System.Windows.Forms.ComboBox
    Friend WithEvents lblFeedbackSubItems As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCourseTitle As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCourseTitle As System.Windows.Forms.ComboBox
    Friend WithEvents lblCourseTitle As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents gbFeedbackInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtTextResult As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents lvFeedback As eZee.Common.eZeeListView
    Friend WithEvents btnDeleteFeedback As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditFeedback As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddFeedback As eZee.Common.eZeeLightButton
    Friend WithEvents colhFeedbackItem As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFeedbackSubItems As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResult As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTextResult As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFdbkGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFdbkGrpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFdbkItemId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFdbkSubItemId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhResultId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGuid As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSaveComplete As eZee.Common.eZeeLightButton
End Class
