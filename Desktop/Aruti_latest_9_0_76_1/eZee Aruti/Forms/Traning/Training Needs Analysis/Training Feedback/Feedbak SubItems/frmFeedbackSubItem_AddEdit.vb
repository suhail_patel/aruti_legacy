﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFeedbackSubItem_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmFeedbackSubItem_AddEdit"
    Private mblnCancel As Boolean = True
    Private objFeedbackSubItem As clshrtnafdbk_subitem_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintFeedback_SubItemunkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintFeedback_SubItemunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintFeedback_SubItemunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmFeedbackSubItem_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFeedbackSubItem = New clshrtnafdbk_subitem_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call setColor()

            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objFeedbackSubItem._Fdbksubitemunkid = mintFeedback_SubItemunkid

                Dim objFeedbackItem As New clshrtnafdbk_item_master
                objFeedbackItem._Fdbkitemunkid = objFeedbackSubItem._Fdbkitemunkid
                cboFeedbackGroup.SelectedValue = objFeedbackItem._Fdbkgroupunkid
                cboFeedbackGroup.Enabled = False : objbtnAddGroup.Enabled = False
                objFeedbackItem = Nothing
            End If

            Call GetValue()

            txtCode.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackSubItem_AddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmFeedbackSubItem_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackSubItem_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFeedbackSubItem_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackSubItem_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFeedbackSubItem_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objFeedbackSubItem = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshrtnafdbk_subitem_master.SetMessages()
            objfrm._Other_ModuleNames = "clshrtnafdbk_subitem_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsVaild() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objFeedbackSubItem.Update()
            Else
                blnFlag = objFeedbackSubItem.Insert()
            End If

            If blnFlag = False And objFeedbackSubItem._Message <> "" Then
                eZeeMsgBox.Show(objFeedbackSubItem._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objFeedbackSubItem = Nothing
                    objFeedbackSubItem = New clshrtnafdbk_subitem_master
                    Call GetValue()
                    cboFeedbackItem.Select()
                Else
                    mintFeedback_SubItemunkid = objFeedbackSubItem._Fdbksubitemunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try

            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If

            Call objFrmLangPopup.displayDialog(txtName.Text, objFeedbackSubItem._Name1, objFeedbackSubItem._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnAddFeedbackItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddFeedbackItem.Click
        Dim frm As New frmFeedbackItems_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim intRefId As Integer = -1

            frm.displayDialog(intRefId, enAction.ADD_ONE)

            If intRefId > 0 Then
                Dim dsCombo As New DataSet
                Dim objFeedbackItem As New clshrtnafdbk_item_master
                dsCombo = objFeedbackItem.getComboList("List", True)
                With cboFeedbackItem
                    .ValueMember = "fdbkitemunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsCombo.Dispose() : objFeedbackItem = Nothing
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddFeedbackItem_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchItems.Click
        Dim frm As New frmCommonSearch
        Try
            If cboFeedbackItem.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = cboFeedbackItem.ValueMember
                .DisplayMember = cboFeedbackItem.DisplayMember
                .DataSource = CType(cboFeedbackItem.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboFeedbackItem.SelectedValue = frm.SelectedValue
                cboFeedbackItem.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchItems_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboFeedbackItem.BackColor = GUI.ColorComp
            cboFeedbackItem.BackColor = GUI.ColorComp
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboFeedbackGroup.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objFeedbackSubItem._Code
            txtName.Text = objFeedbackSubItem._Name
            cboFeedbackItem.SelectedValue = objFeedbackSubItem._Fdbkitemunkid
            txtDescription.Text = objFeedbackSubItem._Description
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objFeedbackSubItem._Code = txtCode.Text.Trim
            objFeedbackSubItem._Name = txtName.Text.Trim
            objFeedbackSubItem._Fdbkitemunkid = CInt(cboFeedbackItem.SelectedValue)
            objFeedbackSubItem._Description = txtDescription.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objFeedbackGroup As New clshrtnafdbk_group_master
        Try
            dsCombo = objFeedbackGroup.getComboList("Group", True)
            cboFeedbackGroup.ValueMember = "fdbkgroupunkid"
            cboFeedbackGroup.DisplayMember = "name"
            cboFeedbackGroup.DataSource = dsCombo.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objFeedbackGroup = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Function IsVaild() As Boolean
        Try
            If CInt(cboFeedbackItem.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Feedback Item is compulsory information.Please Select Feedback Item."), enMsgBoxStyle.Information)
                cboFeedbackItem.Focus()
                Return False
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sub Item Code cannot be blank. Sub Item Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Return False
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sub Item Name cannot be blank. Sub Item Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsVaild", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Combobox's Events "

    Private Sub cboGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFeedbackGroup.SelectedIndexChanged
        Dim objFeedbackItem As New clshrtnafdbk_item_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objFeedbackItem.getComboList("List", True, CInt(cboFeedbackGroup.SelectedValue))
            With cboFeedbackItem
                .ValueMember = "fdbkitemunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim objfrmGroup_AddEdit As New frmFeedbackGroup_AddEdit

            If User._Object._Isrighttoleft = True Then
                objfrmGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmGroup_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmGroup_AddEdit)
            End If

            If objfrmGroup_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillCombo()
                cboFeedbackGroup.Select()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFeedbackSubItems.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFeedbackSubItems.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFeedbackSubItems.Text = Language._Object.getCaption(Me.gbFeedbackSubItems.Name, Me.gbFeedbackSubItems.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
			Me.lblAssessmentDescription.Text = Language._Object.getCaption(Me.lblAssessmentDescription.Name, Me.lblAssessmentDescription.Text)
			Me.lblAssessmentItem.Text = Language._Object.getCaption(Me.lblAssessmentItem.Name, Me.lblAssessmentItem.Text)
			Me.lblAssessmentItemCode.Text = Language._Object.getCaption(Me.lblAssessmentItemCode.Name, Me.lblAssessmentItemCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Feedback Item is compulsory information.Please Select Feedback Item.")
			Language.setMessage(mstrModuleName, 2, "Sub Item Code cannot be blank. Sub Item Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Sub Item Name cannot be blank. Sub Item Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class