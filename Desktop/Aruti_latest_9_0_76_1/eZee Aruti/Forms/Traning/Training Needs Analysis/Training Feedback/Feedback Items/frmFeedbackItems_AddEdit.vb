﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFeedbackItems_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmFeedbackItems_AddEdit"
    Private mblnCancel As Boolean = True
    Private objFeedBackItem As clshrtnafdbk_item_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintFeedBackItemunkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintFeedBackItemunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintFeedBackItemunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmFeedbackItems_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFeedBackItem = New clshrtnafdbk_item_master
        objlblCaption.Text = ""
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            setColor()
            If menAction = enAction.EDIT_ONE Then
                objFeedBackItem._Fdbkitemunkid = mintFeedBackItemunkid
            End If
            FillCombo()
            GetValue()
            txtCode.Focus()
            objlblCaption.Text = Language.getMessage(mstrModuleName, 4, "Note: Please do not map result for feedback item(s)." & vbCrLf & "Which you need to give as remarks while providing feedback.")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackItems_AddEdit", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFeedbackItems_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackItems_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFeedbackItems_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackItems_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFeedbackItems_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objFeedBackItem = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshrtnafdbk_item_master.SetMessages()
            objfrm._Other_ModuleNames = "clshrtnafdbk_item_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Feedback Group is compulsory information.Please Select Feedback Group."), enMsgBoxStyle.Information)
                cboGroup.Focus()
                Exit Sub
                'ElseIf CInt(cboResultGroup.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Result Group is compulsory information.Please Select Result Group."), enMsgBoxStyle.Information)
                '    cboResultGroup.Select()
                '    Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Feedback Item Code cannot be blank. Feedback Item Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Feedback Item Name cannot be blank. Feedback Item Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objFeedBackItem.Update()
            Else
                blnFlag = objFeedBackItem.Insert()
            End If

            If blnFlag = False And objFeedBackItem._Message <> "" Then
                eZeeMsgBox.Show(objFeedBackItem._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objFeedBackItem = Nothing
                    objFeedBackItem = New clshrtnafdbk_item_master
                    cboGroup.Tag = CInt(cboGroup.SelectedValue)
                    Call GetValue()
                    cboGroup.SelectedValue = CInt(cboGroup.Tag)
                    cboGroup.Select()
                Else
                    mintFeedBackItemunkid = objFeedBackItem._Fdbkitemunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            Call objFrmLangPopup.displayDialog(txtName.Text, objFeedBackItem._Name1, objFeedBackItem._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddResultGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResultGroup.Click
        Try
            Dim objCommon As New frmCommonMaster

            If User._Object._Isrighttoleft = True Then
                objCommon.RightToLeft = Windows.Forms.RightToLeft.Yes
                objCommon.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objCommon)
            End If

            Dim dsCombos As New DataSet
            Dim objResultGroup As New clsCommon_Master
            If objCommon.displayDialog(-1, clsCommon_Master.enCommonMaster.RESULT_GROUP, enAction.ADD_ONE) Then
                dsCombos = objResultGroup.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "List")
                With cboResultGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim objfrmGroup_AddEdit As New frmFeedbackGroup_AddEdit

            If User._Object._Isrighttoleft = True Then
                objfrmGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmGroup_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmGroup_AddEdit)
            End If

            If objfrmGroup_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillCombo()
                cboGroup.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboGroup.BackColor = GUI.ColorComp
            cboResultGroup.BackColor = GUI.ColorComp
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objFeedBackItem._Code
            txtName.Text = objFeedBackItem._Name
            cboGroup.SelectedValue = CInt(objFeedBackItem._Fdbkgroupunkid)
            cboResultGroup.SelectedValue = CInt(objFeedBackItem._Resultgroupunkid)
            txtDescription.Text = objFeedBackItem._Description
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objFeedBackItem._Code = txtCode.Text.Trim
            objFeedBackItem._Name = txtName.Text.Trim
            objFeedBackItem._Fdbkgroupunkid = CInt(cboGroup.SelectedValue)
            objFeedBackItem._Resultgroupunkid = CInt(cboResultGroup.SelectedValue)
            objFeedBackItem._Description = txtDescription.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsGroup As DataSet = Nothing
        Try
            Dim objFeedBackGroup As New clshrtnafdbk_group_master
            dsGroup = objFeedBackGroup.getComboList("Group", True)
            cboGroup.ValueMember = "fdbkgroupunkid"
            cboGroup.DisplayMember = "name"
            cboGroup.DataSource = dsGroup.Tables(0)

            Dim objCommonmaster As New clsCommon_Master
            dsGroup = objCommonmaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "Result Group")
            cboResultGroup.ValueMember = "masterunkid"
            cboResultGroup.DisplayMember = "name"
            cboResultGroup.DataSource = dsGroup.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFeedbackItems.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFeedbackItems.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFeedbackItems.Text = Language._Object.getCaption(Me.gbFeedbackItems.Name, Me.gbFeedbackItems.Text)
			Me.lblAssessmentDescription.Text = Language._Object.getCaption(Me.lblAssessmentDescription.Name, Me.lblAssessmentDescription.Text)
			Me.lblAssessmentItem.Text = Language._Object.getCaption(Me.lblAssessmentItem.Name, Me.lblAssessmentItem.Text)
			Me.lblAssessmentItemCode.Text = Language._Object.getCaption(Me.lblAssessmentItemCode.Name, Me.lblAssessmentItemCode.Text)
			Me.lblGroupCode.Text = Language._Object.getCaption(Me.lblGroupCode.Name, Me.lblGroupCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Feedback Group is compulsory information.Please Select Feedback Group.")
			Language.setMessage(mstrModuleName, 2, "Feedback Item Code cannot be blank. Feedback Item Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Feedback Item Name cannot be blank. Feedback Item Name is required information.")
			Language.setMessage(mstrModuleName, 4, "Note: Please do not map result for feedback item(s)." & vbCrLf & "Which you need to give as remarks while providing feedback.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class