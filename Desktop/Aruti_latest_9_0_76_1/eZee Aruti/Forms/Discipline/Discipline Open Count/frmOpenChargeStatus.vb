﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmOpenChargeStatus

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmOpenChargeStatus"
    Private objProceedingMaster As clsdiscipline_proceeding_master
    Private objMembersTran As clsdiscipline_members_tran
    Private objDisciplineFileMaster As clsDiscipline_file_master
    Private objDisciplineFileTran As clsDiscipline_file_tran
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtProceeding As DataTable
    Private mdtMemTran As DataTable
    Private mdtEmployee As DataTable
    Private mdtReference As DataTable
    Private mdtFileTran As DataTable
    Private mdtInvestigator As DataTable
    Private mdsCommittee As DataSet
    Private mintDisciplineProceedingMasterunkid As Integer = -1
    Private mintCommitteeMasterId As Integer = -1
    Private mintChargeCount As Integer = -1
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private dvProceeding As DataView
    Private mintDisciplineFileUnkid As Integer = 0
    Private mblnIsExternal As Boolean = False
    Private mintDisciplineStatusId As Integer = 0
    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private mdtScanAttachDocument As DataTable
    'S.SANDEEP |11-NOV-2019| -- END
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intUnkId As Integer, ByVal blnIsExternal As Boolean, ByVal intDisciplineStatusId As Integer) As Boolean
        Try
            mintDisciplineFileUnkid = intUnkId
            mblnIsExternal = blnIsExternal
            mintDisciplineStatusId = intDisciplineStatusId
            If blnIsExternal = True Then
                objbtnOpen.Text = Language.getMessage(mstrModuleName, 1, "Open Charges Externally")
            Else
                objbtnOpen.Text = Language.getMessage(mstrModuleName, 2, "Open Charges Internally")
            End If
            Call FillCombo()
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        'S.SANDEEP |01-MAY-2020| -- START
        'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
        Dim objCMaster As New clsCommon_Master
        'S.SANDEEP |01-MAY-2020| -- END
        Try
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With

            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_CASE_OPEN_REASON, True, "List")
            With cboReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP |01-MAY-2020| -- END
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillDetails()
        Try
            objDisciplineFileMaster._Disciplinefileunkid = mintDisciplineFileUnkid
            txtChargeDate.Text = Format(objDisciplineFileMaster._Chargedate, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern)
            If objDisciplineFileMaster._Interdictiondate <> Nothing Then
                txtinterdictiondate.Text = Format(objDisciplineFileMaster._Interdictiondate, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern)
            End If
            txtChargeDescription.Text = objDisciplineFileMaster._Charge_Description
            txtReferenceNo.Text = objDisciplineFileMaster._Reference_No

            cboEmployee.SelectedValue = objDisciplineFileMaster._Involved_Employeeunkid

            Dim objEmp As New clsEmployee_Master
            Dim objJob As New clsJobs
            Dim objDepartment As New clsDepartment
            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            objJob._Jobunkid = objEmp._Jobunkid
            objDepartment._Departmentunkid = objEmp._Departmentunkid
            txtJobTitle.Text = objJob._Job_Name
            txtDepartment.Text = objDepartment._Name
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDetails", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetProceedingData()
        Try
            dvProceeding = mdtProceeding.DefaultView

            If mdtProceeding.Rows.Count > 1 Then
                'S.SANDEEP |25-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : NEW DISCIPLINE REPORT 
                'dvProceeding.RowFilter = "AUD <> 'D' AND count <> ''"
                dvProceeding.RowFilter = "AUD <> 'D' AND disciplinefileunkid > 0 "
                'S.SANDEEP |25-MAR-2020| -- END
            Else
                dvProceeding.RowFilter = "AUD <> 'D'"
            End If

            dgvProceedingDetails.AutoGenerateColumns = False

            dgcolhCount.DataPropertyName = "count"
            dgcolhProceedingDate.DataPropertyName = "proceeding_date"
            dgcolhProceedingDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhProceedingDetails.DataPropertyName = "proceeding_details"
            dgcolhDisciplineAction.DataPropertyName = "reason_action"
            dgcolhCommittee.DataPropertyName = "committee"
            dgcolhInvestigator.DataPropertyName = "investigator"
            dgcolhPIComments.DataPropertyName = "person_involved_comments"
            dgcolhCommentRemark.DataPropertyName = "remarks_comments"
            dgcolhApproved.DataPropertyName = "approved_status"
            dgcolhCountStatus.DataPropertyName = "status_name"
            objdgcolhGUID.DataPropertyName = "GUID"
            objcolhdisciplinefiletranunkid.DataPropertyName = "disciplinefiletranunkid"
            objdgcolhProceedingMastId.DataPropertyName = "disciplineproceedingmasterunkid"

            dgvProceedingDetails.DataSource = mdtProceeding
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetProceedingData", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmOpenChargeStatus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objProceedingMaster = New clsdiscipline_proceeding_master
        objMembersTran = New clsdiscipline_members_tran
        objDisciplineFileMaster = New clsDiscipline_file_master
        objDisciplineFileTran = New clsDiscipline_file_tran
        Try
            Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillDetails()

            objProceedingMaster._DisciplineProceedingMasterunkid = mintDisciplineProceedingMasterunkid
            mdtProceeding = objProceedingMaster._ProceedingTable
            objMembersTran._DisciplineProceedingMasterunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintDisciplineProceedingMasterunkid
            mdtMemTran = objMembersTran._MemTranTable
            Call SetProceedingData()
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            Dim objAttachDocs As New clsScan_Attach_Documents
            'objAttachDocs.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
            objAttachDocs.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "", CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            Dim xRow = objAttachDocs._Datatable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.DISCIPLINES _
                                                                         And x.Field(Of Integer)("transactionunkid") = mintDisciplineProceedingMasterunkid _
                                                                         And x.Field(Of String)("form_name") = "'frmDisciplineProceedingList'")
            If xRow.Count > 0 Then
                mdtScanAttachDocument = xRow.CopyToDataTable()
            Else
                mdtScanAttachDocument = objAttachDocs._Datatable.Clone
            End If
            objAttachDocs = Nothing
            'S.SANDEEP |11-NOV-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOpenChargeStatus_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmOpenChargeStatus_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOpenChargeStatus_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmOpenChargeStatus_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            objProceedingMaster = Nothing : objMembersTran = Nothing
            objDisciplineFileMaster = Nothing : objDisciplineFileTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOpenChargeStatus_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_file_master.SetMessages()
            clsdiscipline_members_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_file_master,clsdiscipline_members_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpen.Click
        Dim blnFlag As Boolean = False
        Try
            If dgvProceedingDetails.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, no proceeding information added into proceeding list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
            If CInt(cboReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Opening reason is mandatory information. Please select opening reason to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP |01-MAY-2020| -- END

            If mdtProceeding IsNot Nothing Then
                mdtProceeding.AcceptChanges()
                'S.SANDEEP |25-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : NEW DISCIPLINE REPORT 
                'If mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of String)("count") <> "" And x.Field(Of String)("AUD") <> "D").Count <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, no proceeding information added into proceeding list."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If
                If mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplinefileunkid") > 0 And x.Field(Of String)("AUD") <> "D").Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, no proceeding information added into proceeding list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'S.SANDEEP |25-MAR-2020| -- END
            End If

            objProceedingMaster._Userunkid = User._Object._Userunkid
            'S.SANDEEP [24 MAY 2016] -- Start
            'Email Notification
            objProceedingMaster._LoginTypeId = enLogin_Mode.DESKTOP
            objProceedingMaster._DisciplineFileUnkid = mintDisciplineFileUnkid
            'S.SANDEEP [24 MAY 2016] -- End

            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
            objProceedingMaster._DisciplineOpeningReasonId = CInt(cboReason.SelectedValue)
            'S.SANDEEP |01-MAY-2020| -- END

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'blnFlag = objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtProceeding, mdtMemTran, _
            '                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True)

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'blnFlag = objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtProceeding, mdtMemTran, _
            '                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, Company._Object._Companyunkid)
            'Hemant (22 Jan 2021) -- Start
            'Enhancement #OLD-275 - NMB Discipline Open Case Notification
            'blnFlag = objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtProceeding, mdtMemTran, Nothing, _
            '                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, Company._Object._Companyunkid, "", True) 'S.SANDEEP |25-MAR-2020| -- START {True} -- END
            blnFlag = objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtProceeding, mdtMemTran, mdtScanAttachDocument, _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, Company._Object._Companyunkid, "", True, ConfigParameter._Object._AddProceedingAgainstEachCount) 'S.SANDEEP |25-MAR-2020| -- START {True} -- END
            'Hemant (22 Jan 2021) -- End
            
            'S.SANDEEP |11-NOV-2019| -- END

            'Sohail (30 Nov 2017) -- End
            If blnFlag = False AndAlso objProceedingMaster._Message <> "" Then
                eZeeMsgBox.Show(objProceedingMaster._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True Then
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpen_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboReason.DisplayMember
                .ValueMember = cboReason.ValueMember
                .DataSource = CType(cboReason.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboReason.SelectedValue = frm.SelectedValue
                cboReason.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReferenceNo_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP |01-MAY-2020| -- END

#End Region

#Region " DataGrid Events "

    Private Sub dgvProceedingDetails_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvProceedingDetails.CellClick
        Dim frm As New frmChargeProceedingDetails
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Select Case e.ColumnIndex
                Case objdgcolhAdd.Index
                    Dim dtMemTran As DataTable = mdtMemTran.Clone
                    dtMemTran.Rows.Clear()
                    'S.SANDEEP |11-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'If frm.displayDialog(enAction.ADD_CONTINUE, mintDisciplineFileUnkid, mintDisciplineProceedingMasterunkid, "", mdtProceeding, dtMemTran, True, mblnIsExternal) Then
                    If frm.displayDialog(enAction.ADD_CONTINUE, mintDisciplineFileUnkid, mintDisciplineProceedingMasterunkid, "", mdtProceeding, dtMemTran, mdtScanAttachDocument, True, mblnIsExternal) Then
                        'S.SANDEEP |11-NOV-2019| -- END
                        mdtMemTran.Merge(dtMemTran, True)
                        Call SetProceedingData()
                    End If
                Case objdgcolhEdit.Index
                    If dgvProceedingDetails.Rows(e.RowIndex).Cells(dgcolhCount.Index).Value.ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No Proceeding details is defined to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP |11-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'If frm.displayDialog(enAction.EDIT_ONE, mintDisciplineFileUnkid, mintDisciplineProceedingMasterunkid, dgvProceedingDetails.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString, mdtProceeding, mdtMemTran, True, mblnIsExternal) Then
                    If frm.displayDialog(enAction.EDIT_ONE, mintDisciplineFileUnkid, mintDisciplineProceedingMasterunkid, dgvProceedingDetails.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString, mdtProceeding, mdtMemTran, mdtScanAttachDocument, True, mblnIsExternal) Then
                        'S.SANDEEP |11-NOV-2019| -- END
                        SetProceedingData()
                    End If
                Case objdgcolhDelete.Index
                    If dgvProceedingDetails.Rows(e.RowIndex).Cells(dgcolhCount.Index).Value.ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, No Proceeding details is defined to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    Dim strVoidReason As String = String.Empty
                    Dim dtRow() As DataRow = Nothing
                    If mintDisciplineProceedingMasterunkid > 0 Then
                        dtRow = mdtProceeding.Select("disciplineproceedingmasterunkid = '" & mintDisciplineProceedingMasterunkid & "' AND AUD <> 'D'")
                    Else
                        dtRow = mdtProceeding.Select("GUID = '" & dgvProceedingDetails.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                    End If
                    If mintDisciplineProceedingMasterunkid > 0 Then
                        Dim ofrm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            ofrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            ofrm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(ofrm)
                        End If
                        ofrm.displayDialog(enVoidCategoryType.DISCIPLINE, strVoidReason)
                        ofrm.Dispose()
                        If strVoidReason.Trim.Length <= 0 Then Exit Sub
                    End If
                    If strVoidReason.Trim.Length > 0 Then
                        dtRow(0).Item("isvoid") = True
                        dtRow(0).Item("voiduserunkid") = User._Object._Userunkid
                        dtRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtRow(0).Item("voidreason") = strVoidReason
                        dtRow(0).Item("AUD") = "D"
                    Else
                        mdtProceeding.Rows.Remove(dtRow(0))
                    End If
                    Call SetProceedingData()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvProceedingDetails_CellClick", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbDiscipilneFiling.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDiscipilneFiling.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.objbtnOpen.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnOpen.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbDiscipilneFiling.Text = Language._Object.getCaption(Me.gbDiscipilneFiling.Name, Me.gbDiscipilneFiling.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblJobTitle.Text = Language._Object.getCaption(Me.lblJobTitle.Name, Me.lblJobTitle.Text)
            Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.Name, Me.lblInterdictionDate.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.colhCount.Text = Language._Object.getCaption(CStr(Me.colhCount.Tag), Me.colhCount.Text)
            Me.colhIncident.Text = Language._Object.getCaption(CStr(Me.colhIncident.Tag), Me.colhIncident.Text)
            Me.colhOffCategory.Text = Language._Object.getCaption(CStr(Me.colhOffCategory.Tag), Me.colhOffCategory.Text)
            Me.colhOffence.Text = Language._Object.getCaption(CStr(Me.colhOffence.Tag), Me.colhOffence.Text)
            Me.colhSeverity.Text = Language._Object.getCaption(CStr(Me.colhSeverity.Tag), Me.colhSeverity.Text)
            Me.lblChargeDescription.Text = Language._Object.getCaption(Me.lblChargeDescription.Name, Me.lblChargeDescription.Text)
            Me.dgcolhCount.HeaderText = Language._Object.getCaption(Me.dgcolhCount.Name, Me.dgcolhCount.HeaderText)
            Me.dgcolhProceedingDate.HeaderText = Language._Object.getCaption(Me.dgcolhProceedingDate.Name, Me.dgcolhProceedingDate.HeaderText)
            Me.dgcolhProceedingDetails.HeaderText = Language._Object.getCaption(Me.dgcolhProceedingDetails.Name, Me.dgcolhProceedingDetails.HeaderText)
            Me.dgcolhPIComments.HeaderText = Language._Object.getCaption(Me.dgcolhPIComments.Name, Me.dgcolhPIComments.HeaderText)
            Me.dgcolhCommentRemark.HeaderText = Language._Object.getCaption(Me.dgcolhCommentRemark.Name, Me.dgcolhCommentRemark.HeaderText)
            Me.dgcolhDisciplineAction.HeaderText = Language._Object.getCaption(Me.dgcolhDisciplineAction.Name, Me.dgcolhDisciplineAction.HeaderText)
            Me.dgcolhCommittee.HeaderText = Language._Object.getCaption(Me.dgcolhCommittee.Name, Me.dgcolhCommittee.HeaderText)
            Me.dgcolhInvestigator.HeaderText = Language._Object.getCaption(Me.dgcolhInvestigator.Name, Me.dgcolhInvestigator.HeaderText)
            Me.dgcolhApproved.HeaderText = Language._Object.getCaption(Me.dgcolhApproved.Name, Me.dgcolhApproved.HeaderText)
            Me.dgcolhCountStatus.HeaderText = Language._Object.getCaption(Me.dgcolhCountStatus.Name, Me.dgcolhCountStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Open Charges Externally")
            Language.setMessage(mstrModuleName, 2, "Open Charges Internally")
            Language.setMessage(mstrModuleName, 3, "Sorry, No Proceeding details is defined to perform edit operation.")
            Language.setMessage(mstrModuleName, 4, "Sorry, No Proceeding details is defined to perform delete operation.")
            Language.setMessage(mstrModuleName, 5, "Sorry, no proceeding information added into proceeding list.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'#Region " Private Variables "

'Private ReadOnly mstrModuleName As String = "frmOpenChargeStatus"
'Private mblnCancel As Boolean = True
'Private mintDisciplineFileUnkid As Integer = -1
'Private objDiscChargeMaster As clsDiscipline_file_master
'Private mblnIsExternal As Boolean = False
'Private mintDisciplineStatusId As Integer = 0
'Private objProceedingMaster As clsdiscipline_proceeding_master

'#End Region

'#Region " Display Dialog "

'Public Function displayDialog(ByVal intUnkId As Integer, ByVal blnIsExternal As Boolean, ByVal intDisciplineStatusId As Integer) As Boolean
'    Try
'        mintDisciplineFileUnkid = intUnkId
'        mblnIsExternal = blnIsExternal
'        mintDisciplineStatusId = intDisciplineStatusId
'        If blnIsExternal = True Then
'            objbtnOpen.Text = Language.getMessage(mstrModuleName, 1, "Open Charges Externally")
'        Else
'            objbtnOpen.Text = Language.getMessage(mstrModuleName, 2, "Open Charges Internally")
'        End If
'        Call FillCombo()
'        Call FillDetails()
'        Me.ShowDialog()
'        Return Not mblnCancel
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'    Finally
'    End Try
'End Function

'#End Region

'#Region " Private Methods "

'Private Sub FillCombo()
'    Dim dsCombo As New DataSet
'    Dim objEmp As New clsEmployee_Master
'    Try
'        dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                         User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
'                                         Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                         ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
'        With cboEmployee
'            .ValueMember = "employeeunkid"
'            .DisplayMember = "employeename"
'            .DataSource = dsCombo.Tables("List")
'            .SelectedValue = 0
'            .Text = ""
'        End With

'    Catch ex As Exception
'        Throw New Exception(ex.Message & "; Procedure Name: FillCombo; Module Name: " & mstrModuleName)
'    Finally
'    End Try
'End Sub

'Private Sub FillDetails()
'    Try
'        RemoveHandler lvChargesCount.ItemChecked, AddressOf lvChargesCount_ItemChecked
'        RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged


'        objDiscChargeMaster = New clsDiscipline_file_master
'        objProceedingMaster = New clsdiscipline_proceeding_master
'        objDiscChargeMaster._Disciplinefileunkid = mintDisciplineFileUnkid
'        txtChargeDate.Text = Format(objDiscChargeMaster._Chargedate, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern)
'        If objDiscChargeMaster._Interdictiondate <> Nothing Then
'            txtinterdictiondate.Text = Format(objDiscChargeMaster._Interdictiondate, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern)
'        End If
'        txtChargeDescription.Text = objDiscChargeMaster._Charge_Description
'        txtReferenceNo.Text = objDiscChargeMaster._Reference_No

'        cboEmployee.SelectedValue = objDiscChargeMaster._Involved_Employeeunkid

'        Dim objEmp As New clsEmployee_Master
'        Dim objJob As New clsJobs
'        Dim objDepartment As New clsDepartment
'        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
'        objJob._Jobunkid = objEmp._Jobunkid
'        objDepartment._Departmentunkid = objEmp._Departmentunkid
'        txtJobTitle.Text = objJob._Job_Name
'        txtDepartment.Text = objDepartment._Name
'        objDepartment = Nothing : objJob = Nothing : objEmp = Nothing

'        Dim dsCharges As New DataSet

'        dsCharges = objProceedingMaster.GetClosedCountForOpenProcess(mintDisciplineFileUnkid, "List")
'        For Each r As DataRow In dsCharges.Tables("List").Rows
'            Dim lvItem As New ListViewItem

'            lvItem.Text = ""
'            lvItem.SubItems.Add(r.Item("charge_count").ToString)
'            lvItem.SubItems.Add(r.Item("incident_description").ToString)
'            lvItem.SubItems.Add(r.Item("charge_category").ToString)
'            lvItem.SubItems.Add(r.Item("charge_descr").ToString)
'            lvItem.SubItems.Add(r.Item("charge_severity").ToString)
'            lvItem.SubItems.Add(r.Item("offencecategoryunkid").ToString)
'            lvItem.SubItems.Add(r.Item("offenceunkid").ToString)
'            lvItem.SubItems.Add(r.Item("disciplinefileunkid").ToString)
'            lvItem.SubItems.Add(r.Item("disciplineproceedingmasterunkid").ToString)

'            lvItem.Tag = r.Item("disciplinefiletranunkid")

'            lvChargesCount.Items.Add(lvItem)
'        Next

'        'objDiscChargeTran._Disciplinefileunkid = mintDisciplineFileUnkid
'        'mdtCharges = objDiscChargeTran._ChargesTable

'        'Dim xRows = (From P In mdtCharges.AsEnumerable().Where(Function(x) x.Field(Of Integer)("count_statusid") = clsdiscipline_proceeding_master.enProceedingCountStatus.Close) Select P)

'        'If xRows.Count > 0 Then

'        '    lvChargesCount.Items.Clear()
'        '    For Each r As DataRow In xRows
'        '        Dim lvItem As New ListViewItem

'        '        lvItem.Text = ""
'        '        lvItem.SubItems.Add(r.Item("charge_count").ToString)
'        '        lvItem.SubItems.Add(r.Item("incident_description").ToString)
'        '        lvItem.SubItems.Add(r.Item("charge_category").ToString)
'        '        lvItem.SubItems.Add(r.Item("charge_descr").ToString)
'        '        lvItem.SubItems.Add(r.Item("charge_severity").ToString)
'        '        lvItem.SubItems.Add(r.Item("offencecategoryunkid").ToString)
'        '        lvItem.SubItems.Add(r.Item("offenceunkid").ToString)
'        '        lvItem.SubItems.Add(r.Item("GUID").ToString)
'        '        lvItem.SubItems.Add(r.Item("disciplinefileunkid").ToString)

'        '        lvItem.Tag = r.Item("disciplinefiletranunkid")

'        '        lvChargesCount.Items.Add(lvItem)
'        '    Next

'        'End If

'        objDiscChargeMaster = Nothing : objProceedingMaster = Nothing

'        AddHandler lvChargesCount.ItemChecked, AddressOf lvChargesCount_ItemChecked
'        AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged

'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "FillDetails", mstrModuleName)
'    Finally
'    End Try
'End Sub

'#End Region

'#Region " Form's Events "

'Private Sub frmOpenChargeStatus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'    objDiscChargeMaster = New clsDiscipline_file_master
'    objProceedingMaster = New clsdiscipline_proceeding_master
'    Try
'        Set_Logo(Me, gApplicationType)
'        Language.setLanguage(Me.Name)
'        Call OtherSettings()
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "frmOpenChargeStatus_Load", mstrModuleName)
'    Finally
'    End Try
'End Sub

'Private Sub frmOpenChargeStatus_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'    Try
'        If Asc(e.KeyChar) = 13 Then
'            SendKeys.Send("{TAB}")
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "frmOpenChargeStatus_KeyPress", mstrModuleName)
'    Finally
'    End Try
'End Sub

'Private Sub frmOpenChargeStatus_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'    objDiscChargeMaster = Nothing : objProceedingMaster = Nothing
'End Sub

'Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'    Dim objfrm As New frmLanguage
'    Try
'        If User._Object._Isrighttoleft = True Then
'            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'            objfrm.RightToLeftLayout = True
'            Call Language.ctlRightToLeftlayOut(objfrm)
'        End If

'        Call SetMessages()

'        clsDiscipline_file_master.SetMessages()
'        objfrm._Other_ModuleNames = "clsDiscipline_file_master"
'        objfrm.displayDialog(Me)

'        Call SetLanguage()

'    Catch ex As System.Exception
'        Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'    Finally
'        objfrm.Dispose()
'        objfrm = Nothing
'    End Try
'End Sub

'#End Region

'#Region " Button's Event(s) "

'Private Sub objbtnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpen.Click
'    Try
'        'If lvChargesCount.CheckedItems.Count > 0 Then
'        '    Dim strRemark As String = String.Empty
'        '    strRemark = InputBox(Language.getMessage(mstrModuleName, 2, "Enter reason to open the selected count(s)."), Me.Text)
'        '    If strRemark.Trim.Length <= 0 Then Exit Sub

'        '    Dim mDicValue As New Dictionary(Of Integer, String)
'        '    Dim iSelectedTag As List(Of String) = lvChargesCount.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToList()
'        '    mDicValue.Add(mintDisciplineFileUnkid, String.Join(",", iSelectedTag.ToArray()))

'        '    objDiscChargeStatus._Isexternal = mblnIsExternal
'        '    objDiscChargeStatus._Isvoid = False
'        '    objDiscChargeStatus._Remark = strRemark
'        '    objDiscChargeStatus._Statusdate = ConfigParameter._Object._CurrentDateAndTime
'        '    objDiscChargeStatus._Statusunkid = clsdiscipline_proceeding_master.enProceedingCountStatus.Open
'        '    objDiscChargeStatus._Userunkid = User._Object._Userunkid
'        '    objDiscChargeStatus._Voidatetime = Nothing
'        '    objDiscChargeStatus._Voidreason = ""
'        '    objDiscChargeStatus._Voiduserunkid = -1

'        '    If objDiscChargeStatus.Insert(mDicValue, mintDisciplineStatusId) = False Then
'        '        eZeeMsgBox.Show(objDiscChargeStatus._Message, enMsgBoxStyle.Information)
'        '        Exit Sub
'        '    Else
'        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Checked charge(s) count open successfully."), enMsgBoxStyle.Information)
'        '        Me.Close()
'        '    End If
'        'End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objbtnOpen_Click", mstrModuleName)
'    Finally
'    End Try
'End Sub

'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'    Try
'        Me.Close()
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'    Finally
'    End Try
'End Sub

'#End Region

'#Region " Checkbox Event "

'Private Sub objchkCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkCheckAll.CheckedChanged
'    Try
'        RemoveHandler lvChargesCount.ItemChecked, AddressOf lvChargesCount_ItemChecked
'        For Each lvItem As ListViewItem In lvChargesCount.Items
'            lvItem.Checked = objchkCheckAll.Checked
'        Next
'        AddHandler lvChargesCount.ItemChecked, AddressOf lvChargesCount_ItemChecked
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objchkCheckAll_CheckedChanged", mstrModuleName)
'    Finally
'    End Try
'End Sub

'#End Region

'#Region " Listview Event "

'Private Sub lvChargesCount_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvChargesCount.ItemChecked
'    Try
'        RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
'        If lvChargesCount.CheckedItems.Count <= 0 Then
'            objchkCheckAll.CheckState = CheckState.Unchecked
'        ElseIf lvChargesCount.CheckedItems.Count < lvChargesCount.Items.Count Then
'            objchkCheckAll.CheckState = CheckState.Indeterminate
'        ElseIf lvChargesCount.CheckedItems.Count = lvChargesCount.Items.Count Then
'            objchkCheckAll.CheckState = CheckState.Checked
'        End If
'        AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "lvChargesCount_ItemChecked", mstrModuleName)
'    Finally
'    End Try
'End Sub

'#End Region

''<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'Private Sub OtherSettings()
'    Try
'        Me.SuspendLayout()

'        Call SetLanguage()

'        Me.gbDiscipilneFiling.GradientColor = GUI._eZeeContainerHeaderBackColor
'        Me.gbDiscipilneFiling.ForeColor = GUI._eZeeContainerHeaderForeColor


'        Me.objbtnOpen.GradientBackColor = GUI._ButttonBackColor
'        Me.objbtnOpen.GradientForeColor = GUI._ButttonFontColor

'        Me.btnClose.GradientBackColor = GUI._ButttonBackColor
'        Me.btnClose.GradientForeColor = GUI._ButttonFontColor


'        Me.ResumeLayout()
'    Catch Ex As Exception
'        DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
'    End Try
'End Sub


'Private Sub SetLanguage()
'    Try
'        Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
'        Me.gbDiscipilneFiling.Text = Language._Object.getCaption(Me.gbDiscipilneFiling.Name, Me.gbDiscipilneFiling.Text)
'        Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
'        Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
'        Me.lblJobTitle.Text = Language._Object.getCaption(Me.lblJobTitle.Name, Me.lblJobTitle.Text)
'        Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.Name, Me.lblInterdictionDate.Text)
'        Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
'        Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
'        Me.colhCount.Text = Language._Object.getCaption(CStr(Me.colhCount.Tag), Me.colhCount.Text)
'        Me.colhIncident.Text = Language._Object.getCaption(CStr(Me.colhIncident.Tag), Me.colhIncident.Text)
'        Me.colhOffCategory.Text = Language._Object.getCaption(CStr(Me.colhOffCategory.Tag), Me.colhOffCategory.Text)
'        Me.colhOffence.Text = Language._Object.getCaption(CStr(Me.colhOffence.Tag), Me.colhOffence.Text)
'        Me.colhSeverity.Text = Language._Object.getCaption(CStr(Me.colhSeverity.Tag), Me.colhSeverity.Text)

'    Catch Ex As Exception
'        DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
'    End Try
'End Sub


'Private Sub SetMessages()
'    Try
'        Language.setMessage(mstrModuleName, 1, "Open Charges Externally")
'        Language.setMessage(mstrModuleName, 2, "Open Charges Internally")

'    Catch Ex As Exception
'        DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
'    End Try
'End Sub
'#End Region 'Language & UI Settings
''</Language>