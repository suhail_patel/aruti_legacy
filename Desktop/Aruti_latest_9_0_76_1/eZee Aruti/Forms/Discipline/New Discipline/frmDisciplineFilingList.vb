﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDisciplineFilingList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineAnalysisList"
    Private objDisciplineFile As clsDiscipline_File
    Private blnIsFromMail As Boolean = False
    Private mstrEmails() As String
    Private dsEmailData As New DataSet
    Private mdicDisciplineFile As New Dictionary(Of String, Integer)

#End Region

#Region " Properties "

    Public WriteOnly Property _FromMail() As Boolean
        Set(ByVal value As Boolean)
            blnIsFromMail = value
        End Set
    End Property

    Public Property _Emails() As String()
        Get
            Return mstrEmails
        End Get
        Set(ByVal value As String())
            mstrEmails = value
        End Set
    End Property

    Public Property _EmailData() As DataSet
        Get
            Return dsEmailData
        End Get
        Set(ByVal value As DataSet)
            dsEmailData = value
        End Set
    End Property

    Public Property _dicDisciplineFile() As Dictionary(Of String, Integer)
        Get
            Return mdicDisciplineFile
        End Get
        Set(ByVal value As Dictionary(Of String, Integer))
            mdicDisciplineFile = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objDType As New clsDisciplineType
        Try
            dsCombo = objDType.getComboList("List", True)
            With cboDisciplineType
                .ValueMember = "disciplinetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objDType = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim lvItem As ListViewItem
        Dim StrSearch As String = String.Empty
        Try

            If User._Object.Privilege._AllowToViewChargesProceedingList = True Then    'Pinkal (02-Jul-2012) -- Start

                'S.SANDEEP [12 MAY 2015] -- START
                'dsList = objDisciplineFile.GetList("List", True)

                'If CInt(cboDisciplineType.SelectedValue) > 0 Then
                '    StrSearch &= "AND disciplinetypeunkid = '" & CInt(cboDisciplineType.SelectedValue) & "' "
                'End If

                'If dtpDisciplineDate.Checked = True Then
                '    StrSearch &= "AND FileDate = '" & eZeeDate.convertDate(dtpDisciplineDate.Value) & "' "
                'End If

                'If txtPersonInvolved.Text.Trim.Length > 0 Then
                '    StrSearch &= "AND InvolvedPerson LIKE '%" & txtPersonInvolved.Text.Trim & "%' "
                'End If

                'If StrSearch.Trim.Length > 0 Then
                '    StrSearch = StrSearch.Substring(3)
                '    dtTable = New DataView(dsList.Tables("List"), StrSearch, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
                'End If

                If CInt(cboDisciplineType.SelectedValue) > 0 Then
                    StrSearch &= "AND hrdiscipline_file.disciplinetypeunkid = '" & CInt(cboDisciplineType.SelectedValue) & "' "
                End If

                If dtpDisciplineDate.Checked = True Then
                    StrSearch &= "AND CONVERT(CHAR(8),hrdiscipline_file.trandate,112) = '" & eZeeDate.convertDate(dtpDisciplineDate.Value) & "' "
                End If

                If txtPersonInvolved.Text.Trim.Length > 0 Then
                    StrSearch &= "AND ISNULL(involved.firstname+' '+involved.surname+' '+involved.othername,'') LIKE '%" & txtPersonInvolved.Text.Trim & "%' "
                End If

                If StrSearch.Trim.Length > 0 Then
                    StrSearch = StrSearch.Substring(3)
                End If

                dsList = objDisciplineFile.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , StrSearch)

                dtTable = dsList.Tables(0)
                'S.SANDEEP [12 MAY 2015] -- END

                

                lvDisciplineInfo.Items.Clear()

                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Tag = dtRow.Item("disciplinefileunkid").ToString
                    lvItem.Text = ""
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("FileDate").ToString).ToShortDateString)                    'Date
                    lvItem.SubItems.Add(dtRow.Item("DisciplineType").ToString)                                                      'DisciplineType
                    lvItem.SubItems.Add(dtRow.Item("Severity").ToString)                                                            'Discipline Severity
                    lvItem.SubItems.Add(dtRow.Item("InvolvedPersoncode").ToString & " - " & dtRow.Item("InvolvedPerson").ToString)  'Involved Person
                    lvItem.SubItems(colhPersonInvolved.Index).Tag = dtRow.Item("involved_employeeunkid")
                    lvItem.SubItems.Add(dtRow.Item("incident").ToString)                                                            'Incident
                    lvItem.SubItems.Add(dtRow.Item("involved_employeeunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("against_employeeunkid").ToString)

                    'S.SANDEEP [ 20 MARCH 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                    'If CBool(dtRow.Item("isapproved")) = True Then
                    '    lvItem.ForeColor = Color.Blue
                    'End If

                    If CInt(dtRow.Item("disciplinestatusunkid")) = 1 Then
                        lvItem.ForeColor = Color.SteelBlue
                    End If
                    'S.SANDEEP [ 20 MARCH 2012 ] -- END

                    'S.SANDEEP [ 20 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dtRow.Item("Category").ToString)
                    lvItem.SubItems.Add(dtRow.Item("CategoryId").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Email").ToString)
                    'S.SANDEEP [ 20 APRIL 2012 ] -- END

                    lvDisciplineInfo.Items.Add(lvItem)
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            If dsList.Tables.Count > 0 Then
                dsList.Dispose() : dtTable.Dispose() : lvItem = Nothing : StrSearch = String.Empty
            End If
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnAdd.Enabled = User._Object.Privilege._AddEmployeeDiscipline
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeDiscipline
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeDiscipline
            mnuInAddResolutionSteps.Enabled = User._Object.Privilege._AllowToAddResolutionStep
            mnuExemptedHeadsList.Enabled = User._Object.Privilege._AllowToViewDisciplineExemption
            mnuExemptHead.Enabled = User._Object.Privilege._AllowToAddDiscipileExemption
            mnuPostedHeadsList.Enabled = User._Object.Privilege._AllowToViewDisciplinePosting
            mnuPostTransHead.Enabled = User._Object.Privilege._AllowToAddDisciplinePosting

            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mnuCloseCase.Enabled = User._Object.Privilege._AllowtoCloseCase
            mnuInReopen.Enabled = User._Object.Privilege._AllowtoReOpenCase
            'Anjan (17 Apr 2012)-End 

            'S.SANDEEP [ 19 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            mnuExAddResolutionSteps.Enabled = User._Object.Privilege._AllowToAddResolutionStep
            mnuExReopen.Enabled = User._Object.Privilege._AllowtoReOpenCase
            'S.SANDEEP [ 19 MAY 2012 ] -- END

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuScanDocuments.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            mnuPreview.Enabled = User._Object.Privilege._AllowToViewChargesProceedingList
            mnuPrint.Enabled = User._Object.Privilege._AllowToViewChargesProceedingList
            'Anjan (25 Oct 2012)-End 


            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                mnuExemptHead.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                mnuPostTransHead.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                objSep2.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                mnuViewDisciplineHeads.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                objSep6.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmDisciplineFilingList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDisciplineFile = New clsDiscipline_File
        Try
            Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()

            If blnIsFromMail = True Then
                objcolhCheck.Width = 25
                objchkAll.Visible = True
                objFooter.Visible = False
                objemailFooter.Visible = True
                lvDisciplineInfo.CheckBoxes = True
            Else
                objcolhCheck.Width = 0
                objchkAll.Visible = False
                objFooter.Visible = True
                objemailFooter.Visible = False
                lvDisciplineInfo.CheckBoxes = False
            End If

            FillCombo()
            FillList()
            If lvDisciplineInfo.Items.Count > 0 Then lvDisciplineInfo.Items(0).Selected = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFilingList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmDisciplineFilingList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFilingList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineFilingList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If btnDelete.Enabled = True Then
                If e.Control = True And e.KeyCode = Keys.Delete And lvDisciplineInfo.Focused = True Then
                    Call btnDelete.PerformClick()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFilingList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineFilingList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objDisciplineFile = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_File.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_File"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim frm As New frmDisciplineFiling
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmDisciplineFiling
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvDisciplineInfo.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Discipline?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objDisciplineFile._Isvoid = True
                objDisciplineFile._Voiduserunkid = User._Object._Userunkid
                objDisciplineFile._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objDisciplineFile._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objDisciplineFile.Delete(CInt(lvDisciplineInfo.SelectedItems(0).Tag))

                If objDisciplineFile._Message <> "" Then
                    eZeeMsgBox.Show(objDisciplineFile._Message, enMsgBoxStyle.Information)
                Else
                    lvDisciplineInfo.SelectedItems(0).Remove()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboDisciplineType.SelectedValue = 0
            dtpDisciplineDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDisciplineDate.Checked = False
            txtPersonInvolved.Text = ""

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If lvDisciplineInfo.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please check atleast one disciplinary case to send email."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim strNumber As String = ""
            Dim objLetterFields As New clsLetterFields
            mstrEmails = New String() {}
            Dim blnFlag As Boolean = False
            Dim intCnt As Integer = 0
            ReDim mstrEmails(lvDisciplineInfo.CheckedItems.Count)

            For Each lvItem As ListViewItem In lvDisciplineInfo.CheckedItems
                If lvItem.SubItems(objcolhEmailId.Index).Text.Trim.Length <= 0 Then
                    If blnFlag = False Then
                        Dim strMsg As String = Language.getMessage(mstrModuleName, 11, "Some of the email address(s) are blank.And will not added to the list. Do you want to continue?")
                        If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            blnFlag = True
                            Continue For
                        Else
                            blnFlag = False
                            Exit Sub
                        End If
                    End If
                Else
                    mstrEmails(intCnt) = lvItem.SubItems(colhPersonInvolved.Index).Text & " " & "<" & lvItem.SubItems(objcolhEmailId.Index).Text & ">"
                    strNumber &= "," & lvItem.Tag.ToString
                    intCnt += 1
                    If mdicDisciplineFile.ContainsKey(lvItem.SubItems(objcolhInvolvedEmpId.Index).Text & "|" & lvItem.Tag.ToString) = False Then
                        mdicDisciplineFile.Add(lvItem.SubItems(objcolhInvolvedEmpId.Index).Text & "|" & lvItem.Tag.ToString, CInt(lvItem.Tag))
                    End If
                End If
            Next

            If strNumber <> "" Then
                strNumber = Mid(strNumber, 2)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsEmailData = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Discipline_Module)
                dsEmailData = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Discipline_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                'S.SANDEEP [04 JUN 2015] -- END

            End If
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Controls Events "

    Private Sub lvDisciplineInfo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvDisciplineInfo.SelectedIndexChanged
        Try
            If lvDisciplineInfo.SelectedItems.Count > 0 Then
                If lvDisciplineInfo.SelectedItems(0).ForeColor = Color.SteelBlue Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                    mnuInAddResolutionSteps.Enabled = False
                    'S.SANDEEP [ 20 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    mnuExAddResolutionSteps.Enabled = False
                    mnuExReopen.Enabled = True
                    'S.SANDEEP [ 20 APRIL 2012 ] -- END
                    mnuInReopen.Enabled = True
                    mnuCloseCase.Enabled = False
                Else
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                    mnuInAddResolutionSteps.Enabled = True
                    'S.SANDEEP [ 20 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    mnuExReopen.Enabled = False
                    mnuExAddResolutionSteps.Enabled = True
                    'S.SANDEEP [ 20 APRIL 2012 ] -- END
                    mnuInReopen.Enabled = False
                    mnuCloseCase.Enabled = True
                    Call SetVisibility()
                End If
            Else
                'S.SANDEEP [ 19 MAY 2012 ] -- START
                'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                Call SetVisibility()
                'S.SANDEEP [ 19 MAY 2012 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDisciplineInfo_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuScanDocuments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuScanDocuments.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuScanDocuments_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 18 FEB 2012 ] -- END

    Private Sub mnuExemptHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExemptHead.Click
        Dim frm As New frmExemptDisciplineHeads
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(CInt(lvDisciplineInfo.SelectedItems(0).Tag), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhPersonInvolved.Index).Text, _
                              CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhIncident.Index).Text, _
                              enAction.ADD_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExemptHead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuPostTransHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPostTransHead.Click
        Dim frm As New frmPostDisciplineHeads
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(CInt(lvDisciplineInfo.SelectedItems(0).Tag), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhPersonInvolved.Index).Text, _
                              CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhIncident.Index).Text, _
                              enAction.ADD_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPostTransHead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuExemptedHeadsList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExemptedHeadsList.Click
        Dim frm As New frmExemptDisciplinaryHeadsList
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(CInt(lvDisciplineInfo.SelectedItems(0).Tag))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewDisciplineHeads_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuPostedHeadsList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPostedHeadsList.Click
        Dim frm As New frmPostDisciplineHeadsList
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(CInt(lvDisciplineInfo.SelectedItems(0).Tag))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPostedHeadsList_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuAddResolutionSteps_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuInAddResolutionSteps.Click
        Dim frm As New frmDisciplineResolution
        Try

            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            If CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhCategoryId.Index).Text) = 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot add the internal resolution step. Reason : This case is in External Category."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objResoution As New clsDiscipline_Resolution
            Dim intValue As Integer = -1

            intValue = objResoution.Is_Add_Step_Allowed(CInt(lvDisciplineInfo.SelectedItems(0).Tag))

            Select Case intValue
                Case 4 'UN-APPROVED FOUND WITH STATUS AS (CLOSED)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot add Resolution Step. Reason : There is already as step with CASE CLOSED status, which is in pending approval state."), enMsgBoxStyle.Information)
                    Exit Sub
                Case 5 'UN-APPROVED FOUND WITH STATUS AS (RE-OPENED)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot add Resolution Step. Reason : There is already as step with RE-OPENED status, which is in pending approval state."), enMsgBoxStyle.Information)
                    Exit Sub
            End Select


            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(-1, _
                              CInt(lvDisciplineInfo.SelectedItems(0).Tag), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhPersonInvolved.Index).Text, _
                              CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhIncident.Index).Text, _
                              enAction.ADD_ONE, clsDiscipline_Status.enDisciplineStatusType.INTERNAL)

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAddResolutionSteps_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuExAddResolutionSteps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExAddResolutionSteps.Click
        Dim frm As New frmDisciplineResolution
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            If CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhCategoryId.Index).Text) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add the external resolution step. Reason : this case is in Internal Category"), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objResoution As New clsDiscipline_Resolution
            Dim intValue As Integer = -1

            intValue = objResoution.Is_Add_Step_Allowed(CInt(lvDisciplineInfo.SelectedItems(0).Tag))

            Select Case intValue
                Case 4 'UN-APPROVED FOUND WITH STATUS AS (CLOSED)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot add Resolution Step. Reason : There is already as step with CASE CLOSED status, which is in pending approval state."), enMsgBoxStyle.Information)
                    Exit Sub
                Case 5 'UN-APPROVED FOUND WITH STATUS AS (RE-OPENED)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot add Resolution Step. Reason : There is already as step with RE-OPENED status, which is in pending approval state."), enMsgBoxStyle.Information)
                    Exit Sub
            End Select


            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(-1, _
                              CInt(lvDisciplineInfo.SelectedItems(0).Tag), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhPersonInvolved.Index).Text, _
                              CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhIncident.Index).Text, _
                              enAction.ADD_ONE, clsDiscipline_Status.enDisciplineStatusType.EXTERNAL)

            Call FillList()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExAddResolutionSteps_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuCloseCase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCloseCase.Click
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            Dim objResoution As New clsDiscipline_Resolution
            Dim intValue As Integer = -1

            intValue = objResoution.IsCase_Can_Close(CInt(lvDisciplineInfo.SelectedItems(0).Tag))

            Select Case intValue
                Case 1  'NO DATA PRESENT
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot do this operation. Reason : No data present for this Incident."), enMsgBoxStyle.Information)
                    Exit Sub
                Case 2  'UNAPPROVED DATA FOUND
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot do this operation. Reason : There are some unapproved resolution steps for this Incident."), enMsgBoxStyle.Information)
                    Exit Sub
                Case 3  'NO UNAPPROVED DATA FOUND

                    intValue = objResoution.Is_Add_Step_Allowed(CInt(lvDisciplineInfo.SelectedItems(0).Tag))
                    Select Case intValue
                        Case 3
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You cannot do this operation. Reason : No data present for this Incident after it is RE-OPENED."), enMsgBoxStyle.Information)
                            Exit Sub
                        Case Else
                            Dim frm As New frmDisciplineResolution

                            If User._Object._Isrighttoleft = True Then
                                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                                frm.RightToLeftLayout = True
                                Call Language.ctlRightToLeftlayOut(frm)
                            End If

                            frm.displayDialog(-1, _
                                     CInt(lvDisciplineInfo.SelectedItems(0).Tag), _
                                     lvDisciplineInfo.SelectedItems(0).SubItems(colhPersonInvolved.Index).Text, _
                                     CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                                     lvDisciplineInfo.SelectedItems(0).SubItems(colhIncident.Index).Text, _
                                     enAction.ADD_ONE, CType(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhCategoryId.Index).Text, clsDiscipline_Status.enDisciplineStatusType), 1)
                            Call FillList()
                    End Select
            End Select


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCloseCase_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuReopen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuInReopen.Click
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If

            Dim objResoution As New clsDiscipline_Resolution
            Dim intValue As Integer = -1
            intValue = objResoution.Is_Add_Step_Allowed(CInt(lvDisciplineInfo.SelectedItems(0).Tag))
            Select Case intValue
                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Case 1
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot do this operation. Reason : No data present for this Incident."), enMsgBoxStyle.Information)
                    Exit Sub
                Case 3
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot do this operation. Reason : This case is already in Re-Open State."), enMsgBoxStyle.Information)
                    Exit Sub
                    'S.SANDEEP [ 20 APRIL 2012 ] -- END
                Case 5 'UN-APPROVED FOUND WITH STATUS AS (RE-OPENED)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot RE-OPEN this incident. Reason : There is already as step with RE-OPENED status, which is in pending approval state."), enMsgBoxStyle.Information)
                    Exit Sub
            End Select

            Dim frm As New frmDisciplineResolution

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, _
                              CInt(lvDisciplineInfo.SelectedItems(0).Tag), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhPersonInvolved.Index).Text, _
                              CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhIncident.Index).Text, _
                              enAction.ADD_ONE, clsDiscipline_Status.enDisciplineStatusType.INTERNAL, 3)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuReopen_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuExReopen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExReopen.Click
        Try
            'S.SANDEEP [ 19 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If
            'S.SANDEEP [ 19 MAY 2012 ] -- END

            Dim objResoution As New clsDiscipline_Resolution
            Dim intValue As Integer = -1
            intValue = objResoution.Is_Add_Step_Allowed(CInt(lvDisciplineInfo.SelectedItems(0).Tag))
            Select Case intValue
                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Case 1
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot do this operation. Reason : No data present for this Incident."), enMsgBoxStyle.Information)
                    Exit Sub
                Case 3
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot do this operation. Reason : This case is already in Re-Open State."), enMsgBoxStyle.Information)
                    Exit Sub
                    'S.SANDEEP [ 20 APRIL 2012 ] -- END
                Case 5 'UN-APPROVED FOUND WITH STATUS AS (RE-OPENED)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot RE-OPEN this incident. Reason : There is already as step with RE-OPENED status, which is in pending approval state."), enMsgBoxStyle.Information)
                    Exit Sub
            End Select

            Dim frm As New frmDisciplineResolution

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, _
                              CInt(lvDisciplineInfo.SelectedItems(0).Tag), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhPersonInvolved.Index).Text, _
                              CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                              lvDisciplineInfo.SelectedItems(0).SubItems(colhIncident.Index).Text, _
                              enAction.ADD_ONE, clsDiscipline_Status.enDisciplineStatusType.EXTERNAL, 3)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExReopen_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 20 APRIL 2012 ] -- END


    'S.SANDEEP [ 01 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA DISCIPLINE CHANGES
    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If
            Dim objDiscipline_Charge As New ArutiReports.clsDiscipline_ChargesReport
            objDiscipline_Charge._EmployeeId = CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text)
            objDiscipline_Charge.generateReport(0, enPrintAction.Print, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrint_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreview.Click
        Try
            If lvDisciplineInfo.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineInfo.Select()
                Exit Sub
            End If
            Dim objDiscipline_Charge As New ArutiReports.clsDiscipline_ChargesReport
            objDiscipline_Charge._EmployeeId = CInt(lvDisciplineInfo.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text)
            objDiscipline_Charge.generateReport(0, enPrintAction.Preview, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreview_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        RemoveHandler lvDisciplineInfo.ItemChecked, AddressOf lvDisciplineInfo_ItemChecked
        For Each lvItem As ListViewItem In lvDisciplineInfo.Items
            lvItem.Checked = CBool(objchkAll.CheckState)
        Next
        AddHandler lvDisciplineInfo.ItemChecked, AddressOf lvDisciplineInfo_ItemChecked
    End Sub

    Private Sub lvDisciplineInfo_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDisciplineInfo.ItemChecked
        RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        If lvDisciplineInfo.CheckedItems.Count <= 0 Then
            objchkAll.CheckState = CheckState.Unchecked
        ElseIf lvDisciplineInfo.CheckedItems.Count < lvDisciplineInfo.Items.Count Then
            objchkAll.CheckState = CheckState.Indeterminate
        ElseIf lvDisciplineInfo.CheckedItems.Count = lvDisciplineInfo.Items.Count Then
            objchkAll.CheckState = CheckState.Checked
        End If
        AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
    End Sub
    'S.SANDEEP [ 01 JUNE 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnEClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPersonInvolved.Text = Language._Object.getCaption(Me.lblPersonInvolved.Name, Me.lblPersonInvolved.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.Name, Me.lblDisciplineType.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhDisciplineType.Text = Language._Object.getCaption(CStr(Me.colhDisciplineType.Tag), Me.colhDisciplineType.Text)
			Me.colhSeverity.Text = Language._Object.getCaption(CStr(Me.colhSeverity.Tag), Me.colhSeverity.Text)
			Me.colhPersonInvolved.Text = Language._Object.getCaption(CStr(Me.colhPersonInvolved.Tag), Me.colhPersonInvolved.Text)
			Me.colhIncident.Text = Language._Object.getCaption(CStr(Me.colhIncident.Tag), Me.colhIncident.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.mnuScanDocuments.Text = Language._Object.getCaption(Me.mnuScanDocuments.Name, Me.mnuScanDocuments.Text)
			Me.mnuInAddResolutionSteps.Text = Language._Object.getCaption(Me.mnuInAddResolutionSteps.Name, Me.mnuInAddResolutionSteps.Text)
			Me.mnuExemptHead.Text = Language._Object.getCaption(Me.mnuExemptHead.Name, Me.mnuExemptHead.Text)
			Me.mnuPostTransHead.Text = Language._Object.getCaption(Me.mnuPostTransHead.Name, Me.mnuPostTransHead.Text)
			Me.mnuViewDisciplineHeads.Text = Language._Object.getCaption(Me.mnuViewDisciplineHeads.Name, Me.mnuViewDisciplineHeads.Text)
			Me.mnuExemptedHeadsList.Text = Language._Object.getCaption(Me.mnuExemptedHeadsList.Name, Me.mnuExemptedHeadsList.Text)
			Me.mnuPostedHeadsList.Text = Language._Object.getCaption(Me.mnuPostedHeadsList.Name, Me.mnuPostedHeadsList.Text)
			Me.mnuCloseCase.Text = Language._Object.getCaption(Me.mnuCloseCase.Name, Me.mnuCloseCase.Text)
			Me.mnuInReopen.Text = Language._Object.getCaption(Me.mnuInReopen.Name, Me.mnuInReopen.Text)
			Me.colhCategory.Text = Language._Object.getCaption(CStr(Me.colhCategory.Tag), Me.colhCategory.Text)
			Me.mnuExAddResolutionSteps.Text = Language._Object.getCaption(Me.mnuExAddResolutionSteps.Name, Me.mnuExAddResolutionSteps.Text)
			Me.mnuExReopen.Text = Language._Object.getCaption(Me.mnuExReopen.Name, Me.mnuExReopen.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnEClose.Text = Language._Object.getCaption(Me.btnEClose.Name, Me.btnEClose.Text)
			Me.mnuDisciplineCharge.Text = Language._Object.getCaption(Me.mnuDisciplineCharge.Name, Me.mnuDisciplineCharge.Text)
			Me.mnuPrint.Text = Language._Object.getCaption(Me.mnuPrint.Name, Me.mnuPrint.Text)
			Me.mnuPreview.Text = Language._Object.getCaption(Me.mnuPreview.Name, Me.mnuPreview.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Discipline from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Discipline?")
			Language.setMessage(mstrModuleName, 3, "Select Employee")
			Language.setMessage(mstrModuleName, 4, "Sorry, You cannot do this operation. Reason : No data present for this Incident.")
			Language.setMessage(mstrModuleName, 5, "Sorry, You cannot do this operation. Reason : There are some unapproved resolution steps for this Incident.")
			Language.setMessage(mstrModuleName, 6, "Sorry, You cannot add Resolution Step. Reason : There is already as step with CASE CLOSED status, which is in pending approval state.")
			Language.setMessage(mstrModuleName, 7, "Sorry, You cannot add Resolution Step. Reason : There is already as step with RE-OPENED status, which is in pending approval state.")
			Language.setMessage(mstrModuleName, 8, "Sorry, You cannot do this operation. Reason : No data present for this Incident after it is RE-OPENED.")
			Language.setMessage(mstrModuleName, 9, "Sorry, You cannot RE-OPEN this incident. Reason : There is already as step with RE-OPENED status, which is in pending approval state.")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot add the external resolution step. Reason : this case is in Internal Category")
			Language.setMessage(mstrModuleName, 11, "Some of the email address(s) are blank.And will not added to the list. Do you want to continue?")
			Language.setMessage(mstrModuleName, 12, "Sorry, You cannot do this operation. Reason : This case is already in Re-Open State.")
			Language.setMessage(mstrModuleName, 13, "Please check atleast one disciplinary case to send email.")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot add the internal resolution step. Reason : This case is in External Category.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class