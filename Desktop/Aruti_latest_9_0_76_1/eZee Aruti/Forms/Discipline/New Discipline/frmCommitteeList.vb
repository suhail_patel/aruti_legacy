﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCommitteeList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCommitteeList"
    Private objCommittee As clsDiscipline_Committee

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCMaster As New clsCommon_Master
        Dim objEMaster As New clsEmployee_Master
        Try
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
            With cboCommitteMaster
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEMaster.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEMaster.GetEmployeeList("List", True)
            'End If
            dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMMITTEE_MEMBERS_CATEGORY, True, "List")
            With cboMCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 19 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Dim StrSearch As String = String.Empty
        Dim dTable As DataTable
        Try

            If User._Object.Privilege._AllowToViewDisciplinaryCommitteeList = True Then                'Pinkal (02-Jul-2012) -- Start



                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objCommittee.GetList
                dsList = objCommittee.GetList(Company._Object._Name)
                'Shani(24-Aug-2015) -- End

            lvCommitteMember.Items.Clear()

            If CInt(cboCommitteMaster.SelectedValue) > 0 Then
                StrSearch &= "AND committeemasterunkid ='" & CInt(cboCommitteMaster.SelectedValue) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearch &= "AND employeeunkid ='" & CInt(cboEmployee.SelectedValue) & "' "
            End If

                'S.SANDEEP [ 19 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboMCategory.SelectedValue) > 0 Then
                    StrSearch &= "AND mcategoryunkid ='" & CInt(cboMCategory.SelectedValue) & "' "
                End If
                'S.SANDEEP [ 19 DEC 2012 ] -- END

            If radExternal.Checked = True Then
                StrSearch &= "AND MTypeId = '1' "
            ElseIf radInternal.Checked = True Then
                StrSearch &= "AND MTypeId = '2' "
            End If

            If txtName.Text.Trim.Length > 0 Then
                StrSearch &= "AND ex_name LIKE '%" & txtName.Text.Trim & "%' "
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                dTable = New DataView(dsList.Tables(0), StrSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            For Each dRow As DataRow In dTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("MType").ToString
                If CInt(dRow.Item("employeeunkid")) > 0 Then
                    lvItem.SubItems.Add(dRow.Item("employee_name").ToString)
                    lvItem.SubItems.Add(dRow.Item("company").ToString)
                    lvItem.SubItems.Add(dRow.Item("department").ToString)
                    lvItem.SubItems.Add(dRow.Item("contact").ToString)
                Else
                    lvItem.SubItems.Add(dRow.Item("ex_name").ToString)
                    lvItem.SubItems.Add(dRow.Item("ex_company").ToString)
                    lvItem.SubItems.Add(dRow.Item("ex_department").ToString)
                    lvItem.SubItems.Add(dRow.Item("ex_contactno").ToString)
                End If
                lvItem.SubItems.Add(dRow.Item("committee").ToString)
                lvItem.SubItems.Add(dRow.Item("committeemasterunkid").ToString)
                    'S.SANDEEP [ 19 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dRow.Item("mcategoryname").ToString)
                    'S.SANDEEP [ 19 DEC 2012 ] -- END

                    'S.SANDEEP [24 MAY 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    lvItem.SubItems.Add(dRow.Item("ex_email").ToString)
                    'S.SANDEEP [24 MAY 2016] -- END

                lvItem.Tag = dRow.Item("committeetranunkid")

                lvCommitteMember.Items.Add(lvItem)
            Next

            lvCommitteMember.GridLines = False
            lvCommitteMember.GroupingColumn = objcolhCommitte
            lvCommitteMember.DisplayGroups(True)

            If lvCommitteMember.Items.Count > 2 Then
                colhEmployee.Width = 180 - 20
            Else
                colhEmployee.Width = 180
            End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges
    Private Sub SetVisibility()

        Try

            btnAdd.Enabled = User._Object.Privilege._AllowToAddDisciplinaryCommittee
            btnEdit.Enabled = User._Object.Privilege._AllowToEditDeleteDisciplinaryCommittee

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Varsha Rana (17-Oct-2017) -- End

#End Region

#Region " Form's Events "

    Private Sub frmCommitteeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCommittee = New clsDiscipline_Committee
        Try
            Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommitteeList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCommitteeList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommitteeList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCommitteeList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If btnDelete.Enabled = True Then
                If e.Control = True And e.KeyCode = Keys.Delete And lvCommitteMember.Focused = True Then
                    Call btnDelete.PerformClick()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFilingList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineFilingList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCommittee = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Committee.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Committee"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCommitteMaster.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            radAll.Checked = True
            radExternal.Checked = False
            radInternal.Checked = False
            txtName.Text = ""
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim frm As New frmCommittee_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Fill_List()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmCommittee_AddEdit
        Try
            If lvCommitteMember.SelectedItems.Count > 0 Then
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                If frm.displayDialog(CInt(lvCommitteMember.SelectedItems(0).SubItems(objcolhMasterId.Index).Text), enAction.EDIT_ONE) Then
                    Fill_List()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchCommittee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCommittee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboCommitteMaster.DisplayMember
                .ValueMember = cboCommitteMaster.ValueMember
                .DataSource = CType(cboCommitteMaster.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboCommitteMaster.SelectedValue = frm.SelectedValue
                cboCommitteMaster.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCommittee_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 19 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchMCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMCategory.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboMCategory.DisplayMember
                .ValueMember = cboMCategory.ValueMember
                .DataSource = CType(cboMCategory.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboMCategory.SelectedValue = frm.SelectedValue
                cboMCategory.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchMCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 19 DEC 2012 ] -- END

#End Region

    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
#Region " RadioButton's Events "
    Private Sub radExternal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radExternal.CheckedChanged
        Try
            If radExternal.Checked = True Then
                cboEmployee.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExternal_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'S.SANDEEP [24 MAY 2016] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhType.Text = Language._Object.getCaption(CStr(Me.colhType.Tag), Me.colhType.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
			Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
			Me.colhContactNo.Text = Language._Object.getCaption(CStr(Me.colhContactNo.Tag), Me.colhContactNo.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblCommitteMaster.Text = Language._Object.getCaption(Me.lblCommitteMaster.Name, Me.lblCommitteMaster.Text)
			Me.radAll.Text = Language._Object.getCaption(Me.radAll.Name, Me.radAll.Text)
			Me.radExternal.Text = Language._Object.getCaption(Me.radExternal.Name, Me.radExternal.Text)
			Me.radInternal.Text = Language._Object.getCaption(Me.radInternal.Name, Me.radInternal.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblMembersCategory.Text = Language._Object.getCaption(Me.lblMembersCategory.Name, Me.lblMembersCategory.Text)
			Me.colhMCategory.Text = Language._Object.getCaption(CStr(Me.colhMCategory.Tag), Me.colhMCategory.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class