﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDisciplineResolutionList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineResolutionList"
    Private objDResolution As clsDiscipline_Resolution
    Private mblnIsFromMail As Boolean = False

#End Region

#Region " Property "

    Public Property _IsFromMail() As Boolean
        Get
            Return mblnIsFromMail
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromMail = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objDType As New clsDisciplineType
        Dim objDStatus As New clsDiscipline_Status
        Dim objDAction As New clsAction_Reason
        Try
            dsCombo = objDType.getComboList("List", True)
            With cboDisciplineType
                .ValueMember = "disciplinetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objDAction.getComboList("List", True, True)
            With cboDisciplineAction
                .ValueMember = "actionreasonunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objDStatus.getComboList("List", True)
            With cboDisciplineStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            cboApprovedStatus.Items.Clear()
            With cboApprovedStatus
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
                .Items.Add(Language.getMessage("clsDiscipline_Resolution", 1, "Approved"))
                .Items.Add(Language.getMessage("clsDiscipline_Resolution", 2, "Pending"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objDAction = Nothing : objDStatus = Nothing : objDStatus = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim StrSearch As String = String.Empty
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewProceedingApprovalList = True Then                'Pinkal (02-Jul-2012) -- Start

                'S.SANDEEP [12 MAY 2015] -- START
                'dsList = objDResolution.GetList("List", True)

                'If CInt(cboApprovedStatus.SelectedIndex) > 0 Then
                '    Select Case CInt(cboApprovedStatus.SelectedIndex)
                '        Case 1  'APPROVED
                '            StrSearch &= "AND isapproved = 1 "
                '        Case 2  'PENDING
                '            StrSearch &= "AND isapproved = 0 "
                '    End Select
                'End If

                'If CInt(cboDisciplineAction.SelectedValue) > 0 Then
                '    StrSearch &= "AND disciplinaryactionunkid = '" & CInt(cboDisciplineAction.SelectedValue) & "' "
                'End If

                'If CInt(cboDisciplineStatus.SelectedValue) > 0 Then
                '    StrSearch &= "AND disciplinestatusunkid = '" & CInt(cboDisciplineStatus.SelectedValue) & "' "
                'End If

                'If CInt(cboDisciplineType.SelectedValue) > 0 Then
                '    StrSearch &= "AND disciplinetypeunkid = '" & CInt(cboDisciplineType.SelectedValue) & "' "
                'End If

                'If dtpDisciplineDate.Checked = True Then
                '    StrSearch &= "AND RDate = '" & eZeeDate.convertDate(dtpDisciplineDate.Value) & "' "
                'End If

                'If txtPersonInvolved.Text.Trim.Length > 0 Then
                '    StrSearch &= "AND InvolvedEmployee LIKE '%" & txtPersonInvolved.Text.Trim & "%'"
                'End If

                'If StrSearch.Trim.Length > 0 Then
                '    StrSearch = StrSearch.Substring(3)
                '    dtTable = New DataView(dsList.Tables("List"), StrSearch, "InvolvedEmployee,resolutionunkid,disciplinefileunkid", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsList.Tables("List"), "", "InvolvedEmployee,resolutionunkid,disciplinefileunkid", DataViewRowState.CurrentRows).ToTable
                'End If

            If CInt(cboApprovedStatus.SelectedIndex) > 0 Then
                Select Case CInt(cboApprovedStatus.SelectedIndex)
                    Case 1  'APPROVED
                            StrSearch &= "AND ISNULL(hrdiscipline_resolutions.isapproved,0) = 1 "
                    Case 2  'PENDING
                            StrSearch &= "AND ISNULL(hrdiscipline_resolutions.isapproved,0) = 0 "
                End Select
            End If

            If CInt(cboDisciplineAction.SelectedValue) > 0 Then
                    StrSearch &= "AND hrdiscipline_resolutions.disciplinaryactionunkid = '" & CInt(cboDisciplineAction.SelectedValue) & "' "
            End If

            If CInt(cboDisciplineStatus.SelectedValue) > 0 Then
                    StrSearch &= "AND hrdiscipline_resolutions.disciplinestatusunkid = '" & CInt(cboDisciplineStatus.SelectedValue) & "' "
            End If

            If CInt(cboDisciplineType.SelectedValue) > 0 Then
                    StrSearch &= "AND hrdiscipline_file.disciplinetypeunkid = '" & CInt(cboDisciplineType.SelectedValue) & "' "
            End If

            If dtpDisciplineDate.Checked = True Then
                    StrSearch &= "AND CONVERT(CHAR(8),hrdiscipline_resolutions.trandate,112) = '" & eZeeDate.convertDate(dtpDisciplineDate.Value) & "' "
            End If

            If txtPersonInvolved.Text.Trim.Length > 0 Then
                    StrSearch &= "AND ISNULL(Involved.firstname,'')+' '+ISNULL(Involved.othername,'')+' '+ISNULL(Involved.surname,'') LIKE '%" & txtPersonInvolved.Text.Trim & "%'"
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                End If

                dsList = objDResolution.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, StrSearch)

                dtTable = New DataView(dsList.Tables("List"), "", "InvolvedEmployee,resolutionunkid,disciplinefileunkid", DataViewRowState.CurrentRows).ToTable

                'S.SANDEEP [12 MAY 2015] -- END

            lvDisciplineResolutions.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("RDate").ToString).ToShortDateString)                           'DATE
                lvItem.SubItems.Add(dtRow.Item("DAction").ToString)                                                                 'DISCIPLINARY ACTION
                lvItem.SubItems.Add(dtRow.Item("DStatus").ToString)                                                                 'DISCIPLINE STATUS
                lvItem.SubItems.Add(dtRow.Item("resolutionstep").ToString)                                                          'RESOLUTION STEP
                lvItem.SubItems.Add(dtRow.Item("AStatus").ToString)                                                                 'APPROVED STATUS
                lvItem.SubItems.Add(dtRow.Item("InvolvedEmployeecode").ToString & " - " & dtRow.Item("InvolvedEmployee").ToString)  'INVOLVED EMPLOYEE
                lvItem.SubItems.Add(dtRow.Item("disciplinefileunkid").ToString)                                                     'FILE UNKID
                lvItem.SubItems.Add(dtRow.Item("incident").ToString)                                                                'INCIDENT
                lvItem.SubItems.Add(dtRow.Item("involved_employeeunkid").ToString)                                                  'INVOLVED EMPLOYEE UNKID
                lvItem.SubItems.Add(dtRow.Item("isapproved").ToString)                                                              'ISAPPROVED
                lvItem.SubItems.Add(dtRow.Item("disciplinaryactionunkid").ToString)                                                 'DISCIPLINARY ACTION UNKID
                lvItem.SubItems.Add(dtRow.Item("disciplinestatusunkid").ToString)                                                   'DISCIPLINE STATUS UNKID

                lvItem.Tag = dtRow.Item("resolutionunkid")

                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                If CInt(dtRow.Item("final_statusId")) = 1 Then
                    lvItem.ForeColor = Color.SteelBlue
                ElseIf CBool(dtRow.Item("isapproved")) = True Then
                    lvItem.ForeColor = Color.SeaGreen
                End If
                'S.SANDEEP [ 20 MARCH 2012 ] -- END


                lvDisciplineResolutions.Items.Add(lvItem)
            Next

            If lvDisciplineResolutions.Items.Count >= 4 Then
                colhRSteps.Width = 240 - 20
            Else
                colhRSteps.Width = 240
            End If

            lvDisciplineResolutions.GridLines = False
            lvDisciplineResolutions.GroupingColumn = objcolhPersonInvolved
            lvDisciplineResolutions.DisplayGroups(True)

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
            If dsList.Tables.Count > 0 Then
            dsList.Dispose() : dtTable.Dispose() : lvItem = Nothing : StrSearch = String.Empty
            End If
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnApprove.Enabled = User._Object.Privilege._AllowToApproveResolutionStep
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteResolutionStep
            btnEdit.Enabled = User._Object.Privilege._AllowToEditResolutionStep
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmDisciplineResolutionList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDResolution = New clsDiscipline_Resolution
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            If mblnIsFromMail = False Then
                objChkAll.Visible = False
                objcolhCheck.Width = 0
                colhRSteps.Width = colhRSteps.Width + 25
            Else
                objChkAll.Visible = True
                objcolhCheck.Width = 25
            End If

            FillCombo()
            Fill_List()
            If lvDisciplineResolutions.Items.Count > 0 Then lvDisciplineResolutions.Items(0).Selected = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineResolutionList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineResolutionList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineResolutionList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineResolutionList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If btnDelete.Enabled = False Then Exit Sub
            If e.Control = True And e.KeyCode = Keys.Delete And lvDisciplineResolutions.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineResolutionList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineResolutionList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objDResolution = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Resolution.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Resolution"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmDisciplineResolution
        Try

            If lvDisciplineResolutions.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Resolution Steps from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineResolutions.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvDisciplineResolutions.SelectedItems(0).Tag), _
                                 CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhFileUnkid.Index).Text), _
                                 lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhPersonInvolved.Index).Text, _
                                 CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                                 lvDisciplineResolutions.SelectedItems(0).SubItems(colhIncident.Index).Text, _
                                 enAction.EDIT_ONE) Then
                Call Fill_List()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvDisciplineResolutions.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Resolution Steps from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineResolutions.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Resolution Steps?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objDResolution._Isvoid = True
                objDResolution._Voiduserunkid = User._Object._Userunkid
                objDResolution._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objDResolution._Voidreason = mstrVoidReason
                End If

                objDResolution.Delete(CInt(lvDisciplineResolutions.SelectedItems(0).Tag))
                lvDisciplineResolutions.SelectedItems(0).Remove()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboApprovedStatus.SelectedIndex = 0
            cboDisciplineAction.SelectedValue = 0
            cboDisciplineStatus.SelectedValue = 0
            cboDisciplineType.SelectedValue = 0
            txtPersonInvolved.Text = ""
            dtpDisciplineDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDisciplineDate.Checked = False
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If lvDisciplineResolutions.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Resolution Steps from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvDisciplineResolutions.Select()
                Exit Sub
            End If



            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If objDResolution.ApproveResolutionStep(CInt(lvDisciplineResolutions.SelectedItems(0).Tag), _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhFileUnkid.Index).Text), _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhDisciplineStatusId.Index).Text)) Then
            '    Call Fill_List()
            'End If

            Dim frm As New frmApprovalDate
            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            frm._IsFromList = True
            'S.SANDEEP [ 19 DEC 2012 ] -- END
            Dim dtApprDate As Date
            If frm.displayDialog(dtApprDate, _
                                 CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhFileUnkid.Index).Text), _
                                 Nothing) = False Then
                Exit Sub
            End If

            If CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhDisciplineStatusId.Index).Text) = 3 Then
                objDResolution._IsExReOpen = True
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objDResolution.ApproveResolutionStep(CInt(lvDisciplineResolutions.SelectedItems(0).Tag), _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhFileUnkid.Index).Text), _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
            '                                        dtApprDate, _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhDisciplineStatusId.Index).Text)) Then
            '    Call Fill_List()
            'End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objDResolution.ApproveResolutionStep(ConfigParameter._Object._CurrentDateAndTime.Date, _
            '                                        CStr(ConfigParameter._Object._IsArutiDemo), _
            '                                        Company._Object._Total_Active_Employee_ForAllCompany, _
            '                                        ConfigParameter._Object._NoOfEmployees, _
            '                                        User._Object._Userunkid, _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).Tag), _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhFileUnkid.Index).Text), _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
            '                                        dtApprDate, _
            '                                        CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhDisciplineStatusId.Index).Text)) Then
            If objDResolution.ApproveResolutionStep(FinancialYear._Object._DatabaseName, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    ConfigParameter._Object._CurrentDateAndTime.Date, _
                                                    CStr(ConfigParameter._Object._IsArutiDemo), _
                                                    Company._Object._Total_Active_Employee_ForAllCompany, _
                                                    ConfigParameter._Object._NoOfEmployees, _
                                                    User._Object._Userunkid, _
                                                    CInt(lvDisciplineResolutions.SelectedItems(0).Tag), _
                                                    CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhFileUnkid.Index).Text), _
                                                    CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhInvolvedEmpId.Index).Text), _
                                                    dtApprDate, _
                                                    ConfigParameter._Object._DonotAttendanceinSeconds, _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                    User._Object.Privilege._AllowToApproveEarningDeduction, _
                                                    ConfigParameter._Object._CurrentDateAndTime, _
                                                    ConfigParameter._Object._CreateADUserFromEmpMst, _
                                                    ConfigParameter._Object._UserMustchangePwdOnNextLogon, _
                                                    User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod, _
                                                    CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhDisciplineStatusId.Index).Text), _
                                                    , , getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) Then
                'Sohail (09 Oct 2019) - [_CreateADUserFromEmpMst, _UserMustchangePwdOnNextLogon, _AllowToChangeEOCLeavingDateOnClosedPeriod]
                'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP})} -- END
                'Sohail (21 Aug 2015) -- End
                Call Fill_List()
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 20 APRIL 2012 ] -- END





        Catch ex As Exception
            If objDResolution._Message <> "" Then
                eZeeMsgBox.Show(objDResolution._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub lvDisciplineResolutions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvDisciplineResolutions.SelectedIndexChanged
        Try
            If lvDisciplineResolutions.SelectedItems.Count > 0 Then
                If lvDisciplineResolutions.SelectedItems(0).ForeColor = Color.SteelBlue Or lvDisciplineResolutions.SelectedItems(0).ForeColor = Color.SeaGreen Then
                    btnEdit.Enabled = False : btnDelete.Enabled = False
                    If (lvDisciplineResolutions.SelectedItems(0).ForeColor = Color.SteelBlue Or lvDisciplineResolutions.SelectedItems(0).ForeColor = Color.SeaGreen) AndAlso _
                       CBool(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhIsapproved.Index).Text) = False Then
                        btnApprove.Enabled = User._Object.Privilege._AllowToApproveResolutionStep
                    Else
                        btnApprove.Enabled = False
                    End If
                Else
                    btnEdit.Enabled = True : btnDelete.Enabled = True
                    If CBool(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhIsapproved.Index).Text) = False Then
                        Dim intLastResoutionUnkid As Integer = -1
                        intLastResoutionUnkid = objDResolution.Is_LastResolutionSelected(CInt(lvDisciplineResolutions.SelectedItems(0).SubItems(objcolhFileUnkid.Index).Text))
                        If intLastResoutionUnkid = CInt(lvDisciplineResolutions.SelectedItems(0).Tag) Then
                            btnApprove.Enabled = User._Object.Privilege._AllowToApproveResolutionStep
                        Else
                            btnApprove.Enabled = False
                        End If
                    Else
                        btnApprove.Enabled = False
                    End If
                End If
            Else
                Call SetVisibility()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDisciplineResolutions_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprove.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApprove.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPersonInvolved.Text = Language._Object.getCaption(Me.lblPersonInvolved.Name, Me.lblPersonInvolved.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblDisciplinaryAction.Text = Language._Object.getCaption(Me.lblDisciplinaryAction.Name, Me.lblDisciplinaryAction.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhDisciplinaryAction.Text = Language._Object.getCaption(CStr(Me.colhDisciplinaryAction.Tag), Me.colhDisciplinaryAction.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.colhRSteps.Text = Language._Object.getCaption(CStr(Me.colhRSteps.Tag), Me.colhRSteps.Text)
			Me.lblApprovedStatus.Text = Language._Object.getCaption(Me.lblApprovedStatus.Name, Me.lblApprovedStatus.Text)
			Me.colhApprovedStatus.Text = Language._Object.getCaption(CStr(Me.colhApprovedStatus.Tag), Me.colhApprovedStatus.Text)
			Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.Name, Me.lblDisciplineType.Text)
			Me.colhIncident.Text = Language._Object.getCaption(CStr(Me.colhIncident.Tag), Me.colhIncident.Text)
			Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
			Me.lblComplete.Text = Language._Object.getCaption(Me.lblComplete.Name, Me.lblComplete.Text)
			Me.lblApproved.Text = Language._Object.getCaption(Me.lblApproved.Name, Me.lblApproved.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage("clsDiscipline_Resolution", 2, "Pending")
			Language.setMessage(mstrModuleName, 3, "Please select Resolution Steps from the list to perform further operation on it.")
			Language.setMessage("clsDiscipline_Resolution", 1, "Approved")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Resolution Steps?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class