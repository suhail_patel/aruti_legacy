﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPostDisciplineHeads

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPostDisciplineHeads"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintEmployeeUnkid As Integer = -1
    Private mintDisciplineUnkid As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mstrIncident As String = String.Empty
    Private objEmpED As clsEarningDeduction
    'S.SANDEEP [ 17 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtTable As DataTable
    'S.SANDEEP [ 17 DEC 2012 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFileUnkid As Integer, _
                                  ByVal strEmployee As String, _
                                  ByVal intIEmpId As Integer, _
                                  ByVal strIncidentCaused As String, _
                                  ByVal eAction As enAction) As Boolean
        Try
            mintDisciplineUnkid = intFileUnkid
            mintEmployeeUnkid = intIEmpId
            mstrEmployeeName = strEmployee
            mstrIncident = strIncidentCaused
            menAction = eAction

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtIncident.BackColor = GUI.ColorComp
            txtPesonInvolved.BackColor = GUI.ColorComp
            cboTrnHead.BackColor = GUI.ColorComp
            cboTrnHeadType.BackColor = GUI.ColorComp
            cboTypeOf.BackColor = GUI.ColorComp
            txtAmount.BackColor = GUI.ColorOptional
            cboCostCenter.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim dsList As DataSet = Nothing
        Dim objTrnFormula As New clsTranheadFormulaTran
        Dim objTranHead As New clsTransactionHead
        Dim strHeadName As String = ""
        Try
            If CInt(cboTrnHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Transaction Head. Transaction Head is mandatory information."), enMsgBoxStyle.Information)
                cboTrnHead.Focus()
                Return False
            End If

            dsList = objTrnFormula.getFlateRateHeadFromFormula(CInt(cboTrnHead.SelectedValue), "TranHead")
            For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                With dsRow

                    'Sohail (16 May 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Do not prompt message if default value has been set on transaction head formula (other tham Computed Value)
                    If CInt(dsRow.Item("defaultvalue_id")) <> enTranHeadFormula_DefaultValue.COMPUTED_VALUE Then Continue For
                    'Sohail (16 May 2012) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objEmpED.isExist(mintEmployeeUnkid, CInt(.Item("tranheadunkid").ToString)) = False Then
                    'objTranHead._Tranheadunkid = CInt(.Item("tranheadunkid").ToString)
                    If objEmpED.isExist(FinancialYear._Object._DatabaseName, mintEmployeeUnkid, CInt(.Item("tranheadunkid").ToString)) = False Then
                        objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(.Item("tranheadunkid").ToString)
                        'Sohail (21 Aug 2015) -- End
                        strHeadName = objTranHead._Trnheadname

                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "The Formula of this Transaction Head contains ") & strHeadName & _
                                           Language.getMessage(mstrModuleName, 3, " Tranasaction Head (type of Flate Rate).") & vbCrLf & _
                                           Language.getMessage(mstrModuleName, 4, "If you do not assign ") & strHeadName & _
                                           Language.getMessage(mstrModuleName, 5, " Transaction Head, its value will be cosidered as Zero.") & vbCrLf & vbCrLf & _
                                           Language.getMessage(mstrModuleName, 6, "Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Return False
                        End If
                    End If
                End With
            Next

            objTranHead = New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If objTranHead._Formulaid.Trim = "" AndAlso (objTranHead._Calctype_Id = enCalcType.AsComputedValue OrElse objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            'S.SANDEEP [ 17 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objPdata As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPdata._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPdata._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            Dim objProcessed As New clsTnALeaveTran
            If objProcessed.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), CStr(mintEmployeeUnkid), objPdata._End_Date.Date) = True Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot assign this transaction head. Reason : Payroll is already processed for the current period."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot assign this transaction head. Reason : Payroll is already processed for the current period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 11, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Return False
            End If
            objProcessed = Nothing

            If menAction <> enAction.EDIT_ONE AndAlso chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            End If
            'S.SANDEEP [ 17 DEC 2012 ] -- END


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validattion", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Dim objCostCenter As New clscostcenter_master
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsCombo = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = New DataView(dsCombo.Tables("HeadType"), "Id IN(0," & enTranHeadType.EarningForEmployees & "," & enTranHeadType.DeductionForEmployee & ")", "", DataViewRowState.CurrentRows).ToTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            dsCombo = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombo.Tables("CostCenter")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Dim objTranHead As New clsTransactionHead
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmpED._Employeeunkid = mintEmployeeUnkid
            objEmpED._Employeeunkid(FinancialYear._Object._DatabaseName) = mintEmployeeUnkid
            'Sohail (21 Aug 2015) -- End

            'S.SANDEEP [ 17 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtTable = objEmpED._DataSource
            mdtTable.Rows.Clear()
            'S.SANDEEP [ 17 DEC 2012 ] -- END

            objEmpED._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objEmpED._Periodunkid = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If txtAmount.Text.Length > 0 Then
                objEmpED._Amount = txtAmount.Decimal
            Else
                objEmpED._Amount = Nothing
            End If
            If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
                objEmpED._Isdeduct = False
            ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.DeductionForEmployee Or objTranHead._Trnheadtype_Id = enTranHeadType.EmployeesStatutoryDeductions Then
                objEmpED._Isdeduct = True
            Else
                objEmpED._Isdeduct = Nothing
            End If
            objEmpED._Trnheadtype_Id = objTranHead._Trnheadtype_Id
            objEmpED._Typeof_Id = objTranHead._Typeof_id
            objEmpED._Calctype_Id = objTranHead._Calctype_Id
            objEmpED._Computeon_Id = objTranHead._Computeon_Id
            objEmpED._Formula = objTranHead._Formula
            objEmpED._FormulaId = objTranHead._Formulaid
            objEmpED._Userunkid = User._Object._Userunkid
            objEmpED._CostCenterUnkID = CInt(cboCostCenter.SelectedValue)
            If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                objEmpED._Isapproved = True
                objEmpED._Approveruserunkid = User._Object._Userunkid
            Else
                objEmpED._Isapproved = False
            End If
            objEmpED._Disciplinefileunkid = CInt(IIf(mintDisciplineUnkid <= 0, 0, mintDisciplineUnkid))

            'S.SANDEEP [ 17 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim drED As DataRow
            drED = mdtTable.NewRow
            drED.Item("edunkid") = -1
            drED.Item("employeeunkid") = mintEmployeeUnkid
            drED.Item("tranheadunkid") = CInt(cboTrnHead.SelectedValue)
            drED.Item("trnheadname") = ""
            drED.Item("batchtransactionunkid") = -1
            If txtAmount.Text.Length > 0 Then
                drED.Item("amount") = txtAmount.Decimal
            Else
                drED.Item("amount") = 0
            End If
            drED.Item("currencyid") = 0
            drED.Item("vendorid") = 0
            drED.Item("userunkid") = User._Object._Userunkid
            drED.Item("isvoid") = False
            drED.Item("voiduserunkid") = -1
            drED.Item("voiddatetime") = DBNull.Value
            drED.Item("voidreason") = ""
            drED.Item("membership_categoryunkid") = 0
            If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                drED.Item("isapproved") = True
                drED.Item("approveruserunkid") = User._Object._Userunkid
            Else
                drED.Item("isapproved") = False
                drED.Item("approveruserunkid") = -1
            End If
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            drED.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
            drED.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date.Date)
            drED.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date.Date)
            drED.Item("costcenterunkid") = CInt(cboCostCenter.SelectedValue)
            drED.Item("medicalrefno") = ""
            drED.Item("disciplinefileunkid") = CInt(IIf(mintDisciplineUnkid <= 0, 0, mintDisciplineUnkid))
            drED.Item("AUD") = "A"
            mdtTable.Rows.Add(drED)
            'S.SANDEEP [ 17 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPostDisciplineHeads_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpED = New clsEarningDeduction
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()
            SetColor()
            FillCombo()
            txtPesonInvolved.Text = mstrEmployeeName
            txtIncident.Text = mstrIncident
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPostDisciplineHeads_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPostDisciplineHeads_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPostDisciplineHeads_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPostDisciplineHeads_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmpED = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEarningDeduction.SetMessages()
            objfrm._Other_ModuleNames = "clsEarningDeduction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean
        Try
            If Validation() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.ADD_ONE Then
                'S.SANDEEP [ 17 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'blnFlag = objEmpED.Insert(False)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objEmpED.InsertAllByDataTable(mintEmployeeUnkid.ToString, mdtTable, False, True, chkCopyPreviousEDSlab.Checked, , , chkOverwritePrevEDSlabHeads.Checked)

                'Sohail (15 Dec 2018) -- Start
                'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
                'blnFlag = objEmpED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mintEmployeeUnkid.ToString, mdtTable, False, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                blnFlag = objEmpED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, Nothing, mintEmployeeUnkid.ToString, mdtTable, False, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                'Sohail (15 Dec 2018) -- End
                'Sohail (21 Aug 2015) -- End
                'S.SANDEEP [ 17 DEC 2012 ] -- END
            End If

            If blnFlag = False And objEmpED._Message <> "" Then
                eZeeMsgBox.Show(objEmpED._Message, enMsgBoxStyle.Information)
            Else
                Me.Close()
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objSearchTransactionhead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboTrnHead.DisplayMember
                .ValueMember = cboTrnHead.ValueMember
                .DataSource = CType(cboTrnHead.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If frm.DisplayDialog Then
                cboTrnHead.SelectedValue = frm.SelectedValue
                cboTrnHead.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchTransactionhead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            Dim objMaster As New clsMasterData
            Dim dsList As DataSet
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue))
            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id IN (0," & enTypeOf.Other_Earnings & "," & enTypeOf.Other_Deductions_Emp & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboTypeOf
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            objMaster = Nothing : dsList.Dispose()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged
        Try
            Dim objTranHead As New clsTransactionHead
            Dim dsList As DataSet
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(cboTypeOf.SelectedValue))
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(cboTypeOf.SelectedValue))
            'Sohail (21 Aug 2015) -- End
            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable
            With cboTrnHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            objTranHead = Nothing : dsList.Dispose()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboTrnHead_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrnHead.SelectedIndexChanged
        Try
            Dim objTranHead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                txtAmount.Enabled = True
                txtAmount.BackColor = GUI.ColorComp
            Else
                txtAmount.Enabled = False
                txtAmount.BackColor = GUI.ColorOptional
                txtAmount.Text = "0.0"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHead_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.TextChanged

        Try
            If txtAmount.SelectionLength <= 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtAmount_TextChanged", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 17 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 17 DEC 2012 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbPostDisciplineHeads.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPostDisciplineHeads.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbPostDisciplineHeads.Text = Language._Object.getCaption(Me.gbPostDisciplineHeads.Name, Me.gbPostDisciplineHeads.Text)
            Me.lblIncident.Text = Language._Object.getCaption(Me.lblIncident.Name, Me.lblIncident.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
            Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.Name, Me.lblTypeOf.Text)
            Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Transaction Head. Transaction Head is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "The Formula of this Transaction Head contains")
            Language.setMessage(mstrModuleName, 3, " Tranasaction Head (type of Flate Rate).")
            Language.setMessage(mstrModuleName, 4, "If you do not assign")
            Language.setMessage(mstrModuleName, 5, " Transaction Head, its value will be cosidered as Zero.")
            Language.setMessage(mstrModuleName, 6, "Do you want to proceed?")
            Language.setMessage(mstrModuleName, 7, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form.")
            Language.setMessage(mstrModuleName, 8, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot assign this transaction head. Reason : Payroll is already processed for the current period.")
            Language.setMessage(mstrModuleName, 10, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
			Language.setMessage(mstrModuleName, 11, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class