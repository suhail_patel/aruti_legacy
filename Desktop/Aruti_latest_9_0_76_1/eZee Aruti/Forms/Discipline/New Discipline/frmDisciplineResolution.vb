﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDisciplineResolution

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineResolution"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objDResolution As clsDiscipline_Resolution
    Private mintResolutionUnkid As Integer = -1
    Private mintDisciplineFileUnkid As Integer = -1
    Private objDInvestigators As clsDiscipline_Investigators
    Private mstrEmployeeName As String = String.Empty
    Private mdtTran As DataTable
    Private mintEmployeeUnkid As Integer = 0
    Private mstrIncidentCaused As String = String.Empty
    Private mintOperationStatusId As Integer = 0
    Private menSMode As clsDiscipline_Status.enDisciplineStatusType

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, _
                                  ByVal intFileUnkid As Integer, _
                                  ByVal strEmployee As String, _
                                  ByVal intIEmpId As Integer, _
                                  ByVal strIncidentCaused As String, _
                                  ByVal eAction As enAction, _
                                  Optional ByVal enSMode As clsDiscipline_Status.enDisciplineStatusType = clsDiscipline_Status.enDisciplineStatusType.BOTH, _
                                  Optional ByVal intReopenStatusId As Integer = -1) As Boolean
        Try
            mintResolutionUnkid = intUnkId

            mintDisciplineFileUnkid = intFileUnkid

            mstrEmployeeName = strEmployee

            mintEmployeeUnkid = intIEmpId

            mstrIncidentCaused = strIncidentCaused

            menAction = eAction

            mintOperationStatusId = intReopenStatusId

            menSMode = enSMode

            Me.ShowDialog()

            intUnkId = mintResolutionUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboDisciplinaryAction.BackColor = GUI.ColorComp
            cboDisciplineStatus.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            txtComany.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            txtInvestigatorRemark.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtPosition.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            txtResolutionSteps.BackColor = GUI.ColorComp
            txtTrainersContactNo.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtCaseNo.BackColor = GUI.ColorComp
            txtCourtName.BackColor = GUI.ColorComp
            'S.SANDEEP [ 20 APRIL 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtPesonInvolved.Text = mstrEmployeeName
            txtIncident.Text = mstrIncidentCaused

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            Dim objDisciplineFile As New clsDiscipline_File
            Dim objDisciplinetype As New clsDisciplineType
            Dim objCMaster As New clsCommon_Master

            objDisciplineFile._Disciplinefileunkid = mintDisciplineFileUnkid
            objDisciplinetype._Disciplinetypeunkid = objDisciplineFile._Disciplinetypeunkid
            objCMaster._Masterunkid = objDisciplinetype._Offencecategoryunkid

            txtOffenceCategory.Text = objCMaster._Name
            txtOffence.Text = objDisciplinetype._Name

            objDisciplineFile = Nothing
            objDisciplinetype = Nothing
            objDisciplineFile = Nothing
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            If objDResolution._Trandate <> Nothing Then
                dtpDisciplineDate.Value = objDResolution._Trandate
            End If

            cboDisciplinaryAction.SelectedValue = objDResolution._Disciplinaryactionunkid

            If objDResolution._Action_Start_Date <> Nothing Then
                dtpStartDate.Value = objDResolution._Action_Start_Date
            Else
                dtpStartDate.Enabled = True
            End If

            If objDResolution._Action_End_Date <> Nothing Then
                dtpEndDate.Value = objDResolution._Action_End_Date
            Else
                dtpEndDate.Enabled = True
            End If

            cboDisciplineStatus.SelectedValue = objDResolution._Statusunkid
            txtResolutionSteps.Text = objDResolution._Resolutionstep
            txtRemark.Text = objDResolution._Remark

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            cboCommittee.SelectedValue = objDResolution._Committeemasterunkid
            'S.SANDEEP [ 20 MARCH 2012 ] -- END


            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call objDResolution.Is_ExternallyReopen(mintDisciplineFileUnkid)
            txtCaseNo.Text = objDResolution._Caseno
            txtCourtName.Text = objDResolution._Courtname
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objDResolution._Trandate = dtpDisciplineDate.Value
            objDResolution._Disciplinaryactionunkid = CInt(cboDisciplinaryAction.SelectedValue)
            objDResolution._Disciplinefileunkid = mintDisciplineFileUnkid
            objDResolution._InvolvedEmpId = mintEmployeeUnkid
            objDResolution._Remark = txtRemark.Text
            objDResolution._Resolutionstep = txtResolutionSteps.Text
            objDResolution._Statusunkid = CInt(cboDisciplineStatus.SelectedValue)
            objDResolution._Userunkid = User._Object._Userunkid
            If User._Object.Privilege._AllowToApproveResolutionStep = True Then
                'S.SANDEEP [ 19 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If objDResolution._Approvaldate <> Nothing AndAlso menAction <> enAction.EDIT_ONE Then
                objDResolution._Approveduserunkid = User._Object._Userunkid
                objDResolution._Isapproved = True
            Else
                objDResolution._Approveduserunkid = -1
                    objDResolution._Isapproved = False
                End If
                'S.SANDEEP [ 19 DEC 2012 ] -- END
            Else
                objDResolution._Approveduserunkid = -1
            End If


            If dtpStartDate.Visible = True Then
                objDResolution._Action_Start_Date = dtpStartDate.Value
            End If

            If dtpEndDate.Visible = True Then
                objDResolution._Action_End_Date = dtpEndDate.Value
            End If

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            objDResolution._Committeemasterunkid = CInt(cboCommittee.SelectedValue)
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDResolution._Caseno = txtCaseNo.Text.Trim
            objDResolution._Courtname = txtCourtName.Text.Trim

            If mintOperationStatusId = 3 AndAlso menSMode = clsDiscipline_Status.enDisciplineStatusType.EXTERNAL Then
                objDResolution._IsExReOpen = True
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboDisciplinaryAction.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Disciplinary Action is compulsory information.Please Select Disciplinary Action."), enMsgBoxStyle.Information)
                cboDisciplinaryAction.Select()
                Return False
            ElseIf lvInvestigatorInfo.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Investigator is compulsory information.Please add atleast one Investigator."), enMsgBoxStyle.Information)
                lvInvestigatorInfo.Select()
                Return False
            ElseIf txtResolutionSteps.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Resolution Step is compulsory information.Please enter Resolution Step."), enMsgBoxStyle.Information)
                txtResolutionSteps.Focus()
                Return False
            ElseIf CInt(cboDisciplineStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Status is compulsory information.Please select Status."), enMsgBoxStyle.Information)
                cboDisciplineStatus.Focus()
                Return False
            ElseIf dtpEndDate.Visible = True Then
                If dtpEndDate.Value.Date < dtpStartDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "End Date cannot be less then Start Date."), enMsgBoxStyle.Information)
                    cboDisciplineStatus.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintOperationStatusId = 3 AndAlso menSMode = clsDiscipline_Status.enDisciplineStatusType.EXTERNAL Then
                If txtCaseNo.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "External Caseno is mandatory information. Please enter Caseno to continue."), enMsgBoxStyle.Information)
                    txtCaseNo.Focus()
                    Return False
                End If

                If txtCourtName.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "External Court Name is mandatory information. Please enter Court Name to continue."), enMsgBoxStyle.Information)
                    txtCourtName.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objDStatus As New clsDiscipline_Status
        Dim objDAction As New clsAction_Reason
        Dim objEMaster As New clsEmployee_Master
        'S.SANDEEP [ 20 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
        Dim objCMaster As New clsCommon_Master
        'S.SANDEEP [ 20 MARCH 2012 ] -- END
        Try
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            'dsCombo = objDStatus.getComboList("List", True)
            'With cboDisciplineStatus
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("List")
            '    .SelectedValue = 0
            'End With
            dsCombo = objDStatus.getComboList("List", True, menSMode)
            Dim dTable As DataTable = Nothing
            If mintOperationStatusId > 0 Then
                dTable = New DataView(dsCombo.Tables(0), "Id IN(0," & mintOperationStatusId & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dsCombo.Tables(0), "Id NOT IN(1,3)", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboDisciplineStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dTable
                .SelectedValue = 0
            End With

            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
            With cboCommittee
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            dsCombo = objDAction.getComboList("List", True, True)
            Dim dATable As DataTable = Nothing
            If mintOperationStatusId > 0 AndAlso mintOperationStatusId = 3 Then
                dATable = New DataView(dsCombo.Tables(0), "actionreasonunkid NOT IN (1,2)", "", DataViewRowState.CurrentRows).ToTable
            Else
                dATable = New DataView(dsCombo.Tables(0), "actionreasonunkid NOT IN (3,4)", "", DataViewRowState.CurrentRows).ToTable
            End If
            With cboDisciplinaryAction
                .ValueMember = "actionreasonunkid"
                .DisplayMember = "name"
                .DataSource = dATable
                .SelectedValue = 0
            End With

            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEMaster.GetEmployeeList("List", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombo = objEMaster.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombo = objEMaster.GetEmployeeList("List", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If

            dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            
            'Sohail (23 Nov 2012) -- End
            Dim dtTab As DataTable
            dtTab = New DataView(dsCombo.Tables("List"), "employeeunkid NOT IN(" & mintEmployeeUnkid & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dtTab
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objDStatus = Nothing : objDAction = Nothing : objEMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Try
            lvInvestigatorInfo.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    If CInt(dtRow.Item("employeeunkid")) <> -1 Then
                        lvItem.Text = dtRow.Item("employee_name").ToString
                        lvItem.SubItems.Add(dtRow.Item("department").ToString)
                        lvItem.SubItems.Add(Company._Object._Name.ToString)
                        lvItem.SubItems.Add(dtRow.Item("contact").ToString)
                    Else
                        lvItem.Text = dtRow.Item("ex_name").ToString
                        lvItem.SubItems.Add(dtRow.Item("ex_department").ToString)
                        lvItem.SubItems.Add(dtRow.Item("ex_company").ToString)
                        lvItem.SubItems.Add(dtRow.Item("ex_contactno").ToString)
                    End If
                    lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvItem.SubItems.Add(dtRow.Item("investigator_remark").ToString)
                    lvItem.Tag = dtRow.Item("investigatorunkid")
                    If CInt(dtRow.Item("committeetranunkid")) > 0 Then
                        lvItem.ForeColor = Color.Gray
                    End If

                    lvInvestigatorInfo.Items.Add(lvItem)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            radEmployee.Checked = True
            cboEmployee.SelectedValue = 0
            txtInvestigatorRemark.Text = ""
            txtComany.Text = ""
            txtName.Text = ""
            txtPosition.Text = ""
            txtTrainersContactNo.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmDisciplineResolution_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDResolution = New clsDiscipline_Resolution
        objDInvestigators = New clsDiscipline_Investigators
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            SetColor()

            If menAction = enAction.EDIT_ONE Then
                objDResolution._Resolutionunkid = mintResolutionUnkid
                cboCommittee.Enabled = False : objbtnSearchCommittee.Enabled = False
            End If

            FillCombo()

            GetValue()

            radEmployee.Checked = True

            objDInvestigators._ResolutionUnkid = mintResolutionUnkid
            mdtTran = objDInvestigators._DataTable

            Call Fill_List()

            dtpDisciplineDate.Select()

            If menSMode = clsDiscipline_Status.enDisciplineStatusType.EXTERNAL Then
                gbResolutionSteps.Size = CType(New Point(339, 339), Drawing.Size)
                pnlData.Location = New Point(5, 85)

                If mintOperationStatusId = 3 Then
                    txtCaseNo.ReadOnly = False : txtCourtName.ReadOnly = False
                    lblDisciplinaryAction.Text = Language.getMessage(mstrModuleName, 13, "Ruling")
                Else

                    txtCaseNo.ReadOnly = True : txtCourtName.ReadOnly = True
                End If

            Else
                gbResolutionSteps.Size = CType(New Point(339, 283), Drawing.Size)
                pnlData.Location = New Point(5, 30)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineResolution_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineResolution_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineResolution_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineResolution_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objDInvestigators = Nothing : objDResolution = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Resolution.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Resolution"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnAddStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddStatus.Click
        Dim intRefId As Integer = -1
        Dim frm As New frmDisciplineStatus_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                Dim objDisciplineStatus As New clsDiscipline_Status
                Dim dsList As New DataSet
                dsList = objDisciplineStatus.getComboList("List", True)
                With cboDisciplineStatus
                    .ValueMember = "Id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = intRefId
                End With
                objDisciplineStatus = Nothing
                dsList.Dispose()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddStatus_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddAction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddAction.Click
        Dim intRefId As Integer = -1
        Dim frm As New frmAction_Reason_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE, True)

            If intRefId > -1 Then
                Dim objAction As New clsAction_Reason
                Dim dsList As New DataSet
                dsList = objAction.getComboList("Action", True, True)
                With cboDisciplinaryAction
                    .ValueMember = "actionreasonunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Action")
                    .SelectedValue = intRefId
                End With
                objAction = Nothing
                dsList.Dispose()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddAction_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() Then
                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                If CInt(cboDisciplineStatus.SelectedValue) = 1 Then
                    If txtRemark.Text.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Remark is mandatory information. Please give remark to continue."), enMsgBoxStyle.Information)
                        tabcDisciplineResoution.SelectedTab = tabpRemark
                        Exit Sub
                    End If                    
                End If
                If menAction <> enAction.EDIT_ONE Then
                    If dtpDisciplineDate.Value.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Selected Discipline Resolution date is appropriate.Do you want to continue with this selected date?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
                    End If

                    Select Case CInt(cboDisciplinaryAction.SelectedValue)
                        Case 1, 3 'SUSPENSION,UN-SUSPENSION
                            If dtpStartDate.Value.Date >= ConfigParameter._Object._CurrentDateAndTime.Date AndAlso dtpEndDate.Value.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Selected Discipline Suspension dates are appropriate.Do you want to continue with this selected dates?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
                            End If
                        Case 2, 4 'TERMINATED,UN-TERMINATED
                            If dtpStartDate.Value.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Selected Discipline Termination date is appropriate.Do you want to continue with this selected date?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
                            End If
                        Case Else
                            If dtpStartDate.Value.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Selected Discipline Action date is appropriate.Do you want to continue with this selected date?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
                            End If
                    End Select
                End If
                'S.SANDEEP [ 20 MARCH 2012 ] -- END



                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If menAction <> enAction.EDIT_ONE Then
                    If User._Object.Privilege._AllowToApproveResolutionStep = True Then
                        Dim frm As New frmApprovalDate
                        Dim dtApprDate As Date
                        If frm.displayDialog(dtApprDate, mintDisciplineFileUnkid, dtpDisciplineDate.Value) = False Then
                            Exit Sub
                        Else
                            objDResolution._Approvaldate = dtApprDate
                        End If
                    Else
                        objDResolution._Approvaldate = Nothing
                    End If
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END

                Call SetValue()
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If menAction = enAction.EDIT_ONE Then
                '    blnFlag = objDResolution.Update(mdtTran)
                'Else
                '    blnFlag = objDResolution.Insert(mdtTran)
                'End If

                If menAction = enAction.EDIT_ONE Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objDResolution.Update(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, mdtTran)
                    blnFlag = objDResolution.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod, mdtTran, , , getHostName, getIP, User._Object._Username, enLogin_Mode.DESKTOP)
                    'Sohail (09 Oct 2019) - [_CreateADUserFromEmpMst, _UserMustchangePwdOnNextLogon, _AllowToChangeEOCLeavingDateOnClosedPeriod]
                    'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

                    'Sohail (21 Aug 2015) -- End
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objDResolution.Insert(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, mdtTran)
                    blnFlag = objDResolution.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod, mdtTran, , , getHostName, getIP, User._Object._Username, enLogin_Mode.DESKTOP)
                    'Sohail (09 Oct 2019) - [_CreateADUserFromEmpMst, _UserMustchangePwdOnNextLogon, _AllowToChangeEOCLeavingDateOnClosedPeriod]
                    'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

                    'Sohail (21 Aug 2015) -- End
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                If blnFlag = False And objDResolution._Message <> "" Then
                    eZeeMsgBox.Show(objDResolution._Message, enMsgBoxStyle.Information)
                End If

                If blnFlag Then
                    mblnCancel = False
                    If menAction = enAction.ADD_CONTINUE Then
                        objDResolution = Nothing
                        objDResolution = New clsDiscipline_Resolution
                        Call GetValue()
                        lvInvestigatorInfo.Items.Clear()

                        mdtTran.Clear()

                        dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime
                        dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime
                        dtpDisciplineDate.Value = ConfigParameter._Object._CurrentDateAndTime
                    Else
                        mblnCancel = False
                        mintResolutionUnkid = objDResolution._Resolutionunkid
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAddInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddInvestigator.Click
        Try
            If radEmployee.Checked = True Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information, it cannot be blank. Please select an Employee to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim dtRow As DataRow() = mdtTran.Select("employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND AUD <> 'D' ")
                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected Employee is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            ElseIf radOthers.Checked = True Then
                If txtName.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Name is compulsory information, it can not be blank. Please enter Name to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim dtRow As DataRow() = mdtTran.Select("ex_name LIKE '" & txtName.Text & "' AND AUD <> 'D' ")
                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Entered Employee is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If txtComany.Text.Trim = Company._Object._Name Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Company cannot be same. Please enter another company name."), enMsgBoxStyle.Information)
                txtComany.Focus()
                Exit Sub
            End If

            Dim dRow As DataRow = Nothing
            dRow = mdtTran.NewRow

            dRow.Item("investigatorunkid") = -1
            dRow.Item("resolutionunkid") = mintResolutionUnkid
            If radEmployee.Checked = True Then
                dRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            ElseIf radOthers.Checked = True Then
                dRow.Item("employeeunkid") = -1
            End If
            dRow.Item("ex_name") = txtName.Text
            dRow.Item("ex_company") = txtComany.Text
            dRow.Item("ex_department") = txtPosition.Text
            dRow.Item("ex_contactno") = txtTrainersContactNo.Text
            dRow.Item("investigator_remark") = txtInvestigatorRemark.Text
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("employee_name") = cboEmployee.Text
            dRow.Item("department") = objlblDepartmentValue.Text
            dRow.Item("contact") = objlblEmployeeContactNo.Text

            mdtTran.Rows.Add(dRow)

            Call Fill_List()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddInvestigator_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEditInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditInvestigator.Click
        Try
            If lvInvestigatorInfo.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvInvestigatorInfo.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtTran.Select("investigatorunkid = '" & CInt(lvInvestigatorInfo.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    Dim dtRow As DataRow() = Nothing
                    If CInt(drTemp(0)("employeeunkid")) <> -1 Then
                        dtRow = mdtTran.Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' AND AUD <> 'D' AND GUID <> '" & lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                    Else
                        dtRow = mdtTran.Select("ex_name = '" & txtName.Text & "' AND AUD <> 'D' AND GUID <> '" & lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                    End If
                    If dtRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected investigator already peresent in the list."), enMsgBoxStyle.Information)
                    End If

                    With drTemp(0)
                        .Item("investigatorunkid") = lvInvestigatorInfo.SelectedItems(0).Tag
                        .Item("resolutionunkid") = mintResolutionUnkid
                        If radEmployee.Checked = True Then
                            .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                        ElseIf radOthers.Checked = True Then
                            .Item("employeeunkid") = -1
                        End If
                        .Item("ex_name") = txtName.Text
                        .Item("ex_company") = txtComany.Text
                        .Item("ex_department") = txtPosition.Text
                        .Item("ex_contactno") = txtTrainersContactNo.Text
                        .Item("investigator_remark") = txtInvestigatorRemark.Text
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        .Item("GUID") = Guid.NewGuid.ToString
                        .Item("employee_name") = cboEmployee.Text
                        .Item("department") = objlblDepartmentValue.Text
                        .Item("contact") = objlblEmployeeContactNo.Text
                        .AcceptChanges()
                    End With
                End If
                Call Fill_List()
                Call ResetValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditInvestigator_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteInvestigator.Click
        Try
            If lvInvestigatorInfo.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvInvestigatorInfo.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtTran.Select("investigatorunkid = '" & CInt(lvInvestigatorInfo.SelectedItems(0).Tag) & "'")
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    Call Fill_List()
                End If
            End If
            Call ResetValue()
            lvInvestigatorInfo_SelectedIndexChanged(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteInvestigator_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    Private Sub objbtnSearchCommittee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCommittee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboCommittee.DisplayMember
                .ValueMember = cboCommittee.ValueMember
                .DataSource = CType(cboCommittee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboCommittee.SelectedValue = frm.SelectedValue
                cboCommittee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCommittee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

#End Region

#Region " Controls Events "

    Private Sub radOthers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radOthers.CheckedChanged, radEmployee.CheckedChanged
        Try
            If CType(sender, RadioButton).Name.ToString.ToUpper = "RADOTHERS" AndAlso CType(sender, RadioButton).Checked = True Then
                pnlOthers.Enabled = True
                cboEmployee.Enabled = False
                objlblCompanyValue.Text = ""
                objlblDepartmentValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                objbtnSearchEmployee.Enabled = False
            ElseIf CType(sender, RadioButton).Name.ToString.ToUpper = "RADEMPLOYEE" AndAlso CType(sender, RadioButton).Checked = True Then
                cboEmployee.Enabled = True
                pnlOthers.Enabled = False
                objlblCompanyValue.Text = ""
                objlblDepartmentValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                objbtnSearchEmployee.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radOthers_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                Dim objDept As New clsDepartment

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                objDept._Departmentunkid = objEmp._Departmentunkid
                objlblDepartmentValue.Text = objDept._Name
                objlblCompanyValue.Text = Company._Object._Name
                objlblEmployeeContactNo.Text = objEmp._Present_Tel_No
            Else
                objlblDepartmentValue.Text = ""
                objlblCompanyValue.Text = ""
                objlblEmployeeContactNo.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDisciplinaryAction_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDisciplinaryAction.SelectedIndexChanged
        Try
            If CInt(cboDisciplinaryAction.SelectedValue) > 0 Then
                Dim objDisAction As New clsAction_Reason
                objDisAction._Actionreasonunkid = CInt(cboDisciplinaryAction.SelectedValue)
                txtDescription.Text = objDisAction._Reason_Action
            Else
                txtDescription.Text = ""
            End If

            Select Case CInt(cboDisciplinaryAction.SelectedValue)
                Case 1, 3   'Suspension
                    dtpEndDate.Enabled = True
                    dtpEndDate.Visible = True
                    lblEndDate.Visible = True
                Case 2, 4   'Termination
                    dtpEndDate.Enabled = False
                    dtpEndDate.Visible = False
                    lblEndDate.Visible = False
                    'S.SANDEEP [ 20 MARCH 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                Case Else
                    dtpEndDate.Enabled = False
                    dtpEndDate.Visible = False
                    lblEndDate.Visible = False
                    'S.SANDEEP [ 20 MARCH 2012 ] -- END
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub lvInvestigatorInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvInvestigatorInfo.SelectedIndexChanged
        Try
            If lvInvestigatorInfo.SelectedItems.Count > 0 Then
                cboEmployee.SelectedValue = 0

                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                If lvInvestigatorInfo.SelectedItems(0).ForeColor = Color.Gray Then
                    pnlView.Enabled = False
                Else
                    pnlView.Enabled = True
                End If
                'S.SANDEEP [ 20 MARCH 2012 ] -- END

                txtName.Text = "" : txtPosition.Text = "" : txtComany.Text = "" : txtTrainersContactNo.Text = "" : txtInvestigatorRemark.Text = ""

                If CInt(lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhEmpId.Index).Text) <> -1 Then
                    radEmployee.Enabled = True : radEmployee.Checked = True
                    cboEmployee.SelectedValue = CInt(lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)
                    txtInvestigatorRemark.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhRemark.Index).Text
                    radOthers.Enabled = False : pnlOthers.Enabled = False
                Else
                    radEmployee.Enabled = False : pnlOthers.Enabled = True
                    radOthers.Enabled = True : radOthers.Checked = True
                    txtName.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhInvestigator.Index).Text
                    txtPosition.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhDepartment.Index).Text
                    txtComany.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhCompany.Index).Text
                    txtTrainersContactNo.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhContactNo.Index).Text
                    txtInvestigatorRemark.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhRemark.Index).Text
                End If
            Else
                cboEmployee.SelectedValue = 0
                radEmployee.Enabled = True : radOthers.Enabled = True
                pnlView.Enabled = True
                Call ResetValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvInvestigatorInfo_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboCommittee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCommittee.SelectedIndexChanged
        Try            
            If CInt(cboCommittee.SelectedValue) > 0 Then
                Me.Cursor = Cursors.WaitCursor
                If menAction <> enAction.EDIT_ONE Then
                    Dim dTable As DataTable
                    Dim objCommittee As New clsDiscipline_Committee
                    Dim dRow As DataRow = Nothing

                    'Pinkal (22-Jan-2021) -- Start
                    'Enhancement NMB - Working Discipline changes required by NMB.
                    'objCommittee._CommitteeMasterId(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboCommittee.SelectedValue)
                    objCommittee._CommitteeMasterId(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._DatabaseName, ConfigParameter._Object._IsIncludeInactiveEmp) = CInt(cboCommittee.SelectedValue)
                    'Pinkal (22-Jan-2021) -- End

                    'S.SANDEEP [ 20 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    mdtTran.Rows.Clear()
                    'S.SANDEEP [ 20 APRIL 2012 ] -- END

                    If mdtTran.Rows.Count > 0 Then

                        If objCommittee._DataTable.Rows.Count > 0 AndAlso CStr(objCommittee._DataTable.Rows(0)("Ids")).Trim.Length > 0 Then
                            dTable = New DataView(objCommittee._DataTable, "committeetranunkid NOT IN(" & CStr(objCommittee._DataTable.Rows(0)("Ids")) & ")", "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dTable = New DataView(objCommittee._DataTable, "", "", DataViewRowState.CurrentRows).ToTable
                        End If
                    Else
                        dTable = New DataView(objCommittee._DataTable, "", "", DataViewRowState.CurrentRows).ToTable
                    End If

                    For Each dtRow As DataRow In dTable.Rows
                        dRow = mdtTran.NewRow

                        dRow.Item("investigatorunkid") = -1
                        dRow.Item("resolutionunkid") = mintResolutionUnkid

                        If CInt(dtRow.Item("employeeunkid")) > 0 Then
                            dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                        Else
                            dRow.Item("employeeunkid") = -1
                        End If
                        dRow.Item("ex_name") = dtRow.Item("ex_name")
                        dRow.Item("ex_company") = dtRow.Item("ex_company")
                        dRow.Item("ex_department") = dtRow.Item("ex_department")
                        dRow.Item("ex_contactno") = dtRow.Item("ex_contactno")
                        dRow.Item("investigator_remark") = ""
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        dRow.Item("employee_name") = dtRow.Item("employee_name").ToString.Trim
                        dRow.Item("company") = dtRow.Item("company").ToString.Trim
                        dRow.Item("department") = dtRow.Item("department").ToString.Trim
                        dRow.Item("contact") = dtRow.Item("contact").ToString.Trim
                        dRow.Item("committeetranunkid") = dtRow.Item("committeetranunkid")
                        mdtTran.Rows.Add(dRow)
                    Next
                    Call Fill_List()
                End If
            Else
                lvInvestigatorInfo.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCommittee_SelectedIndexChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbResolutionSteps.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbResolutionSteps.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbPersonInvolved.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPersonInvolved.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbInvestigatorInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInvestigatorInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddInvestigator.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAddInvestigator.GradientForeColor = GUI._ButttonFontColor

			Me.btnEditInvestigator.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEditInvestigator.GradientForeColor = GUI._ButttonFontColor

			Me.btnDeleteInvestigator.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDeleteInvestigator.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbResolutionSteps.Text = Language._Object.getCaption(Me.gbResolutionSteps.Name, Me.gbResolutionSteps.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblDisciplinaryAction.Text = Language._Object.getCaption(Me.lblDisciplinaryAction.Name, Me.lblDisciplinaryAction.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.gbPersonInvolved.Text = Language._Object.getCaption(Me.gbPersonInvolved.Name, Me.gbPersonInvolved.Text)
			Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
			Me.gbInvestigatorInfo.Text = Language._Object.getCaption(Me.gbInvestigatorInfo.Name, Me.gbInvestigatorInfo.Text)
			Me.lblTrainerContactNo.Text = Language._Object.getCaption(Me.lblTrainerContactNo.Name, Me.lblTrainerContactNo.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.radOthers.Text = Language._Object.getCaption(Me.radOthers.Name, Me.radOthers.Text)
			Me.lblEmpContact.Text = Language._Object.getCaption(Me.lblEmpContact.Name, Me.lblEmpContact.Text)
			Me.lblEmpCompany.Text = Language._Object.getCaption(Me.lblEmpCompany.Name, Me.lblEmpCompany.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
			Me.colhInvestigator.Text = Language._Object.getCaption(CStr(Me.colhInvestigator.Tag), Me.colhInvestigator.Text)
			Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
			Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
			Me.colhContactNo.Text = Language._Object.getCaption(CStr(Me.colhContactNo.Tag), Me.colhContactNo.Text)
			Me.btnAddInvestigator.Text = Language._Object.getCaption(Me.btnAddInvestigator.Name, Me.btnAddInvestigator.Text)
			Me.btnEditInvestigator.Text = Language._Object.getCaption(Me.btnEditInvestigator.Name, Me.btnEditInvestigator.Text)
			Me.btnDeleteInvestigator.Text = Language._Object.getCaption(Me.btnDeleteInvestigator.Name, Me.btnDeleteInvestigator.Text)
			Me.lblInvestigatorRemark.Text = Language._Object.getCaption(Me.lblInvestigatorRemark.Name, Me.lblInvestigatorRemark.Text)
			Me.tabpResolution.Text = Language._Object.getCaption(Me.tabpResolution.Name, Me.tabpResolution.Text)
			Me.tabpRemark.Text = Language._Object.getCaption(Me.tabpRemark.Name, Me.tabpRemark.Text)
			Me.lblIncident.Text = Language._Object.getCaption(Me.lblIncident.Name, Me.lblIncident.Text)
			Me.lblCommitee.Text = Language._Object.getCaption(Me.lblCommitee.Name, Me.lblCommitee.Text)
			Me.lblOffence.Text = Language._Object.getCaption(Me.lblOffence.Name, Me.lblOffence.Text)
			Me.lblOffenceCategory.Text = Language._Object.getCaption(Me.lblOffenceCategory.Name, Me.lblOffenceCategory.Text)
			Me.lblCourtName.Text = Language._Object.getCaption(Me.lblCourtName.Name, Me.lblCourtName.Text)
			Me.lblCaseNo.Text = Language._Object.getCaption(Me.lblCaseNo.Name, Me.lblCaseNo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information, it cannot be blank. Please select an Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Selected Employee is already added to the list.")
			Language.setMessage(mstrModuleName, 3, "Name is compulsory information, it can not be blank. Please enter Name to continue.")
			Language.setMessage(mstrModuleName, 4, "Entered Employee is already added to the list.")
			Language.setMessage(mstrModuleName, 5, "Company cannot be same. Please enter another company name.")
			Language.setMessage(mstrModuleName, 6, "Selected investigator already peresent in the list.")
			Language.setMessage(mstrModuleName, 7, "Disciplinary Action is compulsory information.Please Select Disciplinary Action.")
			Language.setMessage(mstrModuleName, 8, "Investigator is compulsory information.Please add atleast one Investigator.")
			Language.setMessage(mstrModuleName, 9, "Resolution Step is compulsory information.Please enter Resolution Step.")
			Language.setMessage(mstrModuleName, 10, "Status is compulsory information.Please select Status.")
			Language.setMessage(mstrModuleName, 11, "End Date cannot be less then Start Date.")
			Language.setMessage(mstrModuleName, 12, "Remark is mandatory information. Please give remark to continue.")
			Language.setMessage(mstrModuleName, 13, "Ruling")
			Language.setMessage(mstrModuleName, 14, "External Caseno is mandatory information. Please enter Caseno to continue.")
			Language.setMessage(mstrModuleName, 15, "External Court Name is mandatory information. Please enter Court Name to continue.")
			Language.setMessage(mstrModuleName, 16, "Selected Discipline Suspension dates are appropriate.Do you want to continue with this selected dates?")
			Language.setMessage(mstrModuleName, 17, "Selected Discipline Termination date is appropriate.Do you want to continue with this selected date?")
			Language.setMessage(mstrModuleName, 18, "Selected Discipline Resolution date is appropriate.Do you want to continue with this selected date?")
			Language.setMessage(mstrModuleName, 19, "Selected Discipline Action date is appropriate.Do you want to continue with this selected date?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class