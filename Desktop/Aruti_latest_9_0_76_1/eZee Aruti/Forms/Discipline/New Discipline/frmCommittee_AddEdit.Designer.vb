﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommittee_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommittee_AddEdit))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEditInvestigator = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbCommitteAddEdit = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objColon4 = New System.Windows.Forms.Label
        Me.lblEmpEmail = New System.Windows.Forms.Label
        Me.objlblEmployeeEmail = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblMembersCategory = New System.Windows.Forms.Label
        Me.objbtnSearchMCategory = New eZee.Common.eZeeGradientButton
        Me.objbtnAddMCategory = New eZee.Common.eZeeGradientButton
        Me.cboMCategory = New System.Windows.Forms.ComboBox
        Me.objbtnAddCommittee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCommittee = New eZee.Common.eZeeGradientButton
        Me.btnDeleteInvestigator = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvInvestigatorInfo = New eZee.Common.eZeeListView(Me.components)
        Me.colhInvestigator = New System.Windows.Forms.ColumnHeader
        Me.colhMCategory = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhContactNo = New System.Windows.Forms.ColumnHeader
        Me.colhEmail = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objMCatId = New System.Windows.Forms.ColumnHeader
        Me.radOthers = New System.Windows.Forms.RadioButton
        Me.btnAddInvestigator = New eZee.Common.eZeeLightButton(Me.components)
        Me.objColon3 = New System.Windows.Forms.Label
        Me.objColon2 = New System.Windows.Forms.Label
        Me.pnlOthers = New System.Windows.Forms.Panel
        Me.lblEmail = New System.Windows.Forms.Label
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblTrainerContactNo = New System.Windows.Forms.Label
        Me.txtTrainersContactNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblPosition = New System.Windows.Forms.Label
        Me.txtComany = New eZee.TextBox.AlphanumericTextBox
        Me.txtPosition = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.objColon1 = New System.Windows.Forms.Label
        Me.lblEmpContact = New System.Windows.Forms.Label
        Me.lblEmpCompany = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.objlblEmployeeContactNo = New System.Windows.Forms.Label
        Me.objlblCompanyValue = New System.Windows.Forms.Label
        Me.objlblDepartmentValue = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.lblCommitteMaster = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboCommitteMaster = New System.Windows.Forms.ComboBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter.SuspendLayout()
        Me.gbCommitteAddEdit.SuspendLayout()
        Me.pnlOthers.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 402)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(695, 55)
        Me.objFooter.TabIndex = 86
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(481, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(584, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEditInvestigator
        '
        Me.btnEditInvestigator.BackColor = System.Drawing.Color.White
        Me.btnEditInvestigator.BackgroundImage = CType(resources.GetObject("btnEditInvestigator.BackgroundImage"), System.Drawing.Image)
        Me.btnEditInvestigator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditInvestigator.BorderColor = System.Drawing.Color.Empty
        Me.btnEditInvestigator.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditInvestigator.FlatAppearance.BorderSize = 0
        Me.btnEditInvestigator.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditInvestigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditInvestigator.ForeColor = System.Drawing.Color.Black
        Me.btnEditInvestigator.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditInvestigator.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditInvestigator.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditInvestigator.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditInvestigator.Location = New System.Drawing.Point(491, 174)
        Me.btnEditInvestigator.Name = "btnEditInvestigator"
        Me.btnEditInvestigator.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditInvestigator.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditInvestigator.Size = New System.Drawing.Size(92, 30)
        Me.btnEditInvestigator.TabIndex = 98
        Me.btnEditInvestigator.Text = "&Edit"
        Me.btnEditInvestigator.UseVisualStyleBackColor = True
        '
        'gbCommitteAddEdit
        '
        Me.gbCommitteAddEdit.BorderColor = System.Drawing.Color.Black
        Me.gbCommitteAddEdit.Checked = False
        Me.gbCommitteAddEdit.CollapseAllExceptThis = False
        Me.gbCommitteAddEdit.CollapsedHoverImage = Nothing
        Me.gbCommitteAddEdit.CollapsedNormalImage = Nothing
        Me.gbCommitteAddEdit.CollapsedPressedImage = Nothing
        Me.gbCommitteAddEdit.CollapseOnLoad = False
        Me.gbCommitteAddEdit.Controls.Add(Me.objColon4)
        Me.gbCommitteAddEdit.Controls.Add(Me.lblEmpEmail)
        Me.gbCommitteAddEdit.Controls.Add(Me.objlblEmployeeEmail)
        Me.gbCommitteAddEdit.Controls.Add(Me.lblEmployee)
        Me.gbCommitteAddEdit.Controls.Add(Me.lblMembersCategory)
        Me.gbCommitteAddEdit.Controls.Add(Me.btnEditInvestigator)
        Me.gbCommitteAddEdit.Controls.Add(Me.objbtnSearchMCategory)
        Me.gbCommitteAddEdit.Controls.Add(Me.objbtnAddMCategory)
        Me.gbCommitteAddEdit.Controls.Add(Me.cboMCategory)
        Me.gbCommitteAddEdit.Controls.Add(Me.objbtnAddCommittee)
        Me.gbCommitteAddEdit.Controls.Add(Me.objbtnSearchCommittee)
        Me.gbCommitteAddEdit.Controls.Add(Me.btnDeleteInvestigator)
        Me.gbCommitteAddEdit.Controls.Add(Me.lvInvestigatorInfo)
        Me.gbCommitteAddEdit.Controls.Add(Me.radOthers)
        Me.gbCommitteAddEdit.Controls.Add(Me.btnAddInvestigator)
        Me.gbCommitteAddEdit.Controls.Add(Me.objColon3)
        Me.gbCommitteAddEdit.Controls.Add(Me.objColon2)
        Me.gbCommitteAddEdit.Controls.Add(Me.pnlOthers)
        Me.gbCommitteAddEdit.Controls.Add(Me.objColon1)
        Me.gbCommitteAddEdit.Controls.Add(Me.lblEmpContact)
        Me.gbCommitteAddEdit.Controls.Add(Me.lblEmpCompany)
        Me.gbCommitteAddEdit.Controls.Add(Me.lblDepartment)
        Me.gbCommitteAddEdit.Controls.Add(Me.objlblEmployeeContactNo)
        Me.gbCommitteAddEdit.Controls.Add(Me.objlblCompanyValue)
        Me.gbCommitteAddEdit.Controls.Add(Me.objlblDepartmentValue)
        Me.gbCommitteAddEdit.Controls.Add(Me.cboEmployee)
        Me.gbCommitteAddEdit.Controls.Add(Me.radEmployee)
        Me.gbCommitteAddEdit.Controls.Add(Me.lblCommitteMaster)
        Me.gbCommitteAddEdit.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbCommitteAddEdit.Controls.Add(Me.cboCommitteMaster)
        Me.gbCommitteAddEdit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbCommitteAddEdit.ExpandedHoverImage = Nothing
        Me.gbCommitteAddEdit.ExpandedNormalImage = Nothing
        Me.gbCommitteAddEdit.ExpandedPressedImage = Nothing
        Me.gbCommitteAddEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCommitteAddEdit.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCommitteAddEdit.HeaderHeight = 25
        Me.gbCommitteAddEdit.HeaderMessage = ""
        Me.gbCommitteAddEdit.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbCommitteAddEdit.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCommitteAddEdit.HeightOnCollapse = 0
        Me.gbCommitteAddEdit.LeftTextSpace = 0
        Me.gbCommitteAddEdit.Location = New System.Drawing.Point(0, 0)
        Me.gbCommitteAddEdit.Name = "gbCommitteAddEdit"
        Me.gbCommitteAddEdit.OpenHeight = 300
        Me.gbCommitteAddEdit.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCommitteAddEdit.ShowBorder = True
        Me.gbCommitteAddEdit.ShowCheckBox = False
        Me.gbCommitteAddEdit.ShowCollapseButton = False
        Me.gbCommitteAddEdit.ShowDefaultBorderColor = True
        Me.gbCommitteAddEdit.ShowDownButton = False
        Me.gbCommitteAddEdit.ShowHeader = True
        Me.gbCommitteAddEdit.Size = New System.Drawing.Size(695, 402)
        Me.gbCommitteAddEdit.TabIndex = 87
        Me.gbCommitteAddEdit.Temp = 0
        Me.gbCommitteAddEdit.Text = "Committee Members"
        Me.gbCommitteAddEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon4
        '
        Me.objColon4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon4.Location = New System.Drawing.Point(113, 182)
        Me.objColon4.Name = "objColon4"
        Me.objColon4.Size = New System.Drawing.Size(8, 15)
        Me.objColon4.TabIndex = 110
        Me.objColon4.Text = ":"
        Me.objColon4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpEmail
        '
        Me.lblEmpEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpEmail.Location = New System.Drawing.Point(30, 182)
        Me.lblEmpEmail.Name = "lblEmpEmail"
        Me.lblEmpEmail.Size = New System.Drawing.Size(80, 15)
        Me.lblEmpEmail.TabIndex = 109
        Me.lblEmpEmail.Text = "Email Id"
        Me.lblEmpEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmployeeEmail
        '
        Me.objlblEmployeeEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmployeeEmail.Location = New System.Drawing.Point(127, 182)
        Me.objlblEmployeeEmail.Name = "objlblEmployeeEmail"
        Me.objlblEmployeeEmail.Size = New System.Drawing.Size(150, 15)
        Me.objlblEmployeeEmail.TabIndex = 111
        Me.objlblEmployeeEmail.Text = "#Email Id"
        Me.objlblEmployeeEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(6, 90)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(127, 15)
        Me.lblEmployee.TabIndex = 107
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMembersCategory
        '
        Me.lblMembersCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembersCategory.Location = New System.Drawing.Point(6, 63)
        Me.lblMembersCategory.Name = "lblMembersCategory"
        Me.lblMembersCategory.Size = New System.Drawing.Size(127, 15)
        Me.lblMembersCategory.TabIndex = 106
        Me.lblMembersCategory.Text = "Members Category"
        Me.lblMembersCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchMCategory
        '
        Me.objbtnSearchMCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMCategory.BorderSelected = False
        Me.objbtnSearchMCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMCategory.Location = New System.Drawing.Point(382, 60)
        Me.objbtnSearchMCategory.Name = "objbtnSearchMCategory"
        Me.objbtnSearchMCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMCategory.TabIndex = 105
        '
        'objbtnAddMCategory
        '
        Me.objbtnAddMCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddMCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddMCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddMCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddMCategory.BorderSelected = False
        Me.objbtnAddMCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddMCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddMCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddMCategory.Location = New System.Drawing.Point(355, 60)
        Me.objbtnAddMCategory.Name = "objbtnAddMCategory"
        Me.objbtnAddMCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddMCategory.TabIndex = 104
        '
        'cboMCategory
        '
        Me.cboMCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMCategory.FormattingEnabled = True
        Me.cboMCategory.Location = New System.Drawing.Point(138, 60)
        Me.cboMCategory.Name = "cboMCategory"
        Me.cboMCategory.Size = New System.Drawing.Size(214, 21)
        Me.cboMCategory.TabIndex = 103
        '
        'objbtnAddCommittee
        '
        Me.objbtnAddCommittee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCommittee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCommittee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCommittee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCommittee.BorderSelected = False
        Me.objbtnAddCommittee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCommittee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCommittee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCommittee.Location = New System.Drawing.Point(355, 33)
        Me.objbtnAddCommittee.Name = "objbtnAddCommittee"
        Me.objbtnAddCommittee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCommittee.TabIndex = 102
        '
        'objbtnSearchCommittee
        '
        Me.objbtnSearchCommittee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCommittee.BorderSelected = False
        Me.objbtnSearchCommittee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCommittee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCommittee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCommittee.Location = New System.Drawing.Point(382, 33)
        Me.objbtnSearchCommittee.Name = "objbtnSearchCommittee"
        Me.objbtnSearchCommittee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCommittee.TabIndex = 100
        '
        'btnDeleteInvestigator
        '
        Me.btnDeleteInvestigator.BackColor = System.Drawing.Color.White
        Me.btnDeleteInvestigator.BackgroundImage = CType(resources.GetObject("btnDeleteInvestigator.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteInvestigator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteInvestigator.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteInvestigator.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteInvestigator.FlatAppearance.BorderSize = 0
        Me.btnDeleteInvestigator.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteInvestigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteInvestigator.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteInvestigator.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteInvestigator.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteInvestigator.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteInvestigator.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteInvestigator.Location = New System.Drawing.Point(589, 174)
        Me.btnDeleteInvestigator.Name = "btnDeleteInvestigator"
        Me.btnDeleteInvestigator.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteInvestigator.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteInvestigator.Size = New System.Drawing.Size(92, 30)
        Me.btnDeleteInvestigator.TabIndex = 99
        Me.btnDeleteInvestigator.Text = "&Delete"
        Me.btnDeleteInvestigator.UseVisualStyleBackColor = True
        '
        'lvInvestigatorInfo
        '
        Me.lvInvestigatorInfo.BackColorOnChecked = True
        Me.lvInvestigatorInfo.ColumnHeaders = Nothing
        Me.lvInvestigatorInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhInvestigator, Me.colhMCategory, Me.colhDepartment, Me.colhCompany, Me.colhContactNo, Me.colhEmail, Me.objcolhEmpId, Me.objcolhGUID, Me.objMCatId})
        Me.lvInvestigatorInfo.CompulsoryColumns = ""
        Me.lvInvestigatorInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvInvestigatorInfo.FullRowSelect = True
        Me.lvInvestigatorInfo.GridLines = True
        Me.lvInvestigatorInfo.GroupingColumn = Nothing
        Me.lvInvestigatorInfo.HideSelection = False
        Me.lvInvestigatorInfo.Location = New System.Drawing.Point(8, 210)
        Me.lvInvestigatorInfo.MinColumnWidth = 50
        Me.lvInvestigatorInfo.MultiSelect = False
        Me.lvInvestigatorInfo.Name = "lvInvestigatorInfo"
        Me.lvInvestigatorInfo.OptionalColumns = ""
        Me.lvInvestigatorInfo.ShowMoreItem = False
        Me.lvInvestigatorInfo.ShowSaveItem = False
        Me.lvInvestigatorInfo.ShowSelectAll = True
        Me.lvInvestigatorInfo.ShowSizeAllColumnsToFit = True
        Me.lvInvestigatorInfo.Size = New System.Drawing.Size(673, 186)
        Me.lvInvestigatorInfo.Sortable = True
        Me.lvInvestigatorInfo.TabIndex = 94
        Me.lvInvestigatorInfo.UseCompatibleStateImageBehavior = False
        Me.lvInvestigatorInfo.View = System.Windows.Forms.View.Details
        '
        'colhInvestigator
        '
        Me.colhInvestigator.Tag = "colhInvestigator"
        Me.colhInvestigator.Text = "Member's Name"
        Me.colhInvestigator.Width = 200
        '
        'colhMCategory
        '
        Me.colhMCategory.Tag = "colhMCategory"
        Me.colhMCategory.Text = "Member Category"
        Me.colhMCategory.Width = 125
        '
        'colhDepartment
        '
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 110
        '
        'colhCompany
        '
        Me.colhCompany.Tag = "colhCompany"
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 110
        '
        'colhContactNo
        '
        Me.colhContactNo.Tag = "colhContactNo"
        Me.colhContactNo.Text = "Contact No"
        Me.colhContactNo.Width = 120
        '
        'colhEmail
        '
        Me.colhEmail.Tag = "colhEmail"
        Me.colhEmail.Text = "Email"
        Me.colhEmail.Width = 200
        '
        'objcolhEmpId
        '
        Me.objcolhEmpId.Tag = "objcolhEmpId"
        Me.objcolhEmpId.Text = ""
        Me.objcolhEmpId.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = ""
        Me.objcolhGUID.Width = 0
        '
        'objMCatId
        '
        Me.objMCatId.Tag = "objMCatId"
        Me.objMCatId.Text = ""
        Me.objMCatId.Width = 0
        '
        'radOthers
        '
        Me.radOthers.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radOthers.BackColor = System.Drawing.Color.Transparent
        Me.radOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOthers.Location = New System.Drawing.Point(561, 4)
        Me.radOthers.Name = "radOthers"
        Me.radOthers.Size = New System.Drawing.Size(128, 17)
        Me.radOthers.TabIndex = 36
        Me.radOthers.TabStop = True
        Me.radOthers.Text = "Other"
        Me.radOthers.UseVisualStyleBackColor = False
        '
        'btnAddInvestigator
        '
        Me.btnAddInvestigator.BackColor = System.Drawing.Color.White
        Me.btnAddInvestigator.BackgroundImage = CType(resources.GetObject("btnAddInvestigator.BackgroundImage"), System.Drawing.Image)
        Me.btnAddInvestigator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddInvestigator.BorderColor = System.Drawing.Color.Empty
        Me.btnAddInvestigator.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddInvestigator.FlatAppearance.BorderSize = 0
        Me.btnAddInvestigator.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddInvestigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddInvestigator.ForeColor = System.Drawing.Color.Black
        Me.btnAddInvestigator.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddInvestigator.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddInvestigator.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddInvestigator.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddInvestigator.Location = New System.Drawing.Point(393, 174)
        Me.btnAddInvestigator.Name = "btnAddInvestigator"
        Me.btnAddInvestigator.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddInvestigator.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddInvestigator.Size = New System.Drawing.Size(92, 30)
        Me.btnAddInvestigator.TabIndex = 97
        Me.btnAddInvestigator.Text = "&Add"
        Me.btnAddInvestigator.UseVisualStyleBackColor = True
        '
        'objColon3
        '
        Me.objColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon3.Location = New System.Drawing.Point(113, 161)
        Me.objColon3.Name = "objColon3"
        Me.objColon3.Size = New System.Drawing.Size(8, 15)
        Me.objColon3.TabIndex = 34
        Me.objColon3.Text = ":"
        Me.objColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon2
        '
        Me.objColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon2.Location = New System.Drawing.Point(113, 140)
        Me.objColon2.Name = "objColon2"
        Me.objColon2.Size = New System.Drawing.Size(8, 15)
        Me.objColon2.TabIndex = 31
        Me.objColon2.Text = ":"
        Me.objColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlOthers
        '
        Me.pnlOthers.Controls.Add(Me.lblEmail)
        Me.pnlOthers.Controls.Add(Me.txtEmail)
        Me.pnlOthers.Controls.Add(Me.lblTrainerContactNo)
        Me.pnlOthers.Controls.Add(Me.txtTrainersContactNo)
        Me.pnlOthers.Controls.Add(Me.lblCompany)
        Me.pnlOthers.Controls.Add(Me.lblPosition)
        Me.pnlOthers.Controls.Add(Me.txtComany)
        Me.pnlOthers.Controls.Add(Me.txtPosition)
        Me.pnlOthers.Controls.Add(Me.lblName)
        Me.pnlOthers.Controls.Add(Me.txtName)
        Me.pnlOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOthers.Location = New System.Drawing.Point(413, 30)
        Me.pnlOthers.Name = "pnlOthers"
        Me.pnlOthers.Size = New System.Drawing.Size(271, 138)
        Me.pnlOthers.TabIndex = 37
        '
        'lblEmail
        '
        Me.lblEmail.Location = New System.Drawing.Point(4, 114)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(74, 15)
        Me.lblEmail.TabIndex = 8
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(81, 111)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(187, 21)
        Me.txtEmail.TabIndex = 9
        '
        'lblTrainerContactNo
        '
        Me.lblTrainerContactNo.Location = New System.Drawing.Point(4, 87)
        Me.lblTrainerContactNo.Name = "lblTrainerContactNo"
        Me.lblTrainerContactNo.Size = New System.Drawing.Size(74, 15)
        Me.lblTrainerContactNo.TabIndex = 6
        Me.lblTrainerContactNo.Text = "Contact No"
        Me.lblTrainerContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTrainersContactNo
        '
        Me.txtTrainersContactNo.Flags = 0
        Me.txtTrainersContactNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTrainersContactNo.Location = New System.Drawing.Point(81, 84)
        Me.txtTrainersContactNo.Name = "txtTrainersContactNo"
        Me.txtTrainersContactNo.Size = New System.Drawing.Size(187, 21)
        Me.txtTrainersContactNo.TabIndex = 7
        '
        'lblCompany
        '
        Me.lblCompany.Location = New System.Drawing.Point(4, 60)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(74, 15)
        Me.lblCompany.TabIndex = 4
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition
        '
        Me.lblPosition.Location = New System.Drawing.Point(4, 33)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(74, 15)
        Me.lblPosition.TabIndex = 2
        Me.lblPosition.Text = "Department"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComany
        '
        Me.txtComany.Flags = 0
        Me.txtComany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtComany.Location = New System.Drawing.Point(81, 57)
        Me.txtComany.Name = "txtComany"
        Me.txtComany.Size = New System.Drawing.Size(187, 21)
        Me.txtComany.TabIndex = 5
        '
        'txtPosition
        '
        Me.txtPosition.Flags = 0
        Me.txtPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPosition.Location = New System.Drawing.Point(81, 30)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.Size = New System.Drawing.Size(187, 21)
        Me.txtPosition.TabIndex = 3
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(4, 6)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(74, 15)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(81, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(187, 21)
        Me.txtName.TabIndex = 1
        '
        'objColon1
        '
        Me.objColon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon1.Location = New System.Drawing.Point(113, 119)
        Me.objColon1.Name = "objColon1"
        Me.objColon1.Size = New System.Drawing.Size(8, 15)
        Me.objColon1.TabIndex = 28
        Me.objColon1.Text = ":"
        Me.objColon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpContact
        '
        Me.lblEmpContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContact.Location = New System.Drawing.Point(30, 161)
        Me.lblEmpContact.Name = "lblEmpContact"
        Me.lblEmpContact.Size = New System.Drawing.Size(80, 15)
        Me.lblEmpContact.TabIndex = 33
        Me.lblEmpContact.Text = "Contact No"
        Me.lblEmpContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpCompany
        '
        Me.lblEmpCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCompany.Location = New System.Drawing.Point(30, 140)
        Me.lblEmpCompany.Name = "lblEmpCompany"
        Me.lblEmpCompany.Size = New System.Drawing.Size(80, 15)
        Me.lblEmpCompany.TabIndex = 30
        Me.lblEmpCompany.Text = "Company"
        Me.lblEmpCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(30, 119)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(80, 15)
        Me.lblDepartment.TabIndex = 27
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmployeeContactNo
        '
        Me.objlblEmployeeContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmployeeContactNo.Location = New System.Drawing.Point(127, 161)
        Me.objlblEmployeeContactNo.Name = "objlblEmployeeContactNo"
        Me.objlblEmployeeContactNo.Size = New System.Drawing.Size(150, 15)
        Me.objlblEmployeeContactNo.TabIndex = 35
        Me.objlblEmployeeContactNo.Text = "#Contact No"
        Me.objlblEmployeeContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCompanyValue
        '
        Me.objlblCompanyValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCompanyValue.Location = New System.Drawing.Point(127, 140)
        Me.objlblCompanyValue.Name = "objlblCompanyValue"
        Me.objlblCompanyValue.Size = New System.Drawing.Size(150, 15)
        Me.objlblCompanyValue.TabIndex = 32
        Me.objlblCompanyValue.Text = "#Company"
        Me.objlblCompanyValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblDepartmentValue
        '
        Me.objlblDepartmentValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDepartmentValue.Location = New System.Drawing.Point(127, 119)
        Me.objlblDepartmentValue.Name = "objlblDepartmentValue"
        Me.objlblDepartmentValue.Size = New System.Drawing.Size(150, 15)
        Me.objlblDepartmentValue.TabIndex = 29
        Me.objlblDepartmentValue.Text = "#DepartmentName"
        Me.objlblDepartmentValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(138, 87)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(214, 21)
        Me.cboEmployee.TabIndex = 25
        '
        'radEmployee
        '
        Me.radEmployee.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radEmployee.BackColor = System.Drawing.Color.Transparent
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(427, 4)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(128, 17)
        Me.radEmployee.TabIndex = 24
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = False
        '
        'lblCommitteMaster
        '
        Me.lblCommitteMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommitteMaster.Location = New System.Drawing.Point(6, 36)
        Me.lblCommitteMaster.Name = "lblCommitteMaster"
        Me.lblCommitteMaster.Size = New System.Drawing.Size(127, 15)
        Me.lblCommitteMaster.TabIndex = 1
        Me.lblCommitteMaster.Text = "Disciplinary Committee"
        Me.lblCommitteMaster.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(355, 87)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 26
        '
        'cboCommitteMaster
        '
        Me.cboCommitteMaster.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommitteMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCommitteMaster.FormattingEnabled = True
        Me.cboCommitteMaster.Location = New System.Drawing.Point(138, 33)
        Me.cboCommitteMaster.Name = "cboCommitteMaster"
        Me.cboCommitteMaster.Size = New System.Drawing.Size(214, 21)
        Me.cboCommitteMaster.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbCommitteAddEdit)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(695, 402)
        Me.Panel1.TabIndex = 88
        '
        'frmCommittee_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(695, 457)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommittee_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Committee Members"
        Me.objFooter.ResumeLayout(False)
        Me.gbCommitteAddEdit.ResumeLayout(False)
        Me.pnlOthers.ResumeLayout(False)
        Me.pnlOthers.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbCommitteAddEdit As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCommitteMaster As System.Windows.Forms.Label
    Friend WithEvents cboCommitteMaster As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents radOthers As System.Windows.Forms.RadioButton
    Friend WithEvents pnlOthers As System.Windows.Forms.Panel
    Friend WithEvents lblTrainerContactNo As System.Windows.Forms.Label
    Friend WithEvents txtTrainersContactNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents txtComany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvInvestigatorInfo As eZee.Common.eZeeListView
    Friend WithEvents colhInvestigator As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDeleteInvestigator As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditInvestigator As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddInvestigator As eZee.Common.eZeeLightButton
    Friend WithEvents objColon3 As System.Windows.Forms.Label
    Friend WithEvents objColon2 As System.Windows.Forms.Label
    Friend WithEvents objColon1 As System.Windows.Forms.Label
    Friend WithEvents lblEmpContact As System.Windows.Forms.Label
    Friend WithEvents lblEmpCompany As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objlblEmployeeContactNo As System.Windows.Forms.Label
    Friend WithEvents objlblCompanyValue As System.Windows.Forms.Label
    Friend WithEvents objlblDepartmentValue As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCommittee As eZee.Common.eZeeGradientButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblMembersCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchMCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddMCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents cboMCategory As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddCommittee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents colhMCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents objMCatId As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objColon4 As System.Windows.Forms.Label
    Friend WithEvents lblEmpEmail As System.Windows.Forms.Label
    Friend WithEvents objlblEmployeeEmail As System.Windows.Forms.Label
    Friend WithEvents colhEmail As System.Windows.Forms.ColumnHeader
End Class
