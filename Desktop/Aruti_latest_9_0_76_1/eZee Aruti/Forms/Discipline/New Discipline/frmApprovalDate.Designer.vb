﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApprovalDate
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApprovalDate))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dtpApprovalDate = New System.Windows.Forms.DateTimePicker
        Me.lblApprovalDate = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSaveOnly = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSaveApprove = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dtpApprovalDate)
        Me.Panel1.Controls.Add(Me.lblApprovalDate)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(268, 92)
        Me.Panel1.TabIndex = 0
        '
        'dtpApprovalDate
        '
        Me.dtpApprovalDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApprovalDate.Location = New System.Drawing.Point(108, 13)
        Me.dtpApprovalDate.Name = "dtpApprovalDate"
        Me.dtpApprovalDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpApprovalDate.TabIndex = 88
        '
        'lblApprovalDate
        '
        Me.lblApprovalDate.Location = New System.Drawing.Point(8, 15)
        Me.lblApprovalDate.Name = "lblApprovalDate"
        Me.lblApprovalDate.Size = New System.Drawing.Size(94, 16)
        Me.lblApprovalDate.TabIndex = 87
        Me.lblApprovalDate.Text = "Approval Date"
        Me.lblApprovalDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOk)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 42)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(268, 50)
        Me.objFooter.TabIndex = 86
        '
        'btnOk
        '
        Me.btnOk.BorderColor = System.Drawing.Color.Black
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOk.Location = New System.Drawing.Point(63, 10)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.ShowDefaultBorderColor = True
        Me.btnOk.Size = New System.Drawing.Size(98, 30)
        Me.btnOk.SplitButtonMenu = Me.cmnuOperation
        Me.btnOk.TabIndex = 17
        Me.btnOk.Text = "Operations"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSaveOnly, Me.mnuSaveApprove})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(160, 70)
        '
        'mnuSaveOnly
        '
        Me.mnuSaveOnly.Name = "mnuSaveOnly"
        Me.mnuSaveOnly.Size = New System.Drawing.Size(159, 22)
        Me.mnuSaveOnly.Text = "Save &Only"
        '
        'mnuSaveApprove
        '
        Me.mnuSaveApprove.Name = "mnuSaveApprove"
        Me.mnuSaveApprove.Size = New System.Drawing.Size(159, 22)
        Me.mnuSaveApprove.Text = "Save && Approve"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(167, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(89, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmApprovalDate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(268, 92)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApprovalDate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approval Date"
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dtpApprovalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblApprovalDate As System.Windows.Forms.Label
    Friend WithEvents btnOk As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSaveOnly As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSaveApprove As System.Windows.Forms.ToolStripMenuItem
End Class
