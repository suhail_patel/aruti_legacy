﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmApprovalDate

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmApprovalDate"
    Private mblnCancel As Boolean = True
    Private objDResolution As clsDiscipline_Resolution
    Private mintDisciplineFileId As Integer = -1
    Private mdtApprovalDate As Date
    Private mdtMinimumDate As Date = Nothing
    'S.SANDEEP [ 19 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsFromList As Boolean = False
    'S.SANDEEP [ 19 DEC 2012 ] -- END

#End Region

    'S.SANDEEP [ 19 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Properites "

    Public WriteOnly Property _IsFromList() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromList = value
        End Set
    End Property

#End Region
    'S.SANDEEP [ 19 DEC 2012 ] -- END

#Region " Display Dialog "

    Public Function displayDialog(ByRef dtApprovalDate As Date, ByVal intDisciplineFileId As Integer, ByVal dtMinDate As Date) As Boolean
        Try

            mdtApprovalDate = mdtApprovalDate
            mintDisciplineFileId = intDisciplineFileId
            mdtMinimumDate = dtMinDate

            Me.ShowDialog()

            dtApprovalDate = mdtApprovalDate

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmDisciplineFiling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDResolution = New clsDiscipline_Resolution
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mblnIsFromList = True Then
                mnuSaveOnly.Visible = False
            End If
            mnuSaveApprove.Visible = User._Object.Privilege._AllowToApproveResolutionStep
            'S.SANDEEP [ 19 DEC 2012 ] -- END

            Dim dtMinDate As Date

            If mdtMinimumDate <> Nothing Then
                dtMinDate = mdtMinimumDate
            Else
                dtMinDate = objDResolution.GetMinClosingDate(mintDisciplineFileId)
            End If


            If dtMinDate <> Nothing Then
                dtpApprovalDate.MinDate = dtMinDate
            Else
                dtpApprovalDate.MinDate = mdtMinimumDate
            End If
            dtpApprovalDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFiling_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprovalDate_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objDResolution = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 19 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuSaveOnly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveOnly.Click
        Try
            mdtApprovalDate = Nothing
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuSaveOnly_Click", mstrModuleName)
        End Try
    End Sub

    'Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
    '    Try
    '        mdtApprovalDate = dtpApprovalDate.Value
    '        mblnCancel = False
    '        Me.Close()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub mnuSaveApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveApprove.Click
        Try
            mdtApprovalDate = dtpApprovalDate.Value
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuSaveApprove_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 19 DEC 2012 ] -- END


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor
			
			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblApprovalDate.Text = Language._Object.getCaption(Me.lblApprovalDate.Name, Me.lblApprovalDate.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.mnuSaveOnly.Text = Language._Object.getCaption(Me.mnuSaveOnly.Name, Me.mnuSaveOnly.Text)
			Me.mnuSaveApprove.Text = Language._Object.getCaption(Me.mnuSaveApprove.Name, Me.mnuSaveApprove.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class