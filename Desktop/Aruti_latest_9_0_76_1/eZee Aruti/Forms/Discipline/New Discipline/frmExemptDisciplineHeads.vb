﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExemptDisciplineHeads

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineResolution"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintEmployeeUnkid As Integer = -1
    Private mintDisciplineUnkid As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mstrIncident As String = String.Empty
    Private objEmpExemption As clsemployee_exemption_Tran

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFileUnkid As Integer, _
                                  ByVal strEmployee As String, _
                                  ByVal intIEmpId As Integer, _
                                  ByVal strIncidentCaused As String, _
                                  ByVal eAction As enAction) As Boolean
        Try
            mintDisciplineUnkid = intFileUnkid
            mintEmployeeUnkid = intIEmpId
            mstrEmployeeName = strEmployee
            mstrIncident = strIncidentCaused
            menAction = eAction

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtIncident.BackColor = GUI.ColorComp
            txtPesonInvolved.BackColor = GUI.ColorComp
            cboTransactionhead.BackColor = GUI.ColorComp
            cboTrnHeadType.BackColor = GUI.ColorComp
            cboTypeOf.BackColor = GUI.ColorComp
            cboYear.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validattion() As Boolean
        Try
            If CInt(cboYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
                cboYear.Focus()
                Return True
            ElseIf lvPeriods.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                Return True
            ElseIf CInt(cboTransactionhead.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Transaction Head is compulsory information.Please Select Transaction Head."), enMsgBoxStyle.Information)
                cboTransactionhead.Focus()
                Return True
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validattion", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Dim objMaster As New clsMasterData
        Dim dtTable As DataTable
        Try
            Dim objyear As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objyear.getComboListPAYYEAR("year", True)
            dsList = objyear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, "year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("year")
                .SelectedValue = 0
            End With

            dsList = objMaster.getComboListForHeadType("HeadType")
            dtTable = New DataView(dsList.Tables("HeadType"), "Id IN(0," & enTranHeadType.EarningForEmployees & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objMaster = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmExemptDisciplineHeads_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpExemption = New clsemployee_exemption_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()
            lvPeriods.GridLines = False
            SetColor()
            FillCombo()
            txtPesonInvolved.Text = mstrEmployeeName
            txtIncident.Text = mstrIncident
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExemptDisciplineHeads_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExemptDisciplineHeads_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExemptDisciplineHeads_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExemptDisciplineHeads_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmpExemption = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_exemption_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_exemption_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim strPeriodList As String = ""
        Dim strEmpList As String = ""
        Try
            If Validattion() = False Then Exit Sub

            If lvPeriods.CheckedItems.Count > 1 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to Exempt this Transaction Head for all selected Period?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If

            For Each lvItem As ListViewItem In lvPeriods.CheckedItems
                strPeriodList &= "," & lvItem.Tag.ToString
            Next
            strPeriodList = Mid(strPeriodList, 2)

            strEmpList = mintEmployeeUnkid.ToString

            If menAction = enAction.ADD_ONE Then

                objEmpExemption._Yearunkid = FinancialYear._Object._YearUnkid
                objEmpExemption._DisciplineFileUnkid = CInt(IIf(mintDisciplineUnkid <= 0, 0, mintDisciplineUnkid))
                objEmpExemption._Tranheadunkid = CInt(cboTransactionhead.SelectedValue)
                objEmpExemption._Isapproved = User._Object.Privilege._AllowToApproveEmpExemtion
                If objEmpExemption._Isapproved = True Then
                    objEmpExemption._Approveruserunkid = User._Object._Userunkid
                End If
                objEmpExemption._Userunkid = User._Object._Userunkid

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objEmpExemption.InsertByPeriod(strEmpList, strPeriodList)
                blnFlag = objEmpExemption.InsertByPeriod(strEmpList, strPeriodList, ConfigParameter._Object._CurrentDateAndTime)
                'S.SANDEEP [04 JUN 2015] -- END

            End If

            If blnFlag = False And objEmpExemption._Message <> "" Then
                eZeeMsgBox.Show(objEmpExemption._Message, enMsgBoxStyle.Information)
            Else
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objSearchTransactionhead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchTransactionhead.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboTransactionhead.DisplayMember
                .ValueMember = cboTransactionhead.ValueMember
                .DataSource = CType(cboTransactionhead.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If frm.DisplayDialog Then
                cboTransactionhead.SelectedValue = frm.SelectedValue
                cboTransactionhead.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchTransactionhead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Evenst's "

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            Dim dsFill As DataSet = Nothing
            Dim objPeriod As New clscommom_period_Tran
            lvPeriods.Items.Clear()
            If CInt(cboYear.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Open)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
                dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2015) -- End
                Dim lvItem As ListViewItem
                RemoveHandler lvPeriods.ItemChecked, AddressOf lvPeriods_ItemChecked
                RemoveHandler objckhAll.CheckedChanged, AddressOf objckhAll_CheckedChanged
                For Each dsRow As DataRow In dsFill.Tables("Period").Rows
                    lvItem = New ListViewItem

                    lvItem.Tag = dsRow.Item("periodunkid").ToString
                    lvItem.SubItems.Add(eZeeDate.convertDate(dsRow.Item("start_date").ToString).ToShortDateString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(dsRow.Item("end_date").ToString).ToShortDateString)
                    lvItem.SubItems.Add(dsRow.Item("period_name").ToString)

                    lvPeriods.Items.Add(lvItem)
                Next
                lvPeriods.GroupingColumn = objcolhPeriodName
                lvPeriods.DisplayGroups(True)
                lvPeriods.GridLines = False
                objPeriod = Nothing
                dsFill.Dispose()
                AddHandler lvPeriods.ItemChecked, AddressOf lvPeriods_ItemChecked
                AddHandler objckhAll.CheckedChanged, AddressOf objckhAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            Dim objMaster As New clsMasterData
            Dim dsList As DataSet : Dim dtTable As DataTable

            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue))
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id <>  " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads

            With cboTypeOf
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            objMaster = Nothing : dsList.Dispose()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged
        Try
            Dim objTranHead As New clsTransactionHead
            Dim dsList As DataSet : Dim dtTable As DataTable

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(cboTypeOf.SelectedValue))
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(cboTypeOf.SelectedValue))
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads
            With cboTransactionhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            objTranHead = Nothing : dsList.Dispose()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objckhAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objckhAll.CheckedChanged
        Try
            RemoveHandler lvPeriods.ItemChecked, AddressOf lvPeriods_ItemChecked
            For Each lvItem As ListViewItem In lvPeriods.Items
                lvItem.Checked = CBool(objckhAll.CheckState)
            Next
            AddHandler lvPeriods.ItemChecked, AddressOf lvPeriods_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objckhAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvPeriods_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriods.ItemChecked
        Try
            RemoveHandler objckhAll.CheckedChanged, AddressOf objckhAll_CheckedChanged
            If lvPeriods.CheckedItems.Count <= 0 Then
                objckhAll.CheckState = CheckState.Unchecked
            ElseIf lvPeriods.CheckedItems.Count < lvPeriods.Items.Count Then
                objckhAll.CheckState = CheckState.Indeterminate
            ElseIf lvPeriods.CheckedItems.Count = lvPeriods.Items.Count Then
                objckhAll.CheckState = CheckState.Checked
            End If
            AddHandler objckhAll.CheckedChanged, AddressOf objckhAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriods_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbExemptHeadInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbExemptHeadInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbExemptHeadInfo.Text = Language._Object.getCaption(Me.gbExemptHeadInfo.Name, Me.gbExemptHeadInfo.Text)
			Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
			Me.lblTransactionhead.Text = Language._Object.getCaption(Me.lblTransactionhead.Name, Me.lblTransactionhead.Text)
			Me.lblIncident.Text = Language._Object.getCaption(Me.lblIncident.Name, Me.lblIncident.Text)
			Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
			Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
			Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.Name, Me.lblTypeOf.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Year is compulsory information.Please Select Year.")
			Language.setMessage(mstrModuleName, 2, "Period is compulsory information.Please Select Period.")
			Language.setMessage(mstrModuleName, 3, "Transaction Head is compulsory information.Please Select Transaction Head.")
			Language.setMessage(mstrModuleName, 4, "Are you sure you want to Exempt this Transaction Head for all selected Period?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class