﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineResolution
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineResolution))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.fpnlFlow = New System.Windows.Forms.FlowLayoutPanel
        Me.gbPersonInvolved = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOffence = New eZee.TextBox.AlphanumericTextBox
        Me.lblOffence = New System.Windows.Forms.Label
        Me.txtOffenceCategory = New eZee.TextBox.AlphanumericTextBox
        Me.lblOffenceCategory = New System.Windows.Forms.Label
        Me.lblIncident = New System.Windows.Forms.Label
        Me.txtIncident = New eZee.TextBox.AlphanumericTextBox
        Me.txtPesonInvolved = New eZee.TextBox.AlphanumericTextBox
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.gbResolutionSteps = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lblDate = New System.Windows.Forms.Label
        Me.objbtnAddStatus = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCommittee = New eZee.Common.eZeeGradientButton
        Me.objbtnAddAction = New eZee.Common.eZeeGradientButton
        Me.lblDisciplinaryAction = New System.Windows.Forms.Label
        Me.tabcDisciplineResoution = New System.Windows.Forms.TabControl
        Me.tabpResolution = New System.Windows.Forms.TabPage
        Me.txtResolutionSteps = New eZee.TextBox.AlphanumericTextBox
        Me.tabpRemark = New System.Windows.Forms.TabPage
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.cboDisciplinaryAction = New System.Windows.Forms.ComboBox
        Me.lblCommitee = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.dtpDisciplineDate = New System.Windows.Forms.DateTimePicker
        Me.cboDisciplineStatus = New System.Windows.Forms.ComboBox
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.cboCommittee = New System.Windows.Forms.ComboBox
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.txtCourtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCaseNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCourtName = New System.Windows.Forms.Label
        Me.lblCaseNo = New System.Windows.Forms.Label
        Me.gbInvestigatorInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlView = New System.Windows.Forms.Panel
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objlblDepartmentValue = New System.Windows.Forms.Label
        Me.objlblCompanyValue = New System.Windows.Forms.Label
        Me.objlblEmployeeContactNo = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.pnlOthers = New System.Windows.Forms.Panel
        Me.lblTrainerContactNo = New System.Windows.Forms.Label
        Me.txtTrainersContactNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblPosition = New System.Windows.Forms.Label
        Me.txtComany = New eZee.TextBox.AlphanumericTextBox
        Me.txtPosition = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmpCompany = New System.Windows.Forms.Label
        Me.objColon3 = New System.Windows.Forms.Label
        Me.lblEmpContact = New System.Windows.Forms.Label
        Me.objColon2 = New System.Windows.Forms.Label
        Me.objColon1 = New System.Windows.Forms.Label
        Me.radOthers = New System.Windows.Forms.RadioButton
        Me.lvInvestigatorInfo = New eZee.Common.eZeeListView(Me.components)
        Me.colhInvestigator = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhContactNo = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsCommittee = New System.Windows.Forms.ColumnHeader
        Me.txtInvestigatorRemark = New eZee.TextBox.AlphanumericTextBox
        Me.btnDeleteInvestigator = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.btnEditInvestigator = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblInvestigatorRemark = New System.Windows.Forms.Label
        Me.btnAddInvestigator = New eZee.Common.eZeeLightButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.fpnlFlow.SuspendLayout()
        Me.gbPersonInvolved.SuspendLayout()
        Me.gbResolutionSteps.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.tabcDisciplineResoution.SuspendLayout()
        Me.tabpResolution.SuspendLayout()
        Me.tabpRemark.SuspendLayout()
        Me.gbInvestigatorInfo.SuspendLayout()
        Me.pnlView.SuspendLayout()
        Me.pnlOthers.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.fpnlFlow)
        Me.pnlMain.Controls.Add(Me.gbInvestigatorInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(907, 556)
        Me.pnlMain.TabIndex = 0
        '
        'fpnlFlow
        '
        Me.fpnlFlow.AutoScroll = True
        Me.fpnlFlow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.fpnlFlow.Controls.Add(Me.gbPersonInvolved)
        Me.fpnlFlow.Controls.Add(Me.gbResolutionSteps)
        Me.fpnlFlow.Location = New System.Drawing.Point(3, 3)
        Me.fpnlFlow.Name = "fpnlFlow"
        Me.fpnlFlow.Size = New System.Drawing.Size(364, 496)
        Me.fpnlFlow.TabIndex = 93
        '
        'gbPersonInvolved
        '
        Me.gbPersonInvolved.BorderColor = System.Drawing.Color.Black
        Me.gbPersonInvolved.Checked = False
        Me.gbPersonInvolved.CollapseAllExceptThis = False
        Me.gbPersonInvolved.CollapsedHoverImage = Nothing
        Me.gbPersonInvolved.CollapsedNormalImage = Nothing
        Me.gbPersonInvolved.CollapsedPressedImage = Nothing
        Me.gbPersonInvolved.CollapseOnLoad = False
        Me.gbPersonInvolved.Controls.Add(Me.txtOffence)
        Me.gbPersonInvolved.Controls.Add(Me.lblOffence)
        Me.gbPersonInvolved.Controls.Add(Me.txtOffenceCategory)
        Me.gbPersonInvolved.Controls.Add(Me.lblOffenceCategory)
        Me.gbPersonInvolved.Controls.Add(Me.lblIncident)
        Me.gbPersonInvolved.Controls.Add(Me.txtIncident)
        Me.gbPersonInvolved.Controls.Add(Me.txtPesonInvolved)
        Me.gbPersonInvolved.Controls.Add(Me.lblPersonalInvolved)
        Me.gbPersonInvolved.ExpandedHoverImage = Nothing
        Me.gbPersonInvolved.ExpandedNormalImage = Nothing
        Me.gbPersonInvolved.ExpandedPressedImage = Nothing
        Me.gbPersonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPersonInvolved.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPersonInvolved.HeaderHeight = 25
        Me.gbPersonInvolved.HeaderMessage = ""
        Me.gbPersonInvolved.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPersonInvolved.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPersonInvolved.HeightOnCollapse = 0
        Me.gbPersonInvolved.LeftTextSpace = 0
        Me.gbPersonInvolved.Location = New System.Drawing.Point(3, 3)
        Me.gbPersonInvolved.Name = "gbPersonInvolved"
        Me.gbPersonInvolved.OpenHeight = 199
        Me.gbPersonInvolved.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPersonInvolved.ShowBorder = True
        Me.gbPersonInvolved.ShowCheckBox = False
        Me.gbPersonInvolved.ShowCollapseButton = False
        Me.gbPersonInvolved.ShowDefaultBorderColor = True
        Me.gbPersonInvolved.ShowDownButton = False
        Me.gbPersonInvolved.ShowHeader = True
        Me.gbPersonInvolved.Size = New System.Drawing.Size(339, 199)
        Me.gbPersonInvolved.TabIndex = 85
        Me.gbPersonInvolved.Temp = 0
        Me.gbPersonInvolved.Text = "Involved Person"
        Me.gbPersonInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOffence
        '
        Me.txtOffence.BackColor = System.Drawing.SystemColors.Window
        Me.txtOffence.Flags = 0
        Me.txtOffence.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOffence.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOffence.Location = New System.Drawing.Point(110, 87)
        Me.txtOffence.Multiline = True
        Me.txtOffence.Name = "txtOffence"
        Me.txtOffence.ReadOnly = True
        Me.txtOffence.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOffence.Size = New System.Drawing.Size(214, 50)
        Me.txtOffence.TabIndex = 94
        '
        'lblOffence
        '
        Me.lblOffence.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffence.Location = New System.Drawing.Point(5, 92)
        Me.lblOffence.Name = "lblOffence"
        Me.lblOffence.Size = New System.Drawing.Size(100, 15)
        Me.lblOffence.TabIndex = 93
        Me.lblOffence.Text = "Offence"
        Me.lblOffence.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOffenceCategory
        '
        Me.txtOffenceCategory.BackColor = System.Drawing.SystemColors.Window
        Me.txtOffenceCategory.Flags = 0
        Me.txtOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOffenceCategory.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOffenceCategory.Location = New System.Drawing.Point(110, 60)
        Me.txtOffenceCategory.Name = "txtOffenceCategory"
        Me.txtOffenceCategory.ReadOnly = True
        Me.txtOffenceCategory.Size = New System.Drawing.Size(214, 21)
        Me.txtOffenceCategory.TabIndex = 92
        '
        'lblOffenceCategory
        '
        Me.lblOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffenceCategory.Location = New System.Drawing.Point(5, 63)
        Me.lblOffenceCategory.Name = "lblOffenceCategory"
        Me.lblOffenceCategory.Size = New System.Drawing.Size(100, 15)
        Me.lblOffenceCategory.TabIndex = 91
        Me.lblOffenceCategory.Text = "Offence Category"
        Me.lblOffenceCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIncident
        '
        Me.lblIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncident.Location = New System.Drawing.Point(5, 147)
        Me.lblIncident.Name = "lblIncident"
        Me.lblIncident.Size = New System.Drawing.Size(100, 15)
        Me.lblIncident.TabIndex = 89
        Me.lblIncident.Text = "Incident" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lblIncident.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIncident
        '
        Me.txtIncident.BackColor = System.Drawing.Color.White
        Me.txtIncident.Flags = 0
        Me.txtIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncident.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtIncident.Location = New System.Drawing.Point(110, 143)
        Me.txtIncident.Multiline = True
        Me.txtIncident.Name = "txtIncident"
        Me.txtIncident.ReadOnly = True
        Me.txtIncident.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtIncident.Size = New System.Drawing.Size(214, 50)
        Me.txtIncident.TabIndex = 88
        '
        'txtPesonInvolved
        '
        Me.txtPesonInvolved.BackColor = System.Drawing.SystemColors.Window
        Me.txtPesonInvolved.Flags = 0
        Me.txtPesonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPesonInvolved.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPesonInvolved.Location = New System.Drawing.Point(110, 33)
        Me.txtPesonInvolved.Name = "txtPesonInvolved"
        Me.txtPesonInvolved.ReadOnly = True
        Me.txtPesonInvolved.Size = New System.Drawing.Size(214, 21)
        Me.txtPesonInvolved.TabIndex = 83
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(5, 36)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(100, 15)
        Me.lblPersonalInvolved.TabIndex = 82
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbResolutionSteps
        '
        Me.gbResolutionSteps.BorderColor = System.Drawing.Color.Black
        Me.gbResolutionSteps.Checked = False
        Me.gbResolutionSteps.CollapseAllExceptThis = False
        Me.gbResolutionSteps.CollapsedHoverImage = Nothing
        Me.gbResolutionSteps.CollapsedNormalImage = Nothing
        Me.gbResolutionSteps.CollapsedPressedImage = Nothing
        Me.gbResolutionSteps.CollapseOnLoad = False
        Me.gbResolutionSteps.Controls.Add(Me.pnlData)
        Me.gbResolutionSteps.Controls.Add(Me.txtCourtName)
        Me.gbResolutionSteps.Controls.Add(Me.txtCaseNo)
        Me.gbResolutionSteps.Controls.Add(Me.lblCourtName)
        Me.gbResolutionSteps.Controls.Add(Me.lblCaseNo)
        Me.gbResolutionSteps.ExpandedHoverImage = Nothing
        Me.gbResolutionSteps.ExpandedNormalImage = Nothing
        Me.gbResolutionSteps.ExpandedPressedImage = Nothing
        Me.gbResolutionSteps.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbResolutionSteps.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbResolutionSteps.HeaderHeight = 25
        Me.gbResolutionSteps.HeaderMessage = ""
        Me.gbResolutionSteps.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbResolutionSteps.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbResolutionSteps.HeightOnCollapse = 0
        Me.gbResolutionSteps.LeftTextSpace = 0
        Me.gbResolutionSteps.Location = New System.Drawing.Point(3, 208)
        Me.gbResolutionSteps.Name = "gbResolutionSteps"
        Me.gbResolutionSteps.OpenHeight = 314
        Me.gbResolutionSteps.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbResolutionSteps.ShowBorder = True
        Me.gbResolutionSteps.ShowCheckBox = False
        Me.gbResolutionSteps.ShowCollapseButton = False
        Me.gbResolutionSteps.ShowDefaultBorderColor = True
        Me.gbResolutionSteps.ShowDownButton = False
        Me.gbResolutionSteps.ShowHeader = True
        Me.gbResolutionSteps.Size = New System.Drawing.Size(339, 339)
        Me.gbResolutionSteps.TabIndex = 84
        Me.gbResolutionSteps.Temp = 0
        Me.gbResolutionSteps.Text = "Proceeding Steps"
        Me.gbResolutionSteps.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.lblDate)
        Me.pnlData.Controls.Add(Me.objbtnAddStatus)
        Me.pnlData.Controls.Add(Me.objbtnSearchCommittee)
        Me.pnlData.Controls.Add(Me.objbtnAddAction)
        Me.pnlData.Controls.Add(Me.lblDisciplinaryAction)
        Me.pnlData.Controls.Add(Me.tabcDisciplineResoution)
        Me.pnlData.Controls.Add(Me.cboDisciplinaryAction)
        Me.pnlData.Controls.Add(Me.lblCommitee)
        Me.pnlData.Controls.Add(Me.lblStatus)
        Me.pnlData.Controls.Add(Me.dtpDisciplineDate)
        Me.pnlData.Controls.Add(Me.cboDisciplineStatus)
        Me.pnlData.Controls.Add(Me.dtpStartDate)
        Me.pnlData.Controls.Add(Me.dtpEndDate)
        Me.pnlData.Controls.Add(Me.lblEndDate)
        Me.pnlData.Controls.Add(Me.cboCommittee)
        Me.pnlData.Controls.Add(Me.lblStartDate)
        Me.pnlData.Location = New System.Drawing.Point(5, 85)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(332, 250)
        Me.pnlData.TabIndex = 111
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(3, 5)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(78, 15)
        Me.lblDate.TabIndex = 103
        Me.lblDate.Text = "Trans.  Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddStatus
        '
        Me.objbtnAddStatus.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddStatus.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddStatus.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddStatus.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddStatus.BorderSelected = False
        Me.objbtnAddStatus.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddStatus.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddStatus.Location = New System.Drawing.Point(306, 110)
        Me.objbtnAddStatus.Name = "objbtnAddStatus"
        Me.objbtnAddStatus.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddStatus.TabIndex = 94
        '
        'objbtnSearchCommittee
        '
        Me.objbtnSearchCommittee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCommittee.BorderSelected = False
        Me.objbtnSearchCommittee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCommittee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCommittee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCommittee.Location = New System.Drawing.Point(306, 29)
        Me.objbtnSearchCommittee.Name = "objbtnSearchCommittee"
        Me.objbtnSearchCommittee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCommittee.TabIndex = 108
        '
        'objbtnAddAction
        '
        Me.objbtnAddAction.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddAction.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddAction.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddAction.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddAction.BorderSelected = False
        Me.objbtnAddAction.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddAction.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddAction.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddAction.Location = New System.Drawing.Point(306, 56)
        Me.objbtnAddAction.Name = "objbtnAddAction"
        Me.objbtnAddAction.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddAction.TabIndex = 85
        '
        'lblDisciplinaryAction
        '
        Me.lblDisciplinaryAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplinaryAction.Location = New System.Drawing.Point(3, 59)
        Me.lblDisciplinaryAction.Name = "lblDisciplinaryAction"
        Me.lblDisciplinaryAction.Size = New System.Drawing.Size(78, 15)
        Me.lblDisciplinaryAction.TabIndex = 84
        Me.lblDisciplinaryAction.Text = "Discip. Action"
        Me.lblDisciplinaryAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcDisciplineResoution
        '
        Me.tabcDisciplineResoution.Controls.Add(Me.tabpResolution)
        Me.tabcDisciplineResoution.Controls.Add(Me.tabpRemark)
        Me.tabcDisciplineResoution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcDisciplineResoution.Location = New System.Drawing.Point(3, 139)
        Me.tabcDisciplineResoution.Multiline = True
        Me.tabcDisciplineResoution.Name = "tabcDisciplineResoution"
        Me.tabcDisciplineResoution.SelectedIndex = 0
        Me.tabcDisciplineResoution.Size = New System.Drawing.Size(316, 110)
        Me.tabcDisciplineResoution.TabIndex = 105
        '
        'tabpResolution
        '
        Me.tabpResolution.Controls.Add(Me.txtResolutionSteps)
        Me.tabpResolution.Location = New System.Drawing.Point(4, 22)
        Me.tabpResolution.Margin = New System.Windows.Forms.Padding(0)
        Me.tabpResolution.Name = "tabpResolution"
        Me.tabpResolution.Size = New System.Drawing.Size(308, 84)
        Me.tabpResolution.TabIndex = 0
        Me.tabpResolution.Text = "Proceeding Steps"
        Me.tabpResolution.UseVisualStyleBackColor = True
        '
        'txtResolutionSteps
        '
        Me.txtResolutionSteps.BackColor = System.Drawing.SystemColors.Window
        Me.txtResolutionSteps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtResolutionSteps.Flags = 0
        Me.txtResolutionSteps.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResolutionSteps.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtResolutionSteps.Location = New System.Drawing.Point(0, 0)
        Me.txtResolutionSteps.Multiline = True
        Me.txtResolutionSteps.Name = "txtResolutionSteps"
        Me.txtResolutionSteps.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtResolutionSteps.Size = New System.Drawing.Size(308, 84)
        Me.txtResolutionSteps.TabIndex = 96
        '
        'tabpRemark
        '
        Me.tabpRemark.Controls.Add(Me.txtRemark)
        Me.tabpRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpRemark.Margin = New System.Windows.Forms.Padding(0)
        Me.tabpRemark.Name = "tabpRemark"
        Me.tabpRemark.Size = New System.Drawing.Size(308, 84)
        Me.tabpRemark.TabIndex = 1
        Me.tabpRemark.Text = "Remarks"
        Me.tabpRemark.UseVisualStyleBackColor = True
        '
        'txtRemark
        '
        Me.txtRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(308, 84)
        Me.txtRemark.TabIndex = 95
        '
        'cboDisciplinaryAction
        '
        Me.cboDisciplinaryAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplinaryAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplinaryAction.FormattingEnabled = True
        Me.cboDisciplinaryAction.Location = New System.Drawing.Point(85, 56)
        Me.cboDisciplinaryAction.Name = "cboDisciplinaryAction"
        Me.cboDisciplinaryAction.Size = New System.Drawing.Size(215, 21)
        Me.cboDisciplinaryAction.TabIndex = 83
        '
        'lblCommitee
        '
        Me.lblCommitee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommitee.Location = New System.Drawing.Point(3, 32)
        Me.lblCommitee.Name = "lblCommitee"
        Me.lblCommitee.Size = New System.Drawing.Size(78, 15)
        Me.lblCommitee.TabIndex = 109
        Me.lblCommitee.Text = "Commitee"
        Me.lblCommitee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(3, 113)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(78, 15)
        Me.lblStatus.TabIndex = 92
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDisciplineDate
        '
        Me.dtpDisciplineDate.Checked = False
        Me.dtpDisciplineDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDisciplineDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDisciplineDate.Location = New System.Drawing.Point(85, 2)
        Me.dtpDisciplineDate.Name = "dtpDisciplineDate"
        Me.dtpDisciplineDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpDisciplineDate.TabIndex = 102
        '
        'cboDisciplineStatus
        '
        Me.cboDisciplineStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineStatus.FormattingEnabled = True
        Me.cboDisciplineStatus.Location = New System.Drawing.Point(85, 110)
        Me.cboDisciplineStatus.Name = "cboDisciplineStatus"
        Me.cboDisciplineStatus.Size = New System.Drawing.Size(215, 21)
        Me.cboDisciplineStatus.TabIndex = 93
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(85, 83)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpStartDate.TabIndex = 88
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(210, 83)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(90, 21)
        Me.dtpEndDate.TabIndex = 89
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(180, 86)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(26, 15)
        Me.lblEndDate.TabIndex = 91
        Me.lblEndDate.Text = "To"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCommittee
        '
        Me.cboCommittee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommittee.DropDownWidth = 250
        Me.cboCommittee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCommittee.FormattingEnabled = True
        Me.cboCommittee.Location = New System.Drawing.Point(85, 29)
        Me.cboCommittee.Name = "cboCommittee"
        Me.cboCommittee.Size = New System.Drawing.Size(215, 21)
        Me.cboCommittee.TabIndex = 107
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(3, 86)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(78, 15)
        Me.lblStartDate.TabIndex = 90
        Me.lblStartDate.Text = "Action Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCourtName
        '
        Me.txtCourtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtCourtName.Flags = 0
        Me.txtCourtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCourtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCourtName.Location = New System.Drawing.Point(90, 60)
        Me.txtCourtName.Name = "txtCourtName"
        Me.txtCourtName.Size = New System.Drawing.Size(215, 21)
        Me.txtCourtName.TabIndex = 115
        '
        'txtCaseNo
        '
        Me.txtCaseNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtCaseNo.Flags = 0
        Me.txtCaseNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaseNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCaseNo.Location = New System.Drawing.Point(90, 33)
        Me.txtCaseNo.Name = "txtCaseNo"
        Me.txtCaseNo.Size = New System.Drawing.Size(121, 21)
        Me.txtCaseNo.TabIndex = 114
        '
        'lblCourtName
        '
        Me.lblCourtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourtName.Location = New System.Drawing.Point(9, 63)
        Me.lblCourtName.Name = "lblCourtName"
        Me.lblCourtName.Size = New System.Drawing.Size(76, 15)
        Me.lblCourtName.TabIndex = 113
        Me.lblCourtName.Text = "Court Name"
        Me.lblCourtName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaseNo
        '
        Me.lblCaseNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaseNo.Location = New System.Drawing.Point(8, 36)
        Me.lblCaseNo.Name = "lblCaseNo"
        Me.lblCaseNo.Size = New System.Drawing.Size(76, 15)
        Me.lblCaseNo.TabIndex = 112
        Me.lblCaseNo.Text = "Case No."
        Me.lblCaseNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbInvestigatorInfo
        '
        Me.gbInvestigatorInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInvestigatorInfo.Checked = False
        Me.gbInvestigatorInfo.CollapseAllExceptThis = False
        Me.gbInvestigatorInfo.CollapsedHoverImage = Nothing
        Me.gbInvestigatorInfo.CollapsedNormalImage = Nothing
        Me.gbInvestigatorInfo.CollapsedPressedImage = Nothing
        Me.gbInvestigatorInfo.CollapseOnLoad = False
        Me.gbInvestigatorInfo.Controls.Add(Me.pnlView)
        Me.gbInvestigatorInfo.Controls.Add(Me.lvInvestigatorInfo)
        Me.gbInvestigatorInfo.Controls.Add(Me.txtInvestigatorRemark)
        Me.gbInvestigatorInfo.Controls.Add(Me.btnDeleteInvestigator)
        Me.gbInvestigatorInfo.Controls.Add(Me.objelLine1)
        Me.gbInvestigatorInfo.Controls.Add(Me.btnEditInvestigator)
        Me.gbInvestigatorInfo.Controls.Add(Me.lblInvestigatorRemark)
        Me.gbInvestigatorInfo.Controls.Add(Me.btnAddInvestigator)
        Me.gbInvestigatorInfo.ExpandedHoverImage = Nothing
        Me.gbInvestigatorInfo.ExpandedNormalImage = Nothing
        Me.gbInvestigatorInfo.ExpandedPressedImage = Nothing
        Me.gbInvestigatorInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInvestigatorInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInvestigatorInfo.HeaderHeight = 25
        Me.gbInvestigatorInfo.HeaderMessage = ""
        Me.gbInvestigatorInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInvestigatorInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInvestigatorInfo.HeightOnCollapse = 0
        Me.gbInvestigatorInfo.LeftTextSpace = 0
        Me.gbInvestigatorInfo.Location = New System.Drawing.Point(370, 3)
        Me.gbInvestigatorInfo.Name = "gbInvestigatorInfo"
        Me.gbInvestigatorInfo.OpenHeight = 300
        Me.gbInvestigatorInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInvestigatorInfo.ShowBorder = True
        Me.gbInvestigatorInfo.ShowCheckBox = False
        Me.gbInvestigatorInfo.ShowCollapseButton = False
        Me.gbInvestigatorInfo.ShowDefaultBorderColor = True
        Me.gbInvestigatorInfo.ShowDownButton = False
        Me.gbInvestigatorInfo.ShowHeader = True
        Me.gbInvestigatorInfo.Size = New System.Drawing.Size(534, 496)
        Me.gbInvestigatorInfo.TabIndex = 92
        Me.gbInvestigatorInfo.Temp = 0
        Me.gbInvestigatorInfo.Text = "Investigator Information"
        Me.gbInvestigatorInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlView
        '
        Me.pnlView.Controls.Add(Me.cboEmployee)
        Me.pnlView.Controls.Add(Me.radEmployee)
        Me.pnlView.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlView.Controls.Add(Me.objlblDepartmentValue)
        Me.pnlView.Controls.Add(Me.objlblCompanyValue)
        Me.pnlView.Controls.Add(Me.objlblEmployeeContactNo)
        Me.pnlView.Controls.Add(Me.lblDepartment)
        Me.pnlView.Controls.Add(Me.pnlOthers)
        Me.pnlView.Controls.Add(Me.lblEmpCompany)
        Me.pnlView.Controls.Add(Me.objColon3)
        Me.pnlView.Controls.Add(Me.lblEmpContact)
        Me.pnlView.Controls.Add(Me.objColon2)
        Me.pnlView.Controls.Add(Me.objColon1)
        Me.pnlView.Controls.Add(Me.radOthers)
        Me.pnlView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlView.Location = New System.Drawing.Point(3, 26)
        Me.pnlView.Name = "pnlView"
        Me.pnlView.Size = New System.Drawing.Size(528, 114)
        Me.pnlView.TabIndex = 101
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(89, 9)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(152, 21)
        Me.cboEmployee.TabIndex = 13
        '
        'radEmployee
        '
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(5, 11)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(78, 17)
        Me.radEmployee.TabIndex = 12
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(247, 9)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 14
        '
        'objlblDepartmentValue
        '
        Me.objlblDepartmentValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDepartmentValue.Location = New System.Drawing.Point(111, 35)
        Me.objlblDepartmentValue.Name = "objlblDepartmentValue"
        Me.objlblDepartmentValue.Size = New System.Drawing.Size(139, 15)
        Me.objlblDepartmentValue.TabIndex = 17
        Me.objlblDepartmentValue.Text = "#DepartmentName"
        Me.objlblDepartmentValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCompanyValue
        '
        Me.objlblCompanyValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCompanyValue.Location = New System.Drawing.Point(111, 52)
        Me.objlblCompanyValue.Name = "objlblCompanyValue"
        Me.objlblCompanyValue.Size = New System.Drawing.Size(139, 15)
        Me.objlblCompanyValue.TabIndex = 20
        Me.objlblCompanyValue.Text = "#Company"
        Me.objlblCompanyValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmployeeContactNo
        '
        Me.objlblEmployeeContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmployeeContactNo.Location = New System.Drawing.Point(111, 71)
        Me.objlblEmployeeContactNo.Name = "objlblEmployeeContactNo"
        Me.objlblEmployeeContactNo.Size = New System.Drawing.Size(139, 15)
        Me.objlblEmployeeContactNo.TabIndex = 23
        Me.objlblEmployeeContactNo.Text = "#Contact No"
        Me.objlblEmployeeContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(14, 35)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(77, 15)
        Me.lblDepartment.TabIndex = 15
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlOthers
        '
        Me.pnlOthers.Controls.Add(Me.lblTrainerContactNo)
        Me.pnlOthers.Controls.Add(Me.txtTrainersContactNo)
        Me.pnlOthers.Controls.Add(Me.lblCompany)
        Me.pnlOthers.Controls.Add(Me.lblPosition)
        Me.pnlOthers.Controls.Add(Me.txtComany)
        Me.pnlOthers.Controls.Add(Me.txtPosition)
        Me.pnlOthers.Controls.Add(Me.lblName)
        Me.pnlOthers.Controls.Add(Me.txtName)
        Me.pnlOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOthers.Location = New System.Drawing.Point(271, 5)
        Me.pnlOthers.Name = "pnlOthers"
        Me.pnlOthers.Size = New System.Drawing.Size(255, 107)
        Me.pnlOthers.TabIndex = 25
        '
        'lblTrainerContactNo
        '
        Me.lblTrainerContactNo.Location = New System.Drawing.Point(5, 87)
        Me.lblTrainerContactNo.Name = "lblTrainerContactNo"
        Me.lblTrainerContactNo.Size = New System.Drawing.Size(72, 15)
        Me.lblTrainerContactNo.TabIndex = 6
        Me.lblTrainerContactNo.Text = "Contact No"
        Me.lblTrainerContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTrainersContactNo
        '
        Me.txtTrainersContactNo.Flags = 0
        Me.txtTrainersContactNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTrainersContactNo.Location = New System.Drawing.Point(81, 84)
        Me.txtTrainersContactNo.Name = "txtTrainersContactNo"
        Me.txtTrainersContactNo.Size = New System.Drawing.Size(172, 21)
        Me.txtTrainersContactNo.TabIndex = 7
        '
        'lblCompany
        '
        Me.lblCompany.Location = New System.Drawing.Point(5, 60)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(72, 15)
        Me.lblCompany.TabIndex = 4
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition
        '
        Me.lblPosition.Location = New System.Drawing.Point(5, 33)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(72, 15)
        Me.lblPosition.TabIndex = 2
        Me.lblPosition.Text = "Department"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComany
        '
        Me.txtComany.Flags = 0
        Me.txtComany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtComany.Location = New System.Drawing.Point(81, 57)
        Me.txtComany.Name = "txtComany"
        Me.txtComany.Size = New System.Drawing.Size(172, 21)
        Me.txtComany.TabIndex = 5
        '
        'txtPosition
        '
        Me.txtPosition.Flags = 0
        Me.txtPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPosition.Location = New System.Drawing.Point(81, 30)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.Size = New System.Drawing.Size(172, 21)
        Me.txtPosition.TabIndex = 3
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(5, 6)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(72, 15)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(81, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(172, 21)
        Me.txtName.TabIndex = 1
        '
        'lblEmpCompany
        '
        Me.lblEmpCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCompany.Location = New System.Drawing.Point(14, 52)
        Me.lblEmpCompany.Name = "lblEmpCompany"
        Me.lblEmpCompany.Size = New System.Drawing.Size(77, 15)
        Me.lblEmpCompany.TabIndex = 18
        Me.lblEmpCompany.Text = "Company"
        Me.lblEmpCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon3
        '
        Me.objColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon3.Location = New System.Drawing.Point(97, 71)
        Me.objColon3.Name = "objColon3"
        Me.objColon3.Size = New System.Drawing.Size(8, 15)
        Me.objColon3.TabIndex = 22
        Me.objColon3.Text = ":"
        Me.objColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpContact
        '
        Me.lblEmpContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContact.Location = New System.Drawing.Point(14, 71)
        Me.lblEmpContact.Name = "lblEmpContact"
        Me.lblEmpContact.Size = New System.Drawing.Size(77, 15)
        Me.lblEmpContact.TabIndex = 21
        Me.lblEmpContact.Text = "Contact No"
        Me.lblEmpContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon2
        '
        Me.objColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon2.Location = New System.Drawing.Point(97, 52)
        Me.objColon2.Name = "objColon2"
        Me.objColon2.Size = New System.Drawing.Size(8, 15)
        Me.objColon2.TabIndex = 19
        Me.objColon2.Text = ":"
        Me.objColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon1
        '
        Me.objColon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon1.Location = New System.Drawing.Point(97, 35)
        Me.objColon1.Name = "objColon1"
        Me.objColon1.Size = New System.Drawing.Size(8, 15)
        Me.objColon1.TabIndex = 16
        Me.objColon1.Text = ":"
        Me.objColon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radOthers
        '
        Me.radOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOthers.Location = New System.Drawing.Point(5, 90)
        Me.radOthers.Name = "radOthers"
        Me.radOthers.Size = New System.Drawing.Size(86, 17)
        Me.radOthers.TabIndex = 24
        Me.radOthers.TabStop = True
        Me.radOthers.Text = "Other"
        Me.radOthers.UseVisualStyleBackColor = True
        '
        'lvInvestigatorInfo
        '
        Me.lvInvestigatorInfo.BackColorOnChecked = True
        Me.lvInvestigatorInfo.ColumnHeaders = Nothing
        Me.lvInvestigatorInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhInvestigator, Me.colhDepartment, Me.colhCompany, Me.colhContactNo, Me.objcolhEmpId, Me.objcolhGUID, Me.objcolhRemark, Me.objcolhIsCommittee})
        Me.lvInvestigatorInfo.CompulsoryColumns = ""
        Me.lvInvestigatorInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvInvestigatorInfo.FullRowSelect = True
        Me.lvInvestigatorInfo.GridLines = True
        Me.lvInvestigatorInfo.GroupingColumn = Nothing
        Me.lvInvestigatorInfo.HideSelection = False
        Me.lvInvestigatorInfo.Location = New System.Drawing.Point(8, 227)
        Me.lvInvestigatorInfo.MinColumnWidth = 50
        Me.lvInvestigatorInfo.MultiSelect = False
        Me.lvInvestigatorInfo.Name = "lvInvestigatorInfo"
        Me.lvInvestigatorInfo.OptionalColumns = ""
        Me.lvInvestigatorInfo.ShowMoreItem = False
        Me.lvInvestigatorInfo.ShowSaveItem = False
        Me.lvInvestigatorInfo.ShowSelectAll = True
        Me.lvInvestigatorInfo.ShowSizeAllColumnsToFit = True
        Me.lvInvestigatorInfo.Size = New System.Drawing.Size(515, 263)
        Me.lvInvestigatorInfo.Sortable = True
        Me.lvInvestigatorInfo.TabIndex = 93
        Me.lvInvestigatorInfo.UseCompatibleStateImageBehavior = False
        Me.lvInvestigatorInfo.View = System.Windows.Forms.View.Details
        '
        'colhInvestigator
        '
        Me.colhInvestigator.Tag = "colhInvestigator"
        Me.colhInvestigator.Text = "Members"
        Me.colhInvestigator.Width = 180
        '
        'colhDepartment
        '
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 115
        '
        'colhCompany
        '
        Me.colhCompany.Tag = "colhCompany"
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 115
        '
        'colhContactNo
        '
        Me.colhContactNo.Tag = "colhContactNo"
        Me.colhContactNo.Text = "Contact No"
        Me.colhContactNo.Width = 100
        '
        'objcolhEmpId
        '
        Me.objcolhEmpId.Tag = "objcolhEmpId"
        Me.objcolhEmpId.Text = ""
        Me.objcolhEmpId.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = ""
        Me.objcolhGUID.Width = 0
        '
        'objcolhRemark
        '
        Me.objcolhRemark.Tag = "objcolhRemark"
        Me.objcolhRemark.Text = "objcolhRemark"
        Me.objcolhRemark.Width = 0
        '
        'objcolhIsCommittee
        '
        Me.objcolhIsCommittee.Tag = "objcolhIsCommittee"
        Me.objcolhIsCommittee.Text = ""
        Me.objcolhIsCommittee.Width = 0
        '
        'txtInvestigatorRemark
        '
        Me.txtInvestigatorRemark.BackColor = System.Drawing.Color.White
        Me.txtInvestigatorRemark.Flags = 0
        Me.txtInvestigatorRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvestigatorRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtInvestigatorRemark.Location = New System.Drawing.Point(92, 143)
        Me.txtInvestigatorRemark.Multiline = True
        Me.txtInvestigatorRemark.Name = "txtInvestigatorRemark"
        Me.txtInvestigatorRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInvestigatorRemark.Size = New System.Drawing.Size(431, 37)
        Me.txtInvestigatorRemark.TabIndex = 97
        '
        'btnDeleteInvestigator
        '
        Me.btnDeleteInvestigator.BackColor = System.Drawing.Color.White
        Me.btnDeleteInvestigator.BackgroundImage = CType(resources.GetObject("btnDeleteInvestigator.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteInvestigator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteInvestigator.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteInvestigator.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteInvestigator.FlatAppearance.BorderSize = 0
        Me.btnDeleteInvestigator.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteInvestigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteInvestigator.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteInvestigator.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteInvestigator.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteInvestigator.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteInvestigator.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteInvestigator.Location = New System.Drawing.Point(431, 191)
        Me.btnDeleteInvestigator.Name = "btnDeleteInvestigator"
        Me.btnDeleteInvestigator.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteInvestigator.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteInvestigator.Size = New System.Drawing.Size(92, 30)
        Me.btnDeleteInvestigator.TabIndex = 96
        Me.btnDeleteInvestigator.Text = "&Delete"
        Me.btnDeleteInvestigator.UseVisualStyleBackColor = True
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(8, 183)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(515, 5)
        Me.objelLine1.TabIndex = 99
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnEditInvestigator
        '
        Me.btnEditInvestigator.BackColor = System.Drawing.Color.White
        Me.btnEditInvestigator.BackgroundImage = CType(resources.GetObject("btnEditInvestigator.BackgroundImage"), System.Drawing.Image)
        Me.btnEditInvestigator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditInvestigator.BorderColor = System.Drawing.Color.Empty
        Me.btnEditInvestigator.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditInvestigator.FlatAppearance.BorderSize = 0
        Me.btnEditInvestigator.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditInvestigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditInvestigator.ForeColor = System.Drawing.Color.Black
        Me.btnEditInvestigator.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditInvestigator.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditInvestigator.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditInvestigator.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditInvestigator.Location = New System.Drawing.Point(333, 191)
        Me.btnEditInvestigator.Name = "btnEditInvestigator"
        Me.btnEditInvestigator.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditInvestigator.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditInvestigator.Size = New System.Drawing.Size(92, 30)
        Me.btnEditInvestigator.TabIndex = 95
        Me.btnEditInvestigator.Text = "&Edit"
        Me.btnEditInvestigator.UseVisualStyleBackColor = True
        '
        'lblInvestigatorRemark
        '
        Me.lblInvestigatorRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvestigatorRemark.Location = New System.Drawing.Point(8, 146)
        Me.lblInvestigatorRemark.Name = "lblInvestigatorRemark"
        Me.lblInvestigatorRemark.Size = New System.Drawing.Size(78, 16)
        Me.lblInvestigatorRemark.TabIndex = 98
        Me.lblInvestigatorRemark.Text = "Remark"
        Me.lblInvestigatorRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAddInvestigator
        '
        Me.btnAddInvestigator.BackColor = System.Drawing.Color.White
        Me.btnAddInvestigator.BackgroundImage = CType(resources.GetObject("btnAddInvestigator.BackgroundImage"), System.Drawing.Image)
        Me.btnAddInvestigator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddInvestigator.BorderColor = System.Drawing.Color.Empty
        Me.btnAddInvestigator.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddInvestigator.FlatAppearance.BorderSize = 0
        Me.btnAddInvestigator.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddInvestigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddInvestigator.ForeColor = System.Drawing.Color.Black
        Me.btnAddInvestigator.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddInvestigator.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddInvestigator.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddInvestigator.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddInvestigator.Location = New System.Drawing.Point(235, 191)
        Me.btnAddInvestigator.Name = "btnAddInvestigator"
        Me.btnAddInvestigator.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddInvestigator.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddInvestigator.Size = New System.Drawing.Size(92, 30)
        Me.btnAddInvestigator.TabIndex = 94
        Me.btnAddInvestigator.Text = "&Add"
        Me.btnAddInvestigator.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.txtDescription)
        Me.objFooter.Controls.Add(Me.lblDescription)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 501)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(907, 55)
        Me.objFooter.TabIndex = 83
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(798, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(97, 11)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(215, 31)
        Me.txtDescription.TabIndex = 87
        Me.txtDescription.Visible = False
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(15, 16)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(76, 15)
        Me.lblDescription.TabIndex = 86
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDescription.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(695, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmDisciplineResolution
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(907, 556)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineResolution"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Proceeding Steps"
        Me.pnlMain.ResumeLayout(False)
        Me.fpnlFlow.ResumeLayout(False)
        Me.gbPersonInvolved.ResumeLayout(False)
        Me.gbPersonInvolved.PerformLayout()
        Me.gbResolutionSteps.ResumeLayout(False)
        Me.gbResolutionSteps.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        Me.tabcDisciplineResoution.ResumeLayout(False)
        Me.tabpResolution.ResumeLayout(False)
        Me.tabpResolution.PerformLayout()
        Me.tabpRemark.ResumeLayout(False)
        Me.tabpRemark.PerformLayout()
        Me.gbInvestigatorInfo.ResumeLayout(False)
        Me.gbInvestigatorInfo.PerformLayout()
        Me.pnlView.ResumeLayout(False)
        Me.pnlOthers.ResumeLayout(False)
        Me.pnlOthers.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbResolutionSteps As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtResolutionSteps As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnAddStatus As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDisciplineStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents objbtnAddAction As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDisciplinaryAction As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplinaryAction As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dtpDisciplineDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents gbPersonInvolved As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtPesonInvolved As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents gbInvestigatorInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlOthers As System.Windows.Forms.Panel
    Friend WithEvents lblTrainerContactNo As System.Windows.Forms.Label
    Friend WithEvents txtTrainersContactNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents txtComany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents radOthers As System.Windows.Forms.RadioButton
    Friend WithEvents objColon3 As System.Windows.Forms.Label
    Friend WithEvents objColon2 As System.Windows.Forms.Label
    Friend WithEvents objColon1 As System.Windows.Forms.Label
    Friend WithEvents lblEmpContact As System.Windows.Forms.Label
    Friend WithEvents lblEmpCompany As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objlblEmployeeContactNo As System.Windows.Forms.Label
    Friend WithEvents objlblCompanyValue As System.Windows.Forms.Label
    Friend WithEvents objlblDepartmentValue As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents lvInvestigatorInfo As eZee.Common.eZeeListView
    Friend WithEvents colhInvestigator As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAddInvestigator As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditInvestigator As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeleteInvestigator As eZee.Common.eZeeLightButton
    Friend WithEvents txtInvestigatorRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblInvestigatorRemark As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objcolhEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents tabcDisciplineResoution As System.Windows.Forms.TabControl
    Friend WithEvents tabpResolution As System.Windows.Forms.TabPage
    Friend WithEvents tabpRemark As System.Windows.Forms.TabPage
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblIncident As System.Windows.Forms.Label
    Friend WithEvents txtIncident As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objcolhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboCommittee As System.Windows.Forms.ComboBox
    Friend WithEvents lblCommitee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCommittee As eZee.Common.eZeeGradientButton
    Friend WithEvents objcolhIsCommittee As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlView As System.Windows.Forms.Panel
    Friend WithEvents fpnlFlow As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtOffence As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOffence As System.Windows.Forms.Label
    Friend WithEvents txtOffenceCategory As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOffenceCategory As System.Windows.Forms.Label
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents txtCourtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCaseNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCourtName As System.Windows.Forms.Label
    Friend WithEvents lblCaseNo As System.Windows.Forms.Label
End Class
