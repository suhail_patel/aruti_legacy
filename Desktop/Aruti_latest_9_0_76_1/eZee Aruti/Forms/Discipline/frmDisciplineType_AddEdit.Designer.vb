﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineType_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineType_AddEdit))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbAccessInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnAddOffenceCatagory = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.cboOffenceCategory = New System.Windows.Forms.ComboBox
        Me.lblOffencecategory = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.lblCaption = New System.Windows.Forms.Label
        Me.nudSeverity = New System.Windows.Forms.NumericUpDown
        Me.lblSeverity = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblAction = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblAccCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1.SuspendLayout()
        Me.gbAccessInfo.SuspendLayout()
        CType(Me.nudSeverity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbAccessInfo)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(407, 256)
        Me.Panel1.TabIndex = 0
        '
        'gbAccessInfo
        '
        Me.gbAccessInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAccessInfo.Checked = False
        Me.gbAccessInfo.CollapseAllExceptThis = False
        Me.gbAccessInfo.CollapsedHoverImage = Nothing
        Me.gbAccessInfo.CollapsedNormalImage = Nothing
        Me.gbAccessInfo.CollapsedPressedImage = Nothing
        Me.gbAccessInfo.CollapseOnLoad = False
        Me.gbAccessInfo.Controls.Add(Me.btnAddOffenceCatagory)
        Me.gbAccessInfo.Controls.Add(Me.objbtnSearch)
        Me.gbAccessInfo.Controls.Add(Me.cboOffenceCategory)
        Me.gbAccessInfo.Controls.Add(Me.lblOffencecategory)
        Me.gbAccessInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbAccessInfo.Controls.Add(Me.lblCaption)
        Me.gbAccessInfo.Controls.Add(Me.nudSeverity)
        Me.gbAccessInfo.Controls.Add(Me.lblSeverity)
        Me.gbAccessInfo.Controls.Add(Me.lblName)
        Me.gbAccessInfo.Controls.Add(Me.txtName)
        Me.gbAccessInfo.Controls.Add(Me.lblAction)
        Me.gbAccessInfo.Controls.Add(Me.txtDescription)
        Me.gbAccessInfo.Controls.Add(Me.txtCode)
        Me.gbAccessInfo.Controls.Add(Me.lblAccCode)
        Me.gbAccessInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAccessInfo.ExpandedHoverImage = Nothing
        Me.gbAccessInfo.ExpandedNormalImage = Nothing
        Me.gbAccessInfo.ExpandedPressedImage = Nothing
        Me.gbAccessInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAccessInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAccessInfo.HeaderHeight = 25
        Me.gbAccessInfo.HeaderMessage = ""
        Me.gbAccessInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAccessInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAccessInfo.HeightOnCollapse = 0
        Me.gbAccessInfo.LeftTextSpace = 0
        Me.gbAccessInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbAccessInfo.Name = "gbAccessInfo"
        Me.gbAccessInfo.OpenHeight = 300
        Me.gbAccessInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAccessInfo.ShowBorder = True
        Me.gbAccessInfo.ShowCheckBox = False
        Me.gbAccessInfo.ShowCollapseButton = False
        Me.gbAccessInfo.ShowDefaultBorderColor = True
        Me.gbAccessInfo.ShowDownButton = False
        Me.gbAccessInfo.ShowHeader = True
        Me.gbAccessInfo.Size = New System.Drawing.Size(407, 201)
        Me.gbAccessInfo.TabIndex = 0
        Me.gbAccessInfo.Temp = 0
        Me.gbAccessInfo.Text = "Disciplinary Offences"
        Me.gbAccessInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAddOffenceCatagory
        '
        Me.btnAddOffenceCatagory.BackColor = System.Drawing.Color.Transparent
        Me.btnAddOffenceCatagory.BackColor1 = System.Drawing.Color.Transparent
        Me.btnAddOffenceCatagory.BackColor2 = System.Drawing.Color.Transparent
        Me.btnAddOffenceCatagory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnAddOffenceCatagory.BorderSelected = False
        Me.btnAddOffenceCatagory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnAddOffenceCatagory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.btnAddOffenceCatagory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnAddOffenceCatagory.Location = New System.Drawing.Point(349, 34)
        Me.btnAddOffenceCatagory.Name = "btnAddOffenceCatagory"
        Me.btnAddOffenceCatagory.Size = New System.Drawing.Size(21, 21)
        Me.btnAddOffenceCatagory.TabIndex = 2
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(376, 34)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 3
        '
        'cboOffenceCategory
        '
        Me.cboOffenceCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOffenceCategory.DropDownWidth = 300
        Me.cboOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOffenceCategory.FormattingEnabled = True
        Me.cboOffenceCategory.Location = New System.Drawing.Point(120, 34)
        Me.cboOffenceCategory.Name = "cboOffenceCategory"
        Me.cboOffenceCategory.Size = New System.Drawing.Size(223, 21)
        Me.cboOffenceCategory.TabIndex = 1
        '
        'lblOffencecategory
        '
        Me.lblOffencecategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffencecategory.Location = New System.Drawing.Point(8, 36)
        Me.lblOffencecategory.Name = "lblOffencecategory"
        Me.lblOffencecategory.Size = New System.Drawing.Size(106, 16)
        Me.lblOffencecategory.TabIndex = 0
        Me.lblOffencecategory.Text = "Offence Category"
        Me.lblOffencecategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(349, 88)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 161
        '
        'lblCaption
        '
        Me.lblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(185, 117)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(158, 16)
        Me.lblCaption.TabIndex = 10
        Me.lblCaption.Text = "(1 Means Minimun Level)"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudSeverity
        '
        Me.nudSeverity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudSeverity.Location = New System.Drawing.Point(120, 115)
        Me.nudSeverity.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.nudSeverity.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudSeverity.Name = "nudSeverity"
        Me.nudSeverity.Size = New System.Drawing.Size(59, 21)
        Me.nudSeverity.TabIndex = 9
        Me.nudSeverity.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblSeverity
        '
        Me.lblSeverity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeverity.Location = New System.Drawing.Point(8, 117)
        Me.lblSeverity.Name = "lblSeverity"
        Me.lblSeverity.Size = New System.Drawing.Size(106, 16)
        Me.lblSeverity.TabIndex = 8
        Me.lblSeverity.Text = "Severity"
        Me.lblSeverity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 90)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(106, 16)
        Me.lblName.TabIndex = 6
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(120, 88)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(223, 21)
        Me.txtName.TabIndex = 7
        '
        'lblAction
        '
        Me.lblAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAction.Location = New System.Drawing.Point(8, 146)
        Me.lblAction.Name = "lblAction"
        Me.lblAction.Size = New System.Drawing.Size(106, 16)
        Me.lblAction.TabIndex = 11
        Me.lblAction.Text = "Description"
        Me.lblAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(120, 142)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(250, 53)
        Me.txtDescription.TabIndex = 12
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(120, 61)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(117, 21)
        Me.txtCode.TabIndex = 5
        '
        'lblAccCode
        '
        Me.lblAccCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccCode.Location = New System.Drawing.Point(8, 63)
        Me.lblAccCode.Name = "lblAccCode"
        Me.lblAccCode.Size = New System.Drawing.Size(106, 16)
        Me.lblAccCode.TabIndex = 4
        Me.lblAccCode.Text = "Code"
        Me.lblAccCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 201)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(407, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(195, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(298, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmDisciplineType_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(407, 256)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineType_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Disciplinary Offences "
        Me.Panel1.ResumeLayout(False)
        Me.gbAccessInfo.ResumeLayout(False)
        Me.gbAccessInfo.PerformLayout()
        CType(Me.nudSeverity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbAccessInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAction As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAccCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents nudSeverity As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblSeverity As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOffencecategory As System.Windows.Forms.Label
    Friend WithEvents btnAddOffenceCatagory As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOffenceCategory As System.Windows.Forms.ComboBox
End Class
