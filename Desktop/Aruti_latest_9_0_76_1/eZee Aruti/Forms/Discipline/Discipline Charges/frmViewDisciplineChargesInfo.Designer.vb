﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewDisciplineChargesInfo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewDisciplineChargesInfo))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbDiscipilneFiling = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtinterdictionDate = New eZee.TextBox.AlphanumericTextBox
        Me.txtchargeDate = New eZee.TextBox.AlphanumericTextBox
        Me.cboReferenceNo = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClosePopup = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchReference = New eZee.Common.eZeeGradientButton
        Me.lblGeneralChargeDescr = New System.Windows.Forms.Label
        Me.txtChargeDescription = New System.Windows.Forms.TextBox
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.dgcolhCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffence = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIncident = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhoffencecategoryunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhoffenceunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfileunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfiletraunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.elLine1 = New eZee.Common.eZeeLine
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.txtJobTitle = New eZee.TextBox.AlphanumericTextBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblJobTitle = New System.Windows.Forms.Label
        Me.lblInterdictionDate = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.DataGridViewImageColumn1 = New System.Windows.Forms.DataGridViewImageColumn
        Me.DataGridViewImageColumn2 = New System.Windows.Forms.DataGridViewImageColumn
        Me.DataGridViewImageColumn3 = New System.Windows.Forms.DataGridViewImageColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbDiscipilneFiling.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbDiscipilneFiling
        '
        Me.gbDiscipilneFiling.BorderColor = System.Drawing.Color.Black
        Me.gbDiscipilneFiling.Checked = False
        Me.gbDiscipilneFiling.CollapseAllExceptThis = False
        Me.gbDiscipilneFiling.CollapsedHoverImage = Nothing
        Me.gbDiscipilneFiling.CollapsedNormalImage = Nothing
        Me.gbDiscipilneFiling.CollapsedPressedImage = Nothing
        Me.gbDiscipilneFiling.CollapseOnLoad = False
        Me.gbDiscipilneFiling.Controls.Add(Me.txtinterdictionDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtchargeDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.cboReferenceNo)
        Me.gbDiscipilneFiling.Controls.Add(Me.objFooter)
        Me.gbDiscipilneFiling.Controls.Add(Me.objbtnSearchReference)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblGeneralChargeDescr)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtChargeDescription)
        Me.gbDiscipilneFiling.Controls.Add(Me.pnlData)
        Me.gbDiscipilneFiling.Controls.Add(Me.elLine1)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblRefNo)
        Me.gbDiscipilneFiling.Controls.Add(Me.cboEmployee)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtDepartment)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtJobTitle)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblDepartment)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblJobTitle)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblInterdictionDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblPersonalInvolved)
        Me.gbDiscipilneFiling.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbDiscipilneFiling.ExpandedHoverImage = Nothing
        Me.gbDiscipilneFiling.ExpandedNormalImage = Nothing
        Me.gbDiscipilneFiling.ExpandedPressedImage = Nothing
        Me.gbDiscipilneFiling.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDiscipilneFiling.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDiscipilneFiling.HeaderHeight = 25
        Me.gbDiscipilneFiling.HeaderMessage = ""
        Me.gbDiscipilneFiling.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDiscipilneFiling.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDiscipilneFiling.HeightOnCollapse = 0
        Me.gbDiscipilneFiling.LeftTextSpace = 0
        Me.gbDiscipilneFiling.Location = New System.Drawing.Point(0, 0)
        Me.gbDiscipilneFiling.Name = "gbDiscipilneFiling"
        Me.gbDiscipilneFiling.OpenHeight = 300
        Me.gbDiscipilneFiling.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDiscipilneFiling.ShowBorder = True
        Me.gbDiscipilneFiling.ShowCheckBox = False
        Me.gbDiscipilneFiling.ShowCollapseButton = False
        Me.gbDiscipilneFiling.ShowDefaultBorderColor = True
        Me.gbDiscipilneFiling.ShowDownButton = False
        Me.gbDiscipilneFiling.ShowHeader = True
        Me.gbDiscipilneFiling.Size = New System.Drawing.Size(876, 498)
        Me.gbDiscipilneFiling.TabIndex = 87
        Me.gbDiscipilneFiling.Temp = 0
        Me.gbDiscipilneFiling.Text = "Disciplinary Charge Information"
        Me.gbDiscipilneFiling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtinterdictionDate
        '
        Me.txtinterdictionDate.BackColor = System.Drawing.SystemColors.Window
        Me.txtinterdictionDate.Flags = 0
        Me.txtinterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtinterdictionDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtinterdictionDate.Location = New System.Drawing.Point(744, 36)
        Me.txtinterdictionDate.Name = "txtinterdictionDate"
        Me.txtinterdictionDate.ReadOnly = True
        Me.txtinterdictionDate.Size = New System.Drawing.Size(124, 21)
        Me.txtinterdictionDate.TabIndex = 151
        '
        'txtchargeDate
        '
        Me.txtchargeDate.BackColor = System.Drawing.SystemColors.Window
        Me.txtchargeDate.Flags = 0
        Me.txtchargeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtchargeDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtchargeDate.Location = New System.Drawing.Point(528, 38)
        Me.txtchargeDate.Name = "txtchargeDate"
        Me.txtchargeDate.ReadOnly = True
        Me.txtchargeDate.Size = New System.Drawing.Size(109, 21)
        Me.txtchargeDate.TabIndex = 150
        '
        'cboReferenceNo
        '
        Me.cboReferenceNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboReferenceNo.DropDownWidth = 400
        Me.cboReferenceNo.Enabled = False
        Me.cboReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReferenceNo.FormattingEnabled = True
        Me.cboReferenceNo.Location = New System.Drawing.Point(110, 65)
        Me.cboReferenceNo.Name = "cboReferenceNo"
        Me.cboReferenceNo.Size = New System.Drawing.Size(307, 21)
        Me.cboReferenceNo.TabIndex = 148
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOk)
        Me.objFooter.Controls.Add(Me.btnClosePopup)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 443)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(876, 55)
        Me.objFooter.TabIndex = 146
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(668, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 15
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClosePopup
        '
        Me.btnClosePopup.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClosePopup.BackColor = System.Drawing.Color.White
        Me.btnClosePopup.BackgroundImage = CType(resources.GetObject("btnClosePopup.BackgroundImage"), System.Drawing.Image)
        Me.btnClosePopup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClosePopup.BorderColor = System.Drawing.Color.Empty
        Me.btnClosePopup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClosePopup.FlatAppearance.BorderSize = 0
        Me.btnClosePopup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClosePopup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClosePopup.ForeColor = System.Drawing.Color.Black
        Me.btnClosePopup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClosePopup.GradientForeColor = System.Drawing.Color.Black
        Me.btnClosePopup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClosePopup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClosePopup.Location = New System.Drawing.Point(771, 13)
        Me.btnClosePopup.Name = "btnClosePopup"
        Me.btnClosePopup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClosePopup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClosePopup.Size = New System.Drawing.Size(97, 30)
        Me.btnClosePopup.TabIndex = 16
        Me.btnClosePopup.Text = "&Close"
        Me.btnClosePopup.UseVisualStyleBackColor = True
        '
        'objbtnSearchReference
        '
        Me.objbtnSearchReference.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReference.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReference.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReference.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReference.BorderSelected = False
        Me.objbtnSearchReference.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReference.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReference.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReference.Location = New System.Drawing.Point(423, 65)
        Me.objbtnSearchReference.Name = "objbtnSearchReference"
        Me.objbtnSearchReference.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReference.TabIndex = 145
        '
        'lblGeneralChargeDescr
        '
        Me.lblGeneralChargeDescr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGeneralChargeDescr.Location = New System.Drawing.Point(450, 69)
        Me.lblGeneralChargeDescr.Name = "lblGeneralChargeDescr"
        Me.lblGeneralChargeDescr.Size = New System.Drawing.Size(72, 51)
        Me.lblGeneralChargeDescr.TabIndex = 140
        Me.lblGeneralChargeDescr.Text = "General Charge Description"
        '
        'txtChargeDescription
        '
        Me.txtChargeDescription.BackColor = System.Drawing.Color.White
        Me.txtChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDescription.Location = New System.Drawing.Point(528, 66)
        Me.txtChargeDescription.Multiline = True
        Me.txtChargeDescription.Name = "txtChargeDescription"
        Me.txtChargeDescription.ReadOnly = True
        Me.txtChargeDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtChargeDescription.Size = New System.Drawing.Size(340, 74)
        Me.txtChargeDescription.TabIndex = 119
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(8, 172)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(860, 264)
        Me.pnlData.TabIndex = 136
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeight = 24
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhCount, Me.dgcolhOffCategory, Me.dgcolhOffence, Me.dgcolhIncident, Me.objdgcolhoffencecategoryunkid, Me.objdgcolhoffenceunkid, Me.objdgcolhGUID, Me.objdgcolhfileunkid, Me.objdgcolhfiletraunkid, Me.dgcolhResponseType, Me.dgcolhResponseDate, Me.dgcolhResponseText})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.ReadOnly = True
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(860, 264)
        Me.dgvData.TabIndex = 0
        '
        'dgcolhCount
        '
        Me.dgcolhCount.HeaderText = "Count"
        Me.dgcolhCount.Name = "dgcolhCount"
        Me.dgcolhCount.ReadOnly = True
        Me.dgcolhCount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCount.Width = 50
        '
        'dgcolhOffCategory
        '
        Me.dgcolhOffCategory.HeaderText = "Off. Category"
        Me.dgcolhOffCategory.Name = "dgcolhOffCategory"
        Me.dgcolhOffCategory.ReadOnly = True
        Me.dgcolhOffCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOffCategory.Width = 200
        '
        'dgcolhOffence
        '
        Me.dgcolhOffence.HeaderText = "Offence"
        Me.dgcolhOffence.Name = "dgcolhOffence"
        Me.dgcolhOffence.ReadOnly = True
        Me.dgcolhOffence.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOffence.Width = 240
        '
        'dgcolhIncident
        '
        Me.dgcolhIncident.HeaderText = "Incident"
        Me.dgcolhIncident.Name = "dgcolhIncident"
        Me.dgcolhIncident.ReadOnly = True
        Me.dgcolhIncident.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhIncident.Width = 370
        '
        'objdgcolhoffencecategoryunkid
        '
        Me.objdgcolhoffencecategoryunkid.HeaderText = "objdgcolhoffencecategoryunkid"
        Me.objdgcolhoffencecategoryunkid.Name = "objdgcolhoffencecategoryunkid"
        Me.objdgcolhoffencecategoryunkid.ReadOnly = True
        Me.objdgcolhoffencecategoryunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhoffencecategoryunkid.Visible = False
        '
        'objdgcolhoffenceunkid
        '
        Me.objdgcolhoffenceunkid.HeaderText = "objdgcolhoffenceunkid"
        Me.objdgcolhoffenceunkid.Name = "objdgcolhoffenceunkid"
        Me.objdgcolhoffenceunkid.ReadOnly = True
        Me.objdgcolhoffenceunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhoffenceunkid.Visible = False
        '
        'objdgcolhGUID
        '
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.ReadOnly = True
        Me.objdgcolhGUID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGUID.Visible = False
        '
        'objdgcolhfileunkid
        '
        Me.objdgcolhfileunkid.HeaderText = "objdgcolhfileunkid"
        Me.objdgcolhfileunkid.Name = "objdgcolhfileunkid"
        Me.objdgcolhfileunkid.ReadOnly = True
        Me.objdgcolhfileunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhfileunkid.Visible = False
        '
        'objdgcolhfiletraunkid
        '
        Me.objdgcolhfiletraunkid.HeaderText = "objdgcolhfiletraunkid"
        Me.objdgcolhfiletraunkid.Name = "objdgcolhfiletraunkid"
        Me.objdgcolhfiletraunkid.ReadOnly = True
        Me.objdgcolhfiletraunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhfiletraunkid.Visible = False
        '
        'dgcolhResponseType
        '
        Me.dgcolhResponseType.HeaderText = "Response Type"
        Me.dgcolhResponseType.Name = "dgcolhResponseType"
        Me.dgcolhResponseType.ReadOnly = True
        Me.dgcolhResponseType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhResponseType.Visible = False
        '
        'dgcolhResponseDate
        '
        Me.dgcolhResponseDate.HeaderText = "Response Date"
        Me.dgcolhResponseDate.Name = "dgcolhResponseDate"
        Me.dgcolhResponseDate.ReadOnly = True
        Me.dgcolhResponseDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhResponseDate.Visible = False
        '
        'dgcolhResponseText
        '
        Me.dgcolhResponseText.HeaderText = "Response"
        Me.dgcolhResponseText.Name = "dgcolhResponseText"
        Me.dgcolhResponseText.ReadOnly = True
        Me.dgcolhResponseText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhResponseText.Visible = False
        '
        'elLine1
        '
        Me.elLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLine1.Location = New System.Drawing.Point(8, 151)
        Me.elLine1.Name = "elLine1"
        Me.elLine1.Size = New System.Drawing.Size(860, 18)
        Me.elLine1.TabIndex = 117
        Me.elLine1.Text = "Charges Count Information"
        Me.elLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(12, 67)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(92, 17)
        Me.lblRefNo.TabIndex = 96
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(110, 38)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(307, 21)
        Me.cboEmployee.TabIndex = 111
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(110, 119)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(307, 21)
        Me.txtDepartment.TabIndex = 108
        '
        'txtJobTitle
        '
        Me.txtJobTitle.BackColor = System.Drawing.SystemColors.Window
        Me.txtJobTitle.Flags = 0
        Me.txtJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJobTitle.Location = New System.Drawing.Point(110, 92)
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.ReadOnly = True
        Me.txtJobTitle.Size = New System.Drawing.Size(307, 21)
        Me.txtJobTitle.TabIndex = 107
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(12, 121)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(92, 17)
        Me.lblDepartment.TabIndex = 106
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobTitle
        '
        Me.lblJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobTitle.Location = New System.Drawing.Point(12, 94)
        Me.lblJobTitle.Name = "lblJobTitle"
        Me.lblJobTitle.Size = New System.Drawing.Size(92, 17)
        Me.lblJobTitle.TabIndex = 105
        Me.lblJobTitle.Text = "Job Title"
        Me.lblJobTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInterdictionDate
        '
        Me.lblInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterdictionDate.Location = New System.Drawing.Point(643, 38)
        Me.lblInterdictionDate.Name = "lblInterdictionDate"
        Me.lblInterdictionDate.Size = New System.Drawing.Size(95, 17)
        Me.lblInterdictionDate.TabIndex = 97
        Me.lblInterdictionDate.Text = "Interdiction Date "
        Me.lblInterdictionDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(449, 38)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(71, 17)
        Me.lblDate.TabIndex = 72
        Me.lblDate.Text = "Charge Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(12, 40)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(92, 17)
        Me.lblPersonalInvolved.TabIndex = 78
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewImageColumn1
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.NullValue = Nothing
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White
        Me.DataGridViewImageColumn1.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewImageColumn1.Frozen = True
        Me.DataGridViewImageColumn1.HeaderText = ""
        Me.DataGridViewImageColumn1.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.DataGridViewImageColumn1.Name = "DataGridViewImageColumn1"
        Me.DataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewImageColumn1.Width = 25
        '
        'DataGridViewImageColumn2
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.NullValue = Nothing
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White
        Me.DataGridViewImageColumn2.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewImageColumn2.HeaderText = ""
        Me.DataGridViewImageColumn2.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.DataGridViewImageColumn2.Name = "DataGridViewImageColumn2"
        Me.DataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewImageColumn2.Width = 25
        '
        'DataGridViewImageColumn3
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.NullValue = Nothing
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Blue
        Me.DataGridViewImageColumn3.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewImageColumn3.HeaderText = ""
        Me.DataGridViewImageColumn3.Image = Global.Aruti.Main.My.Resources.Resources.add_docs
        Me.DataGridViewImageColumn3.Name = "DataGridViewImageColumn3"
        Me.DataGridViewImageColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewImageColumn3.Width = 25
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Count"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 50
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Off. Category"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Offence"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 240
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Incident"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 370
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objdgcolhoffencecategoryunkid"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhoffenceunkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhfileunkid"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhfiletraunkid"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Response Type"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Response Date"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Response"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'frmViewDisciplineChargesInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(876, 498)
        Me.Controls.Add(Me.gbDiscipilneFiling)
        Me.Name = "frmViewDisciplineChargesInfo"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "View Disciplinary Charges"
        Me.gbDiscipilneFiling.ResumeLayout(False)
        Me.gbDiscipilneFiling.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbDiscipilneFiling As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblGeneralChargeDescr As System.Windows.Forms.Label
    Friend WithEvents txtChargeDescription As System.Windows.Forms.TextBox
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents elLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtJobTitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblJobTitle As System.Windows.Forms.Label
    Friend WithEvents lblInterdictionDate As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents DataGridViewImageColumn1 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents DataGridViewImageColumn2 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents DataGridViewImageColumn3 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objbtnSearchReference As eZee.Common.eZeeGradientButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnClosePopup As eZee.Common.eZeeLightButton
    Friend WithEvents cboReferenceNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents txtinterdictionDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtchargeDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dgcolhCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffence As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIncident As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhoffencecategoryunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhoffenceunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfileunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfiletraunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
