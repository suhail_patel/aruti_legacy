﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChargeCountResponse
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChargeCountResponse))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnFinalSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbHearingInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.elLine1 = New eZee.Common.eZeeLine
        Me.lblGeneralChargeDescr = New System.Windows.Forms.Label
        Me.txtChargeDescription = New System.Windows.Forms.TextBox
        Me.txtInterdictionDate = New System.Windows.Forms.TextBox
        Me.txtChargeDate = New System.Windows.Forms.TextBox
        Me.txtReferenceNo = New System.Windows.Forms.TextBox
        Me.lblInterdictionDate = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.txtJobTitle = New eZee.TextBox.AlphanumericTextBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblJobTitle = New System.Windows.Forms.Label
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhAttachment = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDownloadAttachment = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffence = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIncident = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSeverity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhoffencecategoryunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhoffenceunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfileunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfiletraunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        Me.gbHearingInfo.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnFinalSave)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 419)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(755, 55)
        Me.objFooter.TabIndex = 149
        '
        'btnFinalSave
        '
        Me.btnFinalSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinalSave.BackColor = System.Drawing.Color.White
        Me.btnFinalSave.BackgroundImage = CType(resources.GetObject("btnFinalSave.BackgroundImage"), System.Drawing.Image)
        Me.btnFinalSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinalSave.BorderColor = System.Drawing.Color.Empty
        Me.btnFinalSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinalSave.FlatAppearance.BorderSize = 0
        Me.btnFinalSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinalSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalSave.ForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinalSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Location = New System.Drawing.Point(440, 13)
        Me.btnFinalSave.Name = "btnFinalSave"
        Me.btnFinalSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Size = New System.Drawing.Size(97, 30)
        Me.btnFinalSave.TabIndex = 150
        Me.btnFinalSave.Text = "&Final Save"
        Me.btnFinalSave.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(543, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(646, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbHearingInfo
        '
        Me.gbHearingInfo.BorderColor = System.Drawing.Color.Black
        Me.gbHearingInfo.Checked = False
        Me.gbHearingInfo.CollapseAllExceptThis = False
        Me.gbHearingInfo.CollapsedHoverImage = Nothing
        Me.gbHearingInfo.CollapsedNormalImage = Nothing
        Me.gbHearingInfo.CollapsedPressedImage = Nothing
        Me.gbHearingInfo.CollapseOnLoad = False
        Me.gbHearingInfo.Controls.Add(Me.pnlData)
        Me.gbHearingInfo.Controls.Add(Me.elLine1)
        Me.gbHearingInfo.Controls.Add(Me.lblGeneralChargeDescr)
        Me.gbHearingInfo.Controls.Add(Me.txtChargeDescription)
        Me.gbHearingInfo.Controls.Add(Me.txtInterdictionDate)
        Me.gbHearingInfo.Controls.Add(Me.txtChargeDate)
        Me.gbHearingInfo.Controls.Add(Me.txtReferenceNo)
        Me.gbHearingInfo.Controls.Add(Me.lblInterdictionDate)
        Me.gbHearingInfo.Controls.Add(Me.lblDate)
        Me.gbHearingInfo.Controls.Add(Me.txtDepartment)
        Me.gbHearingInfo.Controls.Add(Me.txtJobTitle)
        Me.gbHearingInfo.Controls.Add(Me.lblDepartment)
        Me.gbHearingInfo.Controls.Add(Me.lblJobTitle)
        Me.gbHearingInfo.Controls.Add(Me.lblRefNo)
        Me.gbHearingInfo.Controls.Add(Me.cboEmployee)
        Me.gbHearingInfo.Controls.Add(Me.lblPersonalInvolved)
        Me.gbHearingInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbHearingInfo.ExpandedHoverImage = Nothing
        Me.gbHearingInfo.ExpandedNormalImage = Nothing
        Me.gbHearingInfo.ExpandedPressedImage = Nothing
        Me.gbHearingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHearingInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHearingInfo.HeaderHeight = 25
        Me.gbHearingInfo.HeaderMessage = ""
        Me.gbHearingInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHearingInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHearingInfo.HeightOnCollapse = 0
        Me.gbHearingInfo.LeftTextSpace = 0
        Me.gbHearingInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbHearingInfo.Name = "gbHearingInfo"
        Me.gbHearingInfo.OpenHeight = 300
        Me.gbHearingInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHearingInfo.ShowBorder = True
        Me.gbHearingInfo.ShowCheckBox = False
        Me.gbHearingInfo.ShowCollapseButton = False
        Me.gbHearingInfo.ShowDefaultBorderColor = True
        Me.gbHearingInfo.ShowDownButton = False
        Me.gbHearingInfo.ShowHeader = True
        Me.gbHearingInfo.Size = New System.Drawing.Size(755, 419)
        Me.gbHearingInfo.TabIndex = 150
        Me.gbHearingInfo.Temp = 0
        Me.gbHearingInfo.Text = "Add/Edit Response Information"
        Me.gbHearingInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(12, 212)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(731, 201)
        Me.pnlData.TabIndex = 189
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeight = 24
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhAttachment, Me.objdgcolhDownloadAttachment, Me.dgcolhCount, Me.dgcolhOffCategory, Me.dgcolhOffence, Me.dgcolhIncident, Me.dgcolhSeverity, Me.objdgcolhoffencecategoryunkid, Me.objdgcolhoffenceunkid, Me.objdgcolhGUID, Me.objdgcolhfileunkid, Me.objdgcolhfiletraunkid, Me.dgcolhResponseType, Me.dgcolhResponseDate, Me.dgcolhResponseText})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.ReadOnly = True
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(731, 201)
        Me.dgvData.TabIndex = 0
        '
        'elLine1
        '
        Me.elLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLine1.Location = New System.Drawing.Point(12, 191)
        Me.elLine1.Name = "elLine1"
        Me.elLine1.Size = New System.Drawing.Size(731, 18)
        Me.elLine1.TabIndex = 188
        Me.elLine1.Text = "Charges Count Information"
        Me.elLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblGeneralChargeDescr
        '
        Me.lblGeneralChargeDescr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGeneralChargeDescr.Location = New System.Drawing.Point(12, 117)
        Me.lblGeneralChargeDescr.Name = "lblGeneralChargeDescr"
        Me.lblGeneralChargeDescr.Size = New System.Drawing.Size(92, 51)
        Me.lblGeneralChargeDescr.TabIndex = 187
        Me.lblGeneralChargeDescr.Text = "General Charge Description"
        '
        'txtChargeDescription
        '
        Me.txtChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDescription.Location = New System.Drawing.Point(110, 115)
        Me.txtChargeDescription.Multiline = True
        Me.txtChargeDescription.Name = "txtChargeDescription"
        Me.txtChargeDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtChargeDescription.Size = New System.Drawing.Size(631, 73)
        Me.txtChargeDescription.TabIndex = 186
        '
        'txtInterdictionDate
        '
        Me.txtInterdictionDate.BackColor = System.Drawing.Color.White
        Me.txtInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterdictionDate.Location = New System.Drawing.Point(536, 88)
        Me.txtInterdictionDate.Name = "txtInterdictionDate"
        Me.txtInterdictionDate.ReadOnly = True
        Me.txtInterdictionDate.Size = New System.Drawing.Size(205, 21)
        Me.txtInterdictionDate.TabIndex = 169
        '
        'txtChargeDate
        '
        Me.txtChargeDate.BackColor = System.Drawing.Color.White
        Me.txtChargeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDate.Location = New System.Drawing.Point(536, 61)
        Me.txtChargeDate.Name = "txtChargeDate"
        Me.txtChargeDate.ReadOnly = True
        Me.txtChargeDate.Size = New System.Drawing.Size(205, 21)
        Me.txtChargeDate.TabIndex = 168
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.BackColor = System.Drawing.Color.White
        Me.txtReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferenceNo.Location = New System.Drawing.Point(536, 35)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.ReadOnly = True
        Me.txtReferenceNo.Size = New System.Drawing.Size(205, 21)
        Me.txtReferenceNo.TabIndex = 168
        '
        'lblInterdictionDate
        '
        Me.lblInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterdictionDate.Location = New System.Drawing.Point(438, 90)
        Me.lblInterdictionDate.Name = "lblInterdictionDate"
        Me.lblInterdictionDate.Size = New System.Drawing.Size(92, 17)
        Me.lblInterdictionDate.TabIndex = 167
        Me.lblInterdictionDate.Text = "Interdiction Date "
        Me.lblInterdictionDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(438, 63)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(92, 17)
        Me.lblDate.TabIndex = 166
        Me.lblDate.Text = "Charge Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(110, 88)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(295, 21)
        Me.txtDepartment.TabIndex = 165
        '
        'txtJobTitle
        '
        Me.txtJobTitle.BackColor = System.Drawing.SystemColors.Window
        Me.txtJobTitle.Flags = 0
        Me.txtJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJobTitle.Location = New System.Drawing.Point(110, 61)
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.ReadOnly = True
        Me.txtJobTitle.Size = New System.Drawing.Size(295, 21)
        Me.txtJobTitle.TabIndex = 164
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(12, 90)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(92, 17)
        Me.lblDepartment.TabIndex = 163
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobTitle
        '
        Me.lblJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobTitle.Location = New System.Drawing.Point(12, 63)
        Me.lblJobTitle.Name = "lblJobTitle"
        Me.lblJobTitle.Size = New System.Drawing.Size(92, 17)
        Me.lblJobTitle.TabIndex = 162
        Me.lblJobTitle.Text = "Job Title"
        Me.lblJobTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(438, 36)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(92, 17)
        Me.lblRefNo.TabIndex = 159
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(110, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(295, 21)
        Me.cboEmployee.TabIndex = 157
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(12, 36)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(92, 17)
        Me.lblPersonalInvolved.TabIndex = 156
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Count"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 50
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Off. Category"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 130
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Offence"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 130
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Incident"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 130
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Severity"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 62
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhoffencecategoryunkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhoffenceunkid"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhfileunkid"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhfiletraunkid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Response Type"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Response Date"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Response"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEdit
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.NullValue = CType(resources.GetObject("DataGridViewCellStyle1.NullValue"), Object)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White
        Me.objdgcolhEdit.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.NullValue = CType(resources.GetObject("DataGridViewCellStyle2.NullValue"), Object)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White
        Me.objdgcolhDelete.DefaultCellStyle = DataGridViewCellStyle2
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhAttachment
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.NullValue = CType(resources.GetObject("DataGridViewCellStyle3.NullValue"), Object)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White
        Me.objdgcolhAttachment.DefaultCellStyle = DataGridViewCellStyle3
        Me.objdgcolhAttachment.Frozen = True
        Me.objdgcolhAttachment.HeaderText = ""
        Me.objdgcolhAttachment.Image = Global.Aruti.Main.My.Resources.Resources.add_docs
        Me.objdgcolhAttachment.Name = "objdgcolhAttachment"
        Me.objdgcolhAttachment.ReadOnly = True
        Me.objdgcolhAttachment.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAttachment.Width = 25
        '
        'objdgcolhDownloadAttachment
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.NullValue = CType(resources.GetObject("DataGridViewCellStyle4.NullValue"), Object)
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White
        Me.objdgcolhDownloadAttachment.DefaultCellStyle = DataGridViewCellStyle4
        Me.objdgcolhDownloadAttachment.Frozen = True
        Me.objdgcolhDownloadAttachment.HeaderText = ""
        Me.objdgcolhDownloadAttachment.Image = Global.Aruti.Main.My.Resources.Resources.download_16
        Me.objdgcolhDownloadAttachment.Name = "objdgcolhDownloadAttachment"
        Me.objdgcolhDownloadAttachment.ReadOnly = True
        Me.objdgcolhDownloadAttachment.Width = 25
        '
        'dgcolhCount
        '
        Me.dgcolhCount.Frozen = True
        Me.dgcolhCount.HeaderText = "Count"
        Me.dgcolhCount.Name = "dgcolhCount"
        Me.dgcolhCount.ReadOnly = True
        Me.dgcolhCount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCount.Width = 50
        '
        'dgcolhOffCategory
        '
        Me.dgcolhOffCategory.HeaderText = "Off. Category"
        Me.dgcolhOffCategory.Name = "dgcolhOffCategory"
        Me.dgcolhOffCategory.ReadOnly = True
        Me.dgcolhOffCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOffCategory.Width = 130
        '
        'dgcolhOffence
        '
        Me.dgcolhOffence.HeaderText = "Offence"
        Me.dgcolhOffence.Name = "dgcolhOffence"
        Me.dgcolhOffence.ReadOnly = True
        Me.dgcolhOffence.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOffence.Width = 130
        '
        'dgcolhIncident
        '
        Me.dgcolhIncident.HeaderText = "Incident"
        Me.dgcolhIncident.Name = "dgcolhIncident"
        Me.dgcolhIncident.ReadOnly = True
        Me.dgcolhIncident.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhIncident.Width = 130
        '
        'dgcolhSeverity
        '
        Me.dgcolhSeverity.HeaderText = "Severity"
        Me.dgcolhSeverity.Name = "dgcolhSeverity"
        Me.dgcolhSeverity.ReadOnly = True
        Me.dgcolhSeverity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSeverity.Width = 62
        '
        'objdgcolhoffencecategoryunkid
        '
        Me.objdgcolhoffencecategoryunkid.HeaderText = "objdgcolhoffencecategoryunkid"
        Me.objdgcolhoffencecategoryunkid.Name = "objdgcolhoffencecategoryunkid"
        Me.objdgcolhoffencecategoryunkid.ReadOnly = True
        Me.objdgcolhoffencecategoryunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhoffencecategoryunkid.Visible = False
        '
        'objdgcolhoffenceunkid
        '
        Me.objdgcolhoffenceunkid.HeaderText = "objdgcolhoffenceunkid"
        Me.objdgcolhoffenceunkid.Name = "objdgcolhoffenceunkid"
        Me.objdgcolhoffenceunkid.ReadOnly = True
        Me.objdgcolhoffenceunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhoffenceunkid.Visible = False
        '
        'objdgcolhGUID
        '
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.ReadOnly = True
        Me.objdgcolhGUID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGUID.Visible = False
        '
        'objdgcolhfileunkid
        '
        Me.objdgcolhfileunkid.HeaderText = "objdgcolhfileunkid"
        Me.objdgcolhfileunkid.Name = "objdgcolhfileunkid"
        Me.objdgcolhfileunkid.ReadOnly = True
        Me.objdgcolhfileunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhfileunkid.Visible = False
        '
        'objdgcolhfiletraunkid
        '
        Me.objdgcolhfiletraunkid.HeaderText = "objdgcolhfiletraunkid"
        Me.objdgcolhfiletraunkid.Name = "objdgcolhfiletraunkid"
        Me.objdgcolhfiletraunkid.ReadOnly = True
        Me.objdgcolhfiletraunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhfiletraunkid.Visible = False
        '
        'dgcolhResponseType
        '
        Me.dgcolhResponseType.HeaderText = "Response Type"
        Me.dgcolhResponseType.Name = "dgcolhResponseType"
        Me.dgcolhResponseType.ReadOnly = True
        Me.dgcolhResponseType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhResponseDate
        '
        Me.dgcolhResponseDate.HeaderText = "Response Date"
        Me.dgcolhResponseDate.Name = "dgcolhResponseDate"
        Me.dgcolhResponseDate.ReadOnly = True
        Me.dgcolhResponseDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhResponseText
        '
        Me.dgcolhResponseText.HeaderText = "Response"
        Me.dgcolhResponseText.Name = "dgcolhResponseText"
        Me.dgcolhResponseText.ReadOnly = True
        Me.dgcolhResponseText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmChargeCountResponse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(755, 474)
        Me.Controls.Add(Me.gbHearingInfo)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChargeCountResponse"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Charges Count Response"
        Me.objFooter.ResumeLayout(False)
        Me.gbHearingInfo.ResumeLayout(False)
        Me.gbHearingInfo.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnFinalSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbHearingInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtInterdictionDate As System.Windows.Forms.TextBox
    Friend WithEvents txtChargeDate As System.Windows.Forms.TextBox
    Friend WithEvents lblInterdictionDate As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtJobTitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblJobTitle As System.Windows.Forms.Label
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents lblGeneralChargeDescr As System.Windows.Forms.Label
    Friend WithEvents txtChargeDescription As System.Windows.Forms.TextBox
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents elLine1 As eZee.Common.eZeeLine
    Friend WithEvents txtReferenceNo As System.Windows.Forms.TextBox
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhAttachment As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDownloadAttachment As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffence As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIncident As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSeverity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhoffencecategoryunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhoffenceunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfileunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfiletraunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
