﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.IO.Packaging

#End Region

Public Class frmChargeCountResponse

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmChargeCountResponse"
    Private mintEmployeeId As Integer = 0
    Private mintDisciplineFileId As Integer = 0
    Private mdtCharges As DataTable
    Private objDiscChargeMaster As clsDiscipline_file_master
    Private objDiscChargeTran As clsDiscipline_file_tran
    Private mdtScanAttachment As DataTable = Nothing
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intEmployeeId As Integer, ByVal intDisciplineId As Integer) As Boolean
        Try
            mintEmployeeId = intEmployeeId
            mintDisciplineFileId = intDisciplineId
            Me.ShowDialog()
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objCommonMaster As New clsCommon_Master
            Dim objDscplnFileMaster As New clsDiscipline_file_master

            dsList = objDscplnFileMaster.GetComboList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , , , , )

            Dim mdtEmployee As DataTable
            mdtEmployee = dsList.Tables("List").DefaultView.ToTable(True, "involved_employeeunkid", "employeecode", "employeename")
            With cboEmployee
                .ValueMember = "involved_employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = mdtEmployee
                .SelectedValue = mintEmployeeId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillChargesTran()
        Try
            dgvData.AutoGenerateColumns = False
            dgcolhIncident.DataPropertyName = "incident_description"
            dgcolhOffCategory.DataPropertyName = "charge_category"
            dgcolhOffence.DataPropertyName = "charge_descr"
            dgcolhSeverity.DataPropertyName = "charge_severity"
            objdgcolhoffencecategoryunkid.DataPropertyName = "offencecategoryunkid"
            objdgcolhoffenceunkid.DataPropertyName = "offenceunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            objdgcolhfileunkid.DataPropertyName = "disciplinefileunkid"
            objdgcolhfiletraunkid.DataPropertyName = "disciplinefiletranunkid"
            dgcolhResponseDate.DataPropertyName = "response_date"
            dgcolhResponseDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhResponseText.DataPropertyName = "response_remark"
            dgcolhResponseType.DataPropertyName = "response_type"

            dgvData.DataSource = mdtCharges
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillChargesTran", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillInformation()
        Try
            objDiscChargeMaster._Disciplinefileunkid = mintDisciplineFileId
            txtReferenceNo.Text = objDiscChargeMaster._Reference_No
            txtChargeDate.Text = objDiscChargeMaster._Chargedate.Date.ToShortDateString
            txtChargeDate.Tag = objDiscChargeMaster._Chargedate
            txtChargeDescription.Text = objDiscChargeMaster._Charge_Description
            If objDiscChargeMaster._Interdictiondate <> Nothing Then
                txtInterdictionDate.Text = objDiscChargeMaster._Interdictiondate.Date.ToShortDateString
            End If
            objDiscChargeTran._Disciplinefileunkid = mintDisciplineFileId
            mdtCharges = objDiscChargeTran._ChargesTable
            Call FillChargesTran()
            If objDiscChargeMaster._IsFinal = True Then
                objdgcolhEdit.Visible = False
                objdgcolhDelete.Visible = False
                objdgcolhAttachment.Visible = False
                btnFinalSave.Enabled = False
                btnSave.Enabled = False
                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                objdgcolhDownloadAttachment.Visible = True
            Else
                objdgcolhDownloadAttachment.Visible = False
                'S.SANDEEP |01-OCT-2019| -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInformation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Update_DataRow(ByVal dr As DataRow) As Boolean
        Try
            dr.Item("AUD") = "D"
            dr.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_DataRow", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Function Add_DataRow(ByVal dr As DataRow, ByVal intRowIdx As Integer, ByVal strScreenName As String) As Boolean
        Try
            Dim xRow As DataRow() = Nothing
            If CInt(dgvData.Rows(intRowIdx).Cells(objdgcolhfiletraunkid.Index).Value) > 0 Then
                xRow = mdtScanAttachment.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND transactionunkid = '" & CInt(dgvData.Rows(intRowIdx).Cells(objdgcolhfiletraunkid.Index).Value) & "' AND form_name = '" & strScreenName & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            End If
            If xRow.Length <= 0 Then
                mdtScanAttachment.ImportRow(dr)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub UploadScanAttachment()
        Try
            Dim mstrFolderName As String = ""
            Dim strError As String = ""
            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
            Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.DISCIPLINES) Select (p.Item("Name").ToString)).FirstOrDefault
            If mdtScanAttachment IsNot Nothing Then 'S.SANDEEP |16-JAN-2020| -- START { ISSUE/ENHANCEMENT : OBJECT REFERENCE ERROR (mdtScanAttachment) } -- END
            For Each dRow As DataRow In mdtScanAttachment.Rows
                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                    Dim intScanAttachRefId As Integer = CInt(dRow("scanattachrefid"))

                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                            dRow("fileuniquename") = strFileName
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFileName As String = dRow("fileuniquename").ToString
                    Dim intScanattachrefid As Integer = CInt(dRow("scanattachrefid"))

                    If blnIsIISInstalled Then
                        Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then strPath += "/"
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If clsFileUploadDownload.UpdateFile(CStr(dRow("filepath")), strPath, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Move(CStr(dRow("filepath")), strDocLocalPath)

                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFileName As String = dRow("fileuniquename").ToString
                    If blnIsIISInstalled Then
                        'Gajanan [8-April-2019] -- Start
                        'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                        If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                            'Gajanan [8-April-2019] -- End
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If File.Exists(strDocLocalPath) Then
                                File.Delete(strDocLocalPath)
                            End If
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
            Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UploadScanAttachment", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmChargeCountResponse_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objDiscChargeMaster = New clsDiscipline_file_master
        objDiscChargeTran = New clsDiscipline_file_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call FillInformation()
            Dim objScanAttachment As New clsScan_Attach_Documents
            'S.SANDEEP |04-SEP-2021| -- START
            'objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
            objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , , , , CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END

            Dim xRow As DataRow() = objScanAttachment._Datatable.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND form_name = '" & mstrModuleName & "'")
            If xRow.Count > 0 Then
                mdtScanAttachment = xRow.CopyToDataTable()
            Else
                mdtScanAttachment = objScanAttachment._Datatable.Clone
            End If
            objScanAttachment = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmChargeCountResponse_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmChargeCountResponse_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmChargeCountResponse_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmChargeCountResponse_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objDiscChargeMaster = Nothing
        objDiscChargeTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_file_master.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_file_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click
        Try
            Dim strmessage As String = String.Empty
            strmessage = objDiscChargeTran.IsRespondedAll_Counts(mintDisciplineFileId, mdtCharges)
            If strmessage.Trim.Length > 0 Then
                eZeeMsgBox.Show(strmessage, enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call UploadScanAttachment()
            Dim lstName As New List(Of String)(New String() {mstrModuleName})
            objDiscChargeMaster._Disciplinefileunkid = mintDisciplineFileId
            objDiscChargeMaster._IsFinal = True
            objDiscChargeMaster.Update(ConfigParameter._Object._CurrentDateAndTime, mdtScanAttachment, mstrModuleName, Company._Object._Companyunkid, ConfigParameter._Object._Document_Path, lstName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, mdtCharges, True)
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFinalSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Call UploadScanAttachment()
            objDiscChargeTran.InsertUpdateDelete_DisciplineCharges(User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mdtScanAttachment, mstrModuleName)
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                Dim objDept As New clsDepartment
                Dim objJobs As New clsJobs
                objDept._Departmentunkid = objEmp._Departmentunkid
                objJobs._Jobunkid = objEmp._Jobunkid
                txtDepartment.Text = objDept._Name
                txtJobTitle.Text = objJobs._Job_Name
                objDept = Nothing : objJobs = Nothing : objEmp = Nothing
            Else
                txtDepartment.Text = "" : txtJobTitle.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If cboEmployee.DropDownStyle = ComboBoxStyle.Simple Then
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cboEmployee.KeyDown
        Try
            If cboEmployee.DropDownStyle = ComboBoxStyle.Simple Then
                If e.KeyCode = Keys.Right Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Home Or e.KeyCode = Keys.End Or e.KeyCode = Keys.Tab Then
                    e.Handled = False
                Else
                    e.Handled = True
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvData_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellClick
        Dim frm As New frmAddEditResponse
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Select Case e.ColumnIndex
                Case objdgcolhEdit.Index
                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub
                    Dim dtResponseDate As Date : Dim intResponseType As Integer : Dim strResponseDescr, strResponseType As String

                    If IsDBNull(dgvData.Rows(e.RowIndex).Cells(dgcolhResponseDate.Index).Value) = False Then
                        dtResponseDate = CDate(dgvData.Rows(e.RowIndex).Cells(dgcolhResponseDate.Index).Value)
                    Else
                        dtResponseDate = ConfigParameter._Object._CurrentDateAndTime
                    End If
                    intResponseType = CInt(mdtCharges.Rows(e.RowIndex).Item("responsetypeunkid")) '  CInt(dgvData.Rows(e.RowIndex).Cells(dgcolhResponseType.Index).Value)
                    strResponseDescr = CStr(dgvData.Rows(e.RowIndex).Cells(dgcolhResponseText.Index).Value)
                    strResponseType = CStr(dgvData.Rows(e.RowIndex).Cells(dgcolhResponseType.Index).Value)

                    frm.displayDialog(dtResponseDate, intResponseType, strResponseDescr, strResponseType, CDate(txtChargeDate.Tag))
                    mdtCharges.Rows(e.RowIndex).Item("AUD") = "U"
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'If intResponseType > 0 Then
                    '    mdtCharges.Rows(e.RowIndex).Item("responsetypeunkid") = intResponseType
                    '    mdtCharges.Rows(e.RowIndex).Item("response_date") = dtResponseDate
                    '    mdtCharges.Rows(e.RowIndex).Item("response_remark") = strResponseDescr
                    '    mdtCharges.Rows(e.RowIndex).Item("response_type") = strResponseType
                    'End If
                        mdtCharges.Rows(e.RowIndex).Item("responsetypeunkid") = intResponseType
                        mdtCharges.Rows(e.RowIndex).Item("response_date") = dtResponseDate
                        mdtCharges.Rows(e.RowIndex).Item("response_remark") = strResponseDescr
                    If intResponseType > 0 Then
                        mdtCharges.Rows(e.RowIndex).Item("response_type") = strResponseType
                    Else
                        mdtCharges.Rows(e.RowIndex).Item("response_type") = ""
                    End If
                    'S.SANDEEP |01-OCT-2019| -- END
                    Call FillChargesTran()

                Case objdgcolhDelete.Index
                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    Dim strmessage As String = String.Empty
                    strmessage = objDiscChargeTran.AllowVoidingResponse(CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value))
                    If strmessage.Trim.Length > 0 Then
                        eZeeMsgBox.Show(strmessage, enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to delete response posted for the selected charge count?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        mdtCharges.Rows(e.RowIndex).Item("responsetypeunkid") = 0
                        mdtCharges.Rows(e.RowIndex).Item("response_date") = DBNull.Value
                        mdtCharges.Rows(e.RowIndex).Item("response_remark") = ""
                        mdtCharges.Rows(e.RowIndex).Item("response_type") = ""
                        mdtCharges.Rows(e.RowIndex).Item("AUD") = "U"
                        Call FillChargesTran()

                        Dim dtTable As DataTable = Nothing
                        Dim objScanAttachment As New clsScan_Attach_Documents
                        Dim xRow As DataRow() = Nothing
                        If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) > 0 Then
                            xRow = mdtScanAttachment.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND transactionunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) & "' AND form_name = '" & mstrModuleName & "' ")
                        End If
                        If xRow.Count > 0 Then
                            dtTable = xRow.CopyToDataTable
                        Else
                            dtTable = objScanAttachment._Datatable.Clone
                        End If
                        Dim dRows As DataRow() = dtTable.Select("")
                        dRows.ToList.ForEach(Function(x) Update_DataRow(x))
                    End If

                Case objdgcolhAttachment.Index
                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhAttachment.Index).Value Is imgBlank Then Exit Sub
                    If CStr(dgvData.Rows(e.RowIndex).Cells(dgcolhResponseText.Index).Value).Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please provide response in order to attach document(s)."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    Dim frmSA As New frmScanOrAttachmentInfo
                    If User._Object._Isrighttoleft = True Then
                        frmSA.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frmSA.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frmSA)
                    End If
                    If CInt(cboEmployee.SelectedValue) <= 0 Then Exit Sub
                    frmSA._TransactionID = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value)
                    frmSA._TransactionGuidString = CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value)
                    Dim dtTable As DataTable = Nothing
                    Dim objScanAttachment As New clsScan_Attach_Documents
                    Dim xRow As DataRow() = Nothing
                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) > 0 Then
                        xRow = mdtScanAttachment.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND transactionunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) & "' AND form_name = '" & mstrModuleName & "' ")
                        Dim drcnt As DataRow() = mdtCharges.Select("disciplinefiletranunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) & "' AND AUD <> 'D'")
                        If drcnt.Length > 0 Then
                            drcnt(0).Item("AUD") = "U"
                        End If
                    End If
                    If xRow.Count > 0 Then
                        dtTable = xRow.CopyToDataTable
                    Else
                        dtTable = objScanAttachment._Datatable.Clone
                    End If

                    frmSA._dtAttachment = dtTable.Copy

                    'S.SANDEEP |26-APR-2019| -- START
                    'If frmSA.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), _
                    '                                 enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, True, False) = True Then
                    If frmSA.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), _
                                                     enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, True, CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, True, False) = True Then
                        'S.SANDEEP |26-APR-2019| -- END
                        dtTable = frmSA._dtAttachment.Copy
                        frmSA._dtAttachment.Rows.Clear()

                        Dim dRows As DataRow() = dtTable.Select("")
                        dRows.ToList.ForEach(Function(x) Add_DataRow(x, e.RowIndex, mstrModuleName))
                    End If
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                Case objdgcolhDownloadAttachment.Index
                    Dim objScanAttachment As New clsScan_Attach_Documents
                    'S.SANDEEP |04-SEP-2021| -- START
                    'objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
                    objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , , , , CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                    'S.SANDEEP |04-SEP-2021| -- END
                    Dim xRows As DataRow() = objScanAttachment._Datatable.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND transactionunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) & "' AND form_name = '" & mstrModuleName & "'")
                    If xRows.Length > 0 Then
                        Dim strLocalPath As String = String.Empty
                        Dim fileToAdd As New List(Of String)
                        For Each xRow As DataRow In xRows
                            If IsDBNull(xRow("file_data")) = False Then
                                strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                                Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                                Dim fs As New FileStream(strLocalPath, FileMode.Create)
                                ms.WriteTo(fs)
                                ms.Close()
                                fs.Close()
                                fs.Dispose()
                                If strLocalPath <> "" Then
                                    fileToAdd.Add(strLocalPath)
                                End If
                            ElseIf IsDBNull(xRow("file_data")) = True Then
                                Dim mblnIsSelfServiceInstall As Boolean = IsSelfServiceExist()
                                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                                If mblnIsSelfServiceInstall Then
                                    Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(xRow("scanattachrefid").ToString)) Select (p.Item("Name").ToString)).FirstOrDefault
                                    Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFolderName, "", ConfigParameter._Object._ArutiSelfServiceURL)
                                    If imagebyte IsNot Nothing Then
                                        strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                                        Dim ms As New MemoryStream(imagebyte)
                                        Dim fs As New FileStream(strLocalPath, FileMode.Create)
                                        ms.WriteTo(fs)
                                        ms.Close()
                                        fs.Close()
                                        fs.Dispose()
                                    End If
                                Else
                                    strLocalPath = xRow("filepath").ToString()
                                End If
                                If strLocalPath <> "" Then
                                    fileToAdd.Add(strLocalPath)
                                End If
                            End If
                        Next

                        If fileToAdd.Count > 0 Then
                            Dim sfd As New FolderBrowserDialog
                            If sfd.ShowDialog() = DialogResult.OK Then
                                Using zip As Package = System.IO.Packaging.Package.Open(sfd.SelectedPath + "\" + txtReferenceNo.Text.Trim() + ".zip", FileMode.OpenOrCreate)
                                    For Each item As String In fileToAdd
                                        If IO.File.Exists(item) = False Then Continue For
                                        Dim destFilename As String = ".\" & Path.GetFileName(item)
                                        Dim uri As Uri = PackUriHelper.CreatePartUri(New Uri(destFilename, UriKind.Relative))
                                        If zip.PartExists(uri) Then
                                            zip.DeletePart(uri)
                                        End If
                                        Dim part As PackagePart = zip.CreatePart(uri, "", CompressionOption.Normal)
                                        Using fileStream As New FileStream(item, FileMode.Open, FileAccess.Read)
                                            Using dest As Stream = part.GetStream()
                                                CopyStream(CType(fileStream, Stream), part.GetStream())
                                            End Using
                                        End Using
                                    Next
                                End Using
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "File(s) downloaded successfully to the selected location."), enMsgBoxStyle.Information)
                            End If
                        End If
                        If fileToAdd.Count > 0 Then
                            For Each fl As String In fileToAdd
                                Try
                                    System.IO.File.Delete(fl)
                                Catch ex As Exception

                                End Try
                            Next
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, No file attached."), enMsgBoxStyle.Information)
                    End If
                    'S.SANDEEP |01-OCT-2019| -- END
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellClick", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub dgvData_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvData.Scroll
        Try
            If e.OldValue <> e.NewValue Then
                dgvData.PerformLayout()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_Scroll", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles dgvData.RowPostPaint
        Try
            Dim rect As Rectangle = dgvData.GetCellDisplayRectangle(dgcolhCount.Index, e.RowIndex, True)
            Dim strRowNumber As String = (e.RowIndex + 1).ToString()
            Dim size As SizeF = e.Graphics.MeasureString(strRowNumber, Me.Font)
            If dgvData.RowHeadersWidth < CInt(size.Width + 20) Then
                dgvData.RowHeadersWidth = CInt(size.Width + 20)
            End If
            Dim b As Brush = SystemBrushes.ControlText
            e.Graphics.DrawString(strRowNumber, Me.Font, b, rect.Location.X + 15, rect.Location.Y + ((rect.Height - size.Height) / 2))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_RowPostPaint", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbHearingInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbHearingInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.Name, Me.btnFinalSave.Text)
            Me.gbHearingInfo.Text = Language._Object.getCaption(Me.gbHearingInfo.Name, Me.gbHearingInfo.Text)
            Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.Name, Me.lblInterdictionDate.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblJobTitle.Text = Language._Object.getCaption(Me.lblJobTitle.Name, Me.lblJobTitle.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.lblGeneralChargeDescr.Text = Language._Object.getCaption(Me.lblGeneralChargeDescr.Name, Me.lblGeneralChargeDescr.Text)
            Me.elLine1.Text = Language._Object.getCaption(Me.elLine1.Name, Me.elLine1.Text)
            Me.dgcolhCount.HeaderText = Language._Object.getCaption(Me.dgcolhCount.Name, Me.dgcolhCount.HeaderText)
            Me.dgcolhIncident.HeaderText = Language._Object.getCaption(Me.dgcolhIncident.Name, Me.dgcolhIncident.HeaderText)
            Me.dgcolhOffCategory.HeaderText = Language._Object.getCaption(Me.dgcolhOffCategory.Name, Me.dgcolhOffCategory.HeaderText)
            Me.dgcolhOffence.HeaderText = Language._Object.getCaption(Me.dgcolhOffence.Name, Me.dgcolhOffence.HeaderText)
            Me.dgcolhSeverity.HeaderText = Language._Object.getCaption(Me.dgcolhSeverity.Name, Me.dgcolhSeverity.HeaderText)
            Me.dgcolhResponseType.HeaderText = Language._Object.getCaption(Me.dgcolhResponseType.Name, Me.dgcolhResponseType.HeaderText)
            Me.dgcolhResponseDate.HeaderText = Language._Object.getCaption(Me.dgcolhResponseDate.Name, Me.dgcolhResponseDate.HeaderText)
            Me.dgcolhResponseText.HeaderText = Language._Object.getCaption(Me.dgcolhResponseText.Name, Me.dgcolhResponseText.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Are you sure you want to delete response posted for the selected charge count?")
            Language.setMessage(mstrModuleName, 2, "Please provide response in order to attach document(s).")
            Language.setMessage(mstrModuleName, 3, "Select Employee")
            Language.setMessage(mstrModuleName, 4, "Configuration Path does not Exist.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class