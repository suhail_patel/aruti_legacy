﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmAddEditResponse

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAddEditResponse"
    Private mdtResponseDate As Date
    Private mintResponseType As Integer
    Private mstrResponseDescr As String
    Private mdtDisciplineDate As Date
    Private mstrResponseType As String

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef dtResponseDate As Date, ByRef intResponseType As Integer, ByRef strResponseDescr As String, ByRef strResponseType As String, ByVal dtDisciplineDate As Date) As Boolean
        Try
            mdtResponseDate = dtResponseDate
            mintResponseType = intResponseType
            mstrResponseDescr = strResponseDescr
            mdtDisciplineDate = dtDisciplineDate
            mstrResponseType = strResponseType

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            objpnlResponseType.Visible = ConfigParameter._Object._ShowReponseTypeOnPosting
            'S.SANDEEP |01-OCT-2019| -- END

            Me.ShowDialog()

            dtResponseDate = mdtResponseDate
            intResponseType = mintResponseType
            strResponseDescr = mstrResponseDescr
            strResponseType = mstrResponseType

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtResponse.BackColor = GUI.ColorComp
            cboResponseType.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            dtpResponseDate.Value = mdtResponseDate
            cboResponseType.SelectedValue = mintResponseType
            txtResponse.Text = mstrResponseDescr
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            mdtResponseDate = dtpResponseDate.Value
            mintResponseType = CInt(cboResponseType.SelectedValue)
            mstrResponseDescr = txtResponse.Text
            mstrResponseType = cboResponseType.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If dtpResponseDate.Checked = True Then

                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'If CInt(cboResponseType.SelectedValue) <= 0 Then
                If objbtnAddResponseType.Visible = True AndAlso CInt(cboResponseType.SelectedValue) <= 0 Then
                    'S.SANDEEP |01-OCT-2019| -- END
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Response type is mandatory information. Please select Response type to continue."), enMsgBoxStyle.Information)
                    txtResponse.Focus()
                    Return False
                End If

                If txtResponse.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Response remark is mandatory information. Response remark cannot be blank."), enMsgBoxStyle.Information)
                    txtResponse.Focus()
                    Return False
                End If

                If dtpResponseDate.Value.Date < mdtDisciplineDate.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Response date cannot be less than the charge date. Please set correct response date to continue."), enMsgBoxStyle.Information)
                    dtpResponseDate.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'If CInt(cboResponseType.SelectedValue) > 0 Then
            If objbtnAddResponseType.Visible = True AndAlso CInt(cboResponseType.SelectedValue) > 0 Then
                'S.SANDEEP |01-OCT-2019| -- END
                If dtpResponseDate.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Response date is mandatory information. Please provide Response Date."), enMsgBoxStyle.Information)
                    dtpResponseDate.Focus()
                    Return False
                End If

                If txtResponse.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Response remark is mandatory information. Response remark cannot be blank."), enMsgBoxStyle.Information)
                    txtResponse.Focus()
                    Return False
                End If
            End If

            If txtResponse.Text.Trim.Length > 0 Then
                If dtpResponseDate.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Response date is mandatory information. Please provide Response Date."), enMsgBoxStyle.Information)
                    dtpResponseDate.Focus()
                    Return False
                End If
                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'If CInt(cboResponseType.SelectedValue) <= 0 Then
                If objbtnAddResponseType.Visible = True AndAlso CInt(cboResponseType.SelectedValue) <= 0 Then
                    'S.SANDEEP |01-OCT-2019| -- END
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Response type is mandatory information. Please select Response type to continue."), enMsgBoxStyle.Information)
                    txtResponse.Focus()
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCMaster As New clsCommon_Master
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE, True, "List")
            With cboResponseType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCMaster = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAddEditResponse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            Call FillCombo()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditResponse_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Validation() = False Then Exit Sub
            Call SetValue()
            Call btnClose_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAddResponseType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResponseType.Click
        Dim frm As New frmCommonMaster
        Try
            Dim iRetVal As Integer = -1
            frm.displayDialog(iRetVal, clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE, enAction.ADD_ONE)
            If iRetVal > 0 Then
                Dim objCMaster As New clsCommon_Master
                Dim dList As New DataSet
                dList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE, True, "List")
                With cboResponseType
                    .DisplayMember = "name"
                    .ValueMember = "masterunkid"
                    .DataSource = dList.Tables("List")
                    .SelectedValue = iRetVal
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnAddResponseType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchResponseType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResponseType.Click
        Dim frm As New frmCommonSearch
        Try
            If cboResponseType.DataSource Is Nothing Then Exit Sub
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboResponseType.ValueMember
                .DisplayMember = cboResponseType.DisplayMember
                .DataSource = CType(cboResponseType.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboResponseType.SelectedValue = frm.SelectedValue
                cboResponseType.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResponseType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbChargeCount.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbChargeCount.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbChargeCount.Text = Language._Object.getCaption(Me.gbChargeCount.Name, Me.gbChargeCount.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblResponse.Text = Language._Object.getCaption(Me.lblResponse.Name, Me.lblResponse.Text)
            Me.lblReponseDate.Text = Language._Object.getCaption(Me.lblReponseDate.Name, Me.lblReponseDate.Text)
            Me.lblResponseType.Text = Language._Object.getCaption(Me.lblResponseType.Name, Me.lblResponseType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Response type is mandatory information. Please select Response type to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Response remark is mandatory information. Response remark cannot be blank.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Response date cannot be less than the charge date. Please set correct response date to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Response date is mandatory information. Please provide Response Date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class