﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineFiling
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineFiling))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbDiscipilneFiling = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIsChargeVisibleOnEss = New System.Windows.Forms.CheckBox
        Me.lnkAddCount = New System.Windows.Forms.LinkLabel
        Me.lblGeneralChargeDescr = New System.Windows.Forms.Label
        Me.txtChargeDescription = New System.Windows.Forms.TextBox
        Me.txtReferenceNo = New eZee.TextBox.AlphanumericTextBox
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhAttachment = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhR_Docs = New System.Windows.Forms.DataGridViewLinkColumn
        Me.dgcolhCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffence = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIncident = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSeverity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhoffencecategoryunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhoffenceunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfileunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfiletraunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResponseText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.elLine1 = New eZee.Common.eZeeLine
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.chkNotifyEmployee = New System.Windows.Forms.CheckBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.txtJobTitle = New eZee.TextBox.AlphanumericTextBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblJobTitle = New System.Windows.Forms.Label
        Me.dtpInterdictiondate = New System.Windows.Forms.DateTimePicker
        Me.lblInterdictionDate = New System.Windows.Forms.Label
        Me.dtpDisciplineDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnScanAttachDoc = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnSaveClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbDiscipilneFiling.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbDiscipilneFiling)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(879, 517)
        Me.pnlMain.TabIndex = 0
        '
        'gbDiscipilneFiling
        '
        Me.gbDiscipilneFiling.BorderColor = System.Drawing.Color.Black
        Me.gbDiscipilneFiling.Checked = False
        Me.gbDiscipilneFiling.CollapseAllExceptThis = False
        Me.gbDiscipilneFiling.CollapsedHoverImage = Nothing
        Me.gbDiscipilneFiling.CollapsedNormalImage = Nothing
        Me.gbDiscipilneFiling.CollapsedPressedImage = Nothing
        Me.gbDiscipilneFiling.CollapseOnLoad = False
        Me.gbDiscipilneFiling.Controls.Add(Me.chkIsChargeVisibleOnEss)
        Me.gbDiscipilneFiling.Controls.Add(Me.lnkAddCount)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblGeneralChargeDescr)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtChargeDescription)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtReferenceNo)
        Me.gbDiscipilneFiling.Controls.Add(Me.pnlData)
        Me.gbDiscipilneFiling.Controls.Add(Me.elLine1)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblRefNo)
        Me.gbDiscipilneFiling.Controls.Add(Me.chkNotifyEmployee)
        Me.gbDiscipilneFiling.Controls.Add(Me.cboEmployee)
        Me.gbDiscipilneFiling.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtDepartment)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtJobTitle)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblDepartment)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblJobTitle)
        Me.gbDiscipilneFiling.Controls.Add(Me.dtpInterdictiondate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblInterdictionDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.dtpDisciplineDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblPersonalInvolved)
        Me.gbDiscipilneFiling.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbDiscipilneFiling.ExpandedHoverImage = Nothing
        Me.gbDiscipilneFiling.ExpandedNormalImage = Nothing
        Me.gbDiscipilneFiling.ExpandedPressedImage = Nothing
        Me.gbDiscipilneFiling.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDiscipilneFiling.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDiscipilneFiling.HeaderHeight = 25
        Me.gbDiscipilneFiling.HeaderMessage = ""
        Me.gbDiscipilneFiling.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDiscipilneFiling.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDiscipilneFiling.HeightOnCollapse = 0
        Me.gbDiscipilneFiling.LeftTextSpace = 0
        Me.gbDiscipilneFiling.Location = New System.Drawing.Point(0, 0)
        Me.gbDiscipilneFiling.Name = "gbDiscipilneFiling"
        Me.gbDiscipilneFiling.OpenHeight = 300
        Me.gbDiscipilneFiling.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDiscipilneFiling.ShowBorder = True
        Me.gbDiscipilneFiling.ShowCheckBox = False
        Me.gbDiscipilneFiling.ShowCollapseButton = False
        Me.gbDiscipilneFiling.ShowDefaultBorderColor = True
        Me.gbDiscipilneFiling.ShowDownButton = False
        Me.gbDiscipilneFiling.ShowHeader = True
        Me.gbDiscipilneFiling.Size = New System.Drawing.Size(879, 462)
        Me.gbDiscipilneFiling.TabIndex = 86
        Me.gbDiscipilneFiling.Temp = 0
        Me.gbDiscipilneFiling.Text = "Disciplinary Charge Information"
        Me.gbDiscipilneFiling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIsChargeVisibleOnEss
        '
        Me.chkIsChargeVisibleOnEss.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkIsChargeVisibleOnEss.BackColor = System.Drawing.Color.Transparent
        Me.chkIsChargeVisibleOnEss.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsChargeVisibleOnEss.Location = New System.Drawing.Point(528, 148)
        Me.chkIsChargeVisibleOnEss.Name = "chkIsChargeVisibleOnEss"
        Me.chkIsChargeVisibleOnEss.Size = New System.Drawing.Size(340, 17)
        Me.chkIsChargeVisibleOnEss.TabIndex = 143
        Me.chkIsChargeVisibleOnEss.Text = "Make This Disciplanly Charge Visible on ESS"
        Me.chkIsChargeVisibleOnEss.UseVisualStyleBackColor = False
        '
        'lnkAddCount
        '
        Me.lnkAddCount.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAddCount.Location = New System.Drawing.Point(12, 173)
        Me.lnkAddCount.Name = "lnkAddCount"
        Me.lnkAddCount.Size = New System.Drawing.Size(94, 13)
        Me.lnkAddCount.TabIndex = 141
        Me.lnkAddCount.TabStop = True
        Me.lnkAddCount.Text = "Add New Count"
        Me.lnkAddCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGeneralChargeDescr
        '
        Me.lblGeneralChargeDescr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGeneralChargeDescr.Location = New System.Drawing.Point(450, 41)
        Me.lblGeneralChargeDescr.Name = "lblGeneralChargeDescr"
        Me.lblGeneralChargeDescr.Size = New System.Drawing.Size(72, 51)
        Me.lblGeneralChargeDescr.TabIndex = 140
        Me.lblGeneralChargeDescr.Text = "General Charge Description"
        '
        'txtChargeDescription
        '
        Me.txtChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDescription.Location = New System.Drawing.Point(528, 38)
        Me.txtChargeDescription.Multiline = True
        Me.txtChargeDescription.Name = "txtChargeDescription"
        Me.txtChargeDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtChargeDescription.Size = New System.Drawing.Size(340, 102)
        Me.txtChargeDescription.TabIndex = 119
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtReferenceNo.Flags = 0
        Me.txtReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferenceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReferenceNo.Location = New System.Drawing.Point(110, 65)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.Size = New System.Drawing.Size(307, 21)
        Me.txtReferenceNo.TabIndex = 95
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(12, 192)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(856, 264)
        Me.pnlData.TabIndex = 136
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeight = 24
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhAttachment, Me.dgcolhR_Docs, Me.dgcolhCount, Me.dgcolhOffCategory, Me.dgcolhOffence, Me.dgcolhIncident, Me.dgcolhSeverity, Me.objdgcolhoffencecategoryunkid, Me.objdgcolhoffenceunkid, Me.objdgcolhGUID, Me.objdgcolhfileunkid, Me.objdgcolhfiletraunkid, Me.dgcolhResponseType, Me.dgcolhResponseDate, Me.dgcolhResponseText})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.ReadOnly = True
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(856, 264)
        Me.dgvData.TabIndex = 0
        '
        'objdgcolhEdit
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.NullValue = Nothing
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White
        Me.objdgcolhEdit.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.NullValue = Nothing
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White
        Me.objdgcolhDelete.DefaultCellStyle = DataGridViewCellStyle2
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhAttachment
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.NullValue = Nothing
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Blue
        Me.objdgcolhAttachment.DefaultCellStyle = DataGridViewCellStyle3
        Me.objdgcolhAttachment.HeaderText = ""
        Me.objdgcolhAttachment.Image = Global.Aruti.Main.My.Resources.Resources.add_docs
        Me.objdgcolhAttachment.Name = "objdgcolhAttachment"
        Me.objdgcolhAttachment.ReadOnly = True
        Me.objdgcolhAttachment.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAttachment.Width = 25
        '
        'dgcolhR_Docs
        '
        Me.dgcolhR_Docs.ActiveLinkColor = System.Drawing.Color.Blue
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Blue
        Me.dgcolhR_Docs.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhR_Docs.HeaderText = "Attach Docs"
        Me.dgcolhR_Docs.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.dgcolhR_Docs.Name = "dgcolhR_Docs"
        Me.dgcolhR_Docs.ReadOnly = True
        Me.dgcolhR_Docs.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhR_Docs.Text = "Response Docs"
        Me.dgcolhR_Docs.UseColumnTextForLinkValue = True
        Me.dgcolhR_Docs.Visible = False
        Me.dgcolhR_Docs.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'dgcolhCount
        '
        Me.dgcolhCount.HeaderText = "Count"
        Me.dgcolhCount.Name = "dgcolhCount"
        Me.dgcolhCount.ReadOnly = True
        Me.dgcolhCount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCount.Width = 50
        '
        'dgcolhOffCategory
        '
        Me.dgcolhOffCategory.HeaderText = "Off. Category"
        Me.dgcolhOffCategory.Name = "dgcolhOffCategory"
        Me.dgcolhOffCategory.ReadOnly = True
        Me.dgcolhOffCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOffCategory.Width = 130
        '
        'dgcolhOffence
        '
        Me.dgcolhOffence.HeaderText = "Offence"
        Me.dgcolhOffence.Name = "dgcolhOffence"
        Me.dgcolhOffence.ReadOnly = True
        Me.dgcolhOffence.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOffence.Width = 230
        '
        'dgcolhIncident
        '
        Me.dgcolhIncident.HeaderText = "Incident"
        Me.dgcolhIncident.Name = "dgcolhIncident"
        Me.dgcolhIncident.ReadOnly = True
        Me.dgcolhIncident.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhIncident.Width = 305
        '
        'dgcolhSeverity
        '
        Me.dgcolhSeverity.HeaderText = "Severity"
        Me.dgcolhSeverity.Name = "dgcolhSeverity"
        Me.dgcolhSeverity.ReadOnly = True
        Me.dgcolhSeverity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSeverity.Width = 62
        '
        'objdgcolhoffencecategoryunkid
        '
        Me.objdgcolhoffencecategoryunkid.HeaderText = "objdgcolhoffencecategoryunkid"
        Me.objdgcolhoffencecategoryunkid.Name = "objdgcolhoffencecategoryunkid"
        Me.objdgcolhoffencecategoryunkid.ReadOnly = True
        Me.objdgcolhoffencecategoryunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhoffencecategoryunkid.Visible = False
        '
        'objdgcolhoffenceunkid
        '
        Me.objdgcolhoffenceunkid.HeaderText = "objdgcolhoffenceunkid"
        Me.objdgcolhoffenceunkid.Name = "objdgcolhoffenceunkid"
        Me.objdgcolhoffenceunkid.ReadOnly = True
        Me.objdgcolhoffenceunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhoffenceunkid.Visible = False
        '
        'objdgcolhGUID
        '
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.ReadOnly = True
        Me.objdgcolhGUID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGUID.Visible = False
        '
        'objdgcolhfileunkid
        '
        Me.objdgcolhfileunkid.HeaderText = "objdgcolhfileunkid"
        Me.objdgcolhfileunkid.Name = "objdgcolhfileunkid"
        Me.objdgcolhfileunkid.ReadOnly = True
        Me.objdgcolhfileunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhfileunkid.Visible = False
        '
        'objdgcolhfiletraunkid
        '
        Me.objdgcolhfiletraunkid.HeaderText = "objdgcolhfiletraunkid"
        Me.objdgcolhfiletraunkid.Name = "objdgcolhfiletraunkid"
        Me.objdgcolhfiletraunkid.ReadOnly = True
        Me.objdgcolhfiletraunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhfiletraunkid.Visible = False
        '
        'dgcolhResponseType
        '
        Me.dgcolhResponseType.HeaderText = "Response Type"
        Me.dgcolhResponseType.Name = "dgcolhResponseType"
        Me.dgcolhResponseType.ReadOnly = True
        Me.dgcolhResponseType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhResponseType.Visible = False
        '
        'dgcolhResponseDate
        '
        Me.dgcolhResponseDate.HeaderText = "Response Date"
        Me.dgcolhResponseDate.Name = "dgcolhResponseDate"
        Me.dgcolhResponseDate.ReadOnly = True
        Me.dgcolhResponseDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhResponseDate.Visible = False
        '
        'dgcolhResponseText
        '
        Me.dgcolhResponseText.HeaderText = "Response"
        Me.dgcolhResponseText.Name = "dgcolhResponseText"
        Me.dgcolhResponseText.ReadOnly = True
        Me.dgcolhResponseText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhResponseText.Visible = False
        '
        'elLine1
        '
        Me.elLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLine1.Location = New System.Drawing.Point(12, 171)
        Me.elLine1.Name = "elLine1"
        Me.elLine1.Size = New System.Drawing.Size(856, 18)
        Me.elLine1.TabIndex = 117
        Me.elLine1.Text = "Charges Count Information"
        Me.elLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(12, 67)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(92, 17)
        Me.lblRefNo.TabIndex = 96
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkNotifyEmployee
        '
        Me.chkNotifyEmployee.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkNotifyEmployee.BackColor = System.Drawing.Color.Transparent
        Me.chkNotifyEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNotifyEmployee.Location = New System.Drawing.Point(596, 4)
        Me.chkNotifyEmployee.Name = "chkNotifyEmployee"
        Me.chkNotifyEmployee.Size = New System.Drawing.Size(272, 17)
        Me.chkNotifyEmployee.TabIndex = 116
        Me.chkNotifyEmployee.Text = "Notify Employee for this discipline charge"
        Me.chkNotifyEmployee.UseVisualStyleBackColor = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(110, 92)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(307, 21)
        Me.cboEmployee.TabIndex = 111
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(423, 92)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 112
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(110, 146)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(307, 21)
        Me.txtDepartment.TabIndex = 108
        '
        'txtJobTitle
        '
        Me.txtJobTitle.BackColor = System.Drawing.SystemColors.Window
        Me.txtJobTitle.Flags = 0
        Me.txtJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJobTitle.Location = New System.Drawing.Point(110, 119)
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.ReadOnly = True
        Me.txtJobTitle.Size = New System.Drawing.Size(307, 21)
        Me.txtJobTitle.TabIndex = 107
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(12, 148)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(92, 17)
        Me.lblDepartment.TabIndex = 106
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobTitle
        '
        Me.lblJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobTitle.Location = New System.Drawing.Point(12, 121)
        Me.lblJobTitle.Name = "lblJobTitle"
        Me.lblJobTitle.Size = New System.Drawing.Size(92, 17)
        Me.lblJobTitle.TabIndex = 105
        Me.lblJobTitle.Text = "Job Title"
        Me.lblJobTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpInterdictiondate
        '
        Me.dtpInterdictiondate.Checked = False
        Me.dtpInterdictiondate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInterdictiondate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInterdictiondate.Location = New System.Drawing.Point(307, 38)
        Me.dtpInterdictiondate.Name = "dtpInterdictiondate"
        Me.dtpInterdictiondate.ShowCheckBox = True
        Me.dtpInterdictiondate.Size = New System.Drawing.Size(110, 21)
        Me.dtpInterdictiondate.TabIndex = 98
        '
        'lblInterdictionDate
        '
        Me.lblInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterdictionDate.Location = New System.Drawing.Point(206, 40)
        Me.lblInterdictionDate.Name = "lblInterdictionDate"
        Me.lblInterdictionDate.Size = New System.Drawing.Size(95, 17)
        Me.lblInterdictionDate.TabIndex = 97
        Me.lblInterdictionDate.Text = "Interdiction Date "
        Me.lblInterdictionDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDisciplineDate
        '
        Me.dtpDisciplineDate.Checked = False
        Me.dtpDisciplineDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDisciplineDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDisciplineDate.Location = New System.Drawing.Point(110, 38)
        Me.dtpDisciplineDate.Name = "dtpDisciplineDate"
        Me.dtpDisciplineDate.Size = New System.Drawing.Size(90, 21)
        Me.dtpDisciplineDate.TabIndex = 71
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(12, 40)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(92, 17)
        Me.lblDate.TabIndex = 72
        Me.lblDate.Text = "Charge Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(12, 94)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(92, 17)
        Me.lblPersonalInvolved.TabIndex = 78
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSaveClose)
        Me.objFooter.Controls.Add(Me.btnScanAttachDoc)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 462)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(879, 55)
        Me.objFooter.TabIndex = 85
        '
        'btnScanAttachDoc
        '
        Me.btnScanAttachDoc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnScanAttachDoc.BackColor = System.Drawing.Color.White
        Me.btnScanAttachDoc.BackgroundImage = CType(resources.GetObject("btnScanAttachDoc.BackgroundImage"), System.Drawing.Image)
        Me.btnScanAttachDoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnScanAttachDoc.BorderColor = System.Drawing.Color.Empty
        Me.btnScanAttachDoc.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnScanAttachDoc.FlatAppearance.BorderSize = 0
        Me.btnScanAttachDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnScanAttachDoc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScanAttachDoc.ForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnScanAttachDoc.GradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScanAttachDoc.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.Location = New System.Drawing.Point(12, 13)
        Me.btnScanAttachDoc.Name = "btnScanAttachDoc"
        Me.btnScanAttachDoc.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScanAttachDoc.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.Size = New System.Drawing.Size(121, 30)
        Me.btnScanAttachDoc.TabIndex = 17
        Me.btnScanAttachDoc.Text = "&Attach Document"
        Me.btnScanAttachDoc.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(667, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(770, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Count"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 50
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Incident"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 130
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Off. Category"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 130
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Offence"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 130
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.Frozen = True
        Me.DataGridViewTextBoxColumn5.HeaderText = "Severity"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 62
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.Frozen = True
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhoffencecategoryunkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.Frozen = True
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhoffenceunkid"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.Frozen = True
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.Frozen = True
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhfileunkid"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.Frozen = True
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhfiletraunkid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.Frozen = True
        Me.DataGridViewTextBoxColumn11.HeaderText = "Response Type"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.Frozen = True
        Me.DataGridViewTextBoxColumn12.HeaderText = "Response Date"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.Frozen = True
        Me.DataGridViewTextBoxColumn13.HeaderText = "Response"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'btnSaveClose
        '
        Me.btnSaveClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveClose.BackColor = System.Drawing.Color.White
        Me.btnSaveClose.BackgroundImage = CType(resources.GetObject("btnSaveClose.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveClose.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveClose.FlatAppearance.BorderSize = 0
        Me.btnSaveClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveClose.ForeColor = System.Drawing.Color.Black
        Me.btnSaveClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveClose.Location = New System.Drawing.Point(522, 13)
        Me.btnSaveClose.Name = "btnSaveClose"
        Me.btnSaveClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveClose.Size = New System.Drawing.Size(139, 30)
        Me.btnSaveClose.TabIndex = 18
        Me.btnSaveClose.Text = "Save && Close Case"
        Me.btnSaveClose.UseVisualStyleBackColor = True
        Me.btnSaveClose.Visible = False
        '
        'frmDisciplineFiling
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(879, 517)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineFiling"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Filing Disciplinary Charge "
        Me.pnlMain.ResumeLayout(False)
        Me.gbDiscipilneFiling.ResumeLayout(False)
        Me.gbDiscipilneFiling.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents dtpDisciplineDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbDiscipilneFiling As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblInterdictionDate As System.Windows.Forms.Label
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents txtReferenceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpInterdictiondate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtJobTitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblJobTitle As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents chkNotifyEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents txtChargeDescription As System.Windows.Forms.TextBox
    Friend WithEvents btnScanAttachDoc As eZee.Common.eZeeLightButton
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lblGeneralChargeDescr As System.Windows.Forms.Label
    Friend WithEvents elLine1 As eZee.Common.eZeeLine
    Friend WithEvents lnkAddCount As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhAttachment As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhR_Docs As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents dgcolhCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffence As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIncident As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSeverity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhoffencecategoryunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhoffenceunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfileunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfiletraunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResponseText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkIsChargeVisibleOnEss As System.Windows.Forms.CheckBox
    Friend WithEvents btnSaveClose As eZee.Common.eZeeLightButton
End Class
