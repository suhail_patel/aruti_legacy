﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddEditResponse
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddEditResponse))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbChargeCount = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.objpnlResponseType = New System.Windows.Forms.Panel
        Me.cboResponseType = New System.Windows.Forms.ComboBox
        Me.objbtnAddResponseType = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchResponseType = New eZee.Common.eZeeGradientButton
        Me.lblResponseType = New System.Windows.Forms.Label
        Me.objpnlResponseDescription = New System.Windows.Forms.Panel
        Me.txtResponse = New System.Windows.Forms.TextBox
        Me.lblResponse = New System.Windows.Forms.Label
        Me.dtpResponseDate = New System.Windows.Forms.DateTimePicker
        Me.lblReponseDate = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbChargeCount.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.objpnlResponseType.SuspendLayout()
        Me.objpnlResponseDescription.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbChargeCount)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(478, 263)
        Me.pnlMain.TabIndex = 1
        '
        'gbChargeCount
        '
        Me.gbChargeCount.BorderColor = System.Drawing.Color.Black
        Me.gbChargeCount.Checked = False
        Me.gbChargeCount.CollapseAllExceptThis = False
        Me.gbChargeCount.CollapsedHoverImage = Nothing
        Me.gbChargeCount.CollapsedNormalImage = Nothing
        Me.gbChargeCount.CollapsedPressedImage = Nothing
        Me.gbChargeCount.CollapseOnLoad = False
        Me.gbChargeCount.Controls.Add(Me.FlowLayoutPanel1)
        Me.gbChargeCount.Controls.Add(Me.dtpResponseDate)
        Me.gbChargeCount.Controls.Add(Me.lblReponseDate)
        Me.gbChargeCount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbChargeCount.ExpandedHoverImage = Nothing
        Me.gbChargeCount.ExpandedNormalImage = Nothing
        Me.gbChargeCount.ExpandedPressedImage = Nothing
        Me.gbChargeCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbChargeCount.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbChargeCount.HeaderHeight = 25
        Me.gbChargeCount.HeaderMessage = ""
        Me.gbChargeCount.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbChargeCount.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbChargeCount.HeightOnCollapse = 0
        Me.gbChargeCount.LeftTextSpace = 0
        Me.gbChargeCount.Location = New System.Drawing.Point(0, 0)
        Me.gbChargeCount.Name = "gbChargeCount"
        Me.gbChargeCount.OpenHeight = 300
        Me.gbChargeCount.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbChargeCount.ShowBorder = True
        Me.gbChargeCount.ShowCheckBox = False
        Me.gbChargeCount.ShowCollapseButton = False
        Me.gbChargeCount.ShowDefaultBorderColor = True
        Me.gbChargeCount.ShowDownButton = False
        Me.gbChargeCount.ShowHeader = True
        Me.gbChargeCount.Size = New System.Drawing.Size(478, 208)
        Me.gbChargeCount.TabIndex = 1
        Me.gbChargeCount.Temp = 0
        Me.gbChargeCount.Text = "Response Information"
        Me.gbChargeCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.objpnlResponseType)
        Me.FlowLayoutPanel1.Controls.Add(Me.objpnlResponseDescription)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(2, 63)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(475, 140)
        Me.FlowLayoutPanel1.TabIndex = 163
        '
        'objpnlResponseType
        '
        Me.objpnlResponseType.Controls.Add(Me.cboResponseType)
        Me.objpnlResponseType.Controls.Add(Me.objbtnAddResponseType)
        Me.objpnlResponseType.Controls.Add(Me.objbtnSearchResponseType)
        Me.objpnlResponseType.Controls.Add(Me.lblResponseType)
        Me.objpnlResponseType.Location = New System.Drawing.Point(3, 3)
        Me.objpnlResponseType.Name = "objpnlResponseType"
        Me.objpnlResponseType.Size = New System.Drawing.Size(469, 25)
        Me.objpnlResponseType.TabIndex = 160
        '
        'cboResponseType
        '
        Me.cboResponseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResponseType.DropDownWidth = 300
        Me.cboResponseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResponseType.FormattingEnabled = True
        Me.cboResponseType.Location = New System.Drawing.Point(129, 2)
        Me.cboResponseType.Name = "cboResponseType"
        Me.cboResponseType.Size = New System.Drawing.Size(280, 21)
        Me.cboResponseType.TabIndex = 154
        '
        'objbtnAddResponseType
        '
        Me.objbtnAddResponseType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResponseType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResponseType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResponseType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResponseType.BorderSelected = False
        Me.objbtnAddResponseType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResponseType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResponseType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResponseType.Location = New System.Drawing.Point(442, 2)
        Me.objbtnAddResponseType.Name = "objbtnAddResponseType"
        Me.objbtnAddResponseType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResponseType.TabIndex = 158
        '
        'objbtnSearchResponseType
        '
        Me.objbtnSearchResponseType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResponseType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResponseType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResponseType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResponseType.BorderSelected = False
        Me.objbtnSearchResponseType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResponseType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResponseType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResponseType.Location = New System.Drawing.Point(415, 2)
        Me.objbtnSearchResponseType.Name = "objbtnSearchResponseType"
        Me.objbtnSearchResponseType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResponseType.TabIndex = 157
        '
        'lblResponseType
        '
        Me.lblResponseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResponseType.Location = New System.Drawing.Point(9, 4)
        Me.lblResponseType.Name = "lblResponseType"
        Me.lblResponseType.Size = New System.Drawing.Size(114, 17)
        Me.lblResponseType.TabIndex = 153
        Me.lblResponseType.Text = "Reponse Type"
        Me.lblResponseType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlResponseDescription
        '
        Me.objpnlResponseDescription.Controls.Add(Me.txtResponse)
        Me.objpnlResponseDescription.Controls.Add(Me.lblResponse)
        Me.objpnlResponseDescription.Location = New System.Drawing.Point(3, 34)
        Me.objpnlResponseDescription.Name = "objpnlResponseDescription"
        Me.objpnlResponseDescription.Size = New System.Drawing.Size(469, 103)
        Me.objpnlResponseDescription.TabIndex = 161
        '
        'txtResponse
        '
        Me.txtResponse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResponse.Location = New System.Drawing.Point(129, 2)
        Me.txtResponse.Multiline = True
        Me.txtResponse.Name = "txtResponse"
        Me.txtResponse.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtResponse.Size = New System.Drawing.Size(334, 99)
        Me.txtResponse.TabIndex = 156
        '
        'lblResponse
        '
        Me.lblResponse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResponse.Location = New System.Drawing.Point(9, 5)
        Me.lblResponse.Name = "lblResponse"
        Me.lblResponse.Size = New System.Drawing.Size(114, 17)
        Me.lblResponse.TabIndex = 155
        Me.lblResponse.Text = "Reponse Description"
        '
        'dtpResponseDate
        '
        Me.dtpResponseDate.Enabled = False
        Me.dtpResponseDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpResponseDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpResponseDate.Location = New System.Drawing.Point(131, 36)
        Me.dtpResponseDate.Name = "dtpResponseDate"
        Me.dtpResponseDate.Size = New System.Drawing.Size(108, 21)
        Me.dtpResponseDate.TabIndex = 151
        '
        'lblReponseDate
        '
        Me.lblReponseDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReponseDate.Location = New System.Drawing.Point(11, 38)
        Me.lblReponseDate.Name = "lblReponseDate"
        Me.lblReponseDate.Size = New System.Drawing.Size(114, 17)
        Me.lblReponseDate.TabIndex = 152
        Me.lblReponseDate.Text = "Reponse Date"
        Me.lblReponseDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 208)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(478, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(266, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(369, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmAddEditResponse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(478, 263)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAddEditResponse"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Response"
        Me.pnlMain.ResumeLayout(False)
        Me.gbChargeCount.ResumeLayout(False)
        Me.gbChargeCount.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.objpnlResponseType.ResumeLayout(False)
        Me.objpnlResponseDescription.ResumeLayout(False)
        Me.objpnlResponseDescription.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbChargeCount As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnAddResponseType As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpResponseDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboResponseType As System.Windows.Forms.ComboBox
    Friend WithEvents lblResponse As System.Windows.Forms.Label
    Friend WithEvents txtResponse As System.Windows.Forms.TextBox
    Friend WithEvents lblReponseDate As System.Windows.Forms.Label
    Friend WithEvents lblResponseType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchResponseType As eZee.Common.eZeeGradientButton
    Friend WithEvents objpnlResponseDescription As System.Windows.Forms.Panel
    Friend WithEvents objpnlResponseType As System.Windows.Forms.Panel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
End Class
