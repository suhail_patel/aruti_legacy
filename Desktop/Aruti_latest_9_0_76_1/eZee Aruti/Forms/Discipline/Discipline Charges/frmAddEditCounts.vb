﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmAddEditCounts

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAddEditCounts"
    Private mintDisciplineFileUnkid As Integer = -1
    Private objDiscChargeTran As clsDiscipline_file_tran
    Private mdtCharges As DataTable
    Private menAction As enAction = enAction.ADD_ONE
    Private mintFileTranId As Integer = 0
    Private mstrGuidValue As String = String.Empty
    Private mdtDisciplineDate As Date = Nothing
    Private mintSeverity As Integer = 0
    Private xEditRow As DataRow() = Nothing
    Private mdtScanAttachDoc As DataTable = Nothing
    Private mintEmpId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFileTranId As Integer, _
                                  ByVal eAction As enAction, _
                                  ByVal strGuidValue As String, _
                                  ByVal intDisciplineFileUnkid As Integer, _
                                  ByVal dtDisciplineDate As Date, _
                                  ByRef dtCharges As DataTable, ByVal intEmployeeId As Integer, ByRef dtScanAttachDoc As DataTable) As Boolean
        Try
            mintFileTranId = intFileTranId
            mstrGuidValue = strGuidValue
            mdtCharges = dtCharges
            menAction = eAction
            If menAction <> enAction.EDIT_ONE Then
                objbtnProcess.Text = Language.getMessage(mstrModuleName, 9, "Add")
            Else
                objbtnProcess.Text = Language.getMessage(mstrModuleName, 10, "Edit")
            End If
            mdtDisciplineDate = dtDisciplineDate
            mintDisciplineFileUnkid = intDisciplineFileUnkid
            mdtScanAttachDoc = dtScanAttachDoc
            mintEmpId = intEmployeeId
            Me.ShowDialog()
            dtCharges = mdtCharges
            dtScanAttachDoc = mdtScanAttachDoc
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtResponse.BackColor = GUI.ColorComp
            cboResponseType.BackColor = GUI.ColorComp
            txtIncident.BackColor = GUI.ColorComp
            cboOffenceCategory.BackColor = GUI.ColorComp
            cboDisciplineType.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If mdtCharges IsNot Nothing AndAlso mdtCharges.Rows.Count > 0 Then

                If mintFileTranId > 0 Then
                    xEditRow = mdtCharges.Select("disciplinefiletranunkid = '" & mintFileTranId & "' AND AUD <> 'D'")

                ElseIf mstrGuidValue.Trim.Length > 0 Then
                    xEditRow = mdtCharges.Select("GUID = '" & mstrGuidValue & "' AND AUD <> 'D'")
                End If

                If xEditRow.Count > 0 Then
                    cboOffenceCategory.SelectedValue = CInt(xEditRow(0).Item("offencecategoryunkid"))
                    cboDisciplineType.SelectedValue = CInt(xEditRow(0).Item("offenceunkid"))
                    txtIncident.Text = CStr(xEditRow(0).Item("incident_description"))
                    'cboResponseType.SelectedValue = CInt(xEditRow(0).Item("responsetypeunkid"))
                    'If IsDBNull(xEditRow(0).Item("response_date")) = False Then
                    '    dtpResponseDate.Value = CDate(xEditRow(0).Item("response_date"))
                    '    dtpResponseDate.Checked = True
                    'End If
                    'txtResponse.Text = CStr(xEditRow(0).Item("response_remark"))

                    'If CInt(cboResponseType.SelectedValue) > 0 Or dtpResponseDate.Checked = True Or txtResponse.Text.Length > 0 Then
                    '    chkAssignResponse.Checked = True
                    'End If


                    'Pinkal (19-Dec-2020) -- Start
                    'Enhancement  -  Working on Discipline module for NMB.
                    cboContraryto.SelectedValue = CInt(xEditRow(0).Item("contrarytounkid"))
                    'Pinkal (19-Dec-2020) -- End


                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCMaster As New clsCommon_Master
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE, True, "List")
            With cboResponseType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "List")
            With cboOffenceCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP |18-DEC-2020| -- START
            'ISSUE/ENHANCEMENT : DISCIPLINE CHANGES
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_CONTRARY_TO, True, "List")
            With cboContraryto
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP |18-DEC-2020| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            'S.SANDEEP [24 OCT 2016] -- START
            'If dtpResponseDate.Checked = True Then
            '    If CInt(cboResponseType.SelectedValue) <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Response type is mandatory information. Please select Response type to continue."), enMsgBoxStyle.Information)
            '        txtResponse.Focus()
            '        Return False
            '    End If

            '    If txtResponse.Text.Trim.Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Response remark is mandatory information. Response remark cannot be blank."), enMsgBoxStyle.Information)
            '        txtResponse.Focus()
            '        Return False
            '    End If

            '    If dtpResponseDate.Value.Date < mdtDisciplineDate.Date Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Response date cannot be less than the charge date. Please set correct response date to continue."), enMsgBoxStyle.Information)
            '        dtpResponseDate.Focus()
            '        Return False
            '    End If
            'End If

            'If CInt(cboResponseType.SelectedValue) > 0 Then
            '    If dtpResponseDate.Checked = False Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Response date is mandatory information. Please provide Response Date."), enMsgBoxStyle.Information)
            '        dtpResponseDate.Focus()
            '        Return False
            '    End If

            '    If txtResponse.Text.Trim.Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Response remark is mandatory information. Response remark cannot be blank."), enMsgBoxStyle.Information)
            '        txtResponse.Focus()
            '        Return False
            '    End If
            'End If

            'If txtResponse.Text.Trim.Length > 0 Then
            '    If dtpResponseDate.Checked = False Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Response date is mandatory information. Please provide Response Date."), enMsgBoxStyle.Information)
            '        dtpResponseDate.Focus()
            '        Return False
            '    End If
            '    If CInt(cboResponseType.SelectedValue) <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Response type is mandatory information. Please select Response type to continue."), enMsgBoxStyle.Information)
            '        txtResponse.Focus()
            '        Return False
            '    End If
            'End If
            'S.SANDEEP [24 OCT 2016] -- END


            If CInt(cboOffenceCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Offence Category is compulsory information.Please Select Offence Category."), enMsgBoxStyle.Information)
                cboOffenceCategory.Select()
                Return False
            End If

            If CInt(cboDisciplineType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Offence is compulsory information.Please Select Offence."), enMsgBoxStyle.Information)
                cboDisciplineType.Select()
                Return False
            End If

            If txtIncident.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Incident is mandatory information. Incident cannot be blank."), enMsgBoxStyle.Information)
                txtIncident.Select()
                Return False
            End If


            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            Dim objGrp As New clsGroup_Master
            objGrp._Groupunkid = 1
            If objGrp._Groupname.ToUpper = "NMB PLC" AndAlso CInt(cboContraryto.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Contrary To is compulsory information.Please Select Contrary To."), enMsgBoxStyle.Information)
                cboContraryto.Select()
                objGrp = Nothing
                Return False
            End If
            objGrp = Nothing
            'Pinkal (19-Dec-2020) -- End


            If mdtCharges.Rows.Count > 0 Then
                If mdtCharges.AsEnumerable().Where(Function(x) x.Field(Of String)("incident_description") = txtIncident.Text And x.Field(Of String)("AUD") <> "D").Count > 0 Then
                    Dim xRow() As DataRow = mdtCharges.Select("incident_description = '" & txtIncident.Text.Replace("'", "''") & "' AND AUD <> 'D'")
                    Dim strGuid As String = ""
                    If xRow.Length > 0 Then
                        strGuid = CStr(xRow(0).Item("GUID"))
                    End If
                    If strGuid <> mstrGuidValue Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Incident is already added in the list."), enMsgBoxStyle.Information)
                        txtIncident.Select()
                        Return False
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Clear_Controls()
        Try
            txtIncident.Text = ""
            cboDisciplineType.SelectedValue = 0
            chkAssignResponse.Checked = False
            dtpResponseDate.Checked = False
            cboResponseType.SelectedValue = 0
            txtResponse.Text = ""

            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            cboContraryto.SelectedValue = 0
            'Pinkal (19-Dec-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Clear_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAddEditCounts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDiscChargeTran = New clsDiscipline_file_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            Call FillCombo()
            If chkAssignResponse.Checked = False Then
                pnlResponse.Enabled = False
            Else
                pnlResponse.Enabled = True
            End If
            If menAction = enAction.EDIT_ONE Then
                Call GetValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCounts_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAddEditCounts_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objDiscChargeTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnProcess.Click
        Try
            If Validation() = False Then Exit Sub

            If menAction = enAction.EDIT_ONE Then
                If xEditRow IsNot Nothing AndAlso xEditRow.Count > 0 Then

                    xEditRow(0).Item("disciplinefiletranunkid") = xEditRow(0).Item("disciplinefiletranunkid")
                    xEditRow(0).Item("disciplinefileunkid") = mintDisciplineFileUnkid
                    xEditRow(0).Item("incident_description") = txtIncident.Text
                    xEditRow(0).Item("offencecategoryunkid") = CInt(cboOffenceCategory.SelectedValue)
                    xEditRow(0).Item("offenceunkid") = CInt(cboDisciplineType.SelectedValue)
                    xEditRow(0).Item("isvoid") = False
                    xEditRow(0).Item("voiduserunkid") = -1
                    xEditRow(0).Item("voidreason") = ""
                    xEditRow(0).Item("voiddatetime") = DBNull.Value
                    xEditRow(0).Item("charge_category") = cboOffenceCategory.Text
                    xEditRow(0).Item("charge_descr") = cboDisciplineType.Text
                    xEditRow(0).Item("charge_severity") = mintSeverity

                    'Pinkal (19-Dec-2020) -- Start
                    'Enhancement  -  Working on Discipline module for NMB.
                    'xEditRow(0).Item("AUD") = "A"
                    xEditRow(0).Item("AUD") = "U"
                    'Pinkal (19-Dec-2020) -- End


                    xEditRow(0).Item("Guid") = mstrGuidValue

                    If mstrGuidValue.Trim.Length <= 0 Then mstrGuidValue = CStr(xEditRow(0).Item("Guid"))

                    xEditRow(0).Item("responsetypeunkid") = CInt(cboResponseType.SelectedValue)
                    If dtpResponseDate.Checked = True Then
                        xEditRow(0).Item("response_date") = dtpResponseDate.Value
                    End If
                    xEditRow(0).Item("response_remark") = txtResponse.Text
                    If CInt(cboResponseType.SelectedValue) > 0 Then
                        xEditRow(0).Item("response_type") = cboResponseType.Text
                    End If


                    'Pinkal (19-Dec-2020) -- Start
                    'Enhancement  -  Working on Discipline module for NMB.
                    If CInt(cboDisciplineType.SelectedValue) > 0 Then
                        xEditRow(0).Item("contrarytounkid") = CInt(cboContraryto.SelectedValue)
                        xEditRow(0).Item("contraryto") = cboContraryto.Text
                    End If
                    'Pinkal (19-Dec-2020) -- End


                    xEditRow(0).AcceptChanges()

                End If
            Else
                Dim dtRow As DataRow = mdtCharges.NewRow()

                dtRow.Item("disciplinefiletranunkid") = -1
                dtRow.Item("disciplinefileunkid") = mintDisciplineFileUnkid
                dtRow.Item("incident_description") = txtIncident.Text
                dtRow.Item("offencecategoryunkid") = CInt(cboOffenceCategory.SelectedValue)
                dtRow.Item("offenceunkid") = CInt(cboDisciplineType.SelectedValue)
                dtRow.Item("isvoid") = False
                dtRow.Item("voiduserunkid") = -1
                dtRow.Item("voidreason") = ""
                dtRow.Item("voiddatetime") = DBNull.Value
                dtRow.Item("charge_category") = cboOffenceCategory.Text
                dtRow.Item("charge_descr") = cboDisciplineType.Text
                dtRow.Item("charge_severity") = mintSeverity
                dtRow.Item("AUD") = "A"
                dtRow.Item("Guid") = Guid.NewGuid.ToString

                If mstrGuidValue.Trim.Length <= 0 Then mstrGuidValue = CStr(dtRow.Item("Guid"))

                dtRow.Item("responsetypeunkid") = CInt(cboResponseType.SelectedValue)
                If dtpResponseDate.Checked = True Then
                    dtRow.Item("response_date") = dtpResponseDate.Value
                End If
                dtRow.Item("response_remark") = txtResponse.Text
                If CInt(cboResponseType.SelectedValue) > 0 Then
                    dtRow.Item("response_type") = cboResponseType.Text
                End If


                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                If CInt(cboDisciplineType.SelectedValue) > 0 Then
                    dtRow.Item("contrarytounkid") = CInt(cboContraryto.SelectedValue)
                    dtRow.Item("contraryto") = cboContraryto.Text
                End If
                'Pinkal (19-Dec-2020) -- End


                mdtCharges.Rows.Add(dtRow)
            End If
            mdtCharges.AcceptChanges()

            If menAction = enAction.ADD_CONTINUE Then
                Call Clear_Controls()
            Else
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnProcess_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchOffence_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOffence.Click
        Dim frm As New frmCommonSearch
        Try
            If cboDisciplineType.DataSource Is Nothing Then Exit Sub
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboDisciplineType.ValueMember
                .DisplayMember = cboDisciplineType.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboDisciplineType.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboDisciplineType.SelectedValue = frm.SelectedValue
                cboDisciplineType.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOffence_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboOffenceCategory.ValueMember
                .DisplayMember = cboOffenceCategory.DisplayMember
                .DataSource = CType(cboOffenceCategory.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboOffenceCategory.SelectedValue = frm.SelectedValue
                cboOffenceCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddDisciplinetype_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDisciplinetype.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmDisciplineType_AddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objDisciplineType As New clsDisciplineType
                Dim dsList As New DataSet
                dsList = objDisciplineType.getComboList("DType", True)
                With cboDisciplineType
                    .ValueMember = "disciplinetypeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("DType")
                    .SelectedValue = intRefId
                End With
                objDisciplineType = Nothing
                dsList = Nothing
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddDisciplinetype_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddResponseType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResponseType.Click
        Dim frm As New frmCommonMaster
        Try
            Dim iRetVal As Integer = -1
            frm.displayDialog(iRetVal, clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE, enAction.ADD_ONE)
            If iRetVal > 0 Then
                Dim objCMaster As New clsCommon_Master
                Dim dList As New DataSet
                dList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE, True, "List")
                With cboResponseType
                    .DisplayMember = "name"
                    .ValueMember = "masterunkid"
                    .DataSource = dList.Tables("List")
                    .SelectedValue = iRetVal
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnAddResponseType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchResponseType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResponseType.Click
        Dim frm As New frmCommonSearch
        Try
            If cboResponseType.DataSource Is Nothing Then Exit Sub
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboResponseType.ValueMember
                .DisplayMember = cboResponseType.DisplayMember
                .DataSource = CType(cboResponseType.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboResponseType.SelectedValue = frm.SelectedValue
                cboResponseType.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResponseType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP |18-DEC-2020| -- START
    'ISSUE/ENHANCEMENT : DISCIPLINE CHANGES
    Private Sub objbtnSearchContraryto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchContraryto.Click
        Dim frm As New frmCommonSearch
        Try
            If cboContraryto.DataSource Is Nothing Then Exit Sub
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboContraryto.ValueMember
                .DisplayMember = cboContraryto.DisplayMember
                .DataSource = CType(cboContraryto.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboContraryto.SelectedValue = frm.SelectedValue
                cboContraryto.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResponseType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddContraryto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddContraryto.Click
        Dim frm As New frmCommonMaster
        Try
            Dim iRetVal As Integer = -1
            frm.displayDialog(iRetVal, clsCommon_Master.enCommonMaster.DISCIPLINE_CONTRARY_TO, enAction.ADD_ONE)
            If iRetVal > 0 Then
                Dim objCMaster As New clsCommon_Master
                Dim dList As New DataSet
                dList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_CONTRARY_TO, True, "List")
                With cboContraryto
                    .DisplayMember = "name"
                    .ValueMember = "masterunkid"
                    .DataSource = dList.Tables("List")
                    .SelectedValue = iRetVal
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnAddContraryto_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP |18-DEC-2020| -- END

    Private Sub btnScanAttachDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScanAttachDoc.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If mintEmpId <= 0 Then Exit Sub
            Dim dtAttachment As DataTable = mdtScanAttachDoc.Select("GUID = '" & mstrGuidValue & "'").CopyToDataTable
            frm._TransactionID = mintFileTranId
            frm._dtAttachment = dtAttachment.Copy
            frm._TransactionGuidString = mstrGuidValue
            'S.SANDEEP |26-APR-2019| -- START
            'If frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), _
            '                     enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, mintEmpId, enScanAttactRefId.DISCIPLINES, True, False) = True Then
            If frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), _
                                 enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, True, mintEmpId, enScanAttactRefId.DISCIPLINES, True, False) = True Then
                'S.SANDEEP |26-APR-2019| -- END
                dtAttachment = frm._dtAttachment.Copy
                mdtScanAttachDoc.Merge(dtAttachment)
                'mdtScanAttachDoc = frm._dtAttachment.Copy
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnScanAttachDoc_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboOffenceCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOffenceCategory.SelectedIndexChanged
        Try
            If CInt(cboOffenceCategory.SelectedValue) > 0 Then
                Dim dsFill As DataSet = Nothing
                Dim objDisciplineType As New clsDisciplineType
                dsFill = objDisciplineType.getComboList("DisciplineType", True, CInt(cboOffenceCategory.SelectedValue))
                cboDisciplineType.ValueMember = "disciplinetypeunkid"
                cboDisciplineType.DisplayMember = "name"
                cboDisciplineType.DataSource = dsFill.Tables("DisciplineType")
            Else
                cboDisciplineType.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOffenceCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboDisciplineType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDisciplineType.SelectedIndexChanged
        Try
            If CInt(cboDisciplineType.SelectedValue) > 0 Then
                Dim objDisicipline As New clsDisciplineType
                objDisicipline._Disciplinetypeunkid = CInt(cboDisciplineType.SelectedValue)
                mintSeverity = objDisicipline._Severity
            Else
                mintSeverity = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDisciplineType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox Event "

    Private Sub chkAssignResponse_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAssignResponse.CheckedChanged
        Try
            pnlResponse.Enabled = chkAssignResponse.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkAssignResponse_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbChargeCount.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbChargeCount.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.objbtnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblOffenceCategory.Text = Language._Object.getCaption(Me.lblOffenceCategory.Name, Me.lblOffenceCategory.Text)
            Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.Name, Me.lblDisciplineType.Text)
            Me.lblIncident.Text = Language._Object.getCaption(Me.lblIncident.Name, Me.lblIncident.Text)
            Me.gbChargeCount.Text = Language._Object.getCaption(Me.gbChargeCount.Name, Me.gbChargeCount.Text)
            Me.lblReponseDate.Text = Language._Object.getCaption(Me.lblReponseDate.Name, Me.lblReponseDate.Text)
            Me.lblResponse.Text = Language._Object.getCaption(Me.lblResponse.Name, Me.lblResponse.Text)
            Me.lblResponseType.Text = Language._Object.getCaption(Me.lblResponseType.Name, Me.lblResponseType.Text)
            Me.chkAssignResponse.Text = Language._Object.getCaption(Me.chkAssignResponse.Name, Me.chkAssignResponse.Text)
            'Hemant (30 Nov 2018) -- Start
            'Enhancement : Including Language Settings For Scan/Attachment Button
            btnScanAttachDoc.Text = Language._Object.getCaption(Me.btnScanAttachDoc.Name, Me.btnScanAttachDoc.Text)
            'Hemant (30 Nov 2018) -- End
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Response type is mandatory information. Please select Response type to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Response remark is mandatory information. Response remark cannot be blank.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Response date cannot be less than the charge date. Please set correct response date to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Response date is mandatory information. Please provide Response Date.")
            Language.setMessage(mstrModuleName, 5, "Offence Category is compulsory information.Please Select Offence Category.")
            Language.setMessage(mstrModuleName, 6, "Offence is compulsory information.Please Select Offence.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Incident is mandatory information. Incident cannot be blank.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Incident is already added in the list below.")
            Language.setMessage(mstrModuleName, 9, "Add")
            Language.setMessage(mstrModuleName, 10, "Edit")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    
    
End Class