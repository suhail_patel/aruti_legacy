﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmDisciplineHearingAddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmDisciplineHearingAddEdit"
    Private objCommittee As New clsDiscipline_Committee
    Private objHearingScheduleMaster As clshearing_schedule_master
    Private objHearingScheduleTran As clshearing_schedule_Tran
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtMembers As DataTable
    Private mdtEmployee As DataTable
    Private mdtReference As DataTable
    Private mdtCharges As DataTable
    Private mdtTran As DataTable
    Private mdtView As DataView
    Private mstrFilter As String = String.Empty
    Private minthearingschedulemasterunkid As Integer = -1
    ''' <summary>
    ''' This will be used from Discipline File List
    ''' </summary>
    ''' <remarks></remarks>
    Private mintEmployeeId As Integer = 0
    Private mintDisciplineFileId As Integer = 0
    Private mblnFromDisciplineList As Boolean = False
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private mdtScanAttachment As DataTable
    'S.SANDEEP |11-NOV-2019| -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal inthearingschedulemasterunkid As Integer) As Boolean
        Try
            menAction = eAction
            minthearingschedulemasterunkid = inthearingschedulemasterunkid
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' This will be used from Discipline File List
    ''' </summary>
    ''' <param name="intEmployeeId"></param>
    ''' <param name="intDisciplineId"></param>
    ''' <param name="eAction"></param>
    ''' <remarks></remarks>
    Public Function displayDialog(ByVal intEmployeeId As Integer, ByVal intDisciplineId As Integer, ByVal eAction As enAction, ByVal blnFromDisciplineList As Boolean) As Boolean
        Try
            menAction = eAction
            mintEmployeeId = intEmployeeId
            mintDisciplineFileId = intDisciplineId
            mblnFromDisciplineList = blnFromDisciplineList
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Evenst "

    Private Sub frmDisciplineHearingAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmDisciplineHearingAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineHearingAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineHearingAddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshearing_schedule_master.SetMessages()
            objfrm._Other_ModuleNames = "clshearing_schedule_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineHearingAddEdit_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineHearingAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objHearingScheduleMaster = New clshearing_schedule_master
        objHearingScheduleTran = New clshearing_schedule_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboReferenceNo.Enabled = False
                objbtnSearchRefNo.Enabled = False
                cboCommittee.Enabled = False
                objbtnSearchCommitte.Enabled = False
                objHearingScheduleMaster._Hearingschedulemasterunkid = minthearingschedulemasterunkid
            End If
            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            'objHearingScheduleTran._Hearingschedulemasterunkid = minthearingschedulemasterunkid
            objHearingScheduleTran._Hearingschedulemasterunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())) = minthearingschedulemasterunkid
            'Pinkal (19-Dec-2020) -- End

            mdtTran = objHearingScheduleTran._HeatingScheduleTranTable

            Call GetValue()

            If mblnFromDisciplineList Then
                cboEmployee.SelectedValue = mintEmployeeId
                cboReferenceNo.SelectedValue = mintDisciplineFileId
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                cboReferenceNo.Enabled = False : objbtnSearchRefNo.Enabled = False
            End If

            'Pinkal (22-Jan-2021) -- Start
            'Enhancement NMB - Working Discipline changes required by NMB.
            'lnkAddMember.Visible = False
            'Pinkal (22-Jan-2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineHearingAddEdit_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objCommonMaster As New clsCommon_Master
            Dim objDscplnFileMaster As New clsDiscipline_file_master

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
            With cboCommittee
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objDscplnFileMaster.GetComboList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , , , , )

            mdtEmployee = dsList.Tables("List").DefaultView.ToTable(True, "involved_employeeunkid", "employeecode", "employeename")

            RemoveHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged
            With cboEmployee
                .ValueMember = "involved_employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = mdtEmployee
                .SelectedValue = 0
            End With
            AddHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged

            mdtReference = dsList.Tables("List")

            RemoveHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged
            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = mdtReference
                .SelectedValue = 0
            End With
            AddHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillMembersList()
        Try
            Dim mView As DataView = mdtMembers.DefaultView
            mView.RowFilter = "AUD <> 'D' "
            
            dgvData.AutoGenerateColumns = False
            objcolhCheck.DataPropertyName = "ischecked"
            dgcolhCompany.DataPropertyName = "company"
            dgcolhDepartment.DataPropertyName = "department"
            'Pinkal (22-Jan-2021) -- Start
            'Enhancement NMB - Working Discipline changes required by NMB.
            dgcolhMembers.DataPropertyName = "members"
            dgcolhContactNo.DataPropertyName = "contactno"
            dgcolhEmail.DataPropertyName = "email"

            'dgcolhContactNo.DataPropertyName = "contact"
            'dgcolhMembers.DataPropertyName = "employee_name"
            'dgcolhEmail.DataPropertyName = "emailid"
            'Pinkal (22-Jan-2021) -- End

            objdgcolhcommitteetranunkid.DataPropertyName = "committeetranunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            objdgcolhhearningscheduletranunkid.DataPropertyName = "hearningscheduletranunkid"
            dgvData.DataSource = mView

            'lvMembers.Items.Clear()
            'Dim lvItem As ListViewItem
            'RemoveHandler lvMembers.ItemChecked, AddressOf lvMembers_ItemChecked
            'For Each dtRow As DataRow In mdtMembers.Rows
            '    If CStr(dtRow("AUD")) <> "D" Then
            '        lvItem = New ListViewItem
            '        If CInt(dtRow.Item("employeeunkid")) <> -1 Then
            '            lvItem.SubItems.Add(dtRow.Item("employee_name").ToString)
            '        Else
            '            lvItem.SubItems.Add(dtRow.Item("ex_name").ToString)
            '        End If
            '        If CInt(dtRow.Item("employeeunkid")) <> -1 Then
            '            lvItem.SubItems.Add(dtRow.Item("emailid").ToString)
            '        Else
            '            lvItem.SubItems.Add(dtRow.Item("ex_email").ToString)
            '        End If
            '        lvItem.Tag = dtRow.Item("committeetranunkid").ToString
            '        lvItem.Checked = CBool(dtRow.Item("ischecked"))
            '        lvMembers.Items.Add(lvItem)
            '    End If
            'Next
            'AddHandler lvMembers.ItemChecked, AddressOf lvMembers_ItemChecked

            'RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            'If lvMembers.Items.Count = lvMembers.CheckedItems.Count Then
            '    objchkAll.Checked = True
            'ElseIf lvMembers.CheckedItems.Count = 0 Then
            '    objchkAll.Checked = False
            'Else
            '    objchkAll.CheckState = CheckState.Indeterminate
            'End If
            'AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

            'If lvMembers.Items.Count <= 0 Then
            '    objchkAll.Visible = False
            'Else
            '    objchkAll.Visible = True
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMembersList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillChargeCountList()
        Try
            Dim intChargesCount As Integer = 1
            Dim lvItem As ListViewItem
            lvChargesCount.Items.Clear()

            For Each dtRow As DataRow In mdtCharges.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.Text = intChargesCount.ToString
                    lvItem.SubItems.Add(dtRow.Item("incident_description").ToString)
                    lvItem.SubItems.Add(dtRow.Item("charge_category").ToString)
                    lvItem.SubItems.Add(dtRow.Item("charge_descr").ToString)
                    lvItem.SubItems.Add(dtRow.Item("charge_severity").ToString)
                    lvItem.SubItems.Add(dtRow.Item("offencecategoryunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("offenceunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvItem.SubItems.Add(dtRow.Item("disciplinefileunkid").ToString)
                    lvItem.Tag = dtRow.Item("disciplinefiletranunkid").ToString

                    intChargesCount += 1

                    lvChargesCount.Items.Add(lvItem)
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillChargeCountList", mstrModuleName)
        End Try
    End Sub

    Private Sub FilterReferenceNo()
        Try
            Dim strCsvData As String = ""
            strCsvData = String.Join(",", mdtReference.AsEnumerable().Where(Function(x) x.Field(Of Integer)("involved_employeeunkid") = CInt(cboEmployee.SelectedValue)).Select(Function(y) y.Field(Of Integer)("disciplinefileunkid").ToString).ToArray())

            mdtView = New DataView(mdtReference, "disciplinefileunkid IN (" & strCsvData & ") ", "", DataViewRowState.CurrentRows)

            RemoveHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged
            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = mdtView.ToTable
                If mdtView.ToTable.Rows.Count = 1 Then
                    .SelectedValue = mdtView.ToTable.Rows(0).Item("disciplinefileunkid")
                    Call GetValueByReferenceNo()
                    'S.SANDEEP [25 JUL 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    Call FillChargeCountList()
                    'S.SANDEEP [25 JUL 2016] -- START
                Else
                    .SelectedValue = 0
                End If
            End With
            AddHandler cboReferenceNo.SelectedIndexChanged, AddressOf cboReferenceNo_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterReferenceNo", mstrModuleName)
        End Try
    End Sub

    Private Sub FilterEmployee()
        Try
            mstrFilter = "disciplinefileunkid = '" & CInt(cboReferenceNo.SelectedValue) & "' " & " "
            mdtView = New DataView(mdtReference, mstrFilter, "", DataViewRowState.CurrentRows)

            cboEmployee.SelectedValue = CInt(mdtView.ToTable.Rows(0).Item("involved_employeeunkid").ToString)

            'With cboEmployee
            '    .DisplayMember = "employeename"
            '    .ValueMember = "involved_employeeunkid"
            '    .DataSource = mdtView.ToTable
            '    If mdtView.Table.Rows.Count = 1 Then
            '        .SelectedValue = mdtView.Table.Rows(0).Item("involved_employeeunkid")
            '    Else
            '        .SelectedValue = 0
            '    End If
            'End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValueByPersonInvolved()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim objJob As New clsJobs
            Dim objDepartment As New clsDepartment

            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

            objJob._Jobunkid = objEmployee._Jobunkid
            objDepartment._Departmentunkid = objEmployee._Departmentunkid

            txtJobTitle.Text = objJob._Job_Name
            txtDepartment.Text = objDepartment._Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValuePersonInvolved", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValueByReferenceNo()
        Try
            Dim objDisciplineFileMaster As New clsDiscipline_file_master
            Dim objDisciplineFileTran As New clsDiscipline_file_tran

            objDisciplineFileMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)

            txtChargeDate.Text = CStr(CDate(objDisciplineFileMaster._Chargedate).Date)
            If objDisciplineFileMaster._Interdictiondate <> Nothing Then
                txtInterdictionDate.Text = CStr(CDate(objDisciplineFileMaster._Interdictiondate).Date)
            Else
                txtInterdictionDate.Text = ""
            End If
            txtChargeDescription.Text = objDisciplineFileMaster._Charge_Description

            '=====================  Fill Charges Counts ====================================
            objDisciplineFileTran._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
            mdtCharges = objDisciplineFileTran._ChargesTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValueByReferenceNo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If txtVenue.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please specify Venue. Vanue cannot be blank."), enMsgBoxStyle.Information)
                txtVenue.Focus()
                Return False

                'ElseIf txtRemark.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please specify Remark. Remark cannot be blank."), enMsgBoxStyle.Information)
                '    txtRemark.Focus()
                '    Return False

            ElseIf CInt(cboCommittee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Committee. Committee is mandatory information."), enMsgBoxStyle.Information)
                cboCommittee.Focus()
                Return False
            End If

            If dgvData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no member defined for selected committee."), enMsgBoxStyle.Information)
                dgvData.Focus()
                Return False
            End If

            'If lvMembers.Items.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no member defined for selected committee."), enMsgBoxStyle.Information)
            '    lvMembers.Focus()
            '    Return False

            'ElseIf lvMembers.CheckedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one member from list."), enMsgBoxStyle.Information)
            '    lvMembers.Focus()
            '    Return False

            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub GetValue()
        Try
            If menAction = enAction.EDIT_ONE Then
                Dim objDisciplineFileMaster As New clsDiscipline_file_master
                Dim objHearingScheduleTran As New clshearing_schedule_Tran

                objDisciplineFileMaster._Disciplinefileunkid = objHearingScheduleMaster._Disciplinefileunkid
                cboEmployee.SelectedValue = objDisciplineFileMaster._Involved_Employeeunkid
                cboReferenceNo.SelectedValue = objDisciplineFileMaster._Disciplinefileunkid
                dtpHearingDate.Value = objHearingScheduleMaster._HearingDate
                dtpHearingTime.Value = objHearingScheduleMaster._HearingDate
                txtVenue.Text = objHearingScheduleMaster._HearningVenue.ToString
                txtRemark.Text = objHearingScheduleMaster._Remark.ToString

                Dim intCommitteeMasterId As Integer = objHearingScheduleTran.getCommitteeMasterId(minthearingschedulemasterunkid)
                cboCommittee.SelectedValue = intCommitteeMasterId

                'Dim xRow = mdtTran.AsEnumerable().Where(Function(p) p.Field(Of Integer)("hearingschedulemasterunkid") = minthearingschedulemasterunkid).Select(Function(x) x.Field(Of Integer)("committeetranunkid")).ToArray
                'If xRow.Count > 0 Then
                '    Dim mRow = mdtMembers.AsEnumerable().Where(Function(x) xRow.Contains(x.Field(Of Integer)("committeetranunkid")))
                '    For Each xr In mRow
                '        xr.Item("ischecked") = True
                '    Next
                '    mdtMembers.AcceptChanges()
                '    Call FillMembersList()
                'End If
                    Call FillMembersList()
                End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objHearingScheduleMaster._HearingDate = dtpHearingDate.Value
            objHearingScheduleMaster._HearningTime = dtpHearingTime.Value
            objHearingScheduleMaster._HearningVenue = txtVenue.Text
            objHearingScheduleMaster._Remark = txtRemark.Text
            objHearingScheduleMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
            objHearingScheduleMaster._Userunkid = User._Object._Userunkid
            objHearingScheduleMaster._Isvoid = False
            objHearingScheduleMaster._Voiddatetime = Nothing
            objHearingScheduleMaster._Voiduserunkid = -1
            objHearingScheduleMaster._Voidreason = ""
            'S.SANDEEP [24 MAY 2016] -- Start
            'Email Notification
            objHearingScheduleMaster._LoginTypeId = enLogin_Mode.DESKTOP
            'S.SANDEEP [24 MAY 2016] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetParameters()
        Try
            cboEmployee.SelectedValue = 0
            cboReferenceNo.SelectedValue = 0
            cboCommittee.SelectedValue = 0
            txtJobTitle.Text = ""
            txtDepartment.Text = ""
            txtChargeDate.Text = ""
            txtInterdictionDate.Text = ""
            txtChargeDescription.Text = ""
            txtVenue.Text = ""
            txtRemark.Text = ""
            dtpHearingDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpHearingTime.Value = ConfigParameter._Object._CurrentDateAndTime
            objchkAll.Checked = False
            lvChargesCount.Items.Clear()
            'lvMembers.Items.Clear()

            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            'objHearingScheduleTran._objHearingScheduleTran. = -1
            objHearingScheduleTran._Hearingschedulemasterunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())) = -1
            'Pinkal (19-Dec-2020) -- End
            mdtTran = objHearingScheduleTran._HeatingScheduleTranTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetParameters", mstrModuleName)
        End Try
    End Sub

    Private Sub AddRemoveMember(ByVal intCommitteMemberTranId As Integer, ByVal strAUDValue As String, ByVal strEmail As String, ByVal strMemberName As String)
        Try
            If mdtTran IsNot Nothing Then
                Dim dr As DataRow() = mdtTran.Select("committeetranunkid = '" & intCommitteMemberTranId & "'")
                If dr.Length > 0 Then   'EITD MODE

                    dr(0).Item("hearningscheduletranunkid") = dr(0).Item("hearningscheduletranunkid")
                    dr(0).Item("hearingschedulemasterunkid") = minthearingschedulemasterunkid
                    dr(0).Item("committeetranunkid") = intCommitteMemberTranId
                    dr(0).Item("isnotified") = dr(0).Item("isnotified")
                    dr(0).Item("notify_date") = dr(0).Item("notify_date")

                    If CInt(dr(0).Item("hearningscheduletranunkid")) > 0 AndAlso strAUDValue = "D" Then
                        Dim mstrVoidReason As String = String.Empty
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        frm.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
                        frm = Nothing
                        If menAction = enAction.EDIT_ONE Then
                            If mstrVoidReason.Trim.Length <= 0 Then Exit Sub
                        End If
                        dr(0).Item("isvoid") = True
                        dr(0).Item("voiduserunkid") = User._Object._Userunkid
                        dr(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dr(0).Item("voidreason") = mstrVoidReason
                    Else
                        dr(0).Item("isvoid") = False
                        dr(0).Item("voiduserunkid") = -1
                        dr(0).Item("voiddatetime") = DBNull.Value
                        dr(0).Item("voidreason") = ""
                    End If

                    dr(0).Item("committeemasterunkid") = CInt(cboCommittee.SelectedValue)
                    dr(0).Item("employeeunkid") = dr(0).Item("employeeunkid")
                    dr(0).Item("AUD") = strAUDValue
                    dr(0).Item("GUID") = Guid.NewGuid.ToString()
                    'S.SANDEEP [24 MAY 2016] -- Start
                    'Email Notification
                    dr(0).Item("email") = dr(0).Item("email")
                    dr(0).Item("member_name") = dr(0).Item("member_name")
                    'S.SANDEEP [24 MAY 2016] -- End

                Else    'ADD MODE
                    Dim drow As DataRow = mdtTran.NewRow
                    drow.Item("hearningscheduletranunkid") = -1
                    drow.Item("hearingschedulemasterunkid") = minthearingschedulemasterunkid
                    drow.Item("committeetranunkid") = intCommitteMemberTranId
                    'S.SANDEEP |13-MAY-2021| -- START
                    'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
                    'drow.Item("isnotified") = True
                    'drow.Item("notify_date") = ConfigParameter._Object._CurrentDateAndTime
                    drow.Item("isnotified") = False
                    drow.Item("notify_date") = Nothing
                    'S.SANDEEP |13-MAY-2021| -- END
                    drow.Item("isvoid") = False
                    drow.Item("voiduserunkid") = -1
                    drow.Item("voiddatetime") = DBNull.Value
                    drow.Item("voidreason") = ""
                    drow.Item("committeemasterunkid") = CInt(cboCommittee.SelectedValue)
                    drow.Item("employeeunkid") = 0
                    drow.Item("AUD") = strAUDValue
                    drow.Item("GUID") = Guid.NewGuid.ToString()
                    'S.SANDEEP [24 MAY 2016] -- Start
                    'Email Notification
                    drow.Item("email") = strEmail
                    drow.Item("member_name") = strMemberName
                    'S.SANDEEP [24 MAY 2016] -- End

                    mdtTran.Rows.Add(drow)
                End If
                mdtTran.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddRemoveMember", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'If menAction = enAction.EDIT_ONE Then
            '    'Sohail (30 Nov 2017) -- Start
            '    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '    'blnFlag = objHearingScheduleMaster.Update(ConfigParameter._Object._CurrentDateAndTime, mdtTran)
            '    blnFlag = objHearingScheduleMaster.Update(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid)
            '    'Sohail (30 Nov 2017) -- End
            'Else
            '    'Sohail (30 Nov 2017) -- Start
            '    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '    'blnFlag = objHearingScheduleMaster.Insert(ConfigParameter._Object._CurrentDateAndTime, mdtTran)
            '    blnFlag = objHearingScheduleMaster.Insert(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid)
            '    'Sohail (30 Nov 2017) -- End
            'End If


            If mdtScanAttachment IsNot Nothing Then 'S.SANDEEP |16-JAN-2020| -- START { ISSUE/ENHANCEMENT : OBJECT REFERENCE ERROR (mdtScanAttachment) } -- END
            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist() : Dim strError As String = ""
            Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.DISCIPLINES) Select (p.Item("Name").ToString)).FirstOrDefault

            For Each dRow As DataRow In mdtScanAttachment.Rows
                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                    Dim intScanAttachRefId As Integer = CInt(dRow("scanattachrefid"))

                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                            dRow("fileuniquename") = strFileName
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFileName As String = dRow("fileuniquename").ToString
                    Dim intScanattachrefid As Integer = CInt(dRow("scanattachrefid"))

                    If blnIsIISInstalled Then
                        Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then strPath += "/"
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If clsFileUploadDownload.UpdateFile(CStr(dRow("filepath")), strPath, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Move(CStr(dRow("filepath")), strDocLocalPath)

                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFileName As String = dRow("fileuniquename").ToString
                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If File.Exists(strDocLocalPath) Then
                                File.Delete(strDocLocalPath)
                            End If
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
            Next
            End If

            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objHearingScheduleMaster.Update(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid, mdtScanAttachment, mstrModuleName)
            'Else
            '    blnFlag = objHearingScheduleMaster.Insert(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid, mdtScanAttachment, mstrModuleName)
            'End If

            mdtTran = mdtMembers
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objHearingScheduleMaster.Update(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid, mdtScanAttachment, mstrModuleName, ConfigParameter._Object._EmployeeAsOnDate)
            Else
                blnFlag = objHearingScheduleMaster.Insert(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid, mdtScanAttachment, mstrModuleName, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'Pinkal (19-Dec-2020) -- End
            
            'S.SANDEEP |11-NOV-2019| -- END


            If blnFlag = True Then
                Dim objDisciplineFile As New clsDiscipline_file_master
                Dim lstName As New List(Of String)(New String() {frmDisciplineFiling.Name.ToString, frmChargeCountResponse.Name.ToString})
                objDisciplineFile.SendMailToEmployee(Nothing, mintDisciplineFileId, Company._Object._Companyunkid, Nothing, _
                                                     ConfigParameter._Object._Document_Path, lstName, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime)
                objDisciplineFile = Nothing
            End If

            If blnFlag = False AndAlso objHearingScheduleMaster._Message <> "" Then
                eZeeMsgBox.Show(objHearingScheduleMaster._Message, enMsgBoxStyle.Information)
                Exit Sub
            ElseIf blnFlag = True Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    Call ResetParameters()
                Else
                    Me.Close()
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCommitte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCommitte.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboCommittee.DisplayMember
                .ValueMember = cboCommittee.ValueMember
                .DataSource = CType(cboCommittee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboCommittee.SelectedValue = frm.SelectedValue
                cboCommittee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCommitte_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .CodeMember = "employeecode"
                .DataSource = mdtEmployee
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call FilterReferenceNo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboReferenceNo.DisplayMember
                .ValueMember = cboReferenceNo.ValueMember
                .DataSource = CType(cboReferenceNo.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboReferenceNo.SelectedValue = frm.SelectedValue
                cboReferenceNo.Focus()
            End If

            If CInt(cboReferenceNo.SelectedValue) > 0 Then
                Call FilterEmployee()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRefNo_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "
    'Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
    '    Try
    '        RemoveHandler lvMembers.ItemChecked, AddressOf lvMembers_ItemChecked
    '        For Each lvItem As ListViewItem In lvMembers.Items
    '            lvItem.Checked = objchkAll.Checked
    '            If objchkAll.CheckState = CheckState.Checked Then
    '                Call AddRemoveMember(CInt(lvItem.Tag), "A", lvItem.SubItems(objcolhEmail.Index).Text.ToString, lvItem.SubItems(colhName.Index).Text.ToString)
    '            ElseIf objchkAll.CheckState = CheckState.Unchecked Then
    '                Call AddRemoveMember(CInt(lvItem.Tag), "D", lvItem.SubItems(objcolhEmail.Index).Text.ToString, lvItem.SubItems(colhName.Index).Text.ToString)
    '            End If
    '        Next
    '        AddHandler lvMembers.ItemChecked, AddressOf lvMembers_ItemChecked
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " ComboBox's Events "

    Private Sub cboCommittee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCommittee.SelectedIndexChanged
        Try

            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            'Dim objGrp As New clsGroup_Master
            'objGrp._Groupunkid = 1
            'If objGrp._Groupname.ToUpper = "NMB PLC" Then
            objHearingScheduleTran._Hearingschedulemasterunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = minthearingschedulemasterunkid
            mdtMembers = objHearingScheduleTran._HeatingScheduleTranTable
            If CBool(mdtMembers.Columns.Contains("ischecked")) = False Then
                Dim dCol As New DataColumn
                dCol.ColumnName = "ischecked"
                dCol.DefaultValue = False
                dCol.DataType = Type.GetType("System.Boolean")
                mdtMembers.Columns.Add(dCol)
                mdtMembers.AcceptChanges()
            End If

            '    Exit Sub
            'End If
            'objGrp = Nothing
            'Pinkal (19-Dec-2020) -- End

            If CInt(cboCommittee.SelectedValue) > 0 Then

                'Pinkal (22-Jan-2021) -- Start
                'Enhancement NMB - Working Discipline changes required by NMB.
                'objCommittee._CommitteeMasterId(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboCommittee.SelectedValue)
                objCommittee._CommitteeMasterId(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._DatabaseName, ConfigParameter._Object._IsIncludeInactiveEmp) = CInt(cboCommittee.SelectedValue)

                'mdtMembers = objCommittee._DataTable

                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                Dim dTable As DataTable = Nothing
                Dim dRow As DataRow = Nothing
                ''Pinkal (19-Dec-2020) -- End

                'If CBool(mdtMembers.Columns.Contains("ischecked")) = False Then
                '    Dim dCol As New DataColumn
                '    dCol.ColumnName = "ischecked"
                '    dCol.DefaultValue = False
                '    dCol.DataType = Type.GetType("System.Boolean")
                '    mdtMembers.Columns.Add(dCol)
                '    mdtMembers.AcceptChanges()
                'End If


                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                'If objCommittee._DataTable.Rows.Count > 0 AndAlso CStr(objCommittee._DataTable.Rows(0)("Ids")).Trim.Length > 0 Then
                'dTable = New DataView(objCommittee._DataTable, "committeetranunkid NOT IN(" & CStr(objCommittee._DataTable.Rows(0)("Ids")) & ")", "", DataViewRowState.CurrentRows).ToTable
                'Else
                    dTable = New DataView(objCommittee._DataTable, "", "", DataViewRowState.CurrentRows).ToTable
                'End If
            
                For Each dtRow As DataRow In dTable.Rows
                    Dim intTranId As Integer = CInt(dtRow("committeetranunkid"))
                    If mdtMembers.AsEnumerable().Where(Function(x) x.Field(Of Integer)("committeetranunkid") = intTranId).Count <= 0 Then
                        dRow = mdtMembers.NewRow
                        dRow("hearningscheduletranunkid") = -1
                        dRow("hearingschedulemasterunkid") = minthearingschedulemasterunkid
                        dRow("committeetranunkid") = dtRow("committeetranunkid")
                        dRow("committeemasterunkid") = CInt(cboCommittee.SelectedValue)
                        dRow("committee") = cboCommittee.Text
                        dRow("isvoid") = False
                        dRow("voiduserunkid") = -1
                        dRow("voiddatetime") = DBNull.Value
                        dRow("voidreason") = ""
                        dRow("AUD") = "A"
                        dRow("GUID") = Guid.NewGuid.ToString

                        If CInt(dtRow.Item("employeeunkid")) <> -1 Then
                            dRow("members") = dtRow.Item("employee_name")
                            dRow("department") = dtRow.Item("department")
                            dRow("company") = Company._Object._Name
                            dRow("contactno") = dtRow.Item("contact")
                            dRow("email") = dtRow.Item("emailid")
                            dRow("employeeunkid") = dtRow.Item("employeeunkid")
                        Else
                            dRow("members") = dtRow.Item("ex_name")
                            dRow("department") = dtRow.Item("ex_department")
                            dRow("company") = dtRow.Item("ex_company")
                            dRow("contactno") = dtRow.Item("ex_contactno")
                            dRow("email") = dtRow.Item("ex_email")

                            If intTranId <= 0 Then
                                dRow("ex_name") = dtRow.Item("ex_name")
                                dRow("ex_company") = dtRow.Item("ex_company")
                                dRow("ex_department") = dtRow.Item("ex_department")
                                dRow("ex_contactno") = dtRow.Item("ex_contactno")
                                dRow("ex_email") = dtRow.Item("ex_email")
                            End If

                        End If

                        mdtMembers.Rows.Add(dRow)

                    End If
                Next


                Call FillMembersList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCommittee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then

                Dim frm As New frmCommonSearch

                With frm
                    .DisplayMember = cboEmployee.DisplayMember
                    .ValueMember = cboEmployee.ValueMember
                    .CodeMember = "employeecode"
                    .DataSource = mdtEmployee
                End With

                Dim ch As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = ch.ToString

                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Call FilterReferenceNo()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReferenceNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboReferenceNo.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then

                Dim frm As New frmCommonSearch

                With frm
                    .DisplayMember = cboReferenceNo.DisplayMember
                    .ValueMember = cboReferenceNo.ValueMember
                    .DataSource = CType(cboReferenceNo.DataSource, DataTable)
                End With

                Dim ch As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = ch.ToString

                If frm.DisplayDialog Then
                    cboReferenceNo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboReferenceNo.Text = ""
                End If
                If CInt(cboReferenceNo.SelectedValue) > 0 Then
                    Call FilterEmployee()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReferenceNo_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                lvChargesCount.Items.Clear()
                txtChargeDate.Text = ""
                txtInterdictionDate.Text = ""
                txtChargeDescription.Text = ""
                Call GetValueByPersonInvolved()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReferenceNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReferenceNo.SelectedIndexChanged
        Try
            If CInt(cboReferenceNo.SelectedValue) > 0 Then
                Call GetValueByReferenceNo()
                Call FillChargeCountList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReferenceNo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
#Region " Link Event(s) "

    Private Sub lnkAddAttachment_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddAttachment.LinkClicked
        Dim frm As New frmScanOrAttachmentInfo
        Try
            Dim objScanAttachment As New clsScan_Attach_Documents
            objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "", CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            Dim xRow = objScanAttachment._Datatable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.DISCIPLINES _
                                                                         And x.Field(Of Integer)("transactionunkid") = minthearingschedulemasterunkid _
                                                                         And x.Field(Of String)("form_name") = mstrModuleName)

            If xRow.Count > 0 Then
                mdtScanAttachment = xRow.CopyToDataTable()
            Else
                mdtScanAttachment = objScanAttachment._Datatable.Clone()
            End If
            objScanAttachment = Nothing

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If



            frm._dtAttachment = mdtScanAttachment
            frm._TransactionID = CInt(IIf(minthearingschedulemasterunkid <= 0, CInt(cboReferenceNo.SelectedValue), minthearingschedulemasterunkid))
            Dim strScreen As String = String.Empty
            If minthearingschedulemasterunkid > 0 Then
                strScreen = mstrModuleName
            Else
                strScreen = ""
            End If
            frm.displayDialog(Language.getMessage("frmDisciplineProceedingList", 2, "Select Employee"), enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", strScreen, True, CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, True, False)
            mdtScanAttachment = frm._dtAttachment
            'S.SANDEEP |13-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
            For Each iR As DataRow In mdtScanAttachment.Rows
                iR("form_name") = mstrModuleName
            Next
            mdtScanAttachment.AcceptChanges()
            'S.SANDEEP |13-MAY-2021| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddAttachment_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (19-Dec-2020) -- Start
    'Enhancement  -  Working on Discipline module for NMB.
    Private Sub lnkAddMember_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddMember.LinkClicked
        Dim frm As New frmAddHearingMembers
        Try
            If CInt(cboCommittee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Committee. Committee is mandatory information."), enMsgBoxStyle.Information)
                cboCommittee.Focus()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(enAction.ADD_ONE, -1, minthearingschedulemasterunkid, -1, "", mdtMembers, CInt(cboCommittee.SelectedValue), CInt(cboEmployee.SelectedValue)) Then
                Call FillMembersList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddMember_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (19-Dec-2020) -- End


#End Region
    'S.SANDEEP |11-NOV-2019| -- END

#Region " ListView's Events "

    'Private Sub lvMembers_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        'Dim xRow = mdtMembers.AsEnumerable().Where(Function(p) p.Field(Of Integer)("committeetranunkid") = CInt(e.Item.Tag))
    '        'If xRow.Count > 0 Then
    '        'xRow(0).Item("ischecked") = e.Item.Checked
    '        If e.Item.Checked = True Then
    '            'xRow(0).Item("AUD") = "A"
    '            Call AddRemoveMember(CInt(e.Item.Tag), "A", e.Item.SubItems(objcolhEmail.Index).Text.ToString, e.Item.SubItems(colhName.Index).Text.ToString)
    '        ElseIf e.Item.Checked = False Then
    '            'xRow(0).Item("AUD") = "D"
    '            Call AddRemoveMember(CInt(e.Item.Tag), "D", e.Item.SubItems(objcolhEmail.Index).Text.ToString, e.Item.SubItems(colhName.Index).Text.ToString)
    '        End If
    '        'mdtMembers.AcceptChanges()
    '        'End If

    '        RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
    '        If lvMembers.Items.Count = lvMembers.CheckedItems.Count Then
    '            objchkAll.Checked = True
    '        ElseIf lvMembers.CheckedItems.Count = 0 Then
    '            objchkAll.Checked = False
    '        Else
    '            objchkAll.CheckState = CheckState.Indeterminate
    '        End If
    '        AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvMembers_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub

#End Region

    'Pinkal (19-Dec-2020) -- Start
    'Enhancement  -  Working on Discipline module for NMB.
#Region " Grid Event(s) "

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CInt(dgvRow.Cells(objdgcolhhearningscheduletranunkid.Index).Value) > 0 Then
                    dgvRow.Cells(objdgcolhEdit.Index).Value = imgBlank
                    dgvRow.Cells(objdgcolhDelete.Index).Value = My.Resources.remove
                Else
                    dgvRow.Cells(objdgcolhEdit.Index).Value = My.Resources.edit
                    dgvRow.Cells(objdgcolhDelete.Index).Value = My.Resources.remove
                End If
            Next
            dgvData.Refresh() : dgvData.PerformLayout()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvData.Scroll
        Try
            If e.OldValue <> e.NewValue Then
                dgvData.PerformLayout()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_Scroll", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellClick
        Dim frm As New frmAddHearingMembers
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Select Case e.ColumnIndex

                Case objdgcolhEdit.Index
                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub

                    If frm.displayDialog(enAction.EDIT_ONE, _
                                         CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhhearningscheduletranunkid.Index).Value), _
                                         minthearingschedulemasterunkid, _
                                         CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhcommitteetranunkid.Index).Value), _
                                         CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value), _
                                         mdtMembers, CInt(cboCommittee.SelectedValue), _
                                         CInt(cboEmployee.SelectedValue)) Then

                        'Pinkal (19-Dec-2020) -- End

                        Call FillMembersList()
                    End If

                Case objdgcolhDelete.Index

                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Are you sure you want to delete this member?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim frm1 As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm1.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm1.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
            End If
                        Dim mstrVoidReason As String = String.Empty
                        If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhhearningscheduletranunkid.Index).Value) > 0 Then
                            Dim dRow() As DataRow = mdtMembers.Select("hearningscheduletranunkid = " & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhhearningscheduletranunkid.Index).Value) & " AND AUD <> 'D'")
                            If dRow.Count > 0 Then
                                frm1.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
                                If mstrVoidReason.Length <= 0 Then Exit Sub

                                dRow(0).Item("isvoid") = True
                                dRow(0).Item("voiduserunkid") = User._Object._Userunkid
                                dRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                dRow(0).Item("voidreason") = mstrVoidReason
                                dRow(0).Item("AUD") = "D"
                            End If
            Else
                            Dim dRow() As DataRow = mdtMembers.Select("GUID = '" & CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value) & "' AND AUD <> 'D'")
                            If dRow.Count > 0 Then
                                dRow(0).Item("isvoid") = True
                                dRow(0).Item("voiduserunkid") = User._Object._Userunkid
                                dRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                dRow(0).Item("voidreason") = mstrVoidReason
                                dRow(0).Item("AUD") = "D"
                            End If
                        End If
                        mdtMembers.AcceptChanges()
                        frm1 = Nothing
            End If
                    Call FillMembersList()
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellClick", mstrModuleName)
        End Try
    End Sub

#End Region
    'Pinkal (19-Dec-2020) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbHearingInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbHearingInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbHearingInfo.Text = Language._Object.getCaption(Me.gbHearingInfo.Name, Me.gbHearingInfo.Text)
            Me.lblCommittee.Text = Language._Object.getCaption(Me.lblCommittee.Name, Me.lblCommittee.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.lblVenue.Text = Language._Object.getCaption(Me.lblVenue.Name, Me.lblVenue.Text)
            Me.lblHearingTime.Text = Language._Object.getCaption(Me.lblHearingTime.Name, Me.lblHearingTime.Text)
            Me.lblHearingDate.Text = Language._Object.getCaption(Me.lblHearingDate.Name, Me.lblHearingDate.Text)
            Me.tabpChargeDescription.Text = Language._Object.getCaption(Me.tabpChargeDescription.Name, Me.tabpChargeDescription.Text)
            Me.tabpChargeCount.Text = Language._Object.getCaption(Me.tabpChargeCount.Name, Me.tabpChargeCount.Text)
            Me.colhCount.Text = Language._Object.getCaption(CStr(Me.colhCount.Tag), Me.colhCount.Text)
            Me.colhIncident.Text = Language._Object.getCaption(CStr(Me.colhIncident.Tag), Me.colhIncident.Text)
            Me.colhOffCategory.Text = Language._Object.getCaption(CStr(Me.colhOffCategory.Tag), Me.colhOffCategory.Text)
            Me.colhOffence.Text = Language._Object.getCaption(CStr(Me.colhOffence.Tag), Me.colhOffence.Text)
            Me.colhSeverity.Text = Language._Object.getCaption(CStr(Me.colhSeverity.Tag), Me.colhSeverity.Text)
            Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.Name, Me.lblInterdictionDate.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblJobTitle.Text = Language._Object.getCaption(Me.lblJobTitle.Name, Me.lblJobTitle.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.lnkAddAttachment.Text = Language._Object.getCaption(Me.lnkAddAttachment.Name, Me.lnkAddAttachment.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("frmDisciplineProceedingList", 2, "Select Employee")
            Language.setMessage(mstrModuleName, 1, "Please specify Venue. Vanue cannot be blank.")
            Language.setMessage(mstrModuleName, 3, "Please select Committee. Committee is mandatory information.")
            Language.setMessage(mstrModuleName, 4, "There is no member defined for selected committee.")
            Language.setMessage(mstrModuleName, 5, "Please check atleast one member from list.")
            Language.setMessage(mstrModuleName, 12, "Configuration Path does not Exist.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

  
End Class