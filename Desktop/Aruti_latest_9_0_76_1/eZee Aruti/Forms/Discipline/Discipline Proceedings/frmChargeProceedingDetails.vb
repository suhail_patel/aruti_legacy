﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmChargeProceedingDetails

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmChargeProceedingDetails"
    Private objProceedingMaster As clsdiscipline_proceeding_master
    Private objMembersTran As clsdiscipline_members_tran
    Private objDisciplineFileMaster As clsDiscipline_file_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mintDisciplineProceedingMasterunkid As Integer = -1
    Private mintDisciplineFileUnkid As Integer = 0
    Private mdtMemTran As DataTable
    Private mdtProceeding As DataTable
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mstrEditGUID As String = String.Empty
    Private dtEditedRow() As DataRow = Nothing
    Private mblnIsForOpeningCharges As Boolean = False
    Private mblnIsExternal As Boolean = False
    Private mblnDropDownClosed As Boolean = False
    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private mintEmployeeUnkid As Integer = 0
    Private mdtScanAttachment As DataTable
    'S.SANDEEP |11-NOV-2019| -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, _
                                  ByVal intDisciplineFileUnkid As Integer, _
                                  ByVal intProceedingMasterId As Integer, _
                                  ByVal strEditGUID As String, _
                                  ByRef dtProceeding As DataTable, _
                                  ByRef dtMembers As DataTable, _
                                  ByRef dtScanAttachment As DataTable, _
                                  ByVal blnIsForOpeningCharges As Boolean, _
                                  Optional ByVal blnIsExternal As Boolean = False) As Boolean 'S.SANDEEP |11-NOV-2019| -- START {dtScanAttachment} -- END
        Try
            menAction = eAction
            mintDisciplineProceedingMasterunkid = intProceedingMasterId
            mintDisciplineFileUnkid = intDisciplineFileUnkid
            mdtProceeding = dtProceeding
            mdtMemTran = dtMembers
            mstrEditGUID = strEditGUID
            mblnIsForOpeningCharges = blnIsForOpeningCharges
            mblnIsExternal = blnIsExternal

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            mdtScanAttachment = dtScanAttachment
            If ConfigParameter._Object._AddProceedingAgainstEachCount Then
                tabcDescription_Response.TabPages.Remove(tabpChargeDescription)
                tabcDescription_Response.TabPages.Remove(tabpChargeCount)
            Else
                tabcDescription_Response.TabPages.Remove(tabpCountProceeding)
                Call GetValueByReferenceNo()
            End If
            'S.SANDEEP |01-OCT-2019| -- END

            Me.ShowDialog()

            dtProceeding = mdtProceeding
            dtMembers = mdtMemTran
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            dtScanAttachment = mdtScanAttachment
            'S.SANDEEP |11-NOV-2019| -- END

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmChargeProceedingDetails_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objProceedingMaster = New clsdiscipline_proceeding_master
        objMembersTran = New clsdiscipline_members_tran
        objDisciplineFileMaster = New clsDiscipline_file_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                Call DisplayEditedValue()
                cboCommittee.Enabled = False : objbtnSearchCommittee.Enabled = False
            Else
                cboChargesCount.DrawMode = DrawMode.OwnerDrawFixed
                AddHandler cboChargesCount.DrawItem, AddressOf cboChargesCount_DrawItem
                AddHandler cboChargesCount.DropDownClosed, AddressOf cboChargesCount_DropDownClosed
            End If
            Call Fill_Grid()
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            lnkAddAttachment.Visible = User._Object.Privilege._AllowToScanAttachmentDocuments
            'S.SANDEEP |11-NOV-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmChargeProceedingDetails_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsdiscipline_proceeding_master.SetMessages()
            clsdiscipline_members_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsdiscipline_proceeding_master,clsdiscipline_members_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private Sub GetValueByReferenceNo()
        Try
            Dim objDisciplineFileMaster As New clsDiscipline_file_master
            Dim objDisciplineFileTran As New clsDiscipline_file_tran

            objDisciplineFileMaster._Disciplinefileunkid = mintDisciplineFileUnkid
            txtChargeDescription.Text = objDisciplineFileMaster._Charge_Description

            '=====================  Fill Charges Counts ====================================
            objDisciplineFileTran._Disciplinefileunkid = mintDisciplineFileUnkid
            Dim dtCharges As DataTable = objDisciplineFileTran._ChargesTable
            Call FillChargeCountList(dtCharges)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValueByReferenceNo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillChargeCountList(ByVal mdtCharges As DataTable)
        Try
            Dim intChargesCount As Integer = 1
            Dim lvItem As ListViewItem
            lvChargesCount.Items.Clear()

            For Each dtRow As DataRow In mdtCharges.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.Text = intChargesCount.ToString
                    lvItem.SubItems.Add(dtRow.Item("incident_description").ToString)
                    lvItem.SubItems.Add(dtRow.Item("charge_category").ToString)
                    lvItem.SubItems.Add(dtRow.Item("charge_descr").ToString)
                    lvItem.SubItems.Add(dtRow.Item("charge_severity").ToString)
                    lvItem.SubItems.Add(dtRow.Item("offencecategoryunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("offenceunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvItem.SubItems.Add(dtRow.Item("disciplinefileunkid").ToString)
                    lvItem.Tag = dtRow.Item("disciplinefiletranunkid").ToString

                    intChargesCount += 1

                    lvChargesCount.Items.Add(lvItem)
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillChargeCountList", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP |01-OCT-2019| -- END

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objActionReason As New clsAction_Reason
        Dim objCommonMaster As New clsCommon_Master
        Dim objFileTran As New clsDiscipline_file_tran
        Try
            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
            With cboCommittee
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objActionReason.getComboList("List", True, True)
            With cboDisciplinaryAction
                .DisplayMember = "name"
                .ValueMember = "actionreasonunkid"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objProceedingMaster.getProceedingCountStatus("Count", True)
            With cboCountStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Count")
                .SelectedValue = 0
            End With

            If mblnIsForOpeningCharges = False Then
                objFileTran._Disciplinefileunkid = mintDisciplineFileUnkid
                dsList = New DataSet
                dsList.Tables.Add(objFileTran._ChargesTable.Copy)

                Dim dtable As DataTable = Nothing
                If ConfigParameter._Object._PreventDisciplineChargeunlessEmployeeReceipt Then
                    dtable = New DataView(dsList.Tables(0), "responsetypeunkid > 0", "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If
                Dim dR As DataRow = dtable.NewRow
                dR.Item("disciplinefiletranunkid") = 0
                dR.Item("display_incident") = Language.getMessage(mstrModuleName, 1, "Select")
                dtable.Rows.InsertAt(dR, 0)
                With cboChargesCount
                    .ValueMember = "disciplinefiletranunkid"
                    .DisplayMember = "display_incident"
                    .DataSource = dtable
                    .SelectedValue = 0
                End With
            Else
                dsList = objProceedingMaster.GetClosedCountForOpenProcess(mintDisciplineFileUnkid, "List")
                Dim dR As DataRow = dsList.Tables(0).NewRow
                dR.Item("disciplinefiletranunkid") = 0
                dR.Item("display_incident") = Language.getMessage(mstrModuleName, 1, "Select")
                dsList.Tables(0).Rows.InsertAt(dR, 0)

                With cboChargesCount
                    .ValueMember = "disciplinefiletranunkid"
                    .DisplayMember = "display_incident"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Try
            Dim mView As DataView = mdtMemTran.DefaultView
            If CInt(cboChargesCount.SelectedValue) <= 0 Then
                mView.RowFilter = "AUD <> 'D' "
            Else
                mView.RowFilter = "AUD <> 'D' AND disciplinefiletranunkid = '" & CInt(cboChargesCount.SelectedValue) & "' "
            End If

            dgvData.AutoGenerateColumns = False
            dgcolhCompany.DataPropertyName = "company"
            dgcolhContactNo.DataPropertyName = "contactno"
            dgcolhDepartment.DataPropertyName = "department"
            dgcolhEmail.DataPropertyName = "emailid"
            dgcolhMembers.DataPropertyName = "members"
            objdgcolhcommitteetranunkid.DataPropertyName = "committeetranunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            objdgcolhDisciplineMembersTranUnkid.DataPropertyName = "disciplinememberstranunkid"
            dgvData.DataSource = mView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidInformation() As Boolean
        Try
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'If CInt(cboChargesCount.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Charge count is mandatory information. Please select charge count to continue."), enMsgBoxStyle.Information)
            '    cboChargesCount.Focus()
            '    Return False
            'End If
            'If mblnIsForOpeningCharges Then
            '    Dim strMessage As String
            '    strMessage = objProceedingMaster.IsValidOpen_Operation(CInt(cboChargesCount.SelectedValue))
            '    If strMessage.Trim.Length > 0 Then
            '        eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
            '        Return False
            '    End If
            'End If

            'If menAction <> enAction.EDIT_ONE Then
            '    Dim strMessage As String
            '    strMessage = objProceedingMaster.IsPostingAllowed(CInt(cboChargesCount.SelectedValue), 1, Nothing)
            '    If strMessage.Trim.Length > 0 Then
            '        eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
            '        Return False
            '    End If

            '    strMessage = objProceedingMaster.IsPostingAllowed(CInt(cboChargesCount.SelectedValue), 0, dtpProceedingDate.Value.Date)
            '    If strMessage.Trim.Length > 0 Then
            '        eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
            '        Return False
            '    End If
            'End If

            If tabcDescription_Response.TabPages.Contains(tabpCountProceeding) Then
            If CInt(cboChargesCount.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Charge count is mandatory information. Please select charge count to continue."), enMsgBoxStyle.Information)
                cboChargesCount.Focus()
                Return False
            End If

            If mblnIsForOpeningCharges Then
                Dim strMessage As String
                strMessage = objProceedingMaster.IsValidOpen_Operation(CInt(cboChargesCount.SelectedValue))
                If strMessage.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            If menAction <> enAction.EDIT_ONE Then
                Dim strMessage As String
                    If tabcDescription_Response.TabPages.Contains(tabpCountProceeding) Then
                        strMessage = objProceedingMaster.IsPostingAllowed(CInt(cboChargesCount.SelectedValue), 1, Nothing, True)
                    Else
                        strMessage = objProceedingMaster.IsPostingAllowed(mintDisciplineFileUnkid, 1, Nothing, False)
                    End If
                If strMessage.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                    Return False
                End If

                    If tabcDescription_Response.TabPages.Contains(tabpCountProceeding) Then
                        strMessage = objProceedingMaster.IsPostingAllowed(CInt(cboChargesCount.SelectedValue), 0, dtpProceedingDate.Value.Date, True)
                    Else
                        strMessage = objProceedingMaster.IsPostingAllowed(mintDisciplineFileUnkid, 0, dtpProceedingDate.Value.Date, True)
                    End If
                If strMessage.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            End If
            'S.SANDEEP |01-OCT-2019| -- END

            If CInt(cboDisciplinaryAction.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Disciplinary action is mandatory information. Please select disciplinary action to continue."), enMsgBoxStyle.Information)
                cboDisciplinaryAction.Focus()
                Return False
            End If

            If CInt(cboCountStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Count Status is mandatory information. Please select count status to continue."), enMsgBoxStyle.Information)
                cboChargesCount.Focus()
                Return False
            End If

            If txtProceedingDetials.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Proceeding remark is mandatory information. Please provide proceeding remark to continue."), enMsgBoxStyle.Information)
                txtProceedingDetials.Focus()
                Return False
            End If

            If mdtProceeding.Rows.Count > 0 Then
                If menAction <> enAction.EDIT_ONE Then
                    If mdtProceeding.AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue) AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, This Proceeding detail is already added in the list below for selected Count and Proceeding Date."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

            End If

            If mdtMemTran.AsEnumerable().Where(Function(x) x.Field(Of String)("members") = "").Count = mdtMemTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Count Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Members are mandatory information. Please add atleast one member to continue."), enMsgBoxStyle.Information)
                Return False
            End If


            'S.SANDEEP |18-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : RELAXING PENALTY EXPIRY DATE

            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            'If dtpPenaltyEffDate.Checked AndAlso dtpPenaltyExpiryDate.Checked = False Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Penalty Expiry Date is compulsory information. Please select Penalty Expiry Date."), enMsgBoxStyle.Information)
            '    Return False
            'ElseIf dtpPenaltyEffDate.Checked = False AndAlso dtpPenaltyExpiryDate.Checked Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Penalty Effective Date is compulsory information. Please select Penalty Effective Date."), enMsgBoxStyle.Information)
            '    Return False
            'ElseIf (dtpPenaltyEffDate.Checked AndAlso dtpPenaltyExpiryDate.Checked) AndAlso (dtpPenaltyEffDate.Value.Date > dtpPenaltyExpiryDate.Value.Date) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Penalty Effective Date must be less than Penalty Expiry Date."), enMsgBoxStyle.Information)
            '    dtpPenaltyEffDate.Focus()
            '    Return False
            'End If
            'Pinkal (19-Dec-2020) -- End


            If dtpPenaltyEffDate.Checked = False AndAlso dtpPenaltyExpiryDate.Checked Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Penalty Effective Date is compulsory information. Please select Penalty Effective Date."), enMsgBoxStyle.Information)
                Return False
            ElseIf (dtpPenaltyEffDate.Checked AndAlso dtpPenaltyExpiryDate.Checked) AndAlso (dtpPenaltyEffDate.Value.Date > dtpPenaltyExpiryDate.Value.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Penalty Effective Date must be less than Penalty Expiry Date."), enMsgBoxStyle.Information)
                dtpPenaltyEffDate.Focus()
                Return False
            End If
            
            'S.SANDEEP |18-JUN-2021| -- END

            


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidInformation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub DisplayEditedValue()
        Try
            If mdtProceeding IsNot Nothing Then
                If mintDisciplineProceedingMasterunkid > 0 Then
                    dtEditedRow = mdtProceeding.Select("disciplineproceedingmasterunkid = '" & mintDisciplineProceedingMasterunkid & "' AND AUD <> 'D'")
                Else
                    dtEditedRow = mdtProceeding.Select("GUID = '" & mstrEditGUID & "' AND AUD <> 'D'")
                End If

                Dim dtMemRow() As DataRow = mdtMemTran.Select("disciplineproceedingmasterunkid = '" & mintDisciplineProceedingMasterunkid & "' AND AUD <> 'D'")
                If dtMemRow.Length > 0 Then
                    cboCommittee.SelectedValue = dtMemRow(0).Item("committeemasterunkid")
                Else
                    cboCommittee.SelectedValue = 0
                End If
                If dtEditedRow.Length > 0 Then
                    cboChargesCount.DropDownStyle = ComboBoxStyle.Simple
                    cboChargesCount.SelectedValue = CInt(dtEditedRow(0).Item("disciplinefiletranunkid"))
                    dtpProceedingDate.Value = CDate(dtEditedRow(0).Item("proceeding_date"))
                    cboDisciplinaryAction.SelectedValue = CStr(dtEditedRow(0).Item("actionreasonunkid"))
                    cboCountStatus.SelectedValue = CInt(dtEditedRow(0).Item("count_status"))
                    txtProceedingDetials.Text = CStr(dtEditedRow(0).Item("proceeding_details"))
                    txtPersonInvolvedComments.Text = CStr(dtEditedRow(0).Item("person_involved_comments"))
                    txtRemarkCommnts.Text = CStr(dtEditedRow(0).Item("remarks_comments"))


                    'Pinkal (19-Dec-2020) -- Start
                    'Enhancement  -  Working on Discipline module for NMB.
                    If IsDBNull(dtEditedRow(0).Item("penalty_effectivedate")) = False AndAlso dtEditedRow(0).Item("penalty_effectivedate") IsNot Nothing Then
                        dtpPenaltyEffDate.Value = CDate(dtEditedRow(0).Item("penalty_effectivedate"))
                    End If

                    If IsDBNull(dtEditedRow(0).Item("penalty_expirydate")) = False AndAlso dtEditedRow(0).Item("penalty_expirydate") IsNot Nothing Then
                        dtpPenaltyExpiryDate.Value = CDate(dtEditedRow(0).Item("penalty_expirydate"))
                    End If
                    'Pinkal (19-Dec-2020) -- End

                    If CBool(dtEditedRow(0).Item("isapproved")) = True Then
                        dtpProceedingDate.Enabled = False
                        cboCommittee.Enabled = False
                        cboDisciplinaryAction.Enabled = False
                        txtPersonInvolvedComments.ReadOnly = True
                        txtProceedingDetials.ReadOnly = True
                        txtRemarkCommnts.ReadOnly = True
                        objdgcolhAdd.Visible = False
                        objdgcolhEdit.Visible = False
                        objdgcolhDelete.Visible = False
                        objbtnSearchCommittee.Enabled = False
                        objbtnSearchDisciplinaryAction.Enabled = False

                        'Pinkal (19-Dec-2020) -- Start
                        'Enhancement  -  Working on Discipline module for NMB.
                        dtpPenaltyEffDate.Enabled = False
                        dtpPenaltyExpiryDate.Enabled = False
                        'Pinkal (19-Dec-2020) -- End

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SplitToolTip(ByVal strOrig As String) As String
        Dim strArray As String()
        Dim SPACE As String = " "
        Dim CR As String = vbCrLf
        Dim strOneWord As String = ""
        Dim strBuilder As String = ""
        Dim strReturn As String = ""
        strArray = strOrig.Split(CChar(SPACE))
        For Each strOneWord In strArray
            strBuilder = strBuilder & strOneWord & SPACE
            If Len(strBuilder) > 70 Then
                strReturn = strReturn & strBuilder & CR
                strBuilder = ""
            End If
        Next
        If Len(strBuilder) < 8 Then
            If strReturn.Trim.Length > 0 Then
                strReturn = strReturn.Substring(0, strReturn.Length - 2)
            End If
        End If
        Return strReturn & strBuilder
    End Function

#End Region

#Region " Button's Event "

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If IsValidInformation() = False Then Exit Sub
            If menAction <> enAction.EDIT_ONE Then
                Dim dRow As DataRow = mdtProceeding.NewRow
                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'dRow.Item("count") = (CType(cboChargesCount.SelectedItem, DataRowView).Item("charge_count")).ToString
                If cboChargesCount.SelectedItem IsNot Nothing Then
                dRow.Item("count") = (CType(cboChargesCount.SelectedItem, DataRowView).Item("charge_count")).ToString
                End If
                'S.SANDEEP |01-OCT-2019| -- END
                dRow.Item("disciplineproceedingmasterunkid") = mintDisciplineProceedingMasterunkid
                dRow.Item("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)
                dRow.Item("disciplinefileunkid") = mintDisciplineFileUnkid
                dRow.Item("committeemasterunkid") = CInt(cboCommittee.SelectedValue)

                If CInt(cboCommittee.SelectedValue) > 0 Then
                    dRow.Item("committee") = cboCommittee.Text
                Else
                    dRow.Item("committee") = ""
                End If
                Dim strInvestigators As String = ""
                If mdtMemTran.Rows.Count > 0 Then
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'strInvestigators = String.Join(",", mdtMemTran.AsEnumerable().Where(Function(y) y.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue) And y.Field(Of Integer)("disciplinefiletranunkid") > 0).Select(Function(x) x.Field(Of String)("members").ToString).ToArray)
                    If tabcDescription_Response.TabPages.Contains(tabpCountProceeding) Then
                    strInvestigators = String.Join(",", mdtMemTran.AsEnumerable().Where(Function(y) y.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue) And y.Field(Of Integer)("disciplinefiletranunkid") > 0).Select(Function(x) x.Field(Of String)("members").ToString).ToArray)
                    Else
                        strInvestigators = String.Join(",", mdtMemTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of String)("members").ToString).ToArray)
                    End If
                    'S.SANDEEP |01-OCT-2019| -- END
                End If

                dRow.Item("investigator") = strInvestigators
                dRow.Item("proceeding_date") = dtpProceedingDate.Value
                dRow.Item("proceeding_details") = txtProceedingDetials.Text
                dRow.Item("remarks_comments") = txtRemarkCommnts.Text
                dRow.Item("person_involved_comments") = txtPersonInvolvedComments.Text
                dRow.Item("actionreasonunkid") = CInt(cboDisciplinaryAction.SelectedValue)
                dRow.Item("reason_action") = cboDisciplinaryAction.Text
                dRow.Item("isapproved") = False 'CBool(User._Object.Privilege._AllowToApproveProceedingCounts)
                dRow.Item("count_status") = CInt(cboCountStatus.SelectedValue)
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("status_name") = cboCountStatus.Text
                dRow.Item("approved_status") = Language.getMessage("clsdiscipline_proceeding_master", 5, "Pending")
                dRow.Item("issubmitforapproval") = False
                dRow.Item("statusdate") = ConfigParameter._Object._CurrentDateAndTime
                dRow.Item("remark") = ""
                If mblnIsExternal <> False Then
                    dRow.Item("isexternal") = mblnIsExternal
                Else
                    dRow.Item("isexternal") = dRow.Item("isexternal")
                End If


                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                If dtpPenaltyEffDate.Checked Then
                    dRow.Item("penalty_effectivedate") = dtpPenaltyEffDate.Value
                Else
                    dRow.Item("penalty_effectivedate") = DBNull.Value
                End If

                If dtpPenaltyExpiryDate.Checked Then
                    dRow.Item("penalty_expirydate") = dtpPenaltyExpiryDate.Value
                Else
                    dRow.Item("penalty_expirydate") = DBNull.Value
                End If

                'Pinkal (19-Dec-2020) -- End


                mdtProceeding.Rows.Add(dRow)

            Else
                If dtEditedRow.Length > 0 Then
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'dtEditedRow(0).Item("count") = (CType(cboChargesCount.SelectedItem, DataRowView).Item("charge_count")).ToString
                    If cboChargesCount.SelectedItem IsNot Nothing Then
                    dtEditedRow(0).Item("count") = (CType(cboChargesCount.SelectedItem, DataRowView).Item("charge_count")).ToString
                    End If
                    'S.SANDEEP |01-OCT-2019| -- END
                    dtEditedRow(0).Item("disciplineproceedingmasterunkid") = dtEditedRow(0).Item("disciplineproceedingmasterunkid")
                    dtEditedRow(0).Item("disciplinefiletranunkid") = dtEditedRow(0).Item("disciplinefiletranunkid")
                    dtEditedRow(0).Item("disciplinefileunkid") = mintDisciplineFileUnkid
                    dtEditedRow(0).Item("committeemasterunkid") = CInt(cboCommittee.SelectedValue)
                    If CInt(cboCommittee.SelectedValue) > 0 Then
                        dtEditedRow(0).Item("committee") = cboCommittee.Text
                    Else
                        dtEditedRow(0).Item("committee") = ""
                    End If
                    Dim strInvestigators As String = ""
                    If mdtMemTran.Rows.Count > 0 Then
                        'strInvestigators = String.Join(",", mdtMemTran.AsEnumerable().Select(Function(x) x.Field(Of String)("members").ToString).ToArray)
                        'S.SANDEEP |01-OCT-2019| -- START
                        'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                        'strInvestigators = String.Join(",", mdtMemTran.AsEnumerable().Where(Function(y) y.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)).Select(Function(x) x.Field(Of String)("members").ToString).ToArray)
                        If tabcDescription_Response.TabPages.Contains(tabpCountProceeding) Then
                        strInvestigators = String.Join(",", mdtMemTran.AsEnumerable().Where(Function(y) y.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)).Select(Function(x) x.Field(Of String)("members").ToString).ToArray)
                        Else
                            strInvestigators = String.Join(",", mdtMemTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of String)("members").ToString).ToArray)
                        End If
                        'S.SANDEEP |01-OCT-2019| -- END
                    End If

                    dtEditedRow(0).Item("investigator") = strInvestigators
                    dtEditedRow(0).Item("proceeding_date") = dtpProceedingDate.Value
                    dtEditedRow(0).Item("proceeding_details") = txtProceedingDetials.Text
                    dtEditedRow(0).Item("remarks_comments") = txtRemarkCommnts.Text
                    dtEditedRow(0).Item("person_involved_comments") = txtPersonInvolvedComments.Text
                    dtEditedRow(0).Item("actionreasonunkid") = CInt(cboDisciplinaryAction.SelectedValue)
                    dtEditedRow(0).Item("reason_action") = cboDisciplinaryAction.Text
                    dtEditedRow(0).Item("isapproved") = dtEditedRow(0).Item("isapproved")
                    dtEditedRow(0).Item("count_status") = CInt(cboCountStatus.SelectedValue)
                    dtEditedRow(0).Item("isvoid") = dtEditedRow(0).Item("isvoid")
                    dtEditedRow(0).Item("voiduserunkid") = dtEditedRow(0).Item("voiduserunkid")
                    dtEditedRow(0).Item("voiddatetime") = dtEditedRow(0).Item("voiddatetime")
                    dtEditedRow(0).Item("voidreason") = dtEditedRow(0).Item("voidreason")
                    dtEditedRow(0).Item("AUD") = IIf(dtEditedRow(0).Item("AUD").ToString <> "A", "U", dtEditedRow(0).Item("AUD"))
                    dtEditedRow(0).Item("GUID") = Guid.NewGuid.ToString
                    dtEditedRow(0).Item("status_name") = cboCountStatus.Text
                    dtEditedRow(0).Item("approved_status") = dtEditedRow(0).Item("approved_status")
                    dtEditedRow(0).Item("issubmitforapproval") = dtEditedRow(0).Item("issubmitforapproval")
                    dtEditedRow(0).Item("statusdate") = dtEditedRow(0).Item("statusdate")
                    dtEditedRow(0).Item("remark") = dtEditedRow(0).Item("remark")
                    dtEditedRow(0).Item("isexternal") = dtEditedRow(0).Item("isexternal")


                    'Pinkal (19-Dec-2020) -- Start
                    'Enhancement  -  Working on Discipline module for NMB.
                    If dtpPenaltyEffDate.Checked Then
                        dtEditedRow(0).Item("penalty_effectivedate") = dtpPenaltyEffDate.Value
                    Else
                        dtEditedRow(0).Item("penalty_effectivedate") = DBNull.Value
                    End If

                    If dtpPenaltyExpiryDate.Checked Then
                        dtEditedRow(0).Item("penalty_expirydate") = dtpPenaltyExpiryDate.Value
                    Else
                        dtEditedRow(0).Item("penalty_expirydate") = DBNull.Value
                    End If

                    'Pinkal (19-Dec-2020) -- End


                    mdtProceeding.AcceptChanges()
                End If
            End If
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If menAction <> enAction.EDIT_ONE Then
                mdtMemTran.Rows.Clear()
            End If
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboChargesCount_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChargesCount.SelectedIndexChanged
        Try
            If CInt(cboChargesCount.SelectedValue) > 0 Then
                cboOffenceCategory.Items.Clear()
                cboDisciplineType.Items.Clear()
                Dim datarow = (From P In CType(cboChargesCount.DataSource, DataTable).AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)) Select P)
                If datarow.Count > 0 AndAlso datarow(0).Item("charge_category").ToString.Trim.Length > 0 Then
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    txtIncidentDesc.Text = datarow(0).Item("incident_description").ToString()
                    'S.SANDEEP |01-OCT-2019| -- END
                    Dim iCboItems() As ComboBoxValue = {New ComboBoxValue(CStr(datarow(0).Item("charge_category")), CInt(datarow(0).Item("offencecategoryunkid")))}
                    With cboOffenceCategory
                        .Items.Clear()
                        .Items.AddRange(iCboItems)
                        .SelectedIndex = 0
                        .SelectedValue = CType(cboOffenceCategory.SelectedItem, ComboBoxValue).Value
                    End With
                End If
                datarow = (From P In CType(cboChargesCount.DataSource, DataTable).AsEnumerable().Where(Function(x) x.Field(Of Integer)("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)) Select P)
                If datarow.Count > 0 AndAlso datarow(0).Item("charge_category").ToString.Trim.Length > 0 Then
                    Dim iCboItems() As ComboBoxValue = {New ComboBoxValue(CStr(datarow(0).Item("charge_descr")), CInt(datarow(0).Item("offenceunkid")))}
                    With cboDisciplineType
                        .Items.Clear()
                        .Items.AddRange(iCboItems)
                        .SelectedIndex = 0
                        .SelectedValue = CType(cboDisciplineType.SelectedItem, ComboBoxValue).Value
                    End With
                End If
                'pnlDetails.Enabled = True
            Else
                'pnlDetails.Enabled = False
                cboOffenceCategory.Items.Clear()
                cboDisciplineType.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChargesCount_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboCommittee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCommittee.SelectedIndexChanged
        Try
            If CInt(cboCommittee.SelectedValue) > 0 Then

                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                'Dim objGrp As New clsGroup_Master
                'objGrp._Groupunkid = 1
                'If objGrp._Groupname.ToUpper = "NMB PLC" Then Exit Sub
                'objGrp = Nothing
                'Pinkal (19-Dec-2020) -- End

                Me.Cursor = Cursors.WaitCursor
                If menAction <> enAction.EDIT_ONE Then
                    Dim dTable As DataTable
                    Dim objCommittee As New clsDiscipline_Committee
                    Dim dRow As DataRow = Nothing

                    'Pinkal (22-Jan-2021) -- Start
                    'Enhancement NMB - Working Discipline changes required by NMB.
                    'objCommittee._CommitteeMasterId(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboCommittee.SelectedValue)
                    objCommittee._CommitteeMasterId(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._DatabaseName, ConfigParameter._Object._IsIncludeInactiveEmp) = CInt(cboCommittee.SelectedValue)
                    'Pinkal (22-Jan-2021) -- End

                    mdtMemTran.Rows.Clear()

                    If mdtMemTran.Rows.Count > 0 Then
                        If objCommittee._DataTable.Rows.Count > 0 AndAlso CStr(objCommittee._DataTable.Rows(0)("Ids")).Trim.Length > 0 Then
                            dTable = New DataView(objCommittee._DataTable, "committeetranunkid NOT IN(" & CStr(objCommittee._DataTable.Rows(0)("Ids")) & ")", "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dTable = New DataView(objCommittee._DataTable, "", "", DataViewRowState.CurrentRows).ToTable
                        End If
                    Else
                        dTable = New DataView(objCommittee._DataTable, "", "", DataViewRowState.CurrentRows).ToTable
                    End If

                    'If dTable.Rows.Count <= 0 Then
                    '    dTable.Rows.Add(dTable.NewRow)
                    'End If

                    For Each dtRow As DataRow In dTable.Rows
                        Dim intTranId As Integer = CInt(dtRow("committeetranunkid"))
                        If mdtMemTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("committeetranunkid") = intTranId).Count <= 0 Then
                            dRow = mdtMemTran.NewRow
                            dRow("disciplinememberstranunkid") = -1
                            dRow("disciplineproceedingmasterunkid") = mintDisciplineProceedingMasterunkid
                            dRow("disciplinefiletranunkid") = CInt(cboChargesCount.SelectedValue)
                            dRow("committeetranunkid") = dtRow("committeetranunkid")
                            dRow("committeemasterunkid") = CInt(cboCommittee.SelectedValue)
                            dRow("committee") = cboCommittee.Text
                            dRow("isvoid") = False
                            dRow("voiduserunkid") = -1
                            dRow("voiddatetime") = DBNull.Value
                            dRow("voidreason") = ""
                            dRow("AUD") = "A"
                            dRow("GUID") = Guid.NewGuid.ToString

                            If CInt(dtRow.Item("employeeunkid")) <> -1 Then
                                dRow("members") = dtRow.Item("employee_name")
                                dRow("department") = dtRow.Item("department")
                                dRow("company") = Company._Object._Name
                                dRow("contactno") = dtRow.Item("contact")
                                dRow("emailid") = dtRow.Item("emailid")
                                dRow("employeeunkid") = dtRow.Item("employeeunkid")
                            Else
                                dRow("members") = dtRow.Item("ex_name")
                                dRow("department") = dtRow.Item("ex_department")
                                dRow("company") = dtRow.Item("ex_company")
                                dRow("contactno") = dtRow.Item("ex_contactno")
                                dRow("emailid") = dtRow.Item("ex_email")

                                If intTranId <= 0 Then
                                    dRow("ex_name") = dtRow.Item("ex_name")
                                    dRow("ex_company") = dtRow.Item("ex_company")
                                    dRow("ex_department") = dtRow.Item("ex_department")
                                    dRow("ex_contactno") = dtRow.Item("ex_contactno")
                                    dRow("ex_email") = dtRow.Item("ex_email")
                                End If

                            End If

                            mdtMemTran.Rows.Add(dRow)
                        End If
                    Next
                End If
                Call Fill_Grid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCommittee_SelectedIndexChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub cboChargesCount_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles cboChargesCount.KeyPress
        Try
            If cboChargesCount.DropDownStyle = ComboBoxStyle.Simple Then
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ComboBox_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboChargesCount_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cboChargesCount.KeyDown
        Try
            If cboChargesCount.DropDownStyle = ComboBoxStyle.Simple Then
                If e.KeyCode = Keys.Right Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Home Or e.KeyCode = Keys.End Or e.KeyCode = Keys.Tab Then
                    e.Handled = False
                Else
                    e.Handled = True
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ComboBox_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboChargesCount_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChargesCount.DropDown
        Try
            mblnDropDownClosed = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChargesCount_DropDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboChargesCount_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ToolTip1.Hide(cboChargesCount) : mblnDropDownClosed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChargesCount_DropDownClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboChargesCount_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs)
        Try
            If e.Index < 0 Then
                Return
            End If
            Dim text As String = cboChargesCount.GetItemText(cboChargesCount.Items(e.Index))
            e.DrawBackground()
            Using br As New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                If mblnDropDownClosed = False Then
                    If text.Trim.Length > 0 Then
                        ToolTip1.Show(SplitToolTip(text), cboChargesCount, e.Bounds.Right, e.Bounds.Bottom)
                    End If
                End If
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChargesCount_DrawItem", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAddMember_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddMember.LinkClicked
        Dim frm As New frmAddMembers
        Dim objDfile As New clsDiscipline_file_master
        Try
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'If CInt(cboChargesCount.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Please Select Count. Count is mandatory information."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            If tabcDescription_Response.TabPages.Contains(tabpCountProceeding) Then
            If CInt(cboChargesCount.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Please Select Count. Count is mandatory information."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            End If
            'S.SANDEEP |11-NOV-2019| -- END

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            'If frm.displayDialog(enAction.ADD_ONE, -1, mintDisciplineProceedingMasterunkid, CInt(cboChargesCount.SelectedValue), -1, "", mdtMemTran) Then

            If CInt(cboCommittee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please select Committee. Committee is mandatory information."), enMsgBoxStyle.Information)
                cboCommittee.Focus()
                Exit Sub
            End If
            objDfile._Disciplinefileunkid = mintDisciplineFileUnkid

            If frm.displayDialog(enAction.ADD_ONE, -1, mintDisciplineProceedingMasterunkid, CInt(cboChargesCount.SelectedValue), -1, "", mdtMemTran, CInt(cboCommittee.SelectedValue), objDfile._Involved_Employeeunkid) Then
                'Pinkal (19-Dec-2020) -- End
                Call Fill_Grid()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddMember_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
            objDfile = Nothing
        End Try
    End Sub

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private Sub lnkAddAttachment_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddAttachment.LinkClicked
        Dim frm As New frmScanOrAttachmentInfo
        Try
            Dim objScanAttachment As New clsScan_Attach_Documents
            Dim objDisciplineFile As New clsDiscipline_file_master
            objDisciplineFile._Disciplinefileunkid = mintDisciplineFileUnkid
            mintEmployeeUnkid = objDisciplineFile._Involved_Employeeunkid
            objDisciplineFile = Nothing
            objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", mintEmployeeUnkid, False, Nothing, Nothing, "", CBool(IIf(mintEmployeeUnkid <= 0, True, False)))
            Dim xRow = objScanAttachment._Datatable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.DISCIPLINES _
                                                                         And x.Field(Of Integer)("transactionunkid") = CInt(IIf(mintDisciplineProceedingMasterunkid <= 0, mintDisciplineFileUnkid, mintDisciplineProceedingMasterunkid)) _
                                                                         And x.Field(Of String)("form_name") = "frmDisciplineProceedingList")

            If xRow.Count > 0 Then
                mdtScanAttachment = xRow.CopyToDataTable()
            Else
                mdtScanAttachment = objScanAttachment._Datatable.Clone
            End If
            objScanAttachment = Nothing

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm._dtAttachment = mdtScanAttachment
            frm._TransactionID = CInt(IIf(mintDisciplineProceedingMasterunkid <= 0, mintDisciplineFileUnkid, mintDisciplineProceedingMasterunkid))
            Dim strScreen As String = String.Empty
            If mintDisciplineProceedingMasterunkid > 0 Then
                strScreen = "frmDisciplineProceedingList"
            Else
                strScreen = ""
            End If

            frm.displayDialog(Language.getMessage("frmDisciplineProceedingList", 2, "Select Employee"), enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", strScreen, True, mintEmployeeUnkid, enScanAttactRefId.DISCIPLINES, True, False)
            mdtScanAttachment = frm._dtAttachment

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddAttachment_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP |11-NOV-2019| -- END

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvData_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellClick
        Dim frm As New frmAddMembers
        Dim objDfile As New clsDiscipline_file_master
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Select Case e.ColumnIndex

                Case objdgcolhAdd.Index
                    'If CInt(cboChargesCount.SelectedValue) <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Please Select Count. Select Count is mandatory infomration."), enMsgBoxStyle.Information)
                    '    Exit Sub
                    'End If
                    'If frm.displayDialog(enAction.ADD_ONE, -1, mintDisciplineProceedingMasterunkid, CInt(cboChargesCount.SelectedValue), -1, "", mdtMemTran) Then
                    '    Call Fill_Grid()
                    'End If

                Case objdgcolhEdit.Index
                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub


                    'Pinkal (19-Dec-2020) -- Start
                    'Enhancement  -  Working on Discipline module for NMB.

                    'If frm.displayDialog(enAction.EDIT_ONE, _
                    '                      CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhDisciplineMembersTranUnkid.Index).Value), _
                    '                      mintDisciplineProceedingMasterunkid, _
                    '                      CInt(cboChargesCount.SelectedValue), _
                    '                      CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhcommitteetranunkid.Index).Value), _
                    '                      CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value), mdtMemTran) Then

                    objDfile._Disciplinefileunkid = mintDisciplineFileUnkid
                    If frm.displayDialog(enAction.EDIT_ONE, _
                                         CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhDisciplineMembersTranUnkid.Index).Value), _
                                         mintDisciplineProceedingMasterunkid, _
                                         CInt(cboChargesCount.SelectedValue), _
                                         CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhcommitteetranunkid.Index).Value), _
                                       CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value), mdtMemTran, CInt(cboCommittee.SelectedValue), _
                                       objDfile._Involved_Employeeunkid) Then

                        'Pinkal (19-Dec-2020) -- End

                        Call Fill_Grid()
                    End If

                Case objdgcolhDelete.Index

                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Are you sure you want to delete this member?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                        Dim frm1 As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm1.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm1.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        Dim mstrVoidReason As String = String.Empty

                        If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhDisciplineMembersTranUnkid.Index).Value) > 0 Then
                            Dim dRow() As DataRow = mdtMemTran.Select("disciplinememberstranunkid = " & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhDisciplineMembersTranUnkid.Index).Value) & " AND AUD <> 'D'")
                            If dRow.Count > 0 Then
                                frm1.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
                                If mstrVoidReason.Length <= 0 Then Exit Sub

                                dRow(0).Item("isvoid") = True
                                dRow(0).Item("voiduserunkid") = User._Object._Userunkid
                                dRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                dRow(0).Item("voidreason") = mstrVoidReason
                                dRow(0).Item("AUD") = "D"
                            End If
                        Else
                            Dim dRow() As DataRow = mdtMemTran.Select("GUID = '" & CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value) & "' AND AUD <> 'D'")
                            If dRow.Count > 0 Then
                                dRow(0).Item("isvoid") = True
                                dRow(0).Item("voiduserunkid") = User._Object._Userunkid
                                dRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                dRow(0).Item("voidreason") = mstrVoidReason
                                dRow(0).Item("AUD") = "D"
                            End If
                        End If

                        mdtMemTran.AcceptChanges()

                        frm1 = Nothing
                    End If

                    Call Fill_Grid()

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellClick", mstrModuleName)
        Finally
            objDfile = Nothing
        End Try
    End Sub

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CInt(dgvRow.Cells(objdgcolhcommitteetranunkid.Index).Value) > 0 Then
                    dgvRow.Cells(objdgcolhAdd.Index).Value = My.Resources.add_16
                    dgvRow.Cells(objdgcolhEdit.Index).Value = imgBlank
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'dgvRow.Cells(objdgcolhDelete.Index).Value = imgBlank
                    dgvRow.Cells(objdgcolhDelete.Index).Value = My.Resources.remove
                    'S.SANDEEP |01-OCT-2019| -- END
                Else
                    dgvRow.Cells(objdgcolhAdd.Index).Value = My.Resources.add_16
                    dgvRow.Cells(objdgcolhEdit.Index).Value = My.Resources.edit
                    dgvRow.Cells(objdgcolhDelete.Index).Value = My.Resources.remove
                End If
            Next
            dgvData.Refresh() : dgvData.PerformLayout()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub
    'S.SANDEEP |11-NOV-2019| -- END

    Private Sub dgvData_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvData.Scroll
        Try
            If e.OldValue <> e.NewValue Then
                dgvData.PerformLayout()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_Scroll", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbProceedingsInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbProceedingsInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnScanAttachDoc.GradientBackColor = GUI._ButttonBackColor
            Me.btnScanAttachDoc.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnScanAttachDoc.Text = Language._Object.getCaption(Me.btnScanAttachDoc.Name, Me.btnScanAttachDoc.Text)
            Me.gbProceedingsInformation.Text = Language._Object.getCaption(Me.gbProceedingsInformation.Name, Me.gbProceedingsInformation.Text)
            Me.lblProceedingDate.Text = Language._Object.getCaption(Me.lblProceedingDate.Name, Me.lblProceedingDate.Text)
            Me.lblDisciplineCommitte.Text = Language._Object.getCaption(Me.lblDisciplineCommitte.Name, Me.lblDisciplineCommitte.Text)
            Me.lblDisciAction.Text = Language._Object.getCaption(Me.lblDisciAction.Name, Me.lblDisciAction.Text)
            Me.lblCountStatus.Text = Language._Object.getCaption(Me.lblCountStatus.Name, Me.lblCountStatus.Text)
            Me.tabpProceedingsDetails.Text = Language._Object.getCaption(Me.tabpProceedingsDetails.Name, Me.tabpProceedingsDetails.Text)
            Me.tabpRemarksComments.Text = Language._Object.getCaption(Me.tabpRemarksComments.Name, Me.tabpRemarksComments.Text)
            Me.tabpPersonInvolvedComments.Text = Language._Object.getCaption(Me.tabpPersonInvolvedComments.Name, Me.tabpPersonInvolvedComments.Text)
            Me.lblChargesCount.Text = Language._Object.getCaption(Me.lblChargesCount.Name, Me.lblChargesCount.Text)
            Me.lblOffenceCategory.Text = Language._Object.getCaption(Me.lblOffenceCategory.Name, Me.lblOffenceCategory.Text)
            Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.Name, Me.lblDisciplineType.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.dgcolhMembers.HeaderText = Language._Object.getCaption(Me.dgcolhMembers.Name, Me.dgcolhMembers.HeaderText)
            Me.dgcolhDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhDepartment.Name, Me.dgcolhDepartment.HeaderText)
            Me.dgcolhCompany.HeaderText = Language._Object.getCaption(Me.dgcolhCompany.Name, Me.dgcolhCompany.HeaderText)
            Me.dgcolhContactNo.HeaderText = Language._Object.getCaption(Me.dgcolhContactNo.Name, Me.dgcolhContactNo.HeaderText)
            Me.dgcolhEmail.HeaderText = Language._Object.getCaption(Me.dgcolhEmail.Name, Me.dgcolhEmail.HeaderText)
            Me.lnkAddMember.Text = Language._Object.getCaption(Me.lnkAddMember.Name, Me.lnkAddMember.Text)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.tabpChargeDescription.Text = Language._Object.getCaption(Me.tabpChargeDescription.Name, Me.tabpChargeDescription.Text)
            Me.tabpChargeCount.Text = Language._Object.getCaption(Me.tabpChargeCount.Name, Me.tabpChargeCount.Text)
            Me.colhCount.Text = Language._Object.getCaption(CStr(Me.colhCount.Tag), Me.colhCount.Text)
            Me.colhIncident.Text = Language._Object.getCaption(CStr(Me.colhIncident.Tag), Me.colhIncident.Text)
            Me.colhOffCategory.Text = Language._Object.getCaption(CStr(Me.colhOffCategory.Tag), Me.colhOffCategory.Text)
            Me.colhOffence.Text = Language._Object.getCaption(CStr(Me.colhOffence.Tag), Me.colhOffence.Text)
            Me.colhSeverity.Text = Language._Object.getCaption(CStr(Me.colhSeverity.Tag), Me.colhSeverity.Text)
            Me.tabpCountProceeding.Text = Language._Object.getCaption(Me.tabpCountProceeding.Name, Me.tabpCountProceeding.Text)
            Me.lblIncident.Text = Language._Object.getCaption(Me.lblIncident.Name, Me.lblIncident.Text)
            Me.lnkAddAttachment.Text = Language._Object.getCaption(Me.lnkAddAttachment.Name, Me.lnkAddAttachment.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.LblPenaltyEffDate.Text = Language._Object.getCaption(Me.LblPenaltyEffDate.Name, Me.LblPenaltyEffDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Sorry, Charge count is mandatory information. Please select charge count to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Disciplinary action is mandatory information. Please select disciplinary action to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Count Status is mandatory information. Please select count status to continue.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Proceeding remark is mandatory information. Please provide proceeding remark to continue.")
            Language.setMessage(mstrModuleName, 6, "Sorry, This Proceeding detail is already added in the list below for selected Count and Proceeding Date.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Members are mandatory information. Please add atleast one member to continue.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Please Select Count. Count is mandatory information.")
            Language.setMessage(mstrModuleName, 9, "Are you sure you want to delete this member?")
			Language.setMessage(mstrModuleName, 10, "Sorry, Penalty Expiry Date is compulsory information. Please select Penalty Expiry Date.")
			Language.setMessage(mstrModuleName, 11, "Sorry, Penalty Effective Date is compulsory information. Please select Penalty Effective Date.")
			Language.setMessage(mstrModuleName, 12, "Penalty Effective Date must be less than Penalty Expiry Date.")
			Language.setMessage("frmDisciplineProceedingList", 2, "Select Employee")
			Language.setMessage("clsdiscipline_proceeding_master", 5, "Pending")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class