﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChargeProceedingDetails
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChargeProceedingDetails))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbProceedingsInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tabcDescription_Response = New System.Windows.Forms.TabControl
        Me.tabpChargeDescription = New System.Windows.Forms.TabPage
        Me.txtChargeDescription = New System.Windows.Forms.TextBox
        Me.tabpChargeCount = New System.Windows.Forms.TabPage
        Me.lvChargesCount = New eZee.Common.eZeeListView(Me.components)
        Me.colhCount = New System.Windows.Forms.ColumnHeader
        Me.colhIncident = New System.Windows.Forms.ColumnHeader
        Me.colhOffCategory = New System.Windows.Forms.ColumnHeader
        Me.colhOffence = New System.Windows.Forms.ColumnHeader
        Me.colhSeverity = New System.Windows.Forms.ColumnHeader
        Me.objcolhoffencecategoryunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhoffenceunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhfileunkid = New System.Windows.Forms.ColumnHeader
        Me.tabpCountProceeding = New System.Windows.Forms.TabPage
        Me.txtIncidentDesc = New System.Windows.Forms.TextBox
        Me.lblIncident = New System.Windows.Forms.Label
        Me.cboChargesCount = New System.Windows.Forms.ComboBox
        Me.lblChargesCount = New System.Windows.Forms.Label
        Me.lblOffenceCategory = New System.Windows.Forms.Label
        Me.cboDisciplineType = New System.Windows.Forms.ComboBox
        Me.cboOffenceCategory = New System.Windows.Forms.ComboBox
        Me.lblDisciplineType = New System.Windows.Forms.Label
        Me.pnlDetails = New System.Windows.Forms.Panel
        Me.dtpPenaltyExpiryDate = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtpPenaltyEffDate = New System.Windows.Forms.DateTimePicker
        Me.LblPenaltyEffDate = New System.Windows.Forms.Label
        Me.lnkAddAttachment = New System.Windows.Forms.LinkLabel
        Me.lnkAddMember = New System.Windows.Forms.LinkLabel
        Me.lblProceedingDate = New System.Windows.Forms.Label
        Me.dtpProceedingDate = New System.Windows.Forms.DateTimePicker
        Me.lblDisciplineCommitte = New System.Windows.Forms.Label
        Me.cboCommittee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchCommittee = New eZee.Common.eZeeGradientButton
        Me.lblDisciAction = New System.Windows.Forms.Label
        Me.cboDisciplinaryAction = New System.Windows.Forms.ComboBox
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhMembers = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCompany = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhContactNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhcommitteetranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDisciplineMembersTranUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchDisciplinaryAction = New eZee.Common.eZeeGradientButton
        Me.objstLine1 = New eZee.Common.eZeeStraightLine
        Me.lblCountStatus = New System.Windows.Forms.Label
        Me.tabcProceedings = New System.Windows.Forms.TabControl
        Me.tabpProceedingsDetails = New System.Windows.Forms.TabPage
        Me.txtProceedingDetials = New System.Windows.Forms.TextBox
        Me.tabpRemarksComments = New System.Windows.Forms.TabPage
        Me.txtRemarkCommnts = New System.Windows.Forms.TextBox
        Me.tabpPersonInvolvedComments = New System.Windows.Forms.TabPage
        Me.txtPersonInvolvedComments = New System.Windows.Forms.TextBox
        Me.cboCountStatus = New System.Windows.Forms.ComboBox
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnScanAttachDoc = New eZee.Common.eZeeLightButton(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.gbProceedingsInformation.SuspendLayout()
        Me.tabcDescription_Response.SuspendLayout()
        Me.tabpChargeDescription.SuspendLayout()
        Me.tabpChargeCount.SuspendLayout()
        Me.tabpCountProceeding.SuspendLayout()
        Me.pnlDetails.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabcProceedings.SuspendLayout()
        Me.tabpProceedingsDetails.SuspendLayout()
        Me.tabpRemarksComments.SuspendLayout()
        Me.tabpPersonInvolvedComments.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbProceedingsInformation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(902, 558)
        Me.pnlMain.TabIndex = 0
        '
        'gbProceedingsInformation
        '
        Me.gbProceedingsInformation.BorderColor = System.Drawing.Color.Black
        Me.gbProceedingsInformation.Checked = False
        Me.gbProceedingsInformation.CollapseAllExceptThis = False
        Me.gbProceedingsInformation.CollapsedHoverImage = Nothing
        Me.gbProceedingsInformation.CollapsedNormalImage = Nothing
        Me.gbProceedingsInformation.CollapsedPressedImage = Nothing
        Me.gbProceedingsInformation.CollapseOnLoad = False
        Me.gbProceedingsInformation.Controls.Add(Me.tabcDescription_Response)
        Me.gbProceedingsInformation.Controls.Add(Me.pnlDetails)
        Me.gbProceedingsInformation.Controls.Add(Me.objLine1)
        Me.gbProceedingsInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbProceedingsInformation.ExpandedHoverImage = Nothing
        Me.gbProceedingsInformation.ExpandedNormalImage = Nothing
        Me.gbProceedingsInformation.ExpandedPressedImage = Nothing
        Me.gbProceedingsInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProceedingsInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProceedingsInformation.HeaderHeight = 25
        Me.gbProceedingsInformation.HeaderMessage = ""
        Me.gbProceedingsInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbProceedingsInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProceedingsInformation.HeightOnCollapse = 0
        Me.gbProceedingsInformation.LeftTextSpace = 0
        Me.gbProceedingsInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbProceedingsInformation.Name = "gbProceedingsInformation"
        Me.gbProceedingsInformation.OpenHeight = 300
        Me.gbProceedingsInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProceedingsInformation.ShowBorder = True
        Me.gbProceedingsInformation.ShowCheckBox = False
        Me.gbProceedingsInformation.ShowCollapseButton = False
        Me.gbProceedingsInformation.ShowDefaultBorderColor = True
        Me.gbProceedingsInformation.ShowDownButton = False
        Me.gbProceedingsInformation.ShowHeader = True
        Me.gbProceedingsInformation.Size = New System.Drawing.Size(902, 508)
        Me.gbProceedingsInformation.TabIndex = 179
        Me.gbProceedingsInformation.Temp = 0
        Me.gbProceedingsInformation.Text = "Proceedings Information"
        Me.gbProceedingsInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcDescription_Response
        '
        Me.tabcDescription_Response.Controls.Add(Me.tabpChargeDescription)
        Me.tabcDescription_Response.Controls.Add(Me.tabpChargeCount)
        Me.tabcDescription_Response.Controls.Add(Me.tabpCountProceeding)
        Me.tabcDescription_Response.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcDescription_Response.Location = New System.Drawing.Point(6, 29)
        Me.tabcDescription_Response.Name = "tabcDescription_Response"
        Me.tabcDescription_Response.SelectedIndex = 0
        Me.tabcDescription_Response.Size = New System.Drawing.Size(888, 140)
        Me.tabcDescription_Response.TabIndex = 235
        '
        'tabpChargeDescription
        '
        Me.tabpChargeDescription.Controls.Add(Me.txtChargeDescription)
        Me.tabpChargeDescription.Location = New System.Drawing.Point(4, 22)
        Me.tabpChargeDescription.Name = "tabpChargeDescription"
        Me.tabpChargeDescription.Size = New System.Drawing.Size(880, 114)
        Me.tabpChargeDescription.TabIndex = 0
        Me.tabpChargeDescription.Text = "General Charge Description"
        Me.tabpChargeDescription.UseVisualStyleBackColor = True
        '
        'txtChargeDescription
        '
        Me.txtChargeDescription.BackColor = System.Drawing.Color.White
        Me.txtChargeDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtChargeDescription.Multiline = True
        Me.txtChargeDescription.Name = "txtChargeDescription"
        Me.txtChargeDescription.ReadOnly = True
        Me.txtChargeDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtChargeDescription.Size = New System.Drawing.Size(880, 114)
        Me.txtChargeDescription.TabIndex = 119
        '
        'tabpChargeCount
        '
        Me.tabpChargeCount.Controls.Add(Me.lvChargesCount)
        Me.tabpChargeCount.Location = New System.Drawing.Point(4, 22)
        Me.tabpChargeCount.Name = "tabpChargeCount"
        Me.tabpChargeCount.Size = New System.Drawing.Size(880, 114)
        Me.tabpChargeCount.TabIndex = 1
        Me.tabpChargeCount.Text = "Charge Count(s)"
        Me.tabpChargeCount.UseVisualStyleBackColor = True
        '
        'lvChargesCount
        '
        Me.lvChargesCount.BackColorOnChecked = True
        Me.lvChargesCount.ColumnHeaders = Nothing
        Me.lvChargesCount.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCount, Me.colhIncident, Me.colhOffCategory, Me.colhOffence, Me.colhSeverity, Me.objcolhoffencecategoryunkid, Me.objcolhoffenceunkid, Me.objcolhGUID, Me.objcolhfileunkid})
        Me.lvChargesCount.CompulsoryColumns = ""
        Me.lvChargesCount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvChargesCount.FullRowSelect = True
        Me.lvChargesCount.GridLines = True
        Me.lvChargesCount.GroupingColumn = Nothing
        Me.lvChargesCount.HideSelection = False
        Me.lvChargesCount.Location = New System.Drawing.Point(0, 0)
        Me.lvChargesCount.MinColumnWidth = 50
        Me.lvChargesCount.MultiSelect = False
        Me.lvChargesCount.Name = "lvChargesCount"
        Me.lvChargesCount.OptionalColumns = ""
        Me.lvChargesCount.ShowItemToolTips = True
        Me.lvChargesCount.ShowMoreItem = False
        Me.lvChargesCount.ShowSaveItem = False
        Me.lvChargesCount.ShowSelectAll = True
        Me.lvChargesCount.ShowSizeAllColumnsToFit = True
        Me.lvChargesCount.Size = New System.Drawing.Size(880, 114)
        Me.lvChargesCount.Sortable = True
        Me.lvChargesCount.TabIndex = 1
        Me.lvChargesCount.UseCompatibleStateImageBehavior = False
        Me.lvChargesCount.View = System.Windows.Forms.View.Details
        '
        'colhCount
        '
        Me.colhCount.Tag = "colhCount"
        Me.colhCount.Text = "Count"
        Me.colhCount.Width = 45
        '
        'colhIncident
        '
        Me.colhIncident.DisplayIndex = 3
        Me.colhIncident.Tag = "colhIncident"
        Me.colhIncident.Text = "Incident"
        Me.colhIncident.Width = 255
        '
        'colhOffCategory
        '
        Me.colhOffCategory.DisplayIndex = 1
        Me.colhOffCategory.Tag = "colhOffCategory"
        Me.colhOffCategory.Text = "Offence Category"
        Me.colhOffCategory.Width = 145
        '
        'colhOffence
        '
        Me.colhOffence.DisplayIndex = 2
        Me.colhOffence.Tag = "colhOffence"
        Me.colhOffence.Text = "Offence Description"
        Me.colhOffence.Width = 195
        '
        'colhSeverity
        '
        Me.colhSeverity.Tag = "colhSeverity"
        Me.colhSeverity.Text = "Severity"
        Me.colhSeverity.Width = 55
        '
        'objcolhoffencecategoryunkid
        '
        Me.objcolhoffencecategoryunkid.Tag = "objcolhoffencecategoryunkid"
        Me.objcolhoffencecategoryunkid.Width = 0
        '
        'objcolhoffenceunkid
        '
        Me.objcolhoffenceunkid.Tag = "objcolhoffenceunkid"
        Me.objcolhoffenceunkid.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Width = 0
        '
        'objcolhfileunkid
        '
        Me.objcolhfileunkid.Tag = "objcolhfileunkid"
        Me.objcolhfileunkid.Width = 0
        '
        'tabpCountProceeding
        '
        Me.tabpCountProceeding.Controls.Add(Me.txtIncidentDesc)
        Me.tabpCountProceeding.Controls.Add(Me.lblIncident)
        Me.tabpCountProceeding.Controls.Add(Me.cboChargesCount)
        Me.tabpCountProceeding.Controls.Add(Me.lblChargesCount)
        Me.tabpCountProceeding.Controls.Add(Me.lblOffenceCategory)
        Me.tabpCountProceeding.Controls.Add(Me.cboDisciplineType)
        Me.tabpCountProceeding.Controls.Add(Me.cboOffenceCategory)
        Me.tabpCountProceeding.Controls.Add(Me.lblDisciplineType)
        Me.tabpCountProceeding.Location = New System.Drawing.Point(4, 22)
        Me.tabpCountProceeding.Name = "tabpCountProceeding"
        Me.tabpCountProceeding.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpCountProceeding.Size = New System.Drawing.Size(880, 114)
        Me.tabpCountProceeding.TabIndex = 2
        Me.tabpCountProceeding.Text = "Incident Deatils"
        Me.tabpCountProceeding.UseVisualStyleBackColor = True
        '
        'txtIncidentDesc
        '
        Me.txtIncidentDesc.BackColor = System.Drawing.Color.White
        Me.txtIncidentDesc.Location = New System.Drawing.Point(458, 43)
        Me.txtIncidentDesc.Multiline = True
        Me.txtIncidentDesc.Name = "txtIncidentDesc"
        Me.txtIncidentDesc.ReadOnly = True
        Me.txtIncidentDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtIncidentDesc.Size = New System.Drawing.Size(414, 48)
        Me.txtIncidentDesc.TabIndex = 233
        '
        'lblIncident
        '
        Me.lblIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncident.Location = New System.Drawing.Point(458, 18)
        Me.lblIncident.Name = "lblIncident"
        Me.lblIncident.Size = New System.Drawing.Size(109, 17)
        Me.lblIncident.TabIndex = 232
        Me.lblIncident.Text = "Incident Description"
        Me.lblIncident.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChargesCount
        '
        Me.cboChargesCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChargesCount.DropDownWidth = 400
        Me.cboChargesCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChargesCount.FormattingEnabled = True
        Me.cboChargesCount.Location = New System.Drawing.Point(120, 16)
        Me.cboChargesCount.Name = "cboChargesCount"
        Me.cboChargesCount.Size = New System.Drawing.Size(332, 21)
        Me.cboChargesCount.TabIndex = 227
        '
        'lblChargesCount
        '
        Me.lblChargesCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChargesCount.Location = New System.Drawing.Point(5, 18)
        Me.lblChargesCount.Name = "lblChargesCount"
        Me.lblChargesCount.Size = New System.Drawing.Size(109, 17)
        Me.lblChargesCount.TabIndex = 226
        Me.lblChargesCount.Text = "Select Count"
        Me.lblChargesCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOffenceCategory
        '
        Me.lblOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffenceCategory.Location = New System.Drawing.Point(5, 45)
        Me.lblOffenceCategory.Name = "lblOffenceCategory"
        Me.lblOffenceCategory.Size = New System.Drawing.Size(109, 17)
        Me.lblOffenceCategory.TabIndex = 228
        Me.lblOffenceCategory.Text = "Offence Category"
        Me.lblOffenceCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDisciplineType
        '
        Me.cboDisciplineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineType.DropDownWidth = 1000
        Me.cboDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineType.FormattingEnabled = True
        Me.cboDisciplineType.Location = New System.Drawing.Point(120, 70)
        Me.cboDisciplineType.Name = "cboDisciplineType"
        Me.cboDisciplineType.Size = New System.Drawing.Size(332, 21)
        Me.cboDisciplineType.TabIndex = 231
        '
        'cboOffenceCategory
        '
        Me.cboOffenceCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOffenceCategory.DropDownWidth = 300
        Me.cboOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOffenceCategory.FormattingEnabled = True
        Me.cboOffenceCategory.Location = New System.Drawing.Point(120, 43)
        Me.cboOffenceCategory.Name = "cboOffenceCategory"
        Me.cboOffenceCategory.Size = New System.Drawing.Size(332, 21)
        Me.cboOffenceCategory.TabIndex = 229
        '
        'lblDisciplineType
        '
        Me.lblDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineType.Location = New System.Drawing.Point(5, 72)
        Me.lblDisciplineType.Name = "lblDisciplineType"
        Me.lblDisciplineType.Size = New System.Drawing.Size(109, 17)
        Me.lblDisciplineType.TabIndex = 230
        Me.lblDisciplineType.Text = "Offence"
        Me.lblDisciplineType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlDetails
        '
        Me.pnlDetails.Controls.Add(Me.dtpPenaltyExpiryDate)
        Me.pnlDetails.Controls.Add(Me.Label2)
        Me.pnlDetails.Controls.Add(Me.dtpPenaltyEffDate)
        Me.pnlDetails.Controls.Add(Me.LblPenaltyEffDate)
        Me.pnlDetails.Controls.Add(Me.lnkAddAttachment)
        Me.pnlDetails.Controls.Add(Me.lnkAddMember)
        Me.pnlDetails.Controls.Add(Me.lblProceedingDate)
        Me.pnlDetails.Controls.Add(Me.dtpProceedingDate)
        Me.pnlDetails.Controls.Add(Me.lblDisciplineCommitte)
        Me.pnlDetails.Controls.Add(Me.cboCommittee)
        Me.pnlDetails.Controls.Add(Me.objbtnSearchCommittee)
        Me.pnlDetails.Controls.Add(Me.lblDisciAction)
        Me.pnlDetails.Controls.Add(Me.cboDisciplinaryAction)
        Me.pnlDetails.Controls.Add(Me.pnlData)
        Me.pnlDetails.Controls.Add(Me.objbtnSearchDisciplinaryAction)
        Me.pnlDetails.Controls.Add(Me.objstLine1)
        Me.pnlDetails.Controls.Add(Me.lblCountStatus)
        Me.pnlDetails.Controls.Add(Me.tabcProceedings)
        Me.pnlDetails.Controls.Add(Me.cboCountStatus)
        Me.pnlDetails.Location = New System.Drawing.Point(6, 180)
        Me.pnlDetails.Name = "pnlDetails"
        Me.pnlDetails.Size = New System.Drawing.Size(884, 323)
        Me.pnlDetails.TabIndex = 233
        '
        'dtpPenaltyExpiryDate
        '
        Me.dtpPenaltyExpiryDate.Checked = False
        Me.dtpPenaltyExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPenaltyExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPenaltyExpiryDate.Location = New System.Drawing.Point(136, 112)
        Me.dtpPenaltyExpiryDate.Name = "dtpPenaltyExpiryDate"
        Me.dtpPenaltyExpiryDate.ShowCheckBox = True
        Me.dtpPenaltyExpiryDate.Size = New System.Drawing.Size(124, 21)
        Me.dtpPenaltyExpiryDate.TabIndex = 230
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 114)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 17)
        Me.Label2.TabIndex = 229
        Me.Label2.Text = "Penalty Expiry Date"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPenaltyEffDate
        '
        Me.dtpPenaltyEffDate.Checked = False
        Me.dtpPenaltyEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPenaltyEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPenaltyEffDate.Location = New System.Drawing.Point(136, 85)
        Me.dtpPenaltyEffDate.Name = "dtpPenaltyEffDate"
        Me.dtpPenaltyEffDate.ShowCheckBox = True
        Me.dtpPenaltyEffDate.Size = New System.Drawing.Size(124, 21)
        Me.dtpPenaltyEffDate.TabIndex = 228
        '
        'LblPenaltyEffDate
        '
        Me.LblPenaltyEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPenaltyEffDate.Location = New System.Drawing.Point(6, 86)
        Me.LblPenaltyEffDate.Name = "LblPenaltyEffDate"
        Me.LblPenaltyEffDate.Size = New System.Drawing.Size(121, 19)
        Me.LblPenaltyEffDate.TabIndex = 227
        Me.LblPenaltyEffDate.Text = "Penalty Effective Date"
        Me.LblPenaltyEffDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAddAttachment
        '
        Me.lnkAddAttachment.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAddAttachment.Location = New System.Drawing.Point(283, 7)
        Me.lnkAddAttachment.Name = "lnkAddAttachment"
        Me.lnkAddAttachment.Size = New System.Drawing.Size(132, 17)
        Me.lnkAddAttachment.TabIndex = 226
        Me.lnkAddAttachment.TabStop = True
        Me.lnkAddAttachment.Text = "Add Attachment"
        Me.lnkAddAttachment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lnkAddMember
        '
        Me.lnkAddMember.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAddMember.Location = New System.Drawing.Point(439, 7)
        Me.lnkAddMember.Name = "lnkAddMember"
        Me.lnkAddMember.Size = New System.Drawing.Size(83, 17)
        Me.lnkAddMember.TabIndex = 225
        Me.lnkAddMember.TabStop = True
        Me.lnkAddMember.Text = "Add New"
        Me.lnkAddMember.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProceedingDate
        '
        Me.lblProceedingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProceedingDate.Location = New System.Drawing.Point(6, 5)
        Me.lblProceedingDate.Name = "lblProceedingDate"
        Me.lblProceedingDate.Size = New System.Drawing.Size(103, 17)
        Me.lblProceedingDate.TabIndex = 188
        Me.lblProceedingDate.Text = "Proceeding Date"
        Me.lblProceedingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpProceedingDate
        '
        Me.dtpProceedingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProceedingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProceedingDate.Location = New System.Drawing.Point(136, 3)
        Me.dtpProceedingDate.Name = "dtpProceedingDate"
        Me.dtpProceedingDate.Size = New System.Drawing.Size(124, 21)
        Me.dtpProceedingDate.TabIndex = 189
        '
        'lblDisciplineCommitte
        '
        Me.lblDisciplineCommitte.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineCommitte.Location = New System.Drawing.Point(6, 32)
        Me.lblDisciplineCommitte.Name = "lblDisciplineCommitte"
        Me.lblDisciplineCommitte.Size = New System.Drawing.Size(103, 17)
        Me.lblDisciplineCommitte.TabIndex = 211
        Me.lblDisciplineCommitte.Text = "Committee"
        Me.lblDisciplineCommitte.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCommittee
        '
        Me.cboCommittee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommittee.DropDownWidth = 400
        Me.cboCommittee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCommittee.FormattingEnabled = True
        Me.cboCommittee.Location = New System.Drawing.Point(136, 30)
        Me.cboCommittee.Name = "cboCommittee"
        Me.cboCommittee.Size = New System.Drawing.Size(256, 21)
        Me.cboCommittee.TabIndex = 212
        '
        'objbtnSearchCommittee
        '
        Me.objbtnSearchCommittee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCommittee.BorderSelected = False
        Me.objbtnSearchCommittee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCommittee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCommittee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCommittee.Location = New System.Drawing.Point(398, 30)
        Me.objbtnSearchCommittee.Name = "objbtnSearchCommittee"
        Me.objbtnSearchCommittee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCommittee.TabIndex = 214
        '
        'lblDisciAction
        '
        Me.lblDisciAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciAction.Location = New System.Drawing.Point(6, 59)
        Me.lblDisciAction.Name = "lblDisciAction"
        Me.lblDisciAction.Size = New System.Drawing.Size(103, 17)
        Me.lblDisciAction.TabIndex = 215
        Me.lblDisciAction.Text = "Disciplinary Action"
        Me.lblDisciAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDisciplinaryAction
        '
        Me.cboDisciplinaryAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplinaryAction.DropDownWidth = 400
        Me.cboDisciplinaryAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplinaryAction.FormattingEnabled = True
        Me.cboDisciplinaryAction.Location = New System.Drawing.Point(136, 57)
        Me.cboDisciplinaryAction.Name = "cboDisciplinaryAction"
        Me.cboDisciplinaryAction.Size = New System.Drawing.Size(256, 21)
        Me.cboDisciplinaryAction.TabIndex = 216
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(438, 30)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(438, 285)
        Me.pnlData.TabIndex = 224
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhAdd, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhMembers, Me.dgcolhDepartment, Me.dgcolhCompany, Me.dgcolhContactNo, Me.dgcolhEmail, Me.objdgcolhcommitteetranunkid, Me.objdgcolhDisciplineMembersTranUnkid, Me.objdgcolhGUID})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.ReadOnly = True
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(438, 285)
        Me.dgvData.TabIndex = 223
        '
        'objdgcolhAdd
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle7.NullValue = Nothing
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhAdd.DefaultCellStyle = DataGridViewCellStyle7
        Me.objdgcolhAdd.Frozen = True
        Me.objdgcolhAdd.HeaderText = ""
        Me.objdgcolhAdd.Name = "objdgcolhAdd"
        Me.objdgcolhAdd.ReadOnly = True
        Me.objdgcolhAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAdd.Visible = False
        Me.objdgcolhAdd.Width = 25
        '
        'objdgcolhEdit
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle8.NullValue = Nothing
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhEdit.DefaultCellStyle = DataGridViewCellStyle8
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle9.NullValue = Nothing
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhDelete.DefaultCellStyle = DataGridViewCellStyle9
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhMembers
        '
        Me.dgcolhMembers.HeaderText = "Members"
        Me.dgcolhMembers.Name = "dgcolhMembers"
        Me.dgcolhMembers.ReadOnly = True
        '
        'dgcolhDepartment
        '
        Me.dgcolhDepartment.HeaderText = "Department"
        Me.dgcolhDepartment.Name = "dgcolhDepartment"
        Me.dgcolhDepartment.ReadOnly = True
        '
        'dgcolhCompany
        '
        Me.dgcolhCompany.HeaderText = "Company"
        Me.dgcolhCompany.Name = "dgcolhCompany"
        Me.dgcolhCompany.ReadOnly = True
        '
        'dgcolhContactNo
        '
        Me.dgcolhContactNo.HeaderText = "Contact No"
        Me.dgcolhContactNo.Name = "dgcolhContactNo"
        Me.dgcolhContactNo.ReadOnly = True
        '
        'dgcolhEmail
        '
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.ReadOnly = True
        '
        'objdgcolhcommitteetranunkid
        '
        Me.objdgcolhcommitteetranunkid.HeaderText = "objdgcolhcommitteetranunkid"
        Me.objdgcolhcommitteetranunkid.Name = "objdgcolhcommitteetranunkid"
        Me.objdgcolhcommitteetranunkid.ReadOnly = True
        Me.objdgcolhcommitteetranunkid.Visible = False
        '
        'objdgcolhDisciplineMembersTranUnkid
        '
        Me.objdgcolhDisciplineMembersTranUnkid.HeaderText = "objdgcolhDisciplineMembersTranUnkid"
        Me.objdgcolhDisciplineMembersTranUnkid.Name = "objdgcolhDisciplineMembersTranUnkid"
        Me.objdgcolhDisciplineMembersTranUnkid.ReadOnly = True
        Me.objdgcolhDisciplineMembersTranUnkid.Visible = False
        '
        'objdgcolhGUID
        '
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.ReadOnly = True
        Me.objdgcolhGUID.Visible = False
        '
        'objbtnSearchDisciplinaryAction
        '
        Me.objbtnSearchDisciplinaryAction.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDisciplinaryAction.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDisciplinaryAction.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDisciplinaryAction.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDisciplinaryAction.BorderSelected = False
        Me.objbtnSearchDisciplinaryAction.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDisciplinaryAction.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDisciplinaryAction.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDisciplinaryAction.Location = New System.Drawing.Point(398, 57)
        Me.objbtnSearchDisciplinaryAction.Name = "objbtnSearchDisciplinaryAction"
        Me.objbtnSearchDisciplinaryAction.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDisciplinaryAction.TabIndex = 217
        '
        'objstLine1
        '
        Me.objstLine1.BackColor = System.Drawing.Color.Transparent
        Me.objstLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.objstLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstLine1.Location = New System.Drawing.Point(425, 3)
        Me.objstLine1.Name = "objstLine1"
        Me.objstLine1.Size = New System.Drawing.Size(7, 293)
        Me.objstLine1.TabIndex = 222
        '
        'lblCountStatus
        '
        Me.lblCountStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountStatus.Location = New System.Drawing.Point(6, 141)
        Me.lblCountStatus.Name = "lblCountStatus"
        Me.lblCountStatus.Size = New System.Drawing.Size(103, 17)
        Me.lblCountStatus.TabIndex = 218
        Me.lblCountStatus.Text = "Count Status"
        Me.lblCountStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcProceedings
        '
        Me.tabcProceedings.Controls.Add(Me.tabpProceedingsDetails)
        Me.tabcProceedings.Controls.Add(Me.tabpRemarksComments)
        Me.tabcProceedings.Controls.Add(Me.tabpPersonInvolvedComments)
        Me.tabcProceedings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcProceedings.Location = New System.Drawing.Point(5, 168)
        Me.tabcProceedings.Name = "tabcProceedings"
        Me.tabcProceedings.SelectedIndex = 0
        Me.tabcProceedings.Size = New System.Drawing.Size(414, 146)
        Me.tabcProceedings.TabIndex = 220
        '
        'tabpProceedingsDetails
        '
        Me.tabpProceedingsDetails.Controls.Add(Me.txtProceedingDetials)
        Me.tabpProceedingsDetails.Location = New System.Drawing.Point(4, 22)
        Me.tabpProceedingsDetails.Name = "tabpProceedingsDetails"
        Me.tabpProceedingsDetails.Size = New System.Drawing.Size(406, 120)
        Me.tabpProceedingsDetails.TabIndex = 0
        Me.tabpProceedingsDetails.Text = "Proceedings Details"
        Me.tabpProceedingsDetails.UseVisualStyleBackColor = True
        '
        'txtProceedingDetials
        '
        Me.txtProceedingDetials.BackColor = System.Drawing.Color.White
        Me.txtProceedingDetials.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtProceedingDetials.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProceedingDetials.Location = New System.Drawing.Point(0, 0)
        Me.txtProceedingDetials.Multiline = True
        Me.txtProceedingDetials.Name = "txtProceedingDetials"
        Me.txtProceedingDetials.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtProceedingDetials.Size = New System.Drawing.Size(406, 120)
        Me.txtProceedingDetials.TabIndex = 120
        '
        'tabpRemarksComments
        '
        Me.tabpRemarksComments.Controls.Add(Me.txtRemarkCommnts)
        Me.tabpRemarksComments.Location = New System.Drawing.Point(4, 22)
        Me.tabpRemarksComments.Name = "tabpRemarksComments"
        Me.tabpRemarksComments.Size = New System.Drawing.Size(406, 120)
        Me.tabpRemarksComments.TabIndex = 1
        Me.tabpRemarksComments.Text = "Remarks/Comments"
        Me.tabpRemarksComments.UseVisualStyleBackColor = True
        '
        'txtRemarkCommnts
        '
        Me.txtRemarkCommnts.BackColor = System.Drawing.Color.White
        Me.txtRemarkCommnts.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemarkCommnts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarkCommnts.Location = New System.Drawing.Point(0, 0)
        Me.txtRemarkCommnts.Multiline = True
        Me.txtRemarkCommnts.Name = "txtRemarkCommnts"
        Me.txtRemarkCommnts.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarkCommnts.Size = New System.Drawing.Size(406, 120)
        Me.txtRemarkCommnts.TabIndex = 121
        '
        'tabpPersonInvolvedComments
        '
        Me.tabpPersonInvolvedComments.Controls.Add(Me.txtPersonInvolvedComments)
        Me.tabpPersonInvolvedComments.Location = New System.Drawing.Point(4, 22)
        Me.tabpPersonInvolvedComments.Name = "tabpPersonInvolvedComments"
        Me.tabpPersonInvolvedComments.Size = New System.Drawing.Size(406, 120)
        Me.tabpPersonInvolvedComments.TabIndex = 2
        Me.tabpPersonInvolvedComments.Text = "Person Involved Comments"
        Me.tabpPersonInvolvedComments.UseVisualStyleBackColor = True
        '
        'txtPersonInvolvedComments
        '
        Me.txtPersonInvolvedComments.BackColor = System.Drawing.Color.White
        Me.txtPersonInvolvedComments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtPersonInvolvedComments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPersonInvolvedComments.Location = New System.Drawing.Point(0, 0)
        Me.txtPersonInvolvedComments.Multiline = True
        Me.txtPersonInvolvedComments.Name = "txtPersonInvolvedComments"
        Me.txtPersonInvolvedComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPersonInvolvedComments.Size = New System.Drawing.Size(406, 120)
        Me.txtPersonInvolvedComments.TabIndex = 121
        '
        'cboCountStatus
        '
        Me.cboCountStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountStatus.DropDownWidth = 400
        Me.cboCountStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountStatus.FormattingEnabled = True
        Me.cboCountStatus.Location = New System.Drawing.Point(136, 139)
        Me.cboCountStatus.Name = "cboCountStatus"
        Me.cboCountStatus.Size = New System.Drawing.Size(256, 21)
        Me.cboCountStatus.TabIndex = 219
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(8, 172)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(882, 5)
        Me.objLine1.TabIndex = 179
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOk)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnScanAttachDoc)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 508)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(902, 50)
        Me.objFooter.TabIndex = 87
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(690, 11)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 15
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(793, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnScanAttachDoc
        '
        Me.btnScanAttachDoc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnScanAttachDoc.BackColor = System.Drawing.Color.White
        Me.btnScanAttachDoc.BackgroundImage = CType(resources.GetObject("btnScanAttachDoc.BackgroundImage"), System.Drawing.Image)
        Me.btnScanAttachDoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnScanAttachDoc.BorderColor = System.Drawing.Color.Empty
        Me.btnScanAttachDoc.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnScanAttachDoc.FlatAppearance.BorderSize = 0
        Me.btnScanAttachDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnScanAttachDoc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScanAttachDoc.ForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnScanAttachDoc.GradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScanAttachDoc.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.Location = New System.Drawing.Point(734, 21)
        Me.btnScanAttachDoc.Name = "btnScanAttachDoc"
        Me.btnScanAttachDoc.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScanAttachDoc.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.Size = New System.Drawing.Size(10, 10)
        Me.btnScanAttachDoc.TabIndex = 17
        Me.btnScanAttachDoc.Text = "Scan/Attach &Document"
        Me.btnScanAttachDoc.UseVisualStyleBackColor = True
        Me.btnScanAttachDoc.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Members"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Department"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Company"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Contact No"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhcommitteetranunkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhDisciplineMembersTranUnkid"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'frmChargeProceedingDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 558)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChargeProceedingDetails"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Charge Count Proceeding Details"
        Me.pnlMain.ResumeLayout(False)
        Me.gbProceedingsInformation.ResumeLayout(False)
        Me.tabcDescription_Response.ResumeLayout(False)
        Me.tabpChargeDescription.ResumeLayout(False)
        Me.tabpChargeDescription.PerformLayout()
        Me.tabpChargeCount.ResumeLayout(False)
        Me.tabpCountProceeding.ResumeLayout(False)
        Me.tabpCountProceeding.PerformLayout()
        Me.pnlDetails.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabcProceedings.ResumeLayout(False)
        Me.tabpProceedingsDetails.ResumeLayout(False)
        Me.tabpProceedingsDetails.PerformLayout()
        Me.tabpRemarksComments.ResumeLayout(False)
        Me.tabpRemarksComments.PerformLayout()
        Me.tabpPersonInvolvedComments.ResumeLayout(False)
        Me.tabpPersonInvolvedComments.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnScanAttachDoc As eZee.Common.eZeeLightButton
    Friend WithEvents gbProceedingsInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpProceedingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblProceedingDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCommittee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCommittee As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplineCommitte As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDisciplinaryAction As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDisciplinaryAction As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciAction As System.Windows.Forms.Label
    Friend WithEvents cboCountStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountStatus As System.Windows.Forms.Label
    Friend WithEvents tabcProceedings As System.Windows.Forms.TabControl
    Friend WithEvents tabpProceedingsDetails As System.Windows.Forms.TabPage
    Friend WithEvents txtProceedingDetials As System.Windows.Forms.TextBox
    Friend WithEvents tabpRemarksComments As System.Windows.Forms.TabPage
    Friend WithEvents txtRemarkCommnts As System.Windows.Forms.TextBox
    Friend WithEvents tabpPersonInvolvedComments As System.Windows.Forms.TabPage
    Friend WithEvents txtPersonInvolvedComments As System.Windows.Forms.TextBox
    Friend WithEvents objstLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents cboChargesCount As System.Windows.Forms.ComboBox
    Friend WithEvents lblChargesCount As System.Windows.Forms.Label
    Friend WithEvents cboOffenceCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblOffenceCategory As System.Windows.Forms.Label
    Friend WithEvents cboDisciplineType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplineType As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhMembers As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCompany As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhContactNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhcommitteetranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDisciplineMembersTranUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAddMember As System.Windows.Forms.LinkLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents pnlDetails As System.Windows.Forms.Panel
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tabcDescription_Response As System.Windows.Forms.TabControl
    Friend WithEvents tabpChargeDescription As System.Windows.Forms.TabPage
    Friend WithEvents txtChargeDescription As System.Windows.Forms.TextBox
    Friend WithEvents tabpChargeCount As System.Windows.Forms.TabPage
    Friend WithEvents lvChargesCount As eZee.Common.eZeeListView
    Friend WithEvents colhCount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIncident As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOffCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOffence As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSeverity As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhoffencecategoryunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhoffenceunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhfileunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents tabpCountProceeding As System.Windows.Forms.TabPage
    Friend WithEvents txtIncidentDesc As System.Windows.Forms.TextBox
    Friend WithEvents lblIncident As System.Windows.Forms.Label
    Friend WithEvents lnkAddAttachment As System.Windows.Forms.LinkLabel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpPenaltyEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblPenaltyEffDate As System.Windows.Forms.Label
    Friend WithEvents dtpPenaltyExpiryDate As System.Windows.Forms.DateTimePicker
End Class
