﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMedicalFormList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMedicalFormList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvInjuryList = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhYear = New System.Windows.Forms.ColumnHeader
        Me.colhClaimNo = New System.Windows.Forms.ColumnHeader
        Me.colhClaimDate = New System.Windows.Forms.ColumnHeader
        Me.colhServiceNo = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblRemark = New System.Windows.Forms.Label
        Me.dtpClaimFromDate = New System.Windows.Forms.DateTimePicker
        Me.txtClaimNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblClaimFromDate = New System.Windows.Forms.Label
        Me.txtServiceNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblClaimNo = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblServiceNo = New System.Windows.Forms.Label
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.lblYears = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.dtpClaimTodate = New System.Windows.Forms.DateTimePicker
        Me.lblClaimTodate = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvInjuryList)
        Me.pnlMainInfo.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(844, 517)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lvInjuryList
        '
        Me.lvInjuryList.BackColorOnChecked = True
        Me.lvInjuryList.ColumnHeaders = Nothing
        Me.lvInjuryList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhYear, Me.colhClaimNo, Me.colhClaimDate, Me.colhServiceNo, Me.colhRemark})
        Me.lvInjuryList.CompulsoryColumns = ""
        Me.lvInjuryList.FullRowSelect = True
        Me.lvInjuryList.GridLines = True
        Me.lvInjuryList.GroupingColumn = Nothing
        Me.lvInjuryList.HideSelection = False
        Me.lvInjuryList.Location = New System.Drawing.Point(12, 188)
        Me.lvInjuryList.MinColumnWidth = 50
        Me.lvInjuryList.MultiSelect = False
        Me.lvInjuryList.Name = "lvInjuryList"
        Me.lvInjuryList.OptionalColumns = ""
        Me.lvInjuryList.ShowMoreItem = False
        Me.lvInjuryList.ShowSaveItem = False
        Me.lvInjuryList.ShowSelectAll = True
        Me.lvInjuryList.ShowSizeAllColumnsToFit = True
        Me.lvInjuryList.Size = New System.Drawing.Size(822, 267)
        Me.lvInjuryList.Sortable = True
        Me.lvInjuryList.TabIndex = 5
        Me.lvInjuryList.UseCompatibleStateImageBehavior = False
        Me.lvInjuryList.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 200
        '
        'colhYear
        '
        Me.colhYear.Tag = "colhYear"
        Me.colhYear.Text = "Year"
        Me.colhYear.Width = 100
        '
        'colhClaimNo
        '
        Me.colhClaimNo.Tag = "colhClaimNo"
        Me.colhClaimNo.Text = "Claim No."
        Me.colhClaimNo.Width = 100
        '
        'colhClaimDate
        '
        Me.colhClaimDate.Tag = "colhClaimDate"
        Me.colhClaimDate.Text = "Claim Date"
        Me.colhClaimDate.Width = 100
        '
        'colhServiceNo
        '
        Me.colhServiceNo.Tag = "colhServiceNo"
        Me.colhServiceNo.Text = "Service No"
        Me.colhServiceNo.Width = 100
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 215
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dtpClaimTodate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblClaimTodate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblRemark)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dtpClaimFromDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtClaimNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtRemark)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblClaimFromDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtServiceNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblClaimNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboYear)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblServiceNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.EZeeStraightLine2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblYears)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnReset)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearch)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployee)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 66)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 91
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(822, 115)
        Me.EZeeCollapsibleContainer1.TabIndex = 4
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(492, 63)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(72, 15)
        Me.lblRemark.TabIndex = 202
        Me.lblRemark.Text = "Remark"
        '
        'dtpClaimFromDate
        '
        Me.dtpClaimFromDate.Checked = False
        Me.dtpClaimFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpClaimFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpClaimFromDate.Location = New System.Drawing.Point(373, 60)
        Me.dtpClaimFromDate.Name = "dtpClaimFromDate"
        Me.dtpClaimFromDate.ShowCheckBox = True
        Me.dtpClaimFromDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpClaimFromDate.TabIndex = 5
        '
        'txtClaimNo
        '
        Me.txtClaimNo.Flags = 0
        Me.txtClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClaimNo.Location = New System.Drawing.Point(373, 33)
        Me.txtClaimNo.Name = "txtClaimNo"
        Me.txtClaimNo.Size = New System.Drawing.Size(101, 21)
        Me.txtClaimNo.TabIndex = 4
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(569, 60)
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(239, 21)
        Me.txtRemark.TabIndex = 7
        '
        'lblClaimFromDate
        '
        Me.lblClaimFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimFromDate.Location = New System.Drawing.Point(269, 63)
        Me.lblClaimFromDate.Name = "lblClaimFromDate"
        Me.lblClaimFromDate.Size = New System.Drawing.Size(94, 15)
        Me.lblClaimFromDate.TabIndex = 201
        Me.lblClaimFromDate.Text = "Claim From Date"
        '
        'txtServiceNo
        '
        Me.txtServiceNo.Flags = 0
        Me.txtServiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServiceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtServiceNo.Location = New System.Drawing.Point(569, 33)
        Me.txtServiceNo.Name = "txtServiceNo"
        Me.txtServiceNo.Size = New System.Drawing.Size(239, 21)
        Me.txtServiceNo.TabIndex = 6
        '
        'lblClaimNo
        '
        Me.lblClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimNo.Location = New System.Drawing.Point(269, 36)
        Me.lblClaimNo.Name = "lblClaimNo"
        Me.lblClaimNo.Size = New System.Drawing.Size(94, 15)
        Me.lblClaimNo.TabIndex = 200
        Me.lblClaimNo.Text = "Claim No"
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 150
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(88, 60)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(133, 21)
        Me.cboYear.TabIndex = 2
        '
        'lblServiceNo
        '
        Me.lblServiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServiceNo.Location = New System.Drawing.Point(491, 36)
        Me.lblServiceNo.Name = "lblServiceNo"
        Me.lblServiceNo.Size = New System.Drawing.Size(73, 15)
        Me.lblServiceNo.TabIndex = 203
        Me.lblServiceNo.Text = "Service No"
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(254, 24)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(9, 92)
        Me.EZeeStraightLine2.TabIndex = 88
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'lblYears
        '
        Me.lblYears.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYears.Location = New System.Drawing.Point(8, 63)
        Me.lblYears.Name = "lblYears"
        Me.lblYears.Size = New System.Drawing.Size(74, 15)
        Me.lblYears.TabIndex = 88
        Me.lblYears.Text = "Year"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(795, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(227, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 86
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(772, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 150
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(88, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(133, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 462)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(844, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(643, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 72
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(546, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 71
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(450, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 70
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(739, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(844, 60)
        Me.eZeeHeader.TabIndex = 1
        Me.eZeeHeader.Title = "Employee Injuries List"
        '
        'dtpClaimTodate
        '
        Me.dtpClaimTodate.Checked = False
        Me.dtpClaimTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpClaimTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpClaimTodate.Location = New System.Drawing.Point(373, 87)
        Me.dtpClaimTodate.Name = "dtpClaimTodate"
        Me.dtpClaimTodate.ShowCheckBox = True
        Me.dtpClaimTodate.Size = New System.Drawing.Size(101, 21)
        Me.dtpClaimTodate.TabIndex = 205
        '
        'lblClaimTodate
        '
        Me.lblClaimTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimTodate.Location = New System.Drawing.Point(269, 90)
        Me.lblClaimTodate.Name = "lblClaimTodate"
        Me.lblClaimTodate.Size = New System.Drawing.Size(94, 15)
        Me.lblClaimTodate.TabIndex = 206
        Me.lblClaimTodate.Text = "Claim To Date"
        '
        'frmMedicalFormList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(844, 517)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMedicalFormList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Injuries List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblYears As System.Windows.Forms.Label
    Friend WithEvents lblServiceNo As System.Windows.Forms.Label
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents lblClaimFromDate As System.Windows.Forms.Label
    Friend WithEvents lblClaimNo As System.Windows.Forms.Label
    Friend WithEvents txtServiceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtClaimNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpClaimFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lvInjuryList As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhYear As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClaimNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClaimDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhServiceNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtpClaimTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblClaimTodate As System.Windows.Forms.Label
End Class
