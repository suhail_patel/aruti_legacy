﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmMedicalMaster_AddEdit
    Dim strItemName As String

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmMedicalMaster_AddEdit"
    Private mblnCancel As Boolean = True
    Private objMedicalMaster As clsmedical_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMedicalMasterUnkid As Integer = -1
    Private mintMasterTypeId As Integer = -1
    Dim wkMins As Double

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal MasterTypeId As Integer) As Boolean
        Try
            mintMedicalMasterUnkid = intUnkId
            mintMasterTypeId = MasterTypeId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintMedicalMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmMedicalMaster_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objMedicalMaster = New clsmedical_master
        Try

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            Select Case mintMasterTypeId
                'Case CInt(enMedicalMasterType.Category)
                '    Me.Text = "Add/Edit Category"
                '    Me.gbMasterInfo.Text = "Category Info"

                Case CInt(enMedicalMasterType.Medical_Category)

                    'Pinkal (24-Jul-2012) -- Start
                    'Enhancement : TRA Changes
                    Me.Name = "frmMedicalCategory"
                    Me.Text = Language.getMessage(mstrModuleName, 6, "Add/Edit Medical Category")
                    Me.gbMasterInfo.Text = Language.getMessage(mstrModuleName, 7, "Medical Category Info")
                    'Pinkal (24-Jul-2012) -- End


                Case CInt(enMedicalMasterType.Cover)


                    'Pinkal (24-Jul-2012) -- Start
                    'Enhancement : TRA Changes
                    Me.Name = "frmMedicalCover"
                    Me.Text = Language.getMessage(mstrModuleName, 8, "Add/Edit Cover")
                    Me.gbMasterInfo.Text = Language.getMessage(mstrModuleName, 9, "Cover Info")
                    'Pinkal (24-Jul-2012) -- End

                Case CInt(enMedicalMasterType.Treatment)

                    'Pinkal (24-Jul-2012) -- Start
                    'Enhancement : TRA Changes
                    Me.Name = "frmTreatment"
                    Me.Text = Language.getMessage(mstrModuleName, 10, "Add/Edit Treatment")
                    Me.gbMasterInfo.Text = Language.getMessage(mstrModuleName, 11, "Treatment Info")
                    'Pinkal (24-Jul-2012) -- End

                Case CInt(enMedicalMasterType.Service)

                    'Pinkal (24-Jul-2012) -- Start
                    'Enhancement : TRA Changes

                    Me.Name = "frmService"
                    Me.Text = Language.getMessage(mstrModuleName, 12, "Add/Edit Service")
                    Me.gbMasterInfo.Text = Language.getMessage(mstrModuleName, 13, "Service Info")

                    'Pinkal (24-Jul-2012) -- End

                    Me.Size = CType(New Point(412, 311), Drawing.Size)
                    Me.gbMasterInfo.Size = CType(New Point(382, 212), Drawing.Size)
            End Select

            'Pinkal (12-Oct-2011) -- End

            Call Set_Logo(Me, gApplicationType)

            setColor()
            If menAction = enAction.EDIT_ONE Then
                objMedicalMaster._Mdmasterunkid = mintMedicalMasterUnkid
            End If
            GetValue()
            txtCode.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalMaster_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalMaster_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalMaster_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalMaster_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMedicalMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim strName As String = String.Empty
        Try


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            'If mintMasterTypeId = CInt(enMedicalMasterType.Category) Then
            '    strName = enMedicalMasterType.Category.ToString()
            If mintMasterTypeId = CInt(enMedicalMasterType.Medical_Category) Then
                strName = enMedicalMasterType.Medical_Category.ToString().Replace("_", " ")
            ElseIf mintMasterTypeId = CInt(enMedicalMasterType.Cover) Then
                strName = enMedicalMasterType.Cover.ToString()
            ElseIf mintMasterTypeId = CInt(enMedicalMasterType.Service) Then
                strName = enMedicalMasterType.Service.ToString()
            ElseIf mintMasterTypeId = CInt(enMedicalMasterType.Treatment) Then
                strName = enMedicalMasterType.Treatment.ToString()
            End If

            'Pinkal (12-Oct-2011) -- End

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(strName & Language.getMessage(mstrModuleName, 1, " Code cannot be blank.") & strName & Language.getMessage(mstrModuleName, 2, " Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(strName & Language.getMessage(mstrModuleName, 3, " Name cannot be blank.") & strName & Language.getMessage(mstrModuleName, 4, " Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub

            ElseIf mintMasterTypeId = CInt(enMedicalMasterType.Service) And txtServiceNo.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Service Provider No cannot be blank.Service Provider No is required information."), enMsgBoxStyle.Information)
                txtServiceNo.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objMedicalMaster.Update()
            Else
                blnFlag = objMedicalMaster.Insert()
            End If

            If blnFlag = False And objMedicalMaster._Message <> "" Then
                eZeeMsgBox.Show(objMedicalMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objMedicalMaster = Nothing
                    objMedicalMaster = New clsmedical_master
                    Call GetValue()
                    txtCode.Select()
                Else
                    mintMedicalMasterUnkid = objMedicalMaster._Mdmasterunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Try
            Dim objFrmLangPopup As New NameLanguagePopup_Form
            Try
                Call objFrmLangPopup.displayDialog(txtName.Text, objMedicalMaster._Mdmastername1, objMedicalMaster._Mdmastername2)
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtAlias.BackColor = GUI.ColorOptional
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional

            If mintMasterTypeId = CInt(enMedicalMasterType.Service) Then
                txtServiceNo.BackColor = GUI.ColorComp
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAlias.Text = objMedicalMaster._Mdmasteralias
            txtCode.Text = objMedicalMaster._Mdmastercode
            txtName.Text = objMedicalMaster._Mdmastername
            txtDescription.Text = objMedicalMaster._Mddescription
            'mintMasterTypeId = objMedicalMaster._Mdmastertype_Id
            If mintMasterTypeId = CInt(enMedicalMasterType.Service) Then
                txtServiceNo.Text = objMedicalMaster._MdServiceno
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objMedicalMaster._Mdmasteralias = txtAlias.Text.Trim
            objMedicalMaster._Mdmastercode = txtCode.Text.Trim
            objMedicalMaster._Mdmastername = txtName.Text.Trim
            objMedicalMaster._Mddescription = txtDescription.Text.Trim
            objMedicalMaster._Mdmastertype_Id = mintMasterTypeId

            If mintMasterTypeId = CInt(enMedicalMasterType.Service) Then
                objMedicalMaster._MdServiceno = txtServiceNo.Text.Trim
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbMasterInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMasterInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMasterInfo.Text = Language._Object.getCaption(Me.gbMasterInfo.Name, Me.gbMasterInfo.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblAlias.Text = Language._Object.getCaption(Me.lblAlias.Name, Me.lblAlias.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblNo.Text = Language._Object.getCaption(Me.lblNo.Name, Me.lblNo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, " Code cannot be blank.")
			Language.setMessage(mstrModuleName, 2, " Code is required information.")
			Language.setMessage(mstrModuleName, 3, " Name cannot be blank.")
			Language.setMessage(mstrModuleName, 4, " Name is required information.")
			Language.setMessage(mstrModuleName, 5, "Service Provider No cannot be blank.Service Provider No is required information.")
			Language.setMessage(mstrModuleName, 6, "Add/Edit Medical Category")
			Language.setMessage(mstrModuleName, 7, "Medical Category Info")
			Language.setMessage(mstrModuleName, 8, "Add/Edit Cover")
			Language.setMessage(mstrModuleName, 9, "Cover Info")
			Language.setMessage(mstrModuleName, 10, "Add/Edit Treatment")
			Language.setMessage(mstrModuleName, 11, "Treatment Info")
			Language.setMessage(mstrModuleName, 12, "Add/Edit Service")
			Language.setMessage(mstrModuleName, 13, "Service Info")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class