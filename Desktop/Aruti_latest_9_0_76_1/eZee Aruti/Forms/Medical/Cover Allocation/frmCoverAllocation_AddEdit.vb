﻿Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 10

Public Class frmCoverAllocation_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmCoverAllocation_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objMedcalCover As clsmedical_cover
    Private mintmedicalcoverunkid As Integer = -1
    Private objExchangeRate As clsExchangeRate
    Private dsEmpList As DataSet = Nothing
    Private mstrSearch As String = ""
    Private mstrEmployeeunkid As String = ""
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintmedicalcoverunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintmedicalcoverunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            'cboEmpCategory.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mstrSearch = "employeeunkid = " & objMedcalCover._Employeeunkid
            mstrSearch = "hremployee_master.employeeunkid = " & objMedcalCover._Employeeunkid
            'Shani(24-Aug-2015) -- End

            cboMedicalCategory.SelectedValue = objMedcalCover._Medicalcategoryunkid
            txtRemark.Text = objMedcalCover._Remark
            If objMedcalCover._StartDate <> Nothing Then
                dtpStartDate.Value = objMedcalCover._StartDate
            End If
            If objMedcalCover._EndDate <> Nothing Then
                dtpEndDate.Value = objMedcalCover._EndDate
                dtpEndDate.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            mstrEmployeeunkid = String.Join(",", lvEmployeeList.CheckedItems.Cast(Of ListViewItem).Select(Function(x) x.Tag.ToString()).ToArray())
            'S.SANDEEP [ 06 OCT 2014 ] -- END

            objMedcalCover._Medicalcategoryunkid = CInt(cboMedicalCategory.SelectedValue)
            objMedcalCover._Remark = txtRemark.Text.Trim
            objMedcalCover._Userunkid = User._Object._Userunkid
            objMedcalCover._StartDate = dtpStartDate.Value
            If dtpEndDate.Checked = True Then
                objMedcalCover._EndDate = dtpEndDate.Value
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim DsFill As DataSet = Nothing
        Try

            Dim objMedicalCategory As New clsmedical_master
            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Medical_Category, "Category", True)
            cboMedicalCategory.DisplayMember = "name"
            cboMedicalCategory.ValueMember = "mdmasterunkid"
            cboMedicalCategory.DataSource = DsFill.Tables("Category")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeList(ByVal dtEmployee As DataTable)
        Try

            If dtEmployee IsNot Nothing AndAlso dtEmployee.Rows.Count > 0 Then

                RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked

                'Pinkal (02-Jan-2020) -- Start
                'Enhancement Finca DRC Medical  [0003056] -   Allow to assign medical cover to inactive staff.
                lvEmployeeList.BeginUpdate()
                'Pinkal (02-Jan-2020) -- End

                lvEmployeeList.Items.Clear()
                Dim lvItem As ListViewItem
                For Each dtRow As DataRow In dtEmployee.Rows
                    lvItem = New ListViewItem
                    lvItem.Tag = CInt(dtRow("employeeunkid").ToString)
                    lvItem.Text = ""
                    'Pinkal (02-Jan-2020) -- Start
                    'Enhancement Finca DRC Medical  [0003056] -   Allow to assign medical cover to inactive staff.
                    'lvItem.SubItems.Add(dtRow("employeecode").ToString)
                    'lvItem.SubItems.Add(dtRow("name").ToString())
                    lvItem.SubItems.Add(dtRow("code").ToString)
                    lvItem.SubItems.Add(dtRow("Employee Name").ToString())
                    'Pinkal (02-Jan-2020) -- End
                    lvEmployeeList.Items.Add(lvItem)
                Next

                'Pinkal (02-Jan-2020) -- Start
                'Enhancement Finca DRC Medical  [0003056] -   Allow to assign medical cover to inactive staff.
                lvEmployeeList.EndUpdate()

                'If lvEmployeeList.Items.Count > 16 Then
                '    colhEmpName.Width = 188 - 18
                'Else
                '    colhEmpName.Width = 188
                'End If

                If lvEmployeeList.Items.Count > 16 Then
                    colhEmpName.Width = 210 - 18
                Else
                    colhEmpName.Width = 210
            End If
                'Pinkal (02-Jan-2020) -- End

                AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Sub SelectAllEmployee(ByVal mblnSelectAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvEmployeeList.Items
                RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
                lvItem.Checked = mblnSelectAll
                AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SelectAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            objbtnAddMedicalCategory.Enabled = User._Object.Privilege._AddMedicalMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCoverAllocation_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMedcalCover = New clsmedical_cover
        objExchangeRate = New clsExchangeRate
        Try
            Call Set_Logo(Me, gApplicationType)
            objExchangeRate._ExchangeRateunkid = 1
            SetVisibility()
            SetColor()
            FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objMedcalCover._Medicalcoverunkid = mintmedicalcoverunkid
                GetValue()
                objbtnSearch_Click(New Object(), New EventArgs())
                objchkSelectAll.Checked = True
                objchkSelectAll.Enabled = False
                lnkAdvanceSearch.Enabled = False
                objbtnSearch.Enabled = False
                lvEmployeeList.Enabled = False
                'Pinkal (02-Jan-2020) -- Start
                'Enhancement Finca DRC Medical  [0003056] -   Allow to assign medical cover to inactive staff.
                objbtnReset.Enabled = False
                'Pinkal (02-Jan-2020) -- End
            End If
            'cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCoverAllocation_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCoverAllocation_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCoverAllocation_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCoverAllocation_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCoverAllocation_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCoverAllocation_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMedcalCover = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsmedical_cover.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_cover"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If lvEmployeeList.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select atleast One Employee."), enMsgBoxStyle.Information)
                lvEmployeeList.Select()
                    Exit Sub

            ElseIf CInt(cboMedicalCategory.SelectedValue) = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee's Medical Category is compulsory information.Please Select Employee's Medical Category."), enMsgBoxStyle.Information)
                cboMedicalCategory.Select()
                    Exit Sub
            End If

            'S.SANDEEP [ 06 OCT 2014 ] -- START
            If dtpEndDate.Checked = True Then
                If dtpEndDate.Value.Date <= dtpStartDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, End Date cannot be less or equal to Start Date."), enMsgBoxStyle.Information)
                    dtpEndDate.Focus()
                    Exit Sub
                End If
                End If
            'S.SANDEEP [ 06 OCT 2014 ] -- END


            SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objMedcalCover.Update(mstrEmployeeunkid)
            Else
                blnFlag = objMedcalCover.Insert(mstrEmployeeunkid)
            End If

            If blnFlag = False And objMedcalCover._Message <> "" Then
                eZeeMsgBox.Show(objMedcalCover._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objMedcalCover = Nothing
                    mstrSearch = ""
                    objMedcalCover = New clsmedical_cover
                    Call GetValue()
                    objchkSelectAll.Checked = False
                    'cboEmployee.Select()
                Else
                    mintmedicalcoverunkid = objMedcalCover._Covermasterunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim dtEmployee As DataTable = Nothing
        Try
            Dim objEmployee As New clsEmployee_Master


            'Pinkal (02-Jan-2020) -- Start
            'Enhancement Finca DRC Medical  [0003056] -   Allow to assign medical cover to inactive staff.

            'dsEmpList = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                "Employee", _
            '                                ConfigParameter._Object._ShowFirstAppointmentDate, , , mstrSearch)


            Dim strfield As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                    "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & _
                                    "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & _
                                    "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center

            dsEmpList = objEmployee.GetListForDynamicField(strfield, FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                                      chkInactiveEmp.Checked, "Employee", _
                                                                                      -1, False, mstrSearch, ConfigParameter._Object._ShowFirstAppointmentDate)

            'Pinkal (02-Jan-2020)  -- End


            dtEmployee = dsEmpList.Tables(0)

            FillEmployeeList(dtEmployee)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            dtEmployee.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddMedicalCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddMedicalCategory.Click
        Dim frm As New frmMedicalMaster_AddEdit
        Dim intRefId As Integer = -1
        Try

            frm.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Medical_Category)

            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objMedMaster As New clsmedical_master
                dsList = objMedMaster.getListForCombo(enMedicalMasterType.Medical_Category, "CAT", True)
                With cboMedicalCategory
                    .ValueMember = "mdmasterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("CAT")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objMedMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddMedicalCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Pinkal (02-Jan-2020) -- Start
    'Enhancement Finca DRC Medical  [0003056] -   Allow to assign medical cover to inactive staff.
    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrSearch = ""
            objbtnSearch_Click(objbtnSearch, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    'Pinkal (02-Jan-2020) -- End

#End Region

#Region "LinkButton Event"

    Private Sub lnkAdvanceSearch_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceSearch.LinkClicked
        Try
            Dim objSearch As New frmAdvanceSearch
            'Pinkal (02-Jan-2020) -- Start
            'Enhancement Finca DRC Medical  [0003056] -   Allow to assign medical cover to inactive staff.
            'objSearch._Hr_EmployeeTable_Alias = "hremployee_master"
            'Pinkal (02-Jan-2020) -- End
            If objSearch.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrSearch = objSearch._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceSearch_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            SelectAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvEmployeeList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployeeList.ItemChecked
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            If lvEmployeeList.CheckedItems.Count <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked

            ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate

            ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbCoverAllocation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCoverAllocation.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEffectiveDate.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEffectiveDate.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbCoverAllocation.Text = Language._Object.getCaption(Me.gbCoverAllocation.Name, Me.gbCoverAllocation.Text)
			Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
			Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
			Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
			Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
			Me.lblMedicalCategory.Text = Language._Object.getCaption(Me.lblMedicalCategory.Name, Me.lblMedicalCategory.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lnkAdvanceSearch.Text = Language._Object.getCaption(Me.lnkAdvanceSearch.Name, Me.lnkAdvanceSearch.Text)
			Me.gbEffectiveDate.Text = Language._Object.getCaption(Me.gbEffectiveDate.Name, Me.gbEffectiveDate.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select atleast One Employee.")
			Language.setMessage(mstrModuleName, 2, "Employee's Medical Category is compulsory information.Please Select Employee's Medical Category.")
			Language.setMessage(mstrModuleName, 3, "Sorry, End Date cannot be less or equal to Start Date.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>


End Class


'#Region " Private Variables "
'Private ReadOnly mstrModuleName As String = "frmCoverAllocation_AddEdit"
'Private mblnCancel As Boolean = True
'Private menAction As enAction = enAction.ADD_ONE
'Private objMedcalCover As clsmedical_cover
'Private mintmedicalcoverunkid As Integer = -1
'Dim objExchangeRate As clsExchangeRate
'#End Region

'#Region " Display Dialog "

'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
'    Try
'        mintmedicalcoverunkid = intUnkId
'        menAction = eAction

'        Me.ShowDialog()

'        intUnkId = mintmedicalcoverunkid

'        Return Not mblnCancel
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'    End Try
'End Function

'#End Region

'#Region " Private Methods "

'Private Sub SetColor()
'    Try
'        cboEmployee.BackColor = GUI.ColorComp
'        cboDependants.BackColor = GUI.ColorComp
'        cboEmpService.BackColor = GUI.ColorComp
'        txtEmpServiceNo.BackColor = GUI.ColorComp
'        cboEmpCategory.BackColor = GUI.ColorComp
'        cboEmpService.BackColor = GUI.ColorComp
'        cboEmpCover.BackColor = GUI.ColorComp
'        txtEmpAmount.BackColor = GUI.ColorOptional
'        txtEmpRemark.BackColor = GUI.ColorOptional

'        If tbMedicalCover.TabPages.IndexOf(tbpDepandentsCover) >= -1 Then

'            cboDependants.BackColor = GUI.ColorComp
'            cboDepService.BackColor = GUI.ColorComp
'            txtDepServiceNo.BackColor = GUI.ColorComp
'            cboDepCategory.BackColor = GUI.ColorComp
'            cboDepService.BackColor = GUI.ColorComp
'            cboDepCover.BackColor = GUI.ColorComp
'            txtDepAmount.BackColor = GUI.ColorOptional
'            txtDepRemarks.BackColor = GUI.ColorOptional

'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'    End Try
'End Sub

'Private Sub GetValue()
'    Try
'        cboEmployee.SelectedValue = objMedcalCover._Employeeunkid

'        ' If objMedcalCover._Employeeunkid > 0 And objMedcalCover._Dependentsunkid < 1 Then
'        If rdEmployee.Checked Then
'            cboEmpCategory.SelectedValue = objMedcalCover._Medicalcategoryunkid
'            cboEmpService.SelectedValue = objMedcalCover._Serviceunkid
'            cboEmpCover.SelectedValue = objMedcalCover._Covermasterunkid
'            txtEmpAmount.Text = Format(objMedcalCover._Amount, objExchangeRate._fmtCurrency)
'            txtEmpRemark.Text = objMedcalCover._Remark

'            '            ElseIf objMedcalCover._Employeeunkid > 0 And objMedcalCover._Dependentsunkid > 0 Then
'        ElseIf rdDependants.Checked Then
'            cboDependants.SelectedValue = objMedcalCover._Dependentsunkid
'            cboDepCategory.SelectedValue = objMedcalCover._Medicalcategoryunkid
'            cboDepService.SelectedValue = objMedcalCover._Serviceunkid
'            cboDepCover.SelectedValue = objMedcalCover._Covermasterunkid
'            txtDepAmount.Text = Format(objMedcalCover._Amount, objExchangeRate._fmtCurrency)
'            txtDepRemarks.Text = objMedcalCover._Remark
'        Else
'            rdEmployee.Checked = True
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'    End Try
'End Sub

'Private Sub SetValue()
'    Try
'        objMedcalCover._Employeeunkid = CInt(cboEmployee.SelectedValue)

'        If rdEmployee.Checked Then
'            'objMedcalCover._Membershiptypeunkid = CInt(cbo.SelectedValue)
'            'objMedcalCover._Membershipno = txtEmpMembershipNo.Text
'            objMedcalCover._Medicalcategoryunkid = CInt(cboEmpCategory.SelectedValue)
'            objMedcalCover._Serviceunkid = CInt(cboEmpService.SelectedValue)
'            objMedcalCover._Covermasterunkid = CInt(cboEmpCover.SelectedValue)

'            'Sandeep [ 09 Oct 2010 ] -- Start
'            'Issues Reported by Vimal
'            'objMedcalCover._Amount =  cdec(txtEmpAmount.Text)
'            'Sohail (17 Jun 2011) -- Start
'            'objMedcalCover._Amount = IIf(txtEmpAmount.Text = "", 0, CDec(txtEmpAmount.Text))
'            objMedcalCover._Amount = txtEmpAmount.Decimal
'            'Sohail (17 Jun 2011) -- End
'            'Sandeep [ 09 Oct 2010 ] -- End 
'            objMedcalCover._Remark = txtEmpRemark.Text

'        ElseIf rdDependants.Checked Then
'            objMedcalCover._Dependentsunkid = CInt(cboDependants.SelectedValue)
'            'objMedcalCover._Membershiptypeunkid = CInt(cboDepMembershipname.SelectedValue)
'            'objMedcalCover._Membershipno = txtDepMembershipNo.Text
'            objMedcalCover._Medicalcategoryunkid = CInt(cboDepCategory.SelectedValue)
'            objMedcalCover._Serviceunkid = CInt(cboDepService.SelectedValue)
'            objMedcalCover._Covermasterunkid = CInt(cboDepCover.SelectedValue)
'            'Sohail (17 Jun 2011) -- Start
'            'objMedcalCover._Amount = CDec(txtDepAmount.Text)
'            objMedcalCover._Amount = txtDepAmount.Decimal
'            'Sohail (17 Jun 2011) -- End
'            objMedcalCover._Remark = txtDepRemarks.Text
'        End If


'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'    End Try
'End Sub

'Private Sub FillCombo()
'    Dim DsFill As DataSet = Nothing
    '    Try

'        'FOR EMPLOYEE 
    '        Dim objEmployee As New clsEmployee_Master

'        'Pinkal (24-Jun-2011) -- Start
'        'ISSUE : CHECK FOR ACTIVE EMPLOYEE
'        'DsFill = objEmployee.GetEmployeeList("Employee", True, True)
'        DsFill = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'        'Pinkal (24-Jun-2011) -- End

'        cboEmployee.DisplayMember = "employeename"
'        cboEmployee.ValueMember = "employeeunkid"
'        cboEmployee.DataSource = DsFill.Tables("Employee")

'        Dim objMedicalCategory As New clsmedical_master
'        If rdEmployee.Checked Then

'            'FOR EMPLOYEE MEDICAL CATEGORY
'            DsFill = Nothing

'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes
'            'DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Category, "Category", True)
'            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Medical_Category, "Category", True)
'            'Pinkal (12-Oct-2011) -- End

'            cboEmpCategory.DisplayMember = "name"
'            cboEmpCategory.ValueMember = "mdmasterunkid"
'            cboEmpCategory.DataSource = DsFill.Tables("Category")


'            'FOR EMPLOYEE MEDICAL SERVICE
'            DsFill = Nothing
'            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Service, "Service", True)
'            cboEmpService.DisplayMember = "name"
'            cboEmpService.ValueMember = "mdmasterunkid"
'            cboEmpService.DataSource = DsFill.Tables("Service")

'            'FOR EMPLOYEE MEDICAL COVER
'            DsFill = Nothing
'            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Cover, "Cover", True)
'            cboEmpCover.DisplayMember = "name"
'            cboEmpCover.ValueMember = "mdmasterunkid"
'            cboEmpCover.DataSource = DsFill.Tables("Cover")
'        End If

'        If rdDependants.Checked Then

'            'FOR DEPENDANTS MEDICAL CATEGORY
'            DsFill = Nothing

'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes
'            'DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Category, "Category", True)
'            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Medical_Category, "Category", True)
'            'Pinkal (12-Oct-2011) -- End

'            cboDepCategory.DisplayMember = "name"
'            cboDepCategory.ValueMember = "mdmasterunkid"
'            cboDepCategory.DataSource = DsFill.Tables("Category")


'            'FOR DEPENDANTS MEDICAL SERVICE
'            DsFill = Nothing
'            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Service, "Service", True)
'            cboDepService.DisplayMember = "name"
'            cboDepService.ValueMember = "mdmasterunkid"
'            cboDepService.DataSource = DsFill.Tables("Service")


'            'FOR DEPENDANTS MEDICAL COVER
'            DsFill = Nothing
'            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Cover, "Cover", True)
'            cboDepCover.DisplayMember = "name"
'            cboDepCover.ValueMember = "mdmasterunkid"
'            cboDepCover.DataSource = DsFill.Tables("Cover")

'        End If


'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'    End Try
'End Sub

'Private Sub SetVisibility()

'    Try
'        objbtnAddDepSerivce.Enabled = User._Object.Privilege._AddMedicalMasters
'        objbtnAddDepCategory.Enabled = User._Object.Privilege._AddMedicalMasters
'        objbtnAddEmpCategory.Enabled = User._Object.Privilege._AddMedicalMasters
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'    End Try

'End Sub

'#End Region

'#Region " Form's Events "

'Private Sub frmCoverAllocation_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'    objMedcalCover = New clsmedical_cover
'    objExchangeRate = New clsExchangeRate
'    Try
'        Call Set_Logo(Me, gApplicationType)
'        objExchangeRate._ExchangeRateunkid = 1
'        SetColor()
'        If menAction = enAction.EDIT_ONE Then
'            objMedcalCover._Medicalcoverunkid = mintmedicalcoverunkid
'        End If

'        If objMedcalCover._Employeeunkid > 0 And objMedcalCover._Dependentsunkid < 1 Then
'            rdEmployee.Checked = True
'            rdEmployee.Enabled = True
'            rdDependants.Enabled = False
'        ElseIf objMedcalCover._Employeeunkid > 0 And objMedcalCover._Dependentsunkid > 0 Then
'            rdDependants.Checked = True
'            rdDependants.Enabled = True
'            rdEmployee.Enabled = False
'        ElseIf objMedcalCover._Employeeunkid = 0 And objMedcalCover._Dependentsunkid = 0 Then
'            rdEmployee.Checked = True
'            rdDependants.Enabled = True
'            rdEmployee.Enabled = True
'        End If

'        'FillCombo()
'        Call SetVisibility()
'        GetValue()
'        cboEmployee.Select()
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "frmCoverAllocation_AddEdit_Load", mstrModuleName)
'    End Try
'End Sub

'Private Sub frmCoverAllocation_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'    Try
'        If Asc(e.KeyChar) = 13 Then
'            SendKeys.Send("{TAB}")
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "frmCoverAllocation_AddEdit_KeyPress", mstrModuleName)
'    End Try
'End Sub

'Private Sub frmCoverAllocation_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
'    Try
'        If e.Control = True And e.KeyCode = Keys.S Then
'            btnSave_Click(sender, e)
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "frmCoverAllocation_AddEdit_KeyDown", mstrModuleName)
'    End Try
'End Sub

'Private Sub frmCoverAllocation_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'    objMedcalCover = Nothing
'End Sub

'#End Region

'#Region " Button's Events "

'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'    Dim blnFlag As Boolean = False
'    Try

'        If rdEmployee.Checked Then
'            If CInt(cboEmployee.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
'                cboEmployee.Select()
'                Exit Sub
'                'ElseIf CInt(cboEmpMembershipname.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee's Membership is compulsory information.Please Select Employee's Membership."), enMsgBoxStyle.Information)
'                '    cboEmpMembershipname.Select()
'                '    Exit Sub
'                'ElseIf Trim(txtEmpMembershipNo.Text) = "" Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee's Membership No cannot be blank.Employee's Membership No is required information."), enMsgBoxStyle.Information)
'                '    txtEmpMembershipNo.Select()
'                '    Exit Sub
'            ElseIf CInt(cboEmpCategory.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee's Medical Category is compulsory information.Please Select Employee's Medical Category."), enMsgBoxStyle.Information)
'                cboEmpCategory.Select()
'                Exit Sub
'            ElseIf CInt(cboEmpService.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee's Medical Service is compulsory information.Please Select Employee's Medical Service."), enMsgBoxStyle.Information)
'                cboEmpService.Select()
'                Exit Sub
'            ElseIf txtEmpServiceNo.Text.Trim = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Service Provider No cannot be blank.Service Provider No is required information."), enMsgBoxStyle.Information)
'                txtEmpServiceNo.Select()
'                Exit Sub
'            ElseIf CInt(cboEmpCover.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee's Medical Cover is compulsory information.Please Select Employee's Medical Cover."), enMsgBoxStyle.Information)
'                cboEmpCover.Select()
'                Exit Sub

'            End If

'        ElseIf rdDependants.Checked Then

'            If CInt(cboDependants.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Dependants is compulsory information.Please Select Dependants."), enMsgBoxStyle.Information)
'                cboDependants.Select()
'                Exit Sub

'                'ElseIf CInt(cboDepMembershipname.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Dependants's Membership is compulsory information.Please Select Dependants's Membership."), enMsgBoxStyle.Information)
'                '    cboDepMembershipname.Select()
'                '    Exit Sub
'                'ElseIf Trim(txtDepMembershipNo.Text) = "" Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Dependants's Memberdship No cannot be blank.Dependants's Membership No is required information."), enMsgBoxStyle.Information)
'                '    txtDepMembershipNo.Select()
'                '    Exit Sub
'            ElseIf CInt(cboDepCategory.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Dependants's Medical Category is compulsory information.Please Select Dependants's Medical Category."), enMsgBoxStyle.Information)
'                cboDepCategory.Select()
'                Exit Sub
'            ElseIf CInt(cboDepService.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Dependants's Medical Service is compulsory information.Please Select Dependants's Medical Service."), enMsgBoxStyle.Information)
'                cboDepService.Select()
'                Exit Sub
'            ElseIf txtDepServiceNo.Text.Trim = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Service Provider No cannot be blank.Service Provider No is required information."), enMsgBoxStyle.Information)
'                txtDepServiceNo.Select()
'                Exit Sub
'            ElseIf CInt(cboDepCover.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Dependants's Medical Cover is compulsory information.Please Select Dependants's Medical Cover."), enMsgBoxStyle.Information)
'                cboDepCover.Select()
'                Exit Sub

'            End If

'        End If

'        SetValue()

'        If menAction = enAction.EDIT_ONE Then

'            If rdEmployee.Checked Then
'                blnFlag = objMedcalCover.Update(False)
'            ElseIf rdDependants.Checked Then
'                blnFlag = objMedcalCover.Update(True)
'            End If

'        Else
'            If rdEmployee.Checked Then
'                blnFlag = objMedcalCover.Insert(False)
'            ElseIf rdDependants.Checked Then
'                blnFlag = objMedcalCover.Insert(True)
'            End If

'        End If

'        If blnFlag = False And objMedcalCover._Message <> "" Then
'            eZeeMsgBox.Show(objMedcalCover._Message, enMsgBoxStyle.Information)
'        End If

'        If blnFlag Then
'            mblnCancel = False
'            If menAction = enAction.ADD_CONTINUE Then
'                objMedcalCover = Nothing
'                objMedcalCover = New clsmedical_cover
'                Call GetValue()
'                cboEmployee.Select()
    '        Else
'                mintmedicalcoverunkid = objMedcalCover._Covermasterunkid
'                Me.Close()
'            End If
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'    End Try
'End Sub

'Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'    Dim objfrm As New frmCommonSearch
'    Try
'        objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
'        objfrm.ValueMember = "employeeunkid"
'        objfrm.DisplayMember = "employeename"
'        objfrm.CodeMember = "employeecode"
'        If objfrm.DisplayDialog Then
'            cboEmployee.SelectedValue = objfrm.SelectedValue
'        End If
'        cboEmployee.Select()
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'    End Try
'End Sub

''Pinkal(17-Nov-2010)----- Start

'Private Sub objbtnAddEmpSerivce_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddEmpSerivce.Click
'    Try
'        Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
'        Dim intRefId As Integer = -1
'        Dim dsCover As New DataSet
'        Dim objmedical As New clsmedical_master
'        If objfrmMedicalMaster_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Service) Then
'            dsCover = objmedical.getListForCombo(enMedicalMasterType.Service, "List", True)
'            With cboEmpService
'                .ValueMember = "mdmasterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCover.Tables("List")
'            End With
'            cboEmpService.SelectedValue = intRefId
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objbtnAddEmpSerivce_Click", mstrModuleName)
'    End Try
'End Sub

'Private Sub objbtnAddDepSerivce_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDepSerivce.Click
'    Try
'        Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
'        Dim intRefId As Integer = -1
'        Dim dsCover As New DataSet
'        Dim objmedical As New clsmedical_master
'        If objfrmMedicalMaster_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Service) Then
'            dsCover = objmedical.getListForCombo(enMedicalMasterType.Service, "List", True)
'            With cboDepService
'                .ValueMember = "mdmasterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCover.Tables("List")
'            End With
'            cboDepService.SelectedValue = intRefId
    '        End If
    '    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objbtnAddDepSerivce_Click", mstrModuleName)
    '    End Try
    'End Sub

''Pinkal(17-Nov-2010)----- End

'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'    Me.Close()
'End Sub

'#End Region

'#Region "Dropdown's Event"

'Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
'    Dim dsFill As DataSet = Nothing
'    Try
'        If CInt(cboEmployee.SelectedValue) < 0 Then Exit Sub
'        ''FOR MEMBERSHIP
'        Dim objEmployee As New clsEmployee_Master
'        'dsFill = objEmployee.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
'        'cboEmpMembershipname.ValueMember = "Mem_Id"
'        'cboEmpMembershipname.DisplayMember = "Mem_Name"
'        'cboEmpMembershipname.DataSource = dsFill.Tables("Membership")

'        If rdDependants.Checked Then
'            'FOR DEPENDENTS
'            dsFill = Nothing
'            dsFill = objEmployee.GetEmployee_Dependents(CInt(cboEmployee.SelectedValue), True, "Dependents")
'            cboDependants.ValueMember = "dependent_Id"
'            cboDependants.DisplayMember = "dependent_Name"
'            cboDependants.DataSource = dsFill.Tables("Dependents")

'        End If

'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
'    End Try
'End Sub

''Private Sub cboEmpMembershipname_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
''    Dim dsFill As DataSet = Nothing
''    Dim dtFill As DataTable = Nothing
''    Try
''        'FOR MEMBERSHIP
''        Dim objEmployee As New clsEmployee_Master
''        dsFill = objEmployee.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
''        dtFill = New DataView(dsFill.Tables("Membership"), "Mem_Id=" & CInt(cboEmpMembershipname.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
''        If dtFill.Rows.Count > 0 Then
''            txtEmpMembershipNo.Text = dtFill.Rows(0)("Mem_No").ToString
''        Else
''            txtEmpMembershipNo.Text = ""
''        End If
''    Catch ex As Exception
''        DisplayError.Show("-1", ex.Message, "cboMembershipname_SelectedIndexChanged", mstrModuleName)
''    End Try
''End Sub

'Private Sub cboCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpCategory.SelectedIndexChanged
'    Dim dsFill As DataSet = Nothing
'    Dim dtFill As DataTable = Nothing
'    Try
'        txtEmpAmount.Text = "0.00"
'        Dim objMedicalCategory As New clsmedical_category_master
'        dsFill = objMedicalCategory.GetList("List", True)
'        dtFill = New DataView(dsFill.Tables("List"), "mdcategorymasterunkid=" & CInt(cboEmpCategory.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

'        If dtFill IsNot Nothing Then
'            If dtFill.Rows.Count > 0 Then
'                txtEmpAmount.Text = CDec(dtFill.Rows(0)("amount")).ToString("#0.00")
'            End If
'        End If

'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "cboCategory_SelectedIndexChanged", mstrModuleName)
'    End Try
'End Sub

'Private Sub cboDepCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepCategory.SelectedIndexChanged
    '    Dim dsFill As DataSet = Nothing
'    Dim dtFill As DataTable = Nothing
    '    Try
'        txtDepAmount.Text = "0.00"
'        Dim objMedicalCategory As New clsmedical_category_master
'        dsFill = objMedicalCategory.GetList("List", True)
'        dtFill = New DataView(dsFill.Tables("List"), "mdcategorymasterunkid=" & CInt(cboDepCategory.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

'        If dtFill IsNot Nothing Then
'            If dtFill.Rows.Count > 0 Then
'                txtDepAmount.Text = CDec(dtFill.Rows(0)("amount")).ToString("#0.00")
'            End If
'        End If

    '    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "cboDepCategory_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

''Private Sub cboDependants_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDependants.SelectedIndexChanged
''    Dim dsFill As DataSet = Nothing
''    Try
''        If CInt(cboDependants.SelectedValue) < 0 Then Exit Sub
''        'FOR DEPENDANTS MEMBERSHIP
''        Dim objDepMembership As New clsDependants_Membership_tran
''        dsFill = objDepMembership.GetDependants_Membership(CInt(cboDependants.SelectedValue), True, "Membership")
''        cboDepMembershipname.ValueMember = "Mem_Id"
''        cboDepMembershipname.DisplayMember = "Mem_Name"
''        cboDepMembershipname.DataSource = dsFill.Tables("Membership")
''    Catch ex As Exception
''        DisplayError.Show("-1", ex.Message, "cboDependants_SelectedIndexChanged", mstrModuleName)
''    End Try
''End Sub

''Private Sub cboDepMembershipname_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
''    Dim dsFill As DataSet = Nothing
''    Dim dtFill As DataTable = Nothing
''    Try
''        'FOR DEPENDANTS MEMBERSHIP
''        Dim objDepMembership As New clsDependants_Membership_tran
''        dsFill = objDepMembership.GetDependants_Membership(CInt(cboDependants.SelectedValue), True, "Membership")
''        dtFill = New DataView(dsFill.Tables("Membership"), "Mem_Id=" & CInt(cboDepMembershipname.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
''        If dtFill.Rows.Count > 0 Then
''            txtDepMembershipNo.Text = dtFill.Rows(0)("Mem_No").ToString
''        Else
''            txtDepMembershipNo.Text = ""
''        End If
''    Catch ex As Exception
''        DisplayError.Show("-1", ex.Message, "cboDepMembershipname_SelectedIndexChanged", mstrModuleName)
''    End Try
''End Sub


'Private Sub cboDepService_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepService.SelectedIndexChanged, cboEmpService.SelectedIndexChanged
    '    Dim dsFill As DataSet = Nothing
    '    Dim dtFill As DataTable = Nothing
    '    Try
'        Dim objMedical As New clsmedical_master
'        dsFill = objMedical.GetList("Service", True)


'        If rdDependants.Checked Then

'            dtFill = New DataView(dsFill.Tables("Service"), "mdmasterunkid=" & CInt(cboDepService.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

    '        If dtFill.Rows.Count > 0 Then
'                txtDepServiceNo.Text = dtFill.Rows(0)("mdserviceno").ToString
    '        Else
'                txtDepServiceNo.Text = ""
    '        End If

'        ElseIf rdEmployee.Checked Then

'            dtFill = New DataView(dsFill.Tables("Service"), "mdmasterunkid=" & CInt(cboEmpService.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

'            If dtFill.Rows.Count > 0 Then
'                txtEmpServiceNo.Text = dtFill.Rows(0)("mdserviceno").ToString
'            Else
'                txtEmpServiceNo.Text = ""
'            End If
'        End If

    '    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "cboDepService_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub


'#End Region

'#Region "Radio Button's Event"

'Private Sub rdEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdEmployee.CheckedChanged, rdDependants.CheckedChanged
'    Try
'        If rdEmployee.Checked Then
'            tbMedicalCover.TabPages.Remove(tbpDepandentsCover)
'            If tbMedicalCover.TabPages.Contains(tbpEmployeeCover) = False Then tbMedicalCover.TabPages.Add(tbpEmployeeCover)
'            FillCombo()
'        End If

'        If rdDependants.Checked Then
'            tbMedicalCover.TabPages.Remove(tbpEmployeeCover)
'            If tbMedicalCover.TabPages.Contains(tbpDepandentsCover) = False Then tbMedicalCover.TabPages.Add(tbpDepandentsCover)
'            FillCombo()
'        End If

'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "rdEmployee_CheckedChanged", mstrModuleName)
'    End Try
'End Sub

'#End Region

''Sandeep [ 17 DEC 2010 ] -- Start
'Private Sub objbtnAddCover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddEmpCover.Click
'    Dim frm As New frmMedicalMaster_AddEdit
'    Dim intRefId As Integer = -1
'    Try
'        frm.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Cover)
'        If intRefId > 0 Then
'            Dim dsList As New DataSet
'            Dim objMedMaster As New clsmedical_master
'            dsList = objMedMaster.getListForCombo(enMedicalMasterType.Cover, "COV", True)
'            With cboEmpCover
'                .ValueMember = "mdmasterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("COV")
'                .SelectedValue = intRefId
'            End With
'            dsList.Dispose()
'            objMedMaster = Nothing
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objbtnAddCover_Click", mstrModuleName)
'    Finally
'        If frm IsNot Nothing Then frm.Dispose()
'    End Try
'End Sub

'Private Sub objbtnAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddEmpCategory.Click
'    Dim frm As New frmMedicalMaster_AddEdit
'    Dim intRefId As Integer = -1
'    Try

'        'Pinkal (12-Oct-2011) -- Start
'        'Enhancement : TRA Changes
'        'frm.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Category)
'        frm.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Medical_Category)
'        'Pinkal (12-Oct-2011) -- End


'        If intRefId > 0 Then
'            Dim dsList As New DataSet
'            Dim objMedMaster As New clsmedical_master

'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes
'            'dsList = objMedMaster.getListForCombo(enMedicalMasterType.Category, "CAT", True)
'            dsList = objMedMaster.getListForCombo(enMedicalMasterType.Medical_Category, "CAT", True)
'            'Pinkal (12-Oct-2011) -- End


'            With cboEmpCategory
'                .ValueMember = "mdmasterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("CAT")
'                .SelectedValue = intRefId
'            End With
'            dsList.Dispose()
'            objMedMaster = Nothing
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
'    Finally
'        If frm IsNot Nothing Then frm.Dispose()
'    End Try
'End Sub

'Private Sub objbtnAddDepCover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDepCover.Click
'    Dim frm As New frmMedicalMaster_AddEdit
'    Dim intRefId As Integer = -1
'    Try
'        frm.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Cover)
'        If intRefId > 0 Then
'            Dim dsList As New DataSet
'            Dim objMedMaster As New clsmedical_master
'            dsList = objMedMaster.getListForCombo(enMedicalMasterType.Cover, "COV", True)
'            With cboDepCover
'                .ValueMember = "mdmasterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("COV")
'                .SelectedValue = intRefId
'            End With
'            dsList.Dispose()
'            objMedMaster = Nothing
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objbtnAddCover_Click", mstrModuleName)
'    Finally
'        If frm IsNot Nothing Then frm.Dispose()
'    End Try
'End Sub

'Private Sub objbtnAddDepCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDepCategory.Click
'    Dim frm As New frmMedicalMaster_AddEdit
'    Dim intRefId As Integer = -1
'    Try

'        'Pinkal (12-Oct-2011) -- Start
'        'Enhancement : TRA Changes
'        'frm.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Category)
'        frm.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Medical_Category)
'        'Pinkal (12-Oct-2011) -- End


'        If intRefId > 0 Then
'            Dim dsList As New DataSet
'            Dim objMedMaster As New clsmedical_master

'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes
'            'dsList = objMedMaster.getListForCombo(enMedicalMasterType.Category, "CAT", True)
'            dsList = objMedMaster.getListForCombo(enMedicalMasterType.Medical_Category, "CAT", True)
'            'Pinkal (12-Oct-2011) -- End

'            With cboDepCategory
'                .ValueMember = "mdmasterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("CAT")
'                .SelectedValue = intRefId
'            End With
'            dsList.Dispose()
'            objMedMaster = Nothing
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
'    Finally
'        If frm IsNot Nothing Then frm.Dispose()
'    End Try
'End Sub
''Sandeep [ 17 DEC 2010 ] -- End 
   




