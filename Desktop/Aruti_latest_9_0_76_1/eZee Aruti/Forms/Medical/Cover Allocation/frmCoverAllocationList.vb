﻿Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmCoverAllocationList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmCoverAllocationList"
    Private objMedicalcover As clsmedical_cover
    Dim objExchangeRate As New clsExchangeRate
#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objMedicalcover.GetList("MedicalCover")
            dsList = objMedicalcover.GetList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                             "MedicalCover")
            'Shani(24-Aug-2015) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching = "AND employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboDependents.SelectedValue) > 0 Then
                StrSearching &= "AND dependantsunkid=" & CInt(cboDependents.SelectedValue) & " "
            End If

            If CInt(cboService.SelectedValue) > 0 Then
                StrSearching &= "AND serviceunkid=" & CInt(cboService.SelectedValue) & " "
            End If

            If txtServiceNo.Text.Trim.Length > 0 Then
                StrSearching &= "AND serviceno like '%" & txtServiceNo.Text.Trim & "%'" & " "
            End If

            If CInt(cboMedicalCategory.SelectedValue) > 0 Then
                StrSearching &= "AND medicalcategoryunkid=" & CInt(cboMedicalCategory.SelectedValue) & " "
            End If

            'Sohail (17 Jun 2011) -- Start
            'If txtAmountFrom.Text.Trim.Length > 0 Then
            '    StrSearching &= "AND amount >= " & CDec(txtAmountFrom.Text.Trim) & " "
            'End If

            'If txtAmountTo.Text.Trim.Length > 0 Then
            '    StrSearching &= "AND amount <= " & CDec(txtAmountTo.Text.Trim) & " "
            'End If
            If txtAmountFrom.Decimal <> 0 Then
                StrSearching &= "AND amount >= " & txtAmountFrom.Decimal & " "
            End If

            If txtAmountTo.Decimal <> 0 Then
                StrSearching &= "AND amount <= " & txtAmountTo.Decimal & " "
            End If
            'Sohail (17 Jun 2011) -- End
            If txtRemark.Text.Trim.Length > 0 Then
                StrSearching &= "AND remark like '%" & txtRemark.Text.Trim & "%'" & " "
            End If

            'S.SANDEEP [ 06 OCT 2014 ] -- START
            If dtpStartDate.Checked = True Then
                StrSearching &= "AND startdate = '" & eZeeDate.convertDate(dtpStartDate.Value).ToString & "' "
            End If

            If dtpEndDate.Checked = True Then
                StrSearching &= "AND enddate = '" & eZeeDate.convertDate(dtpEndDate.Value).ToString & "' "
            End If
            'S.SANDEEP [ 06 OCT 2014 ] -- END


            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables("MedicalCover"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("MedicalCover")
            End If

            lstMedicalCover.Items.Clear()
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                'lvItem.Text = dtRow("employeename").ToString
                'lvItem.Tag = CInt(dtRow("medicalcoverunkid").ToString)
                'lvItem.SubItems.Add(dtRow("dependents").ToString())
                'lvItem.SubItems.Add(dtRow("service").ToString())
                'lvItem.SubItems.Add(dtRow("serviceno").ToString())
                'lvItem.SubItems.Add(dtRow("category").ToString())
                'lvItem.SubItems.Add(Format(CDec(dtRow("amount")), objExchangeRate._fmtCurrency))
                'lvItem.SubItems.Add(dtRow("remark").ToString())
                'lstMedicalCover.Items.Add(lvItem)

                lvItem.Tag = CInt(dtRow("medicalcoverunkid").ToString)
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow("category").ToString())
                lvItem.SubItems.Add(dtRow("employeename").ToString)
                lvItem.SubItems.Add(dtRow("remark").ToString())
                lvItem.SubItems.Add(dtRow("dependents").ToString())
                lvItem.SubItems.Add(dtRow("service").ToString())
                lvItem.SubItems.Add(dtRow("serviceno").ToString())
                lvItem.SubItems.Add(Format(CDec(dtRow("amount")), objExchangeRate._fmtCurrency))

                'S.SANDEEP [ 06 OCT 2014 ] -- START
                If dtRow.Item("startdate").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("startdate").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("enddate").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("enddate").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If
                'S.SANDEEP [ 06 OCT 2014 ] -- END


                'Pinkal (12-Oct-2011) -- End

                lstMedicalCover.Items.Add(lvItem)

            Next


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            'If lstMedicalCover.Items.Count > 16 Then
            '    colhRemark.Width = 117 - 18
            'Else
            '    colhRemark.Width = 117
            'End If

            lstMedicalCover.GroupingColumn = colhCategory
            lstMedicalCover.DisplayGroups(True)

            If lstMedicalCover.Items.Count > 16 Then
                'S.SANDEEP [ 06 OCT 2014 ] -- START
                '    colhRemark.Width = 590 - 18
                'Else
                '    colhRemark.Width = 590
                colhRemark.Width = 430 - 20
            Else
                colhRemark.Width = 430
                'S.SANDEEP [ 06 OCT 2014 ] -- END

            End If

            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim DsFill As New DataSet
        Try
            'FOR EMPLOYEE 
            Dim objEmployee As New clsEmployee_Master

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : CHECK FOR ACTIVE EMPLOYEE
            'DsFill = objEmployee.GetEmployeeList("Employee", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'DsFill = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    DsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    DsFill = objEmployee.GetEmployeeList("Employee", True)
            'End If

            DsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            'Pinkal (24-Jun-2011) -- End

            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = DsFill.Tables("Employee")

            'FOR MEDICAL CATEGORY
            DsFill = Nothing
            Dim objMedicalCategory As New clsmedical_master

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Category, "Category", True)
            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Medical_Category, "Category", True)
            'Pinkal (12-Oct-2011) -- End

            cboMedicalCategory.DisplayMember = "name"
            cboMedicalCategory.ValueMember = "mdmasterunkid"
            cboMedicalCategory.DataSource = DsFill.Tables("Category")

            'FOR MEDICAL SERVICE
            DsFill = Nothing
            DsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Service, "Service", True)
            cboService.DisplayMember = "name"
            cboService.ValueMember = "mdmasterunkid"
            cboService.DataSource = DsFill.Tables("Service")


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddMedicalCover
            btnEdit.Enabled = User._Object.Privilege._EditMedicalCover
            btnDelete.Enabled = User._Object.Privilege._DeleteMedicalCover

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCoverAllocationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMedicalcover = New clsmedical_cover
        objExchangeRate = New clsExchangeRate
        Try
            Call Set_Logo(Me, gApplicationType)
            objExchangeRate._ExchangeRateunkid = 1
            Call Language.setLanguage(Me.Name)

            Call SetVisibility()

            FillCombo()
            FillList()
            If lstMedicalCover.Items.Count > 0 Then lstMedicalCover.Items(0).Selected = True
            lstMedicalCover.Select()

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            lstMedicalCover.GridLines = False
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCoverAllocationList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCoverAllocationList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCoverAllocationList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCoverAllocationList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete And lstMedicalCover.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCoverAllocationList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCoverAllocationList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMedicalcover = Nothing
    End Sub
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_cover.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_cover"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrm As New frmCoverAllocation_AddEdit
            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lstMedicalCover.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Medical Cover from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lstMedicalCover.Select()
            Exit Sub
        End If
        Dim objfrmCoverAllocation_AddEdit As New frmCoverAllocation_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lstMedicalCover.SelectedItems(0).Index
            If objfrmCoverAllocation_AddEdit.displayDialog(CInt(lstMedicalCover.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                FillList()
            End If
            objfrmCoverAllocation_AddEdit = Nothing
            If lstMedicalCover.Items.Count > 0 Then
                If lstMedicalCover.Items.Count < intSelectedIndex Then intSelectedIndex = lstMedicalCover.Items.Count
                lstMedicalCover.Items(intSelectedIndex).Selected = True
                lstMedicalCover.EnsureVisible(intSelectedIndex)
            End If
            lstMedicalCover.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmCoverAllocation_AddEdit IsNot Nothing Then objfrmCoverAllocation_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lstMedicalCover.CheckedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Medical Cover from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lstMedicalCover.Select()
            Exit Sub
        End If
       
        Try
            Dim intSelectedIndex As Integer

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'intSelectedIndex = lstMedicalCover.SelectedItems(0).Index
            intSelectedIndex = lstMedicalCover.CheckedItems(0).Index
            'Pinkal (12-Oct-2011) -- End



            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Medical Cover?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
               
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.MEDICAL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objMedicalcover._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objMedicalcover._Voiduserunkid = User._Object._Userunkid
                objMedicalcover._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime


                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
                'objMedicalcover.Delete(CInt(lstMedicalCover.SelectedItems(0).Tag))
                'lstMedicalCover.SelectedItems(0).Remove()
                Dim mstrCoverunkid As String = ""
                For i As Integer = 0 To lstMedicalCover.CheckedItems.Count - 1
                    mstrCoverunkid &= CInt(lstMedicalCover.CheckedItems(i).Tag) & ","
                Next

                If mstrCoverunkid.Trim.Length > 0 Then
                    mstrCoverunkid = mstrCoverunkid.Substring(0, mstrCoverunkid.Trim.Length - 1)
                    objMedicalcover.Delete(mstrCoverunkid)
                    FillList()
                End If

                'Pinkal (12-Oct-2011) -- End

                If lstMedicalCover.Items.Count <= 0 Then
                    Exit Try
                End If

                If lstMedicalCover.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lstMedicalCover.Items.Count - 1
                    lstMedicalCover.Items(intSelectedIndex).Selected = True
                    lstMedicalCover.EnsureVisible(intSelectedIndex)
                ElseIf lstMedicalCover.Items.Count <> 0 Then
                    lstMedicalCover.Items(intSelectedIndex).Selected = True
                    lstMedicalCover.EnsureVisible(intSelectedIndex)
                End If
            End If
            lstMedicalCover.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
            objfrm.ValueMember = "employeeunkid"
            objfrm.DisplayMember = "employeename"
            objfrm.CodeMember = "employeecode"
            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
            End If
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboDependents.SelectedIndex = 0
            cboService.SelectedIndex = 0
            cboMedicalCategory.SelectedIndex = 0
            txtAmountFrom.Text = ""
            txtAmountTo.Text = ""
            txtServiceNo.Text = ""
            txtRemark.Text = ""
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim dsFill As DataSet = Nothing
        Try
            'FOR DEPENDENTS
            Dim objEmployee As New clsEmployee_Master
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'dsFill = objEmployee.GetEmployee_Dependents(CInt(cboEmployee.SelectedValue), True, "Dependents")
            dsFill = objEmployee.GetEmployee_Dependents(CInt(cboEmployee.SelectedValue), True, "Dependents", eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString))
            'Sohail (18 May 2019) -- End
            cboDependents.ValueMember = "dependent_Id"
            cboDependents.DisplayMember = "dependent_Name"
            cboDependents.DataSource = dsFill.Tables("Dependents")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhDependants.Text = Language._Object.getCaption(CStr(Me.colhDependants.Tag), Me.colhDependants.Text)
			Me.colhCategory.Text = Language._Object.getCaption(CStr(Me.colhCategory.Tag), Me.colhCategory.Text)
			Me.colhCoverAmount.Text = Language._Object.getCaption(CStr(Me.colhCoverAmount.Tag), Me.colhCoverAmount.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblMedicalCategory.Text = Language._Object.getCaption(Me.lblMedicalCategory.Name, Me.lblMedicalCategory.Text)
			Me.lbldependants.Text = Language._Object.getCaption(Me.lbldependants.Name, Me.lbldependants.Text)
			Me.colhServiceNo.Text = Language._Object.getCaption(CStr(Me.colhServiceNo.Tag), Me.colhServiceNo.Text)
			Me.colhService.Text = Language._Object.getCaption(CStr(Me.colhService.Tag), Me.colhService.Text)
			Me.lblService.Text = Language._Object.getCaption(Me.lblService.Name, Me.lblService.Text)
			Me.lblServiceNo.Text = Language._Object.getCaption(Me.lblServiceNo.Name, Me.lblServiceNo.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblCoverAmount.Text = Language._Object.getCaption(Me.lblCoverAmount.Name, Me.lblCoverAmount.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Medical Cover from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Medical Cover?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

