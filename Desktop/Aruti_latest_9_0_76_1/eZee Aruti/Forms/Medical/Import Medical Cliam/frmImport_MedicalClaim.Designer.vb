﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImport_MedicalClaim
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImport_MedicalClaim))
        Me.PnlImportdata = New System.Windows.Forms.Panel
        Me.wzImportData = New eZee.Common.eZeeWizard
        Me.wpMapfield = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeecode = New System.Windows.Forms.Label
        Me.lblClaimAmount = New System.Windows.Forms.Label
        Me.lblClaimDate = New System.Windows.Forms.Label
        Me.cboClaimAmt = New System.Windows.Forms.ComboBox
        Me.cboClaimdate = New System.Windows.Forms.ComboBox
        Me.lblSickSheet = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.cboClaimNo = New System.Windows.Forms.ComboBox
        Me.cboSickSheet = New System.Windows.Forms.ComboBox
        Me.lblDependant = New System.Windows.Forms.Label
        Me.cboDepedant = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblClaimno = New System.Windows.Forms.Label
        Me.wpFileSelection = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.wpImportData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.dgEmployeedata = New System.Windows.Forms.DataGridView
        Me.colhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployeecode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhDepedants = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSickSheet = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LblEmpExempted = New System.Windows.Forms.Label
        Me.cboExemptDeduction = New System.Windows.Forms.ComboBox
        Me.PnlImportdata.SuspendLayout()
        Me.wzImportData.SuspendLayout()
        Me.wpMapfield.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.wpFileSelection.SuspendLayout()
        Me.wpImportData.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        CType(Me.dgEmployeedata, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlImportdata
        '
        Me.PnlImportdata.Controls.Add(Me.wzImportData)
        Me.PnlImportdata.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlImportdata.Location = New System.Drawing.Point(0, 0)
        Me.PnlImportdata.Name = "PnlImportdata"
        Me.PnlImportdata.Size = New System.Drawing.Size(617, 395)
        Me.PnlImportdata.TabIndex = 0
        '
        'wzImportData
        '
        Me.wzImportData.Controls.Add(Me.wpMapfield)
        Me.wzImportData.Controls.Add(Me.wpFileSelection)
        Me.wzImportData.Controls.Add(Me.wpImportData)
        Me.wzImportData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wzImportData.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzImportData.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.wzImportData.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.wzImportData.Location = New System.Drawing.Point(0, 0)
        Me.wzImportData.Name = "wzImportData"
        Me.wzImportData.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wpFileSelection, Me.wpMapfield, Me.wpImportData})
        Me.wzImportData.Size = New System.Drawing.Size(617, 395)
        Me.wzImportData.TabIndex = 0
        Me.wzImportData.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzImportData.WelcomeImage = Nothing
        Me.wzImportData.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wpMapfield
        '
        Me.wpMapfield.Controls.Add(Me.gbFieldMapping)
        Me.wpMapfield.Location = New System.Drawing.Point(0, 0)
        Me.wpMapfield.Name = "wpMapfield"
        Me.wpMapfield.Size = New System.Drawing.Size(617, 347)
        Me.wpMapfield.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpMapfield.TabIndex = 9
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.LblEmpExempted)
        Me.gbFieldMapping.Controls.Add(Me.cboExemptDeduction)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeecode)
        Me.gbFieldMapping.Controls.Add(Me.lblClaimAmount)
        Me.gbFieldMapping.Controls.Add(Me.lblClaimDate)
        Me.gbFieldMapping.Controls.Add(Me.cboClaimAmt)
        Me.gbFieldMapping.Controls.Add(Me.cboClaimdate)
        Me.gbFieldMapping.Controls.Add(Me.lblSickSheet)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployee)
        Me.gbFieldMapping.Controls.Add(Me.cboClaimNo)
        Me.gbFieldMapping.Controls.Add(Me.cboSickSheet)
        Me.gbFieldMapping.Controls.Add(Me.lblDependant)
        Me.gbFieldMapping.Controls.Add(Me.cboDepedant)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployee)
        Me.gbFieldMapping.Controls.Add(Me.lblClaimno)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(164, 0)
        Me.gbFieldMapping.Margin = New System.Windows.Forms.Padding(0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(453, 347)
        Me.gbFieldMapping.TabIndex = 1
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(180, 63)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(173, 21)
        Me.cboEmployeeCode.TabIndex = 1
        '
        'lblEmployeecode
        '
        Me.lblEmployeecode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeecode.Location = New System.Drawing.Point(53, 65)
        Me.lblEmployeecode.Name = "lblEmployeecode"
        Me.lblEmployeecode.Size = New System.Drawing.Size(121, 17)
        Me.lblEmployeecode.TabIndex = 19
        Me.lblEmployeecode.Text = "Employee Code"
        Me.lblEmployeecode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClaimAmount
        '
        Me.lblClaimAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimAmount.Location = New System.Drawing.Point(53, 227)
        Me.lblClaimAmount.Name = "lblClaimAmount"
        Me.lblClaimAmount.Size = New System.Drawing.Size(121, 17)
        Me.lblClaimAmount.TabIndex = 16
        Me.lblClaimAmount.Text = "Claim Amount"
        Me.lblClaimAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClaimDate
        '
        Me.lblClaimDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimDate.Location = New System.Drawing.Point(53, 200)
        Me.lblClaimDate.Name = "lblClaimDate"
        Me.lblClaimDate.Size = New System.Drawing.Size(121, 17)
        Me.lblClaimDate.TabIndex = 13
        Me.lblClaimDate.Text = "Claim Date"
        Me.lblClaimDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClaimAmt
        '
        Me.cboClaimAmt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClaimAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClaimAmt.FormattingEnabled = True
        Me.cboClaimAmt.Location = New System.Drawing.Point(180, 225)
        Me.cboClaimAmt.Name = "cboClaimAmt"
        Me.cboClaimAmt.Size = New System.Drawing.Size(173, 21)
        Me.cboClaimAmt.TabIndex = 7
        '
        'cboClaimdate
        '
        Me.cboClaimdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClaimdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClaimdate.FormattingEnabled = True
        Me.cboClaimdate.Location = New System.Drawing.Point(180, 198)
        Me.cboClaimdate.Name = "cboClaimdate"
        Me.cboClaimdate.Size = New System.Drawing.Size(173, 21)
        Me.cboClaimdate.TabIndex = 6
        '
        'lblSickSheet
        '
        Me.lblSickSheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSickSheet.Location = New System.Drawing.Point(53, 146)
        Me.lblSickSheet.Name = "lblSickSheet"
        Me.lblSickSheet.Size = New System.Drawing.Size(121, 17)
        Me.lblSickSheet.TabIndex = 7
        Me.lblSickSheet.Text = "Sick Sheet"
        Me.lblSickSheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(180, 90)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(173, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'cboClaimNo
        '
        Me.cboClaimNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClaimNo.FormattingEnabled = True
        Me.cboClaimNo.Location = New System.Drawing.Point(180, 171)
        Me.cboClaimNo.Name = "cboClaimNo"
        Me.cboClaimNo.Size = New System.Drawing.Size(173, 21)
        Me.cboClaimNo.TabIndex = 5
        '
        'cboSickSheet
        '
        Me.cboSickSheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSickSheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSickSheet.FormattingEnabled = True
        Me.cboSickSheet.Location = New System.Drawing.Point(180, 144)
        Me.cboSickSheet.Name = "cboSickSheet"
        Me.cboSickSheet.Size = New System.Drawing.Size(173, 21)
        Me.cboSickSheet.TabIndex = 4
        '
        'lblDependant
        '
        Me.lblDependant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDependant.Location = New System.Drawing.Point(53, 119)
        Me.lblDependant.Name = "lblDependant"
        Me.lblDependant.Size = New System.Drawing.Size(121, 17)
        Me.lblDependant.TabIndex = 4
        Me.lblDependant.Text = "Dependant"
        Me.lblDependant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepedant
        '
        Me.cboDepedant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepedant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepedant.FormattingEnabled = True
        Me.cboDepedant.Location = New System.Drawing.Point(180, 117)
        Me.cboDepedant.Name = "cboDepedant"
        Me.cboDepedant.Size = New System.Drawing.Size(173, 21)
        Me.cboDepedant.TabIndex = 3
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(53, 92)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(121, 17)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClaimno
        '
        Me.lblClaimno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimno.Location = New System.Drawing.Point(53, 173)
        Me.lblClaimno.Name = "lblClaimno"
        Me.lblClaimno.Size = New System.Drawing.Size(121, 17)
        Me.lblClaimno.TabIndex = 10
        Me.lblClaimno.Text = "Claim No"
        Me.lblClaimno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpFileSelection
        '
        Me.wpFileSelection.Controls.Add(Me.btnOpenFile)
        Me.wpFileSelection.Controls.Add(Me.txtFilePath)
        Me.wpFileSelection.Controls.Add(Me.lblSelectfile)
        Me.wpFileSelection.Controls.Add(Me.lblMessage)
        Me.wpFileSelection.Controls.Add(Me.lblTitle)
        Me.wpFileSelection.Location = New System.Drawing.Point(0, 0)
        Me.wpFileSelection.Name = "wpFileSelection"
        Me.wpFileSelection.Size = New System.Drawing.Size(617, 347)
        Me.wpFileSelection.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpFileSelection.TabIndex = 7
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(584, 185)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 21)
        Me.btnOpenFile.TabIndex = 13
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(186, 185)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(392, 21)
        Me.txtFilePath.TabIndex = 12
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(186, 158)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(96, 20)
        Me.lblSelectfile.TabIndex = 11
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(179, 59)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(399, 31)
        Me.lblMessage.TabIndex = 10
        Me.lblMessage.Text = "This wizard will import Employee Medical Cliam records made from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(179, 19)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(370, 23)
        Me.lblTitle.TabIndex = 9
        Me.lblTitle.Text = "Data Import Wizard"
        '
        'wpImportData
        '
        Me.wpImportData.Controls.Add(Me.btnFilter)
        Me.wpImportData.Controls.Add(Me.dgEmployeedata)
        Me.wpImportData.Controls.Add(Me.Panel1)
        Me.wpImportData.Location = New System.Drawing.Point(0, 0)
        Me.wpImportData.Name = "wpImportData"
        Me.wpImportData.Size = New System.Drawing.Size(617, 347)
        Me.wpImportData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpImportData.TabIndex = 8
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 313)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(91, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 19
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(198, 92)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(197, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(197, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(197, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(197, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'dgEmployeedata
        '
        Me.dgEmployeedata.AllowUserToAddRows = False
        Me.dgEmployeedata.AllowUserToDeleteRows = False
        Me.dgEmployeedata.AllowUserToResizeColumns = False
        Me.dgEmployeedata.AllowUserToResizeRows = False
        Me.dgEmployeedata.BackgroundColor = System.Drawing.Color.White
        Me.dgEmployeedata.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployeedata.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhImage, Me.colhEmployeecode, Me.colhEmployee, Me.colhDepedants, Me.colhSickSheet, Me.colhStatus, Me.colhMessage, Me.objcolhstatus})
        Me.dgEmployeedata.Location = New System.Drawing.Point(12, 64)
        Me.dgEmployeedata.MultiSelect = False
        Me.dgEmployeedata.Name = "dgEmployeedata"
        Me.dgEmployeedata.ReadOnly = True
        Me.dgEmployeedata.RowHeadersVisible = False
        Me.dgEmployeedata.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgEmployeedata.Size = New System.Drawing.Size(594, 246)
        Me.dgEmployeedata.TabIndex = 18
        '
        'colhImage
        '
        Me.colhImage.HeaderText = ""
        Me.colhImage.Name = "colhImage"
        Me.colhImage.ReadOnly = True
        Me.colhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colhImage.Width = 30
        '
        'colhEmployeecode
        '
        Me.colhEmployeecode.HeaderText = "Code"
        Me.colhEmployeecode.Name = "colhEmployeecode"
        Me.colhEmployeecode.ReadOnly = True
        Me.colhEmployeecode.Width = 50
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhDepedants
        '
        Me.colhDepedants.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhDepedants.HeaderText = "Dependant"
        Me.colhDepedants.Name = "colhDepedants"
        Me.colhDepedants.ReadOnly = True
        '
        'colhSickSheet
        '
        Me.colhSickSheet.HeaderText = "Sick Sheet"
        Me.colhSickSheet.Name = "colhSickSheet"
        Me.colhSickSheet.ReadOnly = True
        Me.colhSickSheet.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.ezWait)
        Me.Panel1.Controls.Add(Me.objError)
        Me.Panel1.Controls.Add(Me.objWarning)
        Me.Panel1.Controls.Add(Me.objSuccess)
        Me.Panel1.Controls.Add(Me.lblWarning)
        Me.Panel1.Controls.Add(Me.lblError)
        Me.Panel1.Controls.Add(Me.objTotal)
        Me.Panel1.Controls.Add(Me.lblSuccess)
        Me.Panel1.Controls.Add(Me.lblTotal)
        Me.Panel1.Location = New System.Drawing.Point(12, 10)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(594, 51)
        Me.Panel1.TabIndex = 1
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(472, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(358, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(472, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(404, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(517, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(358, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(517, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(404, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 30
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Import For"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 175
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Message"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        Me.DataGridViewTextBoxColumn6.Width = 175
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objcolhImortedFor"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objcolhstatus"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objcolhDate"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'LblEmpExempted
        '
        Me.LblEmpExempted.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmpExempted.Location = New System.Drawing.Point(53, 254)
        Me.LblEmpExempted.Name = "LblEmpExempted"
        Me.LblEmpExempted.Size = New System.Drawing.Size(121, 17)
        Me.LblEmpExempted.TabIndex = 22
        Me.LblEmpExempted.Text = "Exempt From Deduction"
        Me.LblEmpExempted.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExemptDeduction
        '
        Me.cboExemptDeduction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExemptDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExemptDeduction.FormattingEnabled = True
        Me.cboExemptDeduction.Location = New System.Drawing.Point(180, 252)
        Me.cboExemptDeduction.Name = "cboExemptDeduction"
        Me.cboExemptDeduction.Size = New System.Drawing.Size(173, 21)
        Me.cboExemptDeduction.TabIndex = 21
        '
        'frmImport_MedicalClaim
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 395)
        Me.Controls.Add(Me.PnlImportdata)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImport_MedicalClaim"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Data"
        Me.PnlImportdata.ResumeLayout(False)
        Me.wzImportData.ResumeLayout(False)
        Me.wpMapfield.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.wpFileSelection.ResumeLayout(False)
        Me.wpFileSelection.PerformLayout()
        Me.wpImportData.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        CType(Me.dgEmployeedata, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PnlImportdata As System.Windows.Forms.Panel
    Friend WithEvents wzImportData As eZee.Common.eZeeWizard
    Friend WithEvents wpFileSelection As eZee.Common.eZeeWizardPage
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents wpImportData As eZee.Common.eZeeWizardPage
    Friend WithEvents dgEmployeedata As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents wpMapfield As eZee.Common.eZeeWizardPage
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblClaimAmount As System.Windows.Forms.Label
    Friend WithEvents lblClaimDate As System.Windows.Forms.Label
    Friend WithEvents cboClaimAmt As System.Windows.Forms.ComboBox
    Friend WithEvents cboClaimdate As System.Windows.Forms.ComboBox
    Friend WithEvents lblSickSheet As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents cboClaimNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboSickSheet As System.Windows.Forms.ComboBox
    Friend WithEvents lblDependant As System.Windows.Forms.Label
    Friend WithEvents cboDepedant As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblClaimno As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeecode As System.Windows.Forms.Label
    Friend WithEvents colhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployeecode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhDepedants As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSickSheet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblEmpExempted As System.Windows.Forms.Label
    Friend WithEvents cboExemptDeduction As System.Windows.Forms.ComboBox
End Class
