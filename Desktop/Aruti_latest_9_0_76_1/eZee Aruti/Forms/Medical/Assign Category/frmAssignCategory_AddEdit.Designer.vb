﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssignCategory_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssignCategory_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbMedicalCategory = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchMaritalStatus = New eZee.Common.eZeeGradientButton
        Me.cboMaritalStatus = New System.Windows.Forms.ComboBox
        Me.LblMaritalStatus = New System.Windows.Forms.Label
        Me.objbtnAddCategory = New eZee.Common.eZeeGradientButton
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.lblCategory = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.nudDependants = New System.Windows.Forms.NumericUpDown
        Me.lblMaxDependants = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.o = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbMedicalCategory.SuspendLayout()
        CType(Me.nudDependants, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.o.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbMedicalCategory)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(400, 329)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbMedicalCategory
        '
        Me.gbMedicalCategory.BorderColor = System.Drawing.Color.Black
        Me.gbMedicalCategory.Checked = False
        Me.gbMedicalCategory.CollapseAllExceptThis = False
        Me.gbMedicalCategory.CollapsedHoverImage = Nothing
        Me.gbMedicalCategory.CollapsedNormalImage = Nothing
        Me.gbMedicalCategory.CollapsedPressedImage = Nothing
        Me.gbMedicalCategory.CollapseOnLoad = False
        Me.gbMedicalCategory.Controls.Add(Me.objbtnSearchMaritalStatus)
        Me.gbMedicalCategory.Controls.Add(Me.cboMaritalStatus)
        Me.gbMedicalCategory.Controls.Add(Me.LblMaritalStatus)
        Me.gbMedicalCategory.Controls.Add(Me.objbtnAddCategory)
        Me.gbMedicalCategory.Controls.Add(Me.cboCategory)
        Me.gbMedicalCategory.Controls.Add(Me.lblCategory)
        Me.gbMedicalCategory.Controls.Add(Me.txtAmount)
        Me.gbMedicalCategory.Controls.Add(Me.lblAmount)
        Me.gbMedicalCategory.Controls.Add(Me.nudDependants)
        Me.gbMedicalCategory.Controls.Add(Me.lblMaxDependants)
        Me.gbMedicalCategory.Controls.Add(Me.lblDescription)
        Me.gbMedicalCategory.Controls.Add(Me.txtDescription)
        Me.gbMedicalCategory.ExpandedHoverImage = Nothing
        Me.gbMedicalCategory.ExpandedNormalImage = Nothing
        Me.gbMedicalCategory.ExpandedPressedImage = Nothing
        Me.gbMedicalCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMedicalCategory.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMedicalCategory.HeaderHeight = 25
        Me.gbMedicalCategory.HeightOnCollapse = 0
        Me.gbMedicalCategory.LeftTextSpace = 0
        Me.gbMedicalCategory.Location = New System.Drawing.Point(13, 67)
        Me.gbMedicalCategory.Name = "gbMedicalCategory"
        Me.gbMedicalCategory.OpenHeight = 300
        Me.gbMedicalCategory.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMedicalCategory.ShowBorder = True
        Me.gbMedicalCategory.ShowCheckBox = False
        Me.gbMedicalCategory.ShowCollapseButton = False
        Me.gbMedicalCategory.ShowDefaultBorderColor = True
        Me.gbMedicalCategory.ShowDownButton = False
        Me.gbMedicalCategory.ShowHeader = True
        Me.gbMedicalCategory.Size = New System.Drawing.Size(375, 199)
        Me.gbMedicalCategory.TabIndex = 1
        Me.gbMedicalCategory.Temp = 0
        Me.gbMedicalCategory.Text = "Medical Category"
        Me.gbMedicalCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchMaritalStatus
        '
        Me.objbtnSearchMaritalStatus.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMaritalStatus.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMaritalStatus.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMaritalStatus.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMaritalStatus.BorderSelected = False
        Me.objbtnSearchMaritalStatus.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMaritalStatus.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMaritalStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMaritalStatus.Location = New System.Drawing.Point(284, 115)
        Me.objbtnSearchMaritalStatus.Name = "objbtnSearchMaritalStatus"
        Me.objbtnSearchMaritalStatus.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMaritalStatus.TabIndex = 218
        '
        'cboMaritalStatus
        '
        Me.cboMaritalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMaritalStatus.DropDownWidth = 150
        Me.cboMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMaritalStatus.FormattingEnabled = True
        Me.cboMaritalStatus.IntegralHeight = False
        Me.cboMaritalStatus.Location = New System.Drawing.Point(122, 115)
        Me.cboMaritalStatus.Name = "cboMaritalStatus"
        Me.cboMaritalStatus.Size = New System.Drawing.Size(150, 21)
        Me.cboMaritalStatus.TabIndex = 216
        '
        'LblMaritalStatus
        '
        Me.LblMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMaritalStatus.Location = New System.Drawing.Point(8, 117)
        Me.LblMaritalStatus.Name = "LblMaritalStatus"
        Me.LblMaritalStatus.Size = New System.Drawing.Size(108, 18)
        Me.LblMaritalStatus.TabIndex = 217
        Me.LblMaritalStatus.Text = "Marital Status"
        '
        'objbtnAddCategory
        '
        Me.objbtnAddCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCategory.BorderSelected = False
        Me.objbtnAddCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCategory.Location = New System.Drawing.Point(284, 35)
        Me.objbtnAddCategory.Name = "objbtnAddCategory"
        Me.objbtnAddCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCategory.TabIndex = 214
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.DropDownWidth = 150
        Me.cboCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.IntegralHeight = False
        Me.cboCategory.Location = New System.Drawing.Point(122, 35)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(150, 21)
        Me.cboCategory.TabIndex = 1
        '
        'lblCategory
        '
        Me.lblCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategory.Location = New System.Drawing.Point(8, 36)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(108, 18)
        Me.lblCategory.TabIndex = 96
        Me.lblCategory.Text = "Medical Category"
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(122, 88)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(150, 21)
        Me.txtAmount.TabIndex = 3
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(8, 89)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(108, 18)
        Me.lblAmount.TabIndex = 10
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDependants
        '
        Me.nudDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDependants.Location = New System.Drawing.Point(122, 61)
        Me.nudDependants.Name = "nudDependants"
        Me.nudDependants.Size = New System.Drawing.Size(57, 21)
        Me.nudDependants.TabIndex = 2
        Me.nudDependants.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxDependants
        '
        Me.lblMaxDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxDependants.Location = New System.Drawing.Point(8, 62)
        Me.lblMaxDependants.Name = "lblMaxDependants"
        Me.lblMaxDependants.Size = New System.Drawing.Size(108, 18)
        Me.lblMaxDependants.TabIndex = 7
        Me.lblMaxDependants.Text = "Dependants"
        Me.lblMaxDependants.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 144)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(108, 18)
        Me.lblDescription.TabIndex = 6
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(122, 143)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(243, 48)
        Me.txtDescription.TabIndex = 4
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(400, 61)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Medical Category"
        '
        'o
        '
        Me.o.BorderColor = System.Drawing.Color.Silver
        Me.o.Controls.Add(Me.btnSave)
        Me.o.Controls.Add(Me.btnClose)
        Me.o.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.o.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.o.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.o.GradientColor1 = System.Drawing.SystemColors.Control
        Me.o.GradientColor2 = System.Drawing.SystemColors.Control
        Me.o.Location = New System.Drawing.Point(0, 274)
        Me.o.Name = "o"
        Me.o.Size = New System.Drawing.Size(400, 55)
        Me.o.TabIndex = 10
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(188, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(291, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmAssignCategory_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 329)
        Me.Controls.Add(Me.o)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssignCategory_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assign Medical Category"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbMedicalCategory.ResumeLayout(False)
        Me.gbMedicalCategory.PerformLayout()
        CType(Me.nudDependants, System.ComponentModel.ISupportInitialize).EndInit()
        Me.o.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents o As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbMedicalCategory As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMaxDependants As System.Windows.Forms.Label
    Friend WithEvents nudDependants As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnAddCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents cboMaritalStatus As System.Windows.Forms.ComboBox
    Friend WithEvents LblMaritalStatus As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchMaritalStatus As eZee.Common.eZeeGradientButton
End Class
