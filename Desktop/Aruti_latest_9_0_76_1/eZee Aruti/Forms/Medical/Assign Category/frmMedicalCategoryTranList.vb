﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmMedicalCategoryTranList

#Region "Private Variable"

    Private objMedicalCategoryMaster As clsmedical_category_master
    Dim objExchangeRate As clsExchangeRate
    Private ReadOnly mstrModuleName As String = "frmMedicalCategoryTranList"

#End Region

#Region "Form's Event"

    Private Sub frmMedicalCategoryTranList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMedicalCategoryMaster = New clsmedical_category_master
        objExchangeRate = New clsExchangeRate
        Try
            Call Set_Logo(Me, gApplicationType)
            objExchangeRate._ExchangeRateunkid = 1
            Language.setLanguage(Me.Name)

            Call SetVisibility()

            FillMedicalCategory()
            fillList()
            If lvMedicalCategory.Items.Count > 0 Then lvMedicalCategory.Items(0).Selected = True
            lvMedicalCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalCategoryTranList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalCategoryTranList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            'If e.KeyCode = Keys.Delete  And lvMedicalCategory.Focused = True  Then
            '    btnDelete_Click(sender, e)
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalCategoryTranList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalCategoryTranList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalCategoryTranList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalCategoryTranList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMedicalCategoryMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsmedical_category_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_category_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmAssignCategory_AddEdit
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvMedicalCategory.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Medical Category from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvMedicalCategory.Select()
            Exit Sub
        End If
        Dim objfrmAssignCategory_AddEdit As New frmAssignCategory_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMedicalCategory.SelectedItems(0).Index
            If objfrmAssignCategory_AddEdit.displayDialog(CInt(lvMedicalCategory.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frmAssignCategory_AddEdit = Nothing
            If lvMedicalCategory.Items.Count > 0 Then
                lvMedicalCategory.Items(intSelectedIndex).Selected = True
                lvMedicalCategory.EnsureVisible(intSelectedIndex)
            End If
            lvMedicalCategory.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmAssignCategory_AddEdit IsNot Nothing Then objfrmAssignCategory_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvMedicalCategory.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Medical Category from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvMedicalCategory.Select()
            Exit Sub
        End If
        'If objMedicalCategoryMaster.isUsed(CInt(lvMedicalCategory.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Medical Category. Reason: This Medical Category is in use."), enMsgBoxStyle.Information) '?2
        '    lvMedicalCategory.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMedicalCategory.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Medical Category?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objMedicalCategoryMaster.Delete(CInt(lvMedicalCategory.SelectedItems(0).Tag))
                lvMedicalCategory.SelectedItems(0).Remove()

                If lvMedicalCategory.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvMedicalCategory.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvMedicalCategory.Items.Count - 1
                    lvMedicalCategory.Items(intSelectedIndex).Selected = True
                    lvMedicalCategory.EnsureVisible(intSelectedIndex)
                ElseIf lvMedicalCategory.Items.Count <> 0 Then
                    lvMedicalCategory.Items(intSelectedIndex).Selected = True
                    lvMedicalCategory.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvMedicalCategory.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (09-Sep-2014) -- Start
    'Medical Enhancement - Reports & forms for FDRC

    Private Sub btnMapProvider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMapProvider.Click
        Try
            If lvMedicalCategory.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Medical Category From the list to map with service provider."), enMsgBoxStyle.Information) '?1
                lvMedicalCategory.Select()
                Exit Sub
            End If

            If lvMedicalCategory.SelectedItems.Count > 0 Then
                Dim objMapping As New frmCategory_ProviderMapping
                objMapping.displayDialog(-1, enAction.ADD_ONE, CInt(lvMedicalCategory.SelectedItems(0).Tag))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMapProvider_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (09-Sep-2014) -- End


    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCategory.SelectedIndex = 0
            txtAmountFrom.Text = ""
            txtAmountTo.Text = ""
            txtDependantsFrom.Text = ""
            txtDependantsTo.Text = ""
            txtDescription.Text = ""
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


#End Region

#Region " Private Methods "

    Private Sub FillMedicalCategory()
        Try

            Dim objMedicalMaster As New clsmedical_master

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'Dim dsMedicalCategory As DataSet = objMedicalMaster.getListForCombo(CInt(enMedicalMasterType.Category), "Medical Category List", True)
            Dim dsMedicalCategory As DataSet = objMedicalMaster.getListForCombo(CInt(enMedicalMasterType.Medical_Category), "Medical Category List", True)
            'Pinkal (12-Oct-2011) -- End


            cboCategory.ValueMember = "mdmasterunkid"
            cboCategory.DisplayMember = "name"
            cboCategory.DataSource = dsMedicalCategory.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMedicalCategory", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsMedicalCategory As New DataSet
        Dim strSearching As String = ""
        Dim dtMedicalCategory As New DataTable
        Try

            If User._Object.Privilege._AllowToViewMedicalCategoryList = True Then                'Pinkal (02-Jul-2012) -- Start

            dsMedicalCategory = objMedicalCategoryMaster.GetList("MedicalCategory")

            If CInt(cboCategory.SelectedValue) > 0 Then
                strSearching = "AND mdcategorymasterunkid =" & CInt(cboCategory.SelectedValue) & " "
            End If

            If txtDependantsFrom.Decimal > 0 Then
                strSearching &= "AND dependants_no >=" & txtDependantsFrom.Decimal & " "
            End If

            If txtDependantsTo.Decimal > 0 Then
                strSearching &= "AND dependants_no <=" & txtDependantsTo.Decimal & " "
            End If

            'Sohail (17 Jun 2011) -- Start
            'If txtAmountFrom.Text.Trim.Length > 0 Then
            '    strSearching &= "AND amount >=" & CDec(txtAmountFrom.Text.Trim) & " "
            'End If
            If txtAmountFrom.Decimal <> 0 Then
                strSearching &= "AND amount >=" & txtAmountFrom.Decimal & " "
            End If
            'Sohail (17 Jun 2011) -- End

            'Sohail (17 Jun 2011) -- Start
            'If txtAmountTo.Text.Trim.Length > 0 Then
            '    strSearching &= "AND amount <=" & CDec(txtAmountTo.Text.Trim) & " "
            'End If
            If txtAmountTo.Decimal <> 0 Then
                strSearching &= "AND amount <=" & txtAmountTo.Decimal & " "
            End If
            'Sohail (17 Jun 2011) -- End

            If txtDescription.Text.Trim.Length > 0 Then
                strSearching &= "AND description like '" & txtDescription.Text.Trim & "%'"
            End If


            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtMedicalCategory = New DataView(dsMedicalCategory.Tables("MedicalCategory"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtMedicalCategory = dsMedicalCategory.Tables("MedicalCategory")
            End If


            Dim lvItem As ListViewItem

            lvMedicalCategory.Items.Clear()
            For Each drRow As DataRow In dtMedicalCategory.Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("mdmastercode").ToString
                lvItem.Tag = drRow("medicalcategoryunkid")
                lvItem.SubItems.Add(drRow("mdmastername").ToString)
                lvItem.SubItems.Add(drRow("dependants_no").ToString)
                lvItem.SubItems.Add(Format(CDec(drRow("amount")), objExchangeRate._fmtCurrency))
                lvItem.SubItems.Add(drRow("description").ToString)
                lvMedicalCategory.Items.Add(lvItem)
            Next

            If lvMedicalCategory.Items.Count > 16 Then
                colhDescription.Width = 355 - 18
            Else
                colhDescription.Width = 355
            End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsMedicalCategory.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddAssignMedicalCategory
            btnEdit.Enabled = User._Object.Privilege._EditAssignMedicalCategory
            btnDelete.Enabled = User._Object.Privilege._DeleteAssignMedicalCategory

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblDependants.Text = Language._Object.getCaption(Me.lblDependants.Name, Me.lblDependants.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)
			Me.lblCoverAmountTo.Text = Language._Object.getCaption(Me.lblCoverAmountTo.Name, Me.lblCoverAmountTo.Text)
			Me.colhMaxDependants.Text = Language._Object.getCaption(CStr(Me.colhMaxDependants.Tag), Me.colhMaxDependants.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhCoverAmount.Text = Language._Object.getCaption(CStr(Me.colhCoverAmount.Tag), Me.colhCoverAmount.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblCoverAmountFrom.Text = Language._Object.getCaption(Me.lblCoverAmountFrom.Name, Me.lblCoverAmountFrom.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Medical Category from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Medical Category?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>


End Class