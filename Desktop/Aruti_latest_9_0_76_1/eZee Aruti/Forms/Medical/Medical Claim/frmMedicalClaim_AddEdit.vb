﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 15


Public Class frmMedicalClaim_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMedicalClaim_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objMedicalclaim_master As clsmedical_claim_master
    Private objMedicalclaim_tran As clsmedical_claim_Tran
    Private mintMedicalclaimUnkid As Integer = -1
    Private mdtTran As DataTable

    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    Private mdtSicksheetDate As DateTime = Nothing
    'Pinkal (21-Jul-2014) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMedicalclaimUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintMedicalclaimUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboDependants.BackColor = GUI.ColorOptional
            cboPeriod.BackColor = GUI.ColorComp
            txtSickSheetNo.BackColor = GUI.ColorComp
            txtClaimNo.BackColor = GUI.ColorOptional
            txtClaimAmount.BackColor = GUI.ColorComp
            cboInstitution.BackColor = GUI.ColorComp
            txtInvoiceNo.BackColor = GUI.ColorComp
            txtTotInvoiceAmt.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboPeriod.SelectedValue = objMedicalclaim_master._Periodunkid
            cboInstitution.SelectedValue = objMedicalclaim_master._Instituteunkid
            If menAction = enAction.EDIT_ONE Then
                cboInstitution.Enabled = False
                objbtnAddInstitution.Enabled = False

                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                objbtnSearchInstitution.Enabled = False
                'Pinkal (21-Jul-2014) -- End

                'Accroding to Mr.Andrew Invoice No is Writeable
                'txtInvoiceNo.ReadOnly = True
            End If

            txtInvoiceNo.Text = objMedicalclaim_master._InvoiceNo
            txtTotInvoiceAmt.Text = Format(CDec(objMedicalclaim_master._TotalInvoiceAmount), GUI.fmtCurrency)


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            If objMedicalclaim_master._InvoiceDate = Nothing Then
                dtpInvoicedate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            Else
                dtpInvoicedate.Value = objMedicalclaim_master._InvoiceDate.Date
            End If
            'Pinkal (18-Dec-2012) -- End

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objMedicalclaim_master._Instituteunkid = CInt(cboInstitution.SelectedValue)
            objMedicalclaim_master._Periodunkid = CInt(cboPeriod.SelectedValue)
            objMedicalclaim_master._InvoiceNo = txtInvoiceNo.Text
            objMedicalclaim_master._TotalInvoiceAmount = txtTotInvoiceAmt.Decimal
            objMedicalclaim_master._Userunkid = User._Object._Userunkid
            objMedicalclaim_master._Statusunkid = 1  'Pending

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            objMedicalclaim_master._InvoiceDate = dtpInvoicedate.Value
            'Pinkal (18-Dec-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            'FOR EMPLOYEE 
            'Dim objEmployee As New clsEmployee_Master

            'If menAction = enAction.EDIT_ONE Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            'cboEmployee.DisplayMember = "employeename"
            'cboEmployee.ValueMember = "employeeunkid"
            'cboEmployee.DataSource = dsFill.Tables("Employee")

            'Pinkal (21-Jul-2014) -- End

            'FOR YEAR
            dsFill = Nothing
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.DisplayMember = "name"
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DataSource = dsFill.Tables("Period")

            'RemoveHandler cboInstitution.SelectedIndexChanged, AddressOf cboInstitution_SelectedIndexChanged

            'FOR MEDICAL INSTITUTE
            dsFill = Nothing
            Dim objInstitute As New clsinstitute_master

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            'dsFill = objInstitute.getListForCombo(True, "Institute", True)
            dsFill = objInstitute.getListForCombo(True, "Institute", True, 1)
            'Pinkal (20-Jan-2012) -- End


            cboInstitution.DisplayMember = "name"
            cboInstitution.ValueMember = "instituteunkid"
            cboInstitution.DataSource = dsFill.Tables("Institute")

            'AddHandler cboInstitution.SelectedIndexChanged, AddressOf cboInstitution_SelectedIndexChanged


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Return False

            ElseIf CInt(cboPeriod.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Pay Period is compulsory information.Please Select Pay Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False

            ElseIf CInt(cboInstitution.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Provider is compulsory information.Please Select Provider."), enMsgBoxStyle.Information)
                cboInstitution.Select()
                Return False

            ElseIf txtSickSheetNo.Enabled = True AndAlso txtSickSheetNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sick Sheet No cannot be blank.Sick Sheet No is required information."), enMsgBoxStyle.Information)
                txtSickSheetNo.Select()
                Return False
          

            ElseIf txtClaimAmount.Text.Trim = "" Or txtClaimAmount.Decimal = 0 Then                 'Pinkal (07-Jan-2012) -- Start
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Claim Amount cannot be blank.Claim Amount is required information."), enMsgBoxStyle.Information)
                txtClaimAmount.Select()
                Return False
            
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub FillList()
        Try
            If Not mdtTran Is Nothing Then
                lvClaim.Items.Clear()
                Dim lvitem As ListViewItem
                For Each drRow As DataRow In mdtTran.Rows
                    If CStr(IIf(IsDBNull(drRow.Item("AUD")), "A", drRow.Item("AUD"))) <> "D" Then
                        lvitem = New ListViewItem
                        lvitem.Text = drRow("employeename").ToString
                        lvitem.Tag = CInt(drRow("claimtranunkid"))
                        lvitem.SubItems.Add(drRow("dependentsname").ToString)
                        'lvitem.SubItems.Add(drRow("period_name").ToString)
                        lvitem.SubItems.Add(drRow("sicksheetno").ToString)
                        lvitem.SubItems.Add(CDate(drRow("claimdate")).ToShortDateString)
                        lvitem.SubItems.Add(drRow("claimno").ToString)
                        lvitem.SubItems.Add(Format(CDec(drRow("amount")), GUI.fmtCurrency))
                        lvitem.SubItems.Add(drRow("GUID").ToString)
                        lvitem.SubItems.Add(drRow("employeeunkid").ToString)
                        lvitem.SubItems.Add(drRow("dependantsunkid").ToString)
                        'lvitem.SubItems.Add(drRow("periodunkid").ToString)
                        lvitem.SubItems.Add(drRow("sicksheetunkid").ToString)
                        lvitem.SubItems.Add(drRow("remarks").ToString)

                        'Pinkal (18-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                        lvitem.SubItems.Add(drRow("isempexempted").ToString())
                        'Pinkal (18-Dec-2012) -- End

                        lvClaim.Items.Add(lvitem)
                    End If
                Next
                If lvClaim.Items.Count > 10 Then
                    colhAmount.Width = 180 - 18
                Else
                    colhAmount.Width = 180
                End If
                lvClaim.GroupingColumn = colhEmployee
                lvClaim.DisplayGroups(True)

                If mdtTran.Rows.Count > 0 Then
                    Dim dtrow As DataRow() = mdtTran.Select("AUD <> 'D'")
                    Dim dtTable As DataTable = mdtTran.Clone
                    If dtrow.Length > 0 Then
                        For i As Integer = 0 To dtrow.Length - 1
                            dtTable.ImportRow(dtrow(i))
                        Next
                        txtTotalAmountVal.Text = Format(dtTable.Compute("sum(amount)", "1=1"), GUI.fmtCurrency)
                    Else
                        txtSickSheetNo.Text = ""
                        txtClaimNo.Text = ""
                        txtRemarks.Text = ""
                        If cboDependants.Items.Count > 0 Then cboDependants.SelectedIndex = 0
                        If cboPeriod.Items.Count > 0 Then cboPeriod.SelectedIndex = 0
                        txtClaimAmount.Text = Format("0", GUI.fmtCurrency)
                        txtTotalAmountVal.Text = Format("0", GUI.fmtCurrency)

                        'Pinkal (18-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                        chkEmpexempted.Checked = False
                        'Pinkal (18-Dec-2012) -- End

                    End If
                End If

                cboEmployee.Select()

                If menAction = enAction.ADD_CONTINUE Then
                    txtClaimNo.Text = ""
                    txtClaimAmount.Decimal = 0
                    If cboDependants.Items.Count > 0 Then cboDependants.SelectedIndex = 0

                ElseIf menAction = enAction.EDIT_ONE Then

                    'Pinkal (21-Jul-2014) -- Start
                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                    'If lvClaim.Items.Count > 0 Then
                    '    lvClaim.Items(0).Selected = True
                    'End If
                    'Pinkal (21-Jul-2014) -- End
                    cboEmployee.Enabled = True
                    objbtnSearchEmployee.Enabled = True


                    'Pinkal (21-Jul-2014) -- Start
                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                    'If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
                    'If cboDependants.Items.Count > 0 Then cboDependants.SelectedIndex = 0
                    'txtSickSheetNo.Text = ""
                    txtClaimNo.Text = ""
                    txtRemarks.Text = ""
                    txtClaimAmount.Text = Format("0", GUI.fmtCurrency)

                    If lvClaim.Items.Count > 0 AndAlso lvClaim.Items(0).SubItems(colhSickSheetNo.Index).Text.Trim.Length > 0 Then
                        txtSickSheetNo.Text = lvClaim.Items(0).SubItems(colhSickSheetNo.Index).Text.Trim
                        txtSickSheetNo_Validated(New Object(), New EventArgs())
                    End If

                    If lvClaim.Items.Count > 0 AndAlso CInt(lvClaim.Items(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim) > 0 Then
                        cboEmployee.SelectedValue = CInt(lvClaim.Items(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim)
                    End If

                    dtpClaimDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    'Pinkal (21-Jul-2014) -- End

                    'Pinkal (18-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                    chkEmpexempted.Checked = False
                    'Pinkal (18-Dec-2012) -- End

                End If

            End If
            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If lvClaim.Items.Count > 0 Then
                dtpInvoicedate.Enabled = False
            Else
                dtpInvoicedate.Enabled = True
            End If

            'Pinkal (18-Dec-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AssignListValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddInstitution.Enabled = User._Object.Privilege._AddMedicalInstitutes
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            btnSave.Enabled = User._Object.Privilege._AllowToSaveMedicalClaim
            btnFinalSave.Enabled = User._Object.Privilege._AllowToFinalSaveMedicalClaim
            'S.SANDEEP [ 16 MAY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Function IsValid(ByVal isEdit As Boolean) As Boolean
        Try

            'START FOR CHECK UNIQUE CLAIM NO

            Dim drClaimno As DataRow() = Nothing


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If isEdit = False Then
                'drClaimno = mdtTran.Select("claimno = '" & txtClaimNo.Text.Trim & "' AND employeeunkid <>" & CInt(cboEmployee.SelectedValue) & " AND claimno <> '' AND AUD <> 'D'")
                drClaimno = mdtTran.Select("claimno = '" & txtClaimNo.Text.Trim & "' AND claimno <> '' AND AUD <> 'D'")
            Else
                'drClaimno = mdtTran.Select("claimno = '" & txtClaimNo.Text.Trim & "' AND employeeunkid <>" & CInt(cboEmployee.SelectedValue) & " AND claimno <> '' AND GUID <> '" & lvClaim.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                If menAction = enAction.ADD_CONTINUE Or menAction = enAction.ADD_ONE Then
                    drClaimno = mdtTran.Select("claimno = '" & txtClaimNo.Text.Trim & "' AND claimno <> '' AND GUID <> '" & lvClaim.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
            Else
                    drClaimno = mdtTran.Select("claimno = '" & txtClaimNo.Text.Trim & "' AND claimno <> '' AND AUD <> 'D' AND claimtranunkid <> " & CInt(lvClaim.SelectedItems(0).Tag))
                End If
            End If

            'Pinkal (07-Jan-2012) -- End 

            If drClaimno.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "This Claim No is already added to the list."), enMsgBoxStyle.Information)
                txtClaimNo.Select()
                Return False
            End If

            'END FOR CHECK UNIQUE CLAIM NO


            'START FOR CHECK UNIQUE SICKSHEET NO

            If txtSickSheetNo.Enabled Then

                Dim objSickSheet As New clsmedical_sicksheet

                If objSickSheet.isExist(txtSickSheetNo.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Invalid Sick Sheet No."), enMsgBoxStyle.Information)
                    txtClaimNo.Select()
                    Return False

                ElseIf objSickSheet.isExist(txtSickSheetNo.Text.Trim, -1, CInt(cboEmployee.SelectedValue)) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This Sick Sheet No is not valid for this employee."), enMsgBoxStyle.Information)
                    txtClaimNo.Select()
                    Return False

                ElseIf objSickSheet.isExist(txtSickSheetNo.Text.Trim, -1, -1, CInt(cboInstitution.SelectedValue)) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "This Sick Sheet No is not valid for this provider."), enMsgBoxStyle.Information)
                    txtSickSheetNo.Select()
                    Return False
                End If

                        End If

            'END FOR CHECK UNIQUE SICKSHEET NO

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub ClearControlValue()
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
            If cboDependants.Items.Count > 0 Then cboDependants.SelectedIndex = 0
            If cboPeriod.Items.Count > 0 Then cboPeriod.SelectedIndex = 0
            If cboInstitution.Items.Count > 0 Then cboInstitution.SelectedIndex = 0
            txtRemarks.Text = ""
            txtSickSheetNo.Text = ""
            txtClaimNo.Text = ""
            txtClaimAmount.Text = Format("0", GUI.fmtCurrency)
            txtInvoiceNo.Text = ""
            txtTotInvoiceAmt.Text = Format("0", GUI.fmtCurrency)
            txtTotalAmountVal.Text = Format("0", GUI.fmtCurrency)
            dtpClaimDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            dtpInvoicedate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            chkEmpexempted.Checked = False
            'Pinkal (18-Dec-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControlValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsEmployeeExistForthisInvoice(ByVal isEdit As Boolean) As Boolean
        Try

            If mdtTran.Rows.Count > 0 Then

                Dim drRow As DataRow() = Nothing
                Dim strSearching As String = ""

                If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboDependants.SelectedValue) <= 0 Then
                    strSearching = "employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND sicksheetno = '" & txtSickSheetNo.Text.Trim & "' AND dependantsunkid <= 0 AND AUD <>'D'"

                ElseIf CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboDependants.SelectedValue) > 0 Then
                    strSearching = "employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND sicksheetno = '" & txtSickSheetNo.Text.Trim & "' AND dependantsunkid =  " & CInt(cboDependants.SelectedValue) & " AND AUD <> 'D'"

                End If


                'Pinkal (07-Jan-2012) -- Start
                'Enhancement : TRA Changes
                strSearching &= " AND claimno ='" & txtClaimNo.Text.Trim & "' AND claimno <> ''"
                'Pinkal (07-Jan-2012) -- End


                If isEdit Then
                    strSearching &= " AND claimtranunkid <> " & CInt(lvClaim.SelectedItems(0).Tag)
                End If

                drRow = mdtTran.Select(strSearching, "")


                'Pinkal (07-Jan-2012) -- Start
                'Enhancement : TRA Changes

                'If drRow.Length > 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "This Employee has same Sick sheet No for this invoice.Please define another sick sheet no."))
                '    Return False
                'End If

                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "This Employee has same Claim No for this invoice.Please define another Claim no."))
                    Return False
                End If


                'Pinkal (07-Jan-2012) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsEmployeeExistForthisInvoice", mstrModuleName)
        End Try
        Return True
    End Function


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Save()
        Dim blnFlag As Boolean = False
        Try

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objMedicalclaim_master.Update(mdtTran)
            Else
                blnFlag = objMedicalclaim_master.Insert(mdtTran)
            End If

            If blnFlag = False And objMedicalclaim_master._Message <> "" Then
                eZeeMsgBox.Show(objMedicalclaim_master._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objMedicalclaim_master = Nothing
                    objMedicalclaim_master = New clsmedical_claim_master
                    objMedicalclaim_tran = Nothing
                    objMedicalclaim_tran = New clsmedical_claim_Tran
                    ClearControlValue()
                    mdtTran.Rows.Clear()
                    FillList()
                    cboEmployee.Select()
                Else
                    mintMedicalclaimUnkid = objMedicalclaim_master._Claimunkid
                    Me.Close()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Save", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End


#End Region

#Region " Form's Events "

    Private Sub frmMedicalClaim_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMedicalclaim_master = New clsmedical_claim_master
        objMedicalclaim_tran = New clsmedical_claim_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            SetColor()
            txtRemarks.SelectionMode = False
            txtRemarks.SelectionLength = 0
            txtClaimAmount.Text = Format("0", GUI.fmtCurrency)
            txtTotInvoiceAmt.Text = Format("0", GUI.fmtCurrency)
            txtTotalAmountVal.Text = Format("0", GUI.fmtCurrency)

            If menAction = enAction.EDIT_ONE Then
                objMedicalclaim_master._Claimunkid = mintMedicalclaimUnkid
                objMedicalclaim_tran._Claimunkid = mintMedicalclaimUnkid

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objMedicalclaim_tran.GetClaimTran()

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.

                'Dim objPeriod As New clscommom_period_Tran
                'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = objMedicalclaim_master._Periodunkid
                'mdtTran = objMedicalclaim_tran.GetClaimTran(FinancialYear._Object._DatabaseName, _
                '                                 User._Object._Userunkid, _
                '                                  FinancialYear._Object._YearUnkid, _
                '                                  Company._Object._Companyunkid, _
                '                                  objPeriod._Start_Date, _
                '                                  objPeriod._End_Date, _
                '                                  ConfigParameter._Object._UserAccessModeSetting, _
                '                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, mintMedicalclaimUnkid).Tables(0)

                mdtTran = objMedicalclaim_tran.GetClaimTran(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, mintMedicalclaimUnkid).Tables(0)

                'Pinkal (06-Jan-2016) -- End

                'Shani(24-Aug-2015) -- End

                BtnOperation.Enabled = False
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            mdtTran = objMedicalclaim_tran._DataList
            'Shani(24-Aug-2015) -- End
            FillCombo()
            GetValue()

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'cboEmployee.Select()
            cboInstitution.Select()
            'Pinkal (21-Jul-2014) -- End


            lvClaim.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaim_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalClaim_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                If txtRemarks.SelectionLength > 0 Then txtRemarks.SelectionStart = txtRemarks.Text.Trim.Length + 1 : txtRemarks.SelectedText = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaim_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalClaim_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)

                'Pinkal (20-Jan-2012) -- Start
                'Enhancement : TRA Changes

            ElseIf e.Control = True And e.KeyCode = Keys.F Then
                btnFinalSave_Click(sender, e)

                'Pinkal (20-Jan-2012) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaim_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalClaim_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMedicalclaim_master = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_claim_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_claim_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() Then

                If IsValid(False) = False Then Exit Sub

                If IsEmployeeExistForthisInvoice(False) = False Then Exit Sub

                Dim dtRow As DataRow = Nothing
                dtRow = mdtTran.NewRow
                dtRow("claimtranunkid") = -1
                dtRow("claimunkid") = mintMedicalclaimUnkid
                dtRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dtRow("employeename") = cboEmployee.Text
                dtRow("dependantsunkid") = IIf(CInt(cboDependants.SelectedValue) > 0, CInt(cboDependants.SelectedValue), -1)
                dtRow("dependentsname") = IIf(CInt(cboDependants.SelectedValue) > 0, cboDependants.Text, "")
                'dtRow("periodunkid") = CInt(cboPeriod.SelectedValue)
                'dtRow("period_name") = cboPeriod.Text
                dtRow("sicksheetunkid") = IIf(txtSickSheetNo.Tag Is Nothing, -1, CInt(txtSickSheetNo.Tag))
                dtRow("sicksheetno") = txtSickSheetNo.Text
                dtRow("claimno") = txtClaimNo.Text.Trim
                dtRow("claimdate") = dtpClaimDate.Value.Date
                dtRow("amount") = txtClaimAmount.Text.Trim
                dtRow("remarks") = txtRemarks.Text.Trim
                dtRow("AUD") = "A"
                dtRow("GUID") = Guid.NewGuid.ToString
                dtRow("isempexempted") = chkEmpexempted.Checked

                mdtTran.Rows.Add(dtRow)
                FillList()
                cboEmployee.Select()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvClaim.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Medical Claim from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvClaim.Select()
                Exit Sub
            End If
            If Validation() Then

                If IsValid(True) = False Then Exit Sub

                If IsEmployeeExistForthisInvoice(True) = False Then Exit Sub

                Dim drTemp As DataRow()
                If CInt(lvClaim.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvClaim.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtTran.Select("claimtranunkid = " & CInt(lvClaim.SelectedItems(0).Tag))
                End If

                If drTemp.Length > 0 Then

                    With drTemp(0)
                        .Item("claimtranunkid") = CInt(lvClaim.SelectedItems(0).Tag)
                        .Item("claimunkid") = mintMedicalclaimUnkid
                        .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                        .Item("employeename") = cboEmployee.Text
                        If CInt(cboDependants.SelectedValue) <= 0 Then
                            .Item("dependantsunkid") = -1
                            .Item("dependentsname") = ""
                        Else
                            .Item("dependantsunkid") = CInt(cboDependants.SelectedValue)
                            .Item("dependentsname") = cboDependants.Text
                        End If
                        '.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                        '.Item("period_name") = cboPeriod.Text
                        .Item("sicksheetunkid") = IIf(txtSickSheetNo.Tag Is Nothing, -1, CInt(txtSickSheetNo.Tag))
                        .Item("sicksheetno") = txtSickSheetNo.Text
                        .Item("claimno") = txtClaimNo.Text.Trim
                        .Item("claimdate") = dtpClaimDate.Value.Date
                        .Item("amount") = txtClaimAmount.Text.Trim
                        .Item("remarks") = txtRemarks.Text.Trim


                        'Pinkal (18-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                        .Item("isempexempted") = chkEmpexempted.Checked
                        'Pinkal (18-Dec-2012) -- End


                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        .AcceptChanges()
                    End With
                    FillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim index As Integer = 0
            If lvClaim.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Medical Claim from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvClaim.Select()
                Exit Sub
            End If
            Dim drTemp As DataRow() = Nothing
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Are you sure you want to delete this Medical Claim?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                If CInt(lvClaim.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvClaim.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                    index = lvClaim.SelectedItems(0).Index
                Else
                    drTemp = mdtTran.Select("claimtranunkid = " & CInt(lvClaim.SelectedItems(0).Tag))
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                End If
                FillList()

                'If index = 0 And lvClaim.Items.Count > 0 Then
                '    lvClaim.Items(index).Selected = True
                'ElseIf index > 1 Then
                '    lvClaim.Items(index - 1).Selected = True
                'End If


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If txtInvoiceNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invoice No cannot be blank.Invoice No is required information."), enMsgBoxStyle.Information)
                txtInvoiceNo.Select()
                Exit Sub

            ElseIf txtTotInvoiceAmt.Text.Trim = "" Or txtTotInvoiceAmt.Text = "0" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Total Invoice Amount cannot be blank.Total Invoice Amount is required information."), enMsgBoxStyle.Information)
                txtTotInvoiceAmt.Select()
                Exit Sub

            ElseIf lvClaim.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please Add atleast one Medical Claim."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes

            ElseIf CInt(cboPeriod.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Pay Period is compulsory information.Please Select Pay Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub
                'Pinkal (18-Dec-2012) -- End

            End If

            SetValue()

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            objMedicalclaim_master._IsFinal = False
            'Pinkal (20-Jan-2012) -- End

             Save()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click

        Try

            If txtInvoiceNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invoice No cannot be blank.Invoice No is required information."), enMsgBoxStyle.Information)
                txtInvoiceNo.Select()
                Exit Sub

            ElseIf txtTotInvoiceAmt.Text.Trim = "" Or txtTotInvoiceAmt.Text = "0" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Total Invoice Amount cannot be blank.Total Invoice Amount is required information."), enMsgBoxStyle.Information)
                txtTotInvoiceAmt.Select()
                Exit Sub

            ElseIf lvClaim.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please Add atleast one Medical Claim."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub

            ElseIf Format(txtTotInvoiceAmt.Decimal, GUI.fmtCurrency) <> Format(txtTotalAmountVal.Decimal, GUI.fmtCurrency) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Total Invoice Amout must be equal to total amount."), enMsgBoxStyle.Information)
                Exit Sub


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes

            ElseIf CInt(cboPeriod.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Pay Period is compulsory information.Please Select Pay Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub

                'Pinkal (18-Dec-2012) -- End

            End If

            SetValue()

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            objMedicalclaim_master._IsFinal = True
            'Pinkal (20-Jan-2012) -- End

            Save()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFinalSave_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End



    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

    Private Sub objbtnSearchInstitution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchInstitution.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboInstitution.DataSource, DataTable)
            objfrm.ValueMember = cboInstitution.ValueMember
            objfrm.DisplayMember = cboInstitution.DisplayMember
            If objfrm.DisplayDialog Then
                cboInstitution.SelectedValue = objfrm.SelectedValue
            End If
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchInstitution_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (21-Jul-2014) -- End

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
            objfrm.ValueMember = "employeeunkid"
            objfrm.DisplayMember = "employeename"
            objfrm.CodeMember = "employeecode"
            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
            End If
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCover.Click
        Try
            Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCover As New DataSet
            Dim objmedical As New clsmedical_master
            If objfrmMedicalMaster_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Cover) Then
                dsCover = objmedical.getListForCombo(enMedicalMasterType.Cover, "List", True)
                With cboCover
                    .ValueMember = "mdmasterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCover.Tables("List")
                End With
                cboCover.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddService_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddService.Click
        Try
            Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCover As New DataSet
            Dim objmedical As New clsmedical_master
            If objfrmMedicalMaster_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Service) Then
                dsCover = objmedical.getListForCombo(enMedicalMasterType.Service, "List", True)
                With cboServices
                    .ValueMember = "mdmasterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCover.Tables("List")
                End With
                cboServices.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddService_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddInstitution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInstitution.Click
        Try
            Dim objfrmTraningInstitutes_AddEdit As New frmTraningInstitutes_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCover As New DataSet
            Dim objInstitute As New clsinstitute_master


            objfrmTraningInstitutes_AddEdit.Text = Language.getMessage("frmTraningInstitutesList", 3, "Add/Edit Service Provider")
            objfrmTraningInstitutes_AddEdit.gbTaraningInstitutes.Text = Language.getMessage("frmTraningInstitutesList", 4, "Service Provider")
            objfrmTraningInstitutes_AddEdit.eZeeHeader.Title = Language.getMessage("frmTraningInstitutesList", 4, "Service Provider")
            'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment

            If objfrmTraningInstitutes_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, True) Then
                dsCover = objInstitute.getListForCombo(True, "List", True)
                With cboInstitution
                    .ValueMember = "instituteunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCover.Tables("List")
                End With
                cboInstitution.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddInstitution_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim dsFill As DataSet = Nothing
        Try

            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'Dim objEmployee As New clsEmployee_Master
            'dsFill = objEmployee.GetEmployee_Dependents(CInt(cboEmployee.SelectedValue), True, "Dependents")
            'cboDependants.ValueMember = "dependent_Id"
            'cboDependants.DisplayMember = "dependent_Name"
            'cboDependants.DataSource = dsFill.Tables("Dependents")

            Dim objDependant As New clsDependants_Beneficiary_tran

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'dsFill = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True)
            If CInt(txtSickSheetNo.Tag) > 0 Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsFill = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, mdtSicksheetDate.Date, True)
                dsFill = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, mdtSicksheetDate.Date, True, dtAsOnDate:=mdtSicksheetDate.Date)
                'Sohail (18 May 2019) -- End
            Else
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsFill = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, dtpInvoicedate.Value.Date, True)
                dsFill = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, dtpInvoicedate.Value.Date, True, dtAsOnDate:=dtpInvoicedate.Value.Date)
                'Sohail (18 May 2019) -- End
            End If
            'Pinkal (21-Jul-2014) -- End

            cboDependants.ValueMember = "dependent_Id"
            cboDependants.DisplayMember = "dependants"
            cboDependants.DataSource = dsFill.Tables(0)

            'Pinkal (07-Jan-2012) -- End    


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboInstitution_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboInstitution.SelectedIndexChanged
        Try
            txtSickSheetNo.Text = ""
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim strFilter As String = String.Empty
            Dim objEmployee As New clsEmployee_Master
            Dim dsFill As DataSet = Nothing

            'If CInt(cboInstitution.SelectedValue) > 0 Then

            '    Dim objProvider As New clsinstitute_master
            '    objProvider._Instituteunkid = CInt(cboInstitution.SelectedValue)

            '    If objProvider._IsFormRequire Then
            '        txtSickSheetNo.Enabled = True

            '        'Pinkal (21-Jul-2014) -- Start
            '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            '        Dim objEmployee As New clsEmployee_Master
            '        Dim dsFill As DataSet = Nothing
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , " employeeunkid = 0")
            '        cboEmployee.DisplayMember = "employeename"
            '        cboEmployee.ValueMember = "employeeunkid"
            '        cboEmployee.DataSource = dsFill.Tables("Employee")

            '        'Pinkal (21-Jul-2014) -- End

            '    Else
            '        txtSickSheetNo.Enabled = False

            '        'Pinkal (21-Jul-2014) -- Start
            '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            '        Dim objEmployee As New clsEmployee_Master
            '        Dim dsFill As DataSet = Nothing
            '        If menAction = enAction.EDIT_ONE Then
            '            dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '        Else
            '            dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            '        End If

            '        cboEmployee.DisplayMember = "employeename"
            '        cboEmployee.ValueMember = "employeeunkid"
            '        cboEmployee.DataSource = dsFill.Tables("Employee")

            '        'Pinkal (21-Jul-2014) -- End


            '    End If

            'ElseIf CInt(cboInstitution.SelectedValue) <= 0 Then

            '    'Pinkal (21-Jul-2014) -- Start
            '    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            '    Dim objEmployee As New clsEmployee_Master
            '    Dim dsFill As DataSet = Nothing
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , " employeeunkid = 0")
            '    cboEmployee.DisplayMember = "employeename"
            '    cboEmployee.ValueMember = "employeeunkid"
            '    cboEmployee.DataSource = dsFill.Tables("Employee")

            '    'Pinkal (21-Jul-2014) -- End
            'End If

            If CInt(cboInstitution.SelectedValue) > 0 Then
                Dim objProvider As New clsinstitute_master
                objProvider._Instituteunkid = CInt(cboInstitution.SelectedValue)
                If objProvider._IsFormRequire Then
                    txtSickSheetNo.Enabled = True
                    strFilter = "hremployee_master.employeeunkid = 0 "
                Else
                    txtSickSheetNo.Enabled = False
                    strFilter = ""
                End If
            ElseIf CInt(cboInstitution.SelectedValue) <= 0 Then
                strFilter = "hremployee_master.employeeunkid = 0 "
            End If
            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True, , , , , , , , , , , , , , , strFilter)
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsFill.Tables("Employee")
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboInstitution_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView's Event"

    Private Sub lvclaim_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvClaim.SelectedIndexChanged
        Try
            If lvClaim.SelectedItems.Count > 0 Then

                If menAction = enAction.EDIT_ONE Then
                    cboEmployee.Enabled = False
                    objbtnSearchEmployee.Enabled = False
            Else
                    cboEmployee.Enabled = True
                    objbtnSearchEmployee.Enabled = True
                End If


                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

                'cboEmployee.SelectedValue = lvClaim.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text
                'If CInt(lvClaim.SelectedItems(0).SubItems(objcolhDependantunkid.Index).Text) > 0 Then
                '    cboDependants.SelectedValue = lvClaim.SelectedItems(0).SubItems(objcolhDependantunkid.Index).Text
                'Else
                '    cboDependants.SelectedValue = 0
                'End If

                txtSickSheetNo.Text = lvClaim.SelectedItems(0).SubItems(colhSickSheetNo.Index).Text

                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                txtSickSheetNo_Validated(New Object(), New EventArgs())
                'Pinkal (21-Jul-2014) -- End

                'cboPeriod.SelectedValue = lvClaim.SelectedItems(0).SubItems(objcolhperiodunkid.Index).Text
                dtpClaimDate.Value = CDate(lvClaim.SelectedItems(0).SubItems(colhClaimDate.Index).Text)

                cboEmployee.SelectedValue = lvClaim.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text
                If CInt(lvClaim.SelectedItems(0).SubItems(objcolhDependantunkid.Index).Text) > 0 Then
                    cboDependants.SelectedValue = lvClaim.SelectedItems(0).SubItems(objcolhDependantunkid.Index).Text
                Else
                    cboDependants.SelectedValue = 0
                End If

                'Pinkal (21-Jul-2014) -- End

                txtClaimNo.Text = lvClaim.SelectedItems(0).SubItems(colhClaimNo.Index).Text
                txtClaimAmount.Decimal = CDec(lvClaim.SelectedItems(0).SubItems(colhAmount.Index).Text)
                txtRemarks.Text = lvClaim.SelectedItems(0).SubItems(objcolhRemarks.Index).Text.Trim


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                chkEmpexempted.Checked = CBool(lvClaim.SelectedItems(0).SubItems(objColhEmpexempt.Index).Text.Trim)
                'Pinkal (18-Dec-2012) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvclaim_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtTotInvoiceAmt_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotInvoiceAmt.Validated, txtClaimAmount.Validated
        Try

            CType(sender, eZee.TextBox.NumericTextBox).Text = Format(CType(sender, eZee.TextBox.NumericTextBox).Decimal, GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtTotInvoiceAmt_Validated", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSickSheetNo_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSickSheetNo.Validated
        Try
            If txtSickSheetNo.Text.Trim.Length > 0 Then
                Dim objsickSheet As New clsmedical_sicksheet

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsList As DataSet = objsickSheet.GetList("List", True, txtSickSheetNo.Text.Trim)
                Dim dsList As DataSet = objsickSheet.GetList(FinancialYear._Object._DatabaseName, _
                                                             User._Object._Userunkid, _
                                                             FinancialYear._Object._YearUnkid, _
                                                             Company._Object._Companyunkid, _
                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                             True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                             "List", True, txtSickSheetNo.Text.Trim)
                'Shani(24-Aug-2015) -- End
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    txtSickSheetNo.Tag = dsList.Tables(0).Rows(0)("sicksheetunkid").ToString()

                    'Pinkal (21-Jul-2014) -- Start
                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                    Dim mintEmployeeId As Integer = -1
                    Dim mintServiceProviderId As Integer = -1

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim dsSicksheet As DataSet = objsickSheet.GetList("List", True, txtSickSheetNo.Text.Trim)


                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.

                    'Dim dsSicksheet As DataSet = objsickSheet.GetList(FinancialYear._Object._DatabaseName, _
                    '                                                  User._Object._Userunkid, _
                    '                                                  FinancialYear._Object._YearUnkid, _
                    '                                                  Company._Object._Companyunkid, _
                    '                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                                                  ConfigParameter._Object._UserAccessModeSetting, _
                    '                                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                    '                                                  "List", True, txtSickSheetNo.Text.Trim)


                    'mdtSicksheetDate = Nothing
                    'If dsSicksheet IsNot Nothing AndAlso dsSicksheet.Tables(0).Rows.Count > 0 Then
                    '    mintEmployeeId = CInt(dsSicksheet.Tables(0).Rows(0)("employeeunkid"))
                    '    mintServiceProviderId = CInt(dsSicksheet.Tables(0).Rows(0)("instituteunkid"))
                    '    mdtSicksheetDate = eZeeDate.convertDate((dsSicksheet.Tables(0).Rows(0)("sicksheetdate").ToString())).Date
                    'End If

                    mdtSicksheetDate = Nothing
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        mintEmployeeId = CInt(dsList.Tables(0).Rows(0)("employeeunkid"))
                        mintServiceProviderId = CInt(dsList.Tables(0).Rows(0)("instituteunkid"))
                        mdtSicksheetDate = eZeeDate.convertDate((dsList.Tables(0).Rows(0)("sicksheetdate").ToString())).Date
                    End If

                    'Pinkal (06-Jan-2016) -- End

                    'Shani(24-Aug-2015) -- End

                

                    If mintServiceProviderId = CInt(cboInstitution.SelectedValue) Then
                        Dim objEmployee As New clsEmployee_Master
                        Dim dsFill As DataSet = Nothing

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If menAction = enAction.EDIT_ONE Then
                        '    dsFill = objEmployee.GetEmployeeList("Employee", True, True, mintEmployeeId, , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                        'Else
                        '    dsFill = objEmployee.GetEmployeeList("Employee", True, , mintEmployeeId, , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
                        'End If
                        dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, True, "Employee", True, mintEmployeeId)
                        'S.SANDEEP [04 JUN 2015] -- END

                        cboEmployee.DisplayMember = "employeename"
                        cboEmployee.ValueMember = "employeeunkid"
                        cboEmployee.DataSource = dsFill.Tables("Employee")

                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "This Sick Sheet No is not valid for this provider."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "This Sick Sheet No is not valid for this provider."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                    'Pinkal (21-Jul-2014) -- End
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSickSheetNo_Validated", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ContextMenu Event(s)"

    Private Sub btnImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        Try

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If CInt(cboInstitution.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Provider is compulsory information.Please Select Provider."), enMsgBoxStyle.Information)
                cboInstitution.Select()
                Exit Sub
            End If

            'Pinkal (18-Dec-2012) -- End


            Dim objImport As New frmImport_MedicalClaim
            If objImport.ShowDialog = Windows.Forms.DialogResult.OK Then
                mdtTran = objImport._mdtClaim
                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImportData_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetFileFormat.Click
        Try
            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim objIExcel As New ExcelData
                'Dim objMst As New clsmedical_claim_master
                'Dim dsList As DataSet = objMst.GetImportFileStructure()
                'objIExcel.Export(objSave.FileName, dsList)
                Dim objMst As New clsmedical_claim_master
                Dim dsList As DataSet = objMst.GetImportFileStructure()
                OpenXML_Export(objSave.FileName, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnGetFileFormat_Click", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

#Region "Datepicker Event"

    Private Sub dtpInvoicedate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpInvoicedate.ValueChanged
        Try
            cboEmployee_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpInvoicedate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (21-Jul-2014) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
            Me.gbClaimInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbClaimInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbTreatment.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTreatment.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.BtnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.BtnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.Name, Me.btnFinalSave.Text)
            Me.gbClaimInfo.Text = Language._Object.getCaption(Me.gbClaimInfo.Name, Me.gbClaimInfo.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblDependants.Text = Language._Object.getCaption(Me.lblDependants.Name, Me.lblDependants.Text)
			Me.lblServiceNo.Text = Language._Object.getCaption(Me.lblServiceNo.Name, Me.lblServiceNo.Text)
            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
			Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.Name, Me.lblClaimNo.Text)
			Me.lblClaimDate.Text = Language._Object.getCaption(Me.lblClaimDate.Name, Me.lblClaimDate.Text)
			Me.lblMedicalYear.Text = Language._Object.getCaption(Me.lblMedicalYear.Name, Me.lblMedicalYear.Text)
			Me.lblCover.Text = Language._Object.getCaption(Me.lblCover.Name, Me.lblCover.Text)
			Me.lblInstutution.Text = Language._Object.getCaption(Me.lblInstutution.Name, Me.lblInstutution.Text)
			Me.lblTreatmentDate.Text = Language._Object.getCaption(Me.lblTreatmentDate.Name, Me.lblTreatmentDate.Text)
			Me.lblClaimAmount.Text = Language._Object.getCaption(Me.lblClaimAmount.Name, Me.lblClaimAmount.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhDependants.Text = Language._Object.getCaption(CStr(Me.colhDependants.Tag), Me.colhDependants.Text)
			Me.colhClaimNo.Text = Language._Object.getCaption(CStr(Me.colhClaimNo.Tag), Me.colhClaimNo.Text)
			Me.colhClaimDate.Text = Language._Object.getCaption(CStr(Me.colhClaimDate.Tag), Me.colhClaimDate.Text)
			Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
			Me.lblService.Text = Language._Object.getCaption(Me.lblService.Name, Me.lblService.Text)
			Me.gbTreatment.Text = Language._Object.getCaption(Me.gbTreatment.Name, Me.gbTreatment.Text)
			Me.colhTreatment.Text = Language._Object.getCaption(CStr(Me.colhTreatment.Tag), Me.colhTreatment.Text)
			Me.colhSelect.Text = Language._Object.getCaption(CStr(Me.colhSelect.Tag), Me.colhSelect.Text)
			Me.chkSelect.Text = Language._Object.getCaption(Me.chkSelect.Name, Me.chkSelect.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblTotAmt.Text = Language._Object.getCaption(Me.lblTotAmt.Name, Me.lblTotAmt.Text)
			Me.lblSicksheetNo.Text = Language._Object.getCaption(Me.lblSicksheetNo.Name, Me.lblSicksheetNo.Text)
			Me.lblTotInvoiceAmt.Text = Language._Object.getCaption(Me.lblTotInvoiceAmt.Name, Me.lblTotInvoiceAmt.Text)
			Me.lblInvoiceno.Text = Language._Object.getCaption(Me.lblInvoiceno.Name, Me.lblInvoiceno.Text)
			Me.colhSickSheetNo.Text = Language._Object.getCaption(CStr(Me.colhSickSheetNo.Tag), Me.colhSickSheetNo.Text)
			Me.BtnOperation.Text = Language._Object.getCaption(Me.BtnOperation.Name, Me.BtnOperation.Text)
			Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.Name, Me.btnImportData.Text)
			Me.btnGetFileFormat.Text = Language._Object.getCaption(Me.btnGetFileFormat.Name, Me.btnGetFileFormat.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.chkEmpexempted.Text = Language._Object.getCaption(Me.chkEmpexempted.Name, Me.chkEmpexempted.Text)
			Me.LblInvoiceDate.Text = Language._Object.getCaption(Me.LblInvoiceDate.Name, Me.LblInvoiceDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee.")
			Language.setMessage(mstrModuleName, 2, "Pay Period is compulsory information.Please Select Pay Period.")
			Language.setMessage(mstrModuleName, 3, "Provider is compulsory information.Please Select Provider.")
			Language.setMessage(mstrModuleName, 4, "Sick Sheet No cannot be blank.Sick Sheet No is required information.")
			Language.setMessage(mstrModuleName, 5, "Claim Amount cannot be blank.Claim Amount is required information.")
			Language.setMessage(mstrModuleName, 6, "Invoice No cannot be blank.Invoice No is required information.")
			Language.setMessage(mstrModuleName, 7, "Total Invoice Amount cannot be blank.Total Invoice Amount is required information.")
			Language.setMessage(mstrModuleName, 8, "This Claim No is already added to the list.")
			Language.setMessage(mstrModuleName, 9, "Invalid Sick Sheet No.")
			Language.setMessage(mstrModuleName, 10, "This Sick Sheet No is not valid for this employee.")
			Language.setMessage(mstrModuleName, 11, "Please select Medical Claim from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 12, "Please Add atleast one Medical Claim.")
			Language.setMessage(mstrModuleName, 13, "Are you sure you want to delete this Medical Claim?")
			Language.setMessage(mstrModuleName, 14, "Total Invoice Amout must be equal to total amount.")
			Language.setMessage(mstrModuleName, 15, "This Employee has same Claim No for this invoice.Please define another Claim no.")
			Language.setMessage(mstrModuleName, 16, "This Sick Sheet No is not valid for this provider.")
			Language.setMessage("frmTraningInstitutesList", 3, "Add/Edit Service Provider")
			Language.setMessage("frmTraningInstitutesList", 4, "Service Provider")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class


'Public Class frmMedicalClaim_AddEdit

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmMedicalClaim_AddEdit"
'    Private mblnCancel As Boolean = True
'    Private menAction As enAction = enAction.ADD_ONE
'    Private objMedicalclaim_master As clsmedical_claim_master
'    Private objMedicalclaim_tran As clsmedical_claim_Tran
'    Private mintMedicalclaimUnkid As Integer = -1
'    Private mintMedicalclaimTranUnkid As Integer = -1
'    Private mdtTran As DataTable
'    Dim objExchangeRate As clsExchangeRate
'    Dim mstrTreatmentunkid As String = ""

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intMedicalclaimtranunkid As Integer = -1) As Boolean
'        Try
'            mintMedicalclaimUnkid = intUnkId
'            mintMedicalclaimTranUnkid = intMedicalclaimtranunkid
'            menAction = eAction
'            Me.ShowDialog()
'            intUnkId = mintMedicalclaimUnkid
'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Private Methods "

'    Private Sub SetColor()
'        Try
'            cboEmployee.BackColor = GUI.ColorComp
'            cboDependants.BackColor = GUI.ColorOptional
'            txtServiceNo.BackColor = GUI.ColorComp
'            cboYear.BackColor = GUI.ColorComp
'            txtClaimNo.BackColor = GUI.ColorComp
'            txtClaimAmount.BackColor = GUI.ColorComp
'            cboCover.BackColor = GUI.ColorComp
'            cboInstutution.BackColor = GUI.ColorComp
'            cboServices.BackColor = GUI.ColorComp
'            txtRemarks.BackColor = GUI.ColorOptional
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try
'            cboEmployee.SelectedValue = objMedicalclaim_master._Employeeunkid
'            cboYear.SelectedValue = objMedicalclaim_master._Yearunkid
'            FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            objMedicalclaim_master._Employeeunkid = CInt(cboEmployee.SelectedValue)
'            objMedicalclaim_master._Yearunkid = CInt(cboYear.SelectedValue)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim dsFill As DataSet = Nothing
'        Try
'            'FOR EMPLOYEE 
'            Dim objEmployee As New clsEmployee_Master


'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : CHECK FOR ACTIVE EMPLOYEE
'            'dsFill = objEmployee.GetEmployeeList("Employee", True, True)
'            dsFill = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'            'Pinkal (24-Jun-2011) -- End

'            cboEmployee.DisplayMember = "employeename"
'            cboEmployee.ValueMember = "employeeunkid"
'            cboEmployee.DataSource = dsFill.Tables("Employee")

'            'FOR YEAR
'            dsFill = Nothing
'            Dim objyear As New clsMasterData
'            dsFill = objyear.getComboListPAYYEAR("Year", True)
'            cboYear.DisplayMember = "name"
'            cboYear.ValueMember = "Id"
'            cboYear.DataSource = dsFill.Tables("Year")


'            'FOR MEDICAL COVER
'            dsFill = Nothing
'            Dim objMedicalCategory As New clsmedical_master
'            dsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Cover, "Cover", True)
'            cboCover.DisplayMember = "name"
'            cboCover.ValueMember = "mdmasterunkid"
'            cboCover.DataSource = dsFill.Tables("Cover")

'            'FOR MEDICAL SERVICE
'            dsFill = Nothing
'            dsFill = objMedicalCategory.getListForCombo(enMedicalMasterType.Service, "Service", True)
'            cboServices.DisplayMember = "name"
'            cboServices.ValueMember = "mdmasterunkid"
'            cboServices.DataSource = dsFill.Tables("Service")

'            'FOR MEDICAL INSTITUTE
'            dsFill = Nothing
'            Dim objInstitute As New clsinstitute_master
'            dsFill = objInstitute.getListForCombo(True, "Institute", True)
'            cboInstutution.DisplayMember = "name"
'            cboInstutution.ValueMember = "instituteunkid"
'            cboInstutution.DataSource = dsFill.Tables("Institute")


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Function Validation() As Boolean
'        Try
'            If CInt(cboEmployee.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
'                cboEmployee.Select()
'                Return False
'            ElseIf CInt(cboServices.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Service is compulsory information.Please Select Service."), enMsgBoxStyle.Information)
'                cboServices.Select()
'                Return False
'            ElseIf txtServiceNo.Text.Trim = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Service Provider No cannot be blank.Service Provider No is required information."), enMsgBoxStyle.Information)
'                txtServiceNo.Select()
'                Return False
'            ElseIf CInt(cboYear.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
'                cboYear.Select()
'                Return False
'            ElseIf txtClaimNo.Text.Trim = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Claim No cannot be blank.Claim No is required information."), enMsgBoxStyle.Information)
'                txtClaimNo.Select()
'                Return False
'            ElseIf txtClaimAmount.Text.Trim = "" Or txtClaimAmount.Text = "0" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Claim Amount cannot be blank.Claim Amount is required information."), enMsgBoxStyle.Information)
'                txtClaimAmount.Select()
'                Return False
'            ElseIf CInt(cboCover.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Cover is compulsory information.Please Select Cover."), enMsgBoxStyle.Information)
'                cboCover.Select()
'                Return False

'            ElseIf CInt(cboInstutution.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Institute is compulsory information.Please Select Institute."), enMsgBoxStyle.Information)
'                cboInstutution.Select()
'                Return False
'            ElseIf lvTreatment.CheckedItems.Count = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Treatment is compulsory information.Please Check atleast one Treatment."), enMsgBoxStyle.Information)
'                lvTreatment.Select()
'                If lvTreatment.Items.Count > 0 Then lvTreatment.Items(0).Selected = True
'                Return False
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
'        End Try
'        Return True
'    End Function

'    Private Sub FillList()
'        Try
'            If Not mdtTran Is Nothing Then
'                lvClaim.Items.Clear()
'                Dim lvitem As ListViewItem
'                For Each drRow As DataRow In mdtTran.Rows
'                    If CStr(IIf(IsDBNull(drRow.Item("AUD")), "A", drRow.Item("AUD"))) <> "D" Then
'                        lvitem = New ListViewItem
'                        lvitem.Text = drRow("employeename").ToString
'                        lvitem.Tag = CInt(drRow("claimtranunkid"))
'                        lvitem.SubItems.Add(drRow("dependentsname").ToString)
'                        lvitem.SubItems.Add(drRow("claimno").ToString)
'                        lvitem.SubItems.Add(CDate(drRow("claimdate")).ToShortDateString)
'                        lvitem.SubItems.Add(drRow("covername").ToString)
'                        lvitem.SubItems.Add(drRow("servicename").ToString)
'                        lvitem.SubItems.Add(drRow("serviceno").ToString)
'                        lvitem.SubItems.Add(CDate(drRow("treatmentdate")).ToShortDateString)
'                        lvitem.SubItems.Add(Format(CDec(drRow("amount")), objExchangeRate._fmtCurrency))
'                        lvitem.SubItems.Add(drRow("remarks").ToString)
'                        lvitem.SubItems.Add(drRow("GUID").ToString)
'                        lvitem.SubItems.Add(drRow("employeeunkid").ToString)
'                        lvitem.SubItems.Add(drRow("dependantsunkid").ToString)
'                        '    lvitem.SubItems.Add(drRow("membershipunkid").ToString)
'                        '   lvitem.SubItems.Add(drRow("membershipno").ToString)
'                        lvitem.SubItems.Add(drRow("yearunkid").ToString)
'                        lvitem.SubItems.Add(drRow("covermasterunkid").ToString)
'                        lvitem.SubItems.Add(drRow("serviceunkid").ToString)
'                        lvitem.SubItems.Add(drRow("instituteunkid").ToString)
'                        lvitem.SubItems.Add(drRow("treatmentunkid").ToString)
'                        lvClaim.Items.Add(lvitem)
'                    End If
'                Next
'                If lvClaim.Items.Count > 18 Then
'                    colhAmount.Width = 115 - 18
'                Else
'                    colhAmount.Width = 115
'                End If
'                cboEmployee.Select()
'                If menAction = enAction.ADD_CONTINUE Then

'                    'If cboEmpMembership.Visible Then
'                    '    If cboEmpMembership.Items.Count > 0 Then cboEmpMembership.SelectedIndex = 0
'                    'ElseIf cboMembershipname.Visible Then
'                    '    If cboMembershipname.Items.Count > 0 Then cboMembershipname.SelectedIndex = 0
'                    'End If

'                    txtClaimNo.Text = ""
'                    txtClaimAmount.Text = ""
'                    '  dtpTreatmentdate.Value = Now.Date
'                    ' dtpClaimDate.Value = dtpTreatmentdate.Value
'                    If cboDependants.Items.Count > 0 Then cboDependants.SelectedIndex = 0
'                    If cboCover.Items.Count > 0 Then cboCover.SelectedIndex = 0
'                    If cboServices.Items.Count > 0 Then cboServices.SelectedIndex = 0
'                    If cboInstutution.Items.Count > 0 Then cboInstutution.SelectedIndex = 0
'                    chkSelect_CheckedChanged(New Object(), New EventArgs())

'                ElseIf menAction = enAction.EDIT_ONE Then
'                    '    chkSelect.Checked = False
'                    Dim drRow As DataRow() = mdtTran.Select("claimtranunkid=" & mintMedicalclaimTranUnkid & " AND AUD <> 'D'")
'                    If drRow.Length > 0 Then
'                        lvClaim.Items(mdtTran.Rows.IndexOf(drRow(0))).Selected = True
'                    End If
'                End If

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "AssignListValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillTreatment()
    '    Dim dsFill As DataSet = Nothing
    '    Try
'            Dim objTreatment As New clsmedical_master
'            dsFill = objTreatment.getListForCombo(enMedicalMasterType.Treatment, "Treatment")

'            lvTreatment.Items.Clear()
'            Dim lvitem As ListViewItem

'            For Each drRow As DataRow In dsFill.Tables("Treatment").Rows
'                lvitem = New ListViewItem
'                lvitem.Text = ""
'                lvitem.Tag = CInt(drRow("mdmasterunkid"))
'                lvitem.SubItems.Add(drRow("name").ToString)
'                lvTreatment.Items.Add(lvitem)
'            Next

'            If lvTreatment.Items.Count > 18 Then
'                colhTreatment.Width = 139 - 18
'            Else
'                colhTreatment.Width = 139
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillTreatment", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetTreametValue()
'        Try
'            If mstrTreatmentunkid.Length > 0 Then
'                Dim strArTreatment() As String = mstrTreatmentunkid.Split(CChar(","))

'                For i As Integer = 0 To lvTreatment.Items.Count - 1

'                    For j As Integer = 0 To strArTreatment.Length - 1

'                        If lvTreatment.Items(i).Tag.ToString = strArTreatment(j).ToString Then
'                            lvTreatment.Items(i).Checked = True
'                            Exit For
'                        End If

'                    Next
'                Next

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetTreamentValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetTreametValue()
'        Try
'            mstrTreatmentunkid = ""
'            For i As Integer = 0 To lvTreatment.CheckedItems.Count - 1
'                mstrTreatmentunkid &= lvTreatment.CheckedItems(i).Tag.ToString & ","
'            Next

'            If mstrTreatmentunkid.Length > 0 Then
'                mstrTreatmentunkid = mstrTreatmentunkid.Substring(0, mstrTreatmentunkid.Length - 1)
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetTreametValue", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (10-Mar-2011) -- Start

'    Private Sub SetTreatmentOperation(ByVal blnOperation As Boolean)
'        Try
'            For Each Item As ListViewItem In lvTreatment.Items
'                Item.Checked = blnOperation
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetTreatmentOperation", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (10-Mar-2011) -- End

'    'Private Sub ClearControls()
'    '    Try

'    '        dtpTreatmentdate.Value = Now.Date

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
'    '    End Try
'    'End Sub

'    Private Sub SetVisibility()

'        Try
'            objbtnAddCover.Enabled = User._Object.Privilege._AddMedicalMasters
'            objbtnAddInstitution.Enabled = User._Object.Privilege._AddMedicalInstitutes
'            objbtnAddService.Enabled = User._Object.Privilege._AddMedicalMasters

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try

'    End Sub

'#End Region

'#Region " Form's Events "

'    Private Sub frmMedicalClaim_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objMedicalclaim_master = New clsmedical_claim_master
'        objMedicalclaim_tran = New clsmedical_claim_Tran
'        objExchangeRate = New clsExchangeRate
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Call SetVisibility()
'            SetColor()
'            txtRemarks.SelectionMode = False
'            txtRemarks.SelectionLength = 0

'            'START FOR ASSIGN DECIMAL POINT TO CLAIM AMOUNT
'            objExchangeRate._ExchangeRateunkid = 1

'            'END FOR ASSIGN DECIMAL POINT TO CLAIM AMOUNT
'            If menAction = enAction.EDIT_ONE Then
'                objMedicalclaim_master._Claimunkid = mintMedicalclaimUnkid
'                objMedicalclaim_tran._Claimunkid = mintMedicalclaimUnkid
'                objMedicalclaim_tran.GetClaimTran()
'            End If
'            mdtTran = objMedicalclaim_tran._DataList
'            FillCombo()
'            FillTreatment()
'            'dtpTreatmentdate_ValueChanged(sender, e)
'            GetValue()
'            cboEmployee.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmMedicalClaim_AddEdit_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmMedicalClaim_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                SendKeys.Send("{TAB}")
'                If txtRemarks.SelectionLength > 0 Then txtRemarks.SelectionStart = txtRemarks.Text.Trim.Length + 1 : txtRemarks.SelectedText = ""
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmMedicalClaim_AddEdit_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmMedicalClaim_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
'        Try
'            If e.Control = True And e.KeyCode = Keys.S Then
'                btnSave_Click(sender, e)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmMedicalClaim_AddEdit_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmMedicalClaim_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'        objMedicalclaim_master = Nothing
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'        Try
'            If Validation() Then

'                'START FOR CHECK UNIQUE CLAIM NO
'                Dim drClaimno As DataRow() = mdtTran.Select("claimno = '" & txtClaimNo.Text.Trim & "' AND AUD <> 'D'")

'                If drClaimno.Length > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This Claim No is already added to the list."), enMsgBoxStyle.Information)
'                    txtClaimNo.Select()
'                    Exit Sub
'                End If
'                'END FOR CHECK UNIQUE CLAIM NO
'                GetTreametValue()

'                Dim dtRow As DataRow = Nothing
'                dtRow = mdtTran.NewRow
'                dtRow("claimtranunkid") = -1
'                dtRow("claimunkid") = mintMedicalclaimUnkid
'                dtRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
'                dtRow("employeename") = cboEmployee.Text
'                dtRow("dependantsunkid") = IIf(CInt(cboDependants.SelectedValue) > 0, CInt(cboDependants.SelectedValue), -1)
'                dtRow("dependentsname") = IIf(CInt(cboDependants.SelectedValue) > 0, cboDependants.Text, "")

'                'If cboEmpMembership.Visible Then
'                '    dtRow("membershipunkid") = CInt(cboEmpMembership.SelectedValue)
'                '    dtRow("membershipname") = cboEmpMembership.Text
'                'ElseIf cboMembershipname.Visible Then
'                '    dtRow("membershipunkid") = CInt(cboMembershipname.SelectedValue)
'                '    dtRow("membershipname") = cboMembershipname.Text
'                'End If
'                '  dtRow("membershipno") = txtServiceNo.Text.Trim
'                dtRow("servicename") = cboServices.Text
'                dtRow("serviceno") = txtServiceNo.Text.Trim
'                dtRow("yearunkid") = CInt(cboYear.SelectedValue)
'                dtRow("claimno") = txtClaimNo.Text.Trim
'                dtRow("claimdate") = dtpClaimDate.Value.Date
'                dtRow("amount") = txtClaimAmount.Text.Trim
'                dtRow("covermasterunkid") = CInt(cboCover.SelectedValue)
'                dtRow("covername") = cboCover.Text
'                dtRow("serviceunkid") = CInt(cboServices.SelectedValue)
'                dtRow("instituteunkid") = CInt(cboInstutution.SelectedValue)
'                dtRow("treatmentdate") = dtpTreatmentdate.Value.Date
'                dtRow("treatmentunkid") = mstrTreatmentunkid
'                dtRow("remarks") = txtRemarks.Text.Trim
'                dtRow("AUD") = "A"
'                dtRow("GUID") = Guid.NewGuid.ToString
'                mdtTran.Rows.Add(dtRow)
'                FillList()
'                cboEmployee.Select()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        Try
'            If lvClaim.SelectedItems.Count = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Medical Claim from the list to perform further operation on it."), enMsgBoxStyle.Information)
'                lvClaim.Select()
'                Exit Sub
'            End If
'            If Validation() Then

'                'START FOR CHECK UNIQUE CLAIM NO
'                Dim drClaimno As DataRow() = mdtTran.Select("claimno = '" & txtClaimNo.Text.Trim & "' AND GUID <> '" & lvClaim.SelectedItems(0).SubItems(colhGUID.Index).Text & "'")

'                If drClaimno.Length > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This Claim No is already added to the list."), enMsgBoxStyle.Information)
'                    txtClaimNo.Select()
'                    Exit Sub
'                End If
'                'END FOR CHECK UNIQUE CLAIM NO

'                Dim drTemp As DataRow()
'                If CInt(lvClaim.SelectedItems(0).Tag) = -1 Then
'                    drTemp = mdtTran.Select("GUID = '" & lvClaim.SelectedItems(0).SubItems(colhGUID.Index).Text & "'")
'                Else
'                    drTemp = mdtTran.Select("claimtranunkid = " & CInt(lvClaim.SelectedItems(0).Tag))
'                End If

'                If drTemp.Length > 0 Then

'                    GetTreametValue()

'                    With drTemp(0)
'                        .Item("claimtranunkid") = CInt(lvClaim.SelectedItems(0).Tag)
'                        .Item("claimunkid") = mintMedicalclaimUnkid
'                        .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
'                        .Item("employeename") = cboEmployee.Text
'                        If CInt(cboDependants.SelectedValue) <= 0 Then
'                            .Item("dependantsunkid") = -1
'                            .Item("dependentsname") = ""
'                        Else
'                            .Item("dependantsunkid") = CInt(cboDependants.SelectedValue)
'                            .Item("dependentsname") = cboDependants.Text
'                        End If

'                        'If cboEmpMembership.Visible Then
'                        '    .Item("membershipunkid") = CInt(cboEmpMembership.SelectedValue)
'                        '    .Item("membershipname") = cboEmpMembership.Text
'                        'ElseIf cboMembershipname.Visible Then
'                        '    .Item("membershipunkid") = CInt(cboMembershipname.SelectedValue)
'                        '    .Item("membershipname") = cboMembershipname.Text
'                        'End If
'                        ' .Item("membershipno") = txtServiceNo.Text.Trim

'                        .Item("serviceno") = txtServiceNo.Text.Trim
'                        .Item("yearunkid") = CInt(cboYear.SelectedValue)
'                        .Item("claimno") = txtClaimNo.Text.Trim
'                        .Item("claimdate") = dtpClaimDate.Value.Date
'                        .Item("amount") = txtClaimAmount.Text.Trim
'                        .Item("employeename") = cboEmployee.Text
'                        .Item("covermasterunkid") = CInt(cboCover.SelectedValue)
'                        .Item("covername") = cboCover.Text
'                        .Item("serviceunkid") = CInt(cboServices.SelectedValue)
'                        .Item("servicename") = cboServices.Text
'                        .Item("instituteunkid") = CInt(cboInstutution.SelectedValue)
'                        .Item("treatmentdate") = dtpTreatmentdate.Value.Date
'                        .Item("treatmentunkid") = mstrTreatmentunkid
'                        .Item("remarks") = txtRemarks.Text.Trim

'                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
'                            .Item("AUD") = "U"
'                        End If
'                        .AcceptChanges()
'                    End With
'                    FillList()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        Try
'            Dim index As Integer = 0
'            If lvClaim.SelectedItems.Count = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Medical Claim from the list to perform further operation on it."), enMsgBoxStyle.Information)
'                lvClaim.Select()
'                Exit Sub
'            End If
'            Dim drTemp As DataRow() = Nothing
'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Are you sure you want to delete this Medical Claim?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                If CInt(lvClaim.SelectedItems(0).Tag) = -1 Then
'                    drTemp = mdtTran.Select("GUID = '" & lvClaim.SelectedItems(0).SubItems(colhGUID.Index).Text & "'")
'                    index = lvClaim.SelectedItems(0).Index
'                Else
'                    drTemp = mdtTran.Select("claimtranunkid = " & CInt(lvClaim.SelectedItems(0).Tag))
'                End If
'                If drTemp.Length > 0 Then
'                    drTemp(0).Item("AUD") = "D"
'                End If
'                FillList()

'                If index = 0 And lvClaim.Items.Count > 0 Then
'                    lvClaim.Items(index).Selected = True
'                ElseIf index > 1 Then
'                    lvClaim.Items(index - 1).Selected = True
'                End If


'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Try
'            If lvClaim.Items.Count = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Add atleast one Medcial Claim for this employee."), enMsgBoxStyle.Information)
'                cboEmployee.Select()
'                Exit Sub
'            End If

'            SetValue()
'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objMedicalclaim_master.Update(mdtTran)
'            Else
'                blnFlag = objMedicalclaim_master.Insert(mdtTran)
'            End If

'            If blnFlag = False And objMedicalclaim_master._Message <> "" Then
'                eZeeMsgBox.Show(objMedicalclaim_master._Message, enMsgBoxStyle.Information)
'            End If

'            If blnFlag Then
'                mblnCancel = False
'                If menAction = enAction.ADD_CONTINUE Then
'                    objMedicalclaim_master = Nothing
'                    objMedicalclaim_master = New clsmedical_claim_master
'                    objMedicalclaim_tran = Nothing
'                    objMedicalclaim_tran = New clsmedical_claim_Tran
'                    mdtTran.Rows.Clear()
'                    FillList()
'                    cboEmployee.Select()
    '        Else
'                    mintMedicalclaimUnkid = objMedicalclaim_master._Claimunkid
'                    Me.Close()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim objfrm As New frmCommonSearch
'        Try
'            objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
'            objfrm.ValueMember = "employeeunkid"
'            objfrm.DisplayMember = "employeename"
'            objfrm.CodeMember = "employeecode"
'            If objfrm.DisplayDialog Then
'                cboEmployee.SelectedValue = objfrm.SelectedValue
'            End If
'            cboEmployee.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddCover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCover.Click
'        Try
'            Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
'            Dim intRefId As Integer = -1
'            Dim dsCover As New DataSet
'            Dim objmedical As New clsmedical_master
'            If objfrmMedicalMaster_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Cover) Then
'                dsCover = objmedical.getListForCombo(enMedicalMasterType.Cover, "List", True)
'                With cboCover
'                    .ValueMember = "mdmasterunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsCover.Tables("List")
'                End With
'                cboCover.SelectedValue = intRefId
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddCover_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddService_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddService.Click
'        Try
'            Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
'            Dim intRefId As Integer = -1
'            Dim dsCover As New DataSet
'            Dim objmedical As New clsmedical_master
'            If objfrmMedicalMaster_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Service) Then
'                dsCover = objmedical.getListForCombo(enMedicalMasterType.Service, "List", True)
'                With cboServices
'                    .ValueMember = "mdmasterunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsCover.Tables("List")
'                End With
'                cboServices.SelectedValue = intRefId
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddService_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddInstitution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInstitution.Click
'        Try
'            Dim objfrmTraningInstitutes_AddEdit As New frmTraningInstitutes_AddEdit
'            Dim intRefId As Integer = -1
'            Dim dsCover As New DataSet
'            Dim objInstitute As New clsinstitute_master

'            'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

'            objfrmTraningInstitutes_AddEdit.Text = Language.getMessage("frmTraningInstitutesList", 3, "Add/Edit Service Provider")
'            objfrmTraningInstitutes_AddEdit.gbTaraningInstitutes.Text = Language.getMessage("frmTraningInstitutesList", 4, "Service Provider")
'            objfrmTraningInstitutes_AddEdit.eZeeHeader.Title = Language.getMessage("frmTraningInstitutesList", 4, "Service Provider")

'            'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment

'            If objfrmTraningInstitutes_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, True) Then
'                dsCover = objInstitute.getListForCombo(True, "List", True)
'                With cboInstutution
'                    .ValueMember = "instituteunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsCover.Tables("List")
'                End With
'                cboInstutution.SelectedValue = intRefId
    '        End If
    '    Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddInstitution_Click", mstrModuleName)
    '    End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'#End Region

'#Region "Dropdown's Event"

'    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
'        Dim dsFill As DataSet = Nothing
'        Try

'            ''FOR EMPLOYEE MEMBERSHIP
'            Dim objEmployee As New clsEmployee_Master
'            'dsFill = objEmployee.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
'            'cboEmpMembership.ValueMember = "Mem_Id"
'            'cboEmpMembership.DisplayMember = "Mem_Name"
'            'cboEmpMembership.DataSource = dsFill.Tables("Membership")

'            'FOR DEPENDENTS
'            dsFill = Nothing
'            dsFill = objEmployee.GetEmployee_Dependents(CInt(cboEmployee.SelectedValue), True, "Dependents")
'            cboDependants.ValueMember = "dependent_Id"
'            cboDependants.DisplayMember = "dependent_Name"
'            cboDependants.DataSource = dsFill.Tables("Dependents")

'            'If CInt(cboEmployee.SelectedValue) > 0 And CInt(cboDependants.SelectedValue) < 1 Then
'            '    lblService.Visible = True
'            '    cboEmpMembership.Visible = True
'            '    lblMembershipName.Visible = False
'            '    cboMembershipname.Visible = False
'            'ElseIf CInt(cboEmployee.SelectedValue) < 1 And CInt(cboDependants.SelectedValue) > 0 Then
'            '    lblService.Visible = False
'            '    cboEmpMembership.Visible = False
'            '    lblMembershipName.Visible = True
'            '    cboMembershipname.Visible = True
'            'End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
'        End Try
    'End Sub


'    Private Sub cboServices_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboServices.SelectedIndexChanged
    '    Dim dsFill As DataSet = Nothing
    '    Dim dtFill As DataTable = Nothing
    '    Try
'            Dim objMedical As New clsmedical_master
'            dsFill = objMedical.GetList("Service", True)
'            dtFill = New DataView(dsFill.Tables("Service"), "mdmasterunkid = " & CInt(cboServices.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
    '        If dtFill.Rows.Count > 0 Then
'                txtServiceNo.Text = dtFill.Rows(0)("mdserviceno").ToString
    '        Else
    '            txtServiceNo.Text = ""
    '        End If
    '    Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboServices_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

'    'Private Sub cboEmpMembership_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ''    Dim dsFill As DataSet = Nothing
'    '    Dim dtFill As DataTable = Nothing
    ''    Try

'    '        'FOR EMPLOYEE MEMBERSHIP
'    '        Dim objEmployee As New clsEmployee_Master
'    '        dsFill = objEmployee.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
'    '        dtFill = New DataView(dsFill.Tables("Membership"), "Mem_Id=" & CInt(cboEmpMembership.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
'    '        If dtFill.Rows.Count > 0 Then
'    '            txtServiceNo.Text = dtFill.Rows(0)("Mem_No").ToString
'    '        Else
'    '            txtServiceNo.Text = ""
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cboEmpMembership_SelectedIndexChanged", mstrModuleName)
'    '    End Try

'    'End Sub

'    'Private Sub cboMembershipname_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'    '    Dim dsFill As DataSet = Nothing
'    '    Dim dtFill As DataTable = Nothing
'    '    Try
    ''        'FOR DEPENDENTS MEMBERSHIP
    ''        Dim objDepMembership As New clsDependants_Membership_tran
'    '        dsFill = objDepMembership.GetDependants_Membership(CInt(cboDependants.SelectedValue), True, "Membership")
'    '        dtFill = New DataView(dsFill.Tables("Membership"), "Mem_Id=" & CInt(cboMembershipname.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
'    '        If dtFill.Rows.Count > 0 Then
'    '            txtServiceNo.Text = dtFill.Rows(0)("Mem_No").ToString
'    '        Else
'    '            txtServiceNo.Text = ""
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cboMembershipname_SelectedIndexChanged", mstrModuleName)
'    '    End Try
'    'End Sub

'    ''Private Sub cboDependants_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDependants.SelectedIndexChanged
'    ''    Dim dsFill As DataSet = Nothing
'    ''    Try

'    ''        'FOR DEPENDENTS MEMBERSHIP
'    ''        Dim objDepMembership As New clsDependants_Membership_tran
'    '        dsFill = objDepMembership.GetDependants_Membership(CInt(cboDependants.SelectedValue), True, "Membership")
'    '        cboMembershipname.ValueMember = "Mem_Id"
'    '        cboMembershipname.DisplayMember = "Mem_Name"
'    '        cboMembershipname.DataSource = dsFill.Tables("Membership")

'    '        If CInt(cboEmployee.SelectedValue) > 0 And CInt(cboDependants.SelectedValue) > 0 Then
'    '            lblService.Visible = False
'    '            cboEmpMembership.Visible = False
'    '            lblMembershipName.Visible = True
'    '            cboMembershipname.Visible = True
'    '        ElseIf CInt(cboDependants.SelectedValue) < 1 Then
'    '            lblService.Visible = True
'    '            cboEmpMembership.Visible = True
'    '            lblMembershipName.Visible = False
'    '            cboMembershipname.Visible = False
'    '            cboEmpMembership.SelectedValue = 0
'    '        End If

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cboDependants_SelectedIndexChanged", mstrModuleName)
'    '    End Try
'    'End Sub

'#End Region

'#Region "ListView's Event"

'    Private Sub lvclaim_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvClaim.SelectedIndexChanged
'        Try
'            If lvClaim.SelectedItems.Count > 0 Then
'                If mintMedicalclaimTranUnkid > 0 Then
'                    cboEmployee.Enabled = False
'                    objbtnSearchEmployee.Enabled = False
'                Else
'                    cboEmployee.Enabled = True
'                    objbtnSearchEmployee.Enabled = True
                'End If
'                cboEmployee.SelectedValue = lvClaim.SelectedItems(0).SubItems(colhEmployeeunkid.Index).Text
'                If CInt(lvClaim.SelectedItems(0).SubItems(colhDependantunkid.Index).Text) > 0 Then
'                    cboDependants.SelectedValue = lvClaim.SelectedItems(0).SubItems(colhDependantunkid.Index).Text
'                Else
'                    cboDependants.SelectedValue = 0
'                End If

'                'If cboEmpMembership.Visible Then
'                '    cboEmpMembership.SelectedValue = lvClaim.SelectedItems(0).SubItems(colhMembershipunkid.Index).Text
'                'ElseIf cboMembershipname.Visible Then
'                '    cboMembershipname.SelectedValue = lvClaim.SelectedItems(0).SubItems(colhMembershipunkid.Index).Text
'                'End If


'                txtServiceNo.Text = lvClaim.SelectedItems(0).SubItems(colhServiceNo.Index).Text
'                cboYear.SelectedValue = lvClaim.SelectedItems(0).SubItems(colhYearunkid.Index).Text
'                dtpTreatmentdate.Value = CDate(lvClaim.SelectedItems(0).SubItems(colhTreatmentDate.Index).Text)

'                dtpClaimDate.Value = CDate(lvClaim.SelectedItems(0).SubItems(colhClaimDate.Index).Text)
'                txtClaimNo.Text = lvClaim.SelectedItems(0).SubItems(colhClaimNo.Index).Text
'                cboCover.SelectedValue = lvClaim.SelectedItems(0).SubItems(colhCoverunkid.Index).Text
'                cboServices.SelectedValue = lvClaim.SelectedItems(0).SubItems(colhServiceunkid.Index).Text
'                cboInstutution.SelectedValue = lvClaim.SelectedItems(0).SubItems(colhInstituteunkid.Index).Text
'                txtClaimAmount.Text = lvClaim.SelectedItems(0).SubItems(colhAmount.Index).Text

'                'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

'                txtRemarks.Text = lvClaim.SelectedItems(0).SubItems(colhRemarks.Index).Text.Trim

'                'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment

'                chkSelect_CheckedChanged(sender, e)
'                mstrTreatmentunkid = lvClaim.SelectedItems(0).SubItems(colhTreamentunkid.Index).Text

'                If mstrTreatmentunkid.Length > 0 Then
'                    SetTreametValue()
'                End If

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvclaim_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (10-Mar-2011) -- Start

'    Private Sub lvTreatment_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvTreatment.ItemChecked
'        Try
'            RemoveHandler chkSelect.CheckedChanged, AddressOf chkSelect_CheckedChanged
'            If lvTreatment.CheckedItems.Count <= 0 Then
'                chkSelect.CheckState = CheckState.Unchecked

'            ElseIf lvTreatment.CheckedItems.Count < lvTreatment.Items.Count Then
'                chkSelect.CheckState = CheckState.Indeterminate

'            ElseIf lvTreatment.CheckedItems.Count = lvTreatment.Items.Count Then
'                chkSelect.CheckState = CheckState.Checked

'            End If
'            AddHandler chkSelect.CheckedChanged, AddressOf chkSelect_CheckedChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (10-Mar-2011) -- End

'#End Region

'#Region "DatePicker's Event"

'    Private Sub dtpTreatmentdate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTreatmentdate.ValueChanged, dtpClaimDate.ValueChanged
'        Try

'            If dtpClaimDate.MaxDate >= dtpTreatmentdate.Value.Date Then dtpClaimDate.MinDate = dtpTreatmentdate.Value.Date
'            dtpClaimDate.MaxDate = Now.Date
'            dtpTreatmentdate.MaxDate = Now.Date

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dtpTreatmentdate_ValueChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region "CheckBox's Event"

'    Private Sub chkSelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelect.CheckedChanged
'        Try

'            'Pinkal (10-Mar-2011) -- Start

'            'For i As Integer = 0 To lvTreatment.Items.Count - 1
'            '    lvTreatment.Items(i).Checked = chkSelect.Checked
'            'Next

'            If lvTreatment.Items.Count = 0 Then Exit Sub
'            SetTreatmentOperation(chkSelect.Checked)

'            'Pinkal (10-Mar-2011) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkSelect_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region

   
'End Class


