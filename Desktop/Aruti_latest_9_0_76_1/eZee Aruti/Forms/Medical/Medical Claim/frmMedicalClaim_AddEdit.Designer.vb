﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMedicalClaim_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMedicalClaim_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.BtnOperation = New eZee.Common.eZeeSplitButton
        Me.cmImportClaim = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnImportData = New System.Windows.Forms.ToolStripMenuItem
        Me.btnGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.txtTotalAmountVal = New eZee.TextBox.NumericTextBox
        Me.gbClaimInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchInstitution = New eZee.Common.eZeeGradientButton
        Me.dtpInvoicedate = New System.Windows.Forms.DateTimePicker
        Me.txtTotInvoiceAmt = New eZee.TextBox.NumericTextBox
        Me.LblInvoiceDate = New System.Windows.Forms.Label
        Me.txtSickSheetNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblTotInvoiceAmt = New System.Windows.Forms.Label
        Me.txtInvoiceNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblSicksheetNo = New System.Windows.Forms.Label
        Me.dtpClaimDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnAddInstitution = New eZee.Common.eZeeGradientButton
        Me.cboInstitution = New System.Windows.Forms.ComboBox
        Me.lblInvoiceno = New System.Windows.Forms.Label
        Me.lblClaimDate = New System.Windows.Forms.Label
        Me.lblInstutution = New System.Windows.Forms.Label
        Me.objbtnAddCover = New eZee.Common.eZeeGradientButton
        Me.lvClaim = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhDependants = New System.Windows.Forms.ColumnHeader
        Me.colhSickSheetNo = New System.Windows.Forms.ColumnHeader
        Me.colhClaimDate = New System.Windows.Forms.ColumnHeader
        Me.colhClaimNo = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhDependantunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhsicksheetunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhRemarks = New System.Windows.Forms.ColumnHeader
        Me.objColhEmpexempt = New System.Windows.Forms.ColumnHeader
        Me.objbtnAddService = New eZee.Common.eZeeGradientButton
        Me.lblTotAmt = New System.Windows.Forms.Label
        Me.gbTreatment = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkSelect = New System.Windows.Forms.CheckBox
        Me.lvTreatment = New eZee.Common.eZeeListView(Me.components)
        Me.colhSelect = New System.Windows.Forms.ColumnHeader
        Me.colhTreatment = New System.Windows.Forms.ColumnHeader
        Me.txtServiceNo = New eZee.TextBox.AlphanumericTextBox
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblService = New System.Windows.Forms.Label
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboCover = New System.Windows.Forms.ComboBox
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblServiceNo = New System.Windows.Forms.Label
        Me.gbEmployeeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkEmpexempted = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblDependants = New System.Windows.Forms.Label
        Me.txtClaimAmount = New eZee.TextBox.NumericTextBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblClaimAmount = New System.Windows.Forms.Label
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.cboDependants = New System.Windows.Forms.ComboBox
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.txtClaimNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblClaimNo = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblCover = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFinalSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboServices = New System.Windows.Forms.ComboBox
        Me.dtpTreatmentdate = New System.Windows.Forms.DateTimePicker
        Me.lblTreatmentDate = New System.Windows.Forms.Label
        Me.lblMedicalYear = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.cmImportClaim.SuspendLayout()
        Me.gbClaimInfo.SuspendLayout()
        Me.gbTreatment.SuspendLayout()
        Me.gbEmployeeInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.BtnOperation)
        Me.pnlMainInfo.Controls.Add(Me.txtTotalAmountVal)
        Me.pnlMainInfo.Controls.Add(Me.gbClaimInfo)
        Me.pnlMainInfo.Controls.Add(Me.objbtnAddCover)
        Me.pnlMainInfo.Controls.Add(Me.lvClaim)
        Me.pnlMainInfo.Controls.Add(Me.objbtnAddService)
        Me.pnlMainInfo.Controls.Add(Me.lblTotAmt)
        Me.pnlMainInfo.Controls.Add(Me.gbTreatment)
        Me.pnlMainInfo.Controls.Add(Me.txtServiceNo)
        Me.pnlMainInfo.Controls.Add(Me.btnDelete)
        Me.pnlMainInfo.Controls.Add(Me.lblService)
        Me.pnlMainInfo.Controls.Add(Me.btnAdd)
        Me.pnlMainInfo.Controls.Add(Me.cboCover)
        Me.pnlMainInfo.Controls.Add(Me.btnEdit)
        Me.pnlMainInfo.Controls.Add(Me.lblServiceNo)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeInfo)
        Me.pnlMainInfo.Controls.Add(Me.cboYear)
        Me.pnlMainInfo.Controls.Add(Me.lblCover)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.cboServices)
        Me.pnlMainInfo.Controls.Add(Me.dtpTreatmentdate)
        Me.pnlMainInfo.Controls.Add(Me.lblTreatmentDate)
        Me.pnlMainInfo.Controls.Add(Me.lblMedicalYear)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(882, 529)
        Me.pnlMainInfo.TabIndex = 210
        '
        'BtnOperation
        '
        Me.BtnOperation.BorderColor = System.Drawing.Color.Black
        Me.BtnOperation.ContextMenuStrip = Me.cmImportClaim
        Me.BtnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BtnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.BtnOperation.Location = New System.Drawing.Point(480, 210)
        Me.BtnOperation.Name = "BtnOperation"
        Me.BtnOperation.ShowDefaultBorderColor = True
        Me.BtnOperation.Size = New System.Drawing.Size(93, 30)
        Me.BtnOperation.SplitButtonMenu = Me.cmImportClaim
        Me.BtnOperation.TabIndex = 227
        Me.BtnOperation.Text = "Operation"
        '
        'cmImportClaim
        '
        Me.cmImportClaim.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnImportData, Me.btnGetFileFormat})
        Me.cmImportClaim.Name = "cmImportAccrueLeave"
        Me.cmImportClaim.Size = New System.Drawing.Size(148, 48)
        '
        'btnImportData
        '
        Me.btnImportData.Name = "btnImportData"
        Me.btnImportData.Size = New System.Drawing.Size(147, 22)
        Me.btnImportData.Text = "&Import"
        '
        'btnGetFileFormat
        '
        Me.btnGetFileFormat.Name = "btnGetFileFormat"
        Me.btnGetFileFormat.Size = New System.Drawing.Size(147, 22)
        Me.btnGetFileFormat.Text = "&Get File Format"
        '
        'txtTotalAmountVal
        '
        Me.txtTotalAmountVal.AllowNegative = True
        Me.txtTotalAmountVal.BackColor = System.Drawing.Color.White
        Me.txtTotalAmountVal.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalAmountVal.DigitsInGroup = 0
        Me.txtTotalAmountVal.Flags = 0
        Me.txtTotalAmountVal.Location = New System.Drawing.Point(712, 447)
        Me.txtTotalAmountVal.MaxDecimalPlaces = 4
        Me.txtTotalAmountVal.MaxWholeDigits = 9
        Me.txtTotalAmountVal.Name = "txtTotalAmountVal"
        Me.txtTotalAmountVal.Prefix = ""
        Me.txtTotalAmountVal.RangeMax = 1.7976931348623157E+308
        Me.txtTotalAmountVal.RangeMin = -1.7976931348623157E+308
        Me.txtTotalAmountVal.ReadOnly = True
        Me.txtTotalAmountVal.Size = New System.Drawing.Size(158, 21)
        Me.txtTotalAmountVal.TabIndex = 225
        Me.txtTotalAmountVal.TabStop = False
        Me.txtTotalAmountVal.Text = "0"
        Me.txtTotalAmountVal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gbClaimInfo
        '
        Me.gbClaimInfo.BorderColor = System.Drawing.Color.Black
        Me.gbClaimInfo.Checked = False
        Me.gbClaimInfo.CollapseAllExceptThis = False
        Me.gbClaimInfo.CollapsedHoverImage = Nothing
        Me.gbClaimInfo.CollapsedNormalImage = Nothing
        Me.gbClaimInfo.CollapsedPressedImage = Nothing
        Me.gbClaimInfo.CollapseOnLoad = False
        Me.gbClaimInfo.Controls.Add(Me.objbtnSearchInstitution)
        Me.gbClaimInfo.Controls.Add(Me.dtpInvoicedate)
        Me.gbClaimInfo.Controls.Add(Me.txtTotInvoiceAmt)
        Me.gbClaimInfo.Controls.Add(Me.LblInvoiceDate)
        Me.gbClaimInfo.Controls.Add(Me.txtSickSheetNo)
        Me.gbClaimInfo.Controls.Add(Me.lblTotInvoiceAmt)
        Me.gbClaimInfo.Controls.Add(Me.txtInvoiceNo)
        Me.gbClaimInfo.Controls.Add(Me.lblSicksheetNo)
        Me.gbClaimInfo.Controls.Add(Me.dtpClaimDate)
        Me.gbClaimInfo.Controls.Add(Me.objbtnAddInstitution)
        Me.gbClaimInfo.Controls.Add(Me.cboInstitution)
        Me.gbClaimInfo.Controls.Add(Me.lblInvoiceno)
        Me.gbClaimInfo.Controls.Add(Me.lblClaimDate)
        Me.gbClaimInfo.Controls.Add(Me.lblInstutution)
        Me.gbClaimInfo.ExpandedHoverImage = Nothing
        Me.gbClaimInfo.ExpandedNormalImage = Nothing
        Me.gbClaimInfo.ExpandedPressedImage = Nothing
        Me.gbClaimInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbClaimInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbClaimInfo.HeaderHeight = 25
        Me.gbClaimInfo.HeaderMessage = ""
        Me.gbClaimInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbClaimInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbClaimInfo.HeightOnCollapse = 0
        Me.gbClaimInfo.LeftTextSpace = 0
        Me.gbClaimInfo.Location = New System.Drawing.Point(11, 10)
        Me.gbClaimInfo.Name = "gbClaimInfo"
        Me.gbClaimInfo.OpenHeight = 300
        Me.gbClaimInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbClaimInfo.ShowBorder = True
        Me.gbClaimInfo.ShowCheckBox = False
        Me.gbClaimInfo.ShowCollapseButton = False
        Me.gbClaimInfo.ShowDefaultBorderColor = True
        Me.gbClaimInfo.ShowDownButton = False
        Me.gbClaimInfo.ShowHeader = True
        Me.gbClaimInfo.Size = New System.Drawing.Size(331, 194)
        Me.gbClaimInfo.TabIndex = 8
        Me.gbClaimInfo.TabStop = True
        Me.gbClaimInfo.Temp = 0
        Me.gbClaimInfo.Text = "Claim Info"
        Me.gbClaimInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchInstitution
        '
        Me.objbtnSearchInstitution.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitution.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitution.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitution.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInstitution.BorderSelected = False
        Me.objbtnSearchInstitution.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInstitution.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInstitution.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInstitution.Location = New System.Drawing.Point(307, 26)
        Me.objbtnSearchInstitution.Name = "objbtnSearchInstitution"
        Me.objbtnSearchInstitution.Size = New System.Drawing.Size(21, 27)
        Me.objbtnSearchInstitution.TabIndex = 224
        '
        'dtpInvoicedate
        '
        Me.dtpInvoicedate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInvoicedate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInvoicedate.Location = New System.Drawing.Point(131, 111)
        Me.dtpInvoicedate.Name = "dtpInvoicedate"
        Me.dtpInvoicedate.Size = New System.Drawing.Size(106, 21)
        Me.dtpInvoicedate.TabIndex = 4
        '
        'txtTotInvoiceAmt
        '
        Me.txtTotInvoiceAmt.AllowNegative = True
        Me.txtTotInvoiceAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotInvoiceAmt.DigitsInGroup = 0
        Me.txtTotInvoiceAmt.Flags = 0
        Me.txtTotInvoiceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotInvoiceAmt.Location = New System.Drawing.Point(131, 165)
        Me.txtTotInvoiceAmt.MaxDecimalPlaces = 4
        Me.txtTotInvoiceAmt.MaxWholeDigits = 9
        Me.txtTotInvoiceAmt.Name = "txtTotInvoiceAmt"
        Me.txtTotInvoiceAmt.Prefix = ""
        Me.txtTotInvoiceAmt.RangeMax = 1.7976931348623157E+308
        Me.txtTotInvoiceAmt.RangeMin = -1.7976931348623157E+308
        Me.txtTotInvoiceAmt.Size = New System.Drawing.Size(146, 21)
        Me.txtTotInvoiceAmt.TabIndex = 6
        Me.txtTotInvoiceAmt.Text = "0"
        Me.txtTotInvoiceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblInvoiceDate
        '
        Me.LblInvoiceDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblInvoiceDate.Location = New System.Drawing.Point(6, 112)
        Me.LblInvoiceDate.Name = "LblInvoiceDate"
        Me.LblInvoiceDate.Size = New System.Drawing.Size(119, 19)
        Me.LblInvoiceDate.TabIndex = 230
        Me.LblInvoiceDate.Text = "Invoice Date"
        Me.LblInvoiceDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSickSheetNo
        '
        Me.txtSickSheetNo.Flags = 0
        Me.txtSickSheetNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSickSheetNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSickSheetNo.Location = New System.Drawing.Point(131, 56)
        Me.txtSickSheetNo.Name = "txtSickSheetNo"
        Me.txtSickSheetNo.Size = New System.Drawing.Size(146, 21)
        Me.txtSickSheetNo.TabIndex = 2
        '
        'lblTotInvoiceAmt
        '
        Me.lblTotInvoiceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotInvoiceAmt.Location = New System.Drawing.Point(6, 166)
        Me.lblTotInvoiceAmt.Name = "lblTotInvoiceAmt"
        Me.lblTotInvoiceAmt.Size = New System.Drawing.Size(119, 19)
        Me.lblTotInvoiceAmt.TabIndex = 228
        Me.lblTotInvoiceAmt.Text = "Total Invoice Amount"
        Me.lblTotInvoiceAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInvoiceNo
        '
        Me.txtInvoiceNo.Flags = 0
        Me.txtInvoiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoiceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtInvoiceNo.Location = New System.Drawing.Point(131, 139)
        Me.txtInvoiceNo.Name = "txtInvoiceNo"
        Me.txtInvoiceNo.Size = New System.Drawing.Size(146, 21)
        Me.txtInvoiceNo.TabIndex = 5
        '
        'lblSicksheetNo
        '
        Me.lblSicksheetNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSicksheetNo.Location = New System.Drawing.Point(6, 57)
        Me.lblSicksheetNo.Name = "lblSicksheetNo"
        Me.lblSicksheetNo.Size = New System.Drawing.Size(119, 19)
        Me.lblSicksheetNo.TabIndex = 222
        Me.lblSicksheetNo.Text = "Sick Sheet No."
        Me.lblSicksheetNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpClaimDate
        '
        Me.dtpClaimDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpClaimDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpClaimDate.Location = New System.Drawing.Point(131, 84)
        Me.dtpClaimDate.Name = "dtpClaimDate"
        Me.dtpClaimDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpClaimDate.TabIndex = 3
        '
        'objbtnAddInstitution
        '
        Me.objbtnAddInstitution.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddInstitution.BorderSelected = False
        Me.objbtnAddInstitution.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddInstitution.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddInstitution.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddInstitution.Location = New System.Drawing.Point(283, 29)
        Me.objbtnAddInstitution.Name = "objbtnAddInstitution"
        Me.objbtnAddInstitution.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddInstitution.TabIndex = 14
        '
        'cboInstitution
        '
        Me.cboInstitution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstitution.DropDownWidth = 300
        Me.cboInstitution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstitution.FormattingEnabled = True
        Me.cboInstitution.Location = New System.Drawing.Point(131, 29)
        Me.cboInstitution.Name = "cboInstitution"
        Me.cboInstitution.Size = New System.Drawing.Size(146, 21)
        Me.cboInstitution.TabIndex = 1
        '
        'lblInvoiceno
        '
        Me.lblInvoiceno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvoiceno.Location = New System.Drawing.Point(6, 140)
        Me.lblInvoiceno.Name = "lblInvoiceno"
        Me.lblInvoiceno.Size = New System.Drawing.Size(119, 19)
        Me.lblInvoiceno.TabIndex = 226
        Me.lblInvoiceno.Text = "Invoice No"
        Me.lblInvoiceno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClaimDate
        '
        Me.lblClaimDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimDate.Location = New System.Drawing.Point(6, 85)
        Me.lblClaimDate.Name = "lblClaimDate"
        Me.lblClaimDate.Size = New System.Drawing.Size(119, 19)
        Me.lblClaimDate.TabIndex = 99
        Me.lblClaimDate.Text = "Claim Date"
        Me.lblClaimDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInstutution
        '
        Me.lblInstutution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstutution.Location = New System.Drawing.Point(6, 30)
        Me.lblInstutution.Name = "lblInstutution"
        Me.lblInstutution.Size = New System.Drawing.Size(119, 19)
        Me.lblInstutution.TabIndex = 110
        Me.lblInstutution.Text = "Service Provider "
        Me.lblInstutution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddCover
        '
        Me.objbtnAddCover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCover.BorderSelected = False
        Me.objbtnAddCover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCover.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCover.Location = New System.Drawing.Point(313, 89)
        Me.objbtnAddCover.Name = "objbtnAddCover"
        Me.objbtnAddCover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCover.TabIndex = 7
        Me.objbtnAddCover.Visible = False
        '
        'lvClaim
        '
        Me.lvClaim.BackColorOnChecked = True
        Me.lvClaim.ColumnHeaders = Nothing
        Me.lvClaim.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhDependants, Me.colhSickSheetNo, Me.colhClaimDate, Me.colhClaimNo, Me.colhAmount, Me.objcolhGUID, Me.objcolhEmployeeunkid, Me.objcolhDependantunkid, Me.objcolhsicksheetunkid, Me.objcolhRemarks, Me.objColhEmpexempt})
        Me.lvClaim.CompulsoryColumns = ""
        Me.lvClaim.FullRowSelect = True
        Me.lvClaim.GridLines = True
        Me.lvClaim.GroupingColumn = Nothing
        Me.lvClaim.HideSelection = False
        Me.lvClaim.Location = New System.Drawing.Point(12, 246)
        Me.lvClaim.MinColumnWidth = 50
        Me.lvClaim.MultiSelect = False
        Me.lvClaim.Name = "lvClaim"
        Me.lvClaim.OptionalColumns = ""
        Me.lvClaim.ShowMoreItem = False
        Me.lvClaim.ShowSaveItem = False
        Me.lvClaim.ShowSelectAll = True
        Me.lvClaim.ShowSizeAllColumnsToFit = True
        Me.lvClaim.Size = New System.Drawing.Size(858, 194)
        Me.lvClaim.Sortable = True
        Me.lvClaim.TabIndex = 17
        Me.lvClaim.TabStop = False
        Me.lvClaim.UseCompatibleStateImageBehavior = False
        Me.lvClaim.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhDependants
        '
        Me.colhDependants.Tag = "colhDependants"
        Me.colhDependants.Text = "Dependants"
        Me.colhDependants.Width = 250
        '
        'colhSickSheetNo
        '
        Me.colhSickSheetNo.Tag = "colhSickSheetNo"
        Me.colhSickSheetNo.Text = "Sick Sheet No"
        Me.colhSickSheetNo.Width = 150
        '
        'colhClaimDate
        '
        Me.colhClaimDate.Tag = "colhClaimDate"
        Me.colhClaimDate.Text = "Claim Date"
        Me.colhClaimDate.Width = 120
        '
        'colhClaimNo
        '
        Me.colhClaimNo.Tag = "colhClaimNo"
        Me.colhClaimNo.Text = "Claim No."
        Me.colhClaimNo.Width = 150
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 180
        '
        'objcolhGUID
        '
        Me.objcolhGUID.DisplayIndex = 7
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = "GUID"
        Me.objcolhGUID.Width = 0
        '
        'objcolhEmployeeunkid
        '
        Me.objcolhEmployeeunkid.DisplayIndex = 8
        Me.objcolhEmployeeunkid.Tag = "objcolhEmployeeunkid"
        Me.objcolhEmployeeunkid.Text = "Employeeunkid"
        Me.objcolhEmployeeunkid.Width = 0
        '
        'objcolhDependantunkid
        '
        Me.objcolhDependantunkid.DisplayIndex = 9
        Me.objcolhDependantunkid.Tag = "objcolhDependantunkid"
        Me.objcolhDependantunkid.Text = "Dependantunkid"
        Me.objcolhDependantunkid.Width = 0
        '
        'objcolhsicksheetunkid
        '
        Me.objcolhsicksheetunkid.DisplayIndex = 10
        Me.objcolhsicksheetunkid.Tag = "objcolhsicksheetunkid"
        Me.objcolhsicksheetunkid.Text = "Sicksheetunkid"
        Me.objcolhsicksheetunkid.Width = 0
        '
        'objcolhRemarks
        '
        Me.objcolhRemarks.DisplayIndex = 6
        Me.objcolhRemarks.Tag = "objcolhRemarks"
        Me.objcolhRemarks.Text = "Remarks"
        Me.objcolhRemarks.Width = 0
        '
        'objColhEmpexempt
        '
        Me.objColhEmpexempt.Tag = "objColhEmpexempt"
        Me.objColhEmpexempt.Text = "Employee Exempt From Deduction"
        Me.objColhEmpexempt.Width = 0
        '
        'objbtnAddService
        '
        Me.objbtnAddService.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddService.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddService.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddService.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddService.BorderSelected = False
        Me.objbtnAddService.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddService.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddService.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddService.Location = New System.Drawing.Point(313, 36)
        Me.objbtnAddService.Name = "objbtnAddService"
        Me.objbtnAddService.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddService.TabIndex = 4
        Me.objbtnAddService.Visible = False
        '
        'lblTotAmt
        '
        Me.lblTotAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotAmt.Location = New System.Drawing.Point(624, 447)
        Me.lblTotAmt.Name = "lblTotAmt"
        Me.lblTotAmt.Size = New System.Drawing.Size(82, 19)
        Me.lblTotAmt.TabIndex = 223
        Me.lblTotAmt.Text = "Total Amount"
        Me.lblTotAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbTreatment
        '
        Me.gbTreatment.BorderColor = System.Drawing.Color.Black
        Me.gbTreatment.Checked = False
        Me.gbTreatment.CollapseAllExceptThis = False
        Me.gbTreatment.CollapsedHoverImage = Nothing
        Me.gbTreatment.CollapsedNormalImage = Nothing
        Me.gbTreatment.CollapsedPressedImage = Nothing
        Me.gbTreatment.CollapseOnLoad = False
        Me.gbTreatment.Controls.Add(Me.chkSelect)
        Me.gbTreatment.Controls.Add(Me.lvTreatment)
        Me.gbTreatment.ExpandedHoverImage = Nothing
        Me.gbTreatment.ExpandedNormalImage = Nothing
        Me.gbTreatment.ExpandedPressedImage = Nothing
        Me.gbTreatment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTreatment.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTreatment.HeaderHeight = 25
        Me.gbTreatment.HeaderMessage = ""
        Me.gbTreatment.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTreatment.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTreatment.HeightOnCollapse = 0
        Me.gbTreatment.LeftTextSpace = 0
        Me.gbTreatment.Location = New System.Drawing.Point(47, 362)
        Me.gbTreatment.Name = "gbTreatment"
        Me.gbTreatment.OpenHeight = 300
        Me.gbTreatment.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTreatment.ShowBorder = True
        Me.gbTreatment.ShowCheckBox = False
        Me.gbTreatment.ShowCollapseButton = False
        Me.gbTreatment.ShowDefaultBorderColor = True
        Me.gbTreatment.ShowDownButton = False
        Me.gbTreatment.ShowHeader = True
        Me.gbTreatment.Size = New System.Drawing.Size(172, 61)
        Me.gbTreatment.TabIndex = 10
        Me.gbTreatment.Temp = 0
        Me.gbTreatment.Text = "Treatment"
        Me.gbTreatment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSelect
        '
        Me.chkSelect.AutoSize = True
        Me.chkSelect.Location = New System.Drawing.Point(9, 29)
        Me.chkSelect.Name = "chkSelect"
        Me.chkSelect.Size = New System.Drawing.Size(15, 14)
        Me.chkSelect.TabIndex = 17
        Me.chkSelect.TabStop = False
        Me.chkSelect.UseVisualStyleBackColor = True
        '
        'lvTreatment
        '
        Me.lvTreatment.BackColorOnChecked = True
        Me.lvTreatment.CheckBoxes = True
        Me.lvTreatment.ColumnHeaders = Nothing
        Me.lvTreatment.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhSelect, Me.colhTreatment})
        Me.lvTreatment.CompulsoryColumns = ""
        Me.lvTreatment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTreatment.FullRowSelect = True
        Me.lvTreatment.GridLines = True
        Me.lvTreatment.GroupingColumn = Nothing
        Me.lvTreatment.HideSelection = False
        Me.lvTreatment.Location = New System.Drawing.Point(1, -50)
        Me.lvTreatment.MinColumnWidth = 50
        Me.lvTreatment.MultiSelect = False
        Me.lvTreatment.Name = "lvTreatment"
        Me.lvTreatment.OptionalColumns = ""
        Me.lvTreatment.ShowMoreItem = False
        Me.lvTreatment.ShowSaveItem = False
        Me.lvTreatment.ShowSelectAll = True
        Me.lvTreatment.ShowSizeAllColumnsToFit = True
        Me.lvTreatment.Size = New System.Drawing.Size(168, 108)
        Me.lvTreatment.Sortable = True
        Me.lvTreatment.TabIndex = 16
        Me.lvTreatment.TabStop = False
        Me.lvTreatment.UseCompatibleStateImageBehavior = False
        Me.lvTreatment.View = System.Windows.Forms.View.Details
        '
        'colhSelect
        '
        Me.colhSelect.Tag = "colhSelect"
        Me.colhSelect.Text = ""
        Me.colhSelect.Width = 25
        '
        'colhTreatment
        '
        Me.colhTreatment.Tag = "colhTreatment"
        Me.colhTreatment.Text = "Treatment"
        Me.colhTreatment.Width = 139
        '
        'txtServiceNo
        '
        Me.txtServiceNo.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtServiceNo.Flags = 0
        Me.txtServiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServiceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtServiceNo.Location = New System.Drawing.Point(91, 63)
        Me.txtServiceNo.Name = "txtServiceNo"
        Me.txtServiceNo.ReadOnly = True
        Me.txtServiceNo.Size = New System.Drawing.Size(149, 21)
        Me.txtServiceNo.TabIndex = 5
        Me.txtServiceNo.TabStop = False
        Me.txtServiceNo.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(777, 210)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(93, 30)
        Me.btnDelete.TabIndex = 16
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'lblService
        '
        Me.lblService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblService.Location = New System.Drawing.Point(15, 38)
        Me.lblService.Name = "lblService"
        Me.lblService.Size = New System.Drawing.Size(70, 15)
        Me.lblService.TabIndex = 100
        Me.lblService.Text = "Service"
        Me.lblService.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblService.Visible = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(579, 210)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(93, 30)
        Me.btnAdd.TabIndex = 14
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'cboCover
        '
        Me.cboCover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCover.DropDownWidth = 150
        Me.cboCover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCover.FormattingEnabled = True
        Me.cboCover.Location = New System.Drawing.Point(91, 89)
        Me.cboCover.Name = "cboCover"
        Me.cboCover.Size = New System.Drawing.Size(216, 21)
        Me.cboCover.TabIndex = 6
        Me.cboCover.TabStop = False
        Me.cboCover.Visible = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(678, 210)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(93, 30)
        Me.btnEdit.TabIndex = 15
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'lblServiceNo
        '
        Me.lblServiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServiceNo.Location = New System.Drawing.Point(15, 66)
        Me.lblServiceNo.Name = "lblServiceNo"
        Me.lblServiceNo.Size = New System.Drawing.Size(70, 15)
        Me.lblServiceNo.TabIndex = 95
        Me.lblServiceNo.Text = "Provider No"
        Me.lblServiceNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblServiceNo.Visible = False
        '
        'gbEmployeeInfo
        '
        Me.gbEmployeeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.Checked = False
        Me.gbEmployeeInfo.CollapseAllExceptThis = False
        Me.gbEmployeeInfo.CollapsedHoverImage = Nothing
        Me.gbEmployeeInfo.CollapsedNormalImage = Nothing
        Me.gbEmployeeInfo.CollapsedPressedImage = Nothing
        Me.gbEmployeeInfo.CollapseOnLoad = False
        Me.gbEmployeeInfo.Controls.Add(Me.chkEmpexempted)
        Me.gbEmployeeInfo.Controls.Add(Me.lblPeriod)
        Me.gbEmployeeInfo.Controls.Add(Me.lblDependants)
        Me.gbEmployeeInfo.Controls.Add(Me.txtClaimAmount)
        Me.gbEmployeeInfo.Controls.Add(Me.cboPeriod)
        Me.gbEmployeeInfo.Controls.Add(Me.lblClaimAmount)
        Me.gbEmployeeInfo.Controls.Add(Me.lblRemarks)
        Me.gbEmployeeInfo.Controls.Add(Me.cboDependants)
        Me.gbEmployeeInfo.Controls.Add(Me.txtRemarks)
        Me.gbEmployeeInfo.Controls.Add(Me.txtClaimNo)
        Me.gbEmployeeInfo.Controls.Add(Me.lblClaimNo)
        Me.gbEmployeeInfo.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeInfo.ExpandedHoverImage = Nothing
        Me.gbEmployeeInfo.ExpandedNormalImage = Nothing
        Me.gbEmployeeInfo.ExpandedPressedImage = Nothing
        Me.gbEmployeeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInfo.HeaderHeight = 25
        Me.gbEmployeeInfo.HeaderMessage = ""
        Me.gbEmployeeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.HeightOnCollapse = 0
        Me.gbEmployeeInfo.LeftTextSpace = 0
        Me.gbEmployeeInfo.Location = New System.Drawing.Point(348, 11)
        Me.gbEmployeeInfo.Name = "gbEmployeeInfo"
        Me.gbEmployeeInfo.OpenHeight = 300
        Me.gbEmployeeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInfo.ShowBorder = True
        Me.gbEmployeeInfo.ShowCheckBox = False
        Me.gbEmployeeInfo.ShowCollapseButton = False
        Me.gbEmployeeInfo.ShowDefaultBorderColor = True
        Me.gbEmployeeInfo.ShowDownButton = False
        Me.gbEmployeeInfo.ShowHeader = True
        Me.gbEmployeeInfo.Size = New System.Drawing.Size(522, 193)
        Me.gbEmployeeInfo.TabIndex = 9
        Me.gbEmployeeInfo.TabStop = True
        Me.gbEmployeeInfo.Temp = 0
        Me.gbEmployeeInfo.Text = "Employee Info"
        Me.gbEmployeeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkEmpexempted
        '
        Me.chkEmpexempted.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmpexempted.Location = New System.Drawing.Point(309, 58)
        Me.chkEmpexempted.Name = "chkEmpexempted"
        Me.chkEmpexempted.Size = New System.Drawing.Size(203, 17)
        Me.chkEmpexempted.TabIndex = 11
        Me.chkEmpexempted.Text = "Exempt From Deduction"
        Me.chkEmpexempted.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(306, 30)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(80, 18)
        Me.lblPeriod.TabIndex = 222
        Me.lblPeriod.Text = "Pay Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDependants
        '
        Me.lblDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDependants.Location = New System.Drawing.Point(9, 57)
        Me.lblDependants.Name = "lblDependants"
        Me.lblDependants.Size = New System.Drawing.Size(71, 18)
        Me.lblDependants.TabIndex = 95
        Me.lblDependants.Text = "Dependants"
        Me.lblDependants.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtClaimAmount
        '
        Me.txtClaimAmount.AllowNegative = True
        Me.txtClaimAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtClaimAmount.DigitsInGroup = 0
        Me.txtClaimAmount.Flags = 0
        Me.txtClaimAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimAmount.Location = New System.Drawing.Point(392, 84)
        Me.txtClaimAmount.MaxDecimalPlaces = 6
        Me.txtClaimAmount.MaxWholeDigits = 21
        Me.txtClaimAmount.Name = "txtClaimAmount"
        Me.txtClaimAmount.Prefix = ""
        Me.txtClaimAmount.RangeMax = 1.7976931348623157E+308
        Me.txtClaimAmount.RangeMin = -1.7976931348623157E+308
        Me.txtClaimAmount.Size = New System.Drawing.Size(120, 21)
        Me.txtClaimAmount.TabIndex = 12
        Me.txtClaimAmount.Text = "0"
        Me.txtClaimAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 150
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(392, 29)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(120, 21)
        Me.cboPeriod.TabIndex = 10
        '
        'lblClaimAmount
        '
        Me.lblClaimAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimAmount.Location = New System.Drawing.Point(306, 85)
        Me.lblClaimAmount.Name = "lblClaimAmount"
        Me.lblClaimAmount.Size = New System.Drawing.Size(80, 18)
        Me.lblClaimAmount.TabIndex = 113
        Me.lblClaimAmount.Text = "Claim Amount"
        Me.lblClaimAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(9, 112)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(71, 18)
        Me.lblRemarks.TabIndex = 219
        Me.lblRemarks.Text = "Remarks"
        Me.lblRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDependants
        '
        Me.cboDependants.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDependants.FormattingEnabled = True
        Me.cboDependants.Location = New System.Drawing.Point(85, 56)
        Me.cboDependants.Name = "cboDependants"
        Me.cboDependants.Size = New System.Drawing.Size(190, 21)
        Me.cboDependants.TabIndex = 8
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemarks.Location = New System.Drawing.Point(85, 111)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(427, 73)
        Me.txtRemarks.TabIndex = 13
        '
        'txtClaimNo
        '
        Me.txtClaimNo.Flags = 0
        Me.txtClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClaimNo.Location = New System.Drawing.Point(85, 84)
        Me.txtClaimNo.Name = "txtClaimNo"
        Me.txtClaimNo.Size = New System.Drawing.Size(190, 21)
        Me.txtClaimNo.TabIndex = 9
        '
        'lblClaimNo
        '
        Me.lblClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimNo.Location = New System.Drawing.Point(9, 85)
        Me.lblClaimNo.Name = "lblClaimNo"
        Me.lblClaimNo.Size = New System.Drawing.Size(71, 18)
        Me.lblClaimNo.TabIndex = 97
        Me.lblClaimNo.Text = "Claim No."
        Me.lblClaimNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(85, 29)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(190, 21)
        Me.cboEmployee.TabIndex = 7
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(279, 26)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 27)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(9, 30)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(71, 18)
        Me.lblEmployee.TabIndex = 91
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 150
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(480, 23)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(126, 21)
        Me.cboYear.TabIndex = 8
        Me.cboYear.TabStop = False
        '
        'lblCover
        '
        Me.lblCover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCover.Location = New System.Drawing.Point(15, 92)
        Me.lblCover.Name = "lblCover"
        Me.lblCover.Size = New System.Drawing.Size(70, 15)
        Me.lblCover.TabIndex = 107
        Me.lblCover.Text = "Cover"
        Me.lblCover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCover.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnFinalSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 474)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(882, 55)
        Me.objFooter.TabIndex = 25
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(567, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 18
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(773, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 17
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnFinalSave
        '
        Me.btnFinalSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinalSave.BackColor = System.Drawing.Color.White
        Me.btnFinalSave.BackgroundImage = CType(resources.GetObject("btnFinalSave.BackgroundImage"), System.Drawing.Image)
        Me.btnFinalSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinalSave.BorderColor = System.Drawing.Color.Empty
        Me.btnFinalSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinalSave.FlatAppearance.BorderSize = 0
        Me.btnFinalSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinalSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalSave.ForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinalSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Location = New System.Drawing.Point(670, 13)
        Me.btnFinalSave.Name = "btnFinalSave"
        Me.btnFinalSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Size = New System.Drawing.Size(97, 30)
        Me.btnFinalSave.TabIndex = 16
        Me.btnFinalSave.Text = "&Final Save"
        Me.btnFinalSave.UseVisualStyleBackColor = True
        '
        'cboServices
        '
        Me.cboServices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboServices.DropDownWidth = 150
        Me.cboServices.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboServices.FormattingEnabled = True
        Me.cboServices.Location = New System.Drawing.Point(91, 36)
        Me.cboServices.Name = "cboServices"
        Me.cboServices.Size = New System.Drawing.Size(216, 21)
        Me.cboServices.TabIndex = 4
        Me.cboServices.TabStop = False
        Me.cboServices.Visible = False
        '
        'dtpTreatmentdate
        '
        Me.dtpTreatmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTreatmentdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTreatmentdate.Location = New System.Drawing.Point(486, 149)
        Me.dtpTreatmentdate.Name = "dtpTreatmentdate"
        Me.dtpTreatmentdate.Size = New System.Drawing.Size(93, 21)
        Me.dtpTreatmentdate.TabIndex = 12
        Me.dtpTreatmentdate.TabStop = False
        '
        'lblTreatmentDate
        '
        Me.lblTreatmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTreatmentDate.Location = New System.Drawing.Point(392, 152)
        Me.lblTreatmentDate.Name = "lblTreatmentDate"
        Me.lblTreatmentDate.Size = New System.Drawing.Size(89, 15)
        Me.lblTreatmentDate.TabIndex = 111
        Me.lblTreatmentDate.Text = "Treatment Date"
        Me.lblTreatmentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMedicalYear
        '
        Me.lblMedicalYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicalYear.Location = New System.Drawing.Point(383, 26)
        Me.lblMedicalYear.Name = "lblMedicalYear"
        Me.lblMedicalYear.Size = New System.Drawing.Size(87, 15)
        Me.lblMedicalYear.TabIndex = 101
        Me.lblMedicalYear.Text = "Year"
        Me.lblMedicalYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmMedicalClaim_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(882, 529)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMedicalClaim_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Medical Claim"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.cmImportClaim.ResumeLayout(False)
        Me.gbClaimInfo.ResumeLayout(False)
        Me.gbClaimInfo.PerformLayout()
        Me.gbTreatment.ResumeLayout(False)
        Me.gbTreatment.PerformLayout()
        Me.gbEmployeeInfo.ResumeLayout(False)
        Me.gbEmployeeInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnFinalSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbClaimInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblDependants As System.Windows.Forms.Label
    Friend WithEvents cboDependants As System.Windows.Forms.ComboBox
    Friend WithEvents txtServiceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblServiceNo As System.Windows.Forms.Label
    Friend WithEvents gbEmployeeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtClaimNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblClaimNo As System.Windows.Forms.Label
    Friend WithEvents dtpClaimDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblClaimDate As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblMedicalYear As System.Windows.Forms.Label
    Friend WithEvents lblCover As System.Windows.Forms.Label
    Friend WithEvents cboInstitution As System.Windows.Forms.ComboBox
    Friend WithEvents cboCover As System.Windows.Forms.ComboBox
    Friend WithEvents lblInstutution As System.Windows.Forms.Label
    Friend WithEvents dtpTreatmentdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTreatmentDate As System.Windows.Forms.Label
    Friend WithEvents lblClaimAmount As System.Windows.Forms.Label
    Friend WithEvents txtClaimAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lvClaim As eZee.Common.eZeeListView
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDependants As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClaimNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClaimDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDependantunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddInstitution As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddService As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddCover As eZee.Common.eZeeGradientButton
    Friend WithEvents lblService As System.Windows.Forms.Label
    Friend WithEvents gbTreatment As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lvTreatment As eZee.Common.eZeeListView
    Friend WithEvents colhTreatment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSelect As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkSelect As System.Windows.Forms.CheckBox
    Friend WithEvents cboServices As System.Windows.Forms.ComboBox
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents objcolhRemarks As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblTotAmt As System.Windows.Forms.Label
    Friend WithEvents txtSickSheetNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSicksheetNo As System.Windows.Forms.Label
    Friend WithEvents lblTotInvoiceAmt As System.Windows.Forms.Label
    Friend WithEvents txtInvoiceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblInvoiceno As System.Windows.Forms.Label
    Friend WithEvents colhSickSheetNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtTotInvoiceAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents objcolhsicksheetunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtTotalAmountVal As eZee.TextBox.NumericTextBox
    Friend WithEvents BtnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmImportClaim As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnImportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnGetFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents chkEmpexempted As System.Windows.Forms.CheckBox
    Friend WithEvents objColhEmpexempt As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtpInvoicedate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblInvoiceDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchInstitution As eZee.Common.eZeeGradientButton
End Class
