<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMailBox
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMailBox))
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.chkViewUnreadOnly = New System.Windows.Forms.CheckBox
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlViewMsg = New System.Windows.Forms.Panel
        Me.pnlSenderInformation = New System.Windows.Forms.Panel
        Me.objtxtSenderInfo = New System.Windows.Forms.RichTextBox
        Me.objtxtMessage = New System.Windows.Forms.RichTextBox
        Me.pnlMsgList = New System.Windows.Forms.Panel
        Me.dgvMessage = New System.Windows.Forms.DataGridView
        Me.objdgcolCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgColRecipient = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolSender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolMessageSubject = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolDateTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolMessageUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolMessageTranUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolIsRead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolMsg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApplicant = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objlblMsgTitle = New System.Windows.Forms.Label
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.chkViewApplicant = New System.Windows.Forms.CheckBox
        Me.chkViewEmployee = New System.Windows.Forms.CheckBox
        Me.btnMarkAsRead = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnRead = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlModification = New System.Windows.Forms.Panel
        Me.btnExportLetters = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnPrintLetters = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnTrash = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSent = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnWriteMessage = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.pnlViewMsg.SuspendLayout()
        Me.pnlSenderInformation.SuspendLayout()
        Me.pnlMsgList.SuspendLayout()
        CType(Me.dgvMessage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefFormFooter.SuspendLayout()
        Me.pnlModification.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.chkSelectAll.Location = New System.Drawing.Point(229, 20)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(85, 17)
        Me.chkSelectAll.TabIndex = 0
        Me.chkSelectAll.Text = "Select All"
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'chkViewUnreadOnly
        '
        Me.chkViewUnreadOnly.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.chkViewUnreadOnly.Location = New System.Drawing.Point(320, 20)
        Me.chkViewUnreadOnly.Name = "chkViewUnreadOnly"
        Me.chkViewUnreadOnly.Size = New System.Drawing.Size(112, 17)
        Me.chkViewUnreadOnly.TabIndex = 1
        Me.chkViewUnreadOnly.Text = "View Unread Only"
        Me.chkViewUnreadOnly.UseVisualStyleBackColor = True
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.Transparent
        Me.pnlMain.Controls.Add(Me.pnlViewMsg)
        Me.pnlMain.Controls.Add(Me.pnlMsgList)
        Me.pnlMain.Controls.Add(Me.objefFormFooter)
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Controls.Add(Me.pnlModification)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(832, 487)
        Me.pnlMain.TabIndex = 0
        '
        'pnlViewMsg
        '
        Me.pnlViewMsg.Controls.Add(Me.pnlSenderInformation)
        Me.pnlViewMsg.Controls.Add(Me.objtxtMessage)
        Me.pnlViewMsg.Location = New System.Drawing.Point(168, 66)
        Me.pnlViewMsg.Name = "pnlViewMsg"
        Me.pnlViewMsg.Size = New System.Drawing.Size(654, 360)
        Me.pnlViewMsg.TabIndex = 223
        Me.pnlViewMsg.Visible = False
        '
        'pnlSenderInformation
        '
        Me.pnlSenderInformation.Controls.Add(Me.objtxtSenderInfo)
        Me.pnlSenderInformation.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlSenderInformation.Location = New System.Drawing.Point(0, 0)
        Me.pnlSenderInformation.Name = "pnlSenderInformation"
        Me.pnlSenderInformation.Size = New System.Drawing.Size(654, 78)
        Me.pnlSenderInformation.TabIndex = 5
        '
        'objtxtSenderInfo
        '
        Me.objtxtSenderInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objtxtSenderInfo.Location = New System.Drawing.Point(0, 0)
        Me.objtxtSenderInfo.Name = "objtxtSenderInfo"
        Me.objtxtSenderInfo.Size = New System.Drawing.Size(654, 78)
        Me.objtxtSenderInfo.TabIndex = 0
        Me.objtxtSenderInfo.Text = ""
        '
        'objtxtMessage
        '
        Me.objtxtMessage.BackColor = System.Drawing.Color.White
        Me.objtxtMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objtxtMessage.Location = New System.Drawing.Point(0, 80)
        Me.objtxtMessage.Name = "objtxtMessage"
        Me.objtxtMessage.ReadOnly = True
        Me.objtxtMessage.Size = New System.Drawing.Size(654, 277)
        Me.objtxtMessage.TabIndex = 0
        Me.objtxtMessage.Text = ""
        '
        'pnlMsgList
        '
        Me.pnlMsgList.Controls.Add(Me.dgvMessage)
        Me.pnlMsgList.Controls.Add(Me.objlblMsgTitle)
        Me.pnlMsgList.Location = New System.Drawing.Point(168, 66)
        Me.pnlMsgList.Name = "pnlMsgList"
        Me.pnlMsgList.Size = New System.Drawing.Size(654, 360)
        Me.pnlMsgList.TabIndex = 222
        '
        'dgvMessage
        '
        Me.dgvMessage.AllowUserToAddRows = False
        Me.dgvMessage.AllowUserToDeleteRows = False
        Me.dgvMessage.AllowUserToResizeColumns = False
        Me.dgvMessage.AllowUserToResizeRows = False
        Me.dgvMessage.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvMessage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvMessage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMessage.ColumnHeadersVisible = False
        Me.dgvMessage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolCheck, Me.objdgColRecipient, Me.objdgcolSender, Me.objdgcolMessageSubject, Me.objdgcolDateTime, Me.objdgcolMessageUnkId, Me.objdgcolMessageTranUnkid, Me.objdgcolIsRead, Me.objdgcolMsg, Me.objdgcolhApplicant})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.SteelBlue
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMessage.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvMessage.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvMessage.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvMessage.Location = New System.Drawing.Point(0, 42)
        Me.dgvMessage.MultiSelect = False
        Me.dgvMessage.Name = "dgvMessage"
        Me.dgvMessage.ReadOnly = True
        Me.dgvMessage.RowHeadersVisible = False
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        Me.dgvMessage.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvMessage.RowTemplate.Height = 30
        Me.dgvMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvMessage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMessage.Size = New System.Drawing.Size(654, 304)
        Me.dgvMessage.TabIndex = 0
        '
        'objdgcolCheck
        '
        Me.objdgcolCheck.FalseValue = "0"
        Me.objdgcolCheck.HeaderText = "Check"
        Me.objdgcolCheck.Name = "objdgcolCheck"
        Me.objdgcolCheck.ReadOnly = True
        Me.objdgcolCheck.TrueValue = "1"
        Me.objdgcolCheck.Width = 50
        '
        'objdgColRecipient
        '
        Me.objdgColRecipient.HeaderText = "Recipient"
        Me.objdgColRecipient.Name = "objdgColRecipient"
        Me.objdgColRecipient.ReadOnly = True
        '
        'objdgcolSender
        '
        Me.objdgcolSender.HeaderText = "Sender"
        Me.objdgcolSender.Name = "objdgcolSender"
        Me.objdgcolSender.ReadOnly = True
        '
        'objdgcolMessageSubject
        '
        Me.objdgcolMessageSubject.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.objdgcolMessageSubject.HeaderText = "MessageSubject"
        Me.objdgcolMessageSubject.Name = "objdgcolMessageSubject"
        Me.objdgcolMessageSubject.ReadOnly = True
        '
        'objdgcolDateTime
        '
        Me.objdgcolDateTime.HeaderText = "DateTime"
        Me.objdgcolDateTime.Name = "objdgcolDateTime"
        Me.objdgcolDateTime.ReadOnly = True
        Me.objdgcolDateTime.Width = 85
        '
        'objdgcolMessageUnkId
        '
        Me.objdgcolMessageUnkId.HeaderText = "MessageUnkId"
        Me.objdgcolMessageUnkId.Name = "objdgcolMessageUnkId"
        Me.objdgcolMessageUnkId.ReadOnly = True
        Me.objdgcolMessageUnkId.Visible = False
        '
        'objdgcolMessageTranUnkid
        '
        Me.objdgcolMessageTranUnkid.HeaderText = "MessageTranUnkid"
        Me.objdgcolMessageTranUnkid.Name = "objdgcolMessageTranUnkid"
        Me.objdgcolMessageTranUnkid.ReadOnly = True
        Me.objdgcolMessageTranUnkid.Visible = False
        '
        'objdgcolIsRead
        '
        Me.objdgcolIsRead.HeaderText = "IsRead"
        Me.objdgcolIsRead.Name = "objdgcolIsRead"
        Me.objdgcolIsRead.ReadOnly = True
        Me.objdgcolIsRead.Visible = False
        '
        'objdgcolMsg
        '
        Me.objdgcolMsg.HeaderText = "Msg"
        Me.objdgcolMsg.Name = "objdgcolMsg"
        Me.objdgcolMsg.ReadOnly = True
        Me.objdgcolMsg.Visible = False
        '
        'objdgcolhApplicant
        '
        Me.objdgcolhApplicant.HeaderText = "objdgcolhApplicant"
        Me.objdgcolhApplicant.Name = "objdgcolhApplicant"
        Me.objdgcolhApplicant.ReadOnly = True
        Me.objdgcolhApplicant.Visible = False
        '
        'objlblMsgTitle
        '
        Me.objlblMsgTitle.BackColor = System.Drawing.Color.SteelBlue
        Me.objlblMsgTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.objlblMsgTitle.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblMsgTitle.ForeColor = System.Drawing.Color.White
        Me.objlblMsgTitle.Location = New System.Drawing.Point(0, 0)
        Me.objlblMsgTitle.Name = "objlblMsgTitle"
        Me.objlblMsgTitle.Size = New System.Drawing.Size(654, 42)
        Me.objlblMsgTitle.TabIndex = 30
        Me.objlblMsgTitle.Text = "Sent Message"
        Me.objlblMsgTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.chkViewApplicant)
        Me.objefFormFooter.Controls.Add(Me.chkViewEmployee)
        Me.objefFormFooter.Controls.Add(Me.btnMarkAsRead)
        Me.objefFormFooter.Controls.Add(Me.btnRead)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Controls.Add(Me.btnDelete)
        Me.objefFormFooter.Controls.Add(Me.chkViewUnreadOnly)
        Me.objefFormFooter.Controls.Add(Me.chkSelectAll)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 432)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(832, 55)
        Me.objefFormFooter.TabIndex = 2
        '
        'chkViewApplicant
        '
        Me.chkViewApplicant.Checked = True
        Me.chkViewApplicant.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkViewApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.chkViewApplicant.Location = New System.Drawing.Point(9, 20)
        Me.chkViewApplicant.Name = "chkViewApplicant"
        Me.chkViewApplicant.Size = New System.Drawing.Size(99, 17)
        Me.chkViewApplicant.TabIndex = 7
        Me.chkViewApplicant.Text = "View Applicant"
        Me.chkViewApplicant.UseVisualStyleBackColor = True
        '
        'chkViewEmployee
        '
        Me.chkViewEmployee.Checked = True
        Me.chkViewEmployee.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkViewEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.chkViewEmployee.Location = New System.Drawing.Point(114, 20)
        Me.chkViewEmployee.Name = "chkViewEmployee"
        Me.chkViewEmployee.Size = New System.Drawing.Size(100, 17)
        Me.chkViewEmployee.TabIndex = 6
        Me.chkViewEmployee.Text = "View Employee"
        Me.chkViewEmployee.UseVisualStyleBackColor = True
        '
        'btnMarkAsRead
        '
        Me.btnMarkAsRead.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMarkAsRead.BackColor = System.Drawing.Color.White
        Me.btnMarkAsRead.BackgroundImage = CType(resources.GetObject("btnMarkAsRead.BackgroundImage"), System.Drawing.Image)
        Me.btnMarkAsRead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMarkAsRead.BorderColor = System.Drawing.Color.Empty
        Me.btnMarkAsRead.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMarkAsRead.FlatAppearance.BorderSize = 0
        Me.btnMarkAsRead.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMarkAsRead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMarkAsRead.ForeColor = System.Drawing.Color.Black
        Me.btnMarkAsRead.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMarkAsRead.GradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsRead.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMarkAsRead.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsRead.Location = New System.Drawing.Point(534, 12)
        Me.btnMarkAsRead.Name = "btnMarkAsRead"
        Me.btnMarkAsRead.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMarkAsRead.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsRead.Size = New System.Drawing.Size(96, 30)
        Me.btnMarkAsRead.TabIndex = 3
        Me.btnMarkAsRead.Text = "&Mark as Read"
        Me.btnMarkAsRead.UseVisualStyleBackColor = False
        '
        'btnRead
        '
        Me.btnRead.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRead.BackColor = System.Drawing.Color.White
        Me.btnRead.BackgroundImage = CType(resources.GetObject("btnRead.BackgroundImage"), System.Drawing.Image)
        Me.btnRead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRead.BorderColor = System.Drawing.Color.Empty
        Me.btnRead.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRead.FlatAppearance.BorderSize = 0
        Me.btnRead.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRead.ForeColor = System.Drawing.Color.Black
        Me.btnRead.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRead.GradientForeColor = System.Drawing.Color.Black
        Me.btnRead.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRead.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRead.Location = New System.Drawing.Point(438, 12)
        Me.btnRead.Name = "btnRead"
        Me.btnRead.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRead.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRead.Size = New System.Drawing.Size(90, 30)
        Me.btnRead.TabIndex = 2
        Me.btnRead.Text = "Read"
        Me.btnRead.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(732, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(636, 12)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 4
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.Color.White
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(832, 60)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Mail Box"
        '
        'pnlModification
        '
        Me.pnlModification.BackColor = System.Drawing.Color.White
        Me.pnlModification.Controls.Add(Me.btnExportLetters)
        Me.pnlModification.Controls.Add(Me.btnPrintLetters)
        Me.pnlModification.Controls.Add(Me.btnTrash)
        Me.pnlModification.Controls.Add(Me.btnSent)
        Me.pnlModification.Controls.Add(Me.btnWriteMessage)
        Me.pnlModification.Location = New System.Drawing.Point(9, 66)
        Me.pnlModification.Name = "pnlModification"
        Me.pnlModification.Padding = New System.Windows.Forms.Padding(2)
        Me.pnlModification.Size = New System.Drawing.Size(153, 360)
        Me.pnlModification.TabIndex = 6
        '
        'btnExportLetters
        '
        Me.btnExportLetters.BackColor = System.Drawing.Color.White
        Me.btnExportLetters.BackgroundImage = CType(resources.GetObject("btnExportLetters.BackgroundImage"), System.Drawing.Image)
        Me.btnExportLetters.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExportLetters.BorderColor = System.Drawing.Color.Empty
        Me.btnExportLetters.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnExportLetters.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnExportLetters.FlatAppearance.BorderSize = 0
        Me.btnExportLetters.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportLetters.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExportLetters.ForeColor = System.Drawing.Color.Black
        Me.btnExportLetters.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExportLetters.GradientForeColor = System.Drawing.Color.Black
        Me.btnExportLetters.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExportLetters.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExportLetters.Location = New System.Drawing.Point(2, 150)
        Me.btnExportLetters.Name = "btnExportLetters"
        Me.btnExportLetters.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExportLetters.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExportLetters.Size = New System.Drawing.Size(149, 37)
        Me.btnExportLetters.TabIndex = 10
        Me.btnExportLetters.Text = "Export Letter"
        Me.btnExportLetters.UseVisualStyleBackColor = False
        '
        'btnPrintLetters
        '
        Me.btnPrintLetters.BackColor = System.Drawing.Color.White
        Me.btnPrintLetters.BackgroundImage = CType(resources.GetObject("btnPrintLetters.BackgroundImage"), System.Drawing.Image)
        Me.btnPrintLetters.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPrintLetters.BorderColor = System.Drawing.Color.Empty
        Me.btnPrintLetters.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnPrintLetters.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnPrintLetters.FlatAppearance.BorderSize = 0
        Me.btnPrintLetters.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrintLetters.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintLetters.ForeColor = System.Drawing.Color.Black
        Me.btnPrintLetters.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrintLetters.GradientForeColor = System.Drawing.Color.Black
        Me.btnPrintLetters.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrintLetters.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPrintLetters.Location = New System.Drawing.Point(2, 113)
        Me.btnPrintLetters.Name = "btnPrintLetters"
        Me.btnPrintLetters.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrintLetters.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPrintLetters.Size = New System.Drawing.Size(149, 37)
        Me.btnPrintLetters.TabIndex = 9
        Me.btnPrintLetters.Text = "Print Letter"
        Me.btnPrintLetters.UseVisualStyleBackColor = False
        '
        'btnTrash
        '
        Me.btnTrash.BackColor = System.Drawing.Color.White
        Me.btnTrash.BackgroundImage = CType(resources.GetObject("btnTrash.BackgroundImage"), System.Drawing.Image)
        Me.btnTrash.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTrash.BorderColor = System.Drawing.Color.Empty
        Me.btnTrash.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnTrash.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnTrash.FlatAppearance.BorderSize = 0
        Me.btnTrash.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTrash.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTrash.ForeColor = System.Drawing.Color.Black
        Me.btnTrash.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTrash.GradientForeColor = System.Drawing.Color.Black
        Me.btnTrash.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTrash.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTrash.Location = New System.Drawing.Point(2, 76)
        Me.btnTrash.Name = "btnTrash"
        Me.btnTrash.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTrash.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTrash.Size = New System.Drawing.Size(149, 37)
        Me.btnTrash.TabIndex = 8
        Me.btnTrash.Text = "Trash"
        Me.btnTrash.UseVisualStyleBackColor = False
        '
        'btnSent
        '
        Me.btnSent.BackColor = System.Drawing.Color.White
        Me.btnSent.BackgroundImage = CType(resources.GetObject("btnSent.BackgroundImage"), System.Drawing.Image)
        Me.btnSent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSent.BorderColor = System.Drawing.Color.Empty
        Me.btnSent.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnSent.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSent.FlatAppearance.BorderSize = 0
        Me.btnSent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSent.ForeColor = System.Drawing.Color.Black
        Me.btnSent.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSent.GradientForeColor = System.Drawing.Color.Black
        Me.btnSent.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSent.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSent.Location = New System.Drawing.Point(2, 39)
        Me.btnSent.Name = "btnSent"
        Me.btnSent.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSent.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSent.Selected = True
        Me.btnSent.Size = New System.Drawing.Size(149, 37)
        Me.btnSent.TabIndex = 7
        Me.btnSent.Text = "Sent Message"
        Me.btnSent.UseVisualStyleBackColor = False
        '
        'btnWriteMessage
        '
        Me.btnWriteMessage.BackColor = System.Drawing.Color.White
        Me.btnWriteMessage.BackgroundImage = CType(resources.GetObject("btnWriteMessage.BackgroundImage"), System.Drawing.Image)
        Me.btnWriteMessage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnWriteMessage.BorderColor = System.Drawing.Color.Empty
        Me.btnWriteMessage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnWriteMessage.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnWriteMessage.FlatAppearance.BorderSize = 0
        Me.btnWriteMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWriteMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWriteMessage.ForeColor = System.Drawing.Color.Black
        Me.btnWriteMessage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnWriteMessage.GradientForeColor = System.Drawing.Color.Black
        Me.btnWriteMessage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnWriteMessage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnWriteMessage.Location = New System.Drawing.Point(2, 2)
        Me.btnWriteMessage.Name = "btnWriteMessage"
        Me.btnWriteMessage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnWriteMessage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnWriteMessage.Size = New System.Drawing.Size(149, 37)
        Me.btnWriteMessage.TabIndex = 6
        Me.btnWriteMessage.Text = "Write Message"
        Me.btnWriteMessage.UseVisualStyleBackColor = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Recipient"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Sender"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "MessageSubject"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "DateTime"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 85
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "MessageUnkId"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "MessageTranUnkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "IsRead"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Msg"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'frmMailBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(832, 487)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMailBox"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Mail Box"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlViewMsg.ResumeLayout(False)
        Me.pnlSenderInformation.ResumeLayout(False)
        Me.pnlMsgList.ResumeLayout(False)
        CType(Me.dgvMessage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefFormFooter.ResumeLayout(False)
        Me.pnlModification.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkViewUnreadOnly As System.Windows.Forms.CheckBox
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnMarkAsRead As eZee.Common.eZeeLightButton
    Friend WithEvents btnRead As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlViewMsg As System.Windows.Forms.Panel
    Friend WithEvents pnlSenderInformation As System.Windows.Forms.Panel
    Friend WithEvents objtxtSenderInfo As System.Windows.Forms.RichTextBox
    Friend WithEvents objtxtMessage As System.Windows.Forms.RichTextBox
    Friend WithEvents pnlMsgList As System.Windows.Forms.Panel
    Friend WithEvents dgvMessage As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgColRecipient As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolSender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolMessageSubject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolDateTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolMessageUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolMessageTranUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolIsRead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolMsg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblMsgTitle As System.Windows.Forms.Label
    Friend WithEvents pnlModification As System.Windows.Forms.Panel
    Friend WithEvents btnTrash As eZee.Common.eZeeLightButton
    Friend WithEvents btnWriteMessage As eZee.Common.eZeeLightButton
    Friend WithEvents btnSent As eZee.Common.eZeeLightButton
    Friend WithEvents btnPrintLetters As eZee.Common.eZeeLightButton
    Friend WithEvents btnExportLetters As eZee.Common.eZeeLightButton
 Friend WithEvents objdgcolhApplicant As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents chkViewApplicant As System.Windows.Forms.CheckBox
    Friend WithEvents chkViewEmployee As System.Windows.Forms.CheckBox
End Class
