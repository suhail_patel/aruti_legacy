Option Strict On

#Region " Imports "
Imports System
Imports System.Data
Imports Aruti.Data
Imports Aruti.Data.Language
Imports eZeeCommonLib
Imports System.Windows.Forms
Imports System.IO
Imports System.Text
Imports System.Collections.Specialized

#End Region

Public Class frmMailBox

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmMailBox"
    Private objMessaging As New clsEmail_Master
    Private mblnPermanentDelete As Boolean = False
    Private mintSelectedView As Integer = 1 ' 0 = Inbox, 1 = Sent, 2 = Trash
    Dim objfrm As frmSendMail

#End Region

    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    <Runtime.InteropServices.DllImport("gdiplus.dll")> _
 Private Shared Function GdipEmfToWmfBits(ByVal _hEmf As IntPtr, ByVal _bufferSize As UInteger, ByVal _buffer() As Byte, ByVal _mappingMode As Integer, ByVal _flags As EmfToWmfBitsFlags) As UInteger
    End Function

    Private Enum EmfToWmfBitsFlags
        EmfToWmfBitsFlagsDefault = &H0
        EmfToWmfBitsFlagsEmbedEmf = &H1
        EmfToWmfBitsFlagsIncludePlaceable = &H2
        EmfToWmfBitsFlagsNoXORClip = &H4
    End Enum
    'S.SANDEEP [05-Apr-2018] -- END

#Region " Private Function "

    Private Sub FillMsgList()
        Dim dtMessage As DataTable = Nothing
        Try
            Dim objLetterContent As New clsLetterType
            pnlMsgList.Visible = True
            pnlViewMsg.Visible = False
            chkSelectAll.Checked = False
            dtMessage = objMessaging.GetList(User._Object._Userunkid, mintSelectedView, "List")

            'S.SANDEEP |14-JUN-2019| -- START
            'ISSUE/ENHANCEMENT : SLOW RESPONSE ON OPENING
            'For i As Integer = 0 To dtMessage.Rows.Count - 1
            '    If CInt(dtMessage.Rows(i).Item("lettertypeunkid")) = 0 Then Continue For
            '    objLetterContent._LettertypeUnkId = CInt(dtMessage.Rows(i).Item("lettertypeunkid"))
            '    dtMessage.AcceptChanges()
            'Next
            'S.SANDEEP |14-JUN-2019| -- END


            If mintSelectedView <> 0 Then
                objdgcolSender.Visible = False
            Else
                objdgcolSender.Visible = True
            End If

            If chkViewUnreadOnly.Checked = True Then
                dtMessage = New DataView(dtMessage, "isread = 0 ", "", DataViewRowState.CurrentRows).ToTable
            End If


            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If chkViewApplicant.CheckState = CheckState.Checked And chkViewEmployee.CheckState = CheckState.Checked Then
            ElseIf chkViewApplicant.CheckState = CheckState.Unchecked And chkViewEmployee.CheckState = CheckState.Unchecked Then
                dtMessage = New DataView(dtMessage, "isapplicant = 0 AND isapplicant = 1 ", "", DataViewRowState.CurrentRows).ToTable
            ElseIf chkViewApplicant.CheckState = CheckState.Checked Then
                dtMessage = New DataView(dtMessage, "isapplicant = 1 ", "", DataViewRowState.CurrentRows).ToTable
            ElseIf chkViewEmployee.CheckState = CheckState.Checked Then
                dtMessage = New DataView(dtMessage, "isapplicant = 0 ", "", DataViewRowState.CurrentRows).ToTable
            End If
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            dgvMessage.AutoGenerateColumns = False
            dgvMessage.DataSource = dtMessage

            objdgcolCheck.DataPropertyName = "tempcheck"
            objdgcolDateTime.DataPropertyName = "emaildatetime"
            objdgcolMessageSubject.DataPropertyName = "subject"
            objdgcolSender.DataPropertyName = "Sender"
            objdgColRecipient.DataPropertyName = "Recipient"
            objdgcolMsg.DataPropertyName = "message"

            objdgcolIsRead.DataPropertyName = "isread"
            objdgcolMessageTranUnkid.DataPropertyName = "emailtranunkid"
            objdgcolMessageUnkId.DataPropertyName = "emailmasterunkid"
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objdgcolhApplicant.DataPropertyName = "isapplicant"
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            Select Case mintSelectedView
                Case 1
                    objlblMsgTitle.Text = btnSent.Text & " (" & dgvMessage.Rows.Count & ")"
                Case 2
                    objlblMsgTitle.Text = btnTrash.Text & " (" & dgvMessage.Rows.Count & ")"
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMsgList", mstrModuleName)
        Finally
            If dtMessage IsNot Nothing Then
                dtMessage.Dispose()
            End If
            dtMessage = Nothing
        End Try
    End Sub

    Private Sub ViewMergeData()
        Dim objLetterFields As New clsLetterFields
        Dim dslist As New DataSet
        Dim strGuestUnkid As String = ""
        strGuestUnkid = CStr(objMessaging.GetRecipient(CInt(dgvMessage.SelectedRows(0).Cells(objdgcolMessageTranUnkid.Index).Value), True, CBool(dgvMessage.SelectedRows(0).Cells(objdgcolhApplicant.Index).Value)))

        'S.SANDEEP [04 JUN 2015] -- START
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'If CBool(dgvMessage.SelectedRows(0).Cells(objdgcolhApplicant.Index).Value) = False Then
        '    dslist = objLetterFields.GetEmployeeData(strGuestUnkid, enImg_Email_RefId.Employee_Module, "List")
        'Else
        '    dslist = objLetterFields.GetEmployeeData(strGuestUnkid, enImg_Email_RefId.Applicant_Module, "List")
        'End If
        Dim intModuleId As Integer = 0
        If CBool(dgvMessage.SelectedRows(0).Cells(objdgcolhApplicant.Index).Value) = False Then
            intModuleId = enImg_Email_RefId.Employee_Module
        Else
            intModuleId = enImg_Email_RefId.Applicant_Module
        End If
        dslist = objLetterFields.GetEmployeeData(strGuestUnkid, intModuleId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, "List")
        'S.SANDEEP [04 JUN 2015] -- END
        Dim strDataName As String = ""
        Try
            Dim StrCol As String = ""
            For j As Integer = 0 To dslist.Tables(0).Columns.Count - 1
                StrCol = dslist.Tables(0).Columns(j).ColumnName
                If objtxtMessage.Rtf.Contains("#" & StrCol & "#") Then
                    objtxtMessage.Focus()
                    'S.SANDEEP [05-Apr-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                    'strDataName = objtxtMessage.Rtf.Replace("#" & StrCol & "#", dslist.Tables(0).Rows(0)(StrCol).ToString)
                    If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                        Dim data As Byte() = CType(dslist.Tables(0).Rows(0)(StrCol), Byte())
                        Dim ms As MemoryStream = New MemoryStream(data)
                        Dim img As Image = Image.FromStream(ms)
                        Dim strValue As String = embedImage(img)
                        strDataName = objtxtMessage.Rtf.Replace("#" & StrCol & "#", strValue)
                    ElseIf StrCol.Trim.ToUpper = "KEY DUTIES AND RESPONSIBILITIES" Then
                        strDataName = objtxtMessage.Rtf.Replace("#" & StrCol & "#", dslist.Tables(0).Rows(0)(StrCol).ToString.Replace("\r\n", "\line"))
                    Else
                    strDataName = objtxtMessage.Rtf.Replace("#" & StrCol & "#", dslist.Tables(0).Rows(0)(StrCol).ToString)
                    End If
                    'S.SANDEEP [05-Apr-2018] -- END
                    objtxtMessage.Rtf = strDataName
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    Private Function embedImage(ByVal img As Image) As String
        Dim rtf As New StringBuilder()
        Try
            rtf.Append("{\rtf1\ansi\ansicpg1252\deff0\deflang1033")
            rtf.Append(GetFontTable(Me.Font))
            rtf.Append(GetImagePrefix(img))
            rtf.Append(getRtfImage(img))
            rtf.Append("}")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "embedImage", mstrModuleName)
        Finally
        End Try
        Return rtf.ToString()
    End Function

    Private Function GetFontTable(ByVal font As Font) As String
        Dim fontTable = New StringBuilder()
        ' Append table control string
        fontTable.Append("{\fonttbl{\f0")
        fontTable.Append("\")
        Dim rtfFontFamily = New HybridDictionary()
        rtfFontFamily.Add(FontFamily.GenericMonospace.Name, "\fmodern")
        rtfFontFamily.Add(FontFamily.GenericSansSerif, "\fswiss")
        rtfFontFamily.Add(FontFamily.GenericSerif, "\froman")
        rtfFontFamily.Add("UNKNOWN", "\fnil")

        ' If the font's family corresponds to an RTF family, append the
        ' RTF family name, else, append the RTF for unknown font family.
        fontTable.Append(If(rtfFontFamily.Contains(font.FontFamily.Name), rtfFontFamily(font.FontFamily.Name), rtfFontFamily("UNKNOWN")))
        ' \fcharset specifies the character set of a font in the font table.
        ' 0 is for ANSI.
        fontTable.Append("\fcharset0 ")
        ' Append the name of the font
        fontTable.Append(font.Name)
        ' Close control string
        fontTable.Append(";}}")
        Return fontTable.ToString()
    End Function

    Private Function GetImagePrefix(ByVal _image As Image) As String
        Dim xDpi, yDpi As Single
        Dim rtf = New StringBuilder()
        Using graphics As Graphics = CreateGraphics()
            xDpi = graphics.DpiX
            yDpi = graphics.DpiY
        End Using
        ' Calculate the current width of the image in (0.01)mm
        Dim picw = CInt(Math.Truncate(Math.Round((_image.Width / xDpi) * 2540)))
        ' Calculate the current height of the image in (0.01)mm
        Dim pich = CInt(Math.Truncate(Math.Round((_image.Height / yDpi) * 2540)))
        ' Calculate the target width of the image in twips
        Dim picwgoal = CInt(Math.Truncate(Math.Round((_image.Width / xDpi) * 1440)))
        ' Calculate the target height of the image in twips
        Dim pichgoal = CInt(Math.Truncate(Math.Round((_image.Height / yDpi) * 1440)))
        ' Append values to RTF string
        rtf.Append("{\pict\wmetafile8")
        rtf.Append("\picw")
        rtf.Append(picw)
        rtf.Append("\pich")
        rtf.Append(pich)
        rtf.Append("\picwgoal")
        rtf.Append(picwgoal)
        rtf.Append("\pichgoal")
        rtf.Append(pichgoal)
        rtf.Append(" ")

        Return rtf.ToString()
    End Function

    Private Function getRtfImage(ByVal image As Image) As String
        ' Used to store the enhanced metafile
        Dim stream As MemoryStream = Nothing
        ' Used to create the metafile and draw the image
        Dim graphics As Graphics = Nothing
        ' The enhanced metafile
        Dim metaFile As Imaging.Metafile = Nothing
        Try
            Dim rtf = New StringBuilder()
            stream = New MemoryStream()
            ' Get a graphics context from the RichTextBox
            graphics = CreateGraphics()
            Using graphics
                ' Get the device context from the graphics context
                Dim hdc As IntPtr = graphics.GetHdc()
                ' Create a new Enhanced Metafile from the device context
                metaFile = New Imaging.Metafile(stream, hdc)
                ' Release the device context
                graphics.ReleaseHdc(hdc)
            End Using

            ' Get a graphics context from the Enhanced Metafile
            graphics = graphics.FromImage(metaFile)
            Using graphics
                ' Draw the image on the Enhanced Metafile
                graphics.DrawImage(image, New Rectangle(0, 0, image.Width, image.Height))
            End Using

            ' Get the handle of the Enhanced Metafile
            Dim hEmf As IntPtr = metaFile.GetHenhmetafile()
            ' A call to EmfToWmfBits with a null buffer return the size of the
            ' buffer need to store the WMF bits.  Use this to get the buffer
            ' size.
            Dim bufferSize As UInteger = GdipEmfToWmfBits(hEmf, 0, Nothing, 8, EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)
            ' Create an array to hold the bits
            Dim buffer = New Byte(CInt(bufferSize - 1)) {}
            ' A call to EmfToWmfBits with a valid buffer copies the bits into the
            ' buffer an returns the number of bits in the WMF.  
            Dim _convertedSize As UInteger = GdipEmfToWmfBits(hEmf, bufferSize, buffer, 8, EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)
            ' Append the bits to the RTF string
            For Each t As Byte In buffer
                rtf.Append(String.Format("{0:X2}", t))
            Next t
            Return rtf.ToString()
        Finally
            If graphics IsNot Nothing Then
                graphics.Dispose()
            End If
            If metaFile IsNot Nothing Then
                metaFile.Dispose()
            End If
            If stream IsNot Nothing Then
                stream.Close()
            End If
        End Try
    End Function
    'S.SANDEEP [05-Apr-2018] -- END

#End Region

#Region " Form's Events "

    Private Sub frmMailBox_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If objfrm IsNot Nothing Then objfrm.Dispose()
    End Sub

    Private Sub frmMailBox_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Call SetLanguage()
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()



            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            RemoveHandler chkViewApplicant.CheckedChanged, AddressOf chkViewApplicant_CheckedChanged
            RemoveHandler chkViewEmployee.CheckedChanged, AddressOf chkViewEmployee_CheckedChanged
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call btnSent_Click(Nothing, Nothing)
            If User._Object.Privilege._AllowtoViewSentItems = True Then
            Call btnSent_Click(Nothing, Nothing)
                btnSent.Enabled = User._Object.Privilege._AllowtoViewSentItems
            Else
                btnSent.Enabled = User._Object.Privilege._AllowtoViewSentItems
                chkSelectAll.Enabled = User._Object.Privilege._AllowtoViewSentItems
                chkViewApplicant.Enabled = User._Object.Privilege._AllowtoViewSentItems
                chkViewEmployee.Enabled = User._Object.Privilege._AllowtoViewSentItems
                chkViewUnreadOnly.Enabled = User._Object.Privilege._AllowtoViewSentItems

                chkViewApplicant.Checked = User._Object.Privilege._AllowtoViewSentItems
                chkViewEmployee.Checked = User._Object.Privilege._AllowtoViewSentItems
            End If
            'S.SANDEEP [ 29 OCT 2012 ] -- END


            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            AddHandler chkViewApplicant.CheckedChanged, AddressOf chkViewApplicant_CheckedChanged
            AddHandler chkViewEmployee.CheckedChanged, AddressOf chkViewEmployee_CheckedChanged
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'Anjan (20 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            btnWriteMessage.Enabled = User._Object.Privilege._AllowToSendMail
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteMail
            btnTrash.Enabled = User._Object.Privilege._AllowtoViewTrashItems
            btnPrintLetters.Enabled = User._Object.Privilege._AllowtoPrintLetters
            btnExportLetters.Enabled = User._Object.Privilege._AllowtoExportLetters
            btnRead.Enabled = User._Object.Privilege._AllowtoReadMails
            btnMarkAsRead.Enabled = User._Object.Privilege._AllowtoReadMails
            'Anjan (20 Jan 2012)-End 

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "frmMailBox_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "
    Private Sub btnSent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSent.Click
        mintSelectedView = 1
        Call FillMsgList()
        mblnPermanentDelete = False
    End Sub

    Private Sub btnTrash_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTrash.Click
        mintSelectedView = 2
        Call FillMsgList()
        mblnPermanentDelete = True
    End Sub


    'Pinkal (12-Oct-2011) -- Start

    Private Sub btnPrintLetters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintLetters.Click
        Try
            Dim objfrm As New frmPrint_Export_Letters
            objfrm._IsPrinting = True
            objfrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnPrintLetters_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExportLetters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportLetters.Click
        Try
            Dim objfrm As New frmPrint_Export_Letters
            objfrm._IsPrinting = False
            objfrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnExportLetters_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Oct-2011) -- End


    Private Sub btnWriteMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWriteMessage.Click
        Try
            If objfrm IsNot Nothing Then
                If mblnIsSendDisposed = False Then
                    objfrm.Activate()
                Else
                    objfrm = New frmSendMail
                    objfrm._IsFromDiary = False
                    objfrm._ModulRefId = enImg_Email_RefId.Employee_Module
                    objfrm.StartPosition = FormStartPosition.Manual
                    objfrm.Left = Me.Left + 50
                    objfrm.Top = Me.Top
                    objfrm.Show()
                    mblnIsSendDisposed = False
                End If
            Else
                objfrm = New frmSendMail
            objfrm._IsFromDiary = False
            objfrm._ModulRefId = enImg_Email_RefId.Employee_Module
                objfrm.StartPosition = FormStartPosition.Manual
                objfrm.Left = Me.Left + 50
                objfrm.Top = Me.Top
                objfrm.Show()
                mblnIsSendDisposed = False

            mblnPermanentDelete = False
            mintSelectedView = 1
            End If
            'Call FillMsgList()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnWriteMessage_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        For Each drRow As DataGridViewRow In dgvMessage.Rows
            If mblnPermanentDelete = True Then
                If CBool(drRow.Cells(objdgcolCheck.Index).Value) Then
                    objMessaging.Delete(CInt(drRow.Cells(objdgcolMessageTranUnkid.Index).Value), _
                                        CInt(drRow.Cells(objdgcolMessageUnkId.Index).Value), True)
                End If
            Else
                If CBool(drRow.Cells(objdgcolCheck.Index).Value) Then
                    objMessaging.Delete(CInt(drRow.Cells(objdgcolMessageTranUnkid.Index).Value), _
                                        CInt(drRow.Cells(objdgcolMessageUnkId.Index).Value))
                End If
            End If

        Next
        Call FillMsgList()
    End Sub

    Private Sub btnMarkAsRead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMarkAsRead.Click
        For Each drRow As DataGridViewRow In dgvMessage.Rows
            If CBool(drRow.Cells(objdgcolCheck.Index).Value) Then
                objMessaging.MarkAsReaded(CInt(drRow.Cells(objdgcolMessageTranUnkid.Index).Value))
            End If
        Next
        Call FillMsgList()
    End Sub

    Private Sub btnRead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRead.Click
        If dgvMessage.SelectedRows.Count <= 0 Then Exit Sub

        Try
            objtxtMessage.Text = ""
            Dim objrrtbDocument As New RichTextBox

            pnlMsgList.Visible = False
            pnlViewMsg.Visible = True


            If btnSent.Selected Then
                objtxtSenderInfo.Text = Language.getMessage(mstrModuleName, 2, "Recipients: ") & objMessaging.GetRecipient(CInt(dgvMessage.SelectedRows(0).Cells(objdgcolMessageTranUnkid.Index).Value)) & vbCrLf & vbCrLf
            Else
                objtxtSenderInfo.Text = Language.getMessage(mstrModuleName, 3, "Sender: ") & dgvMessage.SelectedRows(0).Cells(objdgcolSender.Index).Value.ToString & vbCrLf & vbCrLf
            End If
            objtxtSenderInfo.Text &= Language.getMessage(mstrModuleName, 1, "Subject:") & dgvMessage.SelectedRows(0).Cells(objdgcolMessageSubject.Index).Value.ToString

            objrrtbDocument.Rtf = dgvMessage.SelectedRows(0).Cells(objdgcolMsg.Index).Value.ToString
            objtxtMessage.Rtf = objrrtbDocument.Rtf

            Call ViewMergeData()

            If btnSent.Selected Then
                objtxtSenderInfo.SelectionStart = objtxtSenderInfo.Find(Language.getMessage(mstrModuleName, 2, "Recipients: "))
                objtxtSenderInfo.SelectionFont = New Drawing.Font(objtxtMessage.Font, Drawing.FontStyle.Bold)
            Else
                objtxtSenderInfo.SelectionStart = objtxtSenderInfo.Find(Language.getMessage(mstrModuleName, 3, "Sender: "))
                objtxtSenderInfo.SelectionFont = New Drawing.Font(objtxtMessage.Font, Drawing.FontStyle.Bold)
            End If


            objtxtSenderInfo.SelectionStart = objtxtSenderInfo.Find(Language.getMessage(mstrModuleName, 1, "Subject:"))
            objtxtSenderInfo.SelectionFont = New Drawing.Font(objtxtMessage.Font, Drawing.FontStyle.Bold)

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnRead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If objfrm IsNot Nothing Then objfrm.Dispose()
        Me.Close()
    End Sub
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End
#End Region

#Region " Controls "
    Private Sub dgvMessage_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMessage.CellContentClick
        If e.ColumnIndex = objdgcolCheck.Index Then
            dgvMessage.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = Not CBool(dgvMessage.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
        End If
    End Sub

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        For Each drRow As DataGridViewRow In dgvMessage.Rows
            drRow.Cells(objdgcolCheck.Index).Value = chkSelectAll.Checked
        Next
    End Sub

    Private Sub dgvMessage_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles dgvMessage.RowPostPaint
        Try
            If CBool(dgvMessage.Rows(e.RowIndex).Cells(objdgcolIsRead.Index).Value) Then
                dgvMessage.Rows(e.RowIndex).DefaultCellStyle.Font = New Drawing.Font(dgvMessage.Font, Drawing.FontStyle.Regular)
            Else
                dgvMessage.Rows(e.RowIndex).DefaultCellStyle.Font = New Drawing.Font(dgvMessage.Font, Drawing.FontStyle.Bold)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "dgvStore_RowPostPaint", mstrModuleName)
        End Try
    End Sub

    Private Sub chkViewUnreadOnly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkViewUnreadOnly.CheckedChanged
        Call FillMsgList()
    End Sub

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub chkViewEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkViewEmployee.CheckedChanged
        Try
            Call FillMsgList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkViewEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkViewApplicant_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkViewApplicant.CheckedChanged
        Try
            Call FillMsgList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkViewApplicant_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END
    

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnMarkAsRead.GradientBackColor = GUI._ButttonBackColor 
			Me.btnMarkAsRead.GradientForeColor = GUI._ButttonFontColor

			Me.btnRead.GradientBackColor = GUI._ButttonBackColor 
			Me.btnRead.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnTrash.GradientBackColor = GUI._ButttonBackColor 
			Me.btnTrash.GradientForeColor = GUI._ButttonFontColor

			Me.btnWriteMessage.GradientBackColor = GUI._ButttonBackColor 
			Me.btnWriteMessage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSent.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSent.GradientForeColor = GUI._ButttonFontColor

			Me.btnPrintLetters.GradientBackColor = GUI._ButttonBackColor 
			Me.btnPrintLetters.GradientForeColor = GUI._ButttonFontColor

			Me.btnExportLetters.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExportLetters.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.chkViewUnreadOnly.Text = Language._Object.getCaption(Me.chkViewUnreadOnly.Name, Me.chkViewUnreadOnly.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnMarkAsRead.Text = Language._Object.getCaption(Me.btnMarkAsRead.Name, Me.btnMarkAsRead.Text)
			Me.btnRead.Text = Language._Object.getCaption(Me.btnRead.Name, Me.btnRead.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.btnTrash.Text = Language._Object.getCaption(Me.btnTrash.Name, Me.btnTrash.Text)
			Me.btnWriteMessage.Text = Language._Object.getCaption(Me.btnWriteMessage.Name, Me.btnWriteMessage.Text)
			Me.btnSent.Text = Language._Object.getCaption(Me.btnSent.Name, Me.btnSent.Text)
			Me.btnPrintLetters.Text = Language._Object.getCaption(Me.btnPrintLetters.Name, Me.btnPrintLetters.Text)
			Me.btnExportLetters.Text = Language._Object.getCaption(Me.btnExportLetters.Name, Me.btnExportLetters.Text)
			Me.chkViewApplicant.Text = Language._Object.getCaption(Me.chkViewApplicant.Name, Me.chkViewApplicant.Text)
			Me.chkViewEmployee.Text = Language._Object.getCaption(Me.chkViewEmployee.Name, Me.chkViewEmployee.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Subject:")
			Language.setMessage(mstrModuleName, 2, "Recipients:")
			Language.setMessage(mstrModuleName, 3, "Sender:")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class