Option Strict On

#Region " Imports "
Imports System
Imports System.Data
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmLetterType_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmLetterType_AddEdit"
    Private mblnCancel As Boolean = True
    Private mAction As enAction = enAction.ADD_ONE
    Private mFontStyle As New FontStyle
    Private fonts() As FontStyle = New FontStyle() {FontStyle.Bold, FontStyle.Italic, FontStyle.Underline}
    Dim rtbTemp As New RichTextBox
    Private mintLetterTypeUnkid As Integer = -1
    Private objLetterType As clsLetterType
    Private mintModRefId As Integer = -1
#End Region

#Region " Property "
    Public WriteOnly Property _ModuleRefId() As Integer
        Set(ByVal value As Integer)
            mintModRefId = value
        End Set
    End Property
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal intUnkId As Integer, ByVal pAction As enAction) As Boolean
        Try
            mintLetterTypeUnkid = intUnkId

            mAction = pAction

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function
#End Region

#Region " Private Function "
    Public Shared ReadOnly FontSizes As List(Of Single) = New List(Of Single)()
    Private Shared Sub InitializeFontSizes()
        FontSizes.AddRange(New Single() {8, 9, 10, 10.5F, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72})
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCommon As New clsCommon_Master

        'Sandeep [ 21 Aug 2010 ] -- Start
        Dim objMaster As New clsMasterData
        'Sandeep [ 21 Aug 2010 ] -- End 

        Try

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.LETTER_TYPE, True, "Letter")
            With cboGroupType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Letter")
                .SelectedValue = 0
            End With

            objcboFont.Items.Clear()
            For Each obj As FontFamily In FontFamily.Families()
                objcboFont.Items.Add(obj.Name)
            Next

            If FontSizes.Count <= 0 Then
                InitializeFontSizes()
            End If

            objcboFontSize.Items.Clear()
            For i As Integer = 0 To FontSizes.Count - 1
                objcboFontSize.Items.Add(FontSizes.Item(i).ToString)
            Next

            RemoveHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
            objcboFont.SelectedIndex = objcboFont.FindStringExact("Arial")
            objcboFontSize.SelectedIndex = objcboFontSize.FindStringExact("8")
            AddHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged


            'Sandeep [ 21 Aug 2010 ] -- Start
            dsList = objMaster.getEmailFieldTypes()
            With cboDataFields
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 1
            End With
            'Sandeep [ 21 Aug 2010 ] -- End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAlias.Text = objLetterType._Alias
            txtLetterType.Text = objLetterType._LetterName
            txtLetterType1.Text = objLetterType._LetterName1
            txtLetterType2.Text = objLetterType._LetterName2
            rtbLetterContent.Rtf = objLetterType._Lettercontent
            cboGroupType.SelectedValue = objLetterType._LettergroupmasterUnkId
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objLetterType._Fieldtypeid > 0 Then
                cboDataFields.SelectedValue = objLetterType._Fieldtypeid
            End If

            'S.SANDEEP [ 07 NOV 2011 ] -- END
            txtLetterType.Focus()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objLetterType._Alias = txtAlias.Text
            objLetterType._Lettercontent = rtbLetterContent.Rtf
            objLetterType._LetterName = txtLetterType.Text
            objLetterType._LetterName1 = txtLetterType1.Text
            objLetterType._LetterName2 = txtLetterType2.Text
            objLetterType._LettergroupmasterUnkId = CInt(cboGroupType.SelectedValue)
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objLetterType._Fieldtypeid = CInt(cboDataFields.SelectedValue)
            'S.SANDEEP [ 07 NOV 2011 ] -- END
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub ChangeFontSize(ByVal mFSize As Double)
        Try
            If mFSize <= 0 Then
                Exit Sub
            End If
            Dim intStart As Integer = rtbLetterContent.SelectionStart
            Dim intEnd As Integer = rtbLetterContent.SelectionLength
            Dim rtbTempStart As Integer = 0

            If intEnd <= 1 And rtbLetterContent.SelectionFont IsNot Nothing Then
                rtbLetterContent.SelectionFont = New Font(rtbLetterContent.SelectionFont.FontFamily, CInt(mFSize), rtbLetterContent.SelectionFont.Style)
                Return
            End If

            rtbTemp.Rtf = rtbLetterContent.SelectedRtf
            For i As Integer = 0 To intEnd
                rtbTemp.Select(rtbTempStart + i, 1)
                Dim mStyle As New FontStyle
                If rtbLetterContent.SelectionLength > 0 Then
                    mStyle = GetDefaultStyle(rtbTemp.SelectionFont.FontFamily.Name)
                Else
                    mStyle = GetDefaultStyle(objcboFont.Text)
                End If
                If IsNothing(rtbTemp.SelectionFont) Then
                    rtbTemp.SelectionFont = New Font(objcboFont.Text, CInt(mFSize), mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont.FontFamily, CInt(mFSize), mStyle)
                End If
            Next
            rtbTemp.Select(rtbTempStart, intEnd)
            rtbLetterContent.SelectedRtf = rtbTemp.SelectedRtf
            rtbLetterContent.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ChangeFontSize", mstrModuleName)
        End Try
    End Sub

    Private Sub RegainOldStyle()
        Try
            If objtlbbtnBold.Checked = True Then
                Call ApplyStyle(FontStyle.Bold, True)
            Else
                Call ApplyStyle(FontStyle.Bold, False)
            End If
            If objtlbbtnItalic.Checked = True Then
                Call ApplyStyle(FontStyle.Italic, True)
            Else
                Call ApplyStyle(FontStyle.Italic, False)
            End If
            If objtlbbtnUnderline.Checked = True Then
                Call ApplyStyle(FontStyle.Underline, True)
            Else
                Call ApplyStyle(FontStyle.Underline, False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RegainOldStyle", mstrModuleName)
        End Try
    End Sub

    Private Function GetDefaultStyle(ByVal mFontName As String) As FontStyle
        For Each fntName As FontFamily In FontFamily.Families
            If fntName.Name.Equals(mFontName) Then
                If fntName.IsStyleAvailable(FontStyle.Regular) Then
                    Return FontStyle.Regular
                ElseIf fntName.IsStyleAvailable(FontStyle.Bold) Then
                    Return FontStyle.Bold
                ElseIf fntName.IsStyleAvailable(FontStyle.Italic) Then
                    Return FontStyle.Italic
                ElseIf fntName.IsStyleAvailable(FontStyle.Underline) Then
                    Return FontStyle.Underline
                End If
            End If
        Next
    End Function

    Private Sub ChangeFont(ByVal mFontName As String)
        Try
            Dim intStart As Integer = rtbLetterContent.SelectionStart
            Dim intEnd As Integer = rtbLetterContent.SelectionLength
            Dim rtbTempStart As Integer = 0

            Dim mStyle As New FontStyle
            mStyle = GetDefaultStyle(mFontName)

            If intEnd <= 1 And rtbLetterContent.SelectionFont IsNot Nothing Then
                rtbLetterContent.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbLetterContent.SelectionFont.Size <> Nothing, rtbLetterContent.SelectionFont.Size, 8)), mStyle)
                Return
            End If

            rtbTemp.Rtf = rtbLetterContent.SelectedRtf
            For i As Integer = 0 To intEnd - 1
                rtbTemp.Select(rtbTempStart + i, 1)
                If IsNothing(rtbTemp.SelectionFont) Then
                    rtbTemp.SelectionFont = New Font(mFontName, CInt(objcboFontSize.Text), mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbTemp.SelectionFont.Size <> Nothing, rtbTemp.SelectionFont.Size, 8)), mStyle)
                End If
            Next
            rtbTemp.Select(rtbTempStart, intEnd)
            rtbLetterContent.SelectedRtf = rtbTemp.SelectedRtf
            rtbLetterContent.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "ChangeFont", mstrModuleName)
        End Try
    End Sub

    Private Sub ApplyStyle(ByVal mStyle As FontStyle, ByVal mAddStyle As Boolean)
        Try
            Dim intStart As Integer = rtbLetterContent.SelectionStart
            Dim intEnd As Integer = rtbLetterContent.SelectionLength
            Dim rtbTempStart As Integer = 0

            If intEnd <= 1 And rtbLetterContent.SelectionFont IsNot Nothing Then
                If mAddStyle Then
                    rtbLetterContent.SelectionFont = New Font(rtbLetterContent.SelectionFont, rtbLetterContent.SelectionFont.Style Or mStyle)
                Else
                    rtbLetterContent.SelectionFont = New Font(rtbLetterContent.SelectionFont, rtbLetterContent.SelectionFont.Style And (Not mStyle))
                End If
                Return
            End If

            rtbTemp.Rtf = rtbLetterContent.SelectedRtf

            For i As Integer = 0 To intEnd
                rtbTemp.Select(rtbTempStart + i, 1)
                If mAddStyle Then
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style Or mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style And (Not mStyle))
                End If
            Next

            rtbTemp.Select(rtbTempStart, intEnd)
            rtbLetterContent.SelectedRtf = rtbTemp.SelectedRtf
            rtbLetterContent.Select(intStart, intEnd)
            Return
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtAlias.BackColor = GUI.ColorOptional
            txtLetterType.BackColor = GUI.ColorComp
            txtLetterType1.BackColor = GUI.ColorOptional
            txtLetterType2.BackColor = GUI.ColorOptional

            cboGroupType.BackColor = GUI.ColorComp
            rtbLetterContent.BackColor = GUI.ColorComp
            txtLetterType.Focus()

        Catch ex As System.Exception
            Call DisplayError.Show(CStr(-1), ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form Load "

    Private Sub frmLetterType_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Keys.S Then
            Call btnSave_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmLetterType_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If rtbLetterContent.Focused = False Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub frmLetterType_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage
            Call SetColor()
            Call FillCombo()

            objLetterType = New clsLetterType

            If mAction = enAction.EDIT_ONE Then
                objLetterType._LettertypeUnkId = mintLetterTypeUnkid
            End If

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "frmGRMLetterType_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ToolStrip Events "

#Region " Font And Size "
    Private Sub cboFontSize_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles objcboFontSize.KeyPress
        Try
            If Not (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Then
                If (Asc(e.KeyChar) = Asc(GUI.DecimalSeparator) And InStr(Trim(objcboFontSize.Text), GUI.DecimalSeparator) = 0) Or Asc(e.KeyChar) = 8 Then Exit Sub
                e.KeyChar = CChar("")
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFontSize_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFontSize.TextChanged
        Try

            If objcboFontSize.Text = "" Or objcboFontSize.Text = "." Then
                Exit Sub
            Else
                Call ChangeFontSize(CDec(objcboFontSize.Text))
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFont_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFont.SelectedIndexChanged
        Try
            Call ChangeFont(objcboFont.Text)
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFont_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Alignments "

    Private Sub tlbbtnLeftAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftAlign.Click
        Try
            Dim strFormat As New StringFormat(StringFormatFlags.NoClip)
            objtlbbtnCenterAlign.Checked = False
            objtlbbtnRightAlign.Checked = False
            objtlbbtnLeftAlign.Checked = True

            If rtbLetterContent.SelectionAlignment <> HorizontalAlignment.Left Then
                rtbLetterContent.SelectionAlignment = HorizontalAlignment.Left
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnLeftAlign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tlbbtnCenterAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCenterAlign.Click
        Try
            objtlbbtnCenterAlign.Checked = True
            objtlbbtnRightAlign.Checked = False
            objtlbbtnLeftAlign.Checked = False

            If rtbLetterContent.SelectionAlignment <> HorizontalAlignment.Center Then
                rtbLetterContent.SelectionAlignment = HorizontalAlignment.Center
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnCenterAlign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tlbbtnRightAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightAlign.Click
        Try
            objtlbbtnCenterAlign.Checked = False
            objtlbbtnRightAlign.Checked = True
            objtlbbtnLeftAlign.Checked = False
            If rtbLetterContent.SelectionAlignment <> HorizontalAlignment.Right Then
                rtbLetterContent.SelectionAlignment = HorizontalAlignment.Right
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnRightAlign_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Styles "

    Private Sub objtlbbtnBold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBold.Click
        Try
            If objtlbbtnBold.Checked = True Then
                Call ApplyStyle(FontStyle.Bold, True)
            Else
                Call ApplyStyle(FontStyle.Bold, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBold_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnItalic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnItalic.Click
        Try
            If objtlbbtnItalic.Checked = True Then
                Call ApplyStyle(FontStyle.Italic, True)
            Else
                Call ApplyStyle(FontStyle.Italic, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnItalic_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnUnderline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUnderline.Click
        Try
            If objtlbbtnUnderline.Checked = True Then
                Call ApplyStyle(FontStyle.Underline, True)
            Else
                Call ApplyStyle(FontStyle.Underline, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUnderline_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Operations "

    Private Sub objtlbbtnUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUndo.Click
        Try
            rtbLetterContent.Undo()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUndo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnRedo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRedo.Click
        Try
            rtbLetterContent.Redo()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRedo_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objtlbbtnBulletSimple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBulletSimple.Click
        Try
            If objtlbbtnBulletSimple.Checked = True Then
                rtbLetterContent.SelectionBullet = True
            Else
                rtbLetterContent.SelectionBullet = False
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBulletSimple_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnLeftIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftIndent.Click
        Try
            If rtbLetterContent.SelectionIndent = 0 Then
                rtbLetterContent.SelectionIndent = 5
            Else
                rtbLetterContent.SelectionIndent += 10
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnLeftIndent_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnRightIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightIndent.Click
        Try
            If rtbLetterContent.SelectionRightIndent = 0 Then
                rtbLetterContent.SelectionRightIndent = 5
            Else
                rtbLetterContent.SelectionRightIndent += 10
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRightIndent_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCut.Click
        Try
            If rtbLetterContent.SelectedText.Length > 0 Then
                My.Computer.Clipboard.SetText(rtbLetterContent.SelectedText)
                rtbLetterContent.SelectedText = ""
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnCut_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCopy.Click
        Try
            If rtbLetterContent.SelectedText.Length > 0 Then
                My.Computer.Clipboard.SetText(rtbLetterContent.SelectedText)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnCopy_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnPaste.Click
        Try
            If My.Computer.Clipboard.ContainsText Then
                rtbLetterContent.AppendText(My.Computer.Clipboard.GetText())
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnPaste_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnColor.Click
        If pnlColor.Visible = True Then

            pnlColor.Hide()
            pnlColor.SendToBack()
        End If
        pnlColor.BringToFront()

        pnlColor.Show()
    End Sub

    Private Sub SetColor(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objbtn1.MouseClick, _
                                objbtn2.MouseClick, objbtn3.MouseClick, objbtn4.MouseClick, objbtn5.MouseClick, objbtn6.MouseClick, objbtn7.MouseClick, _
                                objbtn8.MouseClick, objbtn9.MouseClick, objbtn10.MouseClick, objbtn11.MouseClick, objbtn12.MouseClick, objbtn13.MouseClick, objbtn14.MouseClick, _
                                 objbtn15.MouseClick, objbtn16.MouseClick, objbtn17.MouseClick, objbtn18.MouseClick, objbtn19.MouseClick, _
                                objbtn20.MouseClick, objbtn21.MouseClick, objbtn22.MouseClick, objbtn23.MouseClick, objbtn24.MouseClick, objbtn25.MouseClick, _
                                objbtn26.MouseClick, objbtn27.MouseClick, objbtn28.MouseClick, objbtn29.MouseClick, objbtn30.MouseClick, objbtn31.MouseClick, _
                                objbtn32.MouseClick, objbtn33.MouseClick, objbtn34.MouseClick, objbtn35.MouseClick, objbtn36.MouseClick, objbtn37.MouseClick, _
                                objbtn38.MouseClick, objbtn39.MouseClick, objbtn40.MouseClick, objbtn41.MouseClick, objbtn42.MouseClick, objbtn43.MouseClick, _
                                 objbtn44.MouseClick, objbtn45.MouseClick, objbtn46.MouseClick, objbtn47.MouseClick, objbtn48.MouseClick

        Try
            rtbLetterContent.SelectionColor = CType(sender, Button).BackColor
            Call RegainOldStyle()
            pnlColor.Hide()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMoreColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoreColor.Click
        Try
            ColorDialog1.ShowDialog()
            rtbLetterContent.SelectionColor = ColorDialog1.Color
            pnlColor.Hide()
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMoreColor_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#End Region

#Region " Button's Events "
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnResult As Boolean = False
        Try
            If Trim(txtLetterType.Text) = "" Then
                Dim strQuestionMsg As String = Language.getMessage(mstrModuleName, 1, "Letter Name is compulsory information, it cannot be blank. Please enter a Letter Name to continue.")
                eZeeMsgBox.Show(strQuestionMsg, enMsgBoxStyle.Information)
                txtLetterType.Focus()
                Exit Sub
            End If

            If CInt(cboGroupType.SelectedValue) = 0 Then
                Dim strTypeMsg As String = Language.getMessage(mstrModuleName, 2, "Letter Group is compulsory information, it cannot be blank. Please select Letter Group to continue.")
                eZeeMsgBox.Show(strTypeMsg, enMsgBoxStyle.Information)
                cboGroupType.Focus()
                Exit Sub
            End If

            If Trim(rtbLetterContent.Text) = "" Then
                Dim strContentMsg As String = Language.getMessage(mstrModuleName, 3, "Letter Content is compulsory information, it cannot be blank. Please enter Letter Content to continue.")
                eZeeMsgBox.Show(strContentMsg, enMsgBoxStyle.Information) '?3
                rtbLetterContent.Focus()
                Exit Sub
            End If

            Call SetValue()

            If mAction = enAction.EDIT_ONE Then
                blnResult = objLetterType.Update
            Else
                blnResult = objLetterType.Insert
            End If

            If blnResult Then
                mblnCancel = False

                If mAction = enAction.ADD_CONTINUE Then
                    objLetterType = Nothing
                    objLetterType = New clsLetterType
                    Call GetValue()
                Else
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Clicks", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLetterGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLetterGroup.Click
        Dim intRefId As Integer = -1
        Dim frm As New frmCommonMaster
        Dim dsList As DataSet = Nothing
        Dim objCommon As clsCommon_Master = Nothing
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.LETTER_TYPE, enAction.ADD_ONE)
            If intRefId <> -1 Then
                dsList = New DataSet
                objCommon = New clsCommon_Master
                dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.LETTER_TYPE, True, "LetterType")
                With cboGroupType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("LetterType")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnLetterGroup_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objCommon = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLetterType.SetMessages()
            objfrm._Other_ModuleNames = "clsLetterType"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End
#End Region

#Region " Controls "
    Private Sub lnkInsertField_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkInsertField.LinkClicked
        Dim objFrm As New frmLetterFields
        Dim mstrColumn_Name As String = ""
        Dim mstrColumnTag As String = ""



        objFrm.displayDialog(CInt(cboDataFields.SelectedValue), mstrColumnTag, mstrColumn_Name)

        'Select Case mintModRefId
        '    Case enModuleRefId.Employee_Module
        '        objFrm.displayDialog(enModuleRefId.Employee_Module, mstrColumnTag, mstrColumn_Name)
        '    Case enModuleRefId.Applicant_Module
        '        objFrm.displayDialog(enModuleRefId.Applicant_Module, mstrColumnTag, mstrColumn_Name)
        '    Case enModuleRefId.Leave_Module
        '    Case enModuleRefId.Payroll_Module
        'End Select
        If mstrColumn_Name.Length > 0 Then
            Dim strColName() As String = Nothing
            strColName = mstrColumn_Name.Split(CChar("|"))
            For Each str1 As String In strColName
                If str1 Is Nothing Then
                    Exit For
                End If

                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If rtbLetterContent.Text.Contains("#" & str1 & "#") Then
                '    Continue For
                'Else
                    rtbLetterContent.SelectedText = ("#" & str1 & "#" & vbCrLf)
                'End If
                'S.SANDEEP [ 07 NOV 2011 ] -- END
            Next
        End If
    End Sub

    Private Sub txtLetterType_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLetterType.LostFocus
        Try
            If txtLetterType1.Text = "" Then
                txtLetterType1.Text = txtLetterType.Text
            End If

            If txtLetterType2.Text = "" Then
                txtLetterType2.Text = txtLetterType.Text
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtLetterType_LostFocus", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbLetterType.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLetterType.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblAlias.Text = Language._Object.getCaption(Me.lblAlias.Name, Me.lblAlias.Text)
			Me.lblLetterType.Text = Language._Object.getCaption(Me.lblLetterType.Name, Me.lblLetterType.Text)
			Me.lblLetterGroup.Text = Language._Object.getCaption(Me.lblLetterGroup.Name, Me.lblLetterGroup.Text)
			Me.lblLetterType2.Text = Language._Object.getCaption(Me.lblLetterType2.Name, Me.lblLetterType2.Text)
			Me.lblLetterType1.Text = Language._Object.getCaption(Me.lblLetterType1.Name, Me.lblLetterType1.Text)
			Me.lnkInsertField.Text = Language._Object.getCaption(Me.lnkInsertField.Name, Me.lnkInsertField.Text)
			Me.gbLetterType.Text = Language._Object.getCaption(Me.gbLetterType.Name, Me.gbLetterType.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnMoreColor.Text = Language._Object.getCaption(Me.btnMoreColor.Name, Me.btnMoreColor.Text)
			Me.lblSelectDataFields.Text = Language._Object.getCaption(Me.lblSelectDataFields.Name, Me.lblSelectDataFields.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Letter Name is compulsory information, it cannot be blank. Please enter a Letter Name to continue.")
			Language.setMessage(mstrModuleName, 2, "Letter Group is compulsory information, it cannot be blank. Please select Letter Group to continue.")
			Language.setMessage(mstrModuleName, 3, "Letter Content is compulsory information, it cannot be blank. Please enter Letter Content to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class