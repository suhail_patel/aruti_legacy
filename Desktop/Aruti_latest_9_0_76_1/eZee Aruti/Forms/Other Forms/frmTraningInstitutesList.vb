﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 2

Public Class frmTraningInstitutesList

#Region "Private Variable"

    Private objInstitutemaster As clsinstitute_master
    Private mstrModuleName As String = ""
    Private mblnIsHospital As Boolean

#End Region

#Region "Form's Property"

    Public WriteOnly Property _IsHospital() As Boolean
        Set(ByVal value As Boolean)
            mblnIsHospital = value
        End Set
    End Property

#End Region

#Region "Form's Event"

    Private Sub frmTraningInstitutesList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objInstitutemaster = New clsinstitute_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            'If mblnIsHospital = True Then
            '    Me.Text = "Service Provider List"
            '    eZeeHeader.Title = "Service Provider"
            'End If


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            Select Case mblnIsHospital
                Case True
                    Me.Name = "frmServiceProviderList"
                    Me.Text = Language.getMessage(mstrModuleName, 4, "Service Provider List")
                    eZeeHeader.Title = Language.getMessage(mstrModuleName, 5, "Service Provider")
                    lblInstitute.Text = Language.getMessage(mstrModuleName, 5, "Service Provider")
                Case False
                    Me.Name = "frmTrainingProviderList"
                    Me.Text = Language.getMessage(mstrModuleName, 6, "Training Provider List")
                    eZeeHeader.Title = Language.getMessage(mstrModuleName, 7, "Training Provider")
                    lblInstitute.Text = Language.getMessage(mstrModuleName, 7, "Training Provider")

                    lblInstitute.Location = New Point(10, 34)
                    cboInstitute.Location = New Point(125, 31)
                    objbtnSearchInstitute.Location = New Point(400, 31)
            End Select

            'Pinkal (20-Jan-2012) -- End 


            mstrModuleName = Me.Name
            Language.setLanguage(Me.Name)

            Call OtherSettings()

            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            FillCombo()
            'Pinkal (20-Jan-2012) -- End


            fillList()
            If lvInstituteinfo.Items.Count > 0 Then lvInstituteinfo.Items(0).Selected = True
            lvInstituteinfo.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTraningInstitutesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTraningInstitutesList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvInstituteinfo.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTraningInstitutesList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTraningInstitutesList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objInstitutemaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsinstitute_master.SetMessages()
            objfrm._Other_ModuleNames = "clsinstitute_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmTraningInstitutes_AddEdit


            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment
            'If mblnIsHospital = True Then
            '    'ObjFrm.Text = "Add/Edit Medical Institute"
            '    'ObjFrm.gbTaraningInstitutes.Text = "Medical Institute"
            '    'ObjFrm.eZeeHeader.Title = "Medical Institute"
            '    ObjFrm.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Service Provider")
            '    ObjFrm.gbTaraningInstitutes.Text = Language.getMessage(mstrModuleName, 2, "Service Provider")
            '    ObjFrm.eZeeHeader.Title = Language.getMessage(mstrModuleName, 2, "Service Provider")
            'Else
            '    ObjFrm.Text = Language.getMessage(mstrModuleName, 3, "Add/Edit Training Provider")
            '    ObjFrm.gbTaraningInstitutes.Text = Language.getMessage(mstrModuleName, 4, "Training Provider")
            '    ObjFrm.eZeeHeader.Title = Language.getMessage(mstrModuleName, 4, "Training Provider")
            'End If
            ''Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment
            'S.SANDEEP [ 20 AUG 2011 ] -- END 

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                ObjFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                ObjFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(ObjFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE, mblnIsHospital) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvInstituteinfo.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Institute from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvInstituteinfo.Select()
                Exit Sub
            End If
            Dim objfrmTraningInstitutes_AddEdit As New frmTraningInstitutes_AddEdit
            Try

                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                If User._Object._Isrighttoleft = True Then
                    objfrmTraningInstitutes_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmTraningInstitutes_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmTraningInstitutes_AddEdit)
                End If
                'S.SANDEEP [ 20 AUG 2011 ] -- END
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvInstituteinfo.SelectedItems(0).Index
                If frmTraningInstitutes_AddEdit.displayDialog(CInt(lvInstituteinfo.SelectedItems(0).Tag), enAction.EDIT_ONE, mblnIsHospital) Then
                    Call fillList()
                End If
                frmTraningInstitutes_AddEdit = Nothing

                lvInstituteinfo.Items(intSelectedIndex).Selected = True
                lvInstituteinfo.EnsureVisible(intSelectedIndex)
                lvInstituteinfo.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If frmTraningInstitutes_AddEdit IsNot Nothing Then frmTraningInstitutes_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvInstituteinfo.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Institute from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvInstituteinfo.Select()
            Exit Sub
        End If
        If objInstitutemaster.isUsed(CInt(lvInstituteinfo.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Institute. Reason: This Institute is in use."), enMsgBoxStyle.Information) '?2
            lvInstituteinfo.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvInstituteinfo.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Institute?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objInstitutemaster.Delete(CInt(lvInstituteinfo.SelectedItems(0).Tag))
                lvInstituteinfo.SelectedItems(0).Remove()

                If lvInstituteinfo.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvInstituteinfo.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvInstituteinfo.Items.Count - 1
                    lvInstituteinfo.Items(intSelectedIndex).Selected = True
                    lvInstituteinfo.EnsureVisible(intSelectedIndex)
                ElseIf lvInstituteinfo.Items.Count <> 0 Then
                    lvInstituteinfo.Items(intSelectedIndex).Selected = True
                    lvInstituteinfo.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvInstituteinfo.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchInstitute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchInstitute.Click
        Dim objfrm As New frmCommonSearch
        Try
            With objfrm
                .ValueMember = cboInstitute.ValueMember
                .DisplayMember = cboInstitute.DisplayMember
                .DataSource = CType(cboInstitute.DataSource, DataTable)
                '.CodeMember = "institute_code"
            End With

            If objfrm.DisplayDialog Then
                cboInstitute.SelectedValue = objfrm.SelectedValue
                cboInstitute.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchInstitute_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboInstitute.SelectedIndex = 0
            If cboStatus.Visible Then cboStatus.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuMakeActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMakeActive.Click
        Try
            If lvInstituteinfo.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Institute from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvInstituteinfo.Select()
                Exit Sub
            End If

            objInstitutemaster._Instituteunkid = CInt(lvInstituteinfo.SelectedItems(0).Tag)
            objInstitutemaster._Isactive = True
            Dim blnFlag As Boolean = objInstitutemaster.Update()

            If blnFlag = False And objInstitutemaster._Message <> "" Then
                eZeeMsgBox.Show(objInstitutemaster._Message, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Selected Record Actived Successfully."), enMsgBoxStyle.Information) '?1
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuMakeActive_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuUserMapping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUserMapping.Click
        Try
            Dim frm As New frmProvider_UserMapping
            frm.displayDialog(-1, enAction.ADD_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUserMapping_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsInstituteList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try


            If mblnIsHospital Then
                'Pinkal (02-Jul-2012) -- Start
                'Enhancement : TRA Changes
                If User._Object.Privilege._AllowToViewServiceProviderList = False Then Exit Sub
                'Pinkal (02-Jul-2012) -- End

                dsInstituteList = objInstitutemaster.GetList("Institute", mblnIsHospital, , CInt(cboStatus.SelectedIndex))
            Else

                'Anjan (25 Oct 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                If User._Object.Privilege._AllowToViewTrainingInstituteList = False Then Exit Sub
                'Anjan (25 Oct 2012)-End 

                dsInstituteList = objInstitutemaster.GetList("Institute", mblnIsHospital, , 1)
            End If

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If CInt(cboInstitute.SelectedValue) > 0 Then
                StrSearching &= "AND instituteunkid = " & CInt(cboInstitute.SelectedValue)
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsInstituteList.Tables("Institute"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsInstituteList.Tables("Institute")
            End If


            Dim lvItem As ListViewItem

            lvInstituteinfo.Items.Clear()
            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("institute_code").ToString
                lvItem.Tag = drRow("instituteunkid")
                lvItem.SubItems.Add(drRow("institute_name").ToString)
                lvItem.SubItems.Add(drRow("telephoneno").ToString)
                lvItem.SubItems.Add(drRow("contact_person").ToString)
                lvItem.SubItems.Add(drRow("institute_email").ToString)


                'Pinkal (20-Jan-2012) -- Start
                'Enhancement : TRA Changes
                lvItem.SubItems.Add(drRow("isactive").ToString)
                'Pinkal (20-Jan-2012) -- End

                lvInstituteinfo.Items.Add(lvItem)
            Next

            If lvInstituteinfo.Items.Count > 16 Then
                colhInstituteEmail.Width = 165 - 18
            Else
                colhInstituteEmail.Width = 165
            End If

            'Pinkal (20-Jan-2012) -- End


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsInstituteList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            If mblnIsHospital = True Then
                btnNew.Enabled = User._Object.Privilege._AddMedicalInstitutes
                btnEdit.Enabled = User._Object.Privilege._EditMedicalInstitutes
                btnDelete.Enabled = User._Object.Privilege._DeleteMedicalInstitutes
                'S.SANDEEP [ 16 MAY 2012 ] -- START
                'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                mnuMakeActive.Enabled = User._Object.Privilege._AllowToMakeServiceProviderActive
                mnuUserMapping.Enabled = User._Object.Privilege._AllowToMapUserWithServiceProvider
                'S.SANDEEP [ 16 MAY 2012 ] -- END
            Else
                btnNew.Enabled = User._Object.Privilege._AddTrainingInstitute
                btnEdit.Enabled = User._Object.Privilege._EditTrainingInstitute
                btnDelete.Enabled = User._Object.Privilege._DeleteTrainingInstitute
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub FillCombo()
        Try
            Dim objInstitute As New clsinstitute_master
            Dim dsList As DataSet = Nothing

            If mblnIsHospital Then
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 8, "Select"))
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 9, "Active"))
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 10, "InActive"))
                cboStatus.SelectedIndex = 0
            Else
                lblStatus.Visible = False
                cboStatus.Visible = False
                btnOperation.Visible = False
            End If

            If mblnIsHospital = False Then
                dsList = objInstitute.getListForCombo(mblnIsHospital, "List", True, 1)
                With cboInstitute
                    .DisplayMember = "name"
                    .ValueMember = "instituteunkid"
                    .DataSource = dsList.Tables(0)
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub



    'Pinkal (20-Jan-2012) -- End


#End Region


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes 

#Region "ComboBox Event"

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Dim dsList As DataSet
        Try
            If CInt(cboStatus.SelectedIndex) < 0 Then Exit Sub

            dsList = objInstitutemaster.getListForCombo(mblnIsHospital, "List", True, CInt(cboStatus.SelectedIndex))
            With cboInstitute
                .DisplayMember = "name"
                .ValueMember = "instituteunkid"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvInstituteinfo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvInstituteinfo.SelectedIndexChanged
        Try
            If mblnIsHospital Then

                If lvInstituteinfo.SelectedItems.Count > 0 Then

                    If CBool(lvInstituteinfo.SelectedItems(0).SubItems(objColhIsActive.Index).Text) = False Then
                        btnEdit.Enabled = False
                        btnDelete.Enabled = False
                        mnuMakeActive.Enabled = True
                    Else
                        btnEdit.Enabled = True
                        btnDelete.Enabled = True
                        mnuMakeActive.Enabled = False
                    End If

                End If

                'S.SANDEEP [ 16 MAY 2012 ] -- START
                'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                Call SetVisibility()
                'S.SANDEEP [ 16 MAY 2012 ] -- END

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvInstituteinfo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (20-Jan-2012) -- End




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhInstitutecode.Text = Language._Object.getCaption(CStr(Me.colhInstitutecode.Tag), Me.colhInstitutecode.Text)
            Me.colhInstituteName.Text = Language._Object.getCaption(CStr(Me.colhInstituteName.Tag), Me.colhInstituteName.Text)
            Me.colhInstituteTelno.Text = Language._Object.getCaption(CStr(Me.colhInstituteTelno.Tag), Me.colhInstituteTelno.Text)
            Me.colhInstituteContactPerson.Text = Language._Object.getCaption(CStr(Me.colhInstituteContactPerson.Tag), Me.colhInstituteContactPerson.Text)
            Me.colhInstituteEmail.Text = Language._Object.getCaption(CStr(Me.colhInstituteEmail.Tag), Me.colhInstituteEmail.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuMakeActive.Text = Language._Object.getCaption(Me.mnuMakeActive.Name, Me.mnuMakeActive.Text)
            Me.mnuUserMapping.Text = Language._Object.getCaption(Me.mnuUserMapping.Name, Me.mnuUserMapping.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Institute from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Institute. Reason: This Institute is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Institute?")
            Language.setMessage(mstrModuleName, 4, "Service Provider List")
            Language.setMessage(mstrModuleName, 5, "Service Provider")
            Language.setMessage(mstrModuleName, 6, "Training Provider List")
            Language.setMessage(mstrModuleName, 7, "Training Provider")
            Language.setMessage(mstrModuleName, 8, "Select")
            Language.setMessage(mstrModuleName, 9, "Active")
            Language.setMessage(mstrModuleName, 10, "InActive")
            Language.setMessage(mstrModuleName, 11, "Selected Record Actived Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class