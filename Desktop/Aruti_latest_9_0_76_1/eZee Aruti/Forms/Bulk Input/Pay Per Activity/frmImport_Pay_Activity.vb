﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImport_Pay_Activity

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImport_Pay_Activity"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Private mDicWeekName As Dictionary(Of String, Integer)

#End Region

#Region " Form's Events "

    Private Sub frmImport_Pay_Activity_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            chkUseEmployeeCC.Checked = True
            txtFilePath.BackColor = GUI.ColorComp

            Dim days() As String = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.DayNames
            mDicWeekName = New Dictionary(Of String, Integer)
            For i As Integer = 0 To 6
                mDicWeekName.Add(days(i).ToString, i)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImport_Pay_Activity_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizPPA_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizPPA.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizPPA.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizPPA_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizPPA_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizPPA.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizPPA.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizPPA.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If chkUseEmployeeCC.Checked = True Then
                                    If CType(ctrl, ComboBox).Name.ToUpper = cboCostCenter.Name.ToUpper Then Continue For
                                End If
                                'S.SANDEEP [21 JUN 2016] -- START
                                'ISSUE : PERIOD WILL BE FOUND BASED ON ACTIVITY DATE, ISSUE WAS DIFFERENT PERIOD WAS IN EXCEL AND DATE GIVEN IS OF DIFFERENT PERIOD.
                                If CType(ctrl, ComboBox).Name.ToUpper = cboPeriod.Name.ToUpper Then Continue For
                                'S.SANDEEP [21 JUN 2016] -- END
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case eZeeWizPPA.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizPPA_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboActivity.Items.Add(dtColumns.ColumnName)
                cboActivityDate.Items.Add(dtColumns.ColumnName)
                cboActivityValue.Items.Add(dtColumns.ColumnName)
                cboCostCenter.Items.Add(dtColumns.ColumnName)
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboEmployeeName.Items.Add(dtColumns.ColumnName)
                cboPeriod.Items.Add(dtColumns.ColumnName)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("ename", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("period", System.Type.GetType("System.String")).DefaultValue = ""
            'Pinkal (16-Jun-2015) -- Start
            'Enhancement - WORKING ON PPA Enhancement Given By Anjan sir.
            mdt_ImportData_Others.Columns.Add("activitycode", System.Type.GetType("System.String")).DefaultValue = ""
            'Pinkal (16-Jun-2015) -- End

            mdt_ImportData_Others.Columns.Add("activity", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("avalue", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("adate", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("ccenter", System.Type.GetType("System.String")).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String")).DefaultValue = ""

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                drNewRow.Item("ename") = dtRow.Item(cboEmployeeName.Text).ToString.Trim
                'S.SANDEEP [21 JUN 2016] -- START
                'ISSUE : PERIOD WILL BE FOUND BASED ON ACTIVITY DATE, ISSUE WAS DIFFERENT PERIOD WAS IN EXCEL AND DATE GIVEN IS OF DIFFERENT PERIOD.
                'drNewRow.Item("period") = dtRow.Item(cboPeriod.Text).ToString.Trim
                drNewRow.Item("period") = ""
                'S.SANDEEP [21 JUN 2016] -- END
                'Pinkal (16-Jun-2015) -- Start
                'Enhancement - WORKING ON PPA Enhancement Given By Anjan sir.
                drNewRow.Item("activitycode") = dtRow.Item(cboActivity.Text).ToString.Trim
                'Pinkal (16-Jun-2015) -- End

                drNewRow.Item("avalue") = dtRow.Item(cboActivityValue.Text).ToString.Trim
                drNewRow.Item("adate") = dtRow.Item(cboActivityDate.Text).ToString.Trim
                If chkUseEmployeeCC.Checked = False Then
                    drNewRow.Item("ccenter") = dtRow.Item(cboCostCenter.Text).ToString.Trim
                End If

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ename"

                'Pinkal (16-Jun-2015) -- Start
                'Enhancement - WORKING ON PPA Enhancement Given By Anjan sir.
                colhActivity.DataPropertyName = "activitycode"
                'Pinkal (16-Jun-2015) -- End

                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizPPA.BackEnabled = False
            eZeeWizPPA.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEmployeeName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Name is mandatory information. Please select Employee Name to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                'S.SANDEEP [21 JUN 2016] -- START
                'ISSUE : PERIOD WILL BE FOUND BASED ON ACTIVITY DATE, ISSUE WAS DIFFERENT PERIOD WAS IN EXCEL AND DATE GIVEN IS OF DIFFERENT PERIOD.
                'If .Item(cboPeriod.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'S.SANDEEP [21 JUN 2016] -- END

                'Pinkal (16-Jun-2015) -- Start
                'Enhancement - WORKING ON PPA Enhancement Given By Anjan sir.

                'If .Item(cboActivity.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Activity Name is mandatory information. Please select Activity Name to continue."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                If .Item(cboActivity.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Activity Code is mandatory information. Please select Activity Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                'Pinkal (16-Jun-2015) -- End


                If .Item(cboActivityDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Activity Date is mandatory information. Please select Activity Date to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboActivityValue.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Activity Value is mandatory information. Please select Activity Value to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If chkUseEmployeeCC.Checked = False Then
                    If .Item(cboCostCenter.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objEMaster As New clsEmployee_Master
            Dim objAMaster As New clsActivity_Master
            Dim objPMaster As New clscommom_period_Tran
            Dim objCMaster As New clscostcenter_master
            Dim objMeasure As New clsUnitMeasure_Master
            Dim objActTran As clsPayActivity_Tran

            Dim iEmployeeId As Integer = 0
            Dim iActivityId As Integer = 0
            Dim iPeriodId As Integer = 0
            Dim iCCenterId As Integer = 0
            Dim iDecAValue As Decimal = 0
            Dim iADate As Date = Nothing

            Dim mDecNF_OT As Decimal = 0
            Dim mDecPH_OT As Decimal = 0
            Dim mDblSftHrs As Double = 0
            Dim mblnIsFormualBased As Boolean = False
            Dim mDecAcitityRate As Decimal = 0
            Dim mIntTrnHeadId As Integer = 0
            Dim mblnIsWeekEnd As Boolean = False
            Dim mintShiftUnkid As Integer = 0
            Dim mblnIsHoliday As Boolean = False
            Dim mDecNormalAmt, mDecNormalOTAmt As Decimal
            Dim dblOT_Hrs As Double = 0
            Dim mintPayActivityTranId As Integer = 0


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            Dim mIntOT_TrnHeadId As Integer = 0
            Dim mIntPH_TrnHeadId As Integer = 0
            'Pinkal (4-Sep-2014) -- End


            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                iEmployeeId = 0 : iActivityId = 0
                iPeriodId = 0 : iCCenterId = 0
                mDecNF_OT = 0 : mDecPH_OT = 0 : mDblSftHrs = 0 : mblnIsFormualBased = False
                mDecAcitityRate = 0 : mIntTrnHeadId = 0 : mblnIsWeekEnd = False : mintShiftUnkid = 0
                mblnIsHoliday = False : mDecNormalAmt = 0 : mDecNormalOTAmt = 0 : dblOT_Hrs = 0
                mintPayActivityTranId = 0


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                mIntOT_TrnHeadId = 0
                mIntPH_TrnHeadId = 0
                'Pinkal (4-Sep-2014) -- End


                objActTran = New clsPayActivity_Tran

                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    iEmployeeId = objEMaster.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If iEmployeeId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                    If chkUseEmployeeCC.Checked = True Then

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEMaster._Employeeunkid = iEmployeeId
                        objEMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iEmployeeId
                        'S.SANDEEP [04 JUN 2015] -- END

                        iCCenterId = objEMaster._Costcenterunkid

                        'Pinkal (15-Oct-2013) -- Start
                        'Enhancement : TRA Changes
                        'mintShiftUnkid = objEMaster._Shiftunkid
                        'Pinkal (15-Oct-2013) -- End

                    End If
                End If

                'S.SANDEEP [21 JUN 2016] -- START
                'ISSUE : PERIOD WILL BE FOUND BASED ON ACTIVITY DATE, ISSUE WAS DIFFERENT PERIOD WAS IN EXCEL AND DATE GIVEN IS OF DIFFERENT PERIOD.
                ''------------------------------ CHECKING IF PERIOD PRESENT.
                'If dtRow.Item("period").ToString.Trim.Length > 0 Then
                '    iPeriodId = objPMaster.GetPeriodByName(dtRow.Item("period").ToString.Trim, enModuleReference.Payroll)
                '    If iPeriodId <= 0 Then
                '        dtRow.Item("image") = imgError
                '        dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Period Not Found.")
                '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                '        dtRow.Item("objStatus") = 2
                '        objError.Text = CStr(Val(objError.Text) + 1)
                '        Continue For
                '    End If
                'End If
                'S.SANDEEP [21 JUN 2016] -- END



                '------------------------------ CHECKING IF ACTIVITY PRESENT.

                'Pinkal (16-Jun-2015) -- Start
                'Enhancement - WORKING ON PPA Enhancement Given By Anjan sir.

                If dtRow.Item("activitycode").ToString.Trim.Length > 0 Then
                    iActivityId = objAMaster.Get_Activity_Id("", dtRow.Item("activitycode").ToString.Trim)
                    If iActivityId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 27, "Activity Code Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                'Pinkal (16-Jun-2015) -- End

                '------------------------------ CHECKING IF ACTIVITY VALUE IS CORRECT.
                If dtRow.Item("avalue").ToString.Trim.Length > 0 Then
                    Decimal.TryParse(dtRow.Item("avalue").ToString.Trim, iDecAValue)
                    If iDecAValue <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Invalid Activity Value.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING IF ACTIVITY DATE IS CORRECT.
                If dtRow.Item("adate").ToString.Trim.Length > 0 Then
                    If IsDate(dtRow.Item("adate").ToString.Trim) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Invalid Activity Date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        iADate = CDate(dtRow.Item("adate").ToString.Trim)
                    End If
                End If


                'S.SANDEEP [21 JUN 2016] -- START
                'ISSUE : PERIOD WILL BE FOUND BASED ON ACTIVITY DATE, ISSUE WAS DIFFERENT PERIOD WAS IN EXCEL AND DATE GIVEN IS OF DIFFERENT PERIOD.
                Dim objMaster As New clsMasterData
                iPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, iADate, FinancialYear._Object._YearUnkid)
                If iPeriodId <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Period Not Found Based On Activity Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                'S.SANDEEP [21 JUN 2016] -- END

                '------------------------------ CHECKING IF COST CENTER PRESENT.
                If chkUseEmployeeCC.Checked = False Then
                    If dtRow.Item("ccenter").ToString.Trim.Length > 0 Then
                        iCCenterId = objCMaster.GetCostCenterUnkId(dtRow.Item("ccenter").ToString.Trim)
                        If iCCenterId <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Cost Center Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If


                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEMaster._Employeeunkid = iEmployeeId
                objEMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iEmployeeId
                'S.SANDEEP [04 JUN 2015] -- END


                If objEMaster._Appointeddate = Nothing OrElse objEMaster._Appointeddate.Date > iADate.Date Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Employee's Appointed Date is greater than activity date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                If objEMaster._Termination_From_Date <> Nothing AndAlso objEMaster._Termination_From_Date.Date < iADate.Date Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 22, "Employee's Leaving Date is less than activity date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                If objEMaster._Termination_To_Date <> Nothing AndAlso objEMaster._Termination_To_Date.Date < iADate.Date Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 23, "Employee's Retirement Date is less than activity date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                If objEMaster._Empl_Enddate <> Nothing AndAlso objEMaster._Empl_Enddate.Date < iADate.Date Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 24, "Employee's EOC Date is less than activity date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                'Pinkal (23-Sep-2014) -- End



                objAMaster._Activityunkid = iActivityId
                objMeasure._Measureunkid = objAMaster._Measureunkid


                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA
                Dim mblnIsOTActivity As Boolean = objAMaster._Is_Ot_Activity
                'Pinkal (23-Sep-2014) -- End


                Dim dLs As New DataSet
                dLs = objActTran.Get_Activity_Rate(objAMaster._Activityunkid, iADate)
                If dLs.Tables(0).Rows.Count <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 18, "No Rate defined for the selected activity for the given date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If


                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes
                Dim objEmpShift As New clsEmployee_Shift_Tran
                mintShiftUnkid = objEmpShift.GetEmployee_Current_ShiftId(iADate, iEmployeeId)
                'Pinkal (15-Oct-2013) -- End



                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                'mDecNF_OT = objAMaster._Normal_Otfactor
                'mDecPH_OT = objAMaster._Ph_Otfactor
                'Pinkal (4-Sep-2014) -- End

                mblnIsFormualBased = objMeasure._Isformula_Based


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA

                'If dLs.Tables(0).Rows.Count > 0 Then
                '    mDecAcitityRate = CDec(dLs.Tables(0).Rows(0)("activity_rate"))
                '    mIntTrnHeadId = CInt(dLs.Tables(0).Rows(0)("tranheadunkid"))
                'End If

                'Pinkal (4-Sep-2014) -- End

                mDblSftHrs = objActTran.Get_Sft_WorkHrs(mDicWeekName, mintShiftUnkid, iADate, mblnIsWeekEnd)

                Dim objEHoliday As New clsemployee_holiday
                If CType(objEHoliday.GetEmployeeHoliday(iEmployeeId, iADate), DataTable).Rows.Count > 0 Then
                    mblnIsHoliday = True
                Else
                    mblnIsHoliday = False
                End If
                objEHoliday = Nothing


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA

                Dim mdecFinalAmt As Decimal = 0 : dblOT_Hrs = 0 : mDecNormalAmt = 0 : mDecNormalOTAmt = 0
                If dLs.Tables(0).Rows.Count > 0 Then
                    Dim dRow() As DataRow = Nothing
                    dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.RATE)
                    If dRow.Length > 0 Then
                        mDecAcitityRate = CDec(dRow(0)("activity_rate"))
                        mIntTrnHeadId = CInt(dRow(0)("tranheadunkid"))
                    End If
                    If mblnIsWeekEnd = False AndAlso mblnIsHoliday = False Then
                        dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.OT)
                        If dRow.Length > 0 Then
                            mDecNF_OT = CDec(dRow(0)("activity_rate"))
                            mIntOT_TrnHeadId = CInt(dRow(0)("tranheadunkid"))
                        End If
                    ElseIf mblnIsWeekEnd OrElse mblnIsHoliday Then
                        dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.PH)
                        If dRow.Length > 0 Then
                            mDecPH_OT = CDec(dRow(0)("activity_rate"))
                            mIntPH_TrnHeadId = CInt(dRow(0)("tranheadunkid"))
                        End If
                    End If
                End If


                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA

                If mblnIsFormualBased = False Then

                    If mDecAcitityRate > 0 Then
                        If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                            If mblnIsOTActivity Then
                                dblOT_Hrs = iDecAValue
                                mdecFinalAmt = ((mDecAcitityRate * mDecPH_OT) * iDecAValue)
                                mDecNormalOTAmt = mdecFinalAmt
                            Else
                                mdecFinalAmt = mDecAcitityRate * iDecAValue
                                mDecNormalAmt = 0
                            End If
                        Else
                            If mDblSftHrs > 0 Then
                                If mblnIsOTActivity Then
                                    dblOT_Hrs = iDecAValue - mDblSftHrs
                                    If dblOT_Hrs <= 0 Then
                                        mdecFinalAmt = mDecAcitityRate * iDecAValue
                                        mDecNormalAmt = mdecFinalAmt
                                    Else
                                        mDecNormalAmt = (CDec(mDblSftHrs * mDecAcitityRate))
                                        mDecNormalOTAmt = CDec(dblOT_Hrs * (mDecAcitityRate * mDecNF_OT))
                                        mdecFinalAmt = mDecNormalAmt + mDecNormalOTAmt
                                    End If
                                Else
                                    mdecFinalAmt = mDecAcitityRate * iDecAValue
                                    mDecNormalAmt = 0
                                End If
                            Else
                                mdecFinalAmt = mDecAcitityRate * iDecAValue
                            End If

                        End If

                    End If

                Else
                    If mblnIsOTActivity Then
                        If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                            dblOT_Hrs = iDecAValue
                        Else
                            If mDblSftHrs > 0 Then
                                dblOT_Hrs = iDecAValue - mDblSftHrs
                                If dblOT_Hrs <= 0 Then
                                    dblOT_Hrs = 0
                                    mIntOT_TrnHeadId = 0
                                    mIntPH_TrnHeadId = 0
                                End If

                            End If

                        End If
                    Else
                        mIntOT_TrnHeadId = 0
                        mIntPH_TrnHeadId = 0
                    End If

                End If

                'Pinkal (23-Sep-2014) -- End

                'Dim mdecFinalAmt As Decimal = 0 : dblOT_Hrs = 0 : mDecNormalAmt = 0 : mDecNormalOTAmt = 0
                'If mDecNF_OT > 0 AndAlso mDecPH_OT > 0 Then
                '    If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                '        dblOT_Hrs = iDecAValue
                '        mdecFinalAmt = ((mDecAcitityRate * mDecPH_OT) * iDecAValue)
                '        mDecNormalOTAmt = mdecFinalAmt
                '    Else
                '        If mDblSftHrs > 0 Then
                '            dblOT_Hrs = iDecAValue - mDblSftHrs
                '            If dblOT_Hrs <= 0 Then
                '                mdecFinalAmt = mDecAcitityRate * iDecAValue
                '                mDecNormalAmt = mdecFinalAmt
                '            Else
                '                mDecNormalAmt = (CDec(mDblSftHrs * mDecAcitityRate))
                '                mDecNormalOTAmt = CDec(dblOT_Hrs * (mDecAcitityRate * mDecNF_OT))
                '                mdecFinalAmt = mDecNormalAmt + mDecNormalOTAmt
                '            End If
                '        Else
                '            mdecFinalAmt = mDecAcitityRate * iDecAValue
                '        End If
                '    End If
                'Else
                '    mdecFinalAmt = mDecAcitityRate * iDecAValue
                'End If

                'Pinkal (4-Sep-2014) -- End

                mintPayActivityTranId = objActTran.isExist(CInt(iEmployeeId), iADate.Date, CInt(iActivityId), CInt(iCCenterId))
                objActTran._Payactivitytranunkid = mintPayActivityTranId

                objActTran._Activity_Date = iADate
                objActTran._Activity_Value = iDecAValue
                objActTran._Activityunkid = CInt(iActivityId)
                objActTran._Approveruserunkid = 0
                objActTran._Costcenterunkid = CInt(iCCenterId)
                objActTran._Employeeunkid = CInt(iEmployeeId)
                objActTran._Isapproved = False
                objActTran._Isposted = False
                objActTran._Isvoid = False
                objActTran._Periodunkid = CInt(iPeriodId)
                objActTran._Userunkid = User._Object._Userunkid
                objActTran._Voiddatetime = Nothing
                objActTran._Voidreason = ""
                objActTran._Voiduserunkid = -1
                If mblnIsFormualBased = False Then
                    objActTran._Activity_Rate = CDec(mDecAcitityRate)
                    objActTran._Amount = CDec(mdecFinalAmt)
                Else
                    objActTran._Activity_Rate = 0
                    objActTran._Amount = 0
                End If
                objActTran._Isformula_Based = mblnIsFormualBased
                objActTran._Tranheadunkid = mIntTrnHeadId


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                objActTran._OT_Tranheadunkid = mIntOT_TrnHeadId
                objActTran._PH_Tranheadunkid = mIntPH_TrnHeadId
                'Pinkal (4-Sep-2014) -- End


                objActTran._Normal_Amount = mDecNormalAmt


                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA
                If mblnIsOTActivity Then
                    If mblnIsHoliday = True Or mblnIsWeekEnd = True Then
                        objActTran._PH_Factor = mDecPH_OT
                        objActTran._Normal_Factor = 0
                        objActTran._Normal_Hrs = 0
                    Else
                        objActTran._PH_Factor = 0
                        objActTran._Normal_Factor = mDecNF_OT
                        objActTran._Normal_Hrs = CDec(mDblSftHrs)
                    End If
                Else
                    dblOT_Hrs = 0
                    mDecNormalOTAmt = 0
                    objActTran._PH_Factor = 0
                    objActTran._Normal_Factor = 0
                    objActTran._Normal_Hrs = CDec(mDblSftHrs)
                End If
                'Pinkal (23-Sep-2014) -- End


                objActTran._Ot_Amount = mDecNormalOTAmt
                objActTran._Ot_Hrs = CDec(IIf(dblOT_Hrs <= 0, 0, dblOT_Hrs))


                If rdUpdateExisting.Checked = True Then
                    If mintPayActivityTranId <= 0 Then

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objActTran.Insert() = True Then
                        If objActTran.Insert(ConfigParameter._Object._CurrentDateAndTime) = True Then
                            'Shani(24-Aug-2015) -- End

                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 19, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Insert Data Failed.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    Else

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objActTran.Update() = True Then
                        If objActTran.Update(ConfigParameter._Object._CurrentDateAndTime) = True Then
                            'Shani(24-Aug-2015) -- End

                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 19, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Insert Data Failed.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                Else

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objActTran.Insert() = True Then
                    If objActTran.Insert(ConfigParameter._Object._CurrentDateAndTime) = True Then
                        'Shani(24-Aug-2015) -- End

                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 19, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Insert Data Failed.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub chkUseEmployeeCC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUseEmployeeCC.CheckedChanged
        Try
            cboCostCenter.Enabled = Not chkUseEmployeeCC.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkUseEmployeeCC_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Pay Per Activity Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonSave.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizPPA.CancelText = Language._Object.getCaption(Me.eZeeWizPPA.Name & "_CancelText", Me.eZeeWizPPA.CancelText)
            Me.eZeeWizPPA.NextText = Language._Object.getCaption(Me.eZeeWizPPA.Name & "_NextText", Me.eZeeWizPPA.NextText)
            Me.eZeeWizPPA.BackText = Language._Object.getCaption(Me.eZeeWizPPA.Name & "_BackText", Me.eZeeWizPPA.BackText)
            Me.eZeeWizPPA.FinishText = Language._Object.getCaption(Me.eZeeWizPPA.Name & "_FinishText", Me.eZeeWizPPA.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblActivity.Text = Language._Object.getCaption(Me.lblActivity.Name, Me.lblActivity.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
            Me.lblActivityDate.Text = Language._Object.getCaption(Me.lblActivityDate.Name, Me.lblActivityDate.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.lblActivityValue.Text = Language._Object.getCaption(Me.lblActivityValue.Name, Me.lblActivityValue.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.chkUseEmployeeCC.Text = Language._Object.getCaption(Me.chkUseEmployeeCC.Name, Me.chkUseEmployeeCC.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhActivity.HeaderText = Language._Object.getCaption(Me.colhActivity.Name, Me.colhActivity.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.lblOperation.Text = Language._Object.getCaption(Me.lblOperation.Name, Me.lblOperation.Text)
            Me.rdUpdateExisting.Text = Language._Object.getCaption(Me.rdUpdateExisting.Name, Me.rdUpdateExisting.Text)
            Me.rdAddNew.Text = Language._Object.getCaption(Me.rdAddNew.Name, Me.rdAddNew.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue.")
            Language.setMessage(mstrModuleName, 4, "Employee Name is mandatory information. Please select Employee Name to continue.")
            Language.setMessage(mstrModuleName, 7, "Activity Date is mandatory information. Please select Activity Date to continue.")
            Language.setMessage(mstrModuleName, 8, "Activity Value is mandatory information. Please select Activity Value to continue.")
            Language.setMessage(mstrModuleName, 9, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Language.setMessage(mstrModuleName, 10, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 11, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 12, "Fail")
            Language.setMessage(mstrModuleName, 13, "Period Not Found Based On Activity Date.")
            Language.setMessage(mstrModuleName, 15, "Invalid Activity Value.")
            Language.setMessage(mstrModuleName, 16, "Invalid Activity Date.")
            Language.setMessage(mstrModuleName, 17, "Cost Center Not Found.")
            Language.setMessage(mstrModuleName, 18, "No Rate defined for the selected activity for the given date.")
            Language.setMessage(mstrModuleName, 19, "Success")
            Language.setMessage(mstrModuleName, 20, "Insert Data Failed.")
            Language.setMessage(mstrModuleName, 21, "Employee's Appointed Date is greater than activity date.")
            Language.setMessage(mstrModuleName, 22, "Employee's Leaving Date is less than activity date.")
            Language.setMessage(mstrModuleName, 23, "Employee's Retirement Date is less than activity date.")
            Language.setMessage(mstrModuleName, 24, "Employee's EOC Date is less than activity date.")
            Language.setMessage(mstrModuleName, 25, "Activity Code is mandatory information. Please select Activity Code to continue.")
            Language.setMessage(mstrModuleName, 27, "Activity Code Not Found.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class