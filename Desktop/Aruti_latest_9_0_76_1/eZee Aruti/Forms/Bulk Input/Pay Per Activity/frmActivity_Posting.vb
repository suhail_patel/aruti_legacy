﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmActivity_Posting

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmActivity_Posting"
    Private objActTran As clsPayActivity_Tran
    Private dtView As DataView
    Private dsList As DataSet

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objPMaster As New clscommom_period_Tran
        Try
            RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            RemoveHandler cboViewBy.SelectedIndexChanged, AddressOf cboViewBy_SelectedIndexChanged

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPMaster.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsCombo = objPMaster.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            With cboViewBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Show Unposted Activity"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Show Posted Activity"))
                .SelectedIndex = 0
            End With

            AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            AddHandler cboViewBy.SelectedIndexChanged, AddressOf cboViewBy_SelectedIndexChanged


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            dsCombo = Nothing
            Dim objActivity As New clsActivity_Master
            dsCombo = objActivity.getComboList("List", True)
            With cboActivitiy
                .ValueMember = "activityunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'Pinkal (4-Sep-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objPMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_List()

        Try
            dsList = objActTran.GetList("List", , CInt(cboPeriod.SelectedValue), , , , CBool(IIf(cboViewBy.SelectedIndex = 0, False, True)))
            dgvData.AutoGenerateColumns = False

            dgcolhDate.DataPropertyName = "activity_date"
            dgcolhEmployee.DataPropertyName = "employeename"
            dgcolhActivity.DataPropertyName = "activityname"
            dgcolhUoM.DataPropertyName = "measure"
            dgcolhActValue.DataPropertyName = "activity_value"
            dgcolhRate.DataPropertyName = "activity_rate"
            dgcolhAmount.DataPropertyName = "amount"


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            objdgcolhSelect.DataPropertyName = "select"
            'Pinkal (06-Mar-2014) -- End


            dgcolhActValue.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhActValue.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhRate.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhRate.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency

            dtView = dsList.Tables(0).DefaultView


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            SetFilter()
            'Pinkal (4-Sep-2014) -- End

            dgvData.DataSource = dtView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If dtView.Table.Rows.Count <= 0 Then Return False

            'Sohail (10 Jul 2018) -- Start
            'MTC Support Issue Id # 2376 : Payments change once payroll is voided and reprocessed. (Not allow to post / void post PPA if one employee is processed) in 72.1.
            'Dim dTemp() As DataRow = dtView.Table.Select("")
            Dim dTemp() As DataRow = dtView.Table.Select("select = true")
            'Sohail (10 Jul 2018) -- End

            Dim sValue As String = String.Empty
            Dim selectedTags As List(Of Integer) = dTemp.Select(Function(x) x.Field(Of Integer)("employeeunkid")).Distinct.ToList
            Dim result() As String = selectedTags.Select(Function(x) x.ToString()).ToArray()
            sValue = String.Join(", ", result)

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            Dim objTnALeaveTran As New clsTnALeaveTran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid, sValue, objPeriod._End_Date) = True Then
            If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), sValue, objPeriod._End_Date) = True Then
                'Sohail (21 Aug 2015) -- End
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot post/void posted activity for this period.") & vbCrLf & _
                '             Language.getMessage(mstrModuleName, 9, "Reason : Payroll is already processed for the last date of selected period."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot post/void posted activity for this period.") & vbCrLf & Language.getMessage(mstrModuleName, 9, "Reason : Payroll is already processed for the last date of selected period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 15, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Return False
            End If
            objTnALeaveTran = Nothing
            objPeriod = Nothing

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Private Sub SetVisibility()
        Try
            btnPostActivity.Enabled = User._Object.Privilege._AllowtoPostActivitytoPayroll
            btnVoidPosting.Enabled = User._Object.Privilege._AllowtoVoidActivtyfromPayroll
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dtView.ToTable.Select("select = True")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    'Pinkal (06-Mar-2014) -- End


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA
    Private Sub SetFilter()
        Try
            If dtView IsNot Nothing Then
                dtView.RowFilter = "ADate >= '" & eZeeDate.convertDate(dtpFromDate.Value.Date) & "' AND ADate <= '" & eZeeDate.convertDate(dtpToDate.Value.Date) & "'"
                If CInt(cboActivitiy.SelectedValue) > 0 Then
                    dtView.RowFilter &= " AND activityunkid = " & CInt(cboActivitiy.SelectedValue)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Sub
    'Pinkal (4-Sep-2014) -- End



#End Region

#Region " Form's Events "

    Private Sub frmActivity_Posting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objActTran = New clsPayActivity_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call Fill_Combo()


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            cboViewBy_SelectedIndexChanged(New Object(), New EventArgs())
            'Pinkal (06-Mar-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivity_Posting_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnPostActivity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPostActivity.Click
        Try
            If Is_Valid() = False Then Exit Sub

            Me.Cursor = Cursors.WaitCursor

            'S.SANDEEP [ 28 AUG 2014 ] -- START
            'If objActTran.Posting_Activity(True, CInt(cboPeriod.SelectedValue), User._Object._Userunkid) = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Data successfully posted to payroll for the selected period."), enMsgBoxStyle.Information)

            '    'Pinkal (06-Mar-2014) -- Start
            '    'Enhancement : TRA Changes

            '    txtSearch.Text = ""
            '    chkSelectAll.Checked = False

            '    'Pinkal (06-Mar-2014) -- End

            '    Call Fill_List()
            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Problem in posting data to payroll."), enMsgBoxStyle.Information)
            'End If

            If cboViewBy.SelectedIndex = 0 Then 'SHOW UNPOSTED ACTIVITY


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA

                If dtView Is Nothing OrElse dtView.Table.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "There is no data to Void Unposted Activity."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim drRow() As DataRow = dsList.Tables(0).Select("select = true")

                Dim blnFlag As Boolean = False
                If drRow.Length > 0 Then

                    'If objActTran.ReProcess_Posted_Activity(dsList, User._Object._Userunkid) Then

                    For Each dRow As DataRow In drRow

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objActTran.ReProcess_Posted_Activity(dRow, User._Object._Userunkid) Then
                        If objActTran.ReProcess_Posted_Activity(dRow, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime) Then
                            'Shani(24-Aug-2015) -- End


                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If objActTran.Posting_Activity(True, CInt(cboPeriod.SelectedValue), User._Object._Userunkid, Nothing, CInt(dRow("employeeunkid")), CInt(dRow("activityunkid")), CInt(dRow("payactivitytranunkid"))) = True Then
                            If objActTran.Posting_Activity(True, CInt(cboPeriod.SelectedValue), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, Nothing, CInt(dRow("employeeunkid")), CInt(dRow("activityunkid")), CInt(dRow("payactivitytranunkid"))) = True Then
                                'Shani(24-Aug-2015) -- End

                                blnFlag = True
                            Else
                                blnFlag = False
                            End If
                        End If
                    Next

                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Data successfully posted to payroll for the selected period."), enMsgBoxStyle.Information)
                        txtSearch.Text = ""
                        chkSelectAll.Checked = False
                        Call Fill_List()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Problem in posting data to payroll."), enMsgBoxStyle.Information)
                    End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please tick atleast one activity to do further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                'Pinkal (4-Sep-2014) -- End

            End If
            'S.SANDEEP [ 28 AUG 2014 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPostActivity_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnVoidPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVoidPosting.Click
        Try
            If Is_Valid() = False Then Exit Sub


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes

            Dim drRow() As DataRow = dsList.Tables(0).Select("select = true")

            If drRow.Length > 0 Then
                Me.Cursor = Cursors.WaitCursor
                Dim blnFlag As Boolean = False
                For Each dr As DataRow In drRow
                    'If objActTran.Posting_Activity(False, CInt(cboPeriod.SelectedValue), User._Object._Userunkid, Nothing, CInt(dr("employeeunkid"))) Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objActTran.VoidPosting_Activity(CInt(cboPeriod.SelectedValue), User._Object._Userunkid, CInt(dr("employeeunkid")), CInt(dr("payactivitytranunkid")), CInt(dr("activityunkid"))) Then
                    If objActTran.VoidPosting_Activity(CInt(cboPeriod.SelectedValue), User._Object._Userunkid, CInt(dr("employeeunkid")), CInt(dr("payactivitytranunkid")), CInt(dr("activityunkid")), ConfigParameter._Object._CurrentDateAndTime) Then
                        'Shani(24-Aug-2015) -- End

                        blnFlag = True
                    Else
                        Exit For
                    End If
                Next

                If blnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data successfully voided from payroll for the selected period."), enMsgBoxStyle.Information)
                    txtSearch.Text = ""
                    chkSelectAll.Checked = False
                    Fill_List()
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Problem in voiding data posted to payroll."), enMsgBoxStyle.Information)
                End If

                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please tick atleast one activity to do further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Pinkal (4-Sep-2014) -- End

            'Pinkal (06-Mar-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVoidPosting_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    Private Sub btnVoidUnpostedActivity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoidUnpostedActivity.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            If dtView Is Nothing OrElse dtView.Table.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "There is no data to Void Unposted Activity."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            'Pinkal (28-Oct-2014) -- Start
            'Enhancement -  CHANGES FOR PPA SET ALERT ON VOIDING UNPOSTED ACTIVITY AND SET TAB STOP ON BULK INPUT AS PER MR.RUTT'S COMMENT
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You are about to Void Selected Unposted Activities. If you press YES then selected activities will get deleted.Are you sure you want to void them ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim drRow() As DataRow = dsList.Tables(0).Select("select = true")

                If drRow.Length > 0 Then
                    Me.Cursor = Cursors.WaitCursor
                    Dim blnFlag As Boolean = False
                    For Each dr As DataRow In drRow

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objActTran.VoidUnPosted_Activity(CInt(cboPeriod.SelectedValue), User._Object._Userunkid, CInt(dr("employeeunkid")), CInt(dr("payactivitytranunkid")), CInt(dr("activityunkid"))) Then
                        If objActTran.VoidUnPosted_Activity(CInt(cboPeriod.SelectedValue), User._Object._Userunkid, CInt(dr("employeeunkid")), CInt(dr("payactivitytranunkid")), CInt(dr("activityunkid")), ConfigParameter._Object._CurrentDateAndTime) Then
                            'Shani(24-Aug-2015) -- End

                            blnFlag = True
                        Else
                            Exit For
                        End If
                    Next

                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Unposted Activity successfully voided for the selected period."), enMsgBoxStyle.Information)
                        txtSearch.Text = ""
                        chkSelectAll.Checked = False
                        Fill_List()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Problem in voiding Unposted Activity data."), enMsgBoxStyle.Information)
                    End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please tick atleast one activity to do further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            End If

            'Pinkal (28-Oct-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVoidUnpostedActivity_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub objbtnSearchMeasures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMeasures.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .DataSource = CType(cboPeriod.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboPeriod.SelectedValue = frm.SelectedValue
                cboPeriod.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMeasures_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAcitivity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAcitivity.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboActivitiy.ValueMember
                .DisplayMember = cboActivitiy.DisplayMember
                .DataSource = CType(cboActivitiy.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboActivitiy.SelectedValue = frm.SelectedValue
                cboActivitiy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAcitivity_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (4-Sep-2014) -- End

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try

            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            dtpFromDate.MinDate = CDate("01-Jan-1900")
            dtpFromDate.MaxDate = CDate("01-Jan-9998")
            dtpToDate.MinDate = CDate("01-Jan-1900")
            dtpToDate.MaxDate = CDate("01-Jan-9998")
            'Pinkal (4-Sep-2014) -- End

            If CInt(cboPeriod.SelectedValue) > 0 Then

                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                dtpFromDate.MinDate = objPeriod._Start_Date.Date
                dtpFromDate.MaxDate = objPeriod._End_Date.Date
                dtpFromDate.Value = objPeriod._Start_Date.Date
                dtpToDate.MinDate = objPeriod._Start_Date.Date
                dtpToDate.MaxDate = objPeriod._End_Date.Date
                'Pinkal (4-Sep-2014) -- End
                Call Fill_List()
            Else

                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                dtpFromDate.MinDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.MinDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpFromDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                'Pinkal (4-Sep-2014) -- End

                dgvData.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            Select Case cboViewBy.SelectedIndex
                Case 0
                    btnPostActivity.Visible = True
                    btnVoidPosting.Visible = False

                    'Pinkal (4-Sep-2014) -- Start
                    'Enhancement - PAY_A CHANGES IN PPA
                    'objdgcolhSelect.Visible = False
                    'chkSelectAll.Visible = False
                    btnVoidUnpostedActivity.Enabled = True
                    'Pinkal (4-Sep-2014) -- End
                Case 1
                    btnPostActivity.Visible = False
                    btnVoidPosting.Visible = True
                    objdgcolhSelect.Visible = True
                    chkSelectAll.Visible = True


                    'Pinkal (4-Sep-2014) -- Start
                    'Enhancement - PAY_A CHANGES IN PPA
                    btnVoidUnpostedActivity.Enabled = False
                    'Pinkal (4-Sep-2014) -- End
            End Select

            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = dgvData.Rows(dgvData.RowCount - 1).Index Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhEmployee.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhActivity.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhUoM.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' "
            End If
            dtView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes

#Region "CheckBox's Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dtView IsNot Nothing AndAlso dtView.Table.Rows.Count > 0 Then
                For Each dr As DataRowView In dtView
                    RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
                    dr("select") = chkSelectAll.Checked
                    AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
                    dr.EndEdit()
                Next
                dtView.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGridView Event"

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhSelect.Index Then

                If dgvData.IsCurrentCellDirty Then
                    dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    dtView.ToTable.AcceptChanges()
                End If

                SetCheckBoxValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (06-Mar-2014) -- End


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

#Region "Combobox Event"

    Private Sub cboActivitiy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboActivitiy.SelectedIndexChanged
        Try
            SetFilter()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboActivitiy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DatePicker Event"

    Private Sub dtpFromDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.ValueChanged, dtpToDate.ValueChanged
        Try
            SetFilter()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpFromDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (4-Sep-2014) -- End











    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbActivityPosting.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbActivityPosting.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnPostActivity.GradientBackColor = GUI._ButttonBackColor
            Me.btnPostActivity.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnVoidPosting.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoidPosting.GradientForeColor = GUI._ButttonFontColor

            Me.btnVoidUnpostedActivity.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoidUnpostedActivity.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnPostActivity.Text = Language._Object.getCaption(Me.btnPostActivity.Name, Me.btnPostActivity.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbActivityPosting.Text = Language._Object.getCaption(Me.gbActivityPosting.Name, Me.gbActivityPosting.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
            Me.btnVoidPosting.Text = Language._Object.getCaption(Me.btnVoidPosting.Name, Me.btnVoidPosting.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.dgcolhDate.HeaderText = Language._Object.getCaption(Me.dgcolhDate.Name, Me.dgcolhDate.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhActivity.HeaderText = Language._Object.getCaption(Me.dgcolhActivity.Name, Me.dgcolhActivity.HeaderText)
            Me.dgcolhUoM.HeaderText = Language._Object.getCaption(Me.dgcolhUoM.Name, Me.dgcolhUoM.HeaderText)
            Me.dgcolhActValue.HeaderText = Language._Object.getCaption(Me.dgcolhActValue.Name, Me.dgcolhActValue.HeaderText)
            Me.dgcolhRate.HeaderText = Language._Object.getCaption(Me.dgcolhRate.Name, Me.dgcolhRate.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.btnVoidUnpostedActivity.Text = Language._Object.getCaption(Me.btnVoidUnpostedActivity.Name, Me.btnVoidUnpostedActivity.Text)
            Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)
            Me.LblActivitiy.Text = Language._Object.getCaption(Me.LblActivitiy.Name, Me.LblActivitiy.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Show Unposted Activity")
            Language.setMessage(mstrModuleName, 2, "Show Posted Activity")
            Language.setMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot post/void posted activity for this period.")
            Language.setMessage(mstrModuleName, 5, "Problem in posting data to payroll.")
            Language.setMessage(mstrModuleName, 6, "Data successfully voided from payroll for the selected period.")
            Language.setMessage(mstrModuleName, 7, "Problem in voiding data posted to payroll.")
            Language.setMessage(mstrModuleName, 8, "Data successfully posted to payroll for the selected period.")
            Language.setMessage(mstrModuleName, 9, "Reason : Payroll is already processed for the last date of selected period.")
            Language.setMessage(mstrModuleName, 10, "There is no data to Void Unposted Activity.")
            Language.setMessage(mstrModuleName, 11, "Unposted Activity successfully voided for the selected period.")
            Language.setMessage(mstrModuleName, 12, "Problem in voiding Unposted Activity data.")
            Language.setMessage(mstrModuleName, 13, "Please tick atleast one activity to do further operation.")
            Language.setMessage(mstrModuleName, 14, "You are about to Void Selected Unposted Activities. If you press YES then selected activities will get deleted.Are you sure you want to void them ?")
			Language.setMessage(mstrModuleName, 15, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class