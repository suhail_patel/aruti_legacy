﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

#End Region

Public Class frmGlobalAssignAcitivty

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalAssignAcitivty"
    Private objActTran As clsPayActivity_Tran
    Private mintPayActivityTranId As Integer = 0
    Private mdtEmployee As DataTable
    Private dtView As DataView
    Private mstrAllocationFilter As String = String.Empty
    Private dtDateRange As DataTable
    'Sohail (16 Jul 2013) -- Start
    'TRA - ENHANCEMENT
    Private mdtMeasure As DataTable
    Private dtViewMeasure As DataView
    'Sohail (16 Jul 2013) -- End

    Private mintEmpCCId As Integer = 0
    Private mDecNF_OT As Decimal = 0
    Private mDecPH_OT As Decimal = 0
    Private mDblSftHrs As Double = 0
    Private mblnIsFormualBased As Boolean = False
    Private mDecAcitityRate As Decimal = 0
    Private mIntTrnHeadId As Integer = 0
    Private mblnIsWeekEnd As Boolean = False
    Private mblnIsHoliday As Boolean = False
    Private mDicWeekName As Dictionary(Of String, Integer)
    Private mblnSkip As Boolean = False
    Private mDecNormalAmt, mDecNormalOTAmt As Decimal
    Private dblOT_Hrs As Double


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA
    Private mIntOT_TrnHeadId As Integer = 0
    Private mIntPH_TrnHeadId As Integer = 0
    'Pinkal (4-Sep-2014) -- End


#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objCC As New clscostcenter_master
        Dim objPeriod As New clscommom_period_Tran 'Sohail (16 Jul 2013) 
        Try
            dsCombo = objCC.getComboList("List", True)
            With cboCostcenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombo.Tables(0)
                .Text = ""
            End With

            'Sohail (16 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (16 Jul 2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCC = Nothing
            objPeriod = Nothing 'Sohail (16 Jul 2013) 
        End Try
    End Sub

    Private Sub Fill_Activity_Grid(ByVal iUoMTypId As Integer)
        Dim objActivity As New clsActivity_Master
        Dim dsList As New DataSet
        Try
            'Sohail (16 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'RemoveHandler lvActivityList.ItemChecked, AddressOf lvActivityList_ItemChecked
            'dsList = objActivity.getGridList("List", CInt(cboMeasures.SelectedValue), iUoMTypId)
            'lvActivityList.Items.Clear()
            'For Each dRow As DataRow In dsList.Tables("List").Rows
            '    Dim lvItem As New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.SubItems.Add(dRow.Item("code").ToString)
            '    lvItem.SubItems.Add(dRow.Item("name").ToString)
            '    lvItem.Tag = dRow.Item("activityunkid").ToString

            '    lvActivityList.Items.Add(lvItem)
            'Next
            'If lvActivityList.Items.Count > 14 Then
            '    colhName.Width = 150 - 20
            'Else
            '    colhName.Width = 150
            'End If
            'AddHandler lvActivityList.ItemChecked, AddressOf lvActivityList_ItemChecked
            dsList = objActivity.getGridList("List", CInt(cboMeasures.SelectedValue), iUoMTypId)

            mdtMeasure = dsList.Tables("List").Copy
            dtViewMeasure = mdtMeasure.DefaultView
            dgvActivityList.AutoGenerateColumns = False
            dgvActivityList.DataSource = dtViewMeasure
            'Sohail (16 Jul 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Activity_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetDataSources()
        Try

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            dtView.Sort = "employeecode asc"
            dtView.RowFilter = "isapproved = 1"
            'Pinkal (24-Aug-2015) -- End

            dgvEmployee.AutoGenerateColumns = False
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            objdgcolhECheck.DataPropertyName = "IsCheck"
            dgvEmployee.DataSource = dtView
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetDataSources", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Employee()
        Dim objEMaster As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEMaster.GetEmployeeList("List", False, , , , , , , , , , , , , dtpDate1.Value.Date, dtpDate2.Value.Date, , , , , mstrAllocationFilter)
            dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                dtpDate1.Value.Date, _
                                                dtpDate2.Value.Date, _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, False, "List", True, , , , , , , , , , , , , , , mstrAllocationFilter)
            'S.SANDEEP [04 JUN 2015] -- END

            dsList.Tables("List").Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                dsList.Tables("List").Rows(i)("IsCheck") = False
            Next
            mdtEmployee = dsList.Tables("List").Copy
            dtView = mdtEmployee.DefaultView
            Call SetDataSources()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee", mstrModuleName)
        Finally
            objEMaster = Nothing
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Dim dTemp() As DataRow = Nothing 'Sohail (16 Jul 2013) 
        Try
            'Sohail (16 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'If lvActivityList.CheckedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Activity is mandatory information. Please tick atleast one activity in order to continue."), enMsgBoxStyle.Information)
            '    Return False
            'End If
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Period is mandatory information. Please select Period in order to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            dTemp = mdtMeasure.Select("IsCheck=True")
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Activity is mandatory information. Please tick atleast one activity in order to continue."), enMsgBoxStyle.Information)
                Return False
            End If
            'Sohail (16 Jul 2013) -- End

            'Dim dTemp() As DataRow = Nothing 'Sohail (16 Jul 2013) 
            dTemp = mdtEmployee.Select("IsCheck=True")
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is mandatory information. Please tick atleast one employee in order to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If txtValue.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Activity Value is mandatory information. Please enter activity value to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid", mstrModuleName)
        End Try
    End Function

    Private Function Insert_Update() As Boolean
        Try
            mblnSkip = False
            If dtDateRange Is Nothing Then Return False
            Dim iDates As List(Of DataRow) = dtDateRange.AsEnumerable().ToList
            Dim iEmp = From p In mdtEmployee.AsEnumerable() Where p.Field(Of Boolean)("IsCheck") = True Select p
            'Sohail (16 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'Dim iTags As List(Of String) = lvActivityList.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToList()
            Dim iTags As List(Of String) = (From p In mdtMeasure.AsEnumerable() Where p.Field(Of Boolean)("IsCheck") = True Select (p.Item("activityunkid").ToString)).ToList
            'Sohail (16 Jul 2013) -- End

            For Each dDate As DataRow In iDates
                For Each iCheck As String In iTags
                    Dim dLs As New DataSet
                    dLs = objActTran.Get_Activity_Rate(CInt(iCheck), CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)).Date)
                    If dLs.Tables(0).Rows.Count <= 0 Then
                        If mblnSkip = False Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, No rate(s) are define for the selected activity(s) in the given date range.") & vbCrLf & _
                                                                 Language.getMessage(mstrModuleName, 8, "As a result, Posting of activity on these dates will be skipped for the selected employee(s).") & vbCrLf & _
                                                                 Language.getMessage(mstrModuleName, 9, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Return False
                            Else
                                mblnSkip = True
                            End If
                        End If
                    End If
                    If mblnSkip = True Then Continue For


                    'Pinkal (4-Sep-2014) -- Start
                    'Enhancement - PAY_A CHANGES IN PPA
                    'If dLs.Tables(0).Rows.Count > 0 Then
                    '    mDecAcitityRate = CDec(dLs.Tables(0).Rows(0)("activity_rate"))
                    '    mIntTrnHeadId = CInt(dLs.Tables(0).Rows(0)("tranheadunkid"))
                    'End If

                    'Pinkal (4-Sep-2014) -- End


                    Dim objActivity As New clsActivity_Master
                    Dim objMeasure As New clsUnitMeasure_Master

                    objActivity._Activityunkid = CInt(iCheck)
                    objMeasure._Measureunkid = objActivity._Measureunkid



                    'Pinkal (23-Sep-2014) -- Start
                    'Enhancement -  PAY_A CHANGES IN PPA
                    Dim mblnIsOTActivity As Boolean = objActivity._Is_Ot_Activity
                    'Pinkal (23-Sep-2014) -- End


                    'Pinkal (4-Sep-2014) -- Start
                    'Enhancement - PAY_A CHANGES IN PPA
                    'mDecNF_OT = objActivity._Normal_Otfactor
                    'mDecPH_OT = objActivity._Ph_Otfactor
                    'Pinkal (4-Sep-2014) -- End

                    
                    mblnIsFormualBased = objMeasure._Isformula_Based
                    For Each emp In iEmp
                        Dim objEmp As New clsEmployee_Master

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp._Employeeunkid = CInt(emp.Item("employeeunkid"))
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(emp.Item("employeeunkid"))
                        'S.SANDEEP [04 JUN 2015] -- END

                        mintEmpCCId = objEmp._Costcenterunkid



                        'Pinkal (23-Sep-2014) -- Start
                        'Enhancement -  PAY_A CHANGES IN PPA

                        If objEmp._Appointeddate = Nothing OrElse objEmp._Appointeddate.Date > CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)).Date Then
                            Continue For
                        End If

                        If objEmp._Termination_From_Date <> Nothing AndAlso objEmp._Termination_From_Date.Date < CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)).Date Then
                            Continue For
                        End If

                        If objEmp._Termination_To_Date <> Nothing AndAlso objEmp._Termination_To_Date.Date < CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)).Date Then
                            Continue For
                        End If

                        If objEmp._Empl_Enddate <> Nothing AndAlso objEmp._Empl_Enddate.Date < CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)).Date Then
                            Continue For
                        End If

                        'Pinkal (23-Sep-2014) -- End


                        'Pinkal (15-Oct-2013) -- Start
                        'Enhancement : TRA Changes
                        'mDblSftHrs = objActTran.Get_Sft_WorkHrs(mDicWeekName, objEmp._Shiftunkid, CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)), mblnIsWeekEnd)
                        Dim objEmpShift As New clsEmployee_Shift_Tran
                        Dim mintShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)), CInt(emp.Item("employeeunkid")))
                        mDblSftHrs = objActTran.Get_Sft_WorkHrs(mDicWeekName, mintShiftId, CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)), mblnIsWeekEnd)
                        'Pinkal (15-Oct-2013) -- End


                        Dim objEHoliday As New clsemployee_holiday
                        If CType(objEHoliday.GetEmployeeHoliday(CInt(emp.Item("employeeunkid")), CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString))), DataTable).Rows.Count > 0 Then
                            mblnIsHoliday = True
                        Else
                            mblnIsHoliday = False
                        End If
                        objEHoliday = Nothing



                        If dLs.Tables(0).Rows.Count > 0 Then

                            'Pinkal (4-Sep-2014) -- Start
                            'Enhancement - PAY_A CHANGES IN PPA
                            Dim dRow() As DataRow = Nothing

                            dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.RATE)
                            If dRow.Length > 0 Then
                                mDecAcitityRate = CDec(dRow(0)("activity_rate"))
                                mIntTrnHeadId = CInt(dRow(0)("tranheadunkid"))
                            End If
                            If mblnIsWeekEnd = False AndAlso mblnIsHoliday = False Then
                                dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.OT)
                                If dRow.Length > 0 Then
                                    mDecNF_OT = CDec(dRow(0)("activity_rate"))
                                    mIntOT_TrnHeadId = CInt(dRow(0)("tranheadunkid"))
                                    mIntPH_TrnHeadId = 0
                                End If
                            ElseIf mblnIsWeekEnd OrElse mblnIsHoliday Then
                                dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.PH)
                                If dRow.Length > 0 Then
                                    mDecPH_OT = CDec(dRow(0)("activity_rate"))
                                    mIntPH_TrnHeadId = CInt(dRow(0)("tranheadunkid"))
                                    mIntOT_TrnHeadId = 0
                                End If
                            End If

                        End If

                        'Dim mdecFinalAmt As Decimal = 0 : dblOT_Hrs = 0 : mDecNormalAmt = 0 : mDecNormalOTAmt = 0
                        'If mDecNF_OT > 0 AndAlso mDecPH_OT > 0 Then
                        '    If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                        '        dblOT_Hrs = txtValue.Decimal
                        '        mdecFinalAmt = ((mDecAcitityRate * mDecPH_OT) * txtValue.Decimal)
                        '        mDecNormalOTAmt = mdecFinalAmt
                        '    Else
                        '        If mDblSftHrs > 0 Then
                        '            dblOT_Hrs = txtValue.Decimal - mDblSftHrs
                        '            If dblOT_Hrs <= 0 Then
                        '                mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                        '                mDecNormalAmt = mdecFinalAmt
                        '            Else
                        '                mDecNormalAmt = (CDec(mDblSftHrs * mDecAcitityRate))
                        '                mDecNormalOTAmt = CDec(dblOT_Hrs * (mDecAcitityRate * mDecNF_OT))
                        '                mdecFinalAmt = mDecNormalAmt + mDecNormalOTAmt
                        '            End If
                        '        Else
                        '            mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                        '        End If
                        '    End If
                        'Else
                        '    mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                        'End If


                        'Pinkal (23-Sep-2014) -- Start
                        'Enhancement -  PAY_A CHANGES IN PPA

                        Dim mdecFinalAmt As Decimal = 0 : dblOT_Hrs = 0 : mDecNormalAmt = 0 : mDecNormalOTAmt = 0

                        If mblnIsFormualBased = False Then

                            If mDecAcitityRate > 0 Then
                            If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                                    If mblnIsOTActivity Then
                                dblOT_Hrs = txtValue.Decimal
                                mdecFinalAmt = ((mDecAcitityRate * mDecPH_OT) * txtValue.Decimal)
                                mDecNormalOTAmt = mdecFinalAmt
                            Else
                                        mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                                        mDecNormalAmt = 0
                                    End If
                                Else
                                If mDblSftHrs > 0 Then
                                        If mblnIsOTActivity Then
                                    dblOT_Hrs = txtValue.Decimal - mDblSftHrs
                                    If dblOT_Hrs <= 0 Then
                                        mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                                        mDecNormalAmt = mdecFinalAmt
                                    Else
                                        mDecNormalAmt = (CDec(mDblSftHrs * mDecAcitityRate))
                                        mDecNormalOTAmt = CDec(dblOT_Hrs * (mDecAcitityRate * mDecNF_OT))
                                        mdecFinalAmt = mDecNormalAmt + mDecNormalOTAmt
                                    End If
                                Else
                                    mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                                            mDecNormalAmt = 0
                                End If
                                    Else
                                        mdecFinalAmt = mDecAcitityRate * txtValue.Decimal
                            End If

                                End If

                            End If

                        Else
                            If mblnIsOTActivity Then
                            If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                                dblOT_Hrs = txtValue.Decimal
                            Else
                                If mDblSftHrs > 0 Then
                                    dblOT_Hrs = txtValue.Decimal - mDblSftHrs

                                    If dblOT_Hrs <= 0 Then
                                        dblOT_Hrs = 0
                                            mIntOT_TrnHeadId = 0
                                            mIntPH_TrnHeadId = 0
                                    End If

                                End If

                            End If
                            Else
                                mIntPH_TrnHeadId = 0
                                mIntOT_TrnHeadId = 0
                        End If

                        End If

                        'Pinkal (23-Sep-2014) -- End

                        'Pinkal (4-Sep-2014) -- End

                        mintPayActivityTranId = objActTran.isExist(CInt(emp.Item("employeeunkid")), _
                                                                   CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)).Date, _
                                                                   CInt(iCheck), _
                                                                   CInt(IIf(cboCostcenter.Enabled = False, mintEmpCCId, CInt(cboCostcenter.SelectedValue))))

                        objActTran._Payactivitytranunkid = mintPayActivityTranId

                        objActTran._Activity_Date = CDate(eZeeDate.convertDate(dDate.Item("iDate").ToString)).Date
                        objActTran._Activity_Value = txtValue.Decimal
                        objActTran._Activityunkid = CInt(iCheck)
                        objActTran._Approveruserunkid = 0
                        objActTran._Costcenterunkid = CInt(IIf(cboCostcenter.Enabled = False, mintEmpCCId, CInt(cboCostcenter.SelectedValue)))
                        objActTran._Employeeunkid = CInt(emp.Item("employeeunkid"))
                        objActTran._Isapproved = False
                        objActTran._Isposted = False
                        objActTran._Isvoid = False
                        objActTran._Periodunkid = CInt(dDate.Item("periodunkid"))
                        objActTran._Userunkid = User._Object._Userunkid
                        objActTran._Voiddatetime = Nothing
                        objActTran._Voidreason = ""
                        objActTran._Voiduserunkid = -1
                        If mblnIsFormualBased = False Then
                            objActTran._Activity_Rate = mDecAcitityRate
                            objActTran._Amount = mdecFinalAmt
                        Else
                            objActTran._Activity_Rate = 0
                            objActTran._Amount = 0
                        End If
                        objActTran._Isformula_Based = mblnIsFormualBased
                        objActTran._Tranheadunkid = mIntTrnHeadId

                        'Pinkal (4-Sep-2014) -- Start
                        'Enhancement - PAY_A CHANGES IN PPA
                        objActTran._OT_Tranheadunkid = mIntOT_TrnHeadId
                        objActTran._PH_Tranheadunkid = mIntPH_TrnHeadId
                        'Pinkal (4-Sep-2014) -- End

                        objActTran._Normal_Amount = mDecNormalAmt


                        'Pinkal (23-Sep-2014) -- Start
                        'Enhancement -  PAY_A CHANGES IN PPA
                        If mblnIsOTActivity Then
                        If mblnIsHoliday = True Or mblnIsWeekEnd = True Then
                            objActTran._PH_Factor = mDecPH_OT
                            objActTran._Normal_Factor = 0
                            objActTran._Normal_Hrs = 0
                        Else
                            objActTran._PH_Factor = 0
                            objActTran._Normal_Factor = mDecNF_OT
                            objActTran._Normal_Hrs = CDec(mDblSftHrs)
                        End If
                        Else
                            dblOT_Hrs = 0
                            mDecNormalOTAmt = 0
                            objActTran._PH_Factor = 0
                            objActTran._Normal_Factor = 0
                            objActTran._Normal_Hrs = CDec(mDblSftHrs)
                        End If
                        'Pinkal (23-Sep-2014) -- End

                        objActTran._Ot_Amount = mDecNormalOTAmt
                        objActTran._Ot_Hrs = CDec(IIf(dblOT_Hrs <= 0, 0, dblOT_Hrs))

                        If rdUpdateExisting.Checked = True Then
                            If mintPayActivityTranId <= 0 Then

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'objActTran.Insert()
                                objActTran.Insert(ConfigParameter._Object._CurrentDateAndTime)
                                'Shani(24-Aug-2015) -- End

                            Else

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'objActTran.Update()
                                objActTran.Update(ConfigParameter._Object._CurrentDateAndTime)
                                'Shani(24-Aug-2015) -- End

                            End If
                        Else

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'objActTran.Insert()
                            objActTran.Insert(ConfigParameter._Object._CurrentDateAndTime)
                            'Shani(24-Aug-2015) -- End

                        End If

                        mintPayActivityTranId = 0
                        objEmp = Nothing
                    Next
                Next
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert_Update", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmGlobalAssignAcitivty_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objActTran = New clsPayActivity_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Fill_Combo()

            Call chkCostCenter_CheckedChanged(sender, e)

            dtpDate1.MinDate = FinancialYear._Object._Database_Start_Date
            dtpDate1.MaxDate = FinancialYear._Object._Database_End_Date

            dtpDate2.MinDate = FinancialYear._Object._Database_Start_Date
            dtpDate2.MaxDate = FinancialYear._Object._Database_End_Date

            radNF.Checked = True

            Dim days() As String = CultureInfo.CurrentCulture.DateTimeFormat.DayNames
            mDicWeekName = New Dictionary(Of String, Integer)
            For i As Integer = 0 To 6
                mDicWeekName.Add(days(i).ToString, i)
            Next

            Call dtpDate1_ValueChanged(sender, e)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalAssignAcitivty_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Try
            If Is_Valid() = False Then Exit Sub
            If Insert_Update() = False Or mblnSkip = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Problem in posting Activity."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Activity posted successfully."), enMsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchMeasures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMeasures.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboMeasures.ValueMember
                .DisplayMember = cboMeasures.DisplayMember
                .DataSource = CType(cboMeasures.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If frm.DisplayDialog Then
                cboMeasures.SelectedValue = frm.SelectedValue
                cboMeasures.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMeasures_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAllocationFilter = ""
            Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sohail (16 Jul 2013) -- Start
    'TRA - ENHANCEMENT
#Region " ComboBox's Events "
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                dtpDate1.MinDate = FinancialYear._Object._Database_Start_Date
                dtpDate1.MaxDate = FinancialYear._Object._Database_End_Date
                dtpDate2.MinDate = FinancialYear._Object._Database_Start_Date
                dtpDate2.MaxDate = FinancialYear._Object._Database_End_Date

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                dtpDate1.MinDate = objPeriod._Start_Date
                dtpDate1.MaxDate = objPeriod._End_Date
                dtpDate2.MinDate = objPeriod._Start_Date
                dtpDate2.MaxDate = objPeriod._End_Date
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (16 Jul 2013) -- End

#Region " Control's Events "

    Private Sub chkCostCenter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCostCenter.CheckedChanged
        Try
            cboCostcenter.Enabled = chkCostCenter.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCostCenter_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dtpDate1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDate1.ValueChanged, dtpDate2.ValueChanged
        Try
            Dim objRates As New clsActivity_Rates
            dtDateRange = objRates.Get_Date_Range(eZeeDate.convertDate(dtpDate1.Value), eZeeDate.convertDate(dtpDate2.Value))
            Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpDate1_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboMeasures_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMeasures.SelectedIndexChanged
        Try
            If CInt(cboMeasures.SelectedValue) > 0 Then
                Call Fill_Activity_Grid(CInt(IIf(radNF.Checked = True, 1, 2)))
            Else
                'Sohail (16 Jul 2013) -- Start
                'TRA - ENHANCEMENT
                'lvActivityList.Items.Clear()
                If dtViewMeasure IsNot Nothing Then dtViewMeasure.RowFilter = "1=2"
                'Sohail (16 Jul 2013) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMeasures_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radFB_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radFB.CheckedChanged, radNF.CheckedChanged
        Try
            If CType(sender, RadioButton).Checked = True Then
                Dim objMeasure As New clsUnitMeasure_Master
                Dim dsCombo As New DataSet : Dim iUoM As Integer = 0
                Select Case CType(sender, RadioButton).Name.ToUpper
                    Case radFB.Name.ToUpper
                        iUoM = 2
                    Case radNF.Name.ToUpper
                        iUoM = 1
                End Select
                dsCombo = objMeasure.getComboList("List", True, iUoM)
                With cboMeasures
                    .ValueMember = "measureunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("List")
                    .SelectedValue = 0
                End With
                objMeasure = Nothing
            Else
                cboMeasures.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radFB_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (16 Jul 2013) -- Start
    'TRA - ENHANCEMENT
    'Private Sub lvActivityList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
    '        If lvActivityList.CheckedItems.Count <= 0 Then
    '            objchkSelectAll.CheckState = CheckState.Unchecked
    '        ElseIf lvActivityList.CheckedItems.Count < lvActivityList.Items.Count Then
    '            objchkSelectAll.CheckState = CheckState.Indeterminate
    '        ElseIf lvActivityList.CheckedItems.Count = lvActivityList.Items.Count Then
    '            objchkSelectAll.CheckState = CheckState.Checked
    '        End If
    '        AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvActivityList_ItemChecked", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Sohail (16 Jul 2013) -- End

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            'Sohail (16 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'RemoveHandler lvActivityList.ItemChecked, AddressOf lvActivityList_ItemChecked
            'For Each lvI As ListViewItem In lvActivityList.Items
            '    lvI.Checked = CBool(objchkSelectAll.CheckState)
            'Next
            'AddHandler lvActivityList.ItemChecked, AddressOf lvActivityList_ItemChecked
            For Each dr As DataRowView In dtViewMeasure
                dr.Item("ischeck") = CBool(objchkSelectAll.CheckState)
            Next
            dgvActivityList.Refresh()
            'Sohail (16 Jul 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchActivity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchActivity.TextChanged
        Try
            'Sohail (16 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'If lvActivityList.Items.Count <= 0 Then Exit Sub
            'lvActivityList.SelectedIndices.Clear()
            'Dim lvFoundItem As ListViewItem = lvActivityList.FindItemWithText(txtSearchActivity.Text, True, 0, True)
            'If lvFoundItem IsNot Nothing Then
            '    lvActivityList.TopItem = lvFoundItem
            '    lvFoundItem.Selected = True
            'End If
            Dim strSearch As String = ""
            If txtSearchActivity.Text.Trim.Length > 0 Then
                strSearch = dgcolhMeasureName.DataPropertyName & " LIKE '%" & txtSearchActivity.Text & "%' OR " & _
                            dgcolhMeasureCode.DataPropertyName & " LIKE '%" & txtSearchActivity.Text & "%'"
                'objchkSelectAll.Checked = False
                'objchkSelectAll.Enabled = False
            Else
                'objchkSelectAll.Enabled = True
            End If
            If CInt(cboMeasures.SelectedValue) <= 0 Then
                strSearch = "1=2"
            End If
            dtViewMeasure.RowFilter = strSearch
            'Sohail (16 Jul 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchActivity_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssessorEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAssessorEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = dgvEmployee.Rows(dgvEmployee.RowCount - 1).Index Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssessorEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAssessorEmp.TextChanged
        Dim strSearch As String = ""
        Try
            If txtAssessorEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtAssessorEmp.Text & "%' OR " & _
                            dgcolhEcode.DataPropertyName & " LIKE '%" & txtAssessorEmp.Text & "%'"
            End If
            dtView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick

            'Dim StrFilter As String = dtView.RowFilter
            'If mdtEmployee IsNot Nothing AndAlso mdtEmployee.Rows.Count > 0 Then
            '    If objchkEmployee.CheckState <> CheckState.Indeterminate Then
            '        For Each dr As DataRow In mdtEmployee.Rows
            '            dr.Item("IsCheck") = CBool(objchkEmployee.CheckState)
            '        Next
            '        dtView = mdtEmployee.DefaultView
            '        dtView.RowFilter = StrFilter
            '        SetDataSources()
            '    End If
            'End If

            For Each dr As DataRowView In dtView
                dr.Item("IsCheck") = CBool(objchkEmployee.CheckState)
            Next
            dgvEmployee.Refresh()
            AddHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvEmployee.IsCurrentCellDirty Then
                    Me.dgvEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim intEmpId As Integer = CInt(dgvEmployee.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value)
                Dim dTemp() As DataRow = mdtEmployee.Select(objdgcolhEmpId.DataPropertyName & " = '" & intEmpId & "'")

                If dTemp.Length > 0 Then
                    If CBool(dTemp(0)(objdgcolhEmpId.DataPropertyName.ToString)) = True Then
                        mdtEmployee.Rows(mdtEmployee.Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName) = CBool(mdtEmployee.Rows(mdtEmployee.Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName))
                    Else
                        mdtEmployee.Rows(mdtEmployee.Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName) = Not CBool(mdtEmployee.Rows(mdtEmployee.Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName))
                    End If
                Else
                    mdtEmployee.Rows(e.RowIndex)(objdgcolhECheck.DataPropertyName) = Not CBool(mdtEmployee.Rows(e.RowIndex)(objdgcolhECheck.DataPropertyName))
                End If
                mdtEmployee.AcceptChanges()

                Dim drRow As DataRow() = mdtEmployee.Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If mdtEmployee.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAllocationFilter = frm._GetFilterString
                Call Fill_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Sohail (16 Jul 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvActivityList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvActivityList.CellContentClick
        Try
            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            If e.ColumnIndex = objdgcolhMCheck.Index Then

                If Me.dgvActivityList.IsCurrentCellDirty Then
                    Me.dgvActivityList.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim intId As Integer = CInt(dgvActivityList.Rows(e.RowIndex).Cells("objdgcolhMeasureId").Value)
                Dim dTemp() As DataRow = mdtMeasure.Select(objdgcolhMeasureId.DataPropertyName & " = '" & intId & "'")

                If dTemp.Length > 0 Then
                    If CBool(dTemp(0)(objdgcolhMeasureId.DataPropertyName.ToString)) = True Then
                        mdtMeasure.Rows(mdtMeasure.Rows.IndexOf(dTemp(0)))(objdgcolhMCheck.DataPropertyName) = CBool(mdtMeasure.Rows(mdtMeasure.Rows.IndexOf(dTemp(0)))(objdgcolhMCheck.DataPropertyName))
                    Else
                        mdtMeasure.Rows(mdtMeasure.Rows.IndexOf(dTemp(0)))(objdgcolhMCheck.DataPropertyName) = Not CBool(mdtMeasure.Rows(mdtMeasure.Rows.IndexOf(dTemp(0)))(objdgcolhMCheck.DataPropertyName))
                    End If
                Else
                    mdtMeasure.Rows(e.RowIndex)(objdgcolhMCheck.DataPropertyName) = Not CBool(mdtMeasure.Rows(e.RowIndex)(objdgcolhMCheck.DataPropertyName))
                End If
                mdtMeasure.AcceptChanges()

                Dim drRow As DataRow() = mdtMeasure.Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If mdtMeasure.Rows.Count = drRow.Length Then
                        objchkSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkSelectAll.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvActivityList_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (16 Jul 2013) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbAssignPayActivity.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssignPayActivity.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbAssignPayActivity.Text = Language._Object.getCaption(Me.gbAssignPayActivity.Name, Me.gbAssignPayActivity.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.eLine1.Text = Language._Object.getCaption(Me.eLine1.Name, Me.eLine1.Text)
            Me.rdUpdateExisting.Text = Language._Object.getCaption(Me.rdUpdateExisting.Name, Me.rdUpdateExisting.Text)
            Me.lblMeasures.Text = Language._Object.getCaption(Me.lblMeasures.Name, Me.lblMeasures.Text)
            Me.radNF.Text = Language._Object.getCaption(Me.radNF.Name, Me.radNF.Text)
            Me.rdAddNew.Text = Language._Object.getCaption(Me.rdAddNew.Name, Me.rdAddNew.Text)
            Me.radFB.Text = Language._Object.getCaption(Me.radFB.Name, Me.radFB.Text)
            Me.lblValue.Text = Language._Object.getCaption(Me.lblValue.Name, Me.lblValue.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.chkCostCenter.Text = Language._Object.getCaption(Me.chkCostCenter.Name, Me.chkCostCenter.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.dgcolhMeasureCode.HeaderText = Language._Object.getCaption(Me.dgcolhMeasureCode.Name, Me.dgcolhMeasureCode.HeaderText)
			Me.dgcolhMeasureName.HeaderText = Language._Object.getCaption(Me.dgcolhMeasureName.Name, Me.dgcolhMeasureName.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
			Language.setMessage(mstrModuleName, 1, "Activity is mandatory information. Please tick atleast one activity in order to continue.")
			Language.setMessage(mstrModuleName, 2, "Employee is mandatory information. Please tick atleast one employee in order to continue.")
			Language.setMessage(mstrModuleName, 3, "Activity Value is mandatory information. Please enter activity value to continue.")
			Language.setMessage(mstrModuleName, 4, "Period is mandatory information. Please select Period in order to continue.")
			Language.setMessage(mstrModuleName, 5, "Problem in posting Activity.")
			Language.setMessage(mstrModuleName, 6, "Activity posted successfully.")
			Language.setMessage(mstrModuleName, 7, "Sorry, No rate(s) are define for the selected activity(s) in the given date range.")
			Language.setMessage(mstrModuleName, 8, "As a result, Posting of activity on these dates will be skipped for the selected employee(s).")
			Language.setMessage(mstrModuleName, 9, "Do you wish to continue?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class