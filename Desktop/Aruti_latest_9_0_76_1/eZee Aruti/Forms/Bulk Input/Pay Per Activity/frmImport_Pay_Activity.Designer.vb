﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImport_Pay_Activity
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImport_Pay_Activity))
        Me.eZeeWizPPA = New eZee.Common.eZeeWizard
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblOperation = New System.Windows.Forms.Label
        Me.pnl4 = New System.Windows.Forms.Panel
        Me.rdUpdateExisting = New System.Windows.Forms.RadioButton
        Me.rdAddNew = New System.Windows.Forms.RadioButton
        Me.chkUseEmployeeCC = New System.Windows.Forms.CheckBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.lblActivityValue = New System.Windows.Forms.Label
        Me.cboActivityValue = New System.Windows.Forms.ComboBox
        Me.cboActivity = New System.Windows.Forms.ComboBox
        Me.lblActivity = New System.Windows.Forms.Label
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.cboEmployeeName = New System.Windows.Forms.ComboBox
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.objlblSign5 = New System.Windows.Forms.Label
        Me.lblActivityDate = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboActivityDate = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhActivity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.objbuttonSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeWizPPA.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.pnl4.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeWizPPA
        '
        Me.eZeeWizPPA.Controls.Add(Me.WizPageMapping)
        Me.eZeeWizPPA.Controls.Add(Me.WizPageSelectFile)
        Me.eZeeWizPPA.Controls.Add(Me.WizPageImporting)
        Me.eZeeWizPPA.Controls.Add(Me.objbuttonSave)
        Me.eZeeWizPPA.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeWizPPA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeWizPPA.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eZeeWizPPA.Location = New System.Drawing.Point(0, 0)
        Me.eZeeWizPPA.Name = "eZeeWizPPA"
        Me.eZeeWizPPA.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageMapping, Me.WizPageImporting})
        Me.eZeeWizPPA.SaveEnabled = True
        Me.eZeeWizPPA.SaveText = "Save && Finish"
        Me.eZeeWizPPA.SaveVisible = False
        Me.eZeeWizPPA.SetSaveIndexBeforeFinishIndex = False
        Me.eZeeWizPPA.Size = New System.Drawing.Size(558, 353)
        Me.eZeeWizPPA.TabIndex = 6
        Me.eZeeWizPPA.WelcomeImage = Nothing
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(558, 305)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.lblOperation)
        Me.gbFieldMapping.Controls.Add(Me.pnl4)
        Me.gbFieldMapping.Controls.Add(Me.chkUseEmployeeCC)
        Me.gbFieldMapping.Controls.Add(Me.lblCostCenter)
        Me.gbFieldMapping.Controls.Add(Me.cboCostCenter)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign6)
        Me.gbFieldMapping.Controls.Add(Me.lblActivityValue)
        Me.gbFieldMapping.Controls.Add(Me.cboActivityValue)
        Me.gbFieldMapping.Controls.Add(Me.cboActivity)
        Me.gbFieldMapping.Controls.Add(Me.lblActivity)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign4)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployeeName)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeeName)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign5)
        Me.gbFieldMapping.Controls.Add(Me.lblActivityDate)
        Me.gbFieldMapping.Controls.Add(Me.lblPeriod)
        Me.gbFieldMapping.Controls.Add(Me.cboActivityDate)
        Me.gbFieldMapping.Controls.Add(Me.cboPeriod)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign3)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign2)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign1)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFieldMapping.Margin = New System.Windows.Forms.Padding(0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(393, 305)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOperation
        '
        Me.lblOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperation.Location = New System.Drawing.Point(33, 252)
        Me.lblOperation.Name = "lblOperation"
        Me.lblOperation.Size = New System.Drawing.Size(123, 15)
        Me.lblOperation.TabIndex = 185
        Me.lblOperation.Text = "Operation"
        Me.lblOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnl4
        '
        Me.pnl4.Controls.Add(Me.rdUpdateExisting)
        Me.pnl4.Controls.Add(Me.rdAddNew)
        Me.pnl4.Location = New System.Drawing.Point(162, 248)
        Me.pnl4.Name = "pnl4"
        Me.pnl4.Size = New System.Drawing.Size(201, 23)
        Me.pnl4.TabIndex = 184
        '
        'rdUpdateExisting
        '
        Me.rdUpdateExisting.Checked = True
        Me.rdUpdateExisting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdUpdateExisting.Location = New System.Drawing.Point(3, 3)
        Me.rdUpdateExisting.Name = "rdUpdateExisting"
        Me.rdUpdateExisting.Size = New System.Drawing.Size(108, 17)
        Me.rdUpdateExisting.TabIndex = 175
        Me.rdUpdateExisting.TabStop = True
        Me.rdUpdateExisting.Text = "Update Existing"
        Me.rdUpdateExisting.UseVisualStyleBackColor = True
        '
        'rdAddNew
        '
        Me.rdAddNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAddNew.Location = New System.Drawing.Point(117, 3)
        Me.rdAddNew.Name = "rdAddNew"
        Me.rdAddNew.Size = New System.Drawing.Size(81, 17)
        Me.rdAddNew.TabIndex = 174
        Me.rdAddNew.Text = "Add New"
        Me.rdAddNew.UseVisualStyleBackColor = True
        '
        'chkUseEmployeeCC
        '
        Me.chkUseEmployeeCC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUseEmployeeCC.Location = New System.Drawing.Point(162, 37)
        Me.chkUseEmployeeCC.Name = "chkUseEmployeeCC"
        Me.chkUseEmployeeCC.Size = New System.Drawing.Size(201, 16)
        Me.chkUseEmployeeCC.TabIndex = 109
        Me.chkUseEmployeeCC.Text = "Use Employee Cost Center"
        Me.chkUseEmployeeCC.UseVisualStyleBackColor = True
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(33, 224)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(123, 15)
        Me.lblCostCenter.TabIndex = 107
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(162, 221)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(201, 21)
        Me.cboCostCenter.TabIndex = 106
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(17, 197)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(10, 15)
        Me.objlblSign6.TabIndex = 105
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblActivityValue
        '
        Me.lblActivityValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityValue.Location = New System.Drawing.Point(33, 197)
        Me.lblActivityValue.Name = "lblActivityValue"
        Me.lblActivityValue.Size = New System.Drawing.Size(123, 15)
        Me.lblActivityValue.TabIndex = 104
        Me.lblActivityValue.Text = "Activity Value"
        Me.lblActivityValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboActivityValue
        '
        Me.cboActivityValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivityValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivityValue.FormattingEnabled = True
        Me.cboActivityValue.Location = New System.Drawing.Point(162, 194)
        Me.cboActivityValue.Name = "cboActivityValue"
        Me.cboActivityValue.Size = New System.Drawing.Size(201, 21)
        Me.cboActivityValue.TabIndex = 103
        '
        'cboActivity
        '
        Me.cboActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivity.FormattingEnabled = True
        Me.cboActivity.Location = New System.Drawing.Point(162, 140)
        Me.cboActivity.Name = "cboActivity"
        Me.cboActivity.Size = New System.Drawing.Size(201, 21)
        Me.cboActivity.TabIndex = 101
        '
        'lblActivity
        '
        Me.lblActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivity.Location = New System.Drawing.Point(33, 143)
        Me.lblActivity.Name = "lblActivity"
        Me.lblActivity.Size = New System.Drawing.Size(123, 15)
        Me.lblActivity.TabIndex = 100
        Me.lblActivity.Text = "Activity"
        Me.lblActivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign4
        '
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(17, 143)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(10, 15)
        Me.objlblSign4.TabIndex = 99
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEmployeeName
        '
        Me.cboEmployeeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeName.FormattingEnabled = True
        Me.cboEmployeeName.Location = New System.Drawing.Point(162, 86)
        Me.cboEmployeeName.Name = "cboEmployeeName"
        Me.cboEmployeeName.Size = New System.Drawing.Size(201, 21)
        Me.cboEmployeeName.TabIndex = 98
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(33, 89)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(123, 15)
        Me.lblEmployeeName.TabIndex = 97
        Me.lblEmployeeName.Text = "Employee Name"
        Me.lblEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign5
        '
        Me.objlblSign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign5.ForeColor = System.Drawing.Color.Red
        Me.objlblSign5.Location = New System.Drawing.Point(17, 170)
        Me.objlblSign5.Name = "objlblSign5"
        Me.objlblSign5.Size = New System.Drawing.Size(10, 15)
        Me.objlblSign5.TabIndex = 96
        Me.objlblSign5.Text = "*"
        Me.objlblSign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblActivityDate
        '
        Me.lblActivityDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityDate.Location = New System.Drawing.Point(33, 170)
        Me.lblActivityDate.Name = "lblActivityDate"
        Me.lblActivityDate.Size = New System.Drawing.Size(123, 15)
        Me.lblActivityDate.TabIndex = 94
        Me.lblActivityDate.Text = "Activity Date"
        Me.lblActivityDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(33, 116)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(123, 15)
        Me.lblPeriod.TabIndex = 93
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboActivityDate
        '
        Me.cboActivityDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivityDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivityDate.FormattingEnabled = True
        Me.cboActivityDate.Location = New System.Drawing.Point(162, 167)
        Me.cboActivityDate.Name = "cboActivityDate"
        Me.cboActivityDate.Size = New System.Drawing.Size(201, 21)
        Me.cboActivityDate.TabIndex = 89
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(162, 113)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(201, 21)
        Me.cboPeriod.TabIndex = 88
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(17, 116)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(10, 15)
        Me.objlblSign3.TabIndex = 86
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(17, 89)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(10, 15)
        Me.objlblSign2.TabIndex = 83
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(186, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(204, 19)
        Me.lblCaption.TabIndex = 34
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(162, 59)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(201, 21)
        Me.cboEmployeeCode.TabIndex = 1
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(33, 62)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(123, 15)
        Me.lblEmployeeCode.TabIndex = 0
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(17, 62)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(10, 15)
        Me.objlblSign1.TabIndex = 0
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(558, 305)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(181, 63)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(365, 45)
        Me.lblMessage.TabIndex = 18
        Me.lblMessage.Text = "This wizard will import Employee 'Pay Per Activity' records made from other syste" & _
            "m."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(181, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(365, 51)
        Me.lblTitle.TabIndex = 17
        Me.lblTitle.Text = "Employee Pay Per Activity Import Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(522, 154)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(24, 20)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(179, 154)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(337, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(179, 131)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(310, 20)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(558, 305)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeight = 22
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhActivity, Me.colhStatus, Me.colhMessage, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(534, 195)
        Me.dgData.TabIndex = 19
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 25
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhActivity
        '
        Me.colhActivity.HeaderText = "Activity"
        Me.colhActivity.Name = "colhActivity"
        Me.colhActivity.ReadOnly = True
        Me.colhActivity.Width = 146
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.Width = 80
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 150
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 270)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(95, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 21
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(534, 51)
        Me.pnlInfo.TabIndex = 2
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(412, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(298, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(412, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(344, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(457, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(298, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(457, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(344, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbuttonSave
        '
        Me.objbuttonSave.BackColor = System.Drawing.Color.White
        Me.objbuttonSave.BackgroundImage = CType(resources.GetObject("objbuttonSave.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonSave.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonSave.FlatAppearance.BorderSize = 0
        Me.objbuttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonSave.ForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonSave.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.Location = New System.Drawing.Point(229, 224)
        Me.objbuttonSave.Name = "objbuttonSave"
        Me.objbuttonSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.Size = New System.Drawing.Size(99, 29)
        Me.objbuttonSave.TabIndex = 7
        Me.objbuttonSave.Text = "Save && Finish"
        Me.objbuttonSave.UseVisualStyleBackColor = False
        Me.objbuttonSave.Visible = False
        '
        'frmImport_Pay_Activity
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(558, 353)
        Me.Controls.Add(Me.eZeeWizPPA)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImport_Pay_Activity"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Pay Per Activity"
        Me.eZeeWizPPA.ResumeLayout(False)
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.pnl4.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.WizPageImporting.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeWizPPA As eZee.Common.eZeeWizard
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonSave As eZee.Common.eZeeLightButton
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboActivity As System.Windows.Forms.ComboBox
    Friend WithEvents lblActivity As System.Windows.Forms.Label
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents objlblSign5 As System.Windows.Forms.Label
    Friend WithEvents lblActivityDate As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboActivityDate As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents lblActivityValue As System.Windows.Forms.Label
    Friend WithEvents cboActivityValue As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents chkUseEmployeeCC As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhActivity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblOperation As System.Windows.Forms.Label
    Friend WithEvents pnl4 As System.Windows.Forms.Panel
    Friend WithEvents rdUpdateExisting As System.Windows.Forms.RadioButton
    Friend WithEvents rdAddNew As System.Windows.Forms.RadioButton
End Class
