﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmSet_Activity_Rate

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSet_Activity_Rate"
    Private mintLoopEndIdx As Integer = 0
    Private objActRate As clsActivity_Rates
    Private mdtTable As DataTable
    Private sCommonMsg As String = String.Empty
    Private mDicUpdate As Dictionary(Of String, Date)
    Private mdtActivity As DataTable
    Private dtViewActivity As DataView


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA
    Private mblnIsPeriodOpen As Boolean = False
    'Pinkal (4-Sep-2014) -- End


#End Region

#Region " Private Methods & Functions "

    Private Sub GenerateDataGrid(ByVal dgvGrid As DataGridView, ByVal mdtTran As DataTable, ByVal sName As String)
        Try
            Dim mdicDGVColumns As New Dictionary(Of Integer, String)
            Dim dgvCol As DataGridViewColumn
            Dim dgvCmbCol As DataGridViewComboBoxColumn
            Dim strTemp As String
            Dim iCnt As Integer = 0
            dgvGrid.Columns.Clear()

            Dim HeaderStyle As New DataGridViewCellStyle
            HeaderStyle.BackColor = Color.SlateGray
            HeaderStyle.ForeColor = Color.White

            dgvGrid.Columns.Add("period", Language.getMessage(mstrModuleName, 3, "Months"))
            dgvCol = dgvGrid.Columns("period")
            dgvCol.Frozen = True
            dgvCol.ReadOnly = True
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle = HeaderStyle
            dgvCol.HeaderCell.Style.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.Width = 125

            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            If sName.ToUpper = radNF.Name.ToUpper Then

                For i As Integer = 1 To 31
                    strTemp = "column" & i.ToString
                    dgvGrid.Columns.Add(strTemp, i.ToString)
                    'Pinkal (18-May-2015) -- Start
                    'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                    dgvCol = dgvGrid.Columns(strTemp)
                    dgvCol.Tag = i.ToString
                    'Pinkal (18-May-2015) -- End

                    dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    dgvCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    dgvCol.DefaultCellStyle.BackColor = Color.AliceBlue
                    dgvCol.HeaderCell.Style.Font = New Font(dgvGrid.Font, FontStyle.Bold)
                    dgvCol.Width = 50

                    'Pinkal (18-May-2015) -- Start
                    'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                    dgvCol.ReadOnly = True
                    'Pinkal (18-May-2015) -- End

                    If mdicDGVColumns.ContainsKey(iCnt) = False Then
                        mdicDGVColumns.Add(iCnt, dgvCol.Name)
                    End If
                    iCnt += 1
                    mintLoopEndIdx += 1
                Next

            ElseIf sName.ToUpper = radFB.Name.ToUpper Then
                Dim dTable As DataTable = CType(cboTransactionHead.DataSource, DataTable)
                For i As Integer = 1 To 31
                    dgvCmbCol = New DataGridViewComboBoxColumn
                    strTemp = "column" & i.ToString
                    dgvGrid.Columns.Add(dgvCmbCol)
                    dgvCmbCol.Name = strTemp
                    dgvCmbCol.HeaderText = i.ToString
                    dgvCmbCol.ValueMember = "tranheadunkid"
                    dgvCmbCol.DisplayMember = "code"
                    dgvCmbCol.DataSource = dTable.Copy
                    dgvCmbCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dgvCmbCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    dgvCmbCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    dgvCmbCol.DefaultCellStyle.BackColor = Color.AliceBlue
                    dgvCmbCol.HeaderCell.Style.Font = New Font(dgvGrid.Font, FontStyle.Bold)
                    dgvCmbCol.Width = 50
                    dgvCmbCol.DropDownWidth = 150
                    dgvCmbCol.Tag = i.ToString
                    dgvCmbCol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    'S.SANDEEP [ 05 AUG 2014 ] -- START
                    dgvCmbCol.ReadOnly = True
                    'S.SANDEEP [ 05 AUG 2014 ] -- END
                    If mdicDGVColumns.ContainsKey(iCnt) = False Then
                        mdicDGVColumns.Add(iCnt, dgvCmbCol.Name)
                    End If
                    iCnt += 1
                    mintLoopEndIdx += 1
                Next

            End If


            strTemp = "periodid"
            dgvGrid.Columns.Add(strTemp, strTemp)
            dgvCol = dgvGrid.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.Width = 100

            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            strTemp = "fmonth"
            dgvGrid.Columns.Add(strTemp, strTemp)
            dgvCol = dgvGrid.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.Width = 100
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If


            strTemp = "fyear"
            dgvGrid.Columns.Add(strTemp, strTemp)
            dgvCol = dgvGrid.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.Width = 100
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            strTemp = "statusid"
            dgvGrid.Columns.Add(strTemp, strTemp)
            dgvCol = dgvGrid.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.Width = 100
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            strTemp = "eday"
            dgvGrid.Columns.Add(strTemp, strTemp)
            dgvCol = dgvGrid.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvGrid.Font, FontStyle.Bold)
            dgvCol.Width = 100
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If


            dgvGrid.AutoGenerateColumns = False

            For i As Integer = 0 To dgvGrid.Columns.Count - 1
                For j As Integer = i To i
                    If j >= 1 Then
                        dgvGrid.Columns(mdicDGVColumns(i)).DataPropertyName = mdtTran.Columns(mdicDGVColumns(i)).ColumnName
                    Else
                        dgvGrid.Columns(mdicDGVColumns(i)).DataPropertyName = mdtTran.Columns(j).ColumnName
                    End If
                Next
            Next

            dgvGrid.DataSource = mdtTran

            'Pinkal (18-May-2015) -- Start
            'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
            Dim xDate As Date = dtpDate1.Value.Date
            'Pinkal (18-May-2015) -- End

            For Each dgvRow As DataGridViewRow In dgvGrid.Rows
                'S.SANDEEP [ 10 MAR 2014 ] -- START
                ' If CInt(cboPeriod.SelectedValue) > 0 Then

                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA

                'Dim iRow() As DataRow = mdtTran.Select("imonth = '" & dtpDate1.Value.Date.Month & "'")
                'If iRow.Length > 0 Then

                'Pinkal (4-Sep-2014) -- End

                'Pinkal (18-May-2015) -- Start
                'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                For Each dgcol As DataGridViewColumn In dgvGrid.Columns
                    If dgcol.Name.StartsWith("column") Then
                        Dim xColDate As String = ""
                        If xDate <= dtpDate2.Value.Date Then
                            xColDate = dgvRow.Cells("fyear").Value.ToString & dgvRow.Cells("fmonth").Value.ToString & Format(CInt(dgcol.Tag), "0#").ToString
                            If xColDate.ToString = eZeeDate.convertDate(xDate).ToString Then
                                If dgcol.Name.StartsWith("column") Then
                                    If xColDate <= eZeeDate.convertDate(dtpDate2.Value.Date) Then
                                        dgvRow.Cells(dgcol.Index).ReadOnly = False
                                    End If
                                End If
                                xDate = xDate.AddDays(1)
                            Else
                                dgvRow.Cells(dgcol.Index).ReadOnly = True
                            End If
                        End If
                    End If

                    'Pinkal (18-May-2015) -- End


                    'If dgvRow.Cells("fmonth").Value.ToString = Format(dtpDate1.Value.Date.Month, "0#") Then
                    '    If dgcol.Name.StartsWith("column") Then
                    '        If CInt(dgcol.Name.Replace("column", "")) < dtpDate1.Value.Date.Day Then
                    '            dgvRow.Cells(dgcol.Index).ReadOnly = True
                    '        End If
                    '    End If
                    'Else
                    '    dgvRow.Cells(dgcol.Index).ReadOnly = True
                    'End If
                Next

                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                'End If
                'Pinkal (4-Sep-2014) -- End

                'End If

                'Pinkal (18-May-2015) -- Start
                'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                'If CInt(dgvRow.Cells("statusid").Value) = enStatusType.Close Then
                '    dgvRow.ReadOnly = True
                'End If
                ''S.SANDEEP [ 10 MAR 2014 ] -- END
                'For Each dgCell As DataGridViewCell In dgvRow.Cells
                '    If dgCell.ColumnIndex > 0 Then
                '        If dgCell.ColumnIndex > CInt(dgvRow.Cells("eday").Value) Then
                '            dgCell.ReadOnly = True
                '        End If
                '    End If
                'Next

                'Pinkal (18-May-2015) -- End

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateDataGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_UoM(ByVal iUoMTypId As Integer)
        Dim dCombo As New DataSet
        Dim objUoM As New clsUnitMeasure_Master
        Try
            dCombo = objUoM.getComboList("List", True, iUoMTypId)
            With cboMeasures
                .ValueMember = "measureunkid"
                .DisplayMember = "name"
                .DataSource = dCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillUoM", mstrModuleName)
        Finally
            dCombo = Nothing : objUoM = Nothing
        End Try
    End Sub

    Private Sub Fill_Combo()
        Dim dCombo As New DataSet
        Dim objUoM As New clsUnitMeasure_Master
        Dim objTHead As New clsTransactionHead
        Dim objPMaster As New clscommom_period_Tran
        Try
            dCombo = objUoM.getComboList("List", True)
            With cboMeasures
                .ValueMember = "measureunkid"
                .DisplayMember = "name"
                .DataSource = dCombo.Tables(0)
                .SelectedValue = 0
            End With

            Dim dtTable As DataTable
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dCombo = objTHead.getComboList("List", True, enTranHeadType.Informational)
            dCombo = objTHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.Informational)
            'Sohail (21 Aug 2015) -- End
            Dim sFilter As String = String.Empty
            sFilter = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION & "," & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & "," & enCalcType.TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_CASH_BENEFIT_TOTAL
            dtTable = New DataView(dCombo.Tables("List"), "calctype_id NOT IN(" & sFilter & ")", "tranheadunkid", DataViewRowState.CurrentRows).ToTable
            With cboTransactionHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged

            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            'dCombo = objPMaster.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dCombo = objPMaster.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, 0)
            dCombo = objPMaster.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, 0)
            'Sohail (21 Aug 2015) -- End
            'Pinkal (4-Sep-2014) -- End

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dCombo.Tables(0)
                .SelectedValue = 0
            End With
            AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged



            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA

            dCombo = objActRate.GetComboForMode("List", False)
            With cboMode
                .ValueMember = "modeid"
                .DisplayMember = "name"
                .DataSource = dCombo.Tables(0)
                .SelectedIndex = 0
            End With
            'Pinkal (4-Sep-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dCombo = Nothing : objUoM = Nothing : objTHead = Nothing
        End Try
    End Sub

    Private Sub Fill_Activity_Grid(ByVal iUoMTypId As Integer)
        Dim objActivity As New clsActivity_Master
        Dim dsList As New DataSet
        Try
            'RemoveHandler lvActivityList.ItemChecked, AddressOf lvActivityList_ItemChecked
            'dsList = objActivity.getGridList("List", CInt(cboMeasures.SelectedValue), iUoMTypId)
            'lvActivityList.Items.Clear()
            'For Each dRow As DataRow In dsList.Tables("List").Rows
            '    Dim lvItem As New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.SubItems.Add(dRow.Item("code").ToString)
            '    lvItem.SubItems.Add(dRow.Item("name").ToString)
            '    lvItem.Tag = dRow.Item("activityunkid").ToString

            '    lvActivityList.Items.Add(lvItem)
            'Next
            'If lvActivityList.Items.Count > 14 Then
            '    colhName.Width = 150 - 20
            'Else
            '    colhName.Width = 150
            'End If
            'AddHandler lvActivityList.ItemChecked, AddressOf lvActivityList_ItemChecked
            ''mdtActivity = dsList.Tables(0)
            ''dvwActivity = mdtActivity.DefaultView
            ''dgvActivity.AutoGenerateColumns = False
            ''objdgcolhECheck.DataPropertyName = "ischeck"
            ''dgcolhAcode.DataPropertyName = "code"
            ''dgcolhName.DataPropertyName = "name"
            ''objdgcolhActivityId.DataPropertyName = "activityunkid"
            ''dgvActivity.DataSource = dvwActivity

            dsList = objActivity.getGridList("List", CInt(cboMeasures.SelectedValue), iUoMTypId)


            'Pinkal (23-Sep-2014) -- Start
            'Enhancement -  PAY_A CHANGES IN PPA
            '  mdtActivity = dsList.Tables("List").Copy
            If CInt(cboMode.SelectedValue) = enPPAMode.RATE Then
                mdtActivity = New DataView(dsList.Tables(0).Copy(), "", "", DataViewRowState.CurrentRows).ToTable
            ElseIf CInt(cboMode.SelectedValue) = enPPAMode.OT Then
                mdtActivity = New DataView(dsList.Tables(0).Copy(), "is_ot_activity = 1", "", DataViewRowState.CurrentRows).ToTable
            ElseIf CInt(cboMode.SelectedValue) = enPPAMode.PH Then
                mdtActivity = New DataView(dsList.Tables(0).Copy(), "is_ot_activity = 1", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Pinkal (23-Sep-2014) -- End


            dtViewActivity = mdtActivity.DefaultView
            dgvActivityList.AutoGenerateColumns = False



            dgvActivityList.DataSource = dtViewActivity
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Activity_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Grid_Data(ByVal sGridName As String, ByVal iPeriods As String)
        Try
            Dim dtRow() As DataRow = mdtTable.Select("periodid IN(" & iPeriods & ") AND statusid = '" & enStatusType.Open & "'", "imonth ASC")

            If sGridName.ToUpper = dgvNF.Name.ToUpper Then
                If dtRow.Length > 0 Then
                    Dim dtDates As DataTable = objActRate.Get_Date_Range(eZeeDate.convertDate(dtpDate1.Value.Date), eZeeDate.convertDate(dtpDate2.Value.Date))
                    For i As Integer = 0 To dtRow.Length - 1
                        Dim idx As Integer = mdtTable.Rows.IndexOf(dtRow(i))
                        'S.SANDEEP [ 07 MAR 2014 ] -- START
                        'Dim dTemp() As DataRow = dtDates.Select("iMonth = '" & CInt(dtRow(i).Item("fmonth")) & "'")
                        'Pinkal (18-May-2015) -- Start
                        'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                        ' Dim dTemp() As DataRow = dtDates.Select("iMonth = '" & CInt(dtRow(i).Item("imonth")) & "'")
                        'S.SANDEEP [ 07 MAR 2014 ] -- END
                        Dim dTemp() As DataRow = dtDates.Select("iDate >= '" & CInt(dtRow(i).Item("PStartDate")) & "' AND iDate <= '" & CInt(dtRow(i).Item("PEndDate")) & "'")
                        'Pinkal (18-May-2015) -- End


                        For j As Integer = 0 To dTemp.Length - 1

                            'Pinkal (18-May-2015) -- Start
                            'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                            Dim index As Integer = 0
                            Dim xRow() As DataRow = CType(dgvNF.DataSource, DataTable).Select("imonth = " & dTemp(j).Item("imonth").ToString())
                            If xRow.Length > 0 Then
                                index = CType(dgvNF.DataSource, DataTable).Rows.IndexOf(xRow(0))
                            Else
                                index = idx
                            End If
                            If dgvNF.Rows(index).Cells(CInt(dTemp(j).Item("iDay"))).ReadOnly = False Then
                                dgvNF.Rows(index).Cells(CInt(dTemp(j).Item("iDay"))).Value = Format(txtRate.Decimal, GUI.fmtCurrency)
                            End If
                            If j = dTemp.Length - 1 Then mdtTable.Rows(index).Item("ischange") = True
                            mdtTable.AcceptChanges()
                            'Pinkal (18-May-2015) -- End
                        Next
                    Next
                End If
            ElseIf sGridName.ToUpper = dgvFB.Name.ToUpper Then
                If dtRow.Length > 0 Then
                    Dim dtDates As DataTable = objActRate.Get_Date_Range(eZeeDate.convertDate(dtpDate1.Value.Date), eZeeDate.convertDate(dtpDate2.Value.Date))
                    For i As Integer = 0 To dtRow.Length - 1
                        Dim idx As Integer = mdtTable.Rows.IndexOf(dtRow(i))
                        'S.SANDEEP [ 07 MAR 2014 ] -- START
                        'Dim dTemp() As DataRow = dtDates.Select("iMonth = '" & CInt(dtRow(i).Item("fmonth")) & "'")


                        'Pinkal (18-May-2015) -- Start
                        'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                        ' Dim dTemp() As DataRow = dtDates.Select("iMonth = '" & CInt(dtRow(i).Item("imonth")) & "'")
                        'S.SANDEEP [ 07 MAR 2014 ] -- END
                        Dim dTemp() As DataRow = dtDates.Select("iDate >= '" & CInt(dtRow(i).Item("PStartDate")) & "' AND iDate <= '" & CInt(dtRow(i).Item("PEndDate")) & "'")
                        'Pinkal (18-May-2015) -- End

                        For j As Integer = 0 To dTemp.Length - 1

                            'Pinkal (18-May-2015) -- Start
                            'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                            Dim index As Integer = 0
                            Dim xRow() As DataRow = CType(dgvFB.DataSource, DataTable).Select("imonth = " & dTemp(j).Item("imonth").ToString())
                            If xRow.Length > 0 Then
                                index = CType(dgvFB.DataSource, DataTable).Rows.IndexOf(xRow(0))
                            Else
                                index = idx
                            End If
                            If dgvFB.Rows(index).Cells(CInt(dTemp(j).Item("iDay"))).ReadOnly = False Then
                                Dim cb As DataGridViewComboBoxCell = TryCast(dgvFB.Rows(index).Cells(CInt(dTemp(j).Item("iDay"))), DataGridViewComboBoxCell)
                                If cb IsNot Nothing Then
                                    cb.Value = cboTransactionHead.SelectedValue
                                End If
                            End If
                            'Pinkal (18-May-2015) -- End

                            If j = dTemp.Length - 1 Then
                                mdtTable.Rows(idx).Item("ischange") = True
                                mdtTable.AcceptChanges()
                            End If
                        Next
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Grid_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid_Data() As Boolean
        Try
            Dim dTemp() As DataRow = Nothing
            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dTemp = mdtActivity.Select("IsCheck=True")

            If mdtActivity Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Activity is mandatory information. Please check atleast one activity in order to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            dTemp = mdtActivity.Select("ischeck=True")
            'S.SANDEEP [ 20 JULY 2013 ] -- END
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Activity is mandatory information. Please check atleast one activity in order to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            dTemp = mdtTable.Select("ischange=True")
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Activity Rates are mandatory information. Please set Rates in order to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid_Data", mstrModuleName)
        Finally
        End Try
    End Function


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    Private Sub ControlEnable(ByVal blnFlag As Boolean)
        Try

            lblFromDate.Enabled = blnFlag
            dtpDate1.Enabled = blnFlag
            lblTo.Enabled = blnFlag
            dtpDate2.Enabled = blnFlag
            If blnFlag Then
                If radNF.Checked Then
                    lblRate.Enabled = blnFlag
                    txtRate.Enabled = blnFlag
                    lblHeads.Enabled = Not blnFlag
                    cboTransactionHead.Enabled = Not blnFlag
                    objbtnSearchHeads.Enabled = Not blnFlag
                ElseIf radFB.Checked Then
                    lblRate.Enabled = Not blnFlag
                    txtRate.Enabled = Not blnFlag
                    lblHeads.Enabled = blnFlag
                    cboTransactionHead.Enabled = blnFlag
                    objbtnSearchHeads.Enabled = blnFlag
                End If
            Else
                lblRate.Enabled = False
                txtRate.Enabled = False
                lblHeads.Enabled = False
                cboTransactionHead.Enabled = False
                objbtnSearchHeads.Enabled = False
            End If

            btnPost.Enabled = blnFlag
            mblnIsPeriodOpen = blnFlag
        Catch ex As Exception
            DisplayError.Show("ControlEnable", ex.Message, "ControlEnable", mstrModuleName)
        End Try
    End Sub

    Private Sub GetActivityRate()
        Try

            If mdtActivity IsNot Nothing Then

                Dim dTemp() As DataRow = Nothing
                dTemp = mdtActivity.Select("IsCheck=True")
                If dTemp.Length > 1 Then

                    If mblnIsPeriodOpen Then
                        mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)), CInt(cboMode.SelectedValue))
                    Else

                        If dgvActivityList.SelectedRows.Count > 0 Then
                            mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)), CInt(cboMode.SelectedValue), CInt(dgvActivityList.SelectedRows(0).Cells(objdgcolhMeasureId.Index).Value), "", , , CInt(cboPeriod.SelectedValue))
                        Else
                            mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)), CInt(cboMode.SelectedValue), 0, "", , , CInt(cboPeriod.SelectedValue))
                        End If

                    End If

                Else

                    If mblnIsPeriodOpen Then
                        If dgvActivityList.SelectedRows.Count > 0 Then
                            mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)), CInt(cboMode.SelectedValue), CInt(dgvActivityList.SelectedRows(0).Cells(objdgcolhMeasureId.Index).Value))
                        Else
                            mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)), CInt(cboMode.SelectedValue), 0)
                        End If
                    Else
                        If dgvActivityList.SelectedRows.Count > 0 Then
                            mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)), CInt(cboMode.SelectedValue), CInt(dgvActivityList.SelectedRows(0).Cells(objdgcolhMeasureId.Index).Value), "", , , CInt(cboPeriod.SelectedValue))
                        Else
                            mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)), CInt(cboMode.SelectedValue), 0, "", , , CInt(cboPeriod.SelectedValue))
                        End If

                    End If

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetActivityRate", mstrModuleName)
        End Try
    End Sub

    'Pinkal (4-Sep-2014) -- End


#End Region

#Region " Form's Events "

    Private Sub frmSet_Activity_Rate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objActRate = New clsActivity_Rates
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Fill_Combo()

            radNF.Checked = True

            sCommonMsg = Language.getMessage(mstrModuleName, 1, "There are some unsaved changes in the grid, And will not be saved if you select the new UoM Type.") & vbCrLf & _
                                            Language.getMessage(mstrModuleName, 11, "Do you wish to continue?")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSet_Activity_Rate_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Try
            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dTemp() As DataRow = Nothing
            If mdtActivity Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Activity is mandatory information. Please check atleast one activity in order to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mdtActivity.AcceptChanges()
            dTemp = mdtActivity.Select("ischeck = True")
            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Activity is mandatory information. Please check atleast one activity in order to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Period is mandatory information. Please select period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If radNF.Checked = True Then
                If txtRate.Decimal <= 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you have not entered any rate for posting for selected dates. This will post the rates as '0' for the selected dates and activity(s).") & vbCrLf & _
                                                           Language.getMessage(mstrModuleName, 11, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
                End If
            ElseIf radFB.Checked = True Then
                If CInt(cboTransactionHead.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Transaction Head is mandatory information. Please select Transaction Head in order to Post."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'S.SANDEEP [ 20 JULY 2013 ] -- END

            If dtpDate1.Value.Date > dtpDate2.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "From Date cannot be less than To Date. Please set the correct Dates to post the rates."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim iPeriods As String = objActRate.Get_CSV_PeriodIds("List", dtpDate1.Value.Date, dtpDate2.Value.Date, FinancialYear._Object._YearUnkid)
            If iPeriods.Trim.Length <= 0 Then
                Exit Sub
            End If

            If radNF.Checked = True Then
                Call Set_Grid_Data(dgvNF.Name, iPeriods)
            ElseIf radFB.Checked = True Then
                Call Set_Grid_Data(dgvFB.Name, iPeriods)
            End If
            'S.SANDEEP [ 10 MAR 2014 ] -- START
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Call GenerateDataGrid(CType(IIf(radFB.Checked = True, dgvFB, dgvNF), DataGridView), mdtTable, CStr(IIf(radFB.Checked = True, radFB.Name, radNF.Name)))
            End If
            'S.SANDEEP [ 10 MAR 2014 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPost_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Is_Valid_Data() = False Then Exit Sub
            Dim sActivity As String = String.Empty
            'Dim selectedTags As List(Of String) = lvActivityList.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).Distinct.ToList()
            Dim selectedTags As List(Of String) = (From p In mdtActivity.AsEnumerable() Where p.Field(Of Boolean)("IsCheck") = True Select (p.Item("activityunkid").ToString)).ToList
            sActivity = String.Join(", ", selectedTags.ToArray())

            Dim objActTran As New clsPayActivity_Tran
            Dim dsList As New DataSet
            dsList = objActTran.GetList("List", , , sActivity, dtpDate1.Value.Date, dtpDate2.Value.Date, True)
            objActTran = Nothing

            If dsList.Tables(0).Rows.Count > 0 Then
                'Sohail (16 Aug 2014) -- Start
                'Enhancement - To update rate in already posted activities before payroll is processed for last date of selected period.
                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There are some transaction already posted to payroll for the selected activity(s) and date range.") & vbCrLf & _
                '                                       Language.getMessage(mstrModuleName, 13, "As a result, Posted transaction rate for the selected date range will not be considered for updation.") & vbCrLf & _
                '                                       Language.getMessage(mstrModuleName, 11, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                '    Exit Sub
                'End If
                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "There are some transaction(s) posted with the old rate for the selected activity(s) and date range.") & vbCrLf & _
                '                                        Language.getMessage(mstrModuleName, 14, "As a result, Unposted transaction(s) rate for the selected date range will be update.") & vbCrLf & _
                '                                       Language.getMessage(mstrModuleName, 11, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then

                'End If
                Dim lstDistEmp As List(Of String) = (From p In dsList.Tables(0).AsEnumerable() Select (p.Item("employeeunkid").ToString)).Distinct.ToList()
                Dim strEmpIDs As String = ""
                strEmpIDs = String.Join(",", lstDistEmp.ToArray)
                Dim objTnA As New clsTnALeaveTran
                If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpIDs, eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString), enModuleReference.Payroll) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, There are some transaction already posted to payroll for the selected activity(s) and date range and Payroll is processed for the last of the selected period. Please void payroll process first to change rates for selected period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, There are some transaction already posted to payroll for the selected activity(s) and date range and Payroll is processed for the last of the selected period. Please void payroll process first to change rates for selected period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 16, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Sub
                End If
                objTnA = Nothing

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There are some transaction already posted to payroll for the selected activity(s) and date range.") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 13, "As a result, Posted transaction rate for the selected date range will be considered for updation for already posted activities.") & vbCrLf & _
                                                        Language.getMessage(mstrModuleName, 11, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
                'Sohail (16 Aug 2014) -- End


            End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objActRate.Insert_Update_Delete(mdtTable, User._Object._Userunkid, sActivity, CBool(IIf(radFB.Checked = True, True, False))) Then
            If objActRate.Insert_Update_Delete(mdtTable, User._Object._Userunkid, sActivity, CBool(IIf(radFB.Checked = True, True, False)), ConfigParameter._Object._CurrentDateAndTime) Then
                'Shani(24-Aug-2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Rates Posted successfully."), enMsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchMeasures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMeasures.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboMeasures.ValueMember
                .DisplayMember = cboMeasures.DisplayMember
                .DataSource = CType(cboMeasures.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If frm.DisplayDialog Then
                cboMeasures.SelectedValue = frm.SelectedValue
                cboMeasures.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMeasures_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchHeads_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchHeads.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboTransactionHead.ValueMember
                .DisplayMember = cboTransactionHead.DisplayMember
                .DataSource = CType(cboTransactionHead.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If frm.DisplayDialog Then
                cboTransactionHead.SelectedValue = frm.SelectedValue
                cboTransactionHead.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchHeads_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub dgvFB_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFB.CellEndEdit
        mdtTable.Rows(e.RowIndex).Item("ischange") = True
    End Sub

    Private Sub dgvFB_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvFB.DataError

    End Sub

    Private Sub dgvNF_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNF.CellEndEdit
        mdtTable.Rows(e.RowIndex).Item("ischange") = True
    End Sub

    Private Sub dgvNF_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvNF.EditingControlShowing
        Try
            If ((Me.dgvNF.CurrentCell.ColumnIndex > 0 And Me.dgvNF.CurrentCell.ColumnIndex <= mintLoopEndIdx)) And Not e.Control Is Nothing Then
                Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                AddHandler txt.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvNF_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvNF_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNF.CellEnter
        Try
            If e.ColumnIndex > 0 Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub radNF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radNF.CheckedChanged
        Try
            If radNF.Checked = True Then
                If mdtTable IsNot Nothing Then
                    Dim dTemp() As DataRow = mdtTable.Select("ischange = true")
                    If dTemp.Length > 0 Then
                        If eZeeMsgBox.Show(sCommonMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            RemoveHandler radNF.CheckedChanged, AddressOf radNF_CheckedChanged
                            radNF.Checked = True
                            AddHandler radNF.CheckedChanged, AddressOf radNF_CheckedChanged
                            Exit Sub
                        End If
                    End If
                End If
                cboTransactionHead.SelectedValue = 0 : cboTransactionHead.Enabled = False : lblHeads.Enabled = False : objbtnSearchHeads.Enabled = False
                txtRate.Enabled = True : lblRate.Enabled = True
                dgvNF.Visible = True : dgvFB.Visible = False
                Fill_UoM(1)

                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA

                'mdtTable = objActRate.Fill_Data("New", False)
                mdtTable = objActRate.Fill_Data("New", False, CInt(cboMode.SelectedValue))

                If mblnIsPeriodOpen Then
                    ControlEnable(True)
                Else
                    ControlEnable(False)
                End If
                'Pinkal (4-Sep-2014) -- End

                Call GenerateDataGrid(dgvNF, mdtTable, radNF.Name)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radNF_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radFB_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radFB.CheckedChanged
        Try
            If radFB.Checked = True Then
                If mdtTable IsNot Nothing Then
                    Dim dTemp() As DataRow = mdtTable.Select("ischange = true")
                    If dTemp.Length > 0 Then
                        If eZeeMsgBox.Show(sCommonMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            RemoveHandler radFB.CheckedChanged, AddressOf radFB_CheckedChanged
                            radFB.Checked = True
                            AddHandler radFB.CheckedChanged, AddressOf radFB_CheckedChanged
                            Exit Sub
                        End If
                    End If
                End If
                txtRate.Enabled = False : lblRate.Enabled = False : txtRate.Text = ""
                cboTransactionHead.Enabled = True : lblHeads.Enabled = True : objbtnSearchHeads.Enabled = True
                dgvNF.Visible = False : dgvFB.Visible = True
                Fill_UoM(2)


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                ' mdtTable = objActRate.Fill_Data("New", True)
                mdtTable = objActRate.Fill_Data("New", True, CInt(cboMode.SelectedValue))
                If mblnIsPeriodOpen Then
                    ControlEnable(True)
                Else
                    ControlEnable(False)
                End If
                'Pinkal (4-Sep-2014) -- End

                Call GenerateDataGrid(dgvFB, mdtTable, radFB.Name)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radFB_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboMeasures_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMeasures.SelectedIndexChanged
        Try
            If CInt(cboMeasures.SelectedValue) > 0 Then
                Call Fill_Activity_Grid(CInt(IIf(radNF.Checked = True, 1, 2)))
            Else
                dgvActivityList.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMeasures_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvFB_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFB.CellEnter
        Try
            If e.ColumnIndex > 0 Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvFB_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvActivityList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvActivityList.CellContentClick, dgvActivityList.CellContentDoubleClick
        Try
            If dgvActivityList.SelectedRows.Count > 0 Then

                If Me.dgvActivityList.IsCurrentCellDirty Then
                    Me.dgvActivityList.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                mdtActivity.AcceptChanges()

                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA

                'Dim dTemp() As DataRow = Nothing
                'dTemp = mdtActivity.Select("IsCheck=True")
                'If dTemp.Length > 1 Then
                '    mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)))
                'Else
                '    mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)), CInt(dgvActivityList.SelectedRows(0).Cells(objdgcolhMeasureId.Index).Value))
                'End If
                GetActivityRate()
                'Pinkal (4-Sep-2014) -- End
                Call GenerateDataGrid(CType(IIf(radFB.Checked = True, dgvFB, dgvNF), DataGridView), mdtTable, CStr(IIf(radFB.Checked = True, radFB.Name, radNF.Name)))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvActivityList_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    Private Sub dgvActivityList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvActivityList.SelectionChanged
        Try
            If dgvActivityList.SelectedRows.Count > 0 Then
                GetActivityRate()
                Call GenerateDataGrid(CType(IIf(radFB.Checked = True, dgvFB, dgvNF), DataGridView), mdtTable, CStr(IIf(radFB.Checked = True, radFB.Name, radNF.Name)))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvActivityList_SelectionChanged", mstrModuleName)
        End Try
    End Sub


    Private Sub cboMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            If mdtActivity IsNot Nothing Then

                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA
                Fill_Activity_Grid(CInt(cboMeasures.SelectedValue))
                'Pinkal (23-Sep-2014) -- End
                GetActivityRate()
                Call GenerateDataGrid(CType(IIf(radFB.Checked = True, dgvFB, dgvNF), DataGridView), mdtTable, CStr(IIf(radFB.Checked = True, radFB.Name, radNF.Name)))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (4-Sep-2014) -- End


    Private Sub txtSearchActivity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchActivity.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchActivity.Text.Trim.Length > 0 Then
                strSearch = dgcolhMeasureName.DataPropertyName & " LIKE '%" & txtSearchActivity.Text & "%' OR " & _
                            dgcolhMeasureCode.DataPropertyName & " LIKE '%" & txtSearchActivity.Text & "%'"
                'objchkSelectAll.Checked = False
                'objchkSelectAll.Enabled = False
            Else
                'objchkSelectAll.Enabled = True
            End If
            If CInt(cboMeasures.SelectedValue) <= 0 Then
                strSearch = "1=2"
            End If
            dtViewActivity.RowFilter = strSearch
            'Sohail (16 Jul 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchActivity_TextChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub lvActivityList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvActivityList.ItemChecked
    '    Try
    '        RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
    '        If lvActivityList.CheckedItems.Count > 1 Then
    '            mdtTable = objActRate.Fill_Data("New", CBool(IIf(radFB.Checked = True, True, False)))
    '            Call GenerateDataGrid(CType(IIf(radFB.Checked = True, dgvFB, dgvNF), DataGridView), mdtTable, CStr(IIf(radFB.Checked = True, radFB.Name, radNF.Name)))
    '        End If
    '        If lvActivityList.CheckedItems.Count <= 0 Then
    '            objchkSelectAll.CheckState = CheckState.Unchecked
    '        ElseIf lvActivityList.CheckedItems.Count < lvActivityList.Items.Count Then
    '            objchkSelectAll.CheckState = CheckState.Indeterminate
    '        ElseIf lvActivityList.CheckedItems.Count = lvActivityList.Items.Count Then
    '            objchkSelectAll.CheckState = CheckState.Checked
    '        End If
    '        AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvActivityList_ItemChecked", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            For Each dr As DataRowView In dtViewActivity
                dr.Item("ischeck") = CBool(objchkSelectAll.CheckState)
            Next
            dgvActivityList.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            dtpDate1.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpDate1.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date

            dtpDate2.MinDate = dtpDate1.MinDate
            dtpDate2.MaxDate = dtpDate1.MaxDate

            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim oPeriod As New clscommom_period_Tran

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'oPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                oPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End

                dtpDate1.MinDate = oPeriod._Start_Date
                dtpDate1.MaxDate = oPeriod._End_Date

                'S.SANDEEP [ 20 JULY 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dtpDate1.Value = oPeriod._Start_Date
                'S.SANDEEP [ 20 JULY 2013 ] -- END


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA

                If oPeriod._Statusid = enStatusType.Close Then
                    ControlEnable(False)
                ElseIf oPeriod._Statusid = enStatusType.Open Then
                    ControlEnable(True)
                End If

                'Pinkal (4-Sep-2014) -- End

                'Pinkal (18-May-2015) -- Start
                'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                dtpDate2.Value = oPeriod._End_Date
                'Pinkal (18-May-2015) -- End


                oPeriod = Nothing
            Else
                dtpDate1.MinDate = FinancialYear._Object._Database_Start_Date
                dtpDate1.MaxDate = FinancialYear._Object._Database_End_Date
            End If

            dtpDate2.MinDate = dtpDate1.MinDate
            dtpDate2.MaxDate = dtpDate1.MaxDate

            'S.SANDEEP [ 10 MAR 2014 ] -- START
            'If CInt(cboPeriod.SelectedValue) > 0 Then

            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            GetActivityRate()
            'Pinkal (4-Sep-2014) -- End

            Call GenerateDataGrid(CType(IIf(radFB.Checked = True, dgvFB, dgvNF), DataGridView), mdtTable, CStr(IIf(radFB.Checked = True, radFB.Name, radNF.Name)))
            'End If
            'S.SANDEEP [ 10 MAR 2014 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region






    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbRates.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRates.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnPost.GradientBackColor = GUI._ButttonBackColor
            Me.btnPost.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.gbRates.Text = Language._Object.getCaption(Me.gbRates.Name, Me.gbRates.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eLine1.Text = Language._Object.getCaption(Me.eLine1.Name, Me.eLine1.Text)
            Me.radFB.Text = Language._Object.getCaption(Me.radFB.Name, Me.radFB.Text)
            Me.radNF.Text = Language._Object.getCaption(Me.radNF.Name, Me.radNF.Text)
            Me.lblMeasures.Text = Language._Object.getCaption(Me.lblMeasures.Name, Me.lblMeasures.Text)
            Me.elOperation.Text = Language._Object.getCaption(Me.elOperation.Name, Me.elOperation.Text)
            Me.lblRate.Text = Language._Object.getCaption(Me.lblRate.Name, Me.lblRate.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblHeads.Text = Language._Object.getCaption(Me.lblHeads.Name, Me.lblHeads.Text)
            Me.btnPost.Text = Language._Object.getCaption(Me.btnPost.Name, Me.btnPost.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.dgcolhMeasureCode.HeaderText = Language._Object.getCaption(Me.dgcolhMeasureCode.Name, Me.dgcolhMeasureCode.HeaderText)
            Me.dgcolhMeasureName.HeaderText = Language._Object.getCaption(Me.dgcolhMeasureName.Name, Me.dgcolhMeasureName.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.LblMode.Text = Language._Object.getCaption(Me.LblMode.Name, Me.LblMode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There are some unsaved changes in the grid, And will not be saved if you select the new UoM Type.")
            Language.setMessage(mstrModuleName, 2, "From Date cannot be less than To Date. Please set the correct Dates to post the rates.")
            Language.setMessage(mstrModuleName, 3, "Months")
            Language.setMessage(mstrModuleName, 4, "Activity is mandatory information. Please check atleast one activity in order to continue.")
            Language.setMessage(mstrModuleName, 5, "Activity Rates are mandatory information. Please set Rates in order to continue.")
            Language.setMessage(mstrModuleName, 6, "Rates Posted successfully.")
            Language.setMessage(mstrModuleName, 7, "There are some transaction already posted to payroll for the selected activity(s) and date range.")
            Language.setMessage(mstrModuleName, 9, "Period is mandatory information. Please select period to continue.")
            Language.setMessage(mstrModuleName, 10, "Sorry, you have not entered any rate for posting for selected dates. This will post the rates as '0' for the selected dates and activity(s).")
            Language.setMessage(mstrModuleName, 11, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 12, "Sorry, Transaction Head is mandatory information. Please select Transaction Head in order to Post.")
            Language.setMessage(mstrModuleName, 13, "As a result, Posted transaction rate for the selected date range will be considered for updation for already posted activities.")
            Language.setMessage(mstrModuleName, 15, "Sorry, There are some transaction already posted to payroll for the selected activity(s) and date range and Payroll is processed for the last of the selected period. Please void payroll process first to change rates for selected period.")
			Language.setMessage(mstrModuleName, 16, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class