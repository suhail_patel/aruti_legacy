﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmActivityList

#Region " Private Varaibles "

    Private objActivity_Master As clsActivity_Master
    Private ReadOnly mstrModuleName As String = "frmActivityList"

#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddPayActivity
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditPayActivity
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeletePayActivity
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objUMaster As New clsUnitMeasure_Master
        Dim objMMaster As New clsMasterData
        Try
            dsList = objUMaster.getComboList("List", True)
            With cboMeasures
                .ValueMember = "measureunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objMMaster.getComboListForHeadType("List")
            'Sohail (15 Oct 2014) -- Start
            'Enhancement - Include Informational heads on Pay Per Activity.
            'Dim dTab As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions & "," & enTranHeadType.Informational & ")", "Id", DataViewRowState.CurrentRows).ToTable
            Dim dTab As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions & ")", "Id", DataViewRowState.CurrentRows).ToTable
            'Sohail (15 Oct 2014) -- End
            With cboHeadType
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dTab
                .SelectedValue = 0
            End With
            'Shani [ 15 OCT 2014 ] -- START
            'Changes- Add New Activity ComboBox on this Screen- Allocate by (AnjanSir)
            dsList = objActivity_Master.getComboList("List", True)
            With cboActivity
                .ValueMember = "activityunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'Shani [ 15 OCT 2014 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objUMaster = Nothing : objMMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim strSearch As String = ""
        Try
            If User._Object.Privilege._AllowtoviewActivityList = False Then Exit Sub

            dsList = objActivity_Master.GetList("List")

            If CInt(cboMeasures.SelectedValue) > 0 Then
                strSearch &= "AND measureunkid = '" & CInt(cboMeasures.SelectedValue) & "' "
            End If

            If CInt(cboHeadType.SelectedValue) > 0 Then
                strSearch &= "AND trnheadtype_id = '" & CInt(cboHeadType.SelectedValue) & "' "
            End If

            'Shani [ 15 OCT 2014 ] -- START
            'Changes- Add New Activity ComboBox on this Screen- Allocate by (AnjanSir) 
            If CInt(cboActivity.SelectedValue) > 0 Then
                strSearch &= "AND activityunkid = '" & CInt(cboActivity.SelectedValue) & "' "
            End If
            'Shani [ 15 OCT 2014 ] -- END

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtTable = New DataView(dsList.Tables("List"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("List")
            End If

            lvActivity_List.Items.Clear()
            For Each dRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem
                lvItem.Text = dRow("code").ToString
                lvItem.SubItems.Add(dRow("name").ToString)
                lvItem.SubItems.Add(dRow("measure").ToString)
                lvItem.SubItems.Add(dRow("headtype").ToString)
                lvItem.SubItems.Add(dRow("description").ToString)
                lvItem.Tag = dRow("activityunkid")
                lvActivity_List.Items.Add(lvItem)
            Next

            If lvActivity_List.Items.Count > 12 Then
                colhDescription.Width = 165 - 18
            Else
                colhDescription.Width = 165
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmActivityList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvActivity_List.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivityList_KeyUp", mstrModuleName)
        End Try

    End Sub

    Private Sub frmActivityList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivityList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmActivityList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objActivity_Master = New clsActivity_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call FillCombo()

            If lvActivity_List.Items.Count > 0 Then lvActivity_List.Items(0).Selected = True
            lvActivity_List.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivityList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmActivityList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objActivity_Master = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsActivity_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsActivity_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmActivity_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvActivity_List.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Activity from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvActivity_List.Select()
            Exit Sub
        End If
        Dim frm As New frmActivity_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvActivity_List.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvActivity_List.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Activity from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvActivity_List.Select()
            Exit Sub
        End If
        Try
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            If objActivity_Master.isUsed(CInt(lvActivity_List.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot deactivate this activity. Reason : This activity is already linked with some transactions."), enMsgBoxStyle.Information) '?1
                lvActivity_List.Select()
                Exit Sub
            End If
            'Sohail (17 Sep 2014) -- End
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Activity?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objActivity_Master.Delete(CInt(lvActivity_List.SelectedItems(0).Tag))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objActivity_Master.Delete(FinancialYear._Object._DatabaseName, CInt(lvActivity_List.SelectedItems(0).Tag))
                objActivity_Master.Delete(FinancialYear._Object._DatabaseName, CInt(lvActivity_List.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime)
                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2015) -- End
                If objActivity_Master._Message <> "" Then
                    eZeeMsgBox.Show(objActivity_Master._Message, enMsgBoxStyle.Information)
                Else
                    lvActivity_List.SelectedItems(0).Remove()
                End If
            End If
            lvActivity_List.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboMeasures.SelectedValue = 0
            cboHeadType.SelectedValue = 0
            cboActivity.SelectedValue = 0 'Shani [ 15 OCT 2014 ] -- 'Changes- Included New Activity ComboBox on this Screen- Allocate by (AnjanSir) 
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchMeasures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMeasures.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboMeasures.ValueMember
                .DisplayMember = cboMeasures.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboMeasures.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboMeasures.SelectedValue = frm.SelectedValue
                cboMeasures.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMeasures_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Shani [ 15 OCT 2014 ] -- START
    'Changes- Add New Activity ComboBox on this Screen- Allocate by (AnjanSir) 
    Private Sub objbtnSearchActivity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchActivity.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboActivity.ValueMember
                .DisplayMember = cboActivity.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboActivity.DataSource, DataTable)
                If .DisplayDialog Then
                    cboActivity.SelectedValue = .SelectedValue
                    cboActivity.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchActivity_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Shani [ 15 OCT 2014 ] -- END


    'Pinkal (11-Feb-2015) -- Start
    'Enhancement - CREATE NEW ACTIVITY MASTER IMPORTATION.
    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim frm As New frmImport_Activity_Master
        Try
            frm.ShowDialog()
            Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (11-Feb-2015) -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.colhMeasures.Text = Language._Object.getCaption(CStr(Me.colhMeasures.Tag), Me.colhMeasures.Text)
            Me.colhTranHead.Text = Language._Object.getCaption(CStr(Me.colhTranHead.Tag), Me.colhTranHead.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblMeasures.Text = Language._Object.getCaption(Me.lblMeasures.Name, Me.lblMeasures.Text)
            Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
            Me.lblActivity.Text = Language._Object.getCaption(Me.lblActivity.Name, Me.lblActivity.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Activity from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Activity?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot deactivate this activity. Reason : This activity is already linked with some transactions.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class