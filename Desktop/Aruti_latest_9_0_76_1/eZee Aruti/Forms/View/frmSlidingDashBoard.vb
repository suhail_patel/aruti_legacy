﻿#Region " Imports "

Imports System
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Drawing.Printing
Imports System.IO
Imports System.Drawing.Drawing2D

#End Region

Public Class frmSlidingDashBoard

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSlidingDashBoard"
    Private objDash As clsDashboard_Class
    Private dsCollection As New DataSet
    Private dsOccasion As New DataSet
    Private intHRSlidingCnt As Integer = 0
    Private intLTSlidingCnt As Integer = 0
    Public Declare Auto Function AnimateWindow Lib "user32" (ByVal hwnd As IntPtr, ByVal time As Integer, ByVal flags As enAnimateWindowFlags) As Boolean
    Private mstrChartName As String = String.Empty
    Private mblnFormLoad As Boolean = False
    Private mDicLeaveSeries As New Dictionary(Of Integer, Integer)
    Private mblnIsHRCalled As Boolean = False

    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage

    Private IsHRMessageShown As Boolean = False
    Private IsPayrollMessaheShown As Boolean = False
    Private IsLeaveMessageShown As Boolean = False

    'Sohail (25 Apr 2019) -- Start
    'Enhancement - 76.1 - Performance enhancement of aruti main view.
    Private dsDates As DataSet
    Private dsEmpDatesCount As DataSet
    Private dsPendingLeaveCount As DataSet
    'Sohail (25 Apr 2019) -- End

#End Region

#Region " ENUMS "

    Public Enum enDashModule
        DSH_HUMANRESOURCE = 1
        DSH_PAYROLL = 2
        DSH_LEEAVETNA = 3
    End Enum

    Public Enum enAnimateWindowFlags
        AW_HOR_POSITIVE = &H1
        AW_HOR_NEGATIVE = &H2
        AW_VER_POSITIVE = &H4
        AW_VER_NEGATIVE = &H8
        AW_CENTER = &H10
        AW_HIDE = &H10000
        AW_ACTIVATE = &H20000
        AW_SLIDE = &H40000
        AW_BLEND = &H80000
    End Enum

    Public Enum enChartOperation
        SAVE_CHART = 1
        PREVIEW_CHART = 2
        PRINT_CHART = 3
    End Enum

#End Region

#Region " Form's Events "

    Private Sub frmSlidingDashBoard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call ControlVisibility()
            Control.CheckForIllegalCrossThreadCalls = False
            Me.SuspendLayout() 'Sohail (25 Apr 2019)
            'Pinkal (28-Feb-2014) -- Start
            'Enhancement : TRA Changes

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDash = New clsDashboard_Class(False)
            objDash = New clsDashboard_Class(FinancialYear._Object._DatabaseName, _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             FinancialYear._Object._Database_Start_Date, _
                                             FinancialYear._Object._Database_End_Date, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._CurrentDateAndTime, True, True, _
                                             User._Object.Privilege._Show_Probation_Dates, _
                                             User._Object.Privilege._Show_Suspension_Dates, _
                                             User._Object.Privilege._Show_Appointment_Dates, _
                                             User._Object.Privilege._Show_Confirmation_Dates, _
                                             User._Object.Privilege._Show_BirthDates, _
                                             User._Object.Privilege._Show_Anniversary_Dates, _
                                             User._Object.Privilege._Show_Contract_Ending_Dates, _
                                             User._Object.Privilege._Show_TodayRetirement_Dates, _
                                             User._Object.Privilege._Show_ForcastedRetirement_Dates, _
                                             User._Object.Privilege._Show_ForcastedEOC_Dates, _
                                             User._Object.Privilege._Show_ForcastedELC_Dates, _
                                             ConfigParameter._Object._Forcasted_ViewSetting, _
                                             ConfigParameter._Object._ForcastedValue, _
                                             ConfigParameter._Object._ForcastedEOC_ViewSetting, _
                                             ConfigParameter._Object._ForcastedEOCValue, _
                                             ConfigParameter._Object._ForcastedELCViewSetting, _
                                             ConfigParameter._Object._ForcastedELCValue, _
                                             ConfigParameter._Object._LeaveBalanceSetting, False, _
                                             User._Object.Privilege._AllowtoViewPendingAccrueData)
            'S.SANDEEP [12-Apr-2018] -- START {Ref#218|#ARUTI-106} [User._Object.Privilege._AllowtoViewPendingAccrueData] -- END
            'Shani(24-Aug-2015) -- End

            'Pinkal (28-Feb-2014) -- End

            Call FillCombo()
            
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'RemoveHandler tabpHR.Enter, AddressOf tabpHR_Enter
            'RemoveHandler tabpLeaveTnA.Enter, AddressOf tabpLeaveTnA_Enter
            'RemoveHandler tabpPayroll.Enter, AddressOf tabpPayroll_Enter
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objbtnHRPrevoius.Enabled = False
            objbtnLTPrevious.Enabled = False

            Call Check_Lic()


            'Pinkal (28-Feb-2014) -- Start
            'Enhancement : TRA Changes
            'Me.SuspendLayout()
            'Pinkal (28-Feb-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            mblnFormLoad = True
            Me.ResumeLayout() 'Sohail (25 Apr 2019)
        End Try
    End Sub

    Private Sub frmSlidingDashBoard_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try

            'Pinkal (28-Feb-2014) -- Start
            'Enhancement : TRA Changes

            'dsCollection = objDash._DataSetCollection
            'Call FillChartAndGrid()
            'Me.ResumeLayout()

            objbtnSummaryReset_Click(sender, e)
            'Me.ResumeLayout()

            'Pinkal (28-Feb-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSlidingDashBoard_Shown", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            dsCombo = objDash.Fill_Divison_Combo("List")
            With cboTDivision
                .ValueMember = "Id"
                .DisplayMember = "DValue"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            With cboSDivision
                .ValueMember = "Id"
                .DisplayMember = "DValue"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub FillChartAndGrid()
        Try
            objSpcTrainingCost.Dock = DockStyle.Fill : objSpcStaff.Dock = DockStyle.Fill
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            gbEmployeeDates.Dock = DockStyle.Fill
            'S.SANDEEP [ 05 MARCH 2012 ] -- END
            objSpcStaff.BringToFront()

            Call GeneRateDataGrid(enDashModule.DSH_HUMANRESOURCE, dgvStaff)
            Call GeneRateDataGrid(enDashModule.DSH_HUMANRESOURCE, dgvTrainingCosting)

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call GeneRateDataGrid(enDashModule.DSH_HUMANRESOURCE, dgvEmpDates)
            'S.SANDEEP [ 05 MARCH 2012 ] -- END



            objSpcLeave.Dock = DockStyle.Fill : objSpcLoginSummary.Dock = DockStyle.Fill
            objSpcLeave.BringToFront()
            Call GeneRateDataGrid(enDashModule.DSH_LEEAVETNA, dgvLeave)
            Call GeneRateDataGrid(enDashModule.DSH_LEEAVETNA, dgvLoginSummary)

            objSpcSalAnalysis.Dock = DockStyle.Fill
            objSpcSalAnalysis.BringToFront()
            Call GeneRateDataGrid(enDashModule.DSH_PAYROLL, dgvSalAnalysis)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillChartAndGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function GeneRateDataGrid(ByVal intDM As enDashModule, ByVal DGV As DataGridView) As Boolean
        Try
            DGV.AutoGenerateColumns = False
            Select Case intDM
                Case enDashModule.DSH_HUMANRESOURCE
                    Select Case DGV.Name.ToUpper
                        Case "DGVSTAFF"
                            Dim MonthId As Integer = Month(FinancialYear._Object._Database_Start_Date)
                            Dim LoopIndex As Integer = DateDiff(DateInterval.Month, FinancialYear._Object._Database_Start_Date, ConfigParameter._Object._CurrentDateAndTime.AddMonths(1))
                            If LoopIndex > 12 Then LoopIndex = 12
                            Dim StrMonth(11) As String
                            For i As Integer = 0 To LoopIndex - 1
                                If MonthId > 12 Then
                                    MonthId = 1
                                End If
                                StrMonth(i) = MonthName(MonthId, True)
                                MonthId += 1
                            Next

                            If StrMonth(0) IsNot Nothing Then
                            dgcolhSJan.DataPropertyName = StrMonth(0).ToString
                            dgcolhSJan.HeaderText = StrMonth(0).ToString
                                dgcolhSJan.Visible = True
                            Else
                                dgcolhSJan.Visible = False
                            End If

                            If StrMonth(1) IsNot Nothing Then
                            dgcolhSFeb.DataPropertyName = StrMonth(1).ToString
                            dgcolhSFeb.HeaderText = StrMonth(1).ToString
                                dgcolhSFeb.Visible = True
                            Else
                                dgcolhSFeb.Visible = False
                            End If


                            If StrMonth(2) IsNot Nothing Then
                            dgcolhSMar.DataPropertyName = StrMonth(2).ToString
                            dgcolhSMar.HeaderText = StrMonth(2).ToString
                                dgcolhSMar.Visible = True
                            Else
                                dgcolhSMar.Visible = False
                            End If
                           

                            If StrMonth(3) IsNot Nothing Then
                            dgcolhSApr.DataPropertyName = StrMonth(3).ToString
                            dgcolhSApr.HeaderText = StrMonth(3).ToString
                                dgcolhSApr.Visible = True
                            Else
                                dgcolhSApr.Visible = False
                            End If


                            If StrMonth(4) IsNot Nothing Then
                            dgcolhSMay.DataPropertyName = StrMonth(4).ToString
                            dgcolhSMay.HeaderText = StrMonth(4).ToString
                                dgcolhSMay.Visible = True
                            Else
                                dgcolhSMay.Visible = False
                            End If


                            If StrMonth(5) IsNot Nothing Then
                            dgcolhSJun.DataPropertyName = StrMonth(5).ToString
                            dgcolhSJun.HeaderText = StrMonth(5).ToString
                                dgcolhSJun.Visible = True
                            Else
                                dgcolhSJun.Visible = False
                            End If


                            If StrMonth(6) IsNot Nothing Then
                            dgcolhSJul.DataPropertyName = StrMonth(6).ToString
                            dgcolhSJul.HeaderText = StrMonth(6).ToString
                                dgcolhSJul.Visible = True
                            Else
                                dgcolhSJul.Visible = False
                            End If


                            If StrMonth(7) IsNot Nothing Then
                            dgcolhSAug.DataPropertyName = StrMonth(7).ToString
                            dgcolhSAug.HeaderText = StrMonth(7).ToString
                                dgcolhSAug.Visible = True
                            Else
                                dgcolhSAug.Visible = False
                            End If


                            If StrMonth(8) IsNot Nothing Then
                            dgcolhSSep.DataPropertyName = StrMonth(8).ToString
                            dgcolhSSep.HeaderText = StrMonth(8).ToString
                                dgcolhSSep.Visible = True
                            Else
                                dgcolhSSep.Visible = False
                            End If


                            If StrMonth(9) IsNot Nothing Then
                            dgcolhSOct.DataPropertyName = StrMonth(9).ToString
                            dgcolhSOct.HeaderText = StrMonth(9).ToString
                                dgcolhSOct.Visible = True
                            Else
                                dgcolhSOct.Visible = False
                            End If


                            If StrMonth(10) IsNot Nothing Then
                            dgcolhSNov.DataPropertyName = StrMonth(10).ToString
                            dgcolhSNov.HeaderText = StrMonth(10).ToString
                                dgcolhSNov.Visible = True
                            Else
                                dgcolhSNov.Visible = False
                            End If


                            If StrMonth(11) IsNot Nothing Then
                            dgcolhSDec.DataPropertyName = StrMonth(11).ToString
                            dgcolhSDec.HeaderText = StrMonth(11).ToString
                                dgcolhSDec.Visible = True
                            Else
                                dgcolhSDec.Visible = False
                            End If

                            objdgcolhViewId.DataPropertyName = "ViewId"
                            dgcolhSParticulars.DataPropertyName = "Particulars"
                            DGV.DataSource = objDash.Staff_TO_Grid

                            'S.SANDEEP [06 Jan 2016] -- START
                            DGV.Refresh()
                            'S.SANDEEP [06 Jan 2016] -- END

                            Call Fill_Staff_TuOv()

                        Case "DGVTRAININGCOSTING"
                            dgcolhAcc.DataPropertyName = "Acc"
                            dgcolhallowance.DataPropertyName = "allowance"
                            dgcolhExam.DataPropertyName = "Exam"
                            dgcolhFees.DataPropertyName = "Fees"
                            dgcolhMaterial.DataPropertyName = "Material"
                            dgcolhMeals.DataPropertyName = "Meals"
                            dgcolhMisc.DataPropertyName = "Misc"
                            dgcolhTitle.DataPropertyName = "Title"
                            dgcolhTravel.DataPropertyName = "Travel"
                            DGV.DataSource = dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper)
                            dgcolhAcc.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight : dgcolhallowance.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                            dgcolhExam.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight : dgcolhFees.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                            dgcolhMaterial.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight : dgcolhMeals.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                            dgcolhMisc.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight : dgcolhTravel.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

                            dgcolhAcc.DefaultCellStyle.Format = GUI.fmtCurrency : dgcolhallowance.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                            dgcolhExam.DefaultCellStyle.Format = GUI.fmtCurrency : dgcolhFees.DefaultCellStyle.Format = GUI.fmtCurrency
                            dgcolhMaterial.DefaultCellStyle.Format = GUI.fmtCurrency : dgcolhMeals.DefaultCellStyle.Format = GUI.fmtCurrency
                            dgcolhMisc.DefaultCellStyle.Format = GUI.fmtCurrency : dgcolhTravel.DefaultCellStyle.Format = GUI.fmtCurrency

                            Call Fill_Training_Cost()

                            'S.SANDEEP [ 05 MARCH 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                        Case "DGVEMPDATES"

                            'Sohail (25 Apr 2019) -- Start
                            'Enhancement - 76.1 - Performance enhancement of aruti main view.
                            'dgcolhECode.DataPropertyName = "ECode"
                            'dgcolhEName.DataPropertyName = "EName"
                            'dgcolhDate.DataPropertyName = "Date"
                            'objdgcolhIsGrp.DataPropertyName = "IsGrp"
                            'objdgcolhEDatesGrpId.DataPropertyName = "GrpId"
                            'objdgcolhEmpId.DataPropertyName = "EmpId"

                            'DGV.DataSource = dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.EMPLOYEE_DATES.ToString.ToUpper)

                            'Call SetGridStyle()

                            'S.SANDEEP [ 05 MARCH 2012 ] -- END
                            FlowLayoutPanel1.Visible = False
                            dsDates = objDash.GetDatesList(False)
                            dsEmpDatesCount = objDash.EmployeeDatesCount()
                            dsPendingLeaveCount = objDash.GetPeningAccrueLeaveCount()
                            'Sohail (13 Jan 2020) -- Start
                            'NMB Issue # : Don't show employee dates counts if user does not have access.
                            bdgProbation.Enabled = User._Object.Privilege._Show_Probation_Dates
                            bdgSuspension.Enabled = User._Object.Privilege._Show_Suspension_Dates
                            bdgAppointed.Enabled = User._Object.Privilege._Show_Appointment_Dates
                            bdgConfirmed.Enabled = User._Object.Privilege._Show_Confirmation_Dates
                            bdgBirthDays.Enabled = User._Object.Privilege._Show_BirthDates
                            bdgAnniversary.Enabled = User._Object.Privilege._Show_Anniversary_Dates
                            bdgContractEnded.Enabled = User._Object.Privilege._Show_Contract_Ending_Dates
                            bdgRetiredToday.Enabled = User._Object.Privilege._Show_TodayRetirement_Dates
                            bdgRetiredForcast.Enabled = User._Object.Privilege._Show_ForcastedRetirement_Dates
                            bdgEOCForcast.Enabled = User._Object.Privilege._Show_ForcastedEOC_Dates
                            bdgELCForcast.Enabled = User._Object.Privilege._Show_ForcastedELC_Dates
                            bdgPendingAccrueLeave.Enabled = User._Object.Privilege._AllowtoViewPendingAccrueData
                            'Sohail (13 Jan 2020) -- End

                            For Each dtRow As DataRow In dsDates.Tables(0).Rows

                                If CInt(dtRow.Item("ID")) = clsDashboard_Class.enEmpDates.PendingAccrueLeaves Then

                                    Dim dtFilter() As DataRow = dsPendingLeaveCount.Tables(0).Select("ID = " & CInt(dtRow.Item("ID")) & " ")

                                    bdgPendingAccrueLeave.Text_Number = dtFilter.Length
                                    bdgPendingAccrueLeave.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase)

                                Else

                                    Dim dtFilter() As DataRow = dsEmpDatesCount.Tables(0).Select("ID = " & CInt(dtRow.Item("ID")) & " ")

                                    Select Case CInt(dtRow.Item("ID"))

                                        Case clsDashboard_Class.enEmpDates.Probation_Dates
                                            'bdgProbation.Text_Number = dtFilter(0).Item("TotalCount").ToString
                                            bdgProbation.Text_Number = dtFilter.Length
                                            bdgProbation.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.Suspension_Dates
                                            bdgSuspension.Text_Number = dtFilter.Length
                                            bdgSuspension.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.Appointment_Dates
                                            bdgAppointed.Text_Number = dtFilter.Length
                                            bdgAppointed.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.Confirmation_Dates
                                            bdgConfirmed.Text_Number = dtFilter.Length
                                            bdgConfirmed.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.BirthDates
                                            bdgBirthDays.Text_Number = dtFilter.Length
                                            bdgBirthDays.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.Anniversary_Dates
                                            bdgAnniversary.Text_Number = dtFilter.Length
                                            bdgAnniversary.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.Contract_Ending_Dates
                                            bdgContractEnded.Text_Number = dtFilter.Length
                                            bdgContractEnded.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.TodayRetirement_Dates
                                            bdgRetiredToday.Text_Number = dtFilter.Length
                                            bdgRetiredToday.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.ForcastedRetirement_Dates
                                            bdgRetiredForcast.Text_Number = dtFilter.Length
                                            bdgRetiredForcast.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.ForcastedEOC_Dates
                                            bdgEOCForcast.Text_Number = dtFilter.Length
                                            bdgEOCForcast.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.ForcastedELC_Dates
                                            bdgELCForcast.Text_Number = dtFilter.Length
                                            bdgELCForcast.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                        Case clsDashboard_Class.enEmpDates.PendingAccrueLeaves
                                            bdgPendingAccrueLeave.Text_Number = dtFilter.Length
                                            bdgPendingAccrueLeave.Text_Caption = StrConv(dtRow.Item("MODE").ToString, VbStrConv.ProperCase).Replace("(S)", "(s)")

                                    End Select
                                
                                End If
                            Next
                            FlowLayoutPanel1.Visible = True
                            'Sohail (25 Apr 2019) -- End
                    End Select
                Case enDashModule.DSH_PAYROLL
                    Select Case DGV.Name.ToUpper
                        Case "DGVSALANALYSIS"
                            dgcolhSACode.DataPropertyName = "PCode"
                            dgcolhSAName.DataPropertyName = "PName"
                            dgcolhSASal.DataPropertyName = "Salary"
                            objdgcolhSAUnkid.DataPropertyName = "PId"
                            objdgcolhIsGroup.DataPropertyName = "IsGrp"
                            objdgcolhGrpId.DataPropertyName = "GrpId"
                            dgcolhSASal.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                            dgcolhSASal.DefaultCellStyle.Format = GUI.fmtCurrency
                            dgvSalAnalysis.DataSource = objDash.Salary_AnalysisToGrid
                            Call Fill_Salary_Analysis()
                    End Select
                Case enDashModule.DSH_LEEAVETNA
                    Select Case DGV.Name.ToUpper
                        Case "DGVLEAVE"

                            Dim MonthId As Integer = Month(FinancialYear._Object._Database_Start_Date)
                            Dim StrMonth(11) As String
                            For i As Integer = 0 To 11
                                If MonthId > 12 Then
                                    MonthId = 1
                                End If
                                StrMonth(i) = MonthName(MonthId, True)
                                MonthId += 1
                            Next


                            dgcolhLJan.DataPropertyName = StrMonth(0).ToString
                            dgcolhLJan.HeaderText = StrMonth(0).ToString

                            dgcolhLFeb.DataPropertyName = StrMonth(1).ToString
                            dgcolhLFeb.HeaderText = StrMonth(1).ToString

                            dgcolhLMar.DataPropertyName = StrMonth(2).ToString
                            dgcolhLMar.HeaderText = StrMonth(2).ToString

                            dgcolhLApr.DataPropertyName = StrMonth(3).ToString
                            dgcolhLApr.HeaderText = StrMonth(3).ToString

                            dgcolhLMay.DataPropertyName = StrMonth(4).ToString
                            dgcolhLMay.HeaderText = StrMonth(4).ToString

                            dgcolhLJun.DataPropertyName = StrMonth(5).ToString
                            dgcolhLJun.HeaderText = StrMonth(5).ToString

                            dgcolhLJul.DataPropertyName = StrMonth(6).ToString
                            dgcolhLJul.HeaderText = StrMonth(6).ToString

                            dgcolhLAug.DataPropertyName = StrMonth(7).ToString
                            dgcolhLAug.HeaderText = StrMonth(7).ToString

                            dgcolhLSep.DataPropertyName = StrMonth(8).ToString
                            dgcolhLSep.HeaderText = StrMonth(8).ToString

                            dgcolhLOct.DataPropertyName = StrMonth(9).ToString
                            dgcolhLOct.HeaderText = StrMonth(9).ToString

                            dgcolhLNov.DataPropertyName = StrMonth(10).ToString
                            dgcolhLNov.HeaderText = StrMonth(10).ToString

                            dgcolhLDec.DataPropertyName = StrMonth(11).ToString
                            dgcolhLDec.HeaderText = StrMonth(11).ToString

                            'dgcolhLApr.DataPropertyName = MonthName(4, True).ToString
                            'dgcolhLAug.DataPropertyName = MonthName(8, True).ToString
                            'dgcolhLDec.DataPropertyName = MonthName(12, True).ToString

                            'dgcolhLJan.DataPropertyName = MonthName(1, True).ToString
                            'dgcolhLFeb.DataPropertyName = MonthName(2, True).ToString
                            'dgcolhLMar.DataPropertyName = MonthName(3, True).ToString
                            'dgcolhLJul.DataPropertyName = MonthName(7, True).ToString
                            'dgcolhLJun.DataPropertyName = MonthName(6, True).ToString
                            'dgcolhLMay.DataPropertyName = MonthName(5, True).ToString
                            'dgcolhLNov.DataPropertyName = MonthName(11, True).ToString
                            'dgcolhLOct.DataPropertyName = MonthName(10, True).ToString
                            'dgcolhLSep.DataPropertyName = MonthName(9, True).ToString

                            dgcolhLName.DataPropertyName = "LName"
                            objdgcolhLId.DataPropertyName = "LId"
                            dgvLeave.DataSource = objDash.Leave_Grid.Tables("List")
                            mDicLeaveSeries = objDash._mDicColor
                            Call Fill_Leave_Analysis()
                        Case "DGVLOGINSUMMARY"
                            dgcolhTAbsent.DataPropertyName = "TotAbsent"
                            dgcolhTCode.DataPropertyName = "SCode"
                            dgcolhTName.DataPropertyName = "Shift"
                            dgcolhTPresent.DataPropertyName = "TotPresent"
                            objdgcolhTUnkid.DataPropertyName = "Sid"
                            dgcolhTEmployee.DataPropertyName = "TotEmp"
                            dgvLoginSummary.DataSource = dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper)
                            Call Fill_Login_Chart()
                    End Select
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GeneRateDataGrid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Static_Chart()
        Try
            For Each item In [Enum].GetValues(GetType(clsDashboard_Class.enDashBoardListIndex))
                Select Case item.ToString.ToUpper
                    Case clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper
                        Call Fill_Salary_Analysis() 'SALARY ANALYSIS
                    Case clsDashboard_Class.enDashBoardListIndex.LEAVE_LIST.ToString.ToUpper
                        Call Fill_Leave_Analysis()  'LEAVE ANALYSIS
                    Case clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper
                        Call Fill_Login_Chart()     'LOGIN SUMMARY
                    Case clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper
                        Call Fill_Training_Cost()   'TRAINING ANALYSIS
                End Select
            Next

            'dsOccasion = objDash.FillOccasion(ConfigParameter._Object._CurrentDateAndTime, "List")

            'dgvOccassion.AutoGenerateColumns = False

            'objdgcolhCheck.DataPropertyName = "IsCheck"
            'dgcolhECode.DataPropertyName = "ECode"
            'dgcolhEName.DataPropertyName = "EName"
            'dgcolhOccasion.DataPropertyName = "Occasion"
            'objdgcolhEmpId.DataPropertyName = "EmpId"

            'dgvOccassion.DataSource = dsOccasion.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Static_Chart", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Salary_Analysis()
        Try
            If dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).Rows.Count > 0 Then
                chSal_Analysis.ChartAreas(0).Visible = True
                chSal_Analysis.ChartAreas(0).RecalculateAxesScale()

                If chSal_Analysis.Legends(0).CustomItems.Count > 0 Then chSal_Analysis.Legends(0).CustomItems.Clear()

                chSal_Analysis.Series(0).Points.DataBindXY(dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).DefaultView, "PName", dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).DefaultView, "TBS")
                chSal_Analysis.Series(1).Points.DataBindXY(dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).DefaultView, "PName", dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).DefaultView, "TCS")
                chSal_Analysis.Series(2).Points.DataBindXY(dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).DefaultView, "PName", dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).DefaultView, "THS")

                chSal_Analysis.ChartAreas(0).AxisY.IsInterlaced = True

                chSal_Analysis.AntiAliasing = DataVisualization.Charting.AntiAliasingStyles.All

                chSal_Analysis.ChartAreas(0).Area3DStyle.Enable3D = False

                chSal_Analysis.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 200, "Periods")
                chSal_Analysis.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 201, "Salary")
                chSal_Analysis.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chSal_Analysis.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)

                chSal_Analysis.ChartAreas(0).AxisY.LabelAutoFitStyle = LabelAutoFitStyles.WordWrap
                chSal_Analysis.ChartAreas(0).AxisY.LabelStyle.Format = GUI.fmtCurrency
                chSal_Analysis.ChartAreas(0).AxisY.IntervalAutoMode = IntervalAutoMode.FixedCount

                chSal_Analysis.Legends(0).Docking = Docking.Bottom


                chSal_Analysis.Series(0).IsVisibleInLegend = False
                chSal_Analysis.Series(1).IsVisibleInLegend = False
                chSal_Analysis.Series(2).IsVisibleInLegend = False

                chSal_Analysis.Series(0).Color = Color.Red
                chSal_Analysis.Series(1).Color = Color.Blue
                chSal_Analysis.Series(2).Color = Color.Green


                Dim legendItem1 As New LegendItem()
                legendItem1.Name = Language.getMessage(mstrModuleName, 406, "Total Bank Salary")
                legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 1
                legendItem1.Color = Color.Red
                chSal_Analysis.Legends(0).CustomItems.Add(legendItem1)

                Dim legendItem2 As New LegendItem()
                legendItem2.Name = Language.getMessage(mstrModuleName, 407, "Total Cash Salary")
                legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 1
                legendItem2.Color = Color.Blue
                chSal_Analysis.Legends(0).CustomItems.Add(legendItem2)

                Dim legendItem3 As New LegendItem()
                legendItem3.Name = Language.getMessage(mstrModuleName, 408, "Total Hold Salary")
                legendItem3.ImageStyle = LegendImageStyle.Rectangle
                legendItem3.ShadowOffset = 1
                legendItem3.Color = Color.Green
                chSal_Analysis.Legends(0).CustomItems.Add(legendItem3)

                chSal_Analysis.Series(0).Tag = "TBS"
                chSal_Analysis.Series(1).Tag = "TCS"
                chSal_Analysis.Series(2).Tag = "THS"

                chSal_Analysis.Series(0).ChartType = SeriesChartType.Column
                chSal_Analysis.Series(1).ChartType = SeriesChartType.Column
                chSal_Analysis.Series(2).ChartType = SeriesChartType.Column

                chSal_Analysis.Series(0)("DrawingStyle") = "Cylinder"
                chSal_Analysis.Series(1)("DrawingStyle") = "Cylinder"
                chSal_Analysis.Series(2)("DrawingStyle") = "Cylinder"

                chSal_Analysis.ChartAreas(0).AxisX.MinorGrid.Enabled = False
                chSal_Analysis.ChartAreas(0).AxisX.MajorGrid.Enabled = False

                chSal_Analysis.Titles(0).Text = Language.getMessage(mstrModuleName, 203, "Salary Analysis")
                chSal_Analysis.Titles(0).TextStyle = TextStyle.Emboss
                chSal_Analysis.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)
                chSal_Analysis.Series(0).SmartLabelStyle.Enabled = True
                chSal_Analysis.Series(0)("LabelStyle") = "Bottom"

                chSal_Analysis.Series(0)("PixelPointWidth") = "50"
                chSal_Analysis.Series(1)("PixelPointWidth") = "50"
                chSal_Analysis.Series(2)("PixelPointWidth") = "50"

                chSal_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.Enabled = True
                chSal_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.BreakLineStyle = BreakLineStyle.Straight
                chSal_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.CollapsibleSpaceThreshold = 10
                chSal_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.Spacing = 2
                chSal_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.LineWidth = 1
                chSal_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.LineColor = Color.Red
            Else
                chSal_Analysis.ChartAreas(0).Visible = False
                chSal_Analysis.Legends(0).Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Salary_Analysis", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Leave_Analysis()
        Try
            If dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LEAVE_LIST.ToString.ToUpper).Rows.Count > 0 Then

                If chLeave_Analysis.Series.Count > 0 Then chLeave_Analysis.Series.Clear()

                If chLeave_Analysis.Legends.Count > 0 Then chLeave_Analysis.Legends.Clear()

                chLeave_Analysis.ChartAreas(0).Visible = True
                chLeave_Analysis.ChartAreas(0).RecalculateAxesScale()
                chLeave_Analysis.ChartAreas(0).AxisX.Interval = 1
                chLeave_Analysis.ChartAreas(0).AxisY.IsInterlaced = True
                chLeave_Analysis.ChartAreas(0).AxisY.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.FixedCount
                chLeave_Analysis.AntiAliasing = DataVisualization.Charting.AntiAliasingStyles.All
                Dim mLSeries As Series
                Dim mLegend As LegendItem
                Dim objLType As New clsleavetype_master
                chLeave_Analysis.ChartAreas(0).ShadowOffset = 4
                chLeave_Analysis.Legends.Add("Legend1")
                If mDicLeaveSeries.Count > 0 Then
                    For Each Key As Integer In mDicLeaveSeries.Keys
                        mLSeries = New Series("Series" & Key.ToString)
                        mLSeries.Color = Color.FromArgb(mDicLeaveSeries(Key))
                        mLSeries.ChartType = SeriesChartType.StackedColumn
                        mLSeries.SmartLabelStyle.Enabled = True
                        chLeave_Analysis.Series.Add(mLSeries)
                        chLeave_Analysis.Series("Series" & Key.ToString)("DrawingStyle") = "Cylinder"
                        mLSeries = Nothing
                        chLeave_Analysis.Series("Series" & Key.ToString).Points.DataBindXY(dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LEAVE_LIST.ToString.ToUpper).DefaultView, "MName", dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LEAVE_LIST.ToString.ToUpper).DefaultView, "Column" & Key)
                        chLeave_Analysis.Series("Series" & Key.ToString).ShadowOffset = 2
                        chLeave_Analysis.Series("Series" & Key.ToString)("LabelStyle") = "Bottom"
                        'chLeave_Analysis.Series("Series" & Key.ToString).IsValueShownAsLabel = True
                        chLeave_Analysis.Series("Series" & Key.ToString)("PixelPointWidth") = "20"
                        chLeave_Analysis.Series("Series" & Key.ToString).IsVisibleInLegend = False
                        chLeave_Analysis.Series("Series" & Key.ToString).Tag = Key
                        chLeave_Analysis.Series("Series" & Key.ToString).ToolTip = Language.getMessage(mstrModuleName, 409, "Leave Issused ") & " : is #VAL "

                        objLType._Leavetypeunkid = Key

                        mLegend = New LegendItem()
                        mLegend.Name = objLType._Leavename
                        mLegend.Color = Color.FromArgb(mDicLeaveSeries(Key))
                        mLegend.ImageStyle = LegendImageStyle.Rectangle
                        mLegend.ShadowOffset = 1

                        chLeave_Analysis.Legends(0).CustomItems.Add(mLegend)
                        chLeave_Analysis.Legends(0).BackColor = Color.Transparent
                    Next
                Else
                    chLeave_Analysis.ChartAreas(0).Visible = False
                    chLeave_Analysis.Legends(0).Enabled = False
                    Exit Sub
                End If
                objLType = Nothing

                For i As Integer = 0 To chLeave_Analysis.Series.Count - 1
                    For Each dp As DataPoint In chLeave_Analysis.Series(i).Points
                        If dp.YValues(0) = 0 Then
                            dp.Label = " "
                            dp.IsVisibleInLegend = False
                        End If
                    Next
                Next

                chLeave_Analysis.Titles(0).Text = Language.getMessage(mstrModuleName, 206, "Leave Analysis")
                chLeave_Analysis.Titles(0).TextStyle = TextStyle.Emboss
                chLeave_Analysis.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)
                chLeave_Analysis.Legends(0).Docking = Docking.Bottom
                chLeave_Analysis.ChartAreas(0).AxisX.LabelAutoFitStyle = LabelAutoFitStyles.WordWrap
                chLeave_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.Enabled = True
                chLeave_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.BreakLineStyle = BreakLineStyle.Straight
                chLeave_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.Spacing = 2
                chLeave_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.LineWidth = 1
                chLeave_Analysis.ChartAreas(0).AxisY.ScaleBreakStyle.LineColor = Color.Red
                chLeave_Analysis.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 204, "Months")
                chLeave_Analysis.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 205, "Total Leave")
                chLeave_Analysis.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chLeave_Analysis.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
            Else
                chLeave_Analysis.ChartAreas(0).Visible = False
                chLeave_Analysis.Legends(0).Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Leave_Analysis", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Login_Chart()
        Try
            If dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper).Rows.Count > 0 Then
                chLoginSummary.ChartAreas(0).Visible = True
                chLoginSummary.Series(0).Points.DataBindXY(dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper).DefaultView, "Shift", dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper).DefaultView, "TotPresent")
                chLoginSummary.Series(1).Points.DataBindXY(dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper).DefaultView, "Shift", dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper).DefaultView, "TotAbsent")
                chLoginSummary.Series(0).LegendText = Language.getMessage(mstrModuleName, 207, "Total Present")
                chLoginSummary.Series(1).LegendText = Language.getMessage(mstrModuleName, 208, "Total Absent")
                chLoginSummary.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 209, "Shift")
                chLoginSummary.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 210, "Total Attandance")
                chLoginSummary.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chLoginSummary.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chLoginSummary.ChartAreas(0).AxisY.IsInterlaced = True
                chLoginSummary.Series(0).IsValueShownAsLabel = True
                chLoginSummary.Series(1).IsValueShownAsLabel = True

                chLoginSummary.Series(0)("LabelStyle") = "Bottom"
                chLoginSummary.Series(1)("LabelStyle") = "Bottom"

                chLoginSummary.Series(0)("DrawingStyle") = "Cylinder"
                chLoginSummary.Series(1)("DrawingStyle") = "Cylinder"
                chLoginSummary.Series(0).ToolTip = chLoginSummary.Series(0).LegendText & ": is #VAL "
                chLoginSummary.Series(1).ToolTip = chLoginSummary.Series(1).LegendText & ": is #VAL "
                chLoginSummary.Titles(0).Text = Language.getMessage(mstrModuleName, 211, "Attendance Summary On : ") & ConfigParameter._Object._CurrentDateAndTime.Date
                chLoginSummary.Titles(0).TextStyle = TextStyle.Emboss
                chLoginSummary.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)
                chLoginSummary.Series(0)("PixelPointWidth") = "40"
                chLoginSummary.Series(1)("PixelPointWidth") = "40"
                chLoginSummary.ChartAreas(0).AxisX.ScrollBar.Enabled = True
                chLoginSummary.ChartAreas(0).AxisX.ScrollBar.IsPositionedInside = True
                chLoginSummary.ChartAreas(0).AxisX.ScrollBar.Size = 15
                chLoginSummary.ChartAreas(0).AxisX.ScaleView.Zoom(1, 10)
                chLoginSummary.ChartAreas(0).AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll
                chLoginSummary.ChartAreas(0).AxisX.ScrollBar.BackColor = Color.Gray
                chLoginSummary.ChartAreas(0).AxisX.ScrollBar.ButtonColor = Color.LightGray
                chLoginSummary.Legends(0).Docking = Docking.Bottom
                'chLoginSummary.Legends(1).Docking = Docking.Bottom
            Else
                chLoginSummary.ChartAreas(0).Visible = False
                chLoginSummary.Legends(0).Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillChart", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Training_Cost()
        Try
            chTrainingCost.DataSource = Nothing
            If dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper).Rows.Count > 0 Then
                chTrainingCost.ChartAreas(0).Visible = True
                chTrainingCost.Series(0).Points.DataBindXY(dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper).DefaultView, "Title", dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper).DefaultView, "TotCosting")
                chTrainingCost.Series(0).Color = Color.RoyalBlue
                chTrainingCost.ChartAreas(0).AxisX.Interval = 1
                chTrainingCost.ChartAreas(0).AxisY.IsInterlaced = True
                chTrainingCost.ChartAreas(0).AxisY.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.FixedCount
                chTrainingCost.AntiAliasing = DataVisualization.Charting.AntiAliasingStyles.All
                chTrainingCost.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 212, "Training Title")
                chTrainingCost.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 213, "Training Costs")
                chTrainingCost.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chTrainingCost.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chTrainingCost.Series(0).LegendText = Language.getMessage(mstrModuleName, 213, "Training Costs")
                chTrainingCost.ChartAreas(0).AxisY.LabelAutoFitStyle = LabelAutoFitStyles.WordWrap
                chTrainingCost.ChartAreas(0).AxisY.LabelStyle.Format = GUI.fmtCurrency
                chTrainingCost.Legends(0).Docking = Docking.Bottom
                'chTrainingCost.Series(0).ToolTip = Language.getMessage(mstrModuleName, 213, "Training Costs") & ":  #VALX is #VAL "
                chTrainingCost.Series(0).ChartType = SeriesChartType.Column
                chTrainingCost.Series(0)("DrawingStyle") = "Cylinder"
                chTrainingCost.ChartAreas(0).AxisX.MinorGrid.Enabled = False
                chTrainingCost.ChartAreas(0).AxisX.MajorGrid.Enabled = False
                chTrainingCost.Titles(0).Text = Language.getMessage(mstrModuleName, 214, "Training Cost Analysis")
                chTrainingCost.Titles(0).TextStyle = TextStyle.Emboss
                chTrainingCost.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)
                chTrainingCost.Series(0).SmartLabelStyle.Enabled = True
                chTrainingCost.Series(0)("LabelStyle") = "Top"
                chTrainingCost.Legends(0).Docking = Docking.Bottom
                chTrainingCost.Series(0)("PixelPointWidth") = "40"
                chTrainingCost.ChartAreas(0).AxisX.LabelAutoFitStyle = LabelAutoFitStyles.WordWrap
                chTrainingCost.ChartAreas(0).AxisY.ScaleBreakStyle.Enabled = True
                chTrainingCost.ChartAreas(0).AxisY.ScaleBreakStyle.BreakLineStyle = BreakLineStyle.Straight
                chTrainingCost.ChartAreas(0).AxisY.ScaleBreakStyle.Spacing = 2
                chTrainingCost.ChartAreas(0).AxisY.ScaleBreakStyle.LineWidth = 1
                chTrainingCost.ChartAreas(0).AxisY.ScaleBreakStyle.LineColor = Color.Red
                If dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper).Rows.Count > 8 Then
                    chTrainingCost.ChartAreas(0).AxisX.ScrollBar.Enabled = True
                    chTrainingCost.ChartAreas(0).AxisX.ScrollBar.IsPositionedInside = True
                    chTrainingCost.ChartAreas(0).AxisX.ScrollBar.Size = 15
                    chTrainingCost.ChartAreas(0).AxisX.ScaleView.Zoom(0, 15)
                    chTrainingCost.ChartAreas(0).AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll
                    chTrainingCost.ChartAreas(0).AxisX.ScrollBar.BackColor = Color.Gray
                    chTrainingCost.ChartAreas(0).AxisX.ScrollBar.ButtonColor = Color.LightGray
                End If
            Else
                chTrainingCost.ChartAreas(0).Visible = False
                chTrainingCost.Legends(0).Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Training_Cost", mstrModuleName)
        End Try
    End Sub

    Private Sub animateWin(ByVal SpcToAnimate As SplitContainer, ByVal SpcToView As SplitContainer, ByVal intMode As Integer)
        Select Case intMode
            'Case 1
            '    AnimateWindow(SpcToView.Handle, 700, enAnimateWindowFlags.AW_HOR_POSITIVE Or enAnimateWindowFlags.AW_SLIDE)
            '    AnimateWindow(SpcToAnimate.Handle, 700, enAnimateWindowFlags.AW_HOR_POSITIVE Or enAnimateWindowFlags.AW_HIDE)
            'Case 2
            '    AnimateWindow(SpcToView.Handle, 700, enAnimateWindowFlags.AW_HOR_NEGATIVE Or enAnimateWindowFlags.AW_SLIDE)
            '    AnimateWindow(SpcToAnimate.Handle, 700, enAnimateWindowFlags.AW_HOR_NEGATIVE Or enAnimateWindowFlags.AW_HIDE)

            Case 1
                SpcToView.Hide()
                AnimateWindow(SpcToAnimate.Handle, 300, enAnimateWindowFlags.AW_HOR_POSITIVE Or enAnimateWindowFlags.AW_SLIDE Or enAnimateWindowFlags.AW_HIDE)
                AnimateWindow(SpcToView.Handle, 500, enAnimateWindowFlags.AW_HOR_POSITIVE Or enAnimateWindowFlags.AW_SLIDE Or enAnimateWindowFlags.AW_ACTIVATE)
            Case 2
                AnimateWindow(SpcToAnimate.Handle, 300, enAnimateWindowFlags.AW_HOR_NEGATIVE Or enAnimateWindowFlags.AW_HIDE Or enAnimateWindowFlags.AW_SLIDE)
                AnimateWindow(SpcToView.Handle, 500, enAnimateWindowFlags.AW_HOR_NEGATIVE Or enAnimateWindowFlags.AW_SLIDE Or enAnimateWindowFlags.AW_ACTIVATE)
        End Select
    End Sub

    Private Sub View3DChart(ByVal chChart As Chart, ByVal blnOperation As Boolean)
        Select Case chChart.Name.ToUpper
            Case "CHTRAININGCOST"
                chTrainingCost.ChartAreas(0).Area3DStyle.Enable3D = blnOperation
                chTrainingCost.ChartAreas(0).AxisX.ScrollBar.Enabled = True
                chTrainingCost.ChartAreas(0).AxisX.ScrollBar.IsPositionedInside = True
                chTrainingCost.ChartAreas(0).AxisX.ScrollBar.Size = 15
                chTrainingCost.ChartAreas(0).AxisX.ScaleView.Zoom(0, 10)
                chTrainingCost.ChartAreas(0).AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll
                chTrainingCost.ChartAreas(0).AxisX.ScrollBar.BackColor = Color.Gray
                chTrainingCost.ChartAreas(0).AxisX.ScrollBar.ButtonColor = Color.LightGray
            Case "CHSAL_ANALYSIS", "CHSTAFFTURNOVER", "CHLOGINSUMMARY", "CHLEAVE_ANALYSIS"
                chChart.ChartAreas(0).Area3DStyle.Enable3D = blnOperation
                chChart.ChartAreas(0).Area3DStyle.IsClustered = True
        End Select
    End Sub

    Private Sub Fill_Staff_TuOv()
        Try
            Dim dsList As New DataSet

            dsList = objDash.StaffTO_List(dgvStaff.DataSource, "Data")

            If dsList.Tables(0).Rows.Count <= 0 Then Exit Sub

            Dim iHired As Integer = dsList.Tables(0).Compute("SUM(Hired)", "")
            Dim iTerm As Integer = dsList.Tables(0).Compute("SUM(Term)", "")
            Dim iActive As Integer = dsList.Tables(0).Compute("SUM(Active)", "")

            If iHired > 0 Or iTerm > 0 Or iActive > 0 Then
                If dsList.Tables("Data").Rows.Count > 0 Then
                    chStaffTurnOver.ChartAreas(0).Visible = True

                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems.Clear()

                    chStaffTurnOver.Series(0).Points.DataBindXY(dsList.Tables("Data").DefaultView, "MonthName", dsList.Tables("Data").DefaultView, "Hired")
                    chStaffTurnOver.Series(1).Points.DataBindXY(dsList.Tables("Data").DefaultView, "MonthName", dsList.Tables("Data").DefaultView, "Term")
                    chStaffTurnOver.Series(2).Points.DataBindXY(dsList.Tables("Data").DefaultView, "MonthName", dsList.Tables("Data").DefaultView, "Active")

                    'chStaffTurnOver.Series(0).LegendText = Language.getMessage(mstrModuleName, 301, "Total Hired")
                    'chStaffTurnOver.Series(1).LegendText = Language.getMessage(mstrModuleName, 302, "Total Terminated")
                    'chStaffTurnOver.Series(2).LegendText = Language.getMessage(mstrModuleName, 303, "Total Active")

                    chStaffTurnOver.Series(0).IsVisibleInLegend = False
                    chStaffTurnOver.Series(1).IsVisibleInLegend = False
                    chStaffTurnOver.Series(2).IsVisibleInLegend = False

                    chStaffTurnOver.Series(0).Color = Color.RoyalBlue
                    chStaffTurnOver.Series(1).Color = Color.Cyan
                    chStaffTurnOver.Series(2).Color = Color.Green

                    Dim legendItem1 As New LegendItem()
                    legendItem1.Name = Language.getMessage(mstrModuleName, 301, "Total Hired")
                    legendItem1.ImageStyle = LegendImageStyle.Rectangle
                    legendItem1.ShadowOffset = 1
                    legendItem1.Color = Color.RoyalBlue
                    chStaffTurnOver.Legends(0).CustomItems.Add(legendItem1)

                    Dim legendItem2 As New LegendItem()
                    legendItem2.Name = Language.getMessage(mstrModuleName, 302, "Total Terminated")
                    legendItem2.ImageStyle = LegendImageStyle.Rectangle
                    legendItem2.ShadowOffset = 1
                    legendItem2.Color = Color.Cyan
                    chStaffTurnOver.Legends(0).CustomItems.Add(legendItem2)

                    Dim legendItem3 As New LegendItem()
                    legendItem3.Name = Language.getMessage(mstrModuleName, 303, "Total Active")
                    legendItem3.ImageStyle = LegendImageStyle.Rectangle
                    legendItem3.ShadowOffset = 1
                    legendItem3.Color = Color.Green
                    chStaffTurnOver.Legends(0).CustomItems.Add(legendItem3)

                    chStaffTurnOver.Series(0).Tag = "Hired"
                    chStaffTurnOver.Series(1).Tag = "Term"
                    chStaffTurnOver.Series(2).Tag = "Active"

                    chStaffTurnOver.ChartAreas(0).AxisX.LabelStyle.Interval = 1

                    chStaffTurnOver.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 304, "Months")
                    chStaffTurnOver.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 305, "Total Employee")

                    chStaffTurnOver.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                    chStaffTurnOver.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)

                    chStaffTurnOver.ChartAreas(0).AxisY.IsInterlaced = True

                    chStaffTurnOver.ChartAreas(0).AxisX.MajorGrid.Enabled = False
                    chStaffTurnOver.ChartAreas(0).AxisX.MinorGrid.Enabled = False

                    chStaffTurnOver.ChartAreas(0).AxisY.MajorGrid.Enabled = False
                    chStaffTurnOver.ChartAreas(0).AxisY.MinorGrid.Enabled = False

                    chStaffTurnOver.Series(0)("DrawingStyle") = "Cylinder"
                    chStaffTurnOver.Series(1)("DrawingStyle") = "Cylinder"
                    chStaffTurnOver.Series(2)("DrawingStyle") = "Cylinder"

                    chStaffTurnOver.Series(0).ToolTip = chStaffTurnOver.Legends(0).CustomItems(0).Name & " : is #VAL "
                    chStaffTurnOver.Series(1).ToolTip = chStaffTurnOver.Legends(0).CustomItems(1).Name & " : is #VAL "
                    chStaffTurnOver.Series(2).ToolTip = chStaffTurnOver.Legends(0).CustomItems(2).Name & " : is #VAL "

                    chStaffTurnOver.Titles(0).Text = Language.getMessage(mstrModuleName, 306, "STAFF TURN OVER")
                    chStaffTurnOver.Titles(0).TextStyle = TextStyle.Emboss
                    chStaffTurnOver.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)

                    chStaffTurnOver.Series(0)("PixelPointWidth") = "50"
                    chStaffTurnOver.Series(1)("PixelPointWidth") = "50"
                    chStaffTurnOver.Series(2)("PixelPointWidth") = "50"

                    chStaffTurnOver.ChartAreas(0).AxisY.ScaleBreakStyle.Enabled = True
                    chStaffTurnOver.ChartAreas(0).AxisY.ScaleBreakStyle.BreakLineStyle = BreakLineStyle.Straight
                    chStaffTurnOver.ChartAreas(0).AxisY.ScaleBreakStyle.Spacing = 1
                    chStaffTurnOver.ChartAreas(0).AxisY.ScaleBreakStyle.LineWidth = 1
                    chStaffTurnOver.ChartAreas(0).AxisY.ScaleBreakStyle.LineColor = Color.Red

                Else
                    chStaffTurnOver.ChartAreas(0).Visible = False
                End If
            Else
                chStaffTurnOver.ChartAreas(0).Visible = False
                chStaffTurnOver.Legends(0).Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Staff_TuOv", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetChartOperation(ByVal mChart As Chart, ByVal enOperation As enChartOperation)
        Select Case enOperation

            Case enChartOperation.PREVIEW_CHART

                mChart.Printing.PrintDocument.DefaultPageSettings.Landscape = True

                mChart.Printing.PrintDocument.DefaultPageSettings.Margins = New System.Drawing.Printing.Margins(100, 100, 100, 100)

                Dim pr As PrinterResolution
                For Each pr In mChart.Printing.PrintDocument.PrinterSettings.PrinterResolutions
                    If pr.Kind.ToString() = "High" Then
                        mChart.Printing.PrintDocument.DefaultPageSettings.PrinterResolution = pr
                    End If
                Next
                mChart.Printing.PrintPreview()

            Case enChartOperation.PRINT_CHART

                mChart.Printing.PrintDocument.DefaultPageSettings.Landscape = True
                mChart.Printing.PrintDocument.DefaultPageSettings.Margins = New System.Drawing.Printing.Margins(100, 100, 100, 100)
                Dim pr As PrinterResolution
                For Each pr In mChart.Printing.PrintDocument.PrinterSettings.PrinterResolutions
                    If pr.Kind.ToString() = "Low" Then
                        mChart.Printing.PrintDocument.DefaultPageSettings.PrinterResolution = pr
                    End If
                Next
                mChart.Printing.Print(True)

            Case enChartOperation.SAVE_CHART

                Dim saveFileDialog1 As New SaveFileDialog()
                saveFileDialog1.Filter = "Bitmap (*.bmp)|*.bmp|JPEG (*.jpg)|*.jpg|EMF (*.emf)|*.emf|PNG (*.png)|*.png|GIF (*.gif)|*.gif|TIFF (*.tif)|*.tif"
                saveFileDialog1.FilterIndex = 2
                saveFileDialog1.RestoreDirectory = True

                If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim format As ChartImageFormat = ChartImageFormat.Bmp
                    If saveFileDialog1.FileName.EndsWith("bmp") Then
                        format = ChartImageFormat.Bmp
                    ElseIf saveFileDialog1.FileName.EndsWith("jpg") Then
                        format = ChartImageFormat.Jpeg
                    ElseIf saveFileDialog1.FileName.EndsWith("emf") Then
                        format = ChartImageFormat.Emf
                    ElseIf saveFileDialog1.FileName.EndsWith("gif") Then
                        format = ChartImageFormat.Gif
                    ElseIf saveFileDialog1.FileName.EndsWith("png") Then
                        format = ChartImageFormat.Png
                    ElseIf saveFileDialog1.FileName.EndsWith("tif") Then
                        format = ChartImageFormat.Tiff
                    End If
                    mChart.SaveImage(saveFileDialog1.FileName, format)
                End If

        End Select
    End Sub

    Private Sub ControlVisibility()
        Try
            objSpcSalAnalysis.Visible = User._Object.Privilege._AllowAccessSalaryAnalysis
            objSpcLoginSummary.Visible = User._Object.Privilege._AllowAccessAttendanceSummary
            objSpcLeave.Visible = User._Object.Privilege._AllowAccessLeaveAnalysis
            objSpcStaff.Visible = User._Object.Privilege._AllowAccessStaffTurnOver
            objSpcTrainingCost.Visible = User._Object.Privilege._AllowAccessTrainingAnalysis

            If User._Object.Privilege._AllowAccessStaffTurnOver = False Or User._Object.Privilege._AllowAccessTrainingAnalysis = False Then
                objFooterHR.Enabled = False
            End If

            If User._Object.Privilege._AllowAccessLeaveAnalysis = False Or User._Object.Privilege._AllowAccessAttendanceSummary = False Then
                objFooterLT.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "controlVisibility", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Sohail (25 Apr 2019) -- Start
    'Enhancement - 76.1 - Performance enhancement of aruti main view.
    'Private Sub SetGridStyle()
    '    Try
    '        Dim pCell As New clsMergeCell
    '        Dim OldColor As Color
    '        For Each dgvRow As DataGridViewRow In dgvEmpDates.Rows
    '            If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                If OldColor <> Color.SteelBlue Then
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Value = "+"
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.ForeColor = Color.White
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.BackColor = Color.SteelBlue
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.SelectionBackColor = dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.BackColor
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.SelectionForeColor = dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.ForeColor
    '                    OldColor = Color.SteelBlue
    '                    pCell.MakeMerge(dgvEmpDates, dgvRow.Index, 1, dgvRow.Cells.Count - 1, Color.SteelBlue, Color.White, dgvRow.Cells(dgcolhECode.Index).Value.ToString, "")
    '                Else
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Value = "+"
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.ForeColor = Color.White
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.BackColor = Color.Gray
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.SelectionBackColor = dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.BackColor
    '                    dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.SelectionForeColor = dgvRow.Cells(objdgcolhEmpCollapse.Index).Style.ForeColor
    '                    OldColor = Color.Gray
    '                    pCell.MakeMerge(dgvEmpDates, dgvRow.Index, 1, dgvRow.Cells.Count - 1, Color.Gray, Color.White, dgvRow.Cells(dgcolhECode.Index).Value.ToString, "")
    '                End If
    '            Else
    '                dgvRow.Visible = False
    '            End If
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetRevieweGridStyle", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Sohail (25 Apr 2019) -- End

    Private Sub objbtnSummaryReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSummaryReset.Click
        Try

            'Pinkal (28-Feb-2014) -- Start
            'Enhancement : TRA Changes

            'Dim dsList As New DataSet
            'dsList = objDash.EmployeeDates(clsDashboard_Class.enDashBoardListIndex.EMPLOYEE_DATES.ToString.ToUpper)

            'dgcolhECode.DataPropertyName = "ECode"
            'dgcolhEName.DataPropertyName = "EName"
            'dgcolhDate.DataPropertyName = "Date"
            'objdgcolhIsGrp.DataPropertyName = "IsGrp"
            'objdgcolhEDatesGrpId.DataPropertyName = "GrpId"
            'objdgcolhEmpId.DataPropertyName = "EmpId"
            'dgvEmpDates.DataSource = dsList.Tables(clsDashboard_Class.enDashBoardListIndex.EMPLOYEE_DATES.ToString.ToUpper)

            'Call SetGridStyle()
            dsCollection.Tables.Clear()
            'Sohail (25 Apr 2019) -- Start
            'Enhancement - 76.1 - Performance enhancement of aruti main view.
            'dgvEmpDates.DataSource = Nothing
            dsDates = Nothing
            dsEmpDatesCount = Nothing
            'Sohail (25 Apr 2019) -- End
            dgvStaff.DataSource = Nothing
            dgvTrainingCosting.DataSource = Nothing
            dgvSalAnalysis.DataSource = Nothing
            dgvLeave.DataSource = Nothing
            dgvLoginSummary.DataSource = Nothing

            tabcDashBoard_SelectedIndexChanged(sender, e)

            'Pinkal (28-Feb-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSummaryReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    Private Sub Check_Lic()
        Try
            If ConfigParameter._Object._IsArutiDemo = False Then
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
                    tabcDashBoard.TabPages.Remove(tabpLeaveTnA)
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) Then
                    tabcDashBoard.TabPages.Remove(tabpTraining_Cost)
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) Then
                    tabcDashBoard.TabPages.Remove(tabpPayroll)
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Check_Lic", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls Events "

#Region " SALARY ANALYSIS "

    Private Sub chSal_Analysis_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chSal_Analysis.MouseMove
        Try
            Dim result As HitTestResult = chSal_Analysis.HitTest(e.X, e.Y)

            If result.ChartElementType = ChartElementType.DataPoint Then
                Me.Cursor = Cursors.Hand
            Else
                Me.Cursor = Cursors.Default
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chSal_Analysis_MouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub chSal_Analysis_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chSal_Analysis.MouseUp
        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then
                mstrChartName = chSal_Analysis.Name
                Exit Sub
            End If

            Dim result As HitTestResult = chSal_Analysis.HitTest(e.X, e.Y)
            If result.ChartElementType = ChartElementType.DataPoint Then
                Dim StrName As String = result.Series().Points.Item(result.PointIndex).AxisLabel.ToString.Trim
                Dim dtTemp() As DataRow = dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).Select("PName = '" & result.Series().Points.Item(result.PointIndex).AxisLabel.ToString.Trim & "'")
                If dtTemp.Length > 0 Then
                    Dim dsDetail As New DataSet
                    Select Case result.Series().Tag.ToString.ToUpper
                        Case "TBS"
                            dsDetail = objDash.Detail_Information(clsDashboard_Class.enViewDetails.SALARY_DETAIL, dtTemp(0)("Pid"), clsDashboard_Class.enViewDetailsByIdx.DEPARTMENT_WISE, "List", 1)
                        Case "TCS"
                            dsDetail = objDash.Detail_Information(clsDashboard_Class.enViewDetails.SALARY_DETAIL, dtTemp(0)("Pid"), clsDashboard_Class.enViewDetailsByIdx.DEPARTMENT_WISE, "List", 2)
                        Case "THS"
                            dsDetail = objDash.Detail_Information(clsDashboard_Class.enViewDetails.SALARY_DETAIL, dtTemp(0)("Pid"), clsDashboard_Class.enViewDetailsByIdx.DEPARTMENT_WISE, "List", 3)
                    End Select
                    Dim frm As New frmDetailInformation
                    If frm.displayDialog(clsDashboard_Class.enViewDetails.SALARY_DETAIL, dsDetail, StrName, CInt(dtTemp(0)("Pid")), result.Series().Tag) = True Then
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chSal_Analysis_MouseUp", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " LEAVE ANALYSIS "

    Private Sub chLeave_Analysis_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chLeave_Analysis.MouseMove
        Try
            Dim result As HitTestResult = chLeave_Analysis.HitTest(e.X, e.Y)

            If result.ChartElementType = ChartElementType.DataPoint Then
                Me.Cursor = Cursors.Hand
            Else
                Me.Cursor = Cursors.Default
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chLeave_Analysis_MouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub chLeave_Analysis_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chLeave_Analysis.MouseUp
        Try

            If e.Button = Windows.Forms.MouseButtons.Right Then
                mstrChartName = chLeave_Analysis.Name
                Exit Sub
            End If
            Dim result As HitTestResult = chLeave_Analysis.HitTest(e.X, e.Y)
            If result.ChartElementType = ChartElementType.DataPoint Then
                Dim StrName As String = result.Series().Points.Item(result.PointIndex).AxisLabel.ToString.Trim
                Dim dtTemp() As DataRow = dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.LEAVE_LIST.ToString.ToUpper).Select("MName = '" & result.Series().Points.Item(result.PointIndex).AxisLabel.ToString.Trim & "'")
                If dtTemp.Length > 0 Then
                    Dim dsDetail As New DataSet
                    dsDetail = objDash.Detail_Information(clsDashboard_Class.enViewDetails.LEAVE_DETAIL, dtTemp(0)("MId"), clsDashboard_Class.enViewDetailsByIdx.DEPARTMENT_WISE, "List", result.Series().Tag)
                    Dim frm As New frmDetailInformation
                    If frm.displayDialog(clsDashboard_Class.enViewDetails.LEAVE_DETAIL, dsDetail, StrName, CInt(dtTemp(0)("MId")), result.Series().Tag) = True Then
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chLeave_Analysis_MouseUp", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ATTENDANCE SUMMARY "

    Private Sub chLoginSummary_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chLoginSummary_MouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub chLoginSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chLoginSummary.MouseUp
        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then
                mstrChartName = chLoginSummary.Name
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chLoginSummary_MouseUp", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " TRAINING COSTING "

    Private Sub chTrainingCost_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chTrainingCost.MouseMove
        Try
            Dim result As HitTestResult = chTrainingCost.HitTest(e.X, e.Y)

            If result.ChartElementType = ChartElementType.DataPoint Then
                Me.Cursor = Cursors.Hand
            Else
                Me.Cursor = Cursors.Default
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chTrainingCost_MouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub chTrainingCost_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chTrainingCost.MouseUp
        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then
                mstrChartName = chTrainingCost.Name
                Exit Sub
            End If
            Dim result As HitTestResult = chTrainingCost.HitTest(e.X, e.Y)
            If result.ChartElementType = ChartElementType.DataPoint Then
                Dim StrName As String = result.Series().Points.Item(result.PointIndex).AxisLabel.ToString.Trim
                Dim dtTemp() As DataRow = dsCollection.Tables(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper).Select("Title = '" & result.Series().Points.Item(result.PointIndex).AxisLabel.ToString.Trim & "'")
                If dtTemp.Length > 0 Then
                    Dim dsDetail As New DataSet
                    dsDetail = objDash.Detail_Information(clsDashboard_Class.enViewDetails.TRAINING_COST, dtTemp(0)("TrainingId"), clsDashboard_Class.enViewDetailsByIdx.DEPARTMENT_WISE, "List")
                    Dim frm As New frmDetailInformation
                    frm.eZeeHeading.Enabled = False
                    If frm.displayDialog(clsDashboard_Class.enViewDetails.TRAINING_COST, dsDetail, StrName, CInt(dtTemp(0)("TrainingId"))) = True Then
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chLeave_Analysis_MouseUp", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " STAFF TURN OVER "

    Private Sub chStaffTurnOver_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chStaffTurnOver.MouseMove
        Try
            Dim result As HitTestResult = chStaffTurnOver.HitTest(e.X, e.Y)
            If result.ChartElementType = ChartElementType.DataPoint Then
                Me.Cursor = Cursors.Hand
            Else
                Me.Cursor = Cursors.Default
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chStaffTurnOver_MouseMove", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chStaffTurnOver_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chStaffTurnOver.MouseUp
        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then
                mstrChartName = chStaffTurnOver.Name
                Exit Sub
            End If

            Dim result As HitTestResult = chStaffTurnOver.HitTest(e.X, e.Y)

            If result.ChartElementType = ChartElementType.DataPoint Then
                Dim StrName As String = "01" & " " & result.Series().Points.Item(result.PointIndex).AxisLabel.ToString.Trim & " " & ConfigParameter._Object._CurrentDateAndTime.Year
                Dim dsDetail As New DataSet
                dsDetail = objDash.Staff_Detail_Informatiom(clsDashboard_Class.enViewDetails.STAFF_DETAILS, CDate(StrName).Month, clsDashboard_Class.enViewDetailsByIdx.DEPARTMENT_WISE, result.Series().Tag.ToString, "Detail")
                Dim frm As New frmDetailInformation
                If frm.displayDialog(clsDashboard_Class.enViewDetails.STAFF_DETAILS, dsDetail, result.Series().Points.Item(result.PointIndex).AxisLabel.ToString.Trim, CDate(StrName).Month, result.Series().Tag.ToString) = True Then
                    Exit Sub
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chStaffTurnOver_MouseUp", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " OTHER EVENTS "

    Private Sub mnuView3D_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuView3D.Click
        Try
        Select Case mstrChartName.ToUpper
            Case "CHTRAININGCOST"
                Call View3DChart(chTrainingCost, CBool(mnuView3D.Checked))

            Case "CHLEAVE_ANALYSIS"
                Call View3DChart(chLeave_Analysis, CBool(mnuView3D.Checked))

            Case "CHLOGINSUMMARY"
                Call View3DChart(chLoginSummary, CBool(mnuView3D.Checked))

            Case "CHSTAFFTURNOVER"
                Call View3DChart(chStaffTurnOver, CBool(mnuView3D.Checked))

            Case "CHSAL_ANALYSIS"
                Call View3DChart(chSal_Analysis, CBool(mnuView3D.Checked))

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuView3D_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRefresh.Click
        If objDash IsNot Nothing Then objDash = Nothing

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'objDash = New clsDashboard_Class
        objDash = New clsDashboard_Class(FinancialYear._Object._DatabaseName, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         FinancialYear._Object._Database_Start_Date, _
                                         FinancialYear._Object._Database_End_Date, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._CurrentDateAndTime, True, True, _
                                         User._Object.Privilege._Show_Probation_Dates, _
                                         User._Object.Privilege._Show_Suspension_Dates, _
                                         User._Object.Privilege._Show_Appointment_Dates, _
                                         User._Object.Privilege._Show_Confirmation_Dates, _
                                         User._Object.Privilege._Show_BirthDates, _
                                         User._Object.Privilege._Show_Anniversary_Dates, _
                                         User._Object.Privilege._Show_Contract_Ending_Dates, _
                                         User._Object.Privilege._Show_TodayRetirement_Dates, _
                                         User._Object.Privilege._Show_ForcastedRetirement_Dates, _
                                         User._Object.Privilege._Show_ForcastedEOC_Dates, _
                                         User._Object.Privilege._Show_ForcastedELC_Dates, _
                                         ConfigParameter._Object._Forcasted_ViewSetting, _
                                         ConfigParameter._Object._ForcastedValue, _
                                         ConfigParameter._Object._ForcastedEOC_ViewSetting, _
                                         ConfigParameter._Object._ForcastedEOCValue, _
                                         ConfigParameter._Object._ForcastedELCViewSetting, _
                                         ConfigParameter._Object._ForcastedELCValue, _
                                         ConfigParameter._Object._LeaveBalanceSetting, True, _
                                         User._Object.Privilege._AllowtoViewPendingAccrueData)
        'S.SANDEEP [12-Apr-2018] -- START {Ref#218|#ARUTI-106} [User._Object.Privilege._AllowtoViewPendingAccrueData] -- END
        'Shani(24-Aug-2015) -- End


        dsCollection = objDash._DataSetCollection

        Select Case mstrChartName.ToUpper
            Case "CHTRAININGCOST"
                Call GeneRateDataGrid(enDashModule.DSH_HUMANRESOURCE, dgvTrainingCosting)

            Case "CHLEAVE_ANALYSIS"
                mDicLeaveSeries.Clear()
                Call GeneRateDataGrid(enDashModule.DSH_LEEAVETNA, dgvLeave)

            Case "CHLOGINSUMMARY"
                Call GeneRateDataGrid(enDashModule.DSH_LEEAVETNA, dgvLoginSummary)

            Case "CHSTAFFTURNOVER"
                Call GeneRateDataGrid(enDashModule.DSH_HUMANRESOURCE, dgvStaff)

            Case "CHSAL_ANALYSIS"
                Call GeneRateDataGrid(enDashModule.DSH_PAYROLL, dgvSalAnalysis)

        End Select
    End Sub

    Private Sub cboTDivision_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTDivision.SelectedIndexChanged
        If mblnFormLoad = False Then Exit Sub
        Dim dsList As New DataSet
        Try
            If cboTDivision.SelectedValue <= 0 Then
                dsList = objDash.Training_Costing(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper, "")
            Else
                dsList = objDash.Training_Costing(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper, cboTDivision.Text)
            End If

            dsCollection.Tables.Remove(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper)
            dsCollection.AcceptChanges()
            dsCollection.Tables.Add(dsList.Tables(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper).Copy)

            Call Fill_Training_Cost()

            Call mnuView3D_Click(sender, e)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTDivision_SelectedIndexChanged", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Private Sub cboSDivision_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSDivision.SelectedIndexChanged
        If mblnFormLoad = False Then Exit Sub
        Dim dsList As New DataSet
        Try
            If cboSDivision.SelectedValue <= 0 Then
                dsList = objDash.Salary_Analysis(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper, "")
            Else
                dsList = objDash.Salary_Analysis(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper, cboSDivision.Text)
            End If

            dsCollection.Tables.Remove(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper)
            dsCollection.AcceptChanges()
            dsCollection.Tables.Add(dsList.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).Copy)

            Call Fill_Salary_Analysis()

            Call mnuView3D_Click(sender, e)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    'Private Sub bgwFillChart_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwFillChart.RunWorkerCompleted
    '    Try
    '        If tabcDashBoard.TabPages.Contains(tabpHR) = True Then
    '            AddHandler tabpHR.Enter, AddressOf tabpHR_Enter
    '            Call tabpHR_Enter(Nothing, Nothing)
    '            mblnIsHRCalled = True
    '        End If
    '        If tabcDashBoard.TabPages.Contains(tabpLeaveTnA) = True Then
    '            AddHandler tabpLeaveTnA.Enter, AddressOf tabpLeaveTnA_Enter
    '            If mblnIsHRCalled = False Then
    '                Call tabpLeaveTnA_Enter(Nothing, Nothing)
    '            End If
    '        End If
    '        If tabcDashBoard.TabPages.Contains(tabpPayroll) = True Then
    '            AddHandler tabpPayroll.Enter, AddressOf tabpPayroll_Enter
    '            If mblnIsHRCalled = False Then
    '                Call tabpPayroll_Enter(Nothing, Nothing)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "bgwFillChart_RunWorkerCompleted", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'Private Sub tabpHR_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpHR.Enter
    '    Try
    '        If IsHRMessageShown = False Then
    '            If dgvStaff.RowCount <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 401, "Sorry, Information is not available for Staff Turnover."), enMsgBoxStyle.Information)
    '                IsHRMessageShown = True
    '                Exit Sub
    '            End If

    '            If dgvTrainingCosting.RowCount <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 402, "Sorry, Information is not available for Training."), enMsgBoxStyle.Information)
    '                IsHRMessageShown = True
    '                Exit Sub
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tabpHR_Enter", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub tabpLeaveTnA_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpLeaveTnA.Enter
    '    Try
    '        If IsLeaveMessageShown = False Then
    '            If dgvLeave.RowCount <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 403, "Sorry, Information is not available for Leave."), enMsgBoxStyle.Information)
    '                IsLeaveMessageShown = True
    '                Exit Sub
    '            End If

    '            If dgvLoginSummary.RowCount <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 404, "Sorry, Information is not available for Login Summary."), enMsgBoxStyle.Information)
    '                IsLeaveMessageShown = True
    '                Exit Sub
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tabpLeaveTnA_Enter", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub tabpPayroll_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpPayroll.Enter
    '    Try
    '        If IsPayrollMessaheShown = False Then
    '            If dgvSalAnalysis.RowCount <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 405, "Sorry, Information is not available for Salary Analysis."), enMsgBoxStyle.Information)
    '                IsPayrollMessaheShown = True
    '                Exit Sub
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tabpPayroll_Enter", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    Private Sub mnuPreviewChart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreviewChart.Click
        Try
            Select Case mstrChartName.ToUpper

                Case "CHTRAININGCOST"
                    Call SetChartOperation(chTrainingCost, enChartOperation.PREVIEW_CHART)
                Case "CHLEAVE_ANALYSIS"
                    Call SetChartOperation(chLeave_Analysis, enChartOperation.PREVIEW_CHART)
                Case "CHLOGINSUMMARY"
                    Call SetChartOperation(chLoginSummary, enChartOperation.PREVIEW_CHART)
                Case "CHSTAFFTURNOVER"
                    Call SetChartOperation(chStaffTurnOver, enChartOperation.PREVIEW_CHART)
                Case "CHSAL_ANALYSIS"
                    Call SetChartOperation(chSal_Analysis, enChartOperation.PREVIEW_CHART)

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreviewChart_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuPrintChart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrintChart.Click
        Try
            Select Case mstrChartName.ToUpper

                Case "CHTRAININGCOST"
                    Call SetChartOperation(chTrainingCost, enChartOperation.PRINT_CHART)
                Case "CHLEAVE_ANALYSIS"
                    Call SetChartOperation(chLeave_Analysis, enChartOperation.PRINT_CHART)
                Case "CHLOGINSUMMARY"
                    Call SetChartOperation(chLoginSummary, enChartOperation.PRINT_CHART)
                Case "CHSTAFFTURNOVER"
                    Call SetChartOperation(chStaffTurnOver, enChartOperation.PRINT_CHART)
                Case "CHSAL_ANALYSIS"
                    Call SetChartOperation(chSal_Analysis, enChartOperation.PRINT_CHART)

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrintChart_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuSaveChart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveChart.Click
        Try
            Select Case mstrChartName.ToUpper

                Case "CHTRAININGCOST"
                    Call SetChartOperation(chTrainingCost, enChartOperation.SAVE_CHART)
                Case "CHLEAVE_ANALYSIS"
                    Call SetChartOperation(chLeave_Analysis, enChartOperation.SAVE_CHART)
                Case "CHLOGINSUMMARY"
                    Call SetChartOperation(chLoginSummary, enChartOperation.SAVE_CHART)
                Case "CHSTAFFTURNOVER"
                    Call SetChartOperation(chStaffTurnOver, enChartOperation.SAVE_CHART)
                Case "CHSAL_ANALYSIS"
                    Call SetChartOperation(chSal_Analysis, enChartOperation.SAVE_CHART)

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSaveChart_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Grid Events "

    'Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
    '    Try
    '        For i As Integer = 0 To dgvOccassion.RowCount - 1
    '            dgvOccassion.Rows(i).Cells(objdgcolhCheck.Index).Value = CBool(objchkAll.CheckState)
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure :  ; Module Name : " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub dgvOccassion_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOccassion.CellContentClick
    '    Try
    '        RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
    '        If e.ColumnIndex = objdgcolhCheck.Index Then
    '            dsOccasion.Tables(0).Rows(e.RowIndex)(e.ColumnIndex) = Not CBool(dsOccasion.Tables(0).Rows(e.RowIndex)(e.ColumnIndex))
    '            dgvOccassion.RefreshEdit()
    '            Dim drRow As DataRow() = dsOccasion.Tables(0).Select("IsCheck = 1", "")

    '            If drRow.Length > 0 Then
    '                If dsOccasion.Tables(0).Rows.Count = drRow.Length Then
    '                    objchkAll.CheckState = CheckState.Checked
    '                Else
    '                    objchkAll.CheckState = CheckState.Indeterminate
    '                End If
    '            Else
    '                objchkAll.CheckState = CheckState.Unchecked
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure : dgvOccassion_CellContentClick ; Module Name : " & mstrModuleName)
    '    Finally
    '        AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
    '    End Try
    'End Sub

    'Private Sub lnkSendMail_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSendMail.LinkClicked
    '    Try
    '        Dim dtTemp() As DataRow = dsOccasion.Tables(0).Select("IsCheck = 1")

    '        If dtTemp.Length > 0 Then
    '            Dim objLetterFields As New clsLetterFields
    '            Dim strEmpIds As String = String.Empty
    '            Dim mstrEmailList As String = String.Empty
    '            Dim dsList As New DataSet
    '            Dim blnFlag As Boolean = False

    '            For i As Integer = 0 To dtTemp.Length - 1
    '                If dtTemp(i)("Email").ToString.Trim.Length > 0 Then
    '                    mstrEmailList &= "," & dtTemp(i)("EName").ToString & "<" & dtTemp(i)("Email").ToString & ">"
    '                    strEmpIds &= "," & dtTemp(i)("EmpId")
    '                Else
    '                    If blnFlag = False Then
    '                        Dim strMsg As String = Language.getMessage(mstrModuleName, 2, "Some of the email address(s) are blank.And will not added to the list. Do you want to continue?")
    '                        If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
    '                            blnFlag = True
    '                            Continue For
    '                        Else
    '                            Exit Sub
    '                        End If
    '                    End If
    '                End If
    '            Next

    '            mstrEmailList = Mid(mstrEmailList, 2)
    '            strEmpIds = Mid(strEmpIds, 2)

    '            If strEmpIds.Length > 0 Then
    '                dsList = objLetterFields.GetEmployeeData(strEmpIds, enImg_Email_RefId.Employee_Module)
    '            End If

    '            Dim objFrm As New frmSendMail

    '            objFrm.displayDialog(enImg_Email_RefId.Employee_Module, mstrEmailList, dsList)

    '        Else
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 215, "Please select atleast one employee to send mail."), enMsgBoxStyle.Information)
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure : lnkSendMail_LinkClicked ; Module Name : " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub dgvTrainingCosting_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTrainingCosting.CellContentClick
        If e.RowIndex <= -1 Then Exit Sub
        Try
            For Each Point As DataPoint In chTrainingCost.Series(0).Points
                If dgvTrainingCosting.Rows(e.RowIndex).Cells(dgcolhTitle.Index).Value.ToString = Point.AxisLabel.ToString Then
                    Point.Color = Color.Red
                Else
                    Point.Color = Color.RoyalBlue
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTrainingCosting_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvTrainingCosting_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvTrainingCosting.SelectionChanged
        Try
            If dgvTrainingCosting.SelectedRows.Count <= 0 Then Exit Sub
            For Each Point As DataPoint In chTrainingCost.Series(0).Points
                If dgvTrainingCosting.SelectedRows(0).Cells(dgcolhTitle.Index).Value.ToString = Point.AxisLabel.ToString Then
                    Point.Color = Color.Red
                Else
                    Point.Color = Color.RoyalBlue
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTrainingCosting_SelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvStaff_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStaff.CellContentClick
        If e.RowIndex <= -1 Then Exit Sub
        Try
            Select Case CInt(dgvStaff.Rows(e.RowIndex).Cells(objdgcolhViewId.Index).Value)
                Case 1  'NEWLY HIRED
                    For Each Point As DataPoint In chStaffTurnOver.Series(0).Points
                        Point.Color = Color.Red
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(0).Color = Color.Red

                    For Each Point As DataPoint In chStaffTurnOver.Series(1).Points
                        Point.Color = Color.Cyan
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(1).Color = Color.Cyan

                    For Each Point As DataPoint In chStaffTurnOver.Series(2).Points
                        Point.Color = Color.Green
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(2).Color = Color.Green
                Case 2  'TOTAL TERMINATED
                    For Each Point As DataPoint In chStaffTurnOver.Series(0).Points
                        Point.Color = Color.RoyalBlue
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(0).Color = Color.RoyalBlue

                    For Each Point As DataPoint In chStaffTurnOver.Series(1).Points
                        Point.Color = Color.Red
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(1).Color = Color.Red

                    For Each Point As DataPoint In chStaffTurnOver.Series(2).Points
                        Point.Color = Color.Green
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(2).Color = Color.Green

                Case 3  'TOTAL ACTIVE EMPLOYEE

                    For Each Point As DataPoint In chStaffTurnOver.Series(0).Points
                        Point.Color = Color.RoyalBlue
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(0).Color = Color.RoyalBlue

                    For Each Point As DataPoint In chStaffTurnOver.Series(1).Points
                        Point.Color = Color.Cyan
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(1).Color = Color.Cyan

                    For Each Point As DataPoint In chStaffTurnOver.Series(2).Points
                        Point.Color = Color.Red
                    Next
                    chStaffTurnOver.Legends(0).CustomItems(2).Color = Color.Red
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvStaff_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvStaff_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvStaff.SelectionChanged
        Try
            If dgvStaff.SelectedRows.Count <= 0 Then Exit Sub

            Select Case CInt(dgvStaff.SelectedRows(0).Cells(objdgcolhViewId.Index).Value)
                Case 1  'NEWLY HIRED
                    For Each Point As DataPoint In chStaffTurnOver.Series(0).Points
                        Point.Color = Color.Red
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(0).Color = Color.Red

                    For Each Point As DataPoint In chStaffTurnOver.Series(1).Points
                        Point.Color = Color.Cyan
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(1).Color = Color.Cyan

                    For Each Point As DataPoint In chStaffTurnOver.Series(2).Points
                        Point.Color = Color.Green
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(2).Color = Color.Green
                Case 2  'TOTAL TERMINATED
                    For Each Point As DataPoint In chStaffTurnOver.Series(0).Points
                        Point.Color = Color.RoyalBlue
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(0).Color = Color.RoyalBlue

                    For Each Point As DataPoint In chStaffTurnOver.Series(1).Points
                        Point.Color = Color.Red
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(1).Color = Color.Red

                    For Each Point As DataPoint In chStaffTurnOver.Series(2).Points
                        Point.Color = Color.Green
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(2).Color = Color.Green

                Case 3  'TOTAL ACTIVE EMPLOYEE

                    For Each Point As DataPoint In chStaffTurnOver.Series(0).Points
                        Point.Color = Color.RoyalBlue
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(0).Color = Color.RoyalBlue

                    For Each Point As DataPoint In chStaffTurnOver.Series(1).Points
                        Point.Color = Color.Cyan
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(1).Color = Color.Cyan

                    For Each Point As DataPoint In chStaffTurnOver.Series(2).Points
                        Point.Color = Color.Red
                    Next
                    If chStaffTurnOver.Legends(0).CustomItems.Count > 0 Then chStaffTurnOver.Legends(0).CustomItems(2).Color = Color.Red
            End Select


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvStaff_SelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvSalAnalysis_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSalAnalysis.CellContentClick, dgvSalAnalysis.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvSalAnalysis.IsCurrentCellDirty Then
                Me.dgvSalAnalysis.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvSalAnalysis.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvSalAnalysis.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvSalAnalysis.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvSalAnalysis.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvSalAnalysis.RowCount - 1
                            If CInt(dgvSalAnalysis.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvSalAnalysis.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvSalAnalysis.Rows(i).Visible = False Then
                                    dgvSalAnalysis.Rows(i).Visible = True
                                Else
                                    dgvSalAnalysis.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSalAnalysis_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvSalAnalysis_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSalAnalysis.DataBindingComplete
        Try
            For i As Integer = 0 To dgvSalAnalysis.RowCount - 1
                If CBool(dgvSalAnalysis.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    If dgvSalAnalysis.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgvSalAnalysis.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        dgvSalAnalysis.Rows(i).DefaultCellStyle.BackColor = Color.Gray
                        dgvSalAnalysis.Rows(i).DefaultCellStyle.ForeColor = Color.White
                    End If
                Else
                    dgvSalAnalysis.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSalAnalysis_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub dgvEmpDates_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpDates.CellContentClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If dgvEmpDates.IsCurrentCellDirty Then
                dgvEmpDates.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvEmpDates.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvEmpDates.Rows(e.RowIndex).Cells(objdgcolhEmpCollapse.Index).Value Is "-" Then
                            dgvEmpDates.Rows(e.RowIndex).Cells(objdgcolhEmpCollapse.Index).Value = "+"
                        Else
                            dgvEmpDates.Rows(e.RowIndex).Cells(objdgcolhEmpCollapse.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvEmpDates.RowCount - 1
                            If CInt(dgvEmpDates.Rows(e.RowIndex).Cells(objdgcolhEDatesGrpId.Index).Value) = CInt(dgvEmpDates.Rows(i).Cells(objdgcolhEDatesGrpId.Index).Value) Then
                                If dgvEmpDates.Rows(i).Visible = False Then
                                    dgvEmpDates.Rows(i).Visible = True
                                Else
                                    dgvEmpDates.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpDates_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvEmpDates_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEmpDates.DataError

    End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region

#End Region

#Region " Button's Events "

    Private Sub objbtnHRNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnHRNext.Click
        Try
            If intHRSlidingCnt > 1 Then Exit Sub
            intHRSlidingCnt += 1

            objbtnHRNext.Enabled = False
            objbtnHRPrevoius.Enabled = True

            Select Case intHRSlidingCnt
                Case 1
                    animateWin(objSpcStaff, objSpcTrainingCost, 1)
                    intHRSlidingCnt += 1
                    objSpcTrainingCost.Invalidate()
                    cboTDivision.Invalidate()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnHRNext_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnHRPrevoius_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnHRPrevoius.Click
        Try
            If intHRSlidingCnt <= 0 Then Exit Sub
            intHRSlidingCnt -= 1

            objbtnHRNext.Enabled = True
            objbtnHRPrevoius.Enabled = False

            Select Case intHRSlidingCnt
                Case 1
                    animateWin(objSpcTrainingCost, objSpcStaff, 2)
                    intHRSlidingCnt = 0
                    objSpcStaff.Invalidate()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnHRPrevoius_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnLTNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLTNext.Click
        Try
            If intLTSlidingCnt > 1 Then Exit Sub
            intLTSlidingCnt += 1

            objbtnLTNext.Enabled = False
            objbtnLTPrevious.Enabled = True

            Select Case intLTSlidingCnt
                Case 1
                    animateWin(objSpcLeave, objSpcLoginSummary, 1)
                    intLTSlidingCnt += 1
                    objSpcLoginSummary.Invalidate()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnLTNext_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnLTPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLTPrevious.Click
        Try
            If intLTSlidingCnt <= 0 Then Exit Sub
            intLTSlidingCnt -= 1

            objbtnLTNext.Enabled = True
            objbtnLTPrevious.Enabled = False

            Select Case intLTSlidingCnt
                Case 1
                    animateWin(objSpcLoginSummary, objSpcLeave, 2)
                    intLTSlidingCnt = 0
                    objSpcLeave.Invalidate()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnLTPrevious_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (25 Apr 2019) -- Start
    'Enhancement - 76.1 - Performance enhancement of aruti main view.
    Private Sub bdgProbation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgProbation.Click _
                                                                                        , bdgSuspension.Click _
                                                                                        , bdgAppointed.Click _
                                                                                        , bdgConfirmed.Click _
                                                                                        , bdgBirthDays.Click _
                                                                                        , bdgAnniversary.Click _
                                                                                        , bdgContractEnded.Click _
                                                                                        , bdgRetiredToday.Click _
                                                                                        , bdgRetiredForcast.Click _
                                                                                        , bdgEOCForcast.Click _
                                                                                        , bdgELCForcast.Click

        Dim bdg As Badge = CType(DirectCast(sender, System.Windows.Forms.Label).Parent.Parent, Badge)
        Try
            If dsEmpDatesCount Is Nothing Then Exit Try

            Dim dtTable As DataTable = Nothing
            Dim intID As Integer = 0
            Select Case bdg.Name
                Case bdgProbation.Name
                    intID = clsDashboard_Class.enEmpDates.Probation_Dates

                Case bdgSuspension.Name
                    intID = clsDashboard_Class.enEmpDates.Suspension_Dates

                Case bdgAppointed.Name
                    intID = clsDashboard_Class.enEmpDates.Appointment_Dates

                Case bdgConfirmed.Name
                    intID = clsDashboard_Class.enEmpDates.Confirmation_Dates

                Case bdgBirthDays.Name
                    intID = clsDashboard_Class.enEmpDates.BirthDates

                Case bdgAnniversary.Name
                    intID = clsDashboard_Class.enEmpDates.Anniversary_Dates

                Case bdgContractEnded.Name
                    intID = clsDashboard_Class.enEmpDates.Contract_Ending_Dates

                Case bdgRetiredToday.Name
                    intID = clsDashboard_Class.enEmpDates.TodayRetirement_Dates

                Case bdgRetiredForcast.Name
                    intID = clsDashboard_Class.enEmpDates.ForcastedRetirement_Dates

                Case bdgEOCForcast.Name
                    intID = clsDashboard_Class.enEmpDates.ForcastedEOC_Dates

                Case bdgELCForcast.Name
                    intID = clsDashboard_Class.enEmpDates.ForcastedELC_Dates

            End Select

            Dim dr() As DataRow = dsEmpDatesCount.Tables(0).Select("ID = " & intID & " ")
            If dr.Length > 0 Then
                dtTable = dr.CopyToDataTable
            Else
                dtTable = New DataTable
                'dtTable.Columns.Add("ID", System.Type.GetType("System.Int32")).DefaultValue = 0
                'dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("Emp Name", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Emp Code", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Date", System.Type.GetType("System.String")).DefaultValue = ""

            End If

            If dtTable.Columns.Contains("ID") = True Then
                dtTable.Columns.Remove("ID")
            End If
            If dtTable.Columns.Contains("EmpId") = True Then
                dtTable.Columns.Remove("EmpId")
            End If
            If dtTable.Columns.Contains("sortdate") = True Then
                dtTable.Columns.Remove("sortdate")
            End If
            dtTable.AsEnumerable().ToList.ForEach(Function(x) updt(x))

            Dim objCommon As New frmCommonValidationList
            objCommon.Text = bdg.Text_Caption
            objCommon.displayDialog(False, bdg.Text_Caption, dtTable)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, bdg.Name & "_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub bdgPendingAccrueLeave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgPendingAccrueLeave.Click


        Try
            If dsPendingLeaveCount Is Nothing Then Exit Try

            Dim dtTable As DataTable = Nothing
            Dim dr() As DataRow = dsPendingLeaveCount.Tables(0).Select("ID = " & clsDashboard_Class.enEmpDates.PendingAccrueLeaves & " ")
            If dr.Length > 0 Then
                dtTable = dr.CopyToDataTable
            Else
                dtTable = New DataTable
                'dtTable.Columns.Add("ID", System.Type.GetType("System.Int32")).DefaultValue = 0
                'dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("Emp Name", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Emp Code", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Leave Type", System.Type.GetType("System.String")).DefaultValue = ""

            End If

            If dtTable.Columns.Contains("ID") = True Then
                dtTable.Columns.Remove("ID")
            End If
            If dtTable.Columns.Contains("EmpId") = True Then
                dtTable.Columns.Remove("EmpId")
            End If
            If dtTable.Columns.Contains("sortdate") = True Then
                dtTable.Columns.Remove("sortdate")
            End If


            Dim objCommon As New frmCommonValidationList
            objCommon.Text = bdgPendingAccrueLeave.Text_Caption
            objCommon.displayDialog(False, bdgPendingAccrueLeave.Text_Caption, dtTable)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgPendingAccrueLeave_Click", mstrModuleName)
        End Try
    End Sub

    Private Function updt(ByVal dr As DataRow) As Boolean
        Try
            dr.Item("Date") = Format(eZeeDate.convertDate(dr.Item("Date").ToString), "dd-MMM-yyyy")
            Return True
        Catch ex As Exception

        End Try
    End Function
    'Sohail (25 Apr 2019) -- End

#End Region


    'Pinkal (28-Feb-2014) -- Start
    'Enhancement : TRA Changes

#Region "TabControl Event"

    Private Sub tabcDashBoard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabcDashBoard.SelectedIndexChanged
        Try
            Me.SuspendLayout()
            tabcDashBoard.Focus()
            If tabcDashBoard.SelectedTab.Name = tabEmpDates.Name Then
                'Sohail (25 Apr 2019) -- Start
                'Enhancement - 76.1 - Performance enhancement of aruti main view.
                'If dgvEmpDates.DataSource Is Nothing OrElse dgvEmpDates.RowCount <= 0 Then
                If dsEmpDatesCount Is Nothing OrElse dsEmpDatesCount.Tables(0).Rows.Count <= 0 Then
                    'Sohail (25 Apr 2019) -- End
                    gbEmployeeDates.Dock = DockStyle.Fill
                    'If dsCollection.Tables.Contains(clsDashboard_Class.enDashBoardListIndex.EMPLOYEE_DATES.ToString.ToUpper) = False Then 'Sohail (25 Apr 2019)
                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'dsCollection.Tables.Add(objDash.EmployeeDates(clsDashboard_Class.enDashBoardListIndex.EMPLOYEE_DATES.ToString.ToUpper).Tables(0).Copy)
                    'Sohail (25 Apr 2019) -- Start
                    'Enhancement - 76.1 - Performance enhancement of aruti main view.
                    'dsCollection.Tables.Add(objDash.EmployeeDates(clsDashboard_Class.enDashBoardListIndex.EMPLOYEE_DATES.ToString.ToUpper).Tables(0).Copy)
                    'Sohail (25 Apr 2019) -- End
                        'S.SANDEEP [04 JUN 2015] -- END
                        Call GeneRateDataGrid(enDashModule.DSH_HUMANRESOURCE, dgvEmpDates)
                    'End If 'Sohail (25 Apr 2019)
                    tabcDashBoard.SelectedTab = tabEmpDates
                End If

            ElseIf tabcDashBoard.SelectedTab.Name = tabpHR.Name Then
                If dgvStaff.DataSource Is Nothing OrElse dgvStaff.RowCount <= 0 Then
                    objSpcStaff.Dock = DockStyle.Fill
                    objSpcStaff.BringToFront()
                    If dsCollection.Tables.Contains(clsDashboard_Class.enDashBoardListIndex.STAFF_LIST.ToString.ToUpper) = False Then
                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'dsCollection.Tables.Add(objDash.EmployeeDates(clsDashboard_Class.enDashBoardListIndex.STAFF_LIST.ToString.ToUpper).Tables(0).Copy)
                        'Sohail (25 Apr 2019) -- Start
                        'Enhancement - 76.1 - Performance enhancement of aruti main view.
                        'dsCollection.Tables.Add(objDash.EmployeeDates(clsDashboard_Class.enDashBoardListIndex.STAFF_LIST.ToString.ToUpper).Tables(0).Copy)
                        'Sohail (25 Apr 2019) -- End
                        'S.SANDEEP [04 JUN 2015] -- END

                        Call GeneRateDataGrid(enDashModule.DSH_HUMANRESOURCE, dgvStaff)
                    End If
                    tabcDashBoard.SelectedTab = tabpHR
                End If

            ElseIf tabcDashBoard.SelectedTab.Name = tabpTraining_Cost.Name Then
                If dgvTrainingCosting.DataSource Is Nothing OrElse dgvTrainingCosting.RowCount <= 0 Then
                    objSpcTrainingCost.Dock = DockStyle.Fill
                    If dsCollection.Tables.Contains(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper) = False Then
                        dsCollection.Tables.Add(objDash.Training_Costing(clsDashboard_Class.enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper).Tables(0).Copy)
                        Call GeneRateDataGrid(enDashModule.DSH_HUMANRESOURCE, dgvTrainingCosting)
                    End If
                    tabcDashBoard.SelectedTab = tabpTraining_Cost
                End If

            ElseIf tabcDashBoard.SelectedTab.Name = tabpPayroll.Name Then
                If dgvSalAnalysis.DataSource Is Nothing OrElse dgvSalAnalysis.RowCount <= 0 Then
                    objSpcSalAnalysis.Dock = DockStyle.Fill
                    objSpcSalAnalysis.BringToFront()
                    If dsCollection.Tables.Contains(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper) = False Then
                        dsCollection.Tables.Add(objDash.Salary_Analysis(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).Tables(0).Copy)
                        Call GeneRateDataGrid(enDashModule.DSH_PAYROLL, dgvSalAnalysis)
                    End If
                    tabcDashBoard.SelectedTab = tabpPayroll
                End If

            ElseIf tabcDashBoard.SelectedTab.Name = tabpLeaveTnA.Name Then
                If dgvLeave.DataSource Is Nothing OrElse dgvLeave.RowCount <= 0 Then
                    objSpcLeave.Dock = DockStyle.Fill
                    objSpcLeave.BringToFront()
                    If dsCollection.Tables.Contains(clsDashboard_Class.enDashBoardListIndex.LEAVE_LIST.ToString.ToUpper) = False Then
                        dsCollection.Tables.Add(objDash.Leave_Analysis(clsDashboard_Class.enDashBoardListIndex.LEAVE_LIST.ToString.ToUpper).Tables(0).Copy)
                        mDicLeaveSeries.Clear()
                        Call GeneRateDataGrid(enDashModule.DSH_LEEAVETNA, dgvLeave)
                    End If
                    tabcDashBoard.SelectedTab = tabpLeaveTnA
                End If
                If dgvLoginSummary.DataSource Is Nothing OrElse dgvLoginSummary.RowCount <= 0 Then
                    objSpcLoginSummary.Dock = DockStyle.Fill
                    If dsCollection.Tables.Contains(clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper) = False Then
                        dsCollection.Tables.Add(objDash.Get_Login_Summary(ConfigParameter._Object._CurrentDateAndTime, clsDashboard_Class.enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper).Tables(0).Copy)
                        Call GeneRateDataGrid(enDashModule.DSH_LEEAVETNA, dgvLoginSummary)
                    End If
                    tabcDashBoard.SelectedTab = tabpLeaveTnA
                End If

            End If
            Me.ResumeLayout()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tabcDashBoard_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (28-Feb-2014) -- End

    
End Class