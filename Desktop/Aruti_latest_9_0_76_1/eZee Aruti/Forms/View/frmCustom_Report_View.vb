﻿#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReport
Imports ArutiReport.Engine
Imports ArutiReport.Engine.Library

#End Region


Public Class frmCustom_Report_View
    Private mstrModuleName As String = "frmCustom_Report_View"
    Dim mstrTemplateName As String = "AddNew"
    Dim mstrXmlPath As String = ""
    Dim mstrQuery As String = ""
    'S.SANDEEP [ 19 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim mstrDatabase_Name As String = String.Empty
    'S.SANDEEP [ 19 JUNE 2012 ] -- END

#Region " Private Methods "

    Private Sub FillList()
        Try
            Dim dsList As New DataSet
            dsList.ReadXml(AppSettings._Object._ApplicationPath & "\TemplateMst")
            lvTemplate.Items.Clear()
            Dim lvItem As ListViewItem
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                lvItem = New ListViewItem
                lvItem.ImageIndex = 0
                lvItem.Text = dsList.Tables(0).Rows(i)("PrintType")
                lvItem.Tag = dsList.Tables(0).Rows(i)("trnasid")
                lvItem.SubItems.Add(dsList.Tables(0).Rows(i)("MstXMLPath"))
                lvTemplate.Items.Add(lvItem)
            Next
            If User._Object.Privilege._AllowToAddTemplate = True Then
                lvItem = New ListViewItem
                lvItem.ImageIndex = 1
                lvItem.Text = "Add New"
                lvItem.Tag = -1
                lvItem.ForeColor = Color.Blue
                lvItem.Selected = True
                lvTemplate.Items.Add(lvItem)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub Insert()
        Try
            Dim dsMst As New DataSet
            Dim sMst As String = AppSettings._Object._ApplicationPath & "\TemplateMst"
            Dim intTemplateId As Integer = 0

            dsMst.ReadXml(AppSettings._Object._ApplicationPath & "TemplateMst")
            With dsMst.Tables(0)
                If .Rows.Count > 0 Then
                    .Rows.Add()
                    intTemplateId = .Compute("max(trnasid)", "") + 1
                Else
                    .Rows.Add()
                    intTemplateId = .Rows.Count + 1
                End If
                .Rows(.Rows.Count - 1)("trnasid") = intTemplateId
                .Rows(.Rows.Count - 1)("PrintType") = mstrTemplateName
                .Rows(.Rows.Count - 1)("ItemPrint") = 6.0
                .Rows(.Rows.Count - 1)("MstXMLPath") = mstrXmlPath
                .Rows(.Rows.Count - 1)("DetXMLPath") = ""
                .Rows(.Rows.Count - 1)("HeightCompHeader") = 90
                .Rows(.Rows.Count - 1)("HeightHeader") = 70
                .Rows(.Rows.Count - 1)("HeightDetail") = 90
                .Rows(.Rows.Count - 1)("HeightDetailFooter") = 43
                .Rows(.Rows.Count - 1)("HeightFooter") = 103
                .Rows(.Rows.Count - 1)("HeightGroup1") = 32
                .Rows(.Rows.Count - 1)("HeightGroupFooter1") = 30
                .Rows(.Rows.Count - 1)("Width") = 956
                .Rows(.Rows.Count - 1)("SuppressHeader") = False
                .Rows(.Rows.Count - 1)("SuppressGroup1") = False
                .Rows(.Rows.Count - 1)("SuppressDetail") = False
                .Rows(.Rows.Count - 1)("SuppressGroupFooter1") = False
                .Rows(.Rows.Count - 1)("SuppressDetailFooter") = False
                .Rows(.Rows.Count - 1)("SuppressFooter") = False
                .Rows(.Rows.Count - 1)("SuppressCompHeader") = False
                .Rows(.Rows.Count - 1)("CanvasWidth") = 10
                .Rows(.Rows.Count - 1)("Landspace") = False
                .Rows(.Rows.Count - 1)("RepHeader") = True
                .Rows(.Rows.Count - 1)("MultPrint") = 1
                .Rows(.Rows.Count - 1)("CanvasHeight") = 12
                .Rows(.Rows.Count - 1)("Top") = 0.25
                .Rows(.Rows.Count - 1)("Left") = 0.25
                .Rows(.Rows.Count - 1)("Right") = 0.25
                .Rows(.Rows.Count - 1)("Bottom") = 0.25
                .Rows(.Rows.Count - 1)("PaperSize") = "Manual"
                .Rows(.Rows.Count - 1)("PageWise") = False
                .Rows(.Rows.Count - 1)("Query") = mstrQuery
            End With
            dsMst.WriteXml(sMst, XmlWriteMode.WriteSchema)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Insert", mstrModuleName)
        End Try
    End Sub

    'Dipti (06 June 2012) - Start
    Private Sub CopyTemplate(ByVal strDestinationTemplate As String, ByVal strSourceTemplate As String)
        Try
            Dim dsCopy As New DataSet

            Dim strSelect As String = ""
            Dim strIndex As String = ""
            Dim blnFlag As Boolean = True
            Dim drRows As DataRow()

            '****Copy Master
            Dim dsMaster As New DataSet
            dsMaster.ReadXml(AppSettings._Object._ApplicationPath & "TemplateMst")
            drRows = dsMaster.Tables(0).Select("PrintType = '" & strSourceTemplate & "'")

            If drRows.Length > 0 Then
                '**** Copy DataFile
                IO.File.Copy(AppSettings._Object._ApplicationPath & "DataXML\" & strSourceTemplate, AppSettings._Object._ApplicationPath & "DataXML\" & strDestinationTemplate)

                Dim intDestinationTemplateId As Integer = dsMaster.Tables(0).Compute("max(trnasid)", "") + 1
                Dim intSourceTemplateId As Integer = -1
                Dim intNextId As Integer = 0

                Dim drRow As DataRow = Nothing

                For Each dRow As DataRow In drRows
                    drRow = dsMaster.Tables(0).NewRow
                    intSourceTemplateId = dRow.Item("trnasid")
                    drRow("trnasid") = intDestinationTemplateId
                    drRow("PrintType") = strDestinationTemplate
                    drRow("ItemPrint") = dRow.Item("ItemPrint")
                    drRow("MstXMLPath") = strDestinationTemplate
                    drRow("DetXMLPath") = dRow.Item("DetXMLPath")
                    drRow("HeightCompHeader") = dRow.Item("HeightCompHeader")
                    drRow("HeightHeader") = dRow.Item("HeightHeader")
                    drRow("HeightDetail") = dRow.Item("HeightDetail")
                    drRow("HeightDetailFooter") = dRow.Item("HeightDetailFooter")
                    drRow("HeightFooter") = dRow.Item("HeightFooter")
                    drRow("HeightGroup1") = dRow.Item("HeightGroup1")
                    drRow("HeightGroupFooter1") = dRow.Item("HeightGroupFooter1")
                    drRow("Width") = dRow.Item("Width")
                    drRow("SuppressHeader") = dRow.Item("SuppressHeader")
                    drRow("SuppressGroup1") = dRow.Item("SuppressGroup1")
                    drRow("SuppressDetail") = dRow.Item("SuppressDetail")
                    drRow("SuppressGroupFooter1") = dRow.Item("SuppressGroupFooter1")
                    drRow("SuppressDetailFooter") = dRow.Item("SuppressDetailFooter")
                    drRow("SuppressFooter") = dRow.Item("SuppressFooter")
                    drRow("SuppressCompHeader") = dRow.Item("SuppressCompHeader")
                    drRow("CanvasWidth") = dRow.Item("CanvasWidth")
                    drRow("Landspace") = dRow.Item("Landspace")
                    drRow("RepHeader") = dRow.Item("RepHeader")
                    drRow("MultPrint") = dRow.Item("MultPrint")
                    drRow("CanvasHeight") = dRow.Item("CanvasHeight")
                    drRow("Top") = dRow.Item("Top")
                    drRow("Left") = dRow.Item("Left")
                    drRow("Right") = dRow.Item("Right")
                    drRow("Bottom") = dRow.Item("Bottom")
                    drRow("PaperSize") = dRow.Item("PaperSize")
                    drRow("PageWise") = dRow.Item("PageWise")
                    drRow("Query") = dRow.Item("Query")
                    dsMaster.Tables(0).Rows.Add(drRow)
                Next
                dsMaster.WriteXml(AppSettings._Object._ApplicationPath & "TemplateMst", XmlWriteMode.WriteSchema)

                '****Copy Details
                Dim dsDetail As New DataSet
                dsDetail.ReadXml(AppSettings._Object._ApplicationPath & "TemplateDet")
                drRows = dsDetail.Tables(0).Select("TypeId = '" & intSourceTemplateId & "'")
                intNextId = dsDetail.Tables(0).Compute("max(trnasid)", "")

                For Each dr As DataRow In drRows
                    drRow = dsDetail.Tables(0).NewRow
                    If dr.Item("FieldType") = "0" Then ' Text
                        drRow("Trnasid") = intNextId + 1
                        drRow("TypeId") = intDestinationTemplateId
                        drRow("Name") = dr.Item("Name")
                        drRow("Text") = dr.Item("Text")
                        drRow("Tag") = dr.Item("Tag")
                        drRow("FieldId") = dr.Item("FieldId")
                        drRow("SectionId") = dr.Item("SectionId")
                        'drRow("Type") = dr.Item("Type")
                        drRow("TextAlign") = dr.Item("TextAlign")
                        drRow("Style") = dr.Item("Style")
                        drRow("FieldType") = dr.Item("FieldType")
                        drRow("FieldName") = dr.Item("FieldName")
                        drRow("Value") = dr.Item("Value")
                        'drRow("Vertical") = dr.Item("Vertical")
                        drRow("LineStyle") = dr.Item("LineStyle")
                        drRow("LineThickness") = dr.Item("LineThickness")
                        drRow("LineColor") = dr.Item("LineColor")
                        drRow("AllR") = dr.Item("AllR")
                        drRow("X") = dr.Item("X")
                        drRow("Y") = dr.Item("Y")
                        drRow("Width") = dr.Item("Width")
                        drRow("Height") = dr.Item("Height")
                        drRow("Font") = dr.Item("Font")
                        drRow("Size") = dr.Item("Size")
                        'drRow("Bold") = dr.Item("Bold")
                        'drRow("Italic") = dr.Item("Italic")
                        'drRow("UnderLine") = dr.Item("UnderLine")
                        drRow("ForeColor") = dr.Item("ForeColor")
                        drRow("BackColor") = dr.Item("BackColor")
                        drRow("panel") = dr.Item("panel")
                        drRow("Format") = dr.Item("Format")
                        'drRow("FilePath") = dr.Item("FilePath")
                        'drRow("Picture") = dr.Item("Picture")           
                        drRow("Suppress") = dr.Item("Suppress")
                        drRow("CanDraw") = dr.Item("CanDraw")
                        drRow("DupSupp") = dr.Item("DupSupp")
                    ElseIf dr.Item("FieldType") = 1 Then 'DB fields
                        drRow("TrnasId") = intNextId + 1
                        drRow("TypeId") = intDestinationTemplateId
                        drRow("Name") = dr.Item("Name")
                        drRow("Text") = dr.Item("Text")
                        drRow("Tag") = dr.Item("Tag")
                        drRow("FieldId") = dr.Item("FieldId")
                        drRow("SectionId") = dr.Item("SectionId")
                        'drRow("Type") = dr.Item("Type")
                        drRow("TextAlign") = dr.Item("TextAlign")
                        drRow("Style") = dr.Item("Style")
                        drRow("FieldType") = dr.Item("FieldType")
                        drRow("FieldName") = dr.Item("FieldName")
                        drRow("Value") = dr.Item("Value")
                        'drRow("Vertical") = dr.Item("Vertical")
                        drRow("LineStyle") = dr.Item("LineStyle")
                        drRow("LineThickness") = dr.Item("LineThickness")
                        drRow("LineColor") = dr.Item("LineColor")
                        drRow("AllR") = dr.Item("AllR")
                        drRow("X") = dr.Item("X")
                        drRow("Y") = dr.Item("Y")
                        drRow("Width") = dr.Item("Width")
                        drRow("Height") = dr.Item("Height")
                        drRow("Font") = dr.Item("Font")
                        drRow("Size") = dr.Item("Size")
                        'drRow("Bold") = dr.Item("Bold")
                        'drRow("Italic") = dr.Item("Italic")
                        'drRow("UnderLine") = dr.Item("UnderLine")
                        drRow("ForeColor") = dr.Item("ForeColor")
                        drRow("BackColor") = dr.Item("BackColor")
                        drRow("panel") = dr.Item("panel")
                        drRow("Format") = dr.Item("Format")
                        'drRow("FilePath") = dr.Item("FilePath")
                        'drRow("Picture") = dr.Item("Picture")           
                        drRow("Suppress") = dr.Item("Suppress")
                        drRow("CanDraw") = dr.Item("CanDraw")
                        drRow("DupSupp") = dr.Item("DupSupp")
                    ElseIf dr.Item("FieldType") = 4 Then 'Rectangle
                        drRow("TrnasId") = intNextId + 1
                        drRow("TypeId") = intDestinationTemplateId
                        drRow("Name") = dr.Item("Name")
                        drRow("Text") = dr.Item("Text")
                        drRow("Tag") = dr.Item("Tag")
                        drRow("FieldId") = dr.Item("FieldId")
                        drRow("SectionId") = dr.Item("SectionId")
                        'drRow("Type") = dr.Item("Type")
                        drRow("TextAlign") = dr.Item("TextAlign")
                        drRow("Style") = dr.Item("Style")
                        drRow("FieldType") = dr.Item("FieldType")
                        drRow("FieldName") = dr.Item("FieldName")
                        drRow("Value") = dr.Item("Value")
                        'drRow("Vertical") = dr.Item("Vertical")
                        drRow("LineStyle") = dr.Item("LineStyle")
                        drRow("LineThickness") = dr.Item("LineThickness")
                        drRow("LineColor") = dr.Item("LineColor")
                        drRow("AllR") = dr.Item("AllR")
                        drRow("X") = dr.Item("X")
                        drRow("Y") = dr.Item("Y")
                        drRow("Width") = dr.Item("Width")
                        drRow("Height") = dr.Item("Height")
                        drRow("Font") = dr.Item("Font")
                        drRow("Size") = dr.Item("Size")
                        'drRow("Bold") = dr.Item("Bold")
                        'drRow("Italic") = dr.Item("Italic")
                        'drRow("UnderLine") = dr.Item("UnderLine")
                        drRow("ForeColor") = dr.Item("ForeColor")
                        drRow("BackColor") = dr.Item("BackColor")
                        drRow("panel") = dr.Item("panel")
                        drRow("Format") = dr.Item("Format")
                        'drRow("FilePath") = dr.Item("FilePath")
                        'drRow("Picture") = dr.Item("Picture")           
                        drRow("Suppress") = dr.Item("Suppress")
                        drRow("CanDraw") = dr.Item("CanDraw")
                        drRow("DupSupp") = dr.Item("DupSupp")
                    ElseIf dr.Item("FieldType") = 5 Then 'line
                        drRow("TrnasId") = intNextId + 1
                        drRow("TypeId") = intDestinationTemplateId
                        drRow("Name") = dr.Item("Name")
                        drRow("Text") = dr.Item("Text")
                        drRow("Tag") = dr.Item("Tag")
                        drRow("FieldId") = dr.Item("FieldId")
                        drRow("SectionId") = dr.Item("SectionId")
                        'drRow("Type") = dr.Item("Type")
                        drRow("TextAlign") = dr.Item("TextAlign")
                        drRow("Style") = dr.Item("Style")
                        drRow("FieldType") = dr.Item("FieldType")
                        drRow("FieldName") = dr.Item("FieldName")
                        drRow("Value") = dr.Item("Value")
                        drRow("Vertical") = dr.Item("Vertical")
                        drRow("LineStyle") = dr.Item("LineStyle")
                        drRow("LineThickness") = dr.Item("LineThickness")
                        drRow("LineColor") = dr.Item("LineColor")
                        ' drRow("AllR") = dr.Item("AllR")
                        drRow("X") = dr.Item("X")
                        drRow("Y") = dr.Item("Y")
                        drRow("Width") = dr.Item("Width")
                        drRow("Height") = dr.Item("Height")
                        drRow("Font") = dr.Item("Font")
                        drRow("Size") = dr.Item("Size")
                        'drRow("Bold") = dr.Item("Bold")
                        'drRow("Italic") = dr.Item("Italic")
                        'drRow("UnderLine") = dr.Item("UnderLine")
                        drRow("ForeColor") = dr.Item("ForeColor")
                        drRow("BackColor") = dr.Item("BackColor")
                        drRow("panel") = dr.Item("panel")
                        'drRow("Format") = dr.Item("Format")
                        'drRow("FilePath") = dr.Item("FilePath")
                        'drRow("Picture") = dr.Item("Picture")           
                        drRow("Suppress") = dr.Item("Suppress")
                        'drRow("CanDraw") = dr.Item("CanDraw")
                        'drRow("DupSupp") = dr.Item("DupSupp")
                    ElseIf dr.Item("FieldType") = 6 Then ' Image
                        drRow("TrnasId") = intNextId + 1
                        drRow("TypeId") = intDestinationTemplateId
                        drRow("Name") = dr.Item("Name")
                        drRow("Text") = dr.Item("Text")
                        drRow("Tag") = dr.Item("Tag")
                        drRow("FieldId") = dr.Item("FieldId")
                        drRow("SectionId") = dr.Item("SectionId")
                        'drRow("Type") = dr.Item("Type")
                        drRow("TextAlign") = dr.Item("TextAlign")
                        drRow("Style") = dr.Item("Style")
                        drRow("FieldType") = dr.Item("FieldType")
                        drRow("FieldName") = dr.Item("FieldName")
                        drRow("Value") = dr.Item("Value")
                        'drRow("Vertical") = dr.Item("Vertical")
                        drRow("LineStyle") = dr.Item("LineStyle")
                        drRow("LineThickness") = dr.Item("LineThickness")
                        drRow("LineColor") = dr.Item("LineColor")
                        drRow("AllR") = dr.Item("AllR")
                        drRow("X") = dr.Item("X")
                        drRow("Y") = dr.Item("Y")
                        drRow("Width") = dr.Item("Width")
                        drRow("Height") = dr.Item("Height")
                        drRow("Font") = dr.Item("Font")
                        drRow("Size") = dr.Item("Size")
                        'drRow("Bold") = dr.Item("Bold")
                        'drRow("Italic") = dr.Item("Italic")
                        'drRow("UnderLine") = dr.Item("UnderLine")
                        drRow("ForeColor") = dr.Item("ForeColor")
                        drRow("BackColor") = dr.Item("BackColor")
                        drRow("panel") = dr.Item("panel")
                        'drRow("Format") = dr.Item("Format")
                        'drRow("FilePath") = dr.Item("FilePath")
                        drRow("Picture") = dr.Item("Picture")
                        drRow("Suppress") = dr.Item("Suppress")
                        'drRow("CanDraw") = dr.Item("CanDraw")
                        'drRow("DupSupp") = dr.Item("DupSupp")
                    End If
                    dsDetail.Tables(0).Rows.Add(drRow)
                    intNextId = intNextId + 1
                Next
                dsDetail.WriteXml(AppSettings._Object._ApplicationPath & "TemplateDet", XmlWriteMode.WriteSchema)

                '****Copy Formula
                Dim dsFormula As New DataSet
                dsFormula.ReadXml(AppSettings._Object._ApplicationPath & "TemplateFormula")
                drRows = dsFormula.Tables(0).Select("TypeId = '" & intSourceTemplateId & "'")
                intNextId = dsFormula.Tables(0).Compute("max(trnasid)", "")

                For Each dr As DataRow In drRows
                    drRow = dsFormula.Tables(0).NewRow
                    drRow("TrnasId") = intNextId + 1
                    drRow("TypeId") = intDestinationTemplateId
                    drRow("Form") = dr("Form")
                    drRow("FormulaName") = dr("FormulaName")
                    drRow("FormulaExp") = dr("FormulaExp")
                    dsFormula.Tables(0).Rows.Add(drRow)
                    intNextId = intNextId + 1
                Next
                dsFormula.WriteXml(AppSettings._Object._ApplicationPath & "TemplateFormula", XmlWriteMode.WriteSchema)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "CopyTemplate", mstrModuleName)
        End Try
    End Sub

    Private Sub RemoveTemplate(ByVal intTemplateId As Integer)
        Try

            Dim drRows As DataRow() = Nothing

            Dim dsMaster As New DataSet
            dsMaster.ReadXml(AppSettings._Object._ApplicationPath & "TemplateMst")
            drRows = dsMaster.Tables(0).Select("trnasid = '" & intTemplateId & "'")

            If drRows.Length > 0 Then
                '**** Delete DataFile
                IO.File.Delete(AppSettings._Object._ApplicationPath & "DataXml\" & drRows(0).Item("PrintType").ToString)

                '****Delete Master
                dsMaster.Tables(0).Rows.Remove(drRows(0))
                dsMaster.WriteXml(AppSettings._Object._ApplicationPath & "TemplateMst", XmlWriteMode.WriteSchema)

                '****Delete Details
                Dim dsDetail As New DataSet
                dsDetail.ReadXml(AppSettings._Object._ApplicationPath & "TemplateDet")
                drRows = dsDetail.Tables(0).Select("TypeId = '" & intTemplateId & "'")

                For Each dr As DataRow In drRows
                    dsDetail.Tables(0).Rows.Remove(dr)
                Next
                dsDetail.WriteXml(AppSettings._Object._ApplicationPath & "TemplateDet", XmlWriteMode.WriteSchema)

                '****Delete Formula
                Dim dsFormula As New DataSet
                dsFormula.ReadXml(AppSettings._Object._ApplicationPath & "TemplateFormula")
                drRows = dsFormula.Tables(0).Select("TypeId = '" & intTemplateId & "'")

                For Each dr As DataRow In drRows
                    dsFormula.Tables(0).Rows.Remove(dr)
                Next
                dsFormula.WriteXml(AppSettings._Object._ApplicationPath & "TemplateFormula", XmlWriteMode.WriteSchema)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "RemoveTemplate", mstrModuleName)
        End Try
    End Sub
    'Dipti (06 June 2012) - End

    'S.SANDEEP [ 19 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            tsmibtnNew.Enabled = User._Object.Privilege._AllowToAddTemplate
            tsmiEdit.Enabled = User._Object.Privilege._AllowToEditTemplate
            tsmiDelete.Enabled = User._Object.Privilege._AllowToDeleteTemplate
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 19 JUNE 2012 ] -- END

#End Region

#Region " Form "

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 19 JUNE 2012 ] -- END

            Call FillList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmMain_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try
            If lvTemplate.SelectedItems.Count > 0 Then
                Dim frm As New Engine.Library.Form1Des
                frm.Template = lvTemplate.SelectedItems(0).Text
                frm.ShowMeNow(lvTemplate.SelectedItems(0).Tag, lvTemplate.SelectedItems(0).SubItems(1).Text)
                frm = Nothing
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnPreview_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Controls "

    Private Sub lvTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvTemplate.Click
        Try
            If lvTemplate.SelectedItems.Count > 0 Then
                If lvTemplate.SelectedItems(0).Tag <> -1 Then
                    mstrTemplateName = lvTemplate.SelectedItems(0).Text
                    Dim frm As New frmUsedDefineReport(mstrTemplateName)
                    frm.TopLevel = False
                    If pnlInformation.Controls.Count > 0 Then
                        pnlInformation.Controls.RemoveAt(0)
                        pnlInformation.Controls.Add(frm)
                    Else
                        pnlInformation.Controls.Add(frm)
                    End If
                    frm.Size = pnlInformation.Size
                    frm.Show()
                    frm.BringToFront()
                Else
                    If pnlInformation.Controls.Count > 0 Then
                        pnlInformation.Controls.RemoveAt(0)
                    End If
                    Me.Cursor = Cursors.WaitCursor
                    Call tsmibtnNew_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTemplate_SelectedIndexChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Private Sub lvTemplate_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvTemplate.DoubleClick
    '    Try

    '        If lvTemplate.SelectedItems.Count > 0 Then
    '            If lvTemplate.SelectedItems(0).Tag = -1 Then
    '                Dim dsMst As New DataSet
    '                dsMst.ReadXml("TemplateMst")
    '                If eZeeMsgBox.Show("Type does not exists in database, do you want to create it?", enMsgBoxStyle.Question + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
    '                    Dim objfrm As New Library.frmNewWizard
    '                    objfrm._Type = "Add New"
    '                    objfrm._MasterTable = dsMst
    '                    objfrm.BringToFront()
    '                    objfrm.ShowDialog()
    '                    If objfrm._IsFinish Then
    '                        mstrTemplateName = objfrm._Type
    '                        mstrXmlPath = objfrm._XmlPath
    '                        mstrQuery = objfrm._Query
    '                        objfrm = Nothing
    '                        Call Insert()
    '                    Else
    '                        objfrm = Nothing
    '                        Exit Sub
    '                    End If
    '                Else
    '                    Exit Sub
    '                End If
    '            Else
    '                mstrTemplateName = lvTemplate.SelectedItems(0).Text
    '            End If

    '            Dim frm As New eZeeReport.Engine.Library.frmReportDesigner_New
    '            frm.TopLevel = False
    '            frm.Template = mstrTemplateName
    '            pnlMain.Controls.Add(frm)
    '            frm.WindowState = FormWindowState.Maximized
    '            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.None
    '            frm._Action = Global.enAction.EDIT_ONE

    '            frm.Show()

    '            frm = Nothing

    '            ' Call FillList()
    '        End If
    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "lvTemplate_DoubleClick", mstrModuleName)
    '    End Try
    'End Sub


    'Dipti (06 June 2012) - Start
    Private Sub tsmibtnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmibtnNew.Click
        Try
            Dim dsMst As New DataSet
            dsMst.ReadXml(AppSettings._Object._ApplicationPath & "TemplateMst")
            Dim objfrm As New frmNewWizard
            objfrm._Type = "Add New"
            objfrm._MasterTable = dsMst
            objfrm.BringToFront()
            objfrm.ShowDialog()
            If objfrm._IsFinish Then
                mstrTemplateName = objfrm._Type
                mstrXmlPath = objfrm._XmlPath
                mstrQuery = objfrm._Query
                'S.SANDEEP [ 19 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mstrDatabase_Name = objfrm._Database_Name
                'S.SANDEEP [ 19 JUNE 2012 ] -- END
                objfrm = Nothing
                Call Insert()
                'S.SANDEEP [ 19 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim objUDR As New clsUserDefineReport
                objUDR._Database_Name = mstrDatabase_Name
                objUDR._Isactive = True
                objUDR._Template_Detail = ""
                objUDR._Template_Formula = ""
                objUDR._Template_Master = ""
                objUDR._Template_Name = mstrTemplateName
                objUDR._Template_Query = mstrQuery
                objUDR._Userunkid = User._Object._Userunkid
                If objUDR.Insert() = False Then
                    eZeeMsgBox.Show(objUDR._Message, enMsgBoxStyle.Information)
                End If
                'S.SANDEEP [ 19 JUNE 2012 ] -- END
            Else
                objfrm = Nothing
                Exit Sub
            End If

            Dim frm As New ArutiReport.Engine.Library.frmReportDesigner_New
            frm.TopLevel = False
            frm.Template = mstrTemplateName
            pnlMain.Controls.Add(frm)
            frm.WindowState = FormWindowState.Maximized
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.None
            frm._Action = Global.enAction.EDIT_ONE

            frm.Show()

            frm = Nothing

            Call FillList()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "tsmibtnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmiEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiEdit.Click
        Try
            If lvTemplate.SelectedItems.Count > 0 Then

                If lvTemplate.SelectedItems(0).Tag = -1 Then
                    eZeeMsgBox.Show("Please select atleast one template from the above list to edit.", enMsgBoxStyle.Information)
                    Exit Sub
                End If

                mstrTemplateName = lvTemplate.SelectedItems(0).Text

                Dim frm As New frmReportDesigner_New
                frm.TopLevel = False
                frm.Template = mstrTemplateName
                pnlMain.Controls.Add(frm)
                frm.WindowState = FormWindowState.Maximized
                frm.FormBorderStyle = Windows.Forms.FormBorderStyle.None
                frm._Action = Global.enAction.EDIT_ONE

                frm.Show()

                frm = Nothing

                Call FillList()
            Else
                eZeeMsgBox.Show("Please select atleast one template from the above list to edit.", enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "tsmiEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmiDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiDelete.Click
        Try
            If lvTemplate.SelectedItems.Count > 0 Then
                'S.SANDEEP [ 19 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim dsData As New DataSet : Dim objUDR As New clsUserDefineReport
                Dim intReportid As Integer = -1
                mstrTemplateName = lvTemplate.SelectedItems(0).Text
                dsData = objUDR.GetList("List", True, -1, mstrTemplateName)
                If dsData.Tables("List").Rows.Count > 0 Then
                    intReportid = dsData.Tables("List").Rows(0)("reportunkid")
                End If
                'S.SANDEEP [ 19 JUNE 2012 ] -- END
                If eZeeMsgBox.Show("Are you sure, you want to delete the selected template?", enMsgBoxStyle.Question + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
                    Call RemoveTemplate(lvTemplate.SelectedItems(0).Tag)
                    lvTemplate.SelectedItems(0).Remove()
                    eZeeMsgBox.Show("Selecte template deleted successfully.", enMsgBoxStyle.Information)
                    'S.SANDEEP [ 19 JUNE 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If objUDR.Delete(intReportid) = False Then
                        eZeeMsgBox.Show(objUDR._Message, enMsgBoxStyle.Information)
                    End If
                    'S.SANDEEP [ 19 JUNE 2012 ] -- END
                End If
                If pnlInformation.Controls.Count > 0 Then pnlInformation.Controls.RemoveAt(0)
            Else
                eZeeMsgBox.Show("Please select atleast one template from the above list to delete.", enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "tsmiDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmiCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiCopy.Click
        Try
            If lvTemplate.SelectedItems.Count > 0 Then
                Dim strTemplateName As String = ""
                Dim blnFlag As Boolean = False
                While blnFlag = False
                    strTemplateName = InputBox("Please enter the name of the new template", "Copy Template", "New")
                    If strTemplateName = "" Then
                        eZeeMsgBox.Show("Please enter the name for the new template.", enMsgBoxStyle.Information)
                        blnFlag = False
                        Continue While
                    Else
                        Dim dsMaster As New DataSet
                        dsMaster.ReadXml(AppSettings._Object._ApplicationPath & "TemplateMst")

                        If dsMaster.Tables(0).Select("PrintType = '" & strTemplateName & "'").Length > 0 Then
                            eZeeMsgBox.Show("Template with this name already exist. Please enter another name.", enMsgBoxStyle.Information)
                            blnFlag = False
                            Continue While
                        Else
                            blnFlag = True
                            Call CopyTemplate(strTemplateName, lvTemplate.SelectedItems(0).Text)
                            eZeeMsgBox.Show("Template copied successfully.", enMsgBoxStyle.Information)
                            Call FillList()
                            Exit While
                        End If
                    End If
                End While
            Else
                eZeeMsgBox.Show("Please select atleast one template from the above list to copy.", enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "tsmiCopy_Click", mstrModuleName)
        End Try
    End Sub
    'Dipti (06 June 2012) - End

#End Region

End Class

'Public Class frmCustom_Report_View1

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmCustom_Report_View"
'    Private objUDR As clsUserDefineReport

'#End Region

'#Region " Form's Events "

'    Private Sub frmCustom_Report_View_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            objUDR = New clsUserDefineReport
'            Call FillReports()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmCustom_Report_View_Load", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Controls "

'    Private Sub tsmibtnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmibtnNew.Click
'        Try
'            Dim blnFlag As Boolean = False
'            Dim objfrm As New Library.frmNewWizard
'            objfrm._Type = "Add New"
'            objfrm.BringToFront()
'            objfrm.ShowDialog()
'            If objfrm._IsFinish Then
'                objUDR._Template_Name = objfrm._Type
'                objUDR._Template_Query = objfrm._Query
'                objUDR._Userunkid = User._Object._Userunkid
'                objUDR._Database_Name = objfrm._Database_Name
'                objUDR._Isactive = True
'                objUDR._Template_Detail = ""
'                objUDR._Template_Formula = ""
'                objUDR._Template_Master = ""
'                objfrm = Nothing
'                If objUDR.Insert = False Then
'                    eZeeMsgBox.Show(objUDR._Message)
'                End If
'                blnFlag = True
'            Else
'                objfrm = Nothing
'                Exit Sub
'            End If

'            Dim frm As New eZeeReport.Engine.Library.frmReportDesigner_New
'            frm.TopLevel = False
'            frm.Template = objUDR._Template_Name
'            If blnFlag = True Then
'                frm.ReportUnkid = objUDR._Reportunkid
'            Else
'                frm.ReportUnkid = lvUserTemplates.SelectedItems(0).Tag
'            End If
'            pnlReportSummary.Controls.Add(frm)
'            frm.WindowState = FormWindowState.Maximized
'            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.None
'            frm._Action = Global.enAction.EDIT_ONE

'            frm.Show()

'            frm = Nothing

'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "tsmibtnNew_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub tsmiEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiEdit.Click
'        Try
'            If lvUserTemplates.SelectedItems.Count > 0 Then

'                Dim frm As New eZeeReport.Engine.Library.frmReportDesigner_New
'                frm.TopLevel = False
'                frm.Template = lvUserTemplates.SelectedItems(0).Text
'                frm.ReportUnkid = lvUserTemplates.SelectedItems(0).Tag
'                pnlReportSummary.Controls.Add(frm)
'                frm.WindowState = FormWindowState.Maximized
'                frm.FormBorderStyle = Windows.Forms.FormBorderStyle.None
'                frm._Action = Global.enAction.EDIT_ONE
'                frm.Show()
'                frm = Nothing

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "tsmiEdit_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Private Function "

'    Private Sub FillReports()
'        Dim dsList As New DataSet
'        Dim lvItem As ListViewItem
'        Try
'            dsList = objUDR.GetList("List")
'            lvUserTemplates.Items.Clear()

'            For Each dRow As DataRow In dsList.Tables("List").Rows
'                lvItem = New ListViewItem

'                lvItem.Text = dRow.Item("template_name").ToString
'                lvItem.Tag = dRow.Item("reportunkid")

'                lvUserTemplates.Items.Add(lvItem)
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillReports", mstrModuleName)
'            lvItem = Nothing : dsList = Nothing
'        End Try
'    End Sub

'#End Region

'End Class