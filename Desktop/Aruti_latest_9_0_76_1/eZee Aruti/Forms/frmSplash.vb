﻿Imports eZeeCommonLib
Imports Aruti.Data
Public NotInheritable Class frmSplash

    Private ReadOnly mstrModuleName As String = "frmSplash"
    Dim frm As frmLogin = Nothing

    'TODO: This form can easily be set as the splash screen for the application by going to the "Application" tab
    '  of the Project Designer ("Properties" under the "Project" menu).


    Private Sub SplashScreen1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up the dialog text at runtime according to the application's assembly information.  

        'TODO: Customize the application's assembly information in the "Application" pane of the project 
        '  properties dialog (under the "Project" menu).

        'Application title
        If My.Application.Info.Title <> "" Then
            'ApplicationTitle.Text = My.Application.Info.Title
        Else
            'If the application title is missing, use the application name, without the extension
            'ApplicationTitle.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If

        'Format the version information using the text set into the Version control at design time as the
        '  formatting string.  This allows for effective localization if desired.
        '  Build and revision information could be included by using the following code and changing the 
        '  Version control's designtime text to "Version {0}.{1:00}.{2}.{3}" or something similar.  See
        '  String.Format() in Help for more information.
        '
        '    Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)

        Version.Text = System.String.Format("Version {0}.{1:0}.{2:0}.{3:000}", My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)

        tmrSplash.Enabled = False
        'Copyright info
        Copyright.Text = My.Application.Info.Copyright
        Dim objmain As New Data.clsMain
        Try
            If objmain.DO_Main(enArutiApplicatinType.Aruti_Payroll) Then
                tmrSplash.Enabled = True
            Else
                Me.Close()
                Exit Sub
            End If

            'Dim objMasterData As New clsMasterData
            'Call objMasterData.InsertDefaultData()


        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SplashScreen1_Load", mstrModuleName)
        End Try
    End Sub


    'Sandeep | 17 JAN 2011 | -- START
    'Private Sub tmrSplash_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrSplash.Tick
    '    Try
    '        tmrSplash.Enabled = False
    '        Dim mblnAppRun As Boolean = True
    '        Dim objMasterData As New clsMasterData
    '        If objMasterData.IsCompanyCreated = False Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please configure Company first from Aruti Configuration."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        Do While mblnAppRun
    '            Try
    '                frm = New frmLogin
    '                'If gRightToLeft = True Then
    '                '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                '    frm.RightToLeftLayout = True
    '                'End If
    '                Dim blnResult As Boolean = frm.displayDialog
    '                If blnResult Then
    '                    Dim cfrm As New frmCommonSelection
    '                    Dim blnCompanyResult As Boolean = cfrm.displayDialog
    '                    cfrm = Nothing
    '                    If blnCompanyResult Then
    '                        Dim dfrm As New frmCommonSelection
    '                        dfrm._IsFinancial_Year = True
    '                        Dim blnDatabaseResult As Boolean = dfrm.displayDialog
    '                        dfrm = Nothing
    '                        If blnDatabaseResult Then
    '                            'If FinancialYear._Object._DatabaseName = "NPK_Jan2010_Dec2010" Then
    '                            '<TODO to be Removed Once The Database is Created> --Start
    '                            'eZeeCommonLib.eZeeDatabase.change_database("hrmseZeePayroll")
    '                            '<TODO to be Removed Once The Database is Created> --END 
    '                            'eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
    '                            'Else
    '                            eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)

    '                            'End If

    '                            Call objMasterData.InsertDefaultData(False)

    '                            Dim objExRate As New clsExchangeRate
    '                            objExRate._ExchangeRateunkid = 1
    '                            GUI.fmtCurrency = objExRate._fmtCurrency
    '                            objExRate = Nothing

    '                            gfrmMDI = New frmNewMDI

    '                            gfrmMDI.ShowDialog()
    '                            If gfrmMDI._Restart = True Then
    '                                eZeeCommonLib.eZeeDatabase.change_database("hrmsConfiguration")
    '                                mblnAppRun = True
    '                            Else
    '                                mblnAppRun = False
    '                            End If
    '                        Else
    '                            Application.Exit()
    '                            mblnAppRun = False
    '                        End If
    '                    Else
    '                        Application.Exit()
    '                        mblnAppRun = False
    '                    End If
    '                Else
    '                    Application.Exit()
    '                    mblnAppRun = False
    '                End If
    '            Catch ex As Exception
    '                DisplayError.Show("-1", ex.Message, "tmr", mstrModuleName)
    '            Finally
    '                frm = Nothing
    '                gfrmMDI = Nothing
    '            End Try
    '        Loop
    '    Catch ex As Exception
    '        Call DisplayError.Show(-1, ex.Message, "tmr", mstrModuleName)
    '    Finally
    '        Me.Close()
    '    End Try
    'End Sub

    Private Sub tmrSplash_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrSplash.Tick
        Try
            tmrSplash.Enabled = False
            Dim mblnAppRun As Boolean = True
            Dim objMasterData As New clsMasterData

            'Anjan (19 DEC 2011)-Start
            'ENHANCEMENT : TRA COMMENTS

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call objMasterData.InsertDefaultData(True)
            Call objMasterData.InsertDefaultData(True, 0)
            'S.SANDEEP [04 JUN 2015] -- END

            'Anjan (19 DEC 2011)-End 

            If objMasterData.IsGroupCreated = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please configure Company Group first from Aruti Configuration."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If objMasterData.IsCompanyCreated = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please configure Company first from Aruti Configuration."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Do While mblnAppRun
                Try

                    'If gRightToLeft = True Then   '<TODO PUT ONe Setting Of Left To Right IN CONFIG. SETTINGS>
                    '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    '    frm.RightToLeftLayout = True
                    'End If

                    'Pinkal (21-Jun-2012) -- Start
                    'Enhancement : TRA Changes

                    frm = New frmLogin
                    Dim blnResult As Boolean = False

                    Dim objPassword As New clsPassowdOptions
                    If objPassword._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                        Dim objUser As New clsUserAddEdit
                        'S.SANDEEP [24 SEP 2015] -- START
                        'Dim dsList As DataSet = objUser.GetList("List", True, True)
                        Dim dsList As DataSet = objUser.GetList("List")
                        'S.SANDEEP [04 SEP 2015] -- END


                        Dim drRow As DataRow() = dsList.Tables(0).Select("username='" & Environment.UserName.ToString() & "' AND userdomain ='" & Environment.UserDomainName.ToString() & "'")
                        If drRow.Length > 0 Then
                            blnResult = True
                            User._Object._Userunkid = CInt(drRow(0)("userunkid"))
                        Else
                            blnResult = frm.displayDialog
                        End If
                    Else
                        blnResult = frm.displayDialog
                    End If

                    'Pinkal (21-Jun-2012) -- End



                    If blnResult Then
                        Dim cfrm As New frmCommonSelection
                        Dim blnCompanyResult As Boolean = cfrm.displayDialog
                        cfrm = Nothing
                        If blnCompanyResult Then
                            Dim dfrm As New frmCommonSelection
                            dfrm._IsFinancial_Year = True
                            Dim blnDatabaseResult As Boolean = dfrm.displayDialog
                            dfrm = Nothing
                            If blnDatabaseResult Then
                                eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                                gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll)
                                gobjLocalization._LangId = User._Object._Languageunkid

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'Call objMasterData.InsertDefaultData(False)
                                Call objMasterData.InsertDefaultData(False, Company._Object._Companyunkid)
                                'S.SANDEEP [04 JUN 2015] -- END

                                gfrmMDI = New frmNewMDI

                                If User._Object._Isrighttoleft = True Then
                                    gfrmMDI.RightToLeft = Windows.Forms.RightToLeft.Yes
                                    gfrmMDI.RightToLeftLayout = True
                                    Call Language.ctlRightToLeftlayOut(gfrmMDI)
                                End If

                                gfrmMDI.ShowDialog()

                                If gfrmMDI._Restart = True Then
                                    eZeeCommonLib.eZeeDatabase.change_database("hrmsConfiguration")
                                    gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Configuration)
                                    gobjLocalization._LangId = User._Object._Languageunkid
                                    mblnAppRun = True
                                Else
                                    mblnAppRun = False
                                End If
                            Else
                                Application.Exit()
                                mblnAppRun = False
                            End If
                        Else
                            Application.Exit()
                            mblnAppRun = False
                        End If
                    Else
                        Application.Exit()
                        mblnAppRun = False
                    End If
                Catch ex As Exception

                    mblnAppRun = False
                    DisplayError.Show("-1", ex.Message, "tmr", mstrModuleName)
                Finally
                    frm = Nothing
                    gfrmMDI = Nothing
                End Try
            Loop

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception

            Call DisplayError.Show(-1, ex.Message, "tmr", mstrModuleName)
        Finally
            Me.Close()
        End Try
    End Sub
    'Sandeep | 17 JAN 2011 | -- END 

End Class
