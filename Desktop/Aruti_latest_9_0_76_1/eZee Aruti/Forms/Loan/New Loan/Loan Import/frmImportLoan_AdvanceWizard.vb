﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportLoan_AdvanceWizard

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmImportLoan_AdvanceWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private mstrSearchText As String = ""
    'Sohail (29 Apr 2019) -- End
#End Region

#Region " Private Function(s) & Method(s) "

    'Nilay (15-Dec-2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private Sub SetColor()
        Try
            cboLoanCalType.BackColor = GUI.ColorComp
            cboInterestCalcType.BackColor = GUI.ColorComp
            cboCurrncy.BackColor = GUI.ColorComp
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            cboMappedHead.BackColor = GUI.ColorOptional
            'Sohail (29 Apr 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    'Nilay (15-Dec-2015) -- End

    Private Sub DoPanelOperation(ByVal pnlHidePanel As Panel, ByVal pnlVisiblePanel As Panel)
        Try
            pnlHidePanel.Visible = False : pnlVisiblePanel.Visible = True
            pnlVisiblePanel.Location = New Point(2, 26)
            pnlVisiblePanel.Size = New Size(605, 356)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoPanelOperation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("firstname", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("surname", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("lnappno", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("lnvocno", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("ladate", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("assignperiod", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("dedperiod", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("loanscheme", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("loanamt", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("interest", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("loanduration", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("exentityname", System.Type.GetType("System.String")).DefaultValue = ""
            'mdt_ImportData_Others.Columns.Add("balanceamount", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("lncaltype", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("lncurrncy", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("lninstallmentamt", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("lnapprovercode", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("lnapprovername", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("lnapproverlevel", System.Type.GetType("System.String")).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))
            'Hemant (30 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
            mdt_ImportData_Others.Columns.Add("lnaccno", System.Type.GetType("System.String")).DefaultValue = ""
            'Hemant (30 Aug 2019) -- End

            Dim dtTemp() As DataRow = Nothing

            dtTemp = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            'If radLoan.Checked = True Then
            '    dtTemp = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            'ElseIf radAdvance.Checked = True Then
            '    dtTemp = mds_ImportData.Tables(0).Select("" & cboLoanEmployeeCode.Text & " IS NULL")
            'End If


            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)

                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow
                'If radLoan.Checked = True Then
                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

                If cboLoanFirstname.Text.Trim.Length > 0 Then
                    drNewRow.Item("firstname") = dtRow.Item(cboLoanFirstname.Text).ToString.Trim
                Else
                    drNewRow.Item("firstname") = ""
                End If

                If cboLoanSurname.Text.Trim.Length > 0 Then
                    drNewRow.Item("surname") = dtRow.Item(cboLoanSurname.Text).ToString.Trim
                Else
                    drNewRow.Item("surname") = ""
                End If

                If cboLoanVocNo.Text.Trim.Length > 0 Then
                    drNewRow.Item("lnvocno") = dtRow.Item(cboLoanVocNo.Text).ToString.Trim
                Else
                    drNewRow.Item("lnvocno") = ""
                End If

                If cboApplicationNo.Text.Trim.Length > 0 Then
                    drNewRow.Item("lnappno") = dtRow.Item(cboApplicationNo.Text).ToString.Trim
                Else
                    drNewRow.Item("lnappno") = ""
                End If

                If cboLoanDate.Text.Trim.Length > 0 Then
                    drNewRow.Item("ladate") = dtRow.Item(cboLoanDate.Text).ToString.Trim
                Else
                    drNewRow.Item("ladate") = ""
                End If

                If cboAssignmentPeriod.Text.Trim.Length > 0 Then
                    drNewRow.Item("assignperiod") = dtRow.Item(cboAssignmentPeriod.Text).ToString.Trim
                Else
                    drNewRow.Item("assignperiod") = ""
                End If

                If cboDeductionPeriod.Text.Trim.Length > 0 Then
                    drNewRow.Item("dedperiod") = dtRow.Item(cboDeductionPeriod.Text).ToString.Trim
                Else
                    drNewRow.Item("dedperiod") = ""
                End If

                If cboExternalEntity.Text.Trim.Length > 0 Then
                    drNewRow.Item("exentityname") = dtRow.Item(cboExternalEntity.Text).ToString.Trim
                Else
                    drNewRow.Item("exentityname") = ""
                End If

                If cboApproverCode.Text.Trim.Length > 0 Then
                    drNewRow.Item("lnapprovercode") = dtRow.Item(cboApproverCode.Text).ToString.Trim
                Else
                    drNewRow.Item("lnapprovercode") = 0
                End If

                If cboApproverName.Text.Trim.Length > 0 Then
                    drNewRow.Item("lnapprovername") = dtRow.Item(cboApproverName.Text).ToString.Trim
                Else
                    drNewRow.Item("lnapprovername") = 0
                End If

                If cboApproverLevel.Text.Trim.Length > 0 Then
                    drNewRow.Item("lnapproverlevel") = dtRow.Item(cboApproverLevel.Text).ToString.Trim
                Else
                    drNewRow.Item("lnapproverlevel") = 0
                End If

                If radLoan.Checked Then
                    If cboLoanScheme.Text.Trim.Length > 0 Then
                        drNewRow.Item("loanscheme") = dtRow.Item(cboLoanScheme.Text).ToString.Trim
                    Else
                        drNewRow.Item("loanscheme") = ""
                    End If

                    If cboLoanAmount.Text.Trim.Length > 0 Then
                        drNewRow.Item("loanamt") = dtRow.Item(cboLoanAmount.Text).ToString.Trim
                    Else
                        drNewRow.Item("loanamt") = 0
                    End If

                    If cboInterestRate.Text.Trim.Length > 0 Then
                        drNewRow.Item("interest") = dtRow.Item(cboInterestRate.Text).ToString.Trim
                    Else
                        drNewRow.Item("interest") = 0
                    End If

                    If cboNoOfInstallment.Text.Trim.Length > 0 Then
                        drNewRow.Item("loanduration") = dtRow.Item(cboNoOfInstallment.Text).ToString.Trim
                    Else
                        drNewRow.Item("loanduration") = 1
                    End If

                    If cboInstallmentAmount.Text.Trim.Length > 0 Then
                        drNewRow.Item("lninstallmentamt") = dtRow.Item(cboInstallmentAmount.Text).ToString.Trim
                    Else
                        drNewRow.Item("lninstallmentamt") = 0
                    End If
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
                    If cboLoanAccountNo.Text.Trim.Length > 0 Then
                        drNewRow.Item("lnaccno") = dtRow.Item(cboLoanAccountNo.Text).ToString.Trim
                    Else
                        drNewRow.Item("lnaccno") = 0
                    End If
                    'Hemant (30 Aug 2019) -- End
                Else
                    If cboAdvanceAmount.Text.Trim.Length > 0 Then
                        drNewRow.Item("loanamt") = dtRow.Item(cboAdvanceAmount.Text).ToString.Trim
                    Else
                        drNewRow.Item("loanamt") = 0
                    End If
                End If

                'If cboLoanBalance.Text.Trim.Length > 0 Then
                '    drNewRow.Item("balanceamount") = dtRow.Item(cboLoanBalance.Text).ToString.Trim
                'Else
                '    drNewRow.Item("balanceamount") = 0
                'End If

                'End If

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                dgData.AutoGenerateColumns = False
                colhEmployee.DataPropertyName = "ecode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizImportLoan.BackEnabled = False
            eZeeWizImportLoan.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                'Nilay (15-Dec-2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If .Item(cboLoanVocNo.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                'If .Item(cboApplicationNo.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Loan Application No. cannot be blank. Please set Loan Application No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                If ConfigParameter._Object._LoanVocNoType = 0 Then
                If .Item(cboLoanVocNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                End If

                If ConfigParameter._Object._LoanApplicationNoType = 0 Then
                If .Item(cboApplicationNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Loan Application No. cannot be blank. Please set Loan Application No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                End If
                'Nilay (15-Dec-2015) -- End

                If .Item(cboLoanDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Loan Date cannot be blank. Please set Loan Date in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboAssignmentPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Assignment Period cannot be blank. Please set Loan Assignment Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboDeductionPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Loan Deduction Period cannot be blank. Please set Loan Deduction Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboApproverCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Approver Code cannot be blank. Please set Approver Code in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboApproverName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Approver Name cannot be blank. Please set Approver Name in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboApproverLevel.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Approver Level cannot be blank. Please set Approver Level in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    Return False
                End If


                If radLoan.Checked = True Then
                    If .Item(cboLoanScheme.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Loan Scheme cannot be blank. Please set Loan Scheme in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanAmount.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Amount cannot be blank. Please set Loan Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'If .Item(cboInterestRate.Text).ToString.Trim.Length <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Loan Interest Rate cannot be blank. Please set Loan Interest Rate in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    '    Return False
                    'End If
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'If CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest Then
                    If CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                        'Sohail (29 Apr 2019) -- End
                    If .Item(cboInterestRate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Loan Interest Rate cannot be blank. Please set Loan Interest Rate in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    End If
                    'Nilay (01-Mar-2016) -- End

                    If .Item(cboNoOfInstallment.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Loan Duration cannot be blank. Please set Loan Duration in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    'If .Item(cboLoanBalance.Text).ToString.Trim.Length <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Loan Balance cannot be blank. Please set Loan Balance in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    '    Return False
                    'End If

                    If .Item(cboInstallmentAmount.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Loan Installment Amount cannot be blank. Please set Loan Installment Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
                    'Hemant (15 Nov 2019) -- Start
                    'If .Item(cboLoanAccountNo.Text).ToString.Trim.Length <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 68, "Loan Account No cannot be blank. Please set Loan Account No in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    '    Return False
                    'End If
                    'Hemant (15 Nov 2019) -- End
                    'Hemant (30 Aug 2019) -- End
                Else
                    If .Item(cboAdvanceAmount.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Advance Amount cannot be blank. Please set Advance Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetDataCombo()
        Try

            'If radLoan.Checked = True Then
            For Each ctrl As Control In pnlLoanMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'If ctrl.Name = cboCurrncy.Name OrElse ctrl.Name = cboLoanCalType.Name Then Continue For
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'If ctrl.Name = cboCurrncy.Name OrElse ctrl.Name = cboLoanCalType.Name OrElse ctrl.Name = cboInterestCalcType.Name Then Continue For
                    If ctrl.Name = cboCurrncy.Name OrElse ctrl.Name = cboLoanCalType.Name OrElse ctrl.Name = cboInterestCalcType.Name OrElse ctrl.Name = cboMappedHead.Name Then Continue For
                    'Sohail (29 Apr 2019) -- End
                    'Sohail (15 Dec 2015) -- End
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboLoanFirstname.Items.Add(dtColumns.ColumnName)
                cboLoanSurname.Items.Add(dtColumns.ColumnName)
                cboLoanVocNo.Items.Add(dtColumns.ColumnName)
                cboApplicationNo.Items.Add(dtColumns.ColumnName)
                cboLoanDate.Items.Add(dtColumns.ColumnName)
                cboAssignmentPeriod.Items.Add(dtColumns.ColumnName)
                cboDeductionPeriod.Items.Add(dtColumns.ColumnName)
                cboLoanScheme.Items.Add(dtColumns.ColumnName)
                cboLoanAmount.Items.Add(dtColumns.ColumnName)
                cboInterestRate.Items.Add(dtColumns.ColumnName)
                cboNoOfInstallment.Items.Add(dtColumns.ColumnName)
                cboExternalEntity.Items.Add(dtColumns.ColumnName)
                cboAdvanceAmount.Items.Add(dtColumns.ColumnName)
                cboInstallmentAmount.Items.Add(dtColumns.ColumnName)
                cboApproverCode.Items.Add(dtColumns.ColumnName)
                cboApproverName.Items.Add(dtColumns.ColumnName)
                cboApproverLevel.Items.Add(dtColumns.ColumnName)
                'Hemant (30 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
                cboLoanAccountNo.Items.Add(dtColumns.ColumnName)
                'Hemant (30 Aug 2019) -- End
            Next
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objExRate As New clsExchangeRate
        Dim objLoan As New clsLoan_Advance 'Sohail (15 Dec 2015)
        Dim objHead As New clsTransactionHead 'Sohail (29 Apr 2019)
        Try
            dsList = objExRate.getComboList("ExRate", True)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                With cboCurrncy
                    .ValueMember = "countryunkid"
                    .DisplayMember = "currency_sign"
                    .DataSource = dsList.Tables("ExRate")
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    '.SelectedValue = 0
                    .SelectedValue = objExRate.getBaseCurrencyCountryID()
                    'Sohail (15 Dec 2015) -- End
                End With
            End If

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'With cboLoanCalType
            '    .Items.Clear()
            '    .Items.Add(Language.getMessage(mstrModuleName, 200, "Simple Interest"))
            '    .Items.Add(Language.getMessage(mstrModuleName, 201, "Reducing Interest"))
            '    .Items.Add(Language.getMessage(mstrModuleName, 202, "No Interest"))
            '    .SelectedIndex = 0
            'End With
            dsList = objLoan.GetLoanCalculationTypeList("List", True)
            With cboLoanCalType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objLoan.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Sohail (15 Dec 2015) -- End

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            With cboMappedHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboMappedHead)
            End With
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
        Finally
            objLoan = Nothing
            objExRate = Nothing
            objHead = Nothing
            'Sohail (29 Apr 2019) -- End
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If (cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboEmployeeCode.Focus()
                Return False
            End If

            If (cboLoanVocNo.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboLoanVocNo.Focus()
                Return False
            End If

            If (cboApplicationNo.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Loan Application No. cannot be blank. Please set Loan Application No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboApplicationNo.Focus()
                Return False
            End If

            If (cboLoanDate.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Loan Date cannot be blank. Please set Loan Date in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboLoanDate.Focus()
                Return False
            End If

            If (cboAssignmentPeriod.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Assignment Period cannot be blank. Please set Loan Assignment Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboAssignmentPeriod.Focus()
                Return False
            End If

            If (cboDeductionPeriod.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Loan Deduction Period cannot be blank. Please set Loan Deduction Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboDeductionPeriod.Focus()
                Return False
            End If

            If (cboExternalEntity.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "External Entity cannot be blank. Please set External Entity in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboExternalEntity.Focus()
                Return False
            End If

            If cboCurrncy.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Currncy Value cannot be blank. Please set Currncy Value in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboCurrncy.Focus()
                Return False
            End If

            If (cboApproverCode.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Approver Code cannot be blank. Please set Approver Code in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboApproverCode.Focus()
                Return False
            End If

            If (cboApproverName.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Approver Name cannot be blank. Please set Approver Name in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboApproverName.Focus()
                Return False
            End If

            If (cboApproverLevel.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Approver Level cannot be blank. Please set Approver Level in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                cboApproverLevel.Focus()
                Return False
            End If

            If radLoan.Checked = True Then

                If (cboLoanScheme.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Loan Scheme cannot be blank. Please set Loan Scheme in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanScheme.Focus()
                    Return False
                End If

                If (cboLoanAmount.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Amount cannot be blank. Please set Loan Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanAmount.Focus()
                    Return False
                End If

                If (cboInterestRate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Loan Interest Rate cannot be blank. Please set Loan Interest Rate in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboInterestRate.Focus()
                    Return False
                End If

                If (cboNoOfInstallment.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Loan Duration cannot be blank. Please set Loan Duration in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboNoOfInstallment.Focus()
                    Return False
                End If

                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If cboLoanCalType.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Loan Calculate Type cannot be blank. Please set Loan Calculate Type in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                '    cboLoanCalType.Focus()
                '    Return False
                'End If
                If CInt(cboLoanCalType.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Loan Calculate Type cannot be blank. Please select Loan Calculate Type in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanCalType.Focus()
                    Return False
                End If

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'If CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                If CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                    'Sohail (29 Apr 2019) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 48, "Interest Calculate Type cannot be blank. Please select Interest Calculate Type in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboInterestCalcType.Focus()
                    Return False
                End If
                'Sohail (15 Dec 2015) -- End

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                If CInt(cboLoanCalType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboMappedHead.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 66, "Please select Mapped Head to continue."), enMsgBoxStyle.Information)
                    cboMappedHead.Focus()
                    Return False
                End If
                'Sohail (29 Apr 2019) -- End

                If (cboInstallmentAmount.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Loan Installment Amount cannot be blank. Please set Loan Installment Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboInstallmentAmount.Focus()
                    Return False
                End If

                'Hemant (30 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
                If (cboLoanAccountNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 68, "Loan Account No cannot be blank. Please set Loan Account No in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboApproverLevel.Focus()
                    Return False
                End If
                'Hemant (30 Aug 2019) -- End
            Else
                If (cboAdvanceAmount.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Advance Amount cannot be blank. Please set Advance Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboAdvanceAmount.Focus()
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            Dim objlnApproverMaster As New clsLoanApprover_master
            'Nilay (01-Mar-2016) -- End

            Dim objLoanScheme As New clsLoan_Scheme
            Dim objProcessPending As clsProcess_pending_loan = Nothing
            Dim objPeriod As New clscommom_period_Tran
            Dim objLoan_Advance As New clsLoan_Advance

            Dim objEmp As New clsEmployee_Master

            Dim intLoanSchemeUnkid As Integer = -1
            Dim intProcessPendingUnkid As Integer = -1
            Dim intPeriodUnkid As Integer = -1
            Dim intDeductionPeriodUnkid As Integer = -1
            Dim intEmployeeId As Integer = -1
            Dim intApproverEmpid As Integer = -1
            Dim intApproverUnkid As Integer = -1
            Dim intCountryUnkid As Integer = -1
            Dim decLoanAmount As Decimal = 0
            Dim intInstallment As Integer = 0
            Dim decEMIAmount As Decimal = 0
            Dim decInterestRate As Decimal = 0
            Dim DtLoanDate As Date = Nothing
            Dim dtDeducationStartDate As Date = Nothing
            'Sohail (22 Sep 2017) -- Start
            'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
            Dim dtDeducationEndDate As Date = Nothing
            'Sohail (22 Sep 2017) -- End
            'Nilay (25-Mar-2016) -- Start
            Dim dtAssignStartDate As Date = Nothing
            Dim dtAssignEndDate As Date = Nothing
            'Nilay (25-Mar-2016) -- End

            'Nilay (30-May-2016) -- Start
            Dim blnIsExteralApprover As Boolean
            'Nilay (30-May-2016) -- End
            Dim objProcesspendingloan As New clsProcess_pending_loan 'Sohail (06 Jul 2018)

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    intEmployeeId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If intEmployeeId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '-------------------- Check Vaid Date 
                If IsDate(dtRow.Item("ladate")) Then
                    DtLoanDate = CDate(dtRow.Item("ladate"))
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 22, "Invalid Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                '------------------------------ CHECKING IF Deducation PERIOD PRESENT.

                If dtRow.Item("dedperiod").ToString.Trim.Length > 0 Then
                    intDeductionPeriodUnkid = objPeriod.GetPeriodByName(dtRow.Item("dedperiod").ToString.Trim, enModuleReference.Payroll)
                    If intDeductionPeriodUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = dtRow.Item("dedperiod").ToString.Trim & " " & Language.getMessage(mstrModuleName, 24, ", Deduction period not found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intDeductionPeriodUnkid
                        dtDeducationStartDate = objPeriod._Start_Date
                        'Sohail (22 Sep 2017) -- Start
                        'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                        dtDeducationEndDate = objPeriod._End_Date
                        'Sohail (22 Sep 2017) -- End
                    End If
                End If

                '------------------------------ CHECKING IF Assign PERIOD PRESENT.
                If dtRow.Item("assignperiod").ToString.Trim.Length > 0 Then
                    intPeriodUnkid = objPeriod.GetPeriodByName(dtRow.Item("assignperiod").ToString.Trim, enModuleReference.Payroll)
                    If intPeriodUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = dtRow.Item("assignperiod").ToString.Trim & " " & Language.getMessage(mstrModuleName, 19, ",Assignment period not found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodUnkid
                        'Nilay (25-Mar-2016) -- Start
                        dtAssignStartDate = objPeriod._Start_Date
                        dtAssignEndDate = objPeriod._End_Date
                        'Nilay (25-Mar-2016) -- End
                        If DtLoanDate.Date >= objPeriod._Start_Date.Date And DtLoanDate.Date <= objPeriod._End_Date.Date Then
                        Else
                            dtRow.Item("image") = imgError
                            'Hemant (25 Nov 2019) -- Start
                            'dtRow.Item("message") = Language.getMessage(mstrModuleName, 22, "Invalid Date.")
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 69, "loan date should be in between deduction period start and end date")
                            'Hemant (25 Nov 2019) -- End
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '--------------Check Assign Period And Deducation Period Validation
                If dtDeducationStartDate.Date < objPeriod._Start_Date.Date Then
                        dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 35, "Deduction period cannot be less then the Loan Period.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                End If

                'If CDec(dtRow.Item("loanamt")) < CDec(dtRow.Item("balanceamount")) Then
                '    dtRow.Item("image") = imgError
                '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 30, "Invalid Balance Amount.")
                '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 100, "Fail")
                '    dtRow.Item("objStatus") = 2
                '    objError.Text = CStr(Val(objError.Text) + 1)
                '    Continue For
                'End If

                If radLoan.Checked = True Then
                    '------------------------------ CHECKING IF LOAN SCHEME PRESENT.
                    If dtRow.Item("loanscheme").ToString.Trim.Length > 0 Then
                        intLoanSchemeUnkid = objLoanScheme.GetLoanSchemeUnkid(dtRow.Item("loanscheme").ToString.Trim)
                        If intLoanSchemeUnkid <= 0 Then
                            If objLoanScheme IsNot Nothing Then objLoanScheme = Nothing
                            objLoanScheme = New clsLoan_Scheme
                            objLoanScheme._Code = dtRow.Item("loanscheme").ToString.Trim
                            objLoanScheme._Isactive = True
                            objLoanScheme._Name = dtRow.Item("loanscheme").ToString.Trim
                            objLoanScheme._Minnetsalary = 0
                            'Sohail (22 Sep 2017) -- Start
                            'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                            objLoanScheme._MaxLoanAmountLimit = 0
                            objLoanScheme._LoanCalcTypeId = Convert.ToInt32(enLoanCalcId.No_Interest)
                            objLoanScheme._InterestCalctypeId = 0
                            'Sohail (29 Apr 2019) -- Start
                            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                            objLoanScheme._Mapped_TranheadUnkid = 0
                            'Sohail (29 Apr 2019) -- End
                            objLoanScheme._InterestRate = 0
                            objLoanScheme._EMI_NetPayPercentage = 0
                            'Sohail (22 Sep 2017) -- End
                            'Sohail (11 Apr 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                            objLoanScheme._TranheadUnkid = 0
                            'Sohail (11 Apr 2018) -- End

                            If objLoanScheme.Insert Then
                                intLoanSchemeUnkid = objLoanScheme._Loanschemeunkid
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = objLoanScheme._Code & "/" & objLoanScheme._Name & " : " & objLoanScheme._Message
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If
                    End If

                    '------------------------------Check Interestrate,Installment,EMI AND Loan Amount Vaild OR not

                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'If Decimal.TryParse(dtRow.Item("interest").ToString, decInterestRate) = False Then
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 34, "Invalid Interest Rate")
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    '    Continue For
                    'End If

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'If CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest Then
                    If CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                        'Sohail (29 Apr 2019) -- End
                        'Nilay (18-Oct-2016) -- Start
                        'If Decimal.TryParse(dtRow.Item("interest").ToString, decInterestRate) = False Then
                        Decimal.TryParse(dtRow.Item("interest").ToString, decInterestRate)
                        If decInterestRate <= 0 Then
                            'Nilay (18-Oct-2016) -- End
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 34, "Invalid Interest Rate")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                    End If
                    'Nilay (01-Mar-2016) -- End

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    If CInt(cboLoanCalType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboMappedHead.SelectedValue) <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 66, "Please select Mapped Head to continue.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                    'Sohail (29 Apr 2019) -- End

                    'Nilay (18-Oct-2016) -- Start
                    'If Decimal.TryParse(dtRow.Item("loanduration").ToString, intInstallment) = False Then
                    Decimal.TryParse(dtRow.Item("loanduration").ToString, intInstallment)
                    If intInstallment <= 0 Then
                        'Nilay (18-Oct-2016) -- End
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 25, "Invalid No Of Installment")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Nilay (18-Oct-2016) -- Start
                    'If Decimal.TryParse(dtRow.Item("lninstallmentamt").ToString, decEMIAmount) = False Then
                    Decimal.TryParse(dtRow.Item("lninstallmentamt").ToString, decEMIAmount)
                    If decEMIAmount <= 0 Then
                        'Nilay (18-Oct-2016) -- End
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Installment Amount")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Sohail (17 Dec 2019) -- Start
                    'Mukuba University # 0004115: Option for EOC to affect number of installments on loans.
                    If intInstallment > 1 Then
                        Dim dtLoanEnd As Date = dtDeducationStartDate.AddMonths(intInstallment).AddDays(-1)
                        Dim objEmpDates As New clsemployee_dates_tran
                        Dim dsEmp As DataSet = objEmpDates.GetEmployeeEndDates(dtLoanEnd, intEmployeeId.ToString)
                        If dsEmp.Tables(0).Rows.Count > 0 Then
                            Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, dtDeducationStartDate, eZeeDate.convertDate(dsEmp.Tables(0).Rows(0).Item("finalenddate").ToString).AddDays(1)))
                            If intInstallment > intEmpTenure Then
                                'Hemant (24 Nov 2021) -- Start
                                'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
                                If ConfigParameter._Object._SkipEOCValidationOnLoanTenure = False Then
                                    'Hemant (24 Nov 2021) -- End
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 69, "Sorry, Loan tenure should not be greater than employee tenure month") & " [" & intEmpTenure & "]."
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                                End If 'Hemant (24 Nov 2021)
                            End If
                        End If
                    End If
                    'Sohail (17 Dec 2019) -- End

                End If

                'Nilay (18-Oct-2016) -- Start
                'If Decimal.TryParse(dtRow.Item("loanamt").ToString, decLoanAmount) = False Then
                Decimal.TryParse(dtRow.Item("loanamt").ToString, decLoanAmount)
                If decLoanAmount <= 0 Then
                    'Nilay (18-Oct-2016) -- End
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 27, "Invalid Loan/Advance Amount")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                '------------------------------ Check Interestrate.
                If radLoan.Checked = True Then
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'If cboLoanCalType.SelectedIndex = 2 Then
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'If cboLoanCalType.SelectedValue = enLoanCalcId.No_Interest Then
                    If cboLoanCalType.SelectedValue = enLoanCalcId.No_Interest OrElse cboLoanCalType.SelectedValue = enLoanCalcId.No_Interest_With_Mapped_Head Then
                        'Sohail (29 Apr 2019) -- End
                        'Sohail (15 Dec 2015) -- End
                        If decInterestRate > 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 43, "No Interest") & " " & Language.getMessage(mstrModuleName, 28, "calculation type cannot have rate.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    Else
                        If decInterestRate <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 29, "Cannot allow Interest Rate to blank")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If


                '------------------------------ Check Approver Valid------------
                Dim objLoanApprover As New clsLoanApprover_master
                If dtRow.Item("lnapprovercode").ToString.Trim.Length > 0 Then

                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    Dim dsAppvrList As DataSet = objlnApproverMaster.GetList(FinancialYear._Object._DatabaseName, _
                                                                             User._Object._Userunkid, _
                                                                             FinancialYear._Object._YearUnkid, _
                                                                             Company._Object._Companyunkid, _
                                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                                             True, _
                                                                             ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                             "List")

                    For Each dRow In dsAppvrList.Tables("List").Rows
                        If dtRow.Item("lnapprovercode").ToString = dRow("code").ToString Then
                            intApproverEmpid = dRow("approverempunkid")
                            'Nilay (30-May-2016) -- Start
                            blnIsExteralApprover = CBool(dRow("isexternalapprover"))
                            'Nilay (30-May-2016) -- End
                        End If
                    Next

                    'intApproverEmpid = objEmp.GetEmployeeUnkid("", dtRow.Item("lnapprovercode").ToString.Trim)
                    'Nilay (01-Mar-2016) -- End

                    If intApproverEmpid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 30, "Approver Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else

                        Dim dtApprover As DataTable = objLoanApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, _
                                                                                          User._Object._Userunkid, _
                                                                                          FinancialYear._Object._YearUnkid, _
                                                                                          Company._Object._Companyunkid, _
                                                                                          intEmployeeId, _
                                                                                          ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                                          intLoanSchemeUnkid)
                        'Nilay (30-May-2016) -- End
                        'Dim dRow() As DataRow = dtApprover.Select("approverempunkid = " & intApproverEmpid)
                        Dim dRow() As DataRow = dtApprover.Select("approverempunkid = " & intApproverEmpid & " AND isexternalapprover = " & blnIsExteralApprover)
                        'Nilay (30-May-2016) -- End

                        If dRow.Length <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 31, "This Employee is not assigned to given approver.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            If dtApprover.Select("MAX(priority) > " & CInt(dRow(0).Item("priority"))).Length > 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 47, "This Approver is Not Final Approver.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            Else
                                intApproverUnkid = CInt(dRow(0).Item("lnapproverunkid"))
                            End If
                        End If
                    End If
                End If
                '-----------------------------------Get Exchange Rate
                Dim objExRate As New clsExchangeRate
                Dim decPaidExRate As Decimal = 0
                'Sohail (20 Sep 2017) -- Start
                'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
                Dim decBaseExRate As Decimal = 0
                'Sohail (20 Sep 2017) -- End
                Dim dsList As DataSet = Nothing
                dsList = objExRate.GetList("ExRate", True, , , cboCurrncy.SelectedValue, True, DtLoanDate.Date, True)
                If dsList.Tables("ExRate").Rows.Count > 0 Then
                    'mdecBaseExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                    decPaidExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                    'mintPaidCurrId = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                    'Sohail (22 Sep 2017) -- Start
                    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                    decBaseExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                    'Sohail (22 Sep 2017) -- End
                Else
                    'mdecBaseExRate = 0
                    decPaidExRate = 0
                    'Sohail (22 Sep 2017) -- Start
                    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                    decBaseExRate = 0
                    'Sohail (22 Sep 2017) -- End
                End If

                '-----------------------------------Get Interest Rate Amount And Installment

                'Shani(17-DEC-2015) -- Start
                'Issue : 
                'Dim mdecInstallmentAmount As Decimal = 0
                'Dim mdecInstrAmount As Decimal = 0
                Dim mdecInstallmentAmount As Decimal = decEMIAmount
                Dim mdecInstrAmount As Decimal = intInstallment
                'Shani(17-DEC-2015) -- End
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                Dim mdecTotInstallmentAmount As Decimal = 0
                Dim mdecTotIntrstAmount As Decimal = 0
                'Sohail (15 Dec 2015) -- End

                Dim dtEndDate As Date = DateAdd(DateInterval.Month, intInstallment, DtLoanDate)
                Dim intCalType As Integer = 0
                If radLoan.Checked Then
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'If cboLoanCalType.SelectedIndex = 0 Then ' Simple Interest
                    '    intCalType = enLoanCalcId.Simple_Interest '1
                    'ElseIf cboLoanCalType.SelectedIndex = 1 Then 'Reducing Interest
                    '    intCalType = enLoanCalcId.Reducing_Amount '3
                    'ElseIf cboLoanCalType.SelectedIndex = 2 Then 'NO INTEREST
                    '    intCalType = enLoanCalcId.No_Interest '4
                    'End If
                    'Sohail (15 Dec 2015) -- End

                    'Nilay (15-Dec-2015) -- Start
                    'objLoan_Advance.Calulate_Projected_Loan_Balance(decLoanAmount, CInt(DateDiff(DateInterval.Day, DtLoanDate.Date, dtEndDate.Date)), decInterestRate, intCalType, intInstallment, mdecInstrAmount, mdecInstallmentAmount)
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'If intCalType <> enLoanCalcId.No_Interest Then
                    'Sohail (15 Dec 2015) -- End
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'objLoan_Advance.Calulate_Projected_Loan_Balance(decLoanAmount, CInt(DateDiff(DateInterval.Day, DtLoanDate.Date, dtEndDate.Date)), decInterestRate, intCalType, intInstallment, mdecInstrAmount, mdecInstallmentAmount)
                    objLoan_Advance.Calulate_Projected_Loan_Balance(decLoanAmount _
                                                                    , CInt(DateDiff(DateInterval.Day, DtLoanDate.Date, dtEndDate.Date)) _
                                                                    , decInterestRate _
                                                                    , CInt(cboLoanCalType.SelectedValue) _
                                                                    , CInt(cboInterestCalcType.SelectedValue) _
                                                                    , intInstallment _
                                                                    , CInt(DateDiff(DateInterval.Day, objPeriod._Start_Date.Date, objPeriod._End_Date.Date.AddDays(1))) _
                                                                    , decEMIAmount _
                                                                    , mdecInstrAmount _
                                                                    , mdecInstallmentAmount _
                                                                    , mdecTotIntrstAmount _
                                                                    , mdecTotInstallmentAmount _
                                                                    )
                    'Sohail (15 Dec 2015) -- End
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'End If
                    'Sohail (15 Dec 2015) -- End
                    'Nilay (15-Dec-2015) -- End

                End If

                '------------------------------Check Exist Loan -----------
                'Hemant (15 Dec 2018) -- Start
                'Enhancement : Cant import advance for an employee with an existing advance in the same for BBLMS
                'Dim dtloan As DataSet = objLoan_Advance.GetExistLoan(intLoanSchemeUnkid, intPeriodUnkid, intDeductionPeriodUnkid, intEmployeeId, radLoan.Checked, "List")
                'Sohail (21 May 2020) -- Start
                'NMB Issue # : System was allowing to assign same loan even if same loan is in progress or on hold on loan import.
                'Dim dtloan As DataSet = objLoan_Advance.GetExistLoan(intLoanSchemeUnkid, intPeriodUnkid, intDeductionPeriodUnkid, intEmployeeId, radLoan.Checked, decLoanAmount, "List")
                Dim blnIsLoanExist As Boolean = objLoan_Advance.GetExistLoan(intLoanSchemeUnkid, intEmployeeId, radLoan.Checked, Nothing, decLoanAmount)
                'Sohail (21 May 2020) -- End
                'Hemant (15 Dec 2018) -- End

                'Sohail (21 May 2020) -- Start
                'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
                'If dtloan.Tables(0).Rows.Count > 0 Then
                If blnIsLoanExist = True Then
                    'Sohail (21 May 2020) -- End
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 32, "This Loan is alrady Assign to this employee.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                '------------------------------Monthly EMI -----------
                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                If radLoan.Checked = True Then
                    objLoanScheme = New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = intLoanSchemeUnkid
                    Dim mdecEMI_NetPayPercentage As Decimal = objLoanScheme._EMI_NetPayPercentage
                    'Sohail (11 Apr 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                    Dim mintTranheadUnkid As Integer = objLoanScheme._TranheadUnkid
                    'Sohail (11 Apr 2018) -- End
                    If mdecEMI_NetPayPercentage > 0 Then
                        'Sohail (11 Apr 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                        'Dim objTnALeave As New clsTnALeaveTran
                        'Dim ds As DataSet = objTnALeave.GetPreviousNetPay(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtDeducationStartDate, dtDeducationEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, dtDeducationStartDate.AddDays(-1), "List", intEmployeeId.ToString(), "")
                        Dim ds As DataSet = Nothing
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        Dim dsMapped As DataSet = Nothing
                        'Sohail (06 Jul 2018) -- End
                        'If mintTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                        Dim objTnALeave As New clsTnALeaveTran
                            ds = objTnALeave.GetPreviousNetPay(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtDeducationStartDate, dtDeducationEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, dtDeducationStartDate.AddDays(-1), "List", intEmployeeId.ToString(), "")
                        'Else 'Sohail (06 Jul 2018)
                        If mintTranheadUnkid > 0 Then 'Sohail (06 Jul 2018)
                            Dim objMaster As New clsMasterData
                            Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), dtDeducationStartDate.AddDays(-1), 0, 0, True, False, Nothing, False)
                            Dim strFilter As String = ""
                            Dim dtStart As Date = dtDeducationStartDate
                            Dim dtEnd As Date = dtDeducationEndDate
                            Dim strDBName As String = FinancialYear._Object._DatabaseName
                            Dim intYearId As Integer = FinancialYear._Object._YearUnkid
                            If intPrevPeriod <= 0 Then
                                strFilter = " 1 = 2 "
                            Else
                                objPeriod = New clscommom_period_Tran
                                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                                dtStart = objPeriod._Start_Date
                                dtEnd = objPeriod._End_Date
                                If intYearId <> objPeriod._Yearunkid Then
                                    intYearId = objPeriod._Yearunkid
                                    strDBName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid)
                                End If
                                strFilter = " prpayrollprocess_tran.tranheadunkid = " & mintTranheadUnkid & " "
                            End If
                            Dim objProcess As New clsPayrollProcessTran
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'ds = objProcess.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", intEmployeeId, intPrevPeriod, , , strFilter)
                            dsMapped = objProcess.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", intEmployeeId, intPrevPeriod, , , strFilter)
                            'Sohail (06 Jul 2018) -- End
                        End If
                        'Sohail (11 Apr 2018) -- End
                        If ds.Tables(0).Rows.Count <= 0 Then
                            dtRow.Item("image") = imgError
                            'Sohail (11 Apr 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                            'dtRow.Item("message") = Language.getMessage(mstrModuleName, 52, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee.")
                            'If mintTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 52, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee.")
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'Else
                            '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 58, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee for the net pay head set on loan scheme master screen.")
                            'End If
                            'Sohail (06 Jul 2018) -- End
                            'Sohail (11 Apr 2018) -- End
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        ElseIf mintTranheadUnkid > 0 AndAlso dsMapped.Tables(0).Rows.Count <= 0 Then
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 58, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee for the net pay head set on loan scheme master screen.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'Sohail (06 Jul 2018) -- End
                        Else
                            Dim decAmt As Decimal = mdecInstallmentAmount
                            If decBaseExRate <> 0 Then
                                decAmt = decAmt * decBaseExRate / decPaidExRate
                            Else
                                decAmt = decAmt * decPaidExRate
                            End If
                            'Sohail (11 Apr 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                            'If decAmt > ((CDec(ds.Tables(0).Rows(0)("total_amount")) * mdecEMI_NetPayPercentage) / 100) Then
                            Dim decPrevNetPay As Decimal = 0
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            Dim decPrevActualNetPay As Decimal = 0
                            Dim decOtherLoanEMI As Decimal = 0
                            'Sohail (06 Jul 2018) -- End
                            If mintTranheadUnkid <= 0 Then
                                decPrevNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))
                            Else
                                'Sohail (06 Jul 2018) -- Start
                                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                                'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("amountPaidCurrency"))
                                decPrevNetPay = CDec(dsMapped.Tables(0).Rows(0)("amountPaidCurrency"))
                                'Sohail (06 Jul 2018) -- End
                            End If
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'Dim decCheckPoint As Decimal = decPrevNetPay - decAmt
                            decPrevActualNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))

                            Dim objLoanAdvance As New clsLoan_Advance
                            Dim dsLoanCalc As DataSet = objLoanAdvance.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                      dtDeducationStartDate, dtDeducationEndDate, ConfigParameter._Object._UserAccessModeSetting, True, _
                                                      "Loan", dtDeducationEndDate.AddDays(1), intEmployeeId.ToString(), 0, _
                                                      dtDeducationStartDate, True, , , True, True, , True, True, , , , 0, , , , , Nothing)
                            Dim dtTable As DataTable = New DataView(dsLoanCalc.Tables(0), "statusunkid IN (" & CInt(enLoanStatus.IN_PROGRESS) & ", " & CInt(enLoanStatus.ON_HOLD) & ") AND transactionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
                            For Each dRow As DataRow In dtTable.Rows

                                If CInt(dRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dRow.Item("iscalculateinterest").ToString) = False Then
                                    decOtherLoanEMI += 0
                                ElseIf CInt(dRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dRow.Item("iscalculateinterest").ToString) = True Then
                                    decOtherLoanEMI += CDec(dRow.Item("TotalMonthlyInterestPaidCurrency").ToString) + CDec(dRow.Item("TotInterestAmountPaidCurrency").ToString)
                                Else
                                    decOtherLoanEMI += CDec(dRow.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(dRow.Item("TotPMTAmountPaidCurrency").ToString)
                                End If
                            Next
                            Dim dsLoan As DataSet = objProcesspendingloan.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtDeducationStartDate, dtDeducationEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "Loan", 0, "lnloan_process_pending_loan.employeeunkid = " & intEmployeeId & " AND lnloan_process_pending_loan.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " AND lnloan_process_pending_loan.loan_statusunkid IN ( " & CInt(enLoanApplicationStatus.PENDING) & ", " & CInt(enLoanApplicationStatus.APPROVED) & " ) ")
                            For Each drow As DataRow In dsLoan.Tables(0).Rows
                                If CBool(drow.Item("isloan")) = True Then
                                    Dim dtEnd_Date As Date = DateAdd(DateInterval.Month, CInt(drow.Item("noofinstallment")), dtDeducationStartDate).AddDays(-1)
                                    Dim decIntAmt As Decimal
                                    Dim decEMI As Decimal
                                    Dim decTotIntAmt As Decimal
                                    Dim decTotEMI As Decimal
                                    objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(drow.Item("Amount")), CInt(DateDiff(DateInterval.Day, dtDeducationStartDate, dtEnd_Date.Date)), CDec(drow.Item("interest_rate")), CType(drow.Item("loancalctype_id"), enLoanCalcId), CType(drow.Item("interest_calctype_id"), enLoanInterestCalcType), CInt(drow.Item("noofinstallment")), CInt(DateDiff(DateInterval.Day, dtDeducationStartDate.Date, dtDeducationEndDate.Date.AddDays(1))), CDec(drow.Item("installmentamt")), decIntAmt, decEMI, decTotIntAmt, decTotEMI)
                                    decOtherLoanEMI += decEMI
                                Else
                                    decOtherLoanEMI += CDec(drow.Item("amount"))
                                End If

                            Next
                            'Sohail (19 Mar 2020) -- Start
                            'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                            'Dim decCheckPoint As Decimal = decPrevActualNetPay - decAmt - decOtherLoanEMI
                            Dim decCheckPoint As Decimal = decAmt + decOtherLoanEMI
                            'Sohail (19 Mar 2020) -- End
                            'Sohail (06 Jul 2018) -- End
                            'Sohail (19 Mar 2020) -- Start
                            'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                            'If decPrevNetPay <= 0 OrElse ((decPrevNetPay * mdecEMI_NetPayPercentage) / 100) > decCheckPoint Then
                            If decPrevNetPay <= 0 OrElse ((decPrevNetPay * mdecEMI_NetPayPercentage) / 100) < decCheckPoint Then
                                'Sohail (19 Mar 2020) -- End
                                dtRow.Item("image") = imgError
                                'Sohail (11 Apr 2018) -- Start
                                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                                'dtRow.Item("message") = Language.getMessage(mstrModuleName, 53, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 54, "of pervious net pay.") & " " & Format(CDec(ds.Tables(0).Rows(0)("total_amount")), GUI.fmtCurrency)
                                If mintTranheadUnkid <= 0 Then
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 53, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 54, "of previous net pay.") & " " & Format(decPrevNetPay, GUI.fmtCurrency)
                                Else
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 59, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 60, "of previous net pay head") & " " & Format(decPrevNetPay, GUI.fmtCurrency)
                                End If
                                'Sohail (11 Apr 2018) -- End
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If
                    End If

                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    If objLoanScheme._MaxNoOfInstallment > 0 AndAlso intInstallment > objLoanScheme._MaxNoOfInstallment Then
                        dtRow.Item("image") = imgError
                        'Sohail (11 Apr 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                        'dtRow.Item("message") = Language.getMessage(mstrModuleName, 55, "Sorry, you can not apply for this loan scheme: Reasone, Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 56, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 57, " Scheme.")
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 55, "Sorry, you can not apply for this loan scheme: Reason, Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 56, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 57, " Scheme.")
                        'Sohail (11 Apr 2018) -- End
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                    'Varsha (25 Nov 2017) -- End

                    'Sohail (16 May 2018) -- Start
                    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                Else
                    Dim mdecEMI_NetPayPercentage As Decimal = ConfigParameter._Object._Advance_NetPayPercentage
                    Dim mintTranheadUnkid As Integer = ConfigParameter._Object._Advance_NetPayTranheadUnkid
                    If mdecEMI_NetPayPercentage > 0 Then
                        Dim ds As DataSet = Nothing
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        Dim dsMapped As DataSet = Nothing
                        'Sohail (06 Jul 2018) -- End
                        'If mintTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                            Dim objTnALeave As New clsTnALeaveTran
                            ds = objTnALeave.GetPreviousNetPay(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtDeducationStartDate, dtDeducationEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, dtDeducationStartDate.AddDays(-1), "List", intEmployeeId.ToString(), "")
                        'Else 'Sohail (06 Jul 2018)
                        If mintTranheadUnkid > 0 Then 'Sohail (06 Jul 2018)
                            Dim objMaster As New clsMasterData
                            Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), dtDeducationStartDate.AddDays(-1), 0, 0, True, False, Nothing, False)
                            Dim strFilter As String = ""
                            Dim dtStart As Date = dtDeducationStartDate
                            Dim dtEnd As Date = dtDeducationEndDate
                            Dim strDBName As String = FinancialYear._Object._DatabaseName
                            Dim intYearId As Integer = FinancialYear._Object._YearUnkid
                            If intPrevPeriod <= 0 Then
                                strFilter = " 1 = 2 "
                            Else
                                objPeriod = New clscommom_period_Tran
                                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                                dtStart = objPeriod._Start_Date
                                dtEnd = objPeriod._End_Date
                                If intYearId <> objPeriod._Yearunkid Then
                                    intYearId = objPeriod._Yearunkid
                                    strDBName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid)
                                End If
                                strFilter = " prpayrollprocess_tran.tranheadunkid = " & mintTranheadUnkid & " "
                            End If
                            Dim objProcess As New clsPayrollProcessTran
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'ds = objProcess.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", intEmployeeId, intPrevPeriod, , , strFilter)
                            dsMapped = objProcess.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", intEmployeeId, intPrevPeriod, , , strFilter)
                            'Sohail (06 Jul 2018) -- End
                        End If
                        If ds.Tables(0).Rows.Count <= 0 Then
                            dtRow.Item("image") = imgError
                            'If mintTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 61, "Sorry, you can not apply for this Advance: Reason, There is no previous salary history for this employee.")
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'Else
                            '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 62, "Sorry, you can not apply for this Advance: Reason, There is no previous salary history for this employee for the net pay head set on loan scheme master screen.")
                            'End If
                            'Sohail (06 Jul 2018) -- End
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        ElseIf mintTranheadUnkid > 0 AndAlso dsMapped.Tables(0).Rows.Count <= 0 Then
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 62, "Sorry, you can not apply for this Advance: Reason, There is no previous salary history for this employee for the net pay head set on loan scheme master screen.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'Sohail (06 Jul 2018) -- End
                        Else
                            Dim decAmt As Decimal = decLoanAmount
                            If decBaseExRate <> 0 Then
                                decAmt = decAmt * decBaseExRate / decPaidExRate
                            Else
                                decAmt = decAmt * decPaidExRate
                            End If
                            Dim decPrevNetPay As Decimal = 0
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            Dim decPrevActualNetPay As Decimal = 0
                            Dim decOtherLoanEMI As Decimal = 0
                            'Sohail (06 Jul 2018) -- End
                            If mintTranheadUnkid <= 0 Then
                                'Gajanan (23-May-2018) -- Start
                                'CCK Enhancement - Ref # 180 - On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
                                'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))
                                decPrevNetPay = CDec(ds.Tables(0).Rows(0)("basicsalary"))
                                'Gajanan (23-May-2018) -- End
                            Else
                                'Sohail (06 Jul 2018) -- Start
                                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                                'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("amountPaidCurrency"))
                                decPrevNetPay = CDec(dsMapped.Tables(0).Rows(0)("amountPaidCurrency"))
                                'Sohail (06 Jul 2018) -- End
                            End If
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'Dim decCheckPoint As Decimal = decPrevNetPay - decAmt
                            decPrevActualNetPay = CDec(ds.Tables(0).Rows(0)("basicsalary"))

                            Dim objLoanAdvance As New clsLoan_Advance
                            Dim dsLoanCalc As DataSet = objLoanAdvance.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                      dtDeducationStartDate, dtDeducationEndDate, ConfigParameter._Object._UserAccessModeSetting, True, _
                                                      "Loan", dtDeducationEndDate.AddDays(1), intEmployeeId.ToString(), 0, _
                                                      dtDeducationStartDate, True, , , True, True, , True, True, , , , 0, , , , , Nothing)
                            Dim dtTable As DataTable = New DataView(dsLoanCalc.Tables(0), "statusunkid IN (" & CInt(enLoanStatus.IN_PROGRESS) & ", " & CInt(enLoanStatus.ON_HOLD) & ") AND transactionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
                            For Each dRow As DataRow In dtTable.Rows

                                If CInt(dRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dRow.Item("iscalculateinterest").ToString) = False Then
                                    decOtherLoanEMI += 0
                                ElseIf CInt(dRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dRow.Item("iscalculateinterest").ToString) = True Then
                                    decOtherLoanEMI += CDec(dRow.Item("TotalMonthlyInterestPaidCurrency").ToString) + CDec(dRow.Item("TotInterestAmountPaidCurrency").ToString)
                                Else
                                    decOtherLoanEMI += CDec(dRow.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(dRow.Item("TotPMTAmountPaidCurrency").ToString)
                                End If
                            Next
                            Dim dsLoan As DataSet = objProcesspendingloan.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtDeducationStartDate, dtDeducationEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "Loan", 0, "lnloan_process_pending_loan.employeeunkid = " & intEmployeeId & " AND lnloan_process_pending_loan.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " AND lnloan_process_pending_loan.loan_statusunkid IN ( " & CInt(enLoanApplicationStatus.PENDING) & ", " & CInt(enLoanApplicationStatus.APPROVED) & " ) ")
                            For Each drow As DataRow In dsLoan.Tables(0).Rows
                                If CBool(drow.Item("isloan")) = True Then
                                    Dim dtEnd_Date As Date = DateAdd(DateInterval.Month, CInt(drow.Item("noofinstallment")), dtDeducationStartDate).AddDays(-1)
                                    Dim decIntAmt As Decimal
                                    Dim decEMI As Decimal
                                    Dim decTotIntAmt As Decimal
                                    Dim decTotEMI As Decimal
                                    objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(drow.Item("Amount")), CInt(DateDiff(DateInterval.Day, dtDeducationStartDate, dtEnd_Date.Date)), CDec(drow.Item("interest_rate")), CType(drow.Item("loancalctype_id"), enLoanCalcId), CType(drow.Item("interest_calctype_id"), enLoanInterestCalcType), CInt(drow.Item("noofinstallment")), CInt(DateDiff(DateInterval.Day, dtDeducationStartDate.Date, dtDeducationEndDate.Date.AddDays(1))), CDec(drow.Item("installmentamt")), decIntAmt, decEMI, decTotIntAmt, decTotEMI)
                                    decOtherLoanEMI += decEMI
                                Else
                                    decOtherLoanEMI += CDec(drow.Item("amount"))
                                End If

                            Next
                            'Sohail (19 Mar 2020) -- Start
                            'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                            'Dim decCheckPoint As Decimal = decPrevActualNetPay - decAmt - decOtherLoanEMI
                            Dim decCheckPoint As Decimal = decAmt + decOtherLoanEMI
                            'Sohail (19 Mar 2020) -- End
                            'Sohail (06 Jul 2018) -- End
                            'Sohail (19 Mar 2020) -- Start
                            'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                            'If decPrevNetPay <= 0 OrElse ((decPrevNetPay * mdecEMI_NetPayPercentage) / 100) > decCheckPoint Then
                            If decPrevNetPay <= 0 OrElse ((decPrevNetPay * mdecEMI_NetPayPercentage) / 100) < decCheckPoint Then
                                'Sohail (19 Mar 2020) -- End
                                dtRow.Item("image") = imgError
                                If mintTranheadUnkid <= 0 Then
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 63, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 54, "of previous net pay.") & " " & Format(decPrevNetPay, GUI.fmtCurrency)
                                Else
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 64, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 60, "of previous net pay head") & " " & Format(decPrevNetPay, GUI.fmtCurrency)
                                End If
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If
                    End If
                    'Sohail (16 May 2018) -- End

                    'Gajanan (23-May-2018) -- Start
                    'CCK Enhancement - Ref # 180 - On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
                    If ConfigParameter._Object._Advance_DontAllowAfterDays > 0 Then
                        If dtDeducationStartDate.AddDays(ConfigParameter._Object._Advance_DontAllowAfterDays) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 65, "Sorry, you can not apply for Advance: Reason, Days to apply for Advance has been exceeded.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                    'Gajanan (23-May-2018) -- End

                End If
                'Sohail (22 Sep 2017) -- End

                '------------------------------ INSERTING PROCESS PENDING LOAN.

                If objProcessPending IsNot Nothing Then objProcessPending = Nothing

                objProcessPending = New clsProcess_pending_loan

                If radLoan.Checked = True Then
                    objProcessPending._Isloan = True
                    objProcessPending._Loanschemeunkid = intLoanSchemeUnkid
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
                    objProcessPending._Loan_Account_No = dtRow.Item("lnaccno").ToString
                    'Hemant (30 Aug 2019) -- End
                ElseIf radAdvance.Checked = True Then
                    objProcessPending._Isloan = False
                    objProcessPending._Loanschemeunkid = -1
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
                    objProcessPending._Loan_Account_No = ""
                    'Hemant (30 Aug 2019) -- End
                End If
                If ConfigParameter._Object._LoanApplicationNoType = 0 Then
                    objProcessPending._Application_No = dtRow.Item("lnappno").ToString
                End If

                objProcessPending._Application_Date = DtLoanDate.Date
                objProcessPending._Loan_Amount = decLoanAmount
                objProcessPending._Employeeunkid = intEmployeeId
                If dtRow.Item("exentityname").ToString.Trim <> "" Then
                    objProcessPending._Isexternal_Entity = True
                    objProcessPending._External_Entity_Name = dtRow.Item("exentityname").ToString
                Else
                    objProcessPending._Isexternal_Entity = False
                    objProcessPending._External_Entity_Name = ""
                End If
                objProcessPending._Countryunkid = cboCurrncy.SelectedValue
                objProcessPending._DeductionPeriodunkid = intDeductionPeriodUnkid
                objProcessPending._InstallmentAmount = decEMIAmount
                objProcessPending._NoOfInstallment = intInstallment
                objProcessPending._Loan_Statusunkid = 1
                objProcessPending._Emp_Remark = ""
                objProcessPending._Userunkid = User._Object._Userunkid
                objProcessPending._Isvoid = False
                objProcessPending._Voiddatetime = Nothing
                objProcessPending._Voiduserunkid = -1
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'objProcessPending._Approved_Amount = decLoanAmount
                objProcessPending._Approved_Amount = 0.0
                'objProcessPending._Approverunkid = intApproverUnkid
                objProcessPending._Approverunkid = 0
                'Nilay (01-Mar-2016) -- End
                objProcessPending._IsImportedLoan = True
                'Nilay (15-Dec-2015) -- Start
                objProcessPending._IsLoanApprover_ForLoanScheme = ConfigParameter._Object._IsLoanApprover_ForLoanScheme
                'Nilay (15-Dec-2015) -- End

                If objProcessPending.Insert(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            ConfigParameter._Object._LoanApplicationNoType, _
                                            ConfigParameter._Object._LoanApplicationPrifix) Then

                    intProcessPendingUnkid = objProcessPending._Processpendingloanunkid
                    objProcessPending._Processpendingloanunkid = intProcessPendingUnkid
                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'objProcessPending._Loan_Statusunkid = 4
                    objProcessPending._Loan_Statusunkid = 2
                    objProcessPending._Approved_Amount = decLoanAmount
                    objProcessPending._Approverunkid = intApproverUnkid
                    'Nilay (01-Mar-2016) -- End
                    objProcessPending.Update(Nothing)

                    If objProcessPending._Message <> "" Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objProcessPending._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If

                    Dim bln As Boolean = (New clsloanapproval_process_Tran).UpdateVisibleStatusByProcessUnkId(intProcessPendingUnkid, intEmployeeId, intApproverUnkid, intApproverEmpid)
                    If bln = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objProcessPending._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                objLoan_Advance = New clsLoan_Advance

                objLoan_Advance._Effective_Date = dtDeducationStartDate.Date
                objLoan_Advance._Employeeunkid = intEmployeeId
                objLoan_Advance._Isbrought_Forward = False
                objLoan_Advance._Loan_Purpose = ""
                objLoan_Advance._Loanvoucher_No = dtRow.Item("lnvocno")
                objLoan_Advance._Periodunkid = intPeriodUnkid
                objLoan_Advance._LoanStatus = 1
                objLoan_Advance._Deductionperiodunkid = intDeductionPeriodUnkid
                objLoan_Advance._Processpendingloanunkid = intProcessPendingUnkid
                objLoan_Advance._Isloan = radLoan.Checked

                If radLoan.Checked Then
                    objLoan_Advance._Advance_Amount = 0
                    objLoan_Advance._Loan_Amount = decLoanAmount
                Else
                    objLoan_Advance._Advance_Amount = decLoanAmount
                    objLoan_Advance._Loan_Amount = 0
                End If
                objLoan_Advance._Approverunkid = intApproverUnkid
                objLoan_Advance._CountryUnkid = cboCurrncy.SelectedValue

                If radLoan.Checked Then
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'objLoan_Advance._Calctype_Id = intCalType
                    objLoan_Advance._Calctype_Id = CInt(cboLoanCalType.SelectedValue)
                    objLoan_Advance._Interest_Calctype_Id = CInt(cboInterestCalcType.SelectedValue)
                    'Sohail (15 Dec 2015) -- End
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    objLoan_Advance._Mapped_TranheadUnkid = CInt(cboMappedHead.SelectedValue)
                    'Sohail (29 Apr 2019) -- End
                Else
                    objLoan_Advance._Calctype_Id = 0
                    objLoan_Advance._Interest_Calctype_Id = 0 'Sohail (15 Dec 2015)
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    objLoan_Advance._Mapped_TranheadUnkid = 0
                    'Sohail (29 Apr 2019) -- End
                End If
                objLoan_Advance._Exchange_rate = decPaidExRate
                objLoan_Advance._Basecurrency_amount = decLoanAmount / decPaidExRate
                objLoan_Advance._Opening_balance = decLoanAmount / decPaidExRate
                objLoan_Advance._Opening_balancePaidCurrency = decLoanAmount

                If radLoan.Checked = True Then
                    If dtRow.Item("exentityname").ToString.Trim <> "" Then
                        objLoan_Advance._Balance_Amount = decLoanAmount / decPaidExRate
                        objLoan_Advance._Balance_AmountPaidCurrency = decLoanAmount
                    End If
                    objLoan_Advance._Loanschemeunkid = intLoanSchemeUnkid

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'objLoan_Advance._Interest_Rate = decInterestRate
                    'objLoan_Advance._Interest_Amount = mdecInstrAmount / decPaidExRate
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'If CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest Then
                    If CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboLoanCalType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                        'Sohail (29 Apr 2019) -- End
                    objLoan_Advance._Interest_Rate = decInterestRate
                        objLoan_Advance._Interest_Amount = mdecInstrAmount / decPaidExRate
                    Else
                        objLoan_Advance._Interest_Rate = 0
                        objLoan_Advance._Interest_Amount = 0
                    End If
                    'Sohail (15 Dec 2015) -- End
                    objLoan_Advance._Loan_Duration = intInstallment
                    objLoan_Advance._Net_Amount = (decLoanAmount + mdecInstrAmount) / decPaidExRate
                    objLoan_Advance._Emi_Tenure = intInstallment
                    objLoan_Advance._Emi_Amount = mdecInstallmentAmount / decPaidExRate
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    objLoan_Advance._EMI_Principal_amount = mdecInstallmentAmount - mdecInstrAmount
                    objLoan_Advance._EMI_Interest_amount = mdecInstrAmount
                    'Sohail (15 Dec 2015) -- End
                Else
                    If dtRow.Item("exentityname").ToString.Trim <> "" Then
                        objLoan_Advance._Balance_Amount = decLoanAmount / decPaidExRate
                        objLoan_Advance._Balance_AmountPaidCurrency = decLoanAmount
                    End If
                    objLoan_Advance._Loanschemeunkid = 0
                    objLoan_Advance._Net_Amount = 0
                    objLoan_Advance._Emi_Tenure = 1
                End If
                objLoan_Advance._Userunkid = User._Object._Userunkid
                objLoan_Advance._Voiduserunkid = -1
                objLoan_Advance._Voiddatetime = Nothing
                objLoan_Advance._Isvoid = False

                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'objLoan_Advance._RateOfInterest = decInterestRate
                objLoan_Advance._RateOfInterest = objLoan_Advance._Interest_Rate
                'Sohail (15 Dec 2015) -- End
                objLoan_Advance._NoOfInstallments = IIf(radLoan.Checked = True, intInstallment, 1)
                objLoan_Advance._InstallmentAmount = mdecInstallmentAmount
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid) Then

                'Nilay (25-Mar-2016) -- Start
                'If objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName) Then
                If objLoan_Advance.Insert(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          dtAssignStartDate, _
                                          dtAssignEndDate, _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True) Then
                    'Nilay (25-Mar-2016) -- End

                    'Sohail (15 Dec 2015) -- End
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 38, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objLoan_Advance._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 37, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 67, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Apr 2019) -- End

#End Region

#Region " Form's Event(s) "

    Private Sub frmImportLoan_AdvanceWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            'Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
            radLoan.Checked = True
            Dim strtext As String = Language.getMessage(mstrModuleName, 44, "Note : If Loan is already cacluated with interest in the file you are trying to import. Please make sure that you import with") & " '"
            strtext &= Language.getMessage(mstrModuleName, 43, "No Interest") & "' " & Language.getMessage(mstrModuleName, 45, "as calulation type.")
            objlblNote.Text = strtext
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            strtext = ""
            strtext = Language.getMessage(mstrModuleName, 51, "Note : If you are importing Loan Data with External Approver then Please specify User Name in Approver Code column.")
            objlblNote1.Text = strtext
            'Nilay (01-Mar-2016) -- End

            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportLoan_AdvanceWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            'Call SetMessages()

            objfrm.displayDialog(Me)

            'Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLoanApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim frm As New frmCommonSearch
        'Try

        '    If User._Object._Isrighttoleft = True Then
        '        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
        '        frm.RightToLeftLayout = True
        '        Call Language.ctlRightToLeftlayOut(frm)
        '    End If

        '    With frm
        '        .ValueMember = cboApprovedBy.ValueMember
        '        .DisplayMember = cboApprovedBy.DisplayMember
        '        .DataSource = cboApprovedBy.DataSource
        '        .CodeMember = "Code"
        '    End With

        '    If frm.DisplayDialog Then
        '        cboApprovedBy.SelectedValue = frm.SelectedValue
        '        cboApprovedBy.Focus()
        '    End If

        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "objbtnSearchLoanApprover_Click", mstrModuleName)
        'Finally
        '    If frm IsNot Nothing Then frm.Dispose()
        'End Try
    End Sub

#End Region

#Region " Control's Event(s) "

    'Private Sub radAdvance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Call DoPanelOperation(pnlLoanMapping, pnlAdvance)
    'End Sub

    'Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Call DoPanelOperation(pnlAdvance, pnlLoanMapping)
    'End Sub

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If cmb.Text <> "" And cmb.Name <> cboCurrncy.Name And cmb.Name <> cboLoanCalType.Name Then
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If cmb.Text <> "" AndAlso cmb.Name <> cboCurrncy.Name AndAlso cmb.Name <> cboLoanCalType.Name AndAlso cmb.Name <> cboInterestCalcType.Name Then
            If cmb.Text <> "" AndAlso cmb.Name <> cboCurrncy.Name AndAlso cmb.Name <> cboLoanCalType.Name AndAlso cmb.Name <> cboInterestCalcType.Name AndAlso cmb.Name <> cboMappedHead.Name Then
                'Sohail (29 Apr 2019) -- End
                'Sohail (15 Dec 2015) -- End

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                'If radLoan.Checked = True Then
                For Each cr As Control In pnlLoanMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'If cr.Name <> cmb.Name AndAlso cr.Name <> cboCurrncy.Name AndAlso cr.Name <> cboLoanCalType.Name Then
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        'If cr.Name <> cmb.Name AndAlso cr.Name <> cboCurrncy.Name AndAlso cr.Name <> cboLoanCalType.Name AndAlso cr.Name <> cboInterestCalcType.Name Then
                        If cr.Name <> cmb.Name AndAlso cr.Name <> cboCurrncy.Name AndAlso cr.Name <> cboLoanCalType.Name AndAlso cr.Name <> cboInterestCalcType.Name AndAlso cr.Name <> cboMappedHead.Name Then
                            'Sohail (29 Apr 2019) -- End
                            'Sohail (15 Dec 2015) -- End
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next
                'End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Dependant Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private Sub cboLoanCalType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanCalType.SelectedIndexChanged
        Try
            lblLoanInstallmentAmt.Visible = True
            lblLoanPrincipalAmt.Visible = False
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If CInt(cboLoanCalType.SelectedValue) = enLoanCalcId.No_Interest Then
            If CInt(cboLoanCalType.SelectedValue) = enLoanCalcId.No_Interest OrElse CInt(cboLoanCalType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                'Sohail (29 Apr 2019) -- End
                cboInterestCalcType.Enabled = False
                cboInterestCalcType.SelectedValue = 0
            Else
                cboInterestCalcType.Enabled = True
            End If
            If CInt(cboLoanCalType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                lblLoanInstallmentAmt.Visible = False
                lblLoanPrincipalAmt.Visible = True
            End If
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboLoanCalType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                cboMappedHead.Enabled = True
            Else
                cboMappedHead.Enabled = False
                cboMappedHead.SelectedValue = 0
            End If
            'Sohail (29 Apr 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanCalType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Dec 2015) -- End

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private Sub cboMappedHead_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboMappedHead.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMappedHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMappedHead.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMappedHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMappedHead.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMappedHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMappedHead.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Apr 2019) -- End

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportLoan_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportLoan.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportLoan.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportLoan_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportLoan_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportLoan.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportLoan.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Please select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    If radAdvance.Checked = False AndAlso radLoan.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Please select Importaion Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Please select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    If radLoan.Checked Then
                        lblLoanAmount.Visible = True
                        lblAdvanceAmount.Visible = False
                        cboLoanAmount.Visible = True
                        cboAdvanceAmount.Visible = False

                        cboLoanScheme.Enabled = True
                        cboInterestRate.Enabled = True
                        cboNoOfInstallment.Enabled = True
                        cboLoanCalType.Enabled = True
                        cboInstallmentAmount.Enabled = True
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        cboInterestCalcType.Enabled = True
                        'Sohail (15 Dec 2015) -- End
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        cboMappedHead.Enabled = True
                        'Sohail (29 Apr 2019) -- End
                        'Hemant (30 Aug 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
                        cboLoanAccountNo.Enabled = True
                        'Hemant (30 Aug 2019) -- End
                    Else
                        lblLoanAmount.Visible = False
                        lblAdvanceAmount.Visible = True
                        cboLoanAmount.Visible = False
                        cboAdvanceAmount.Visible = True

                        cboLoanScheme.Enabled = False
                        cboInterestRate.Enabled = False
                        cboNoOfInstallment.Enabled = False
                        cboLoanCalType.Enabled = False
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        cboInterestCalcType.Enabled = False
                        'Sohail (15 Dec 2015) -- End
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        cboMappedHead.Enabled = False
                        'Sohail (29 Apr 2019) -- End
                        cboInstallmentAmount.Enabled = False
                        'Hemant (30 Aug 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES (Need option to import loan account number)
                        cboLoanAccountNo.Enabled = False
                        'Hemant (30 Aug 2019) -- End
                    End If

                    Call SetDataCombo()
                Case eZeeWizImportLoan.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        If IsValid() = False Then
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                Case eZeeWizImportLoan.Pages.IndexOf(WizPageImporting)
                    Me.Close()

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportLoan_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkGenerateFile_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkGenerateFile.LinkClicked
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                'strFilePath = ObjFile.DirectoryName & "\"
                'strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'strFilePath &= ObjFile.Extension
                Dim dTable As New DataTable("Import_Format")
                dTable.Columns.Add("Employeecode") : dTable.Columns.Add("EmployeeFirstname") : dTable.Columns.Add("EmployeeSurname")
                dTable.Columns.Add("LoanVocNo") : dTable.Columns.Add("LoanApplicationNo") : dTable.Columns.Add("LoanDate")
                dTable.Columns.Add("AssignmentPeriod") : dTable.Columns.Add("DeductionPeriod") : dTable.Columns.Add("LoanScheme")
                dTable.Columns.Add("LoanAmount") : dTable.Columns.Add("InterestRate") : dTable.Columns.Add("NoOfInstallment")
                dTable.Columns.Add("ExternalEntityName") : dTable.Columns.Add("InstallmentAmount") : dTable.Columns.Add("ApproverCode")
                dTable.Columns.Add("ApproverName") : dTable.Columns.Add("ApproverLevel") : dTable.Columns.Add("LoanAccountNo")
                'Hemant (30 Aug 2019) -- [LoanAccountNo]

                dsList.Tables.Add(dTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(dlgSaveFile.FileName, dsList)
                OpenXML_Export(dlgSaveFile.FileName, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 46, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkGenerateFile_LinkClicked", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList = Nothing
        End Try
    End Sub

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In pnlLoanMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing) Select (p)).ToList
            For Each ctrl In lstCombos
                If radAdvance.Checked = True Then
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'If ctrl.Name = cboLoanScheme.Name OrElse ctrl.Name = cboInterestRate.Name OrElse ctrl.Name = cboInstallmentAmount.Name OrElse ctrl.Name = cboLoanCalType.Name OrElse ctrl.Name = cboInterestCalcType.Name OrElse ctrl.Name = cboNoOfInstallment.Name OrElse ctrl.Name = cboLoanAmount.Name Then Continue For
                    If ctrl.Name = cboLoanScheme.Name OrElse ctrl.Name = cboInterestRate.Name OrElse ctrl.Name = cboInstallmentAmount.Name OrElse ctrl.Name = cboLoanCalType.Name OrElse ctrl.Name = cboInterestCalcType.Name OrElse ctrl.Name = cboNoOfInstallment.Name OrElse ctrl.Name = cboLoanAmount.Name OrElse ctrl.Name = cboMappedHead.Name OrElse _
                       ctrl.Name = cboLoanAccountNo.Name Then Continue For
                    'Hemant (30 Aug 2019) -- [ctrl.Name = cboLoanAccountNo.Name]
                    'Sohail (29 Apr 2019) -- End
                End If
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper Like "*" & strName.ToUpper & "*") Select (p)).FirstOrDefault
                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In pnlLoanMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Dec 2015) -- End

#End Region

    'Shani(26-Nov-2015) -- Start
    'ENHANCEMENT : Add Loan Import Form
#Region "Old Code"

    '#Region " Private Variable(s) "

    'Private ReadOnly mstrModuleName As String = "frmImportLoan_AdvanceWizard"
    'Private mds_ImportData As DataSet
    'Private m_dsImportData_eZee As DataSet
    'Private mdt_ImportData_Others As New DataTable
    'Dim dvGriddata As DataView = Nothing

    'Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    'Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    'Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

    '#End Region

    '#Region " Private Function(s) & Method(s) "

    'Private Sub DoPanelOperation(ByVal pnlHidePanel As Panel, ByVal pnlVisiblePanel As Panel)
    '    Try
    '        pnlHidePanel.Visible = False : pnlVisiblePanel.Visible = True
    '        pnlVisiblePanel.Location = New Point(2, 26)
    '        pnlVisiblePanel.Size = New Size(605, 356)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "DoPanelOperation", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub CreateDataTable()
    '    Try
    '        Dim blnIsNotThrown As Boolean = True
    '        ezWait.Active = True

    '        mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("firstname", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("surname", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("vocno", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("ladate", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("assignperiod", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("dedperiod", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("loanscheme", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("la_amount", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("interest", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("loanduration", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("loanschdule", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("schduleby", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("exentityname", System.Type.GetType("System.String")).DefaultValue = ""
    '        mdt_ImportData_Others.Columns.Add("balanceamount", System.Type.GetType("System.String")).DefaultValue = ""

    '        mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
    '        mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
    '        mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

    '        Dim dtTemp() As DataRow = Nothing

    '        If radLoan.Checked = True Then
    '            dtTemp = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
    '        ElseIf radAdvance.Checked = True Then
    '            dtTemp = mds_ImportData.Tables(0).Select("" & cboAEmployeeCode.Text & " IS NULL")
    '        End If


    '        For i As Integer = 0 To dtTemp.Length - 1
    '            mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
    '        Next
    '        mds_ImportData.AcceptChanges()

    '        Dim drNewRow As DataRow

    '        For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
    '            blnIsNotThrown = CheckInvalidData(dtRow)

    '            If blnIsNotThrown = False Then Exit Sub

    '            drNewRow = mdt_ImportData_Others.NewRow
    '            If radLoan.Checked = True Then
    '                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

    '                If cboFirstname.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("firstname") = dtRow.Item(cboFirstname.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("firstname") = ""
    '                End If

    '                If cboLastname.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("surname") = dtRow.Item(cboLastname.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("surname") = ""
    '                End If

    '                If cboLoanVocNo.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("vocno") = dtRow.Item(cboLoanVocNo.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("vocno") = ""
    '                End If

    '                If cboLoanAssignedDate.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("ladate") = dtRow.Item(cboLoanAssignedDate.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("ladate") = ""
    '                End If

    '                If cboLoanAssignedPeriod.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("assignperiod") = dtRow.Item(cboLoanAssignedPeriod.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("assignperiod") = ""
    '                End If

    '                If cboLoanDeductionPeriod.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("dedperiod") = dtRow.Item(cboLoanDeductionPeriod.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("dedperiod") = ""
    '                End If

    '                If cboLoanScheme.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("loanscheme") = dtRow.Item(cboLoanScheme.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("loanscheme") = ""
    '                End If

    '                If cboLoanAmount.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("la_amount") = dtRow.Item(cboLoanAmount.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("la_amount") = 0
    '                End If

    '                If cboInterestRate.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("interest") = dtRow.Item(cboInterestRate.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("interest") = 0
    '                End If

    '                If cboLoanDuration.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("loanduration") = dtRow.Item(cboLoanDuration.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("loanduration") = 1
    '                End If

    '                'If cboLoanSchedule.Text.Trim.Length > 0 Then
    '                '    drNewRow.Item("loanschdule") = dtRow.Item(cboLoanSchedule.Text).ToString.Trim
    '                'Else
    '                '    drNewRow.Item("loanschdule") = ""
    '                'End If

    '                If cboLoanSchedule.SelectedValue > 0 Then
    '                    drNewRow.Item("loanschdule") = cboLoanSchedule.SelectedValue.ToString
    '                Else
    '                    drNewRow.Item("loanschdule") = ""
    '                End If

    '                If cboSchduleBy.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("schduleby") = dtRow.Item(cboSchduleBy.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("schduleby") = 0
    '                End If

    '                If cboExternalEntity.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("exentityname") = dtRow.Item(cboExternalEntity.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("exentityname") = ""
    '                End If

    '                If cboLoanBalance.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("balanceamount") = dtRow.Item(cboLoanBalance.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("balanceamount") = 0
    '                End If

    '            ElseIf radAdvance.Checked = True Then

    '                drNewRow.Item("ecode") = dtRow.Item(cboAEmployeeCode.Text).ToString.Trim

    '                If cboAFirstname.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("firstname") = dtRow.Item(cboAFirstname.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("firstname") = ""
    '                End If

    '                If cboASurname.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("surname") = dtRow.Item(cboASurname.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("surname") = ""
    '                End If

    '                If cboAdvanceVocNo.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("vocno") = dtRow.Item(cboAdvanceVocNo.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("vocno") = ""
    '                End If

    '                If cboAAssignDate.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("ladate") = dtRow.Item(cboAAssignDate.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("ladate") = ""
    '                End If

    '                If cboAAssignPeriod.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("assignperiod") = dtRow.Item(cboAAssignPeriod.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("assignperiod") = ""
    '                End If

    '                If cboADeductPeriod.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("dedperiod") = dtRow.Item(cboADeductPeriod.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("dedperiod") = ""
    '                End If

    '                If cboAdvanceAmt.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("la_amount") = dtRow.Item(cboAdvanceAmt.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("la_amount") = 0
    '                End If

    '                drNewRow.Item("loanduration") = 1

    '                If cboAExEntity.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("exentityname") = dtRow.Item(cboAExEntity.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("exentityname") = ""
    '                End If

    '                If cboAdvaceBalance.Text.Trim.Length > 0 Then
    '                    drNewRow.Item("balanceamount") = dtRow.Item(cboAdvaceBalance.Text).ToString.Trim
    '                Else
    '                    drNewRow.Item("balanceamount") = 0
    '                End If
    '            End If

    '            drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
    '            drNewRow.Item("message") = ""
    '            drNewRow.Item("status") = ""
    '            drNewRow.Item("objStatus") = ""

    '            mdt_ImportData_Others.Rows.Add(drNewRow)
    '            objTotal.Text = CStr(Val(objTotal.Text) + 1)
    '        Next

    '        If blnIsNotThrown = True Then
    '            dgData.AutoGenerateColumns = False
    '            colhEmployee.DataPropertyName = "ecode"
    '            colhMessage.DataPropertyName = "message"
    '            objcolhImage.DataPropertyName = "image"
    '            colhStatus.DataPropertyName = "status"
    '            objcolhstatus.DataPropertyName = "objStatus"
    '            dvGriddata = New DataView(mdt_ImportData_Others)
    '            dgData.DataSource = dvGriddata
    '        End If

    '        Call Import_Data()

    '        ezWait.Active = False
    '        eZeeWizImportLoan.BackEnabled = False
    '        eZeeWizImportLoan.CancelText = "Finish"

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
    '    Try
    '        With dtRow
    '            If radLoan.Checked = True Then

    '                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboLoanVocNo.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboLoanAssignedDate.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Assignment Date cannot be blank. Please set Loan Assignment Date in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboLoanAssignedPeriod.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Loan Assignment Period cannot be blank. Please set Loan Assignment Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboLoanDeductionPeriod.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Loan Deduction Period cannot be blank. Please set Loan Deduction Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboLoanScheme.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Loan Scheme cannot be blank. Please set Loan Scheme in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboLoanAmount.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Loan Amount cannot be blank. Please set Loan Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboLoanDuration.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Loan Duration cannot be blank. Please set Loan Duration in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                'If .Item(cboLoanSchedule.Text).ToString.Trim.Length <= 0 Then
    '                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                '    Return False
    '                'End If

    '                If cboLoanSchedule.SelectedValue <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboSchduleBy.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Schduled Value cannot be blank. Please set Schduled Value in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboExternalEntity.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "External Entity cannot be blank. Please set External Entity in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '                If .Item(cboLoanBalance.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Loan Balance cannot be blank. Please set Loan Balance in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If

    '            ElseIf radAdvance.Checked = True Then

    '                If .Item(cboAEmployeeCode.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                    cboEmployeeCode.Focus()
    '                    Return False
    '                End If

    '                If .Item(cboAdvanceVocNo.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Advance Voc. No. cannot be blank. Please set Advance Voc. No. in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                    cboLoanVocNo.Focus()
    '                    Return False
    '                End If

    '                If .Item(cboAAssignDate.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Advance Assignment Date cannot be blank. Please set Advance Assignment Date in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                    cboAAssignDate.Focus()
    '                    Return False
    '                End If

    '                If .Item(cboAAssignPeriod.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Advance Assignment Period cannot be blank. Please set Advance Assignment Period in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                    cboAAssignPeriod.Focus()
    '                    Return False
    '                End If

    '                If .Item(cboADeductPeriod.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Advance Deduction Period cannot be blank. Please set Advance Deduction Period in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                    cboADeductPeriod.Focus()
    '                    Return False
    '                End If

    '                If .Item(cboAdvanceAmt.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Advance Amount cannot be blank. Please set Advance Amount in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                    cboLoanAmount.Focus()
    '                    Return False
    '                End If

    '                If .Item(cboAExEntity.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "External Entity cannot be blank. Please set External Entity in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                    cboExternalEntity.Focus()
    '                    Return False
    '                End If

    '                If .Item(cboAdvaceBalance.Text).ToString.Trim.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Advance Balance cannot be blank. Please set Loan Balance in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                    cboLoanBalance.Focus()
    '                    Return False
    '                End If

    '            End If

    '        End With
    '        Return True
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
    '    Finally
    '    End Try
    'End Function

    'Private Sub SetDataCombo()
    '    Try

    '        If radLoan.Checked = True Then
    '            For Each ctrl As Control In pnlLoanMapping.Controls
    '                If TypeOf ctrl Is ComboBox AndAlso ctrl.Name <> "cboLoanSchedule" Then
    '                    Call ClearCombo(CType(ctrl, ComboBox))
    '                End If
    '            Next

    '            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
    '                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
    '                cboFirstname.Items.Add(dtColumns.ColumnName)
    '                cboLastname.Items.Add(dtColumns.ColumnName)
    '                cboLoanVocNo.Items.Add(dtColumns.ColumnName)
    '                cboLoanAssignedDate.Items.Add(dtColumns.ColumnName)
    '                cboLoanAssignedPeriod.Items.Add(dtColumns.ColumnName)
    '                cboLoanDeductionPeriod.Items.Add(dtColumns.ColumnName)
    '                cboLoanScheme.Items.Add(dtColumns.ColumnName)
    '                cboLoanAmount.Items.Add(dtColumns.ColumnName)
    '                cboInterestRate.Items.Add(dtColumns.ColumnName)
    '                cboLoanDuration.Items.Add(dtColumns.ColumnName)
    '                'cboLoanSchedule.Items.Add(dtColumns.ColumnName)
    '                cboSchduleBy.Items.Add(dtColumns.ColumnName)
    '                cboExternalEntity.Items.Add(dtColumns.ColumnName)
    '                cboLoanBalance.Items.Add(dtColumns.ColumnName)
    '            Next

    '        ElseIf radAdvance.Checked = True Then
    '            For Each ctrl As Control In pnlAdvance.Controls
    '                If TypeOf ctrl Is ComboBox Then
    '                    Call ClearCombo(CType(ctrl, ComboBox))
    '                End If
    '            Next

    '            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
    '                cboAEmployeeCode.Items.Add(dtColumns.ColumnName)
    '                cboAFirstname.Items.Add(dtColumns.ColumnName)
    '                cboASurname.Items.Add(dtColumns.ColumnName)
    '                cboAdvanceVocNo.Items.Add(dtColumns.ColumnName)
    '                cboAAssignDate.Items.Add(dtColumns.ColumnName)
    '                cboAAssignPeriod.Items.Add(dtColumns.ColumnName)
    '                cboADeductPeriod.Items.Add(dtColumns.ColumnName)
    '                cboAdvanceAmt.Items.Add(dtColumns.ColumnName)
    '                cboAExEntity.Items.Add(dtColumns.ColumnName)
    '                cboAdvaceBalance.Items.Add(dtColumns.ColumnName)
    '            Next

    '        End If



    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub FillCombo()
    '    Dim dsCombo As New DataSet
    '    Dim objApprover As New clsLoan_Approver
    '    Dim objMasterData As New clsMasterData
    '    Try
    '        'Nilay (10-Oct-2015) -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'dsCombo = objApprover.GetList("List", True, True, True)
    '        dsCombo = objApprover.GetList(FinancialYear._Object._DatabaseName, _
    '                                      User._Object._Userunkid, _
    '                                      FinancialYear._Object._YearUnkid, _
    '                                      Company._Object._Companyunkid, _
    '                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                      ConfigParameter._Object._UserAccessModeSetting, _
    '                                      True, _
    '                                      ConfigParameter._Object._IsIncludeInactiveEmp, _
    '                                      "List", True, True, True)
    '        'Nilay (10-Oct-2015) -- End


    '        With cboApprovedBy
    '            .ValueMember = "approverunkid"
    '            .DisplayMember = "approvername"
    '            .DataSource = dsCombo.Tables("List")
    '            .SelectedValue = 0
    '        End With

    '        dsCombo = objMasterData.GetLoanScheduling("LoanScheduling")
    '        With cboLoanSchedule
    '            .ValueMember = "Id"
    '            .DisplayMember = "Name"
    '            .DataSource = dsCombo.Tables("LoanScheduling")
    '            .SelectedValue = 0
    '        End With

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
    '    Finally
    '        dsCombo.Dispose() : objApprover = Nothing : objMasterData = Nothing
    '    End Try
    'End Sub

    'Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
    '    Try
    '        cboComboBox.Items.Clear()
    '        AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
    '    End Try
    'End Sub

    'Private Function IsValid() As Boolean
    '    Try
    '        If radLoan.Checked = True Then

    '            If (cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboEmployeeCode.Focus()
    '                Return False
    '            End If

    '            If (cboLoanVocNo.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanVocNo.Focus()
    '                Return False
    '            End If

    '            If (cboLoanAssignedDate.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Assignment Date cannot be blank. Please set Loan Assignment Date in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanAssignedDate.Focus()
    '                Return False
    '            End If

    '            If (cboLoanAssignedPeriod.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Loan Assignment Period cannot be blank. Please set Loan Assignment Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanAssignedPeriod.Focus()
    '                Return False
    '            End If

    '            If (cboLoanDeductionPeriod.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Loan Deduction Period cannot be blank. Please set Loan Deduction Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanDeductionPeriod.Focus()
    '                Return False
    '            End If

    '            If (cboLoanScheme.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Loan Scheme cannot be blank. Please set Loan Scheme in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanScheme.Focus()
    '                Return False
    '            End If

    '            If (cboLoanAmount.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Loan Amount cannot be blank. Please set Loan Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanAmount.Focus()
    '                Return False
    '            End If

    '            If (cboLoanDuration.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Loan Duration cannot be blank. Please set Loan Duration in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanDuration.Focus()
    '                Return False
    '            End If

    '            'If (cboLoanSchedule.Text).ToString.Trim.Length <= 0 Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '            '    Return False
    '            'End If

    '            If cboLoanSchedule.SelectedValue <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanSchedule.Focus()
    '                Return False
    '            End If

    '            If (cboSchduleBy.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Schduled Value cannot be blank. Please set Schduled Value in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboSchduleBy.Focus()
    '                Return False
    '            End If

    '            If (cboExternalEntity.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "External Entity cannot be blank. Please set External Entity in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboExternalEntity.Focus()
    '                Return False
    '            End If

    '            If (cboLoanBalance.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Loan Balance cannot be blank. Please set Loan Balance in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
    '                cboLoanBalance.Focus()
    '                Return False
    '            End If

    '        ElseIf radAdvance.Checked = True Then

    '            If (cboAEmployeeCode.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                cboEmployeeCode.Focus()
    '                Return False
    '            End If

    '            If (cboAdvanceVocNo.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Advance Voc. No. cannot be blank. Please set Advance Voc. No. in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                cboLoanVocNo.Focus()
    '                Return False
    '            End If

    '            If (cboAAssignDate.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Advance Assignment Date cannot be blank. Please set Advance Assignment Date in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                cboAAssignDate.Focus()
    '                Return False
    '            End If

    '            If (cboAAssignPeriod.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Advance Assignment Period cannot be blank. Please set Advance Assignment Period in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                cboAAssignPeriod.Focus()
    '                Return False
    '            End If

    '            If (cboADeductPeriod.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Advance Deduction Period cannot be blank. Please set Advance Deduction Period in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                cboADeductPeriod.Focus()
    '                Return False
    '            End If

    '            If (cboAdvanceAmt.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Advance Amount cannot be blank. Please set Advance Amount in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                cboLoanAmount.Focus()
    '                Return False
    '            End If

    '            If (cboAExEntity.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "External Entity cannot be blank. Please set External Entity in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                cboExternalEntity.Focus()
    '                Return False
    '            End If

    '            If (cboAdvaceBalance.Text).ToString.Trim.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Advance Balance cannot be blank. Please set Loan Balance in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
    '                cboLoanBalance.Focus()
    '                Return False
    '            End If
    '        End If
    '        Return True
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try
    'End Function

    'Private Sub Import_Data()
    '    Try
    '        If mdt_ImportData_Others.Rows.Count <= 0 Then
    '            Exit Sub
    '        End If

    '        Dim objLoanScheme As New clsLoan_Scheme
    '        Dim objProcessPending As clsProcess_pending_loan = Nothing
    '        Dim objPeriod As New clscommom_period_Tran
    '        Dim objLoan_Advance As clsLoan_Advance

    '        Dim objEmp As New clsEmployee_Master

    '        Dim intLoanSchemeUnkid As Integer = -1
    '        Dim intProcessPendingUnkid As Integer = -1
    '        Dim intPeriodUnkid As Integer = -1
    '        Dim intDeductionPeriodUnkid As Integer = -1
    '        Dim intEmployeeId As Integer = -1

    '        For Each dtRow As DataRow In mdt_ImportData_Others.Rows
    '            Try
    '                dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
    '                Application.DoEvents()
    '                ezWait.Refresh()
    '            Catch ex As Exception
    '            End Try

    '            'S.SANDEEP [ 21 MAR 2013 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            If CDec(dtRow.Item("la_amount")) < CDec(dtRow.Item("balanceamount")) Then
    '                dtRow.Item("image") = imgError
    '                dtRow.Item("message") = Language.getMessage(mstrModuleName, 30, "Invalid Balance Amount.")
    '                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                dtRow.Item("objStatus") = 2
    '                objError.Text = CStr(Val(objError.Text) + 1)
    '                Continue For
    '            End If
    '            'S.SANDEEP [ 21 MAR 2013 ] -- END

    '            '------------------------------ CHECKING IF EMPLOYEE PRESENT.
    '            If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
    '                intEmployeeId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
    '                If intEmployeeId <= 0 Then
    '                    dtRow.Item("image") = imgError
    '                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 24, "Employee Not Found.")
    '                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                    dtRow.Item("objStatus") = 2
    '                    objError.Text = CStr(Val(objError.Text) + 1)
    '                    Continue For
    '                End If
    '            End If

    '            '------------------------------ CHECKING IF PERIOD PRESENT.
    '            If dtRow.Item("assignperiod").ToString.Trim.Length > 0 Then
    '                intPeriodUnkid = objPeriod.GetPeriodByName(dtRow.Item("assignperiod").ToString.Trim, enModuleReference.Payroll)
    '                If intPeriodUnkid <= 0 Then
    '                    dtRow.Item("image") = imgError
    '                    dtRow.Item("message") = dtRow.Item("assignperiod").ToString.Trim & " " & Language.getMessage(mstrModuleName, 28, ", Not Found.")
    '                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                    dtRow.Item("objStatus") = 2
    '                    objError.Text = CStr(Val(objError.Text) + 1)
    '                    Continue For
    '                Else
    '                    If IsDate(dtRow.Item("ladate")) Then
    '                        'Nilay (10-Oct-2015) -- Start
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'objPeriod._Periodunkid = intPeriodUnkid
    '                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodUnkid
    '                        'Nilay (10-Oct-2015) -- End

    '                        If CDate(dtRow.Item("ladate")) >= objPeriod._Start_Date.Date And CDate(dtRow.Item("ladate")) <= objPeriod._End_Date.Date Then
    '                        Else
    '                            dtRow.Item("image") = imgError
    '                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Date.")
    '                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                            dtRow.Item("objStatus") = 2
    '                            objError.Text = CStr(Val(objError.Text) + 1)
    '                            Continue For
    '                        End If
    '                    Else
    '                        dtRow.Item("image") = imgError
    '                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Date.")
    '                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                        dtRow.Item("objStatus") = 2
    '                        objError.Text = CStr(Val(objError.Text) + 1)
    '                        Continue For
    '                    End If
    '                End If
    '            End If

    '            If dtRow.Item("dedperiod").ToString.Trim.Length > 0 Then
    '                intDeductionPeriodUnkid = objPeriod.GetPeriodByName(dtRow.Item("dedperiod").ToString.Trim, enModuleReference.Payroll)
    '                If intDeductionPeriodUnkid <= 0 Then
    '                    dtRow.Item("image") = imgError
    '                    dtRow.Item("message") = dtRow.Item("dedperiod").ToString.Trim & " " & Language.getMessage(mstrModuleName, 28, ", Not Found.")
    '                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                    dtRow.Item("objStatus") = 2
    '                    objError.Text = CStr(Val(objError.Text) + 1)
    '                    Continue For
    '                End If
    '            End If

    '            If radLoan.Checked = True Then
    '                '------------------------------ CHECKING IF LOAN SCHEME PRESENT.
    '                If dtRow.Item("loanscheme").ToString.Trim.Length > 0 Then
    '                    intLoanSchemeUnkid = objLoanScheme.GetLoanSchemeUnkid(dtRow.Item("loanscheme").ToString.Trim)
    '                    If intLoanSchemeUnkid <= 0 Then
    '                        If objLoanScheme IsNot Nothing Then objLoanScheme = Nothing
    '                        objLoanScheme = New clsLoan_Scheme

    '                        objLoanScheme._Code = dtRow.Item("loanscheme").ToString.Trim
    '                        objLoanScheme._Isactive = True
    '                        objLoanScheme._Name = dtRow.Item("loanscheme").ToString.Trim
    '                        objLoanScheme._Minnetsalary = 0

    '                        If objLoanScheme.Insert Then
    '                            intLoanSchemeUnkid = objLoanScheme._Loanschemeunkid
    '                        Else
    '                            dtRow.Item("image") = imgError
    '                            dtRow.Item("message") = objLoanScheme._Code & "/" & objLoanScheme._Name & " : " & objLoanScheme._Message
    '                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                            dtRow.Item("objStatus") = 2
    '                            objError.Text = CStr(Val(objError.Text) + 1)
    '                            Continue For
    '                        End If
    '                    End If
    '                End If
    '            End If

    '            '------------------------------ INSERTING PROCESS PENDING LOAN.

    '            If objProcessPending IsNot Nothing Then objProcessPending = Nothing

    '            objProcessPending = New clsProcess_pending_loan

    '            If radLoan.Checked = True Then
    '                objProcessPending._Isloan = True
    '                objProcessPending._Loanschemeunkid = intLoanSchemeUnkid
    '            ElseIf radAdvance.Checked = True Then
    '                objProcessPending._Isloan = False
    '                objProcessPending._Loanschemeunkid = -1
    '            End If

    '            objProcessPending._Application_No = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & Now.ToString("hhmmss") & Now.Millisecond.ToString

    '            If IsDate(dtRow.Item("ladate")) Then
    '                objProcessPending._Application_Date = CDate(dtRow.Item("ladate")).AddDays(-1)
    '            Else
    '                dtRow.Item("image") = imgError
    '                dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Date.")
    '                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                dtRow.Item("objStatus") = 2
    '                objError.Text = CStr(Val(objError.Text) + 1)
    '                Continue For
    '            End If
    '            objProcessPending._Loan_Amount = CDec(dtRow.Item("la_amount"))
    '            objProcessPending._Approved_Amount = CDec(dtRow.Item("la_amount"))
    '            objProcessPending._Employeeunkid = intEmployeeId
    '            objProcessPending._Approverunkid = cboApprovedBy.SelectedValue
    '            objProcessPending._Loan_Statusunkid = 4
    '            objProcessPending._Isvoid = False
    '            objProcessPending._Voiddatetime = Nothing
    '            objProcessPending._Voiduserunkid = -1
    '            objProcessPending._Isexternal_Entity = True
    '            objProcessPending._External_Entity_Name = dtRow.Item("exentityname").ToString
    '            objProcessPending._Userunkid = User._Object._Userunkid

    '            'Nilay (10-Oct-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'If objProcessPending.Insert Then
    '            If objProcessPending.Insert(FinancialYear._Object._DatabaseName, _
    '                                        User._Object._Userunkid, _
    '                                        FinancialYear._Object._YearUnkid, _
    '                                        Company._Object._Companyunkid, _
    '                                        ConfigParameter._Object._LoanApplicationNoType, _
    '                                        ConfigParameter._Object._LoanApplicationPrifix) Then
    '                'Nilay (10-Oct-2015) -- End
    '                intProcessPendingUnkid = objProcessPending._Processpendingloanunkid
    '            Else
    '                dtRow.Item("image") = imgError
    '                dtRow.Item("message") = objProcessPending._Message
    '                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                dtRow.Item("objStatus") = 2
    '                objError.Text = CStr(Val(objError.Text) + 1)
    '                Continue For
    '            End If

    '            objLoan_Advance = New clsLoan_Advance

    '            If radLoan.Checked = True Then
    '                objLoan_Advance._Isloan = True
    '                objLoan_Advance._Advance_Amount = 0
    '                objLoan_Advance._Calctype_Id = 1
    '                objLoan_Advance._Interest_Rate = CDbl(dtRow.Item("interest"))
    '                objLoan_Advance._Loan_Amount = CDec(dtRow.Item("la_amount"))
    '                objLoan_Advance._Balance_Amount = CDec(dtRow.Item("balanceamount"))
    '                objLoan_Advance._Loan_Duration = dtRow.Item("loanduration")

    '                Dim mdecNetAmount As Decimal = 0 : Dim mdecInterest_Amount As Decimal = 0

    '                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Simple_Interest, CDec(dtRow.Item("la_amount")), dtRow.Item("loanduration"), CDbl(dtRow.Item("interest")), mdecNetAmount, mdecInterest_Amount)

    '                objLoan_Advance._Interest_Amount = mdecInterest_Amount
    '                objLoan_Advance._Net_Amount = mdecNetAmount


    '                If CInt(cboLoanSchedule.SelectedValue) > 0 Then
    '                    Select Case CInt(cboLoanSchedule.SelectedValue)
    '                        Case 1  'AMOUNTS
    '                            Dim mintTotalEMI As Integer = 0
    '                            objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanSchedule.SelectedValue), mdecNetAmount, CDec(dtRow.Item("schduleby")), mintTotalEMI)
    '                            objLoan_Advance._Emi_Amount = CDec(dtRow.Item("schduleby"))
    '                            objLoan_Advance._Emi_Tenure = mintTotalEMI

    '                            If mintTotalEMI > CInt(dtRow.Item("loanduration")) Then
    '                                dtRow.Item("image") = imgError
    '                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 27, "The Number of Installments cannot be greater than the loan duration.")
    '                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                                dtRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Continue For
    '                            End If

    '                        Case 2  'MONTHS
    '                            Dim mdecInstallmentAmount As Decimal = 0
    '                            objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanSchedule.SelectedValue), mdecNetAmount, mdecInstallmentAmount, CInt(dtRow.Item("schduleby")))
    '                            objLoan_Advance._Emi_Amount = mdecInstallmentAmount
    '                            objLoan_Advance._Emi_Tenure = CInt(dtRow.Item("schduleby"))

    '                            If CInt(dtRow.Item("schduleby")) > dtRow.Item("loanduration") Then
    '                                dtRow.Item("image") = imgError
    '                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 27, "The Number of Installments cannot be greater than the loan duration.")
    '                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                                dtRow.Item("objStatus") = 2
    '                                objError.Text = CStr(Val(objError.Text) + 1)
    '                                Continue For
    '                            End If

    '                    End Select
    '                End If

    '                objLoan_Advance._Loanscheduleunkid = cboLoanSchedule.SelectedValue
    '                objLoan_Advance._Loanschemeunkid = intLoanSchemeUnkid
    '                objLoan_Advance._Net_Amount = CDec(dtRow.Item("la_amount")) + mdecInterest_Amount
    '                'S.SANDEEP [ 18 JAN 2014 ] -- START
    '                If CDec(dtRow.Item("balanceamount")) <= 0 Then
    '                    objLoan_Advance._LoanBF_Amount = CDec(dtRow.Item("la_amount")) + mdecInterest_Amount
    '                Else
    '                    objLoan_Advance._LoanBF_Amount = CDec(dtRow.Item("balanceamount"))
    '                End If
    '                'S.SANDEEP [ 18 JAN 2014 ] -- END

    '            ElseIf radAdvance.Checked = True Then

    '                objLoan_Advance._Isloan = False
    '                objLoan_Advance._Advance_Amount = CDec(dtRow.Item("la_amount"))
    '                objLoan_Advance._Balance_Amount = CDec(dtRow.Item("la_amount"))
    '                objLoan_Advance._Calctype_Id = 0
    '                objLoan_Advance._Loanscheduleunkid = 0
    '                objLoan_Advance._Loanschemeunkid = 0
    '                objLoan_Advance._Net_Amount = 0
    '                objLoan_Advance._Loan_Duration = 1

    '            End If

    '            objLoan_Advance._Approverunkid = cboApprovedBy.SelectedValue
    '            objLoan_Advance._Employeeunkid = intEmployeeId
    '            objLoan_Advance._Isbrought_Forward = False

    '            If dtRow.Item("vocno").ToString.Trim.Length > 0 Then
    '                objLoan_Advance._Loanvoucher_No = dtRow.Item("vocno")
    '            Else
    '                objLoan_Advance._Loanvoucher_No = ""
    '            End If

    '            objLoan_Advance._LoanStatus = 1
    '            objLoan_Advance._Userunkid = User._Object._Userunkid
    '            objLoan_Advance._Processpendingloanunkid = intProcessPendingUnkid
    '            objLoan_Advance._Periodunkid = intPeriodUnkid
    '            objLoan_Advance._Deductionperiodunkid = intDeductionPeriodUnkid


    '            If IsDate(dtRow.Item("ladate")) Then
    '                objLoan_Advance._Effective_Date = CDate(dtRow.Item("ladate"))
    '            Else
    '                dtRow.Item("image") = imgError
    '                dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Date.")
    '                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                dtRow.Item("objStatus") = 2
    '                objError.Text = CStr(Val(objError.Text) + 1)
    '                Continue For
    '            End If

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            If objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid) Then
    '                'If objLoan_Advance.Insert() Then
    '                'S.SANDEEP [04 JUN 2015] -- END

    '                dtRow.Item("image") = imgAccept
    '                dtRow.Item("message") = ""
    '                dtRow.Item("status") = Language.getMessage(mstrModuleName, 29, "Success")
    '                dtRow.Item("objStatus") = 1
    '                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
    '            Else
    '                dtRow.Item("image") = imgError
    '                dtRow.Item("message") = objLoan_Advance._Message
    '                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
    '                dtRow.Item("objStatus") = 2
    '                objError.Text = CStr(Val(objError.Text) + 1)
    '            End If

    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    '#End Region

    '#Region " Form's Event(s) "

    'Private Sub frmImportLoan_AdvanceWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    Try
    '        Call Set_Logo(Me, gApplicationType)
    '        Language.setLanguage(Me.Name)
    '        Call OtherSettings()
    '        txtFilePath.BackColor = GUI.ColorComp
    '        radLoan.Checked = True
    '        Call FillCombo()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "frmImportLoan_AdvanceWizard_Load", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '    Dim objfrm As New frmLanguage
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        Call SetMessages()

    '        objfrm.displayDialog(Me)

    '        Call SetLanguage()

    '    Catch ex As System.Exception
    '        Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '    Finally
    '        objfrm.Dispose()
    '        objfrm = Nothing
    '    End Try
    'End Sub

    '#End Region

    '#Region " Button's Event(s) "

    'Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
    '    Try
    '        Dim objFileOpen As New OpenFileDialog
    '        objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx"|XML File(*.xml)|*.xml"

    '        If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            txtFilePath.Text = objFileOpen.FileName
    '        End If

    '        objFileOpen = Nothing

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub objbtnSearchLoanApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLoanApprover.Click
    '    Dim frm As New frmCommonSearch
    '    Try

    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If

    '        With frm
    '            .ValueMember = cboApprovedBy.ValueMember
    '            .DisplayMember = cboApprovedBy.DisplayMember
    '            .DataSource = cboApprovedBy.DataSource
    '            .CodeMember = "Code"
    '        End With

    '        If frm.DisplayDialog Then
    '            cboApprovedBy.SelectedValue = frm.SelectedValue
    '            cboApprovedBy.Focus()
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchLoanApprover_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub

    '#End Region

    '#Region " Control's Event(s) "

    'Private Sub radAdvance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAdvance.CheckedChanged
    '    Call DoPanelOperation(pnlLoanMapping, pnlAdvance)
    'End Sub

    'Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
    '    Call DoPanelOperation(pnlAdvance, pnlLoanMapping)
    'End Sub

    'Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cmb As ComboBox = CType(sender, ComboBox)

    '        If cmb.Text <> "" Then

    '            cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

    '            If radLoan.Checked = True Then
    '                For Each cr As Control In pnlLoanMapping.Controls
    '                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

    '                        If cr.Name = "cboLoanSchedule" Then Continue For

    '                        If cr.Name <> cmb.Name Then

    '                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '                                cmb.SelectedIndex = -1
    '                                cmb.Select()
    '                                Exit Sub
    '                            End If

    '                        End If

    '                    End If
    '                Next
    '            ElseIf radAdvance.Checked = True Then
    '                For Each cr As Control In pnlAdvance.Controls
    '                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

    '                        If cr.Name <> cmb.Name Then

    '                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '                                cmb.SelectedIndex = -1
    '                                cmb.Select()
    '                                Exit Sub
    '                            End If

    '                        End If

    '                    End If
    '                Next
    '            End If
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
    '    Try
    '        Dim savDialog As New SaveFileDialog
    '        dvGriddata.RowFilter = "objStatus = 2"
    '        Dim dtTable As DataTable = dvGriddata.ToTable
    '        If dtTable.Rows.Count > 0 Then
    '            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx""
    '            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
    '                dtTable.Columns.Remove("image")
    '                dtTable.Columns.Remove("objstatus")
    '                If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Dependant Wizard") = True Then
    '                    Process.Start(savDialog.FileName)
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
    '    Try
    '        dvGriddata.RowFilter = "objStatus = 1"
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
    '    Try
    '        dvGriddata.RowFilter = "objStatus = 2"
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
    '    Try
    '        dvGriddata.RowFilter = "objStatus = 0"
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
    '    Try
    '        dvGriddata.RowFilter = ""
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
    '    End Try
    'End Sub

    '#End Region

    '#Region " eZee Wizard "

    'Private Sub eZeeWizImportLoan_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportLoan.AfterSwitchPages
    '    Try
    '        Select Case e.NewIndex
    '            Case eZeeWizImportLoan.Pages.IndexOf(WizPageImporting)
    '                Call CreateDataTable()
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "eZeeWizImportLoan_AfterSwitchPages", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub eZeeWizImportLoan_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportLoan.BeforeSwitchPages
    '    Try
    '        Select Case e.OldIndex
    '            Case eZeeWizImportLoan.Pages.IndexOf(WizPageSelectFile)
    '                If Not System.IO.File.Exists(txtFilePath.Text) Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '                    e.Cancel = True
    '                    Exit Sub
    '                End If

    '                Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

    '                If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
    '                    mds_ImportData = New DataSet
    '                    Using iCSV As New clsCSVData
    '                        iCSV.LoadCSV(txtFilePath.Text, True)
    '                        mds_ImportData = iCSV.CSVDataSet.Copy
    '                    End Using
    '                ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
    '                    Dim iExcelData As New ExcelData
    '                    Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
    '                    mds_ImportData = iExcelData.Import(txtFilePath.Text)
    '                ElseIf ImportFile.Extension.ToLower = ".xml" Then
    '                    mds_ImportData = New DataSet
    '                    mds_ImportData.ReadXml(txtFilePath.Text)
    '                Else
    '                    e.Cancel = True
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '                    Exit Sub
    '                End If

    '            Case eZeeWizImportLoan.Pages.IndexOf(WizPageApprover)
    '                If e.NewIndex > e.OldIndex Then
    '                    If cboApprovedBy.SelectedValue <= 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Approver is mandatory information. Please select approver to import the loan/advace data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '                        cboApprovedBy.Focus()
    '                        e.Cancel = True
    '                        Exit Sub
    '                    End If
    '                End If

    '                Call SetDataCombo()

    '            Case eZeeWizImportLoan.Pages.IndexOf(WizPageMapping)
    '                If e.NewIndex > e.OldIndex Then
    '                    If IsValid() = False Then
    '                        e.Cancel = True
    '                        Exit Sub
    '                    End If
    '                End If
    '            Case eZeeWizImportLoan.Pages.IndexOf(WizPageImporting)
    '                Me.Close()

    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "eZeeWizImportLoan_BeforeSwitchPages", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    '#End Region


#End Region
'Shani(26-Nov-2015) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeWizImportLoan.CancelText = Language._Object.getCaption(Me.eZeeWizImportLoan.Name & "_CancelText" , Me.eZeeWizImportLoan.CancelText)
			Me.eZeeWizImportLoan.NextText = Language._Object.getCaption(Me.eZeeWizImportLoan.Name & "_NextText" , Me.eZeeWizImportLoan.NextText)
			Me.eZeeWizImportLoan.BackText = Language._Object.getCaption(Me.eZeeWizImportLoan.Name & "_BackText" , Me.eZeeWizImportLoan.BackText)
			Me.eZeeWizImportLoan.FinishText = Language._Object.getCaption(Me.eZeeWizImportLoan.Name & "_FinishText" , Me.eZeeWizImportLoan.FinishText)
			Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblLoanInterestRate.Text = Language._Object.getCaption(Me.lblLoanInterestRate.Name, Me.lblLoanInterestRate.Text)
			Me.lblLoanAmount.Text = Language._Object.getCaption(Me.lblLoanAmount.Name, Me.lblLoanAmount.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblLoanDeductionPeriod.Text = Language._Object.getCaption(Me.lblLoanDeductionPeriod.Name, Me.lblLoanDeductionPeriod.Text)
			Me.lblLoanAssignedPeriod.Text = Language._Object.getCaption(Me.lblLoanAssignedPeriod.Name, Me.lblLoanAssignedPeriod.Text)
			Me.lblLoanDate.Text = Language._Object.getCaption(Me.lblLoanDate.Name, Me.lblLoanDate.Text)
			Me.lblLoanVocNo.Text = Language._Object.getCaption(Me.lblLoanVocNo.Name, Me.lblLoanVocNo.Text)
			Me.lblLoanFirstname.Text = Language._Object.getCaption(Me.lblLoanFirstname.Name, Me.lblLoanFirstname.Text)
			Me.lblLoanEmpCode.Text = Language._Object.getCaption(Me.lblLoanEmpCode.Name, Me.lblLoanEmpCode.Text)
			Me.lblLoanSurname.Text = Language._Object.getCaption(Me.lblLoanSurname.Name, Me.lblLoanSurname.Text)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
			Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
			Me.lblImportation.Text = Language._Object.getCaption(Me.lblImportation.Name, Me.lblImportation.Text)
			Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.Name, Me.radAdvance.Text)
			Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.Name, Me.radLoan.Text)
			Me.elImportationType.Text = Language._Object.getCaption(Me.elImportationType.Name, Me.elImportationType.Text)
			Me.lvlLoanApproverName.Text = Language._Object.getCaption(Me.lvlLoanApproverName.Name, Me.lvlLoanApproverName.Text)
			Me.lblLoanApproverName.Text = Language._Object.getCaption(Me.lblLoanApproverName.Name, Me.lblLoanApproverName.Text)
			Me.lblLoanApproverCode.Text = Language._Object.getCaption(Me.lblLoanApproverCode.Name, Me.lblLoanApproverCode.Text)
			Me.lblLoanInstallmentAmt.Text = Language._Object.getCaption(Me.lblLoanInstallmentAmt.Name, Me.lblLoanInstallmentAmt.Text)
			Me.lblLoanCurrncy.Text = Language._Object.getCaption(Me.lblLoanCurrncy.Name, Me.lblLoanCurrncy.Text)
			Me.lblLoanCalType.Text = Language._Object.getCaption(Me.lblLoanCalType.Name, Me.lblLoanCalType.Text)
			Me.lblExternalEntity.Text = Language._Object.getCaption(Me.lblExternalEntity.Name, Me.lblExternalEntity.Text)
			Me.lblDurationMonth.Text = Language._Object.getCaption(Me.lblDurationMonth.Name, Me.lblDurationMonth.Text)
			Me.lblAppNo.Text = Language._Object.getCaption(Me.lblAppNo.Name, Me.lblAppNo.Text)
			Me.Label11.Text = Language._Object.getCaption(Me.Label11.Name, Me.Label11.Text)
			Me.lblAdvanceAmount.Text = Language._Object.getCaption(Me.lblAdvanceAmount.Name, Me.lblAdvanceAmount.Text)
			Me.lnkGenerateFile.Text = Language._Object.getCaption(Me.lnkGenerateFile.Name, Me.lnkGenerateFile.Text)
			Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.Name, Me.lblInterestCalcType.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
			Me.lblLoanPrincipalAmt.Text = Language._Object.getCaption(Me.lblLoanPrincipalAmt.Name, Me.lblLoanPrincipalAmt.Text)
			Me.lblMappedHead.Text = Language._Object.getCaption(Me.lblMappedHead.Name, Me.lblMappedHead.Text)
			Me.lblLoanAccNumber.Text = Language._Object.getCaption(Me.lblLoanAccNumber.Name, Me.lblLoanAccNumber.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 2, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 3, "Loan Application No. cannot be blank. Please set Loan Application No. in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 4, "Loan Date cannot be blank. Please set Loan Date in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 5, "Loan Assignment Period cannot be blank. Please set Loan Assignment Period in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 6, "Loan Deduction Period cannot be blank. Please set Loan Deduction Period in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 7, "Approver Code cannot be blank. Please set Approver Code in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 8, "Approver Name cannot be blank. Please set Approver Name in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 9, "Approver Level cannot be blank. Please set Approver Level in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 10, "Loan Scheme cannot be blank. Please set Loan Scheme in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 11, "Loan Amount cannot be blank. Please set Loan Amount in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 12, "Loan Interest Rate cannot be blank. Please set Loan Interest Rate in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 13, "Loan Duration cannot be blank. Please set Loan Duration in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 15, "Loan Installment Amount cannot be blank. Please set Loan Installment Amount in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 16, "Advance Amount cannot be blank. Please set Advance Amount in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 17, "External Entity cannot be blank. Please set External Entity in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 18, "Currncy Value cannot be blank. Please set Currncy Value in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 19, ",Assignment period not found.")
			Language.setMessage(mstrModuleName, 20, "Loan Calculate Type cannot be blank. Please select Loan Calculate Type in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 21, "Employee Not Found.")
			Language.setMessage(mstrModuleName, 22, "Invalid Date.")
			Language.setMessage(mstrModuleName, 24, ", Deduction period not found.")
			Language.setMessage(mstrModuleName, 25, "Invalid No Of Installment")
			Language.setMessage(mstrModuleName, 26, "Invalid Installment Amount")
			Language.setMessage(mstrModuleName, 27, "Invalid Loan/Advance Amount")
			Language.setMessage(mstrModuleName, 28, "calculation type cannot have rate.")
			Language.setMessage(mstrModuleName, 29, "Cannot allow Interest Rate to blank")
			Language.setMessage(mstrModuleName, 30, "Approver Not Found.")
			Language.setMessage(mstrModuleName, 31, "This Employee is not assigned to given approver.")
			Language.setMessage(mstrModuleName, 32, "This Loan is alrady Assign to this employee.")
			Language.setMessage(mstrModuleName, 33, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 34, "Invalid Interest Rate")
			Language.setMessage(mstrModuleName, 35, "Deduction period cannot be less then the Loan Period.")
			Language.setMessage(mstrModuleName, 37, "Fail")
			Language.setMessage(mstrModuleName, 38, "Success")
			Language.setMessage(mstrModuleName, 39, "Please select proper file to Import Data from.")
			Language.setMessage(mstrModuleName, 40, "Please select Importaion Type.")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 43, "No Interest")
			Language.setMessage(mstrModuleName, 44, "Note : If Loan is already cacluated with interest in the file you are trying to import. Please make sure that you import with")
			Language.setMessage(mstrModuleName, 45, "as calulation type.")
			Language.setMessage(mstrModuleName, 46, "Template Exported Successfully.")
			Language.setMessage(mstrModuleName, 47, "This Approver is Not Final Approver.")
			Language.setMessage(mstrModuleName, 48, "Interest Calculate Type cannot be blank. Please select Interest Calculate Type in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
			Language.setMessage(mstrModuleName, 51, "Note : If you are importing Loan Data with External Approver then Please specify User Name in Approver Code column.")
			Language.setMessage(mstrModuleName, 52, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee.")
			Language.setMessage(mstrModuleName, 53, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the")
			Language.setMessage(mstrModuleName, 54, "of previous net pay.")
			Language.setMessage(mstrModuleName, 55, "Sorry, you can not apply for this loan scheme: Reason, Installment months cannot be greater than")
			Language.setMessage(mstrModuleName, 56, " for")
			Language.setMessage(mstrModuleName, 57, " Scheme.")
			Language.setMessage(mstrModuleName, 58, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee for the net pay head set on loan scheme master screen.")
			Language.setMessage(mstrModuleName, 59, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the")
			Language.setMessage(mstrModuleName, 60, "of previous net pay head")
			Language.setMessage(mstrModuleName, 61, "Sorry, you can not apply for this Advance: Reason, There is no previous salary history for this employee.")
			Language.setMessage(mstrModuleName, 62, "Sorry, you can not apply for this Advance: Reason, There is no previous salary history for this employee for the net pay head set on loan scheme master screen.")
			Language.setMessage(mstrModuleName, 63, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the")
			Language.setMessage(mstrModuleName, 64, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the")
            Language.setMessage(mstrModuleName, 65, "Sorry, you can not apply for Advance: Reason, Days to apply for Advance has been exceeded.")
			Language.setMessage(mstrModuleName, 66, "Please select Mapped Head to continue.")
			Language.setMessage(mstrModuleName, 67, "Type to Search")
			Language.setMessage(mstrModuleName, 68, "Loan Account No cannot be blank. Please set Loan Account No in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 69, "Sorry, Loan tenure should not be greater than employee tenure month")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

