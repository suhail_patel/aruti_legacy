﻿Option Strict Off

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmLoanApplicationList

#Region " Private Varaibles "
    Private objLoanAdvanceunkid As clsLoan_Advance
    Private objProcesspendingloan As clsProcess_pending_loan
    Private ReadOnly mstrModuleName As String = "frmLoanApplicationList"
    Private mintSeletedValue As Integer = 1
    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    Private mstrAdvanceFilter As String = String.Empty
    'Nilay (20-Sept-2016) -- End

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private mstrSearchText As String = String.Empty
    'Nilay (27-Oct-2016) -- End

#End Region

#Region " Private Function "

    Private Sub FillList()
        Dim dsProcessPendingLoan As New DataSet
        Dim dtTable As New DataTable
        Dim StrSearching As String = String.Empty
        Dim objExchangeRate As New clsExchangeRate
        Try

            If User._Object.Privilege._AllowToViewProcessLoanAdvanceList = True Then

                If txtApplicationNo.Text.Trim <> "" Then
                    StrSearching &= "AND lnloan_process_pending_loan.Application_No like '" & txtApplicationNo.Text.Trim & "%' "
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboLoanScheme.SelectedValue) > 0 Then
                    'Nilay (08-Dec-2015) -- Start
                    'StrSearching &= "AND lnloan_process_pending_loanloanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
                    StrSearching &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
                    'Nilay (08-Dec-2015) -- End
                End If

                If dtpAppDate.Checked Then
                    'Nilay (15-Dec-2015) -- Start
                    'StrSearching &= "AND (lnloan_process_pending_loan.application_date) ='" & eZeeDate.convertDate(dtpAppDate.Value) & "' "
                    StrSearching &= "AND CONVERT(CHAR(8),lnloan_process_pending_loan.application_date,112)='" & eZeeDate.convertDate(dtpAppDate.Value) & "' "
                    'Nilay (15-Dec-2015) -- End
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    StrSearching &= "AND lnloan_process_pending_loan.loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                End If

                If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                    StrSearching &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = " & CInt(cboLoanAdvance.SelectedIndex) & " "
                End If

                If txtAmount.Text.Trim <> "" AndAlso txtAmount.Decimal > 0 Then
                    StrSearching &= "AND lnloan_process_pending_loan.loan_amount  " & cboAmountCondition.Text & " " & txtAmount.Decimal & " "
                End If
                'Nilay (28-Aug-2015) -- AND lnloan_process_pending_loan.Amount

                If txtApprovedAmount.Text.Trim <> "" AndAlso txtApprovedAmount.Decimal > 0 Then
                    StrSearching &= "AND lnloan_process_pending_loan.approved_amount  " & cboAppAmountCondition.Text & " " & txtApprovedAmount.Decimal & " "
                End If
                'Nilay (28-Aug-2015) -- AND lnloan_process_pending_loan.Approved_Amount

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                If mstrAdvanceFilter.Trim.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If
                'Nilay (20-Sept-2016) -- End

                If StrSearching.Trim.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If

                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                dsProcessPendingLoan = objProcesspendingloan.GetList(FinancialYear._Object._DatabaseName, _
                                                                     User._Object._Userunkid, _
                                                                     FinancialYear._Object._YearUnkid, _
                                                                     Company._Object._Companyunkid, _
                                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                                     True, _
                                                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                     "Loan", , StrSearching)
                'Nilay (10-Oct-2015) -- End

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'dtTable = New DataView(dsProcessPendingLoan.Tables("Loan"), "", "Application_No desc", DataViewRowState.CurrentRows).ToTable
                dtTable = New DataView(dsProcessPendingLoan.Tables("Loan"), "", "processpendingloanunkid desc", DataViewRowState.CurrentRows).ToTable
                'Nilay (20-Sept-2016) -- End

                dgvLoanApplicationList.AutoGenerateColumns = False
                dgcolhApplicationNo.DataPropertyName = "Application_No"
                dgcolhAppDate.DataPropertyName = "applicationdate"
                dgcolhAppDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern

                dgcolhEmployee.DataPropertyName = "EmpName"
                dgcolhLoanScheme.DataPropertyName = "LoanScheme"
                dgcolhLoan_Advance.DataPropertyName = "Loan_Advance"

                dgcolhAmount.DataPropertyName = "Amount"
                dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency

                dgcolhApp_Amount.DataPropertyName = "Approved_Amount"
                dgcolhApp_Amount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhApp_Amount.DefaultCellStyle.Format = GUI.fmtCurrency

                dgcolhStatus.DataPropertyName = "LoanStatus"
                objdgcolhIsLoan.DataPropertyName = "isloan"
                objdgcolhStatusunkid.DataPropertyName = "loan_statusunkid"
                objdgcolhEmpId.DataPropertyName = "employeeunkid"
                objdgcolhIsExternal.DataPropertyName = "isexternal_entity"
                objdgcolhApprover.DataPropertyName = "approverunkid"
                objdgcolhSchemeID.DataPropertyName = "loanschemeunkid"
                objdgcolhprocesspendingloanunkid.DataPropertyName = "processpendingloanunkid"
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                dgcolhCancelRemark.DataPropertyName = "remark"
                'Nilay (20-Sept-2016) -- End

                dgvLoanApplicationList.DataSource = dtTable

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.CANCELLED Then
                    dgcolhCancelRemark.Visible = True
                Else
                    dgcolhCancelRemark.Visible = False
                End If
                'Nilay (20-Sept-2016) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
        Finally
            objbtnSearch.ShowResult(CStr(dgvLoanApplicationList.RowCount))
            'Nilay (20-Sept-2016) -- End
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objMaster As New clsMasterData
        Try
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True)
            'End If
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, _
                                                   ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                   "Employee", True)
            'Nilay (10-Oct-2015) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Nilay (27-Oct-2016) -- End

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            dsCombos = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboLoanScheme)
            'Nilay (27-Oct-2016) -- End

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'dsCombos = objProcesspendingloan.GetLoan_Status("Status", , True)
            dsCombos = objProcesspendingloan.GetLoan_Status("Status", , True, True)
            'Nilay (20-Sept-2016) -- End
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Status")
                .SelectedValue = 1
            End With

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 2, "Loan"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 3, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsCombos = objMaster.GetCondition(False, True)
            dsCombos = objMaster.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsCombos.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            cboAmountCondition.ValueMember = "id"
            cboAmountCondition.DisplayMember = "Name"
            cboAmountCondition.DataSource = dtCondition
            cboAmountCondition.SelectedIndex = 0

            cboAppAmountCondition.ValueMember = "id"
            cboAppAmountCondition.DisplayMember = "Name"
            cboAppAmountCondition.DataSource = dtCondition.Copy
            cboAppAmountCondition.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Private Sub Generate_MenuItems()
    '    Dim dsList As New DataSet
    '    Dim objMaster As New clsMasterData
    '    Try
    '        dsList = objProcesspendingloan.GetLoan_Status("Status", False, False)
    '        cmnuStatus.Items.Clear()

    '        For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '            Dim objbtnpopupFCStatus As New ToolStripMenuItem
    '            objbtnpopupFCStatus.Name = "objbtnpopupFCStatus" & i
    '            objbtnpopupFCStatus.Text = dsList.Tables(0).Rows(i)("Name").ToString
    '            objbtnpopupFCStatus.Tag = dsList.Tables(0).Rows(i)("Id")
    '            AddHandler objbtnpopupFCStatus.Click, AddressOf FCStatus_Click
    '            cmnuStatus.Items.Add(objbtnpopupFCStatus)
    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub FCStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmLoanApplication_AddEdit
    '    Try

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FCStatus_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddPendingLoan
            btnEdit.Enabled = User._Object.Privilege._EditPendingLoan
            btnDelete.Enabled = User._Object.Privilege._DeletePendingLoan
            mnuAssign.Enabled = User._Object.Privilege._AllowAssignPendingLoan
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            mnuGlobalCancelApproved.Enabled = User._Object.Privilege._AllowToCancelApprovedLoanApp
            'Nilay (20-Sept-2016) -- End


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in payroll module.
            mnuGlobalApprove.Enabled = User._Object.Privilege._AllowToProcessGlobalLoanApprove
            mnuGlobalAssign.Enabled = User._Object.Privilege._AllowToProcessGlobalLoanAssign
            'Varsha Rana (17-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 7, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End


#End Region

#Region " Form's Events "

    Private Sub frmLoanApplicationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objProcesspendingloan = New clsProcess_pending_loan
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            'Call Generate_MenuItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApplicationList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanApplicationList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmLoanApplicationList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objProcesspendingloan = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsProcess_pending_loan"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchTrnHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrnHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboLoanAdvance.SelectedIndex = 0
            cboLoanScheme.SelectedValue = 0
            'Nilay (16-Nov-2016) -- Start
            Call SetDefaultSearchText(cboEmployee)
            Call SetDefaultSearchText(cboLoanScheme)
            lblEmployee.Focus()
            'Nilay (16-Nov-2016) -- End

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'cboStatus.SelectedValue = 1
            cboStatus.SelectedValue = enLoanApplicationStatus.PENDING
            mstrAdvanceFilter = ""
            'Nilay (20-Sept-2016) -- End
            txtAmount.Text = ""
            txtApprovedAmount.Text = ""
            txtApplicationNo.Text = ""
            dtpAppDate.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmLoanApplication_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                'Hemant (30 Aug 2019) -- Start
                'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                'Call FillList()
                If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = False Then
                Call FillList()
                Else
                    dgvLoanApplicationList.DataSource = Nothing
                End If
                'Hemant (30 Aug 2019) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmLoanApplication_AddEdit
        Try

            If dgvLoanApplicationList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Select Loan Application from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvLoanApplicationList.Select()
                Exit Sub
            End If

            'Sohail (14 Jul 2018) -- Start
            'Issue : Audit Trail Issues in Loan module in 72.1.
            If CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) <> CInt(enLoanApplicationStatus.PENDING) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "You cannot Edit this loan application. Reason: This loan application status is") & " " & dgvLoanApplicationList.SelectedRows(0).Cells(dgcolhStatus.Index).Value.ToString(), enMsgBoxStyle.Information) '?1
                dgvLoanApplicationList.Select()
                Exit Sub
            End If

            If CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) = CInt(enLoanApplicationStatus.PENDING) Then
                Dim objApprovalProcessTran As New clsloanapproval_process_Tran
                If CBool(objApprovalProcessTran.IsPendingLoanApplication(CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value))) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "You cannot Edit this loan application. Reason: It is already in approval process."), enMsgBoxStyle.Information) '?1
                    dgvLoanApplicationList.Select()
                    Exit Sub
                End If
            End If
            'Sohail (14 Jul 2018) -- End

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

            frm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            'Select Case objProcesspendingloan._Loan_Statusunkid
            '    Case 2, 3
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance status is Approved or Rejected."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    Case 4
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance status is Assigned."), enMsgBoxStyle.Information)
            '        Exit Sub
            'End Select

            'If objProcesspendingloan._Application_Date.Date < FinancialYear._Object._Database_Start_Date Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot Delete Loan/Advance Application. Reason : Loan/Advance has been brought forwarded."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'Sohail (14 Jul 2018) -- Start
            'Issue : Audit Trail Issues in Loan module in 72.1.
            If CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) <> CInt(enLoanApplicationStatus.PENDING) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "You cannot Delete this loan application. Reason: This loan application status is") & " " & dgvLoanApplicationList.SelectedRows(0).Cells(dgcolhStatus.Index).Value.ToString(), enMsgBoxStyle.Information) '?1
                dgvLoanApplicationList.Select()
                Exit Sub
            End If

            If CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) = CInt(enLoanApplicationStatus.PENDING) Then
                Dim objApprovalProcessTran As New clsloanapproval_process_Tran
                If CBool(objApprovalProcessTran.IsPendingLoanApplication(CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value))) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "You cannot Delete this loan application. Reason: It is already in approval process."), enMsgBoxStyle.Information) '?1
                    dgvLoanApplicationList.Select()
                    Exit Sub
                End If
            End If
            'Sohail (14 Jul 2018) -- End

            If dgvLoanApplicationList.SelectedRows.Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Loan/Advance Application?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                    Me.Cursor = Cursors.WaitCursor
                    'Shani (21-Jul-2016) -- End


                    Dim frm As New frmReasonSelection
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.LOAN, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objProcesspendingloan._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing
                    objProcesspendingloan._Isvoid = True
                    objProcesspendingloan._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objProcesspendingloan._Voiduserunkid = User._Object._Userunkid
                    If objProcesspendingloan.Delete(CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value)) = False Then
                        eZeeMsgBox.Show(objProcesspendingloan._Message, enMsgBoxStyle.Information)
                    Else
                        'Shani (21-Jul-2016) -- Start
                        'Enhancement - Create New Loan Notification 

                        'Nilay (10-Dec-2016) -- Start
                        'Issue #26: Setting to be included on configuration for Loan flow Approval process
                        If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then
                            If CBool(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) Then
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                '                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhSchemeID.Index).Value), _
                                '                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhEmpId.Index).Value), -1, _
                                '                                             clsProcess_pending_loan.enApproverEmailType.Loan_Approver, True, _
                                '                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), _
                                '                                             ConfigParameter._Object._ArutiSelfServiceURL, True, enLogin_Mode.DESKTOP, 0, 0)
                                objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhSchemeID.Index).Value), _
                                                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhEmpId.Index).Value), -1, _
                                                                             clsProcess_pending_loan.enApproverEmailType.Loan_Approver, True, _
                                                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), _
                                                                             ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0)
                                'Sohail (30 Nov 2017) -- End
                            Else
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                '                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhSchemeID.Index).Value), _
                                '                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhEmpId.Index).Value), -1, _
                                '                                             clsProcess_pending_loan.enApproverEmailType.Loan_Advance, True, _
                                '                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), _
                                '                                             ConfigParameter._Object._ArutiSelfServiceURL, True, enLogin_Mode.DESKTOP, 0, 0)
                                objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhSchemeID.Index).Value), _
                                                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhEmpId.Index).Value), -1, _
                                                                             clsProcess_pending_loan.enApproverEmailType.Loan_Advance, True, _
                                                                             CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), _
                                                                             ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0)
                                'Sohail (30 Nov 2017) -- End
                            End If
                        End If
                        'Nilay (10-Dec-2016) -- End
                        'Shani (21-Jul-2016) -- End
                        FillList()
                    End If
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.Default
            'Shani (21-Jul-2016) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssign.Click
        Dim frm As New frmNewLoanAdvance_AddEdit
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If dgvLoanApplicationList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select loan/advance application from the list to perform further operation."), enMsgBoxStyle.Information)
                dgvLoanApplicationList.Select()
                Exit Sub
            End If

            Dim mintPendingApprovalTranID As Integer = -1
            Dim objApprovalTran As New clsloanapproval_process_Tran
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objApprovalTran.GetApprovalTranList(CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhEmpId.Index).Value), _
            '                                                            CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), _
            '                                                            "lnloanapproval_process_tran.statusunkid = 2")

            Dim dsList As DataSet = objApprovalTran.GetApprovalTranList(FinancialYear._Object._DatabaseName, _
                                                                        User._Object._Userunkid, _
                                                                        FinancialYear._Object._YearUnkid, _
                                                                        Company._Object._Companyunkid, _
                                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                        ConfigParameter._Object._UserAccessModeSetting, _
                                                                        True, _
                                                                        ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                        "List", _
                                                                        CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhEmpId.Index).Value), _
                                                                        CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), _
                                                                        "lnloanapproval_process_tran.statusunkid = 2")
            'Nilay (10-Oct-2015) -- End


            Dim dtList As DataTable = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dtList = New DataView(dsList.Tables(0), "", "priority desc", DataViewRowState.CurrentRows).ToTable
                If dtList.Rows.Count > 0 Then
                    mintPendingApprovalTranID = CInt(dtList.Rows(0)("pendingloantranunkid"))
                End If
            End If
            dtList = Nothing
            dsList = Nothing
            objApprovalTran = Nothing

            Dim objAssignLoanAdvance As New frmNewLoanAdvance_AddEdit
            objAssignLoanAdvance._PendingApprovalTranID = mintPendingApprovalTranID
            objAssignLoanAdvance._Employeeid = CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhEmpId.Index).Value)
            objAssignLoanAdvance.displayDialog(-1, enAction.ADD_ONE, -1)
            FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

#Region " Combobox Event "

    Private Sub cboStatus_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectionChangeCommitted
        Try
            mintSeletedValue = CInt(cboStatus.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboEmployee)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    .CodeMember = "Code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboLoanScheme.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboLoanScheme.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            If CInt(cboLoanScheme.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.GotFocus
        Try
            With cboLoanScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.Leave
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_Leave", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End

#End Region

#Region "DataGrid Event"

    Private Sub dgvLoanApplicationList_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLoanApplicationList.SelectionChanged
        Try
            If dgvLoanApplicationList.SelectedRows.Count > 0 Then

                If dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value = 1 Then 'Pending
                    'Nilay (05-May-2016) -- Start
                    'btnEdit.Enabled = True
                    'btnDelete.Enabled = True
                    'mnuAssign.Enabled = False

                    'Varsha Rana (16-Oct-2017) -- Start
                    'Issue - 70.01 - Loan/advance application assign screen will be open when application is in pending status.
                    'Dim blnFlag As Boolean
                    'Dim objApprovalProcessTran As New clsloanapproval_process_Tran
                    'blnFlag = objApprovalProcessTran.IsPendingLoanApplication(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value)
                    Dim blnFlag As Boolean = True
                    If CInt(dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) = enLoanApplicationStatus.APPROVED Then
                        blnFlag = False
                    End If

                    'Varsha Rana (16-Oct-2017) -- End

                    btnEdit.Enabled = blnFlag
                    btnDelete.Enabled = blnFlag
                    mnuAssign.Enabled = Not blnFlag
                    'Nilay (05-May-2016) -- End
                Else
                    If dgvLoanApplicationList.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value = 2 Then 'Approved
                        mnuAssign.Enabled = True
                    Else
                        mnuAssign.Enabled = False
                    End If
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanApplicationList_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Menu Events"

    Private Sub mnuGlobalAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalAssign.Click
        Try
            Dim frm As New frmGlobalAssignLoanAdvance
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub


    Private Sub mnuGlobalApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalApprove.Click
        Try
            Dim frm As New frmGlobalApproveLoan

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim objApproveLoan As New frmGlobalApproveLoan
            objApproveLoan.displayDialog(enAction.ADD_ONE)
            FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalApprove_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    Private Sub mnuGlobalCancelApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuGlobalCancelApproved.Click
        Try
            Dim frm As New frmGlobalCancelApprovedApplication

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(enAction.ADD_ONE)
            FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalCancelApproved_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (20-Sept-2016) -- End

#End Region


    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
#Region " Link Button's Event "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                mstrAdvanceFilter = frm._GetFilterString
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region
    'Nilay (20-Sept-2016) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblApprovedAmount.Text = Language._Object.getCaption(Me.lblApprovedAmount.Name, Me.lblApprovedAmount.Text)
            Me.lblAppNo.Text = Language._Object.getCaption(Me.lblAppNo.Name, Me.lblAppNo.Text)
            Me.lblAppDate.Text = Language._Object.getCaption(Me.lblAppDate.Name, Me.lblAppDate.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuAssign.Text = Language._Object.getCaption(Me.mnuAssign.Name, Me.mnuAssign.Text)
            Me.mnuGlobalApprove.Text = Language._Object.getCaption(Me.mnuGlobalApprove.Name, Me.mnuGlobalApprove.Text)
            Me.mnuGlobalAssign.Text = Language._Object.getCaption(Me.mnuGlobalAssign.Name, Me.mnuGlobalAssign.Text)
            Me.mnuGlobalCancelApproved.Text = Language._Object.getCaption(Me.mnuGlobalCancelApproved.Name, Me.mnuGlobalCancelApproved.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.dgcolhApplicationNo.HeaderText = Language._Object.getCaption(Me.dgcolhApplicationNo.Name, Me.dgcolhApplicationNo.HeaderText)
            Me.dgcolhAppDate.HeaderText = Language._Object.getCaption(Me.dgcolhAppDate.Name, Me.dgcolhAppDate.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhLoanScheme.HeaderText = Language._Object.getCaption(Me.dgcolhLoanScheme.Name, Me.dgcolhLoanScheme.HeaderText)
            Me.dgcolhLoan_Advance.HeaderText = Language._Object.getCaption(Me.dgcolhLoan_Advance.Name, Me.dgcolhLoan_Advance.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhApp_Amount.HeaderText = Language._Object.getCaption(Me.dgcolhApp_Amount.Name, Me.dgcolhApp_Amount.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhCancelRemark.HeaderText = Language._Object.getCaption(Me.dgcolhCancelRemark.Name, Me.dgcolhCancelRemark.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Loan")
            Language.setMessage(mstrModuleName, 3, "Advance")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Loan/Advance Application?")
            Language.setMessage(mstrModuleName, 5, "Please select loan/advance application from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 6, "Please Select Loan Application from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 7, "Type to Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class