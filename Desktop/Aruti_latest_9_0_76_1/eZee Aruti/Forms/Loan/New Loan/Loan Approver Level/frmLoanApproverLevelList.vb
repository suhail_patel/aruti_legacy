﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmLoanApproverLevelList

#Region "Private Variables"

    Private objlnAprroverLevel As clslnapproverlevel_master
    Private ReadOnly mstrModuleName As String = "frmLoanApproverLevelList"

#End Region

#Region "Form's Event"

    Private Sub frmLoanAprroverLevelList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objlnAprroverLevel = New clslnapproverlevel_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)

            Call OtherSttings()

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in Loan module.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End



            FillList()

            If lvLoanApproverLevelList.Items.Count > 0 Then
                lvLoanApproverLevelList.Items(0).Selected = True
                lvLoanApproverLevelList.Select()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAprroverLevelList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAprroverLevelList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAprroverLevelList_KeyUp", mstrModuleName)
        End Try

    End Sub

    Private Sub frmLoanAprroverLevelList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Close()
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clslnapproverlevel_master.SetMessages()

            objfrm._Other_ModuleNames = "clslnapproverlevel_master"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objLoanApproverLevel_AddEdit As New frmLoanApproverLevel_AddEdit

            If objLoanApproverLevel_AddEdit.DisplayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvLoanApproverLevelList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan Approver Level from the List to perform further Operation."), enMsgBoxStyle.Information)
            lvLoanApproverLevelList.Select()
            Exit Sub
        End If

        Dim objfrmLoanApproverLevel_AddEdit As New frmLoanApproverLevel_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLoanApproverLevelList.SelectedItems(0).Index
            If objfrmLoanApproverLevel_AddEdit.DisplayDialog(CInt(lvLoanApproverLevelList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            objfrmLoanApproverLevel_AddEdit = Nothing

            lvLoanApproverLevelList.Items(intSelectedIndex).Selected = True
            lvLoanApproverLevelList.EnsureVisible(intSelectedIndex)
            lvLoanApproverLevelList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvLoanApproverLevelList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan Approver Level from the List to perform further Operation."), enMsgBoxStyle.Information)
            lvLoanApproverLevelList.Select()
            Exit Sub
        End If
        If objlnAprroverLevel.isUsed(CInt(lvLoanApproverLevelList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Loan Approver Level. Reason: This Loan Approver Level is in use."), enMsgBoxStyle.Information)
            lvLoanApproverLevelList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLoanApproverLevelList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to Delete this Loan Approver Level?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objlnAprroverLevel.Delete(CInt(lvLoanApproverLevelList.SelectedItems(0).Tag))
                lvLoanApproverLevelList.SelectedItems(0).Remove()

                If lvLoanApproverLevelList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvLoanApproverLevelList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvLoanApproverLevelList.Items.Count - 1
                    lvLoanApproverLevelList.Items(intSelectedIndex).Selected = True
                    lvLoanApproverLevelList.EnsureVisible(intSelectedIndex)
                ElseIf lvLoanApproverLevelList.Items.Count <> 0 Then
                    lvLoanApproverLevelList.Items(intSelectedIndex).Selected = True
                    lvLoanApproverLevelList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvLoanApproverLevelList.Select()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillList()
        Dim dslnAprroverLevelList As New DataSet

        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in Loan module.
            If User._Object.Privilege._AllowToViewLoanApproverLevelList = True Then

                'Varsha Rana (17-Oct-2017) -- End

            dslnAprroverLevelList = objlnAprroverLevel.GetList("List")

            Dim lvItem As ListViewItem

            lvLoanApproverLevelList.Items.Clear()

            For Each drRow As DataRow In dslnAprroverLevelList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("lnlevelname").ToString
                lvItem.Tag = drRow("lnlevelunkid")
                lvItem.SubItems.Add(drRow("priority").ToString)
                lvLoanApproverLevelList.Items.Add(lvItem)
            Next

            End If 'Varsha Rana (17-Oct-2017) -- End
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dslnAprroverLevelList.Dispose()
        End Try
    End Sub


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges in loan module.
    Private Sub SetVisibility()
        Try

            btnNew.Enabled = User._Object.Privilege._AllowToAddLoanApproverLevel
            btnEdit.Enabled = User._Object.Privilege._AllowToEditLoanApproverLevel
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteLoanApproverLevel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End


#End Region

#Region "Language & UI Setting"
    Private Sub OtherSttings()

    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhLoanApproverLevelName.Text = Language._Object.getCaption(CStr(Me.colhLoanApproverLevelName.Tag), Me.colhLoanApproverLevelName.Text)
            Me.colhLoanApproverPriority.Text = Language._Object.getCaption(CStr(Me.colhLoanApproverPriority.Tag), Me.colhLoanApproverPriority.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Loan Approver Level from the List to perform further Operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Loan Approver Level. Reason: This Loan Approver Level is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to Delete this Loan Approver Level?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class