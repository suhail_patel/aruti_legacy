﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalCancelApprovedApplication
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalCancelApprovedApplication))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchScheme = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblTo = New System.Windows.Forms.Label
        Me.txtApplicationNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblApplicationNo = New System.Windows.Forms.Label
        Me.cboLoanAdvance = New System.Windows.Forms.ComboBox
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblApplicationDate = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.cboAmountCondition = New System.Windows.Forms.ComboBox
        Me.lblApprovedAmount = New System.Windows.Forms.Label
        Me.txtApprovedAmount = New eZee.TextBox.NumericTextBox
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.dgvLoanApproved = New System.Windows.Forms.DataGridView
        Me.objdgcolhchkSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhApplicationNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanScheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoan_Advance = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApp_Amount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsLoan = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSchemeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhprocesspendingloanunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.gbApprovedApplication = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.gbFilter.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLoanApproved, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbApprovedApplication.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 443)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(862, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(669, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(765, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbInfo
        '
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.txtRemarks)
        Me.gbInfo.Controls.Add(Me.lblRemarks)
        Me.gbInfo.Controls.Add(Me.cboStatus)
        Me.gbInfo.Controls.Add(Me.lblStatus)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(592, 1)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(269, 119)
        Me.gbInfo.TabIndex = 1
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtRemarks.Location = New System.Drawing.Point(75, 59)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemarks.Size = New System.Drawing.Size(184, 49)
        Me.txtRemarks.TabIndex = 1
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(6, 60)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(61, 15)
        Me.lblRemarks.TabIndex = 260
        Me.lblRemarks.Text = "Remarks"
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(75, 33)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(184, 21)
        Me.cboStatus.TabIndex = 0
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(6, 36)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(61, 15)
        Me.lblStatus.TabIndex = 7
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.objbtnSearchScheme)
        Me.gbFilter.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilter.Controls.Add(Me.dtpToDate)
        Me.gbFilter.Controls.Add(Me.lblTo)
        Me.gbFilter.Controls.Add(Me.txtApplicationNo)
        Me.gbFilter.Controls.Add(Me.lblApplicationNo)
        Me.gbFilter.Controls.Add(Me.cboLoanAdvance)
        Me.gbFilter.Controls.Add(Me.lblLoanAdvance)
        Me.gbFilter.Controls.Add(Me.dtpFromDate)
        Me.gbFilter.Controls.Add(Me.objbtnReset)
        Me.gbFilter.Controls.Add(Me.lblApplicationDate)
        Me.gbFilter.Controls.Add(Me.objbtnSearch)
        Me.gbFilter.Controls.Add(Me.lnkAllocation)
        Me.gbFilter.Controls.Add(Me.cboAmountCondition)
        Me.gbFilter.Controls.Add(Me.lblApprovedAmount)
        Me.gbFilter.Controls.Add(Me.txtApprovedAmount)
        Me.gbFilter.Controls.Add(Me.cboLoanScheme)
        Me.gbFilter.Controls.Add(Me.cboEmployee)
        Me.gbFilter.Controls.Add(Me.lblLoanScheme)
        Me.gbFilter.Controls.Add(Me.lblEmployee)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(0, 1)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(586, 119)
        Me.gbFilter.TabIndex = 0
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Criteria"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchScheme
        '
        Me.objbtnSearchScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchScheme.BorderSelected = False
        Me.objbtnSearchScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchScheme.Location = New System.Drawing.Point(288, 64)
        Me.objbtnSearchScheme.Name = "objbtnSearchScheme"
        Me.objbtnSearchScheme.Size = New System.Drawing.Size(21, 18)
        Me.objbtnSearchScheme.TabIndex = 247
        Me.objbtnSearchScheme.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(288, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 18)
        Me.objbtnSearchEmployee.TabIndex = 243
        Me.objbtnSearchEmployee.Visible = False
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(245, 87)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.ShowCheckBox = True
        Me.dtpToDate.Size = New System.Drawing.Size(94, 21)
        Me.dtpToDate.TabIndex = 4
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(199, 90)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(40, 15)
        Me.lblTo.TabIndex = 265
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtApplicationNo
        '
        Me.txtApplicationNo.Flags = 0
        Me.txtApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicationNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicationNo.Location = New System.Drawing.Point(443, 33)
        Me.txtApplicationNo.Name = "txtApplicationNo"
        Me.txtApplicationNo.Size = New System.Drawing.Size(134, 21)
        Me.txtApplicationNo.TabIndex = 5
        '
        'lblApplicationNo
        '
        Me.lblApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationNo.Location = New System.Drawing.Point(357, 36)
        Me.lblApplicationNo.Name = "lblApplicationNo"
        Me.lblApplicationNo.Size = New System.Drawing.Size(79, 15)
        Me.lblApplicationNo.TabIndex = 261
        Me.lblApplicationNo.Text = "Application No."
        Me.lblApplicationNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanAdvance
        '
        Me.cboLoanAdvance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAdvance.FormattingEnabled = True
        Me.cboLoanAdvance.Location = New System.Drawing.Point(443, 87)
        Me.cboLoanAdvance.Name = "cboLoanAdvance"
        Me.cboLoanAdvance.Size = New System.Drawing.Size(134, 21)
        Me.cboLoanAdvance.TabIndex = 8
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(357, 90)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(79, 15)
        Me.lblLoanAdvance.TabIndex = 259
        Me.lblLoanAdvance.Text = "Loan/Advance"
        Me.lblLoanAdvance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(100, 87)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(93, 21)
        Me.dtpFromDate.TabIndex = 3
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(559, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 256
        Me.objbtnReset.TabStop = False
        '
        'lblApplicationDate
        '
        Me.lblApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationDate.Location = New System.Drawing.Point(8, 90)
        Me.lblApplicationDate.Name = "lblApplicationDate"
        Me.lblApplicationDate.Size = New System.Drawing.Size(86, 15)
        Me.lblApplicationDate.TabIndex = 263
        Me.lblApplicationDate.Text = "Application Date"
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(536, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 255
        Me.objbtnSearch.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(436, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(94, 15)
        Me.lnkAllocation.TabIndex = 0
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboAmountCondition
        '
        Me.cboAmountCondition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAmountCondition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAmountCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAmountCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAmountCondition.FormattingEnabled = True
        Me.cboAmountCondition.Location = New System.Drawing.Point(533, 60)
        Me.cboAmountCondition.Name = "cboAmountCondition"
        Me.cboAmountCondition.Size = New System.Drawing.Size(44, 21)
        Me.cboAmountCondition.TabIndex = 7
        '
        'lblApprovedAmount
        '
        Me.lblApprovedAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedAmount.Location = New System.Drawing.Point(357, 64)
        Me.lblApprovedAmount.Name = "lblApprovedAmount"
        Me.lblApprovedAmount.Size = New System.Drawing.Size(79, 15)
        Me.lblApprovedAmount.TabIndex = 252
        Me.lblApprovedAmount.Text = "Approved Amt"
        Me.lblApprovedAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtApprovedAmount
        '
        Me.txtApprovedAmount.AllowNegative = True
        Me.txtApprovedAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtApprovedAmount.DigitsInGroup = 0
        Me.txtApprovedAmount.Flags = 0
        Me.txtApprovedAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprovedAmount.Location = New System.Drawing.Point(443, 60)
        Me.txtApprovedAmount.MaxDecimalPlaces = 6
        Me.txtApprovedAmount.MaxWholeDigits = 21
        Me.txtApprovedAmount.Name = "txtApprovedAmount"
        Me.txtApprovedAmount.Prefix = ""
        Me.txtApprovedAmount.RangeMax = 1.7976931348623157E+308
        Me.txtApprovedAmount.RangeMin = -1.7976931348623157E+308
        Me.txtApprovedAmount.Size = New System.Drawing.Size(84, 21)
        Me.txtApprovedAmount.TabIndex = 6
        Me.txtApprovedAmount.Text = "0"
        Me.txtApprovedAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownWidth = 300
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(100, 60)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(239, 21)
        Me.cboLoanScheme.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(100, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(239, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 63)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(86, 15)
        Me.lblLoanScheme.TabIndex = 245
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(86, 15)
        Me.lblEmployee.TabIndex = 242
        Me.lblEmployee.Text = "Employee"
        '
        'dgvLoanApproved
        '
        Me.dgvLoanApproved.AllowUserToAddRows = False
        Me.dgvLoanApproved.AllowUserToDeleteRows = False
        Me.dgvLoanApproved.AllowUserToResizeRows = False
        Me.dgvLoanApproved.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvLoanApproved.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvLoanApproved.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhchkSelect, Me.dgcolhApplicationNo, Me.dgcolhAppDate, Me.dgcolhEmployee, Me.dgcolhLoanScheme, Me.dgcolhLoan_Advance, Me.dgcolhAmount, Me.dgcolhApp_Amount, Me.dgcolhStatus, Me.objdgcolhIsLoan, Me.objdgcolhStatusunkid, Me.objdgcolhEmployeeunkid, Me.objdgcolhSchemeID, Me.objdgcolhprocesspendingloanunkid})
        Me.dgvLoanApproved.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLoanApproved.Location = New System.Drawing.Point(0, 0)
        Me.dgvLoanApproved.MultiSelect = False
        Me.dgvLoanApproved.Name = "dgvLoanApproved"
        Me.dgvLoanApproved.RowHeadersVisible = False
        Me.dgvLoanApproved.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLoanApproved.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanApproved.Size = New System.Drawing.Size(855, 283)
        Me.dgvLoanApproved.TabIndex = 9
        '
        'objdgcolhchkSelect
        '
        Me.objdgcolhchkSelect.Frozen = True
        Me.objdgcolhchkSelect.HeaderText = ""
        Me.objdgcolhchkSelect.Name = "objdgcolhchkSelect"
        Me.objdgcolhchkSelect.Width = 25
        '
        'dgcolhApplicationNo
        '
        Me.dgcolhApplicationNo.HeaderText = "Application No"
        Me.dgcolhApplicationNo.Name = "dgcolhApplicationNo"
        Me.dgcolhApplicationNo.ReadOnly = True
        '
        'dgcolhAppDate
        '
        Me.dgcolhAppDate.HeaderText = "Application Date"
        Me.dgcolhAppDate.Name = "dgcolhAppDate"
        Me.dgcolhAppDate.ReadOnly = True
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.Width = 200
        '
        'dgcolhLoanScheme
        '
        Me.dgcolhLoanScheme.HeaderText = "Loan Scheme"
        Me.dgcolhLoanScheme.Name = "dgcolhLoanScheme"
        Me.dgcolhLoanScheme.ReadOnly = True
        Me.dgcolhLoanScheme.Width = 200
        '
        'dgcolhLoan_Advance
        '
        Me.dgcolhLoan_Advance.HeaderText = "Loan / Advance"
        Me.dgcolhLoan_Advance.Name = "dgcolhLoan_Advance"
        Me.dgcolhLoan_Advance.ReadOnly = True
        '
        'dgcolhAmount
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhAmount.HeaderText = "Loan Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        '
        'dgcolhApp_Amount
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhApp_Amount.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhApp_Amount.HeaderText = "Approved Amount"
        Me.dgcolhApp_Amount.Name = "dgcolhApp_Amount"
        Me.dgcolhApp_Amount.ReadOnly = True
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        '
        'objdgcolhIsLoan
        '
        Me.objdgcolhIsLoan.HeaderText = "objdgcolhIsLoan"
        Me.objdgcolhIsLoan.Name = "objdgcolhIsLoan"
        Me.objdgcolhIsLoan.ReadOnly = True
        Me.objdgcolhIsLoan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsLoan.Visible = False
        '
        'objdgcolhStatusunkid
        '
        Me.objdgcolhStatusunkid.HeaderText = "objdgcolhStatusunkid"
        Me.objdgcolhStatusunkid.Name = "objdgcolhStatusunkid"
        Me.objdgcolhStatusunkid.ReadOnly = True
        Me.objdgcolhStatusunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhStatusunkid.Visible = False
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'objdgcolhSchemeID
        '
        Me.objdgcolhSchemeID.HeaderText = "objdgcolhSchemeID"
        Me.objdgcolhSchemeID.Name = "objdgcolhSchemeID"
        Me.objdgcolhSchemeID.ReadOnly = True
        Me.objdgcolhSchemeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhSchemeID.Visible = False
        '
        'objdgcolhprocesspendingloanunkid
        '
        Me.objdgcolhprocesspendingloanunkid.HeaderText = "objdgcolhprocesspendingloanunkid"
        Me.objdgcolhprocesspendingloanunkid.Name = "objdgcolhprocesspendingloanunkid"
        Me.objdgcolhprocesspendingloanunkid.ReadOnly = True
        Me.objdgcolhprocesspendingloanunkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhprocesspendingloanunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhprocesspendingloanunkid.Visible = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(7, 6)
        Me.chkSelectAll.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 10
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'gbApprovedApplication
        '
        Me.gbApprovedApplication.BorderColor = System.Drawing.Color.Black
        Me.gbApprovedApplication.Checked = False
        Me.gbApprovedApplication.CollapseAllExceptThis = False
        Me.gbApprovedApplication.CollapsedHoverImage = Nothing
        Me.gbApprovedApplication.CollapsedNormalImage = Nothing
        Me.gbApprovedApplication.CollapsedPressedImage = Nothing
        Me.gbApprovedApplication.CollapseOnLoad = False
        Me.gbApprovedApplication.Controls.Add(Me.Panel1)
        Me.gbApprovedApplication.ExpandedHoverImage = Nothing
        Me.gbApprovedApplication.ExpandedNormalImage = Nothing
        Me.gbApprovedApplication.ExpandedPressedImage = Nothing
        Me.gbApprovedApplication.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApprovedApplication.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApprovedApplication.HeaderHeight = 25
        Me.gbApprovedApplication.HeaderMessage = ""
        Me.gbApprovedApplication.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbApprovedApplication.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApprovedApplication.HeightOnCollapse = 0
        Me.gbApprovedApplication.LeftTextSpace = 0
        Me.gbApprovedApplication.Location = New System.Drawing.Point(0, 126)
        Me.gbApprovedApplication.Name = "gbApprovedApplication"
        Me.gbApprovedApplication.OpenHeight = 300
        Me.gbApprovedApplication.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApprovedApplication.ShowBorder = True
        Me.gbApprovedApplication.ShowCheckBox = False
        Me.gbApprovedApplication.ShowCollapseButton = False
        Me.gbApprovedApplication.ShowDefaultBorderColor = True
        Me.gbApprovedApplication.ShowDownButton = False
        Me.gbApprovedApplication.ShowHeader = True
        Me.gbApprovedApplication.Size = New System.Drawing.Size(861, 311)
        Me.gbApprovedApplication.TabIndex = 11
        Me.gbApprovedApplication.Temp = 0
        Me.gbApprovedApplication.Text = "Approved Loan/Advance Application(s)."
        Me.gbApprovedApplication.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkSelectAll)
        Me.Panel1.Controls.Add(Me.dgvLoanApproved)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(3, 25)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(855, 283)
        Me.Panel1.TabIndex = 263
        '
        'frmGlobalCancelApprovedApplication
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(862, 498)
        Me.Controls.Add(Me.gbApprovedApplication)
        Me.Controls.Add(Me.gbInfo)
        Me.Controls.Add(Me.gbFilter)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalCancelApprovedApplication"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Cancel Approved Application"
        Me.objFooter.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        Me.gbFilter.ResumeLayout(False)
        Me.gbFilter.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLoanApproved, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbApprovedApplication.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents cboAmountCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovedAmount As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents txtApprovedAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLoanAdvance As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents txtApplicationNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApplicationNo As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblApplicationDate As System.Windows.Forms.Label
    Friend WithEvents dgvLoanApproved As System.Windows.Forms.DataGridView
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhchkSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhApplicationNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanScheme As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoan_Advance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApp_Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsLoan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSchemeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhprocesspendingloanunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbApprovedApplication As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    
End Class
