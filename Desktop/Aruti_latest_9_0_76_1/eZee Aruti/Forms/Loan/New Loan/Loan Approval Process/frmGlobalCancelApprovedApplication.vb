﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalCancelApprovedApplication

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalCancelApprovedApplication"
    Private objPendingLoan As clsProcess_pending_loan
    Private mstrAdvanceFilter As String = ""
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtTable As New DataTable
    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private mstrSearchText As String = String.Empty
    'Nilay (27-Oct-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmGlobalCancelApprovedApplication_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsProcess_pending_loan"
            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalCancelApprovedApplication_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGlobalCancelApprovedApplication_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPendingLoan = New clsProcess_pending_loan
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalCancelApprovedApplication_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objMaster As New clsMasterData
        Try
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, _
                                                  ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                  "Employee", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Employee")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Nilay (27-Oct-2016) -- End

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboLoanScheme)
            'Nilay (27-Oct-2016) -- End

            dsList = objPendingLoan.GetLoan_Status("Status", False, False, True)
            Dim xRow = From p In dsList.Tables("Status") Where CInt(p.Item("Id")) = enLoanApplicationStatus.CANCELLED Select p
            Dim dtTable = xRow.CopyToDataTable
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 5
            End With

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 2, "Loan"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 3, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(False, True)
            dsList = objMaster.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable

            With cboAmountCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dtCondition
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strSearch As String = String.Empty
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If txtApplicationNo.Text.Trim <> "" Then
                strSearch &= "AND lnloan_process_pending_loan.Application_No like '" & txtApplicationNo.Text.Trim & "%' "
            End If

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                strSearch &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = " & CInt(cboLoanAdvance.SelectedIndex) & " "
            End If

            If txtApprovedAmount.Text.Trim <> "" AndAlso txtApprovedAmount.Decimal > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.approved_amount  " & cboAmountCondition.Text & " " & txtApprovedAmount.Decimal & " "
            End If

            If dtpFromDate.Checked Then
                strSearch &= "AND lnloan_process_pending_loan.application_date >='" & eZeeDate.convertDate(dtpFromDate.Value) & "' "
            End If

            If dtpToDate.Checked Then
                strSearch &= "AND lnloan_process_pending_loan.application_date <='" & eZeeDate.convertDate(dtpToDate.Value) & "' "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter
            End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objPendingLoan.GetList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, _
                                            ConfigParameter._Object._IsIncludeInactiveEmp, _
                                            "List", enLoanApplicationStatus.APPROVED, strSearch)

            If dsList.Tables("List").Columns.Contains("IsChecked") = False Then
                dsList.Tables("List").Columns.Add("IsChecked", Type.GetType("System.Boolean")).DefaultValue = False
            End If

            mdtTable = New DataView(dsList.Tables("List"), "", "processpendingloanunkid desc", DataViewRowState.CurrentRows).ToTable

            dgvLoanApproved.AutoGenerateColumns = False

            objdgcolhchkSelect.DataPropertyName = "IsChecked"
            dgcolhApplicationNo.DataPropertyName = "Application_No"
            dgcolhAppDate.DataPropertyName = "applicationdate"
            dgcolhAppDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern

            dgcolhEmployee.DataPropertyName = "EmpName"
            dgcolhLoanScheme.DataPropertyName = "LoanScheme"
            dgcolhLoan_Advance.DataPropertyName = "Loan_Advance"

            dgcolhAmount.DataPropertyName = "Amount"
            dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhApp_Amount.DataPropertyName = "Approved_Amount"
            dgcolhApp_Amount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhApp_Amount.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhStatus.DataPropertyName = "LoanStatus"
            objdgcolhIsLoan.DataPropertyName = "isloan"
            objdgcolhStatusunkid.DataPropertyName = "loan_statusunkid"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            objdgcolhSchemeID.DataPropertyName = "loanschemeunkid"
            objdgcolhprocesspendingloanunkid.DataPropertyName = "processpendingloanunkid"

            dgvLoanApproved.DataSource = mdtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            Call objbtnSearch.ShowResult(CStr(dgvLoanApproved.RowCount))
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Status is mandatory information. Please select atleast one status from the list."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                If txtRemarks.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Remarks cannot be blank. Please specify remarks."), enMsgBoxStyle.Information)
                    txtRemarks.Focus()
                    Return False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 8, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End

#End Region

#Region " Button's Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboLoanScheme.SelectedValue = 0
            'Nilay (16-Nov-2016) -- Start
            Call SetDefaultSearchText(cboEmployee)
            Call SetDefaultSearchText(cboLoanScheme)
            lblEmployee.Focus()
            'Nilay (16-Nov-2016) -- End

            cboLoanAdvance.SelectedIndex = 0
            cboAmountCondition.SelectedIndex = 0
            txtApplicationNo.Text = ""
            txtApprovedAmount.Decimal = 0
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpFromDate.Checked = False
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Checked = False
            txtRemarks.Text = ""
            mstrAdvanceFilter = ""

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchScheme.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboLoanScheme.ValueMember
                .DisplayMember = cboLoanScheme.DisplayMember
                .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                .CodeMember = "Code"
            End With

            If objFrm.DisplayDialog Then
                cboLoanScheme.SelectedValue = objFrm.SelectedValue
                cboLoanScheme.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchScheme_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim enMailType As New clsProcess_pending_loan.enApproverEmailType
        Try
            If dgvLoanApproved.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "No Records found."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If mdtTable Is Nothing Then Exit Sub

            Dim dRow As DataRow() = mdtTable.Select("IsChecked=1")

            If dRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one record for further process."), enMsgBoxStyle.Information)
                dgvLoanApproved.Focus()
                Exit Sub
            End If

            If IsValidate() = True Then
                Me.Cursor = Cursors.WaitCursor
                For i As Integer = 0 To dRow.Length - 1
                    objPendingLoan._Processpendingloanunkid = CInt(dRow(i).Item("processpendingloanunkid"))
                    objPendingLoan._Loan_Statusunkid = CInt(cboStatus.SelectedValue)
                    objPendingLoan._Remark = txtRemarks.Text.Trim.ToString
                    objPendingLoan._Userunkid = User._Object._Userunkid

                    If objPendingLoan.Update(False) = False Then
                        eZeeMsgBox.Show(objPendingLoan._Message, enMsgBoxStyle.Information)
                        dgvLoanApproved.Rows(mdtTable.Rows.IndexOf(dRow(i))).Cells(dgcolhApplicationNo.Index).Selected = True
                        Exit Sub
                    End If

                    'Nilay (10-Dec-2016) -- Start
                    'Issue #26: Setting to be included on configuration for Loan flow Approval process
                    If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then

                    If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.CANCELLED Then

                        If CBool(dRow(i).Item("isloan")) = True Then
                            enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                        ElseIf CBool(dRow(i).Item("isloan")) = False Then
                            enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                        End If

                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objPendingLoan.Send_Notification_Employee(CInt(dRow(i).Item("employeeunkid")), _
                            '                                      CInt(dRow(i).Item("processpendingloanunkid")), _
                            '                                      clsProcess_pending_loan.enNoticationLoanStatus.CANCELLED, _
                            '                                      enMailType, _
                            '                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                                      txtRemarks.Text.Trim, _
                            '                                      enLogin_Mode.DESKTOP, 0, _
                            '                                      User._Object._Userunkid)
                        objPendingLoan.Send_Notification_Employee(CInt(dRow(i).Item("employeeunkid")), _
                                                                  CInt(dRow(i).Item("processpendingloanunkid")), _
                                                                  clsProcess_pending_loan.enNoticationLoanStatus.CANCELLED, _
                                                                  enMailType, _
                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, _
                                                                  txtRemarks.Text.Trim, _
                                                                  enLogin_Mode.DESKTOP, 0, _
                                                                  User._Object._Userunkid)
                            'Sohail (30 Nov 2017) -- End
                    End If
                    End If
                    'Nilay (10-Dec-2016) -- End
                Next
                Call FillList()
                txtRemarks.Text = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " Link Button's Event "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                mstrAdvanceFilter = frm._GetFilterString
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If mdtTable.Rows.Count > 0 Then
                For Each dRow As DataRow In mdtTable.Rows
                    dRow.Item("IsChecked") = chkSelectAll.Checked
                Next
                mdtTable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Private Sub dgvLoanApproved_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanApproved.CellContentClick, _
                                                                                                                                            dgvLoanApproved.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhchkSelect.Index Then
                If dgvLoanApproved.IsCurrentCellDirty Then
                    dgvLoanApproved.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                mdtTable.AcceptChanges()

                Dim dRow As DataRow() = mdtTable.Select("IsChecked = 1")

                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

                If dRow.Length <= 0 Then
                    chkSelectAll.CheckState = CheckState.Unchecked
                ElseIf dRow.Length < mdtTable.Rows.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                ElseIf dRow.Length = mdtTable.Rows.Count Then
                    chkSelectAll.CheckState = CheckState.Checked
                End If

                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanApproved_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
#Region " ComBox's Events "
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboEmployee)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    .CodeMember = "Code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboLoanScheme.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboLoanScheme.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            If CInt(cboLoanScheme.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.GotFocus
        Try
            With cboLoanScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.Leave
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_Leave", mstrModuleName)
        End Try
    End Sub
#End Region
    'Nilay (27-Oct-2016) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbApprovedApplication.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApprovedApplication.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblApprovedAmount.Text = Language._Object.getCaption(Me.lblApprovedAmount.Name, Me.lblApprovedAmount.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
            Me.lblApplicationNo.Text = Language._Object.getCaption(Me.lblApplicationNo.Name, Me.lblApplicationNo.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblApplicationDate.Text = Language._Object.getCaption(Me.lblApplicationDate.Name, Me.lblApplicationDate.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.dgcolhApplicationNo.HeaderText = Language._Object.getCaption(Me.dgcolhApplicationNo.Name, Me.dgcolhApplicationNo.HeaderText)
            Me.dgcolhAppDate.HeaderText = Language._Object.getCaption(Me.dgcolhAppDate.Name, Me.dgcolhAppDate.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhLoanScheme.HeaderText = Language._Object.getCaption(Me.dgcolhLoanScheme.Name, Me.dgcolhLoanScheme.HeaderText)
            Me.dgcolhLoan_Advance.HeaderText = Language._Object.getCaption(Me.dgcolhLoan_Advance.Name, Me.dgcolhLoan_Advance.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhApp_Amount.HeaderText = Language._Object.getCaption(Me.dgcolhApp_Amount.Name, Me.dgcolhApp_Amount.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.gbApprovedApplication.Text = Language._Object.getCaption(Me.gbApprovedApplication.Name, Me.gbApprovedApplication.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Loan")
            Language.setMessage(mstrModuleName, 3, "Advance")
            Language.setMessage(mstrModuleName, 4, "No Records found.")
            Language.setMessage(mstrModuleName, 5, "Please check atleast one record for further process.")
            Language.setMessage(mstrModuleName, 6, "Status is mandatory information. Please select atleast one status from the list.")
            Language.setMessage(mstrModuleName, 7, "Remarks cannot be blank. Please specify remarks.")
            Language.setMessage(mstrModuleName, 8, "Type to Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class