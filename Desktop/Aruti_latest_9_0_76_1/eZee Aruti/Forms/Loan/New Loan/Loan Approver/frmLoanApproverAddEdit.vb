﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLoanApproverAddEdit

#Region "Private Variables"

    'Hemant (19 Mar 2019) -- Start
    'ENHANCEMENT : Add Missing Column fields of AT Tables & Show its on "Audit & Trails" Module.
    'Private Shared ReadOnly mstrModuleName As String = "frmLoanAprroverAddEdit"
    Private Shared ReadOnly mstrModuleName As String = "frmLoanApproverAddEdit"
    'Hemant (19 Mar 2019) -- End
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mintlnApproverunkid As Integer = -1
    Private mstrAdvanceFilter As String = ""
    Private objlnApproverLevelMaster As clslnapproverlevel_master
    Private objlnApproverMaster As clsLoanApprover_master
    Private objlnApproverTran As clsLoanApprover_tran
    Private mdtEmployee As DataTable
    Private mdtAssignedEmp As DataTable
    Private dtEmpView As DataView
    Private dtAssignEmpView As DataView
    Private mdtLoanSchemeMapping As DataTable = Nothing
    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Private mstrEmployeeIDs As String = String.Empty
    'Nilay (01-Mar-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal intlnApproverunkid As Integer) As Boolean
        Try
            mintlnApproverunkid = intlnApproverunkid
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region "Private Methods"

    Private Sub Fill_Combo()
        Dim dsList As DataSet = Nothing
        Dim dsCombo As DataSet = Nothing
        Dim objUsr As New clsUserAddEdit
        Dim objPswd As New clsPassowdOptions
        Dim objEmp As New clsEmployee_Master
        objlnApproverLevelMaster = New clslnapproverlevel_master

        Try
            dsList = objlnApproverLevelMaster.getListForCombo("List", True)

            With cboLoanApproveLevel
                .ValueMember = "lnlevelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetList("List", , True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , "isapproved = 1")
            dsCombo = objEmp.GetList(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     ConfigParameter._Object._UserAccessModeSetting, _
                                     True, False, "List", , , , "isapproved = 1")
            'Nilay (10-Oct-2015) -- End

            mdtEmployee = dsCombo.Tables("List")
            mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If objPswd._IsEmployeeAsUser Then
            '    dsCombo = objUsr.getComboList("User", True, False, True, Company._Object._Companyunkid, 702, FinancialYear._Object._YearUnkid)
            'Else
            '    dsCombo = objUsr.getComboList("User", True, False, False, )
            'End If
            dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, CStr(702), FinancialYear._Object._YearUnkid, True)
            'Nilay (01-Mar-2016) -- End

            With cboUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("User")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        End Try

    End Sub

    Private Sub Fill_Employee()
        Try
            If mdtEmployee.Rows.Count > 0 Then
                Dim dEmp As DataTable = Nothing
                Dim sFilter As String = String.Empty

                If mstrAdvanceFilter.Trim.Length > 0 Then
                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'sFilter &= "AND " & mstrAdvanceFilter
                    sFilter &= "AND " & mstrAdvanceFilter.Replace("ADF.", "")
                    'Nilay (01-Mar-2016) -- End
                End If

                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'Dim mstrIds As String = String.Empty
                'If txtName.Tag IsNot Nothing AndAlso mstrIds.Trim.Length > 0 Then
                '    sFilter &= "AND employeeunkid NOT IN(" & txtName.Tag.ToString & "," & mstrIds & ")"
                'Else
                '    sFilter &= "AND employeeunkid NOT IN(" & txtName.Tag.ToString & ")"
                'End If
                If chkExternalApprover.Checked = False Then
                    If txtName.Tag IsNot Nothing Then
                        sFilter &= "AND employeeunkid NOT IN(" & txtName.Tag.ToString & ")"
                    End If
                End If
                If mstrEmployeeIDs.Trim.Length > 0 Then
                    sFilter &= "AND employeeunkid NOT IN(" & mstrEmployeeIDs & ") "
                End If
                'Nilay (01-Mar-2016) -- End

                If sFilter.Trim.Length > 0 Then
                    sFilter = sFilter.Substring(3)
                    dEmp = New DataView(mdtEmployee, sFilter, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dEmp = New DataView(mdtEmployee, "", "", DataViewRowState.CurrentRows).ToTable
                End If

                dtEmpView = dEmp.DefaultView

                dgvAEmployee.AutoGenerateColumns = False
                objdgcolhECheck.DataPropertyName = "ischeck"
                dgcolhEcode.DataPropertyName = "employeecode"
                dgcolhEName.DataPropertyName = "name"
                objdgcolhEmpId.DataPropertyName = "employeeunkid"
                dgvAEmployee.DataSource = dtEmpView

                If txtName.Tag IsNot Nothing Then
                    mdtAssignedEmp = objlnApproverTran._TranDataTable
                    Call Fill_Assigned_Employee()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Assigned_Employee()
        Try
            dtAssignEmpView = mdtAssignedEmp.DefaultView
            dtAssignEmpView.RowFilter = " AUD <> 'D' "
            dgvAssignedEmp.AutoGenerateColumns = False
            objdgcolhaCheck.DataPropertyName = "ischeck"
            dgcolhaCode.DataPropertyName = "ecode"
            dgcolhaEmp.DataPropertyName = "ename"
            dgcolhaDepartment.DataPropertyName = "edept"
            dgcolhaJob.DataPropertyName = "ejob"
            objdgcolhaEmpId.DataPropertyName = "employeeunkid"
            objdgcolhAMasterId.DataPropertyName = "lnapproverunkid"
            objdgcolhATranId.DataPropertyName = "lnapprovertranunkid"
            dgvAssignedEmp.DataSource = dtAssignEmpView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Assigned_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid_Data() As Boolean
        Try
            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan Approver is mandatory information. Please provide Loan Approver to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboLoanApproveLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Approver Level is mandatory information. Please provide Loan Approver Level to continue"), enMsgBoxStyle.Information)
                cboLoanApproveLevel.Focus()
                Return False
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CInt(cboUser.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "User is mandatory information. Please provide User to continue"), enMsgBoxStyle.Information)
            '    cboUser.Focus()
            '    Return False
            'End If
            If chkExternalApprover.Checked = False Then
                If CInt(cboUser.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "User is mandatory information. Please provide User to continue"), enMsgBoxStyle.Information)
                    cboUser.Focus()
                    Return False
                End If
            End If
            'Nilay (01-Mar-2016) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid_Data", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Add_DataRow(ByVal dRow As DataRow)
        Try

            If mdtAssignedEmp Is Nothing Then Exit Sub

            Dim mdtRow As DataRow = Nothing
            Dim dtAEmp() As DataRow = Nothing
            dtAEmp = mdtAssignedEmp.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
            If dtAEmp.Length <= 0 Then
                mdtRow = mdtAssignedEmp.NewRow
                mdtRow.Item("lnapprovertranunkid") = -1
                mdtRow.Item("lnloanapproverunkid") = mintlnApproverunkid
                mdtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                mdtRow.Item("userunkid") = User._Object._Userunkid
                mdtRow.Item("isvoid") = False
                mdtRow.Item("voiddatetime") = DBNull.Value
                mdtRow.Item("voiduserunkid") = -1
                mdtRow.Item("voidreason") = ""
                mdtRow.Item("AUD") = "A"
                mdtRow.Item("GUID") = Guid.NewGuid.ToString
                mdtRow.Item("ecode") = dRow.Item("employeecode")
                mdtRow.Item("ename") = dRow.Item("name")
                mdtRow.Item("edept") = dRow.Item("DeptName")
                mdtRow.Item("ejob") = dRow.Item("job_name")
                mdtAssignedEmp.Rows.Add(mdtRow)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objlnApproverMaster._ApproverEmpunkid = CInt(txtName.Tag)
            objlnApproverMaster._lnLevelunkid = CInt(cboLoanApproveLevel.SelectedValue)
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'objlnApproverMaster._MappedUserunkid = CInt(cboUser.SelectedValue)
            If chkExternalApprover.Checked = False Then
                objlnApproverMaster._MappedUserunkid = CInt(cboUser.SelectedValue)
            Else
                objlnApproverMaster._MappedUserunkid = CInt(txtName.Tag)
            End If
            objlnApproverMaster._IsExternalApprover = chkExternalApprover.Checked
            'Nilay (01-Mar-2016) -- End
            objlnApproverMaster._dtLoanSchemeMapping = mdtLoanSchemeMapping
            objlnApproverMaster._Isactive = True
            objlnApproverMaster._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            chkExternalApprover.Checked = objlnApproverMaster._IsExternalApprover

            'txtName.Tag = objlnApproverMaster._ApproverEmpunkid
            'Dim objEmployee As New clsEmployee_Master

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''objEmployee._Employeeunkid = objlnApproverMaster._ApproverEmpunkid
            'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objlnApproverMaster._ApproverEmpunkid
            ''S.SANDEEP [04 JUN 2015] -- END
            'txtName.Text = objEmployee._Firstname + " " + objEmployee._Surname
            'cboLoanApproveLevel.SelectedValue = objlnApproverMaster._lnLevelunkid
            'cboUser.SelectedValue = objlnApproverMaster._MappedUserunkid

            Dim objEmployee As New clsEmployee_Master

            If objlnApproverMaster._IsExternalApprover = True Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = objlnApproverMaster._ApproverEmpunkid
                Dim strAppName As String = objUser._Username
                If strAppName.Trim.Length > 0 Then
                    txtName.Text = strAppName
                Else
                    txtName.Text = objUser._Username
                End If
                txtName.Tag = objUser._Userunkid
                objUser = Nothing
            Else
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objlnApproverMaster._ApproverEmpunkid
                txtName.Text = objEmployee._Firstname + " " + objEmployee._Surname
                txtName.Tag = objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            End If

            cboLoanApproveLevel.SelectedValue = objlnApproverMaster._lnLevelunkid
            cboUser.SelectedValue = objlnApproverMaster._MappedUserunkid
            'Nilay (01-Mar-2016) -- End

            If menAction = enAction.EDIT_ONE Then
                Dim objLoanSchemeMapping As New clsapprover_scheme_mapping
                mdtLoanSchemeMapping = objLoanSchemeMapping.GetLoanSchemeForMapping(mintlnApproverunkid)
                objLoanSchemeMapping = Nothing
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                mstrEmployeeIDs = objlnApproverMaster.GetApproverEmployeeId(CInt(txtName.Tag), CBool(objlnApproverMaster._IsExternalApprover))
                'Nilay (01-Mar-2016) -- End
            End If

            If txtName.Text.Trim.Length > 0 Then
                Call Fill_Employee()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            lnkMapLoanScheme.Visible = ConfigParameter._Object._IsLoanApprover_ForLoanScheme
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Form's Event"

    Private Sub frmLoanAprroverAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objlnApproverMaster = New clsLoanApprover_master
        objlnApproverTran = New clsLoanApprover_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Combo()
            If menAction = enAction.EDIT_ONE Then
                objlnApproverMaster._lnApproverunkid = mintlnApproverunkid
                objlnApproverTran._LoanApproverunkid = mintlnApproverunkid
                objlnApproverTran.GetData()
                objbtnSearchEmployee.Enabled = False
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                chkExternalApprover.Enabled = False
                'Nilay (01-Mar-2016) -- End
            End If
            SetVisibility()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAprroverAddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanApproverAddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpenseApprover_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsLoanApprover_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            If Is_Valid_Data() = False Then Exit Sub

            If dtEmpView IsNot Nothing Then
                Dim drCheck() As DataRow = Nothing
                drCheck = dtEmpView.Table.Select("ischeck=true")
                Dim iblFlag As Boolean = False
                If drCheck.Length > 0 Then
                    Me.Cursor = Cursors.WaitCursor
                    For i As Integer = 0 To drCheck.Length - 1
                        Call Add_DataRow(drCheck(i))
                    Next
                End If
                Call Fill_Assigned_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnDeleteA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteA.Click
        Try
            If dtAssignEmpView Is Nothing Then
                Exit Sub
            End If
            dtAssignEmpView.Table.AcceptChanges()
            Dim dtmp() As DataRow = dtAssignEmpView.Table.Select("ischeck=True AND AUD <> 'D' ")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one of the employee to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            ' Me.SuspendLayout()

            Dim blnFlag As Boolean = False
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete Selected Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor

                'Pinkal (26-Sep-2017) -- Start
                'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
                Dim objLoan As New clsProcess_pending_loan
                Dim mblnFlag As Boolean = True
                Dim i As Integer
                For i = 0 To dtmp.Length - 1
                    If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                        If objLoan.GetApproverPendingLoanFormCount(mintlnApproverunkid, dtmp(i)("employeeunkid").ToString()) <= 0 Then
                        dtmp(i).Item("AUD") = "D"
                        dtmp(i).Item("isvoid") = True
                        dtmp(i).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtmp(i).Item("voiduserunkid") = User._Object._Userunkid
                        dtmp(i).Item("voidreason") = ""
                        dtmp(i).AcceptChanges()
                        Else
                            mblnFlag = False
                        End If
                    End If
                Next
                If mblnFlag = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "This Employee(s) has pending loan application(s).You cannot delete this employee(s)."), enMsgBoxStyle.Information)
                End If
                objLoan = Nothing
                'Pinkal (26-Sep-2017) -- End

                dtAssignEmpView.Table.AcceptChanges()
                txtAssignedEmpSearch.Text = ""
                Fill_Assigned_Employee()
                RemoveHandler objchkAssignEmp.CheckedChanged, AddressOf objchkAssignEmp_CheckedChanged
                objchkAssignEmp.Checked = False
                AddHandler objchkAssignEmp.CheckedChanged, AddressOf objchkAssignEmp_CheckedChanged
            End If
            'Me.ResumeLayout()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteA_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Is_Valid_Data() = False Then Exit Sub

            Dim count As Integer = CInt(mdtAssignedEmp.Compute("Count(employeeunkid)", "AUD <> 'D'"))
            If mdtAssignedEmp.Rows.Count = 0 OrElse count = 0 OrElse dgvAssignedEmp.Rows.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Employee is compulsory information.Please Check atleast One Employee."), enMsgBoxStyle.Information)
                dgvAEmployee.Select()
                Exit Sub
            End If

            If ConfigParameter._Object._IsLoanApprover_ForLoanScheme Then
                If mdtLoanSchemeMapping Is Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please map loan scheme for this approver."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            'If objlnApproverMaster.IsEmployeeExistForUser(CInt(cboUser.SelectedValue), CInt(txtName.Tag)) = False Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You cannot map") & " " & txtName.Text.Trim & " " & Language.getMessage(mstrModuleName, 11, "to") & "  " & cboUser.Text & " " & Language.getMessage(mstrModuleName, 12, "user.Reason: This User is already mapped to other approver(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If


            Me.Cursor = Cursors.WaitCursor

            SetValue()

            If menAction <> enAction.EDIT_ONE Then
                If objlnApproverMaster.Insert(mdtAssignedEmp) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Approver saved successfully."), enMsgBoxStyle.Information)
                    mblnCancel = False
                    Me.Close()
                Else
                    eZeeMsgBox.Show(objlnApproverMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                If objlnApproverMaster.Update(mdtAssignedEmp) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Loan Approver updated successfully."), enMsgBoxStyle.Information)
                    mblnCancel = False
                    Me.Close()
                Else
                    eZeeMsgBox.Show(objlnApproverMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'With frm
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "name"
            '    .CodeMember = "employeecode"
            '    .DataSource = mdtEmployee
            'End With

            'If frm.DisplayDialog Then
            '    txtName.Text = frm.SelectedAlias & " - " & frm.SelectedText
            '    txtName.Tag = frm.SelectedValue
            '    Dim objOption As New clsPassowdOptions
            '    If objOption._IsEmployeeAsUser Then
            '        Dim objUser As New clsUserAddEdit
            '        Dim mintUserID As Integer = objUser.Return_UserId(CInt(frm.SelectedValue), Company._Object._Companyunkid)
            '        Dim drRow() As DataRow = CType(cboUser.DataSource, DataTable).Select("userunkid = " & mintUserID)
            '        If drRow.Length > 0 Then
            '            cboUser.SelectedValue = mintUserID
            '        Else
            '            cboUser.SelectedValue = 0
            '        End If
            '    End If
            'End If
            'If txtName.Text.Trim.Length > 0 Then Call Fill_Employee()

            If chkExternalApprover.Checked = False Then
                With frm
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "name"
                    .CodeMember = "employeecode"
                    .DataSource = mdtEmployee
                End With

                If frm.DisplayDialog Then
                    txtName.Text = frm.SelectedAlias & " - " & frm.SelectedText
                    txtName.Tag = frm.SelectedValue
                    Dim objoption As New clsPassowdOptions
                    If objoption._IsEmployeeAsUser Then
                        Dim objuser As New clsUserAddEdit
                        Dim mintuserid As Integer = objuser.Return_UserId(CInt(frm.SelectedValue), Company._Object._Companyunkid)
                        Dim drrow() As DataRow = CType(cboUser.DataSource, DataTable).Select("userunkid = " & mintuserid)
                        If drrow.Length > 0 Then
                            cboUser.SelectedValue = mintuserid
                        Else
                            cboUser.SelectedValue = 0
                        End If
                    End If
                End If

            Else
                Dim objUser As New clsUserAddEdit
                Dim dsList As DataSet

                dsList = objUser.GetExternalApproverList("List", Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         "345", True)
                objUser = Nothing

                With frm
                    .ValueMember = "userunkid"
                    .DisplayMember = "name"
                    .CodeMember = "username"
                    .DataSource = dsList.Tables("List")
                End With

                If frm.DisplayDialog Then
                    txtName.Text = frm.SelectedText
                    txtName.Tag = frm.SelectedValue
                End If

            End If

            mstrEmployeeIDs = objlnApproverMaster.GetApproverEmployeeId(CInt(txtName.Tag), chkExternalApprover.Checked)

            If txtName.Text.Trim.Length > 0 Then Call Fill_Employee()
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboLoanApproveLevel.ValueMember
                .DisplayMember = cboLoanApproveLevel.DisplayMember
                .DataSource = CType(cboLoanApproveLevel.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboLoanApproveLevel.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboUser.ValueMember
                .DisplayMember = cboUser.DisplayMember
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboUser.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            txtSearchEmp.Text = ""
            objchkEmployee.Checked = False
            Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
#Region "CheckBox Events"
    Private Sub chkExternalApprover_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExternalApprover.CheckedChanged
        Try
            If chkExternalApprover.Checked = True Then
                LblUser.Visible = False
                cboUser.Visible = False
                objbtnSearchUser.Visible = False
            Else
                LblUser.Visible = True
                cboUser.Visible = True
                cboUser.SelectedValue = 0
                objbtnSearchUser.Visible = True
            End If

            If menAction <> enAction.EDIT_ONE Then
                txtName.Text = ""
                txtName.Tag = Nothing
                mstrAdvanceFilter = ""
                dgvAEmployee.DataSource = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExternalApprover_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Nilay (01-Mar-2016) -- End

#Region "Data Grid Events"

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                dtEmpView.Table.AcceptChanges()

                Dim drRow As DataRow() = dtEmpView.Table.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtEmpView.Table.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAssignedEmp_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAssignedEmp.CellContentClick
        Try
            RemoveHandler objchkAssignEmp.CheckedChanged, AddressOf objchkAssignEmp_CheckedChanged

            If e.ColumnIndex = objdgcolhaCheck.Index Then

                If Me.dgvAssignedEmp.IsCurrentCellDirty Then
                    Me.dgvAssignedEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                dtAssignEmpView.Table.AcceptChanges()

                Dim drRow As DataRow() = dtAssignEmpView.Table.Select("ischeck = true")
                If drRow.Length > 0 Then
                    If dtAssignEmpView.Table.Rows.Count = drRow.Length Then
                        objchkAssignEmp.CheckState = CheckState.Checked
                    Else
                        objchkAssignEmp.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAssignEmp.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkAssignEmp.CheckedChanged, AddressOf objchkAssignEmp_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAssignedEmp_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Link Button Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                'Nilay (01-Mar-2016) -- End
                mstrAdvanceFilter = frm._GetFilterString
                Call Fill_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkMapLoanScheme_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkMapLoanScheme.LinkClicked
        Try
            If txtName.Text.Trim.Length > 0 AndAlso CInt(cboLoanApproveLevel.SelectedValue) > 0 Then
                Dim objFrm As New frmApprover_LoanSchemeMapping
                objFrm._EmployeeName = txtName.Text.Trim
                objFrm._ApproverLevel = cboLoanApproveLevel.Text
                If mdtLoanSchemeMapping IsNot Nothing AndAlso mdtLoanSchemeMapping.Rows.Count > 0 Then
                    objFrm._dtLoanSchemeMapping = mdtLoanSchemeMapping
                End If
                'Pinkal (26-Sep-2017) -- Start
                'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
                If objFrm.displayDialog(-1, enAction.ADD_ONE, mintlnApproverunkid) Then
                mdtLoanSchemeMapping = objFrm._dtLoanSchemeMapping
                End If
                'Pinkal (26-Sep-2017) -- End
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Select Name or Level to do futher operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkMapLoanScheme_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If dtEmpView Is Nothing Then Exit Sub
            'Shani (21-Jul-2016) -- End
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' "
            End If
            dtEmpView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtaSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAssignedEmpSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAssignedEmp.Rows.Count > 0 Then
                        If dgvAssignedEmp.SelectedRows(0).Index = dgvAssignedEmp.Rows(dgvAssignedEmp.RowCount - 1).Index Then
                            Exit Sub
                            dgvAssignedEmp.Rows(dgvAssignedEmp.SelectedRows(0).Index + 1).Selected = True
                        End If
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAssignedEmp.Rows.Count > 0 Then
                        If dgvAssignedEmp.SelectedRows(0).Index = 0 Then
                            Exit Sub
                            dgvAssignedEmp.Rows(dgvAssignedEmp.SelectedRows(0).Index - 1).Selected = True
                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtaSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssignedEmpSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAssignedEmpSearch.TextChanged
        Try

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If dtAssignEmpView Is Nothing Then Exit Sub
            'Shani (21-Jul-2016) -- End

            Dim strSearch As String = "AUD <> 'D'"
            If txtAssignedEmpSearch.Text.Trim.Length > 0 Then
                strSearch &= " AND (" & dgcolhaCode.DataPropertyName & " LIKE '%" & txtAssignedEmpSearch.Text & "%' OR " & dgcolhaEmp.DataPropertyName & " LIKE '%" & txtAssignedEmpSearch.Text & "%' )"
            End If
            dtAssignEmpView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtAssignedEmpSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In dtEmpView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            'Nilay (28-Aug-2015) -- Start
            dtEmpView.Table.AcceptChanges()
            'Nilay (28-Aug-2015) -- End
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAssignEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAssignEmp.CheckedChanged
        Try
            RemoveHandler dgvAssignedEmp.CellContentClick, AddressOf dgvAssignedEmp_CellContentClick
            For Each dr As DataRowView In dtAssignEmpView
                dr.Item("ischeck") = CBool(objchkAssignEmp.CheckState)
            Next
            dtAssignEmpView.Table.AcceptChanges()
            dgvAssignedEmp.Refresh()
            AddHandler dgvAssignedEmp.CellContentClick, AddressOf dgvAssignedEmp_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAssignEmp_CheckedChanged", mstrModuleName)
        End Try
    End Sub


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAssignedEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssignedEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteA.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteA.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.Name, Me.lblApproveLevel.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.lblLoanApproverName.Text = Language._Object.getCaption(Me.lblLoanApproverName.Name, Me.lblLoanApproverName.Text)
            Me.LblUser.Text = Language._Object.getCaption(Me.LblUser.Name, Me.LblUser.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.gbAssignedEmployee.Text = Language._Object.getCaption(Me.gbAssignedEmployee.Name, Me.gbAssignedEmployee.Text)
            Me.btnDeleteA.Text = Language._Object.getCaption(Me.btnDeleteA.Name, Me.btnDeleteA.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.dgcolhaCode.HeaderText = Language._Object.getCaption(Me.dgcolhaCode.Name, Me.dgcolhaCode.HeaderText)
            Me.dgcolhaEmp.HeaderText = Language._Object.getCaption(Me.dgcolhaEmp.Name, Me.dgcolhaEmp.HeaderText)
            Me.dgcolhaDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhaDepartment.Name, Me.dgcolhaDepartment.HeaderText)
            Me.dgcolhaJob.HeaderText = Language._Object.getCaption(Me.dgcolhaJob.Name, Me.dgcolhaJob.HeaderText)
            Me.lnkMapLoanScheme.Text = Language._Object.getCaption(Me.lnkMapLoanScheme.Name, Me.lnkMapLoanScheme.Text)
            Me.chkExternalApprover.Text = Language._Object.getCaption(Me.chkExternalApprover.Name, Me.chkExternalApprover.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Loan Approver is mandatory information. Please provide Loan Approver to continue.")
            Language.setMessage(mstrModuleName, 2, "Loan Approver Level is mandatory information. Please provide Loan Approver Level to continue")
            Language.setMessage(mstrModuleName, 3, "User is mandatory information. Please provide User to continue")
            Language.setMessage(mstrModuleName, 4, "Please check atleast one of the employee to unassign.")
            Language.setMessage(mstrModuleName, 5, "Loan Approver saved successfully.")
            Language.setMessage(mstrModuleName, 6, "Loan Approver updated successfully.")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete Selected Employee?")
            Language.setMessage(mstrModuleName, 8, "Please map loan scheme for this approver.")
            Language.setMessage(mstrModuleName, 9, "Please Select Name or Level to do futher operation.")
            Language.setMessage(mstrModuleName, 13, "Employee is compulsory information.Please Check atleast One Employee.")
            Language.setMessage(mstrModuleName, 14, "This Employee(s) has pending loan application(s).You cannot delete this employee(s).")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class