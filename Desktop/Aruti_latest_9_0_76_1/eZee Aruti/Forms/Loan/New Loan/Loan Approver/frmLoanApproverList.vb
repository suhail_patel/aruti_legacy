﻿

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLoanApproverList

#Region "Private Variable"

    Private objLoanApprover As clsLoanApprover_master
    Private ReadOnly mstrModuleName As String = "frmLoanApproverList"
    Private mintUserMappunkid As Integer = -1

#End Region

#Region "Form's Event"

    Private Sub frmLoanApproverList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoanApprover = New clsLoanApprover_master
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in loan module.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End


            fillList()
            If lvApproverList.Items.Count > 0 Then lvApproverList.Items(0).Selected = True
            lvApproverList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApproverList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanApproverList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApproverList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanApproverList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLoanApprover = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsLoanApprover_master.SetMessages()
            clsLoanApprover_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsLoanApprover_master,clsLoanApprover_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmLoanApproverAddEdit
            If ObjFrm.displayDialog(enAction.ADD_CONTINUE, -1) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvApproverList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverList.Select()
            Exit Sub
        End If
        Dim objfrmLoanApproverAddEdit As New frmLoanApproverAddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverList.SelectedItems(0).Index
            If objfrmLoanApproverAddEdit.displayDialog(enAction.EDIT_ONE, CInt(lvApproverList.SelectedItems(0).Tag)) Then
                Call fillList()
            End If
            objfrmLoanApproverAddEdit = Nothing

            lvApproverList.Items(intSelectedIndex).Selected = True
            lvApproverList.EnsureVisible(intSelectedIndex)
            lvApproverList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmLoanApproverAddEdit IsNot Nothing Then objfrmLoanApproverAddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvApproverList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverList.Select()
            Exit Sub
        End If

        'Nilay (21-Oct-2015) -- Start
        'ENHANCEMENT : NEW LOAN Given By Rutta
        If objLoanApprover.isUsed(CInt(lvApproverList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use."), enMsgBoxStyle.Information) '?2
            lvApproverList.Select()
            Exit Sub
        End If
        'Nilay (21-Oct-2015) -- End

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.LOAN, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objLoanApprover._VoidReason = mstrVoidReason
                End If
                frm = Nothing
                objLoanApprover._Isvoid = True
                objLoanApprover._VoidDateTime = ConfigParameter._Object._CurrentDateAndTime
                objLoanApprover._VoidUserunkid = User._Object._Userunkid
                objLoanApprover.Delete(CInt(lvApproverList.SelectedItems(0).Tag))
                lvApproverList.SelectedItems(0).Remove()

                If lvApproverList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvApproverList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvApproverList.Items.Count - 1
                    lvApproverList.Items(intSelectedIndex).Selected = True
                    lvApproverList.EnsureVisible(intSelectedIndex)
                ElseIf lvApproverList.Items.Count <> 0 Then
                    lvApproverList.Items(intSelectedIndex).Selected = True
                    lvApproverList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvApproverList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActive.Click, btnInActive.Click
        Try

            If lvApproverList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If

            objLoanApprover._lnApproverunkid = CInt(lvApproverList.SelectedItems(0).Tag)

            If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNACTIVE" Then
                objLoanApprover._Isactive = True
                If objLoanApprover.MakeActiveInActive(CInt(lvApproverList.SelectedItems(0).Tag)) = False Then
                    eZeeMsgBox.Show(objLoanApprover._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Approver active successfully."), enMsgBoxStyle.Information)
                End If

            ElseIf CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNINACTIVE" Then


                'Pinkal (26-Sep-2017) -- Start
                'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
                Dim objLoanForm As New clsProcess_pending_loan()
                If objLoanForm.GetApproverPendingLoanFormCount(Convert.ToInt32(lvApproverList.SelectedItems(0).Tag), "") > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You cannot inactivate this approver.Reason :This Approver has Pending Loan Application Form(s)."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objLoanForm = Nothing
                'Pinkal (26-Sep-2017) -- End

                objLoanApprover._Isactive = False
                If objLoanApprover.MakeActiveInActive(CInt(lvApproverList.SelectedItems(0).Tag)) = False Then
                    eZeeMsgBox.Show(objLoanApprover._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Approver inactive successfully."), enMsgBoxStyle.Information)
                End If
            End If
            fillList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnActive_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboLoanApprover.DataSource, DataTable)
            With cboLoanApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboLoanLevel.SelectedIndex = 0
            cboLoanApprover.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            btnActive.Visible = False
            btnInActive.Visible = True
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboLoanLevel.DataSource, DataTable)
            With cboLoanLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable
        Dim strSearching As String = ""
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in Loan module.
            If User._Object.Privilege._AllowToViewLoanApproverList = True Then
                'Varsha Rana (17-Oct-2017) -- End

            If CInt(cboLoanLevel.SelectedValue) > 0 Then
                strSearching &= "AND lnapproverlevel_master.lnlevelunkid = " & CInt(cboLoanLevel.SelectedValue)
            End If

            If CInt(cboLoanApprover.SelectedValue) > 0 Then
                strSearching &= "AND lnloanapprover_master.lnapproverunkid = " & CInt(cboLoanApprover.SelectedValue)
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dtApprover = objLoanApprover.GetList("List", IIf(cboStatus.SelectedIndex = 0, True, False), False, "", "", 0, "", strSearching).Tables(0)
            dtApprover = objLoanApprover.GetList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, _
                                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                 "List", _
                                                 IIf(cboStatus.SelectedIndex = 0, True, False), _
                                                 False, _
                                                 strSearching).Tables(0)
            'Nilay (10-Oct-2015) -- End

            Dim lvItem As ListViewItem

            lvApproverList.Items.Clear()
            For Each drRow As DataRow In dtApprover.Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("lnlevelname").ToString
                lvItem.Tag = drRow("lnapproverunkid")
                lvItem.SubItems.Add(drRow("name").ToString)
                lvItem.SubItems.Add(drRow("departmentname").ToString())
                lvItem.SubItems.Add(drRow("jobname").ToString())
                lvItem.SubItems.Add(drRow("mappeduser").ToString())
                lvItem.SubItems.Add(drRow("isactive").ToString())
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                lvItem.SubItems.Add(drRow("extapprover"))
                'Nilay (01-Mar-2016) -- End
                lvApproverList.Items.Add(lvItem)
            Next

            lvApproverList.GroupingColumn = colhLevel
            lvApproverList.SortBy(colhLevel.Index, SortOrder.Ascending)
            lvApproverList.DisplayGroups(True)

            If lvApproverList.Items.Count > 16 Then
                colhMappedUser.Width = 140 - 20
            Else
                colhMappedUser.Width = 140
            End If
            End If 'varsha (17-Oct-2017) -- End

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsApproverList.Dispose()
        End Try

    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges in loan module.
    Private Sub SetVisibility()
        Try

            btnNew.Enabled = User._Object.Privilege._AllowToAddLoanApprover
            btnEdit.Enabled = User._Object.Privilege._AllowToEditLoanApprover
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteLoanApprover
            btnInActive.Enabled = User._Object.Privilege._AllowToSetActiveInactiveLoanApprover
            btnActive.Enabled = User._Object.Privilege._AllowToSetActiveInactiveLoanApprover

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End



    Private Sub FillCombo()
        Try
            Dim objLevel As New clslnapproverlevel_master
            Dim dsList As DataSet = objLevel.getListForCombo("Level", True)
            cboLoanLevel.DisplayMember = "name"
            cboLoanLevel.ValueMember = "lnlevelunkid"
            cboLoanLevel.DataSource = dsList.Tables(0)

            cboStatus.Items.Add(Language.getMessage(mstrModuleName, 5, "Active"))
            cboStatus.Items.Add(Language.getMessage(mstrModuleName, 6, "InActive"))
            cboStatus.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvApproverList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvApproverList.SelectedIndexChanged
        Try
            If lvApproverList.SelectedItems.Count > 0 Then
                If CBool(lvApproverList.SelectedItems(0).SubItems(objcolhIsActive.Index).Text) = True Then
                    btnActive.Visible = False
                    btnInActive.Visible = True
                ElseIf CBool(lvApproverList.SelectedItems(0).SubItems(objcolhIsActive.Index).Text) = False Then
                    btnActive.Visible = True
                    btnInActive.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvApproverList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown Event"

    Private Sub cboLoanLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanLevel.SelectedIndexChanged
        Try

            Dim objApprover As New clsLoanApprover_master
            Dim dtTable As DataTable = Nothing
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CInt(cboLoanLevel.SelectedValue) > 0 Then
            '    dtTable = objApprover.GetList("List", True, False, , , , "lnapproverlevel_master.lnlevelunkid = " & CInt(cboLoanLevel.SelectedValue)).Tables(0)
            'Else
            '    dtTable = objApprover.GetList("List", True, False).Tables(0)
            'End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CInt(cboLoanLevel.SelectedValue) > 0 Then
            '    dtTable = objApprover.GetList(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, _
            '                                  ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                  "List", True, False, _
            '                                  "lnapproverlevel_master.lnlevelunkid = " & CInt(cboLoanLevel.SelectedValue)).Tables(0)
            'Else
            '    dtTable = objApprover.GetList(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, _
            '                                  ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                  "List", True, False).Tables(0)
            'End If

            dtTable = objApprover.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, _
                                          "List", True, False, _
                                      IIf(CInt(cboLoanLevel.SelectedValue) > 0, "lnapproverlevel_master.lnlevelunkid = " & CInt(cboLoanLevel.SelectedValue), "")).Tables(0)

            'Nilay (01-Mar-2016) -- End

            'Nilay (10-Oct-2015) -- End

            Dim dr As DataRow = dtTable.NewRow
            dr("lnapproverunkid") = 0
            dr("name") = Language.getMessage(mstrModuleName, 4, "Select")
            dtTable.Rows.InsertAt(dr, 0)

            cboLoanApprover.DisplayMember = "name"
            cboLoanApprover.ValueMember = "lnapproverunkid"
            cboLoanApprover.DataSource = dtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnActive.GradientBackColor = GUI._ButttonBackColor
            Me.btnActive.GradientForeColor = GUI._ButttonFontColor

            Me.btnInActive.GradientBackColor = GUI._ButttonBackColor
            Me.btnInActive.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLoanLevel.Text = Language._Object.getCaption(Me.lblLoanLevel.Name, Me.lblLoanLevel.Text)
            Me.lblLoanApprover.Text = Language._Object.getCaption(Me.lblLoanApprover.Name, Me.lblLoanApprover.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhLoanApprover.Text = Language._Object.getCaption(CStr(Me.colhLoanApprover.Tag), Me.colhLoanApprover.Text)
            Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.colhMappedUser.Text = Language._Object.getCaption(CStr(Me.colhMappedUser.Tag), Me.colhMappedUser.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnActive.Text = Language._Object.getCaption(Me.btnActive.Name, Me.btnActive.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.btnInActive.Text = Language._Object.getCaption(Me.btnInActive.Name, Me.btnInActive.Text)
            Me.colhIsExternalApprover.Text = Language._Object.getCaption(CStr(Me.colhIsExternalApprover.Tag), Me.colhIsExternalApprover.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Active")
            Language.setMessage(mstrModuleName, 6, "InActive")
            Language.setMessage(mstrModuleName, 7, "Approver active successfully.")
            Language.setMessage(mstrModuleName, 8, "Approver inactive successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class