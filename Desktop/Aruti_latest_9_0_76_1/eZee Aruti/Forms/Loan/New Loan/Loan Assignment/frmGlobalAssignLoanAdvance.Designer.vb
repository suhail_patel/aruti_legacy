﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalAssignLoanAdvance
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalAssignLoanAdvance))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMail = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchScheme = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboLoan = New System.Windows.Forms.ComboBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.lblLoan = New System.Windows.Forms.Label
        Me.cboApprover = New System.Windows.Forms.ComboBox
        Me.lblApprover = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.lblMode = New System.Windows.Forms.Label
        Me.gbBasicInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblMappedHead = New System.Windows.Forms.Label
        Me.cboMappedHead = New System.Windows.Forms.ComboBox
        Me.btnApply = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtGRate = New eZee.TextBox.NumericTextBox
        Me.lblGRate = New System.Windows.Forms.Label
        Me.radApplyToAll = New System.Windows.Forms.RadioButton
        Me.radApplyToChecked = New System.Windows.Forms.RadioButton
        Me.lblInterestCalcType = New System.Windows.Forms.Label
        Me.cboGInterestCalcType = New System.Windows.Forms.ComboBox
        Me.lblGLoanCalcType = New System.Windows.Forms.Label
        Me.cboGLoanCalcType = New System.Windows.Forms.ComboBox
        Me.lblPurpose = New System.Windows.Forms.Label
        Me.txtPrefix = New System.Windows.Forms.TextBox
        Me.txtPurpose = New eZee.TextBox.AlphanumericTextBox
        Me.nudManualNo = New System.Windows.Forms.NumericUpDown
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblVoucherNo = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvDataList = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhAppNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDeductionPeriod = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhEffDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanType = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhInterestType = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhMappedHead = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhRate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNoOfEMI = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInstallmentAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhInstallmentAmtTag = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsEx = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPendingunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApprId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDeductionPeriodId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCalcTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgMatchInstallmentAmt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhInterestAmt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhNetAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIntCalcTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMappedHeadUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMail.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBasicInfo.SuspendLayout()
        CType(Me.nudManualNo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        CType(Me.dgvDataList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMail
        '
        Me.pnlMail.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMail.Controls.Add(Me.gbBasicInfo)
        Me.pnlMail.Controls.Add(Me.EZeeFooter1)
        Me.pnlMail.Controls.Add(Me.dgvDataList)
        Me.pnlMail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMail.Location = New System.Drawing.Point(0, 0)
        Me.pnlMail.Name = "pnlMail"
        Me.pnlMail.Size = New System.Drawing.Size(979, 552)
        Me.pnlMail.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchScheme)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.cboCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoan)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoan)
        Me.gbFilterCriteria.Controls.Add(Me.cboApprover)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprover)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboMode)
        Me.gbFilterCriteria.Controls.Add(Me.lblMode)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(2, 2)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(452, 175)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchScheme
        '
        Me.objbtnSearchScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchScheme.BorderSelected = False
        Me.objbtnSearchScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchScheme.Location = New System.Drawing.Point(354, 88)
        Me.objbtnSearchScheme.Name = "objbtnSearchScheme"
        Me.objbtnSearchScheme.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchScheme.TabIndex = 249
        Me.objbtnSearchScheme.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(354, 61)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 95
        Me.objbtnSearchEmployee.Visible = False
        '
        'lblCurrency
        '
        Me.lblCurrency.BackColor = System.Drawing.Color.Transparent
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(248, 37)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(66, 15)
        Me.lblCurrency.TabIndex = 247
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(320, 34)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(122, 21)
        Me.cboCurrency.TabIndex = 1
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(425, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 23)
        Me.objbtnReset.TabIndex = 4
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(400, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 23)
        Me.objbtnSearch.TabIndex = 3
        Me.objbtnSearch.TabStop = False
        '
        'cboLoan
        '
        Me.cboLoan.DropDownWidth = 200
        Me.cboLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoan.FormattingEnabled = True
        Me.cboLoan.Location = New System.Drawing.Point(90, 88)
        Me.cboLoan.Name = "cboLoan"
        Me.cboLoan.Size = New System.Drawing.Size(352, 21)
        Me.cboLoan.TabIndex = 3
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(280, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(111, 17)
        Me.lnkAllocation.TabIndex = 243
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLoan
        '
        Me.lblLoan.BackColor = System.Drawing.Color.Transparent
        Me.lblLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoan.Location = New System.Drawing.Point(8, 91)
        Me.lblLoan.Name = "lblLoan"
        Me.lblLoan.Size = New System.Drawing.Size(76, 15)
        Me.lblLoan.TabIndex = 2
        Me.lblLoan.Text = "Loan Scheme"
        Me.lblLoan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApprover
        '
        Me.cboApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprover.DropDownWidth = 300
        Me.cboApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprover.FormattingEnabled = True
        Me.cboApprover.Location = New System.Drawing.Point(90, 115)
        Me.cboApprover.Name = "cboApprover"
        Me.cboApprover.Size = New System.Drawing.Size(352, 21)
        Me.cboApprover.TabIndex = 4
        '
        'lblApprover
        '
        Me.lblApprover.BackColor = System.Drawing.Color.Transparent
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 117)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(76, 15)
        Me.lblApprover.TabIndex = 14
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(90, 61)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(352, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 65)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(76, 15)
        Me.lblEmployee.TabIndex = 12
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.DropDownWidth = 200
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(90, 34)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(134, 21)
        Me.cboMode.TabIndex = 0
        '
        'lblMode
        '
        Me.lblMode.BackColor = System.Drawing.Color.Transparent
        Me.lblMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMode.Location = New System.Drawing.Point(8, 37)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(76, 15)
        Me.lblMode.TabIndex = 0
        Me.lblMode.Text = "Mode"
        Me.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBasicInfo
        '
        Me.gbBasicInfo.BorderColor = System.Drawing.Color.Black
        Me.gbBasicInfo.Checked = False
        Me.gbBasicInfo.CollapseAllExceptThis = False
        Me.gbBasicInfo.CollapsedHoverImage = Nothing
        Me.gbBasicInfo.CollapsedNormalImage = Nothing
        Me.gbBasicInfo.CollapsedPressedImage = Nothing
        Me.gbBasicInfo.CollapseOnLoad = False
        Me.gbBasicInfo.Controls.Add(Me.lblMappedHead)
        Me.gbBasicInfo.Controls.Add(Me.cboMappedHead)
        Me.gbBasicInfo.Controls.Add(Me.btnApply)
        Me.gbBasicInfo.Controls.Add(Me.txtGRate)
        Me.gbBasicInfo.Controls.Add(Me.lblGRate)
        Me.gbBasicInfo.Controls.Add(Me.radApplyToAll)
        Me.gbBasicInfo.Controls.Add(Me.radApplyToChecked)
        Me.gbBasicInfo.Controls.Add(Me.lblInterestCalcType)
        Me.gbBasicInfo.Controls.Add(Me.cboGInterestCalcType)
        Me.gbBasicInfo.Controls.Add(Me.lblGLoanCalcType)
        Me.gbBasicInfo.Controls.Add(Me.cboGLoanCalcType)
        Me.gbBasicInfo.Controls.Add(Me.lblPurpose)
        Me.gbBasicInfo.Controls.Add(Me.txtPrefix)
        Me.gbBasicInfo.Controls.Add(Me.txtPurpose)
        Me.gbBasicInfo.Controls.Add(Me.nudManualNo)
        Me.gbBasicInfo.Controls.Add(Me.lblPeriod)
        Me.gbBasicInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbBasicInfo.Controls.Add(Me.lblVoucherNo)
        Me.gbBasicInfo.ExpandedHoverImage = Nothing
        Me.gbBasicInfo.ExpandedNormalImage = Nothing
        Me.gbBasicInfo.ExpandedPressedImage = Nothing
        Me.gbBasicInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicInfo.HeaderHeight = 25
        Me.gbBasicInfo.HeaderMessage = ""
        Me.gbBasicInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbBasicInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicInfo.HeightOnCollapse = 0
        Me.gbBasicInfo.LeftTextSpace = 0
        Me.gbBasicInfo.Location = New System.Drawing.Point(460, 2)
        Me.gbBasicInfo.Name = "gbBasicInfo"
        Me.gbBasicInfo.OpenHeight = 300
        Me.gbBasicInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicInfo.ShowBorder = True
        Me.gbBasicInfo.ShowCheckBox = False
        Me.gbBasicInfo.ShowCollapseButton = False
        Me.gbBasicInfo.ShowDefaultBorderColor = True
        Me.gbBasicInfo.ShowDownButton = False
        Me.gbBasicInfo.ShowHeader = True
        Me.gbBasicInfo.Size = New System.Drawing.Size(517, 175)
        Me.gbBasicInfo.TabIndex = 1
        Me.gbBasicInfo.Temp = 0
        Me.gbBasicInfo.Text = "Basic Information"
        Me.gbBasicInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMappedHead
        '
        Me.lblMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMappedHead.Location = New System.Drawing.Point(6, 118)
        Me.lblMappedHead.Name = "lblMappedHead"
        Me.lblMappedHead.Size = New System.Drawing.Size(102, 15)
        Me.lblMappedHead.TabIndex = 25
        Me.lblMappedHead.Text = "Mapped Head"
        Me.lblMappedHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMappedHead
        '
        Me.cboMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMappedHead.FormattingEnabled = True
        Me.cboMappedHead.Location = New System.Drawing.Point(114, 115)
        Me.cboMappedHead.Name = "cboMappedHead"
        Me.cboMappedHead.Size = New System.Drawing.Size(158, 21)
        Me.cboMappedHead.TabIndex = 24
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.BackColor = System.Drawing.Color.White
        Me.btnApply.BackgroundImage = CType(resources.GetObject("btnApply.BackgroundImage"), System.Drawing.Image)
        Me.btnApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApply.BorderColor = System.Drawing.Color.Empty
        Me.btnApply.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApply.FlatAppearance.BorderSize = 0
        Me.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApply.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApply.ForeColor = System.Drawing.Color.Black
        Me.btnApply.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApply.GradientForeColor = System.Drawing.Color.Black
        Me.btnApply.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Location = New System.Drawing.Point(418, 135)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Size = New System.Drawing.Size(89, 30)
        Me.btnApply.TabIndex = 9
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'txtGRate
        '
        Me.txtGRate.AllowNegative = True
        Me.txtGRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtGRate.DigitsInGroup = 0
        Me.txtGRate.Flags = 0
        Me.txtGRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGRate.Location = New System.Drawing.Point(345, 115)
        Me.txtGRate.MaxDecimalPlaces = 6
        Me.txtGRate.MaxWholeDigits = 21
        Me.txtGRate.Name = "txtGRate"
        Me.txtGRate.Prefix = ""
        Me.txtGRate.RangeMax = 1.7976931348623157E+308
        Me.txtGRate.RangeMin = -1.7976931348623157E+308
        Me.txtGRate.Size = New System.Drawing.Size(58, 21)
        Me.txtGRate.TabIndex = 3
        Me.txtGRate.Text = "0"
        Me.txtGRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblGRate
        '
        Me.lblGRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGRate.Location = New System.Drawing.Point(281, 118)
        Me.lblGRate.Name = "lblGRate"
        Me.lblGRate.Size = New System.Drawing.Size(58, 15)
        Me.lblGRate.TabIndex = 22
        Me.lblGRate.Text = "Rate %"
        '
        'radApplyToAll
        '
        Me.radApplyToAll.BackColor = System.Drawing.Color.Transparent
        Me.radApplyToAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplyToAll.Location = New System.Drawing.Point(307, 142)
        Me.radApplyToAll.Name = "radApplyToAll"
        Me.radApplyToAll.Size = New System.Drawing.Size(93, 17)
        Me.radApplyToAll.TabIndex = 5
        Me.radApplyToAll.TabStop = True
        Me.radApplyToAll.Text = "Apply To All"
        Me.radApplyToAll.UseVisualStyleBackColor = False
        '
        'radApplyToChecked
        '
        Me.radApplyToChecked.BackColor = System.Drawing.Color.Transparent
        Me.radApplyToChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplyToChecked.Location = New System.Drawing.Point(184, 142)
        Me.radApplyToChecked.Name = "radApplyToChecked"
        Me.radApplyToChecked.Size = New System.Drawing.Size(116, 17)
        Me.radApplyToChecked.TabIndex = 4
        Me.radApplyToChecked.TabStop = True
        Me.radApplyToChecked.Text = "Apply To Checked"
        Me.radApplyToChecked.UseVisualStyleBackColor = False
        '
        'lblInterestCalcType
        '
        Me.lblInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestCalcType.Location = New System.Drawing.Point(6, 91)
        Me.lblInterestCalcType.Name = "lblInterestCalcType"
        Me.lblInterestCalcType.Size = New System.Drawing.Size(102, 15)
        Me.lblInterestCalcType.TabIndex = 18
        Me.lblInterestCalcType.Text = "Interest Calculation"
        Me.lblInterestCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGInterestCalcType
        '
        Me.cboGInterestCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGInterestCalcType.FormattingEnabled = True
        Me.cboGInterestCalcType.Location = New System.Drawing.Point(114, 88)
        Me.cboGInterestCalcType.Name = "cboGInterestCalcType"
        Me.cboGInterestCalcType.Size = New System.Drawing.Size(158, 21)
        Me.cboGInterestCalcType.TabIndex = 2
        '
        'lblGLoanCalcType
        '
        Me.lblGLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGLoanCalcType.Location = New System.Drawing.Point(6, 65)
        Me.lblGLoanCalcType.Name = "lblGLoanCalcType"
        Me.lblGLoanCalcType.Size = New System.Drawing.Size(102, 15)
        Me.lblGLoanCalcType.TabIndex = 16
        Me.lblGLoanCalcType.Text = "Loan Calculation"
        Me.lblGLoanCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGLoanCalcType
        '
        Me.cboGLoanCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGLoanCalcType.FormattingEnabled = True
        Me.cboGLoanCalcType.Location = New System.Drawing.Point(114, 61)
        Me.cboGLoanCalcType.Name = "cboGLoanCalcType"
        Me.cboGLoanCalcType.Size = New System.Drawing.Size(158, 21)
        Me.cboGLoanCalcType.TabIndex = 1
        '
        'lblPurpose
        '
        Me.lblPurpose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPurpose.Location = New System.Drawing.Point(278, 65)
        Me.lblPurpose.Name = "lblPurpose"
        Me.lblPurpose.Size = New System.Drawing.Size(61, 15)
        Me.lblPurpose.TabIndex = 8
        Me.lblPurpose.Text = "Purpose"
        '
        'txtPrefix
        '
        Me.txtPrefix.Enabled = False
        Me.txtPrefix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrefix.Location = New System.Drawing.Point(345, 34)
        Me.txtPrefix.MaxLength = 6
        Me.txtPrefix.Name = "txtPrefix"
        Me.txtPrefix.ReadOnly = True
        Me.txtPrefix.Size = New System.Drawing.Size(77, 21)
        Me.txtPrefix.TabIndex = 7
        Me.txtPrefix.Text = "ISGLA_"
        Me.txtPrefix.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPurpose
        '
        Me.txtPurpose.Flags = 0
        Me.txtPurpose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPurpose.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPurpose.Location = New System.Drawing.Point(345, 64)
        Me.txtPurpose.Multiline = True
        Me.txtPurpose.Name = "txtPurpose"
        Me.txtPurpose.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPurpose.Size = New System.Drawing.Size(162, 45)
        Me.txtPurpose.TabIndex = 6
        '
        'nudManualNo
        '
        Me.nudManualNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudManualNo.Location = New System.Drawing.Point(428, 34)
        Me.nudManualNo.Maximum = New Decimal(New Integer() {9999999, 0, 0, 0})
        Me.nudManualNo.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudManualNo.Name = "nudManualNo"
        Me.nudManualNo.Size = New System.Drawing.Size(79, 21)
        Me.nudManualNo.TabIndex = 8
        Me.nudManualNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudManualNo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(6, 37)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(102, 15)
        Me.lblPeriod.TabIndex = 4
        Me.lblPeriod.Text = "Assigned Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(114, 34)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(158, 21)
        Me.cboPayPeriod.TabIndex = 0
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucherNo.Location = New System.Drawing.Point(278, 37)
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.Size = New System.Drawing.Size(61, 15)
        Me.lblVoucherNo.TabIndex = 12
        Me.lblVoucherNo.Text = "Voc #"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnAssign)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 502)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(979, 50)
        Me.EZeeFooter1.TabIndex = 2
        '
        'btnAssign
        '
        Me.btnAssign.BackColor = System.Drawing.Color.White
        Me.btnAssign.BackgroundImage = CType(resources.GetObject("btnAssign.BackgroundImage"), System.Drawing.Image)
        Me.btnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAssign.BorderColor = System.Drawing.Color.Empty
        Me.btnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAssign.FlatAppearance.BorderSize = 0
        Me.btnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAssign.ForeColor = System.Drawing.Color.Black
        Me.btnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Location = New System.Drawing.Point(759, 11)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Size = New System.Drawing.Size(101, 30)
        Me.btnAssign.TabIndex = 0
        Me.btnAssign.Text = "&Assign"
        Me.btnAssign.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(866, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(101, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvDataList
        '
        Me.dgvDataList.AllowUserToAddRows = False
        Me.dgvDataList.AllowUserToDeleteRows = False
        Me.dgvDataList.AllowUserToResizeRows = False
        Me.dgvDataList.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvDataList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDataList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDataList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhSelect, Me.dgcolhAppNo, Me.dgcolhEmployee, Me.dgcolhApprover, Me.dgcolhAmount, Me.dgcolhDeductionPeriod, Me.dgcolhEffDate, Me.dgcolhLoanType, Me.dgcolhInterestType, Me.dgcolhMappedHead, Me.dgcolhRate, Me.dgcolhNoOfEMI, Me.dgcolhInstallmentAmount, Me.objdgcolhInstallmentAmtTag, Me.objdgcolhIsGrp, Me.objdgcolhIsEx, Me.objdgcolhPendingunkid, Me.objdgcolhGrpId, Me.objdgcolhApprId, Me.objdgcolhEmpId, Me.objdgcolhDeductionPeriodId, Me.objdgcolhCalcTypeId, Me.objdgMatchInstallmentAmt, Me.objdgcolhInterestAmt, Me.objdgcolhNetAmount, Me.objdgcolhIntCalcTypeId, Me.objdgcolhMappedHeadUnkId})
        Me.dgvDataList.Location = New System.Drawing.Point(0, 183)
        Me.dgvDataList.Name = "dgvDataList"
        Me.dgvDataList.RowHeadersVisible = False
        Me.dgvDataList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDataList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDataList.Size = New System.Drawing.Size(975, 318)
        Me.dgvDataList.TabIndex = 1
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.Frozen = True
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.Frozen = True
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhSelect.Width = 25
        '
        'dgcolhAppNo
        '
        Me.dgcolhAppNo.Frozen = True
        Me.dgcolhAppNo.HeaderText = "App. Voc #"
        Me.dgcolhAppNo.Name = "dgcolhAppNo"
        Me.dgcolhAppNo.ReadOnly = True
        Me.dgcolhAppNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAppNo.Width = 80
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.Frozen = True
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee.Width = 180
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.Frozen = True
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        Me.dgcolhApprover.ReadOnly = True
        Me.dgcolhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprover.Width = 110
        '
        'dgcolhAmount
        '
        Me.dgcolhAmount.Frozen = True
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAmount.Width = 110
        '
        'dgcolhDeductionPeriod
        '
        Me.dgcolhDeductionPeriod.HeaderText = "Deduction Period"
        Me.dgcolhDeductionPeriod.Name = "dgcolhDeductionPeriod"
        Me.dgcolhDeductionPeriod.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'dgcolhEffDate
        '
        Me.dgcolhEffDate.HeaderText = "Effective Date"
        Me.dgcolhEffDate.Name = "dgcolhEffDate"
        Me.dgcolhEffDate.ReadOnly = True
        Me.dgcolhEffDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEffDate.Width = 85
        '
        'dgcolhLoanType
        '
        Me.dgcolhLoanType.DropDownWidth = 200
        Me.dgcolhLoanType.HeaderText = "Loan Calculation"
        Me.dgcolhLoanType.Name = "dgcolhLoanType"
        '
        'dgcolhInterestType
        '
        Me.dgcolhInterestType.HeaderText = "Interest Calc."
        Me.dgcolhInterestType.Name = "dgcolhInterestType"
        '
        'dgcolhMappedHead
        '
        Me.dgcolhMappedHead.HeaderText = "Mapped Head"
        Me.dgcolhMappedHead.Name = "dgcolhMappedHead"
        Me.dgcolhMappedHead.ReadOnly = True
        Me.dgcolhMappedHead.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhMappedHead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgcolhRate
        '
        DataGridViewCellStyle1.Format = "F2"
        Me.dgcolhRate.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhRate.HeaderText = "Rate %"
        Me.dgcolhRate.Name = "dgcolhRate"
        Me.dgcolhRate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRate.Width = 50
        '
        'dgcolhNoOfEMI
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F0"
        Me.dgcolhNoOfEMI.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhNoOfEMI.HeaderText = "No. Of Instl."
        Me.dgcolhNoOfEMI.Name = "dgcolhNoOfEMI"
        Me.dgcolhNoOfEMI.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhNoOfEMI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhNoOfEMI.Width = 75
        '
        'dgcolhInstallmentAmount
        '
        Me.dgcolhInstallmentAmount.HeaderText = "Installment Amount"
        Me.dgcolhInstallmentAmount.Name = "dgcolhInstallmentAmount"
        Me.dgcolhInstallmentAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhInstallmentAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInstallmentAmount.Width = 110
        '
        'objdgcolhInstallmentAmtTag
        '
        Me.objdgcolhInstallmentAmtTag.HeaderText = "objdgcolhInstallmentAmtTag"
        Me.objdgcolhInstallmentAmtTag.Name = "objdgcolhInstallmentAmtTag"
        Me.objdgcolhInstallmentAmtTag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhInstallmentAmtTag.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhIsEx
        '
        Me.objdgcolhIsEx.HeaderText = "objdgcolhIsEx"
        Me.objdgcolhIsEx.Name = "objdgcolhIsEx"
        Me.objdgcolhIsEx.ReadOnly = True
        Me.objdgcolhIsEx.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIsEx.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsEx.Visible = False
        '
        'objdgcolhPendingunkid
        '
        Me.objdgcolhPendingunkid.HeaderText = "objdgcolhPendingunkid"
        Me.objdgcolhPendingunkid.Name = "objdgcolhPendingunkid"
        Me.objdgcolhPendingunkid.ReadOnly = True
        Me.objdgcolhPendingunkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhPendingunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhPendingunkid.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhGrpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhApprId
        '
        Me.objdgcolhApprId.HeaderText = "objdgcolhApprId"
        Me.objdgcolhApprId.Name = "objdgcolhApprId"
        Me.objdgcolhApprId.ReadOnly = True
        Me.objdgcolhApprId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhApprId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApprId.Visible = False
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.ReadOnly = True
        Me.objdgcolhEmpId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEmpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpId.Visible = False
        '
        'objdgcolhDeductionPeriodId
        '
        Me.objdgcolhDeductionPeriodId.HeaderText = "objdgcolhDeductionPeriodId"
        Me.objdgcolhDeductionPeriodId.Name = "objdgcolhDeductionPeriodId"
        Me.objdgcolhDeductionPeriodId.ReadOnly = True
        Me.objdgcolhDeductionPeriodId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhDeductionPeriodId.Visible = False
        '
        'objdgcolhCalcTypeId
        '
        Me.objdgcolhCalcTypeId.HeaderText = "objdgcolhCalcTypeId"
        Me.objdgcolhCalcTypeId.Name = "objdgcolhCalcTypeId"
        Me.objdgcolhCalcTypeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCalcTypeId.Visible = False
        '
        'objdgMatchInstallmentAmt
        '
        Me.objdgMatchInstallmentAmt.HeaderText = "objdgMatchInstallmentAmt"
        Me.objdgMatchInstallmentAmt.Name = "objdgMatchInstallmentAmt"
        Me.objdgMatchInstallmentAmt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgMatchInstallmentAmt.Visible = False
        '
        'objdgcolhInterestAmt
        '
        Me.objdgcolhInterestAmt.HeaderText = "objdgcolhInterestAmt"
        Me.objdgcolhInterestAmt.Name = "objdgcolhInterestAmt"
        Me.objdgcolhInterestAmt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhInterestAmt.Visible = False
        '
        'objdgcolhNetAmount
        '
        Me.objdgcolhNetAmount.HeaderText = "objdgcolhNetAmount"
        Me.objdgcolhNetAmount.Name = "objdgcolhNetAmount"
        Me.objdgcolhNetAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhNetAmount.Visible = False
        '
        'objdgcolhIntCalcTypeId
        '
        Me.objdgcolhIntCalcTypeId.HeaderText = "objdgcolhIntCalcTypeId"
        Me.objdgcolhIntCalcTypeId.Name = "objdgcolhIntCalcTypeId"
        Me.objdgcolhIntCalcTypeId.ReadOnly = True
        Me.objdgcolhIntCalcTypeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIntCalcTypeId.Visible = False
        '
        'objdgcolhMappedHeadUnkId
        '
        Me.objdgcolhMappedHeadUnkId.HeaderText = "objdgcolhMappedHeadUnkId"
        Me.objdgcolhMappedHeadUnkId.Name = "objdgcolhMappedHeadUnkId"
        Me.objdgcolhMappedHeadUnkId.ReadOnly = True
        Me.objdgcolhMappedHeadUnkId.Visible = False
        '
        'frmGlobalAssignLoanAdvance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(979, 552)
        Me.Controls.Add(Me.pnlMail)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalAssignLoanAdvance"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Assigned Loan / Advance"
        Me.pnlMail.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBasicInfo.ResumeLayout(False)
        Me.gbBasicInfo.PerformLayout()
        CType(Me.nudManualNo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        CType(Me.dgvDataList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMail As System.Windows.Forms.Panel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboLoan As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoan As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblMode As System.Windows.Forms.Label
    Friend WithEvents cboApprover As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents dgvDataList As System.Windows.Forms.DataGridView
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbBasicInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPurpose As System.Windows.Forms.Label
    Friend WithEvents txtPurpose As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVoucherNo As System.Windows.Forms.Label
    Friend WithEvents nudManualNo As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtPrefix As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents lblInterestCalcType As System.Windows.Forms.Label
    Friend WithEvents cboGInterestCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblGLoanCalcType As System.Windows.Forms.Label
    Friend WithEvents cboGLoanCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents radApplyToChecked As System.Windows.Forms.RadioButton
    Friend WithEvents radApplyToAll As System.Windows.Forms.RadioButton
    Friend WithEvents lblGRate As System.Windows.Forms.Label
    Friend WithEvents txtGRate As eZee.TextBox.NumericTextBox
    Friend WithEvents btnApply As eZee.Common.eZeeLightButton
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhAppNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDeductionPeriod As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhEffDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanType As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhInterestType As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhMappedHead As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhRate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNoOfEMI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInstallmentAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhInstallmentAmtTag As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsEx As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPendingunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApprId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDeductionPeriodId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCalcTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgMatchInstallmentAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhInterestAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhNetAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIntCalcTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMappedHeadUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblMappedHead As System.Windows.Forms.Label
    Friend WithEvents cboMappedHead As System.Windows.Forms.ComboBox
End Class
