﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanParameter
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanParameter))
        Me.pnlLoanParameters = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblEndDate = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlradOpr = New System.Windows.Forms.Panel
        Me.radInterestRate = New System.Windows.Forms.RadioButton
        Me.radInstallment = New System.Windows.Forms.RadioButton
        Me.radTopup = New System.Windows.Forms.RadioButton
        Me.pnlControls = New System.Windows.Forms.Panel
        Me.lblInterestAmt = New System.Windows.Forms.Label
        Me.lblPrincipalAmt = New System.Windows.Forms.Label
        Me.txtInterestAmt = New eZee.TextBox.NumericTextBox
        Me.txtPrincipalAmt = New eZee.TextBox.NumericTextBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.objbtnPeriodSearch = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.txtInstallmentAmt = New eZee.TextBox.NumericTextBox
        Me.lblEMIAmount = New System.Windows.Forms.Label
        Me.lblDuration = New System.Windows.Forms.Label
        Me.txtLoanInterest = New eZee.TextBox.NumericTextBox
        Me.lblTopupAmt = New System.Windows.Forms.Label
        Me.nudDuration = New System.Windows.Forms.NumericUpDown
        Me.txtTopupAmt = New eZee.TextBox.NumericTextBox
        Me.lblLoanInterest = New System.Windows.Forms.Label
        Me.elOperation = New eZee.Common.eZeeLine
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.pnlStatus = New System.Windows.Forms.Panel
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.pnlLoanParameters.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlradOpr.SuspendLayout()
        Me.pnlControls.SuspendLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStatus.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlLoanParameters
        '
        Me.pnlLoanParameters.Controls.Add(Me.objFooter)
        Me.pnlLoanParameters.Controls.Add(Me.pnlradOpr)
        Me.pnlLoanParameters.Controls.Add(Me.pnlControls)
        Me.pnlLoanParameters.Controls.Add(Me.elOperation)
        Me.pnlLoanParameters.Controls.Add(Me.objLine1)
        Me.pnlLoanParameters.Controls.Add(Me.pnlStatus)
        Me.pnlLoanParameters.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLoanParameters.Location = New System.Drawing.Point(0, 0)
        Me.pnlLoanParameters.Name = "pnlLoanParameters"
        Me.pnlLoanParameters.Size = New System.Drawing.Size(392, 340)
        Me.pnlLoanParameters.TabIndex = 1
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblEndDate)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 290)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(392, 50)
        Me.objFooter.TabIndex = 13
        '
        'objlblEndDate
        '
        Me.objlblEndDate.Location = New System.Drawing.Point(10, 18)
        Me.objlblEndDate.Name = "objlblEndDate"
        Me.objlblEndDate.Size = New System.Drawing.Size(164, 13)
        Me.objlblEndDate.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(180, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.CausesValidation = False
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(283, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlradOpr
        '
        Me.pnlradOpr.Controls.Add(Me.radInterestRate)
        Me.pnlradOpr.Controls.Add(Me.radInstallment)
        Me.pnlradOpr.Controls.Add(Me.radTopup)
        Me.pnlradOpr.Location = New System.Drawing.Point(13, 25)
        Me.pnlradOpr.Name = "pnlradOpr"
        Me.pnlradOpr.Size = New System.Drawing.Size(367, 23)
        Me.pnlradOpr.TabIndex = 351
        '
        'radInterestRate
        '
        Me.radInterestRate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radInterestRate.Location = New System.Drawing.Point(13, 3)
        Me.radInterestRate.Name = "radInterestRate"
        Me.radInterestRate.Size = New System.Drawing.Size(97, 17)
        Me.radInterestRate.TabIndex = 348
        Me.radInterestRate.TabStop = True
        Me.radInterestRate.Text = "Loan Rate(%)"
        Me.radInterestRate.UseVisualStyleBackColor = True
        '
        'radInstallment
        '
        Me.radInstallment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radInstallment.Location = New System.Drawing.Point(116, 3)
        Me.radInstallment.Name = "radInstallment"
        Me.radInstallment.Size = New System.Drawing.Size(137, 17)
        Me.radInstallment.TabIndex = 348
        Me.radInstallment.TabStop = True
        Me.radInstallment.Text = "Loan INSTL. Amt./No."
        Me.radInstallment.UseVisualStyleBackColor = True
        '
        'radTopup
        '
        Me.radTopup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radTopup.Location = New System.Drawing.Point(259, 3)
        Me.radTopup.Name = "radTopup"
        Me.radTopup.Size = New System.Drawing.Size(103, 17)
        Me.radTopup.TabIndex = 348
        Me.radTopup.TabStop = True
        Me.radTopup.Text = "Loan Topup"
        Me.radTopup.UseVisualStyleBackColor = True
        '
        'pnlControls
        '
        Me.pnlControls.Controls.Add(Me.lblInterestAmt)
        Me.pnlControls.Controls.Add(Me.lblPrincipalAmt)
        Me.pnlControls.Controls.Add(Me.txtInterestAmt)
        Me.pnlControls.Controls.Add(Me.txtPrincipalAmt)
        Me.pnlControls.Controls.Add(Me.cboPeriod)
        Me.pnlControls.Controls.Add(Me.objlblExRate)
        Me.pnlControls.Controls.Add(Me.objbtnPeriodSearch)
        Me.pnlControls.Controls.Add(Me.lblPeriod)
        Me.pnlControls.Controls.Add(Me.dtpEffectiveDate)
        Me.pnlControls.Controls.Add(Me.lblEffectiveDate)
        Me.pnlControls.Controls.Add(Me.txtInstallmentAmt)
        Me.pnlControls.Controls.Add(Me.lblEMIAmount)
        Me.pnlControls.Controls.Add(Me.lblDuration)
        Me.pnlControls.Controls.Add(Me.txtLoanInterest)
        Me.pnlControls.Controls.Add(Me.lblTopupAmt)
        Me.pnlControls.Controls.Add(Me.nudDuration)
        Me.pnlControls.Controls.Add(Me.txtTopupAmt)
        Me.pnlControls.Controls.Add(Me.lblLoanInterest)
        Me.pnlControls.Location = New System.Drawing.Point(2, 68)
        Me.pnlControls.Name = "pnlControls"
        Me.pnlControls.Size = New System.Drawing.Size(388, 138)
        Me.pnlControls.TabIndex = 350
        '
        'lblInterestAmt
        '
        Me.lblInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestAmt.Location = New System.Drawing.Point(211, 85)
        Me.lblInterestAmt.Name = "lblInterestAmt"
        Me.lblInterestAmt.Size = New System.Drawing.Size(80, 16)
        Me.lblInterestAmt.TabIndex = 353
        Me.lblInterestAmt.Text = "Interest Amt."
        Me.lblInterestAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPrincipalAmt
        '
        Me.lblPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrincipalAmt.Location = New System.Drawing.Point(10, 85)
        Me.lblPrincipalAmt.Name = "lblPrincipalAmt"
        Me.lblPrincipalAmt.Size = New System.Drawing.Size(88, 16)
        Me.lblPrincipalAmt.TabIndex = 352
        Me.lblPrincipalAmt.Text = "Principal Amt."
        Me.lblPrincipalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInterestAmt
        '
        Me.txtInterestAmt.AllowNegative = True
        Me.txtInterestAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtInterestAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInterestAmt.DigitsInGroup = 0
        Me.txtInterestAmt.Flags = 0
        Me.txtInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterestAmt.Location = New System.Drawing.Point(294, 83)
        Me.txtInterestAmt.MaxDecimalPlaces = 6
        Me.txtInterestAmt.MaxWholeDigits = 21
        Me.txtInterestAmt.Name = "txtInterestAmt"
        Me.txtInterestAmt.Prefix = ""
        Me.txtInterestAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInterestAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInterestAmt.ReadOnly = True
        Me.txtInterestAmt.Size = New System.Drawing.Size(60, 21)
        Me.txtInterestAmt.TabIndex = 351
        Me.txtInterestAmt.Text = "0"
        Me.txtInterestAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipalAmt
        '
        Me.txtPrincipalAmt.AllowNegative = True
        Me.txtPrincipalAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtPrincipalAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPrincipalAmt.DigitsInGroup = 0
        Me.txtPrincipalAmt.Flags = 0
        Me.txtPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrincipalAmt.Location = New System.Drawing.Point(102, 84)
        Me.txtPrincipalAmt.MaxDecimalPlaces = 6
        Me.txtPrincipalAmt.MaxWholeDigits = 21
        Me.txtPrincipalAmt.Name = "txtPrincipalAmt"
        Me.txtPrincipalAmt.Prefix = ""
        Me.txtPrincipalAmt.RangeMax = 1.7976931348623157E+308
        Me.txtPrincipalAmt.RangeMin = -1.7976931348623157E+308
        Me.txtPrincipalAmt.ReadOnly = True
        Me.txtPrincipalAmt.Size = New System.Drawing.Size(102, 21)
        Me.txtPrincipalAmt.TabIndex = 350
        Me.txtPrincipalAmt.Text = "0"
        Me.txtPrincipalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(102, 2)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(252, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(194, 31)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(179, 15)
        Me.objlblExRate.TabIndex = 349
        '
        'objbtnPeriodSearch
        '
        Me.objbtnPeriodSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnPeriodSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnPeriodSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnPeriodSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnPeriodSearch.BorderSelected = False
        Me.objbtnPeriodSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnPeriodSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnPeriodSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnPeriodSearch.Location = New System.Drawing.Point(360, 2)
        Me.objbtnPeriodSearch.Name = "objbtnPeriodSearch"
        Me.objbtnPeriodSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnPeriodSearch.TabIndex = 2
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 4)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(88, 16)
        Me.lblPeriod.TabIndex = 0
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(102, 29)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpEffectiveDate.TabIndex = 4
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(8, 32)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(88, 16)
        Me.lblEffectiveDate.TabIndex = 3
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'txtInstallmentAmt
        '
        Me.txtInstallmentAmt.AllowNegative = True
        Me.txtInstallmentAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtInstallmentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentAmt.DigitsInGroup = 0
        Me.txtInstallmentAmt.Flags = 0
        Me.txtInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentAmt.Location = New System.Drawing.Point(102, 111)
        Me.txtInstallmentAmt.MaxDecimalPlaces = 6
        Me.txtInstallmentAmt.MaxWholeDigits = 21
        Me.txtInstallmentAmt.Name = "txtInstallmentAmt"
        Me.txtInstallmentAmt.Prefix = ""
        Me.txtInstallmentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentAmt.ReadOnly = True
        Me.txtInstallmentAmt.Size = New System.Drawing.Size(252, 21)
        Me.txtInstallmentAmt.TabIndex = 12
        Me.txtInstallmentAmt.Text = "0"
        Me.txtInstallmentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEMIAmount
        '
        Me.lblEMIAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIAmount.Location = New System.Drawing.Point(10, 113)
        Me.lblEMIAmount.Name = "lblEMIAmount"
        Me.lblEMIAmount.Size = New System.Drawing.Size(88, 16)
        Me.lblEMIAmount.TabIndex = 11
        Me.lblEMIAmount.Text = "Installment Amt."
        Me.lblEMIAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDuration
        '
        Me.lblDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDuration.Location = New System.Drawing.Point(169, 57)
        Me.lblDuration.Name = "lblDuration"
        Me.lblDuration.Size = New System.Drawing.Size(122, 16)
        Me.lblDuration.TabIndex = 7
        Me.lblDuration.Text = "INSTL. No. (In Months)"
        Me.lblDuration.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtLoanInterest
        '
        Me.txtLoanInterest.AllowNegative = True
        Me.txtLoanInterest.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanInterest.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanInterest.DigitsInGroup = 0
        Me.txtLoanInterest.Flags = 0
        Me.txtLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanInterest.Location = New System.Drawing.Point(102, 56)
        Me.txtLoanInterest.MaxDecimalPlaces = 6
        Me.txtLoanInterest.MaxWholeDigits = 21
        Me.txtLoanInterest.Name = "txtLoanInterest"
        Me.txtLoanInterest.Prefix = ""
        Me.txtLoanInterest.RangeMax = 1.7976931348623157E+308
        Me.txtLoanInterest.RangeMin = -1.7976931348623157E+308
        Me.txtLoanInterest.Size = New System.Drawing.Size(47, 21)
        Me.txtLoanInterest.TabIndex = 6
        Me.txtLoanInterest.Text = "0"
        Me.txtLoanInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTopupAmt
        '
        Me.lblTopupAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTopupAmt.Location = New System.Drawing.Point(10, 111)
        Me.lblTopupAmt.Name = "lblTopupAmt"
        Me.lblTopupAmt.Size = New System.Drawing.Size(88, 16)
        Me.lblTopupAmt.TabIndex = 345
        Me.lblTopupAmt.Text = "Topup Amount"
        Me.lblTopupAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDuration
        '
        Me.nudDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDuration.Location = New System.Drawing.Point(294, 56)
        Me.nudDuration.Maximum = New Decimal(New Integer() {9999999, 0, 0, 0})
        Me.nudDuration.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudDuration.Name = "nudDuration"
        Me.nudDuration.Size = New System.Drawing.Size(60, 21)
        Me.nudDuration.TabIndex = 8
        Me.nudDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudDuration.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtTopupAmt
        '
        Me.txtTopupAmt.AllowNegative = True
        Me.txtTopupAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtTopupAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTopupAmt.DigitsInGroup = 0
        Me.txtTopupAmt.Flags = 0
        Me.txtTopupAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTopupAmt.Location = New System.Drawing.Point(102, 111)
        Me.txtTopupAmt.MaxDecimalPlaces = 6
        Me.txtTopupAmt.MaxWholeDigits = 21
        Me.txtTopupAmt.Name = "txtTopupAmt"
        Me.txtTopupAmt.Prefix = ""
        Me.txtTopupAmt.RangeMax = 1.7976931348623157E+308
        Me.txtTopupAmt.RangeMin = -1.7976931348623157E+308
        Me.txtTopupAmt.Size = New System.Drawing.Size(252, 21)
        Me.txtTopupAmt.TabIndex = 344
        Me.txtTopupAmt.Text = "0"
        Me.txtTopupAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLoanInterest
        '
        Me.lblLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterest.Location = New System.Drawing.Point(8, 58)
        Me.lblLoanInterest.Name = "lblLoanInterest"
        Me.lblLoanInterest.Size = New System.Drawing.Size(88, 16)
        Me.lblLoanInterest.TabIndex = 5
        Me.lblLoanInterest.Text = "Interest (%)"
        Me.lblLoanInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elOperation
        '
        Me.elOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elOperation.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elOperation.Location = New System.Drawing.Point(12, 9)
        Me.elOperation.Name = "elOperation"
        Me.elOperation.Size = New System.Drawing.Size(368, 13)
        Me.elOperation.TabIndex = 347
        Me.elOperation.Text = "Operation Type"
        Me.elOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(10, 50)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(368, 15)
        Me.objLine1.TabIndex = 346
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStatus
        '
        Me.pnlStatus.Controls.Add(Me.txtRemark)
        Me.pnlStatus.Controls.Add(Me.lblRemark)
        Me.pnlStatus.Controls.Add(Me.cboStatus)
        Me.pnlStatus.Controls.Add(Me.lblStatus)
        Me.pnlStatus.Location = New System.Drawing.Point(2, 206)
        Me.pnlStatus.Name = "pnlStatus"
        Me.pnlStatus.Size = New System.Drawing.Size(386, 83)
        Me.pnlStatus.TabIndex = 353
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtRemark.Location = New System.Drawing.Point(102, 30)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemark.Size = New System.Drawing.Size(252, 47)
        Me.txtRemark.TabIndex = 32
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(10, 32)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(83, 16)
        Me.lblRemark.TabIndex = 6
        Me.lblRemark.Text = "Remark"
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(102, 3)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(252, 21)
        Me.cboStatus.TabIndex = 5
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(10, 6)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(88, 16)
        Me.lblStatus.TabIndex = 4
        Me.lblStatus.Text = "Status"
        '
        'frmLoanParameter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(392, 340)
        Me.Controls.Add(Me.pnlLoanParameters)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanParameter"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan Parameter"
        Me.pnlLoanParameters.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.pnlradOpr.ResumeLayout(False)
        Me.pnlControls.ResumeLayout(False)
        Me.pnlControls.PerformLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStatus.ResumeLayout(False)
        Me.pnlStatus.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlLoanParameters As System.Windows.Forms.Panel
    Friend WithEvents lblDuration As System.Windows.Forms.Label
    Friend WithEvents nudDuration As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtLoanInterest As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLoanInterest As System.Windows.Forms.Label
    Friend WithEvents txtInstallmentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIAmount As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnPeriodSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTopupAmt As System.Windows.Forms.Label
    Friend WithEvents txtTopupAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents elOperation As eZee.Common.eZeeLine
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents radTopup As System.Windows.Forms.RadioButton
    Friend WithEvents radInstallment As System.Windows.Forms.RadioButton
    Friend WithEvents radInterestRate As System.Windows.Forms.RadioButton
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents pnlControls As System.Windows.Forms.Panel
    Friend WithEvents pnlradOpr As System.Windows.Forms.Panel
    Friend WithEvents objlblEndDate As System.Windows.Forms.Label
    Friend WithEvents txtInterestAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtPrincipalAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPrincipalAmt As System.Windows.Forms.Label
    Friend WithEvents lblInterestAmt As System.Windows.Forms.Label
    Friend WithEvents pnlStatus As System.Windows.Forms.Panel
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
End Class
