﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewLoanAdvanceList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewLoanAdvanceList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.dgvLoanAdvanceList = New System.Windows.Forms.DataGridView
        Me.objEFooter = New eZee.Common.eZeeFooter
        Me.btnEmailClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmailOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboCalcType = New System.Windows.Forms.ComboBox
        Me.lblCalcType = New System.Windows.Forms.Label
        Me.objLoanSchemeSearch = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboOtherOpStatus = New System.Windows.Forms.ComboBox
        Me.lblOtherOpStatus = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.cboCondition = New System.Windows.Forms.ComboBox
        Me.txtVocNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.cboLoanAdvance = New System.Windows.Forms.ComboBox
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblVocNo = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.cboAssignPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblCurrOpenPeriod = New System.Windows.Forms.Label
        Me.btnPreview = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPayment = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReceived = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuViewApprovalForm = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuChangeStatus = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuLoanOperation = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep3 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuGlobalChangeStatus = New System.Windows.Forms.ToolStripMenuItem
        Me.btnChangeStatus = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhVocNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDeductionPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanScheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanAdvance = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanCalcType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInterestCalcType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCurrency = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInstallments = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBalance = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPaidLoan = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOtherOpApproval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsLoan = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhstatusunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhemployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLoanAdvanceunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhVisibleStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhhisbrought_forward = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhOpStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProcessPendingUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLoanSchemeUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvLoanAdvanceList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objEFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.BackColor = System.Drawing.SystemColors.Control
        Me.pnlMainInfo.Controls.Add(Me.objChkAll)
        Me.pnlMainInfo.Controls.Add(Me.dgvLoanAdvanceList)
        Me.pnlMainInfo.Controls.Add(Me.objEFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(892, 533)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(17, 193)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 88
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'dgvLoanAdvanceList
        '
        Me.dgvLoanAdvanceList.AllowUserToAddRows = False
        Me.dgvLoanAdvanceList.AllowUserToDeleteRows = False
        Me.dgvLoanAdvanceList.AllowUserToResizeRows = False
        Me.dgvLoanAdvanceList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvLoanAdvanceList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvLoanAdvanceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLoanAdvanceList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhVocNo, Me.dgcolhPeriod, Me.dgcolhDeductionPeriod, Me.dgcolhEmployee, Me.dgcolhLoanScheme, Me.dgcolhLoanAdvance, Me.dgcolhLoanCalcType, Me.dgcolhInterestCalcType, Me.dgcolhAmount, Me.dgcolhCurrency, Me.dgcolhInstallments, Me.dgcolhBalance, Me.dgcolhPaidLoan, Me.dgcolhStatus, Me.dgcolhOtherOpApproval, Me.objdgcolhIsLoan, Me.objdgcolhstatusunkid, Me.objdgcolhemployeeunkid, Me.objdgcolhLoanAdvanceunkid, Me.objdgcolhVisibleStatus, Me.objdgcolhhisbrought_forward, Me.objdgcolhOpStatusId, Me.objdgcolhProcessPendingUnkid, Me.objdgcolhLoanSchemeUnkid})
        Me.dgvLoanAdvanceList.Location = New System.Drawing.Point(9, 187)
        Me.dgvLoanAdvanceList.MultiSelect = False
        Me.dgvLoanAdvanceList.Name = "dgvLoanAdvanceList"
        Me.dgvLoanAdvanceList.ReadOnly = True
        Me.dgvLoanAdvanceList.RowHeadersVisible = False
        Me.dgvLoanAdvanceList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLoanAdvanceList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanAdvanceList.Size = New System.Drawing.Size(871, 289)
        Me.dgvLoanAdvanceList.TabIndex = 0
        '
        'objEFooter
        '
        Me.objEFooter.BorderColor = System.Drawing.Color.Silver
        Me.objEFooter.Controls.Add(Me.btnEmailClose)
        Me.objEFooter.Controls.Add(Me.btnEmailOk)
        Me.objEFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objEFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objEFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objEFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objEFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objEFooter.Location = New System.Drawing.Point(0, 423)
        Me.objEFooter.Name = "objEFooter"
        Me.objEFooter.Size = New System.Drawing.Size(892, 55)
        Me.objEFooter.TabIndex = 5
        Me.objEFooter.Visible = False
        '
        'btnEmailClose
        '
        Me.btnEmailClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailClose.BackColor = System.Drawing.Color.White
        Me.btnEmailClose.BackgroundImage = CType(resources.GetObject("btnEmailClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmailClose.FlatAppearance.BorderSize = 0
        Me.btnEmailClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailClose.ForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.Location = New System.Drawing.Point(783, 12)
        Me.btnEmailClose.Name = "btnEmailClose"
        Me.btnEmailClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.Size = New System.Drawing.Size(97, 30)
        Me.btnEmailClose.TabIndex = 2
        Me.btnEmailClose.Text = "&Close"
        Me.btnEmailClose.UseVisualStyleBackColor = True
        '
        'btnEmailOk
        '
        Me.btnEmailOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailOk.BackColor = System.Drawing.Color.White
        Me.btnEmailOk.BackgroundImage = CType(resources.GetObject("btnEmailOk.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailOk.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmailOk.FlatAppearance.BorderSize = 0
        Me.btnEmailOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailOk.ForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Location = New System.Drawing.Point(680, 12)
        Me.btnEmailOk.Name = "btnEmailOk"
        Me.btnEmailOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Size = New System.Drawing.Size(97, 30)
        Me.btnEmailOk.TabIndex = 1
        Me.btnEmailOk.Text = "&Ok"
        Me.btnEmailOk.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(892, 58)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Loan / Advance List"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.lblCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.objLoanSchemeSearch)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboOtherOpStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblOtherOpStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.cboCondition)
        Me.gbFilterCriteria.Controls.Add(Me.txtVocNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmount)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmount)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblVocNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.cboAssignPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(871, 117)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCalcType
        '
        Me.cboCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCalcType.DropDownWidth = 200
        Me.cboCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalcType.FormattingEnabled = True
        Me.cboCalcType.Location = New System.Drawing.Point(280, 87)
        Me.cboCalcType.Name = "cboCalcType"
        Me.cboCalcType.Size = New System.Drawing.Size(107, 21)
        Me.cboCalcType.TabIndex = 78
        '
        'lblCalcType
        '
        Me.lblCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcType.Location = New System.Drawing.Point(187, 89)
        Me.lblCalcType.Name = "lblCalcType"
        Me.lblCalcType.Size = New System.Drawing.Size(87, 16)
        Me.lblCalcType.TabIndex = 79
        Me.lblCalcType.Text = "Calculation Type"
        Me.lblCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLoanSchemeSearch
        '
        Me.objLoanSchemeSearch.BackColor = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objLoanSchemeSearch.BorderSelected = False
        Me.objLoanSchemeSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objLoanSchemeSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objLoanSchemeSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objLoanSchemeSearch.Location = New System.Drawing.Point(304, 60)
        Me.objLoanSchemeSearch.Name = "objLoanSchemeSearch"
        Me.objLoanSchemeSearch.Size = New System.Drawing.Size(21, 21)
        Me.objLoanSchemeSearch.TabIndex = 12
        Me.objLoanSchemeSearch.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(304, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 11
        Me.objbtnSearchEmployee.Visible = False
        '
        'cboOtherOpStatus
        '
        Me.cboOtherOpStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherOpStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherOpStatus.FormattingEnabled = True
        Me.cboOtherOpStatus.Location = New System.Drawing.Point(502, 87)
        Me.cboOtherOpStatus.Name = "cboOtherOpStatus"
        Me.cboOtherOpStatus.Size = New System.Drawing.Size(135, 21)
        Me.cboOtherOpStatus.TabIndex = 5
        '
        'lblOtherOpStatus
        '
        Me.lblOtherOpStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherOpStatus.Location = New System.Drawing.Point(401, 89)
        Me.lblOtherOpStatus.Name = "lblOtherOpStatus"
        Me.lblOtherOpStatus.Size = New System.Drawing.Size(95, 16)
        Me.lblOtherOpStatus.TabIndex = 76
        Me.lblOtherOpStatus.Text = "Other Opr. Status"
        Me.lblOtherOpStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 150
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(720, 87)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(139, 21)
        Me.cboCurrency.TabIndex = 9
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(651, 89)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(63, 16)
        Me.lblCurrency.TabIndex = 73
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCondition
        '
        Me.cboCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Location = New System.Drawing.Point(817, 60)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(42, 21)
        Me.cboCondition.TabIndex = 8
        '
        'txtVocNo
        '
        Me.txtVocNo.Flags = 0
        Me.txtVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVocNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVocNo.Location = New System.Drawing.Point(720, 33)
        Me.txtVocNo.Name = "txtVocNo"
        Me.txtVocNo.Size = New System.Drawing.Size(139, 21)
        Me.txtVocNo.TabIndex = 6
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(649, 63)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(65, 15)
        Me.lblAmount.TabIndex = 14
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(720, 60)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(95, 21)
        Me.txtAmount.TabIndex = 7
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboLoanAdvance
        '
        Me.cboLoanAdvance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAdvance.DropDownWidth = 125
        Me.cboLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAdvance.FormattingEnabled = True
        Me.cboLoanAdvance.Location = New System.Drawing.Point(98, 87)
        Me.cboLoanAdvance.Name = "cboLoanAdvance"
        Me.cboLoanAdvance.Size = New System.Drawing.Size(83, 21)
        Me.cboLoanAdvance.TabIndex = 2
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(8, 90)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(84, 15)
        Me.lblLoanAdvance.TabIndex = 8
        Me.lblLoanAdvance.Text = "Loan/Advance"
        Me.lblLoanAdvance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 100
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(502, 60)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(135, 21)
        Me.cboStatus.TabIndex = 4
        '
        'lblVocNo
        '
        Me.lblVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVocNo.Location = New System.Drawing.Point(648, 35)
        Me.lblVocNo.Name = "lblVocNo"
        Me.lblVocNo.Size = New System.Drawing.Size(66, 16)
        Me.lblVocNo.TabIndex = 6
        Me.lblVocNo.Text = "Voucher No."
        Me.lblVocNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(401, 62)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(95, 16)
        Me.lblStatus.TabIndex = 10
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 63)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(84, 15)
        Me.lblLoanScheme.TabIndex = 3
        Me.lblLoanScheme.Text = "Loan Scheme"
        Me.lblLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(98, 60)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(289, 21)
        Me.cboLoanScheme.TabIndex = 1
        '
        'cboAssignPeriod
        '
        Me.cboAssignPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssignPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssignPeriod.FormattingEnabled = True
        Me.cboAssignPeriod.Location = New System.Drawing.Point(502, 33)
        Me.cboAssignPeriod.Name = "cboAssignPeriod"
        Me.cboAssignPeriod.Size = New System.Drawing.Size(135, 21)
        Me.cboAssignPeriod.TabIndex = 3
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(401, 35)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(95, 16)
        Me.lblPayPeriod.TabIndex = 12
        Me.lblPayPeriod.Text = "Assigned Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(98, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(289, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(84, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(844, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(821, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblCurrOpenPeriod)
        Me.objFooter.Controls.Add(Me.btnPreview)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnChangeStatus)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 478)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(892, 55)
        Me.objFooter.TabIndex = 2
        '
        'lblCurrOpenPeriod
        '
        Me.lblCurrOpenPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrOpenPeriod.ForeColor = System.Drawing.Color.Red
        Me.lblCurrOpenPeriod.Location = New System.Drawing.Point(237, 15)
        Me.lblCurrOpenPeriod.Name = "lblCurrOpenPeriod"
        Me.lblCurrOpenPeriod.Size = New System.Drawing.Size(223, 26)
        Me.lblCurrOpenPeriod.TabIndex = 74
        Me.lblCurrOpenPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.BackColor = System.Drawing.Color.White
        Me.btnPreview.BackgroundImage = CType(resources.GetObject("btnPreview.BackgroundImage"), System.Drawing.Image)
        Me.btnPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPreview.BorderColor = System.Drawing.Color.Empty
        Me.btnPreview.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.Color.Black
        Me.btnPreview.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPreview.GradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPreview.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.Location = New System.Drawing.Point(126, 13)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPreview.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.Size = New System.Drawing.Size(105, 30)
        Me.btnPreview.TabIndex = 1
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.ContextMenuStrip = Me.mnuOperations
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 0
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPayment, Me.mnuReceived, Me.objSep1, Me.mnuViewApprovalForm, Me.mnuChangeStatus, Me.objSep2, Me.mnuLoanOperation, Me.objSep3, Me.mnuGlobalChangeStatus})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(190, 154)
        '
        'mnuPayment
        '
        Me.mnuPayment.Name = "mnuPayment"
        Me.mnuPayment.Size = New System.Drawing.Size(189, 22)
        Me.mnuPayment.Text = "&Payment"
        '
        'mnuReceived
        '
        Me.mnuReceived.Name = "mnuReceived"
        Me.mnuReceived.Size = New System.Drawing.Size(189, 22)
        Me.mnuReceived.Text = "&Received"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(186, 6)
        '
        'mnuViewApprovalForm
        '
        Me.mnuViewApprovalForm.Name = "mnuViewApprovalForm"
        Me.mnuViewApprovalForm.Size = New System.Drawing.Size(189, 22)
        Me.mnuViewApprovalForm.Tag = "mnuViewApprovalForm"
        Me.mnuViewApprovalForm.Text = "&View Approved Form"
        '
        'mnuChangeStatus
        '
        Me.mnuChangeStatus.Name = "mnuChangeStatus"
        Me.mnuChangeStatus.Size = New System.Drawing.Size(189, 22)
        Me.mnuChangeStatus.Tag = "mnuChangeStatus"
        Me.mnuChangeStatus.Text = "Global Change Status"
        Me.mnuChangeStatus.Visible = False
        '
        'objSep2
        '
        Me.objSep2.Name = "objSep2"
        Me.objSep2.Size = New System.Drawing.Size(186, 6)
        Me.objSep2.Tag = "objSep2"
        '
        'mnuLoanOperation
        '
        Me.mnuLoanOperation.Name = "mnuLoanOperation"
        Me.mnuLoanOperation.Size = New System.Drawing.Size(189, 22)
        Me.mnuLoanOperation.Tag = "mnuLoanOperation"
        Me.mnuLoanOperation.Text = "Other Loan Operation"
        '
        'objSep3
        '
        Me.objSep3.Name = "objSep3"
        Me.objSep3.Size = New System.Drawing.Size(186, 6)
        Me.objSep3.Tag = "objSep3"
        '
        'mnuGlobalChangeStatus
        '
        Me.mnuGlobalChangeStatus.Name = "mnuGlobalChangeStatus"
        Me.mnuGlobalChangeStatus.Size = New System.Drawing.Size(189, 22)
        Me.mnuGlobalChangeStatus.Tag = "mnuGlobalChangeStatus"
        Me.mnuGlobalChangeStatus.Text = "Global Change Status"
        '
        'btnChangeStatus
        '
        Me.btnChangeStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnChangeStatus.BackColor = System.Drawing.Color.White
        Me.btnChangeStatus.BackgroundImage = CType(resources.GetObject("btnChangeStatus.BackgroundImage"), System.Drawing.Image)
        Me.btnChangeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnChangeStatus.BorderColor = System.Drawing.Color.Empty
        Me.btnChangeStatus.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnChangeStatus.FlatAppearance.BorderSize = 0
        Me.btnChangeStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChangeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangeStatus.ForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnChangeStatus.GradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Location = New System.Drawing.Point(466, 13)
        Me.btnChangeStatus.Name = "btnChangeStatus"
        Me.btnChangeStatus.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Size = New System.Drawing.Size(105, 30)
        Me.btnChangeStatus.TabIndex = 2
        Me.btnChangeStatus.Text = "&Change Status"
        Me.btnChangeStatus.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(680, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 4
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(577, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 3
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(577, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        Me.btnNew.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(783, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Voc #"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Period"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Deduction Period"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 175
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Loan Scheme"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 175
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Loan/Advance"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 80
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Loan Calculation Type"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 150
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Interest Type"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Loan Amount"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Currency"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 55
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "No. Of Installments"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Width = 130
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Curr. Principal Balance"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Width = 120
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Paid Loan"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Other Opr. Approval"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Width = 120
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Is Loan"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Statusunkid"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Employeeunkid"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "objdgcolhLoanAdvanceunkid"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "objdgcolhVisibleStatus"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "objdgcolhhisbrought_forward"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "objdgcolhOpStatusId"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "objdgcolhProcessPendingUnkid"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.ReadOnly = True
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhVocNo
        '
        Me.dgcolhVocNo.HeaderText = "Voc #"
        Me.dgcolhVocNo.Name = "dgcolhVocNo"
        Me.dgcolhVocNo.ReadOnly = True
        Me.dgcolhVocNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhPeriod
        '
        Me.dgcolhPeriod.HeaderText = "Period"
        Me.dgcolhPeriod.Name = "dgcolhPeriod"
        Me.dgcolhPeriod.ReadOnly = True
        Me.dgcolhPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDeductionPeriod
        '
        Me.dgcolhDeductionPeriod.HeaderText = "Deduction Period"
        Me.dgcolhDeductionPeriod.Name = "dgcolhDeductionPeriod"
        Me.dgcolhDeductionPeriod.ReadOnly = True
        Me.dgcolhDeductionPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee.Width = 175
        '
        'dgcolhLoanScheme
        '
        Me.dgcolhLoanScheme.HeaderText = "Loan Scheme"
        Me.dgcolhLoanScheme.Name = "dgcolhLoanScheme"
        Me.dgcolhLoanScheme.ReadOnly = True
        Me.dgcolhLoanScheme.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhLoanScheme.Width = 175
        '
        'dgcolhLoanAdvance
        '
        Me.dgcolhLoanAdvance.HeaderText = "Loan/Advance"
        Me.dgcolhLoanAdvance.Name = "dgcolhLoanAdvance"
        Me.dgcolhLoanAdvance.ReadOnly = True
        Me.dgcolhLoanAdvance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhLoanAdvance.Width = 80
        '
        'dgcolhLoanCalcType
        '
        Me.dgcolhLoanCalcType.HeaderText = "Loan Calculation Type"
        Me.dgcolhLoanCalcType.Name = "dgcolhLoanCalcType"
        Me.dgcolhLoanCalcType.ReadOnly = True
        Me.dgcolhLoanCalcType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhLoanCalcType.Width = 150
        '
        'dgcolhInterestCalcType
        '
        Me.dgcolhInterestCalcType.HeaderText = "Interest Type"
        Me.dgcolhInterestCalcType.Name = "dgcolhInterestCalcType"
        Me.dgcolhInterestCalcType.ReadOnly = True
        '
        'dgcolhAmount
        '
        Me.dgcolhAmount.HeaderText = "Loan Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCurrency
        '
        Me.dgcolhCurrency.HeaderText = "Currency"
        Me.dgcolhCurrency.Name = "dgcolhCurrency"
        Me.dgcolhCurrency.ReadOnly = True
        Me.dgcolhCurrency.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCurrency.Width = 55
        '
        'dgcolhInstallments
        '
        Me.dgcolhInstallments.HeaderText = "No. Of Installments"
        Me.dgcolhInstallments.Name = "dgcolhInstallments"
        Me.dgcolhInstallments.ReadOnly = True
        Me.dgcolhInstallments.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInstallments.Width = 130
        '
        'dgcolhBalance
        '
        Me.dgcolhBalance.HeaderText = "Curr. Principal Balance"
        Me.dgcolhBalance.Name = "dgcolhBalance"
        Me.dgcolhBalance.ReadOnly = True
        Me.dgcolhBalance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBalance.Width = 120
        '
        'dgcolhPaidLoan
        '
        Me.dgcolhPaidLoan.HeaderText = "Paid Loan"
        Me.dgcolhPaidLoan.Name = "dgcolhPaidLoan"
        Me.dgcolhPaidLoan.ReadOnly = True
        Me.dgcolhPaidLoan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhOtherOpApproval
        '
        Me.dgcolhOtherOpApproval.HeaderText = "Other Opr. Approval"
        Me.dgcolhOtherOpApproval.Name = "dgcolhOtherOpApproval"
        Me.dgcolhOtherOpApproval.ReadOnly = True
        Me.dgcolhOtherOpApproval.Width = 120
        '
        'objdgcolhIsLoan
        '
        Me.objdgcolhIsLoan.HeaderText = "Is Loan"
        Me.objdgcolhIsLoan.Name = "objdgcolhIsLoan"
        Me.objdgcolhIsLoan.ReadOnly = True
        Me.objdgcolhIsLoan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsLoan.Visible = False
        '
        'objdgcolhstatusunkid
        '
        Me.objdgcolhstatusunkid.HeaderText = "Statusunkid"
        Me.objdgcolhstatusunkid.Name = "objdgcolhstatusunkid"
        Me.objdgcolhstatusunkid.ReadOnly = True
        Me.objdgcolhstatusunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhstatusunkid.Visible = False
        '
        'objdgcolhemployeeunkid
        '
        Me.objdgcolhemployeeunkid.HeaderText = "Employeeunkid"
        Me.objdgcolhemployeeunkid.Name = "objdgcolhemployeeunkid"
        Me.objdgcolhemployeeunkid.ReadOnly = True
        Me.objdgcolhemployeeunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhemployeeunkid.Visible = False
        '
        'objdgcolhLoanAdvanceunkid
        '
        Me.objdgcolhLoanAdvanceunkid.HeaderText = "objdgcolhLoanAdvanceunkid"
        Me.objdgcolhLoanAdvanceunkid.Name = "objdgcolhLoanAdvanceunkid"
        Me.objdgcolhLoanAdvanceunkid.ReadOnly = True
        Me.objdgcolhLoanAdvanceunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhLoanAdvanceunkid.Visible = False
        '
        'objdgcolhVisibleStatus
        '
        Me.objdgcolhVisibleStatus.HeaderText = "objdgcolhVisibleStatus"
        Me.objdgcolhVisibleStatus.Name = "objdgcolhVisibleStatus"
        Me.objdgcolhVisibleStatus.ReadOnly = True
        Me.objdgcolhVisibleStatus.Visible = False
        '
        'objdgcolhhisbrought_forward
        '
        Me.objdgcolhhisbrought_forward.HeaderText = "objdgcolhhisbrought_forward"
        Me.objdgcolhhisbrought_forward.Name = "objdgcolhhisbrought_forward"
        Me.objdgcolhhisbrought_forward.ReadOnly = True
        Me.objdgcolhhisbrought_forward.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhhisbrought_forward.Visible = False
        '
        'objdgcolhOpStatusId
        '
        Me.objdgcolhOpStatusId.HeaderText = "objdgcolhOpStatusId"
        Me.objdgcolhOpStatusId.Name = "objdgcolhOpStatusId"
        Me.objdgcolhOpStatusId.ReadOnly = True
        Me.objdgcolhOpStatusId.Visible = False
        '
        'objdgcolhProcessPendingUnkid
        '
        Me.objdgcolhProcessPendingUnkid.HeaderText = "objdgcolhProcessPendingUnkid"
        Me.objdgcolhProcessPendingUnkid.Name = "objdgcolhProcessPendingUnkid"
        Me.objdgcolhProcessPendingUnkid.ReadOnly = True
        Me.objdgcolhProcessPendingUnkid.Visible = False
        '
        'objdgcolhLoanSchemeUnkid
        '
        Me.objdgcolhLoanSchemeUnkid.HeaderText = "objdgcolhLoanSchemeUnkid"
        Me.objdgcolhLoanSchemeUnkid.Name = "objdgcolhLoanSchemeUnkid"
        Me.objdgcolhLoanSchemeUnkid.ReadOnly = True
        Me.objdgcolhLoanSchemeUnkid.Visible = False
        '
        'frmNewLoanAdvanceList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(892, 533)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNewLoanAdvanceList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan / Advance List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        CType(Me.dgvLoanAdvanceList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objEFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboLoanAdvance As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents cboAssignPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnChangeStatus As eZee.Common.eZeeLightButton
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtVocNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVocNo As System.Windows.Forms.Label
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReceived As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objLoanSchemeSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents btnPreview As eZee.Common.eZeeLightButton
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuViewApprovalForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuChangeStatus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvLoanAdvanceList As System.Windows.Forms.DataGridView
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents objSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuLoanOperation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents lblCurrOpenPeriod As System.Windows.Forms.Label
    Friend WithEvents cboOtherOpStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherOpStatus As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objSep3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuGlobalChangeStatus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCalcType As System.Windows.Forms.Label
    Friend WithEvents objEFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnEmailClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmailOk As eZee.Common.eZeeLightButton
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhVocNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDeductionPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanScheme As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanAdvance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanCalcType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInterestCalcType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCurrency As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInstallments As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPaidLoan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOtherOpApproval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsLoan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhstatusunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhemployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLoanAdvanceunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhVisibleStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhhisbrought_forward As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhOpStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProcessPendingUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLoanSchemeUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
