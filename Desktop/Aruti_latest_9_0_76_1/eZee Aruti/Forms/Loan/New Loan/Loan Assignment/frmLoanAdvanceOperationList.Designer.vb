﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanAdvanceOperationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanAdvanceOperationList))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlPendingControls = New System.Windows.Forms.Panel
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.chkMyApprovals = New System.Windows.Forms.CheckBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.tbOperationList = New System.Windows.Forms.TabControl
        Me.tbPending = New System.Windows.Forms.TabPage
        Me.dgPendingOp = New System.Windows.Forms.DataGridView
        Me.dgcolhPeriodName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOperation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInstlNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInstlAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTopupAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhisgrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhloanOpTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhmappeduserid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhlnapproverunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApproverEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPriority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhlnotheroptranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIdentifyGuid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLoanAdvanceTranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tbApproved = New System.Windows.Forms.TabPage
        Me.lvData = New eZee.Common.eZeeListView(Me.components)
        Me.colhEffectiveDate = New System.Windows.Forms.ColumnHeader
        Me.colhRate = New System.Windows.Forms.ColumnHeader
        Me.colhINSTLNo = New System.Windows.Forms.ColumnHeader
        Me.colhINSTLAmt = New System.Windows.Forms.ColumnHeader
        Me.colhTopup = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriod = New System.Windows.Forms.ColumnHeader
        Me.objcolhIdType = New System.Windows.Forms.ColumnHeader
        Me.objcolhStatusId = New System.Windows.Forms.ColumnHeader
        Me.colhChangeType = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsdefault = New System.Windows.Forms.ColumnHeader
        Me.objcolhApproverunkid = New System.Windows.Forms.ColumnHeader
        Me.colhApprover = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnChangeStatus = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblEffDate = New System.Windows.Forms.Label
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtVocNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblVocNo = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.txtLoanScheme = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmployeeName = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIdentifyGuid = New System.Windows.Forms.ColumnHeader
        Me.pnlMain.SuspendLayout()
        Me.pnlPendingControls.SuspendLayout()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbOperationList.SuspendLayout()
        Me.tbPending.SuspendLayout()
        CType(Me.dgPendingOp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbApproved.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.pnlPendingControls)
        Me.pnlMain.Controls.Add(Me.tbOperationList)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.txtVocNo)
        Me.pnlMain.Controls.Add(Me.lblVocNo)
        Me.pnlMain.Controls.Add(Me.objLine1)
        Me.pnlMain.Controls.Add(Me.txtLoanScheme)
        Me.pnlMain.Controls.Add(Me.txtEmployeeName)
        Me.pnlMain.Controls.Add(Me.lblEmpName)
        Me.pnlMain.Controls.Add(Me.lblLoanScheme)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(788, 473)
        Me.pnlMain.TabIndex = 0
        '
        'pnlPendingControls
        '
        Me.pnlPendingControls.Controls.Add(Me.objbtnSearch)
        Me.pnlPendingControls.Controls.Add(Me.cboStatus)
        Me.pnlPendingControls.Controls.Add(Me.chkMyApprovals)
        Me.pnlPendingControls.Controls.Add(Me.lblStatus)
        Me.pnlPendingControls.Location = New System.Drawing.Point(558, 3)
        Me.pnlPendingControls.Name = "pnlPendingControls"
        Me.pnlPendingControls.Size = New System.Drawing.Size(227, 65)
        Me.pnlPendingControls.TabIndex = 249
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(198, 6)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 256
        Me.objbtnSearch.TabStop = False
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(79, 9)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(115, 21)
        Me.cboStatus.TabIndex = 255
        '
        'chkMyApprovals
        '
        Me.chkMyApprovals.Checked = True
        Me.chkMyApprovals.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMyApprovals.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkMyApprovals.Location = New System.Drawing.Point(79, 39)
        Me.chkMyApprovals.Name = "chkMyApprovals"
        Me.chkMyApprovals.Size = New System.Drawing.Size(145, 17)
        Me.chkMyApprovals.TabIndex = 253
        Me.chkMyApprovals.Text = "My Approvals"
        Me.chkMyApprovals.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.Location = New System.Drawing.Point(8, 13)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(65, 13)
        Me.lblStatus.TabIndex = 254
        Me.lblStatus.Text = "Status"
        '
        'tbOperationList
        '
        Me.tbOperationList.Controls.Add(Me.tbPending)
        Me.tbOperationList.Controls.Add(Me.tbApproved)
        Me.tbOperationList.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tbOperationList.Location = New System.Drawing.Point(0, 86)
        Me.tbOperationList.Name = "tbOperationList"
        Me.tbOperationList.SelectedIndex = 0
        Me.tbOperationList.Size = New System.Drawing.Size(788, 337)
        Me.tbOperationList.TabIndex = 248
        '
        'tbPending
        '
        Me.tbPending.Controls.Add(Me.dgPendingOp)
        Me.tbPending.Location = New System.Drawing.Point(4, 22)
        Me.tbPending.Name = "tbPending"
        Me.tbPending.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPending.Size = New System.Drawing.Size(780, 311)
        Me.tbPending.TabIndex = 1
        Me.tbPending.Text = "Pending Operation"
        Me.tbPending.UseVisualStyleBackColor = True
        '
        'dgPendingOp
        '
        Me.dgPendingOp.AllowUserToAddRows = False
        Me.dgPendingOp.AllowUserToDeleteRows = False
        Me.dgPendingOp.AllowUserToResizeRows = False
        Me.dgPendingOp.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgPendingOp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgPendingOp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgPendingOp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhPeriodName, Me.dgcolhEffectiveDate, Me.dgcolhOperation, Me.dgcolhRate, Me.dgcolhInstlNo, Me.dgcolhInstlAmount, Me.dgcolhTopupAmount, Me.dgcolhApprover, Me.dgcolhStatus, Me.dgcolhRemark, Me.objdgcolhisgrp, Me.objdgcolhloanOpTypeId, Me.objdgcolhmappeduserid, Me.objdgcolhEmployeeId, Me.objdgcolhlnapproverunkid, Me.objdgcolhApproverEmpId, Me.objdgcolhPriority, Me.objdgcolhlnotheroptranunkid, Me.objdgcolhIdentifyGuid, Me.objdgcolhStatusunkid, Me.objdgcolhLoanAdvanceTranunkid})
        Me.dgPendingOp.Location = New System.Drawing.Point(0, 0)
        Me.dgPendingOp.MultiSelect = False
        Me.dgPendingOp.Name = "dgPendingOp"
        Me.dgPendingOp.ReadOnly = True
        Me.dgPendingOp.RowHeadersVisible = False
        Me.dgPendingOp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPendingOp.Size = New System.Drawing.Size(777, 311)
        Me.dgPendingOp.TabIndex = 0
        '
        'dgcolhPeriodName
        '
        Me.dgcolhPeriodName.HeaderText = "Period"
        Me.dgcolhPeriodName.MinimumWidth = 2
        Me.dgcolhPeriodName.Name = "dgcolhPeriodName"
        Me.dgcolhPeriodName.ReadOnly = True
        Me.dgcolhPeriodName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhPeriodName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhPeriodName.Width = 2
        '
        'dgcolhEffectiveDate
        '
        Me.dgcolhEffectiveDate.HeaderText = "Effective Date"
        Me.dgcolhEffectiveDate.Name = "dgcolhEffectiveDate"
        Me.dgcolhEffectiveDate.ReadOnly = True
        Me.dgcolhEffectiveDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEffectiveDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhOperation
        '
        Me.dgcolhOperation.HeaderText = "Operation"
        Me.dgcolhOperation.Name = "dgcolhOperation"
        Me.dgcolhOperation.ReadOnly = True
        Me.dgcolhOperation.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhOperation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOperation.Width = 125
        '
        'dgcolhRate
        '
        Me.dgcolhRate.HeaderText = "Rate(%)"
        Me.dgcolhRate.Name = "dgcolhRate"
        Me.dgcolhRate.ReadOnly = True
        Me.dgcolhRate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRate.Width = 80
        '
        'dgcolhInstlNo
        '
        Me.dgcolhInstlNo.HeaderText = "INSTL No."
        Me.dgcolhInstlNo.Name = "dgcolhInstlNo"
        Me.dgcolhInstlNo.ReadOnly = True
        Me.dgcolhInstlNo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhInstlNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInstlNo.Width = 80
        '
        'dgcolhInstlAmount
        '
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgcolhInstlAmount.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhInstlAmount.HeaderText = "INSTL Amount"
        Me.dgcolhInstlAmount.Name = "dgcolhInstlAmount"
        Me.dgcolhInstlAmount.ReadOnly = True
        Me.dgcolhInstlAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInstlAmount.Width = 125
        '
        'dgcolhTopupAmount
        '
        Me.dgcolhTopupAmount.HeaderText = "Topup Amount"
        Me.dgcolhTopupAmount.Name = "dgcolhTopupAmount"
        Me.dgcolhTopupAmount.ReadOnly = True
        Me.dgcolhTopupAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTopupAmount.Width = 125
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        Me.dgcolhApprover.ReadOnly = True
        Me.dgcolhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprover.Width = 200
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhStatus.Width = 200
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRemark.Width = 200
        '
        'objdgcolhisgrp
        '
        Me.objdgcolhisgrp.HeaderText = "objIsGrp"
        Me.objdgcolhisgrp.Name = "objdgcolhisgrp"
        Me.objdgcolhisgrp.ReadOnly = True
        Me.objdgcolhisgrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhisgrp.Visible = False
        '
        'objdgcolhloanOpTypeId
        '
        Me.objdgcolhloanOpTypeId.HeaderText = "objLoanOpTypeId"
        Me.objdgcolhloanOpTypeId.Name = "objdgcolhloanOpTypeId"
        Me.objdgcolhloanOpTypeId.ReadOnly = True
        Me.objdgcolhloanOpTypeId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhloanOpTypeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhloanOpTypeId.Visible = False
        '
        'objdgcolhmappeduserid
        '
        Me.objdgcolhmappeduserid.HeaderText = "objMappedUserId"
        Me.objdgcolhmappeduserid.Name = "objdgcolhmappeduserid"
        Me.objdgcolhmappeduserid.ReadOnly = True
        Me.objdgcolhmappeduserid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhmappeduserid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhmappeduserid.Visible = False
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "objEmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEmployeeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeId.Visible = False
        '
        'objdgcolhlnapproverunkid
        '
        Me.objdgcolhlnapproverunkid.HeaderText = "objApproverunkid"
        Me.objdgcolhlnapproverunkid.Name = "objdgcolhlnapproverunkid"
        Me.objdgcolhlnapproverunkid.ReadOnly = True
        Me.objdgcolhlnapproverunkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhlnapproverunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhlnapproverunkid.Visible = False
        '
        'objdgcolhApproverEmpId
        '
        Me.objdgcolhApproverEmpId.HeaderText = "objApproverEmpId"
        Me.objdgcolhApproverEmpId.Name = "objdgcolhApproverEmpId"
        Me.objdgcolhApproverEmpId.ReadOnly = True
        Me.objdgcolhApproverEmpId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhApproverEmpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApproverEmpId.Visible = False
        '
        'objdgcolhPriority
        '
        Me.objdgcolhPriority.HeaderText = "objPriority"
        Me.objdgcolhPriority.Name = "objdgcolhPriority"
        Me.objdgcolhPriority.ReadOnly = True
        Me.objdgcolhPriority.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhPriority.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhPriority.Visible = False
        '
        'objdgcolhlnotheroptranunkid
        '
        Me.objdgcolhlnotheroptranunkid.HeaderText = "objlnOtherOpTranunkid"
        Me.objdgcolhlnotheroptranunkid.Name = "objdgcolhlnotheroptranunkid"
        Me.objdgcolhlnotheroptranunkid.ReadOnly = True
        Me.objdgcolhlnotheroptranunkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhlnotheroptranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhlnotheroptranunkid.Visible = False
        '
        'objdgcolhIdentifyGuid
        '
        Me.objdgcolhIdentifyGuid.HeaderText = "objIdentifyGuid"
        Me.objdgcolhIdentifyGuid.Name = "objdgcolhIdentifyGuid"
        Me.objdgcolhIdentifyGuid.ReadOnly = True
        Me.objdgcolhIdentifyGuid.Visible = False
        '
        'objdgcolhStatusunkid
        '
        Me.objdgcolhStatusunkid.HeaderText = "objlnStatusunkid"
        Me.objdgcolhStatusunkid.Name = "objdgcolhStatusunkid"
        Me.objdgcolhStatusunkid.ReadOnly = True
        Me.objdgcolhStatusunkid.Visible = False
        '
        'objdgcolhLoanAdvanceTranunkid
        '
        Me.objdgcolhLoanAdvanceTranunkid.HeaderText = "objLoanAdvanceTranunkid"
        Me.objdgcolhLoanAdvanceTranunkid.Name = "objdgcolhLoanAdvanceTranunkid"
        Me.objdgcolhLoanAdvanceTranunkid.ReadOnly = True
        Me.objdgcolhLoanAdvanceTranunkid.Visible = False
        '
        'tbApproved
        '
        Me.tbApproved.Controls.Add(Me.lvData)
        Me.tbApproved.Location = New System.Drawing.Point(4, 22)
        Me.tbApproved.Name = "tbApproved"
        Me.tbApproved.Padding = New System.Windows.Forms.Padding(3)
        Me.tbApproved.Size = New System.Drawing.Size(780, 311)
        Me.tbApproved.TabIndex = 0
        Me.tbApproved.Text = "Approved Operation"
        Me.tbApproved.UseVisualStyleBackColor = True
        '
        'lvData
        '
        Me.lvData.BackColorOnChecked = True
        Me.lvData.ColumnHeaders = Nothing
        Me.lvData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEffectiveDate, Me.colhRate, Me.colhINSTLNo, Me.colhINSTLAmt, Me.colhTopup, Me.objcolhPeriod, Me.objcolhIdType, Me.objcolhStatusId, Me.colhChangeType, Me.objcolhIsdefault, Me.objcolhApproverunkid, Me.colhApprover, Me.objcolhIdentifyGuid})
        Me.lvData.CompulsoryColumns = ""
        Me.lvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvData.FullRowSelect = True
        Me.lvData.GridLines = True
        Me.lvData.GroupingColumn = Nothing
        Me.lvData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvData.HideSelection = False
        Me.lvData.Location = New System.Drawing.Point(3, 3)
        Me.lvData.MinColumnWidth = 50
        Me.lvData.MultiSelect = False
        Me.lvData.Name = "lvData"
        Me.lvData.OptionalColumns = ""
        Me.lvData.ShowMoreItem = False
        Me.lvData.ShowSaveItem = False
        Me.lvData.ShowSelectAll = True
        Me.lvData.ShowSizeAllColumnsToFit = True
        Me.lvData.Size = New System.Drawing.Size(774, 305)
        Me.lvData.Sortable = True
        Me.lvData.TabIndex = 247
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.Details
        '
        'colhEffectiveDate
        '
        Me.colhEffectiveDate.Tag = "colhEffectiveDate"
        Me.colhEffectiveDate.Text = "Effective Date"
        Me.colhEffectiveDate.Width = 100
        '
        'colhRate
        '
        Me.colhRate.Tag = "colhRate"
        Me.colhRate.Text = "Rate(%)"
        Me.colhRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhRate.Width = 97
        '
        'colhINSTLNo
        '
        Me.colhINSTLNo.Tag = "colhINSTLNo"
        Me.colhINSTLNo.Text = "INSTL No."
        Me.colhINSTLNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhINSTLNo.Width = 94
        '
        'colhINSTLAmt
        '
        Me.colhINSTLAmt.Tag = "colhINSTLAmt"
        Me.colhINSTLAmt.Text = "INSTL Amount"
        Me.colhINSTLAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhINSTLAmt.Width = 125
        '
        'colhTopup
        '
        Me.colhTopup.Tag = "colhTopup"
        Me.colhTopup.Text = "Topup Amount"
        Me.colhTopup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhTopup.Width = 125
        '
        'objcolhPeriod
        '
        Me.objcolhPeriod.Tag = "objcolhPeriod"
        Me.objcolhPeriod.Text = "objcolhPeriod"
        Me.objcolhPeriod.Width = 0
        '
        'objcolhIdType
        '
        Me.objcolhIdType.Tag = "objcolhIdType"
        Me.objcolhIdType.Text = "objcolhIdType"
        Me.objcolhIdType.Width = 0
        '
        'objcolhStatusId
        '
        Me.objcolhStatusId.Tag = "objcolhStatusId"
        Me.objcolhStatusId.Text = "objcolhStatusId"
        Me.objcolhStatusId.Width = 0
        '
        'colhChangeType
        '
        Me.colhChangeType.Tag = "colhChangeType"
        Me.colhChangeType.Text = "Operation"
        Me.colhChangeType.Width = 130
        '
        'objcolhIsdefault
        '
        Me.objcolhIsdefault.Tag = "objcolhIsdefault"
        Me.objcolhIsdefault.Text = "objcolhIsdefault"
        Me.objcolhIsdefault.Width = 0
        '
        'objcolhApproverunkid
        '
        Me.objcolhApproverunkid.Tag = "objcolhApproverunkid"
        Me.objcolhApproverunkid.Text = "objcolhApproverunkid"
        Me.objcolhApproverunkid.Width = 0
        '
        'colhApprover
        '
        Me.colhApprover.Text = "Approver"
        Me.colhApprover.Width = 250
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnChangeStatus)
        Me.objFooter.Controls.Add(Me.objlblEffDate)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 423)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(788, 50)
        Me.objFooter.TabIndex = 240
        '
        'btnChangeStatus
        '
        Me.btnChangeStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnChangeStatus.BackColor = System.Drawing.Color.White
        Me.btnChangeStatus.BackgroundImage = CType(resources.GetObject("btnChangeStatus.BackgroundImage"), System.Drawing.Image)
        Me.btnChangeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnChangeStatus.BorderColor = System.Drawing.Color.Empty
        Me.btnChangeStatus.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnChangeStatus.FlatAppearance.BorderSize = 0
        Me.btnChangeStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChangeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangeStatus.ForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnChangeStatus.GradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Location = New System.Drawing.Point(267, 10)
        Me.btnChangeStatus.Name = "btnChangeStatus"
        Me.btnChangeStatus.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Size = New System.Drawing.Size(97, 30)
        Me.btnChangeStatus.TabIndex = 4
        Me.btnChangeStatus.Text = "Change &Status"
        Me.btnChangeStatus.UseVisualStyleBackColor = True
        '
        'objlblEffDate
        '
        Me.objlblEffDate.Location = New System.Drawing.Point(9, 19)
        Me.objlblEffDate.Name = "objlblEffDate"
        Me.objlblEffDate.Size = New System.Drawing.Size(238, 13)
        Me.objlblEffDate.TabIndex = 3
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(576, 10)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(370, 10)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(679, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(473, 10)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'txtVocNo
        '
        Me.txtVocNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtVocNo.Flags = 0
        Me.txtVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVocNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVocNo.Location = New System.Drawing.Point(425, 39)
        Me.txtVocNo.Name = "txtVocNo"
        Me.txtVocNo.ReadOnly = True
        Me.txtVocNo.Size = New System.Drawing.Size(110, 21)
        Me.txtVocNo.TabIndex = 239
        '
        'lblVocNo
        '
        Me.lblVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVocNo.Location = New System.Drawing.Point(345, 42)
        Me.lblVocNo.Name = "lblVocNo"
        Me.lblVocNo.Size = New System.Drawing.Size(74, 15)
        Me.lblVocNo.TabIndex = 238
        Me.lblVocNo.Text = "Vocher No."
        Me.lblVocNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(12, 69)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(765, 13)
        Me.objLine1.TabIndex = 237
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLoanScheme
        '
        Me.txtLoanScheme.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanScheme.Flags = 0
        Me.txtLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanScheme.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLoanScheme.Location = New System.Drawing.Point(110, 39)
        Me.txtLoanScheme.Name = "txtLoanScheme"
        Me.txtLoanScheme.ReadOnly = True
        Me.txtLoanScheme.Size = New System.Drawing.Size(218, 21)
        Me.txtLoanScheme.TabIndex = 233
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.BackColor = System.Drawing.Color.White
        Me.txtEmployeeName.Flags = 0
        Me.txtEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeName.Location = New System.Drawing.Point(110, 12)
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.ReadOnly = True
        Me.txtEmployeeName.Size = New System.Drawing.Size(425, 21)
        Me.txtEmployeeName.TabIndex = 234
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(12, 15)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(92, 15)
        Me.lblEmpName.TabIndex = 236
        Me.lblEmpName.Text = "Employee Name"
        Me.lblEmpName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(12, 42)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(79, 15)
        Me.lblLoanScheme.TabIndex = 235
        Me.lblLoanScheme.Text = "Loan Scheme"
        Me.lblLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Period"
        Me.DataGridViewTextBoxColumn1.MinimumWidth = 2
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 10
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Effective Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Rate(%)"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "INSTL No."
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn5.HeaderText = "INSTL Amount"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Topup Amount"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 150
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Operation"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 170
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Approver"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 200
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 200
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 200
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "IsGrp"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "OpTypeId"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "MappedUserId"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "EmployeeId"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "LoanOpTypeId"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Approverunkid"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "ApproverEmpId"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "objlnOtherOpTranunkid"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "objIdentifyGuid"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "objlnStatusunkid"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "objLoanAdvanceTranunkid"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'objcolhIdentifyGuid
        '
        Me.objcolhIdentifyGuid.Tag = "objcolhIdentifyGuid"
        Me.objcolhIdentifyGuid.Width = 0
        '
        'frmLoanAdvanceOperationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(788, 473)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanAdvanceOperationList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan/Advance Operation List"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.pnlPendingControls.ResumeLayout(False)
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbOperationList.ResumeLayout(False)
        Me.tbPending.ResumeLayout(False)
        CType(Me.dgPendingOp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbApproved.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents txtLoanScheme As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployeeName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents txtVocNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVocNo As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents lvData As eZee.Common.eZeeListView
    Friend WithEvents colhEffectiveDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhINSTLNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhINSTLAmt As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTopup As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIdType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhStatusId As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhChangeType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objlblEffDate As System.Windows.Forms.Label
    Friend WithEvents tbOperationList As System.Windows.Forms.TabControl
    Friend WithEvents tbApproved As System.Windows.Forms.TabPage
    Friend WithEvents tbPending As System.Windows.Forms.TabPage
    Friend WithEvents btnChangeStatus As eZee.Common.eZeeLightButton
    Friend WithEvents dgPendingOp As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIsdefault As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhApproverunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApprover As System.Windows.Forms.ColumnHeader
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlPendingControls As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents chkMyApprovals As System.Windows.Forms.CheckBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents dgcolhPeriodName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOperation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInstlNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInstlAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTopupAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhisgrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhloanOpTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhmappeduserid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhlnapproverunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApproverEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPriority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhlnotheroptranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIdentifyGuid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLoanAdvanceTranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIdentifyGuid As System.Windows.Forms.ColumnHeader
End Class
