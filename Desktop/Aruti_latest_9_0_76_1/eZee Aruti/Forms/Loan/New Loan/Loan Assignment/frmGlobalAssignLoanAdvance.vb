﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

#End Region

Public Class frmGlobalAssignLoanAdvance

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalAssignLoanAdvance"

    Private objLoan_Advance As clsLoan_Advance
    Private objPendingLoan As clsProcess_pending_loan
    Private objlnInterest As clslnloan_interest_tran
    Private objlnEMI As clslnloan_emitenure_tran
    Private mdtTran As DataTable

    Private mblnIsFormLoad As Boolean = False
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage
    Private mstrAdvanceFilter As String = String.Empty

    Private mblnIsDurationChanged As Boolean = False
    Private mblnIsAmountChanged As Boolean = False

    'Nilay (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Private mdtStartDate As Date
    Private mdtEndDate As Date
    'Nilay (21-Jul-2016) -- End

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private mstrSearchText As String = String.Empty
    'Nilay (27-Oct-2016) -- End

    'Hemant (30 Aug 2019) -- Start
    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
    Private trd As Thread
    Private objEmailList As New List(Of clsEmailCollection)
    'Hemant (30 Aug 2019) -- End

#End Region

#Region " Private Functions & Procedure "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objApprover As New clsLoanApprover_master
        Dim objScheme As New clsLoan_Scheme
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objCurrency As New clsExchangeRate
        Dim objHead As New clsTransactionHead 'Sohail (29 Apr 2019)
        Try
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, _
                                             ConfigParameter._Object._IsIncludeInactiveEmp, _
                                             "List", True)
            'Nilay (10-Oct-2015) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Nilay (27-Oct-2016) -- End

            dsCombo = objApprover.getListForCombo("List", True, True, True)
            With cboApprover
                .DisplayMember = "name"
                .ValueMember = "lnapproverunkid"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsCombo = objScheme.getComboList(True, "List")
            dsCombo = objScheme.getComboList(True, "List", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoan
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboLoan)
            'Nilay (27-Oct-2016) -- End

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                FinancialYear._Object._YearUnkid, _
                                                FinancialYear._Object._DatabaseName, _
                                                FinancialYear._Object._Database_Start_Date, _
                                                "List", True, enStatusType.Open)
            'Nilay (10-Oct-2015) -- End


            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
                .SelectedValue = 0
            End With


            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
            'cboMode.Items.Clear()
            'cboMode.Items.Add(Language.getMessage(mstrModuleName, 1, "Loan"))
            'cboMode.Items.Add(Language.getMessage(mstrModuleName, 2, "Advance"))
            'cboMode.SelectedIndex = 0
            Dim objMaster As New clsMasterData
            dsCombo = objMaster.GetLoanAdvance("List")
            With cboMode
                .ValueMember = "ID"
                .DisplayMember = "NAME"
                .DataSource = dsCombo.Tables("List")
            End With
            'Nilay (08-Dec-2016) -- End

            dsCombo = objCurrency.getComboList("Currency", True)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombo.Tables("Currency")
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                '.SelectedValue = 0
                .SelectedValue = objCurrency.getBaseCurrencyCountryID()
                'Sohail (15 Dec 2015) -- End
            End With

            'Nilay (08-Aug-2016) -- Start
            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
            Dim dsLType As New DataSet
            Dim objLnAdv As New clsLoan_Advance

            dsLType = objLnAdv.GetLoanCalculationTypeList("List", True)
            With cboGLoanCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsLType.Tables(0)
            End With

            Dim dsItype As New DataSet
            dsItype = objLnAdv.GetLoan_Interest_Calculation_Type("List", True)

            With cboGInterestCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsItype.Tables(0)
            End With
            'Nilay (08-Aug-2016) -- END

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            dsCombo = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            With cboMappedHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboMappedHead)
            End With
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objEmp = Nothing : objApprover = Nothing : objScheme = Nothing : objPeriod = Nothing
            objCurrency = Nothing
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.SelectionForeColor = Color.Black
            dgvcsChild.SelectionBackColor = Color.White
            dgvcsChild.BackColor = Color.White


            For i As Integer = 0 To dgvDataList.RowCount - 1
                If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvDataList.Rows(i).DefaultCellStyle = dgvcsHeader
                Else
                    dgvDataList.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvDataList.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsCollapseChild As New DataGridViewCellStyle
            objdgvsCollapseChild.Font = New Font(dgvDataList.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseChild.ForeColor = Color.Black
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


            For i As Integer = 0 To dgvDataList.RowCount - 1
                If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = "▬"
                        dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                    End If
                    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                Else
                    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If ConfigParameter._Object._LoanVocNoType = 1 Then
                nudManualNo.Enabled = False
            Else
                nudManualNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboCurrency.BackColor = GUI.ColorComp
            txtPurpose.BackColor = GUI.ColorOptional
            txtPrefix.BackColor = GUI.ColorComp
            nudManualNo.BackColor = GUI.ColorComp
            cboApprover.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorOptional
            cboLoan.BackColor = GUI.ColorOptional
            cboMode.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            cboMappedHead.BackColor = GUI.ColorOptional
            'Sohail (29 Apr 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridDataSoruce()
        Dim objHead As New clsTransactionHead 'Sohail (29 Apr 2019)
        Try
            dgvDataList.AutoGenerateColumns = False

            objdgcolhSelect.DataPropertyName = "IsCheck"
            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhApprover.DataPropertyName = "Approver"
            dgcolhAmount.DataPropertyName = "Amount"
            Dim dsPrd As New DataSet
            Dim objPrd As New clscommom_period_Tran
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsPrd = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsPrd = objPrd.getListForCombo(enModuleReference.Payroll, _
                                           FinancialYear._Object._YearUnkid, _
                                           FinancialYear._Object._DatabaseName, _
                                           FinancialYear._Object._Database_Start_Date, _
                                           "List", True, enStatusType.Open)
            'Nilay (10-Oct-2015) -- End


            If dsPrd.Tables(0).Rows.Count > 0 Then
                With dgcolhDeductionPeriod
                    .ValueMember = "periodunkid"
                    .DisplayMember = "name"
                    .DataSource = dsPrd.Tables(0)
                    .DataPropertyName = "deductionperiodunkid"
                    .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                End With
            End If
            dgcolhEffDate.DataPropertyName = "dstartdate"
            objdgcolhDeductionPeriodId.DataPropertyName = "deductionperiodunkid"

            Dim dsLType As New DataSet
            Dim objLnAdv As New clsLoan_Advance
            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
            'If CInt(cboMode.SelectedIndex) = 0 Then
            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                'Nilay (08-Dec-2016) -- End
                dsLType = objLnAdv.GetLoanCalculationTypeList("List", True)
            Else
                dsLType = objLnAdv.GetLoanCalculationTypeList("List", True, True)
            End If

            With dgcolhLoanType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsLType.Tables(0)
                .DataPropertyName = "calctypeId"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            End With
            objdgcolhCalcTypeId.DataPropertyName = "calctypeId"

            'Nilay (08-Aug-2016) -- Start
            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
            Dim dsItype As New DataSet
            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                dsItype = objLnAdv.GetLoan_Interest_Calculation_Type("List", True)
            Else
                dsItype = objLnAdv.GetLoan_Interest_Calculation_Type("List", True, True)
            End If
            'Nilay (08-Aug-2016) -- END

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            dsLType = objLnAdv.GetLoan_Interest_Calculation_Type("List", True)
            With dgcolhInterestType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsItype.Tables(0) ''Nilay (08-Aug-2016) -- [Removed : dsLType.Tables(0)]
                .DataPropertyName = "interest_calctype_id"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            End With
            objdgcolhIntCalcTypeId.DataPropertyName = "interest_calctype_id"
            'Sohail (15 Dec 2015) -- End

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            dsLType = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            With dgcolhMappedHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsLType.Tables(0)
                .DataPropertyName = "mapped_tranheadunkid"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            End With
            objdgcolhMappedHeadUnkId.DataPropertyName = "mapped_tranheadunkid"
            'Sohail (29 Apr 2019) -- End

            dgcolhRate.DataPropertyName = "rate"
            dgcolhNoOfEMI.DataPropertyName = "noofinstallment"
            dgcolhInstallmentAmount.DataPropertyName = "installmentamt"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhIsEx.DataPropertyName = "IsEx"
            objdgcolhPendingunkid.DataPropertyName = "Pendingunkid"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            objdgcolhApprId.DataPropertyName = "ApprId"
            objdgcolhEmpId.DataPropertyName = "EmpId"
            objdgMatchInstallmentAmt.DataPropertyName = "instlamt"
            objdgcolhInterestAmt.DataPropertyName = "intrsamt"
            objdgcolhNetAmount.DataPropertyName = "netamt"
            'S.SANDEEP [01 AUG 2015] -- START
            dgcolhAppNo.DataPropertyName = "AppNo"
            'S.SANDEEP [01 AUG 2015] -- END

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
            If CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                dgvDataList.Columns(dgcolhLoanType.Index).Visible = False
                dgvDataList.Columns(dgcolhInterestType.Index).Visible = False
                dgvDataList.Columns(dgcolhRate.Index).Visible = False
                dgvDataList.Columns(dgcolhNoOfEMI.Index).Visible = False
                dgvDataList.Columns(dgcolhInstallmentAmount.Index).Visible = False
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                dgvDataList.Columns(dgcolhMappedHead.Index).Visible = False
                'Sohail (29 Apr 2019) -- End
            ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                dgvDataList.Columns(dgcolhLoanType.Index).Visible = True
                dgvDataList.Columns(dgcolhInterestType.Index).Visible = True
                dgvDataList.Columns(dgcolhRate.Index).Visible = True
                dgvDataList.Columns(dgcolhNoOfEMI.Index).Visible = True
                dgvDataList.Columns(dgcolhInstallmentAmount.Index).Visible = True
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                dgvDataList.Columns(dgcolhMappedHead.Index).Visible = True
                'Sohail (29 Apr 2019) -- End
            End If
            'Nilay (08-Dec-2016) -- End

            dgvDataList.DataSource = mdtTran

            dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhRate.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhRate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhNoOfEMI.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNoOfEMI.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhInstallmentAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhInstallmentAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhInstallmentAmount.DefaultCellStyle.Format = GUI.fmtCurrency

            Call SetGridColor()

            Call SetCollapseValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridDataSoruce", mstrModuleName)
        Finally
            objHead = Nothing 'Sohail (29 Apr 2019)
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            Dim StrSearching As String = String.Empty

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
            'Select Case CInt(cboMode.SelectedIndex)
            '    Case 0
            '        StrSearching &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 1 "
            '    Case 1
            '        StrSearching &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 2 "
            'End Select
            Select Case CInt(cboMode.SelectedValue)
                Case enLoanAdvance.LOAN
                    StrSearching &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 1 "
                Case enLoanAdvance.ADVANCE
                    StrSearching &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 2 "
            End Select
            'Nilay (08-Dec-2016) -- End

            If CInt(cboLoan.SelectedValue) > 0 Then
                StrSearching &= "AND ISNULL(lnloan_scheme_master.loanschemeunkid,0) = '" & CInt(cboLoan.SelectedValue) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hremployee_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboApprover.SelectedValue) > 0 Then
                StrSearching &= "AND lnloanapprover_master.lnapproverunkid = '" & CInt(cboApprover.SelectedValue) & "' "
            End If

            If CInt(cboCurrency.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.countryunkid = '" & CInt(cboCurrency.SelectedValue) & "' "
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    StrSearching &= "AND " & mstrAdvanceFilter
            'End If
            'Nilay (01-Mar-2016) -- End

            If StrSearching.Trim.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mdtTran = objPendingLoan.GetToAssignList(2, FinancialYear._Object._DatabaseName, _
            '                                         User._Object._Userunkid, _
            '                                         FinancialYear._Object._YearUnkid, _
            '                                         Company._Object._Companyunkid, _
            '                                         ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                         UserAccessLevel._AccessLevelFilterString, StrSearching)

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'mdtTran = objPendingLoan.GetToAssignList(FinancialYear._Object._DatabaseName, _
            '                                     User._Object._Userunkid, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     ConfigParameter._Object._UserAccessModeSetting, _
            '                                     True, _
            '                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                     2, StrSearching)

            mdtTran = objPendingLoan.GetToAssignList(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                     True, _
                                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                     enLoanApplicationStatus.APPROVED, StrSearching, mstrAdvanceFilter)
            'Nilay (08-Dec-2016) -- [enLoanApplicationStatus.APPROVED]

            'Nilay (01-Mar-2016) -- End
        
            'Nilay (10-Oct-2015) -- End

            Call SetGridDataSoruce()

            If mdtTran.Rows.Count <= 0 Then
                cboMode.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValueOnGrid()
        Try
            'If mdtTran IsNot Nothing Then
            '    Dim dtmp() As DataRow = Nothing
            '    If radChecked.Checked = True Then
            '        dtmp = mdtTran.Select("IsCheck= true AND IsGrp = false")
            '    ElseIf radAll.Checked = True Then
            '        dtmp = mdtTran.Select("IsGrp = false")
            '    End If

            '    For xRowIdx As Integer = 0 To dtmp.Length - 1
            '        If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
            '            dtmp(xRowIdx).Item("deductionperiod") = cboDeductionPeriod.Text
            '            dtmp(xRowIdx).Item("deductionperiodunkid") = CInt(cboDeductionPeriod.SelectedValue)
            '        End If
            '        If nudDuration.Value > 0 Then
            '            dtmp(xRowIdx).Item("noofinstallment") = nudDuration.Value
            '        End If
            '        If txtInstallmentAmt.Decimal > 0 Then
            '            dtmp(xRowIdx).Item("installmentamt") = Format(CDec(txtInstallmentAmt.Decimal), GUI.fmtCurrency)
            '        End If
            '        If txtLoanInterest.Decimal > 0 Then
            '            dtmp(xRowIdx).Item("rate") = txtLoanInterest.Text
            '        End If
            '        If nudDuration.Value > 1 Then
            '            dtmp(xRowIdx).Item("duration") = nudDuration.Value
            '        End If
            '        dtmp(xRowIdx).Item("ischange") = True
            '    Next
            '    mdtTran.AcceptChanges()
            '    Call SetGridDataSoruce()
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValueOnGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        'Sohail (15 Dec 2015) - [eIntRateCalcTypeID]
        Try
            '        RemoveHandler dgvDataList.CellValueChanged, AddressOf dgvDataList_CellValueChanged

            Dim objLoan_Advance As New clsLoan_Advance

            'S.SANDEEP [01 AUG 2015] -- START
            If eLnCalcType <= 0 Then
                If dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value.ToString.Trim.Length <= 0 Then
                    dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value = mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("orginstlamt")
                End If
                GoTo xlbl
            End If
            'S.SANDEEP [01 AUG 2015] -- END

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If eLnCalcType <> enLoanCalcId.No_Interest Then
            'Sohail (15 Dec 2015) -- End

            Dim mdecInstallmentAmount As Decimal = 0
            Dim mdecInstrAmount As Decimal = 0
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            Dim mdecTotInstallmentAmount As Decimal = 0
            Dim mdecTotIntrstAmount As Decimal = 0
            'Sohail (15 Dec 2015) -- End

            If dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value.ToString.Trim.Length <= 0 Then
                dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value = 1
            End If

            Dim dblEffMonth As Double = 0
            If CDbl(dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value) <= 0 Then
                dblEffMonth = 1
            Else
                dblEffMonth = CDbl(dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value)
            End If

            Dim dtEndDate As Date = DateAdd(DateInterval.Month, _
                                           dblEffMonth, _
                                            CDate(dgvDataList.CurrentRow.Cells(dgcolhEffDate.Index).Value))

            Dim mdecRate As Decimal = 0
            If dgvDataList.CurrentRow.Cells(dgcolhRate.Index).Value.ToString = "" Then
                mdecRate = 0
            Else
                mdecRate = CDec(dgvDataList.CurrentRow.Cells(dgcolhRate.Index).Value)
            End If


            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value), _
            '                                                                CInt(DateDiff(DateInterval.Day, CDate(dgvDataList.CurrentRow.Cells(dgcolhEffDate.Index).Value), dtEndDate.Date)), _
            '                                                                mdecRate, _
            '                                                                eLnCalcType, CInt(dblEffMonth), mdecInstrAmount, mdecInstallmentAmount)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If CInt(dgvDataList.CurrentRow.Cells(dgcolhDeductionPeriod.Index).Value) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dgvDataList.CurrentRow.Cells(dgcolhDeductionPeriod.Index).Value)
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            'Sohail (29 Jan 2016) -- Start
            'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
            'objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value) _
            '                                                , CInt(DateDiff(DateInterval.Day, CDate(dgvDataList.CurrentRow.Cells(dgcolhEffDate.Index).Value), dtEndDate.Date.AddDays(1))) _
            '                                                , mdecRate _
            '                                                , eLnCalcType _
            '                                                , eIntRateCalcTypeID _
            '                                                , CInt(dblEffMonth) _
            '                                                , CInt(DateDiff(DateInterval.Day, dtFPeriodStart, dtFPeriodEnd.Date.AddDays(1))) _
            '                                                , CDec(mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("orginstlamt")) _
            '                                                , mdecInstrAmount _
            '                                                , mdecInstallmentAmount _
            '                                                , mdecTotIntrstAmount _
            '                                                , mdecTotInstallmentAmount _
            '                                                )
            objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value) _
                                                           , CInt(DateDiff(DateInterval.Day, CDate(dgvDataList.CurrentRow.Cells(dgcolhEffDate.Index).Value), dtEndDate.Date)) _
                                                            , mdecRate _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(dblEffMonth) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , CDec(mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("orginstlamt")) _
                                                            , mdecInstrAmount _
                                                            , mdecInstallmentAmount _
                                                            , mdecTotIntrstAmount _
                                                            , mdecTotInstallmentAmount _
                                                            )
            'Sohail (29 Jan 2016) -- End
            'Sohail (15 Dec 2015) -- End

            'Nilay (15-Dec-2015) -- Start
            'mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("instlnum") = CInt(dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value)
            'Nilay (15-Dec-2015) -- End
            dgvDataList.CurrentRow.Cells(objdgMatchInstallmentAmt.Index).Value = mdecInstallmentAmount
            dgvDataList.CurrentRow.Cells(objdgcolhInterestAmt.Index).Value = mdecInstrAmount
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'dgvDataList.CurrentRow.Cells(objdgcolhNetAmount.Index).Value = mdecInstrAmount + CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value)
            'dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value = CDec(Format(mdecInstallmentAmount, GUI.fmtCurrency))
            dgvDataList.CurrentRow.Cells(objdgcolhNetAmount.Index).Value = mdecTotIntrstAmount + mdecTotInstallmentAmount
            dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value = CDec(Format(mdecInstallmentAmount, GUI.fmtCurrency))
            'Sohail (15 Dec 2015) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'Else
            '    'Nilay (15-Dec-2015) -- Start
            '    'Requested by Rutta

            '    'Dim intNoOfEMI As Integer = 1
            '    'mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("instlnum") = CInt(dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value)
            '    'If dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value.ToString.Trim.Length <= 0 Then
            '    '    dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value = mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("orginstlamt")
            '    'End If

            '    'If blnIsDurationChange = False Then

            '    '    'S.SANDEEP [01 AUG 2015] -- START
            '    '    Dim dblAmountValue As Decimal
            '    '    If CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) = 0 Then
            '    '        dblAmountValue = CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value)
            '    '    Else
            '    '        dblAmountValue = CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value)
            '    '    End If
            '    '    'S.SANDEEP [01 AUG 2015] -- END

            '    '    'Nilay (12-Dec-2015) -- Start
            '    '    'If CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) > 0 Then
            '    '    '    intNoOfEMI = CInt(IIf(CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) = 0, 0, CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value) / CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value)))
            '    '    'Else
            '    '    '    intNoOfEMI = 1
            '    '    'End If
            '    'If CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) > 0 Then
            '    '        intNoOfEMI = CInt(IIf(CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) = 0, 0, CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value) / CDec(mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("orginstlamt"))))
            '    'Else
            '    '    intNoOfEMI = 1
            '    'End If
            '    '    'Nilay (12-Dec-2015) -- End

            '    '    'S.SANDEEP [01 AUG 2015] -- START
            '    '    'If CDec(CDec(IIf(CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) = 0, 0, CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value) / CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value)))) > intNoOfEMI Then
            '    '    '    intNoOfEMI += 1
            '    '    'End If

            '    '    'Nilay (12-Dec-2015) -- Start
            '    '    'If CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) > 0 Then
            '    '    '    If CDec(CDec(IIf(CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) = 0, 0, CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value) / CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value)))) > intNoOfEMI Then
            '    '    '        intNoOfEMI += 1
            '    '    '    End If
            '    '    'End If
            '    '    'If CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) > 0 Then
            '    '    '    If CDec(CDec(IIf(CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value) = 0, 0, CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value) / CDec(mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("orginstlamt"))))) > intNoOfEMI Then
            '    '    '        intNoOfEMI += 1
            '    '    '    End If
            '    '    'End If
            '    '    'Nilay (12-Dec-2015) -- End

            '    '    'S.SANDEEP [01 AUG 2015] -- END

            '    '    dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value = intNoOfEMI
            '    '    mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("instlnum") = intNoOfEMI
            '    '    mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("instlamt") = CDec(dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value)
            '    'Else
            '    '    Dim decInstallmentAmt As Decimal
            '    '    If intNoOfEMI > 0 Then
            '    '        decInstallmentAmt = CDec(IIf(intNoOfEMI = 0, 0, CDec(dgvDataList.CurrentRow.Cells(dgcolhAmount.Index).Value) / CInt(dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value)))
            '    '    Else
            '    '        decInstallmentAmt = 0
            '    'End If

            '    '    mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("instlamt") = decInstallmentAmt
            '    '    'dgvDataList.CurrentRow.Cells(objdgcolhInstallmentAmtTag.Index).Value = decInstallmentAmt
            '    '    dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value = CDec(Format(decInstallmentAmt, GUI.fmtCurrency))
            '    'End If
            '    dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value = mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("instlnum")
            '    dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).Value = Format((mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("orginstlamt")), GUI.fmtCurrency)

            '    'Nilay (15-Dec-2015) -- End

            'End If
            'Sohail (15 Dec 2015) -- End
xlbl:
            'AddHandler dgvDataList.CellValueChanged, AddressOf dgvDataList_CellValueChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_Projected_Loan_Balance", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Assignment period is mandatory information. Please select assignment period to continue."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
            End If
            Dim dtAssignDate As String = ""
            Dim objPrd As New clscommom_period_Tran
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Nilay (10-Oct-2015) -- End
            dtAssignDate = eZeeDate.convertDate(objPrd._Start_Date).ToString
            objPrd = Nothing

            Dim dtmp() As DataRow = Nothing
            dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Please check atleast one approved loan to assign."), enMsgBoxStyle.Information)
                dgvDataList.Focus()
                Return False
            End If

            dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND dstartdate = ''")
            If dtmp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), enMsgBoxStyle.Information)
                dgvDataList.Focus()
                Return False
            End If

            'Hemant (10 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT#4217(Sport Pesa) - employees were posted loan applications on September period which payroll is already done and the applications were approved.they now found out that they cant assign them in October.
            'dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND mdate < '" & dtAssignDate & "'")
            'If dtmp.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Effective date cannot be less then the assigned period date.") & "(" & eZeeDate.convertDate(dtAssignDate.ToString).ToShortDateString & ")", enMsgBoxStyle.Information)
            '    dgvDataList.Focus()
            '    Return False
            'End If
            'Hemant (10 Oct 2019) -- End

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
            'If CInt(cboMode.SelectedIndex) = 0 Then
            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                'Nilay (08-Dec-2016) -- End
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <= 0")
                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type to continue."), enMsgBoxStyle.Information)
                    dgvDataList.Focus()
                    Return False
                End If

                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> " & enLoanCalcId.No_Interest & " AND interest_calctype_Id <= 0")
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> " & enLoanCalcId.No_Interest & " AND calctypeId <> " & enLoanCalcId.No_Interest_With_Mapped_Head & " AND interest_calctype_Id <= 0")
                'Sohail (29 Apr 2019) -- End
                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Interest calculation type is mandatory information. Please select Interest calculation type to continue."), enMsgBoxStyle.Information)
                    dgvDataList.Focus()
                    Return False
                End If
                'Sohail (15 Dec 2015) -- End

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId = " & enLoanCalcId.No_Interest_With_Mapped_Head & " AND mapped_tranheadunkid <= 0")
                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Please select Mapped Head to continue."), enMsgBoxStyle.Information)
                    dgvDataList.Focus()
                    Return False
                End If
                'Sohail (29 Apr 2019) -- End

                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> '" & enLoanCalcId.No_Interest & "' AND rate = ''")
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> '" & enLoanCalcId.No_Interest & "' AND (rate IS NULL OR rate = '')")
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> '" & enLoanCalcId.No_Interest & "' AND calctypeId <> " & enLoanCalcId.No_Interest_With_Mapped_Head & " AND (rate IS NULL OR rate = '')")
                'Sohail (29 Apr 2019) -- End
                'Sohail (15 Dec 2015) -- End
                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Loan rate is mandatory information. Please enter loan rate to continue."), enMsgBoxStyle.Information)
                    dgvDataList.Focus()
                    Return False
                End If

                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND instlnum <= 0")
                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, No Of Installment cannot be less than Zero. Please enter correct no of installment to continue."), enMsgBoxStyle.Information)
                    dgvDataList.Focus()
                    Return False
                End If

                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND instlamt <= 0")
                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Installment amount cannot be less than Zero. Please enter correct installment amount to continue."), enMsgBoxStyle.Information)
                    dgvDataList.Focus()
                    Return False
                End If
            End If

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim dtTable() As DataRow = Nothing
            dtTable = mdtTran.Select("IsCheck = true AND IsGrp = false")
            If dtTable.Length > 0 Then
                For Each dtRow As DataRow In dtTable

                    Dim objPayment As New clsPayment_tran
                    'Nilay (10-Oct-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim ds As DataSet = objPayment.GetListByPeriod("List", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(dtRow.Item("deductionperiodunkid")), clsPayment_tran.enPayTypeId.PAYMENT, dtRow.Item("EmpId").ToString, " AND 1 = 1 ")
                    Dim ds As DataSet = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, _
                                                                   User._Object._Userunkid, _
                                                                   FinancialYear._Object._YearUnkid, _
                                                                   Company._Object._Companyunkid, _
                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                                   True, _
                                                                   ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                   "List", _
                                                                   clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                                   CInt(dtRow.Item("deductionperiodunkid")), _
                                                                   clsPayment_tran.enPayTypeId.PAYMENT, _
                                                                   dtRow.Item("EmpId").ToString, False)
                    'Nilay (10-Oct-2015) -- End


                    If ds.Tables("List").Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot assign Loan/Advance. Reason: Payslip Payment is already done for selected period for Employee") & " : " & dtRow.Item("Employee").ToString.Trim, enMsgBoxStyle.Information)
                        Return False
                    End If

                    Dim objDPeriod As New clscommom_period_Tran
                    'Nilay (10-Oct-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objDPeriod._Periodunkid = CInt(dtRow.Item("deductionperiodunkid"))
                    objDPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dtRow.Item("deductionperiodunkid"))
                    'Nilay (10-Oct-2015) -- End

                    Dim objTnA As New clsTnALeaveTran
                    If objTnA.IsPayrollProcessDone(CInt(dtRow.Item("deductionperiodunkid")), dtRow.Item("EmpId").ToString, objDPeriod._End_Date, enModuleReference.Payroll) = True Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot assign Loan/Advance. Reason: Payroll process is already done for the last date of selected deduction period for Employee.") & " : " & dtRow.Item("Employee").ToString.Trim, enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot assign Loan/Advance. Reason: Payroll process is already done for the last date of selected deduction period for Employee.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 36, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Return False
                    End If
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Dim objPendingLoan As New clsProcess_pending_loan
                    'Sohail (21 Mar 2018) -- Start
                    'TRA Issue - Support Issue Id # 0002126 : Duplicate loan transactions are coming at time of salary in 70.2.
                    'objPendingLoan._Processpendingloanunkid = CInt(dtRow.Item("objdgcolhPendingunkid").ToString)
                    objPendingLoan._Processpendingloanunkid = CInt(dtRow.Item("Pendingunkid"))
                    'Sohail (21 Mar 2018) -- End
                    Dim objLoanScheme As New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = objPendingLoan._Loanschemeunkid
                    'Hemant (03 Apr 2019) -- Start
                    'ISSUE#3684: Error "Column 'dgcolhNoOfEMI' does not belong to table List" on gloabal loan assign    
                    'If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(dtRow.Item("dgcolhNoOfEMI").ToString) > objLoanScheme._MaxNoOfInstallment Then
                    If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(dtRow.Item("noofinstallment").ToString) > objLoanScheme._MaxNoOfInstallment Then
                        'Hemant (03 Apr 2019) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 30, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 31, " Scheme and Application No is ") & objPendingLoan._Application_No & ".", enMsgBoxStyle.Information)
                        Return False
                    End If
                    'Varsha (25 Nov 2017) -- End

                    'Sohail (21 Mar 2018) -- Start
                    'TRA Issue - Support Issue Id # 0002126 : Duplicate loan transactions are coming at time of salary in 70.2.
                    If CInt(cboMode.SelectedValue) = CInt(enLoanAdvance.LOAN) Then 'Sohail (16 May 2018)
                    If objLoan_Advance.GetExistLoan(objPendingLoan._Loanschemeunkid, CInt(dtRow.Item("EmpId")), True) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, you can not assign loan for the selected scheme: Reason, The selected loan scheme is already assigned and having (In-progress or On hold) status for selected period for Employee") & " : " & dtRow.Item("Employee").ToString.Trim & " " & Language.getMessage(mstrModuleName, 33, "for Scheme") & " " & objLoanScheme._Name & ". " & Language.getMessage(mstrModuleName, 34, "Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations."), enMsgBoxStyle.Information)
                        Return False
                    End If

                        'Sohail (17 Dec 2019) -- Start
                        'Mukuba University # 0004115: Option for EOC to affect number of installments on loans.
                        If CInt(dtRow.Item("noofinstallment")) > 1 Then
                            Dim dtLoanEnd As Date = objDPeriod._Start_Date.Date.AddMonths(CInt(dtRow.Item("noofinstallment"))).AddDays(-1)
                            Dim objEmpDates As New clsemployee_dates_tran
                            Dim dsEmp As DataSet = objEmpDates.GetEmployeeEndDates(dtLoanEnd, dtRow.Item("EmpId").ToString)
                            If dsEmp.Tables(0).Rows.Count > 0 Then
                                Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, objDPeriod._Start_Date.Date, eZeeDate.convertDate(dsEmp.Tables(0).Rows(0).Item("finalenddate").ToString).AddDays(1)))
                                If CInt(dtRow.Item("noofinstallment")) > intEmpTenure Then
                                    'Hemant (24 Nov 2021) -- Start
                                    'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
                                    If ConfigParameter._Object._SkipEOCValidationOnLoanTenure = False Then
                                        'Hemant (24 Nov 2021) -- End
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "Sorry, Loan tenure should not be greater than employee tenure month") & " [" & intEmpTenure & "].", enMsgBoxStyle.Information)
                                    Return False
                                    End If 'Hemant (24 Nov 2021)
                                End If
                            End If
                        End If
                        'Sohail (17 Dec 2019) -- End
                    End If 'Sohail (16 May 2018)
                    'Sohail (21 Mar 2018) -- End
                    'Sohail (16 May 2018) -- Start
                    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                    If ConfigParameter._Object._Advance_NetPayPercentage > 0 AndAlso CInt(cboMode.SelectedValue) = CInt(enLoanAdvance.ADVANCE) Then
                        If objLoan_Advance.GetExistLoan(0, CInt(dtRow.Item("EmpId")), False) = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, you can not assign Advance: Reason, The Advance is already assigned and having (In-progress or On hold) status for selected period for Employee") & " : " & dtRow.Item("Employee").ToString.Trim, enMsgBoxStyle.Information)
                            Return False
                        End If
                    End If
                    'Sohail (16 May 2018) -- End
                Next
            End If
            'Sohail (07 May 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub SetValue(ByVal xrow As DataRow)
        Try

            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            mdtStartDate = objPeriod._Start_Date
            mdtEndDate = objPeriod._End_Date
            'Nilay (21-Jul-2016) -- End

            objLoan_Advance._Deductionperiodunkid = CInt(xrow.Item("deductionperiodunkid"))
            objLoan_Advance._Effective_Date = eZeeDate.convertDate(xrow.Item("mdate").ToString).Date
            objLoan_Advance._Employeeunkid = CInt(xrow.Item("EmpId"))
            objLoan_Advance._Isbrought_Forward = False
            objLoan_Advance._Loan_Purpose = txtPurpose.Text
            If ConfigParameter._Object._LoanVocNoType = 1 Then
                objLoan_Advance._Loanvoucher_No = ""
            Else
                objLoan_Advance._Loanvoucher_No = txtPrefix.Text.Trim & nudManualNo.Value.ToString
            End If
            objLoan_Advance._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objLoan_Advance._LoanStatus = 1
            objLoan_Advance._Processpendingloanunkid = CInt(xrow.Item("Pendingunkid"))
            objLoan_Advance._Approverunkid = CInt(xrow.Item("ApprId"))
            objLoan_Advance._CountryUnkid = CInt(xrow.Item("countryunkid"))
            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
            'If CInt(cboMode.SelectedIndex) = 0 Then 'LOAN
            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                'Nilay (08-Dec-2016) -- End
                objLoan_Advance._Isloan = True
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'Select Case CInt(xrow.Item("calctypeId"))
                '    Case enLoanCalcId.Simple_Interest
                '        objLoan_Advance._Calctype_Id = enLoanCalcId.Simple_Interest '1
                '    Case enLoanCalcId.Reducing_Amount
                '        objLoan_Advance._Calctype_Id = enLoanCalcId.Reducing_Amount '3
                '    Case enLoanCalcId.No_Interest
                '        objLoan_Advance._Calctype_Id = enLoanCalcId.No_Interest '4
                'End Select
                objLoan_Advance._Calctype_Id = CInt(xrow.Item("calctypeId"))
                objLoan_Advance._Interest_Calctype_Id = CInt(xrow.Item("interest_calctype_Id"))
                'Sohail (15 Dec 2015) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                objLoan_Advance._Mapped_TranheadUnkid = CInt(xrow.Item("mapped_tranheadunkid"))
                'Sohail (29 Apr 2019) -- End
                objLoan_Advance._Loanschemeunkid = CInt(xrow.Item("GrpId"))
                objLoan_Advance._Loan_Amount = CDec(xrow.Item("Amount"))
                If xrow.Item("rate").ToString.Trim.Length <= 0 Then
                    objLoan_Advance._Interest_Rate = CDec(0)
                Else
                    objLoan_Advance._Interest_Rate = CDec(xrow.Item("rate"))
                End If
                objLoan_Advance._Loan_Duration = CInt(xrow.Item("noofinstallment"))
                objLoan_Advance._Interest_Amount = CDec(xrow.Item("intrsamt")) / CDec(xrow.Item("PaidExRate"))
                objLoan_Advance._Net_Amount = CDec(xrow.Item("netamt")) / CDec(xrow.Item("PaidExRate"))
                objLoan_Advance._Emi_Tenure = CInt(xrow.Item("noofinstallment"))
                objLoan_Advance._Emi_Amount = CDec(xrow.Item("instlamt")) / CDec(xrow.Item("PaidExRate"))
                'Nilay (08-Dec-2016) -- Start
                'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
                'ElseIf CInt(cboMode.SelectedIndex) = 1 Then 'ADVANCE
            ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                'Nilay (08-Dec-2016) -- End
                objLoan_Advance._Isloan = False
                objLoan_Advance._Advance_Amount = CDec(xrow.Item("Amount"))
            End If
            objLoan_Advance._Exchange_rate = CDec(xrow.Item("PaidExRate"))
            objLoan_Advance._Basecurrency_amount = CDec(xrow.Item("Amount")) / CDec(xrow.Item("PaidExRate"))
            If CBool(xrow.Item("IsEx")) = True Then
                objLoan_Advance._Balance_Amount = CDec(xrow.Item("Amount")) / CDec(xrow.Item("PaidExRate"))
                objLoan_Advance._Balance_AmountPaidCurrency = CDec(xrow.Item("Amount")) 'Sohail (07 May 2015)
            End If
            If xrow.Item("rate").ToString.Trim.Length <= 0 Then
                objLoan_Advance._RateOfInterest = CDec(0)
            Else
                objLoan_Advance._RateOfInterest = CDec(xrow.Item("rate"))
            End If
            objLoan_Advance._NoOfInstallments = CInt(xrow.Item("noofinstallment"))
            objLoan_Advance._InstallmentAmount = CDec(xrow.Item("instlamt"))
            objLoan_Advance._Userunkid = User._Object._Userunkid
            objLoan_Advance._Voiduserunkid = -1
            objLoan_Advance._Voiddatetime = Nothing
            objLoan_Advance._Isvoid = False
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            objLoan_Advance._Opening_balance = CDec(xrow.Item("Amount")) / CDec(xrow.Item("PaidExRate"))
            objLoan_Advance._Opening_balancePaidCurrency = CDec(xrow.Item("Amount"))
            'Sohail (07 May 2015) -- End
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            objLoan_Advance._EMI_Principal_amount = CDec(xrow.Item("instlamt")) - CDec(xrow.Item("intrsamt"))
            objLoan_Advance._EMI_Interest_amount = CDec(xrow.Item("intrsamt"))
            'Sohail (15 Dec 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 28, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Apr 2019) -- End

    'Hemant (30 Aug 2019) -- Start
    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
    Private Sub Send_Notification()
        Try
            If objEmailList.Count > 0 Then
                RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Stop()
                Dim objSendMail As New clsSendMail
                For Each obj In objEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                    Try
                        objSendMail.SendMail(Company._Object._Companyunkid)
                    Catch ex As Exception

                    End Try
                Next
                objEmailList.Clear()
                AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Start()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If objEmailList.Count > 0 Then
                objEmailList.Clear()
            End If
        End Try
    End Sub
    'Hemant (30 Aug 2019) -- End


#End Region

#Region " Form's Events "

    Private Sub frmGlobalAssignLoanAdvance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLoan_Advance = Nothing : objPendingLoan = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalAssignLoanAdvance_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmGlobalAssignLoanAdvance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objLoan_Advance = New clsLoan_Advance : objPendingLoan = New clsProcess_pending_loan
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            mblnIsFormLoad = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalAssignLoanAdvance_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgvDataList_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataList.CellValueChanged
        Try
            If mblnIsFormLoad = True Then
                If e.RowIndex <= -1 Then Exit Sub

                RemoveHandler dgvDataList.CellValueChanged, AddressOf dgvDataList_CellValueChanged

                Select Case e.ColumnIndex
                    Case dgcolhRate.Index
                        If dgvDataList.Rows(e.RowIndex).Cells(dgcolhRate.Index).Value.ToString <> "" Then
                            If CDec(dgvDataList.Rows(e.RowIndex).Cells(dgcolhRate.Index).Value) > 100 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Rate of Interest cannot exceed 100. Please enter correct rate of interest."), enMsgBoxStyle.Information)
                                dgvDataList.Rows(e.RowIndex).Cells(dgcolhRate.Index).Value = 0
                                Exit Sub
                            End If
                        End If
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'Call Calculate_Projected_Loan_Balance(CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value), enLoanCalcId), False)
                        Call Calculate_Projected_Loan_Balance(CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value), enLoanCalcId), CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhIntCalcTypeId.Index).Value), enLoanInterestCalcType), False)
                        'Sohail (15 Dec 2015) -- End
                    Case dgcolhNoOfEMI.Index, dgcolhInstallmentAmount.Index
                        If e.ColumnIndex = dgcolhNoOfEMI.Index Then
                            'Sohail (15 Dec 2015) -- Start
                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                            'Call Calculate_Projected_Loan_Balance(CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value), enLoanCalcId), True)
                            If dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value.ToString = "" OrElse CInt(dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value) <= 0 Then
                                dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).Value = 1
                            End If
                            Call Calculate_Projected_Loan_Balance(CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value), enLoanCalcId), CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhIntCalcTypeId.Index).Value), enLoanInterestCalcType), True)
                            'Sohail (15 Dec 2015) -- End
                        Else
                            'Sohail (15 Dec 2015) -- Start
                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                            'Call Calculate_Projected_Loan_Balance(CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value), enLoanCalcId), False)
                            Call Calculate_Projected_Loan_Balance(CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value), enLoanCalcId), CType(CInt(dgvDataList.CurrentRow.Cells(objdgcolhIntCalcTypeId.Index).Value), enLoanInterestCalcType), False)
                            'Sohail (15 Dec 2015) -- End
                        End If
                End Select

                AddHandler dgvDataList.CellValueChanged, AddressOf dgvDataList_CellValueChanged

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvDataList_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDataList.EditingControlShowing
        Try
            Select Case dgvDataList.CurrentCell.ColumnIndex
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'Case dgcolhDeductionPeriod.Index, dgcolhLoanType.Index
                Case dgcolhDeductionPeriod.Index, dgcolhLoanType.Index, dgcolhInterestType.Index, dgcolhMappedHead.Index
                    'Sohail (29 Apr 2019) - [dgcolhMappedHead]
                    'Sohail (15 Dec 2015) -- End
                    If e.Control IsNot Nothing Then
                        Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                        RemoveHandler cbo.SelectionChangeCommitted, AddressOf Combo_SelectionChangeCommitted
                        AddHandler cbo.SelectionChangeCommitted, AddressOf Combo_SelectionChangeCommitted
                    End If
                Case dgcolhInstallmentAmount.Index
                    If e.Control IsNot Nothing Then
                        Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                        RemoveHandler txt.KeyPress, AddressOf tb_keypress
                        AddHandler txt.KeyPress, AddressOf tb_keypress
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_EditingControlShowing", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvDataList_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataList.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'Case dgcolhDeductionPeriod.Index, dgcolhLoanType.Index
                Case dgcolhDeductionPeriod.Index, dgcolhLoanType.Index, dgcolhInterestType.Index, dgcolhMappedHead.Index
                    'Sohail (29 Apr 2019) - [dgcolhMappedHead]
                    'Sohail (15 Dec 2015) -- End
                    If CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = False Then
                        SendKeys.Send("{F2}")
                    End If
                Case dgcolhRate.Index, dgcolhNoOfEMI.Index, dgcolhInstallmentAmount.Index
                    SendKeys.Send("{F2}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_CellEnter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvDataList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvDataList.DataError

    End Sub

    Private Sub dgvDataList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataList.CellContentClick, dgvDataList.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvDataList.IsCurrentCellDirty Then
                Me.dgvDataList.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        'If dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value.ToString = "▬" Then
                        '    dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "+"
                        'Else
                        '    dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "▬"
                        'End If

                        If dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvDataList.RowCount - 1
                            If CInt(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvDataList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvDataList.Rows(i).Visible = False Then
                                    dgvDataList.Rows(i).Visible = True
                                Else
                                    dgvDataList.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case 1
                        For i = e.RowIndex + 1 To dgvDataList.RowCount - 1
                            Dim blnFlg As Boolean = CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value)
                            If CInt(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvDataList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgvDataList.Rows(i).Cells(objdgcolhSelect.Index).Value = blnFlg
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvDataList_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvDataList.DataBindingComplete
        Try
            If dgvDataList.DataSource IsNot Nothing AndAlso dgvDataList.RowCount > 0 Then
                For i As Integer = 0 To dgvDataList.RowCount - 1
                    If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) Then
                        If TypeOf CType(dgvDataList.Rows(i).Cells(dgcolhDeductionPeriod.Index), DataGridViewComboBoxCell) Is DataGridViewComboBoxCell Then
                            With CType(dgvDataList.Rows(i).Cells(dgcolhDeductionPeriod.Index), DataGridViewComboBoxCell)
                                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                                .ReadOnly = True
                            End With
                        End If

                        If TypeOf CType(dgvDataList.Rows(i).Cells(dgcolhLoanType.Index), DataGridViewComboBoxCell) Is DataGridViewComboBoxCell Then
                            With CType(dgvDataList.Rows(i).Cells(dgcolhLoanType.Index), DataGridViewComboBoxCell)
                                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                                .ReadOnly = True
                            End With
                        End If
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        If TypeOf CType(dgvDataList.Rows(i).Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell) Is DataGridViewComboBoxCell Then
                            With CType(dgvDataList.Rows(i).Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell)
                                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                                .ReadOnly = True
                            End With
                        End If
                        'Sohail (15 Dec 2015) -- End
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        If TypeOf CType(dgvDataList.Rows(i).Cells(dgcolhMappedHead.Index), DataGridViewComboBoxCell) Is DataGridViewComboBoxCell Then
                            With CType(dgvDataList.Rows(i).Cells(dgcolhMappedHead.Index), DataGridViewComboBoxCell)
                                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                                .ReadOnly = True
                            End With
                        End If
                        'Sohail (29 Apr 2019) -- End
                    Else
                        'Nilay (08-Dec-2016) -- Start
                        'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
                        'If CInt(cboMode.SelectedIndex) = 0 Then
                        If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                            'Nilay (08-Dec-2016) -- End
                            If CInt(dgvDataList.Rows(i).Cells(objdgcolhCalcTypeId.Index).Value) <= 0 Then
                                dgvDataList.Rows(i).Cells(dgcolhRate.Index).ReadOnly = True
                                dgvDataList.Rows(i).Cells(dgcolhNoOfEMI.Index).ReadOnly = True
                                dgvDataList.Rows(i).Cells(dgcolhInstallmentAmount.Index).ReadOnly = True
                            End If
                        Else
                            If TypeOf CType(dgvDataList.Rows(i).Cells(dgcolhLoanType.Index), DataGridViewComboBoxCell) Is DataGridViewComboBoxCell Then
                                With CType(dgvDataList.Rows(i).Cells(dgcolhLoanType.Index), DataGridViewComboBoxCell)
                                    .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                                    'Nilay (01-Mar-2016) -- Start
                                    .DropDownWidth = 250
                                    'Nilay (01-Mar-2016) -- End
                                    .ReadOnly = True
                                End With
                            End If
                            'Sohail (15 Dec 2015) -- Start
                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                            If TypeOf CType(dgvDataList.Rows(i).Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell) Is DataGridViewComboBoxCell Then
                                With CType(dgvDataList.Rows(i).Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell)
                                    .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                                    .ReadOnly = True
                                End With
                            End If
                            'Sohail (15 Dec 2015) -- End
                            'Sohail (29 Apr 2019) -- Start
                            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                            If TypeOf CType(dgvDataList.Rows(i).Cells(dgcolhMappedHead.Index), DataGridViewComboBoxCell) Is DataGridViewComboBoxCell Then
                                With CType(dgvDataList.Rows(i).Cells(dgcolhMappedHead.Index), DataGridViewComboBoxCell)
                                    .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                                    .ReadOnly = True
                                End With
                            End If
                            'Sohail (29 Apr 2019) -- End
                            dgvDataList.Rows(i).Cells(dgcolhRate.Index).ReadOnly = True
                            dgvDataList.Rows(i).Cells(dgcolhNoOfEMI.Index).ReadOnly = True
                            dgvDataList.Rows(i).Cells(dgcolhInstallmentAmount.Index).ReadOnly = True
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, _
        cboApprover.SelectedIndexChanged, _
        cboMode.SelectedIndexChanged, _
        cboLoan.SelectedIndexChanged, _        
        cboMappedHead.SelectedIndexChanged
        'Sohail (29 Apr 2019) - [cboMappedHead]
        Dim cbo As ComboBox = CType(sender, ComboBox) 'Sohail (29 Apr 2019)

        'Nilay (27-Oct-2016) -- Start
        'Enhancements: Searchable Dropdown
        'Sohail (29 Apr 2019) -- Start
        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
        'If CType(sender, ComboBox).Name.ToUpper = "CBOEMPLOYEE" Then
        '    If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)
        'End If
        If cbo.Name = cboEmployee.Name OrElse cbo.Name = cboLoan.Name OrElse cbo.Name = cboMappedHead.Name Then
            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If
        End If
        'Sohail (29 Apr 2019) -- End
        'Nilay (27-Oct-2016) -- End

        If mblnIsFormLoad = False Then Exit Sub

        Select Case CType(sender, ComboBox).Name.ToUpper
            Case "CBOMODE"
                Select Case CInt(cboMode.SelectedValue) 'Nilay (08-Dec-2016) -- [REPLACED cboMode.SelectedIndex]
                    Case enLoanAdvance.LOAN 'Nilay (08-Dec-2016) -- [REPLACED 0] 
                        cboLoan.SelectedValue = 0
                        cboLoan.Enabled = True
                        objbtnSearchScheme.Enabled = True
                        'Nilay (08-Aug-2016) -- Start
                        'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
                        cboGLoanCalcType.Enabled = True
                        cboGInterestCalcType.Enabled = True
                        txtGRate.Enabled = True
                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        radApplyToAll.Enabled = True
                        radApplyToChecked.Enabled = True
                        btnApply.Enabled = True
                        'Nilay (04-Nov-2016) -- End
                        'Nilay (08-Aug-2016) -- End
                    Case enLoanAdvance.ADVANCE 'Nilay (08-Dec-2016) -- [REPLACED 1]
                        cboLoan.SelectedValue = 0
                        cboLoan.Enabled = False
                        objbtnSearchScheme.Enabled = False
                        'Nilay (08-Aug-2016) -- Start
                        'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
                        cboGLoanCalcType.Enabled = False
                        cboGInterestCalcType.Enabled = False
                        txtGRate.Enabled = False
                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        radApplyToAll.Enabled = False
                        radApplyToChecked.Enabled = False
                        btnApply.Enabled = False
                        'Nilay (04-Nov-2016) -- End
                        'Nilay (08-Aug-2016) -- End
                End Select
        End Select
    End Sub

    Private Sub Combo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'RemoveHandler dgvDataList.CellValueChanged, AddressOf dgvDataList_CellValueChanged

            Dim cbo As ComboBox = TryCast(sender, ComboBox)
            If cbo IsNot Nothing Then
                If dgvDataList.CurrentCell.ColumnIndex = dgcolhDeductionPeriod.Index Then

                    'S.SANDEEP [01 AUG 2015] -- START
                    dgvDataList.CurrentRow.Cells(objdgcolhDeductionPeriodId.Index).Value = CInt(cbo.SelectedValue)
                    'S.SANDEEP [01 AUG 2015] -- END

                    If CInt(cbo.SelectedValue) > 0 Then
                        Dim objPeriod As New clscommom_period_Tran
                        'Nilay (10-Oct-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objPeriod._Periodunkid = CInt(cbo.SelectedValue)
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cbo.SelectedValue)
                        'Nilay (10-Oct-2015) -- End

                        dgvDataList.CurrentRow.Cells(dgcolhEffDate.Index).Value = objPeriod._Start_Date.ToShortDateString
                        mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("mdate") = eZeeDate.convertDate(objPeriod._Start_Date).ToString

                        Dim objExRate As New clsExchangeRate : Dim dsList As New DataSet
                        dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._Start_Date.Date, True)
                        If dsList.Tables("ExRate").Rows.Count > 0 Then
                            mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("PaidExRate") = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                        End If

                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        Call Calculate_Projected_Loan_Balance(CType(dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value, enLoanCalcId), CType(dgvDataList.CurrentRow.Cells(objdgcolhIntCalcTypeId.Index).Value, enLoanInterestCalcType), False)
                        'Sohail (15 Dec 2015) -- End

                        objPeriod = Nothing : objExRate = Nothing
                    Else
                        mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("PaidExRate") = 1
                        dgvDataList.CurrentRow.Cells(dgcolhEffDate.Index).Value = ""
                        mdtTran.Rows(dgvDataList.CurrentRow.Index).Item("mdate") = ""
                    End If
                ElseIf dgvDataList.CurrentCell.ColumnIndex = dgcolhLoanType.Index Then

                    If dgvDataList.CurrentRow.Cells(dgcolhEffDate.Index).Value.ToString = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), enMsgBoxStyle.Information)
                        cbo.SelectedValue = 0
                        Exit Sub
                    End If

                    'S.SANDEEP [01 AUG 2015] -- START
                    dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value = CInt(cbo.SelectedValue)
                    'S.SANDEEP [01 AUG 2015] -- END

                    If CInt(cbo.SelectedValue) > 0 Then

                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        If CInt(cbo.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                            dgvDataList.CurrentRow.Cells(dgcolhMappedHead.Index).Value = 0
                            dgvDataList.CurrentRow.Cells(dgcolhMappedHead.Index).ReadOnly = True
                            CType(dgvDataList.CurrentRow.Cells(dgcolhMappedHead.Index), DataGridViewComboBoxCell).DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                        End If
                        'Sohail (29 Apr 2019) -- End

                        If CInt(cbo.SelectedValue) = enLoanCalcId.No_Interest Then
                            dgvDataList.CurrentRow.Cells(dgcolhRate.Index).Value = ""
                            dgvDataList.CurrentRow.Cells(dgcolhRate.Index).ReadOnly = True
                            'Nilay (15-Dec-2015) -- Start
                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                            'dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).ReadOnly = False
                            'dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).ReadOnly = False
                            dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).ReadOnly = True
                            dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).ReadOnly = True
                            'Nilay (15-Dec-2015) -- End

                            'Sohail (15 Dec 2015) -- Start
                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                            CType(dgvDataList.CurrentRow.Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell).Value = 0
                            CType(dgvDataList.CurrentRow.Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell).DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                            CType(dgvDataList.CurrentRow.Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell).ReadOnly = True
                            'Sohail (15 Dec 2015) -- End
                            'Sohail (29 Apr 2019) -- Start
                            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        ElseIf CInt(cbo.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                            dgvDataList.CurrentRow.Cells(dgcolhRate.Index).Value = ""
                            dgvDataList.CurrentRow.Cells(dgcolhRate.Index).ReadOnly = True
                            dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).ReadOnly = True
                            dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).ReadOnly = True
                            dgvDataList.CurrentRow.Cells(dgcolhMappedHead.Index).ReadOnly = False

                            CType(dgvDataList.CurrentRow.Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell).Value = 0
                            CType(dgvDataList.CurrentRow.Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell).DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                            CType(dgvDataList.CurrentRow.Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell).ReadOnly = True

                            CType(dgvDataList.CurrentRow.Cells(dgcolhMappedHead.Index), DataGridViewComboBoxCell).DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                            CType(dgvDataList.CurrentRow.Cells(dgcolhMappedHead.Index), DataGridViewComboBoxCell).ReadOnly = False
                            'Sohail (29 Apr 2019) -- End
                        Else
                            dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).ReadOnly = False
                            dgvDataList.CurrentRow.Cells(dgcolhRate.Index).ReadOnly = False
                            dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).ReadOnly = True

                            'Sohail (15 Dec 2015) -- Start
                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                            CType(dgvDataList.CurrentRow.Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell).DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                            CType(dgvDataList.CurrentRow.Cells(dgcolhInterestType.Index), DataGridViewComboBoxCell).ReadOnly = False
                            'Sohail (15 Dec 2015) -- End
                        End If
                        Call Calculate_Projected_Loan_Balance(CType(CInt(cbo.SelectedValue), enLoanCalcId), CType(dgvDataList.CurrentRow.Cells(objdgcolhIntCalcTypeId.Index).Value, enLoanInterestCalcType), False)
                    Else
                        dgvDataList.CurrentRow.Cells(dgcolhRate.Index).Value = ""
                        dgvDataList.CurrentRow.Cells(dgcolhRate.Index).ReadOnly = True
                        dgvDataList.CurrentRow.Cells(dgcolhNoOfEMI.Index).ReadOnly = True
                        dgvDataList.CurrentRow.Cells(dgcolhInstallmentAmount.Index).ReadOnly = True
                    End If

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                ElseIf dgvDataList.CurrentCell.ColumnIndex = dgcolhInterestType.Index Then

                    If dgvDataList.CurrentRow.Cells(dgcolhEffDate.Index).Value.ToString = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), enMsgBoxStyle.Information)
                        cbo.SelectedValue = 0
                        Exit Sub
                    End If

                    dgvDataList.CurrentRow.Cells(objdgcolhIntCalcTypeId.Index).Value = CInt(cbo.SelectedValue)

                    If CInt(cbo.SelectedValue) > 0 Then
                        Call Calculate_Projected_Loan_Balance(CType(dgvDataList.CurrentRow.Cells(objdgcolhCalcTypeId.Index).Value, enLoanCalcId), CType(CInt(cbo.SelectedValue), enLoanInterestCalcType), False)
                    End If
                End If
                'Sohail (15 Dec 2015) -- End

            End If

            'AddHandler dgvDataList.CellValueChanged, AddressOf dgvDataList_CellValueChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combo_SelectionChangeCommitted", mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (08-Aug-2016) -- Start
    'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
    Private Sub cboGLoanCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGLoanCalcType.SelectedIndexChanged
        Try
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                cboMappedHead.SelectedValue = 0
                cboMappedHead.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- End

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
            'If CInt(cboGLoanCalcType.SelectedValue) > 0 Then
            'Nilay (08-Dec-2016) -- End
                If CInt(cboGLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                    cboGInterestCalcType.SelectedValue = 0
                    cboGInterestCalcType.Enabled = False
                    txtGRate.Text = CStr(0)
                    txtGRate.Enabled = False
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboGLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                cboGInterestCalcType.SelectedValue = 0
                cboGInterestCalcType.Enabled = False
                txtGRate.Text = CStr(0)
                txtGRate.Enabled = False
                cboMappedHead.Enabled = True
                'Sohail (29 Apr 2019) -- End
                Else
                    cboGInterestCalcType.SelectedValue = 0
                    cboGInterestCalcType.Enabled = True
                    txtGRate.Text = "0"
                    txtGRate.Enabled = True
                End If
            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
            'End If
            'Nilay (08-Dec-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGLoanCalcType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Nilay (08-Aug-2016) -- End

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress, cboLoan.KeyPress, cboMappedHead.KeyPress
        'Sohail (29 Apr 2019) - [cboLoan, cboMappedHead]
        Dim cbo As ComboBox = CType(sender, ComboBox) 'Sohail (29 Apr 2019)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    '.ValueMember = cboEmployee.ValueMember
                    '.DisplayMember = cboEmployee.DisplayMember
                    '.DataSource = CType(cboEmployee.DataSource, DataTable)
                    '.CodeMember = "employeecode"
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If
                    'Sohail (29 Apr 2019) -- End
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'cboEmployee.SelectedValue = frm.SelectedValue
                    cbo.SelectedValue = frm.SelectedValue
                    'Sohail (29 Apr 2019) -- End
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'cboEmployee.Text = ""
                    cbo.Text = ""
                    'Sohail (29 Apr 2019) -- End
                End If
            End If
        Catch ex As Exception
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
            'Sohail (29 Apr 2019) -- End
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus, cboLoan.GotFocus, cboMappedHead.GotFocus
        'Sohail (29 Apr 2019) - [cboLoan, cboMappedHead]
        Dim cbo As ComboBox = CType(sender, ComboBox) 'Sohail (29 Apr 2019)
        Try
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'With cboEmployee
            With cbo
                'Sohail (29 Apr 2019) -- End
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
            'Sohail (29 Apr 2019) -- End
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave, cboLoan.Leave, cboMappedHead.Leave
        'Sohail (29 Apr 2019) - [cboLoan, cboMappedHead]
        Dim cbo As ComboBox = CType(sender, ComboBox) 'Sohail (29 Apr 2019)
        Try
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            '    Call SetDefaultSearchText(cboEmployee)
            'End If
            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
            'Sohail (29 Apr 2019) -- End
        Catch ex As Exception
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
            'Sohail (29 Apr 2019) -- End
        End Try
    End Sub

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    'Private Sub cboLoan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoan.KeyPress
    '    Try
    '        If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cboLoan.ValueMember
    '                .DisplayMember = cboLoan.DisplayMember
    '                .DataSource = CType(cboLoan.DataSource, DataTable)
    '                .CodeMember = "Code"
    '            End With
    '            Dim c As Char = Convert.ToChar(e.KeyChar)
    '            frm.TypedText = c.ToString
    '            If frm.DisplayDialog Then
    '                cboLoan.SelectedValue = frm.SelectedValue
    '                e.KeyChar = ChrW(Keys.ShiftKey)
    '            Else
    '                cboLoan.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboLoan_KeyPress", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoan.SelectedIndexChanged
    '    Try
    '        If CInt(cboLoan.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoan)

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboLoan_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboLoan_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoan.GotFocus
    '    Try
    '        With cboLoan
    '            .ForeColor = Color.Black
    '            .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

    '            If .Text = mstrSearchText Then
    '                .Text = ""
    '            End If
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboLoan_GotFocus", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboLoan_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoan.Leave
    '    Try
    '        If CInt(cboLoan.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoan)

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboLoan_Leave", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (29 Apr 2019) -- End
    'Nilay (27-Oct-2016) -- End

#End Region

#Region " Button Event(s) "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Currency is mandatory information. Please select currency to continue."), enMsgBoxStyle.Information)
                'Nilay (12-Dec-2015) -- Start
                cboCurrency.Focus()
                'Nilay (12-Dec-2015) -- End
                Exit Sub
            End If
            cboMode.Enabled = False
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboApprover.SelectedValue = 0
            'Nilay (16-Nov-2016) -- Start
            Call SetDefaultSearchText(cboEmployee)
            Call SetDefaultSearchText(cboLoan)
            cboMode.Focus()
            'Nilay (16-Nov-2016) -- End
            cboMode.SelectedIndex = 0
            cboLoan.SelectedValue = 0
            mstrAdvanceFilter = ""
            cboCurrency.SelectedValue = 0
            dgvDataList.DataSource = Nothing
            cboMode.Enabled = True
            'Nilay (08-Aug-2016) -- Start
            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
            cboGLoanCalcType.Enabled = True
            cboGLoanCalcType.SelectedValue = 0
            cboGInterestCalcType.Enabled = True
            cboGInterestCalcType.SelectedValue = 0
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            cboMappedHead.Enabled = True
            cboMappedHead.SelectedValue = 0
            'Sohail (29 Apr 2019) -- End
            txtGRate.Enabled = True
            txtGRate.Text = "0"
            radApplyToChecked.Checked = False
            radApplyToAll.Checked = False
            'Nilay (08-Aug-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub

            Dim dtmp() As DataRow = Nothing
            dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false")

            'Nilay (08-Aug-2016) -- Start
            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
            If mdtTran.Select("IsGrp = false").Length <> dtmp.Length Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "You have not ticked all records for assignment.") & vbCrLf & Language.getMessage(mstrModuleName, 27, "Do you want to continue?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    dgvDataList.Focus()
                    Exit Sub
                End If
            End If
            'Nilay (08-Aug-2016) -- End

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Me.Cursor = Cursors.WaitCursor

            Dim enMailType As New clsProcess_pending_loan.enApproverEmailType
            If CInt(cboMode.SelectedIndex) = 0 Then 'LOAN
                enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
            ElseIf CInt(cboMode.SelectedIndex) = 1 Then 'ADVANCE
                enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
            End If
            'Nilay (20-Sept-2016) -- End

            If dtmp.Length > 0 Then
                objEmailList = New List(Of clsEmailCollection) 'Hemant (30 Aug 2019)
                For ridx As Integer = 0 To dtmp.Length - 1
                    If objLoan_Advance IsNot Nothing Then objLoan_Advance = Nothing
                    objLoan_Advance = New clsLoan_Advance

                    Call SetValue(dtmp(ridx))

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'blnFlag = objLoan_Advance.Insert()

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'blnFlag = objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid)

                    'Nilay (25-Mar-2016) -- Start
                    'blnFlag = objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                    '                                 FinancialYear._Object._DatabaseName)

                    'Nilay (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                    'blnFlag = objLoan_Advance.Insert(FinancialYear._Object._DatabaseName, _
                    '                                 User._Object._Userunkid, _
                    '                                 FinancialYear._Object._YearUnkid, _
                    '                                 Company._Object._Companyunkid, _
                    '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                                 ConfigParameter._Object._UserAccessModeSetting, _
                    '                                 True)

                    blnFlag = objLoan_Advance.Insert(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     mdtStartDate, _
                                                     mdtEndDate, _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                     True)
                    'Nilay (21-Jul-2016) -- End

                    
                    'Nilay (25-Mar-2016) -- End

                    'Sohail (15 Dec 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- END

                    If blnFlag Then
                        If ConfigParameter._Object._LoanVocNoType = 0 Then nudManualNo.Value += 1
                        'Shani (21-Jul-2016) -- Start
                        'Enhancement - Create New Loan Notification  
                        Dim objProcessLoan As New clsProcess_pending_loan

                        'Nilay (10-Sept-2016) -- Start
                        'Enhancement - Create New Loan Notification 
                        'objProcessLoan.Send_Notification_After_Assign(FinancialYear._Object._DatabaseName, _
                        '                                              User._Object._Userunkid, _
                        '                                              FinancialYear._Object._YearUnkid, _
                        '                                              Company._Object._Companyunkid, _
                        '                                              ConfigParameter._Object._IsIncludeInactiveEmp, _
                        '                                              mdtStartDate, mdtEndDate, _
                        '                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                                              ConfigParameter._Object._UserAccessModeSetting, _
                        '                                              ConfigParameter._Object._ArutiSelfServiceURL, _
                        '                                              objLoan_Advance._Loanadvancetranunkid, enLogin_Mode.DESKTOP)
                        'objProcessLoan.Send_Notification_Employee(CInt(dtmp(ridx).Item("EmpId")), _
                        '                                          CInt(dtmp(ridx).Item("Pendingunkid")), _
                        '                                          clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                        '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                        '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                        '                                          enLogin_Mode.DESKTOP, 0, 0)

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'Dim enMailType As New clsProcess_pending_loan.enApproverEmailType
                        'If CInt(cboMode.SelectedIndex) = 0 Then 'LOAN
                        '    enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                        'ElseIf CInt(cboMode.SelectedIndex) = 1 Then 'ADVANCE
                        '    enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                        'End If
                        'Nilay (20-Sept-2016) -- End

                        'Nilay (10-Dec-2016) -- Start
                        'Issue #26: Setting to be included on configuration for Loan flow Approval process
                        Dim oEmail As New List(Of clsEmailCollection) 'Hemant (30 Aug 2019)
                        If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then

                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcessLoan.Send_Notification_Employee(CInt(dtmp(ridx).Item("EmpId")), _
                            '                                          CInt(dtmp(ridx).Item("Pendingunkid")), _
                            '                                          clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                            '                                          enMailType, _
                            '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                            '                                          enLogin_Mode.DESKTOP, 0, 0, False) 'Nilay (27-Dec-2016) -- [False]
                            objProcessLoan.Send_Notification_Employee(CInt(dtmp(ridx).Item("EmpId")), _
                                                                      CInt(dtmp(ridx).Item("Pendingunkid")), _
                                                                      clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                                                                      enMailType, _
                                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                     enLogin_Mode.DESKTOP, 0, 0, False)
                            'Sohail (30 Nov 2017) -- End

                            'Hemant (30 Aug 2019) -- Start
                            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                            'objProcessLoan.Send_Notification_After_Assign(FinancialYear._Object._DatabaseName, _
                            '                                         User._Object._Userunkid, _
                            '                                         FinancialYear._Object._YearUnkid, _
                            '                                         Company._Object._Companyunkid, _
                            '                                         ConfigParameter._Object._IsIncludeInactiveEmp, _
                            '                                         mdtStartDate, mdtEndDate, _
                            '                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                                         ConfigParameter._Object._UserAccessModeSetting, _
                            '                                         ConfigParameter._Object._ArutiSelfServiceURL, _
                            '                                         objLoan_Advance._Loanadvancetranunkid, _
                            '                                         enMailType, _
                            '                                         enLogin_Mode.DESKTOP)
                        objProcessLoan.Send_Notification_After_Assign(FinancialYear._Object._DatabaseName, _
                                                                      User._Object._Userunkid, _
                                                                      FinancialYear._Object._YearUnkid, _
                                                                      Company._Object._Companyunkid, _
                                                                      ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                      mdtStartDate, mdtEndDate, _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                                      ConfigParameter._Object._ArutiSelfServiceURL, _
                                                                      objLoan_Advance._Loanadvancetranunkid, _
                                                                      enMailType, _
                                                                     enLogin_Mode.DESKTOP, False, oEmail, ConfigParameter._Object._Notify_LoanAdvance_Users)

                            objEmailList.AddRange(oEmail)
                            'Hemant (30 Aug 2019) -- End

                        End If
                        'Nilay (10-Dec-2016) -- End

                        'Nilay (10-Sept-2016) -- End

                        'Shani (21-Jul-2016) -- End
                    Else
                        If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
                            Dim iRowIdx As Integer = -1
                            iRowIdx = mdtTran.Rows.IndexOf(dtmp(ridx))
                            If iRowIdx <> -1 Then
                                dgvDataList.Rows(iRowIdx).DefaultCellStyle.ForeColor = Color.Red
                            End If
                            eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                Next
                If blnFlag Then
                    Call FillGrid()
                End If

                'Hemant (30 Aug 2019) -- Start
                'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
                'Hemant (30 Aug 2019) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
        Finally
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Me.Cursor = Cursors.Default
            'Nilay (20-Sept-2016) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboEmployee.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    If .DisplayDialog Then
                        cboEmployee.SelectedValue = .SelectedValue
                    End If
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchScheme.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboLoan.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboLoan.ValueMember
                    .DisplayMember = cboLoan.DisplayMember
                    .DataSource = CType(cboLoan.DataSource, DataTable)
                    If .DisplayDialog Then
                        cboLoan.SelectedValue = .SelectedValue
                    End If
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchScheme_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Nilay (08-Aug-2016) -- Start
    'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If dgvDataList.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "No Data records found."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If radApplyToAll.Checked = False AndAlso radApplyToChecked.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Please either tick Apply To Checked OR Apply To All for further process."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If radApplyToChecked.Checked = True Then
                Dim dR() As DataRow = Nothing
                dR = mdtTran.Select("IsCheck = true AND IsGrp = false")
                If dR.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please check atleast one record from the list for further process."), enMsgBoxStyle.Information)
                    dgvDataList.Focus()
                    Exit Sub
                End If
            End If

            If CInt(cboGLoanCalcType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please select atleast one Loan calculation type for further process."), enMsgBoxStyle.Information)
                cboGLoanCalcType.Focus()
                Exit Sub
            End If

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If CInt(cboGLoanCalcType.SelectedValue) > 0 AndAlso CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest Then
            If CInt(cboGLoanCalcType.SelectedValue) > 0 AndAlso CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                'Sohail (29 Apr 2019) -- End
                If CInt(cboGInterestCalcType.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Please select atleast one Loan Interest calculation type for further process."), enMsgBoxStyle.Information)
                    cboGInterestCalcType.Focus()
                    Exit Sub
                End If
                If CInt(cboGInterestCalcType.SelectedValue) > 0 Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    'If CInt(txtGRate.Text) <= 0 OrElse txtGRate.Text.Trim = "" Then
                    If CDec(txtGRate.Text) <= 0 OrElse txtGRate.Text.Trim = "" Then
                        'Varsha (25 Nov 2017) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Please specify interest rate for further process."), enMsgBoxStyle.Information)
                        txtGRate.Focus()
                        Exit Sub
                    End If
                End If
            End If

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboGLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboMappedHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Please select Mapped Head to continue."), enMsgBoxStyle.Information)
                cboMappedHead.Focus()
                Exit Sub
            End If
            'Sohail (29 Apr 2019) -- End

            Call SetCalculation(radApplyToAll.Checked)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApply_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCalculation(ByVal blnIsApplyToAll As Boolean)
        Try
            Dim dtmp As DataRow() = Nothing
            If blnIsApplyToAll = True Then
                dtmp = mdtTran.Select("1=1")
            Else
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false")
            End If
            If dtmp.Length > 0 Then
                For Each dRow As DataRow In dtmp
                    dgvDataList.CurrentCell = dgvDataList.Rows(mdtTran.Rows.IndexOf(dRow)).Cells(dgcolhLoanType.Index)
                    If blnIsApplyToAll = True Then
                        'dRow.Item("IsCheck") = radApplyToAll.Checked
                    End If
                    If CBool(dgvDataList.CurrentRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                        dRow.Item("calctypeId") = CInt(cboGLoanCalcType.SelectedValue)
                        dRow.Item("interest_calctype_id") = CInt(cboGInterestCalcType.SelectedValue)
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        'dRow.Item("rate") = IIf(CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest, CDec(txtGRate.Text), "")
                        dRow.Item("rate") = IIf(CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head, CDec(txtGRate.Text), "")
                        dRow.Item("mapped_tranheadunkid") = CInt(cboMappedHead.SelectedValue)
                        'Sohail (29 Apr 2019) -- End
                        Call Combo_SelectionChangeCommitted(cboGLoanCalcType, New EventArgs)
                    End If
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCalculation", mstrModuleName)
        End Try
    End Sub
    'Nilay (08-Aug-2016) -- End


#End Region

#Region " Link Event "

    'S.SANDEEP [01 AUG 2015] -- START
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Try
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'Dim frm As New frmAdvanceSearch
                'frm._Hr_EmployeeTable_Alias = "hremployee_master"
                'frm.ShowDialog()
                'mstrAdvanceFilter = frm._GetFilterString
                Dim frm As New frmAdvanceSearch

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                mstrAdvanceFilter = frm._GetFilterString
                    Call FillGrid()
                End If
                'Nilay (01-Mar-2016) -- End

                
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [01 AUG 2015] -- END

#End Region


    '#Region " Private Variables "

    '    Private ReadOnly mstrModuleName As String = "frmGlobalAssignLoanAdvance"

    '    Private objLoan_Advance As clsLoan_Advance
    '    Private objPendingLoan As clsProcess_pending_loan
    '    Private objlnInterest As clslnloan_interest_tran
    '    Private objlnEMI As clslnloan_emitenure_tran

    '    Private mdtTran As DataTable
    '    Private mdtRate As DataTable
    '    Private mdtEMI As DataTable

    '    Private mblnIsFormLoad As Boolean = False
    '    Private dtChkRow() As DataRow = Nothing
    '    Private imgPlusIcon As Image = My.Resources.plus_blue
    '    Private imgMinusIcon As Image = My.Resources.minus_blue
    '    Private imgBlankIcon As Image = My.Resources.blankImage
    '    Private mstrAdvanceFilter As String = String.Empty

    '#End Region

    '#Region " Private Functions & Procedure "

    '    Private Sub FillCombo()
    '        Dim dsCombo As New DataSet
    '        Dim objApprover As New clsLoanApprover_master
    '        Dim objScheme As New clsLoan_Scheme
    '        Dim objEmp As New clsEmployee_Master
    '        Dim objPeriod As New clscommom_period_Tran
    '        Dim objCurrency As New clsExchangeRate
    '        Try
    '            dsCombo = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
    '            With cboEmployee
    '                .ValueMember = "employeeunkid"
    '                .DisplayMember = "employeename"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objApprover.getListForCombo("List", True, True, True)
    '            With cboApprover
    '                .DisplayMember = "name"
    '                .ValueMember = "lnapproverunkid"
    '                .DataSource = dsCombo.Tables(0)
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objScheme.getComboList(True, "List")
    '            With cboLoan
    '                .ValueMember = "loanschemeunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
    '            With cboPayPeriod
    '                .ValueMember = "periodunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List").Copy
    '                .SelectedValue = 0
    '            End With

    '            With cboDeductionPeriod
    '                .ValueMember = "periodunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            cboMode.Items.Clear()
    '            cboMode.Items.Add(Language.getMessage(mstrModuleName, 1, "Loan"))
    '            cboMode.Items.Add(Language.getMessage(mstrModuleName, 2, "Advance"))
    '            cboMode.SelectedIndex = 0

    '            dsCombo = objCurrency.getComboList("Currency", True)
    '            With cboCurrency
    '                .ValueMember = "countryunkid"
    '                .DisplayMember = "currency_sign"
    '                .DataSource = dsCombo.Tables("Currency")
    '                .SelectedValue = 0
    '            End With

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
    '        Finally
    '            dsCombo.Dispose() : objEmp = Nothing : objApprover = Nothing : objScheme = Nothing : objPeriod = Nothing
    '            objCurrency = Nothing
    '        End Try
    '    End Sub

    '    Private Sub SetGridColor()
    '        Try
    '            Dim dgvcsHeader As New DataGridViewCellStyle
    '            dgvcsHeader.ForeColor = Color.White
    '            dgvcsHeader.SelectionBackColor = Color.Gray
    '            dgvcsHeader.SelectionForeColor = Color.White
    '            dgvcsHeader.BackColor = Color.Gray

    '            Dim dgvcsChild As New DataGridViewCellStyle
    '            dgvcsChild.ForeColor = Color.Black
    '            dgvcsChild.SelectionForeColor = Color.Black
    '            dgvcsChild.SelectionBackColor = Color.White
    '            dgvcsChild.BackColor = Color.White


    '            For i As Integer = 0 To dgvDataList.RowCount - 1
    '                If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                    dgvDataList.Rows(i).DefaultCellStyle = dgvcsHeader
    '                    'Else
    '                    '    dgvDataList.Rows(i).DefaultCellStyle = dgvcsChild
    '                End If
    '            Next

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SetCollapseValue()
    '        Try
    '            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
    '            objdgvsCollapseHeader.Font = New Font(dgvDataList.Font.FontFamily, 13, FontStyle.Bold)
    '            objdgvsCollapseHeader.ForeColor = Color.White
    '            objdgvsCollapseHeader.BackColor = Color.Gray
    '            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
    '            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

    '            Dim objdgvsCollapseChild As New DataGridViewCellStyle
    '            objdgvsCollapseChild.Font = New Font(dgvDataList.Font.FontFamily, 12, FontStyle.Bold)
    '            objdgvsCollapseChild.ForeColor = Color.Black
    '            objdgvsCollapseChild.SelectionBackColor = Color.White
    '            objdgvsCollapseChild.SelectionBackColor = Color.White
    '            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


    '            For i As Integer = 0 To dgvDataList.RowCount - 1
    '                If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                    If dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
    '                        'dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = "▬"
    '                        dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
    '                    End If
    '                    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
    '                Else
    '                    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
    '                    '    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseChild
    '                End If
    '            Next

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Function IsValidData() As Boolean
    '        Try

    '            dtChkRow = mdtTran.Select("IsCheck= true AND IsGrp = false")

    '            If dtChkRow.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one employee in order to assign Loan or Advance."), enMsgBoxStyle.Information)
    '                Return False
    '            End If

    '            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Assigned Period is compulsory information. Please select Assigned Period."), enMsgBoxStyle.Information)
    '                cboPayPeriod.Focus()
    '                Return False
    '            End If

    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)

    '            If dtpDate.Value.Date > objPeriod._End_Date Or dtpDate.Value.Date < objPeriod._Start_Date Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Effective date should be in between ") & _
    '                                objPeriod._Start_Date & Language.getMessage(mstrModuleName, 13, " And ") & _
    '                                objPeriod._End_Date, enMsgBoxStyle.Information)
    '                dtpDate.Focus()
    '                Exit Function
    '            End If

    '            Return True

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
    '        Finally
    '        End Try
    '    End Function

    '    Private Sub SetVisibility()
    '        Try
    '            If ConfigParameter._Object._LoanVocNoType = 1 Then
    '                nudManualNo.Enabled = False
    '            Else
    '                nudManualNo.Enabled = True
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SetColor()
    '        Try
    '            txtInstallmentAmt.BackColor = GUI.ColorComp
    '            txtLoanInterest.BackColor = GUI.ColorComp
    '            txtPurpose.BackColor = GUI.ColorOptional
    '            txtPrefix.BackColor = GUI.ColorComp
    '            nudManualNo.BackColor = GUI.ColorComp
    '            nudDuration.BackColor = GUI.ColorComp
    '            cboApprover.BackColor = GUI.ColorOptional
    '            cboDeductionPeriod.BackColor = GUI.ColorComp
    '            cboEmployee.BackColor = GUI.ColorOptional
    '            cboLoan.BackColor = GUI.ColorOptional
    '            cboMode.BackColor = GUI.ColorComp
    '            cboPayPeriod.BackColor = GUI.ColorComp
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SetGridDataSoruce()
    '        Try
    '            dgvDataList.AutoGenerateColumns = False

    '            dgcolhAmount.DataPropertyName = "Amount"
    '            dgcolhApprover.DataPropertyName = "Approver"
    '            dgcolhEmployee.DataPropertyName = "Employee"
    '            objdgcolhSelect.DataPropertyName = "IsCheck"
    '            objdgcolhGrpId.DataPropertyName = "GrpId"
    '            objdgcolhIsEx.DataPropertyName = "IsEx"
    '            objdgcolhIsGrp.DataPropertyName = "IsGrp"
    '            objdgcolhPendingunkid.DataPropertyName = "Pendingunkid"
    '            objdgcolhApprId.DataPropertyName = "ApprId"
    '            objdgcolhEmpId.DataPropertyName = "EmpId"
    '            dgcolhDeductionPeriod.DataPropertyName = "deductionperiod"
    '            dgcolhDuration.DataPropertyName = "duration"
    '            dgcolhNoOfEMI.DataPropertyName = "noofinstallment"
    '            dgcolhInstallmentAmount.DataPropertyName = "installmentamt"
    '            dgcolhRate.DataPropertyName = "rate"
    '            objdgcolhDeductionPeriodId.DataPropertyName = "deductionperiodunkid"


    '            dgvDataList.DataSource = mdtTran

    '            dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
    '            dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    '            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency

    '            dgcolhDuration.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
    '            dgcolhDuration.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    '            dgcolhRate.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
    '            dgcolhRate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    '            dgcolhNoOfEMI.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
    '            dgcolhNoOfEMI.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    '            dgcolhInstallmentAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
    '            dgcolhInstallmentAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    '            dgcolhInstallmentAmount.DefaultCellStyle.Format = GUI.fmtCurrency



    '            Call SetGridColor()

    '            Call SetCollapseValue()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetGridDataSoruce", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub FillGrid()
    '        Try
    '            Dim StrSearching As String = String.Empty

    '            Select Case CInt(cboMode.SelectedIndex)
    '                Case 0
    '                    StrSearching &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 1 "
    '                Case 1
    '                    StrSearching &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 2 "
    '            End Select

    '            If CInt(cboLoan.SelectedValue) > 0 Then
    '                StrSearching &= "AND ISNULL(lnloan_scheme_master.loanschemeunkid,0) = '" & CInt(cboLoan.SelectedValue) & "' "
    '            End If

    '            If CInt(cboEmployee.SelectedValue) > 0 Then
    '                StrSearching &= "AND hremployee_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
    '            End If

    '            If CInt(cboApprover.SelectedValue) > 0 Then
    '                StrSearching &= "AND lnloanapprover_master.lnapproverunkid = '" & CInt(cboApprover.SelectedValue) & "' "
    '            End If

    '            If CInt(cboCurrency.SelectedValue) > 0 Then
    '                StrSearching &= "AND lnloan_process_pending_loan.countryunkid = '" & CInt(cboCurrency.SelectedValue) & "' "
    '            End If

    '            If mstrAdvanceFilter.Trim.Length > 0 Then
    '                StrSearching &= "AND " & mstrAdvanceFilter
    '            End If

    '            If StrSearching.Trim.Length > 0 Then
    '                StrSearching = StrSearching.Substring(3)
    '            End If

    '            mdtTran = objPendingLoan.GetToAssignList(2, FinancialYear._Object._DatabaseName, _
    '                                                     User._Object._Userunkid, _
    '                                                     FinancialYear._Object._YearUnkid, _
    '                                                     Company._Object._Companyunkid, _
    '                                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
    '                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                     UserAccessLevel._AccessLevelFilterString, StrSearching)

    '            Call SetGridDataSoruce()

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SetValueOnGrid()
    '        Try
    '            If mdtTran IsNot Nothing Then
    '                Dim dtmp() As DataRow = Nothing
    '                If radChecked.Checked = True Then
    '                    dtmp = mdtTran.Select("IsCheck= true AND IsGrp = false")
    '                ElseIf radAll.Checked = True Then
    '                    dtmp = mdtTran.Select("IsGrp = false")
    '                End If

    '                For xRowIdx As Integer = 0 To dtmp.Length - 1
    '                    If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
    '                        dtmp(xRowIdx).Item("deductionperiod") = cboDeductionPeriod.Text
    '                        dtmp(xRowIdx).Item("deductionperiodunkid") = CInt(cboDeductionPeriod.SelectedValue)
    '                    End If
    '                    If nudDuration.Value > 0 Then
    '                        dtmp(xRowIdx).Item("noofinstallment") = nudDuration.Value
    '                    End If
    '                    If txtInstallmentAmt.Decimal > 0 Then
    '                        dtmp(xRowIdx).Item("installmentamt") = Format(CDec(txtInstallmentAmt.Decimal), GUI.fmtCurrency)
    '                    End If
    '                    If txtLoanInterest.Decimal > 0 Then
    '                        dtmp(xRowIdx).Item("rate") = txtLoanInterest.Text
    '                    End If
    '                    If nudDuration.Value > 1 Then
    '                        dtmp(xRowIdx).Item("duration") = nudDuration.Value
    '                    End If
    '                    dtmp(xRowIdx).Item("ischange") = True
    '                Next
    '                mdtTran.AcceptChanges()
    '                Call SetGridDataSoruce()
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetValueOnGrid", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Function SetLoanAdvanceValue(ByVal intValue As Integer) As Boolean
    '        Try
    '            objLoan_Advance = New clsLoan_Advance
    '            Select Case CInt(cboMode.SelectedIndex)
    '                Case 0  'LOAN ASSIGNMENT
    '                    objLoan_Advance._Loan_Amount = CDec(dtChkRow(intValue)("Amount"))
    '                    objLoan_Advance._Loanschemeunkid = CInt(dtChkRow(intValue)("GrpId"))
    '                    objLoan_Advance._Net_Amount = CDec(dtChkRow(intValue)("Amount"))
    '                    objLoan_Advance._Isloan = True
    '                Case 1  'ADVANCE ASSIGNMENT
    '                    objLoan_Advance._Advance_Amount = CDec(dtChkRow(intValue)("Amount"))
    '                    objLoan_Advance._Loanscheduleunkid = 0
    '                    objLoan_Advance._Loanschemeunkid = 0
    '                    objLoan_Advance._Net_Amount = 0
    '                    objLoan_Advance._Isloan = False
    '            End Select
    '            objLoan_Advance._Approverunkid = CInt(dtChkRow(intValue)("ApprId"))
    '            objLoan_Advance._CountryUnkid = CInt(dtChkRow(intValue)("countryunkid"))

    '            If CBool(dtChkRow(intValue)("IsEx")) = True Then
    '                Select Case CInt(cboMode.SelectedIndex)
    '                    Case 0
    '                        objLoan_Advance._Balance_Amount = CDec(dtChkRow(intValue)("Amount"))
    '                    Case 1
    '                        objLoan_Advance._Balance_Amount = CDec(dtChkRow(intValue)("Amount"))
    '                End Select
    '            Else
    '                objLoan_Advance._Balance_Amount = 0
    '            End If

    '            Select Case CInt(cboMode.SelectedIndex)
    '                Case 0
    '                    If radSimpleInterest.Checked Then
    '                        objLoan_Advance._Calctype_Id = enLoanCalcId.Simple_Interest '1
    '                    ElseIf radNoInterest.Checked Then
    '                        objLoan_Advance._Calctype_Id = enLoanCalcId.No_Interest   '4
    '                    ElseIf radReduceBalInterest.Checked Then
    '                        objLoan_Advance._Calctype_Id = enLoanCalcId.Reducing_Amount '3
    '                    End If
    '                Case Else
    '                    objLoan_Advance._Calctype_Id = 0
    '            End Select
    '            objLoan_Advance._Deductionperiodunkid = CInt(dtChkRow(intValue).Item("deductionperiodunkid"))
    '            objLoan_Advance._Effective_Date = dtpDate.Value
    '            objLoan_Advance._Employeeunkid = CInt(dtChkRow(intValue)("EmpId"))
    '            objLoan_Advance._Isbrought_Forward = False
    '            objLoan_Advance._Loan_Purpose = txtPurpose.Text
    '            If ConfigParameter._Object._LoanVocNoType = 1 Then
    '                objLoan_Advance._Loanvoucher_No = ""
    '            Else
    '                objLoan_Advance._Loanvoucher_No = txtPrefix.Text.Trim & nudManualNo.Value.ToString
    '            End If
    '            objLoan_Advance._Periodunkid = CInt(cboPayPeriod.SelectedValue)
    '            objLoan_Advance._Userunkid = User._Object._Userunkid
    '            objLoan_Advance._Voiduserunkid = -1
    '            objLoan_Advance._Voiddatetime = Nothing
    '            objLoan_Advance._Isvoid = False
    '            objLoan_Advance._LoanStatus = 1
    '            objLoan_Advance._Processpendingloanunkid = CInt(dtChkRow(intValue)("Pendingunkid"))

    '            'Select Case CInt(cboMode.SelectedIndex)
    '            '    Case 0
    '            '        Dim objPeriod As New clscommom_period_Tran
    '            '        objPeriod._Periodunkid = CInt(dtChkRow(intValue).Item("deductionperiodunkid"))
    '            '        objlnInterest._Loanadvancetranunkid = -1
    '            '        mdtRate = objlnInterest._DataTable
    '            '        Dim dRow As DataRow = mdtRate.NewRow()
    '            '        With dRow
    '            '            .Item("lninteresttranunkid") = -1
    '            '            .Item("loanadvancetranunkid") = -1
    '            '            .Item("periodunkid") = dtChkRow(intValue).Item("deductionperiodunkid")
    '            '            .Item("effectivedate") = objPeriod._Start_Date.Date
    '            '            .Item("interest_rate") = dtChkRow(intValue).Item("rate")
    '            '            .Item("userunkid") = User._Object._Userunkid
    '            '            .Item("isvoid") = False
    '            '            .Item("voiduserunkid") = -1
    '            '            .Item("voiddatetime") = DBNull.Value
    '            '            .Item("voidreason") = ""
    '            '            .Item("AUD") = "A"
    '            '            .Item("GUID") = Guid.NewGuid.ToString()
    '            '            .Item("dperiod") = ""
    '            '            .Item("ddate") = ""
    '            '            .Item("pstatusid") = enStatusType.Open
    '            '        End With
    '            '        mdtRate.Rows.Add(dRow)
    '            '        objlnInterest = Nothing

    '            '        objlnEMI = New clslnloan_emitenure_tran
    '            '        objlnEMI._Loanadvancetranunkid = -1
    '            '        mdtEMI = objlnEMI._DataTable

    '            '        dRow = mdtEMI.NewRow()
    '            '        With dRow
    '            '            dRow.Item("lnemitranunkid") = -1
    '            '            dRow.Item("loanadvancetranunkid") = -1
    '            '            dRow.Item("periodunkid") = dtChkRow(intValue).Item("deductionperiodunkid")
    '            '            dRow.Item("effectivedate") = objPeriod._Start_Date.Date
    '            '            dRow.Item("emi_tenure") = dtChkRow(intValue).Item("noofinstallment")
    '            '            dRow.Item("emi_amount") = dtChkRow(intValue).Item("installmentamt")
    '            '            dRow.Item("loan_duration") = dtChkRow(intValue).Item("duration")
    '            '            dRow.Item("userunkid") = User._Object._Userunkid
    '            '            dRow.Item("isvoid") = False
    '            '            dRow.Item("voiduserunkid") = -1
    '            '            dRow.Item("voiddatetime") = DBNull.Value
    '            '            dRow.Item("voidreason") = ""
    '            '            dRow.Item("AUD") = "A"
    '            '            dRow.Item("GUID") = Guid.NewGuid.ToString()
    '            '            dRow.Item("dperiod") = ""
    '            '            dRow.Item("ddate") = ""
    '            '            dRow.Item("pstatusid") = enStatusType.Open
    '            '        End With
    '            '        mdtEMI.Rows.Add(dRow)
    '            '        objlnEMI = Nothing

    '            '        objPeriod = Nothing
    '            'End Select


    '            Return True

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetLoanAdvanceValue", mstrModuleName)
    '            Return False
    '        Finally
    '        End Try
    '    End Function

    '#End Region

    '#Region " Form's Events "

    '    Private Sub frmGlobalAssignLoanAdvance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    '        Try
    '            objLoan_Advance = Nothing : objPendingLoan = Nothing : dtChkRow = Nothing
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "frmGlobalAssignLoanAdvance_FormClosed", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub frmGlobalAssignLoanAdvance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        objLoan_Advance = New clsLoan_Advance : objPendingLoan = New clsProcess_pending_loan
    '        Try
    '            Call Set_Logo(Me, gApplicationType)

    '            Language.setLanguage(Me.Name)
    '            Call OtherSettings()

    '            Call SetVisibility()
    '            Call SetColor()
    '            Call FillCombo()
    '            mblnIsFormLoad = True
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "frmGlobalAssignLoanAdvance_Load", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '        Dim objfrm As New frmLanguage
    '        Try
    '            If User._Object._Isrighttoleft = True Then
    '                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                objfrm.RightToLeftLayout = True
    '                Call Language.ctlRightToLeftlayOut(objfrm)
    '            End If

    '            Call SetMessages()
    '            clsLoan_Advance.SetMessages()
    '            objfrm._Other_ModuleNames = "clsLoan_Advance"
    '            objfrm.displayDialog(Me)

    '            Call SetLanguage()

    '        Catch ex As System.Exception
    '            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '        Finally
    '            objfrm.Dispose()
    '            objfrm = Nothing
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Button's Events "

    '    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
    '        Dim blnFlag As Boolean = False : Dim blnFail As Boolean = False
    '        Try
    '            If mdtTran Is Nothing Then Exit Sub

    '            If cboMode.SelectedIndex = 0 Then
    '                dtChkRow = mdtTran.Select("ischange = true AND IsCheck = true AND IsGrp = false")
    '                If dtChkRow.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, no changes are made in the current list for loan/advance assignment."), enMsgBoxStyle.Information)
    '                    Exit Sub
    '                End If
    '            Else
    '                dtChkRow = mdtTran.Select("IsCheck = true AND IsGrp = false")
    '                If dtChkRow.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, no changes are made in the current list for loan/advance assignment."), enMsgBoxStyle.Information)
    '                    Exit Sub
    '                End If
    '            End If

    '            If IsValidData() = False Then Exit Sub

    '            If dtChkRow.Length > 0 Then
    '                For iRowIdx As Integer = 0 To dtChkRow.Length - 1
    '                    If SetLoanAdvanceValue(iRowIdx) Then
    '                        If objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid) Then
    '                            blnFlag = True
    '                        Else
    '                            dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(iRowIdx))).DefaultCellStyle.BackColor = Color.Red
    '                            dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(iRowIdx))).DefaultCellStyle.SelectionBackColor = Color.Red
    '                            blnFail = True
    '                            blnFlag = False
    '                        End If
    '                    Else
    '                        dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(iRowIdx))).DefaultCellStyle.BackColor = Color.Red
    '                        dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(iRowIdx))).DefaultCellStyle.SelectionBackColor = Color.Red
    '                        blnFail = True
    '                    End If
    '                Next
    '            End If
    '            If blnFlag = True And blnFail = False Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Loan/Advance successfully assigned."), enMsgBoxStyle.Information)
    '                Call FillGrid()
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Some Loan/Advance assignment failed and marked with red color."), enMsgBoxStyle.Information)
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '        Try
    '            Me.Close()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
    '        Try
    '            cboEmployee.SelectedValue = 0
    '            cboApprover.SelectedValue = 0
    '            cboMode.SelectedIndex = 0
    '            cboLoan.SelectedValue = 0
    '            mstrAdvanceFilter = ""
    '            dgvDataList.DataSource = Nothing
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
    '        Try
    '            If CInt(cboCurrency.SelectedValue) <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Currency is mandatory information. Please select currency to continue."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If
    '            Call FillGrid()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region

    '#Region " DataGrid Events "

    '    Private Sub dgvDataList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataList.CellContentClick, dgvDataList.CellContentDoubleClick
    '        Try
    '            Dim i As Integer
    '            If e.RowIndex = -1 Then Exit Sub

    '            If Me.dgvDataList.IsCurrentCellDirty Then
    '                Me.dgvDataList.CommitEdit(DataGridViewDataErrorContexts.Commit)
    '            End If

    '            If CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                Select Case CInt(e.ColumnIndex)
    '                    Case 0
    '                        'If dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value.ToString = "▬" Then
    '                        '    dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "+"
    '                        'Else
    '                        '    dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "▬"
    '                        'End If

    '                        If dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
    '                            dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
    '                        Else
    '                            dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
    '                        End If

    '                        For i = e.RowIndex + 1 To dgvDataList.RowCount - 1
    '                            If CInt(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvDataList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
    '                                If dgvDataList.Rows(i).Visible = False Then
    '                                    dgvDataList.Rows(i).Visible = True
    '                                Else
    '                                    dgvDataList.Rows(i).Visible = False
    '                                End If
    '                            Else
    '                                Exit For
    '                            End If
    '                        Next
    '                    Case 1
    '                        For i = e.RowIndex + 1 To dgvDataList.RowCount - 1
    '                            Dim blnFlg As Boolean = CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value)
    '                            If CInt(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvDataList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
    '                                dgvDataList.Rows(i).Cells(objdgcolhSelect.Index).Value = blnFlg
    '                            End If
    '                        Next
    '                End Select
    '            End If

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "dgvDataList_CellContentClick", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Combobox Events "

    '    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, _
    '        cboApprover.SelectedIndexChanged, _
    '        cboMode.SelectedIndexChanged, _
    '        cboLoan.SelectedIndexChanged
    '        If mblnIsFormLoad = False Then Exit Sub

    '        Select Case CType(sender, ComboBox).Name.ToUpper
    '            Case "CBOMODE"
    '                Select Case cboMode.SelectedIndex
    '                    Case 0
    '                        cboLoan.SelectedValue = 0
    '                        cboLoan.Enabled = True
    '                        gbLoanInfo.Enabled = True
    '                        objbtnSearchScheme.Enabled = True
    '                    Case 1
    '                        cboLoan.SelectedValue = 0
    '                        cboLoan.Enabled = False
    '                        gbLoanInfo.Enabled = False
    '                        objbtnSearchScheme.Enabled = False
    '                End Select
    '        End Select
    '    End Sub

    '#End Region

    '#Region " Link Click Events "

    '    Private Sub lnkSetValue_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetValue.LinkClicked
    '        Try
    '            If CInt(cboMode.SelectedIndex) >= 0 Then
    '                Select Case CInt(cboMode.SelectedIndex)
    '                    Case 0
    '                        If Trim(txtLoanInterest.Text) = "" Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Loan Interest cannot be blank. Loan Interest is compulsory information."), enMsgBoxStyle.Information)
    '                            txtLoanInterest.Focus()
    '                            Exit Sub
    '                        ElseIf CDbl(Trim(txtLoanInterest.Text)) > 100 Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Loan Interest cannot be greater than 100%. Please provide proper Loan Interest."), enMsgBoxStyle.Information)
    '                            txtLoanInterest.Focus()
    '                            Exit Sub
    '                        End If

    '                        If radChecked.Checked = False AndAlso radAll.Checked = False Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Please select atleast one operation to continue."), enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If

    '                        If radSimpleInterest.Checked = False AndAlso radReduceBalInterest.Checked = False AndAlso radNoInterest.Checked = False Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Interest calculation type is mandatory information. Please select interest calculation type to set."), enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If

    '                End Select
    '            End If

    '            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
    '                Dim objPeriod As New clscommom_period_Tran
    '                objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)

    '                If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Deduction period is compulsory information. Please select Deduction period to continue."), enMsgBoxStyle.Information)
    '                    cboDeductionPeriod.Focus()
    '                    Exit Sub
    '                End If

    '                If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
    '                    Dim objDPeriod As New clscommom_period_Tran
    '                    objDPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)

    '                    If objDPeriod._Start_Date < objPeriod._Start_Date Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Deduction period cannot be less than the Loan Period."), enMsgBoxStyle.Information)
    '                        cboDeductionPeriod.Focus()
    '                        Exit Sub
    '                    End If

    '                    objDPeriod = Nothing
    '                End If

    '                objPeriod = Nothing
    '            End If

    '            If radAll.Checked = False Then
    '                Dim dtmp() As DataRow = mdtTran.Select("IsCheck = true AND IsGrp = false")
    '                If dtmp.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one employee in order to assign Loan or Advance."), enMsgBoxStyle.Information)
    '                    Exit Sub
    '                End If
    '            End If

    '            Call SetValueOnGrid()

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lnkSetValue_LinkClicked", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
    '        Try
    '            Dim frm As New frmAdvanceSearch
    '            frm._Hr_EmployeeTable_Alias = "hremployee_master"
    '            frm.ShowDialog()
    '            mstrAdvanceFilter = frm._GetFilterString
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
    '        End Try
    '    End Sub

    '#End Region

    'Public Class frmGlobalAssignLoanAdvance

    '#Region " Private Variables "

    '    Private ReadOnly mstrModuleName As String = "frmGlobalAssignLoanAdvance"
    '    Private mdtTran As DataTable
    '    Private objLoan_Advance As clsLoan_Advance
    '    Private objPendingLoan As clsProcess_pending_loan
    '    Private mblnIsFormLoad As Boolean = False
    '    Private dtChkRow() As DataRow = Nothing

    '    Private mdecInterest_Amount As Decimal = 0
    '    Private mdecNetAmount As Decimal = 0
    '    Private mdecInstallmentAmount As Decimal = 0
    '    Private mintTotalEMI As Integer = 0
    '    Private imgPlusIcon As Image = My.Resources.plus_blue
    '    Private imgMinusIcon As Image = My.Resources.minus_blue
    '    Private imgBlankIcon As Image = My.Resources.blankImage


    '#End Region

    '#Region " Private Functions & Procedure "

    '    Private Sub FillCombo()
    '        Dim dsCombo As New DataSet
    '        Dim objBranch As New clsStation
    '        Dim objDept As New clsDepartment
    '        Dim objSection As New clsSections
    '        Dim objJob As New clsJobs
    '        Dim objEmp As New clsEmployee_Master
    '        Dim objApprover As New clsLoan_Approver
    '        Dim objScheme As New clsLoan_Scheme
    '        Dim objPeriod As New clscommom_period_Tran
    '        Dim objCMaster As New clsMasterData
    '        Try

    '            dsCombo = objBranch.getComboList("List", True)
    '            With cboBranch
    '                .ValueMember = "stationunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objDept.getComboList("List", True)
    '            With cboDepartment
    '                .ValueMember = "departmentunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objSection.getComboList("List", True)
    '            With cboSection
    '                .ValueMember = "sectionunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objJob.getComboList("List", True)
    '            With cboJob
    '                .ValueMember = "jobunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'dsCombo = objEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
    '            dsCombo = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
    '            'Sohail (06 Jan 2012) -- End
    '            With cboEmployee
    '                .ValueMember = "employeeunkid"
    '                .DisplayMember = "employeename"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objApprover.GetList("List", True, True)
    '            With cboApprover
    '                .ValueMember = "approverunkid"
    '                .DisplayMember = "approvername"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objScheme.getComboList(True, "List")
    '            With cboLoan
    '                .ValueMember = "loanschemeunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objCMaster.GetLoanScheduling("List")
    '            With cboLoanScheduleBy
    '                .ValueMember = "Id"
    '                .DisplayMember = "Name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
    '            With cboPayPeriod
    '                .ValueMember = "periodunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List").Copy
    '                .SelectedValue = 0
    '            End With

    '            With cboDeductionPeriod
    '                .ValueMember = "periodunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With

    '            cboMode.Items.Clear()
    '            cboMode.Items.Add(Language.getMessage(mstrModuleName, 1, "Loan"))
    '            cboMode.Items.Add(Language.getMessage(mstrModuleName, 2, "Advance"))
    '            cboMode.SelectedIndex = 0

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '        Finally
    '            dsCombo.Dispose() : objBranch = Nothing : objDept = Nothing : objSection = Nothing _
    '          : objJob = Nothing : objEmp = Nothing : objApprover = Nothing : objScheme = Nothing _
    '          : objCMaster = Nothing : objPeriod = Nothing
    '        End Try
    '    End Sub

    '    Private Sub FillGrid()
    '        mdtTran = objPendingLoan.GetToAssignList(2, _
    '                                                 CInt(cboDepartment.SelectedValue), _
    '                                                 CInt(cboBranch.SelectedValue), _
    '                                                 CInt(cboSection.SelectedValue), _
    '                                                 CInt(cboJob.SelectedValue), _
    '                                                 CInt(cboEmployee.SelectedValue), _
    '                                                 CInt(cboApprover.SelectedValue), _
    '                                                 CInt(cboMode.SelectedIndex), _
    '                                                 CInt(cboLoan.SelectedValue), "mData")

    '        dgvDataList.AutoGenerateColumns = False

    '        dgcolhAmount.DataPropertyName = "Amount"
    '        dgcolhApprover.DataPropertyName = "Approver"
    '        dgcolhCode.DataPropertyName = "ECode"
    '        dgcolhEmployee.DataPropertyName = "Employee"
    '        objdgcolhSelect.DataPropertyName = "IsCheck"
    '        objdgcolhGrpId.DataPropertyName = "GrpId"
    '        objdgcolhIsEx.DataPropertyName = "IsEx"
    '        objdgcolhIsGrp.DataPropertyName = "IsGrp"
    '        objdgcolhPendingunkid.DataPropertyName = "Pendingunkid"
    '        objdgcolhApprId.DataPropertyName = "ApprId"
    '        objdgcolhEmpId.DataPropertyName = "EmpId"

    '        dgvDataList.DataSource = mdtTran

    '        dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
    '        dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    '        dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency

    '        Call SetGridColor()

    '        Call SetCollapseValue()

    '    End Sub

    '    Private Sub SetGridColor()
    '        Try
    '            Dim dgvcsHeader As New DataGridViewCellStyle
    '            dgvcsHeader.ForeColor = Color.White
    '            dgvcsHeader.SelectionBackColor = Color.Gray
    '            dgvcsHeader.SelectionForeColor = Color.White
    '            dgvcsHeader.BackColor = Color.Gray

    '            Dim dgvcsChild As New DataGridViewCellStyle
    '            dgvcsChild.ForeColor = Color.Black
    '            dgvcsChild.SelectionForeColor = Color.Black
    '            dgvcsChild.SelectionBackColor = Color.White
    '            dgvcsChild.BackColor = Color.White


    '            For i As Integer = 0 To dgvDataList.RowCount - 1
    '                If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                    dgvDataList.Rows(i).DefaultCellStyle = dgvcsHeader
    '                    'Else
    '                    '    dgvDataList.Rows(i).DefaultCellStyle = dgvcsChild
    '                End If
    '            Next

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SetCollapseValue()
    '        Try
    '            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
    '            objdgvsCollapseHeader.Font = New Font(dgvDataList.Font.FontFamily, 13, FontStyle.Bold)
    '            objdgvsCollapseHeader.ForeColor = Color.White
    '            objdgvsCollapseHeader.BackColor = Color.Gray
    '            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
    '            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

    '            Dim objdgvsCollapseChild As New DataGridViewCellStyle
    '            objdgvsCollapseChild.Font = New Font(dgvDataList.Font.FontFamily, 12, FontStyle.Bold)
    '            objdgvsCollapseChild.ForeColor = Color.Black
    '            objdgvsCollapseChild.SelectionBackColor = Color.White
    '            objdgvsCollapseChild.SelectionBackColor = Color.White
    '            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


    '            For i As Integer = 0 To dgvDataList.RowCount - 1
    '                If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                    If dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
    '                        'dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = "▬"
    '                        dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
    '                    End If
    '                    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
    '                Else
    '                    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
    '                    '    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseChild
    '                End If
    '            Next

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Function IsValidData() As Boolean
    '        Try
    '            'If ConfigParameter._Object._LoanVocNoType = 0 Then
    '            '    If txtVoucherNo.Text.Trim.Length <= 0 Then
    '            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Voucher No. cannot be blank. Voucher No. is compulsory information."), enMsgBoxStyle.Information)
    '            '        txtVoucherNo.Focus()
    '            '        Return False
    '            '    End If
    '            'End If

    '            dtChkRow = mdtTran.Select("IsCheck= true AND IsGrp = false")

    '            If dtChkRow.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one employee in order to assign Loan or Advance."), enMsgBoxStyle.Information)
    '                Return False
    '            End If

    '            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Assigned Period is compulsory information. Please select Assigned Period."), enMsgBoxStyle.Information)
    '                cboPayPeriod.Focus()
    '                Return False
    '            End If

    '            If CInt(cboMode.SelectedIndex) > 0 Then
    '                Select Case CInt(cboMode.SelectedIndex)
    '                    Case 0
    '                        If Trim(txtLoanInterest.Text) = "" Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Loan Interest cannot be blank. Loan Interest is compulsory information."), enMsgBoxStyle.Information)
    '                            txtLoanInterest.Focus()
    '                            Return False
    '                        ElseIf CDbl(Trim(txtLoanInterest.Text)) > 100 Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Loan Interest cannot be greater than 100%. Please provide proper Loan Interest."), enMsgBoxStyle.Information)
    '                            txtLoanInterest.Focus()
    '                            Return False
    '                        End If

    '                        If CInt(cboLoanScheduleBy.SelectedValue) <= 0 Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Loan Schedule is compulsory information. Please select Loan Schedule to continue."), enMsgBoxStyle.Information)
    '                            cboLoanScheduleBy.Focus()
    '                            Return False
    '                        End If

    '                        Select Case CInt(cboLoanScheduleBy.SelectedValue)
    '                            Case 1  'AMOUNT WISE
    '                                If txtInstallmentAmt.Decimal = 0 Then
    '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Installment Amount cannot be blank. Installment Amount is compulsory information."), enMsgBoxStyle.Information)
    '                                    txtInstallmentAmt.Focus()
    '                                    Return False
    '                                End If
    '                            Case 2  'PERIOD WISE
    '                                If txtInstallmentNo.Decimal = 0 Then
    '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "No. of Installments cannot be blank. No. of Installments is compulsory information."), enMsgBoxStyle.Information)
    '                                    txtInstallmentNo.Focus()
    '                                    Return False
    '                                End If
    '                        End Select
    '                End Select
    '            End If


    '            '<TODO -- NET AMOUNT VALIDATION IS TO BE KEPT LIKE IN LOAN/ADVANCE ADD EDIT> 

    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)

    '            If dtpDate.Value.Date > objPeriod._End_Date Or dtpDate.Value.Date < objPeriod._Start_Date Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Effective date should be in between ") & _
    '                                objPeriod._Start_Date & Language.getMessage(mstrModuleName, 13, " And ") & _
    '                                objPeriod._End_Date, enMsgBoxStyle.Information)
    '                dtpDate.Focus()
    '                Return False
    '            End If

    '            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Deduction period is compulsory information. Please select Deduction period to continue."), enMsgBoxStyle.Information)
    '                cboDeductionPeriod.Focus()
    '                Return False
    '            End If

    '            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
    '                Dim objDPeriod As New clscommom_period_Tran
    '                objDPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)

    '                If objDPeriod._Start_Date < objPeriod._Start_Date Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Deduction period cannot be less than the Loan Period."), enMsgBoxStyle.Information)
    '                    cboDeductionPeriod.Focus()
    '                    Return False
    '                End If

    '                objDPeriod = Nothing
    '            End If

    '            objPeriod = Nothing

    '            Return True

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
    '        Finally
    '        End Try
    '    End Function

    '    Private Sub SetVisibility()
    '        Try
    '            If ConfigParameter._Object._LoanVocNoType = 1 Then
    '                nudManualNo.Enabled = False
    '            Else
    '                nudManualNo.Enabled = True
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SetColor()
    '        Try
    '            txtInstallmentAmt.BackColor = GUI.ColorComp
    '            txtInstallmentNo.BackColor = GUI.ColorComp
    '            txtLoanInterest.BackColor = GUI.ColorComp
    '            txtPurpose.BackColor = GUI.ColorOptional
    '            txtPrefix.BackColor = GUI.ColorComp
    '            nudManualNo.BackColor = GUI.ColorComp
    '            nudDuration.BackColor = GUI.ColorComp
    '            cboApprover.BackColor = GUI.ColorOptional
    '            cboBranch.BackColor = GUI.ColorOptional
    '            cboDeductionPeriod.BackColor = GUI.ColorComp
    '            cboDepartment.BackColor = GUI.ColorOptional
    '            cboEmployee.BackColor = GUI.ColorOptional
    '            cboJob.BackColor = GUI.ColorOptional
    '            cboLoan.BackColor = GUI.ColorOptional
    '            cboLoanScheduleBy.BackColor = GUI.ColorComp
    '            cboMode.BackColor = GUI.ColorComp
    '            cboPayPeriod.BackColor = GUI.ColorComp
    '            cboSection.BackColor = GUI.ColorOptional
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Function SetLoanAdvanceValue(ByVal intValue As Integer) As Boolean
    '        Try
    '            objLoan_Advance = New clsLoan_Advance
    '            Call Calculate_Loan_Amount(intValue)
    '            Select Case CInt(cboMode.SelectedIndex)
    '                Case 0  'LOAN ASSIGNMENT
    '                    objLoan_Advance._Loan_Amount = CDec(dtChkRow(intValue)("Amount"))
    '                    objLoan_Advance._Loan_Duration = CInt(nudDuration.Value)
    '                    objLoan_Advance._Loanscheduleunkid = CInt(cboLoanScheduleBy.SelectedValue)
    '                    objLoan_Advance._Loanschemeunkid = CInt(dtChkRow(intValue)("GrpId"))
    '                    objLoan_Advance._Net_Amount = mdecNetAmount
    '                    objLoan_Advance._Isloan = True
    '                    'S.SANDEEP [ 18 JAN 2014 ] -- START
    '                    objLoan_Advance._LoanBF_Amount = mdecNetAmount
    '                    'S.SANDEEP [ 18 JAN 2014 ] -- END
    '                Case 1  'ADVANCE ASSIGNMENT
    '                    objLoan_Advance._Advance_Amount = CDec(dtChkRow(intValue)("Amount"))
    '                    objLoan_Advance._Loanscheduleunkid = 0
    '                    objLoan_Advance._Loanschemeunkid = 0
    '                    objLoan_Advance._Net_Amount = 0
    '                    objLoan_Advance._Isloan = False
    '            End Select
    '            objLoan_Advance._Approverunkid = CInt(dtChkRow(intValue)("ApprId"))

    '            If CBool(dtChkRow(intValue)("IsEx")) = True Then
    '                Select Case CInt(cboMode.SelectedIndex)
    '                    Case 0
    '                        objLoan_Advance._Balance_Amount = mdecNetAmount
    '                    Case 1
    '                        objLoan_Advance._Balance_Amount = CDec(dtChkRow(intValue)("Amount"))
    '                End Select
    '            Else
    '                objLoan_Advance._Balance_Amount = 0
    '            End If

    '            Select Case CInt(cboMode.SelectedIndex)
    '                Case 0
    '                    If radSimpleInterest.Checked Then
    '                        objLoan_Advance._Calctype_Id = enLoanCalcId.Simple_Interest '1
    '                    ElseIf radCompoundInterest.Checked Then
    '                        objLoan_Advance._Calctype_Id = enLoanCalcId.Compound_Interest   '2
    '                    ElseIf radReduceBalInterest.Checked Then
    '                        objLoan_Advance._Calctype_Id = enLoanCalcId.Reducing_Amount '3
    '                    End If
    '                Case Else
    '                    objLoan_Advance._Calctype_Id = 0
    '            End Select
    '            objLoan_Advance._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
    '            objLoan_Advance._Effective_Date = dtpDate.Value

    '            Select Case CInt(cboMode.SelectedIndex)
    '                Case 0
    '                    Calculate_Loan_EMI()
    '                    If CInt(cboLoanScheduleBy.SelectedValue) > 0 Then
    '                        Select Case CInt(cboLoanScheduleBy.SelectedValue)
    '                            Case 1  'AMOUNT WISE
    '                                If txtInstallmentAmt.Decimal > mdecNetAmount Then
    '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "The Installments Amount cannot be greater than the Net Amount."), enMsgBoxStyle.Information)
    '                                    Return False
    '                                End If

    '                                If mintTotalEMI > CInt(nudDuration.Value) Then
    '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Total Installment No. cannot be greater than loan duration."), enMsgBoxStyle.Information)
    '                                    Return False
    '                                End If

    '                                If radReduceBalInterest.Checked Then
    '                                    objLoan_Advance._Emi_Amount = mdecInstallmentAmount
    '                                Else
    '                                    objLoan_Advance._Emi_Amount = txtInstallmentAmt.Decimal
    '                                End If
    '                                objLoan_Advance._Emi_Tenure = mintTotalEMI
    '                            Case 2  'PERIOD WISE
    '                                If mdecInstallmentAmount > mdecNetAmount Then
    '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "The Installments Amount cannot be greater than the Net Amount."), enMsgBoxStyle.Information)
    '                                    Return False
    '                                End If
    '                                If txtInstallmentNo.Int > CInt(nudDuration.Value) Then
    '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Total Installment No. cannot be greater than loan duration."), enMsgBoxStyle.Information)
    '                                    Return False
    '                                End If
    '                                objLoan_Advance._Emi_Amount = mdecInstallmentAmount
    '                                objLoan_Advance._Emi_Tenure = CInt(txtInstallmentNo.Text)
    '                        End Select
    '                    End If
    '            End Select

    '            objLoan_Advance._Employeeunkid = CInt(dtChkRow(intValue)("EmpId"))
    '            Select Case CInt(cboMode.SelectedIndex)
    '                Case 0
    '                    objLoan_Advance._Interest_Amount = mdecInterest_Amount
    '                    objLoan_Advance._Interest_Rate = txtLoanInterest.Decimal
    '                Case Else
    '                    objLoan_Advance._Interest_Amount = 0
    '                    objLoan_Advance._Interest_Rate = 0
    '            End Select
    '            objLoan_Advance._Isbrought_Forward = False
    '            objLoan_Advance._Loan_Purpose = txtPurpose.Text
    '            If ConfigParameter._Object._LoanVocNoType = 1 Then
    '                objLoan_Advance._Loanvoucher_No = ""
    '            Else
    '                objLoan_Advance._Loanvoucher_No = txtPrefix.Text.Trim & nudManualNo.Value.ToString
    '            End If
    '            objLoan_Advance._Periodunkid = CInt(cboPayPeriod.SelectedValue)
    '            objLoan_Advance._Userunkid = User._Object._Userunkid
    '            objLoan_Advance._Voiduserunkid = -1
    '            objLoan_Advance._Voiddatetime = Nothing
    '            objLoan_Advance._Isvoid = False
    '            objLoan_Advance._LoanStatus = 1
    '            objLoan_Advance._Processpendingloanunkid = CInt(dtChkRow(intValue)("Pendingunkid"))


    '            Return True

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetLoanAdvanceValue", mstrModuleName)
    '            Return False
    '        Finally
    '        End Try
    '    End Function

    '    Private Sub Calculate_Loan_Amount(ByVal iCnt As Integer)
    '        Try
    '            If radSimpleInterest.Checked Then
    '                mdecNetAmount = 0 : mdecInterest_Amount = 0
    '                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Simple_Interest, CDec(dtChkRow(iCnt)("Amount")), CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount)
    '            ElseIf radCompoundInterest.Checked Then
    '                mdecNetAmount = 0 : mdecInterest_Amount = 0
    '                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Compound_Interest, CDec(dtChkRow(iCnt)("Amount")), CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount)
    '            ElseIf radReduceBalInterest.Checked Then
    '                mdecNetAmount = 0 : mdecInterest_Amount = 0
    '                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Reducing_Amount, CDec(dtChkRow(iCnt)("Amount")), CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount, mdecInstallmentAmount)
    '                'If radReduceBalInterest.Checked Then
    '                '    cboLoanScheduleBy.SelectedValue = 1
    '                '    cboLoanScheduleBy.Enabled = False
    '                '    txtInstallmentAmt.Text = CStr(mdecEMIAmt)
    '                'End If
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub Calculate_Loan_EMI()
    '        Select Case CInt(cboLoanScheduleBy.SelectedValue)
    '            Case 1
    '                If radReduceBalInterest.Checked Then
    '                    objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanScheduleBy.SelectedValue), mdecNetAmount, mdecInstallmentAmount, mintTotalEMI)
    '                Else
    '                    objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanScheduleBy.SelectedValue), mdecNetAmount, txtInstallmentAmt.Decimal, mintTotalEMI)
    '                End If
    '            Case 2
    '                objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanScheduleBy.SelectedValue), mdecNetAmount, mdecInstallmentAmount, txtInstallmentNo.Int)
    '        End Select
    '    End Sub

    '#End Region

    '#Region " Form's Events "

    '    Private Sub frmGlobalAssignLoanAdvance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    '        Try
    '            objLoan_Advance = Nothing : objPendingLoan = Nothing : dtChkRow = Nothing
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "frmGlobalAssignLoanAdvance_FormClosed", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub frmGlobalAssignLoanAdvance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        objLoan_Advance = New clsLoan_Advance : objPendingLoan = New clsProcess_pending_loan
    '        Try
    '            Call Set_Logo(Me, gApplicationType)
    '            'Anjan (02 Sep 2011)-Start
    '            'Issue : Including Language Settings.
    '            Language.setLanguage(Me.Name)
    '            Call OtherSettings()
    '            'Anjan (02 Sep 2011)-End 
    '            Call SetVisibility()
    '            Call SetColor()
    '            Call FillCombo()
    '            Call FillGrid()
    '            mblnIsFormLoad = True
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "frmGlobalAssignLoanAdvance_Load", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub
    '    'Anjan (02 Sep 2011)-Start
    '    'Issue : Including Language Settings.
    '    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '        Dim objfrm As New frmLanguage
    '        Try
    '            If User._Object._Isrighttoleft = True Then
    '                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                objfrm.RightToLeftLayout = True
    '                Call Language.ctlRightToLeftlayOut(objfrm)
    '            End If

    '            Call SetMessages()
    '            clsLoan_Advance.SetMessages()
    '            objfrm._Other_ModuleNames = "clsLoan_Advance"
    '            objfrm.displayDialog(Me)

    '            Call SetLanguage()

    '        Catch ex As System.Exception
    '            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '        Finally
    '            objfrm.Dispose()
    '            objfrm = Nothing
    '        End Try
    '    End Sub
    '    'Anjan (02 Sep 2011)-End 

    '#End Region

    '#Region " Button's Events "

    '    Private Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
    '        Dim blnFlag As Boolean = False
    '        Dim blnFail As Boolean = False
    '        Try
    '            If IsValidData() = False Then Exit Sub

    '            If dtChkRow.Length > 0 Then
    '                For i As Integer = 0 To dtChkRow.Length - 1
    '                    Select Case CInt(cboMode.SelectedIndex)
    '                        Case 0  'LOAN ASSIGNMENT
    '                            If SetLoanAdvanceValue(i) Then
    '                                If objLoan_Advance.Insert Then
    '                                    blnFlag = True
    '                                Else
    '                                    dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.BackColor = Color.Red
    '                                    dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.SelectionBackColor = Color.Red

    '                                    blnFail = True
    '                                    blnFlag = False

    '                                End If
    '                            Else
    '                                dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.BackColor = Color.Red
    '                                dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.SelectionBackColor = Color.Red

    '                                blnFail = True

    '                            End If
    '                        Case 1  'ADVANCE ASSIGNMENT
    '                            If SetLoanAdvanceValue(i) Then
    '                                If objLoan_Advance.Insert Then
    '                                    blnFlag = True
    '                                Else
    '                                    dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.BackColor = Color.Red
    '                                    dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.SelectionBackColor = Color.Red

    '                                    blnFail = True
    '                                    blnFlag = False

    '                                End If
    '                            Else
    '                                dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.BackColor = Color.Red
    '                                dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.SelectionBackColor = Color.Red

    '                                blnFail = True

    '                            End If
    '                    End Select
    '                Next
    '            End If

    '            If blnFlag = True And blnFail = False Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Loan/Advance successfully assigned."), enMsgBoxStyle.Information)
    '                Call FillGrid()
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Some Loan/Advance assignment failed and marked with red color."), enMsgBoxStyle.Information)
    '            End If

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '        Try
    '            Me.Close()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Controls "

    '    Private Sub dgvDataList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataList.CellContentClick, dgvDataList.CellContentDoubleClick
    '        Try
    '            Dim i As Integer
    '            If e.RowIndex = -1 Then Exit Sub

    '            If Me.dgvDataList.IsCurrentCellDirty Then
    '                Me.dgvDataList.CommitEdit(DataGridViewDataErrorContexts.Commit)
    '            End If

    '            If CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                Select Case CInt(e.ColumnIndex)
    '                    Case 0
    '                        'If dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value.ToString = "▬" Then
    '                        '    dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "+"
    '                        'Else
    '                        '    dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "▬"
    '                        'End If

    '                        If dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
    '                            dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
    '                        Else
    '                            dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
    '                        End If

    '                        For i = e.RowIndex + 1 To dgvDataList.RowCount - 1
    '                            If CInt(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvDataList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
    '                                If dgvDataList.Rows(i).Visible = False Then
    '                                    dgvDataList.Rows(i).Visible = True
    '                                Else
    '                                    dgvDataList.Rows(i).Visible = False
    '                                End If
    '                            Else
    '                                Exit For
    '                            End If
    '                        Next
    '                    Case 1
    '                        For i = e.RowIndex + 1 To dgvDataList.RowCount - 1
    '                            Dim blnFlg As Boolean = CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value)
    '                            If CInt(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvDataList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
    '                                dgvDataList.Rows(i).Cells(objdgcolhSelect.Index).Value = blnFlg
    '                            End If
    '                        Next
    '                End Select
    '            End If

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "dgvDataList_CellContentClick", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged, _
    '                                                                                                                   cboDepartment.SelectedIndexChanged, _
    '                                                                                                                   cboSection.SelectedIndexChanged, _
    '                                                                                                                   cboJob.SelectedIndexChanged, _
    '                                                                                                                   cboEmployee.SelectedIndexChanged, _
    '                                                                                                                   cboApprover.SelectedIndexChanged, _
    '                                                                                                                   cboMode.SelectedIndexChanged, _
    '                                                                                                                   cboLoan.SelectedIndexChanged
    '        If mblnIsFormLoad = False Then Exit Sub

    '        Select Case CType(sender, ComboBox).Name.ToUpper
    '            Case "CBOMODE"
    '                Select Case cboMode.SelectedIndex
    '                    Case 0
    '                        cboLoan.SelectedValue = 0
    '                        cboLoan.Enabled = True
    '                        gbLoanInfo.Enabled = True
    '                    Case 1
    '                        cboLoan.SelectedValue = 0
    '                        cboLoan.Enabled = False
    '                        gbLoanInfo.Enabled = False
    '                End Select
    '        End Select

    '        Call FillGrid()
    '    End Sub

    '    Private Sub cboLoanScheduleBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanScheduleBy.SelectedIndexChanged
    '        Try
    '            Select Case CInt(cboLoanScheduleBy.SelectedValue)
    '                Case 1  'AMOUNT WISE
    '                    lblEMIAmount.Visible = True
    '                    txtInstallmentAmt.Visible = True
    '                    lblNoOfInstallment.Visible = False
    '                    txtInstallmentNo.Visible = False
    '                Case 2  'PERIOD WISE
    '                    lblEMIAmount.Visible = False
    '                    txtInstallmentAmt.Visible = False
    '                    lblNoOfInstallment.Visible = True
    '                    txtInstallmentNo.Visible = True
    '            End Select
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "cboLoanScheduleBy_SelectedIndexChanged", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
    '        Try
    '            If txtInstallmentAmt.Text = "" Or txtInstallmentAmt.Text = "." Then Exit Sub
    '            Call Calculate_Loan_EMI()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub txtInstallmentNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallmentNo.TextChanged
    '        Try
    '            If txtInstallmentNo.Text = "" Or txtInstallmentNo.Text = "." Then Exit Sub
    '            Call Calculate_Loan_EMI()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "txtInstallmentNo_TextChanged", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub radReduceBalInterest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radReduceBalInterest.CheckedChanged
    '        Try
    '            If radReduceBalInterest.Checked Then
    '                cboLoanScheduleBy.SelectedValue = 1 : cboLoanScheduleBy.Enabled = False
    '                txtInstallmentAmt.Enabled = False
    '                txtInstallmentNo.Enabled = False
    '            Else
    '                cboLoanScheduleBy.SelectedValue = 0 : cboLoanScheduleBy.Enabled = True
    '                txtInstallmentAmt.Enabled = True
    '                txtInstallmentNo.Enabled = True
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "radReduceBalInterest_CheckedChanged", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.btnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnApply.GradientBackColor = GUI._ButttonBackColor
            Me.btnApply.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLoan.Text = Language._Object.getCaption(Me.lblLoan.Name, Me.lblLoan.Text)
			Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbBasicInfo.Text = Language._Object.getCaption(Me.gbBasicInfo.Name, Me.gbBasicInfo.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblPurpose.Text = Language._Object.getCaption(Me.lblPurpose.Name, Me.lblPurpose.Text)
			Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.Name, Me.lblVoucherNo.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.Name, Me.lblInterestCalcType.Text)
            Me.lblGLoanCalcType.Text = Language._Object.getCaption(Me.lblGLoanCalcType.Name, Me.lblGLoanCalcType.Text)
            Me.radApplyToChecked.Text = Language._Object.getCaption(Me.radApplyToChecked.Name, Me.radApplyToChecked.Text)
            Me.radApplyToAll.Text = Language._Object.getCaption(Me.radApplyToAll.Name, Me.radApplyToAll.Text)
            Me.lblGRate.Text = Language._Object.getCaption(Me.lblGRate.Name, Me.lblGRate.Text)
            Me.btnApply.Text = Language._Object.getCaption(Me.btnApply.Name, Me.btnApply.Text)
			Me.dgcolhAppNo.HeaderText = Language._Object.getCaption(Me.dgcolhAppNo.Name, Me.dgcolhAppNo.HeaderText)
			Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
			Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
			Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhDeductionPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhDeductionPeriod.Name, Me.dgcolhDeductionPeriod.HeaderText)
			Me.dgcolhEffDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffDate.Name, Me.dgcolhEffDate.HeaderText)
			Me.dgcolhLoanType.HeaderText = Language._Object.getCaption(Me.dgcolhLoanType.Name, Me.dgcolhLoanType.HeaderText)
			Me.dgcolhInterestType.HeaderText = Language._Object.getCaption(Me.dgcolhInterestType.Name, Me.dgcolhInterestType.HeaderText)
			Me.dgcolhMappedHead.HeaderText = Language._Object.getCaption(Me.dgcolhMappedHead.Name, Me.dgcolhMappedHead.HeaderText)
			Me.dgcolhRate.HeaderText = Language._Object.getCaption(Me.dgcolhRate.Name, Me.dgcolhRate.HeaderText)
			Me.dgcolhNoOfEMI.HeaderText = Language._Object.getCaption(Me.dgcolhNoOfEMI.Name, Me.dgcolhNoOfEMI.HeaderText)
			Me.dgcolhInstallmentAmount.HeaderText = Language._Object.getCaption(Me.dgcolhInstallmentAmount.Name, Me.dgcolhInstallmentAmount.HeaderText)
			Me.lblMappedHead.Text = Language._Object.getCaption(Me.lblMappedHead.Name, Me.lblMappedHead.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 5, "Sorry, Rate of Interest cannot exceed 100. Please enter correct rate of interest.")
			Language.setMessage(mstrModuleName, 7, "Sorry, Deduction period is mandatory information. Please select deduction period to continue.")
			Language.setMessage(mstrModuleName, 8, "Sorry, Currency is mandatory information. Please select currency to continue.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Assignment period is mandatory information. Please select assignment period to continue.")
			Language.setMessage(mstrModuleName, 10, "Sorry, Please check atleast one approved loan to assign.")
			Language.setMessage(mstrModuleName, 11, "Sorry, Deduction period is mandatory information. Please select deduction period to continue.")
			Language.setMessage(mstrModuleName, 12, "Sorry, Effective date cannot be less then the assigned period date.")
			Language.setMessage(mstrModuleName, 13, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type to continue.")
			Language.setMessage(mstrModuleName, 14, "Sorry, Loan rate is mandatory information. Please enter loan rate to continue.")
			Language.setMessage(mstrModuleName, 15, "Sorry, No Of Installment cannot be less than Zero. Please enter correct no of installment to continue.")
			Language.setMessage(mstrModuleName, 16, "Sorry, Installment amount cannot be less than Zero. Please enter correct installment amount to continue.")
			Language.setMessage(mstrModuleName, 17, "Sorry, Interest calculation type is mandatory information. Please select Interest calculation type to continue.")
			Language.setMessage(mstrModuleName, 18, "Sorry, you cannot assign Loan/Advance. Reason: Payslip Payment is already done for selected period for Employee")
			Language.setMessage(mstrModuleName, 19, "Sorry, you cannot assign Loan/Advance. Reason: Payroll process is already done for the last date of selected deduction period for Employee.")
                        Language.setMessage(mstrModuleName, 20, "No Data records found.")
                        Language.setMessage(mstrModuleName, 21, "Please either tick Apply To Checked OR Apply To All for further process.")
                        Language.setMessage(mstrModuleName, 22, "Please check atleast one record from the list for further process.")
                        Language.setMessage(mstrModuleName, 23, "Please select atleast one Loan calculation type for further process.")
                        Language.setMessage(mstrModuleName, 24, "Please select atleast one Loan Interest calculation type for further process.")
                        Language.setMessage(mstrModuleName, 25, "Please specify interest rate for further process.")
                        Language.setMessage(mstrModuleName, 26, "You have not ticked all records for assignment.")
                        Language.setMessage(mstrModuleName, 27, "Do you want to continue?")
                        Language.setMessage(mstrModuleName, 28, "Type to Search")
			Language.setMessage(mstrModuleName, 29, "Installment months cannot be greater than")
			Language.setMessage(mstrModuleName, 30, " for")
			Language.setMessage(mstrModuleName, 31, " Scheme and Application No is")
			Language.setMessage(mstrModuleName, 32, "Sorry, you can not assign loan for the selected scheme: Reason, The selected loan scheme is already assigned and having (In-progress or On hold) status for selected period for Employee")
			Language.setMessage(mstrModuleName, 33, "for Scheme")
			Language.setMessage(mstrModuleName, 34, "Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations.")
			Language.setMessage(mstrModuleName, 35, "Sorry, you can not assign Advance: Reason, The Advance is already assigned and having (In-progress or On hold) status for selected period for Employee")
			Language.setMessage(mstrModuleName, 36, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 37, "Please select Mapped Head to continue.")
			Language.setMessage(mstrModuleName, 38, "Sorry, Loan tenure should not be greater than employee tenure month")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class