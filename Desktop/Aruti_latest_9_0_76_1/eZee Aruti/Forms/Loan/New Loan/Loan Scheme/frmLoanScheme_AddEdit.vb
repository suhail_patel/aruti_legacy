﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmLoanScheme_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLoanScheme_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLoanScheme As clsLoan_Scheme
    Private menAction As enAction = enAction.ADD_ONE
    Private mintLoanSchemeunkid As Integer = -1

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private mstrSearchText As String = ""
    'Sohail (11 Apr 2018) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintLoanSchemeunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintLoanSchemeunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            txtMinSalary.BackColor = GUI.ColorOptional
            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 52
            txtMaxLonaAmount.BackColor = GUI.ColorOptional
            'S.SANDEEP [20-SEP-2017] -- END

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            cboLoanCalcType.BackColor = GUI.ColorComp
            cboInterestCalcType.BackColor = GUI.ColorOptional
            txtLoanRate.BackColor = GUI.ColorOptional
            'S.SANDEEP [20-SEP-2017] -- END
            txtEMIExceedPerc.BackColor = GUI.ColorOptional 'Sohail (22 Sep 2017)
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            cboCostCenter.BackColor = GUI.ColorOptional
            'Sohail (14 Mar 2019) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            cboNetPay.BackColor = GUI.ColorOptional
            cboMappedHead.BackColor = GUI.ColorOptional
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objExchangeRate As New clsExchangeRate
        objExchangeRate._ExchangeRateunkid = 1

        txtCode.Text = objLoanScheme._Code
        txtName.Text = objLoanScheme._Name
        txtDescription.Text = objLoanScheme._Description
        txtMinSalary.Text = Format(objLoanScheme._Minnetsalary, objExchangeRate._fmtCurrency)


        'Pinkal (14-Apr-2015) -- Start
        'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
        chkShowonESS.Checked = objLoanScheme._IsShowonESS
        'Pinkal (14-Apr-2015) -- End

        'Nilay (19-Oct-2016) -- Start
        'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
        chkShowLoanBalOnPayslip.Checked = objLoanScheme._IsShowLoanBalOnPayslip
        'Nilay (19-Oct-2016) -- End

        'If objLoanScheme._Isinclude_Parttime = True Then
        '    chkIncludePartTimeEmp.Checked = True
        'Else
        '    chkIncludePartTimeEmp.Checked = False
        'End If

        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 52
        txtMaxLonaAmount.Text = Format(objLoanScheme._MaxLoanAmountLimit, objExchangeRate._fmtCurrency)
        'S.SANDEEP [20-SEP-2017] -- END

        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 50
        cboLoanCalcType.SelectedValue = objLoanScheme._LoanCalcTypeId
        cboInterestCalcType.SelectedValue = objLoanScheme._InterestCalctypeId
        txtLoanRate.Text = CStr(objLoanScheme._InterestRate)
        'S.SANDEEP [20-SEP-2017] -- END
        txtEMIExceedPerc.Text = Format(objLoanScheme._EMI_NetPayPercentage, objExchangeRate._fmtCurrency) 'Sohail (22 Sep 2017)


        'Varsha (25 Nov 2017) -- Start
        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
        txtMaxNoOfInstallment.Text = CStr(objLoanScheme._MaxNoOfInstallment)
        'Varsha (25 Nov 2017) -- End

        'Sohail (11 Apr 2018) -- Start
        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
        Call txtEMIExceedPerc_Leave(txtEMIExceedPerc, New System.EventArgs)
        cboNetPay.SelectedValue = objLoanScheme._TranheadUnkid
        'Sohail (11 Apr 2018) -- End
        'Sohail (14 Mar 2019) -- Start
        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
        cboCostCenter.SelectedValue = objLoanScheme._Costcenterunkid
        'Sohail (14 Mar 2019) -- End
        'Sohail (29 Apr 2019) -- Start
        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
        cboMappedHead.SelectedValue = objLoanScheme._Mapped_TranheadUnkid
        'Sohail (29 Apr 2019) -- End

        'Sohail (02 Apr 2018) -- Start
        'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
        If objLoanScheme._RefLoanSchemeUnkid > 0 Then
            EnableControls(False)
        End If
        'Sohail (02 Apr 2018) -- End

        objExchangeRate = Nothing
    End Sub

    Private Sub SetValue()
        objLoanScheme._code = txtCode.Text
        objLoanScheme._name = txtName.Text
        objLoanScheme._Description = txtDescription.Text
        objLoanScheme._Minnetsalary = txtMinSalary.Decimal

        'Pinkal (14-Apr-2015) -- Start
        'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
        objLoanScheme._IsShowonESS = chkShowonESS.Checked
        'Pinkal (14-Apr-2015) -- End

        'If chkIncludePartTimeEmp.Checked Then
        '    objLoanScheme._Isinclude_Parttime = True
        'Else
        '    objLoanScheme._Isinclude_Parttime = False
        'End If

        'Nilay (19-Oct-2016) -- Start
        'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
        objLoanScheme._IsShowLoanBalOnPayslip = chkShowLoanBalOnPayslip.Checked
        'Nilay (19-Oct-2016) -- End

        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 52
        objLoanScheme._MaxLoanAmountLimit = txtMaxLonaAmount.Decimal
        'S.SANDEEP [20-SEP-2017] -- END

        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 50
        objLoanScheme._LoanCalcTypeId = CInt(cboLoanCalcType.SelectedValue)
        objLoanScheme._InterestCalctypeId = CInt(cboInterestCalcType.SelectedValue)
        objLoanScheme._InterestRate = CDec(txtLoanRate.Text)
        'S.SANDEEP [20-SEP-2017] -- END
        'Sohail (22 Sep 2017) -- Start
        'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
        objLoanScheme._EMI_NetPayPercentage = txtEMIExceedPerc.Decimal
        'Sohail (22 Sep 2017) -- End

        'Varsha (25 Nov 2017) -- Start
        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
        objLoanScheme._MaxNoOfInstallment = txtMaxNoOfInstallment.Int
        'Varsha (25 Nov 2017) -- End

        'Sohail (11 Apr 2018) -- Start
        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
        objLoanScheme._TranheadUnkid = CInt(cboNetPay.SelectedValue)
        'Sohail (11 Apr 2018) -- End
        'Sohail (14 Mar 2019) -- Start
        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
        objLoanScheme._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
        'Sohail (14 Mar 2019) -- End
        'Sohail (29 Apr 2019) -- Start
        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
        objLoanScheme._Mapped_TranheadUnkid = CInt(cboMappedHead.SelectedValue)
        'Sohail (29 Apr 2019) -- End

    End Sub

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private Sub FillCombo()
        Dim objLoan_Advance As New clsLoan_Advance
        'Sohail (11 Apr 2018) -- Start
        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
        Dim objHead As New clsTransactionHead
        'Sohail (11 Apr 2018) -- End
        'Sohail (14 Mar 2019) -- Start
        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
        Dim objCCetnter As New clscostcenter_master
        'Sohail (14 Mar 2019) -- End
        Dim dsList As New DataSet
        Try
            dsList = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Sohail (11 Apr 2018) -- Start
            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
            'Sohail (29 Apr 2019) -- Start
            'Isssue#0003774(Sport Pesa):  net pay transhead is not showing on the list. Optional basic salary head not working. when you map other transaction head on "basic salary head (Optional)" (loan setting->configuration) the system gives error during application
            'dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False)
            'Sohail (29 Apr 2019) -- End
            With cboNetPay
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboNetPay)
            End With
            'Sohail (11 Apr 2018) -- End

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            With cboMappedHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboMappedHead)
            End With
            'Sohail (29 Apr 2019) -- End

            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            dsList = objCCetnter.getComboList("List", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboCostCenter)
            End With
            'Sohail (14 Mar 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objLoan_Advance = Nothing : dsList.Dispose()
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objCCetnter = Nothing
            'Sohail (14 Mar 2019) -- End
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (02 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
    Private Sub EnableControls(ByVal blnEnable As Boolean)
        Try
            'Dim lst As IEnumerable(Of Control) = pnlMainInfo.Controls.OfType(Of Control)().Where(Function(t) t.Name <> objFooter.Name)
            'For Each ctrl In lst
            '    ctrl.Enabled = blnEnable
            'Next
            Dim lst As IEnumerable(Of Control) = gbLoanInfo.Controls.OfType(Of Control).Where(Function(x) x.Name <> txtCode.Name AndAlso x.Name <> txtName.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableControls", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Apr 2018) -- End

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 7, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Apr 2018) -- End

#End Region

#Region " Form's Events "

    Private Sub frmLoanScheme_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoanScheme = Nothing
    End Sub

    Private Sub frmLoanScheme_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmLoanScheme_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmLoanScheme_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoanScheme = New clsLoan_Scheme
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call setColor()

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            Call FillCombo()
            'S.SANDEEP [20-SEP-2017] -- END

            If menAction = enAction.EDIT_ONE Then
                objLoanScheme._Loanschemeunkid = mintLoanSchemeunkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanScheme_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Scheme.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Scheme"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
#Region " Combobox Event(s) "

    Private Sub cboLoanCalcType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanCalcType.SelectedIndexChanged
        Try
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                cboMappedHead.SelectedValue = 0
                cboMappedHead.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- End
            If CInt(cboLoanCalcType.SelectedValue) > 0 Then
                Select Case CInt(cboLoanCalcType.SelectedValue)
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'Case enLoanCalcId.No_Interest
                    Case enLoanCalcId.No_Interest, enLoanCalcId.No_Interest_With_Mapped_Head
                        'Sohail (29 Apr 2019) -- End
                        cboInterestCalcType.SelectedValue = 0
                        txtLoanRate.Text = CStr(0)
                        pnlCalcType.Enabled = False
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                            cboMappedHead.Enabled = True
                        End If
                        'Sohail (29 Apr 2019) -- End
                    Case Else
                        pnlCalcType.Enabled = True
                End Select
            Else
                cboInterestCalcType.SelectedValue = 0
                txtLoanRate.Text = CStr(0)
                pnlCalcType.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanCalcType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private Sub cboNetPay_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboNetPay.KeyPress, cboCostCenter.KeyPress, cboMappedHead.KeyPress
        'Sohail (29 Apr 2019) - [cboMappedHead]
        'Sohail (14 Mar 2019) - [cboCostCenter.KeyPress]
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    'Sohail (14 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                    '.CodeMember = "code"
                    If cbo.Name = cboCostCenter.Name Then
                        .CodeMember = "costcentercode"
                    Else
                    .CodeMember = "code"
                    End If
                    'Sohail (14 Mar 2019) -- End


                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNetPay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNetPay.SelectedIndexChanged, cboCostCenter.SelectedIndexChanged, cboMappedHead.SelectedIndexChanged
        'Sohail (29 Apr 2019) - [cboMappedHead]
        'Sohail (14 Mar 2019) - [cboCostCenter.SelectedIndexChanged]
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNetPay_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNetPay.GotFocus, cboCostCenter.GotFocus, cboMappedHead.GotFocus
        'Sohail (29 Apr 2019) - [cboMappedHead]
        'Sohail (14 Mar 2019) - [cboCostCenter.GotFocus]
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNetPay_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNetPay.Leave, cboCostCenter.Leave, cboMappedHead.Leave
        'Sohail (29 Apr 2019) - [cboMappedHead]
        'Sohail (14 Mar 2019) - [cboCostCenter.Leave]
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Apr 2018) -- End

#End Region
    'S.SANDEEP [20-SEP-2017] -- END

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try


            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan Scheme cannot be blank. Loan Scheme is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            '--Vimal 28-Aug-2010 Start

            If Trim(txtMinSalary.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Minimum Salary cannot be blank. Please insert Minimum Salary."), enMsgBoxStyle.Information)
                txtMinSalary.Focus()
                Exit Sub
            End If

            '--Vimal 28-Aug-2010 End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            If CInt(cboLoanCalcType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type."), enMsgBoxStyle.Information)
                cboLoanCalcType.Focus()
                Exit Sub
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboMappedHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Mapped Head."), enMsgBoxStyle.Information)
                cboMappedHead.Focus()
                Exit Sub
                'Sohail (29 Apr 2019) -- End
            End If
            'S.SANDEEP [20-SEP-2017] -- END

            'Sohail (05 Oct 2017) -- Start
            'Enhancement - 70.1 - Do not allow to enter % more than 100 for EMI limitation on previous net pay.
            If txtEMIExceedPerc.Decimal < 0 OrElse txtEMIExceedPerc.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Percentage should be in between 0 and 100."), enMsgBoxStyle.Information)
                txtEMIExceedPerc.Focus()
                Exit Sub
            End If
            'Sohail (05 Oct 2017) -- End

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            If txtMaxNoOfInstallment.Int < 0 OrElse txtMaxNoOfInstallment.Int > 360 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Maximum number of installment should not be greater than 360."), enMsgBoxStyle.Information)
                txtMaxNoOfInstallment.Focus()
                Exit Sub
            End If
            'Varsha (25 Nov 2017) -- End


            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objLoanScheme.Update()
            Else
                blnFlag = objLoanScheme.Insert()
            End If

            If blnFlag = False And objLoanScheme._Message <> "" Then
                eZeeMsgBox.Show(objLoanScheme._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLoanScheme = Nothing
                    objLoanScheme = New clsLoan_Scheme
                    Call GetValue()
                    txtCode.Focus()
                Else
                    mintLoanSchemeunkid = objLoanScheme._Loanschemeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
#Region " Other Controls Events "
    Private Sub txtEMIExceedPerc_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEMIExceedPerc.Leave
        Try
            If txtEMIExceedPerc.Decimal = 0 Then
                cboNetPay.SelectedValue = 0
                cboNetPay.Enabled = False

            Else
                cboNetPay.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtEMIExceedPerc_Leave", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (11 Apr 2018) -- End


   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbLoanInfo.Text = Language._Object.getCaption(Me.gbLoanInfo.Name, Me.gbLoanInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lnEligibility.Text = Language._Object.getCaption(Me.lnEligibility.Name, Me.lnEligibility.Text)
			Me.lblMinSalary.Text = Language._Object.getCaption(Me.lblMinSalary.Name, Me.lblMinSalary.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.chkShowonESS.Text = Language._Object.getCaption(Me.chkShowonESS.Name, Me.chkShowonESS.Text)
			Me.chkShowLoanBalOnPayslip.Text = Language._Object.getCaption(Me.chkShowLoanBalOnPayslip.Name, Me.chkShowLoanBalOnPayslip.Text)
			Me.lblMaximumLoanAmount.Text = Language._Object.getCaption(Me.lblMaximumLoanAmount.Name, Me.lblMaximumLoanAmount.Text)
			Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.Name, Me.lblInterestCalcType.Text)
			Me.lblLoanCalcType.Text = Language._Object.getCaption(Me.lblLoanCalcType.Name, Me.lblLoanCalcType.Text)
			Me.lblLoanInterest.Text = Language._Object.getCaption(Me.lblLoanInterest.Name, Me.lblLoanInterest.Text)
			Me.elDefaultInfo.Text = Language._Object.getCaption(Me.elDefaultInfo.Name, Me.elDefaultInfo.Text)
			Me.lblEMIExceedPerc.Text = Language._Object.getCaption(Me.lblEMIExceedPerc.Name, Me.lblEMIExceedPerc.Text)
			Me.lblEMIExceedPerc2.Text = Language._Object.getCaption(Me.lblEMIExceedPerc2.Name, Me.lblEMIExceedPerc2.Text)
			Me.lblMaxNoOfInstallment.Text = Language._Object.getCaption(Me.lblMaxNoOfInstallment.Name, Me.lblMaxNoOfInstallment.Text)
			Me.lblNetPay.Text = Language._Object.getCaption(Me.lblNetPay.Name, Me.lblNetPay.Text)
			Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblMappedHead.Text = Language._Object.getCaption(Me.lblMappedHead.Name, Me.lblMappedHead.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Loan Scheme cannot be blank. Loan Scheme is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Minimum Salary cannot be blank. Please insert Minimum Salary.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Percentage should be in between 0 and 100.")
			Language.setMessage(mstrModuleName, 6, "Sorry, Maximum number of installment should not be greater than 360.")
			Language.setMessage(mstrModuleName, 7, "Type to Search")
			Language.setMessage(mstrModuleName, 8, "Please select Mapped Head.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class