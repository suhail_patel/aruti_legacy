﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanScheme_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanScheme_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbLoanInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboMappedHead = New System.Windows.Forms.ComboBox
        Me.lblMappedHead = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboNetPay = New System.Windows.Forms.ComboBox
        Me.lblNetPay = New System.Windows.Forms.Label
        Me.txtMaxNoOfInstallment = New eZee.TextBox.NumericTextBox
        Me.lblMaxNoOfInstallment = New System.Windows.Forms.Label
        Me.lblEMIExceedPerc2 = New System.Windows.Forms.Label
        Me.txtEMIExceedPerc = New eZee.TextBox.NumericTextBox
        Me.lblEMIExceedPerc = New System.Windows.Forms.Label
        Me.elDefaultInfo = New eZee.Common.eZeeLine
        Me.pnlCalcType = New System.Windows.Forms.Panel
        Me.cboInterestCalcType = New System.Windows.Forms.ComboBox
        Me.txtLoanRate = New eZee.TextBox.NumericTextBox
        Me.lblInterestCalcType = New System.Windows.Forms.Label
        Me.lblLoanInterest = New System.Windows.Forms.Label
        Me.cboLoanCalcType = New System.Windows.Forms.ComboBox
        Me.lblLoanCalcType = New System.Windows.Forms.Label
        Me.lblMaximumLoanAmount = New System.Windows.Forms.Label
        Me.txtMaxLonaAmount = New eZee.TextBox.NumericTextBox
        Me.chkShowLoanBalOnPayslip = New System.Windows.Forms.CheckBox
        Me.chkShowonESS = New System.Windows.Forms.CheckBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lnEligibility = New eZee.Common.eZeeLine
        Me.txtMinSalary = New eZee.TextBox.NumericTextBox
        Me.lblMinSalary = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbLoanInfo.SuspendLayout()
        Me.pnlCalcType.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbLoanInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(636, 489)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbLoanInfo
        '
        Me.gbLoanInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLoanInfo.Checked = False
        Me.gbLoanInfo.CollapseAllExceptThis = False
        Me.gbLoanInfo.CollapsedHoverImage = Nothing
        Me.gbLoanInfo.CollapsedNormalImage = Nothing
        Me.gbLoanInfo.CollapsedPressedImage = Nothing
        Me.gbLoanInfo.CollapseOnLoad = False
        Me.gbLoanInfo.Controls.Add(Me.cboMappedHead)
        Me.gbLoanInfo.Controls.Add(Me.lblMappedHead)
        Me.gbLoanInfo.Controls.Add(Me.cboCostCenter)
        Me.gbLoanInfo.Controls.Add(Me.lblCostCenter)
        Me.gbLoanInfo.Controls.Add(Me.cboNetPay)
        Me.gbLoanInfo.Controls.Add(Me.lblNetPay)
        Me.gbLoanInfo.Controls.Add(Me.txtMaxNoOfInstallment)
        Me.gbLoanInfo.Controls.Add(Me.lblMaxNoOfInstallment)
        Me.gbLoanInfo.Controls.Add(Me.lblEMIExceedPerc2)
        Me.gbLoanInfo.Controls.Add(Me.txtEMIExceedPerc)
        Me.gbLoanInfo.Controls.Add(Me.lblEMIExceedPerc)
        Me.gbLoanInfo.Controls.Add(Me.elDefaultInfo)
        Me.gbLoanInfo.Controls.Add(Me.pnlCalcType)
        Me.gbLoanInfo.Controls.Add(Me.cboLoanCalcType)
        Me.gbLoanInfo.Controls.Add(Me.lblLoanCalcType)
        Me.gbLoanInfo.Controls.Add(Me.lblMaximumLoanAmount)
        Me.gbLoanInfo.Controls.Add(Me.txtMaxLonaAmount)
        Me.gbLoanInfo.Controls.Add(Me.chkShowLoanBalOnPayslip)
        Me.gbLoanInfo.Controls.Add(Me.chkShowonESS)
        Me.gbLoanInfo.Controls.Add(Me.lblDescription)
        Me.gbLoanInfo.Controls.Add(Me.txtDescription)
        Me.gbLoanInfo.Controls.Add(Me.lnEligibility)
        Me.gbLoanInfo.Controls.Add(Me.txtMinSalary)
        Me.gbLoanInfo.Controls.Add(Me.lblMinSalary)
        Me.gbLoanInfo.Controls.Add(Me.txtName)
        Me.gbLoanInfo.Controls.Add(Me.lblName)
        Me.gbLoanInfo.Controls.Add(Me.txtCode)
        Me.gbLoanInfo.Controls.Add(Me.lblCode)
        Me.gbLoanInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbLoanInfo.ExpandedHoverImage = Nothing
        Me.gbLoanInfo.ExpandedNormalImage = Nothing
        Me.gbLoanInfo.ExpandedPressedImage = Nothing
        Me.gbLoanInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanInfo.HeaderHeight = 25
        Me.gbLoanInfo.HeaderMessage = ""
        Me.gbLoanInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLoanInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanInfo.HeightOnCollapse = 0
        Me.gbLoanInfo.LeftTextSpace = 0
        Me.gbLoanInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbLoanInfo.Name = "gbLoanInfo"
        Me.gbLoanInfo.OpenHeight = 300
        Me.gbLoanInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanInfo.ShowBorder = True
        Me.gbLoanInfo.ShowCheckBox = False
        Me.gbLoanInfo.ShowCollapseButton = False
        Me.gbLoanInfo.ShowDefaultBorderColor = True
        Me.gbLoanInfo.ShowDownButton = False
        Me.gbLoanInfo.ShowHeader = True
        Me.gbLoanInfo.Size = New System.Drawing.Size(636, 439)
        Me.gbLoanInfo.TabIndex = 0
        Me.gbLoanInfo.Temp = 0
        Me.gbLoanInfo.Text = "Loan Scheme Information"
        Me.gbLoanInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMappedHead
        '
        Me.cboMappedHead.DropDownWidth = 250
        Me.cboMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMappedHead.FormattingEnabled = True
        Me.cboMappedHead.Location = New System.Drawing.Point(453, 233)
        Me.cboMappedHead.Name = "cboMappedHead"
        Me.cboMappedHead.Size = New System.Drawing.Size(171, 21)
        Me.cboMappedHead.TabIndex = 356
        '
        'lblMappedHead
        '
        Me.lblMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMappedHead.Location = New System.Drawing.Point(339, 235)
        Me.lblMappedHead.Name = "lblMappedHead"
        Me.lblMappedHead.Size = New System.Drawing.Size(108, 15)
        Me.lblMappedHead.TabIndex = 355
        Me.lblMappedHead.Text = "Mapped Head"
        Me.lblMappedHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownWidth = 250
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(162, 184)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(171, 21)
        Me.cboCostCenter.TabIndex = 4
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(8, 186)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(148, 16)
        Me.lblCostCenter.TabIndex = 353
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNetPay
        '
        Me.cboNetPay.DropDownWidth = 250
        Me.cboNetPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNetPay.FormattingEnabled = True
        Me.cboNetPay.Location = New System.Drawing.Point(162, 360)
        Me.cboNetPay.Name = "cboNetPay"
        Me.cboNetPay.Size = New System.Drawing.Size(171, 21)
        Me.cboNetPay.TabIndex = 8
        '
        'lblNetPay
        '
        Me.lblNetPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetPay.Location = New System.Drawing.Point(14, 362)
        Me.lblNetPay.Name = "lblNetPay"
        Me.lblNetPay.Size = New System.Drawing.Size(142, 16)
        Me.lblNetPay.TabIndex = 350
        Me.lblNetPay.Text = "Net Pay head (Optional)"
        Me.lblNetPay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMaxNoOfInstallment
        '
        Me.txtMaxNoOfInstallment.AllowNegative = True
        Me.txtMaxNoOfInstallment.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxNoOfInstallment.DigitsInGroup = 0
        Me.txtMaxNoOfInstallment.Flags = 0
        Me.txtMaxNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxNoOfInstallment.Location = New System.Drawing.Point(162, 387)
        Me.txtMaxNoOfInstallment.MaxDecimalPlaces = 0
        Me.txtMaxNoOfInstallment.MaxWholeDigits = 21
        Me.txtMaxNoOfInstallment.Name = "txtMaxNoOfInstallment"
        Me.txtMaxNoOfInstallment.Prefix = ""
        Me.txtMaxNoOfInstallment.RangeMax = 1.7976931348623157E+308
        Me.txtMaxNoOfInstallment.RangeMin = -1.7976931348623157E+308
        Me.txtMaxNoOfInstallment.Size = New System.Drawing.Size(171, 21)
        Me.txtMaxNoOfInstallment.TabIndex = 9
        Me.txtMaxNoOfInstallment.Text = "0"
        Me.txtMaxNoOfInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxNoOfInstallment
        '
        Me.lblMaxNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxNoOfInstallment.Location = New System.Drawing.Point(11, 391)
        Me.lblMaxNoOfInstallment.Name = "lblMaxNoOfInstallment"
        Me.lblMaxNoOfInstallment.Size = New System.Drawing.Size(145, 16)
        Me.lblMaxNoOfInstallment.TabIndex = 347
        Me.lblMaxNoOfInstallment.Text = "Maximum No. Of Installment"
        Me.lblMaxNoOfInstallment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEMIExceedPerc2
        '
        Me.lblEMIExceedPerc2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIExceedPerc2.Location = New System.Drawing.Point(339, 335)
        Me.lblEMIExceedPerc2.Name = "lblEMIExceedPerc2"
        Me.lblEMIExceedPerc2.Size = New System.Drawing.Size(141, 16)
        Me.lblEMIExceedPerc2.TabIndex = 345
        Me.lblEMIExceedPerc2.Text = "% of previous net pay"
        Me.lblEMIExceedPerc2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEMIExceedPerc
        '
        Me.txtEMIExceedPerc.AllowNegative = False
        Me.txtEMIExceedPerc.BackColor = System.Drawing.SystemColors.Window
        Me.txtEMIExceedPerc.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEMIExceedPerc.DigitsInGroup = 0
        Me.txtEMIExceedPerc.Flags = 65536
        Me.txtEMIExceedPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEMIExceedPerc.Location = New System.Drawing.Point(278, 333)
        Me.txtEMIExceedPerc.MaxDecimalPlaces = 6
        Me.txtEMIExceedPerc.MaxWholeDigits = 21
        Me.txtEMIExceedPerc.Name = "txtEMIExceedPerc"
        Me.txtEMIExceedPerc.Prefix = ""
        Me.txtEMIExceedPerc.RangeMax = 1.7976931348623157E+308
        Me.txtEMIExceedPerc.RangeMin = -1.7976931348623157E+308
        Me.txtEMIExceedPerc.Size = New System.Drawing.Size(55, 21)
        Me.txtEMIExceedPerc.TabIndex = 7
        Me.txtEMIExceedPerc.Text = "0"
        Me.txtEMIExceedPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEMIExceedPerc
        '
        Me.lblEMIExceedPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIExceedPerc.Location = New System.Drawing.Point(11, 335)
        Me.lblEMIExceedPerc.Name = "lblEMIExceedPerc"
        Me.lblEMIExceedPerc.Size = New System.Drawing.Size(258, 16)
        Me.lblEMIExceedPerc.TabIndex = 343
        Me.lblEMIExceedPerc.Text = "Monthly Installment Amount should not exceed"
        Me.lblEMIExceedPerc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elDefaultInfo
        '
        Me.elDefaultInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elDefaultInfo.Location = New System.Drawing.Point(8, 211)
        Me.elDefaultInfo.Name = "elDefaultInfo"
        Me.elDefaultInfo.Size = New System.Drawing.Size(616, 18)
        Me.elDefaultInfo.TabIndex = 341
        Me.elDefaultInfo.Text = "Default Calculation Type && Interest Rate"
        '
        'pnlCalcType
        '
        Me.pnlCalcType.Controls.Add(Me.cboInterestCalcType)
        Me.pnlCalcType.Controls.Add(Me.txtLoanRate)
        Me.pnlCalcType.Controls.Add(Me.lblInterestCalcType)
        Me.pnlCalcType.Controls.Add(Me.lblLoanInterest)
        Me.pnlCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCalcType.Location = New System.Drawing.Point(3, 257)
        Me.pnlCalcType.Name = "pnlCalcType"
        Me.pnlCalcType.Size = New System.Drawing.Size(622, 25)
        Me.pnlCalcType.TabIndex = 339
        '
        'cboInterestCalcType
        '
        Me.cboInterestCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestCalcType.FormattingEnabled = True
        Me.cboInterestCalcType.Location = New System.Drawing.Point(159, 2)
        Me.cboInterestCalcType.Name = "cboInterestCalcType"
        Me.cboInterestCalcType.Size = New System.Drawing.Size(171, 21)
        Me.cboInterestCalcType.TabIndex = 0
        '
        'txtLoanRate
        '
        Me.txtLoanRate.AllowNegative = False
        Me.txtLoanRate.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanRate.DigitsInGroup = 0
        Me.txtLoanRate.Flags = 65536
        Me.txtLoanRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanRate.Location = New System.Drawing.Point(545, 2)
        Me.txtLoanRate.MaxDecimalPlaces = 6
        Me.txtLoanRate.MaxWholeDigits = 21
        Me.txtLoanRate.Name = "txtLoanRate"
        Me.txtLoanRate.Prefix = ""
        Me.txtLoanRate.RangeMax = 1.7976931348623157E+308
        Me.txtLoanRate.RangeMin = -1.7976931348623157E+308
        Me.txtLoanRate.Size = New System.Drawing.Size(77, 21)
        Me.txtLoanRate.TabIndex = 337
        Me.txtLoanRate.Text = "0"
        Me.txtLoanRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInterestCalcType
        '
        Me.lblInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestCalcType.Location = New System.Drawing.Point(5, 4)
        Me.lblInterestCalcType.Name = "lblInterestCalcType"
        Me.lblInterestCalcType.Size = New System.Drawing.Size(148, 16)
        Me.lblInterestCalcType.TabIndex = 0
        Me.lblInterestCalcType.Text = "Int. Calc. Type"
        Me.lblInterestCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanInterest
        '
        Me.lblLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterest.Location = New System.Drawing.Point(336, 5)
        Me.lblLoanInterest.Name = "lblLoanInterest"
        Me.lblLoanInterest.Size = New System.Drawing.Size(108, 15)
        Me.lblLoanInterest.TabIndex = 336
        Me.lblLoanInterest.Text = "Rate (%)"
        Me.lblLoanInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanCalcType
        '
        Me.cboLoanCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanCalcType.DropDownWidth = 250
        Me.cboLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanCalcType.FormattingEnabled = True
        Me.cboLoanCalcType.Location = New System.Drawing.Point(162, 233)
        Me.cboLoanCalcType.Name = "cboLoanCalcType"
        Me.cboLoanCalcType.Size = New System.Drawing.Size(171, 21)
        Me.cboLoanCalcType.TabIndex = 5
        '
        'lblLoanCalcType
        '
        Me.lblLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanCalcType.Location = New System.Drawing.Point(8, 235)
        Me.lblLoanCalcType.Name = "lblLoanCalcType"
        Me.lblLoanCalcType.Size = New System.Drawing.Size(148, 16)
        Me.lblLoanCalcType.TabIndex = 332
        Me.lblLoanCalcType.Text = "Loan Calc. Type"
        Me.lblLoanCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMaximumLoanAmount
        '
        Me.lblMaximumLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaximumLoanAmount.Location = New System.Drawing.Point(8, 90)
        Me.lblMaximumLoanAmount.Name = "lblMaximumLoanAmount"
        Me.lblMaximumLoanAmount.Size = New System.Drawing.Size(148, 16)
        Me.lblMaximumLoanAmount.TabIndex = 25
        Me.lblMaximumLoanAmount.Text = "Maximum Loan Amount"
        Me.lblMaximumLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMaxLonaAmount
        '
        Me.txtMaxLonaAmount.AllowNegative = True
        Me.txtMaxLonaAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxLonaAmount.DigitsInGroup = 0
        Me.txtMaxLonaAmount.Flags = 0
        Me.txtMaxLonaAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxLonaAmount.Location = New System.Drawing.Point(162, 88)
        Me.txtMaxLonaAmount.MaxDecimalPlaces = 6
        Me.txtMaxLonaAmount.MaxWholeDigits = 21
        Me.txtMaxLonaAmount.Name = "txtMaxLonaAmount"
        Me.txtMaxLonaAmount.Prefix = ""
        Me.txtMaxLonaAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMaxLonaAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMaxLonaAmount.Size = New System.Drawing.Size(171, 21)
        Me.txtMaxLonaAmount.TabIndex = 2
        Me.txtMaxLonaAmount.Text = "0"
        Me.txtMaxLonaAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkShowLoanBalOnPayslip
        '
        Me.chkShowLoanBalOnPayslip.AutoSize = True
        Me.chkShowLoanBalOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLoanBalOnPayslip.Location = New System.Drawing.Point(14, 414)
        Me.chkShowLoanBalOnPayslip.Name = "chkShowLoanBalOnPayslip"
        Me.chkShowLoanBalOnPayslip.Size = New System.Drawing.Size(171, 17)
        Me.chkShowLoanBalOnPayslip.TabIndex = 12
        Me.chkShowLoanBalOnPayslip.Text = "Show Loan Balance On Payslip"
        Me.chkShowLoanBalOnPayslip.UseVisualStyleBackColor = True
        '
        'chkShowonESS
        '
        Me.chkShowonESS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowonESS.Location = New System.Drawing.Point(339, 36)
        Me.chkShowonESS.Name = "chkShowonESS"
        Me.chkShowonESS.Size = New System.Drawing.Size(135, 17)
        Me.chkShowonESS.TabIndex = 10
        Me.chkShowonESS.Text = "Show on ESS"
        Me.chkShowonESS.UseVisualStyleBackColor = True
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 118)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(148, 16)
        Me.lblDescription.TabIndex = 4
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(162, 115)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(462, 63)
        Me.txtDescription.TabIndex = 3
        '
        'lnEligibility
        '
        Me.lnEligibility.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEligibility.Location = New System.Drawing.Point(11, 286)
        Me.lnEligibility.Name = "lnEligibility"
        Me.lnEligibility.Size = New System.Drawing.Size(613, 17)
        Me.lnEligibility.TabIndex = 7
        Me.lnEligibility.Text = "Loan Eligibility Info"
        '
        'txtMinSalary
        '
        Me.txtMinSalary.AllowNegative = True
        Me.txtMinSalary.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMinSalary.DigitsInGroup = 0
        Me.txtMinSalary.Flags = 0
        Me.txtMinSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinSalary.Location = New System.Drawing.Point(162, 306)
        Me.txtMinSalary.MaxDecimalPlaces = 6
        Me.txtMinSalary.MaxWholeDigits = 21
        Me.txtMinSalary.Name = "txtMinSalary"
        Me.txtMinSalary.Prefix = ""
        Me.txtMinSalary.RangeMax = 1.7976931348623157E+308
        Me.txtMinSalary.RangeMin = -1.7976931348623157E+308
        Me.txtMinSalary.Size = New System.Drawing.Size(171, 21)
        Me.txtMinSalary.TabIndex = 6
        Me.txtMinSalary.Text = "0"
        Me.txtMinSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMinSalary
        '
        Me.lblMinSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinSalary.Location = New System.Drawing.Point(11, 308)
        Me.lblMinSalary.Name = "lblMinSalary"
        Me.lblMinSalary.Size = New System.Drawing.Size(145, 16)
        Me.lblMinSalary.TabIndex = 8
        Me.lblMinSalary.Text = "Minimum Net Monthly Salary"
        Me.lblMinSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(162, 61)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(312, 21)
        Me.txtName.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 63)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(148, 16)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Loan Scheme Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(162, 34)
        Me.txtCode.MaxLength = 255
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(171, 21)
        Me.txtCode.TabIndex = 0
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 36)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(148, 16)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 439)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(636, 50)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(531, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(431, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmLoanScheme_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(636, 489)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanScheme_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Loan Scheme"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbLoanInfo.ResumeLayout(False)
        Me.gbLoanInfo.PerformLayout()
        Me.pnlCalcType.ResumeLayout(False)
        Me.pnlCalcType.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbLoanInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnEligibility As eZee.Common.eZeeLine
    Friend WithEvents txtMinSalary As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMinSalary As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents chkShowonESS As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowLoanBalOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents lblMaximumLoanAmount As System.Windows.Forms.Label
    Friend WithEvents txtMaxLonaAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents cboInterestCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterestCalcType As System.Windows.Forms.Label
    Friend WithEvents cboLoanCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanCalcType As System.Windows.Forms.Label
    Friend WithEvents txtLoanRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLoanInterest As System.Windows.Forms.Label
    Friend WithEvents pnlCalcType As System.Windows.Forms.Panel
    Friend WithEvents elDefaultInfo As eZee.Common.eZeeLine
    Friend WithEvents lblEMIExceedPerc As System.Windows.Forms.Label
    Friend WithEvents txtEMIExceedPerc As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIExceedPerc2 As System.Windows.Forms.Label
    Friend WithEvents txtMaxNoOfInstallment As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMaxNoOfInstallment As System.Windows.Forms.Label
    Friend WithEvents cboNetPay As System.Windows.Forms.ComboBox
    Friend WithEvents lblNetPay As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboMappedHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblMappedHead As System.Windows.Forms.Label
End Class
