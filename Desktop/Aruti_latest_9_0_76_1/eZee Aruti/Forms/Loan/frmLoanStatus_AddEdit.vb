﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.clsPayment_tran

Public Class frmLoanStatus_AddEdit

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmLoanStatus_AddEdit"
    Private mblnIsFromLoan As Boolean = False
    Private mintTranUnkid As Integer = -1
    Private mblnCancel As Boolean = True
    Private objLoan_Advance As clsLoan_Advance
    Private objEmployee As clsEmployee_Master
    Private objLoanScheme As clsLoan_Scheme
    Private objStatusTran As clsLoan_Status_tran
    Private mstrStatusData As String = String.Empty
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal blnIsLoan As Boolean) As Boolean
        mintTranUnkid = intUnkId

        mblnIsFromLoan = blnIsLoan

        Me.ShowDialog()

        intUnkId = mintTranUnkid

        Return Not mblnCancel
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtEmployeeName.BackColor = GUI.ColorComp
            txtLoanScheme.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional
            txtSettlementAmt.BackColor = GUI.ColorOptional
            cboStatus.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMaster As New clsMasterData
        Try
            If mblnIsFromLoan = True Then
                dsCombos = objMaster.GetLoan_Saving_Status("Status")
                If objLoan_Advance._Balance_Amount > 0 Then
                    dsCombos.Tables(0).Rows.RemoveAt(4)
                End If
            End If
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Status")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Info()
        Try
            txtEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " "
            txtLoanScheme.Text = objLoanScheme._Name
            cboStatus.SelectedValue = objLoan_Advance._LoanStatus
            txtBalance.Text = Format(objLoan_Advance._Balance_Amount, GUI.fmtCurrency) 'Sohail (11 May 2011)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objStatusTran._Isvoid = False
            objStatusTran._Loanadvancetranunkid = objLoan_Advance._Loanadvancetranunkid
            objStatusTran._Remark = txtRemarks.Text
            If txtSettlementAmt.Text.Trim <> "" Then
                objStatusTran._Settle_Amount = txtSettlementAmt.Decimal 'Sohail (11 May 2011)
            Else
                objStatusTran._Settle_Amount = 0
            End If
            objStatusTran._Staus_Date = dtpDate.Value
            objStatusTran._Voiddatetime = Nothing
            objStatusTran._Voiduserunkid = -1
            objStatusTran._Statusunkid = CInt(cboStatus.SelectedValue)

            'Please Add
            mstrStatusData = objStatusTran._Isvoid & "|" & _
                                       objStatusTran._Loanadvancetranunkid & "|" & _
                                       objStatusTran._Remark & "|" & _
                                       objStatusTran._Settle_Amount & "|" & _
                                       objStatusTran._Staus_Date & "|" & _
                                       objStatusTran._Voiddatetime & "|" & _
                                       objStatusTran._Voiduserunkid & "|" & _
                                       objStatusTran._Statusunkid & "|"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Status is compulsory information. Please select Status to continue."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            End If

            Select Case CInt(cboStatus.SelectedValue)
                Case 3, 4
                    If txtRemarks.Text.Trim = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is compulsory information."), enMsgBoxStyle.Information)
                        txtRemarks.Focus()
                        Return False
                    End If
                    lnkAddSettlement.Enabled = False
            End Select

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function


    'Private Sub GetValue()
    '    Try
    '        objStatusTran._Isvoid = objStatusTran._Isvoid
    '        objStatusTran._Loanadvancetranunkid = objStatusTran._Loanadvancetranunkid
    '        txtRemarks.Text = objStatusTran._Remark
    '        txtSettlementAmt.Text = CStr(objStatusTran._Settle_Amount)
    '        dtpDate.Value = objStatusTran._Staus_Date
    '        objStatusTran._Voiddatetime = objStatusTran._Voiddatetime
    '        objStatusTran._Voiduserunkid = objStatusTran._Voiduserunkid
    '        cboStatus.SelectedValue = objStatusTran._Statusunkid
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmLoanStatus_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoan_Advance = New clsLoan_Advance
        objEmployee = New clsEmployee_Master
        objLoanScheme = New clsLoan_Scheme
        objStatusTran = New clsLoan_Status_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call FillCombo()
            lnkAddSettlement.Enabled = False
            objLoan_Advance._Loanadvancetranunkid = mintTranUnkid

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = objLoan_Advance._Employeeunkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoan_Advance._Employeeunkid
            'S.SANDEEP [04 JUN 2015] -- END

            objLoanScheme._Loanschemeunkid = objLoan_Advance._Loanschemeunkid
            Call Fill_Info()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanStatus_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatus_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoan_Advance = Nothing
    End Sub

    Private Sub frmStatus_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmStatus_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub

            Select Case CInt(cboStatus.SelectedValue)
                Case 3, 4
                    Dim objPayment As New clsPayment_tran

                    'S.SANDEEP [ 29 May 2013 ] -- START
                    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
                    Dim ePRef As enPaymentRefId
                    If mblnIsFromLoan = True Then
                        ePRef = enPaymentRefId.LOAN
                    Else
                        ePRef = enPaymentRefId.ADVANCE
                    End If
                    'S.SANDEEP [ 29 May 2013 ] -- END

                    If objPayment.IsPaymentDone(enPayTypeId.PAYMENT, mintTranUnkid, ePRef) = False Then 'S.SANDEEP [ 29 May 2013 ] -- START -- END
                        'objPayment.IsPaymentDone(enPayTypeId.PAYMENT, mintTranUnkid) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot set the selected status for this transaction. Reason : Payment is not made to employee for this transaction."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    objPayment = Nothing

                    If txtSettlementAmt.Text.Trim = "" Then
                        If CInt(cboStatus.SelectedValue) = 3 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You are about to ") & CStr(cboStatus.Text) & _
                                               Language.getMessage(mstrModuleName, 3, "selected transaction. Do you want to settle it?"), _
                                               CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                lnkAddSettlement.Focus()
                                Exit Sub
                            End If
                        ElseIf CInt(cboStatus.SelectedValue) = 4 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot set the selected status for this transaction. Reason : Loan/Advance is not settled."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
            End Select

            Call SetValue()



            blnFlag = objStatusTran.Insert

            If blnFlag Then
                objLoan_Advance._LoanStatus = CInt(cboStatus.SelectedValue)
                objLoan_Advance.Update()
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
    'Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
    '    Try
    '        Select Case CInt(cboStatus.SelectedValue)
    '            Case 3, 4
    '                lnkAddSettlement.Enabled = True
    '            Case Else
    '                txtSettlementAmt.Text = ""
    '                lnkAddSettlement.Enabled = False
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
#End Region

    Private Sub lnkAddSettlement_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddSettlement.LinkClicked
        Dim frm As New frmPayment_AddEdit
        Dim mintPaymentTranId As Integer = -1
        Dim objPayment_Tran As New clsPayment_tran
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetValue()
            frm._Loan_Status_data = mstrStatusData
            frm._IsFrom_Status = True
            If frm.displayDialog(mintPaymentTranId, enAction.ADD_ONE, CInt(IIf(mblnIsFromLoan = True, enPaymentRefId.LOAN, enPaymentRefId.ADVANCE)), mintTranUnkid, enPayTypeId.RECEIVED) Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddSettlement_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub txtRemarks_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemarks.TextChanged
        Try
            If txtRemarks.Text.Trim = "" Then
                lnkAddSettlement.Enabled = False
            Else
                Select Case CInt(cboStatus.SelectedValue)
                    Case 3, 4
                        lnkAddSettlement.Enabled = True
                    Case 0, 1, 2
                        lnkAddSettlement.Enabled = False
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemarks_TextChanged", mstrModuleName)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			Me.gbLoanStatusInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoanStatusInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbLoanStatusInfo.Text = Language._Object.getCaption(Me.gbLoanStatusInfo.Name, Me.gbLoanStatusInfo.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.lblSettlementAmt.Text = Language._Object.getCaption(Me.lblSettlementAmt.Name, Me.lblSettlementAmt.Text)
			Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
			Me.lnkAddSettlement.Text = Language._Object.getCaption(Me.lnkAddSettlement.Name, Me.lnkAddSettlement.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot set the selected status for this transaction. Reason : Payment is not made to employee for this transaction.")
			Language.setMessage(mstrModuleName, 2, "You are about to")
			Language.setMessage(mstrModuleName, 3, "selected transaction. Do you want to settle it?")
			Language.setMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is compulsory information.")
			Language.setMessage(mstrModuleName, 5, "Status is compulsory information. Please select Status to continue.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot set the selected status for this transaction. Reason : Loan/Advance is not settled.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class