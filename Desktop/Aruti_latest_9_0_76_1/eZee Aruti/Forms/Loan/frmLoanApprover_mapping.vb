﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmLoanapprover_mapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLoanapprover_mapping"
    Private objLoanApprover As clsLoan_Approver
    Private objmapping As clsloan_approver_mapping

#End Region

#Region " Form's Events "

    Private Sub frmLoanapprover_mapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objmapping = New clsloan_approver_mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            lvmapApprover.GridLines = False
            FillCombo()
            ApproverFillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanapprover_mapping_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm._Other_ModuleNames = ""
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Private Method "
    
    Private Sub ApproverFillList()

        objLoanApprover = New clsLoan_Approver

        Dim lvItem As ListViewItem
        Dim dsList As New DataSet

        Try
            lvApprover.Items.Clear()
            dsList = objLoanApprover.GetList("ApproverList")

            For Each drRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Tag = drRow("approvertranunkid")

                'S.SANDEEP [ 08 AUG 2011 ] -- START
                'ISSUE : GENERAL CHANGES
                'If IsDBNull(drRow("approvername")) Then
                '    lvItem.SubItems.Add("")
                'Else
                '    lvItem.Text = drRow("approvername").ToString
                'End If
                lvItem.Text = ""
                If IsDBNull(drRow("approvername")) Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(drRow("approvername").ToString)
                End If
                'S.SANDEEP [ 08 AUG 2011 ] -- END 
                lvItem.SubItems.Add(drRow("approverunkid").ToString)
                lvApprover.Items.Add(lvItem)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ApproverFillList", mstrModuleName)
        Finally
            If dsList IsNot Nothing Then
                dsList.Dispose()
            End If
            dsList = Nothing
            lvItem = Nothing
        End Try
    End Sub

    Private Sub FillMappedApproverList(ByVal dsMappedApprover As DataSet)
        Try
            Dim lvItem As ListViewItem
            lvmapApprover.Items.Clear()

            For Each drRow As DataRow In dsMappedApprover.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Tag = CInt(drRow("userunkid"))
                lvItem.Text = ""
                lvItem.SubItems.Add(drRow("username").ToString)
                lvItem.SubItems.Add(drRow("Approver").ToString)
                lvItem.SubItems.Add(drRow("approverunkid").ToString)
                lvItem.SubItems.Add(drRow("approvertranunkid").ToString)
                lvmapApprover.Items.Add(lvItem)
            Next
            lvmapApprover.GroupingColumn = colhUser
            lvmapApprover.DisplayGroups(True)
            lvmapApprover.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMappedApproverList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objUser As New clsUserAddEdit

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsFill = objUser.getComboList("List", True)
            Dim objOption As New clsPassowdOptions
            If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                If objOption._IsEmployeeAsUser Then
                    'S.SHARMA [ 22 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'dsFill = objUser.getComboList("List", True, False, True)
                    'Else
                    'dsFill = objUser.getComboList("List", True, False)
                    dsFill = objUser.getComboList("List", True, False, True, Company._Object._Companyunkid, 345, FinancialYear._Object._YearUnkid)
                Else
                    dsFill = objUser.getComboList("List", True, False, , , 345)
                    'S.SHARMA [ 22 JAN 2013 ] -- END
                End If
            ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
                'S.SHARMA [ 22 JAN 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsFill = objUser.getComboList("List", True, True)
                dsFill = objUser.getComboList("List", True, True, , , 345)
                'S.SHARMA [ 22 JAN 2013 ] -- END
            ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                'S.SHARMA [ 22 JAN 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsFill = objUser.getComboList("List", True, True)
                dsFill = objUser.getComboList("List", True, True, , , 345)
                'S.SHARMA [ 22 JAN 2013 ] -- END
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            cboUser.DisplayMember = "name"
            cboUser.ValueMember = "userunkid"
            cboUser.DataSource = dsFill.Tables("List")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function CheckForItems(ByVal lvItem As ListViewItem) As Boolean
        Try
            If lvmapApprover.Items.Count > 0 Then

                For Each lvAppItem As ListViewItem In lvmapApprover.Items
                    If lvAppItem.SubItems(colhUser.Index).Text = cboUser.Text _
                     And lvAppItem.SubItems(colhMapApprover.Index).Text = lvItem.SubItems(colhFromEmployee.Index).Text Then
                        Return False
                    End If
                Next

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckForItem", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub ClearCheckedApprover()
        Try
            If lvApprover.Items.Count > 0 Then
                For i As Integer = 0 To lvApprover.CheckedItems.Count - 1
                    lvApprover.CheckedItems(0).Checked = False
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCheckedApprover", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboUser_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUser.SelectedValueChanged
        Try

            If CInt(cboUser.SelectedValue) <= 0 Then
                lvmapApprover.Items.Clear()
            End If

            If CInt(cboUser.SelectedValue) > 0 Then
                ClearCheckedApprover()
                Dim dsMappedApprover As DataSet = objmapping.GetList("List", CInt(cboUser.SelectedValue))
                If dsMappedApprover IsNot Nothing AndAlso dsMappedApprover.Tables(0).Rows.Count > 0 Then
                    FillMappedApproverList(dsMappedApprover)
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUser_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try

            If CInt(cboUser.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "User is compulsory information.Please Select User."), enMsgBoxStyle.Information)
                cboUser.Select()
                Exit Sub
            End If

            If CInt(cboUser.SelectedValue) > 0 Then

                For Each lvItem As ListViewItem In lvApprover.CheckedItems
                    If CheckForItems(lvItem) Then
                        Dim lvAppItem As New ListViewItem
                        lvAppItem.Tag = CInt(cboUser.SelectedValue)
                        lvAppItem.Text = ""
                        lvAppItem.SubItems.Add(cboUser.Text)
                        lvAppItem.SubItems.Add(lvItem.SubItems(colhFromEmployee.Index).Text)
                        lvAppItem.SubItems.Add(lvItem.SubItems(objcolhapproveunkid.Index).Text)
                        lvAppItem.SubItems.Add(lvItem.Tag.ToString)
                        lvAppItem.Checked = False
                        lvmapApprover.Items.Add(lvAppItem)
                    End If
                Next
                lvmapApprover.GroupingColumn = colhUser
                lvmapApprover.DisplayGroups(True)
                lvmapApprover.GridLines = False

                ClearCheckedApprover()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnUnassign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnassign.Click
        Try
            If lvmapApprover.Items.Count > 0 Then
                For Each lvItem As ListViewItem In lvmapApprover.CheckedItems
                    lvmapApprover.Items.Remove(lvItem)
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnassign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim blnflag As Boolean = False
            If lvmapApprover.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Approver Mapping is compulsory information.Please Map Loan Approver to User."), enMsgBoxStyle.Information)
                cboUser.Select()
                Exit Sub
            End If

            Dim mintUserunkid As Integer = -1


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes


            'objmapping.Delete(CInt(cboUser.SelectedValue))

            'For i = 0 To lvmapApprover.Items.Count - 1
            '    objmapping._Approvertranunkid = CInt(lvmapApprover.Items(i).SubItems(objcolhMapApprovertranunkid.Index).Text)
            '    objmapping._Userunkid = CInt(lvmapApprover.Items(i).Tag)

            '    If objmapping.isExist(objmapping._Approvertranunkid, objmapping._Userunkid) = False Then
            '        blnflag = objmapping.Insert()
            '    End If

            '    If blnflag = False And objmapping._Message <> "" Then
            '        eZeeMsgBox.Show(objmapping._Message, enMsgBoxStyle.Information)
            '    End If
            'Next

            Dim dctLoanApprover As New Dictionary(Of Integer, String)
            For i = 0 To lvmapApprover.Items.Count - 1
                objmapping._Approvertranunkid = CInt(lvmapApprover.Items(i).SubItems(objcolhMapApprovertranunkid.Index).Text)
                objmapping._Userunkid = CInt(lvmapApprover.Items(i).Tag)

                If dctLoanApprover.ContainsKey(objmapping._Userunkid) Then
                    dctLoanApprover(objmapping._Userunkid) &= "," & objmapping._Approvertranunkid
                Else
                    dctLoanApprover.Add(objmapping._Userunkid, objmapping._Approvertranunkid.ToString())
                End If
            Next

            blnflag = objmapping.Insert(dctLoanApprover)

            If blnflag And objmapping._Message = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Information successfully saved."), enMsgBoxStyle.Information)
                Me.Close()
            End If

            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboUser.ValueMember
                .CodeMember = cboUser.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboUser.SelectedValue = objFrm.SelectedValue
            End If
            cboUser.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

    'S.SANDEEP [ 08 AUG 2011 ] -- START
    'ISSUE : GENERAL CHANGES
#Region " Other Events "

    Private Sub objChkASelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkASelect.CheckedChanged
        Try
            RemoveHandler lvApprover.ItemChecked, AddressOf lvApprover_ItemChecked
            For Each LVI As ListViewItem In lvApprover.Items
                LVI.Checked = CBool(objChkASelect.CheckState)
            Next
            AddHandler lvApprover.ItemChecked, AddressOf lvApprover_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkASelect_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvApprover_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvApprover.ItemChecked
        Try
            RemoveHandler objChkASelect.CheckedChanged, AddressOf objChkASelect_CheckedChanged
            If lvApprover.CheckedItems.Count <= 0 Then
                objChkASelect.CheckState = CheckState.Unchecked
            ElseIf lvApprover.CheckedItems.Count < lvApprover.Items.Count Then
                objChkASelect.CheckState = CheckState.Indeterminate
            ElseIf lvApprover.CheckedItems.Count = lvApprover.Items.Count Then
                objChkASelect.CheckState = CheckState.Checked
            End If
            AddHandler objChkASelect.CheckedChanged, AddressOf objChkASelect_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvApprover_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 08 AUG 2011 ] -- END 
 
 
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.objbtnUnassign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnUnassign.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAssign.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.colhFromEmployee.Text = Language._Object.getCaption(CStr(Me.colhFromEmployee.Tag), Me.colhFromEmployee.Text)
			Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhUser.Text = Language._Object.getCaption(CStr(Me.colhUser.Tag), Me.colhUser.Text)
			Me.colhMapApprover.Text = Language._Object.getCaption(CStr(Me.colhMapApprover.Tag), Me.colhMapApprover.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "User is compulsory information.Please Select User.")
			Language.setMessage(mstrModuleName, 2, "Loan Approver Mapping is compulsory information.Please Map Loan Approver to User.")
			Language.setMessage(mstrModuleName, 3, "Information successfully saved.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class