﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmLoanSchemeList
    Private ReadOnly mstrModuleName As String = "frmLoanSchemeList"
    Private objLoanScheme As clsLoan_Scheme

#Region " Private Methods "

    Private Sub FillList()
        Dim strSearching As String = ""
        Dim dsLoanScheme As New DataSet
        Dim dtTable As DataTable
        Dim objExchangeRate As New clsExchangeRate

        Try

            If User._Object.Privilege._AllowToViewLoanSchemeList = True Then                'Pinkal (09-Jul-2012) -- Start

            objLoanScheme = New clsLoan_Scheme

            objExchangeRate._ExchangeRateunkid = 1
            dsLoanScheme = objLoanScheme.GetList("List")

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                strSearching &= "AND loanschemeunkid =" & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            'Sohail (10 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            If txtMinSalary.Decimal <> 0 Then
                'If Trim(txtMinSalary.Text).Length > 0 Then
                'Sohail (10 Feb 2012) -- End
                strSearching &= "AND minnetsalary =" & txtMinSalary.Text & " "
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtTable = New DataView(dsLoanScheme.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsLoanScheme.Tables(0)
            End If

            Dim lvItem As ListViewItem

            lvLoanScheme.Items.Clear()
            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("code").ToString
                lvItem.Tag = drRow("loanschemeunkid")
                lvItem.SubItems.Add(drRow("name").ToString)
                lvItem.SubItems.Add(drRow("description").ToString)

                'Anjan (11 May 2011)-Start
                'lvItem.SubItems.Add(Format(CDbl(drRow("minnetsalary")), objExchangeRate._fmtCurrency))
                lvItem.SubItems.Add(Format(CDec(drRow("minnetsalary")), objExchangeRate._fmtCurrency))
                'Anjan (11 May 2011)-End 


                lvLoanScheme.Items.Add(lvItem)
            Next

            If lvLoanScheme.Items.Count > 16 Then
                colhDescription.Width = 290 - 18
            Else
                colhDescription.Width = 290
            End If

            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsLoanScheme.Dispose()
            '--Vimal 28-Aug-2010 Start
            'objLoanScheme = Nothing
            '--Vimal 28-Aug-2010 End
            objExchangeRate = Nothing
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        objLoanScheme = New clsLoan_Scheme
        Try
            dsList = objLoanScheme.getComboList(True)
            cboLoanScheme.ValueMember = "loanschemeunkid"
            cboLoanScheme.DisplayMember = "name"
            cboLoanScheme.DataSource = dsList.Tables(0)
            cboLoanScheme.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddLoanScheme
            btnEdit.Enabled = User._Object.Privilege._EditLoanScheme
            btnDelete.Enabled = User._Object.Privilege._DeleteLoanScheme

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try


    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmLoanSchemeList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvLoanScheme.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmLoanSchemeList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmLoanSchemeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoanScheme = New clsLoan_Scheme
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()

            Call FillList()
            Call FillCombo()

            If lvLoanScheme.Items.Count > 0 Then lvLoanScheme.Items(0).Selected = True
            lvLoanScheme.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanSchemeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanSchemeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoanScheme = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Scheme.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Scheme"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmLoanScheme_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvLoanScheme.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditLoanScheme = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        If lvLoanScheme.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan Scheme from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvLoanScheme.Select()
            Exit Sub
        End If
        Dim frm As New frmLoanScheme_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLoanScheme.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvLoanScheme.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            lvLoanScheme.Items(intSelectedIndex).Selected = True
            lvLoanScheme.EnsureVisible(intSelectedIndex)
            lvLoanScheme.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvLoanScheme.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan Scheme from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvLoanScheme.Select()
            Exit Sub
        End If
        If objLoanScheme.isUsed(CInt(lvLoanScheme.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Loan Scheme. Reason: This Loan Scheme is in use."), enMsgBoxStyle.Information) '?2
            lvLoanScheme.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLoanScheme.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Loan Scheme?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objLoanScheme.Delete(CInt(lvLoanScheme.SelectedItems(0).Tag))
                lvLoanScheme.SelectedItems(0).Remove()

                If lvLoanScheme.Items.Count <= 0 Then
                    Exit Try
                End If

                'If lvLoanScheme.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvLoanScheme.Items.Count - 1
                '    lvLoanScheme.Items(intSelectedIndex).Selected = True
                '    lvLoanScheme.EnsureVisible(intSelectedIndex)
                'ElseIf lvLoanScheme.Items.Count <> 0 Then
                '    lvLoanScheme.Items(intSelectedIndex).Selected = True
                '    lvLoanScheme.EnsureVisible(intSelectedIndex)
                'End If
            End If
            lvLoanScheme.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboLoanScheme.SelectedValue = 0
            txtMinSalary.Text = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLoanScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLoanScheme.Click
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet
        Try
            objLoanScheme = New clsLoan_Scheme
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            dsList = objLoanScheme.getComboList(False, "List")
            With cboLoanScheme
                objfrm.DataSource = dsList.Tables("List")
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "Code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLoanScheme_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objLoanScheme = Nothing
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhLoanName.Text = Language._Object.getCaption(CStr(Me.colhLoanName.Tag), Me.colhLoanName.Text)
			Me.colhMinSalary.Text = Language._Object.getCaption(CStr(Me.colhMinSalary.Tag), Me.colhMinSalary.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.lblMinSalary.Text = Language._Object.getCaption(Me.lblMinSalary.Name, Me.lblMinSalary.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Loan Scheme from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Loan Scheme. Reason: This Loan Scheme is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Loan Scheme?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class