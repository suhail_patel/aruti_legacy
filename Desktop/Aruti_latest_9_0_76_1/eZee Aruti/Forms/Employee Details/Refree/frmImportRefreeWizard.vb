﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportRefreeWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportRefreeWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    'S.SANDEEP [ 08 NOV 2013 ] -- START
    Dim dvGriddata As DataView = Nothing
    'S.SANDEEP [ 08 NOV 2013 ] -- END

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    'S.SANDEEP [ 12 DEC 2013 ] -- START

    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim iExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'S.SANDEEP [ 11 MAR 2014 ] -- END

    'S.SANDEEP [ 12 DEC 2013 ] -- END

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objARefreeTran As clsemployee_refree_approval_tran
    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim RefreeApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployeeRefereeList)))
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [17-April-2019] -- End
#End Region

#Region " Form's Events "

    Private Sub frmImportRefreeWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objApprovalData = New clsEmployeeDataApproval
            'Gajanan [17-April-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportRefreeWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEmp_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEmp.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportEmp_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEmp.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                Select Case CType(ctrl, ComboBox).Name.ToUpper
                                    'S.SANDEEP [19-JUL-2018] -- START
                                    Case cboFirstname.Name.ToUpper, cboSurname.Name.ToUpper, cboAddress.Name.ToUpper, cboCity.Name.ToUpper, cboCountry.Name.ToUpper, _
                                         cboGender.Name.ToUpper, cboRelation.Name.ToUpper, cboState.Name.ToUpper, cboTelephone.Name.ToUpper, cboCompany.Name.ToUpper, _
                                         cboMobile.Name.ToUpper, cboPosition.Name.ToUpper, cboEmail.Name.ToUpper
                                        'Case "CBOFIRSTNAME", "CBOLASTNAME", "CBOADDRESS", "CBOCITY", "CBOCOUNTRY", "CBOGENDER", "CBORELATION", "CBOSTATE", _
                                        '     "CBOTELEPHONE", "CBOCOMPANY", "CBOMOBILE", "CBOPOSITION", "CBOEMAIL" 'S.SANDEEP [ 12 DEC 2013 ] -- START -- END
                                        'S.SANDEEP [19-JUL-2018] -- END
                                        Continue For
                                    Case Else
                                        If CType(ctrl, ComboBox).Text = "" Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please the select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                            e.Cancel = True
                                            Exit For
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboAddress.Items.Add(dtColumns.ColumnName)
                cboCity.Items.Add(dtColumns.ColumnName)
                cboCountry.Items.Add(dtColumns.ColumnName)
                cboECode.Items.Add(dtColumns.ColumnName)
                cboFirstname.Items.Add(dtColumns.ColumnName)
                cboGender.Items.Add(dtColumns.ColumnName)
                cboSurname.Items.Add(dtColumns.ColumnName)
                cboRefree_Name.Items.Add(dtColumns.ColumnName)
                cboRelation.Items.Add(dtColumns.ColumnName)
                cboState.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP [ 08 NOV 2013 ] -- START
                cboCompany.Items.Add(dtColumns.ColumnName)
                cboMobile.Items.Add(dtColumns.ColumnName)
                cboPosition.Items.Add(dtColumns.ColumnName)
                cboTelephone.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP [ 08 NOV 2013 ] -- END
                'S.SANDEEP [ 12 DEC 2013 ] -- START
                cboEmail.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP [ 12 DEC 2013 ] -- END


                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known


                If dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 2, "ECODE") Then
                    dtColumns.Caption = "ECODE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 3, "FIRSTNAME") Then
                    dtColumns.Caption = "FIRSTNAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 4, "SURNAME") Then
                    dtColumns.Caption = "SURNAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 5, "REFREE_NAME") Then
                    dtColumns.Caption = "REFREE_NAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 6, "RELATION") Then
                    dtColumns.Caption = "RELATION"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 7, "ADDRESS") Then
                    dtColumns.Caption = "ADDRESS"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 8, "COUNTRY") Then
                    dtColumns.Caption = "COUNTRY"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 9, "STATE") Then
                    dtColumns.Caption = "STATE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 10, "CITY") Then
                    dtColumns.Caption = "CITY"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 11, "EMAIL") Then
                    dtColumns.Caption = "EMAIL"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 12, "GENDER") Then
                    dtColumns.Caption = "GENDER"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 13, "COMPANY") Then
                    dtColumns.Caption = "COMPANY"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 14, "TELEPHONE") Then
                    dtColumns.Caption = "TELEPHONE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 15, "MOBILE_NO") Then
                    dtColumns.Caption = "MOBILE_NO"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 16, "POSITION") Then
                    dtColumns.Caption = "POSITION"
                End If
                'Gajanan (24 Nov 2018) -- End

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ECode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Firstname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Surname", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("RefreeName", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Relation", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Address", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Country", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("State", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("City", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Gender", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("RefreeName", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "RefreeName")
            mdt_ImportData_Others.Columns.Add("Relation", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Relation")
            mdt_ImportData_Others.Columns.Add("Address", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Address")
            mdt_ImportData_Others.Columns.Add("Country", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Country")
            mdt_ImportData_Others.Columns.Add("State", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "State")
            mdt_ImportData_Others.Columns.Add("City", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "City")
            mdt_ImportData_Others.Columns.Add("Gender", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Gender")



            'S.SANDEEP [ 08 NOV 2013 ] -- START

            'Gajanan [27-May-2019] -- Start              
            'mdt_ImportData_Others.Columns.Add("company", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("mobile", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("position", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("telephone", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("company", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Company")
            mdt_ImportData_Others.Columns.Add("mobile", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "mobile")
            mdt_ImportData_Others.Columns.Add("position", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "position")
            mdt_ImportData_Others.Columns.Add("telephone", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "telephone")
            'Gajanan [27-May-2019] -- End

            'S.SANDEEP [ 08 NOV 2013 ] -- END

            'S.SANDEEP [ 12 DEC 2013 ] -- START
            mdt_ImportData_Others.Columns.Add("email", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "email")
            'S.SANDEEP [ 12 DEC 2013 ] -- END


            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            'S.SANDEEP [ 08 NOV 2013 ] -- START
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))
            'S.SANDEEP [ 08 NOV 2013 ] -- END

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData_Others.Columns.Add("EmployeeId", System.Type.GetType("System.String"))
            'Gajanan [27-May-2019] -- End

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboECode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ECode") = dtRow.Item(cboECode.Text).ToString.Trim

                If cboFirstname.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Firstname") = dtRow.Item(cboFirstname.Text).ToString.Trim
                Else
                    drNewRow.Item("Firstname") = ""
                End If

                If cboSurname.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Surname") = dtRow.Item(cboSurname.Text).ToString.Trim
                Else
                    drNewRow.Item("Surname") = ""
                End If

                drNewRow.Item("RefreeName") = dtRow.Item(cboRefree_Name.Text).ToString.Trim

                If cboRelation.Text.Trim.Length > 0 Then
                    drNewRow.Item("Relation") = dtRow.Item(cboRelation.Text).ToString.Trim
                Else
                    drNewRow.Item("Relation") = ""
                End If

                If cboAddress.Text.Trim.Length > 0 Then
                    drNewRow.Item("Address") = dtRow.Item(cboAddress.Text).ToString.Trim
                Else
                    drNewRow.Item("Address") = ""
                End If

                If cboCountry.Text.Trim.Length > 0 Then
                    drNewRow.Item("Country") = dtRow.Item(cboCountry.Text).ToString.Trim
                Else
                    drNewRow.Item("Country") = ""
                End If

                If cboState.Text.Trim.Length > 0 Then
                    drNewRow.Item("State") = dtRow.Item(cboState.Text).ToString.Trim
                Else
                    drNewRow.Item("State") = ""
                End If

                If cboCity.Text.Trim.Length > 0 Then
                    drNewRow.Item("City") = dtRow.Item(cboCity.Text).ToString.Trim
                Else
                    drNewRow.Item("City") = ""
                End If

                If cboGender.Text.Trim.Length > 0 Then
                    drNewRow.Item("Gender") = dtRow.Item(cboGender.Text).ToString.Trim
                Else
                    drNewRow.Item("Gender") = ""
                End If

                'S.SANDEEP [ 08 NOV 2013 ] -- START
                If cboCompany.Text.Trim.Length > 0 Then
                    drNewRow.Item("company") = dtRow.Item(cboCompany.Text).ToString.Trim
                Else
                    drNewRow.Item("company") = ""
                End If

                If cboMobile.Text.Trim.Length > 0 Then
                    drNewRow.Item("mobile") = dtRow.Item(cboMobile.Text).ToString.Trim
                Else
                    drNewRow.Item("mobile") = ""
                End If

                If cboPosition.Text.Trim.Length > 0 Then
                    drNewRow.Item("position") = dtRow.Item(cboPosition.Text).ToString.Trim
                Else
                    drNewRow.Item("position") = ""
                End If

                If cboTelephone.Text.Trim.Length > 0 Then
                    drNewRow.Item("telephone") = dtRow.Item(cboTelephone.Text).ToString.Trim
                Else
                    drNewRow.Item("telephone") = ""
                End If
                'S.SANDEEP [ 08 NOV 2013 ] -- END

                'S.SANDEEP [ 12 DEC 2013 ] -- START
                If cboEmail.Text.Trim.Length > 0 Then
                    drNewRow.Item("email") = dtRow.Item(cboEmail.Text).ToString.Trim
                Else
                    drNewRow.Item("email") = ""
                End If
                'S.SANDEEP [ 12 DEC 2013 ] -- END

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                'S.SANDEEP [ 08 NOV 2013 ] -- START
                drNewRow.Item("objStatus") = ""
                'S.SANDEEP [ 08 NOV 2013 ] -- END



                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)

            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ECode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                'S.SANDEEP [ 08 NOV 2013 ] -- START
                objcolhstatus.DataPropertyName = "objStatus"
                'S.SANDEEP [ 08 NOV 2013 ] -- END
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            'S.SANDEEP [ 08 NOV 2013 ] -- START
            ezWait.Active = False
            'S.SANDEEP [ 08 NOV 2013 ] -- END
            eZeeWizImportEmp.BackEnabled = False
            eZeeWizImportEmp.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboECode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Referee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboRefree_Name.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Reference Name cannot be blank. Please set Reference Name in order to import Employee Reference(s)."), enMsgBoxStyle.Information)
                    Return False
                End If


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

               
                'S.SANDEEP [13-AUG-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002445|ARUTI-283}
                'If .Item(cboGender.Text).ToString.Trim.Length > 0 Then
                '    Select Case .Item(cboGender.Text).ToString.ToUpper
                '        Case "MALE", "FEMALE", "M", "F"
                '        Case Else
                '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Gender data in file is not vaild."), enMsgBoxStyle.Information)
                '            Return False
                '    End Select
                'End If
                If cboGender.Text.Trim.Length > 0 Then
                If .Item(cboGender.Text).ToString.Trim.Length > 0 Then
                    Select Case .Item(cboGender.Text).ToString.ToUpper
                        Case "MALE", "FEMALE", "M", "F"
                        Case Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Gender data in file is not vaild."), enMsgBoxStyle.Information)
                            Return False
                    End Select
                End If
                End If
                'S.SANDEEP [13-AUG-2018] -- END
                'Gajanan [17-DEC-2018] -- End

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            'S.SANDEEP [ 08 NOV 2013 ] -- START
            btnFilter.Enabled = False
            'S.SANDEEP [ 08 NOV 2013 ] -- END

            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objCMaster As New clsCommon_Master
            Dim objEMaster As New clsEmployee_Master
            Dim objMaster As New clsMasterData
            Dim objSTMaster As New clsstate_master
            Dim objCTMaster As New clscity_master

            Dim objERTran As clsEmployee_Refree_tran

            Dim intRelationId As Integer = -1
            Dim intEmployeeUnkid As Integer = -1


            'Gajanan [21-June-2019] -- Start      

            'Dim intCountryId As Integer = -1
            'Dim intStateId As Integer = -1
            'Dim intCityId As Integer = -1

            Dim intCountryId As Integer = 0
            Dim intStateId As Integer = 0
            Dim intCityId As Integer = 0
            'Gajanan [21-June-2019] -- End

            Dim intRefereeTranId As Integer = -1

            'Gajanan [27-May-2019] -- Start
            Dim EmpList As New List(Of String)
            'Gajanan [27-May-2019] -- End

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim isEmployeeApprove As Boolean = False
                'Gajanan [22-Feb-2019] -- End


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                isEmployeeApprove = objEMaster.IsEmployeeApproved(dtRow.Item("ECode").ToString.Trim)
                'Gajanan [17-April-2019] -- End


                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ECode").ToString.Trim.Length > 0 Then
                    intEmployeeUnkid = objEMaster.GetEmployeeUnkid("", dtRow.Item("ECode").ToString.Trim)
                    If intEmployeeUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                        'S.SANDEEP [ 08 NOV 2013 ] -- START
                        dtRow.Item("objStatus") = 2
                        'S.SANDEEP [ 08 NOV 2013 ] -- END
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                        'Gajanan [27-May-2019] -- Start
                    Else
                        dtRow.Item("EmployeeId") = intEmployeeUnkid
                        'Gajanan [27-May-2019] -- End
                    End If
                End If

                '------------------------------ CHECKING RELATION PRESENT
                If dtRow.Item("Relation").ToString.Trim.Length > 0 Then


                    'Gajanan [21-June-2019] -- Start      
                    'intRelationId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, dtRow.Item("Relation").ToString.Trim)
                    intRelationId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RELATIONS, dtRow.Item("Relation").ToString.Trim)
                    'Gajanan [21-June-2019] -- End


                    If intRelationId <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master

                        objCMaster._Code = dtRow.Item("Relation").ToString
                        objCMaster._Name = dtRow.Item("Relation").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.SKILL_CATEGORY

                        If objCMaster.Insert() Then
                            intRelationId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                            'S.SANDEEP [ 08 NOV 2013 ] -- START
                            dtRow.Item("objStatus") = 2
                            'S.SANDEEP [ 08 NOV 2013 ] -- END
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                'S.SANDEEP [ 04 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                ''---------------------> COUNTRY
                'If dtRow.Item("country").ToString.Trim.Length > 0 Then
                '    intCountryId = objMaster.GetCountryUnkId(dtRow.Item("country").ToString.Trim)
                '    If intCountryId <= 0 Then
                '        If Not objMaster Is Nothing Then objMaster = Nothing
                '        objMaster = New clsMasterData
                '        intCountryId = objMaster.InsertCountry(dtRow.Item("country").ToString.Trim)
                '    End If
                'End If

                ''---------------------> STATE
                'If dtRow.Item("state").ToString.Trim.Length > 0 Then
                '    intStateId = objSTMaster.GetStateUnkId(dtRow.Item("state").ToString.Trim)
                '    If intStateId <= 0 Then
                '        If Not objSTMaster Is Nothing Then objSTMaster = Nothing
                '        objSTMaster = New clsstate_master
                '        objSTMaster._Code = dtRow.Item("state").ToString.Trim
                '        objSTMaster._Countryunkid = intCountryId
                '        objSTMaster._Isactive = True
                '        objSTMaster._Name = dtRow.Item("state").ToString.Trim
                '        If objSTMaster.Insert Then
                '            intStateId = objSTMaster._Stateunkid
                '        Else
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objSTMaster._Code & "/" & objSTMaster._Name & ":" & objSTMaster._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '            intStateId = -1
                '        End If
                '    End If
                'End If

                ''---------------------> CITY
                'If dtRow.Item("city").ToString.Trim.Length > 0 Then
                '    intCityId = objCTMaster.GetCityUnkId(dtRow.Item("city").ToString.Trim)
                '    If intCityId <= 0 Then
                '        If Not objCTMaster Is Nothing Then objCTMaster = Nothing
                '        objCTMaster = New clscity_master
                '        objCTMaster._Code = dtRow.Item("city").ToString.Trim
                '        objCTMaster._Countryunkid = intCountryId
                '        objCTMaster._Isactive = True
                '        objCTMaster._Name = dtRow.Item("city").ToString.Trim
                '        objCTMaster._Stateunkid = intStateId
                '        If objCTMaster.Insert Then
                '            intCityId = objCTMaster._Cityunkid
                '        Else
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objCTMaster._Code & "/" & objCTMaster._Name & ":" & objCTMaster._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '            intCityId = -1
                '        End If
                '    End If
                'End If
                'S.SANDEEP [ 04 APRIL 2012 ] -- END


                'S.SANDEEP [ 12 DEC 2013 ] -- START
                '------------------------------------ CHECKING VALID EMAIL
                If dtRow.Item("email").ToString.Trim.Length > 0 Then
                    'S.SANDEEP [ 11 MAR 2014 ] -- START
                    'Dim iEmailExp As New System.Text.RegularExpressions.Regex(iExpression)
                    Dim iEmailExp As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
                    'S.SANDEEP [ 11 MAR 2014 ] -- END
                    If iEmailExp.IsMatch(dtRow.Item("email").ToString.Trim) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Email.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                End If
                'S.SANDEEP [ 12 DEC 2013 ] -- END

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If RefreeApprovalFlowVal Is Nothing Then
                If RefreeApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [17-April-2019] -- End

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If objApprovalData.IsApproverPresent(enScreenName.frmEmployeeRefereeList, FinancialYear._Object._DatabaseName, _
                                                          ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                          FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                          User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, intEmployeeUnkid, Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then


                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = objApprovalData._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")

                        'Gajanan [27-May-2019] -- Start              


                        'dtRow.Item("objStatus") = 2
                        'Gajanan [27-May-2019] -- End

                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For
                    End If
                    'Gajanan [17-April-2019] -- End


                    Dim intOperationType As Integer = 0 : Dim strMsg As String = ""
                    objARefreeTran = New clsemployee_refree_approval_tran
                    If objARefreeTran.isExist(intEmployeeUnkid, dtRow.Item("RefreeName").ToString.Trim, Nothing, -1, "", Nothing, False, intOperationType) = True Then
                        If intOperationType > 0 Then
                            Select Case intOperationType
                                Case clsEmployeeDataApproval.enOperationType.EDITED
                                    strMsg = Language.getMessage(mstrModuleName, 5, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                                Case clsEmployeeDataApproval.enOperationType.DELETED
                                    strMsg = Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                                Case clsEmployeeDataApproval.enOperationType.ADDED
                                    strMsg = Language.getMessage(mstrModuleName, 9, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                            End Select
                        End If

                        'gajanan |30-MAR-2019| -- START

                    
                        dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = strMsg
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                    dtRow.Item("objStatus") = 2
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Continue For
                        'gajanan |30-MAR-2019| -- END

                    End If
                    'gajanan |30-MAR-2019| -- START

                    'dtRow.Item("image") = imgError
                    'dtRow.Item("message") = strMsg
                    'dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                    'dtRow.Item("objStatus") = 2
                    'objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    'Continue For
                    'gajanan |30-MAR-2019| -- END


                End If
                'Gajanan [17-DEC-2018] -- End


                objERTran = New clsEmployee_Refree_tran

                intRefereeTranId = objERTran.GetRefereeTranId(intEmployeeUnkid, dtRow.Item("RefreeName").ToString.Trim)

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                If intRefereeTranId > 0 Then
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "Employee Reference Already Exist.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                    'S.SANDEEP [ 08 NOV 2013 ] -- START
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    'S.SANDEEP [ 08 NOV 2013 ] -- END
                Else
                    '    objERTran._Address = dtRow.Item("Address").ToString.Trim
                    '    objERTran._Cityunkid = intCityId
                    '    objERTran._Countryunkid = intCountryId
                    '    objERTran._Employeeunkid = intEmployeeUnkid
                    '    'S.SANDEEP [13-AUG-2018] -- START
                    '    'ISSUE/ENHANCEMENT : {#0002445|ARUTI-283}
                    '    'Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                    '    '    Case "MALE"
                    '    '        objERTran._Gender = 1
                    '    '    Case "FEMALE"
                    '    '        objERTran._Gender = 2
                    '    '    Case Else
                    '    '        objERTran._Gender = 0
                    '    'End Select
                    '    Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                    '        Case "MALE", "M"
                    '            objERTran._Gender = 1
                    '        Case "FEMALE", "F"
                    '            objERTran._Gender = 2
                    '        Case Else
                    '            objERTran._Gender = 0
                    '    End Select
                    '    'S.SANDEEP [13-AUG-2018] -- END
                    '    objERTran._Relationunkid = intRelationId
                    '    objERTran._Name = dtRow.Item("RefreeName").ToString.Trim
                    '    objERTran._Stateunkid = intStateId
                    '    'S.SANDEEP [ 08 NOV 2013 ] -- START
                    '    objERTran._Company = dtRow.Item("company").ToString.Trim
                    '    objERTran._Mobile_No = dtRow.Item("mobile").ToString.Trim
                    '    objERTran._Ref_Position = dtRow.Item("position").ToString.Trim
                    '    objERTran._Telephone_No = dtRow.Item("telephone").ToString.Trim
                    '    'S.SANDEEP [ 08 NOV 2013 ] -- END

                    '    'S.SANDEEP [ 12 DEC 2013 ] -- START
                    '    objERTran._Email = dtRow.Item("email").ToString.Trim
                    '    'S.SANDEEP [ 12 DEC 2013 ] -- END

                    '    If objERTran.Insert Then
                    '        dtRow.Item("image") = imgAccept
                    '        dtRow.Item("message") = ""
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success")
                    '        'S.SANDEEP [ 08 NOV 2013 ] -- START
                    '        dtRow.Item("objStatus") = 1
                    '        'S.SANDEEP [ 08 NOV 2013 ] -- END
                    '        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    '    Else
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = objERTran._Message
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                    '        'S.SANDEEP [ 08 NOV 2013 ] -- START
                    '        dtRow.Item("objStatus") = 2
                    '        'S.SANDEEP [ 08 NOV 2013 ] -- END
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '    End If
                    'End If

                    ''If intRefereeTranId > 0 Then
                    ''    objERTran._Refereetranunkid = intRefereeTranId
                    ''End If

                    ''objERTran._Address = dtRow.Item("Address").ToString.Trim
                    ''objERTran._Cityunkid = intCityId
                    ''objERTran._Countryunkid = intCountryId
                    ''objERTran._Employeeunkid = intEmployeeUnkid
                    ''Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                    ''    Case "MALE"
                    ''        objERTran._Gender = 1
                    ''    Case "FEMALE"
                    ''        objERTran._Gender = 2
                    ''    Case Else
                    ''        objERTran._Gender = 0
                    ''End Select
                    ''objERTran._Relationunkid = intRelationId
                    ''objERTran._Name = dtRow.Item("RefreeName").ToString.Trim
                    ''objERTran._Stateunkid = intStateId
                    ''objERTran._Company = dtRow.Item("company").ToString.Trim
                    ''objERTran._Mobile_No = dtRow.Item("mobile").ToString.Trim
                    ''objERTran._Ref_Position = dtRow.Item("position").ToString.Trim
                    ''objERTran._Telephone_No = dtRow.Item("telephone").ToString.Trim
                    ''objERTran._Email = dtRow.Item("email").ToString.Trim

                    ''If intRefereeTranId > 0 Then
                    ''    If objERTran.Update() Then
                    ''        dtRow.Item("image") = imgAccept
                    ''        dtRow.Item("message") = Language.getMessage(mstrModuleName, 100, "Updated Successfully.")
                    ''        dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success")
                    ''        dtRow.Item("objStatus") = 1
                    ''        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    ''    Else
                    ''        dtRow.Item("image") = imgError
                    ''        dtRow.Item("message") = objERTran._Message
                    ''        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                    ''        dtRow.Item("objStatus") = 2
                    ''        objError.Text = CStr(Val(objError.Text) + 1)
                    ''    End If
                    ''Else
                    ''    If objERTran.Insert() Then
                    ''        dtRow.Item("image") = imgAccept
                    ''        dtRow.Item("message") = ""
                    ''        dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success")
                    ''        dtRow.Item("objStatus") = 1
                    ''        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    ''    Else
                    ''        dtRow.Item("image") = imgError
                    ''        dtRow.Item("message") = objERTran._Message
                    ''        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                    ''        dtRow.Item("objStatus") = 2
                    ''        objError.Text = CStr(Val(objError.Text) + 1)
                    ''    End If
                    ''End If
                    ''S.SANDEEP [14-JUN-2018] -- END

                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    'If RefreeApprovalFlowVal Is Nothinge Then
                    If RefreeApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                        'Gajanan [22-Feb-2019] -- End
                        objARefreeTran = New clsemployee_refree_approval_tran
                        objARefreeTran._Audittype = enAuditType.ADD
                        objARefreeTran._Audituserunkid = User._Object._Userunkid

                        objARefreeTran._Address = dtRow.Item("Address").ToString.Trim
                        objARefreeTran._Cityunkid = intCityId
                        objARefreeTran._Countryunkid = intCountryId
                        objARefreeTran._Employeeunkid = intEmployeeUnkid
                        Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                            Case "MALE", "M"
                                objARefreeTran._Gender = 1
                            Case "FEMALE", "F"
                                objARefreeTran._Gender = 2
                            Case Else
                                objARefreeTran._Gender = 0
                        End Select
                        objARefreeTran._Relationunkid = intRelationId
                        objARefreeTran._Name = dtRow.Item("RefreeName").ToString.Trim
                        objARefreeTran._Company = dtRow.Item("company").ToString.Trim
                        objARefreeTran._Mobile_No = dtRow.Item("mobile").ToString.Trim
                        objARefreeTran._Ref_Position = dtRow.Item("position").ToString.Trim
                        objARefreeTran._Telephone_No = dtRow.Item("telephone").ToString.Trim
                        objARefreeTran._Email = dtRow.Item("email").ToString.Trim


                        objARefreeTran._Isvoid = False
                        objARefreeTran._Tranguid = Guid.NewGuid.ToString()
                        objARefreeTran._Transactiondate = Now
                        objARefreeTran._Approvalremark = ""
                        objARefreeTran._Isweb = False
                        objARefreeTran._Isfinal = False
                        objARefreeTran._Ip = getIP()
                        objARefreeTran._Host = getHostName()
                        objARefreeTran._Form_Name = mstrModuleName
                        objARefreeTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objARefreeTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                        objARefreeTran._Refereetranunkid = -1
                    Else
                    objERTran._Address = dtRow.Item("Address").ToString.Trim
                    objERTran._Cityunkid = intCityId
                    objERTran._Countryunkid = intCountryId
                    objERTran._Employeeunkid = intEmployeeUnkid
                    'S.SANDEEP [13-AUG-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002445|ARUTI-283}
                    'Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                    '    Case "MALE"
                    '        objERTran._Gender = 1
                    '    Case "FEMALE"
                    '        objERTran._Gender = 2
                    '    Case Else
                    '        objERTran._Gender = 0
                    'End Select
                    Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                        Case "MALE", "M"
                            objERTran._Gender = 1
                        Case "FEMALE", "F"
                            objERTran._Gender = 2
                        Case Else
                            objERTran._Gender = 0
                    End Select
                    'S.SANDEEP [13-AUG-2018] -- END
                    objERTran._Relationunkid = intRelationId
                    objERTran._Name = dtRow.Item("RefreeName").ToString.Trim
                    objERTran._Stateunkid = intStateId
                    'S.SANDEEP [ 08 NOV 2013 ] -- START
                    objERTran._Company = dtRow.Item("company").ToString.Trim
                    objERTran._Mobile_No = dtRow.Item("mobile").ToString.Trim
                    objERTran._Ref_Position = dtRow.Item("position").ToString.Trim
                    objERTran._Telephone_No = dtRow.Item("telephone").ToString.Trim
                    'S.SANDEEP [ 08 NOV 2013 ] -- END

                    'S.SANDEEP [ 12 DEC 2013 ] -- START
                    objERTran._Email = dtRow.Item("email").ToString.Trim
                    'S.SANDEEP [ 12 DEC 2013 ] -- END



                 End If
                    Dim blnFlag As Boolean = False
                    Dim strMsg As String = String.Empty


                    If RefreeApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                        blnFlag = objARefreeTran.Insert(Company._Object._Companyunkid)
                        If blnFlag = False AndAlso objARefreeTran._Message <> "" Then
                            'dtRow.Item("message") = objARefreeTran._Message
                            strMsg = objARefreeTran._Message
                        Else
                            'Gajanan [27-May-2019] -- Start
                            If EmpList.Contains(intEmployeeUnkid) = False Then
                                EmpList.Add(intEmployeeUnkid)
                            End If
                            'objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                            '     ConfigParameter._Object._UserAccessModeSetting, _
                            '     Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                            '     CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                            '     enScreenName.frmEmployeeRefereeList, ConfigParameter._Object._EmployeeAsOnDate, _
                            '     User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                            '     User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , intEmployeeUnkid.ToString(), , , _
                            '     "employeeunkid= " & intEmployeeUnkid & " and name = '" & dtRow.Item("RefreeName").ToString.Trim & "' ", Nothing, False, , _
                            '     "employeeunkid= " & intEmployeeUnkid & " and name = '" & dtRow.Item("RefreeName").ToString.Trim & "' ", Nothing)
                            'Gajanan [27-May-2019] -- End
                        End If


                    Else
                        blnFlag = objERTran.Insert()
                        If blnFlag = False AndAlso objERTran._Message <> "" Then strMsg = objERTran._Message 'dtRow.Item("message") = objERTran._Message
                    End If

                    If blnFlag Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success")
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError

                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.

                        dtRow.Item("message") = strMsg
                        'gajanan |30-MAR-2019| -- START
                        'dtRow.Item("message") = objERTran._Message
                        'gajanan |30-MAR-2019| -- END

                        'Gajanan [17-April-2019] -- End


                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                End If

                'If intRefereeTranId > 0 Then
                '    objERTran._Refereetranunkid = intRefereeTranId
                'End If

                'objERTran._Address = dtRow.Item("Address").ToString.Trim
                'objERTran._Cityunkid = intCityId
                'objERTran._Countryunkid = intCountryId
                'objERTran._Employeeunkid = intEmployeeUnkid
                'Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                '    Case "MALE"
                '        objERTran._Gender = 1
                '    Case "FEMALE"
                '        objERTran._Gender = 2
                '    Case Else
                '        objERTran._Gender = 0
                'End Select
                'objERTran._Relationunkid = intRelationId
                'objERTran._Name = dtRow.Item("RefreeName").ToString.Trim
                'objERTran._Stateunkid = intStateId
                'objERTran._Company = dtRow.Item("company").ToString.Trim
                'objERTran._Mobile_No = dtRow.Item("mobile").ToString.Trim
                'objERTran._Ref_Position = dtRow.Item("position").ToString.Trim
                'objERTran._Telephone_No = dtRow.Item("telephone").ToString.Trim
                'objERTran._Email = dtRow.Item("email").ToString.Trim

                'If intRefereeTranId > 0 Then
                '    If objERTran.Update() Then
                '        dtRow.Item("image") = imgAccept
                '        dtRow.Item("message") = Language.getMessage(mstrModuleName, 100, "Updated Successfully.")
                '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success")
                '        dtRow.Item("objStatus") = 1
                '        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                '    Else
                '        dtRow.Item("image") = imgError
                '        dtRow.Item("message") = objERTran._Message
                '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                '        dtRow.Item("objStatus") = 2
                '        objError.Text = CStr(Val(objError.Text) + 1)
                '    End If
                'Else
                '    If objERTran.Insert() Then
                '        dtRow.Item("image") = imgAccept
                '        dtRow.Item("message") = ""
                '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success")
                '        dtRow.Item("objStatus") = 1
                '        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                '    Else
                '        dtRow.Item("image") = imgError
                '        dtRow.Item("message") = objERTran._Message
                '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                '        dtRow.Item("objStatus") = 2
                '        objError.Text = CStr(Val(objError.Text) + 1)
                '    End If
                'End If
                'S.SANDEEP [14-JUN-2018] -- END
            Next

            'Gajanan [27-May-2019] -- Start
            If RefreeApprovalFlowVal Is Nothing Then
                If mdt_ImportData_Others.Select("status <> '" & Language.getMessage(mstrModuleName, 6, "Fail") & "'").Count > 0 Then
                    If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                        Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                        mdt_ImportData_Others.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 6, "Fail") & "'"
                        objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                                   Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                   CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                                   enScreenName.frmEmployeeRefereeList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                   User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                   User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                                   User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                                   ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData_Others.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
                    End If
                End If
            End If
            'Gajanan [27-May-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            'S.SANDEEP [ 08 NOV 2013 ] -- START
            btnFilter.Enabled = True
            'S.SANDEEP [ 08 NOV 2013 ] -- END
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 08 NOV 2013 ] -- START
    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 08 NOV 2013 ] -- END

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper Like "*" & strName.ToUpper & "*") Select (p)).FirstOrDefault

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END    
#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizImportEmp.CancelText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_CancelText", Me.eZeeWizImportEmp.CancelText)
            Me.eZeeWizImportEmp.NextText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_NextText", Me.eZeeWizImportEmp.NextText)
            Me.eZeeWizImportEmp.BackText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_BackText", Me.eZeeWizImportEmp.BackText)
            Me.eZeeWizImportEmp.FinishText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_FinishText", Me.eZeeWizImportEmp.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblRefereeName.Text = Language._Object.getCaption(Me.lblRefereeName.Name, Me.lblRefereeName.Text)
            Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
            Me.lblRelation.Text = Language._Object.getCaption(Me.lblRelation.Name, Me.lblRelation.Text)
            Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblLastName.Text = Language._Object.getCaption(Me.lblLastName.Name, Me.lblLastName.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
            Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.Name, Me.lblMobile.Text)
            Me.lblTelephone.Text = Language._Object.getCaption(Me.lblTelephone.Name, Me.lblTelephone.Text)
            Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
            Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please the select proper file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please the select proper field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Referee(s).")
            Language.setMessage(mstrModuleName, 4, "Reference Name cannot be blank. Please set Reference Name in order to import Employee Reference(s).")
            Language.setMessage(mstrModuleName, 5, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 6, "Fail")
            Language.setMessage(mstrModuleName, 7, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 8, "Employee Reference Already Exist.")
            Language.setMessage(mstrModuleName, 9, "Success")
            Language.setMessage(mstrModuleName, 10, "Invalid Email.")
            Language.setMessage(mstrModuleName, 11, "Gender data in file is not vaild.")
            Language.setMessage(mstrModuleName, 41, " Fields From File.")
            Language.setMessage(mstrModuleName, 42, "Please select different Field.")
            Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
            Language.setMessage(mstrModuleName, 50, "is already Mapped with")
            Language.setMessage("clsEmployee_Refree_tran", 2, "ECODE")
			Language.setMessage("clsEmployee_Refree_tran", 3, "FIRSTNAME")
			Language.setMessage("clsEmployee_Refree_tran", 4, "SURNAME")
			Language.setMessage("clsEmployee_Refree_tran", 5, "REFREE_NAME")
			Language.setMessage("clsEmployee_Refree_tran", 6, "RELATION")
			Language.setMessage("clsEmployee_Refree_tran", 7, "ADDRESS")
			Language.setMessage("clsEmployee_Refree_tran", 8, "COUNTRY")
			Language.setMessage("clsEmployee_Refree_tran", 9, "STATE")
			Language.setMessage("clsEmployee_Refree_tran", 10, "CITY")
			Language.setMessage("clsEmployee_Refree_tran", 11, "EMAIL")
			Language.setMessage("clsEmployee_Refree_tran", 12, "GENDER")
			Language.setMessage("clsEmployee_Refree_tran", 13, "COMPANY")
			Language.setMessage("clsEmployee_Refree_tran", 14, "TELEPHONE")
			Language.setMessage("clsEmployee_Refree_tran", 15, "MOBILE_NO")
			Language.setMessage("clsEmployee_Refree_tran", 16, "POSITION")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class