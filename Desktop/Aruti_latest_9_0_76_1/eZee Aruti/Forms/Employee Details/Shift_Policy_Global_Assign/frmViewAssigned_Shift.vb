﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmViewAssigned_Shift

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmViewAssigned_Shift"
    Private mstrAdvanceFilter As String = ""
    Private objEmp_Shift_Tran As clsEmployee_Shift_Tran
    Dim iTable As DataTable = Nothing

#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim objEMaster As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Dim objSMaster As New clsshift_master
        Dim dsList As New DataSet
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                    User._Object._Userunkid, _
            '                                    FinancialYear._Object._YearUnkid, _
            '                                    Company._Object._Companyunkid, _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    ConfigParameter._Object._UserAccessModeSetting, _
            '                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                              True, False, "Emp", True)




            'Pinkal (06-Jan-2016) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "List")
            With cboShiftType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objSMaster.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEMaster = Nothing : objCMaster = Nothing
            objSMaster = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub Fill_Grid()
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            If User._Object.Privilege._AllowToViewAssignedShiftList = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End

            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes
            'iTable = objEmp_Shift_Tran.Get_List("List", dtpDate1.Value.Date, dtpDate2.Value.Date, CInt(cboEmployee.SelectedValue), CInt(cboShiftType.SelectedValue), CInt(cboShift.SelectedValue), mstrAdvanceFilter)

            Dim mdtDate1 As Date = Nothing
            Dim mdtDate2 As Date = Nothing

            If dtpDate1.Checked Then
                mdtDate1 = dtpDate1.Value.Date
            End If

            If dtpDate2.Checked Then
                mdtDate2 = dtpDate2.Value.Date
            End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.

            'iTable = objEmp_Shift_Tran.Get_List("List", mdtDate1.Date, mdtDate2.Date, CInt(cboEmployee.SelectedValue), CInt(cboShiftType.SelectedValue), CInt(cboShift.SelectedValue), mstrAdvanceFilter)
            iTable = objEmp_Shift_Tran.Get_List(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                ConfigParameter._Object._UserAccessModeSetting, "List", _
                                                mdtDate1.Date, _
                                                mdtDate2.Date, _
                                                CInt(cboEmployee.SelectedValue), _
                                                CInt(cboShiftType.SelectedValue), _
                                                CInt(cboShift.SelectedValue), _
                                                mstrAdvanceFilter)
            'Shani(24-Aug-2015) -- End

            'Pinkal (03-Jan-2014) -- End


            dgvData.AutoGenerateColumns = False
            objdgcolhcheck.DataPropertyName = "ischeck"
            objdgcolhshifttranid.DataPropertyName = "shifttranunkid"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgcolhCode.DataPropertyName = "ecode"
            dgcolhEffectiveDate.DataPropertyName = "effectivedate"
            dgcolhEmployee.DataPropertyName = "ename"
            dgcolhShift.DataPropertyName = "shiftname"
            dgvData.DataSource = iTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges
    Private Sub SetVisibility()
        Try
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteAssignedShift
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End

   

#End Region

#Region " Form's Events "

    Private Sub frmViewAssigned_Shift_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmp_Shift_Tran = New clsEmployee_Shift_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmViewAssigned_Shift_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Shift_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Shift_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If iTable IsNot Nothing Then
                Dim dtmp() As DataRow = iTable.Select("ischeck = True")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one employee in order to perform further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                If dtmp.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction(s)?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim blnFlag, blnShown As Boolean
                        blnFlag = False : blnShown = False
                        Dim iReason As String = String.Empty
                        Dim frm As New frmReasonSelection

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        frm.displayDialog(enVoidCategoryType.EMPLOYEE, iReason)
                        If iReason.Length <= 0 Then Exit Sub
                        Dim iMessage As String = String.Empty
                        For iRow As Integer = 0 To dtmp.Length - 1
                            iMessage = ""
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objEmp_Shift_Tran.isUsed(CInt(dtmp(iRow).Item("employeeunkid")), CDate(dtmp(iRow).Item("effectivedate")), iMessage, CInt(dtmp(iRow).Item("shiftunkid"))) 'S.SANDEEP [ 31 OCT 2013 ] -- START {CInt(dtmp(iRow).Item("shiftunkid"))} -- END
                            objEmp_Shift_Tran.isUsed(CInt(dtmp(iRow).Item("employeeunkid")), CDate(dtmp(iRow).Item("effectivedate")), iMessage, CInt(dtmp(iRow).Item("shiftunkid")), FinancialYear._Object._DatabaseName)
                            'Sohail (21 Aug 2015) -- End
                            If iMessage <> "" Then
                                If blnShown = False Then
                                    eZeeMsgBox.Show(iMessage, enMsgBoxStyle.Information)
                                    blnShown = True
                                    Continue For
                                Else
                                    blnShown = True
                                    Continue For
                                End If
                            End If
                            'S.SANDEEP [ 31 OCT 2013 ] -- START
                            'blnFlag = objEmp_Shift_Tran.Void_Assigned_Shift(CInt(dtmp(iRow).Item("shifttranunkid")), CInt(dtmp(iRow).Item("employeeunkid")), iReason, User._Object._Userunkid)
                            blnFlag = objEmp_Shift_Tran.Void_Assigned_Shift(CInt(dtmp(iRow).Item("shifttranunkid")), CInt(dtmp(iRow).Item("employeeunkid")), iReason, User._Object._Userunkid, CDate(dtmp(iRow).Item("effectivedate")), CInt(dtmp(iRow).Item("shiftunkid")))
                            'S.SANDEEP [ 31 OCT 2013 ] -- END
                            If blnFlag = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Issue in removing shift from employee."), enMsgBoxStyle.Information)
                                dgvData.Rows(iTable.Rows.IndexOf(dtmp(iRow))).DefaultCellStyle.ForeColor = Color.Red
                                Exit For
                            End If
                        Next
                        If blnFlag = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Shift removed successfully."), enMsgBoxStyle.Information)
                            Call Fill_Grid()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboShift.SelectedValue = 0
            cboShiftType.SelectedValue = 0
            dtpDate1.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDate2.Value = ConfigParameter._Object._CurrentDateAndTime
            objChkAll.Checked = False
            iTable = Nothing : dgvData.DataSource = Nothing : mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            If iTable IsNot Nothing Then
                For Each dr As DataRow In iTable.Rows
                    dr.Item("ischeck") = CBool(objChkAll.CheckState)
                Next
                dgvData.Refresh()
            End If
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

            If e.ColumnIndex = objdgcolhcheck.Index Then

                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                iTable.AcceptChanges()
                Dim drRow As DataRow() = iTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If iTable.Rows.Count = drRow.Length Then
                        objChkAll.CheckState = CheckState.Checked
                    Else
                        objChkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objChkAll.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
            Me.lblShiftType.Text = Language._Object.getCaption(Me.lblShiftType.Name, Me.lblShiftType.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.dgcolhCode.HeaderText = Language._Object.getCaption(Me.dgcolhCode.Name, Me.dgcolhCode.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffectiveDate.Name, Me.dgcolhEffectiveDate.HeaderText)
            Me.dgcolhShift.HeaderText = Language._Object.getCaption(Me.dgcolhShift.Name, Me.dgcolhShift.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please check atleast one employee in order to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction(s)?")
            Language.setMessage(mstrModuleName, 3, "Issue in removing shift from employee.")
            Language.setMessage(mstrModuleName, 4, "Shift removed successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class