﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeAssets_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeAssets_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objAsset As clsEmployee_Assets_Tran
    Private mintEmpAssetTranUnkid As Integer = -1
    Private mintSelectedEmployee As Integer = -1

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 14 AUG 2012 ] -- END

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = -1) As Boolean 'S.SANDEEP [ 14 AUG 2012 ] -- START -- END
        'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintEmpAssetTranUnkid = intUnkId
            menAction = eAction

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintEmployeeUnkid = intEmployeeUnkid
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            Me.ShowDialog()

            intUnkId = mintEmpAssetTranUnkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            txtAssetNo.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            cboAsset.BackColor = GUI.ColorComp
            cboCondition.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            'cboStatus.BackColor = GUI.ColorComp'Sohail (04 Jan 2012)
            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            txtSerialNo.BackColor = GUI.ColorComp
            'Sandeep [ 17 DEC 2010 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAssetNo.Text = objAsset._Asset_No
            txtRemark.Text = objAsset._Remark
            cboAsset.SelectedValue = objAsset._Assetunkid
            cboCondition.SelectedValue = objAsset._Conditionunkid
            cboEmployee.SelectedValue = objAsset._Employeeunkid
            'cboStatus.SelectedValue = objAsset._Statusunkid'Sohail (04 Jan 2012)
            If objAsset._Assign_Return_Date <> Nothing Then
                dtpAssignedDate.Value = objAsset._Assign_Return_Date
            End If
            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            txtSerialNo.Text = objAsset._Assetserial_No
            'Sandeep [ 17 DEC 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAsset._Asset_No = txtAssetNo.Text
            objAsset._Remark = txtRemark.Text
            objAsset._Assetunkid = CInt(cboAsset.SelectedValue)
            objAsset._Conditionunkid = CInt(cboCondition.SelectedValue)
            objAsset._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'Sohail (04 Jan 2012) -- Start
            'objAsset._Statusunkid = CInt(cboStatus.SelectedValue)
            If menAction = enAction.EDIT_ONE Then
                objAsset._Statusunkid = objAsset._Statusunkid
            Else
                objAsset._Statusunkid = enEmpAssetStatus.Assigned
            End If
            'Sohail (04 Jan 2012) -- End
            objAsset._Assign_Return_Date = dtpAssignedDate.Value
            If mintEmpAssetTranUnkid = -1 Then
                objAsset._Isvoid = False
                objAsset._Voiddatetime = Nothing
                objAsset._Voidreason = ""
                objAsset._Voiduserunkid = -1
                objAsset._Userunkid = 1
            Else
                objAsset._Isvoid = objAsset._Isvoid
                objAsset._Voiddatetime = objAsset._Voiddatetime
                objAsset._Voidreason = objAsset._Voidreason
                objAsset._Voiduserunkid = objAsset._Voiduserunkid
                objAsset._Userunkid = objAsset._Userunkid
            End If
            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            objAsset._Assetserial_No = txtSerialNo.Text
            'Sandeep [ 17 DEC 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData 'Sohail (04 Jan 2012)
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSET_CONDITION, True, "Condition")
            With cboCondition
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Condition")
            End With

            'Sohail (04 Jan 2012) -- Start
            'dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSET_STATUS, True, "Status")
            'With cboStatus
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("Status")
            'End With            
            'Sohail (04 Jan 2012) -- End

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSETS, True, "Assets")
            With cboAsset
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Assets")
            End With

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If


            'S.SANDEEP [14-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmAssetsRegisterList))) Then 'S.SANDEEP [09-AUG-2018] -- START -- END
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                           mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)
            'S.SANDEEP [14-AUG-2018] -- END

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (04 Jan 2012) -- Start
        Finally
            objCommon = Nothing
            objEmp = Nothing
            objMaster = Nothing
            'Sohail (04 Jan 2012) -- End
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            If txtAssetNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Asset No. cannot be blank. Asset No. is compulsory information."), enMsgBoxStyle.Information)
                txtAssetNo.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboAsset.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Asset is compulsory information. Please select Asset to continue."), enMsgBoxStyle.Information)
                cboAsset.Focus()
                Return False
            End If

            If CInt(cboCondition.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Asset Condition is compulsory information. Please select Asset Condition to continue."), enMsgBoxStyle.Information)
                cboCondition.Focus()
                Return False
            End If

            'Sohail (04 Jan 2012) -- Start
            'If CInt(cboStatus.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Asset Status is compulsory information. Please select Asset Status to continue."), enMsgBoxStyle.Information)
            '    cboStatus.Focus()
            '    Return False
            'End If
            'Sohail (04 Jan 2012) -- End

            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            If txtSerialNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Serial No. cannot be blank. Serial No. is compulsory information."), enMsgBoxStyle.Information)
                txtSerialNo.Focus()
                Return False
            End If
            'Sandeep [ 17 DEC 2010 ] -- End 


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function


    Private Sub SetVisibility()

        Try
            objbtnAddAsset.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddCondition.Enabled = User._Object.Privilege._AddCommonMasters
            'objbtnAddStatus.Enabled = User._Object.Privilege._AddCommonMasters'Sohail (04 Jan 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmEmployeeAssets_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAsset = Nothing
    End Sub

    Private Sub frmEmployeeAssets_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSaveInfo.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeAssets_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeAssets_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAsset = New clsEmployee_Assets_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objAsset._Assetstranunkid = mintEmpAssetTranUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
            End If

            Call GetValue()

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            cboEmployee.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAssets_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Assets_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Assets_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnSaveInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAsset.Update
            Else
                blnFlag = objAsset.Insert
            End If

            If blnFlag = False And objAsset._Message <> "" Then
                eZeeMsgBox.Show(objAsset._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAsset = Nothing
                    objAsset = New clsEmployee_Assets_Tran
                    Call GetValue()
                    cboEmployee.Focus()
                Else
                    mintEmpAssetTranUnkid = objAsset._Assetstranunkid
                    Me.Close()
                End If
            End If

            If mintSelectedEmployee <> -1 Then
                cboEmployee.SelectedValue = mintSelectedEmployee
            End If

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveInfo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Contols "
    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            mintSelectedEmployee = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddAsset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddAsset.Click
        Try
            Dim dsCombos As New DataSet
            Dim mintAssetId As Integer = -1
            Dim objComm As New clsCommon_Master
            Dim objfrmCommonMaster As New frmCommonMaster

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmCommonMaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCommonMaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCommonMaster)
            End If
            'Anjan (02 Sep 2011)-End 

            objfrmCommonMaster.displayDialog(mintAssetId, clsCommon_Master.enCommonMaster.ASSETS, enAction.ADD_ONE)
            dsCombos = objComm.getComboList(clsCommon_Master.enCommonMaster.ASSETS, True, "Asset")
            With cboAsset
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Asset")
                .SelectedValue = mintAssetId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddAsset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 


            With objFrm
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 15 DEC 2010 ] -- Start
    'Issue : Mr. Rutta's Comment
    'Sohail (04 Jan 2012) -- Start
    'Private Sub objbtnAddStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmCommonMaster
    '    Dim intRefId As Integer = -1
    '    Try
    '        'Anjan (02 Sep 2011)-Start
    '        'Issue : Including Language Settings.
    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If
    '        'Anjan (02 Sep 2011)-End 

    '        frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.ASSET_STATUS, enAction.ADD_ONE)
    '        If intRefId > 0 Then
    '            Dim dsList As New DataSet
    '            Dim objCMaster As New clsCommon_Master
    '            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ASSET_STATUS, True, "STATUS")
    '            With cboStatus
    '                .ValueMember = "masterunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsList.Tables("STATUS")
    '                .SelectedValue = intRefId
    '            End With
    '            dsList.Dispose()
    '            objCMaster = Nothing
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnAddStatus_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub
    'Sohail (04 Jan 2012) -- End
    Private Sub objbtnAddCondition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCondition.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1

        Try

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.ASSET_CONDITION, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ASSET_CONDITION, True, "CONDI")
                With cboCondition
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("CONDI")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCondition_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 15 DEC 2010 ] -- End 

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAssetRegister.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAssetRegister.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveInfo.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveInfo.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAssetRegister.Text = Language._Object.getCaption(Me.gbAssetRegister.Name, Me.gbAssetRegister.Text)
			Me.lblCondition.Text = Language._Object.getCaption(Me.lblCondition.Name, Me.lblCondition.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblAsset.Text = Language._Object.getCaption(Me.lblAsset.Name, Me.lblAsset.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSaveInfo.Text = Language._Object.getCaption(Me.btnSaveInfo.Name, Me.btnSaveInfo.Text)
			Me.lnEmployeeName.Text = Language._Object.getCaption(Me.lnEmployeeName.Name, Me.lnEmployeeName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnAssetInfo.Text = Language._Object.getCaption(Me.lnAssetInfo.Name, Me.lnAssetInfo.Text)
			Me.lblAssetNo.Text = Language._Object.getCaption(Me.lblAssetNo.Name, Me.lblAssetNo.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblSerialNo.Text = Language._Object.getCaption(Me.lblSerialNo.Name, Me.lblSerialNo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Asset No. cannot be blank. Asset No. is compulsory information.")
			Language.setMessage(mstrModuleName, 2, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 3, "Asset is compulsory information. Please select Asset to continue.")
			Language.setMessage(mstrModuleName, 4, "Asset Condition is compulsory information. Please select Asset Condition to continue.")
			Language.setMessage(mstrModuleName, 5, "Serial No. cannot be blank. Serial No. is compulsory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class