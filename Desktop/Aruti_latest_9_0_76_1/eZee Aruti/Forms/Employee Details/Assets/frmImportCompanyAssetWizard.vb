﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportCompanyAssetWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportRefreeWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " Form's Events "

    Private Sub frmImportCompanyAssetWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportCompanyAssetWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEmp_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEmp.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportEmp_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEmp.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If CType(ctrl, ComboBox).Name <> cboRemark.Name Then
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                            End If
                        Next
                    End If
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboCompany_Asset.Items.Add(dtColumns.ColumnName)
                cboAsset_Number.Items.Add(dtColumns.ColumnName)
                cboAssignment_Date.Items.Add(dtColumns.ColumnName)
                cboAsset_Condition.Items.Add(dtColumns.ColumnName)
                cboEmployee_Code.Items.Add(dtColumns.ColumnName)
                cboSerial_Number.Items.Add(dtColumns.ColumnName)
                cboRemark.Items.Add(dtColumns.ColumnName)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known
                If dtColumns.ColumnName = Language.getMessage("clsEmployee_Assets_Tran", 8, "Employee_Code") Then
                    dtColumns.Caption = "Employee_Code"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Assets_Tran", 9, "Company_Asset") Then
                    dtColumns.Caption = "Company_Asset"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Assets_Tran", 10, "Assignment_Date") Then
                    dtColumns.Caption = "Assignment_Date"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Assets_Tran", 11, "Asset_Condition") Then
                    dtColumns.Caption = "Asset_Condition"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Assets_Tran", 12, "Asset_Number") Then
                    dtColumns.Caption = "Asset_Number"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Assets_Tran", 13, "Serial_Number") Then
                    dtColumns.Caption = "Serial_Number"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Assets_Tran", 14, "Remark") Then
                    dtColumns.Caption = "Remark"
                End If
                'Gajanan (24 Nov 2018) -- End

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ECode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Asset", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Adate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Condition", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("AssetNo", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("SerialNo", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Remark", GetType(System.String))

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployee_Code.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ECode") = dtRow.Item(cboEmployee_Code.Text).ToString.Trim
                drNewRow.Item("Asset") = dtRow.Item(cboCompany_Asset.Text).ToString.Trim
                drNewRow.Item("Adate") = dtRow.Item(cboAssignment_Date.Text).ToString.Trim
                drNewRow.Item("Condition") = dtRow.Item(cboAsset_Condition.Text).ToString.Trim
                drNewRow.Item("AssetNo") = dtRow.Item(cboAsset_Number.Text).ToString.Trim
                drNewRow.Item("SerialNo") = dtRow.Item(cboSerial_Number.Text).ToString.Trim
                drNewRow.Item("Remark") = dtRow.Item(cboRemark.Text).ToString.Trim

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)

            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ECode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizImportEmp.BackEnabled = False
            eZeeWizImportEmp.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployee_Code.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Company Asset(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboCompany_Asset.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Asset Name cannot be blank. Please set Asset Name in order to import Employee Company Asset(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboAssignment_Date.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Date cannot be blank. Please set Date in order to import Employee Company Asset(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboAsset_Condition.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Condition cannot be blank. Please set Condition in order to import Employee Company Asset(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboAsset_Number.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Asset No. cannot be blank. Please set Asset No. in order to import Employee Company Asset(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboSerial_Number.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Serial No. cannot be blank. Please set Serial No. in order to import Employee Company Asset(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objCMaster As New clsCommon_Master
            Dim objEMaster As New clsEmployee_Master
            Dim objECAssets As clsEmployee_Assets_Tran

            Dim intAssetUnkid As Integer = -1
            Dim intEmployeeUnkid As Integer = -1
            Dim intConditionUnkid As Integer = -1
            Dim intECAssetTranId As Integer = -1

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows

                Application.DoEvents()

                intAssetUnkid = 0 : intEmployeeUnkid = 0
                intConditionUnkid = 0 : intECAssetTranId = 0

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ECode").ToString.Trim.Length > 0 Then
                    intEmployeeUnkid = objEMaster.GetEmployeeUnkid("", dtRow.Item("ECode").ToString.Trim)
                    If intEmployeeUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING CORRECT DATE
                If dtRow.Item("Adate").ToString.Trim.Length > 0 Then
                    If IsDate(dtRow.Item("Adate")) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING ASSET NAME PRESENT
                If dtRow.Item("Asset").ToString.Trim.Length > 0 Then
                    intAssetUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.ASSETS, dtRow.Item("Asset").ToString.Trim)
                    If intAssetUnkid <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master

                        objCMaster._Code = dtRow.Item("Asset").ToString
                        objCMaster._Name = dtRow.Item("Asset").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.ASSETS

                        If objCMaster.Insert() Then
                            intAssetUnkid = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Invalid Asset Assignment Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '------------------------------ CHECKING ASSET CONDITION PRESENT
                If dtRow.Item("Condition").ToString.Trim.Length > 0 Then
                    intConditionUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.ASSET_CONDITION, dtRow.Item("Condition").ToString.Trim)
                    If intConditionUnkid <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master

                        objCMaster._Code = dtRow.Item("Condition").ToString
                        objCMaster._Name = dtRow.Item("Condition").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.ASSET_CONDITION

                        If objCMaster.Insert() Then
                            intConditionUnkid = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                objECAssets = New clsEmployee_Assets_Tran

                If objECAssets.isExist(intEmployeeUnkid, intAssetUnkid, dtRow.Item("SerialNo").ToString.Trim) = True Then
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Company Asset Already Exist.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                Else
                    objECAssets._Asset_No = dtRow.Item("AssetNo")
                    objECAssets._Assetserial_No = dtRow.Item("SerialNo")
                    objECAssets._Assetunkid = intAssetUnkid
                    objECAssets._Conditionunkid = intConditionUnkid
                    objECAssets._Employeeunkid = intEmployeeUnkid
                    objECAssets._Statusunkid = 1 'Assigned
                    objECAssets._Userunkid = User._Object._Userunkid
                    objECAssets._Voiddatetime = Nothing
                    objECAssets._Voiduserunkid = 0
                    objECAssets._Remark = dtRow.Item("Remark")
                    objECAssets._Assign_Return_Date = dtRow.Item("Adate")
                    If objECAssets.Insert() = True Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objECAssets._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizImportEmp.CancelText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_CancelText", Me.eZeeWizImportEmp.CancelText)
            Me.eZeeWizImportEmp.NextText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_NextText", Me.eZeeWizImportEmp.NextText)
            Me.eZeeWizImportEmp.BackText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_BackText", Me.eZeeWizImportEmp.BackText)
            Me.eZeeWizImportEmp.FinishText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_FinishText", Me.eZeeWizImportEmp.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblSerialNo.Text = Language._Object.getCaption(Me.lblSerialNo.Name, Me.lblSerialNo.Text)
            Me.lblAssetNumber.Text = Language._Object.getCaption(Me.lblAssetNumber.Name, Me.lblAssetNumber.Text)
            Me.lblCondition.Text = Language._Object.getCaption(Me.lblCondition.Name, Me.lblCondition.Text)
            Me.lblAssignDate.Text = Language._Object.getCaption(Me.lblAssignDate.Name, Me.lblAssignDate.Text)
            Me.lblCompanyAsset.Text = Language._Object.getCaption(Me.lblCompanyAsset.Name, Me.lblCompanyAsset.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Company Asset(s).")
            Language.setMessage(mstrModuleName, 4, "Asset Name cannot be blank. Please set Asset Name in order to import Employee Company Asset(s).")
            Language.setMessage(mstrModuleName, 5, "Date cannot be blank. Please set Date in order to import Employee Company Asset(s).")
            Language.setMessage(mstrModuleName, 6, "Condition cannot be blank. Please set Condition in order to import Employee Company Asset(s).")
            Language.setMessage(mstrModuleName, 7, "Asset No. cannot be blank. Please set Asset No. in order to import Employee Company Asset(s).")
			Language.setMessage("clsEmployee_Assets_Tran", 8, "Employee_Code")
			Language.setMessage("clsEmployee_Assets_Tran", 9, "Company_Asset")
			Language.setMessage("clsEmployee_Assets_Tran", 10, "Assignment_Date")
			Language.setMessage("clsEmployee_Assets_Tran", 11, "Asset_Condition")
			Language.setMessage("clsEmployee_Assets_Tran", 12, "Asset_Number")
			Language.setMessage("clsEmployee_Assets_Tran", 13, "Serial_Number")
			Language.setMessage(mstrModuleName, 14, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
            Language.setMessage(mstrModuleName, 8, "Serial No. cannot be blank. Please set Serial No. in order to import Employee Company Asset(s).")
            Language.setMessage(mstrModuleName, 9, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 10, "Fail")
            Language.setMessage(mstrModuleName, 11, "Company Asset Already Exist.")
            Language.setMessage(mstrModuleName, 12, "Success")
            Language.setMessage(mstrModuleName, 13, "Invalid Asset Assignment Date")
            Language.setMessage(mstrModuleName, 14, "This field is already selected.Please Select New field.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class