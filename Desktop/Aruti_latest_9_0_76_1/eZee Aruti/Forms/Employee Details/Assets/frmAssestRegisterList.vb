﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAssetsRegisterList

#Region " Private Varaibles "
    Private objAsset As clsEmployee_Assets_Tran
    Private ReadOnly mstrModuleName As String = "frmAssetsRegisterList"
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 
#End Region

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Property "

    Dim mintEmployeeUnkid As Integer = -1
    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

#End Region
    'S.SANDEEP [ 14 AUG 2012 ] -- END

#Region " Private Functions "
    Private Sub FillCombo()
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData 'Sohail (04 Jan 2012)
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSET_CONDITION, True, "Condition")
            With cboCondition
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Condition")
            End With



            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSET_STATUS, True, "Status")
            'With cboStatus
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("Status")
            'End With
            'S.SANDEEP [ 29 DEC 2011 ] -- END

            'Sohail (04 Jan 2012) -- Start
            dsList = objMaster.getComboListForEmployeeAseetStatus("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Status")
            End With
            'Sohail (04 Jan 2012) -- End

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSETS, True, "Assets")
            With cboAsset
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Assets")
            End With


            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, )
            'End If

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmAssetsRegisterList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)


            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                         mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", _
                                         True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)

            'S.SANDEEP [20-JUN-2018] -- End


            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (04 Jan 2012) -- Start
        Finally
            objCommon = Nothing
            objEmp = Nothing
            objMaster = Nothing
            'Sohail (04 Jan 2012) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try



            If User._Object.Privilege._AllowToViewCompanyAssetList = True Then                'Pinkal (02-Jul-2012) -- Start

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Sohail (04 Jan 2012) -- Start
                ''dsList = objAsset.GetList("AssetList")
                'dsList = objAsset.GetList("AssetList", CInt(cboEmployee.SelectedValue), CInt(cboAsset.SelectedValue), CInt(cboCondition.SelectedValue), CInt(cboStatus.SelectedValue), txtAssestNo.Text.Trim, mstrAdvanceFilter)
                ''Sohail (04 Jan 2012) -- End

                ''Sohail (04 Jan 2012) -- Start
                ''If txtAssestNo.Text.Trim <> "" Then
                ''    StrSearching &= "AND AssetNo LIKE '%" & txtAssestNo.Text.Trim & "%'" & " "
                ''End If

                ''If CInt(cboAsset.SelectedValue) > 0 Then
                ''    StrSearching &= "AND AssetId = " & CInt(cboAsset.SelectedValue) & " "
                ''End If

                ''If CInt(cboCondition.SelectedValue) > 0 Then
                ''    StrSearching &= "AND CondId = " & CInt(cboCondition.SelectedValue) & " "
                ''End If

                ''If CInt(cboEmployee.SelectedValue) > 0 Then
                ''    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
                ''End If
                ''Sohail (04 Jan 2012) -- End

                ''S.SANDEEP [ 29 DEC 2011 ] -- START
                ''ENHANCEMENT : TRA CHANGES 
                ''TYPE : EMPLOYEMENT CONTRACT PROCESS
                ''If CInt(cboStatus.SelectedValue) > 0 Then
                ''    StrSearching &= "AND StatusId = " & CInt(cboStatus.SelectedValue) & " "
                ''End If
                ''S.SANDEEP [ 29 DEC 2011 ] -- END


                'If dtpDate.Checked Then
                '    StrSearching &= "AND Date='" & eZeeDate.convertDate(dtpDate.Value) & "' "
                'End If


                'If StrSearching.Length > 0 Then
                '    StrSearching = StrSearching.Substring(3)
                '    dtTable = New DataView(dsList.Tables("AssetList"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsList.Tables("AssetList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'End If

                If dtpDate.Checked Then
                    StrSearching &= "AND CONVERT(CHAR(8),hremployee_assets_tran.assign_return_date,112) ='" & eZeeDate.convertDate(dtpDate.Value) & "' "
                End If

                If mstrAdvanceFilter.Trim.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmAssetsRegisterList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If


                'dsList = objAsset.GetList(FinancialYear._Object._DatabaseName, _
                '                          User._Object._Userunkid, _
                '                          FinancialYear._Object._YearUnkid, _
                '                          Company._Object._Companyunkid, _
                '                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                          ConfigParameter._Object._UserAccessModeSetting, True, _
                '                          ConfigParameter._Object._IsIncludeInactiveEmp, "AssetList", , _
                '                          CInt(cboEmployee.SelectedValue), StrSearching, CInt(cboAsset.SelectedValue), _
                '                          CInt(cboCondition.SelectedValue), CInt(cboStatus.SelectedValue), txtAssestNo.Text.Trim)

                dsList = objAsset.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, "AssetList", , _
                                          CInt(cboEmployee.SelectedValue), StrSearching, CInt(cboAsset.SelectedValue), _
                                          CInt(cboCondition.SelectedValue), CInt(cboStatus.SelectedValue), txtAssestNo.Text.Trim)

                'S.SANDEEP [20-JUN-2018] -- End


                dtTable = New DataView(dsList.Tables("AssetList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'S.SANDEEP [04 JUN 2015] -- END




                lvAssetRegister.Items.Clear()

                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("EmpName").ToString
                    lvItem.SubItems.Add(dtRow.Item("Asset").ToString)
                    lvItem.SubItems.Add(dtRow.Item("AssetNo").ToString)

                    'S.SANDEEP [ 20 NOV 2014 ] -- START
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString)
                    If dtRow.Item("Date").ToString.Trim.Length > 0 Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    'S.SANDEEP [ 20 NOV 2014 ] -- END

                    lvItem.SubItems.Add(dtRow.Item("Status").ToString)
                    lvItem.SubItems(colhStatus.Index).Tag = CInt(dtRow.Item("asset_statusunkid").ToString)

                    lvItem.SubItems.Add(dtRow.Item("Condtion").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Remark").ToString)

                    lvItem.Tag = dtRow.Item("assetstranunkid")

                    lvAssetRegister.Items.Add(lvItem)

                    lvItem = Nothing
                Next

                If lvAssetRegister.Items.Count > 3 Then
                    colhRemark.Width = 173 - 18
                Else
                    colhRemark.Width = 173
                End If

                lvAssetRegister.GroupingColumn = colhEmployeeName
                lvAssetRegister.DisplayGroups(True)

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnAssign.Enabled = User._Object.Privilege._AddEmployeeAssets
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeAssets
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeAssets
            btnChangeStatus.Enabled = User._Object.Privilege._AllowChangeAssetStatus 'Sohail (04 Jan 2012)

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges
            mnuImportCompanyAssets.Enabled = User._Object.Privilege._AllowToImportCompanyAssets
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmAssetsRegisterList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAsset = New clsEmployee_Assets_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()

            Call SetVisibility()
            Call FillCombo()

            'Call FillList()

            If lvAssetRegister.Items.Count > 0 Then lvAssetRegister.Items(0).Selected = True
            lvAssetRegister.Select()

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssetsRegisterList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvAssetRegister.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmAssetsRegisterList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmAssetsRegisterList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAsset = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Assets_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Assets_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Buton's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAssetRegister.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAssetRegister.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAssetRegister.SelectedItems(0).Index

            'Sohail (04 Jan 2012) -- Start
            If CInt(lvAssetRegister.SelectedItems(0).SubItems(colhStatus.Index).Tag) > 0 AndAlso CInt(lvAssetRegister.SelectedItems(0).SubItems(colhStatus.Index).Tag) <> enEmpAssetStatus.Assigned Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry! You can delete this transaction only when status is Assigned."), enMsgBoxStyle.Information) '?1
                lvAssetRegister.Select()
                Exit Sub
            End If
            'Sohail (04 Jan 2012) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objAsset.Delete(CInt(lvAssetRegister.SelectedItems(0).Tag), True, CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt")), 1, "Testing")
                objAsset._Isvoid = True
                objAsset._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objAsset._Voiduserunkid = User._Object._Userunkid
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                'Anjan (02 Sep 2011)-Start
                'Issue : Including Language Settings.
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Anjan (02 Sep 2011)-End 


                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objAsset._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objAsset.Delete(CInt(lvAssetRegister.SelectedItems(0).Tag))
                'Sandeep [ 16 Oct 2010 ] -- End 

                lvAssetRegister.SelectedItems(0).Remove()

                If lvAssetRegister.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAssetRegister.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAssetRegister.Items.Count - 1
                    lvAssetRegister.Items(intSelectedIndex).Selected = True
                    lvAssetRegister.EnsureVisible(intSelectedIndex)
                ElseIf lvAssetRegister.Items.Count <> 0 Then
                    lvAssetRegister.Items(intSelectedIndex).Selected = True
                    lvAssetRegister.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvAssetRegister.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvAssetRegister.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAssetRegister.Select()
            Exit Sub
        End If
        Dim frm As New frmEmployeeAssets_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAssetRegister.SelectedItems(0).Index

            'Sohail (04 Jan 2012) -- Start
            If CInt(lvAssetRegister.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enEmpAssetStatus.Returned _
                                    OrElse CInt(lvAssetRegister.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enEmpAssetStatus.WrittenOff Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! You can not edit transaction when status is Returned OR Written Off."), enMsgBoxStyle.Information)
                lvAssetRegister.Select()
                Exit Try
            End If
            'Sohail (04 Jan 2012) -- End

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(CInt(lvAssetRegister.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            '    Call FillList()
            'End If

            If frm.displayDialog(CInt(lvAssetRegister.SelectedItems(0).Tag), enAction.EDIT_ONE, mintEmployeeUnkid) Then
                If mintEmployeeUnkid > 0 Then Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            frm = Nothing

            lvAssetRegister.Items(intSelectedIndex).Selected = True
            lvAssetRegister.EnsureVisible(intSelectedIndex)
            lvAssetRegister.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim frm As New frmEmployeeAssets_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
            '    Call FillList()
            'End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, mintEmployeeUnkid) Then
                If mintEmployeeUnkid > 0 Then Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAsset.SelectedValue = 0
            cboCondition.SelectedValue = 0

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboEmployee.SelectedValue = 0
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            Else
                cboEmployee.SelectedValue = 0
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'cboStatus.SelectedValue = 0
            'S.SANDEEP [ 29 DEC 2011 ] -- END
            txtAssestNo.Text = ""
            dtpDate.Checked = False
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 


            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 
#End Region

    'Sohail (04 Jan 2012) -- Start
#Region " Other Control's Events "
    Private Sub mnuReturned_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReturned.Click, mnuWrittenOff.Click, mnuLost.Click
        Dim objAssetStatus As New frmEmpAssetStatus_AddEdit
        Try
            If lvAssetRegister.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Employee Asset from the list to perform further operation."), enMsgBoxStyle.Information)
                lvAssetRegister.Select()
                Exit Try
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAssetRegister.SelectedItems(0).Index

            Dim menStatus As enEmpAssetStatus
            Dim obj As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
            If obj.Name = mnuReturned.Name Then
                menStatus = enEmpAssetStatus.Returned
            ElseIf obj.Name = mnuWrittenOff.Name Then
                menStatus = enEmpAssetStatus.WrittenOff
            ElseIf obj.Name = mnuLost.Name Then
                menStatus = enEmpAssetStatus.Lost
            Else
                menStatus = enEmpAssetStatus.WrittenOff
            End If

            If CInt(lvAssetRegister.SelectedItems(0).SubItems(colhStatus.Index).Tag) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry! Current Status not found."), enMsgBoxStyle.Information)
                lvAssetRegister.Select()
                Exit Try
            ElseIf CInt(lvAssetRegister.SelectedItems(0).SubItems(colhStatus.Index).Tag) = CInt(menStatus) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry! New Status should be different."), enMsgBoxStyle.Information)
                lvAssetRegister.Select()
                Exit Try
            ElseIf CInt(lvAssetRegister.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enEmpAssetStatus.Returned _
                        OrElse CInt(lvAssetRegister.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enEmpAssetStatus.WrittenOff Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! You can not change status when status is Returned OR Written Off."), enMsgBoxStyle.Information)
                lvAssetRegister.Select()
                Exit Try
            End If

            If objAssetStatus.displayDialog(CInt(lvAssetRegister.SelectedItems(0).Tag), menStatus) Then
                Call FillList()
            End If

            objAssetStatus = Nothing

            lvAssetRegister.Items(intSelectedIndex).Selected = True
            lvAssetRegister.EnsureVisible(intSelectedIndex)
            lvAssetRegister.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 12 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuGetFileFormat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension
                strFilePath = ObjFile.DirectoryName & "\" & strFilePath
                dsList = objAsset.GetImportFileFormat("List")
                dsList.Tables(0).Rows.Clear()

                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                    Case 2   'XML
                        dsList.WriteXml(strFilePath)
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "File exported successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImportCompanyAssets_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuImportCompanyAssets.Click
        Dim frm As New frmImportCompanyAssetWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportCompanyAssets_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 12 JAN 2012 ] -- END

#End Region
    'Sohail (04 Jan 2012) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblAsset.Text = Language._Object.getCaption(Me.lblAsset.Name, Me.lblAsset.Text)
            Me.lblAssetNo.Text = Language._Object.getCaption(Me.lblAssetNo.Name, Me.lblAssetNo.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblCodition.Text = Language._Object.getCaption(Me.lblCodition.Name, Me.lblCodition.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhEmployeeName.Text = Language._Object.getCaption(CStr(Me.colhEmployeeName.Tag), Me.colhEmployeeName.Text)
            Me.colhAssetName.Text = Language._Object.getCaption(CStr(Me.colhAssetName.Tag), Me.colhAssetName.Text)
            Me.colhAssestNo.Text = Language._Object.getCaption(CStr(Me.colhAssestNo.Tag), Me.colhAssestNo.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.colhCondition.Text = Language._Object.getCaption(CStr(Me.colhCondition.Tag), Me.colhCondition.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
            Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
            Me.mnuReturned.Text = Language._Object.getCaption(Me.mnuReturned.Name, Me.mnuReturned.Text)
            Me.mnuWrittenOff.Text = Language._Object.getCaption(Me.mnuWrittenOff.Name, Me.mnuWrittenOff.Text)
            Me.mnuLost.Text = Language._Object.getCaption(Me.mnuLost.Name, Me.mnuLost.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
            Me.mnuImportCompanyAssets.Text = Language._Object.getCaption(Me.mnuImportCompanyAssets.Name, Me.mnuImportCompanyAssets.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?")
            Language.setMessage(mstrModuleName, 3, "Sorry! You can delete this transaction only when status is Assigned.")
            Language.setMessage(mstrModuleName, 4, "Sorry! You can not edit transaction when status is Returned OR Written Off.")
            Language.setMessage(mstrModuleName, 5, "Sorry! Current Status not found.")
            Language.setMessage(mstrModuleName, 6, "Sorry! New Status should be different.")
            Language.setMessage(mstrModuleName, 7, "Sorry! You can not change status when status is Returned OR Written Off.")
            Language.setMessage(mstrModuleName, 8, "Please select Employee Asset from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 9, "File exported successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class