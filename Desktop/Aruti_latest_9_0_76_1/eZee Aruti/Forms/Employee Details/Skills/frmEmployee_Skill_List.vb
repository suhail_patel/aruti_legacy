﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployee_Skill_List

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployee_Skill_List"
    Private objSkillTran As clsEmployee_Skill_Tran
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 


    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private imgInfo As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.information)
    Private mintTransactionId As Integer = 0
    Private objASkillTran As clsEmployeeSkill_Approval_Tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim SkillApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployee_Skill_List)))
    Private intParentRowIndex As Integer = -1
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim isEmployeeApprove As Boolean = False
    'Gajanan [22-Feb-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End
#End Region

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Property "
    Dim mintEmployeeUnkid As Integer = -1
    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property
#End Region
    'S.SANDEEP [ 14 AUG 2012 ] -- END

#Region " Private Function "
    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objSkillMaster As New clsskill_master
        Dim objCommom As New clsCommon_Master
        Dim dsList As New DataSet
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, )
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployee_Skill_List))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If


            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                    User._Object._Userunkid, _
            '                                    FinancialYear._Object._YearUnkid, _
            '                                    Company._Object._Companyunkid, _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    ConfigParameter._Object._UserAccessModeSetting, _
            '                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                                                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'S.SANDEEP [20-JUN-2018] -- End


            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = objSkillMaster.getComboList("Skills", True)
            With cboSkill
                .ValueMember = "skillunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Skills")
                .SelectedValue = 0
            End With

            dsList = objCommom.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "SkillCategory")
            With cboSkillCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("SkillCategory")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objCommom = Nothing
            objSkillMaster = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try


            If User._Object.Privilege._AllowToViewEmpSkillList = True Then    'Pinkal (02-Jul-2012) -- Start

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'dsList = objSkillTran.GetList("Emp", False)

                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
                'End If

                'If CInt(cboSkillCategory.SelectedValue) > 0 Then
                '    StrSearching &= "AND CatId = " & CInt(cboSkillCategory.SelectedValue) & " "
                'End If

                'If CInt(cboSkill.SelectedValue) > 0 Then
                '    StrSearching &= "AND SkillId = " & CInt(cboSkill.SelectedValue) & " "
                'End If

                ''Anjan (21 Nov 2011)-Start
                ''ENHANCEMENT : TRA COMMENTS
                'If mstrAdvanceFilter.Length > 0 Then
                '    StrSearching &= "AND " & mstrAdvanceFilter
                'End If
                ''Anjan (21 Nov 2011)-End 


                'If StrSearching.Length > 0 Then
                '    StrSearching = StrSearching.Substring(3)
                '    dtTable = New DataView(dsList.Tables("Emp"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = dsList.Tables("Emp")
                'End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboSkillCategory.SelectedValue) > 0 Then
                    StrSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboSkillCategory.SelectedValue) & " "
                End If

                If CInt(cboSkill.SelectedValue) > 0 Then
                    StrSearching &= "AND hrskill_master.skillunkid = " & CInt(cboSkill.SelectedValue) & " "
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If


                'Pinkal (28-Dec-2015) -- Start
                'Enhancement - Working on Changes in SS for Employee Master.

                'dsList = objSkillTran.GetList(FinancialYear._Object._DatabaseName, _
                '                              User._Object._Userunkid, _
                '                              FinancialYear._Object._YearUnkid, _
                '                              Company._Object._Companyunkid, _
                '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                              ConfigParameter._Object._UserAccessModeSetting, True, _
                '                              ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , StrSearching)

                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployee_Skill_List))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If



                'dsList = objSkillTran.GetList(FinancialYear._Object._DatabaseName, _
                '                              User._Object._Userunkid, _
                '                              FinancialYear._Object._YearUnkid, _
                '                              Company._Object._Companyunkid, _
                '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                              ConfigParameter._Object._UserAccessModeSetting, True, _
                '                          ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , StrSearching, True)


                dsList = objSkillTran.GetList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                                       ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , StrSearching, _
                                                       True, mblnAddApprovalCondition)


                'S.SANDEEP [20-JUN-2018] -- End



                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsList.Tables(0).Columns.Add(dcol)





                If SkillApprovalFlowVal Is Nothing Then
                    Dim dsPending As New DataSet
                    dsPending = objASkillTran.GetList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , StrSearching, True, mblnAddApprovalCondition)

                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsList.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [17-DEC-2018] -- End


                'Pinkal (28-Dec-2015) -- End


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'dtTable = dsList.Tables("Emp")
                dtTable = New DataView(dsList.Tables("Emp"), "", "name", DataViewRowState.CurrentRows).ToTable()
                'Gajanan [17-DEC-2018] -- End


                lvSkillList.BeginUpdate()
                'S.SANDEEP [04 JUN 2015] -- END

                lvSkillList.Items.Clear()



                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'For Each dtRow As DataRow In dtTable.Rows
                '    lvItem = New ListViewItem

                '    lvItem.Text = dtRow.Item("NAME").ToString
                '    lvItem.SubItems.Add(dtRow.Item("Category").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("SkillName").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("Description").ToString)
                '    lvItem.Tag = dtRow.Item("SkillTranId")
                '    lvSkillList.Items.Add(lvItem)
                '    lvItem = Nothing
                'Next

                RemoveHandler lvSkillList.ItemSelectionChanged, AddressOf lvSkillList_ItemSelectionChanged

                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dtRow.Item("Category").ToString()
                    lvItem.SubItems.Add(dtRow.Item("SkillName").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Description").ToString)
                    lvItem.SubItems.Add(dtRow.Item("tranguid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("EmpId").ToString)
                    lvItem.SubItems(objdgcolhtranguid.Index).Tag = dtRow.Item("operationtypeid").ToString()
                    lvItem.SubItems.Add(dtRow.Item("NAME").ToString)
                    If dtRow.Item("tranguid").ToString.Length > 0 Then
                        lvItem.BackColor = Color.PowderBlue
                        lvItem.ForeColor = Color.Black
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                        objtblPanel.Visible = True
                        btnApprovalinfo.Visible = False
                    End If
                    If CInt(dtRow.Item("operationtypeid")) > 0 Then
                        lvItem.ToolTipText = colhSkillCategory.Text & " : " & dtRow("pCategory").ToString() & " - " & colhSkill.Text & " : " & dtRow("pSkill").ToString
                    End If
                    lvItem.SubItems.Add(dtRow("OperationType").ToString())
                    lvItem.Tag = dtRow.Item("SkillTranId")
                    lvSkillList.Items.Add(lvItem)
                    lvItem = Nothing
                Next
                colhOperationType.DisplayIndex = 0
                AddHandler lvSkillList.ItemSelectionChanged, AddressOf lvSkillList_ItemSelectionChanged
                'Gajanan [17-DEC-2018] -- End

                lvSkillList.GroupingColumn = colhEmployee
                lvSkillList.DisplayGroups(True)

                colhDescription.Width += colhEmployee.Width

                colhEmployee.Width = 0

                If lvSkillList.Items.Count > 16 Then
                    colhDescription.Width = 484 - 10
                Else
                    colhDescription.Width = 484
                End If


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                lvSkillList.EndUpdate()
                'S.SANDEEP [04 JUN 2015] -- END

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeSkill
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeSkill
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeSkill

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuExportEmployeeSkills.Enabled = User._Object.Privilege._AllowtoExportEmpSkills
            mnuImportEmployeeSkills.Enabled = User._Object.Privilege._AllowtoImportEmpSkills
            'Anjan (25 Oct 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmEmployee_Skill_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSkillTran = New clsEmployee_Skill_Tran

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objASkillTran = New clsEmployeeSkill_Approval_Tran
        'Gajanan [17-DEC-2018] -- End

        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()

            Call FillCombo()

            'Call FillList()


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objtblPanel.Visible = False
            btnApprovalinfo.Visible = False

            'Gajanan [17-DEC-2018] -- End


            Call SetVisibility()

            If lvSkillList.Items.Count > 0 Then lvSkillList.Items(0).Selected = True
            lvSkillList.Select()

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Skill_List_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvSkillList.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmEmployee_Skill_List_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployee_Skill_List_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objSkillTran = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Skill_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Skill_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Buton's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        'Gajanan [9-April-2019] -- Start
        objApprovalData = New clsEmployeeDataApproval
        'Gajanan [9-April-2019] -- End

        If lvSkillList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSkillList.Select()
            Exit Sub
        End If

        Try

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(lvSkillList.SelectedItems(0).SubItems(objdgcolhempid.Index).Text) & "'")
            If itmp.Length > 0 Then
                isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
            Else

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'isEmployeeApprove = True
                isEmployeeApprove = False
                'Gajanan [17-April-2019] -- End

            End If
            'Gajanan [22-Feb-2019] -- End




            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If SkillApprovalFlowVal Is Nothing Then
            If SkillApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmEmployee_Skill_List, FinancialYear._Object._DatabaseName, _
                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(lvSkillList.SelectedItems(0).SubItems(objdgcolhempid.Index).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End

                Dim item As ListViewItem = Nothing
                item = lvSkillList.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvSkillList.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text.Trim.Length > 0).FirstOrDefault()
                If item IsNot Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), enMsgBoxStyle.Information)
                    lvSkillList.SelectedItems(0).Selected = False
                    Exit Sub
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSkillList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objSkillTran.Delete(CInt(lvSkillList.SelectedItems(0).Tag), True, 1, CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt")), "TESTING")
                Dim frm As New frmReasonSelection

                'Anjan (02 Sep 2011)-Start
                'Issue : Including Language Settings.
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Anjan (02 Sep 2011)-End 

                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                frm = Nothing
'objSkillTran.Delete(CInt(lvSkillList.SelectedItems(0).Tag), True, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If SkillApprovalFlowVal Is Nothing Then
                If SkillApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [22-Feb-2019] -- End
                    objASkillTran._Isvoid = True
                    objASkillTran._Audituserunkid = User._Object._Userunkid
                    objASkillTran._Isweb = False
                    objASkillTran._Ip = getIP()
                    objASkillTran._Host = getHostName()
                    objASkillTran._Form_Name = mstrModuleName
                    If objASkillTran.Delete(CInt(lvSkillList.SelectedItems(0).Tag), mstrVoidReason, Company._Object._Companyunkid, Nothing) = False Then
                        If objASkillTran._Message <> "" Then
                            eZeeMsgBox.Show(objASkillTran._Message, enMsgBoxStyle.Information)
                        End If

                        Exit Sub
                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                                 enScreenName.frmEmployee_Skill_List, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                 User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                 User._Object._Username, clsEmployeeDataApproval.enOperationType.DELETED, , lvSkillList.SelectedItems(0).SubItems(objdgcolhempid.Index).Text, , , _
                                                                 " skillstranunkid = " & CInt(lvSkillList.SelectedItems(0).Tag), Nothing, , , _
                                                                 " skillstranunkid = " & CInt(lvSkillList.SelectedItems(0).Tag), Nothing)
                    End If
                Else
                    If objSkillTran.Delete(CInt(lvSkillList.SelectedItems(0).Tag), True, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) = False Then
                        If objSkillTran._Message <> "" Then
                            eZeeMsgBox.Show(objSkillTran._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                End If


                'Sandeep [ 16 Oct 2010 ] -- End 

                lvSkillList.SelectedItems(0).Remove()

                If lvSkillList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvSkillList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvSkillList.Items.Count - 1
                    lvSkillList.Items(intSelectedIndex).Selected = True
                    lvSkillList.EnsureVisible(intSelectedIndex)
                ElseIf lvSkillList.Items.Count <> 0 Then
                    lvSkillList.Items(intSelectedIndex).Selected = True
                    lvSkillList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvSkillList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvSkillList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSkillList.Select()
            Exit Sub
        End If
        Dim frm As New frmEmployeeSkills_AddEdit
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If SkillApprovalFlowVal Is Nothing Then
                Dim item As ListViewItem = Nothing
                item = lvSkillList.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvSkillList.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text.Trim.Length > 0).FirstOrDefault()
                If item IsNot Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), enMsgBoxStyle.Information)
                    lvSkillList.SelectedItems(0).Selected = False
                    Exit Sub
                End If
            End If
            'Gajanan [17-DEC-2018] -- End
            

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSkillList.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(CInt(lvSkillList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            '    Call FillList()
            'End If

            If frm.displayDialog(CInt(lvSkillList.SelectedItems(0).Tag), enAction.EDIT_ONE, mintEmployeeUnkid) Then
                If mintEmployeeUnkid > 0 Then Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            frm = Nothing

            lvSkillList.Items(intSelectedIndex).Selected = True
            lvSkillList.EnsureVisible(intSelectedIndex)
            lvSkillList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeSkills_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
            '    Call FillList()
            'End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, mintEmployeeUnkid) Then
                If mintEmployeeUnkid > 0 Then Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboEmployee.SelectedValue = 0
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            Else
                cboEmployee.SelectedValue = 0
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            cboSkill.SelectedValue = 0
            cboSkillCategory.SelectedValue = 0
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private Sub btnApprovalinfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Dim empid As Integer = 0
            If lvSkillList.SelectedItems.Count > 0 Then
                empid = CInt(lvSkillList.SelectedItems(0).SubItems(objdgcolhempid.Index).Text)
            End If
            Dim frm As New frmViewEmployeeDataApproval
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            '" ( " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications) & _
            '       ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences) & _
            '       ", " & CInt() & _
            '       ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences) & " ) "


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'frm.displayDialog(User._Object._Userunkid, 0, enUserPriviledge.AllowToApproveRejectEmployeeSkills, enScreenName.frmEmployee_Skill_List, CType(lvSkillList.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag, clsEmployeeDataApproval.enOperationType), "EM.employeeunkid = " & empid, False)
            frm.displayDialog(User._Object._Userunkid, 0, enUserPriviledge.AllowToApproveRejectEmployeeSkills, enScreenName.frmEmployee_Skill_List, clsEmployeeDataApproval.enOperationType.NONE, "EM.employeeunkid = " & empid, False)
            'Gajanan [17-DEC-2018] -- End

            If frm IsNot Nothing Then frm.Dispose()
            Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprovalinfo_Click", mstrModuleName)
        End Try
    End Sub
    'Gajanan [17-DEC-2018] -- End

#End Region

#Region " Controls Events "

    Private Sub mnuExportEmployeeSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportEmployeeSkills.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'S.SANDEEP [12-Jan-2018] -- END
                strFilePath &= ObjFile.Extension

                dsList = objSkillTran.GetEmployeeSkillData_Export()

                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                    Case 2   'XML
                        dsList.WriteXml(strFilePath)
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "File exported successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList.Dispose() : path = String.Empty : strFilePath = String.Empty : dlgSaveFile = Nothing : ObjFile = Nothing
        End Try
    End Sub

    Private Sub mnuImportEmployeeSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmployeeSkills.Click
        Dim frm As New frmImportEmpSkillWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportEmployeeSkills_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 
#End Region

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
#Region " Listview Events "

    Private Sub lvSkillList_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvSkillList.ItemSelectionChanged
        Try
            If lvSkillList.SelectedItems.Count > 0 Then

                If intParentRowIndex <> -1 Then
                    lvSkillList.Items(intParentRowIndex).BackColor = Color.White
                    intParentRowIndex = -1
                End If

                If lvSkillList.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Text.Length > 0 Then
                    If lvSkillList.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag IsNot Nothing Then
                        If CInt(lvSkillList.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            Dim item As ListViewItem = lvSkillList.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvSkillList.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text = "").FirstOrDefault()
                            If item IsNot Nothing Then intParentRowIndex = item.Index
                            lvSkillList.Items(intParentRowIndex).BackColor = Color.LightCoral
                        End If
                    End If
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                    btnOperation.Visible = False
                    btnApprovalinfo.Visible = True
                    objtblPanel.Visible = True
                Else
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                    btnOperation.Visible = True
                    btnApprovalinfo.Visible = False
                    objtblPanel.Visible = False
                End If
            Else
                btnEdit.Enabled = True
                btnDelete.Enabled = True
                btnOperation.Visible = True
                btnApprovalinfo.Visible = False
                objtblPanel.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSkillList_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub


#End Region
    'Gajanan [17-DEC-2018] -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprovalinfo.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApprovalinfo.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblSkillCategory.Text = Language._Object.getCaption(Me.lblSkillCategory.Name, Me.lblSkillCategory.Text)
            Me.lblSkill.Text = Language._Object.getCaption(Me.lblSkill.Name, Me.lblSkill.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhSkillCategory.Text = Language._Object.getCaption(CStr(Me.colhSkillCategory.Tag), Me.colhSkillCategory.Text)
            Me.colhSkill.Text = Language._Object.getCaption(CStr(Me.colhSkill.Tag), Me.colhSkill.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuImportEmployeeSkills.Text = Language._Object.getCaption(Me.mnuImportEmployeeSkills.Name, Me.mnuImportEmployeeSkills.Text)
            Me.mnuExportEmployeeSkills.Text = Language._Object.getCaption(Me.mnuExportEmployeeSkills.Name, Me.mnuExportEmployeeSkills.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.Name, Me.lblPendingData.Text)
			Me.btnApprovalinfo.Text = Language._Object.getCaption(Me.btnApprovalinfo.Name, Me.btnApprovalinfo.Text)
			Me.colhOperationType.Text = Language._Object.getCaption(CStr(Me.colhOperationType.Tag), Me.colhOperationType.Text)
			Me.lblParentData.Text = Language._Object.getCaption(Me.lblParentData.Name, Me.lblParentData.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?")
            Language.setMessage(mstrModuleName, 3, "File exported successfully.")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process.")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class