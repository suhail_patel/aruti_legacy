﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportEmpSkillWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportEmpSkillWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objASkillTran As clsEmployeeSkill_Approval_Tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim SkillApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployee_Skill_List)))
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [17-April-2019] -- End

#End Region

#Region " Form's Events "

    Private Sub frmImportExperienceWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objASkillTran = New clsEmployeeSkill_Approval_Tran
            objApprovalData = New clsEmployeeDataApproval
            'Gajanan [17-April-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportExperienceWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEmp_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEmp.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportEmp_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEmp.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                Select Case CType(ctrl, ComboBox).Name.ToUpper
                                    'S.SANDEEP [19-JUL-2018] -- START
                                    Case cboFIRSTNAME.Name.ToUpper, cboSURNAME.Name.ToUpper, cboDESCRIPTION.Name.ToUpper
                                        'Case "CBOFIRSTNAME", "CBOLASTNAME", "CBODESCRIPTION"
                                        'S.SANDEEP [19-JUL-2018] -- END
                                        Continue For
                                    Case Else
                                        If CType(ctrl, ComboBox).Text = "" Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                            e.Cancel = True
                                            Exit For
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboSKILL_CATEGORY.Items.Add(dtColumns.ColumnName)
                cboECODE.Items.Add(dtColumns.ColumnName)
                cboFIRSTNAME.Items.Add(dtColumns.ColumnName)
                cboSKILL_NAME.Items.Add(dtColumns.ColumnName)
                cboSURNAME.Items.Add(dtColumns.ColumnName)
                cboDESCRIPTION.Items.Add(dtColumns.ColumnName)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known

                If dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 3, "ECODE") Then
                    dtColumns.Caption = "ECODE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 4, "FIRSTNAME") Then
                    dtColumns.Caption = "FIRSTNAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 5, "SURNAME") Then
                    dtColumns.Caption = "SURNAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 6, "SKILL_CATEGORY") Then
                    dtColumns.Caption = "SKILL_CATEGORY"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 7, "SKILL_NAME") Then
                    dtColumns.Caption = "SKILL_NAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmployee_Refree_tran", 8, "DESCRIPTION") Then
                    dtColumns.Caption = "DESCRIPTION"
                End If
                'Gajanan (24 Nov 2018) -- End

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ECode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Firstname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Surname", System.Type.GetType("System.String"))
            'Gajanan [27-May-2019] -- Start
            'mdt_ImportData_Others.Columns.Add("Skill_Category", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Skill", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Description", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("Skill_Category", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "scategory")
            mdt_ImportData_Others.Columns.Add("Skill", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "skill")
            mdt_ImportData_Others.Columns.Add("Description", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "description")
            'Gajanan [27-May-2019] -- End

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData_Others.Columns.Add("EmployeeId", System.Type.GetType("System.String"))
            'Gajanan [27-May-2019] -- End

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboECODE.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ECode") = dtRow.Item(cboECODE.Text).ToString.Trim

                If cboFIRSTNAME.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Firstname") = dtRow.Item(cboFIRSTNAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Firstname") = ""
                End If

                If cboSURNAME.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Surname") = dtRow.Item(cboSURNAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Surname") = ""
                End If

                If cboSKILL_CATEGORY.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Skill_Category") = dtRow.Item(cboSKILL_CATEGORY.Text).ToString.Trim
                Else
                    drNewRow.Item("Skill_Category") = ""
                End If

                If cboSKILL_NAME.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Skill") = dtRow.Item(cboSKILL_NAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Skill") = ""
                End If

                If cboDESCRIPTION.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Description") = dtRow.Item(cboDESCRIPTION.Text).ToString.Trim
                Else
                    drNewRow.Item("Description") = ""
                End If

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ECode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                dgData.AutoGenerateColumns = False
                dgData.DataSource = mdt_ImportData_Others
            End If

            Call Import_Data()

            eZeeWizImportEmp.BackEnabled = False
            eZeeWizImportEmp.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboECODE.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Experience(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboSKILL_CATEGORY.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Skill Category cannot be blank. Please set Skill Category in order to import Employee Experience(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboSKILL_NAME.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Skill cannot be blank. Please set Skill in order to import Employee Experience(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objCMaster As New clsCommon_Master
            Dim objSMaster As New clsskill_master
            Dim objEMaster As New clsEmployee_Master

            Dim objESTran As clsEmployee_Skill_Tran

            Dim intSkillCategoryUnkid As Integer = -1
            Dim intSkillUnkid As Integer = -1
            Dim intEmployeeUnkid As Integer = -1
            Dim intSkillTranUnkid As Integer = -1
            'Gajanan [27-May-2019] -- Start
            Dim EmpList As New List(Of String)
            'Gajanan [27-May-2019] -- End

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim isEmployeeApprove As Boolean = False
                'Gajanan [22-Feb-2019] -- End



                'Gajanan [9-NOV-2019] -- Start   
                'Enhancement:Worked On NMB Bio Data Approval Query Optimization   

                ''Gajanan [17-April-2019] -- Start
                ''Enhancement - Implementing Employee Approver Flow On Employee Data.
                'isEmployeeApprove = objEMaster.IsEmployeeApproved(dtRow.Item("ECode").ToString.Trim)
                ''Gajanan [17-April-2019] -- End

                'Gajanan [9-NOV-2019] -- End


                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ECode").ToString.Trim.Length > 0 Then
                    intEmployeeUnkid = objEMaster.GetEmployeeUnkid("", dtRow.Item("ECode").ToString.Trim)
                    If intEmployeeUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                'Gajanan [27-May-2019] -- Start              

                isEmployeeApprove = objEMaster.IsEmployeeApproved(dtRow.Item("ECode").ToString.Trim)
                If isEmployeeApprove Then
                    dtRow.Item("EmployeeId") = intEmployeeUnkid
                End If
                'Gajanan [27-May-2019] -- End


                '------------------------------ CHECKING IF SKILL CATEGORY PRESENT
                If dtRow.Item("Skill_Category").ToString.Trim.Length > 0 Then
                    intSkillCategoryUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, dtRow.Item("Skill_Category").ToString.Trim)
                    If intSkillCategoryUnkid <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master

                        objCMaster._Code = dtRow.Item("Skill_Category").ToString
                        objCMaster._Name = dtRow.Item("Skill_Category").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.SKILL_CATEGORY

                        If objCMaster.Insert() Then
                            intSkillCategoryUnkid = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '------------------------------ CHECKING IF SKILL PRESENT
                If dtRow.Item("Skill").ToString.Trim.Length > 0 Then
                    intSkillUnkid = objSMaster.GetSkillUnkid(dtRow.Item("Skill").ToString.Trim)
                    If intSkillUnkid <= 0 Then
                        If Not objSMaster Is Nothing Then objSMaster = Nothing
                        objSMaster = New clsskill_master

                        objSMaster._Isactive = True
                        objSMaster._Skillcategoryunkid = intSkillCategoryUnkid
                        objSMaster._Skillcode = dtRow.Item("Skill").ToString.Trim
                        objSMaster._Skillname = dtRow.Item("Skill").ToString.Trim

                        If objSMaster.Insert Then
                            intSkillUnkid = objSMaster._Skillunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objSMaster._Skillcode & "/" & objSMaster._Skillname & ":" & objSMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                objESTran = New clsEmployee_Skill_Tran

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If SkillApprovalFlowVal Is Nothing Then
                If SkillApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                    If objApprovalData.IsApproverPresent(enScreenName.frmEmployee_Skill_List, FinancialYear._Object._DatabaseName, _
                                                          ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                          FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                          User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, intEmployeeUnkid, Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = objApprovalData._Message
                        'Gajanan [27-May-2019] -- Start              
                        'dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")

                        'dtRow.Item("objStatus") = 2
                        'Gajanan [27-May-2019] -- End

                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For
                    End If
                    'Gajanan [17-April-2019] -- End

                    Dim intOperationType As Integer = 0 : Dim strMsg As String = ""
                    If objASkillTran.isExist(intEmployeeUnkid, False, Nothing, intSkillUnkid, "", Nothing, True, intOperationType) = True Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                strMsg = Language.getMessage("frmEmployeeSkills_AddEdit", 100, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in edit mode.")
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                strMsg = Language.getMessage("frmEmployeeSkills_AddEdit", 102, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in deleted mode.")
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                strMsg = Language.getMessage("frmEmployeeSkills_AddEdit", 104, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in added mode.")
                        End Select

                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = strMsg
                        'Gajanan [27-May-2019] -- Start              
                        'dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                        'dtRow.Item("objStatus") = 2
                        'Gajanan [27-May-2019] -- End

                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For

                    End If



                End If
                'Gajanan [17-DEC-2018] -- End

                intSkillTranUnkid = objESTran.GetSkillTranUnkid(intEmployeeUnkid, dtRow.Item("Skill").ToString.Trim)

                If intSkillTranUnkid > 0 Then
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "Employee Skill Already Exist.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                Else
                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'objESTran._Description = dtRow.Item("Description").ToString.Trim
                    'objESTran._Emp_App_Unkid = intEmployeeUnkid
                    'objESTran._Isapplicant = 0
                    'objESTran._Skillcategoryunkid = intSkillCategoryUnkid
                    'objESTran._Skillunkid = intSkillUnkid
                    'objESTran._Userunkid = User._Object._Userunkid
                    'If objESTran.Insert() Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success")
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objESTran._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If


                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    'If SkillApprovalFlowVal Is Nothing  Then
                    If SkillApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                        'Gajanan [22-Feb-2019] -- End


                        objASkillTran._Audittype = enAuditType.ADD
                        objASkillTran._Audituserunkid = User._Object._Userunkid
                        objASkillTran._Description = dtRow.Item("Description").ToString.Trim
                        objASkillTran._Emp_App_Unkid = intEmployeeUnkid
                        objASkillTran._Isapplicant = False
                        objASkillTran._Skillcategoryunkid = intSkillCategoryUnkid
                        objASkillTran._Skillunkid = intSkillUnkid
                        objASkillTran._Isvoid = False
                        objASkillTran._Tranguid = Guid.NewGuid.ToString()
                        objASkillTran._Transactiondate = Now
                        objASkillTran._Approvalremark = ""
                        objASkillTran._Isweb = False
                        objASkillTran._Isfinal = False
                        objASkillTran._Ip = getIP()
                        objASkillTran._Host = getHostName()
                        objASkillTran._Form_Name = mstrModuleName
                        objASkillTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objASkillTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    Else
                    objESTran._Description = dtRow.Item("Description").ToString.Trim
                    objESTran._Emp_App_Unkid = intEmployeeUnkid
                    objESTran._Isapplicant = 0
                    objESTran._Skillcategoryunkid = intSkillCategoryUnkid
                    objESTran._Skillunkid = intSkillUnkid
                    objESTran._Userunkid = User._Object._Userunkid
                    End If

                    Dim blnFlag As Boolean = False


                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'If SkillApprovalFlowVal Is Nothing Then

                    Dim strMsg As String = String.Empty

                    If SkillApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                        'Gajanan [22-Feb-2019] -- End
                        blnFlag = objASkillTran.Insert(Company._Object._Companyunkid)
                        If blnFlag = False AndAlso objASkillTran._Message <> "" Then

                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            'dtRow.Item("message") = objASkillTran._Message
                            strMsg = objASkillTran._Message
                            'Gajanan [17-April-2019] -- End


                        Else
                            'Gajanan [27-May-2019] -- Start
                            If EmpList.Contains(intEmployeeUnkid) = False Then
                                EmpList.Add(intEmployeeUnkid)
                            End If
                            'objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                            '                                     ConfigParameter._Object._UserAccessModeSetting, _
                            '                                     Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                            '                                     CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                            '                                     enScreenName.frmEmployee_Skill_List, ConfigParameter._Object._EmployeeAsOnDate, _
                            '                                     User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                            '                                     User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , intEmployeeUnkid, , , _
                            '                                     "employeeunkid= " & intEmployeeUnkid & " and skillunkid = " & intSkillUnkid, Nothing, , , _
                            '                                     "emp_app_unkid= " & intEmployeeUnkid & " and skillunkid = " & intSkillUnkid, Nothing)
                            'Gajanan [27-May-2019] -- End
                        End If
                    Else
                        blnFlag = objESTran.Insert()

                        If blnFlag = False AndAlso objESTran._Message <> "" Then strMsg = objESTran._Message 'dtRow.Item("message") = objESTran._Message
                    End If

                    If blnFlag Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success")
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError

                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'dtRow.Item("message") = objESTran._Message
                        dtRow.Item("message") = strMsg
                        'Gajanan [17-April-2019] -- End 

                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 6, "Fail")
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    'Gajanan [17-DEC-2018] -- End
                End If

                'Gajanan [27-May-2019] -- Start              
                Application.DoEvents()
                'Gajanan [27-May-2019] -- End

            Next
            'Gajanan [27-May-2019] -- Start
            If SkillApprovalFlowVal Is Nothing Then
                If mdt_ImportData_Others.Select("status <> '" & Language.getMessage(mstrModuleName, 6, "Fail") & "'").Count > 0 Then
                    If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                        Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                        mdt_ImportData_Others.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 6, "Fail") & "'"
                        objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                                   Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                   CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                                   enScreenName.frmEmployee_Skill_List, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                   User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                   User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                                   User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                                   ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData_Others.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
                    End If
                End If
            End If
            'Gajanan [27-May-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper Like "*" & strName.ToUpper & "*") Select (p)).FirstOrDefault

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End


                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END 

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizImportEmp.CancelText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_CancelText", Me.eZeeWizImportEmp.CancelText)
            Me.eZeeWizImportEmp.NextText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_NextText", Me.eZeeWizImportEmp.NextText)
            Me.eZeeWizImportEmp.BackText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_BackText", Me.eZeeWizImportEmp.BackText)
            Me.eZeeWizImportEmp.FinishText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_FinishText", Me.eZeeWizImportEmp.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblSkillCategory.Text = Language._Object.getCaption(Me.lblSkillCategory.Name, Me.lblSkillCategory.Text)
            Me.lblDesciption.Text = Language._Object.getCaption(Me.lblDesciption.Name, Me.lblDesciption.Text)
            Me.lblSkills.Text = Language._Object.getCaption(Me.lblSkills.Name, Me.lblSkills.Text)
            Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblLastName.Text = Language._Object.getCaption(Me.lblLastName.Name, Me.lblLastName.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Experience(s).")
            Language.setMessage(mstrModuleName, 4, "Skill Category cannot be blank. Please set Skill Category in order to import Employee Experience(s).")
            Language.setMessage(mstrModuleName, 5, "Skill cannot be blank. Please set Skill in order to import Employee Experience(s).")
            Language.setMessage(mstrModuleName, 6, "Fail")
            Language.setMessage(mstrModuleName, 7, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 8, "Employee Skill Already Exist.")
            Language.setMessage(mstrModuleName, 9, "Success")
            Language.setMessage(mstrModuleName, 10, "This field is already selected.Please Select New field.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class