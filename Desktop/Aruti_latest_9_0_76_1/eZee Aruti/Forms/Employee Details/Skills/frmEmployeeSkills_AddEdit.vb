﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeSkills_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeSkills_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objSkillTran As clsEmployee_Skill_Tran
    Private mintSkillTranUnkid As Integer = -1
    Private mintSelectedEmp_AppId As Integer = -1
    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 14 AUG 2012 ] -- END


    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objASkillTran As clsEmployeeSkill_Approval_Tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim SkillApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployee_Skill_List)))
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim isEmployeeApprove As Boolean = False
    'Gajanan [22-Feb-2019] -- End



    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End


    'Gajanan [21-June-2019] -- Start      
    Private OldData As clsEmployee_Skill_Tran
    'Gajanan [21-June-2019] -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = -1) As Boolean 'S.SANDEEP [ 14 AUG 2012 ] -- START -- END
        'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintSkillTranUnkid = intUnkId
            menAction = eAction

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintEmployeeUnkid = intEmployeeUnkid
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            Me.ShowDialog()

            intUnkId = mintSkillTranUnkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Variables "
    Private Sub SetColor()
        Try
            txtSkillDescription.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorComp
            cboSkill.BackColor = GUI.ColorComp
            cboSkillCategory.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'objSkillTran._Description = txtSkillDescription.Text
            'objSkillTran._Emp_App_Unkid = CInt(cboEmployee.SelectedValue)
            'If mintSkillTranUnkid = -1 Then '<TODO>
            '    objSkillTran._Isapplicant = False
            'Else
            '    objSkillTran._Isapplicant = objSkillTran._Isapplicant
            'End If
            'objSkillTran._Skillcategoryunkid = CInt(cboSkillCategory.SelectedValue)
            'objSkillTran._Skillunkid = CInt(cboSkill.SelectedValue)
            'objSkillTran._Userunkid = User._Object._Userunkid
            'If mintSkillTranUnkid = -1 Then
            '    objSkillTran._Voiddatetime = Nothing
            '    objSkillTran._Voidreason = ""
            '    objSkillTran._Voiduserunkid = -1
            'Else
            '    objSkillTran._Voiddatetime = objSkillTran._Voiddatetime
            '    objSkillTran._Voidreason = objSkillTran._Voidreason
            '    objSkillTran._Voiduserunkid = objSkillTran._Voiduserunkid
            'End If


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If SkillApprovalFlowVal Is Nothing Then
            If SkillApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                'Gajanan [22-Feb-2019] -- End

                objASkillTran._Audittype = enAuditType.ADD
                objASkillTran._Audituserunkid = User._Object._Userunkid
                objASkillTran._Description = txtSkillDescription.Text
                objASkillTran._Emp_App_Unkid = CInt(cboEmployee.SelectedValue)
                If mintSkillTranUnkid = -1 Then
                    objASkillTran._Isapplicant = False
                Else
                    objASkillTran._Isapplicant = objSkillTran._Isapplicant
                End If
                objASkillTran._Skillcategoryunkid = CInt(cboSkillCategory.SelectedValue)
                objASkillTran._Skillunkid = CInt(cboSkill.SelectedValue)

                objASkillTran._Isvoid = False
                objASkillTran._Tranguid = Guid.NewGuid.ToString()
                objASkillTran._Transactiondate = Now
                objASkillTran._Approvalremark = ""
                objASkillTran._Isweb = False
                objASkillTran._Isfinal = False
                objASkillTran._Ip = getIP()
                objASkillTran._Host = getHostName()
                objASkillTran._Form_Name = mstrModuleName
                objASkillTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                If menAction <> enAction.EDIT_ONE Then
                    objASkillTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    objASkillTran._Skillstranunkid = -1
                Else
                    objASkillTran._Skillstranunkid = mintSkillTranUnkid
                    objASkillTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.EDITED
                End If
            Else
            objSkillTran._Description = txtSkillDescription.Text
            objSkillTran._Emp_App_Unkid = CInt(cboEmployee.SelectedValue)
                If mintSkillTranUnkid = -1 Then
                objSkillTran._Isapplicant = False
            Else
                objSkillTran._Isapplicant = objSkillTran._Isapplicant
            End If
            objSkillTran._Skillcategoryunkid = CInt(cboSkillCategory.SelectedValue)
            objSkillTran._Skillunkid = CInt(cboSkill.SelectedValue)
            objSkillTran._Userunkid = User._Object._Userunkid
            If mintSkillTranUnkid = -1 Then
                objSkillTran._Voiddatetime = Nothing
                objSkillTran._Voidreason = ""
                objSkillTran._Voiduserunkid = -1
            Else
                objSkillTran._Voiddatetime = objSkillTran._Voiddatetime
                objSkillTran._Voidreason = objSkillTran._Voidreason
                objSkillTran._Voiduserunkid = objSkillTran._Voiduserunkid
            End If
            End If


            'Gajanan [17-DEC-2018] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtSkillDescription.Text = objSkillTran._Description
            cboEmployee.SelectedValue = objSkillTran._Emp_App_Unkid
            cboSkill.SelectedValue = objSkillTran._Skillunkid
            cboSkillCategory.SelectedValue = objSkillTran._Skillcategoryunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master
        Try
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "Category")
            With cboSkillCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Category")
            End With


            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , , False)
            'End If

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployee_Skill_List))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                           mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'S.SANDEEP [20-JUN-2018] -- End


            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos = Nothing
            objCommon = Nothing
            objEmp = Nothing
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            'Gajanan [21-June-2019] -- Start      
            If IsNothing(OldData) = False Then
                If OldData._Skillcategoryunkid = CInt(cboSkillCategory.SelectedValue) AndAlso _
                   OldData._Skillunkid = CInt(cboSkill.SelectedValue) AndAlso _
                   OldData._Description = txtSkillDescription.Text Then

                    If menAction = enAction.EDIT_ONE Then
                        objSkillTran = Nothing
                        OldData = Nothing
                        Me.Close()
                        Return False
                    End If
                Else
                    OldData = Nothing
                End If
            End If
            'Gajanan [21-June-2019] -- End


            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboSkillCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Skill Category is compulsory information. Please select Skill Category to continue."), enMsgBoxStyle.Information)
                cboSkillCategory.Focus()
                Return False
            End If

            If CInt(cboSkill.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Skill is compulsory information. Please select Skill to continue."), enMsgBoxStyle.Information)
                cboSkill.Focus()
                Return False
            End If

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'If SkillApprovalFlowVal Is Nothing Then
            If SkillApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                If objApprovalData.IsApproverPresent(enScreenName.frmEmployee_Skill_List, FinancialYear._Object._DatabaseName, _
                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    Return False
                End If
                'Gajanan [17-April-2019] -- End



                'Gajanan [10-June-2019] -- Start      

                'If objSkillTran.isExist(CInt(cboEmployee.SelectedValue), CInt(cboSkill.SelectedValue), False, CInt(IIf(menAction <> enAction.EDIT_ONE, mintSkillTranUnkid, 0))) Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Selected skill is already assigned to the employee. Please assign new skill."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                If objSkillTran.isExist(CInt(cboEmployee.SelectedValue), CInt(cboSkill.SelectedValue), False, mintSkillTranUnkid) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Selected skill is already assigned to the employee. Please assign new skill."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Gajanan [10-June-2019] -- End

                Dim intOperationType As Integer = 0
                If objASkillTran.isExist(CInt(cboEmployee.SelectedValue), False, Nothing, CInt(cboSkill.SelectedValue), "", Nothing, True, intOperationType) Then
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If menAction = enAction.EDIT_ONE Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in edit mode."), enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in edit mode."), enMsgBoxStyle.Information)
                                End If
                                Return False

                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If menAction = enAction.EDIT_ONE Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in deleted mode."), enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in deleted mode."), enMsgBoxStyle.Information)
                                End If
                                Return False

                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If menAction = enAction.EDIT_ONE Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in added mode."), enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 105, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in added mode."), enMsgBoxStyle.Information)
                                End If
                                Return False

                        End Select
                    End If
                End If


            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function


    Private Sub SetVisibility()

        Try
            objbtnAddSkill.Enabled = User._Object.Privilege._AddSkills
            objbtnAddCategory.Enabled = User._Object.Privilege._AddCommonMasters
            'Gajanan [17-DEC-2018] -- Start
            If mintSkillTranUnkid > 0 Then
                cboEmployee.Enabled = False
            Else
                cboEmployee.Enabled = True
            End If
            'Gajanan [17-DEC-2018] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmEmployeeSkills_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objSkillTran = Nothing
    End Sub

    Private Sub frmEmployeeSkills_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSaveInfo.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeSkills_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeSkills_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSkillTran = New clsEmployee_Skill_Tran

        'Gajanan [9-April-2019] -- Start
        objApprovalData = New clsEmployeeDataApproval
        'Gajanan [9-April-2019] -- End


        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objASkillTran = New clsEmployeeSkill_Approval_Tran
        'Gajanan [17-DEC-2018] -- End
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()
            'Gajanan [17-DEC-2018] -- Start
            SetVisibility()
            'Gajanan [17-DEC-2018] -- End


            If menAction = enAction.EDIT_ONE Then
                objSkillTran._Skillstranunkid = mintSkillTranUnkid
                'Gajanan [21-June-2019] -- Start      
                OldData = objSkillTran
                'Gajanan [21-June-2019] -- End
            End If

            Call GetValue()

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            cboEmployee.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAssets_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Skill_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Skill_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnSaveInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Call SetValue()
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.


            If menAction = enAction.EDIT_ONE Then
'blnFlag = objSkillTran.Update
                If SkillApprovalFlowVal Is Nothing AndAlso mintSkillTranUnkid > 0 AndAlso isEmployeeApprove = True Then

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    If objSkillTran.isExist(CInt(cboEmployee.SelectedValue), CInt(cboSkill.SelectedValue), False, mintSkillTranUnkid, Nothing) = False Then
                        'Gajanan [17-April-2019] -- End

                    blnFlag = objASkillTran.Insert(Company._Object._Companyunkid)
                    If blnFlag = False AndAlso objASkillTran._Message <> "" Then
                        eZeeMsgBox.Show(objASkillTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.



                        Else

                            If blnFlag <> False Then
                                objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                                 enScreenName.frmEmployee_Skill_List, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                 User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                 User._Object._Username, clsEmployeeDataApproval.enOperationType.EDITED, , cboEmployee.SelectedValue.ToString(), , , _
                                                                 "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and skillunkid = " & cboSkill.SelectedValue.ToString(), Nothing, , , _
                                                                 "emp_app_unkid= " & cboEmployee.SelectedValue.ToString() & " and skillunkid = " & cboSkill.SelectedValue.ToString(), Nothing)
                            End If
                            'Gajanan [17-April-2019] -- End
                        End If
                    End If
                Else
                    blnFlag = objSkillTran.Update()
                End If
'Gajanan [17-DEC-2018] -- End
            Else

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'blnFlag = objSkillTran.Insert

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If SkillApprovalFlowVal Is Nothing AndAlso mintSkillTranUnkid <= 0 AndAlso isEmployeeApprove = True Then
                If SkillApprovalFlowVal Is Nothing AndAlso mintSkillTranUnkid <= 0 AndAlso isEmployeeApprove = True Then

                    If objSkillTran.isExist(CInt(cboEmployee.SelectedValue), CInt(cboSkill.SelectedValue), False, mintSkillTranUnkid, Nothing) = False Then

                    End If


                    'Gajanan [22-Feb-2019] -- End
                    blnFlag = objASkillTran.Insert(Company._Object._Companyunkid)
                    If blnFlag = False AndAlso objASkillTran._Message <> "" Then
                        eZeeMsgBox.Show(objASkillTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.



                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                         ConfigParameter._Object._UserAccessModeSetting, _
                                                         Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                         CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                         enScreenName.frmEmployee_Skill_List, ConfigParameter._Object._EmployeeAsOnDate, _
                                                         User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                         User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , cboEmployee.SelectedValue.ToString(), , , _
                                                         "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and skillunkid = " & cboSkill.SelectedValue.ToString(), Nothing, , , _
                                                         "emp_app_unkid= " & cboEmployee.SelectedValue.ToString() & " and skillunkid = " & cboSkill.SelectedValue.ToString(), Nothing)
                        'Gajanan [17-April-2019] -- End
                    End If
            Else
                    blnFlag = objSkillTran.Insert()
                End If
                'Gajanan [17-DEC-2018] -- End
            End If
            If blnFlag = False And objSkillTran._Message <> "" Then
                eZeeMsgBox.Show(objSkillTran._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objSkillTran = Nothing
                    objSkillTran = New clsEmployee_Skill_Tran
                    Call GetValue()
                    cboEmployee.Focus()
                Else
                    mintSkillTranUnkid = objSkillTran._Skillstranunkid
                    Me.Close()
                End If
            End If

            If mintSelectedEmp_AppId <> -1 Then
                cboEmployee.SelectedValue = mintSelectedEmp_AppId
            End If


            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub cboSkillCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSkillCategory.SelectedIndexChanged
        Dim objSkills As New clsskill_master
        Dim dsList As New DataSet
        Try
            If CInt(cboSkillCategory.SelectedValue) > 0 Then
                dsList = objSkills.getComboList("Skills", True, CInt(cboSkillCategory.SelectedValue))
                With cboSkill
                    .ValueMember = "skillunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Skills")
                    'Sandeep [ 14 Aug 2010 ] -- Start
                    '.SelectedValue = 0
                    If mintSkillTranUnkid = -1 Then
                        .SelectedValue = 0
                    Else
                        .SelectedValue = objSkillTran._Skillunkid
                    End If
                    'Sandeep [ 14 Aug 2010 ] -- End 
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSkillCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            mintSelectedEmp_AppId = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(cboEmployee.SelectedValue) & "'")
            If itmp.Length > 0 Then
                isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
            Else
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'isEmployeeApprove = True
                isEmployeeApprove = False
                'Gajanan [17-April-2019] -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Gajanan [22-Feb-2019] -- End

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objfrm
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
            End With
            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub objbtnAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCategory.Click
        Try
            Dim dsCombos As New DataSet
            Dim mintCategoryId As Integer = -1
            Dim objComm As New clsCommon_Master
            Dim objfrmCommonMaster As New frmCommonMaster

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmCommonMaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCommonMaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCommonMaster)
            End If
            'Anjan (02 Sep 2011)-End 


            'Sandeep [ 14 Aug 2010 ] -- Start
            'objfrmCommonMaster.displayDialog(mintCategoryId, clsCommon_Master.enCommonMaster.SKILL_CATEGORY, enAction.ADD_ONE)
            'dsCombos = objComm.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "SkillCategory")
            'With cboSkillCategory
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Asset")
            '    .SelectedValue = mintCategoryId
            'End With
            objfrmCommonMaster.displayDialog(mintCategoryId, clsCommon_Master.enCommonMaster.SKILL_CATEGORY, enAction.ADD_ONE)
            If mintCategoryId <> -1 Then
                dsCombos = objComm.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "SkillCategory")
                With cboSkillCategory
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("SkillCategory")
                    .SelectedValue = mintCategoryId
                End With
            End If
            'Sandeep [ 14 Aug 2010 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub objbtnAddSkill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkill.Click
        Dim dsCombos As New DataSet
        Dim mintSkillId As Integer = -1
        Dim mintCategoryId As Integer = -1
        Dim objSkillMaster As New clsskill_master
        Dim frm As New frmSkill_AddEdit
        Dim objComm As New clsCommon_Master
        Dim objfrmCommonMaster As New frmCommonMaster
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(mintSkillId, enAction.ADD_ONE) Then
                dsCombos = objSkillMaster.getComboList("Skills", True, CInt(cboSkillCategory.SelectedValue))
                With cboSkill
                    .ValueMember = "skillunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Skills")
                    .SelectedValue = mintSkillId
                End With
            End If

            dsCombos = objComm.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "SkillCategory")
            With cboSkillCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("SkillCategory")
                .SelectedValue = mintCategoryId
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSkill_Click", mstrModuleName)
        End Try
    End Sub
    
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSkillRegister.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSkillRegister.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveInfo.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveInfo.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSkillRegister.Text = Language._Object.getCaption(Me.gbSkillRegister.Name, Me.gbSkillRegister.Text)
			Me.lblSkillcategory.Text = Language._Object.getCaption(Me.lblSkillcategory.Name, Me.lblSkillcategory.Text)
			Me.lblSkillDescription.Text = Language._Object.getCaption(Me.lblSkillDescription.Name, Me.lblSkillDescription.Text)
			Me.lblSkill.Text = Language._Object.getCaption(Me.lblSkill.Name, Me.lblSkill.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSaveInfo.Text = Language._Object.getCaption(Me.btnSaveInfo.Name, Me.btnSaveInfo.Text)
			Me.lnSkillInfo.Text = Language._Object.getCaption(Me.lnSkillInfo.Name, Me.lnSkillInfo.Text)
			Me.lnEmployeeName.Text = Language._Object.getCaption(Me.lnEmployeeName.Name, Me.lnEmployeeName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Skill Category is compulsory information. Please select Skill Category to continue.")
			Language.setMessage(mstrModuleName, 3, "Skill is compulsory information. Please select Skill to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class