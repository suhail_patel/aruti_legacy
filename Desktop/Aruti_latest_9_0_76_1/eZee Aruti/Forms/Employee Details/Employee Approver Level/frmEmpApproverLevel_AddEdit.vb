﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 2

Public Class frmEmpApproverLevel_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmEmpApproverLevel_AddEdit"
    Private mblnCancel As Boolean = True
    Private objApproverLevel As clsempapproverlevel_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintEmpApproverLevelUnkid As Integer = -1
    Private eApproverType As enEmpApproverType

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal eApprType As enEmpApproverType) As Boolean
        Try
            mintEmpApproverLevelUnkid = intUnkId
            menAction = eAction
            eApproverType = eApprType

            Me.ShowDialog()

            intUnkId = mintEmpApproverLevelUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Form's Event"

    Private Sub frmEmpApproverLevel_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objApproverLevel = New clsempapproverlevel_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objApproverLevel._EmpAppLevelunkid = mintEmpApproverLevelUnkid
            End If
            GetValue()
            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpApproverLevel_AddEdit_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmpApproverLevel_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpApproverLevel_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpApproverLevel_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpApproverLevel_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpApproverLevel_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objApproverLevel = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsempapproverlevel_master.SetMessages()
            objfrm._Other_ModuleNames = "clsempapproverlevel_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Approver Level Code cannot be blank. Approver Level Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Approver Level Name cannot be blank. Approver Level Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objApproverLevel.Update()
            Else
                blnFlag = objApproverLevel.Insert()
            End If

            If blnFlag = False And objApproverLevel._Message <> "" Then
                eZeeMsgBox.Show(objApproverLevel._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objApproverLevel = Nothing
                    objApproverLevel = New clsempapproverlevel_master
                    Call GetValue()
                    txtCode.Focus()
                Else
                    mintEmpApproverLevelUnkid = objApproverLevel._EmpAppLevelunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtName.Text, objApproverLevel._EmpAppLevelname1, objApproverLevel._EmpAppLevelname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            nudPriority.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objApproverLevel._EmpAppLevelcode
            txtName.Text = objApproverLevel._EmpAppLevelname
            nudPriority.Value = objApproverLevel._Priority
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objApproverLevel._EmpAppLevelcode = txtCode.Text.Trim
            objApproverLevel._EmpAppLevelname = txtName.Text.Trim
            objApproverLevel._Priority = CInt(nudPriority.Value)
            objApproverLevel._FormName = mstrModuleName
            objApproverLevel._ClientIP = getIP()
            objApproverLevel._HostName = getHostName()
            objApproverLevel._IsFromWeb = False
            objApproverLevel._AuditUserId = User._Object._Userunkid
            objApproverLevel._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objApproverLevel._Approvertypeid = eApproverType
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbApproverLevelInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApproverLevelInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbApproverLevelInfo.Text = Language._Object.getCaption(Me.gbApproverLevelInfo.Name, Me.gbApproverLevelInfo.Text)
            Me.lblLevelName.Text = Language._Object.getCaption(Me.lblLevelName.Name, Me.lblLevelName.Text)
            Me.lblLevelCode.Text = Language._Object.getCaption(Me.lblLevelCode.Name, Me.lblLevelCode.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblPriority.Text = Language._Object.getCaption(Me.lblPriority.Name, Me.lblPriority.Text)
            Me.lblPRemark.Text = Language._Object.getCaption(Me.lblPRemark.Name, Me.lblPRemark.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Approver Level Code cannot be blank. Approver Level Code is required information.")
            Language.setMessage(mstrModuleName, 2, "Approver Level Name cannot be blank. Approver Level Name is required information.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class