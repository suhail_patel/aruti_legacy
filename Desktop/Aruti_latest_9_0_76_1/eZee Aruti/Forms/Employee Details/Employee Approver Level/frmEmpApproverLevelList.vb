﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmEmpApproverLevelList

#Region "Private Variable"

    Private objApproverLevel As clsempapproverlevel_master
    Private ReadOnly mstrModuleName As String = "frmEmpApproverLevelList"
    Private eApproverType As enEmpApproverType

#End Region

#Region " Property "

    Public WriteOnly Property _ApproverType() As enEmpApproverType
        Set(ByVal value As enEmpApproverType)
            eApproverType = value
        End Set
    End Property

#End Region

#Region "Form's Event"

    Private Sub frmEmpApproverLevelList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApproverLevel = New clsempapproverlevel_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            fillList()

            If lvApproverLevelList.Items.Count > 0 Then lvApproverLevelList.Items(0).Selected = True
            lvApproverLevelList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpApproverLevelList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpApproverLevelList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpApproverLevelList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpApproverLevelList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Close()
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsempapproverlevel_master.SetMessages()
            objfrm._Other_ModuleNames = "clsempapproverlevel_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objApproverLevel_AddEdit As New frmEmpApproverLevel_AddEdit
            If objApproverLevel_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, eApproverType) Then
                Call fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvApproverLevelList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver Level from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverLevelList.Select()
            Exit Sub
        End If
        Dim objfrmLeaveApproverLevel_AddEdit As New frmEmpApproverLevel_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverLevelList.SelectedItems(0).Index
            If objfrmLeaveApproverLevel_AddEdit.displayDialog(CInt(lvApproverLevelList.SelectedItems(0).Tag), enAction.EDIT_ONE, eApproverType) Then
                Call fillList()
            End If
            objfrmLeaveApproverLevel_AddEdit = Nothing

            lvApproverLevelList.Items(intSelectedIndex).Selected = True
            lvApproverLevelList.EnsureVisible(intSelectedIndex)
            lvApproverLevelList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmLeaveApproverLevel_AddEdit IsNot Nothing Then objfrmLeaveApproverLevel_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvApproverLevelList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver Level from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverLevelList.Select()
            Exit Sub
        End If
        If objApproverLevel.isUsed(CInt(lvApproverLevelList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use."), enMsgBoxStyle.Information) '?2
            lvApproverLevelList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverLevelList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver Level?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objApproverLevel._FormName = mstrModuleName
                objApproverLevel._ClientIP = getIP()
                objApproverLevel._HostName = getHostName()
                objApproverLevel._IsFromWeb = False
                objApproverLevel._AuditUserId = User._Object._Userunkid
                objApproverLevel._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime

                objApproverLevel.Delete(CInt(lvApproverLevelList.SelectedItems(0).Tag))
                lvApproverLevelList.SelectedItems(0).Remove()

                If lvApproverLevelList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvApproverLevelList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvApproverLevelList.Items.Count - 1
                    lvApproverLevelList.Items(intSelectedIndex).Selected = True
                    lvApproverLevelList.EnsureVisible(intSelectedIndex)
                ElseIf lvApproverLevelList.Items.Count <> 0 Then
                    lvApproverLevelList.Items(intSelectedIndex).Selected = True
                    lvApproverLevelList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvApproverLevelList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsApproverLevelList As New DataSet
        Try

            If User._Object.Privilege._AllowToViewEmpApproverLevelList = True Then

                dsApproverLevelList = objApproverLevel.GetList(eApproverType, "List")

                Dim lvItem As ListViewItem

                lvApproverLevelList.Items.Clear()
                For Each drRow As DataRow In dsApproverLevelList.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("empapplevelcode").ToString
                    lvItem.Tag = drRow("empapplevelunkid")
                    lvItem.SubItems.Add(drRow("empapplevelName").ToString)
                    lvItem.SubItems.Add(drRow("priority").ToString)
                    lvApproverLevelList.Items.Add(lvItem)
                Next

                If lvApproverLevelList.Items.Count > 16 Then
                    colhEmpApprovePriority.Width = 100 - 18
                Else
                    colhEmpApprovePriority.Width = 100
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsApproverLevelList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddEmpApproverLevel
            btnEdit.Enabled = User._Object.Privilege._AllowToEditEmpApproverLevel
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteEmpApproverLevel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhEmpApproveLevelCode.Text = Language._Object.getCaption(CStr(Me.colhEmpApproveLevelCode.Tag), Me.colhEmpApproveLevelCode.Text)
            Me.colhEmpApproveLevelName.Text = Language._Object.getCaption(CStr(Me.colhEmpApproveLevelName.Tag), Me.colhEmpApproveLevelName.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Approver Level from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver Level?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class