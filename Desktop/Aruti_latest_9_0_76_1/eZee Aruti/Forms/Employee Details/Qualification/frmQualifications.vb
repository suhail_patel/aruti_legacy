﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO

Public Class frmQualifications

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmQualifications"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objQualification As clsEmp_Qualification_Tran
    Private mintQualificationUnkid As Integer = -1
    Private mintSeletedEmpId As Integer = -1

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 14 AUG 2012 ] -- END


    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END


    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private mdtQualificationDocument As DataTable
    Private objDocument As clsScan_Attach_Documents

    'SHANI (20 JUN 2015) -- End 
    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Private mstrFolderName As String = ""
    'SHANI (16 JUL 2015) -- End 

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objAQualificationTran As clsEmp_Qualification_Approval_Tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim QualificationApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmQualificationsList)))
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim isEmployeeApprove As Boolean = False
    'Gajanan [22-Feb-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End



    'Gajanan [21-June-2019] -- Start      
    Private OldData As clsEmp_Qualification_Tran
    'Gajanan [21-June-2019] -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = -1) As Boolean
        'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean 'S.SANDEEP [ 14 AUG 2012 ] -- START -- END
        Try
            mintQualificationUnkid = intUnkId
            menAction = eAction

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintEmployeeUnkid = intEmployeeUnkid
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            Me.ShowDialog()

            intUnkId = mintQualificationUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboCertificateGroup.BackColor = GUI.ColorComp
            cboCertificates.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            txtAwardNo.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            cboInstitution.BackColor = GUI.ColorComp


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            cboResultCode.BackColor = GUI.ColorComp
            nudLevel.BackColor = GUI.ColorOptional
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            txtOtherQualificationGrp.BackColor = GUI.ColorComp
            txtOtherQualification.BackColor = GUI.ColorComp
            txtOtherResultCode.BackColor = GUI.ColorComp
            'Pinkal (25-APR-2012) -- End

            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            txtOtherInstitute.BackColor = GUI.ColorComp
            'S.SANDEEP [23 JUL 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'If dtpEndDate.Checked = True Then
            '    objQualification._Award_End_Date = dtpEndDate.Value
            'Else
            '    objQualification._Award_End_Date = Nothing
            'End If

            'If dtpStartDate.Checked = True Then
            '    objQualification._Award_Start_Date = dtpStartDate.Value
            'Else
            '    objQualification._Award_Start_Date = Nothing
            'End If

            'objQualification._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objQualification._Qualificationgroupunkid = CInt(cboCertificateGroup.SelectedValue)
            'objQualification._Qualificationunkid = CInt(cboCertificates.SelectedValue)
            'objQualification._Reference_No = txtAwardNo.Text
            'objQualification._Remark = txtRemark.Text
            'objQualification._Instituteunkid = CInt(cboInstitution.SelectedValue)
            'objQualification._Transaction_Date = dtpCertificateDate.Value

            'If mintQualificationUnkid = -1 Then
            '    objQualification._Userunkid = User._Object._Userunkid
            '    objQualification._Isvoid = False
            '    objQualification._Voiddatetime = Nothing
            '    objQualification._Voiduserunkid = -1
            'Else
            '    objQualification._Userunkid = objQualification._Userunkid
            '    objQualification._Isvoid = objQualification._Isvoid
            '    objQualification._Voiddatetime = objQualification._Voiddatetime
            '    objQualification._Voiduserunkid = objQualification._Voiduserunkid
            'End If


            ''Pinkal (12-Oct-2011) -- Start
            ''Enhancement : TRA Changes
            'objQualification._Resultunkid = CInt(cboResultCode.SelectedValue)
            'objQualification._GPAcode = CDec(IIf(CStr(nudLevel.Value) = "", 0, nudLevel.Value))
            ''Pinkal (12-Oct-2011) -- End


            ''Pinkal (25-APR-2012) -- Start
            ''Enhancement : TRA Changes
            'If pnlOtherQualification.Visible Then
            '    objQualification._Other_QualificationGrp = txtOtherQualificationGrp.Text.Trim
            '    objQualification._Other_Qualification = txtOtherQualification.Text.Trim
            '    objQualification._other_ResultCode = txtOtherResultCode.Text.Trim
            '    objQualification._Qualificationgroupunkid = 0
            '    objQualification._Qualificationunkid = 0
            '    objQualification._Resultunkid = 0
            '    'S.SANDEEP [23 JUL 2016] -- START
            '    'CCK- OTHER QUALIFICATION CHANGE
            '    objQualification._Other_institute = txtOtherInstitute.Text.Trim
            '    'S.SANDEEP [23 JUL 2016] -- END
            'Else
            '    objQualification._Other_QualificationGrp = ""
            '    objQualification._Other_Qualification = ""
            '    objQualification._other_ResultCode = ""
            '    objQualification._Other_institute = ""
            'End If

            ''Pinkal (25-APR-2012) -- End


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If QualificationApprovalFlowVal Is Nothing Then
            If QualificationApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                'Gajanan [22-Feb-2019] -- End

                objAQualificationTran._Audittype = enAuditType.ADD
                objAQualificationTran._Audituserunkid = User._Object._Userunkid

                If dtpEndDate.Checked = True Then
                    objAQualificationTran._Award_End_Date = dtpEndDate.Value
                Else
                    objAQualificationTran._Award_End_Date = Nothing
                End If

                If dtpStartDate.Checked = True Then
                    objAQualificationTran._Award_Start_Date = dtpStartDate.Value
                Else
                    objAQualificationTran._Award_Start_Date = Nothing
                End If

                objAQualificationTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objAQualificationTran._Qualificationgroupunkid = CInt(cboCertificateGroup.SelectedValue)
                objAQualificationTran._Qualificationunkid = CInt(cboCertificates.SelectedValue)
                objAQualificationTran._Reference_No = txtAwardNo.Text
                objAQualificationTran._Remark = txtRemark.Text
                objAQualificationTran._Instituteunkid = CInt(cboInstitution.SelectedValue)
                objAQualificationTran._Certificatedate = dtpCertificateDate.Value


                objAQualificationTran._Isvoid = False
                objAQualificationTran._Tranguid = Guid.NewGuid.ToString()
                objAQualificationTran._Transactiondate = Now
                objAQualificationTran._Approvalremark = ""
                objAQualificationTran._Isweb = False
                objAQualificationTran._Isfinal = False
                objAQualificationTran._Ip = getIP()
                objAQualificationTran._Host = getHostName()
                objAQualificationTran._Form_Name = mstrModuleName
                objAQualificationTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

                objAQualificationTran._Resultunkid = CInt(cboResultCode.SelectedValue)
                objAQualificationTran._Gpacode = CDec(IIf(CStr(nudLevel.Value) = "", 0, nudLevel.Value))

                If pnlOtherQualification.Visible Then
                    objAQualificationTran._Other_Qualificationgrp = txtOtherQualificationGrp.Text.Trim
                    objAQualificationTran._Other_Qualification = txtOtherQualification.Text.Trim
                    objAQualificationTran._Other_Resultcode = txtOtherResultCode.Text.Trim
                    objAQualificationTran._Qualificationgroupunkid = 0
                    objAQualificationTran._Qualificationunkid = 0
                    objAQualificationTran._Resultunkid = 0
                    objAQualificationTran._Other_Institute = txtOtherInstitute.Text.Trim
                Else
                    objAQualificationTran._Other_Qualificationgrp = ""
                    objAQualificationTran._Other_Qualification = ""
                    objAQualificationTran._Other_Resultcode = ""
                    objAQualificationTran._Other_Institute = ""
                End If
                If menAction <> enAction.EDIT_ONE Then
                    objAQualificationTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    objAQualificationTran._Qualificationtranunkid = -1
                Else
                    objAQualificationTran._Qualificationtranunkid = mintQualificationUnkid
                    objAQualificationTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.EDITED
                End If
            Else
            If dtpEndDate.Checked = True Then
                objQualification._Award_End_Date = dtpEndDate.Value
            Else
                objQualification._Award_End_Date = Nothing
            End If

            If dtpStartDate.Checked = True Then
                objQualification._Award_Start_Date = dtpStartDate.Value
            Else
                objQualification._Award_Start_Date = Nothing
            End If

            objQualification._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objQualification._Qualificationgroupunkid = CInt(cboCertificateGroup.SelectedValue)
            objQualification._Qualificationunkid = CInt(cboCertificates.SelectedValue)
            objQualification._Reference_No = txtAwardNo.Text
            objQualification._Remark = txtRemark.Text
            objQualification._Instituteunkid = CInt(cboInstitution.SelectedValue)
            objQualification._Transaction_Date = dtpCertificateDate.Value

            If mintQualificationUnkid = -1 Then
                objQualification._Userunkid = User._Object._Userunkid
                objQualification._Isvoid = False
                objQualification._Voiddatetime = Nothing
                objQualification._Voiduserunkid = -1
            Else
                    objQualification._Userunkid = User._Object._Userunkid
                objQualification._Isvoid = objQualification._Isvoid
                objQualification._Voiddatetime = objQualification._Voiddatetime
                objQualification._Voiduserunkid = objQualification._Voiduserunkid
            End If


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            objQualification._Resultunkid = CInt(cboResultCode.SelectedValue)
            objQualification._GPAcode = CDec(IIf(CStr(nudLevel.Value) = "", 0, nudLevel.Value))
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            If pnlOtherQualification.Visible Then
                objQualification._Other_QualificationGrp = txtOtherQualificationGrp.Text.Trim
                objQualification._Other_Qualification = txtOtherQualification.Text.Trim
                objQualification._other_ResultCode = txtOtherResultCode.Text.Trim
                objQualification._Qualificationgroupunkid = 0
                objQualification._Qualificationunkid = 0
                objQualification._Resultunkid = 0
                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                objQualification._Other_institute = txtOtherInstitute.Text.Trim
                'S.SANDEEP [23 JUL 2016] -- END
            Else
                objQualification._Other_QualificationGrp = ""
                objQualification._Other_Qualification = ""
                objQualification._other_ResultCode = ""
                objQualification._Other_institute = ""
            End If

            'Pinkal (25-APR-2012) -- End
            End If



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
        cboEmployee.SelectedValue = objQualification._Employeeunkid
        cboCertificateGroup.SelectedValue = objQualification._Qualificationgroupunkid
        cboCertificates.SelectedValue = objQualification._Qualificationunkid
        txtAwardNo.Text = objQualification._Reference_No

        If objQualification._Award_Start_Date = Nothing Then
            dtpStartDate.Checked = False
        Else
            dtpStartDate.Value = objQualification._Award_Start_Date
            dtpStartDate.Checked = True
        End If

        If objQualification._Award_End_Date = Nothing Then
            dtpEndDate.Checked = False
        Else
            dtpEndDate.Value = objQualification._Award_End_Date
            dtpEndDate.Checked = True
        End If

        cboInstitution.SelectedValue = objQualification._Instituteunkid
        If objQualification._Transaction_Date = Nothing Then
            dtpCertificateDate.Value = Now
        Else
            dtpCertificateDate.Value = objQualification._Transaction_Date
        End If
            txtRemark.Text = objQualification._Remark


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            cboResultCode.SelectedValue = objQualification._Resultunkid
            nudLevel.Value = objQualification._GPAcode
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            If menAction = enAction.EDIT_ONE Then lnkOtherQualification.Enabled = False

            If objQualification._Other_QualificationGrp.Trim.Length <= 0 Then
                pnlOtherQualification.Visible = False
            Else
                pnlOtherQualification.Visible = True
                txtOtherInstitute.Visible = pnlOtherQualification.Visible
                cboInstitution.Visible = Not pnlOtherQualification.Visible
                objbtnAddInstitution.Visible = Not pnlOtherQualification.Visible
                objbtnSearchProvider.Visible = Not pnlOtherQualification.Visible
            End If

            txtOtherQualificationGrp.Text = objQualification._Other_QualificationGrp.Trim
            txtOtherQualification.Text = objQualification._Other_Qualification.Trim
            txtOtherResultCode.Text = objQualification._other_ResultCode.Trim
            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            txtOtherInstitute.Text = objQualification._Other_institute
            'S.SANDEEP [23 JUL 2016] -- END

            'Pinkal (25-APR-2012) -- End

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'passing Qualification tranunkid for the Other Qualification , reason the attachment is not coming for Other Qualification. 
            'mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, CInt(IIf(pnlOtherQualification.Visible = True, -1, cboCertificates.SelectedValue)))

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, mintQualificationUnkid)
            mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, mintQualificationUnkid, ConfigParameter._Object._Document_Path)

            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            If menAction <> enAction.EDIT_ONE Then
                If mdtQualificationDocument IsNot Nothing Then mdtQualificationDocument.Rows.Clear()
            End If
            'S.SANDEEP [23 JUL 2016] -- END

            'Shani(24-Aug-2015) -- End

            'Shani(24-Aug-2015) -- End
            'SHANI (20 JUN 2015) -- End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master

        'Sandeep [ 09 Oct 2010 ] -- Start
        Dim objInstitute As New clsinstitute_master
        'Sandeep [ 09 Oct 2010 ] -- End 

        'SHANI (20 JUN 2015) -- Start
        'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
        Dim objCMaster As New clsCommon_Master
        'SHANI (20 JUN 2015) -- End 

        Try
            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , , False)
            'End If


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmQualificationsList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            'S.SANDEEP [14-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                           mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", _
            '                           True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'Gajanan [12-JAN-2019] -- Start
            'dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                           mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", _
            '                           mblnOnlyApproved, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                       mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", _
                                       True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)
            'Gajanan [12-JAN-2019] -- End
            'S.SANDEEP [20-JUN-2018] -- End

            'S.SANDEEP [14-AUG-2018] -- END
            


            'S.SANDEEP [20-JUN-2018] -- End


            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- END

            
            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QGroup")
            With cboCertificateGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("QGroup")
                .SelectedValue = 0
            End With

            'Sandeep [ 09 Oct 2010 ] -- Start
            dsCombos = objInstitute.getListForCombo(False, "List", True)
            With cboInstitution
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'Sandeep [ 09 Oct 2010 ] -- End 

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            'SHANI (20 JUN 2015) -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

                'Gajanan [21-June-2019] -- Start
    'Private Function IsValid() As Boolean
    Private Function IsValid(Optional ByVal IsFromAttachment As Boolean = False) As Boolean
                'Gajanan [21-June-2019] -- End        
        Try

            'Gajanan [21-June-2019] -- Start      



            Dim blnIsChange As Boolean = True


            If IsFromAttachment = False AndAlso IsNothing(OldData) = False Then

                If OldData._Award_End_Date = dtpEndDate.Value AndAlso _
                   OldData._Award_Start_Date = dtpStartDate.Value AndAlso _
                   OldData._Qualificationgroupunkid = CInt(cboCertificateGroup.SelectedValue) AndAlso _
                   OldData._Qualificationunkid = CInt(cboCertificates.SelectedValue) AndAlso _
                   OldData._Reference_No = txtAwardNo.Text AndAlso _
                   OldData._Remark = txtRemark.Text AndAlso _
                   OldData._Instituteunkid = CInt(cboInstitution.SelectedValue) AndAlso _
                   OldData._Transaction_Date = dtpCertificateDate.Value AndAlso _
                   OldData._Resultunkid = CInt(cboResultCode.SelectedValue) AndAlso _
                   OldData._GPAcode = CDec(IIf(CStr(nudLevel.Value) = "", 0, nudLevel.Value)) Then

                    blnIsChange = False
                End If



                If pnlOtherQualification.Visible Then


                    If OldData._Other_QualificationGrp = txtOtherQualificationGrp.Text.Trim AndAlso _
                        OldData._Other_Qualification = txtOtherQualification.Text.Trim AndAlso _
                        OldData._other_ResultCode = txtOtherResultCode.Text.Trim AndAlso _
                        OldData._Qualificationgroupunkid = 0 AndAlso _
                        OldData._Qualificationunkid = 0 AndAlso _
                        OldData._Resultunkid = 0 AndAlso _
                        OldData._Other_institute = txtOtherInstitute.Text.Trim Then

                        blnIsChange = False

                    End If
                End If
            End If
    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience
            If ConfigParameter._Object._QualificationCertificateAttachmentMandatory AndAlso IsFromAttachment = False Then
                If mdtQualificationDocument Is Nothing OrElse mdtQualificationDocument.Select("AUD <> 'D'").Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), enMsgBoxStyle.Information)
                    Exit Function
                End If
            End If
    'Gajanan [5-Dec-2019] -- End

            If IsFromAttachment = False AndAlso blnIsChange = False AndAlso IsNothing(mdtQualificationDocument) = False AndAlso CInt(mdtQualificationDocument.Select("AUD <> '' ").Count) > 0 Then
                blnIsChange = True
            End If



            If blnIsChange = False Then
                If menAction = enAction.EDIT_ONE Then
                    objQualification = Nothing
                    OldData = Nothing
                    Me.Close()
                    Return False
                End If
            End If
            'Gajanan [21-June-2019] -- End



            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If pnlOtherQualification.Visible = False Then

            If CInt(cboCertificateGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Qualification Group is compulsory information. Please select Qualification Group to continue."), enMsgBoxStyle.Information)
                cboCertificateGroup.Focus()
                Return False
            End If

            If CInt(cboCertificates.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Qualification is compulsory information. Please select Qualification to continue."), enMsgBoxStyle.Information)
                cboCertificates.Focus()
                Return False
            End If


            If CInt(cboResultCode.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Result Code is compulsory information. Please select Result Code to continue."), enMsgBoxStyle.Information)
                cboResultCode.Focus()
                Return False
            End If

            Else

                If txtOtherQualificationGrp.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Other Qualification Group cannot be blank.Other Qualification Group is compulsory information."), enMsgBoxStyle.Information)
                    txtOtherQualificationGrp.Select()
                    Return False
                End If

                If txtOtherQualification.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Other Qualification cannot be blank.Other Qualification is compulsory information."), enMsgBoxStyle.Information)
                    txtOtherQualification.Select()
                    Return False
                End If

                If txtOtherResultCode.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Other Result Code cannot be blank.Other Result Code is compulsory information."), enMsgBoxStyle.Information)
                    txtOtherResultCode.Select()
                    Return False
                End If

                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                'If txtOtherInstitute.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Other Institute cannot be blank.Other Institute is compulsory information."), enMsgBoxStyle.Information)
                '    txtOtherInstitute.Select()
                '    Return False
                'End If
                'S.SANDEEP [23 JUL 2016] -- END

            End If


            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES (ANDY'S REQUEST TO REMOVE MANDATORY)
            'If CInt(cboInstitution.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Provider is compulsory information. Please select Provider to continue."), enMsgBoxStyle.Information)
            '    cboInstitution.Focus()
            '    Return False
            'End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                If dtpEndDate.Value.Date <= dtpStartDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "End date cannot be less or equal to start date."), enMsgBoxStyle.Information)
                    dtpEndDate.Focus()
                    Return False
                End If
            End If

            'If txtAward.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Award Name cannot be blank. Award Name is compulsory information."), enMsgBoxStyle.Information)
            '    txtAward.Focus()
            '    Return False
            'End If

            'Anjan (09 Oct 2010)-Start
            'Issue : Rutta wants this to be optional.
            'If txtAwardNo.Text.Trim = "" Then
            '    'Sandeep [ 14 Aug 2010 ] -- Start
            '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Award No. cannot be blank. Award No. is compulsory information."), enMsgBoxStyle.Information)
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Ref. No. cannot be blank. Ref. No. is compulsory information."), enMsgBoxStyle.Information)
            '    'Sandeep [ 14 Aug 2010 ] -- End 
            '    txtAwardNo.Focus()
            '    Return False
            'End If
            'Anjan (09 Oct 2010)-End

            'If txtInstitution.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Institution Name cannot be blank. Institution Name is compulsory information."), enMsgBoxStyle.Information)
            '    txtInstitution.Focus()
            '    Return False
            'End If

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'If QualificationApprovalFlowVal Is Nothing Then
            If QualificationApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                If objApprovalData.IsApproverPresent(enScreenName.frmQualificationsList, FinancialYear._Object._DatabaseName, _
                                                   ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                   FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                   User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    Return False
                End If
                'Gajanan [17-April-2019] -- End


                If objQualification.isExist(CInt(cboEmployee.SelectedValue), CInt(cboCertificates.SelectedValue), txtAwardNo.Text.Trim, CInt(cboInstitution.SelectedValue), mintQualificationUnkid, txtOtherQualification.Text.Trim, Nothing) = True Then
                    eZeeMsgBox.Show(Language.getMessage("clsEmp_Qualification_Tran", 1, "This Qualification is already assigned to particular employee. Please assign new Qualification."), enMsgBoxStyle.Information)
                    Return False
                End If
                Dim intOperationType As Integer = 0 : Dim strMsg As String = ""


                'Gajanan [21-June-2019] -- Start      
                objAQualificationTran._Award_End_Date = objQualification._Award_End_Date
                objAQualificationTran._Award_Start_Date = objQualification._Award_Start_Date
                'Gajanan [21-June-2019] -- End

                If objAQualificationTran.isExist(CInt(cboEmployee.SelectedValue), CInt(cboCertificates.SelectedValue), txtAwardNo.Text, CInt(cboInstitution.SelectedValue), Nothing, "", txtOtherQualification.Text.Trim, Nothing, intOperationType) = True Then
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 29, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 30, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 31, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 32, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 33, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 34, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                                End If
                        End Select
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function


    Private Sub SetVisibility()

        Try
            objbtnAddGroup.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddQulification.Enabled = User._Object.Privilege._AddQualification_Course
            objbtnAddInstitution.Enabled = User._Object.Privilege._AddTrainingInstitute

            'Gajanan [17-DEC-2018] -- Start
            If mintQualificationUnkid > 0 Then
                cboEmployee.Enabled = False
            Else
                cboEmployee.Enabled = True
            End If
            'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objEmployee As New clsEmployee_Master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [04 JUN 2015] -- END

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")

                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee") & " " & "<b>" & " " & getTitleCase(objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname) & "</b></span></p>")

                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 143, "with employeecode") & " " & "<b>" & " " & objEmployee._Employeecode & "</b>." & " " & Language.getMessage("frmEmployeeMaster", 144, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 145, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage("frmEmployeeMaster", 146, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")

                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                'Gajanan (21 Nov 2018) -- Start

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language


                'StrMessage.Append("<TABLE border = '1' WIDTH = '50%' style='margin-left: 25px'>")
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%' >")
                'Gajanan [27-Mar-2019] -- End
                'Gajanan (21 Nov 2018) -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:15%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 11, "Field") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 12, "Old Value") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 13, "New Value") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enEmployeeData.QUALIFICATIONS

                            If objQualification._Qualificationgroupunkid <> CInt(cboCertificateGroup.SelectedValue) Then
                                Dim objQGroup As New clsCommon_Master
                                objQGroup._Masterunkid = objQualification._Qualificationgroupunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 14, "Qualification Group") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objQGroup._Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboCertificateGroup.SelectedValue) <= 0, "", cboCertificateGroup.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            ElseIf objQualification._Other_QualificationGrp.Trim.Length > 0 Then

                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 15, "Other Qualification Group") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objQualification._Other_QualificationGrp.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtOtherQualificationGrp.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            End If

                            If objQualification._Qualificationunkid <> CInt(cboCertificates.SelectedValue) Then
                                Dim objQual As New clsqualification_master
                                objQual._Qualificationunkid = objQualification._Qualificationunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 16, "Qualification/Award") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objQual._Qualificationname.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboCertificates.SelectedValue) <= 0, "", cboCertificates.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            ElseIf objQualification._Other_Qualification.Trim.Length > 0 Then

                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 17, "Other Qualification/Award") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objQualification._Other_Qualification.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtOtherQualification.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            End If

                            If objQualification._Resultunkid <> CInt(cboResultCode.SelectedValue) Then
                                Dim objresult As New clsresult_master
                                objresult._Resultunkid = objQualification._Resultunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 18, "Result") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objresult._Resultname.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboResultCode.SelectedValue) <= 0, "", cboResultCode.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            ElseIf objQualification._other_ResultCode.Trim.Length > 0 Then

                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 19, "Other Result") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objQualification._other_ResultCode.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtOtherResultCode.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            End If

                            If objQualification._GPAcode <> nudLevel.Value Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 20, "GPA") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objQualification._GPAcode.ToString("#0.00") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & nudLevel.Value.ToString("0.00") & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objQualification._Award_Start_Date.Date <> dtpStartDate.Value.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 21, "Start Date") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objQualification._Award_Start_Date <> Nothing, objQualification._Award_Start_Date.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(dtpStartDate.Checked, dtpStartDate.Value.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objQualification._Award_End_Date.Date <> dtpEndDate.Value.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 22, "Award Date") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objQualification._Award_End_Date <> Nothing, objQualification._Award_End_Date.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(dtpEndDate.Checked, dtpEndDate.Value.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objQualification._Instituteunkid <> CInt(cboInstitution.SelectedValue) Then
                                Dim objInstitute As New clsinstitute_master
                                objInstitute._Instituteunkid = objQualification._Instituteunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 23, "Provider") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objInstitute._Institute_Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboInstitution.SelectedValue) <= 0, "", cboInstitution.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If menAction = enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification of Changes in Employee Qualification(s).")
                            objSendMail._Message = dicNotification(sKey)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                            'Sohail (17 Dec 2014) -- Start
                            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                            'objSendMail._Form_Name = mstrModuleName
                            objSendMail._Form_Name = "" 'Please pass Form Name for WEB
                            'Sohail (17 Dec 2014) -- End
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                            objSendMail._UserUnkid = User._Object._Userunkid
                            objSendMail._SenderAddress = User._Object._Email
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If

            ElseIf menAction <> enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 10, "Notifications to newly added Employee Qualification(s).")
                            Dim sMsg As String = dicNotification(sKey)
                            'Set_Notification(User._Object._Firstname.Trim & " " & User._Object._Lastname.Trim)
                            objSendMail._Message = sMsg
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                            'Sohail (17 Dec 2014) -- Start
                            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                            'objSendMail._Form_Name = mstrModuleName
                            objSendMail._Form_Name = "" 'Please pass Form Name for WEB
                            'Sohail (17 Dec 2014) -- End
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                            objSendMail._UserUnkid = User._Object._Userunkid
                            objSendMail._SenderAddress = User._Object._Email
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- END


    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtQualificationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtQualificationDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("scanattachrefid") = enScanAttactRefId.QUALIFICATIONS
                'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.
dRow("form_name") = mstrForm_Name
                dRow("userunkid") = User._Object._Userunkid
'Gajanan [17-DEC-2018] -- End

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'passing Qualification tranunkid for the Other Qualification , reason the attachment is not coming for Other Qualification. 
                'dRow("transactionunkid") = IIf(pnlOtherQualification.Visible = True, -1, cboCertificates.SelectedValue)
                dRow("transactionunkid") = mintQualificationUnkid
                'Shani(24-Aug-2015) -- End

                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = dtpCertificateDate.Value.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                dRow("filesize_kb") = f.Length / 1024
                'SHANI (16 JUL 2015) -- End 
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mdtQualificationDocument.Rows.Add(dRow)
            Call FillQualificationAttachment()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
        End Try
    End Sub

    Private Sub FillQualificationAttachment()
        Dim dtView As DataView
        Try
            If mdtQualificationDocument Is Nothing Then Exit Sub

            dtView = New DataView(mdtQualificationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvQualification.AutoGenerateColumns = False
            colhName.DataPropertyName = "filename"
            colhSize.DataPropertyName = "filesize"
            objcolhGUID.DataPropertyName = "GUID"
            objcolhScanUnkId.DataPropertyName = "scanattachtranunkid"
            dgvQualification.DataSource = dtView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillQualificationAttachment", mstrModuleName)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 

#End Region

#Region " Form's Events "
    Private Sub frmQualifications_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objQualification = Nothing
    End Sub

    Private Sub frmQualifications_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmQualifications_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmQualifications_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objQualification = New clsEmp_Qualification_Tran
        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objAQualificationTran = New clsEmp_Qualification_Approval_Tran
        'Gajanan [17-DEC-2018] -- End

        'SHANI (20 JUN 2015) -- Start
        'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
        objDocument = New clsScan_Attach_Documents
        'SHANI (20 JUN 2015) -- End 

        'Gajanan [9-April-2019] -- Start
        objApprovalData = New clsEmployeeDataApproval
        'Gajanan [9-April-2019] -- End
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()

           

            If menAction = enAction.EDIT_ONE Then
                objQualification._Qualificationtranunkid = mintQualificationUnkid
                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                cboEmployee.Enabled = False
                cboCertificateGroup.Enabled = False
                cboCertificates.Enabled = False
                'SHANI (20 JUN 2015) -- End 


                'S.SANDEEP [16 Jan 2016] -- START
                objbtnSearchEmployee.Enabled = False
                objbtnSearchQGrp.Enabled = False
                objbtnSearchQualification.Enabled = False
                objbtnAddGroup.Enabled = False
                objbtnAddQulification.Enabled = False
                'S.SANDEEP [16 Jan 2016] -- END

                'Gajanan [21-June-2019] -- Start      
                OldData = objQualification
                'Gajanan [21-June-2019] -- End

            End If

            Call GetValue()

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            Call FillQualificationAttachment()
            'SHANI (20 JUN 2015) -- End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            cboEmployee.Focus()
            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            'mdsDoc = (New clsScan_Attach_Documents).GetDocType("Docs")
            mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
            'SHANI (16 JUL 2015) -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmQualifications_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmp_Qualification_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmp_Qualification_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Buttons Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
            Cursor.Current = Cursors.WaitCursor
            'SHANI (27 JUL 2015) -- End 

            If IsValid() = False Then Exit Sub


            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory

            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            'If ConfigParameter._Object._QualificationCertificateAttachmentMandatory Then
            '    If mdtQualificationDocument Is Nothing OrElse mdtQualificationDocument.Select("AUD <> 'D'").Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If
            'Gajanan [5-Dec-2019] -- End
            'SHANI (27 JUL 2015) -- End 

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dicNotification = New Dictionary(Of String, String)
            If menAction = enAction.EDIT_ONE Then
                If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        dicNotification.Add(objUsr._Email, StrMessage)
                    Next
                    objUsr = Nothing
                End If

            ElseIf menAction <> enAction.EDIT_ONE Then
                If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(2).Split(CChar(","))
                    objUsr._Userunkid = CInt(sId)
                    StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If

                Next
                objUsr = Nothing
            End If
            End If

            'S.SANDEEP [ 18 SEP 2012 ] -- END


            Call SetValue()



            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim ExtraFilter As String = "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and instituteunkid = " & cboInstitution.SelectedValue.ToString() & " "

            Dim ExtraFilterForQuery As String = "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and instituteunkid = " & cboInstitution.SelectedValue.ToString() & " "

            If dtpStartDate.Checked = True Then
                ExtraFilter &= " and Award_Start_Date_fc = '" + eZeeDate.convertDate(dtpStartDate.Value.Date).ToString() + "' "
                ExtraFilterForQuery &= " and convert(varchar(8), cast(Award_Start_Date as datetime), 112)   =  '" + eZeeDate.convertDate(dtpStartDate.Value.Date).ToString() + "' "
            End If

            If dtpEndDate.Checked = True Then
                ExtraFilter &= " and Award_End_Date_fc = '" + eZeeDate.convertDate(dtpEndDate.Value.Date).ToString() + "' "
                ExtraFilterForQuery &= " and convert(varchar(8), cast(Award_End_Date as datetime), 112)   =  '" + eZeeDate.convertDate(dtpEndDate.Value.Date).ToString() + "' "
            End If

            If txtOtherQualification.Text.Trim.Length > 0 Then
                ExtraFilter &= " and other_qualification = '" + txtOtherQualification.Text + "' "
                ExtraFilterForQuery &= " and other_qualification = '" + txtOtherQualification.Text + "' "
            End If

            'Gajanan [17-April-2019] -- End


            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151104)
            'Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
            If mdtQualificationDocument.Rows.Count > 0 Then
            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                'Shani(24-Aug-2015) -- End

            Dim strError As String = ""

            For Each dRow As DataRow In mdtQualificationDocument.Rows
                If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                    Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                    If blnIsIISInstalled Then

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If clsFileUploadDownload.UploadFile(CStr(dRow("localpath")), mstrFolderName, strFileName, strError) = False Then
                        If clsFileUploadDownload.UploadFile(CStr(dRow("localpath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            'Shani(24-Aug-2015) -- End

                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                            dRow("fileuniquename") = strFileName
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Copy(CStr(dRow("localpath")), strDocLocalPath, True)
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then

                        'Gajanan [22-Feb-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.



                        'Dim strFileName As String = dRow("fileuniquename").ToString
                        'If blnIsIISInstalled Then

                        '    'Shani(24-Aug-2015) -- Start
                        '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        '    'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError) = False Then
                        '    If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                        '        'Shani(24-Aug-2015) -- End

                        '        eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                        '        Exit Sub
                        '    End If
                        'Else
                        '    If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                        '        Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                        '        If File.Exists(strDocLocalPath) Then
                        '            File.Delete(strDocLocalPath)
                        '        End If
                        '    Else
                        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                        '        Exit Sub
                        '    End If
                        'End If
                        'Gajanan [22-Feb-2019] -- End

                End If
            Next

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151104)
            End If
            'Shani(24-Aug-2015) -- End

            'SHANI (16 JUL 2015) -- End 

            If menAction = enAction.EDIT_ONE Then
                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                'blnFlag = objQualification.Update()

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'blnFlag = objQualification.Update(mdtQualificationDocument)

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If QualificationApprovalFlowVal Is Nothing Then
                If QualificationApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [22-Feb-2019] -- End
                    blnFlag = objAQualificationTran.Insert(Company._Object._Companyunkid, Nothing, False, mdtQualificationDocument)
                    If blnFlag = False AndAlso objAQualificationTran._Message <> "" Then
                        eZeeMsgBox.Show(objAQualificationTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.



                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                             Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                             CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                             enScreenName.frmQualificationsList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                             User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                             User._Object._Username, clsEmployeeDataApproval.enOperationType.EDITED, , _
                                                             cboEmployee.SelectedValue.ToString(), , , ExtraFilter, Nothing, False, _
                                                             clsEmployeeAddress_approval_tran.enAddressType.NONE, ExtraFilterForQuery)

        'Gajanan [17-April-2019] -- End

                    End If
                Else
                blnFlag = objQualification.Update(mdtQualificationDocument)
                End If
                'Gajanan [17-DEC-2018] -- End

                'SHANI (20 JUN 2015) -- End 
            Else



                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                ''SHANI (20 JUN 2015) -- Start
                ''Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                ''blnFlag = objQualification.Insert()
                'blnFlag = objQualification.Insert(mdtQualificationDocument)
                ''SHANI (20 JUN 2015) -- End 


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If QualificationApprovalFlowVal Is Nothing Then
                If QualificationApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [22-Feb-2019] -- End

                    blnFlag = objAQualificationTran.Insert(Company._Object._Companyunkid, , , mdtQualificationDocument)
                    If blnFlag = False AndAlso objAQualificationTran._Message <> "" Then
                        eZeeMsgBox.Show(objAQualificationTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    Else

                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                            ConfigParameter._Object._UserAccessModeSetting, _
                                                            Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                            CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                            enScreenName.frmQualificationsList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                            User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                            User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , _
                                                            cboEmployee.SelectedValue.ToString(), , , ExtraFilter, Nothing, False, _
                                                            clsEmployeeAddress_approval_tran.enAddressType.NONE, ExtraFilterForQuery)

        'Gajanan [17-April-2019] -- End

                    End If
                Else
                blnFlag = objQualification.Insert(mdtQualificationDocument)
                End If
                'Gajanan [17-DEC-2018] -- End

            End If

            If blnFlag = False And objQualification._Message <> "" Then
                eZeeMsgBox.Show(objQualification._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objQualification = Nothing
                    objQualification = New clsEmp_Qualification_Tran
                    Call GetValue()
                    'SHANI (20 JUN 2015) -- Start
                    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                    cboDocumentType.SelectedValue = 0
                    dtpCertificateDate.Value = Today.Date
                    Call FillQualificationAttachment()
                    If mintEmployeeUnkid <= 0 Then
                        cboEmployee.Enabled = True
                    End If
                    cboCertificateGroup.Enabled = True
                    cboCertificates.Enabled = True
                    'SHANI (20 JUN 2015) -- End
                    cboEmployee.Focus()
                Else
                    mintQualificationUnkid = objQualification._Qualificationtranunkid
                    Me.Close()
                End If
            End If

            If mintSeletedEmpId <> -1 Then
                cboEmployee.SelectedValue = mintSeletedEmpId
            End If

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [09-AUG-2018] -- START
            'If blnFlag = True Then
            '   trd = New Thread(AddressOf Send_Notification)
            '   trd.IsBackground = True
            '   trd.Start()
            'End If
            If ConfigParameter._Object._SkipEmployeeApprovalFlow = False Then
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            End If
            'S.SANDEEP [09-AUG-2018] -- END

            'S.SANDEEP [ 18 SEP 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
        Finally
            Cursor.Current = Cursors.Default
            'SHANI (27 JUL 2015) -- End 
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objfrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddQulification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQulification.Click
        Dim frm As New frmQualificationCourse_AddEdit
        Dim dsList As New DataSet
        Dim intQualifyId As Integer = -1
        Dim objQualify As New clsqualification_master
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(intQualifyId, enAction.ADD_ONE) Then
                dsList = objQualify.GetComboList("Qualify", True, CInt(cboCertificateGroup.SelectedValue))
                With cboCertificates
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Qualify")
                    .SelectedValue = intQualifyId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddQulification_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objQualify = Nothing
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Dim frm As New frmCommonMaster
        Dim dsList As New DataSet
        Dim intGroupId As Integer = -1
        Dim objGroup As New clsCommon_Master
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intGroupId, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, enAction.ADD_ONE)
            If intGroupId > -1 Then
                dsList = objGroup.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "Group")
                With cboCertificateGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Group")
                    .SelectedValue = intGroupId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objGroup = Nothing
        End Try
    End Sub


    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private Sub btnAddAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try

            'Gajanan [21-June-2019] -- Start      
            'If IsValid() = False Then Exit Sub
            If IsValid(True) = False Then Exit Sub
            'Gajanan [21-June-2019] -- End

            If CInt(cboDocumentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Document Type is compulsory information. Please select Document Type to continue."), enMsgBoxStyle.Information)
                cboDocumentType.Focus()
                Exit Sub
            End If

            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                Dim f As New System.IO.FileInfo(ofdAttachment.FileName)


                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "You can not attach .exe(Executable File) for the security reason."))
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End


                Call AddDocumentAttachment(f, ofdAttachment.FileName.ToString)
                f = Nothing

                If dgvQualification.Rows.Count > 0 Then
                    cboEmployee.Enabled = False
                    cboCertificateGroup.Enabled = False
                    cboCertificates.Enabled = False
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddAttachment_Click :", mstrModuleName)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 

#End Region

#Region " Controls "
    Private Sub cboCertificateGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCertificateGroup.SelectedIndexChanged
        Try
            If CInt(cboCertificateGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objQualify As New clsqualification_master
                dsList = objQualify.GetComboList("Qualify", True, CInt(cboCertificateGroup.SelectedValue))
                With cboCertificates
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Qualify")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCertificateGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            mintSeletedEmpId = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddInstitution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInstitution.Click
        Dim intRefId As Integer = -1
        Dim frm As New frmTraningInstitutes_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

            frm.Text = Language.getMessage("frmTraningInstitutesList", 3, "Add/Edit Training Provider")
            frm.gbTaraningInstitutes.Text = Language.getMessage("frmTraningInstitutesList", 8, "Training Provider")
            frm.eZeeHeader.Title = Language.getMessage("frmTraningInstitutesList", 8, "Training Provider")

            'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment


            frm.displayDialog(intRefId, enAction.ADD_ONE, False)
            If intRefId > -1 Then
                Dim objInstitute As New clsinstitute_master
                Dim dsList As New DataSet
                dsList = objInstitute.getListForCombo(False, "List", True)
                With cboInstitution
                    .ValueMember = "instituteunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddInstitution_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Private Sub cboCertificates_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCertificates.SelectedIndexChanged
        Try
            If cboCertificates.SelectedIndex < 0 Then Exit Sub

            Dim objQualification As New clsqualification_master
            Dim dsList As DataSet = objQualification.GetResultCodeFromQualification(cboCertificates.SelectedValue.ToString(), True)
            cboResultCode.DisplayMember = "resultname"
            cboResultCode.ValueMember = "resultunkid"
            cboResultCode.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCertificates_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Oct-2011) -- End

    'S.SANDEEP [ 27 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchQGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchQGrp.Click, objbtnSearchProvider.Click, objbtnSearchQualification.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DataSource = Nothing
                Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                    Case "OBJBTNSEARCHQGRP"
                        .ValueMember = cboCertificateGroup.ValueMember
                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        '.DisplayMember = cboCertificates.DisplayMember
                        .DisplayMember = cboCertificateGroup.DisplayMember
                        'Gajanan [17-DEC-2018] -- End
                        .DataSource = CType(cboCertificateGroup.DataSource, DataTable)
                    Case "OBJBTNSEARCHPROVIDER"
                        .ValueMember = cboInstitution.ValueMember
                        .DisplayMember = cboInstitution.DisplayMember
                        .DataSource = CType(cboInstitution.DataSource, DataTable)
                    Case "OBJBTNSEARCHQUALIFICATION"

                        If CInt(cboCertificateGroup.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Please,Select qualification group to do further opration on it."), enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                        .ValueMember = cboCertificates.ValueMember
                        .DisplayMember = cboCertificates.DisplayMember
                        .DataSource = CType(cboCertificates.DataSource, DataTable)
                        End If


                End Select
                If .DisplayDialog Then
                    Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                        Case "OBJBTNSEARCHQGRP"
                            cboCertificateGroup.SelectedValue = .SelectedValue
                            cboCertificateGroup.Focus()
                        Case "OBJBTNSEARCHPROVIDER"
                            cboInstitution.SelectedValue = .SelectedValue
                            cboInstitution.Focus()
                        Case "OBJBTNSEARCHQUALIFICATION"
                            cboCertificates.SelectedValue = .SelectedValue
                            cboCertificates.Focus()
                    End Select
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchQGrp_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 27 FEB 2012 ] -- END


    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private Sub dgvQualification_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvQualification.CellContentClick
        Try
            'SHANI (27 JUL 2015) -- Start
            'Enhancement - 
            Cursor.Current = Cursors.WaitCursor
            'SHANI (27 JUL 2015) -- End 

            If e.RowIndex >= 0 Then
                Dim xrow() As DataRow

                If CInt(dgvQualification.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) > 0 Then
                    xrow = mdtQualificationDocument.Select("scanattachtranunkid = " & CInt(dgvQualification.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) & "")
                Else
                    xrow = mdtQualificationDocument.Select("GUID = '" & dgvQualification.Rows(e.RowIndex).Cells(objcolhGUID.Index).Value.ToString & "'")
                End If

                If e.ColumnIndex = objcohDelete.Index Then
                    If xrow.Length > 0 Then
                        If MessageBox.Show(Language.getMessage(mstrModuleName, 25, "Are you sure, you want to delete this attachment?"), "Aruti", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                            xrow(0).Item("AUD") = "D"
                            Call FillQualificationAttachment()
                        End If
                    End If
                ElseIf e.ColumnIndex = objcolhDownload.Index Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("file_path").ToString
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If

                    Dim finfo As New IO.FileInfo(xPath)

                    sfdAttachment.Filter = "File Format (" & finfo.Extension & ")|*" & finfo.Extension & ""
                    sfdAttachment.FileName = xrow(0).Item("filename").ToString

                    If sfdAttachment.ShowDialog = Windows.Forms.DialogResult.OK Then
                        'SHANI (16 JUL 2015) -- Start
                        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                        'Upload Image folder to access those attachemts from Server and 
                        'all client machines if ArutiSelfService is installed otherwise save then on Document path
                        'IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            Dim strError As String = ""
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            If blnIsIISInstalled Then

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError)
                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)
                                'Shani(24-Aug-2015) -- End

                                If strError <> "" Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If
                                'SHANI (27 JUL 2015) -- Start
                                'Enhancement - 
                                If filebytes IsNot Nothing Then
                                Dim ms As New MemoryStream(filebytes)
                                Dim fs As New FileStream(sfdAttachment.FileName, FileMode.Create)
                                ms.WriteTo(fs)

                                ms.Close()
                                fs.Close()
                                fs.Dispose()
                            Else
                                    If System.IO.File.Exists(xrow(0).Item("filepath").ToString) Then
                                        xPath = xrow(0).Item("filepath").ToString

                                        Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow(0).Item("filepath").ToString))

                                        'Shani(24-Aug-2015) -- Start
                                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                        'If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError) = False Then
                                        If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                            'Shani(24-Aug-2015) -- End

                                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                            Exit Try
                                        Else
                                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                                strPath += "/"
                                            End If
                                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                                            xrow(0).Item("AUD") = "U"
                                            xrow(0).Item("fileuniquename") = strFileUniqueName
                                            xrow(0).Item("filepath") = strPath
                                            xrow(0).Item("filesize") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).Item("filesize_kb") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).AcceptChanges()
                                            Dim objScanAttach As New clsScan_Attach_Documents
                                            objScanAttach._Datatable = mdtQualificationDocument
                                            objScanAttach.InsertUpdateDelete_Documents()
                                            If objDocument._Message <> "" Then
                                                objScanAttach = Nothing
                                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                                                Exit Try
                                            End If
                                            objScanAttach = Nothing
                                            xrow(0).Item("AUD") = ""
                                            xrow(0).AcceptChanges()
                        IO.File.Copy(xPath, sfdAttachment.FileName, True)
                    End If
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "sorry, file you are trying to access does not exists on Aruti self service application folder."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If

                                'SHANI (27 JUL 2015) -- End
                            Else
                                IO.File.Copy(xPath, sfdAttachment.FileName, True)
                            End If
                        Else
                        IO.File.Copy(xPath, sfdAttachment.FileName, True)
                    End If
                        'SHANI (16 JUL 2015) -- End 
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvQualification_CellContentClick :", mstrModuleName)
            'SHANI (27 JUL 2015) -- Start
            'Enhancement - 
        Finally
            Cursor.Current = Cursors.Default
            'SHANI (27 JUL 2015) -- End 
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(cboEmployee.SelectedValue) & "'")
            If itmp.Length > 0 Then
                isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
            Else
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'isEmployeeApprove = True
                isEmployeeApprove = False
                'Gajanan [17-April-2019] -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Gajanan [22-Feb-2019] -- End

#End Region



    'Pinkal (25-APR-2012) -- Start
    'Enhancement : TRA Changes

#Region "LinkButton Event"

    Private Sub lnkOtherQualification_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOtherQualification.LinkClicked
        Try

            'Gajanan [27-May-2019] -- Start              
            'If pnlOtherQualification.Visible = False Then
            '    pnlOtherQualification.Visible = True
            'End If

            pnlOtherQualification.Visible = Not (pnlOtherQualification.Visible)
            'Gajanan [27-May-2019] -- End

            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            If pnlOtherQualification.Visible = True Then
                txtOtherInstitute.Visible = pnlOtherQualification.Visible
            End If

            If pnlOtherQualification.Visible = True Then
                cboInstitution.Visible = False
                objbtnAddInstitution.Visible = cboInstitution.Visible
                objbtnSearchProvider.Visible = cboInstitution.Visible
            End If
            'S.SANDEEP [23 JUL 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkOtherQualification_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (25-APR-2012) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			Call SetLanguage()
			
			Me.gbEmployeeQualification.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeQualification.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddAttachment.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAddAttachment.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbEmployeeQualification.Text = Language._Object.getCaption(Me.gbEmployeeQualification.Name, Me.gbEmployeeQualification.Text)
			Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
			Me.lblQualificationGroup.Text = Language._Object.getCaption(Me.lblQualificationGroup.Name, Me.lblQualificationGroup.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblInstitution.Text = Language._Object.getCaption(Me.lblInstitution.Name, Me.lblInstitution.Text)
			Me.lblReferenceNo.Text = Language._Object.getCaption(Me.lblReferenceNo.Name, Me.lblReferenceNo.Text)
			Me.lblAwardDate.Text = Language._Object.getCaption(Me.lblAwardDate.Name, Me.lblAwardDate.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lnQualificationInfo.Text = Language._Object.getCaption(Me.lnQualificationInfo.Name, Me.lnQualificationInfo.Text)
			Me.lnEmployeeName.Text = Language._Object.getCaption(Me.lnEmployeeName.Name, Me.lnEmployeeName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblGPAcode.Text = Language._Object.getCaption(Me.lblGPAcode.Name, Me.lblGPAcode.Text)
			Me.lblResultCode.Text = Language._Object.getCaption(Me.lblResultCode.Name, Me.lblResultCode.Text)
			Me.lblOtherResultCode.Text = Language._Object.getCaption(Me.lblOtherResultCode.Name, Me.lblOtherResultCode.Text)
			Me.lblOtherQualificationGrp.Text = Language._Object.getCaption(Me.lblOtherQualificationGrp.Name, Me.lblOtherQualificationGrp.Text)
			Me.lblOtherQualification.Text = Language._Object.getCaption(Me.lblOtherQualification.Name, Me.lblOtherQualification.Text)
			Me.lnkOtherQualification.Text = Language._Object.getCaption(Me.lnkOtherQualification.Name, Me.lnkOtherQualification.Text)
            Me.lblDocumentType.Text = Language._Object.getCaption(Me.lblDocumentType.Name, Me.lblDocumentType.Text)
			Me.btnAddAttachment.Text = Language._Object.getCaption(Me.btnAddAttachment.Name, Me.btnAddAttachment.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.colhName.HeaderText = Language._Object.getCaption(Me.colhName.Name, Me.colhName.HeaderText)
			Me.colhSize.HeaderText = Language._Object.getCaption(Me.colhSize.Name, Me.colhSize.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Qualification Group is compulsory information. Please select Qualification Group to continue.")
			Language.setMessage(mstrModuleName, 3, "Qualification is compulsory information. Please select Qualification to continue.")
			Language.setMessage(mstrModuleName, 4, "End date cannot be less or equal to start date.")
			Language.setMessage(mstrModuleName, 5, "Result Code is compulsory information. Please select Result Code to continue.")
			Language.setMessage(mstrModuleName, 6, "Other Qualification Group cannot be blank.Other Qualification Group is compulsory information.")
			Language.setMessage(mstrModuleName, 7, "Other Qualification cannot be blank.Other Qualification is compulsory information.")
			Language.setMessage(mstrModuleName, 8, "Other Result Code cannot be blank.Other Result Code is compulsory information.")
			Language.setMessage(mstrModuleName, 9, "Notification of Changes in Employee Qualification(s).")
			Language.setMessage(mstrModuleName, 10, "Notifications to newly added Employee Qualification(s).")
			Language.setMessage(mstrModuleName, 11, "Field")
			Language.setMessage(mstrModuleName, 12, "Old Value")
			Language.setMessage(mstrModuleName, 13, "New Value")
			Language.setMessage(mstrModuleName, 14, "Qualification Group")
			Language.setMessage(mstrModuleName, 15, "Other Qualification Group")
			Language.setMessage(mstrModuleName, 16, "Qualification/Award")
			Language.setMessage(mstrModuleName, 17, "Other Qualification/Award")
			Language.setMessage(mstrModuleName, 18, "Result")
			Language.setMessage(mstrModuleName, 19, "Other Result")
			Language.setMessage(mstrModuleName, 20, "GPA")
			Language.setMessage(mstrModuleName, 21, "Start Date")
			Language.setMessage(mstrModuleName, 22, "Award Date")
			Language.setMessage(mstrModuleName, 23, "Provider")
			Language.setMessage(mstrModuleName, 24, "Document Type is compulsory information. Please select Document Type to continue.")
			Language.setMessage(mstrModuleName, 25, "Are you sure, you want to delete this attachment?")
			Language.setMessage(mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation.")
			Language.setMessage(mstrModuleName, 27, "Configuration Path does not Exist.")
			Language.setMessage(mstrModuleName, 28, "sorry, file you are trying to access does not exists on Aruti self service application folder.")
			Language.setMessage(mstrModuleName, 29, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
			Language.setMessage(mstrModuleName, 30, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
			Language.setMessage(mstrModuleName, 31, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
			Language.setMessage(mstrModuleName, 32, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
			Language.setMessage(mstrModuleName, 33, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
			Language.setMessage(mstrModuleName, 34, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
			Language.setMessage(mstrModuleName, 35, "You can not attach .exe(Executable File) for the security reason.")
			Language.setMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee")
			Language.setMessage("frmEmployeeMaster", 143, "with employeecode")
			Language.setMessage("frmEmployeeMaster", 144, "Following information has been changed by user")
			Language.setMessage("frmEmployeeMaster", 145, "from Machine")
			Language.setMessage("frmEmployeeMaster", 146, "and IPAddress")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")
			Language.setMessage("clsEmp_Qualification_Tran", 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
			Language.setMessage("frmTraningInstitutesList", 3, "Add/Edit Training Provider")
			Language.setMessage(mstrModuleName, 6, "Selected information is already present for particular employee.")
			Language.setMessage("frmTraningInstitutesList", 8, "Training Provider")
			Language.setMessage(mstrModuleName, 29, "Please,Select qualification group to do further opration on it.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class