﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportQualificationWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportQualificationWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objAQualificationTran As clsEmp_Qualification_Approval_Tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim QualificationApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployee_Skill_List)))
    'Gajanan [17-DEC-2018] -- End



    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [17-April-2019] -- End

#End Region

#Region " Form's Events "

    Private Sub frmImportQualificationWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
            cboISSYNC_RECRUIT.Enabled = False


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objApprovalData = New clsEmployeeDataApproval
            objAQualificationTran = New clsEmp_Qualification_Approval_Tran
            'Gajanan [17-April-2019] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportQualificationWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEmp_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEmp.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportEmp_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEmp.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    'S.SANDEEP [ 26 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If radOthQualification.Checked = False AndAlso radQualification.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please check atleast one import type to continue."), enMsgBoxStyle.Information)
                        e.Cancel = True
                        Exit Sub
                    End If

                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If radOthQualification.Checked = True Then
                    '    lblQualificationGroup.Tag = lblQualificationGroup.Text
                    '    lblQualification.Tag = lblQualification.Text
                    '    lblResult.Tag = lblResult.Text

                    '    lblQualificationGroup.Text = Language.getMessage(mstrModuleName, 15, "Other Qualification Group")
                    '    lblQualification.Text = Language.getMessage(mstrModuleName, 16, "Other Qualification")
                    '    lblResult.Text = Language.getMessage(mstrModuleName, 17, "Other Result")
                    'ElseIf radQualification.Checked = True Then
                    '    lblQualificationGroup.Text = lblQualificationGroup.Tag
                    '    lblQualification.Text = lblQualification.Tag
                    '    lblResult.Text = lblResult.Tag
                    'End If

                    If radOthQualification.Checked = True Then
                        lblQualificationGroup.Visible = False : lblQualification.Visible = False : lblResult.Visible = False
                        lblOthQualificationGroup.Visible = True : lblOthQualification.Visible = True : lblOthResult.Visible = True
                    ElseIf radQualification.Checked = True Then
                        lblQualificationGroup.Visible = True : lblQualification.Visible = True : lblResult.Visible = True
                        lblOthQualificationGroup.Visible = False : lblOthQualification.Visible = False : lblOthResult.Visible = False
                    End If
                    'S.SANDEEP [ 01 DEC 2012 ] -- END


                    'S.SANDEEP [ 26 APRIL 2012 ] -- END

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                Select Case CType(ctrl, ComboBox).Name.ToUpper
                                    'S.SANDEEP [19-JUL-2018] -- START
                                    Case cboFIRSTNAME.Name.ToUpper, cboSURNAME.Name.ToUpper, cboREF_NO.Name.ToUpper, cboSTART_DATE.Name.ToUpper, cboEND_DATE.Name.ToUpper, _
                                         cboREMARK.Name.ToUpper, cboRESULT.Name.ToUpper, cboINSTITUTE.Name.ToUpper, cboISSYNC_RECRUIT.Name.ToUpper
                                        'Case "CBOFIRSTNAME", "CBOLASTNAME", "CBOREFNO", "CBOSTARTDATE", "CBOENDDATE", "CBOREMARK", "CBORESULT", "CBOINSTITUTE", "CBOISSYNC" 'S.SANDEEP [ 26 APRIL 2012 CBORESULT ] -- START -- END 'S.SANDEEP [ 27 APRIL 2012 (ANDY'S REQUEST TO REMOVE MANDATORY) CBOINSTITUTE ] -- START -- END
                                        '    If CType(ctrl, ComboBox).Name.ToUpper = "CBOISSYNC" Then
                                        If CType(ctrl, ComboBox).Name.ToUpper = cboISSYNC_RECRUIT.Name.ToUpper Then
                                            'S.SANDEEP [19-JUL-2018] -- END
                                            If cboISSYNC_RECRUIT.Text.Trim.Length > 0 Then
                                                If CType(ctrl, ComboBox).Text = "" Then
                                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Select Is Sync with Recruitment to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                                    e.Cancel = True
                                                    Exit For
                                                End If
                                            Else
                                                If CType(ctrl, ComboBox).Text.Trim.Length > 0 Then
                                                    If cboINSTITUTE.Text.Trim.Length <= 0 Then
                                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Select Institute to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                                        e.Cancel = True
                                                        Exit For
                                                    End If
                                                End If
                                            End If
                                        End If
                                        Continue For
                                    Case Else
                                        If CType(ctrl, ComboBox).Text = "" Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                            e.Cancel = True
                                            Exit For
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboECODE.Items.Add(dtColumns.ColumnName)
                cboEND_DATE.Items.Add(dtColumns.ColumnName)
                cboFIRSTNAME.Items.Add(dtColumns.ColumnName)
                cboINSTITUTE.Items.Add(dtColumns.ColumnName)
                cboSURNAME.Items.Add(dtColumns.ColumnName)
                cboQUALIFICATION_NAME.Items.Add(dtColumns.ColumnName)
                cboQUALIFICATION_GROUP.Items.Add(dtColumns.ColumnName)
                cboREF_NO.Items.Add(dtColumns.ColumnName)
                cboREMARK.Items.Add(dtColumns.ColumnName)
                cboSTART_DATE.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP [ 26 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                cboRESULT.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP [ 26 APRIL 2012 ] -- END


                'S.SANDEEP [ 04 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                cboISSYNC_RECRUIT.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP [ 04 MAY 2012 ] -- END


                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known


                If dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 5, "ECODE") Then
                    dtColumns.Caption = "ECODE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 6, "FIRSTNAME") Then
                    dtColumns.Caption = "FIRSTNAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 7, "SURNAME") Then
                    dtColumns.Caption = "SURNAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 8, "QUALIFICATION_GROUP") Then
                    dtColumns.Caption = "QUALIFICATION_GROUP"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 9, "QUALIFICATION_NAME") Then
                    dtColumns.Caption = "QUALIFICATION_NAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 10, "RESULT") Then
                    dtColumns.Caption = "RESULT"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 11, "INSTITUTE") Then
                    dtColumns.Caption = "INSTITUTE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 12, "OTHER_QUALIFICATION_GROUP") Then
                    dtColumns.Caption = "OTHER_QUALIFICATION_GROUP"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 13, "OTHER_QUALIFICATION_NAME") Then
                    dtColumns.Caption = "OTHER_QUALIFICATION_NAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 14, "OTHER_RESULT") Then
                    dtColumns.Caption = "OTHER_RESULT"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 15, "INSTITUTE") Then
                    dtColumns.Caption = "INSTITUTE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 16, "REF_NO") Then
                    dtColumns.Caption = "REF_NO"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 17, "START_DATE") Then
                    dtColumns.Caption = "START_DATE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 18, "END_DATE") Then
                    dtColumns.Caption = "END_DATE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 19, "REMARK") Then
                    dtColumns.Caption = "REMARK"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsEmp_Qualification_Tran", 20, "ISSYNC_RECRUIT") Then
                    dtColumns.Caption = "ISSYNC_RECRUIT"
                End If

                'Gajanan (24 Nov 2018) -- End

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ECode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Firstname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Surname", System.Type.GetType("System.String"))


            'Gajanan [27-May-2019] -- Start              



            'mdt_ImportData_Others.Columns.Add("QGroup", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Qualification", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("RefNo", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("StartDate", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("EndDate", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Institute", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Remark", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("QGroup", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "QGroup")
            mdt_ImportData_Others.Columns.Add("Qualification", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Qualification")
            mdt_ImportData_Others.Columns.Add("RefNo", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "RefNo")
            mdt_ImportData_Others.Columns.Add("StartDate", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "StartDate")
            mdt_ImportData_Others.Columns.Add("EndDate", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "EndDate")
            mdt_ImportData_Others.Columns.Add("Institute", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Institute")
            mdt_ImportData_Others.Columns.Add("Remark", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Remark")

            'Gajanan [27-May-2019] -- End

            'S.SANDEEP [ 26 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Gajanan [27-May-2019] -- Start              
            'mdt_ImportData_Others.Columns.Add("Result", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Result", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Result")
            'Gajanan [27-May-2019] -- End

            'S.SANDEEP [ 26 APRIL 2012 ] -- END


            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES


            'Gajanan [27-May-2019] -- Start              
            'mdt_ImportData_Others.Columns.Add("IsSync", System.Type.GetType("System.Boolean"))
            mdt_ImportData_Others.Columns.Add("IsSync", System.Type.GetType("System.Boolean")).ExtendedProperties.Add("col", "IsSync")
            'Gajanan [27-May-2019] -- End

            'S.SANDEEP [ 04 MAY 2012 ] -- END



            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData_Others.Columns.Add("EmployeeId", System.Type.GetType("System.String"))
            'Gajanan [27-May-2019] -- End

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboECODE.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)

                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ECode") = dtRow.Item(cboECODE.Text).ToString.Trim

                If cboFIRSTNAME.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Firstname") = dtRow.Item(cboFIRSTNAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Firstname") = ""
                End If

                If cboSURNAME.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Surname") = dtRow.Item(cboSURNAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Surname") = ""
                End If

                If cboQUALIFICATION_GROUP.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("QGroup") = dtRow.Item(cboQUALIFICATION_GROUP.Text).ToString.Trim
                Else
                    drNewRow.Item("QGroup") = ""
                End If

                If cboQUALIFICATION_NAME.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Qualification") = dtRow.Item(cboQUALIFICATION_NAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Qualification") = ""
                End If

                If cboREF_NO.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("RefNo") = dtRow.Item(cboREF_NO.Text).ToString.Trim
                Else
                    drNewRow.Item("RefNo") = ""
                End If

                If cboSTART_DATE.Text.Trim.Length > 0 Then
                    drNewRow.Item("StartDate") = dtRow.Item(cboSTART_DATE.Text).ToString.Trim
                Else
                    drNewRow.Item("StartDate") = ""
                End If

                If cboEND_DATE.Text.Trim.Length > 0 Then
                    drNewRow.Item("EndDate") = dtRow.Item(cboEND_DATE.Text).ToString.Trim
                Else
                    drNewRow.Item("EndDate") = ""
                End If


                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'drNewRow.Item("Institute") = dtRow.Item(cboInstitute.Text).ToString.Trim
                If cboINSTITUTE.Text.Trim.Length > 0 Then
                    drNewRow.Item("Institute") = dtRow.Item(cboINSTITUTE.Text).ToString.Trim
                Else
                    drNewRow.Item("Institute") = ""
                End If
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                If cboREMARK.Text.Trim.Length > 0 Then
                    'S.SANDEEP [ 26 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'drNewRow.Item("Remark") = dtRow.Item(cboInstitute.Text).ToString.Trim
                    drNewRow.Item("Remark") = dtRow.Item(cboREMARK.Text).ToString.Trim
                    'S.SANDEEP [ 26 APRIL 2012 ] -- END
                Else
                    drNewRow.Item("Remark") = ""
                End If

                'S.SANDEEP [ 26 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If cboRESULT.Text.Trim.Length > 0 Then
                    drNewRow.Item("Result") = dtRow.Item(cboRESULT.Text).ToString.Trim
                Else
                    drNewRow.Item("Result") = ""
                End If
                'S.SANDEEP [ 26 APRIL 2012 ] -- END


                'S.SANDEEP [ 04 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If cboISSYNC_RECRUIT.Text.Trim.Length > 0 Then
                    drNewRow.Item("IsSync") = CBool(dtRow.Item(cboISSYNC_RECRUIT.Text))
                Else
                    drNewRow.Item("IsSync") = False
                End If
                'S.SANDEEP [ 04 MAY 2012 ] -- END


                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ECode"
                dgcolhQualification.DataPropertyName = "Qualification"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizImportEmp.BackEnabled = False
            eZeeWizImportEmp.CancelText = "Finish"

        Catch ex As System.ArgumentException
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please check the selected file column 'ISSYNC_RECRUIT' should only be in 'True' Or 'False' Format. Please modify it and try to import again."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboECODE.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Qualification(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboQUALIFICATION_GROUP.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Qualification Group cannot be blank. Please set Qualification Group in order to import Employee Qualification(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboQUALIFICATION_NAME.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Qualification cannot be blank. Please set Qualification in order to import Employee Qualification(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'If .Item(cboInstitute.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Institute cannot be blank. Please set Institute in order to import Employee Qualification(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False

            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objCMaster As New clsCommon_Master
            Dim objQMaster As New clsqualification_master
            Dim objIMaster As New clsinstitute_master
            Dim objEMaster As New clsEmployee_Master

            'S.SANDEEP [ 26 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim objEQTran As New clsEmp_Qualification_Tran
            Dim objEQTran As clsEmp_Qualification_Tran
            Dim objResult As New clsresult_master


            'Gajanan [21-June-2019] -- Start      
            'Dim intQualificationGrpId As Integer = -1
            'Dim intQualificationId As Integer = -1
            'Dim intInstituteId As Integer = -1

            Dim intQualificationGrpId As Integer = 0
            Dim intQualificationId As Integer = 0
            Dim intInstituteId As Integer = 0
            'Gajanan [21-June-2019] -- End
            'S.SANDEEP [ 26 APRIL 2012 ] -- END


            Dim intEmployeeUnkid As Integer = -1
            Dim intEQTranId As Integer = -1

            'S.SANDEEP [ 26 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim intResultId As Integer = 0
            'S.SANDEEP [ 26 APRIL 2012 ] -- END

            'Gajanan [27-May-2019] -- Start
            Dim EmpList As New List(Of String)
            'Gajanan [27-May-2019] -- End

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim isEmployeeApprove As Boolean = False
                'Gajanan [22-Feb-2019] -- End



                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                isEmployeeApprove = objEMaster.IsEmployeeApproved(dtRow.Item("ECode").ToString.Trim)
                'Gajanan [17-April-2019] -- End


                'S.SANDEEP [ 04 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try
                'S.SANDEEP [ 04 MAY 2012 ] -- END

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ECode").ToString.Trim.Length > 0 Then
                    intEmployeeUnkid = objEMaster.GetEmployeeUnkid("", dtRow.Item("ECode").ToString.Trim)
                    If intEmployeeUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                'S.SANDEEP [ 26 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If radQualification.Checked = True Then
                    '------------------------------ CHECKING IF QUALIFICATION GROUP PRESENT.
                    If dtRow.Item("QGroup").ToString.Trim.Length > 0 Then
                        intQualificationGrpId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, dtRow.Item("QGroup").ToString.Trim)
                        If intQualificationGrpId <= 0 Then
                            If objCMaster IsNot Nothing Then objCMaster = Nothing
                            objCMaster = New clsCommon_Master
                            objCMaster._Code = dtRow.Item("QGroup").ToString.Trim
                            objCMaster._Isactive = True
                            objCMaster._Mastertype = clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP
                            objCMaster._Name = dtRow.Item("QGroup").ToString.Trim

                            If objCMaster.Insert Then
                                intQualificationGrpId = objCMaster._Masterunkid
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = objCMaster._Code & " / " & objCMaster._Name & " / " & objCMaster._Message
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If
                    End If

                    '------------------------------ CHECKING IF QUALIFICATION PRESENT.
                    If dtRow.Item("Qualification").ToString.Trim.Length > 0 Then

                        'S.SANDEEP [16 Jan 2016] -- START
                        'intQualificationId = objQMaster.GetQualificationUnkid(dtRow.Item("Qualification").ToString.Trim)
                        intQualificationId = objQMaster.GetQualificationUnkid(dtRow.Item("Qualification").ToString.Trim, intQualificationGrpId)
                        'S.SANDEEP [16 Jan 2016] -- END

                        If intQualificationId <= 0 Then
                            If objQMaster IsNot Nothing Then objQMaster = Nothing
                            objQMaster = New clsqualification_master

                            objQMaster._Isactive = True
                            objQMaster._Qualificationcode = dtRow.Item("Qualification").ToString.Trim
                            objQMaster._Qualificationgroupunkid = intQualificationGrpId
                            objQMaster._Qualificationname = dtRow.Item("Qualification").ToString.Trim

                            If objQMaster.Insert Then
                                intQualificationId = objQMaster._Qualificationunkid
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = objQMaster._Qualificationcode & " / " & objQMaster._Qualificationname & " / " & objQMaster._Message
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If
                    End If

                    'S.SANDEEP [ 26 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    '------------------------------ CHECKING IF RESULT PRESENT.
                    If dtRow.Item("Result").ToString.Trim.Length > 0 Then
                        intResultId = objResult.GetResultUnkid(dtRow.Item("Result").ToString.Trim)
                        If intResultId <= 0 Then
                            If objResult IsNot Nothing Then objResult = Nothing
                            objResult = New clsresult_master

                            objResult._Isactive = True
                            objResult._Resultcode = dtRow.Item("Result").ToString.Trim
                            objResult._Resultgroupunkid = 0
                            objResult._ResultLevel = 0
                            objResult._Resultname = dtRow.Item("Result").ToString.Trim

                            If objResult.Insert Then
                                intResultId = objResult._Resultunkid
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = objResult._Resultcode & " / " & objResult._Resultname & " / " & objResult._Message
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If

                        End If
                    End If
                    'S.SANDEEP [ 26 APRIL 2012 ] -- END
                End If

                '------------------------------ CHECKING IF INSTITUTE PRESENT.
                If dtRow.Item("Institute").ToString.Trim.Length > 0 Then
                    intInstituteId = objIMaster.GetInstituteUnkid(True, dtRow.Item("Institute").ToString.Trim)
                    If intInstituteId <= 0 Then
                        If objIMaster IsNot Nothing Then objIMaster = Nothing
                        objIMaster = New clsinstitute_master

                        objIMaster._Institute_Code = dtRow.Item("Institute").ToString.Trim
                        objIMaster._Institute_Name = dtRow.Item("Institute").ToString.Trim
                        objIMaster._Isactive = True
                        objIMaster._Ishospital = False

                        'S.SANDEEP [ 04 MAY 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        objIMaster._Issync_Recruit = CBool(dtRow.Item("IsSync"))
                        'S.SANDEEP [ 04 MAY 2012 ] -- END

                        If objIMaster.Insert Then
                            intInstituteId = objIMaster._Instituteunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objIMaster._Institute_Code & " / " & objIMaster._Institute_Name & " / " & objIMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If
                'S.SANDEEP [ 26 APRIL 2012 ] -- END

                objEQTran = New clsEmp_Qualification_Tran

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If QualificationApprovalFlowVal Is Nothing Then
                If QualificationApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [17-April-2019] -- End


                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If objApprovalData.IsApproverPresent(enScreenName.frmQualificationsList, FinancialYear._Object._DatabaseName, _
                                                          ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                          FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                          User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, intEmployeeUnkid, Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                        eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)

                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = objApprovalData._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For
                    End If
                    'Gajanan [17-April-2019] -- End


                    objAQualificationTran = New clsEmp_Qualification_Approval_Tran
                    Dim intOperationType As Integer = 0 : Dim strMsg As String = ""
                    If objAQualificationTran.isExist(intEmployeeUnkid, intQualificationId, dtRow("RefNo").ToString(), intInstituteId, Nothing, "", dtRow("Qualification").ToString(), Nothing, intOperationType) = True Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                strMsg = Language.getMessage("frmQualifications", 30, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                strMsg = Language.getMessage("frmQualifications", 32, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                strMsg = Language.getMessage("frmQualifications", 34, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                        End Select

                        'Gajanan [30-Mar-2019] -- Start
                        dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = strMsg
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 2
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Continue For
                        'Gajanan [30-Mar-2019] -- End

                    End If
                    'Gajanan [30-Mar-2019] -- Start
                    'dtRow.Item("image") = imgError
                    'dtRow.Item("message") = strMsg
                    'dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    'dtRow.Item("objStatus") = 2
                    'objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    'Continue For
                    'Gajanan [30-Mar-2019] -- End
                End If
                'Gajanan [17-DEC-2018] -- End



                'S.SANDEEP [ 26 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'intEQTranId = objEQTran.GetQualificationTranId(intEmployeeUnkid, dtRow.Item("Qualification").ToString.Trim)

                'Anjan (17 Apr 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'If radOthQualification.Checked = True Then
                '    intEQTranId = objEQTran.GetQualificationTranId(intEmployeeUnkid, dtRow.Item("Qualification").ToString.Trim, True)
                'ElseIf radQualification.Checked = True Then
                '    intEQTranId = objEQTran.GetQualificationTranId(intEmployeeUnkid, dtRow.Item("Qualification").ToString.Trim)
                'End If

                If radOthQualification.Checked = True Then
                    intEQTranId = objEQTran.GetQualificationTranId(intEmployeeUnkid, dtRow.Item("Qualification").ToString.Trim, dtRow.Item("StartDate").ToString.Trim, dtRow.Item("EndDate").ToString.Trim, True)
                ElseIf radQualification.Checked = True Then
                    intEQTranId = objEQTran.GetQualificationTranId(intEmployeeUnkid, dtRow.Item("Qualification").ToString.Trim, dtRow.Item("StartDate").ToString.Trim, dtRow.Item("EndDate").ToString.Trim)
                End If
                'Anjan (17 Apr 2012)-End 

                'S.SANDEEP [ 26 APRIL 2012 ] -- END




                If intEQTranId > 0 Then
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Employee Qualification Already Exist.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                Else

                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    'If QualificationApprovalFlowVal Is Nothing Then
                    If QualificationApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                        'Gajanan [22-Feb-2019] -- End
                        objAQualificationTran = New clsEmp_Qualification_Approval_Tran
                        objAQualificationTran._Audittype = enAuditType.ADD
                        objAQualificationTran._Audituserunkid = User._Object._Userunkid
                        objAQualificationTran._Employeeunkid = intEmployeeUnkid
                        objAQualificationTran._Isvoid = False
                        objAQualificationTran._Tranguid = Guid.NewGuid.ToString()
                        objAQualificationTran._Transactiondate = Now
                        objAQualificationTran._Approvalremark = ""
                        objAQualificationTran._Isweb = False
                        objAQualificationTran._Isfinal = False
                        objAQualificationTran._Ip = getIP()
                        objAQualificationTran._Host = getHostName()
                        objAQualificationTran._Form_Name = mstrModuleName
                        objAQualificationTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objAQualificationTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                        objAQualificationTran._Qualificationtranunkid = -1
                        Try
                            If dtRow.Item("StartDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("StartDate")) Then
                                objAQualificationTran._Award_Start_Date = CDate(dtRow.Item("StartDate"))
                            Else
                                objAQualificationTran._Award_Start_Date = Nothing
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Invalid Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Finally
                        End Try

                        Try
                            If dtRow.Item("EndDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("EndDate")) Then
                                objAQualificationTran._Award_End_Date = CDate(dtRow.Item("EndDate"))
                            Else
                                objAQualificationTran._Award_End_Date = Nothing
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Invalid Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Finally
                        End Try

                        objEQTran._Employeeunkid = intEmployeeUnkid
                        objEQTran._Instituteunkid = intInstituteId
                       
                        If radQualification.Checked = True Then
                            objAQualificationTran._Qualificationgroupunkid = intQualificationGrpId
                            objAQualificationTran._Qualificationunkid = intQualificationId
                            objAQualificationTran._Resultunkid = intResultId
                        ElseIf radOthQualification.Checked = True Then
                            objAQualificationTran._Qualificationgroupunkid = 0
                            objAQualificationTran._Qualificationunkid = 0
                            objAQualificationTran._Resultunkid = 0

                            objAQualificationTran._Other_Qualificationgrp = dtRow.Item("QGroup").ToString.Trim
                            objAQualificationTran._Other_Qualification = dtRow.Item("Qualification").ToString.Trim
                            objAQualificationTran._Other_Resultcode = dtRow.Item("Result").ToString.Trim
                        End If

                        objAQualificationTran._Reference_No = dtRow.Item("RefNo").ToString.Trim
                        objAQualificationTran._Remark = dtRow.Item("Remark").ToString.Trim
                        objAQualificationTran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime

                    Else
                    Try
                        If dtRow.Item("StartDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("StartDate")) Then
                            objEQTran._Award_Start_Date = CDate(dtRow.Item("StartDate"))
                        Else
                            objEQTran._Award_Start_Date = Nothing
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Invalid Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Finally
                    End Try

                    Try
                        If dtRow.Item("EndDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("EndDate")) Then
                            objEQTran._Award_End_Date = CDate(dtRow.Item("EndDate"))
                        Else
                            objEQTran._Award_End_Date = Nothing
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Invalid Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Finally
                    End Try

                    objEQTran._Employeeunkid = intEmployeeUnkid
                    objEQTran._Instituteunkid = intInstituteId
                    'S.SANDEEP [ 26 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'objEQTran._Qualificationgroupunkid = intQualificationGrpId
                    'objEQTran._Qualificationunkid = intQualificationId
                    If radQualification.Checked = True Then
                        objEQTran._Qualificationgroupunkid = intQualificationGrpId
                        objEQTran._Qualificationunkid = intQualificationId
                        objEQTran._Resultunkid = intResultId
                    ElseIf radOthQualification.Checked = True Then
                        objEQTran._Qualificationgroupunkid = 0
                        objEQTran._Qualificationunkid = 0
                        objEQTran._Resultunkid = 0

                        objEQTran._Other_QualificationGrp = dtRow.Item("QGroup").ToString.Trim
                        objEQTran._Other_Qualification = dtRow.Item("Qualification").ToString.Trim
                        objEQTran._other_ResultCode = dtRow.Item("Result").ToString.Trim
                    End If
                    'S.SANDEEP [ 26 APRIL 2012 ] -- END
                    objEQTran._Reference_No = dtRow.Item("RefNo").ToString.Trim
                    objEQTran._Remark = dtRow.Item("Remark").ToString.Trim
                    objEQTran._Transaction_Date = ConfigParameter._Object._CurrentDateAndTime
                    objEQTran._Userunkid = User._Object._Userunkid
                    End If

                    'Try
                    '    If dtRow.Item("StartDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("StartDate")) Then
                    '        objEQTran._Award_Start_Date = CDate(dtRow.Item("StartDate"))
                    '    Else
                    '        objEQTran._Award_Start_Date = Nothing
                    '    End If
                    'Catch ex As Exception
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Invalid Date")
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    '    Continue For
                    'Finally
                    'End Try

                    'Try
                    '    If dtRow.Item("EndDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("EndDate")) Then
                    '        objEQTran._Award_End_Date = CDate(dtRow.Item("EndDate"))
                    '    Else
                    '        objEQTran._Award_End_Date = Nothing
                    '    End If
                    'Catch ex As Exception
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Invalid Date")
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    '    Continue For
                    'Finally
                    'End Try

                    'objEQTran._Employeeunkid = intEmployeeUnkid
                    'objEQTran._Instituteunkid = intInstituteId
                    ''S.SANDEEP [ 26 APRIL 2012 ] -- START
                    ''ENHANCEMENT : TRA CHANGES
                    ''objEQTran._Qualificationgroupunkid = intQualificationGrpId
                    ''objEQTran._Qualificationunkid = intQualificationId
                    'If radQualification.Checked = True Then
                    '    objEQTran._Qualificationgroupunkid = intQualificationGrpId
                    '    objEQTran._Qualificationunkid = intQualificationId
                    '    objEQTran._Resultunkid = intResultId
                    'ElseIf radOthQualification.Checked = True Then
                    '    objEQTran._Qualificationgroupunkid = 0
                    '    objEQTran._Qualificationunkid = 0
                    '    objEQTran._Resultunkid = 0

                    '    objEQTran._Other_QualificationGrp = dtRow.Item("QGroup").ToString.Trim
                    '    objEQTran._Other_Qualification = dtRow.Item("Qualification").ToString.Trim
                    '    objEQTran._other_ResultCode = dtRow.Item("Result").ToString.Trim
                    'End If
                    ''S.SANDEEP [ 26 APRIL 2012 ] -- END
                    'objEQTran._Reference_No = dtRow.Item("RefNo").ToString.Trim
                    'objEQTran._Remark = dtRow.Item("Remark").ToString.Trim
                    'objEQTran._Transaction_Date = ConfigParameter._Object._CurrentDateAndTime
                    'objEQTran._Userunkid = User._Object._Userunkid

                    'If objEQTran.Insert Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Success")
                    '    dtRow.Item("objStatus") = 1
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objEQTran._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If


                    Dim blnFlag As Boolean = False
                    If QualificationApprovalFlowVal Is Nothing Then
                        blnFlag = objAQualificationTran.Insert(Company._Object._Companyunkid)
                        If blnFlag = False AndAlso objAQualificationTran._Message <> "" Then
                            dtRow.Item("message") = objAQualificationTran._Message


                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.



                        Else
                            'Gajanan [27-May-2019] -- Start
                            If EmpList.Contains(intEmployeeUnkid) = False Then
                                EmpList.Add(intEmployeeUnkid)
                            End If



                            'Dim ExtraFilter As String = "employeeunkid= " & intEmployeeUnkid & " and instituteunkid = " & intInstituteId & " "
                            'Dim ExtraFilterForQuery As String = "employeeunkid= " & intEmployeeUnkid & " and instituteunkid = " & intInstituteId & " "

                            'If dtRow.Item("StartDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("StartDate")) Then
                            '    ExtraFilter &= " and Award_Start_Date_fc = '" + eZeeDate.convertDate(objAQualificationTran._Award_Start_Date).ToString() + "' "
                            '    ExtraFilterForQuery &= " and convert(varchar(8), cast(Award_Start_Date as datetime), 112)   =  '" + eZeeDate.convertDate(objAQualificationTran._Award_Start_Date).ToString() + "' "
                            'End If


                            'If dtRow.Item("EndDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("EndDate")) Then
                            '    ExtraFilter &= " and Award_End_Date_fc = '" + eZeeDate.convertDate(objAQualificationTran._Award_End_Date).ToString() + "' "
                            '    ExtraFilterForQuery &= " and convert(varchar(8), cast(Award_End_Date as datetime), 112)   =  '" + eZeeDate.convertDate(objAQualificationTran._Award_End_Date).ToString() + "' "
                            'End If


                            'If radOthQualification.Checked = True Then
                            '    ExtraFilter &= " and other_qualification = '" + objAQualificationTran._Other_Qualification + "' "
                            '    ExtraFilterForQuery &= " and other_qualification = '" + objAQualificationTran._Other_Qualification + "' "
                            'End If

                            
                            'objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                            '                                ConfigParameter._Object._UserAccessModeSetting, _
                            '                                Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                            '                                CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                            '                                enScreenName.frmQualificationsList, ConfigParameter._Object._EmployeeAsOnDate, _
                            '                                User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                            '                                User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , _
                            '                                intEmployeeUnkid, , , ExtraFilter, Nothing, False, _
                            '                                clsEmployeeAddress_approval_tran.enAddressType.NONE, ExtraFilterForQuery)



                            'Gajanan [17-April-2019] -- End
                        End If
                    Else
                        blnFlag = objEQTran.Insert()
                        If blnFlag = False AndAlso objEQTran._Message <> "" Then dtRow.Item("message") = objEQTran._Message
                    End If

                    If blnFlag Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 102, "Success")
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        'Gajanan |30-MAR-2019| -- START
                        dtRow.Item("message") = objEQTran._Message
                        'Gajanan |30-MAR-2019| -- END
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If

                End If
                'Gajanan [27-May-2019] -- Start              
                Application.DoEvents()
                'Gajanan [27-May-2019] -- End
            Next



            'Gajanan [27-May-2019] -- Start
            If QualificationApprovalFlowVal Is Nothing Then
                If mdt_ImportData_Others.Select("status <> '" & Language.getMessage(mstrModuleName, 7, "Fail") & "'").Count > 0 Then
                    If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                        Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                        mdt_ImportData_Others.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 7, "Fail") & "'"
                        objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                                   Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                   CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                                   enScreenName.frmQualificationsList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                   User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                   User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                                   User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                                   ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData_Others.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
                    End If
                End If
            End If
            'Gajanan [27-May-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cmb.Name.ToString.ToUpper = "CBOINSTITUTE" AndAlso cmb.SelectedIndex > 0 Then
                            cboISSYNC_RECRUIT.Enabled = True
                        End If

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known

                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizImportEmp.CancelText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_CancelText", Me.eZeeWizImportEmp.CancelText)
            Me.eZeeWizImportEmp.NextText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_NextText", Me.eZeeWizImportEmp.NextText)
            Me.eZeeWizImportEmp.BackText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_BackText", Me.eZeeWizImportEmp.BackText)
            Me.eZeeWizImportEmp.FinishText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_FinishText", Me.eZeeWizImportEmp.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblQualificationGroup.Text = Language._Object.getCaption(Me.lblQualificationGroup.Name, Me.lblQualificationGroup.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
            Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblLastName.Text = Language._Object.getCaption(Me.lblLastName.Name, Me.lblLastName.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.lblMessage2.Text = Language._Object.getCaption(Me.lblMessage2.Name, Me.lblMessage2.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.dgcolhQualification.HeaderText = Language._Object.getCaption(Me.dgcolhQualification.Name, Me.dgcolhQualification.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.radQualification.Text = Language._Object.getCaption(Me.radQualification.Name, Me.radQualification.Text)
            Me.radOthQualification.Text = Language._Object.getCaption(Me.radOthQualification.Name, Me.radOthQualification.Text)
            Me.gbImportType.Text = Language._Object.getCaption(Me.gbImportType.Name, Me.gbImportType.Text)
            Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
            Me.lblIsSync.Text = Language._Object.getCaption(Me.lblIsSync.Name, Me.lblIsSync.Text)
            Me.lblOthQualificationGroup.Text = Language._Object.getCaption(Me.lblOthQualificationGroup.Name, Me.lblOthQualificationGroup.Text)
            Me.lblOthQualification.Text = Language._Object.getCaption(Me.lblOthQualification.Name, Me.lblOthQualification.Text)
            Me.lblOthResult.Text = Language._Object.getCaption(Me.lblOthResult.Name, Me.lblOthResult.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Qualification(s).")
            Language.setMessage(mstrModuleName, 4, "Qualification Group cannot be blank. Please set Qualification Group in order to import Employee Qualification(s).")
            Language.setMessage(mstrModuleName, 5, "Qualification cannot be blank. Please set Qualification in order to import Employee Qualification(s).")
            Language.setMessage(mstrModuleName, 6, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 7, "Fail")
            Language.setMessage(mstrModuleName, 8, "Success")
            Language.setMessage(mstrModuleName, 9, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 10, "Employee Qualification Already Exist.")
            Language.setMessage(mstrModuleName, 11, "Invalid Date")
            Language.setMessage(mstrModuleName, 12, "Please check the selected file column 'ISSYNC_RECRUIT' should only be in 'True' Or 'False' Format. Please modify it and try to import again.")
            Language.setMessage(mstrModuleName, 13, "Please check atleast one import type to continue.")
            Language.setMessage(mstrModuleName, 14, "Please Select Is Sync with Recruitment to Import Data.")
            Language.setMessage(mstrModuleName, 15, "Please Select Institute to Import Data.")
            Language.setMessage(mstrModuleName, 41, " Fields From File.")
            Language.setMessage(mstrModuleName, 42, "Please select different Field.")
            Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
            Language.setMessage(mstrModuleName, 50, "is already Mapped with")
            Language.setMessage(mstrModuleName, 101, "Fail")
            Language.setMessage(mstrModuleName, 102, "Success")
            Language.setMessage("clsEmp_Qualification_Tran", 5, "ECODE")
            Language.setMessage("clsEmp_Qualification_Tran", 6, "FIRSTNAME")
            Language.setMessage("clsEmp_Qualification_Tran", 7, "SURNAME")
            Language.setMessage("clsEmp_Qualification_Tran", 8, "QUALIFICATION_GROUP")
            Language.setMessage("clsEmp_Qualification_Tran", 9, "QUALIFICATION_NAME")
            Language.setMessage("clsEmp_Qualification_Tran", 10, "RESULT")
            Language.setMessage("clsEmp_Qualification_Tran", 11, "INSTITUTE")
            Language.setMessage("clsEmp_Qualification_Tran", 12, "OTHER_QUALIFICATION_GROUP")
            Language.setMessage("clsEmp_Qualification_Tran", 13, "OTHER_QUALIFICATION_NAME")
            Language.setMessage("clsEmp_Qualification_Tran", 14, "OTHER_RESULT")
            Language.setMessage("clsEmp_Qualification_Tran", 15, "INSTITUTE")
			Language.setMessage("clsEmp_Qualification_Tran", 16, "REF_NO")
			Language.setMessage("clsEmp_Qualification_Tran", 17, "START_DATE")
			Language.setMessage("clsEmp_Qualification_Tran", 18, "END_DATE")
			Language.setMessage("clsEmp_Qualification_Tran", 19, "REMARK")
			Language.setMessage("clsEmp_Qualification_Tran", 20, "ISSYNC_RECRUIT")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class