﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportQualificationWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportQualificationWizard))
        Me.eZeeWizImportEmp = New eZee.Common.eZeeWizard
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.lblOthResult = New System.Windows.Forms.Label
        Me.lblOthQualificationGroup = New System.Windows.Forms.Label
        Me.lblOthQualification = New System.Windows.Forms.Label
        Me.lblIsSync = New System.Windows.Forms.Label
        Me.cboISSYNC_RECRUIT = New System.Windows.Forms.ComboBox
        Me.lblResult = New System.Windows.Forms.Label
        Me.cboRESULT = New System.Windows.Forms.ComboBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblInstitute = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.cboEND_DATE = New System.Windows.Forms.ComboBox
        Me.cboREMARK = New System.Windows.Forms.ComboBox
        Me.cboINSTITUTE = New System.Windows.Forms.ComboBox
        Me.cboSTART_DATE = New System.Windows.Forms.ComboBox
        Me.lblCaption = New System.Windows.Forms.Label
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.cboQUALIFICATION_GROUP = New System.Windows.Forms.ComboBox
        Me.lblQualificationGroup = New System.Windows.Forms.Label
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.cboREF_NO = New System.Windows.Forms.ComboBox
        Me.lblQualification = New System.Windows.Forms.Label
        Me.cboQUALIFICATION_NAME = New System.Windows.Forms.ComboBox
        Me.cboECODE = New System.Windows.Forms.ComboBox
        Me.cboSURNAME = New System.Windows.Forms.ComboBox
        Me.lblFirstName = New System.Windows.Forms.Label
        Me.cboFIRSTNAME = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.lblLastName = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbImportType = New System.Windows.Forms.GroupBox
        Me.radQualification = New System.Windows.Forms.RadioButton
        Me.radOthQualification = New System.Windows.Forms.RadioButton
        Me.lblMessage2 = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhlogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeWizImportEmp.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.gbImportType.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeWizImportEmp
        '
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageMapping)
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageSelectFile)
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageImporting)
        Me.eZeeWizImportEmp.Controls.Add(Me.objbuttonBack)
        Me.eZeeWizImportEmp.Controls.Add(Me.objbuttonCancel)
        Me.eZeeWizImportEmp.Controls.Add(Me.objbuttonNext)
        Me.eZeeWizImportEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeWizImportEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeWizImportEmp.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eZeeWizImportEmp.Location = New System.Drawing.Point(0, 0)
        Me.eZeeWizImportEmp.Name = "eZeeWizImportEmp"
        Me.eZeeWizImportEmp.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageMapping, Me.WizPageImporting})
        Me.eZeeWizImportEmp.SaveEnabled = True
        Me.eZeeWizImportEmp.SaveText = "Save && Finish"
        Me.eZeeWizImportEmp.SaveVisible = False
        Me.eZeeWizImportEmp.SetSaveIndexBeforeFinishIndex = False
        Me.eZeeWizImportEmp.Size = New System.Drawing.Size(696, 428)
        Me.eZeeWizImportEmp.TabIndex = 4
        Me.eZeeWizImportEmp.WelcomeImage = Nothing
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(696, 380)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFieldMapping.Controls.Add(Me.lblOthResult)
        Me.gbFieldMapping.Controls.Add(Me.lblOthQualificationGroup)
        Me.gbFieldMapping.Controls.Add(Me.lblOthQualification)
        Me.gbFieldMapping.Controls.Add(Me.lblIsSync)
        Me.gbFieldMapping.Controls.Add(Me.cboISSYNC_RECRUIT)
        Me.gbFieldMapping.Controls.Add(Me.lblResult)
        Me.gbFieldMapping.Controls.Add(Me.cboRESULT)
        Me.gbFieldMapping.Controls.Add(Me.lblRemark)
        Me.gbFieldMapping.Controls.Add(Me.lblInstitute)
        Me.gbFieldMapping.Controls.Add(Me.lblEndDate)
        Me.gbFieldMapping.Controls.Add(Me.lblStartDate)
        Me.gbFieldMapping.Controls.Add(Me.cboEND_DATE)
        Me.gbFieldMapping.Controls.Add(Me.cboREMARK)
        Me.gbFieldMapping.Controls.Add(Me.cboINSTITUTE)
        Me.gbFieldMapping.Controls.Add(Me.cboSTART_DATE)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign6)
        Me.gbFieldMapping.Controls.Add(Me.cboQUALIFICATION_GROUP)
        Me.gbFieldMapping.Controls.Add(Me.lblQualificationGroup)
        Me.gbFieldMapping.Controls.Add(Me.lblRefNo)
        Me.gbFieldMapping.Controls.Add(Me.cboREF_NO)
        Me.gbFieldMapping.Controls.Add(Me.lblQualification)
        Me.gbFieldMapping.Controls.Add(Me.cboQUALIFICATION_NAME)
        Me.gbFieldMapping.Controls.Add(Me.cboECODE)
        Me.gbFieldMapping.Controls.Add(Me.cboSURNAME)
        Me.gbFieldMapping.Controls.Add(Me.lblFirstName)
        Me.gbFieldMapping.Controls.Add(Me.cboFIRSTNAME)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblLastName)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign1)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign2)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(164, 0)
        Me.gbFieldMapping.Margin = New System.Windows.Forms.Padding(0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(532, 380)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(410, 344)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(113, 20)
        Me.lnkAutoMap.TabIndex = 115
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOthResult
        '
        Me.lblOthResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOthResult.Location = New System.Drawing.Point(52, 178)
        Me.lblOthResult.Name = "lblOthResult"
        Me.lblOthResult.Size = New System.Drawing.Size(142, 17)
        Me.lblOthResult.TabIndex = 30
        Me.lblOthResult.Text = "Other Result"
        Me.lblOthResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOthQualificationGroup
        '
        Me.lblOthQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOthQualificationGroup.Location = New System.Drawing.Point(53, 124)
        Me.lblOthQualificationGroup.Name = "lblOthQualificationGroup"
        Me.lblOthQualificationGroup.Size = New System.Drawing.Size(142, 17)
        Me.lblOthQualificationGroup.TabIndex = 27
        Me.lblOthQualificationGroup.Text = "Other Qualification Group"
        Me.lblOthQualificationGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOthQualification
        '
        Me.lblOthQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOthQualification.Location = New System.Drawing.Point(53, 151)
        Me.lblOthQualification.Name = "lblOthQualification"
        Me.lblOthQualification.Size = New System.Drawing.Size(142, 17)
        Me.lblOthQualification.TabIndex = 28
        Me.lblOthQualification.Text = "Other Qualification"
        Me.lblOthQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIsSync
        '
        Me.lblIsSync.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsSync.Location = New System.Drawing.Point(51, 313)
        Me.lblIsSync.Name = "lblIsSync"
        Me.lblIsSync.Size = New System.Drawing.Size(142, 17)
        Me.lblIsSync.TabIndex = 22
        Me.lblIsSync.Text = "Is Sync with Recruitment"
        Me.lblIsSync.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboISSYNC_RECRUIT
        '
        Me.cboISSYNC_RECRUIT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboISSYNC_RECRUIT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboISSYNC_RECRUIT.FormattingEnabled = True
        Me.cboISSYNC_RECRUIT.Location = New System.Drawing.Point(201, 311)
        Me.cboISSYNC_RECRUIT.Name = "cboISSYNC_RECRUIT"
        Me.cboISSYNC_RECRUIT.Size = New System.Drawing.Size(201, 21)
        Me.cboISSYNC_RECRUIT.TabIndex = 23
        '
        'lblResult
        '
        Me.lblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(52, 178)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(142, 17)
        Me.lblResult.TabIndex = 12
        Me.lblResult.Text = "Result"
        Me.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRESULT
        '
        Me.cboRESULT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRESULT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRESULT.FormattingEnabled = True
        Me.cboRESULT.Location = New System.Drawing.Point(201, 176)
        Me.cboRESULT.Name = "cboRESULT"
        Me.cboRESULT.Size = New System.Drawing.Size(201, 21)
        Me.cboRESULT.TabIndex = 13
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(52, 340)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(142, 17)
        Me.lblRemark.TabIndex = 24
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInstitute
        '
        Me.lblInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstitute.Location = New System.Drawing.Point(52, 286)
        Me.lblInstitute.Name = "lblInstitute"
        Me.lblInstitute.Size = New System.Drawing.Size(142, 17)
        Me.lblInstitute.TabIndex = 20
        Me.lblInstitute.Text = "Institute"
        Me.lblInstitute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(52, 259)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(142, 17)
        Me.lblEndDate.TabIndex = 18
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(52, 232)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(142, 17)
        Me.lblStartDate.TabIndex = 16
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEND_DATE
        '
        Me.cboEND_DATE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEND_DATE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEND_DATE.FormattingEnabled = True
        Me.cboEND_DATE.Location = New System.Drawing.Point(201, 257)
        Me.cboEND_DATE.Name = "cboEND_DATE"
        Me.cboEND_DATE.Size = New System.Drawing.Size(201, 21)
        Me.cboEND_DATE.TabIndex = 19
        '
        'cboREMARK
        '
        Me.cboREMARK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboREMARK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboREMARK.FormattingEnabled = True
        Me.cboREMARK.Location = New System.Drawing.Point(201, 338)
        Me.cboREMARK.Name = "cboREMARK"
        Me.cboREMARK.Size = New System.Drawing.Size(201, 21)
        Me.cboREMARK.TabIndex = 25
        '
        'cboINSTITUTE
        '
        Me.cboINSTITUTE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboINSTITUTE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboINSTITUTE.FormattingEnabled = True
        Me.cboINSTITUTE.Location = New System.Drawing.Point(201, 284)
        Me.cboINSTITUTE.Name = "cboINSTITUTE"
        Me.cboINSTITUTE.Size = New System.Drawing.Size(201, 21)
        Me.cboINSTITUTE.TabIndex = 21
        '
        'cboSTART_DATE
        '
        Me.cboSTART_DATE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSTART_DATE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSTART_DATE.FormattingEnabled = True
        Me.cboSTART_DATE.Location = New System.Drawing.Point(201, 230)
        Me.cboSTART_DATE.Name = "cboSTART_DATE"
        Me.cboSTART_DATE.Size = New System.Drawing.Size(201, 21)
        Me.cboSTART_DATE.TabIndex = 17
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(271, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(258, 19)
        Me.lblCaption.TabIndex = 0
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(36, 151)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign6.TabIndex = 9
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboQUALIFICATION_GROUP
        '
        Me.cboQUALIFICATION_GROUP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQUALIFICATION_GROUP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQUALIFICATION_GROUP.FormattingEnabled = True
        Me.cboQUALIFICATION_GROUP.Location = New System.Drawing.Point(201, 122)
        Me.cboQUALIFICATION_GROUP.Name = "cboQUALIFICATION_GROUP"
        Me.cboQUALIFICATION_GROUP.Size = New System.Drawing.Size(201, 21)
        Me.cboQUALIFICATION_GROUP.TabIndex = 8
        '
        'lblQualificationGroup
        '
        Me.lblQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGroup.Location = New System.Drawing.Point(52, 124)
        Me.lblQualificationGroup.Name = "lblQualificationGroup"
        Me.lblQualificationGroup.Size = New System.Drawing.Size(142, 17)
        Me.lblQualificationGroup.TabIndex = 7
        Me.lblQualificationGroup.Text = "Qualification Group"
        Me.lblQualificationGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(52, 205)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(142, 17)
        Me.lblRefNo.TabIndex = 14
        Me.lblRefNo.Text = "Ref. No"
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboREF_NO
        '
        Me.cboREF_NO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboREF_NO.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboREF_NO.FormattingEnabled = True
        Me.cboREF_NO.Location = New System.Drawing.Point(201, 203)
        Me.cboREF_NO.Name = "cboREF_NO"
        Me.cboREF_NO.Size = New System.Drawing.Size(201, 21)
        Me.cboREF_NO.TabIndex = 15
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(52, 151)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(142, 17)
        Me.lblQualification.TabIndex = 10
        Me.lblQualification.Text = "Qualification"
        Me.lblQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQUALIFICATION_NAME
        '
        Me.cboQUALIFICATION_NAME.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQUALIFICATION_NAME.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQUALIFICATION_NAME.FormattingEnabled = True
        Me.cboQUALIFICATION_NAME.Location = New System.Drawing.Point(201, 149)
        Me.cboQUALIFICATION_NAME.Name = "cboQUALIFICATION_NAME"
        Me.cboQUALIFICATION_NAME.Size = New System.Drawing.Size(201, 21)
        Me.cboQUALIFICATION_NAME.TabIndex = 11
        '
        'cboECODE
        '
        Me.cboECODE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboECODE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboECODE.FormattingEnabled = True
        Me.cboECODE.Location = New System.Drawing.Point(201, 41)
        Me.cboECODE.Name = "cboECODE"
        Me.cboECODE.Size = New System.Drawing.Size(201, 21)
        Me.cboECODE.TabIndex = 2
        '
        'cboSURNAME
        '
        Me.cboSURNAME.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSURNAME.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSURNAME.FormattingEnabled = True
        Me.cboSURNAME.Location = New System.Drawing.Point(201, 95)
        Me.cboSURNAME.Name = "cboSURNAME"
        Me.cboSURNAME.Size = New System.Drawing.Size(201, 21)
        Me.cboSURNAME.TabIndex = 6
        '
        'lblFirstName
        '
        Me.lblFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(52, 70)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(142, 17)
        Me.lblFirstName.TabIndex = 3
        Me.lblFirstName.Text = "Firstname"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFIRSTNAME
        '
        Me.cboFIRSTNAME.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFIRSTNAME.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFIRSTNAME.FormattingEnabled = True
        Me.cboFIRSTNAME.Location = New System.Drawing.Point(201, 68)
        Me.cboFIRSTNAME.Name = "cboFIRSTNAME"
        Me.cboFIRSTNAME.Size = New System.Drawing.Size(201, 21)
        Me.cboFIRSTNAME.TabIndex = 4
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(52, 43)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(142, 17)
        Me.lblEmployeeCode.TabIndex = 1
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLastName
        '
        Me.lblLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(52, 97)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(142, 17)
        Me.lblLastName.TabIndex = 5
        Me.lblLastName.Text = "Surname"
        Me.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(36, 43)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign1.TabIndex = 0
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(36, 124)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign2.TabIndex = 3
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.gbImportType)
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage2)
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(696, 380)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'gbImportType
        '
        Me.gbImportType.Controls.Add(Me.radQualification)
        Me.gbImportType.Controls.Add(Me.radOthQualification)
        Me.gbImportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbImportType.Location = New System.Drawing.Point(179, 185)
        Me.gbImportType.Name = "gbImportType"
        Me.gbImportType.Size = New System.Drawing.Size(454, 47)
        Me.gbImportType.TabIndex = 22
        Me.gbImportType.TabStop = False
        Me.gbImportType.Text = "Import Type"
        '
        'radQualification
        '
        Me.radQualification.Location = New System.Drawing.Point(91, 19)
        Me.radQualification.Name = "radQualification"
        Me.radQualification.Size = New System.Drawing.Size(139, 17)
        Me.radQualification.TabIndex = 21
        Me.radQualification.TabStop = True
        Me.radQualification.Text = "Qualification"
        Me.radQualification.UseVisualStyleBackColor = True
        '
        'radOthQualification
        '
        Me.radOthQualification.Location = New System.Drawing.Point(236, 19)
        Me.radOthQualification.Name = "radOthQualification"
        Me.radOthQualification.Size = New System.Drawing.Size(139, 17)
        Me.radOthQualification.TabIndex = 21
        Me.radOthQualification.TabStop = True
        Me.radOthQualification.Text = "Other Qualification"
        Me.radOthQualification.UseVisualStyleBackColor = True
        '
        'lblMessage2
        '
        Me.lblMessage2.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage2.ForeColor = System.Drawing.Color.Maroon
        Me.lblMessage2.Location = New System.Drawing.Point(176, 298)
        Me.lblMessage2.Name = "lblMessage2"
        Me.lblMessage2.Size = New System.Drawing.Size(486, 55)
        Me.lblMessage2.TabIndex = 20
        Me.lblMessage2.Text = "Please set all date(s) columns in selected file as blank if not available."
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(181, 49)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(481, 45)
        Me.lblMessage.TabIndex = 18
        Me.lblMessage.Text = "This wizard will import Employee 'Qualifications' records made from other system." & _
            ""
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(181, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(481, 23)
        Me.lblTitle.TabIndex = 17
        Me.lblTitle.Text = "Employee Qualifications Import Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(639, 149)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(179, 149)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(454, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(181, 126)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(310, 20)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(696, 380)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.dgcolhQualification, Me.colhlogindate, Me.colhStatus, Me.colhMessage, Me.objcolhstatus, Me.objcolhDate})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(672, 269)
        Me.dgData.TabIndex = 19
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'dgcolhQualification
        '
        Me.dgcolhQualification.HeaderText = "Qualification"
        Me.dgcolhQualification.Name = "dgcolhQualification"
        Me.dgcolhQualification.ReadOnly = True
        Me.dgcolhQualification.Width = 180
        '
        'colhlogindate
        '
        Me.colhlogindate.HeaderText = "Date"
        Me.colhlogindate.Name = "colhlogindate"
        Me.colhlogindate.ReadOnly = True
        Me.colhlogindate.Visible = False
        Me.colhlogindate.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'objcolhDate
        '
        Me.objcolhDate.HeaderText = "objcolhDate"
        Me.objcolhDate.Name = "objcolhDate"
        Me.objcolhDate.ReadOnly = True
        Me.objcolhDate.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 344)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 22
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(672, 51)
        Me.pnlInfo.TabIndex = 2
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(550, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(436, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(550, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(482, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(595, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(436, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(595, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(482, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(303, 315)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(471, 315)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(387, 315)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'frmImportQualificationWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(696, 428)
        Me.Controls.Add(Me.eZeeWizImportEmp)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportQualificationWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Employee Qualifications Wizard"
        Me.eZeeWizImportEmp.ResumeLayout(False)
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.gbImportType.ResumeLayout(False)
        Me.WizPageImporting.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeWizImportEmp As eZee.Common.eZeeWizard
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents cboQUALIFICATION_GROUP As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualificationGroup As System.Windows.Forms.Label
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents cboREF_NO As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents cboQUALIFICATION_NAME As System.Windows.Forms.ComboBox
    Friend WithEvents cboECODE As System.Windows.Forms.ComboBox
    Friend WithEvents cboSURNAME As System.Windows.Forms.ComboBox
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents cboFIRSTNAME As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents cboEND_DATE As System.Windows.Forms.ComboBox
    Friend WithEvents cboREMARK As System.Windows.Forms.ComboBox
    Friend WithEvents cboINSTITUTE As System.Windows.Forms.ComboBox
    Friend WithEvents cboSTART_DATE As System.Windows.Forms.ComboBox
    Friend WithEvents lblMessage2 As System.Windows.Forms.Label
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhlogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents lblInstitute As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radQualification As System.Windows.Forms.RadioButton
    Friend WithEvents radOthQualification As System.Windows.Forms.RadioButton
    Friend WithEvents gbImportType As System.Windows.Forms.GroupBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents cboRESULT As System.Windows.Forms.ComboBox
    Friend WithEvents lblIsSync As System.Windows.Forms.Label
    Friend WithEvents cboISSYNC_RECRUIT As System.Windows.Forms.ComboBox
    Friend WithEvents lblOthQualificationGroup As System.Windows.Forms.Label
    Friend WithEvents lblOthQualification As System.Windows.Forms.Label
    Friend WithEvents lblOthResult As System.Windows.Forms.Label
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
End Class
