﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmpDayOffList

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmEmpDayOffList"
    Private mstrAdvanceFilter As String = ""
    Private objDayOff As New clsemployee_dayoff_Tran
    Private mDicUsed As New Dictionary(Of Integer, Integer)

#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim objEMaster As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If

            dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objEMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Try
            RemoveHandler lvDayOff.ItemChecked, AddressOf lvDayOff_ItemChecked
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If User._Object.Privilege._AllowToViewEmployeeDayOff = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End

            'Pinkal (17-Jan-2017) -- Start
            'Enhancement - Working on DAYOFF Error given by AMOS. 
            'dsList = objDayOff.GetList("List", mstrAdvanceFilter, CInt(cboEmployee.SelectedValue), dtpDate1.Value.Date, dtpDate2.Value.Date)
            dsList = objDayOff.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                  , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                  , mstrAdvanceFilter, CInt(cboEmployee.SelectedValue), dtpDate1.Value.Date, dtpDate2.Value.Date)
            'Pinkal (17-Jan-2017) -- End
            lvDayOff.Items.Clear()
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables("List").Rows
                    Dim lvItem As New ListViewItem

                    lvItem.Text = ""
                    lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("dayoffdate").ToString).ToShortDateString)
                    lvItem.SubItems.Add(dRow.Item("employeename").ToString)
                    lvItem.SubItems.Add(dRow.Item("employeeunkid").ToString)
                    lvItem.Tag = dRow.Item("empdayofftranunkid")


                    If mDicUsed.Keys.Count > 0 Then
                        If mDicUsed.ContainsKey(CInt(dRow.Item("empdayofftranunkid").ToString)) Then
                            lvItem.ForeColor = Color.Blue
                        End If
                    End If

                    lvDayOff.Items.Add(lvItem)
                Next
                lvDayOff.GroupingColumn = objcolhEmp
                lvDayOff.DisplayGroups(True)
                If lvDayOff.Groups.Count > 2 Then
                    colhDate.Width = 354 - 20
                Else
                    colhDate.Width = 354
                End If
            End If

            If mDicUsed.Keys.Count > 0 Then mDicUsed.Clear()

            AddHandler lvDayOff.ItemChecked, AddressOf lvDayOff_ItemChecked
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            btnNew.Enabled = User._Object.Privilege._AllowToAssignEmployeeDayOff
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteEmployeeDayOff
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmpDayOffList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDayOff = New clsemployee_dayoff_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpDayOffList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEmpDayOffList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvDayOff.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmEmpDayOffList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAssignDayOff
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvDayOff.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one item in order to delete from list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim iVoidReason As String = String.Empty
            Dim frm As New frmReasonSelection

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(enVoidCategoryType.EMPLOYEE, iVoidReason)
            If iVoidReason.Trim.Length <= 0 Then Exit Sub

            Dim blnUsed As Boolean = False : Dim blnDeleted As Boolean = False
            mDicUsed = New Dictionary(Of Integer, Integer)
            For Each lvItem As ListViewItem In lvDayOff.CheckedItems
                If objDayOff.isUsed(CInt(lvItem.SubItems(objcolhEmpId.Index).Text), CDate(lvItem.SubItems(colhDate.Index).Text)) = False Then
                    objDayOff._Isvoid = True
                    objDayOff._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objDayOff._Voidreason = iVoidReason
                    objDayOff._Voiduserunkid = User._Object._Userunkid
                    If objDayOff.Delete(CInt(lvItem.Tag), CInt(lvItem.SubItems(objcolhEmpId.Index).Text)) = False Then
                        eZeeMsgBox.Show(objDayOff._Message, enMsgBoxStyle.Information)
                        lvItem.ForeColor = Color.Red
                        Exit Sub
                    End If
                Else
                    If mDicUsed.ContainsKey(CInt(lvItem.Tag)) = False Then
                        mDicUsed.Add(CInt(lvItem.Tag), CInt(lvItem.Tag))
                    End If
                    blnUsed = True
                    Continue For
                End If
            Next
            If blnUsed = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "There are some checked employee(s) transactions already linked with time and attendance, due to this employee will not be deleted, and these employee are highlighted in blue."), enMsgBoxStyle.Information)
            End If
            objchkAll.Checked = False
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            dtpDate1.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDate2.Value = ConfigParameter._Object._CurrentDateAndTime
            lvDayOff.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvDayOff.ItemChecked, AddressOf lvDayOff_ItemChecked
            For Each lvItem As ListViewItem In lvDayOff.Items
                lvItem.Checked = CBool(objchkAll.CheckState)
            Next
            AddHandler lvDayOff.ItemChecked, AddressOf lvDayOff_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvDayOff_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDayOff.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvDayOff.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvDayOff.CheckedItems.Count < lvDayOff.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvDayOff.CheckedItems.Count = lvDayOff.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDayOff_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please check atleast one item in order to delete from list.")
			Language.setMessage(mstrModuleName, 2, "There are some checked employee(s) transactions already linked with time and attendance, due to this employee will not be deleted, and these employee are highlighted in blue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

  
End Class