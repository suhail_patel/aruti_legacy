﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBulkMovementUpdateWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBulkMovementUpdateWizard))
        Me.WizUpdateEmployee = New eZee.Common.eZeeWizard
        Me.wizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFiledMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.objpnlCtrls = New System.Windows.Forms.Panel
        Me.objlblSign51 = New System.Windows.Forms.Label
        Me.objlblSign49 = New System.Windows.Forms.Label
        Me.lblExemptionToDt = New System.Windows.Forms.Label
        Me.cboExemptionToDate = New System.Windows.Forms.ComboBox
        Me.lblExemptionFromDt = New System.Windows.Forms.Label
        Me.cboExemptionFromDate = New System.Windows.Forms.ComboBox
        Me.cboExemptionChangeReason = New System.Windows.Forms.ComboBox
        Me.lblExemptionReason = New System.Windows.Forms.Label
        Me.objlblSign50 = New System.Windows.Forms.Label
        Me.EZeeLine7 = New eZee.Common.eZeeLine
        Me.objlblSign48 = New System.Windows.Forms.Label
        Me.lblRpIssuePlace = New System.Windows.Forms.Label
        Me.cboResidentIssuePlace = New System.Windows.Forms.ComboBox
        Me.objlblSign47 = New System.Windows.Forms.Label
        Me.lblWpIssuePlace = New System.Windows.Forms.Label
        Me.objlblSign46 = New System.Windows.Forms.Label
        Me.cboWorkPermitIssuePlace = New System.Windows.Forms.ComboBox
        Me.objlblSign45 = New System.Windows.Forms.Label
        Me.lblRPCountry = New System.Windows.Forms.Label
        Me.cboResidentCountry = New System.Windows.Forms.ComboBox
        Me.objlblSign44 = New System.Windows.Forms.Label
        Me.lblRPExpiryDate = New System.Windows.Forms.Label
        Me.cboResidentExpiryDate = New System.Windows.Forms.ComboBox
        Me.objlblSign43 = New System.Windows.Forms.Label
        Me.lblRPIssueDate = New System.Windows.Forms.Label
        Me.cboResidentIssueDate = New System.Windows.Forms.ComboBox
        Me.objlblSign41 = New System.Windows.Forms.Label
        Me.lblRPno = New System.Windows.Forms.Label
        Me.cboResidentPermitNo = New System.Windows.Forms.ComboBox
        Me.cboResidentChangeReason = New System.Windows.Forms.ComboBox
        Me.lblRPReason = New System.Windows.Forms.Label
        Me.objlblSign42 = New System.Windows.Forms.Label
        Me.EZeeLine6 = New eZee.Common.eZeeLine
        Me.objlblSign40 = New System.Windows.Forms.Label
        Me.lblWPCountry = New System.Windows.Forms.Label
        Me.cboWorkPermitCountry = New System.Windows.Forms.ComboBox
        Me.objlblSign39 = New System.Windows.Forms.Label
        Me.lblWPExpiryDate = New System.Windows.Forms.Label
        Me.cboWorkPermitExpiryDate = New System.Windows.Forms.ComboBox
        Me.objlblSign38 = New System.Windows.Forms.Label
        Me.lblWPIssueDt = New System.Windows.Forms.Label
        Me.cboWorkPermitIssueDate = New System.Windows.Forms.ComboBox
        Me.objlblSign36 = New System.Windows.Forms.Label
        Me.lblWPno = New System.Windows.Forms.Label
        Me.cboWorkPermitNo = New System.Windows.Forms.ComboBox
        Me.cboWorkPermitChangeReason = New System.Windows.Forms.ComboBox
        Me.lblWPReason = New System.Windows.Forms.Label
        Me.objlblSign37 = New System.Windows.Forms.Label
        Me.EZeeLine5 = New eZee.Common.eZeeLine
        Me.objlblSign35 = New System.Windows.Forms.Label
        Me.lblSuspToDt = New System.Windows.Forms.Label
        Me.cboSuspensionToDate = New System.Windows.Forms.ComboBox
        Me.objlblSign33 = New System.Windows.Forms.Label
        Me.lblSuspFromDt = New System.Windows.Forms.Label
        Me.cboSuspensionFromDate = New System.Windows.Forms.ComboBox
        Me.cboSuspensionChangeReason = New System.Windows.Forms.ComboBox
        Me.lblSuspensionReason = New System.Windows.Forms.Label
        Me.objlblSign34 = New System.Windows.Forms.Label
        Me.EZeeLine4 = New eZee.Common.eZeeLine
        Me.objlblSign31 = New System.Windows.Forms.Label
        Me.lbConfirmationDt = New System.Windows.Forms.Label
        Me.cboConfirmationDate = New System.Windows.Forms.ComboBox
        Me.cboConfirmationChangeReason = New System.Windows.Forms.ComboBox
        Me.lblConfirmationReason = New System.Windows.Forms.Label
        Me.objlblSign32 = New System.Windows.Forms.Label
        Me.EZeeLine3 = New eZee.Common.eZeeLine
        Me.objlblSign30 = New System.Windows.Forms.Label
        Me.lblProbationToDt = New System.Windows.Forms.Label
        Me.cboProbationToDate = New System.Windows.Forms.ComboBox
        Me.objlblSign28 = New System.Windows.Forms.Label
        Me.lblProbationFromDt = New System.Windows.Forms.Label
        Me.cboProbationFromDate = New System.Windows.Forms.ComboBox
        Me.cboProbationChangeReason = New System.Windows.Forms.ComboBox
        Me.lblPbReason = New System.Windows.Forms.Label
        Me.objlblSign29 = New System.Windows.Forms.Label
        Me.EZeeLine2 = New eZee.Common.eZeeLine
        Me.objlblSign13 = New System.Windows.Forms.Label
        Me.cboIsExcludePayroll = New System.Windows.Forms.ComboBox
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.lblIsexcludePayroll = New System.Windows.Forms.Label
        Me.lblRetirementDt = New System.Windows.Forms.Label
        Me.objlblSign27 = New System.Windows.Forms.Label
        Me.cboRetirementDate = New System.Windows.Forms.ComboBox
        Me.cboRetirementChangeReason = New System.Windows.Forms.ComboBox
        Me.objlblSign5 = New System.Windows.Forms.Label
        Me.lblRtReason = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.objlblSign26 = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.cboCostCenterChangeReason = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblCCReason = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.objlblSign25 = New System.Windows.Forms.Label
        Me.objlblSign7 = New System.Windows.Forms.Label
        Me.cboCategorizeChangeReason = New System.Windows.Forms.ComboBox
        Me.lblClassGrp = New System.Windows.Forms.Label
        Me.lblJobReason = New System.Windows.Forms.Label
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.objlblSign24 = New System.Windows.Forms.Label
        Me.objlblSign8 = New System.Windows.Forms.Label
        Me.cboAllocationChangeReason = New System.Windows.Forms.ComboBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.lblAllocationReason = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.objlblSign23 = New System.Windows.Forms.Label
        Me.objlblSign11 = New System.Windows.Forms.Label
        Me.lblECODate = New System.Windows.Forms.Label
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.cboEOCDate = New System.Windows.Forms.ComboBox
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.objlblSign12 = New System.Windows.Forms.Label
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.lblLeavingDate = New System.Windows.Forms.Label
        Me.cboTerminationReason = New System.Windows.Forms.ComboBox
        Me.cboLeavingDate = New System.Windows.Forms.ComboBox
        Me.lblEOCReason = New System.Windows.Forms.Label
        Me.lblBranch = New System.Windows.Forms.Label
        Me.objlblSign22 = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.objlblSign14 = New System.Windows.Forms.Label
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.lblDepGroup = New System.Windows.Forms.Label
        Me.objlblSign21 = New System.Windows.Forms.Label
        Me.cboDepartmentGroup = New System.Windows.Forms.ComboBox
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.objlblSign15 = New System.Windows.Forms.Label
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.lblSecGroup = New System.Windows.Forms.Label
        Me.objlblSign20 = New System.Windows.Forms.Label
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.cboTeam = New System.Windows.Forms.ComboBox
        Me.objlblSign16 = New System.Windows.Forms.Label
        Me.lblTeam = New System.Windows.Forms.Label
        Me.lblSections = New System.Windows.Forms.Label
        Me.objlblSign19 = New System.Windows.Forms.Label
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.objlblSign17 = New System.Windows.Forms.Label
        Me.lblUnit = New System.Windows.Forms.Label
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.objlblSign18 = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.objelLine4 = New eZee.Common.eZeeLine
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblCaption = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.wizPageFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.fpnlData = New System.Windows.Forms.FlowLayoutPanel
        Me.gbUpdateAllocation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocationFormat = New System.Windows.Forms.LinkLabel
        Me.chkAllocationChangeReson = New System.Windows.Forms.CheckBox
        Me.dtpAllocEffDate = New System.Windows.Forms.DateTimePicker
        Me.objlblAllocationCaption = New System.Windows.Forms.Label
        Me.chkBranch = New System.Windows.Forms.CheckBox
        Me.chkDepartment = New System.Windows.Forms.CheckBox
        Me.chkDepGroup = New System.Windows.Forms.CheckBox
        Me.chkSecGroup = New System.Windows.Forms.CheckBox
        Me.chkSections = New System.Windows.Forms.CheckBox
        Me.chkUnitGroup = New System.Windows.Forms.CheckBox
        Me.chkClassGrp = New System.Windows.Forms.CheckBox
        Me.chkUnit = New System.Windows.Forms.CheckBox
        Me.chkTeam = New System.Windows.Forms.CheckBox
        Me.chkClass = New System.Windows.Forms.CheckBox
        Me.gbCostCenter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkCostCenterFormat = New System.Windows.Forms.LinkLabel
        Me.chkCcChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpCCEffDate = New System.Windows.Forms.DateTimePicker
        Me.objlblCCCaption = New System.Windows.Forms.Label
        Me.chkCostCenter = New System.Windows.Forms.CheckBox
        Me.gbCategorization = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkCategorizeFormat = New System.Windows.Forms.LinkLabel
        Me.chkCateChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpCatEffDate = New System.Windows.Forms.DateTimePicker
        Me.objlbJobCaption = New System.Windows.Forms.Label
        Me.chkJobGroup = New System.Windows.Forms.CheckBox
        Me.chkJob = New System.Windows.Forms.CheckBox
        Me.gbEOCInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkTerminationFormat = New System.Windows.Forms.LinkLabel
        Me.chkIsexcludePayroll = New System.Windows.Forms.CheckBox
        Me.dtpTerEffDate = New System.Windows.Forms.DateTimePicker
        Me.objlblEOCCaption = New System.Windows.Forms.Label
        Me.chkEOCDate = New System.Windows.Forms.CheckBox
        Me.chkEOCReason = New System.Windows.Forms.CheckBox
        Me.chkLeavingDate = New System.Windows.Forms.CheckBox
        Me.gbRetirementInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkRetirementFormat = New System.Windows.Forms.LinkLabel
        Me.chkRtChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpRetrEffDate = New System.Windows.Forms.DateTimePicker
        Me.chkRetirementDt = New System.Windows.Forms.CheckBox
        Me.objlblRetireCaption = New System.Windows.Forms.Label
        Me.gbProbationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkProbationToDt = New System.Windows.Forms.CheckBox
        Me.lnkProbationFormat = New System.Windows.Forms.LinkLabel
        Me.chkPbChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpPbEffDate = New System.Windows.Forms.DateTimePicker
        Me.chkProbationFromDt = New System.Windows.Forms.CheckBox
        Me.objlblProbationCaption = New System.Windows.Forms.Label
        Me.gbConfirmationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkConfirmationFormat = New System.Windows.Forms.LinkLabel
        Me.chkConfChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpConEffDate = New System.Windows.Forms.DateTimePicker
        Me.chkConfirmationDt = New System.Windows.Forms.CheckBox
        Me.objlblConfirmationCaption = New System.Windows.Forms.Label
        Me.gbSuspensionInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkSuspensionToDt = New System.Windows.Forms.CheckBox
        Me.lnkSuspensionFormat = New System.Windows.Forms.LinkLabel
        Me.chkSuspChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpSuspEffDate = New System.Windows.Forms.DateTimePicker
        Me.chkSuspensionFrmDt = New System.Windows.Forms.CheckBox
        Me.objlblSuspensionCaption = New System.Windows.Forms.Label
        Me.gbWorkPermitInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkWpIssuePlace = New System.Windows.Forms.CheckBox
        Me.chkWpCountry = New System.Windows.Forms.CheckBox
        Me.chkWpExpiryDt = New System.Windows.Forms.CheckBox
        Me.chkWpIssueDt = New System.Windows.Forms.CheckBox
        Me.lnkWorkPermitFormat = New System.Windows.Forms.LinkLabel
        Me.chkWpChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpWpEffDate = New System.Windows.Forms.DateTimePicker
        Me.ChkWorkPermitNo = New System.Windows.Forms.CheckBox
        Me.objlblWorkPermitCaption = New System.Windows.Forms.Label
        Me.gbResidentPermitInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkRpIssuePlace = New System.Windows.Forms.CheckBox
        Me.chkRpCountry = New System.Windows.Forms.CheckBox
        Me.chkRpExpiryDt = New System.Windows.Forms.CheckBox
        Me.chkRpIssueDt = New System.Windows.Forms.CheckBox
        Me.lnkResidentPermitFormat = New System.Windows.Forms.LinkLabel
        Me.chkRpChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpRpEffDate = New System.Windows.Forms.DateTimePicker
        Me.ChkResidentPermitNo = New System.Windows.Forms.CheckBox
        Me.objlblResidentPermitCaption = New System.Windows.Forms.Label
        Me.gbExemptionInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkExemptionToDt = New System.Windows.Forms.CheckBox
        Me.lnkExemptionFormat = New System.Windows.Forms.LinkLabel
        Me.chkExemptionChangeReason = New System.Windows.Forms.CheckBox
        Me.dtpExemptionEffDate = New System.Windows.Forms.DateTimePicker
        Me.chkExemptionFrmDt = New System.Windows.Forms.CheckBox
        Me.objlblExemptionCaption = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblTitle = New System.Windows.Forms.Label
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.wizPageData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.objbuttonSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.WizUpdateEmployee.SuspendLayout()
        Me.wizPageMapping.SuspendLayout()
        Me.gbFiledMapping.SuspendLayout()
        Me.objpnlCtrls.SuspendLayout()
        Me.wizPageFile.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.fpnlData.SuspendLayout()
        Me.gbUpdateAllocation.SuspendLayout()
        Me.gbCostCenter.SuspendLayout()
        Me.gbCategorization.SuspendLayout()
        Me.gbEOCInfo.SuspendLayout()
        Me.gbRetirementInfo.SuspendLayout()
        Me.gbProbationInfo.SuspendLayout()
        Me.gbConfirmationInfo.SuspendLayout()
        Me.gbSuspensionInfo.SuspendLayout()
        Me.gbWorkPermitInfo.SuspendLayout()
        Me.gbResidentPermitInfo.SuspendLayout()
        Me.gbExemptionInfo.SuspendLayout()
        Me.wizPageData.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'WizUpdateEmployee
        '
        Me.WizUpdateEmployee.Controls.Add(Me.wizPageMapping)
        Me.WizUpdateEmployee.Controls.Add(Me.wizPageFile)
        Me.WizUpdateEmployee.Controls.Add(Me.wizPageData)
        Me.WizUpdateEmployee.Controls.Add(Me.objbuttonSave)
        Me.WizUpdateEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizUpdateEmployee.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WizUpdateEmployee.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.WizUpdateEmployee.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.WizUpdateEmployee.Location = New System.Drawing.Point(0, 0)
        Me.WizUpdateEmployee.Name = "WizUpdateEmployee"
        Me.WizUpdateEmployee.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizPageFile, Me.wizPageMapping, Me.wizPageData})
        Me.WizUpdateEmployee.SaveEnabled = True
        Me.WizUpdateEmployee.SaveText = "Save && Finish"
        Me.WizUpdateEmployee.SaveVisible = False
        Me.WizUpdateEmployee.SetSaveIndexBeforeFinishIndex = False
        Me.WizUpdateEmployee.Size = New System.Drawing.Size(770, 518)
        Me.WizUpdateEmployee.TabIndex = 0
        Me.WizUpdateEmployee.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WizUpdateEmployee.WelcomeImage = Nothing
        Me.WizUpdateEmployee.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wizPageMapping
        '
        Me.wizPageMapping.Controls.Add(Me.gbFiledMapping)
        Me.wizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.wizPageMapping.Name = "wizPageMapping"
        Me.wizPageMapping.Size = New System.Drawing.Size(770, 470)
        Me.wizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageMapping.TabIndex = 8
        '
        'gbFiledMapping
        '
        Me.gbFiledMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFiledMapping.Checked = False
        Me.gbFiledMapping.CollapseAllExceptThis = False
        Me.gbFiledMapping.CollapsedHoverImage = Nothing
        Me.gbFiledMapping.CollapsedNormalImage = Nothing
        Me.gbFiledMapping.CollapsedPressedImage = Nothing
        Me.gbFiledMapping.CollapseOnLoad = False
        Me.gbFiledMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFiledMapping.Controls.Add(Me.objpnlCtrls)
        Me.gbFiledMapping.Controls.Add(Me.objelLine4)
        Me.gbFiledMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.lblCaption)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign1)
        Me.gbFiledMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFiledMapping.ExpandedHoverImage = Nothing
        Me.gbFiledMapping.ExpandedNormalImage = Nothing
        Me.gbFiledMapping.ExpandedPressedImage = Nothing
        Me.gbFiledMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFiledMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFiledMapping.HeaderHeight = 25
        Me.gbFiledMapping.HeaderMessage = ""
        Me.gbFiledMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFiledMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFiledMapping.HeightOnCollapse = 0
        Me.gbFiledMapping.LeftTextSpace = 0
        Me.gbFiledMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFiledMapping.Name = "gbFiledMapping"
        Me.gbFiledMapping.OpenHeight = 300
        Me.gbFiledMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFiledMapping.ShowBorder = True
        Me.gbFiledMapping.ShowCheckBox = False
        Me.gbFiledMapping.ShowCollapseButton = False
        Me.gbFiledMapping.ShowDefaultBorderColor = True
        Me.gbFiledMapping.ShowDownButton = False
        Me.gbFiledMapping.ShowHeader = True
        Me.gbFiledMapping.Size = New System.Drawing.Size(604, 469)
        Me.gbFiledMapping.TabIndex = 0
        Me.gbFiledMapping.Temp = 0
        Me.gbFiledMapping.Text = "Field Mapping"
        Me.gbFiledMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(435, 36)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(135, 20)
        Me.lnkAutoMap.TabIndex = 77
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objpnlCtrls
        '
        Me.objpnlCtrls.AutoScroll = True
        Me.objpnlCtrls.Controls.Add(Me.objlblSign51)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign49)
        Me.objpnlCtrls.Controls.Add(Me.lblExemptionToDt)
        Me.objpnlCtrls.Controls.Add(Me.cboExemptionToDate)
        Me.objpnlCtrls.Controls.Add(Me.lblExemptionFromDt)
        Me.objpnlCtrls.Controls.Add(Me.cboExemptionFromDate)
        Me.objpnlCtrls.Controls.Add(Me.cboExemptionChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblExemptionReason)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign50)
        Me.objpnlCtrls.Controls.Add(Me.EZeeLine7)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign48)
        Me.objpnlCtrls.Controls.Add(Me.lblRpIssuePlace)
        Me.objpnlCtrls.Controls.Add(Me.cboResidentIssuePlace)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign47)
        Me.objpnlCtrls.Controls.Add(Me.lblWpIssuePlace)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign46)
        Me.objpnlCtrls.Controls.Add(Me.cboWorkPermitIssuePlace)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign45)
        Me.objpnlCtrls.Controls.Add(Me.lblRPCountry)
        Me.objpnlCtrls.Controls.Add(Me.cboResidentCountry)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign44)
        Me.objpnlCtrls.Controls.Add(Me.lblRPExpiryDate)
        Me.objpnlCtrls.Controls.Add(Me.cboResidentExpiryDate)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign43)
        Me.objpnlCtrls.Controls.Add(Me.lblRPIssueDate)
        Me.objpnlCtrls.Controls.Add(Me.cboResidentIssueDate)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign41)
        Me.objpnlCtrls.Controls.Add(Me.lblRPno)
        Me.objpnlCtrls.Controls.Add(Me.cboResidentPermitNo)
        Me.objpnlCtrls.Controls.Add(Me.cboResidentChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblRPReason)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign42)
        Me.objpnlCtrls.Controls.Add(Me.EZeeLine6)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign40)
        Me.objpnlCtrls.Controls.Add(Me.lblWPCountry)
        Me.objpnlCtrls.Controls.Add(Me.cboWorkPermitCountry)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign39)
        Me.objpnlCtrls.Controls.Add(Me.lblWPExpiryDate)
        Me.objpnlCtrls.Controls.Add(Me.cboWorkPermitExpiryDate)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign38)
        Me.objpnlCtrls.Controls.Add(Me.lblWPIssueDt)
        Me.objpnlCtrls.Controls.Add(Me.cboWorkPermitIssueDate)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign36)
        Me.objpnlCtrls.Controls.Add(Me.lblWPno)
        Me.objpnlCtrls.Controls.Add(Me.cboWorkPermitNo)
        Me.objpnlCtrls.Controls.Add(Me.cboWorkPermitChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblWPReason)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign37)
        Me.objpnlCtrls.Controls.Add(Me.EZeeLine5)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign35)
        Me.objpnlCtrls.Controls.Add(Me.lblSuspToDt)
        Me.objpnlCtrls.Controls.Add(Me.cboSuspensionToDate)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign33)
        Me.objpnlCtrls.Controls.Add(Me.lblSuspFromDt)
        Me.objpnlCtrls.Controls.Add(Me.cboSuspensionFromDate)
        Me.objpnlCtrls.Controls.Add(Me.cboSuspensionChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblSuspensionReason)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign34)
        Me.objpnlCtrls.Controls.Add(Me.EZeeLine4)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign31)
        Me.objpnlCtrls.Controls.Add(Me.lbConfirmationDt)
        Me.objpnlCtrls.Controls.Add(Me.cboConfirmationDate)
        Me.objpnlCtrls.Controls.Add(Me.cboConfirmationChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblConfirmationReason)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign32)
        Me.objpnlCtrls.Controls.Add(Me.EZeeLine3)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign30)
        Me.objpnlCtrls.Controls.Add(Me.lblProbationToDt)
        Me.objpnlCtrls.Controls.Add(Me.cboProbationToDate)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign28)
        Me.objpnlCtrls.Controls.Add(Me.lblProbationFromDt)
        Me.objpnlCtrls.Controls.Add(Me.cboProbationFromDate)
        Me.objpnlCtrls.Controls.Add(Me.cboProbationChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblPbReason)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign29)
        Me.objpnlCtrls.Controls.Add(Me.EZeeLine2)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign13)
        Me.objpnlCtrls.Controls.Add(Me.cboIsExcludePayroll)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign4)
        Me.objpnlCtrls.Controls.Add(Me.lblIsexcludePayroll)
        Me.objpnlCtrls.Controls.Add(Me.lblRetirementDt)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign27)
        Me.objpnlCtrls.Controls.Add(Me.cboRetirementDate)
        Me.objpnlCtrls.Controls.Add(Me.cboRetirementChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign5)
        Me.objpnlCtrls.Controls.Add(Me.lblRtReason)
        Me.objpnlCtrls.Controls.Add(Me.lblDepartment)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign26)
        Me.objpnlCtrls.Controls.Add(Me.cboDepartment)
        Me.objpnlCtrls.Controls.Add(Me.EZeeLine1)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign6)
        Me.objpnlCtrls.Controls.Add(Me.cboCostCenterChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblJob)
        Me.objpnlCtrls.Controls.Add(Me.lblCCReason)
        Me.objpnlCtrls.Controls.Add(Me.cboJob)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign25)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign7)
        Me.objpnlCtrls.Controls.Add(Me.cboCategorizeChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblClassGrp)
        Me.objpnlCtrls.Controls.Add(Me.lblJobReason)
        Me.objpnlCtrls.Controls.Add(Me.cboClassGroup)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign24)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign8)
        Me.objpnlCtrls.Controls.Add(Me.cboAllocationChangeReason)
        Me.objpnlCtrls.Controls.Add(Me.lblClass)
        Me.objpnlCtrls.Controls.Add(Me.lblAllocationReason)
        Me.objpnlCtrls.Controls.Add(Me.cboClass)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign23)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign11)
        Me.objpnlCtrls.Controls.Add(Me.lblECODate)
        Me.objpnlCtrls.Controls.Add(Me.objelLine3)
        Me.objpnlCtrls.Controls.Add(Me.cboEOCDate)
        Me.objpnlCtrls.Controls.Add(Me.objelLine2)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign12)
        Me.objpnlCtrls.Controls.Add(Me.objelLine1)
        Me.objpnlCtrls.Controls.Add(Me.lblLeavingDate)
        Me.objpnlCtrls.Controls.Add(Me.cboTerminationReason)
        Me.objpnlCtrls.Controls.Add(Me.cboLeavingDate)
        Me.objpnlCtrls.Controls.Add(Me.lblEOCReason)
        Me.objpnlCtrls.Controls.Add(Me.lblBranch)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign22)
        Me.objpnlCtrls.Controls.Add(Me.cboBranch)
        Me.objpnlCtrls.Controls.Add(Me.cboCostCenter)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign14)
        Me.objpnlCtrls.Controls.Add(Me.lblCostCenter)
        Me.objpnlCtrls.Controls.Add(Me.lblDepGroup)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign21)
        Me.objpnlCtrls.Controls.Add(Me.cboDepartmentGroup)
        Me.objpnlCtrls.Controls.Add(Me.cboJobGroup)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign15)
        Me.objpnlCtrls.Controls.Add(Me.lblJobGroup)
        Me.objpnlCtrls.Controls.Add(Me.lblSecGroup)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign20)
        Me.objpnlCtrls.Controls.Add(Me.cboSectionGroup)
        Me.objpnlCtrls.Controls.Add(Me.cboTeam)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign16)
        Me.objpnlCtrls.Controls.Add(Me.lblTeam)
        Me.objpnlCtrls.Controls.Add(Me.lblSections)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign19)
        Me.objpnlCtrls.Controls.Add(Me.cboSection)
        Me.objpnlCtrls.Controls.Add(Me.cboUnit)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign17)
        Me.objpnlCtrls.Controls.Add(Me.lblUnit)
        Me.objpnlCtrls.Controls.Add(Me.lblUnitGroup)
        Me.objpnlCtrls.Controls.Add(Me.objlblSign18)
        Me.objpnlCtrls.Controls.Add(Me.cboUnitGroup)
        Me.objpnlCtrls.Location = New System.Drawing.Point(6, 68)
        Me.objpnlCtrls.Name = "objpnlCtrls"
        Me.objpnlCtrls.Size = New System.Drawing.Size(597, 401)
        Me.objpnlCtrls.TabIndex = 75
        '
        'objlblSign51
        '
        Me.objlblSign51.Enabled = False
        Me.objlblSign51.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign51.ForeColor = System.Drawing.Color.Red
        Me.objlblSign51.Location = New System.Drawing.Point(3, 752)
        Me.objlblSign51.Name = "objlblSign51"
        Me.objlblSign51.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign51.TabIndex = 152
        Me.objlblSign51.Text = "*"
        Me.objlblSign51.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign49
        '
        Me.objlblSign49.Enabled = False
        Me.objlblSign49.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign49.ForeColor = System.Drawing.Color.Red
        Me.objlblSign49.Location = New System.Drawing.Point(3, 726)
        Me.objlblSign49.Name = "objlblSign49"
        Me.objlblSign49.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign49.TabIndex = 151
        Me.objlblSign49.Text = "*"
        Me.objlblSign49.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblExemptionToDt
        '
        Me.lblExemptionToDt.Enabled = False
        Me.lblExemptionToDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExemptionToDt.Location = New System.Drawing.Point(28, 750)
        Me.lblExemptionToDt.Name = "lblExemptionToDt"
        Me.lblExemptionToDt.Size = New System.Drawing.Size(92, 17)
        Me.lblExemptionToDt.TabIndex = 150
        Me.lblExemptionToDt.Text = "Exemption To Date"
        Me.lblExemptionToDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExemptionToDate
        '
        Me.cboExemptionToDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExemptionToDate.Enabled = False
        Me.cboExemptionToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExemptionToDate.FormattingEnabled = True
        Me.cboExemptionToDate.Location = New System.Drawing.Point(142, 755)
        Me.cboExemptionToDate.Name = "cboExemptionToDate"
        Me.cboExemptionToDate.Size = New System.Drawing.Size(135, 21)
        Me.cboExemptionToDate.TabIndex = 149
        '
        'lblExemptionFromDt
        '
        Me.lblExemptionFromDt.Enabled = False
        Me.lblExemptionFromDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExemptionFromDt.Location = New System.Drawing.Point(28, 726)
        Me.lblExemptionFromDt.Name = "lblExemptionFromDt"
        Me.lblExemptionFromDt.Size = New System.Drawing.Size(108, 17)
        Me.lblExemptionFromDt.TabIndex = 146
        Me.lblExemptionFromDt.Text = "Exemption From Date"
        Me.lblExemptionFromDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExemptionFromDate
        '
        Me.cboExemptionFromDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExemptionFromDate.Enabled = False
        Me.cboExemptionFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExemptionFromDate.FormattingEnabled = True
        Me.cboExemptionFromDate.Location = New System.Drawing.Point(142, 727)
        Me.cboExemptionFromDate.Name = "cboExemptionFromDate"
        Me.cboExemptionFromDate.Size = New System.Drawing.Size(135, 21)
        Me.cboExemptionFromDate.TabIndex = 143
        '
        'cboExemptionChangeReason
        '
        Me.cboExemptionChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExemptionChangeReason.Enabled = False
        Me.cboExemptionChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExemptionChangeReason.FormattingEnabled = True
        Me.cboExemptionChangeReason.Location = New System.Drawing.Point(431, 731)
        Me.cboExemptionChangeReason.Name = "cboExemptionChangeReason"
        Me.cboExemptionChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboExemptionChangeReason.TabIndex = 144
        '
        'lblExemptionReason
        '
        Me.lblExemptionReason.Enabled = False
        Me.lblExemptionReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExemptionReason.Location = New System.Drawing.Point(332, 733)
        Me.lblExemptionReason.Name = "lblExemptionReason"
        Me.lblExemptionReason.Size = New System.Drawing.Size(92, 17)
        Me.lblExemptionReason.TabIndex = 148
        Me.lblExemptionReason.Text = "Change Reason"
        Me.lblExemptionReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign50
        '
        Me.objlblSign50.Enabled = False
        Me.objlblSign50.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign50.ForeColor = System.Drawing.Color.Red
        Me.objlblSign50.Location = New System.Drawing.Point(309, 733)
        Me.objlblSign50.Name = "objlblSign50"
        Me.objlblSign50.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign50.TabIndex = 147
        Me.objlblSign50.Text = "*"
        Me.objlblSign50.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'EZeeLine7
        '
        Me.EZeeLine7.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine7.Location = New System.Drawing.Point(5, 715)
        Me.EZeeLine7.Name = "EZeeLine7"
        Me.EZeeLine7.Size = New System.Drawing.Size(571, 9)
        Me.EZeeLine7.TabIndex = 145
        Me.EZeeLine7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign48
        '
        Me.objlblSign48.Enabled = False
        Me.objlblSign48.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign48.ForeColor = System.Drawing.Color.Red
        Me.objlblSign48.Location = New System.Drawing.Point(308, 684)
        Me.objlblSign48.Name = "objlblSign48"
        Me.objlblSign48.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign48.TabIndex = 141
        Me.objlblSign48.Text = "*"
        Me.objlblSign48.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblRpIssuePlace
        '
        Me.lblRpIssuePlace.Enabled = False
        Me.lblRpIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRpIssuePlace.Location = New System.Drawing.Point(331, 684)
        Me.lblRpIssuePlace.Name = "lblRpIssuePlace"
        Me.lblRpIssuePlace.Size = New System.Drawing.Size(92, 17)
        Me.lblRpIssuePlace.TabIndex = 142
        Me.lblRpIssuePlace.Text = "Issue Place"
        Me.lblRpIssuePlace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResidentIssuePlace
        '
        Me.cboResidentIssuePlace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidentIssuePlace.Enabled = False
        Me.cboResidentIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidentIssuePlace.FormattingEnabled = True
        Me.cboResidentIssuePlace.Location = New System.Drawing.Point(429, 682)
        Me.cboResidentIssuePlace.Name = "cboResidentIssuePlace"
        Me.cboResidentIssuePlace.Size = New System.Drawing.Size(135, 21)
        Me.cboResidentIssuePlace.TabIndex = 140
        '
        'objlblSign47
        '
        Me.objlblSign47.Enabled = False
        Me.objlblSign47.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign47.ForeColor = System.Drawing.Color.Red
        Me.objlblSign47.Location = New System.Drawing.Point(308, 595)
        Me.objlblSign47.Name = "objlblSign47"
        Me.objlblSign47.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign47.TabIndex = 138
        Me.objlblSign47.Text = "*"
        Me.objlblSign47.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblWpIssuePlace
        '
        Me.lblWpIssuePlace.Enabled = False
        Me.lblWpIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWpIssuePlace.Location = New System.Drawing.Point(331, 595)
        Me.lblWpIssuePlace.Name = "lblWpIssuePlace"
        Me.lblWpIssuePlace.Size = New System.Drawing.Size(92, 17)
        Me.lblWpIssuePlace.TabIndex = 139
        Me.lblWpIssuePlace.Text = "Issue Place"
        Me.lblWpIssuePlace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign46
        '
        Me.objlblSign46.Enabled = False
        Me.objlblSign46.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign46.ForeColor = System.Drawing.Color.Red
        Me.objlblSign46.Location = New System.Drawing.Point(3, 684)
        Me.objlblSign46.Name = "objlblSign46"
        Me.objlblSign46.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign46.TabIndex = 137
        Me.objlblSign46.Text = "*"
        Me.objlblSign46.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboWorkPermitIssuePlace
        '
        Me.cboWorkPermitIssuePlace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorkPermitIssuePlace.Enabled = False
        Me.cboWorkPermitIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWorkPermitIssuePlace.FormattingEnabled = True
        Me.cboWorkPermitIssuePlace.Location = New System.Drawing.Point(429, 593)
        Me.cboWorkPermitIssuePlace.Name = "cboWorkPermitIssuePlace"
        Me.cboWorkPermitIssuePlace.Size = New System.Drawing.Size(135, 21)
        Me.cboWorkPermitIssuePlace.TabIndex = 136
        '
        'objlblSign45
        '
        Me.objlblSign45.Enabled = False
        Me.objlblSign45.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign45.ForeColor = System.Drawing.Color.Red
        Me.objlblSign45.Location = New System.Drawing.Point(3, 657)
        Me.objlblSign45.Name = "objlblSign45"
        Me.objlblSign45.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign45.TabIndex = 134
        Me.objlblSign45.Text = "*"
        Me.objlblSign45.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblRPCountry
        '
        Me.lblRPCountry.Enabled = False
        Me.lblRPCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRPCountry.Location = New System.Drawing.Point(27, 684)
        Me.lblRPCountry.Name = "lblRPCountry"
        Me.lblRPCountry.Size = New System.Drawing.Size(92, 17)
        Me.lblRPCountry.TabIndex = 135
        Me.lblRPCountry.Text = "Country"
        Me.lblRPCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResidentCountry
        '
        Me.cboResidentCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidentCountry.Enabled = False
        Me.cboResidentCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidentCountry.FormattingEnabled = True
        Me.cboResidentCountry.Location = New System.Drawing.Point(140, 682)
        Me.cboResidentCountry.Name = "cboResidentCountry"
        Me.cboResidentCountry.Size = New System.Drawing.Size(135, 21)
        Me.cboResidentCountry.TabIndex = 133
        '
        'objlblSign44
        '
        Me.objlblSign44.Enabled = False
        Me.objlblSign44.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign44.ForeColor = System.Drawing.Color.Red
        Me.objlblSign44.Location = New System.Drawing.Point(307, 657)
        Me.objlblSign44.Name = "objlblSign44"
        Me.objlblSign44.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign44.TabIndex = 131
        Me.objlblSign44.Text = "*"
        Me.objlblSign44.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblRPExpiryDate
        '
        Me.lblRPExpiryDate.Enabled = False
        Me.lblRPExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRPExpiryDate.Location = New System.Drawing.Point(330, 657)
        Me.lblRPExpiryDate.Name = "lblRPExpiryDate"
        Me.lblRPExpiryDate.Size = New System.Drawing.Size(92, 17)
        Me.lblRPExpiryDate.TabIndex = 132
        Me.lblRPExpiryDate.Text = "Expiry Date"
        Me.lblRPExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResidentExpiryDate
        '
        Me.cboResidentExpiryDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidentExpiryDate.Enabled = False
        Me.cboResidentExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidentExpiryDate.FormattingEnabled = True
        Me.cboResidentExpiryDate.Location = New System.Drawing.Point(429, 655)
        Me.cboResidentExpiryDate.Name = "cboResidentExpiryDate"
        Me.cboResidentExpiryDate.Size = New System.Drawing.Size(135, 21)
        Me.cboResidentExpiryDate.TabIndex = 130
        '
        'objlblSign43
        '
        Me.objlblSign43.Enabled = False
        Me.objlblSign43.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign43.ForeColor = System.Drawing.Color.Red
        Me.objlblSign43.Location = New System.Drawing.Point(3, 657)
        Me.objlblSign43.Name = "objlblSign43"
        Me.objlblSign43.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign43.TabIndex = 128
        Me.objlblSign43.Text = "*"
        Me.objlblSign43.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblRPIssueDate
        '
        Me.lblRPIssueDate.Enabled = False
        Me.lblRPIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRPIssueDate.Location = New System.Drawing.Point(27, 657)
        Me.lblRPIssueDate.Name = "lblRPIssueDate"
        Me.lblRPIssueDate.Size = New System.Drawing.Size(92, 17)
        Me.lblRPIssueDate.TabIndex = 129
        Me.lblRPIssueDate.Text = "Issue Date"
        Me.lblRPIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResidentIssueDate
        '
        Me.cboResidentIssueDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidentIssueDate.Enabled = False
        Me.cboResidentIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidentIssueDate.FormattingEnabled = True
        Me.cboResidentIssueDate.Location = New System.Drawing.Point(140, 655)
        Me.cboResidentIssueDate.Name = "cboResidentIssueDate"
        Me.cboResidentIssueDate.Size = New System.Drawing.Size(135, 21)
        Me.cboResidentIssueDate.TabIndex = 127
        '
        'objlblSign41
        '
        Me.objlblSign41.Enabled = False
        Me.objlblSign41.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign41.ForeColor = System.Drawing.Color.Red
        Me.objlblSign41.Location = New System.Drawing.Point(3, 630)
        Me.objlblSign41.Name = "objlblSign41"
        Me.objlblSign41.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign41.TabIndex = 123
        Me.objlblSign41.Text = "*"
        Me.objlblSign41.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblRPno
        '
        Me.lblRPno.Enabled = False
        Me.lblRPno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRPno.Location = New System.Drawing.Point(26, 630)
        Me.lblRPno.Name = "lblRPno"
        Me.lblRPno.Size = New System.Drawing.Size(108, 17)
        Me.lblRPno.TabIndex = 124
        Me.lblRPno.Text = "Resident Permit No"
        Me.lblRPno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResidentPermitNo
        '
        Me.cboResidentPermitNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidentPermitNo.Enabled = False
        Me.cboResidentPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidentPermitNo.FormattingEnabled = True
        Me.cboResidentPermitNo.Location = New System.Drawing.Point(140, 628)
        Me.cboResidentPermitNo.Name = "cboResidentPermitNo"
        Me.cboResidentPermitNo.Size = New System.Drawing.Size(135, 21)
        Me.cboResidentPermitNo.TabIndex = 120
        '
        'cboResidentChangeReason
        '
        Me.cboResidentChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidentChangeReason.Enabled = False
        Me.cboResidentChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidentChangeReason.FormattingEnabled = True
        Me.cboResidentChangeReason.Location = New System.Drawing.Point(429, 628)
        Me.cboResidentChangeReason.Name = "cboResidentChangeReason"
        Me.cboResidentChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboResidentChangeReason.TabIndex = 121
        '
        'lblRPReason
        '
        Me.lblRPReason.Enabled = False
        Me.lblRPReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRPReason.Location = New System.Drawing.Point(330, 630)
        Me.lblRPReason.Name = "lblRPReason"
        Me.lblRPReason.Size = New System.Drawing.Size(92, 17)
        Me.lblRPReason.TabIndex = 126
        Me.lblRPReason.Text = "Change Reason"
        Me.lblRPReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign42
        '
        Me.objlblSign42.Enabled = False
        Me.objlblSign42.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign42.ForeColor = System.Drawing.Color.Red
        Me.objlblSign42.Location = New System.Drawing.Point(307, 630)
        Me.objlblSign42.Name = "objlblSign42"
        Me.objlblSign42.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign42.TabIndex = 125
        Me.objlblSign42.Text = "*"
        Me.objlblSign42.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'EZeeLine6
        '
        Me.EZeeLine6.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine6.Location = New System.Drawing.Point(3, 616)
        Me.EZeeLine6.Name = "EZeeLine6"
        Me.EZeeLine6.Size = New System.Drawing.Size(571, 9)
        Me.EZeeLine6.TabIndex = 122
        Me.EZeeLine6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign40
        '
        Me.objlblSign40.Enabled = False
        Me.objlblSign40.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign40.ForeColor = System.Drawing.Color.Red
        Me.objlblSign40.Location = New System.Drawing.Point(3, 595)
        Me.objlblSign40.Name = "objlblSign40"
        Me.objlblSign40.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign40.TabIndex = 118
        Me.objlblSign40.Text = "*"
        Me.objlblSign40.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblWPCountry
        '
        Me.lblWPCountry.Enabled = False
        Me.lblWPCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWPCountry.Location = New System.Drawing.Point(27, 595)
        Me.lblWPCountry.Name = "lblWPCountry"
        Me.lblWPCountry.Size = New System.Drawing.Size(92, 17)
        Me.lblWPCountry.TabIndex = 119
        Me.lblWPCountry.Text = "Country"
        Me.lblWPCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWorkPermitCountry
        '
        Me.cboWorkPermitCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorkPermitCountry.Enabled = False
        Me.cboWorkPermitCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWorkPermitCountry.FormattingEnabled = True
        Me.cboWorkPermitCountry.Location = New System.Drawing.Point(140, 593)
        Me.cboWorkPermitCountry.Name = "cboWorkPermitCountry"
        Me.cboWorkPermitCountry.Size = New System.Drawing.Size(135, 21)
        Me.cboWorkPermitCountry.TabIndex = 117
        '
        'objlblSign39
        '
        Me.objlblSign39.Enabled = False
        Me.objlblSign39.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign39.ForeColor = System.Drawing.Color.Red
        Me.objlblSign39.Location = New System.Drawing.Point(307, 567)
        Me.objlblSign39.Name = "objlblSign39"
        Me.objlblSign39.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign39.TabIndex = 115
        Me.objlblSign39.Text = "*"
        Me.objlblSign39.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblWPExpiryDate
        '
        Me.lblWPExpiryDate.Enabled = False
        Me.lblWPExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWPExpiryDate.Location = New System.Drawing.Point(330, 567)
        Me.lblWPExpiryDate.Name = "lblWPExpiryDate"
        Me.lblWPExpiryDate.Size = New System.Drawing.Size(92, 17)
        Me.lblWPExpiryDate.TabIndex = 116
        Me.lblWPExpiryDate.Text = "Expiry Date"
        Me.lblWPExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWorkPermitExpiryDate
        '
        Me.cboWorkPermitExpiryDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorkPermitExpiryDate.Enabled = False
        Me.cboWorkPermitExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWorkPermitExpiryDate.FormattingEnabled = True
        Me.cboWorkPermitExpiryDate.Location = New System.Drawing.Point(429, 565)
        Me.cboWorkPermitExpiryDate.Name = "cboWorkPermitExpiryDate"
        Me.cboWorkPermitExpiryDate.Size = New System.Drawing.Size(135, 21)
        Me.cboWorkPermitExpiryDate.TabIndex = 114
        '
        'objlblSign38
        '
        Me.objlblSign38.Enabled = False
        Me.objlblSign38.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign38.ForeColor = System.Drawing.Color.Red
        Me.objlblSign38.Location = New System.Drawing.Point(3, 567)
        Me.objlblSign38.Name = "objlblSign38"
        Me.objlblSign38.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign38.TabIndex = 112
        Me.objlblSign38.Text = "*"
        Me.objlblSign38.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblWPIssueDt
        '
        Me.lblWPIssueDt.Enabled = False
        Me.lblWPIssueDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWPIssueDt.Location = New System.Drawing.Point(27, 567)
        Me.lblWPIssueDt.Name = "lblWPIssueDt"
        Me.lblWPIssueDt.Size = New System.Drawing.Size(92, 17)
        Me.lblWPIssueDt.TabIndex = 113
        Me.lblWPIssueDt.Text = "Issue Date"
        Me.lblWPIssueDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWorkPermitIssueDate
        '
        Me.cboWorkPermitIssueDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorkPermitIssueDate.Enabled = False
        Me.cboWorkPermitIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWorkPermitIssueDate.FormattingEnabled = True
        Me.cboWorkPermitIssueDate.Location = New System.Drawing.Point(140, 565)
        Me.cboWorkPermitIssueDate.Name = "cboWorkPermitIssueDate"
        Me.cboWorkPermitIssueDate.Size = New System.Drawing.Size(135, 21)
        Me.cboWorkPermitIssueDate.TabIndex = 111
        '
        'objlblSign36
        '
        Me.objlblSign36.Enabled = False
        Me.objlblSign36.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign36.ForeColor = System.Drawing.Color.Red
        Me.objlblSign36.Location = New System.Drawing.Point(3, 539)
        Me.objlblSign36.Name = "objlblSign36"
        Me.objlblSign36.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign36.TabIndex = 107
        Me.objlblSign36.Text = "*"
        Me.objlblSign36.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblWPno
        '
        Me.lblWPno.Enabled = False
        Me.lblWPno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWPno.Location = New System.Drawing.Point(26, 539)
        Me.lblWPno.Name = "lblWPno"
        Me.lblWPno.Size = New System.Drawing.Size(108, 17)
        Me.lblWPno.TabIndex = 108
        Me.lblWPno.Text = "Work Permit No"
        Me.lblWPno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWorkPermitNo
        '
        Me.cboWorkPermitNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorkPermitNo.Enabled = False
        Me.cboWorkPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWorkPermitNo.FormattingEnabled = True
        Me.cboWorkPermitNo.Location = New System.Drawing.Point(140, 537)
        Me.cboWorkPermitNo.Name = "cboWorkPermitNo"
        Me.cboWorkPermitNo.Size = New System.Drawing.Size(135, 21)
        Me.cboWorkPermitNo.TabIndex = 104
        '
        'cboWorkPermitChangeReason
        '
        Me.cboWorkPermitChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorkPermitChangeReason.Enabled = False
        Me.cboWorkPermitChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWorkPermitChangeReason.FormattingEnabled = True
        Me.cboWorkPermitChangeReason.Location = New System.Drawing.Point(429, 537)
        Me.cboWorkPermitChangeReason.Name = "cboWorkPermitChangeReason"
        Me.cboWorkPermitChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboWorkPermitChangeReason.TabIndex = 105
        '
        'lblWPReason
        '
        Me.lblWPReason.Enabled = False
        Me.lblWPReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWPReason.Location = New System.Drawing.Point(330, 539)
        Me.lblWPReason.Name = "lblWPReason"
        Me.lblWPReason.Size = New System.Drawing.Size(92, 17)
        Me.lblWPReason.TabIndex = 110
        Me.lblWPReason.Text = "Change Reason"
        Me.lblWPReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign37
        '
        Me.objlblSign37.Enabled = False
        Me.objlblSign37.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign37.ForeColor = System.Drawing.Color.Red
        Me.objlblSign37.Location = New System.Drawing.Point(307, 539)
        Me.objlblSign37.Name = "objlblSign37"
        Me.objlblSign37.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign37.TabIndex = 109
        Me.objlblSign37.Text = "*"
        Me.objlblSign37.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'EZeeLine5
        '
        Me.EZeeLine5.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine5.Location = New System.Drawing.Point(3, 521)
        Me.EZeeLine5.Name = "EZeeLine5"
        Me.EZeeLine5.Size = New System.Drawing.Size(571, 9)
        Me.EZeeLine5.TabIndex = 106
        Me.EZeeLine5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign35
        '
        Me.objlblSign35.Enabled = False
        Me.objlblSign35.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign35.ForeColor = System.Drawing.Color.Red
        Me.objlblSign35.Location = New System.Drawing.Point(3, 498)
        Me.objlblSign35.Name = "objlblSign35"
        Me.objlblSign35.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign35.TabIndex = 102
        Me.objlblSign35.Text = "*"
        Me.objlblSign35.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblSuspToDt
        '
        Me.lblSuspToDt.Enabled = False
        Me.lblSuspToDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSuspToDt.Location = New System.Drawing.Point(26, 499)
        Me.lblSuspToDt.Name = "lblSuspToDt"
        Me.lblSuspToDt.Size = New System.Drawing.Size(92, 17)
        Me.lblSuspToDt.TabIndex = 103
        Me.lblSuspToDt.Text = "Suspension To Date"
        Me.lblSuspToDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSuspensionToDate
        '
        Me.cboSuspensionToDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSuspensionToDate.Enabled = False
        Me.cboSuspensionToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSuspensionToDate.FormattingEnabled = True
        Me.cboSuspensionToDate.Location = New System.Drawing.Point(140, 497)
        Me.cboSuspensionToDate.Name = "cboSuspensionToDate"
        Me.cboSuspensionToDate.Size = New System.Drawing.Size(135, 21)
        Me.cboSuspensionToDate.TabIndex = 101
        '
        'objlblSign33
        '
        Me.objlblSign33.Enabled = False
        Me.objlblSign33.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign33.ForeColor = System.Drawing.Color.Red
        Me.objlblSign33.Location = New System.Drawing.Point(3, 472)
        Me.objlblSign33.Name = "objlblSign33"
        Me.objlblSign33.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign33.TabIndex = 97
        Me.objlblSign33.Text = "*"
        Me.objlblSign33.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblSuspFromDt
        '
        Me.lblSuspFromDt.Enabled = False
        Me.lblSuspFromDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSuspFromDt.Location = New System.Drawing.Point(26, 472)
        Me.lblSuspFromDt.Name = "lblSuspFromDt"
        Me.lblSuspFromDt.Size = New System.Drawing.Size(108, 17)
        Me.lblSuspFromDt.TabIndex = 98
        Me.lblSuspFromDt.Text = "Suspension From Date"
        Me.lblSuspFromDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSuspensionFromDate
        '
        Me.cboSuspensionFromDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSuspensionFromDate.Enabled = False
        Me.cboSuspensionFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSuspensionFromDate.FormattingEnabled = True
        Me.cboSuspensionFromDate.Location = New System.Drawing.Point(140, 470)
        Me.cboSuspensionFromDate.Name = "cboSuspensionFromDate"
        Me.cboSuspensionFromDate.Size = New System.Drawing.Size(135, 21)
        Me.cboSuspensionFromDate.TabIndex = 94
        '
        'cboSuspensionChangeReason
        '
        Me.cboSuspensionChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSuspensionChangeReason.Enabled = False
        Me.cboSuspensionChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSuspensionChangeReason.FormattingEnabled = True
        Me.cboSuspensionChangeReason.Location = New System.Drawing.Point(429, 474)
        Me.cboSuspensionChangeReason.Name = "cboSuspensionChangeReason"
        Me.cboSuspensionChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboSuspensionChangeReason.TabIndex = 95
        '
        'lblSuspensionReason
        '
        Me.lblSuspensionReason.Enabled = False
        Me.lblSuspensionReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSuspensionReason.Location = New System.Drawing.Point(330, 476)
        Me.lblSuspensionReason.Name = "lblSuspensionReason"
        Me.lblSuspensionReason.Size = New System.Drawing.Size(92, 17)
        Me.lblSuspensionReason.TabIndex = 100
        Me.lblSuspensionReason.Text = "Change Reason"
        Me.lblSuspensionReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign34
        '
        Me.objlblSign34.Enabled = False
        Me.objlblSign34.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign34.ForeColor = System.Drawing.Color.Red
        Me.objlblSign34.Location = New System.Drawing.Point(307, 476)
        Me.objlblSign34.Name = "objlblSign34"
        Me.objlblSign34.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign34.TabIndex = 99
        Me.objlblSign34.Text = "*"
        Me.objlblSign34.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'EZeeLine4
        '
        Me.EZeeLine4.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine4.Location = New System.Drawing.Point(3, 458)
        Me.EZeeLine4.Name = "EZeeLine4"
        Me.EZeeLine4.Size = New System.Drawing.Size(571, 9)
        Me.EZeeLine4.TabIndex = 96
        Me.EZeeLine4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign31
        '
        Me.objlblSign31.Enabled = False
        Me.objlblSign31.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign31.ForeColor = System.Drawing.Color.Red
        Me.objlblSign31.Location = New System.Drawing.Point(3, 435)
        Me.objlblSign31.Name = "objlblSign31"
        Me.objlblSign31.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign31.TabIndex = 87
        Me.objlblSign31.Text = "*"
        Me.objlblSign31.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lbConfirmationDt
        '
        Me.lbConfirmationDt.Enabled = False
        Me.lbConfirmationDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbConfirmationDt.Location = New System.Drawing.Point(26, 435)
        Me.lbConfirmationDt.Name = "lbConfirmationDt"
        Me.lbConfirmationDt.Size = New System.Drawing.Size(108, 17)
        Me.lbConfirmationDt.TabIndex = 88
        Me.lbConfirmationDt.Text = "Confirmation Date"
        Me.lbConfirmationDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboConfirmationDate
        '
        Me.cboConfirmationDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboConfirmationDate.Enabled = False
        Me.cboConfirmationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboConfirmationDate.FormattingEnabled = True
        Me.cboConfirmationDate.Location = New System.Drawing.Point(140, 433)
        Me.cboConfirmationDate.Name = "cboConfirmationDate"
        Me.cboConfirmationDate.Size = New System.Drawing.Size(135, 21)
        Me.cboConfirmationDate.TabIndex = 84
        '
        'cboConfirmationChangeReason
        '
        Me.cboConfirmationChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboConfirmationChangeReason.Enabled = False
        Me.cboConfirmationChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboConfirmationChangeReason.FormattingEnabled = True
        Me.cboConfirmationChangeReason.Location = New System.Drawing.Point(429, 433)
        Me.cboConfirmationChangeReason.Name = "cboConfirmationChangeReason"
        Me.cboConfirmationChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboConfirmationChangeReason.TabIndex = 85
        '
        'lblConfirmationReason
        '
        Me.lblConfirmationReason.Enabled = False
        Me.lblConfirmationReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConfirmationReason.Location = New System.Drawing.Point(330, 435)
        Me.lblConfirmationReason.Name = "lblConfirmationReason"
        Me.lblConfirmationReason.Size = New System.Drawing.Size(92, 17)
        Me.lblConfirmationReason.TabIndex = 90
        Me.lblConfirmationReason.Text = "Change Reason"
        Me.lblConfirmationReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign32
        '
        Me.objlblSign32.Enabled = False
        Me.objlblSign32.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign32.ForeColor = System.Drawing.Color.Red
        Me.objlblSign32.Location = New System.Drawing.Point(307, 435)
        Me.objlblSign32.Name = "objlblSign32"
        Me.objlblSign32.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign32.TabIndex = 89
        Me.objlblSign32.Text = "*"
        Me.objlblSign32.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'EZeeLine3
        '
        Me.EZeeLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine3.Location = New System.Drawing.Point(3, 421)
        Me.EZeeLine3.Name = "EZeeLine3"
        Me.EZeeLine3.Size = New System.Drawing.Size(571, 9)
        Me.EZeeLine3.TabIndex = 86
        Me.EZeeLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign30
        '
        Me.objlblSign30.Enabled = False
        Me.objlblSign30.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign30.ForeColor = System.Drawing.Color.Red
        Me.objlblSign30.Location = New System.Drawing.Point(3, 399)
        Me.objlblSign30.Name = "objlblSign30"
        Me.objlblSign30.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign30.TabIndex = 82
        Me.objlblSign30.Text = "*"
        Me.objlblSign30.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblProbationToDt
        '
        Me.lblProbationToDt.Enabled = False
        Me.lblProbationToDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProbationToDt.Location = New System.Drawing.Point(26, 399)
        Me.lblProbationToDt.Name = "lblProbationToDt"
        Me.lblProbationToDt.Size = New System.Drawing.Size(108, 17)
        Me.lblProbationToDt.TabIndex = 83
        Me.lblProbationToDt.Text = "Probation To Date"
        Me.lblProbationToDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboProbationToDate
        '
        Me.cboProbationToDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProbationToDate.Enabled = False
        Me.cboProbationToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProbationToDate.FormattingEnabled = True
        Me.cboProbationToDate.Location = New System.Drawing.Point(140, 397)
        Me.cboProbationToDate.Name = "cboProbationToDate"
        Me.cboProbationToDate.Size = New System.Drawing.Size(135, 21)
        Me.cboProbationToDate.TabIndex = 81
        '
        'objlblSign28
        '
        Me.objlblSign28.Enabled = False
        Me.objlblSign28.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign28.ForeColor = System.Drawing.Color.Red
        Me.objlblSign28.Location = New System.Drawing.Point(3, 372)
        Me.objlblSign28.Name = "objlblSign28"
        Me.objlblSign28.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign28.TabIndex = 77
        Me.objlblSign28.Text = "*"
        Me.objlblSign28.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblProbationFromDt
        '
        Me.lblProbationFromDt.Enabled = False
        Me.lblProbationFromDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProbationFromDt.Location = New System.Drawing.Point(26, 372)
        Me.lblProbationFromDt.Name = "lblProbationFromDt"
        Me.lblProbationFromDt.Size = New System.Drawing.Size(108, 17)
        Me.lblProbationFromDt.TabIndex = 78
        Me.lblProbationFromDt.Text = "Probation From Date"
        Me.lblProbationFromDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboProbationFromDate
        '
        Me.cboProbationFromDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProbationFromDate.Enabled = False
        Me.cboProbationFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProbationFromDate.FormattingEnabled = True
        Me.cboProbationFromDate.Location = New System.Drawing.Point(140, 370)
        Me.cboProbationFromDate.Name = "cboProbationFromDate"
        Me.cboProbationFromDate.Size = New System.Drawing.Size(135, 21)
        Me.cboProbationFromDate.TabIndex = 74
        '
        'cboProbationChangeReason
        '
        Me.cboProbationChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProbationChangeReason.Enabled = False
        Me.cboProbationChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProbationChangeReason.FormattingEnabled = True
        Me.cboProbationChangeReason.Location = New System.Drawing.Point(429, 370)
        Me.cboProbationChangeReason.Name = "cboProbationChangeReason"
        Me.cboProbationChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboProbationChangeReason.TabIndex = 75
        '
        'lblPbReason
        '
        Me.lblPbReason.Enabled = False
        Me.lblPbReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPbReason.Location = New System.Drawing.Point(330, 372)
        Me.lblPbReason.Name = "lblPbReason"
        Me.lblPbReason.Size = New System.Drawing.Size(92, 17)
        Me.lblPbReason.TabIndex = 80
        Me.lblPbReason.Text = "Change Reason"
        Me.lblPbReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign29
        '
        Me.objlblSign29.Enabled = False
        Me.objlblSign29.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign29.ForeColor = System.Drawing.Color.Red
        Me.objlblSign29.Location = New System.Drawing.Point(307, 372)
        Me.objlblSign29.Name = "objlblSign29"
        Me.objlblSign29.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign29.TabIndex = 79
        Me.objlblSign29.Text = "*"
        Me.objlblSign29.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'EZeeLine2
        '
        Me.EZeeLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine2.Location = New System.Drawing.Point(3, 358)
        Me.EZeeLine2.Name = "EZeeLine2"
        Me.EZeeLine2.Size = New System.Drawing.Size(571, 9)
        Me.EZeeLine2.TabIndex = 76
        Me.EZeeLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign13
        '
        Me.objlblSign13.Enabled = False
        Me.objlblSign13.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign13.ForeColor = System.Drawing.Color.Red
        Me.objlblSign13.Location = New System.Drawing.Point(3, 5)
        Me.objlblSign13.Name = "objlblSign13"
        Me.objlblSign13.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign13.TabIndex = 27
        Me.objlblSign13.Text = "*"
        Me.objlblSign13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboIsExcludePayroll
        '
        Me.cboIsExcludePayroll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIsExcludePayroll.Enabled = False
        Me.cboIsExcludePayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIsExcludePayroll.FormattingEnabled = True
        Me.cboIsExcludePayroll.Location = New System.Drawing.Point(429, 297)
        Me.cboIsExcludePayroll.Name = "cboIsExcludePayroll"
        Me.cboIsExcludePayroll.Size = New System.Drawing.Size(135, 21)
        Me.cboIsExcludePayroll.TabIndex = 20
        '
        'objlblSign4
        '
        Me.objlblSign4.Enabled = False
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(3, 335)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign4.TabIndex = 54
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblIsexcludePayroll
        '
        Me.lblIsexcludePayroll.Enabled = False
        Me.lblIsexcludePayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsexcludePayroll.Location = New System.Drawing.Point(330, 299)
        Me.lblIsexcludePayroll.Name = "lblIsexcludePayroll"
        Me.lblIsexcludePayroll.Size = New System.Drawing.Size(92, 17)
        Me.lblIsexcludePayroll.TabIndex = 59
        Me.lblIsexcludePayroll.Text = "Isexclude Payroll"
        Me.lblIsexcludePayroll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRetirementDt
        '
        Me.lblRetirementDt.Enabled = False
        Me.lblRetirementDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetirementDt.Location = New System.Drawing.Point(26, 335)
        Me.lblRetirementDt.Name = "lblRetirementDt"
        Me.lblRetirementDt.Size = New System.Drawing.Size(92, 17)
        Me.lblRetirementDt.TabIndex = 55
        Me.lblRetirementDt.Text = "Retirement Date"
        Me.lblRetirementDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign27
        '
        Me.objlblSign27.Enabled = False
        Me.objlblSign27.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign27.ForeColor = System.Drawing.Color.Red
        Me.objlblSign27.Location = New System.Drawing.Point(307, 299)
        Me.objlblSign27.Name = "objlblSign27"
        Me.objlblSign27.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign27.TabIndex = 33
        Me.objlblSign27.Text = "*"
        Me.objlblSign27.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboRetirementDate
        '
        Me.cboRetirementDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRetirementDate.Enabled = False
        Me.cboRetirementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRetirementDate.FormattingEnabled = True
        Me.cboRetirementDate.Location = New System.Drawing.Point(140, 333)
        Me.cboRetirementDate.Name = "cboRetirementDate"
        Me.cboRetirementDate.Size = New System.Drawing.Size(135, 21)
        Me.cboRetirementDate.TabIndex = 21
        '
        'cboRetirementChangeReason
        '
        Me.cboRetirementChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRetirementChangeReason.Enabled = False
        Me.cboRetirementChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRetirementChangeReason.FormattingEnabled = True
        Me.cboRetirementChangeReason.Location = New System.Drawing.Point(429, 333)
        Me.cboRetirementChangeReason.Name = "cboRetirementChangeReason"
        Me.cboRetirementChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboRetirementChangeReason.TabIndex = 22
        '
        'objlblSign5
        '
        Me.objlblSign5.Enabled = False
        Me.objlblSign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign5.ForeColor = System.Drawing.Color.Red
        Me.objlblSign5.Location = New System.Drawing.Point(3, 59)
        Me.objlblSign5.Name = "objlblSign5"
        Me.objlblSign5.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign5.TabIndex = 30
        Me.objlblSign5.Text = "*"
        Me.objlblSign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblRtReason
        '
        Me.lblRtReason.Enabled = False
        Me.lblRtReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRtReason.Location = New System.Drawing.Point(330, 335)
        Me.lblRtReason.Name = "lblRtReason"
        Me.lblRtReason.Size = New System.Drawing.Size(92, 17)
        Me.lblRtReason.TabIndex = 57
        Me.lblRtReason.Text = "Change Reason"
        Me.lblRtReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Enabled = False
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(27, 59)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(92, 17)
        Me.lblDepartment.TabIndex = 31
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign26
        '
        Me.objlblSign26.Enabled = False
        Me.objlblSign26.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign26.ForeColor = System.Drawing.Color.Red
        Me.objlblSign26.Location = New System.Drawing.Point(307, 335)
        Me.objlblSign26.Name = "objlblSign26"
        Me.objlblSign26.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign26.TabIndex = 56
        Me.objlblSign26.Text = "*"
        Me.objlblSign26.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Enabled = False
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(140, 57)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(135, 21)
        Me.cboDepartment.TabIndex = 3
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(3, 321)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(571, 9)
        Me.EZeeLine1.TabIndex = 53
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign6
        '
        Me.objlblSign6.Enabled = False
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(307, 209)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign6.TabIndex = 31
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboCostCenterChangeReason
        '
        Me.cboCostCenterChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenterChangeReason.Enabled = False
        Me.cboCostCenterChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenterChangeReason.FormattingEnabled = True
        Me.cboCostCenterChangeReason.Location = New System.Drawing.Point(429, 171)
        Me.cboCostCenterChangeReason.Name = "cboCostCenterChangeReason"
        Me.cboCostCenterChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboCostCenterChangeReason.TabIndex = 13
        '
        'lblJob
        '
        Me.lblJob.Enabled = False
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(330, 209)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(92, 17)
        Me.lblJob.TabIndex = 63
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCCReason
        '
        Me.lblCCReason.Enabled = False
        Me.lblCCReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCCReason.Location = New System.Drawing.Point(330, 173)
        Me.lblCCReason.Name = "lblCCReason"
        Me.lblCCReason.Size = New System.Drawing.Size(92, 17)
        Me.lblCCReason.TabIndex = 65
        Me.lblCCReason.Text = "Change Reason"
        Me.lblCCReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Enabled = False
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(429, 207)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(135, 21)
        Me.cboJob.TabIndex = 15
        '
        'objlblSign25
        '
        Me.objlblSign25.Enabled = False
        Me.objlblSign25.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign25.ForeColor = System.Drawing.Color.Red
        Me.objlblSign25.Location = New System.Drawing.Point(307, 173)
        Me.objlblSign25.Name = "objlblSign25"
        Me.objlblSign25.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign25.TabIndex = 30
        Me.objlblSign25.Text = "*"
        Me.objlblSign25.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign7
        '
        Me.objlblSign7.Enabled = False
        Me.objlblSign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign7.ForeColor = System.Drawing.Color.Red
        Me.objlblSign7.Location = New System.Drawing.Point(307, 86)
        Me.objlblSign7.Name = "objlblSign7"
        Me.objlblSign7.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign7.TabIndex = 28
        Me.objlblSign7.Text = "*"
        Me.objlblSign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboCategorizeChangeReason
        '
        Me.cboCategorizeChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategorizeChangeReason.Enabled = False
        Me.cboCategorizeChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategorizeChangeReason.FormattingEnabled = True
        Me.cboCategorizeChangeReason.Location = New System.Drawing.Point(140, 234)
        Me.cboCategorizeChangeReason.Name = "cboCategorizeChangeReason"
        Me.cboCategorizeChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboCategorizeChangeReason.TabIndex = 16
        '
        'lblClassGrp
        '
        Me.lblClassGrp.Enabled = False
        Me.lblClassGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGrp.Location = New System.Drawing.Point(331, 86)
        Me.lblClassGrp.Name = "lblClassGrp"
        Me.lblClassGrp.Size = New System.Drawing.Size(92, 17)
        Me.lblClassGrp.TabIndex = 69
        Me.lblClassGrp.Text = "Class Group"
        Me.lblClassGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobReason
        '
        Me.lblJobReason.Enabled = False
        Me.lblJobReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobReason.Location = New System.Drawing.Point(26, 236)
        Me.lblJobReason.Name = "lblJobReason"
        Me.lblJobReason.Size = New System.Drawing.Size(92, 17)
        Me.lblJobReason.TabIndex = 46
        Me.lblJobReason.Text = "Change Reason"
        Me.lblJobReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.Enabled = False
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(429, 84)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(135, 21)
        Me.cboClassGroup.TabIndex = 9
        '
        'objlblSign24
        '
        Me.objlblSign24.Enabled = False
        Me.objlblSign24.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign24.ForeColor = System.Drawing.Color.Red
        Me.objlblSign24.Location = New System.Drawing.Point(3, 236)
        Me.objlblSign24.Name = "objlblSign24"
        Me.objlblSign24.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign24.TabIndex = 45
        Me.objlblSign24.Text = "*"
        Me.objlblSign24.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign8
        '
        Me.objlblSign8.Enabled = False
        Me.objlblSign8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign8.ForeColor = System.Drawing.Color.Red
        Me.objlblSign8.Location = New System.Drawing.Point(307, 113)
        Me.objlblSign8.Name = "objlblSign8"
        Me.objlblSign8.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign8.TabIndex = 29
        Me.objlblSign8.Text = "*"
        Me.objlblSign8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAllocationChangeReason
        '
        Me.cboAllocationChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocationChangeReason.Enabled = False
        Me.cboAllocationChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocationChangeReason.FormattingEnabled = True
        Me.cboAllocationChangeReason.Location = New System.Drawing.Point(140, 138)
        Me.cboAllocationChangeReason.Name = "cboAllocationChangeReason"
        Me.cboAllocationChangeReason.Size = New System.Drawing.Size(135, 21)
        Me.cboAllocationChangeReason.TabIndex = 11
        '
        'lblClass
        '
        Me.lblClass.Enabled = False
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(330, 113)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(92, 17)
        Me.lblClass.TabIndex = 67
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAllocationReason
        '
        Me.lblAllocationReason.Enabled = False
        Me.lblAllocationReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocationReason.Location = New System.Drawing.Point(26, 140)
        Me.lblAllocationReason.Name = "lblAllocationReason"
        Me.lblAllocationReason.Size = New System.Drawing.Size(92, 17)
        Me.lblAllocationReason.TabIndex = 37
        Me.lblAllocationReason.Text = "Change Reason"
        Me.lblAllocationReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.Enabled = False
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(429, 111)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(135, 21)
        Me.cboClass.TabIndex = 10
        '
        'objlblSign23
        '
        Me.objlblSign23.Enabled = False
        Me.objlblSign23.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign23.ForeColor = System.Drawing.Color.Red
        Me.objlblSign23.Location = New System.Drawing.Point(3, 140)
        Me.objlblSign23.Name = "objlblSign23"
        Me.objlblSign23.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign23.TabIndex = 36
        Me.objlblSign23.Text = "*"
        Me.objlblSign23.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign11
        '
        Me.objlblSign11.Enabled = False
        Me.objlblSign11.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign11.ForeColor = System.Drawing.Color.Red
        Me.objlblSign11.Location = New System.Drawing.Point(3, 272)
        Me.objlblSign11.Name = "objlblSign11"
        Me.objlblSign11.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign11.TabIndex = 48
        Me.objlblSign11.Text = "*"
        Me.objlblSign11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblECODate
        '
        Me.lblECODate.Enabled = False
        Me.lblECODate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblECODate.Location = New System.Drawing.Point(27, 272)
        Me.lblECODate.Name = "lblECODate"
        Me.lblECODate.Size = New System.Drawing.Size(92, 17)
        Me.lblECODate.TabIndex = 49
        Me.lblECODate.Text = "EOC Date"
        Me.lblECODate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(7, 258)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(567, 9)
        Me.objelLine3.TabIndex = 47
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEOCDate
        '
        Me.cboEOCDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEOCDate.Enabled = False
        Me.cboEOCDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEOCDate.FormattingEnabled = True
        Me.cboEOCDate.Location = New System.Drawing.Point(140, 270)
        Me.cboEOCDate.Name = "cboEOCDate"
        Me.cboEOCDate.Size = New System.Drawing.Size(135, 21)
        Me.cboEOCDate.TabIndex = 17
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(7, 160)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(567, 9)
        Me.objelLine2.TabIndex = 38
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign12
        '
        Me.objlblSign12.Enabled = False
        Me.objlblSign12.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign12.ForeColor = System.Drawing.Color.Red
        Me.objlblSign12.Location = New System.Drawing.Point(3, 299)
        Me.objlblSign12.Name = "objlblSign12"
        Me.objlblSign12.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign12.TabIndex = 50
        Me.objlblSign12.Text = "*"
        Me.objlblSign12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(7, 195)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(557, 9)
        Me.objelLine1.TabIndex = 42
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLeavingDate
        '
        Me.lblLeavingDate.Enabled = False
        Me.lblLeavingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeavingDate.Location = New System.Drawing.Point(26, 299)
        Me.lblLeavingDate.Name = "lblLeavingDate"
        Me.lblLeavingDate.Size = New System.Drawing.Size(92, 17)
        Me.lblLeavingDate.TabIndex = 51
        Me.lblLeavingDate.Text = "Leaving Date"
        Me.lblLeavingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTerminationReason
        '
        Me.cboTerminationReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTerminationReason.Enabled = False
        Me.cboTerminationReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTerminationReason.FormattingEnabled = True
        Me.cboTerminationReason.Location = New System.Drawing.Point(429, 270)
        Me.cboTerminationReason.Name = "cboTerminationReason"
        Me.cboTerminationReason.Size = New System.Drawing.Size(135, 21)
        Me.cboTerminationReason.TabIndex = 18
        '
        'cboLeavingDate
        '
        Me.cboLeavingDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeavingDate.Enabled = False
        Me.cboLeavingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeavingDate.FormattingEnabled = True
        Me.cboLeavingDate.Location = New System.Drawing.Point(140, 297)
        Me.cboLeavingDate.Name = "cboLeavingDate"
        Me.cboLeavingDate.Size = New System.Drawing.Size(135, 21)
        Me.cboLeavingDate.TabIndex = 19
        '
        'lblEOCReason
        '
        Me.lblEOCReason.Enabled = False
        Me.lblEOCReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEOCReason.Location = New System.Drawing.Point(331, 272)
        Me.lblEOCReason.Name = "lblEOCReason"
        Me.lblEOCReason.Size = New System.Drawing.Size(92, 17)
        Me.lblEOCReason.TabIndex = 61
        Me.lblEOCReason.Text = "EOC Reason"
        Me.lblEOCReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBranch
        '
        Me.lblBranch.Enabled = False
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(27, 5)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(92, 17)
        Me.lblBranch.TabIndex = 26
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign22
        '
        Me.objlblSign22.Enabled = False
        Me.objlblSign22.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign22.ForeColor = System.Drawing.Color.Red
        Me.objlblSign22.Location = New System.Drawing.Point(307, 272)
        Me.objlblSign22.Name = "objlblSign22"
        Me.objlblSign22.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign22.TabIndex = 32
        Me.objlblSign22.Text = "*"
        Me.objlblSign22.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Enabled = False
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(140, 3)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(135, 21)
        Me.cboBranch.TabIndex = 1
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.Enabled = False
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(140, 171)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(135, 21)
        Me.cboCostCenter.TabIndex = 12
        '
        'objlblSign14
        '
        Me.objlblSign14.Enabled = False
        Me.objlblSign14.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign14.ForeColor = System.Drawing.Color.Red
        Me.objlblSign14.Location = New System.Drawing.Point(3, 32)
        Me.objlblSign14.Name = "objlblSign14"
        Me.objlblSign14.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign14.TabIndex = 28
        Me.objlblSign14.Text = "*"
        Me.objlblSign14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Enabled = False
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(27, 173)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(92, 17)
        Me.lblCostCenter.TabIndex = 40
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepGroup
        '
        Me.lblDepGroup.Enabled = False
        Me.lblDepGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepGroup.Location = New System.Drawing.Point(26, 32)
        Me.lblDepGroup.Name = "lblDepGroup"
        Me.lblDepGroup.Size = New System.Drawing.Size(92, 17)
        Me.lblDepGroup.TabIndex = 29
        Me.lblDepGroup.Text = "Dep. Group"
        Me.lblDepGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign21
        '
        Me.objlblSign21.Enabled = False
        Me.objlblSign21.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign21.ForeColor = System.Drawing.Color.Red
        Me.objlblSign21.Location = New System.Drawing.Point(3, 173)
        Me.objlblSign21.Name = "objlblSign21"
        Me.objlblSign21.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign21.TabIndex = 39
        Me.objlblSign21.Text = "*"
        Me.objlblSign21.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboDepartmentGroup
        '
        Me.cboDepartmentGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartmentGroup.Enabled = False
        Me.cboDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGroup.FormattingEnabled = True
        Me.cboDepartmentGroup.Location = New System.Drawing.Point(140, 30)
        Me.cboDepartmentGroup.Name = "cboDepartmentGroup"
        Me.cboDepartmentGroup.Size = New System.Drawing.Size(135, 21)
        Me.cboDepartmentGroup.TabIndex = 2
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.Enabled = False
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(140, 207)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(135, 21)
        Me.cboJobGroup.TabIndex = 14
        '
        'objlblSign15
        '
        Me.objlblSign15.Enabled = False
        Me.objlblSign15.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign15.ForeColor = System.Drawing.Color.Red
        Me.objlblSign15.Location = New System.Drawing.Point(3, 86)
        Me.objlblSign15.Name = "objlblSign15"
        Me.objlblSign15.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign15.TabIndex = 32
        Me.objlblSign15.Text = "*"
        Me.objlblSign15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Enabled = False
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(26, 209)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(92, 17)
        Me.lblJobGroup.TabIndex = 44
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSecGroup
        '
        Me.lblSecGroup.Enabled = False
        Me.lblSecGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSecGroup.Location = New System.Drawing.Point(27, 86)
        Me.lblSecGroup.Name = "lblSecGroup"
        Me.lblSecGroup.Size = New System.Drawing.Size(92, 17)
        Me.lblSecGroup.TabIndex = 33
        Me.lblSecGroup.Text = "Sec. Group"
        Me.lblSecGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign20
        '
        Me.objlblSign20.Enabled = False
        Me.objlblSign20.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign20.ForeColor = System.Drawing.Color.Red
        Me.objlblSign20.Location = New System.Drawing.Point(3, 209)
        Me.objlblSign20.Name = "objlblSign20"
        Me.objlblSign20.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign20.TabIndex = 43
        Me.objlblSign20.Text = "*"
        Me.objlblSign20.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGroup.Enabled = False
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(140, 84)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(135, 21)
        Me.cboSectionGroup.TabIndex = 4
        '
        'cboTeam
        '
        Me.cboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeam.Enabled = False
        Me.cboTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeam.FormattingEnabled = True
        Me.cboTeam.Location = New System.Drawing.Point(429, 57)
        Me.cboTeam.Name = "cboTeam"
        Me.cboTeam.Size = New System.Drawing.Size(135, 21)
        Me.cboTeam.TabIndex = 8
        '
        'objlblSign16
        '
        Me.objlblSign16.Enabled = False
        Me.objlblSign16.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign16.ForeColor = System.Drawing.Color.Red
        Me.objlblSign16.Location = New System.Drawing.Point(3, 113)
        Me.objlblSign16.Name = "objlblSign16"
        Me.objlblSign16.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign16.TabIndex = 34
        Me.objlblSign16.Text = "*"
        Me.objlblSign16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblTeam
        '
        Me.lblTeam.Enabled = False
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(331, 59)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(92, 17)
        Me.lblTeam.TabIndex = 71
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSections
        '
        Me.lblSections.Enabled = False
        Me.lblSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSections.Location = New System.Drawing.Point(26, 113)
        Me.lblSections.Name = "lblSections"
        Me.lblSections.Size = New System.Drawing.Size(92, 17)
        Me.lblSections.TabIndex = 35
        Me.lblSections.Text = "Sections"
        Me.lblSections.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign19
        '
        Me.objlblSign19.Enabled = False
        Me.objlblSign19.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign19.ForeColor = System.Drawing.Color.Red
        Me.objlblSign19.Location = New System.Drawing.Point(307, 59)
        Me.objlblSign19.Name = "objlblSign19"
        Me.objlblSign19.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign19.TabIndex = 27
        Me.objlblSign19.Text = "*"
        Me.objlblSign19.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.Enabled = False
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(140, 111)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(135, 21)
        Me.cboSection.TabIndex = 5
        '
        'cboUnit
        '
        Me.cboUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnit.Enabled = False
        Me.cboUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnit.FormattingEnabled = True
        Me.cboUnit.Location = New System.Drawing.Point(429, 30)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(135, 21)
        Me.cboUnit.TabIndex = 7
        '
        'objlblSign17
        '
        Me.objlblSign17.Enabled = False
        Me.objlblSign17.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign17.ForeColor = System.Drawing.Color.Red
        Me.objlblSign17.Location = New System.Drawing.Point(307, 5)
        Me.objlblSign17.Name = "objlblSign17"
        Me.objlblSign17.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign17.TabIndex = 25
        Me.objlblSign17.Text = "*"
        Me.objlblSign17.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblUnit
        '
        Me.lblUnit.Enabled = False
        Me.lblUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnit.Location = New System.Drawing.Point(330, 32)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(92, 17)
        Me.lblUnit.TabIndex = 73
        Me.lblUnit.Text = "Unit"
        Me.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Enabled = False
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(331, 5)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(92, 17)
        Me.lblUnitGroup.TabIndex = 24
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign18
        '
        Me.objlblSign18.Enabled = False
        Me.objlblSign18.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign18.ForeColor = System.Drawing.Color.Red
        Me.objlblSign18.Location = New System.Drawing.Point(307, 32)
        Me.objlblSign18.Name = "objlblSign18"
        Me.objlblSign18.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign18.TabIndex = 26
        Me.objlblSign18.Text = "*"
        Me.objlblSign18.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGroup.Enabled = False
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(429, 3)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(135, 21)
        Me.cboUnitGroup.TabIndex = 6
        '
        'objelLine4
        '
        Me.objelLine4.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine4.Location = New System.Drawing.Point(12, 59)
        Me.objelLine4.Name = "objelLine4"
        Me.objelLine4.Size = New System.Drawing.Size(568, 9)
        Me.objelLine4.TabIndex = 25
        Me.objelLine4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(146, 35)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(283, 21)
        Me.cboEmployeeCode.TabIndex = 0
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(331, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(268, 19)
        Me.lblCaption.TabIndex = 34
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(12, 37)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign1.TabIndex = 23
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(36, 37)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(92, 17)
        Me.lblEmployeeCode.TabIndex = 24
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wizPageFile
        '
        Me.wizPageFile.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.wizPageFile.Controls.Add(Me.btnOpenFile)
        Me.wizPageFile.Controls.Add(Me.txtFilePath)
        Me.wizPageFile.Controls.Add(Me.lblTitle)
        Me.wizPageFile.Controls.Add(Me.lblSelectfile)
        Me.wizPageFile.Location = New System.Drawing.Point(0, 0)
        Me.wizPageFile.Name = "wizPageFile"
        Me.wizPageFile.Size = New System.Drawing.Size(770, 470)
        Me.wizPageFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageFile.TabIndex = 7
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.fpnlData)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(177, 119)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = False
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(546, 350)
        Me.EZeeCollapsibleContainer1.TabIndex = 31
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fpnlData
        '
        Me.fpnlData.AutoScroll = True
        Me.fpnlData.Controls.Add(Me.gbUpdateAllocation)
        Me.fpnlData.Controls.Add(Me.gbCostCenter)
        Me.fpnlData.Controls.Add(Me.gbCategorization)
        Me.fpnlData.Controls.Add(Me.gbEOCInfo)
        Me.fpnlData.Controls.Add(Me.gbRetirementInfo)
        Me.fpnlData.Controls.Add(Me.gbProbationInfo)
        Me.fpnlData.Controls.Add(Me.gbConfirmationInfo)
        Me.fpnlData.Controls.Add(Me.gbSuspensionInfo)
        Me.fpnlData.Controls.Add(Me.gbWorkPermitInfo)
        Me.fpnlData.Controls.Add(Me.gbResidentPermitInfo)
        Me.fpnlData.Controls.Add(Me.gbExemptionInfo)
        Me.fpnlData.Location = New System.Drawing.Point(2, 2)
        Me.fpnlData.Margin = New System.Windows.Forms.Padding(0)
        Me.fpnlData.Name = "fpnlData"
        Me.fpnlData.Size = New System.Drawing.Size(543, 345)
        Me.fpnlData.TabIndex = 30
        '
        'gbUpdateAllocation
        '
        Me.gbUpdateAllocation.BorderColor = System.Drawing.Color.Black
        Me.gbUpdateAllocation.Checked = False
        Me.gbUpdateAllocation.CollapseAllExceptThis = False
        Me.gbUpdateAllocation.CollapsedHoverImage = Nothing
        Me.gbUpdateAllocation.CollapsedNormalImage = Nothing
        Me.gbUpdateAllocation.CollapsedPressedImage = Nothing
        Me.gbUpdateAllocation.CollapseOnLoad = False
        Me.gbUpdateAllocation.Controls.Add(Me.lnkAllocationFormat)
        Me.gbUpdateAllocation.Controls.Add(Me.chkAllocationChangeReson)
        Me.gbUpdateAllocation.Controls.Add(Me.dtpAllocEffDate)
        Me.gbUpdateAllocation.Controls.Add(Me.objlblAllocationCaption)
        Me.gbUpdateAllocation.Controls.Add(Me.chkBranch)
        Me.gbUpdateAllocation.Controls.Add(Me.chkDepartment)
        Me.gbUpdateAllocation.Controls.Add(Me.chkDepGroup)
        Me.gbUpdateAllocation.Controls.Add(Me.chkSecGroup)
        Me.gbUpdateAllocation.Controls.Add(Me.chkSections)
        Me.gbUpdateAllocation.Controls.Add(Me.chkUnitGroup)
        Me.gbUpdateAllocation.Controls.Add(Me.chkClassGrp)
        Me.gbUpdateAllocation.Controls.Add(Me.chkUnit)
        Me.gbUpdateAllocation.Controls.Add(Me.chkTeam)
        Me.gbUpdateAllocation.Controls.Add(Me.chkClass)
        Me.gbUpdateAllocation.ExpandedHoverImage = Nothing
        Me.gbUpdateAllocation.ExpandedNormalImage = Nothing
        Me.gbUpdateAllocation.ExpandedPressedImage = Nothing
        Me.gbUpdateAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUpdateAllocation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUpdateAllocation.HeaderHeight = 25
        Me.gbUpdateAllocation.HeaderMessage = ""
        Me.gbUpdateAllocation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbUpdateAllocation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUpdateAllocation.HeightOnCollapse = 0
        Me.gbUpdateAllocation.LeftTextSpace = 0
        Me.gbUpdateAllocation.Location = New System.Drawing.Point(3, 3)
        Me.gbUpdateAllocation.Name = "gbUpdateAllocation"
        Me.gbUpdateAllocation.OpenHeight = 300
        Me.gbUpdateAllocation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUpdateAllocation.ShowBorder = True
        Me.gbUpdateAllocation.ShowCheckBox = True
        Me.gbUpdateAllocation.ShowCollapseButton = False
        Me.gbUpdateAllocation.ShowDefaultBorderColor = True
        Me.gbUpdateAllocation.ShowDownButton = False
        Me.gbUpdateAllocation.ShowHeader = True
        Me.gbUpdateAllocation.Size = New System.Drawing.Size(520, 161)
        Me.gbUpdateAllocation.TabIndex = 24
        Me.gbUpdateAllocation.Temp = 0
        Me.gbUpdateAllocation.Text = "Allocation Effective Date"
        Me.gbUpdateAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocationFormat
        '
        Me.lnkAllocationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocationFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkAllocationFormat.Name = "lnkAllocationFormat"
        Me.lnkAllocationFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkAllocationFormat.TabIndex = 35
        Me.lnkAllocationFormat.TabStop = True
        Me.lnkAllocationFormat.Text = "Get Import Format"
        Me.lnkAllocationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkAllocationChangeReson
        '
        Me.chkAllocationChangeReson.Enabled = False
        Me.chkAllocationChangeReson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAllocationChangeReson.Location = New System.Drawing.Point(405, 59)
        Me.chkAllocationChangeReson.Name = "chkAllocationChangeReson"
        Me.chkAllocationChangeReson.Size = New System.Drawing.Size(108, 17)
        Me.chkAllocationChangeReson.TabIndex = 32
        Me.chkAllocationChangeReson.Text = "Change Reason"
        Me.chkAllocationChangeReson.UseVisualStyleBackColor = True
        '
        'dtpAllocEffDate
        '
        Me.dtpAllocEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAllocEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAllocEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpAllocEffDate.Name = "dtpAllocEffDate"
        Me.dtpAllocEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpAllocEffDate.TabIndex = 29
        '
        'objlblAllocationCaption
        '
        Me.objlblAllocationCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAllocationCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblAllocationCaption.Location = New System.Drawing.Point(12, 106)
        Me.objlblAllocationCaption.Name = "objlblAllocationCaption"
        Me.objlblAllocationCaption.Size = New System.Drawing.Size(501, 47)
        Me.objlblAllocationCaption.TabIndex = 28
        Me.objlblAllocationCaption.Text = "#value"
        '
        'chkBranch
        '
        Me.chkBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBranch.Location = New System.Drawing.Point(12, 36)
        Me.chkBranch.Name = "chkBranch"
        Me.chkBranch.Size = New System.Drawing.Size(125, 17)
        Me.chkBranch.TabIndex = 17
        Me.chkBranch.Text = "Branch"
        Me.chkBranch.UseVisualStyleBackColor = True
        '
        'chkDepartment
        '
        Me.chkDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDepartment.Location = New System.Drawing.Point(12, 82)
        Me.chkDepartment.Name = "chkDepartment"
        Me.chkDepartment.Size = New System.Drawing.Size(125, 17)
        Me.chkDepartment.TabIndex = 5
        Me.chkDepartment.Text = "Department"
        Me.chkDepartment.UseVisualStyleBackColor = True
        '
        'chkDepGroup
        '
        Me.chkDepGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDepGroup.Location = New System.Drawing.Point(12, 59)
        Me.chkDepGroup.Name = "chkDepGroup"
        Me.chkDepGroup.Size = New System.Drawing.Size(125, 17)
        Me.chkDepGroup.TabIndex = 18
        Me.chkDepGroup.Text = "Department Group"
        Me.chkDepGroup.UseVisualStyleBackColor = True
        '
        'chkSecGroup
        '
        Me.chkSecGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSecGroup.Location = New System.Drawing.Point(143, 36)
        Me.chkSecGroup.Name = "chkSecGroup"
        Me.chkSecGroup.Size = New System.Drawing.Size(125, 17)
        Me.chkSecGroup.TabIndex = 19
        Me.chkSecGroup.Text = "Sec. Group"
        Me.chkSecGroup.UseVisualStyleBackColor = True
        '
        'chkSections
        '
        Me.chkSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSections.Location = New System.Drawing.Point(143, 59)
        Me.chkSections.Name = "chkSections"
        Me.chkSections.Size = New System.Drawing.Size(125, 17)
        Me.chkSections.TabIndex = 19
        Me.chkSections.Text = "Sections"
        Me.chkSections.UseVisualStyleBackColor = True
        '
        'chkUnitGroup
        '
        Me.chkUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnitGroup.Location = New System.Drawing.Point(143, 82)
        Me.chkUnitGroup.Name = "chkUnitGroup"
        Me.chkUnitGroup.Size = New System.Drawing.Size(125, 17)
        Me.chkUnitGroup.TabIndex = 20
        Me.chkUnitGroup.Text = "Unit Group"
        Me.chkUnitGroup.UseVisualStyleBackColor = True
        '
        'chkClassGrp
        '
        Me.chkClassGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClassGrp.Location = New System.Drawing.Point(274, 82)
        Me.chkClassGrp.Name = "chkClassGrp"
        Me.chkClassGrp.Size = New System.Drawing.Size(125, 17)
        Me.chkClassGrp.TabIndex = 6
        Me.chkClassGrp.Text = "Class Group"
        Me.chkClassGrp.UseVisualStyleBackColor = True
        '
        'chkUnit
        '
        Me.chkUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnit.Location = New System.Drawing.Point(274, 36)
        Me.chkUnit.Name = "chkUnit"
        Me.chkUnit.Size = New System.Drawing.Size(125, 17)
        Me.chkUnit.TabIndex = 20
        Me.chkUnit.Text = "Unit"
        Me.chkUnit.UseVisualStyleBackColor = True
        '
        'chkTeam
        '
        Me.chkTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTeam.Location = New System.Drawing.Point(274, 59)
        Me.chkTeam.Name = "chkTeam"
        Me.chkTeam.Size = New System.Drawing.Size(125, 17)
        Me.chkTeam.TabIndex = 21
        Me.chkTeam.Text = "Team"
        Me.chkTeam.UseVisualStyleBackColor = True
        '
        'chkClass
        '
        Me.chkClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClass.Location = New System.Drawing.Point(405, 36)
        Me.chkClass.Name = "chkClass"
        Me.chkClass.Size = New System.Drawing.Size(108, 17)
        Me.chkClass.TabIndex = 9
        Me.chkClass.Text = "Class"
        Me.chkClass.UseVisualStyleBackColor = True
        '
        'gbCostCenter
        '
        Me.gbCostCenter.BorderColor = System.Drawing.Color.Black
        Me.gbCostCenter.Checked = False
        Me.gbCostCenter.CollapseAllExceptThis = False
        Me.gbCostCenter.CollapsedHoverImage = Nothing
        Me.gbCostCenter.CollapsedNormalImage = Nothing
        Me.gbCostCenter.CollapsedPressedImage = Nothing
        Me.gbCostCenter.CollapseOnLoad = False
        Me.gbCostCenter.Controls.Add(Me.lnkCostCenterFormat)
        Me.gbCostCenter.Controls.Add(Me.chkCcChangeReason)
        Me.gbCostCenter.Controls.Add(Me.dtpCCEffDate)
        Me.gbCostCenter.Controls.Add(Me.objlblCCCaption)
        Me.gbCostCenter.Controls.Add(Me.chkCostCenter)
        Me.gbCostCenter.ExpandedHoverImage = Nothing
        Me.gbCostCenter.ExpandedNormalImage = Nothing
        Me.gbCostCenter.ExpandedPressedImage = Nothing
        Me.gbCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCostCenter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCostCenter.HeaderHeight = 25
        Me.gbCostCenter.HeaderMessage = ""
        Me.gbCostCenter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbCostCenter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCostCenter.HeightOnCollapse = 0
        Me.gbCostCenter.LeftTextSpace = 0
        Me.gbCostCenter.Location = New System.Drawing.Point(3, 170)
        Me.gbCostCenter.Name = "gbCostCenter"
        Me.gbCostCenter.OpenHeight = 300
        Me.gbCostCenter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCostCenter.ShowBorder = True
        Me.gbCostCenter.ShowCheckBox = True
        Me.gbCostCenter.ShowCollapseButton = False
        Me.gbCostCenter.ShowDefaultBorderColor = True
        Me.gbCostCenter.ShowDownButton = False
        Me.gbCostCenter.ShowHeader = True
        Me.gbCostCenter.Size = New System.Drawing.Size(520, 113)
        Me.gbCostCenter.TabIndex = 28
        Me.gbCostCenter.Temp = 0
        Me.gbCostCenter.Text = "Cost center Effective Date"
        Me.gbCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkCostCenterFormat
        '
        Me.lnkCostCenterFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkCostCenterFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkCostCenterFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkCostCenterFormat.Name = "lnkCostCenterFormat"
        Me.lnkCostCenterFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkCostCenterFormat.TabIndex = 36
        Me.lnkCostCenterFormat.TabStop = True
        Me.lnkCostCenterFormat.Text = "Get Import Format"
        Me.lnkCostCenterFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkCcChangeReason
        '
        Me.chkCcChangeReason.Enabled = False
        Me.chkCcChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCcChangeReason.Location = New System.Drawing.Point(143, 34)
        Me.chkCcChangeReason.Name = "chkCcChangeReason"
        Me.chkCcChangeReason.Size = New System.Drawing.Size(125, 17)
        Me.chkCcChangeReason.TabIndex = 31
        Me.chkCcChangeReason.Text = "Change Reason"
        Me.chkCcChangeReason.UseVisualStyleBackColor = True
        '
        'dtpCCEffDate
        '
        Me.dtpCCEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCCEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCCEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpCCEffDate.Name = "dtpCCEffDate"
        Me.dtpCCEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpCCEffDate.TabIndex = 28
        '
        'objlblCCCaption
        '
        Me.objlblCCCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCCCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblCCCaption.Location = New System.Drawing.Point(9, 64)
        Me.objlblCCCaption.Name = "objlblCCCaption"
        Me.objlblCCCaption.Size = New System.Drawing.Size(501, 39)
        Me.objlblCCCaption.TabIndex = 27
        Me.objlblCCCaption.Text = "#value"
        '
        'chkCostCenter
        '
        Me.chkCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCostCenter.Location = New System.Drawing.Point(12, 34)
        Me.chkCostCenter.Name = "chkCostCenter"
        Me.chkCostCenter.Size = New System.Drawing.Size(125, 17)
        Me.chkCostCenter.TabIndex = 21
        Me.chkCostCenter.Text = "Cost Center"
        Me.chkCostCenter.UseVisualStyleBackColor = True
        '
        'gbCategorization
        '
        Me.gbCategorization.BorderColor = System.Drawing.Color.Black
        Me.gbCategorization.Checked = False
        Me.gbCategorization.CollapseAllExceptThis = False
        Me.gbCategorization.CollapsedHoverImage = Nothing
        Me.gbCategorization.CollapsedNormalImage = Nothing
        Me.gbCategorization.CollapsedPressedImage = Nothing
        Me.gbCategorization.CollapseOnLoad = False
        Me.gbCategorization.Controls.Add(Me.lnkCategorizeFormat)
        Me.gbCategorization.Controls.Add(Me.chkCateChangeReason)
        Me.gbCategorization.Controls.Add(Me.dtpCatEffDate)
        Me.gbCategorization.Controls.Add(Me.objlbJobCaption)
        Me.gbCategorization.Controls.Add(Me.chkJobGroup)
        Me.gbCategorization.Controls.Add(Me.chkJob)
        Me.gbCategorization.ExpandedHoverImage = Nothing
        Me.gbCategorization.ExpandedNormalImage = Nothing
        Me.gbCategorization.ExpandedPressedImage = Nothing
        Me.gbCategorization.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCategorization.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCategorization.HeaderHeight = 25
        Me.gbCategorization.HeaderMessage = ""
        Me.gbCategorization.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbCategorization.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCategorization.HeightOnCollapse = 0
        Me.gbCategorization.LeftTextSpace = 0
        Me.gbCategorization.Location = New System.Drawing.Point(3, 289)
        Me.gbCategorization.Name = "gbCategorization"
        Me.gbCategorization.OpenHeight = 300
        Me.gbCategorization.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCategorization.ShowBorder = True
        Me.gbCategorization.ShowCheckBox = True
        Me.gbCategorization.ShowCollapseButton = False
        Me.gbCategorization.ShowDefaultBorderColor = True
        Me.gbCategorization.ShowDownButton = False
        Me.gbCategorization.ShowHeader = True
        Me.gbCategorization.Size = New System.Drawing.Size(520, 112)
        Me.gbCategorization.TabIndex = 29
        Me.gbCategorization.Temp = 0
        Me.gbCategorization.Text = "Categorization Effective Date"
        Me.gbCategorization.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkCategorizeFormat
        '
        Me.lnkCategorizeFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkCategorizeFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkCategorizeFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkCategorizeFormat.Name = "lnkCategorizeFormat"
        Me.lnkCategorizeFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkCategorizeFormat.TabIndex = 37
        Me.lnkCategorizeFormat.TabStop = True
        Me.lnkCategorizeFormat.Text = "Get Import Format"
        Me.lnkCategorizeFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkCateChangeReason
        '
        Me.chkCateChangeReason.Enabled = False
        Me.chkCateChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCateChangeReason.Location = New System.Drawing.Point(274, 35)
        Me.chkCateChangeReason.Name = "chkCateChangeReason"
        Me.chkCateChangeReason.Size = New System.Drawing.Size(125, 17)
        Me.chkCateChangeReason.TabIndex = 32
        Me.chkCateChangeReason.Text = "Change Reason"
        Me.chkCateChangeReason.UseVisualStyleBackColor = True
        '
        'dtpCatEffDate
        '
        Me.dtpCatEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCatEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCatEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpCatEffDate.Name = "dtpCatEffDate"
        Me.dtpCatEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpCatEffDate.TabIndex = 27
        '
        'objlbJobCaption
        '
        Me.objlbJobCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlbJobCaption.ForeColor = System.Drawing.Color.Red
        Me.objlbJobCaption.Location = New System.Drawing.Point(9, 63)
        Me.objlbJobCaption.Name = "objlbJobCaption"
        Me.objlbJobCaption.Size = New System.Drawing.Size(501, 40)
        Me.objlbJobCaption.TabIndex = 26
        Me.objlbJobCaption.Text = "#value"
        '
        'chkJobGroup
        '
        Me.chkJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkJobGroup.Location = New System.Drawing.Point(10, 35)
        Me.chkJobGroup.Name = "chkJobGroup"
        Me.chkJobGroup.Size = New System.Drawing.Size(126, 17)
        Me.chkJobGroup.TabIndex = 21
        Me.chkJobGroup.Text = "Job Group"
        Me.chkJobGroup.UseVisualStyleBackColor = True
        '
        'chkJob
        '
        Me.chkJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkJob.Location = New System.Drawing.Point(142, 35)
        Me.chkJob.Name = "chkJob"
        Me.chkJob.Size = New System.Drawing.Size(125, 17)
        Me.chkJob.TabIndex = 8
        Me.chkJob.Text = "Job"
        Me.chkJob.UseVisualStyleBackColor = True
        '
        'gbEOCInfo
        '
        Me.gbEOCInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEOCInfo.Checked = False
        Me.gbEOCInfo.CollapseAllExceptThis = False
        Me.gbEOCInfo.CollapsedHoverImage = Nothing
        Me.gbEOCInfo.CollapsedNormalImage = Nothing
        Me.gbEOCInfo.CollapsedPressedImage = Nothing
        Me.gbEOCInfo.CollapseOnLoad = False
        Me.gbEOCInfo.Controls.Add(Me.lnkTerminationFormat)
        Me.gbEOCInfo.Controls.Add(Me.chkIsexcludePayroll)
        Me.gbEOCInfo.Controls.Add(Me.dtpTerEffDate)
        Me.gbEOCInfo.Controls.Add(Me.objlblEOCCaption)
        Me.gbEOCInfo.Controls.Add(Me.chkEOCDate)
        Me.gbEOCInfo.Controls.Add(Me.chkEOCReason)
        Me.gbEOCInfo.Controls.Add(Me.chkLeavingDate)
        Me.gbEOCInfo.ExpandedHoverImage = Nothing
        Me.gbEOCInfo.ExpandedNormalImage = Nothing
        Me.gbEOCInfo.ExpandedPressedImage = Nothing
        Me.gbEOCInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEOCInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEOCInfo.HeaderHeight = 25
        Me.gbEOCInfo.HeaderMessage = ""
        Me.gbEOCInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbEOCInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEOCInfo.HeightOnCollapse = 0
        Me.gbEOCInfo.LeftTextSpace = 0
        Me.gbEOCInfo.Location = New System.Drawing.Point(3, 407)
        Me.gbEOCInfo.Name = "gbEOCInfo"
        Me.gbEOCInfo.OpenHeight = 300
        Me.gbEOCInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEOCInfo.ShowBorder = True
        Me.gbEOCInfo.ShowCheckBox = True
        Me.gbEOCInfo.ShowCollapseButton = False
        Me.gbEOCInfo.ShowDefaultBorderColor = True
        Me.gbEOCInfo.ShowDownButton = False
        Me.gbEOCInfo.ShowHeader = True
        Me.gbEOCInfo.Size = New System.Drawing.Size(520, 138)
        Me.gbEOCInfo.TabIndex = 25
        Me.gbEOCInfo.Temp = 0
        Me.gbEOCInfo.Text = "Termination Effective Date"
        Me.gbEOCInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkTerminationFormat
        '
        Me.lnkTerminationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkTerminationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkTerminationFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkTerminationFormat.Name = "lnkTerminationFormat"
        Me.lnkTerminationFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkTerminationFormat.TabIndex = 38
        Me.lnkTerminationFormat.TabStop = True
        Me.lnkTerminationFormat.Text = "Get Import Format"
        Me.lnkTerminationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkIsexcludePayroll
        '
        Me.chkIsexcludePayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsexcludePayroll.Location = New System.Drawing.Point(142, 57)
        Me.chkIsexcludePayroll.Name = "chkIsexcludePayroll"
        Me.chkIsexcludePayroll.Size = New System.Drawing.Size(126, 17)
        Me.chkIsexcludePayroll.TabIndex = 29
        Me.chkIsexcludePayroll.Text = "Isexclude Payroll"
        Me.chkIsexcludePayroll.UseVisualStyleBackColor = True
        '
        'dtpTerEffDate
        '
        Me.dtpTerEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTerEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTerEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpTerEffDate.Name = "dtpTerEffDate"
        Me.dtpTerEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpTerEffDate.TabIndex = 26
        '
        'objlblEOCCaption
        '
        Me.objlblEOCCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEOCCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblEOCCaption.Location = New System.Drawing.Point(8, 84)
        Me.objlblEOCCaption.Name = "objlblEOCCaption"
        Me.objlblEOCCaption.Size = New System.Drawing.Size(501, 41)
        Me.objlblEOCCaption.TabIndex = 25
        Me.objlblEOCCaption.Text = "#value"
        '
        'chkEOCDate
        '
        Me.chkEOCDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEOCDate.Location = New System.Drawing.Point(10, 34)
        Me.chkEOCDate.Name = "chkEOCDate"
        Me.chkEOCDate.Size = New System.Drawing.Size(126, 17)
        Me.chkEOCDate.TabIndex = 12
        Me.chkEOCDate.Text = "EOC Date"
        Me.chkEOCDate.UseVisualStyleBackColor = True
        '
        'chkEOCReason
        '
        Me.chkEOCReason.Enabled = False
        Me.chkEOCReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEOCReason.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkEOCReason.Location = New System.Drawing.Point(142, 34)
        Me.chkEOCReason.Name = "chkEOCReason"
        Me.chkEOCReason.Size = New System.Drawing.Size(126, 17)
        Me.chkEOCReason.TabIndex = 23
        Me.chkEOCReason.Text = "EOC Reason"
        Me.chkEOCReason.UseVisualStyleBackColor = True
        '
        'chkLeavingDate
        '
        Me.chkLeavingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLeavingDate.Location = New System.Drawing.Point(10, 57)
        Me.chkLeavingDate.Name = "chkLeavingDate"
        Me.chkLeavingDate.Size = New System.Drawing.Size(126, 17)
        Me.chkLeavingDate.TabIndex = 13
        Me.chkLeavingDate.Text = "Leaving Date"
        Me.chkLeavingDate.UseVisualStyleBackColor = True
        '
        'gbRetirementInfo
        '
        Me.gbRetirementInfo.BorderColor = System.Drawing.Color.Black
        Me.gbRetirementInfo.Checked = False
        Me.gbRetirementInfo.CollapseAllExceptThis = False
        Me.gbRetirementInfo.CollapsedHoverImage = Nothing
        Me.gbRetirementInfo.CollapsedNormalImage = Nothing
        Me.gbRetirementInfo.CollapsedPressedImage = Nothing
        Me.gbRetirementInfo.CollapseOnLoad = False
        Me.gbRetirementInfo.Controls.Add(Me.lnkRetirementFormat)
        Me.gbRetirementInfo.Controls.Add(Me.chkRtChangeReason)
        Me.gbRetirementInfo.Controls.Add(Me.dtpRetrEffDate)
        Me.gbRetirementInfo.Controls.Add(Me.chkRetirementDt)
        Me.gbRetirementInfo.Controls.Add(Me.objlblRetireCaption)
        Me.gbRetirementInfo.ExpandedHoverImage = Nothing
        Me.gbRetirementInfo.ExpandedNormalImage = Nothing
        Me.gbRetirementInfo.ExpandedPressedImage = Nothing
        Me.gbRetirementInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRetirementInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRetirementInfo.HeaderHeight = 25
        Me.gbRetirementInfo.HeaderMessage = ""
        Me.gbRetirementInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbRetirementInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRetirementInfo.HeightOnCollapse = 0
        Me.gbRetirementInfo.LeftTextSpace = 0
        Me.gbRetirementInfo.Location = New System.Drawing.Point(3, 551)
        Me.gbRetirementInfo.Name = "gbRetirementInfo"
        Me.gbRetirementInfo.OpenHeight = 300
        Me.gbRetirementInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRetirementInfo.ShowBorder = True
        Me.gbRetirementInfo.ShowCheckBox = True
        Me.gbRetirementInfo.ShowCollapseButton = False
        Me.gbRetirementInfo.ShowDefaultBorderColor = True
        Me.gbRetirementInfo.ShowDownButton = False
        Me.gbRetirementInfo.ShowHeader = True
        Me.gbRetirementInfo.Size = New System.Drawing.Size(520, 109)
        Me.gbRetirementInfo.TabIndex = 28
        Me.gbRetirementInfo.Temp = 0
        Me.gbRetirementInfo.Text = "Retirement Effective Date"
        Me.gbRetirementInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkRetirementFormat
        '
        Me.lnkRetirementFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkRetirementFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkRetirementFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkRetirementFormat.Name = "lnkRetirementFormat"
        Me.lnkRetirementFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkRetirementFormat.TabIndex = 38
        Me.lnkRetirementFormat.TabStop = True
        Me.lnkRetirementFormat.Text = "Get Import Format"
        Me.lnkRetirementFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkRtChangeReason
        '
        Me.chkRtChangeReason.Enabled = False
        Me.chkRtChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRtChangeReason.Location = New System.Drawing.Point(142, 35)
        Me.chkRtChangeReason.Name = "chkRtChangeReason"
        Me.chkRtChangeReason.Size = New System.Drawing.Size(126, 17)
        Me.chkRtChangeReason.TabIndex = 10
        Me.chkRtChangeReason.Text = "Change Reason"
        Me.chkRtChangeReason.UseVisualStyleBackColor = True
        '
        'dtpRetrEffDate
        '
        Me.dtpRetrEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpRetrEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRetrEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpRetrEffDate.Name = "dtpRetrEffDate"
        Me.dtpRetrEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpRetrEffDate.TabIndex = 7
        '
        'chkRetirementDt
        '
        Me.chkRetirementDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRetirementDt.Location = New System.Drawing.Point(10, 35)
        Me.chkRetirementDt.Name = "chkRetirementDt"
        Me.chkRetirementDt.Size = New System.Drawing.Size(126, 17)
        Me.chkRetirementDt.TabIndex = 4
        Me.chkRetirementDt.Text = "Retirement Date"
        Me.chkRetirementDt.UseVisualStyleBackColor = True
        '
        'objlblRetireCaption
        '
        Me.objlblRetireCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblRetireCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblRetireCaption.Location = New System.Drawing.Point(12, 64)
        Me.objlblRetireCaption.Name = "objlblRetireCaption"
        Me.objlblRetireCaption.Size = New System.Drawing.Size(501, 34)
        Me.objlblRetireCaption.TabIndex = 6
        Me.objlblRetireCaption.Text = "#value"
        '
        'gbProbationInfo
        '
        Me.gbProbationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbProbationInfo.Checked = False
        Me.gbProbationInfo.CollapseAllExceptThis = False
        Me.gbProbationInfo.CollapsedHoverImage = Nothing
        Me.gbProbationInfo.CollapsedNormalImage = Nothing
        Me.gbProbationInfo.CollapsedPressedImage = Nothing
        Me.gbProbationInfo.CollapseOnLoad = False
        Me.gbProbationInfo.Controls.Add(Me.chkProbationToDt)
        Me.gbProbationInfo.Controls.Add(Me.lnkProbationFormat)
        Me.gbProbationInfo.Controls.Add(Me.chkPbChangeReason)
        Me.gbProbationInfo.Controls.Add(Me.dtpPbEffDate)
        Me.gbProbationInfo.Controls.Add(Me.chkProbationFromDt)
        Me.gbProbationInfo.Controls.Add(Me.objlblProbationCaption)
        Me.gbProbationInfo.ExpandedHoverImage = Nothing
        Me.gbProbationInfo.ExpandedNormalImage = Nothing
        Me.gbProbationInfo.ExpandedPressedImage = Nothing
        Me.gbProbationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProbationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProbationInfo.HeaderHeight = 25
        Me.gbProbationInfo.HeaderMessage = ""
        Me.gbProbationInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbProbationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProbationInfo.HeightOnCollapse = 0
        Me.gbProbationInfo.LeftTextSpace = 0
        Me.gbProbationInfo.Location = New System.Drawing.Point(3, 666)
        Me.gbProbationInfo.Name = "gbProbationInfo"
        Me.gbProbationInfo.OpenHeight = 300
        Me.gbProbationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProbationInfo.ShowBorder = True
        Me.gbProbationInfo.ShowCheckBox = True
        Me.gbProbationInfo.ShowCollapseButton = False
        Me.gbProbationInfo.ShowDefaultBorderColor = True
        Me.gbProbationInfo.ShowDownButton = False
        Me.gbProbationInfo.ShowHeader = True
        Me.gbProbationInfo.Size = New System.Drawing.Size(520, 135)
        Me.gbProbationInfo.TabIndex = 41
        Me.gbProbationInfo.Temp = 0
        Me.gbProbationInfo.Text = "Probation Effective Date"
        Me.gbProbationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkProbationToDt
        '
        Me.chkProbationToDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkProbationToDt.Location = New System.Drawing.Point(10, 58)
        Me.chkProbationToDt.Name = "chkProbationToDt"
        Me.chkProbationToDt.Size = New System.Drawing.Size(126, 17)
        Me.chkProbationToDt.TabIndex = 41
        Me.chkProbationToDt.Text = "To Date"
        Me.chkProbationToDt.UseVisualStyleBackColor = True
        '
        'lnkProbationFormat
        '
        Me.lnkProbationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkProbationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkProbationFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkProbationFormat.Name = "lnkProbationFormat"
        Me.lnkProbationFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkProbationFormat.TabIndex = 38
        Me.lnkProbationFormat.TabStop = True
        Me.lnkProbationFormat.Text = "Get Import Format"
        Me.lnkProbationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkPbChangeReason
        '
        Me.chkPbChangeReason.Enabled = False
        Me.chkPbChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPbChangeReason.Location = New System.Drawing.Point(143, 35)
        Me.chkPbChangeReason.Name = "chkPbChangeReason"
        Me.chkPbChangeReason.Size = New System.Drawing.Size(126, 17)
        Me.chkPbChangeReason.TabIndex = 10
        Me.chkPbChangeReason.Text = "Change Reason"
        Me.chkPbChangeReason.UseVisualStyleBackColor = True
        '
        'dtpPbEffDate
        '
        Me.dtpPbEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPbEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPbEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpPbEffDate.Name = "dtpPbEffDate"
        Me.dtpPbEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpPbEffDate.TabIndex = 7
        '
        'chkProbationFromDt
        '
        Me.chkProbationFromDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkProbationFromDt.Location = New System.Drawing.Point(10, 35)
        Me.chkProbationFromDt.Name = "chkProbationFromDt"
        Me.chkProbationFromDt.Size = New System.Drawing.Size(126, 17)
        Me.chkProbationFromDt.TabIndex = 4
        Me.chkProbationFromDt.Text = "From Date"
        Me.chkProbationFromDt.UseVisualStyleBackColor = True
        '
        'objlblProbationCaption
        '
        Me.objlblProbationCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProbationCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblProbationCaption.Location = New System.Drawing.Point(12, 87)
        Me.objlblProbationCaption.Name = "objlblProbationCaption"
        Me.objlblProbationCaption.Size = New System.Drawing.Size(501, 34)
        Me.objlblProbationCaption.TabIndex = 6
        Me.objlblProbationCaption.Text = "#value"
        '
        'gbConfirmationInfo
        '
        Me.gbConfirmationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbConfirmationInfo.Checked = False
        Me.gbConfirmationInfo.CollapseAllExceptThis = False
        Me.gbConfirmationInfo.CollapsedHoverImage = Nothing
        Me.gbConfirmationInfo.CollapsedNormalImage = Nothing
        Me.gbConfirmationInfo.CollapsedPressedImage = Nothing
        Me.gbConfirmationInfo.CollapseOnLoad = False
        Me.gbConfirmationInfo.Controls.Add(Me.lnkConfirmationFormat)
        Me.gbConfirmationInfo.Controls.Add(Me.chkConfChangeReason)
        Me.gbConfirmationInfo.Controls.Add(Me.dtpConEffDate)
        Me.gbConfirmationInfo.Controls.Add(Me.chkConfirmationDt)
        Me.gbConfirmationInfo.Controls.Add(Me.objlblConfirmationCaption)
        Me.gbConfirmationInfo.ExpandedHoverImage = Nothing
        Me.gbConfirmationInfo.ExpandedNormalImage = Nothing
        Me.gbConfirmationInfo.ExpandedPressedImage = Nothing
        Me.gbConfirmationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbConfirmationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbConfirmationInfo.HeaderHeight = 25
        Me.gbConfirmationInfo.HeaderMessage = ""
        Me.gbConfirmationInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbConfirmationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbConfirmationInfo.HeightOnCollapse = 0
        Me.gbConfirmationInfo.LeftTextSpace = 0
        Me.gbConfirmationInfo.Location = New System.Drawing.Point(3, 807)
        Me.gbConfirmationInfo.Name = "gbConfirmationInfo"
        Me.gbConfirmationInfo.OpenHeight = 300
        Me.gbConfirmationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbConfirmationInfo.ShowBorder = True
        Me.gbConfirmationInfo.ShowCheckBox = True
        Me.gbConfirmationInfo.ShowCollapseButton = False
        Me.gbConfirmationInfo.ShowDefaultBorderColor = True
        Me.gbConfirmationInfo.ShowDownButton = False
        Me.gbConfirmationInfo.ShowHeader = True
        Me.gbConfirmationInfo.Size = New System.Drawing.Size(520, 120)
        Me.gbConfirmationInfo.TabIndex = 42
        Me.gbConfirmationInfo.Temp = 0
        Me.gbConfirmationInfo.Text = "Confirmation Effective Date"
        Me.gbConfirmationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkConfirmationFormat
        '
        Me.lnkConfirmationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkConfirmationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkConfirmationFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkConfirmationFormat.Name = "lnkConfirmationFormat"
        Me.lnkConfirmationFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkConfirmationFormat.TabIndex = 38
        Me.lnkConfirmationFormat.TabStop = True
        Me.lnkConfirmationFormat.Text = "Get Import Format"
        Me.lnkConfirmationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkConfChangeReason
        '
        Me.chkConfChangeReason.Enabled = False
        Me.chkConfChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConfChangeReason.Location = New System.Drawing.Point(143, 35)
        Me.chkConfChangeReason.Name = "chkConfChangeReason"
        Me.chkConfChangeReason.Size = New System.Drawing.Size(126, 17)
        Me.chkConfChangeReason.TabIndex = 10
        Me.chkConfChangeReason.Text = "Change Reason"
        Me.chkConfChangeReason.UseVisualStyleBackColor = True
        '
        'dtpConEffDate
        '
        Me.dtpConEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpConEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpConEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpConEffDate.Name = "dtpConEffDate"
        Me.dtpConEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpConEffDate.TabIndex = 7
        '
        'chkConfirmationDt
        '
        Me.chkConfirmationDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConfirmationDt.Location = New System.Drawing.Point(10, 35)
        Me.chkConfirmationDt.Name = "chkConfirmationDt"
        Me.chkConfirmationDt.Size = New System.Drawing.Size(126, 17)
        Me.chkConfirmationDt.TabIndex = 4
        Me.chkConfirmationDt.Text = "Confirmation Date"
        Me.chkConfirmationDt.UseVisualStyleBackColor = True
        '
        'objlblConfirmationCaption
        '
        Me.objlblConfirmationCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblConfirmationCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblConfirmationCaption.Location = New System.Drawing.Point(12, 69)
        Me.objlblConfirmationCaption.Name = "objlblConfirmationCaption"
        Me.objlblConfirmationCaption.Size = New System.Drawing.Size(501, 34)
        Me.objlblConfirmationCaption.TabIndex = 6
        Me.objlblConfirmationCaption.Text = "#value"
        '
        'gbSuspensionInfo
        '
        Me.gbSuspensionInfo.BorderColor = System.Drawing.Color.Black
        Me.gbSuspensionInfo.Checked = False
        Me.gbSuspensionInfo.CollapseAllExceptThis = False
        Me.gbSuspensionInfo.CollapsedHoverImage = Nothing
        Me.gbSuspensionInfo.CollapsedNormalImage = Nothing
        Me.gbSuspensionInfo.CollapsedPressedImage = Nothing
        Me.gbSuspensionInfo.CollapseOnLoad = False
        Me.gbSuspensionInfo.Controls.Add(Me.chkSuspensionToDt)
        Me.gbSuspensionInfo.Controls.Add(Me.lnkSuspensionFormat)
        Me.gbSuspensionInfo.Controls.Add(Me.chkSuspChangeReason)
        Me.gbSuspensionInfo.Controls.Add(Me.dtpSuspEffDate)
        Me.gbSuspensionInfo.Controls.Add(Me.chkSuspensionFrmDt)
        Me.gbSuspensionInfo.Controls.Add(Me.objlblSuspensionCaption)
        Me.gbSuspensionInfo.ExpandedHoverImage = Nothing
        Me.gbSuspensionInfo.ExpandedNormalImage = Nothing
        Me.gbSuspensionInfo.ExpandedPressedImage = Nothing
        Me.gbSuspensionInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSuspensionInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSuspensionInfo.HeaderHeight = 25
        Me.gbSuspensionInfo.HeaderMessage = ""
        Me.gbSuspensionInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbSuspensionInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSuspensionInfo.HeightOnCollapse = 0
        Me.gbSuspensionInfo.LeftTextSpace = 0
        Me.gbSuspensionInfo.Location = New System.Drawing.Point(3, 933)
        Me.gbSuspensionInfo.Name = "gbSuspensionInfo"
        Me.gbSuspensionInfo.OpenHeight = 300
        Me.gbSuspensionInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSuspensionInfo.ShowBorder = True
        Me.gbSuspensionInfo.ShowCheckBox = True
        Me.gbSuspensionInfo.ShowCollapseButton = False
        Me.gbSuspensionInfo.ShowDefaultBorderColor = True
        Me.gbSuspensionInfo.ShowDownButton = False
        Me.gbSuspensionInfo.ShowHeader = True
        Me.gbSuspensionInfo.Size = New System.Drawing.Size(520, 135)
        Me.gbSuspensionInfo.TabIndex = 42
        Me.gbSuspensionInfo.Temp = 0
        Me.gbSuspensionInfo.Text = "Suspension Effective Date"
        Me.gbSuspensionInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSuspensionToDt
        '
        Me.chkSuspensionToDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSuspensionToDt.Location = New System.Drawing.Point(10, 58)
        Me.chkSuspensionToDt.Name = "chkSuspensionToDt"
        Me.chkSuspensionToDt.Size = New System.Drawing.Size(126, 17)
        Me.chkSuspensionToDt.TabIndex = 41
        Me.chkSuspensionToDt.Text = "To Date"
        Me.chkSuspensionToDt.UseVisualStyleBackColor = True
        '
        'lnkSuspensionFormat
        '
        Me.lnkSuspensionFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkSuspensionFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSuspensionFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkSuspensionFormat.Name = "lnkSuspensionFormat"
        Me.lnkSuspensionFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkSuspensionFormat.TabIndex = 38
        Me.lnkSuspensionFormat.TabStop = True
        Me.lnkSuspensionFormat.Text = "Get Import Format"
        Me.lnkSuspensionFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkSuspChangeReason
        '
        Me.chkSuspChangeReason.Enabled = False
        Me.chkSuspChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSuspChangeReason.Location = New System.Drawing.Point(143, 35)
        Me.chkSuspChangeReason.Name = "chkSuspChangeReason"
        Me.chkSuspChangeReason.Size = New System.Drawing.Size(126, 17)
        Me.chkSuspChangeReason.TabIndex = 10
        Me.chkSuspChangeReason.Text = "Change Reason"
        Me.chkSuspChangeReason.UseVisualStyleBackColor = True
        '
        'dtpSuspEffDate
        '
        Me.dtpSuspEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpSuspEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpSuspEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpSuspEffDate.Name = "dtpSuspEffDate"
        Me.dtpSuspEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpSuspEffDate.TabIndex = 7
        '
        'chkSuspensionFrmDt
        '
        Me.chkSuspensionFrmDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSuspensionFrmDt.Location = New System.Drawing.Point(10, 35)
        Me.chkSuspensionFrmDt.Name = "chkSuspensionFrmDt"
        Me.chkSuspensionFrmDt.Size = New System.Drawing.Size(126, 17)
        Me.chkSuspensionFrmDt.TabIndex = 4
        Me.chkSuspensionFrmDt.Text = "From Date"
        Me.chkSuspensionFrmDt.UseVisualStyleBackColor = True
        '
        'objlblSuspensionCaption
        '
        Me.objlblSuspensionCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSuspensionCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblSuspensionCaption.Location = New System.Drawing.Point(12, 87)
        Me.objlblSuspensionCaption.Name = "objlblSuspensionCaption"
        Me.objlblSuspensionCaption.Size = New System.Drawing.Size(501, 34)
        Me.objlblSuspensionCaption.TabIndex = 6
        Me.objlblSuspensionCaption.Text = "#value"
        '
        'gbWorkPermitInfo
        '
        Me.gbWorkPermitInfo.BorderColor = System.Drawing.Color.Black
        Me.gbWorkPermitInfo.Checked = False
        Me.gbWorkPermitInfo.CollapseAllExceptThis = False
        Me.gbWorkPermitInfo.CollapsedHoverImage = Nothing
        Me.gbWorkPermitInfo.CollapsedNormalImage = Nothing
        Me.gbWorkPermitInfo.CollapsedPressedImage = Nothing
        Me.gbWorkPermitInfo.CollapseOnLoad = False
        Me.gbWorkPermitInfo.Controls.Add(Me.chkWpIssuePlace)
        Me.gbWorkPermitInfo.Controls.Add(Me.chkWpCountry)
        Me.gbWorkPermitInfo.Controls.Add(Me.chkWpExpiryDt)
        Me.gbWorkPermitInfo.Controls.Add(Me.chkWpIssueDt)
        Me.gbWorkPermitInfo.Controls.Add(Me.lnkWorkPermitFormat)
        Me.gbWorkPermitInfo.Controls.Add(Me.chkWpChangeReason)
        Me.gbWorkPermitInfo.Controls.Add(Me.dtpWpEffDate)
        Me.gbWorkPermitInfo.Controls.Add(Me.ChkWorkPermitNo)
        Me.gbWorkPermitInfo.Controls.Add(Me.objlblWorkPermitCaption)
        Me.gbWorkPermitInfo.ExpandedHoverImage = Nothing
        Me.gbWorkPermitInfo.ExpandedNormalImage = Nothing
        Me.gbWorkPermitInfo.ExpandedPressedImage = Nothing
        Me.gbWorkPermitInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbWorkPermitInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbWorkPermitInfo.HeaderHeight = 25
        Me.gbWorkPermitInfo.HeaderMessage = ""
        Me.gbWorkPermitInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbWorkPermitInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbWorkPermitInfo.HeightOnCollapse = 0
        Me.gbWorkPermitInfo.LeftTextSpace = 0
        Me.gbWorkPermitInfo.Location = New System.Drawing.Point(3, 1074)
        Me.gbWorkPermitInfo.Name = "gbWorkPermitInfo"
        Me.gbWorkPermitInfo.OpenHeight = 300
        Me.gbWorkPermitInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbWorkPermitInfo.ShowBorder = True
        Me.gbWorkPermitInfo.ShowCheckBox = True
        Me.gbWorkPermitInfo.ShowCollapseButton = False
        Me.gbWorkPermitInfo.ShowDefaultBorderColor = True
        Me.gbWorkPermitInfo.ShowDownButton = False
        Me.gbWorkPermitInfo.ShowHeader = True
        Me.gbWorkPermitInfo.Size = New System.Drawing.Size(520, 135)
        Me.gbWorkPermitInfo.TabIndex = 44
        Me.gbWorkPermitInfo.Temp = 0
        Me.gbWorkPermitInfo.Text = "Work Permit Effective Date"
        Me.gbWorkPermitInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkWpIssuePlace
        '
        Me.chkWpIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWpIssuePlace.Location = New System.Drawing.Point(297, 58)
        Me.chkWpIssuePlace.Name = "chkWpIssuePlace"
        Me.chkWpIssuePlace.Size = New System.Drawing.Size(126, 17)
        Me.chkWpIssuePlace.TabIndex = 48
        Me.chkWpIssuePlace.Text = "Issue Place"
        Me.chkWpIssuePlace.UseVisualStyleBackColor = True
        '
        'chkWpCountry
        '
        Me.chkWpCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWpCountry.Location = New System.Drawing.Point(143, 35)
        Me.chkWpCountry.Name = "chkWpCountry"
        Me.chkWpCountry.Size = New System.Drawing.Size(126, 17)
        Me.chkWpCountry.TabIndex = 45
        Me.chkWpCountry.Text = "Country"
        Me.chkWpCountry.UseVisualStyleBackColor = True
        '
        'chkWpExpiryDt
        '
        Me.chkWpExpiryDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWpExpiryDt.Location = New System.Drawing.Point(143, 58)
        Me.chkWpExpiryDt.Name = "chkWpExpiryDt"
        Me.chkWpExpiryDt.Size = New System.Drawing.Size(126, 17)
        Me.chkWpExpiryDt.TabIndex = 44
        Me.chkWpExpiryDt.Text = "Expiry Date"
        Me.chkWpExpiryDt.UseVisualStyleBackColor = True
        '
        'chkWpIssueDt
        '
        Me.chkWpIssueDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWpIssueDt.Location = New System.Drawing.Point(10, 58)
        Me.chkWpIssueDt.Name = "chkWpIssueDt"
        Me.chkWpIssueDt.Size = New System.Drawing.Size(126, 17)
        Me.chkWpIssueDt.TabIndex = 41
        Me.chkWpIssueDt.Text = "Issue Date"
        Me.chkWpIssueDt.UseVisualStyleBackColor = True
        '
        'lnkWorkPermitFormat
        '
        Me.lnkWorkPermitFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkWorkPermitFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkWorkPermitFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkWorkPermitFormat.Name = "lnkWorkPermitFormat"
        Me.lnkWorkPermitFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkWorkPermitFormat.TabIndex = 38
        Me.lnkWorkPermitFormat.TabStop = True
        Me.lnkWorkPermitFormat.Text = "Get Import Format"
        Me.lnkWorkPermitFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkWpChangeReason
        '
        Me.chkWpChangeReason.Enabled = False
        Me.chkWpChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWpChangeReason.Location = New System.Drawing.Point(297, 35)
        Me.chkWpChangeReason.Name = "chkWpChangeReason"
        Me.chkWpChangeReason.Size = New System.Drawing.Size(126, 17)
        Me.chkWpChangeReason.TabIndex = 10
        Me.chkWpChangeReason.Text = "Change Reason"
        Me.chkWpChangeReason.UseVisualStyleBackColor = True
        '
        'dtpWpEffDate
        '
        Me.dtpWpEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpWpEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpWpEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpWpEffDate.Name = "dtpWpEffDate"
        Me.dtpWpEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpWpEffDate.TabIndex = 7
        '
        'ChkWorkPermitNo
        '
        Me.ChkWorkPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkWorkPermitNo.Location = New System.Drawing.Point(10, 35)
        Me.ChkWorkPermitNo.Name = "ChkWorkPermitNo"
        Me.ChkWorkPermitNo.Size = New System.Drawing.Size(126, 17)
        Me.ChkWorkPermitNo.TabIndex = 4
        Me.ChkWorkPermitNo.Text = "Permit No"
        Me.ChkWorkPermitNo.UseVisualStyleBackColor = True
        '
        'objlblWorkPermitCaption
        '
        Me.objlblWorkPermitCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblWorkPermitCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblWorkPermitCaption.Location = New System.Drawing.Point(12, 87)
        Me.objlblWorkPermitCaption.Name = "objlblWorkPermitCaption"
        Me.objlblWorkPermitCaption.Size = New System.Drawing.Size(501, 34)
        Me.objlblWorkPermitCaption.TabIndex = 6
        Me.objlblWorkPermitCaption.Text = "#value"
        '
        'gbResidentPermitInfo
        '
        Me.gbResidentPermitInfo.BorderColor = System.Drawing.Color.Black
        Me.gbResidentPermitInfo.Checked = False
        Me.gbResidentPermitInfo.CollapseAllExceptThis = False
        Me.gbResidentPermitInfo.CollapsedHoverImage = Nothing
        Me.gbResidentPermitInfo.CollapsedNormalImage = Nothing
        Me.gbResidentPermitInfo.CollapsedPressedImage = Nothing
        Me.gbResidentPermitInfo.CollapseOnLoad = False
        Me.gbResidentPermitInfo.Controls.Add(Me.chkRpIssuePlace)
        Me.gbResidentPermitInfo.Controls.Add(Me.chkRpCountry)
        Me.gbResidentPermitInfo.Controls.Add(Me.chkRpExpiryDt)
        Me.gbResidentPermitInfo.Controls.Add(Me.chkRpIssueDt)
        Me.gbResidentPermitInfo.Controls.Add(Me.lnkResidentPermitFormat)
        Me.gbResidentPermitInfo.Controls.Add(Me.chkRpChangeReason)
        Me.gbResidentPermitInfo.Controls.Add(Me.dtpRpEffDate)
        Me.gbResidentPermitInfo.Controls.Add(Me.ChkResidentPermitNo)
        Me.gbResidentPermitInfo.Controls.Add(Me.objlblResidentPermitCaption)
        Me.gbResidentPermitInfo.ExpandedHoverImage = Nothing
        Me.gbResidentPermitInfo.ExpandedNormalImage = Nothing
        Me.gbResidentPermitInfo.ExpandedPressedImage = Nothing
        Me.gbResidentPermitInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbResidentPermitInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbResidentPermitInfo.HeaderHeight = 25
        Me.gbResidentPermitInfo.HeaderMessage = ""
        Me.gbResidentPermitInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbResidentPermitInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbResidentPermitInfo.HeightOnCollapse = 0
        Me.gbResidentPermitInfo.LeftTextSpace = 0
        Me.gbResidentPermitInfo.Location = New System.Drawing.Point(3, 1215)
        Me.gbResidentPermitInfo.Name = "gbResidentPermitInfo"
        Me.gbResidentPermitInfo.OpenHeight = 300
        Me.gbResidentPermitInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbResidentPermitInfo.ShowBorder = True
        Me.gbResidentPermitInfo.ShowCheckBox = True
        Me.gbResidentPermitInfo.ShowCollapseButton = False
        Me.gbResidentPermitInfo.ShowDefaultBorderColor = True
        Me.gbResidentPermitInfo.ShowDownButton = False
        Me.gbResidentPermitInfo.ShowHeader = True
        Me.gbResidentPermitInfo.Size = New System.Drawing.Size(520, 135)
        Me.gbResidentPermitInfo.TabIndex = 46
        Me.gbResidentPermitInfo.Temp = 0
        Me.gbResidentPermitInfo.Text = "Resident Permit Effective Date"
        Me.gbResidentPermitInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkRpIssuePlace
        '
        Me.chkRpIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRpIssuePlace.Location = New System.Drawing.Point(297, 58)
        Me.chkRpIssuePlace.Name = "chkRpIssuePlace"
        Me.chkRpIssuePlace.Size = New System.Drawing.Size(126, 17)
        Me.chkRpIssuePlace.TabIndex = 49
        Me.chkRpIssuePlace.Text = "Issue Place"
        Me.chkRpIssuePlace.UseVisualStyleBackColor = True
        '
        'chkRpCountry
        '
        Me.chkRpCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRpCountry.Location = New System.Drawing.Point(143, 35)
        Me.chkRpCountry.Name = "chkRpCountry"
        Me.chkRpCountry.Size = New System.Drawing.Size(126, 17)
        Me.chkRpCountry.TabIndex = 45
        Me.chkRpCountry.Text = "Country"
        Me.chkRpCountry.UseVisualStyleBackColor = True
        '
        'chkRpExpiryDt
        '
        Me.chkRpExpiryDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRpExpiryDt.Location = New System.Drawing.Point(143, 58)
        Me.chkRpExpiryDt.Name = "chkRpExpiryDt"
        Me.chkRpExpiryDt.Size = New System.Drawing.Size(126, 17)
        Me.chkRpExpiryDt.TabIndex = 44
        Me.chkRpExpiryDt.Text = "Expiry Date"
        Me.chkRpExpiryDt.UseVisualStyleBackColor = True
        '
        'chkRpIssueDt
        '
        Me.chkRpIssueDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRpIssueDt.Location = New System.Drawing.Point(10, 58)
        Me.chkRpIssueDt.Name = "chkRpIssueDt"
        Me.chkRpIssueDt.Size = New System.Drawing.Size(126, 17)
        Me.chkRpIssueDt.TabIndex = 41
        Me.chkRpIssueDt.Text = "Issue Date"
        Me.chkRpIssueDt.UseVisualStyleBackColor = True
        '
        'lnkResidentPermitFormat
        '
        Me.lnkResidentPermitFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkResidentPermitFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkResidentPermitFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkResidentPermitFormat.Name = "lnkResidentPermitFormat"
        Me.lnkResidentPermitFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkResidentPermitFormat.TabIndex = 38
        Me.lnkResidentPermitFormat.TabStop = True
        Me.lnkResidentPermitFormat.Text = "Get Import Format"
        Me.lnkResidentPermitFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkRpChangeReason
        '
        Me.chkRpChangeReason.Enabled = False
        Me.chkRpChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRpChangeReason.Location = New System.Drawing.Point(297, 35)
        Me.chkRpChangeReason.Name = "chkRpChangeReason"
        Me.chkRpChangeReason.Size = New System.Drawing.Size(126, 17)
        Me.chkRpChangeReason.TabIndex = 10
        Me.chkRpChangeReason.Text = "Change Reason"
        Me.chkRpChangeReason.UseVisualStyleBackColor = True
        '
        'dtpRpEffDate
        '
        Me.dtpRpEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpRpEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRpEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpRpEffDate.Name = "dtpRpEffDate"
        Me.dtpRpEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpRpEffDate.TabIndex = 7
        '
        'ChkResidentPermitNo
        '
        Me.ChkResidentPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkResidentPermitNo.Location = New System.Drawing.Point(10, 35)
        Me.ChkResidentPermitNo.Name = "ChkResidentPermitNo"
        Me.ChkResidentPermitNo.Size = New System.Drawing.Size(126, 17)
        Me.ChkResidentPermitNo.TabIndex = 4
        Me.ChkResidentPermitNo.Text = "Permit No"
        Me.ChkResidentPermitNo.UseVisualStyleBackColor = True
        '
        'objlblResidentPermitCaption
        '
        Me.objlblResidentPermitCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblResidentPermitCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblResidentPermitCaption.Location = New System.Drawing.Point(12, 87)
        Me.objlblResidentPermitCaption.Name = "objlblResidentPermitCaption"
        Me.objlblResidentPermitCaption.Size = New System.Drawing.Size(501, 34)
        Me.objlblResidentPermitCaption.TabIndex = 6
        Me.objlblResidentPermitCaption.Text = "#value"
        '
        'gbExemptionInfo
        '
        Me.gbExemptionInfo.BorderColor = System.Drawing.Color.Black
        Me.gbExemptionInfo.Checked = False
        Me.gbExemptionInfo.CollapseAllExceptThis = False
        Me.gbExemptionInfo.CollapsedHoverImage = Nothing
        Me.gbExemptionInfo.CollapsedNormalImage = Nothing
        Me.gbExemptionInfo.CollapsedPressedImage = Nothing
        Me.gbExemptionInfo.CollapseOnLoad = False
        Me.gbExemptionInfo.Controls.Add(Me.chkExemptionToDt)
        Me.gbExemptionInfo.Controls.Add(Me.lnkExemptionFormat)
        Me.gbExemptionInfo.Controls.Add(Me.chkExemptionChangeReason)
        Me.gbExemptionInfo.Controls.Add(Me.dtpExemptionEffDate)
        Me.gbExemptionInfo.Controls.Add(Me.chkExemptionFrmDt)
        Me.gbExemptionInfo.Controls.Add(Me.objlblExemptionCaption)
        Me.gbExemptionInfo.ExpandedHoverImage = Nothing
        Me.gbExemptionInfo.ExpandedNormalImage = Nothing
        Me.gbExemptionInfo.ExpandedPressedImage = Nothing
        Me.gbExemptionInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExemptionInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExemptionInfo.HeaderHeight = 25
        Me.gbExemptionInfo.HeaderMessage = ""
        Me.gbExemptionInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbExemptionInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExemptionInfo.HeightOnCollapse = 0
        Me.gbExemptionInfo.LeftTextSpace = 0
        Me.gbExemptionInfo.Location = New System.Drawing.Point(3, 1356)
        Me.gbExemptionInfo.Name = "gbExemptionInfo"
        Me.gbExemptionInfo.OpenHeight = 300
        Me.gbExemptionInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExemptionInfo.ShowBorder = True
        Me.gbExemptionInfo.ShowCheckBox = True
        Me.gbExemptionInfo.ShowCollapseButton = False
        Me.gbExemptionInfo.ShowDefaultBorderColor = True
        Me.gbExemptionInfo.ShowDownButton = False
        Me.gbExemptionInfo.ShowHeader = True
        Me.gbExemptionInfo.Size = New System.Drawing.Size(520, 135)
        Me.gbExemptionInfo.TabIndex = 47
        Me.gbExemptionInfo.Temp = 0
        Me.gbExemptionInfo.Text = "Exemption Effective Date"
        Me.gbExemptionInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExemptionToDt
        '
        Me.chkExemptionToDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExemptionToDt.Location = New System.Drawing.Point(10, 58)
        Me.chkExemptionToDt.Name = "chkExemptionToDt"
        Me.chkExemptionToDt.Size = New System.Drawing.Size(126, 17)
        Me.chkExemptionToDt.TabIndex = 41
        Me.chkExemptionToDt.Text = "To Date"
        Me.chkExemptionToDt.UseVisualStyleBackColor = True
        '
        'lnkExemptionFormat
        '
        Me.lnkExemptionFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkExemptionFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkExemptionFormat.Location = New System.Drawing.Point(272, 4)
        Me.lnkExemptionFormat.Name = "lnkExemptionFormat"
        Me.lnkExemptionFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkExemptionFormat.TabIndex = 38
        Me.lnkExemptionFormat.TabStop = True
        Me.lnkExemptionFormat.Text = "Get Import Format"
        Me.lnkExemptionFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkExemptionChangeReason
        '
        Me.chkExemptionChangeReason.Enabled = False
        Me.chkExemptionChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExemptionChangeReason.Location = New System.Drawing.Point(143, 35)
        Me.chkExemptionChangeReason.Name = "chkExemptionChangeReason"
        Me.chkExemptionChangeReason.Size = New System.Drawing.Size(126, 17)
        Me.chkExemptionChangeReason.TabIndex = 10
        Me.chkExemptionChangeReason.Text = "Change Reason"
        Me.chkExemptionChangeReason.UseVisualStyleBackColor = True
        '
        'dtpExemptionEffDate
        '
        Me.dtpExemptionEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpExemptionEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpExemptionEffDate.Location = New System.Drawing.Point(410, 2)
        Me.dtpExemptionEffDate.Name = "dtpExemptionEffDate"
        Me.dtpExemptionEffDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpExemptionEffDate.TabIndex = 7
        '
        'chkExemptionFrmDt
        '
        Me.chkExemptionFrmDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExemptionFrmDt.Location = New System.Drawing.Point(10, 35)
        Me.chkExemptionFrmDt.Name = "chkExemptionFrmDt"
        Me.chkExemptionFrmDt.Size = New System.Drawing.Size(126, 17)
        Me.chkExemptionFrmDt.TabIndex = 4
        Me.chkExemptionFrmDt.Text = "From Date"
        Me.chkExemptionFrmDt.UseVisualStyleBackColor = True
        '
        'objlblExemptionCaption
        '
        Me.objlblExemptionCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExemptionCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblExemptionCaption.Location = New System.Drawing.Point(12, 87)
        Me.objlblExemptionCaption.Name = "objlblExemptionCaption"
        Me.objlblExemptionCaption.Size = New System.Drawing.Size(501, 34)
        Me.objlblExemptionCaption.TabIndex = 6
        Me.objlblExemptionCaption.Text = "#value"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(695, 92)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(28, 21)
        Me.btnOpenFile.TabIndex = 21
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(177, 92)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(512, 21)
        Me.txtFilePath.TabIndex = 20
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(173, 22)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(432, 23)
        Me.lblTitle.TabIndex = 22
        Me.lblTitle.Text = "Employee Movement Update Wizard"
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(177, 72)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(126, 17)
        Me.lblSelectfile.TabIndex = 19
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wizPageData
        '
        Me.wizPageData.Controls.Add(Me.btnFilter)
        Me.wizPageData.Controls.Add(Me.dgData)
        Me.wizPageData.Controls.Add(Me.pnlInfo)
        Me.wizPageData.Location = New System.Drawing.Point(0, 0)
        Me.wizPageData.Name = "wizPageData"
        Me.wizPageData.Size = New System.Drawing.Size(770, 470)
        Me.wizPageData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizPageData.TabIndex = 9
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 434)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 24
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhStatus, Me.colhMessage, Me.objdgcolhEmployeeId, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(747, 359)
        Me.dgData.TabIndex = 20
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.Visible = False
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(747, 51)
        Me.pnlInfo.TabIndex = 3
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(625, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(511, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(625, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(557, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(670, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(511, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(670, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(557, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbuttonSave
        '
        Me.objbuttonSave.BackColor = System.Drawing.Color.White
        Me.objbuttonSave.BackgroundImage = CType(resources.GetObject("objbuttonSave.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonSave.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonSave.FlatAppearance.BorderSize = 0
        Me.objbuttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonSave.ForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonSave.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.Location = New System.Drawing.Point(229, 224)
        Me.objbuttonSave.Name = "objbuttonSave"
        Me.objbuttonSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.Size = New System.Drawing.Size(99, 29)
        Me.objbuttonSave.TabIndex = 0
        Me.objbuttonSave.Text = "Save && Finish"
        Me.objbuttonSave.UseVisualStyleBackColor = False
        Me.objbuttonSave.Visible = False
        '
        'frmBulkMovementUpdateWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(770, 518)
        Me.Controls.Add(Me.WizUpdateEmployee)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBulkMovementUpdateWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Movement Update Wizard"
        Me.WizUpdateEmployee.ResumeLayout(False)
        Me.wizPageMapping.ResumeLayout(False)
        Me.gbFiledMapping.ResumeLayout(False)
        Me.objpnlCtrls.ResumeLayout(False)
        Me.wizPageFile.ResumeLayout(False)
        Me.wizPageFile.PerformLayout()
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.fpnlData.ResumeLayout(False)
        Me.gbUpdateAllocation.ResumeLayout(False)
        Me.gbCostCenter.ResumeLayout(False)
        Me.gbCategorization.ResumeLayout(False)
        Me.gbEOCInfo.ResumeLayout(False)
        Me.gbRetirementInfo.ResumeLayout(False)
        Me.gbProbationInfo.ResumeLayout(False)
        Me.gbConfirmationInfo.ResumeLayout(False)
        Me.gbSuspensionInfo.ResumeLayout(False)
        Me.gbWorkPermitInfo.ResumeLayout(False)
        Me.gbResidentPermitInfo.ResumeLayout(False)
        Me.gbExemptionInfo.ResumeLayout(False)
        Me.wizPageData.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WizUpdateEmployee As eZee.Common.eZeeWizard
    Friend WithEvents wizPageFile As eZee.Common.eZeeWizardPage
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents fpnlData As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents gbUpdateAllocation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpAllocEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblAllocationCaption As System.Windows.Forms.Label
    Friend WithEvents chkBranch As System.Windows.Forms.CheckBox
    Friend WithEvents chkDepartment As System.Windows.Forms.CheckBox
    Friend WithEvents chkDepGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkSecGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkSections As System.Windows.Forms.CheckBox
    Friend WithEvents chkUnitGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkClassGrp As System.Windows.Forms.CheckBox
    Friend WithEvents chkUnit As System.Windows.Forms.CheckBox
    Friend WithEvents chkTeam As System.Windows.Forms.CheckBox
    Friend WithEvents chkClass As System.Windows.Forms.CheckBox
    Friend WithEvents gbCostCenter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpCCEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblCCCaption As System.Windows.Forms.Label
    Friend WithEvents chkCostCenter As System.Windows.Forms.CheckBox
    Friend WithEvents gbCategorization As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpCatEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlbJobCaption As System.Windows.Forms.Label
    Friend WithEvents chkJobGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkJob As System.Windows.Forms.CheckBox
    Friend WithEvents gbEOCInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpTerEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblEOCCaption As System.Windows.Forms.Label
    Friend WithEvents chkEOCDate As System.Windows.Forms.CheckBox
    Friend WithEvents chkEOCReason As System.Windows.Forms.CheckBox
    Friend WithEvents chkLeavingDate As System.Windows.Forms.CheckBox
    Friend WithEvents gbRetirementInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpRetrEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkRetirementDt As System.Windows.Forms.CheckBox
    Friend WithEvents objlblRetireCaption As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents wizPageData As eZee.Common.eZeeWizardPage
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonSave As eZee.Common.eZeeLightButton
    Friend WithEvents wizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFiledMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboTerminationReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblEOCReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign22 As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents objlblSign21 As System.Windows.Forms.Label
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents objlblSign20 As System.Windows.Forms.Label
    Friend WithEvents cboTeam As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents objlblSign19 As System.Windows.Forms.Label
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents objlblSign18 As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents objlblSign17 As System.Windows.Forms.Label
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents lblSections As System.Windows.Forms.Label
    Friend WithEvents objlblSign16 As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblSecGroup As System.Windows.Forms.Label
    Friend WithEvents objlblSign15 As System.Windows.Forms.Label
    Friend WithEvents cboDepartmentGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepGroup As System.Windows.Forms.Label
    Friend WithEvents objlblSign14 As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents objlblSign13 As System.Windows.Forms.Label
    Friend WithEvents cboLeavingDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeavingDate As System.Windows.Forms.Label
    Friend WithEvents objlblSign12 As System.Windows.Forms.Label
    Friend WithEvents cboEOCDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblECODate As System.Windows.Forms.Label
    Friend WithEvents objlblSign11 As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents objlblSign8 As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGrp As System.Windows.Forms.Label
    Friend WithEvents objlblSign7 As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objlblSign5 As System.Windows.Forms.Label
    Friend WithEvents cboRetirementDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblRetirementDt As System.Windows.Forms.Label
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objelLine4 As eZee.Common.eZeeLine
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents chkAllocationChangeReson As System.Windows.Forms.CheckBox
    Friend WithEvents chkCcChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents chkCateChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents chkRtChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents cboAllocationChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocationReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign23 As System.Windows.Forms.Label
    Friend WithEvents cboCostCenterChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblCCReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign25 As System.Windows.Forms.Label
    Friend WithEvents cboCategorizeChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign24 As System.Windows.Forms.Label
    Friend WithEvents cboRetirementChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblRtReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign26 As System.Windows.Forms.Label
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents chkIsexcludePayroll As System.Windows.Forms.CheckBox
    Friend WithEvents cboIsExcludePayroll As System.Windows.Forms.ComboBox
    Friend WithEvents lblIsexcludePayroll As System.Windows.Forms.Label
    Friend WithEvents objlblSign27 As System.Windows.Forms.Label
    Friend WithEvents lnkAllocationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkCostCenterFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkCategorizeFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkTerminationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkRetirementFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents gbProbationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkProbationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents chkPbChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents dtpPbEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkProbationFromDt As System.Windows.Forms.CheckBox
    Friend WithEvents objlblProbationCaption As System.Windows.Forms.Label
    Friend WithEvents chkProbationToDt As System.Windows.Forms.CheckBox
    Friend WithEvents gbConfirmationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkConfirmationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents chkConfChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents dtpConEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkConfirmationDt As System.Windows.Forms.CheckBox
    Friend WithEvents objlblConfirmationCaption As System.Windows.Forms.Label
    Friend WithEvents gbSuspensionInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkSuspensionToDt As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSuspensionFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents chkSuspChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents dtpSuspEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkSuspensionFrmDt As System.Windows.Forms.CheckBox
    Friend WithEvents objlblSuspensionCaption As System.Windows.Forms.Label
    Friend WithEvents gbWorkPermitInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkWpIssueDt As System.Windows.Forms.CheckBox
    Friend WithEvents lnkWorkPermitFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents chkWpChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents dtpWpEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents ChkWorkPermitNo As System.Windows.Forms.CheckBox
    Friend WithEvents objlblWorkPermitCaption As System.Windows.Forms.Label
    Friend WithEvents chkWpExpiryDt As System.Windows.Forms.CheckBox
    Friend WithEvents chkWpCountry As System.Windows.Forms.CheckBox
    Friend WithEvents gbResidentPermitInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkRpCountry As System.Windows.Forms.CheckBox
    Friend WithEvents chkRpExpiryDt As System.Windows.Forms.CheckBox
    Friend WithEvents chkRpIssueDt As System.Windows.Forms.CheckBox
    Friend WithEvents lnkResidentPermitFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents chkRpChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents dtpRpEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents ChkResidentPermitNo As System.Windows.Forms.CheckBox
    Friend WithEvents objlblResidentPermitCaption As System.Windows.Forms.Label
    Friend WithEvents objpnlCtrls As System.Windows.Forms.Panel
    Friend WithEvents objlblSign28 As System.Windows.Forms.Label
    Friend WithEvents lblProbationFromDt As System.Windows.Forms.Label
    Friend WithEvents cboProbationFromDate As System.Windows.Forms.ComboBox
    Friend WithEvents cboProbationChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblPbReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign29 As System.Windows.Forms.Label
    Friend WithEvents EZeeLine2 As eZee.Common.eZeeLine
    Friend WithEvents objlblSign30 As System.Windows.Forms.Label
    Friend WithEvents lblProbationToDt As System.Windows.Forms.Label
    Friend WithEvents cboProbationToDate As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign31 As System.Windows.Forms.Label
    Friend WithEvents lbConfirmationDt As System.Windows.Forms.Label
    Friend WithEvents cboConfirmationDate As System.Windows.Forms.ComboBox
    Friend WithEvents cboConfirmationChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblConfirmationReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign32 As System.Windows.Forms.Label
    Friend WithEvents EZeeLine3 As eZee.Common.eZeeLine
    Friend WithEvents objlblSign35 As System.Windows.Forms.Label
    Friend WithEvents lblSuspToDt As System.Windows.Forms.Label
    Friend WithEvents cboSuspensionToDate As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign33 As System.Windows.Forms.Label
    Friend WithEvents lblSuspFromDt As System.Windows.Forms.Label
    Friend WithEvents cboSuspensionFromDate As System.Windows.Forms.ComboBox
    Friend WithEvents cboSuspensionChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblSuspensionReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign34 As System.Windows.Forms.Label
    Friend WithEvents EZeeLine4 As eZee.Common.eZeeLine
    Friend WithEvents objlblSign38 As System.Windows.Forms.Label
    Friend WithEvents lblWPIssueDt As System.Windows.Forms.Label
    Friend WithEvents cboWorkPermitIssueDate As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign36 As System.Windows.Forms.Label
    Friend WithEvents lblWPno As System.Windows.Forms.Label
    Friend WithEvents cboWorkPermitNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboWorkPermitChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblWPReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign37 As System.Windows.Forms.Label
    Friend WithEvents EZeeLine5 As eZee.Common.eZeeLine
    Friend WithEvents objlblSign39 As System.Windows.Forms.Label
    Friend WithEvents lblWPExpiryDate As System.Windows.Forms.Label
    Friend WithEvents cboWorkPermitExpiryDate As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign40 As System.Windows.Forms.Label
    Friend WithEvents lblWPCountry As System.Windows.Forms.Label
    Friend WithEvents cboWorkPermitCountry As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign45 As System.Windows.Forms.Label
    Friend WithEvents lblRPCountry As System.Windows.Forms.Label
    Friend WithEvents cboResidentCountry As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign44 As System.Windows.Forms.Label
    Friend WithEvents lblRPExpiryDate As System.Windows.Forms.Label
    Friend WithEvents cboResidentExpiryDate As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign43 As System.Windows.Forms.Label
    Friend WithEvents lblRPIssueDate As System.Windows.Forms.Label
    Friend WithEvents cboResidentIssueDate As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign41 As System.Windows.Forms.Label
    Friend WithEvents lblRPno As System.Windows.Forms.Label
    Friend WithEvents cboResidentPermitNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboResidentChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblRPReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign42 As System.Windows.Forms.Label
    Friend WithEvents EZeeLine6 As eZee.Common.eZeeLine
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents chkWpIssuePlace As System.Windows.Forms.CheckBox
    Friend WithEvents chkRpIssuePlace As System.Windows.Forms.CheckBox
    Friend WithEvents cboWorkPermitIssuePlace As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign46 As System.Windows.Forms.Label
    Friend WithEvents objlblSign47 As System.Windows.Forms.Label
    Friend WithEvents lblWpIssuePlace As System.Windows.Forms.Label
    Friend WithEvents objlblSign48 As System.Windows.Forms.Label
    Friend WithEvents lblRpIssuePlace As System.Windows.Forms.Label
    Friend WithEvents cboResidentIssuePlace As System.Windows.Forms.ComboBox
    Friend WithEvents gbExemptionInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkExemptionToDt As System.Windows.Forms.CheckBox
    Friend WithEvents lnkExemptionFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents chkExemptionChangeReason As System.Windows.Forms.CheckBox
    Friend WithEvents dtpExemptionEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkExemptionFrmDt As System.Windows.Forms.CheckBox
    Friend WithEvents objlblExemptionCaption As System.Windows.Forms.Label
    Friend WithEvents lblExemptionToDt As System.Windows.Forms.Label
    Friend WithEvents cboExemptionToDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblExemptionFromDt As System.Windows.Forms.Label
    Friend WithEvents cboExemptionFromDate As System.Windows.Forms.ComboBox
    Friend WithEvents cboExemptionChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblExemptionReason As System.Windows.Forms.Label
    Friend WithEvents objlblSign50 As System.Windows.Forms.Label
    Friend WithEvents EZeeLine7 As eZee.Common.eZeeLine
    Friend WithEvents objlblSign51 As System.Windows.Forms.Label
    Friend WithEvents objlblSign49 As System.Windows.Forms.Label
End Class
